function loginValidateInit() {
	$("username").focus();
}
function checkform() {
	allowSubmit = 0;
	if ($F("username") == "") {
			$("userError").innerHTML = lang_error_login_username;
			allowSubmit++;
	} else {
			$("userError").innerHTML = "";
	}
	if ($F("password")=="") {
			$("passError").innerHTML = lang_error_login_password;
			allowSubmit++;
	} else {
			$("passError").innerHTML = "";
	}
	if (allowSubmit>0) {
		return false;
	} else {
		return true;
	}
}
addEvent(window,'load', loginValidateInit);
