/* 	umum	*/
function sendRequest() {
	var oForm = document.forms[0];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
    oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
//		alert(oXmlHttp.readyState);
		if (oXmlHttp.readyState == 4) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "";
			if (oXmlHttp.status == 200) {
				saveResult(oXmlHttp.responseText);
			} else {
				saveResult("Ada Error : "+oXmlHttp.statusText);
			}
		}else if(oXmlHttp.readyState == 3){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 2){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 1){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 0){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
//	alert(sBody);
}

function getRequestBody(oForm) {
	var aParams = new Array();
	for (var i=0 ; i < oForm.elements.length; i++) {
		var sParam = encodeURIComponent(oForm.elements[i].name);
		sParam += "=";
		sParam += encodeURIComponent(oForm.elements[i].value);
		aParams.push(sParam);
	}
	return aParams.join("&");
}

function saveResult(sMessage) {
//	alert(sMessage);
	if((sMessage.length>0)
 		&& (sMessage.length<=30)){
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = "### Sukses ###<br>"+ sMessage;
	}else if((sMessage.length>0) && (sMessage.length>30)){
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = sMessage;
	}
}
/*	end umum	*/
function formatulang(a){
	var s = a.replace(/\,/g,'');
	return s;
}

function formatcemua(input){
	var num = input.toString();
	if(!isNaN(num)){
		if(num.indexOf('.') > -1){
			num = num.split('.');
			num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
			if(num[1].length > 2){
				while(num[1].length > 2){
					num[1] = num[1].substring(0,num[1].length-1);
				}
			}
			input = num[0];  
		}else{
			input = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'')
		};
	}
	return input;
}

function reformat(input){
	var num = input.value.replace(/\,/g,'');
	if(!isNaN(num)){
  		if(num.indexOf('.') > -1){
    		num = num.split('.');
    		num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
   			if(num[1].length > 2){
      			alert('maksimum 2 desimal !!!');
      			num[1] = num[1].substring(0,num[1].length-1);
   			}  
			input.value = num[0]+'.'+num[1];        
 		}else{ 
			input.value = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'') 
		};
	}else{ 
		alert('input harus numerik !!!');
     	input.value = input.value.substring(0,input.value.length-1);
	}
}
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
/* transfer	*/
function sendRequestx() {
	var oForm = document.forms[1];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
  oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				saveResultx(oXmlHttp.responseText);
			} else {
//				saveResultx("Ada Error : "+oXmlHttp.statusText);
				saveResultx(oXmlHttp.responseText);
			}
		}else if(oXmlHttp.readyState == 3){
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 2){
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 1){
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 0){
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
//	alert(sBody);
}

function saveResultx(sMessage) {
//	alert(sMessage);
	var divStatus = document.getElementById("hasil");
	divStatus.innerHTML = sMessage;
}
/* end transfer	*/
/* notapprove	*/
function sendRequestxx() {
	var oForm = document.forms[1];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
    oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				saveResultxx(oXmlHttp.responseText);
			} else {
				saveResultxx("Ada Error : "+oXmlHttp.statusText);
			}
		}else if(oXmlHttp.readyState == 3){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 2){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 1){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}else if(oXmlHttp.readyState == 0){
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
}

function saveResultxx(sMessage) {
	if((sMessage.length>0)
 		&& (sMessage.length<=30)){
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = "### Sukses ###<br>"+ sMessage;
	}else if((sMessage.length>0) && (sMessage.length>30)){
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = sMessage;
	}
}
/* end notapprove	*/
function slideSwitch() {
    var $active = $('#slideshow DIV.active');
    if ( $active.length == 0 ) $active = $('#slideshow DIV:last');
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow DIV:first');
    $active.addClass('last-active');
    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

$(function() {
    setInterval( "slideSwitch()", 10000 );
});
/* Tab */
function setSiteURL() { 
   //window.site = "http://mis/dialogue/index.php/";
   window.site = "http://"+location.host+"/index.php/";
} 
function hide(div){
	$(div).hide("slow");	
};
function show(page,div){
//  alert(page);
	$.ajax({
		url:""+site+""+page+"", 
		beforeSend:function(response){
			$(div).slideUp("slow");
		},
		success: function(response){
    		$(div).html(response);
    		$(div).slideDown("slow");
  		},
  		dataType:"html"  		
  	});
  	return false;
};
function showModal(page,div){
	$.ajax({
		url:""+site+""+page+"",
		beforeSend:function(response){
			$(div).html("");
			$(div).slideUp("slow");
		},
		success: function(response){
    		$(div).html(response);
    		$(div).slideDown("slow");
  		},
  		dataType:"html"  		
  	});
  	return false;
};
function hapus(page,div){
	if(confirm("Apakah anda yakin data ini akan dihapus ?")==1){
	$.ajax({
		url:""+site+""+page+"",
		beforeSend:function(response){
			$(div).slideUp("slow");
		},
		success: function(response){
    		$(div).html(response);
    		$(div).slideDown("slow");
  		},
  		dataType:"html"  		
  	});
	}
  	return false;
};
$(document).ready( function() {
	setSiteURL();
	$("#loading").hide();
    $("#loading").ajaxStart(function()
		{
		  	$(this).show();
		});
    $("#loading").ajaxStop(function(){
        $(this).hide();
    });
    $("#submit").click(function(){
	    return false;
    });
});		
/* End  of Tab */
function sitab(konten){
	$("a.active").removeClass("active");  
	$(this).addClass("active");  
	$(".content").slideUp();  
	var content_show = konten;//$(this).attr("title");
	$("#"+content_show).slideDown();  
}
function view_suppliergroup(a){
    lebar =450;
    tinggi=400;
    eval('window.open(a+"/supplier/cform/suppliergroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function cektahun(input){
	var num = input.value.replace(/\,/g,'');
	if(isNaN(num)){
		alert('input harus numerik !!!');
     	input.value = input.value.substring(0,input.value.length-1);
	}
}
function trim(value) {
  value = value.replace(/^\s+/,'');
  value = value.replace(/\s+$/,'');
  return value;
}

function DateAdd(ItemType, DateToWorkOn, ValueToBeAdded)
{
    switch (ItemType)
    {
        case 'd': //add days
            DateToWorkOn.setDate(DateToWorkOn.getDate() + ValueToBeAdded)
            break;
        case 'm': //add months
            DateToWorkOn.setMonth(DateToWorkOn.getMonth() + ValueToBeAdded)
            break;
        case 'y': //add years
            DateToWorkOn.setYear(DateToWorkOn.getFullYear() + ValueToBeAdded)
            break;
        case 'h': //add days
            DateToWorkOn.setHours(DateToWorkOn.getHours() + ValueToBeAdded)
            break;
        case 'n': //add minutes
            DateToWorkOn.setMinutes(DateToWorkOn.getMinutes() + ValueToBeAdded)
            break;
        case 's': //add seconds
            DateToWorkOn.setSeconds(DateToWorkOn.getSeconds() + ValueToBeAdded)
            break;
    }
    return DateToWorkOn;
}
function getJatuhTempo(source,destination,range) {
	var nilai;
/*
  alert(source);
	var tsplit	= source.split('-'); // d-m-Y
	var t	= parseInt(tsplit['0']);
	var tanggal = new Date(tsplit['2'],tsplit['1'],t);//,0,0,0); 
  alert(tanggal);
  var tanggal = new Date();
*/
  tanggal.setDate(tanggal.getDate() + parseInt(range));
  alert(tanggal);
/*
	var tgl	= tanggal.getDate()+parseInt(range);
  tgl=tgl.toString();
  if(tgl.length==1)tgl='0'+tgl;
	var bln = tanggal.getMonth();
  bln=bln.toString();
  if(bln.length==1)bln='0'+bln;
	var thn = tanggal.getFullYear();
	nilai = tgl+'-'+bln+'-'+thn;
	destination.value	= nilai; 
*/	
}
function DateAdds(objDate, strInterval, intIncrement)
{
    if(typeof(objDate) == "string")
    {
        objDate = new Date(objDate);

        if (isNaN(objDate))
        {
            throw("DateAdd: Date is not a valid date");
        }
    }
    else if(typeof(objDate) != "object" || objDate.constructor.toString().indexOf("Date()") == -1)
    {
        throw("DateAdd: First parameter must be a date object");
    }

    if(
    strInterval != "M"
    && strInterval != "D"
    && strInterval != "Y"
    && strInterval != "h"
    && strInterval != "m"
    && strInterval != "uM"
    && strInterval != "uD"
    && strInterval != "uY"
    && strInterval != "uh"
    && strInterval != "um"
    && strInterval != "us"
    )
    {
        throw("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
    }

    if(typeof(intIncrement) != "number")
    {
        throw("DateAdd: Third parameter must be a number");
    }

    switch(strInterval)
    {
        case "M":
        objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
        break;

        case "D":
        objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
        break;

        case "Y":
        objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
        break;

        case "h":
        objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
        break;

        case "m":
        objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
        break;

        case "s":
        objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
        break;

        case "uM":
        objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
        break;

        case "uD":
        objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
        break;

        case "uY":
        objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
        break;

        case "uh":
        objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
        break;

        case "um":
        objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
        break;

        case "us":
        objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
        break;
    }
    return objDate;
}
function formatDate(d) {
  var dd = d.getDate();
  if ( dd < 10 ) dd = '0' + dd;
  var mm = d.getMonth()+1;
  if ( mm < 10 ) mm = '0' + mm;
  var yy = d.getFullYear();
//  if ( yy < 10 ) yy = '0' + yy;
  return dd+'-'+mm+'-'+yy;
}
function gede(a){
  a.value=a.value.toUpperCase();
}
/*
public static void setCacheExpireDate(HttpServletResponse response, int seconds) {
	if (response != null) {
		Calendar cal = new GregorianCalendar();
		cal.roll(Calendar.SECOND, seconds);
		response.setHeader("Cache-Control", "PUBLIC, max-age=" + seconds + ", must-revalidate");
		response.setHeader("Expires", htmlExpiresDateFormat().format(cal.getTime()));
	}
}
public static DateFormat htmlExpiresDateFormat() {
    DateFormat httpDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
    httpDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    return httpDateFormat;
}
*/
