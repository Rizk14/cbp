function sendRequest() {
	var oForm = document.forms[0];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
	oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "";
			if (oXmlHttp.status == 200) {
				saveResult(oXmlHttp.responseText);
			} else {
				saveResult("Ada Error : " + oXmlHttp.statusText);
			}
		} else if (oXmlHttp.readyState == 3) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 2) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 1) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 0) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
}

function getRequestBody(oForm) {
	var aParams = new Array();
	for (var i = 0; i < oForm.elements.length; i++) {
		var sParam = encodeURIComponent(oForm.elements[i].name);
		sParam += "=";
		sParam += encodeURIComponent(oForm.elements[i].value);
		aParams.push(sParam);
	}
	return aParams.join("&");
}

function saveResult(sMessage) {
	if ((sMessage.length > 0) && (sMessage.length <= 30)) {
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = "### Sukses ###<br>" + sMessage;
	} else if ((sMessage.length > 0) && (sMessage.length > 30)) {
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = sMessage;
	}
}

function formatulang(a) {
	var s = a.replace(/\,/g, '');
	return s;
}

function formatcemua(input) {
	var num = input.toString();
	if (!isNaN(num)) {
		if (num.indexOf('.') > -1) {
			num = num.split('.');
			num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1,').split('').reverse().join('').replace(/^[\,]/, '');
			if (num[1].length > 2) {
				while (num[1].length > 2) {
					num[1] = num[1].substring(0, num[1].length - 1);
				}
			}
			input = num[0];
		} else {
			input = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1,').split('').reverse().join('').replace(/^[\,]/, '')
		};
	}
	return input;
}

function reformat(input) {
	var num = input.value.replace(/\,/g, '');
	if (!isNaN(num)) {
		if (num.indexOf('.') > -1) {
			num = num.split('.');
			num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1,').split('').reverse().join('').replace(/^[\,]/, '');
			if (num[1].length > 2) {
				alert('maksimum 2 desimal !!!');
				num[1] = num[1].substring(0, num[1].length - 1);
			}
			input.value = num[0] + '.' + num[1];
		} else {
			input.value = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1,').split('').reverse().join('').replace(/^[\,]/, '');
		}
	} else {
		alert('input harus numerik !!!');
		input.value = input.value.substring(0, input.value.length - 1);
	}
}

function roundNumber(num, dec) {
	var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
	return result;
}

function sendRequestx() {
	var oForm = document.forms[1];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
	oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				saveResultx(oXmlHttp.responseText);
			} else {
				saveResultx(oXmlHttp.responseText);
			}
		} else if (oXmlHttp.readyState == 3) {
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 2) {
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 1) {
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 0) {
			var divStatus = document.getElementById("hasil");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
}

function saveResultx(sMessage) {
	var divStatus = document.getElementById("hasil");
	divStatus.innerHTML = sMessage;
}

function sendRequestxx() {
	var oForm = document.forms[1];
	var sBody = getRequestBody(oForm);
	var oXmlHttp = zXmlHttp.createRequest();
	oXmlHttp.open("post", oForm.action, true);
	oXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	oXmlHttp.onreadystatechange = function () {
		if (oXmlHttp.readyState == 4) {
			if (oXmlHttp.status == 200) {
				saveResultxx(oXmlHttp.responseText);
			} else {
				saveResultxx("Ada Error : " + oXmlHttp.statusText);
			}
		} else if (oXmlHttp.readyState == 3) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 2) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 1) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		} else if (oXmlHttp.readyState == 0) {
			var divStatus = document.getElementById("pesan");
			divStatus.innerHTML = "<center><br /><img src='../../img/loader.gif'><br /><b>Sedang dalam proses ...</b></center>";
		}
	};
	oXmlHttp.send(sBody);
}

function saveResultxx(sMessage) {
	if ((sMessage.length > 0) && (sMessage.length <= 30)) {
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = "### Sukses ###<br>" + sMessage;
	} else if ((sMessage.length > 0) && (sMessage.length > 30)) {
		var divStatus = document.getElementById("pesan");
		divStatus.innerHTML = sMessage;
	}
}

function slideSwitch() {
	var $active = $('#slideshow DIV.active');
	if ($active.length == 0) $active = $('#slideshow DIV:last');
	var $next = $active.next().length ? $active.next() : $('#slideshow DIV:first');
	$active.addClass('last-active');
	$next.css({
		opacity: 0.0
	}).addClass('active').animate({
		opacity: 1.0
	}, 1000, function () {
		$active.removeClass('active last-active');
	});
}
$(function () {
	setInterval("slideSwitch()", 10000);
});

function setSiteURL() {
	var full_url = window.location.href;
	var baseurlx = full_url.split('/');
	var baseurl  = baseurlx[0]+"//"+baseurlx[2]+"/"+baseurlx[3]+"/index.php/";
	window.site = baseurl;
}

function hide(div) {
	$(div).hide("slow");
};

function show(page, div) {
	$.ajax({
		url: "" + site + "" + page + "",
		beforeSend: function (response) {
			$(div).slideUp("slow");
		},
		success: function (response) {
			$(div).html(response);
			$(div).slideDown("slow");
		},
		dataType: "html"
	});
	return false;
};

function showModal(page, div) {
	$.ajax({
		url: "" + site + "" + page + "",
		beforeSend: function (response) {
			$(div).html("");
			$(div).slideUp("slow");
		},
		success: function (response) {
			$(div).html(response);
			$(div).slideDown("slow");
		},
		dataType: "html"
	});
	return false;
};

function hapus(page, div) {
	if (confirm("Apakah anda yakin data ini akan dihapus ?") == 1) {
		$.ajax({
			url: "" + site + "" + page + "",
			beforeSend: function (response) {
				$(div).slideUp("slow");
			},
			success: function (response) {
				$(div).html(response);
				$(div).slideDown("slow");
			},
			dataType: "html"
		});
	}
	return false;
};
function reprint(page, div) {
	if (confirm("Yakin akan membuka akses cetak ulang?") == 1) {
	  $.ajax({
		url: "" + site + "" + page + "",
		beforeSend: function(response) {
		  $(div).slideUp("slow");
		},
		success: function(response) {
		  $(div).html(response);
		  $(div).slideDown("slow");
		},
		dataType: "html"
	  });
	}
	return false;
  }
$(document).ready(function () {
	setSiteURL();
	$("#loading").hide();
	$("#loading").ajaxStart(function () {
		$(this).show();
	});
	$("#loading").ajaxStop(function () {
		$(this).hide();
	});
	$("#submit").click(function () {
		return false;
	});
});

function sitab(konten) {
	$("a.active").removeClass("active");
	$(this).addClass("active");
	$(".content").slideUp();
	var content_show = konten;
	$("#" + content_show).slideDown();
}

function view_suppliergroup(a) {
	lebar = 450;
	tinggi = 400;
	eval('window.open(a+"/supplier/cform/suppliergroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
}

function cektahun(input) {
	var num = input.value.replace(/\,/g, '');
	if (isNaN(num)) {
		alert('input harus numerik !!!');
		input.value = input.value.substring(0, input.value.length - 1);
	}
}

function trim(value) {
	value = value.replace(/^\s+/, '');
	value = value.replace(/\s+$/, '');
	return value;
}

function DateAdd(ItemType, DateToWorkOn, ValueToBeAdded) {
	switch (ItemType) {
		case 'd':
		DateToWorkOn.setDate(DateToWorkOn.getDate() + ValueToBeAdded)
		break;
		case 'm':
		DateToWorkOn.setMonth(DateToWorkOn.getMonth() + ValueToBeAdded)
		break;
		case 'y':
		DateToWorkOn.setYear(DateToWorkOn.getFullYear() + ValueToBeAdded)
		break;
		case 'h':
		DateToWorkOn.setHours(DateToWorkOn.getHours() + ValueToBeAdded)
		break;
		case 'n':
		DateToWorkOn.setMinutes(DateToWorkOn.getMinutes() + ValueToBeAdded)
		break;
		case 's':
		DateToWorkOn.setSeconds(DateToWorkOn.getSeconds() + ValueToBeAdded)
		break;
	}
	return DateToWorkOn;
}

function getJatuhTempo(source, destination, range) {
	var nilai;
	tanggal.setDate(tanggal.getDate() + parseInt(range));
	alert(tanggal);
}

function DateAdds(objDate, strInterval, intIncrement) {
	if (typeof (objDate) == "string") {
		objDate = new Date(objDate);
		if (isNaN(objDate)) {
			throw ("DateAdd: Date is not a valid date");
		}
	} else if (typeof (objDate) != "object" || objDate.constructor.toString().indexOf("Date()") == -1) {
		throw ("DateAdd: First parameter must be a date object");
	}
	if (strInterval != "M" && strInterval != "D" && strInterval != "Y" && strInterval != "h" && strInterval != "m" && strInterval != "uM" && strInterval != "uD" && strInterval != "uY" && strInterval != "uh" && strInterval != "um" && strInterval != "us") {
		throw ("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
	}
	if (typeof (intIncrement) != "number") {
		throw ("DateAdd: Third parameter must be a number");
	}
	switch (strInterval) {
		case "M":
		objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
		break;
		case "D":
		objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
		break;
		case "Y":
		objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
		break;
		case "h":
		objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
		break;
		case "m":
		objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
		break;
		case "s":
		objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
		break;
		case "uM":
		objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
		break;
		case "uD":
		objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
		break;
		case "uY":
		objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
		break;
		case "uh":
		objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
		break;
		case "um":
		objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
		break;
		case "us":
		objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
		break;
	}
	return objDate;
}

function formatDate(d) {
	var dd = d.getDate();
	if (dd < 10) dd = '0' + dd;
	var mm = d.getMonth() + 1;
	if (mm < 10) mm = '0' + mm;
	var yy = d.getFullYear();
	return dd + '-' + mm + '-' + yy;
}

function gede(a) {
	a.value = a.value.toUpperCase();
}

function no_comma(a){
	var s = a.replace(/\,/g,'');
	return s;
}

function reNumber(input){
	return parseFloat(no_comma(input));
}

function disable(){
	setTimeout(function(){
		$(":submit").prop('disabled',true);
	}, 100);
}


var date_diff_indays = function(d1,d2){
	var dt1 = d1.split('-');
	var dt2 = d2.split('-');
	var dat1 = '';
	var dat2 = '';

	if(dt1[0].length == 4){
		dat1 = dt1[0]+"-"+dt1[1]+"-"+dt1[2];
	}else{
		dat1 = dt1[2]+"-"+dt1[1]+"-"+dt1[0];
	}

	if(dt2[0].length == 4){
		dat2 = dt2[0]+"-"+dt2[1]+"-"+dt2[2];
	}else{
		dat2 = dt2[2]+"-"+dt2[1]+"-"+dt2[0];
	}

	var date1 = new Date(dat1);
	var date2 = new Date(dat2);

	return Math.floor( (Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate()) - Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate()) ) /(86400000) );
}

var date_diff_months = function(d1,d2){
	if(d1 == '' || d2 == ''){
		return 0;
	}else{
		var dt1 = d1.split('-');
		var dt2 = d2.split('-');
		var dat1 = '';
		var dat2 = '';

		if(dt1[0].length == 4){
			dat1 = dt1[0]+"-"+dt1[1]+"-"+dt1[2];
		}else{
			dat1 = dt1[2]+"-"+dt1[1]+"-"+dt1[0];
		}

		if(dt2[0].length == 4){
			dat2 = dt2[0]+"-"+dt2[1]+"-"+dt2[2];
		}else{
			dat2 = dt2[2]+"-"+dt2[1]+"-"+dt2[0];
		}

		var date1 = new Date(dat1);
		var date2 = new Date(dat2);
		var years = (date2.getFullYear() - date1.getFullYear()) * 12;
		var months1 = date1.getMonth()+1;
		var months2 = date2.getMonth()+1;
		return years-months1+months2;
	}
}

function comma(input){
	var num = input.toString();
	if(!isNaN(num)){
		if(num.indexOf('.') > -1){
			num = num.split('.');
			num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');
			if(num[1].length > 2){
				while(num[1].length > 2){
					num[1] = num[1].substring(0,num[1].length-1);
				}
			}
			input = num[0];
		}else{
			input = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'')
		};
	}
	return input;
}

var date_diff_indays = function(d1,d2){
		var dt1 = d1.split('-');
		var dt2 = d2.split('-');
		var dat1 = '';
		var dat2 = '';

		if(dt1[0].length == 4){
			dat1 = dt1[0]+"-"+dt1[1]+"-"+dt1[2];
		}else{
			dat1 = dt1[2]+"-"+dt1[1]+"-"+dt1[0];
		}

		if(dt2[0].length == 4){
			dat2 = dt2[0]+"-"+dt2[1]+"-"+dt2[2];
		}else{
			dat2 = dt2[2]+"-"+dt2[1]+"-"+dt2[0];
		}

		var date1 = new Date(dat1);
		var date2 = new Date(dat2);

		return Math.floor( (Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate()) - Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate()) ) /(86400000) );
	}
	function balik(page, div) {
		if (
		  confirm("Apakah anda yakin data ini akan dikembalikan ke status sales ?") ==
		  1
		) {
		  $.ajax({
			url: "" + site + "" + page + "",
			beforeSend: function(response) {
			  $(div).slideUp("slow");
			},
			success: function(response) {
			  $(div).html(response);
			  $(div).slideDown("slow");
			},
			dataType: "html"
		  });
		}
		return false;
	  }
	function taxParse(dreference){
		$.ajax({
			type     : 'post',
			dataType : 'json',
			url      : window.site + 'main/taxvalue/' + dreference + '/',
			async    : false,
			global   : false,
			success  : function(res){
				taxVal   = parseFloat(res.n_tax);
				taxValue = parseFloat(res.n_tax_val);
				exlValue = parseFloat(res.excl_divider);
			}
		})
	}

	function gede(a) {
		a.value = a.value.toUpperCase();
	}
	
	function hanyaAngka(e) {
		e.value = e.value.replace(/[^\d.-]/g, '');
	}
