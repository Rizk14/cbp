<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iap,$isupplier,$iop) 
    {
			$this->db->query("update tm_ap set f_ap_cancel='t' WHERE i_ap='$iap' and i_supplier='$isupplier'");
			$this->db->query("update tm_op set f_op_close='f' WHERE i_op='$iop' and i_supplier='G0000' ",False);
    }
  /* function bacasemua($cari, $num,$offset)
    {
			$this->db->select(" a.*, b.e_supplier_name from tm_ap a, tr_supplier b
								where a.i_supplier=b.i_supplier 
								and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
								or upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')
								order by a.i_ap desc",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }  
    function cari($cari,$num,$offset)
    {
			$this->db->select(" a.*, b.e_supplier_name from tm_ap a, tr_supplier b
								where a.i_supplier=b.i_supplier 
								and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
								or upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    } */
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.i_ap, a.i_supplier, a.i_op, a.i_area, a.d_ap, a.v_ap_gross,
													a.i_op, a.f_ap_cancel, b.e_supplier_name
													from tm_ap a, tr_supplier b
													where a.i_supplier=b.i_supplier 
													and (upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')
													and a.i_supplier='$isupplier' and
													a.d_ap >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_ap <= to_date('$dto','dd-mm-yyyy')
													order by a.i_ap desc ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
   /* function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.i_ap, a.i_supplier, a.i_op, a.i_area, a.d_ap, a.v_ap_gross,
													a.i_op, a.f_ap_cancel, b.e_supplier_name from tm_ap a, tr_supplier b
													where a.i_supplier=b.i_supplier 
													and (upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')
													and a.i_supplier='$isupplier' and
													a.d_ap >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_ap <= to_date('$dto','dd-mm-yyyy')
													order by a.i_ap desc ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    } */
    function bacasupplier($num,$offset)
    {
			$this->db->select(" * from tr_supplier",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function carisupplier($cari,$num,$offset)
    {
			$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
								order by i_supplier",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
