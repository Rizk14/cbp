<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($isupplier,$cari,$num,$offset,$dfrom,$dto)
    {
		  $this->db->select(" distinct a.*, b.e_supplier_name, b.e_supplier_npwp, d.i_pajak, d.d_pajak, d.v_netto from tm_bbkretur a, tr_supplier b, tm_bbkretur_item c
                              left join tm_dtap d on (c.i_dtap=d.i_dtap)
                              where a.i_supplier=b.i_supplier and a.i_bbkretur=c.i_bbkretur
				  			  and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
				  			  or upper(a.i_bbkretur) like '%$cari%')
                              and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
                              and a.i_supplier='$isupplier' and a.i_kn is not null
                              order by a.i_bbkretur",false)->limit($num,$offset);
#and (a.d_cetak isnull)
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ibbkretur,$isupplier)
    {
		  $this->db->select(" distinct a.*, b.e_supplier_name, b.e_supplier_address, b.f_supplier_ppn, e_supplier_city, b.e_supplier_npwp, d.i_pajak, d.d_pajak
		  					  ,d.v_netto , d.v_gross, d.v_discount
		  					  from tm_bbkretur a, tr_supplier b, tm_bbkretur_item c 
		  					  left join tm_dtap d on (c.i_dtap=d.i_dtap)
                              where a.i_supplier=b.i_supplier and a.i_bbkretur=c.i_bbkretur
                              and a.i_supplier='$isupplier' and a.i_bbkretur='$ibbkretur'
                              order by a.i_bbkretur",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ibbkretur,$isupplier)
    {
		  
		  $this->db->select(" * from tm_bbkretur_item
					  inner join tr_product_motif on (tm_bbkretur_item.i_product_motif=tr_product_motif.i_product_motif
					  and tm_bbkretur_item.i_product=tr_product_motif.i_product)
					  where i_bbkretur = '$ibbkretur' and i_supplier='$isupplier' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("	 distinct a.*, b.e_supplier_name, b.e_supplier_npwp, d.i_pajak, d.v_netto from tm_bbkretur a, tr_supplier b, tm_bbkretur_item c
                              left join tm_dtap d on (c.i_dtap=d.i_dtap)
                              where a.i_supplier=b.i_supplier and a.i_bbkretur=c.i_bbkretur
				  			  and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
				  			  or upper(a.i_bbkretur) like '%$cari%')
                              and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
                              and a.i_supplier='$isupplier'
                              order by a.i_bbkretur",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select("	* FROM tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
							ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($area,$ikn)
    {
		$this->db->query("	update tm_bbkretur set n_print=n_print+1 
          							where i_kn = '$ikn' and i_area = '$area' ",false);
    }
}
?>
