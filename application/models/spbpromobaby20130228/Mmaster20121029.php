<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispb,$iarea)
    {
	    $this->db->select(" a.e_remark1 AS emark1, a.*, e.e_price_groupname, f.e_promo_name, b.i_customer_group,
                          d.e_area_name, b.e_customer_name, b.e_customer_address, c.e_salesman_name, b.f_customer_first
                          from tm_spb a
    			                left join tm_promo f on (a.i_spb_program=f.i_promo)
                          inner join tr_customer b on (a.i_customer=b.i_customer)
                          inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                          inner join tr_customer_area d on (a.i_customer=d.i_customer)
                          left join tr_price_group e on (a.i_price_group=e.i_price_group)
                          where a.i_spb ='$ispb' and a.i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($ispb,$iarea)
    {
		  $this->db->select("a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				                 where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				                 order by a.n_item_no", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    /*	
    function bacadetailnilaispb($ispb,$iarea)
    {
		return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a, tr_product_motif b, tr_product_price c
			        where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif and a.i_product=c.i_product ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea)
    {
		return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a, tr_product_motif b, tr_product_price c
			        where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif and a.i_product=c.i_product ", false);
    }
    */
    function bacadetailnilaispb($ispb,$iarea)
    {
		return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a
			        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea)
    {
		return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
			        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function insertheader($ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
						 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb, 
						 $fspbconsigment, $ispbprogram, $ispbold, $eremark1)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_spb'		=> $ispb,
			'd_spb'		=> $dspb,
			'i_customer'	=> $icustomer,
			'i_area'	=> $iarea,
			'i_spb_po'	=> $ispbpo,
			'i_spb_program'	=> $ispbprogram,
			'n_spb_toplength'	=> $nspbtoplength,
			'i_salesman' 		=> $isalesman,
			'i_price_group' 	=> $ipricegroup,
			'd_spb_receive'		=> $dspb,
			'f_spb_op'		=> $fspbop,
			'e_customer_pkpnpwp'	=> $ecustomerpkpnpwp,
			'f_spb_pkp'		=> $fspbpkp,
			'f_spb_plusppn'		=> $fspbplusppn,
			'f_spb_plusdiscount'	=> $fspbplusdiscount,
			'f_spb_stockdaerah'	=> $fspbstockdaerah,
			'f_spb_program' 	=> $fspbprogram,
			'f_spb_valid' 		=> $fspbvalid,
			'f_spb_siapnotagudang'	=> $fspbsiapnota,
			'f_spb_cancel' 		=> $fspbcancel,
			'n_spb_discount1' 	=> $nspbdiscount1,
			'n_spb_discount2' 	=> $nspbdiscount2,
			'n_spb_discount3' 	=> $nspbdiscount3,
			'n_spb_discount4' 	=> $nspbdiscount4,
			'v_spb_discount1' 	=> $vspbdiscount1,
			'v_spb_discount2' 	=> $vspbdiscount2,
			'v_spb_discount3' 	=> $vspbdiscount3,
			'v_spb_discount4' 	=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 		=> $vspb,
			'f_spb_consigment' 	=> $fspbconsigment,
			'd_spb_entry'		=> $dentry,
			'i_spb_old'		=> $ispbold,
			'i_product_group'	=> '01',
			'e_remark1'		=> $eremark1
    		)
    	);
    	
    	$this->db->insert('tm_spb');
    }
    function insertheadersjc($isjc, $dsjc, $ispb, $dspb, $icustomer, $iarea, $isalesman, 
							 $fsjccancel, $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3, 
							 $vsjcdiscount1, $vsjcdiscount2, $vsjcdiscount3, $vsjcgross,
							 $vsjcdiscounttotal, $vsjcnetto, $isjctype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_sjc'			=> $isjc,
			'i_sjc_type'		=> $isjctype,
			'd_sjc'			=> $dsjc,
			'i_spb'			=> $ispb,
			'd_spb'			=> $dspb,
			'i_customer'		=> $icustomer,
			'i_area'		=> $iarea,
			'i_salesman' 		=> $isalesman,
			'f_sjc_cancel' 		=> $fsjccancel,
			'n_sjc_discount1' 	=> $nsjcdiscount1,
			'n_sjc_discount2' 	=> $nsjcdiscount2,
			'n_sjc_discount3' 	=> $nsjcdiscount3,
			'v_sjc_discount1' 	=> $vsjcdiscount1,
			'v_sjc_discount2' 	=> $vsjcdiscount2,
			'v_sjc_discount3' 	=> $vsjcdiscount3,
			'v_sjc_gross'		=> $vsjcgross,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_netto'		=> $vsjcnetto,
			'd_sjc_entry'		=> $dentry
    		)
    	);
    	
    	$this->db->insert('tm_sjc');
    }
    function insertdetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,$iproductmotif,$eremark,$i)
    {
	if($eremark=='0') $eremark=null;
    	$this->db->set(
    		array(
				'i_spb'		 => $ispb,
				'i_area'    	 => $iarea,
				'i_product'	 => $iproduct,
        'i_product_status'=> $iproductstatus,
				'i_product_grade'=> $iproductgrade,
				'i_product_motif'=> $iproductmotif,
				'n_order'   	=> $norder,
        'n_deliver'  	=> $ndeliver,
				'v_unit_price'	=> $vunitprice,
				'e_product_name'=> $eproductname,
				'e_remark'	=> $eremark,
          			'n_item_no'     => $i
    		)
    	);
    	$this->db->insert('tm_spb_item');
    }
    function insertdetailsjc($isjc,$iarea,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nquantity,$vunitprice,$isjctype)
    {
    	$this->db->set(
    		array(
				'i_sjc'			=> $isjc,
				'i_area'		=> $iarea,
				'i_product'		=> $iproduct,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'e_product_name'	=> $eproductname,
				'n_quantity'		=> $nquantity,
				'v_unit_price'		=> $vunitprice,
				'i_sjc_type'		=> $isjctype
    		)
    	);
    	$this->db->insert('tm_sjc_item');
    }
    function updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
						 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb, 
             $fspbconsigment,$ispbold,$eremark1,$ispbprogram)
    {
	/*
	* 'f_spb_stockdaerah'	=> $fspbstockdaerah,
	* 'f_spb_siapnotagudang'	=> $fspbsiapnota,
	* 'f_spb_valid' 		=> $fspbvalid,
	*/
	$query 		= $this->db->query("SELECT current_timestamp as c");
	$row   		= $query->row();
	$dspbupdate	= $row->c;
    	$data = array(	'd_spb'			=> $dspb,
			'i_customer'		=> $icustomer,
			'i_spb_po'		=> $ispbpo,
			'n_spb_toplength'	=> $nspbtoplength,
			'i_salesman' 		=> $isalesman,
			'i_price_group' 	=> $ipricegroup,
			'd_spb_receive'		=> $dspb,
      'i_spb_program'   => $ispbprogram,
			'f_spb_op'		=> $fspbop,
			'e_customer_pkpnpwp'	=> $ecustomerpkpnpwp,
			'f_spb_pkp'		=> $fspbpkp,
			'f_spb_plusppn'		=> $fspbplusppn,
			'f_spb_plusdiscount'	=> $fspbplusdiscount,
			'f_spb_stockdaerah'	=> $fspbstockdaerah,
			'f_spb_program' 	=> $fspbprogram,
			'f_spb_valid' 		=> $fspbvalid,
			'f_spb_siapnotagudang'	=> $fspbsiapnota,
			'f_spb_cancel' 		=> $fspbcancel,
			'n_spb_discount1'	=> $nspbdiscount1,
			'n_spb_discount2'	=> $nspbdiscount2,
			'n_spb_discount3'	=> $nspbdiscount3,
			'n_spb_discount4'	=> $nspbdiscount4,
			'v_spb_discount1'	=> $vspbdiscount1,
			'v_spb_discount2'	=> $vspbdiscount2,
			'v_spb_discount3'	=> $vspbdiscount3,
			'v_spb_discount4'	=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 		=> $vspb,
			'f_spb_consigment'	=> $fspbconsigment,
			'd_spb_update'		=> $dspbupdate,
      			'i_spb_old'         	=> $ispbold,
			'e_remark1'		=> $eremark1);
	$this->db->where('i_spb', $ispb);
	$this->db->where('i_area', $iarea);
	$this->db->update('tm_spb', $data); 
    }
    function updateheadersjc($ispb, $iarea, $dsjc, $icustomer, $isalesman, $fsjccancel,
							 $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3, $vsjcdiscount1, 
							 $vsjcdiscount2, $vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross, 
							 $vsjcnetto, $isjc, $isjctype)
    {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjcupdate	= $row->c;
    	$data = array(
			'd_sjc'			=> $dsjc,
			'i_customer'		=> $icustomer,
			'i_salesman' 		=> $isalesman,
			'f_sjc_cancel' 		=> $fsjccancel,
			'n_sjc_discount1' 	=> $nsjcdiscount1,
			'n_sjc_discount2' 	=> $nsjcdiscount2,
			'n_sjc_discount3' 	=> $nsjcdiscount3,
			'v_sjc_discount1' 	=> $vsjcdiscount1,
			'v_sjc_discount2' 	=> $vsjcdiscount2,
			'v_sjc_discount3' 	=> $vsjcdiscount3,
			'v_sjc_gross'		=> $vsjcgross,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_netto'		=> $vsjcnetto,
			'd_sjc_update'		=> $dsjcupdate
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->where('i_sjc', $isjc);
		$this->db->where('i_sjc_type', $isjctype);
		$this->db->update('tm_sjc', $data); 
    }

	function uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb)
    {
    	$data = array(
			'v_spb_discount1' 	=> $vspbdiscount1,
			'v_spb_discount2' 	=> $vspbdiscount2,
			'v_spb_discount3' 	=> $vspbdiscount3,
			'v_spb_discount4' 	=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 		=> $vspb
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }

	function upheadsjc(	$ispb, $iarea, $isjc, $isjctype, $vsjcdiscount1, $vsjcdiscount2,
						$vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross, $vsjcnetto)
    {
    	$data = array(
			'v_sjc_discount1' 	=> $vsjcdiscount1,
			'v_sjc_discount2' 	=> $vsjcdiscount2,
			'v_sjc_discount3' 	=> $vsjcdiscount3,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_gross'		=> $vsjcgross,
			'v_sjc_netto'		=> $vsjcnetto,
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->where('i_sjc', $isjc);
		$this->db->where('i_sjc_type', $isjctype);
		$this->db->update('tm_sjc', $data); 
    }

    public function deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
    }

    public function deletedetailsjc($isjc, $iarea, $isjctype, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_sjc_item WHERE i_sjc='$isjc' and i_area='$iarea' and i_sjc_type='$isjctype'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
    }
	
    public function delete($ispb, $iarea) 
    {
		$this->db->query("DELETE FROM tm_spb WHERE i_spb='$ispb' and i_area='$iarea'");
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'");
    }
    function bacasemua($iarea)
    {
		$this->db->select("* from tm_spb where i_area='$iarea' order by i_spb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$kdharga,$promo,$kdgroup)
    {
		if($offset=='' || $offset=='index')
			$offset=0;
		$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
		foreach($q->result() as $pro){
			$p		= $pro->f_all_product;
			$b		= $pro->f_all_baby;
			$r		= $pro->f_all_reguler;
			$tipe	= $pro->i_promo_type;
		}
		if($p=='f'){
			if( ($tipe=='1') || ($tipe=='3')){
				if($b=='t'){
					if($kdgroup!='G0031'){
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tr_product c, tr_product_price d, tr_product_type e, tr_product_group f, tr_product_status g
										where c.i_product=a.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group
										and f.i_product_group='01' and c.i_product_status=g.i_product_status
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	order by a.i_product limit $num offset $offset" ,false);
					}else{
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tr_product c, tr_product_price d, tr_product_type e, tr_product_group f, tr_product_status g
										where c.i_product=a.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	order by a.i_product limit $num offset $offset" ,false);
					}
				}else{
					if($kdgroup!='G0031'){
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d, tr_product_type e, 
                    tr_product_group f, tr_product_status g
										where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
										and a.i_product=c.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group
										and f.i_product_group='01' and c.i_product_status=g.i_product_status
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
					}else{
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d, tr_product_type e, 
                    tr_product_group f, tr_product_status g
										where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
										and a.i_product=c.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
					}
				}
			}else{
				if($kdgroup!='G0031'){
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
											a.e_product_motifname as namamotif, c.i_product_status,
											c.e_product_name as nama,b.v_unit_price as harga, g.e_product_statusname
											from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_type e, tr_product_group f, tr_product_status g
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and c.i_product_type=e.i_product_type
											and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
											and f.i_product_group='01' and a.i_product_motif='00'
											and a.i_product=c.i_product
										 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
				}else{
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
											a.e_product_motifname as namamotif, c.i_product_status,
											c.e_product_name as nama,b.v_unit_price as harga, g.e_product_statusname
											from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_type e, tr_product_group f, tr_product_status g
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and c.i_product_type=e.i_product_type and a.i_product_motif='00'
											and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
											and a.i_product=c.i_product
										 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
				}
			}
		}else{
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
																	a.e_product_motifname as namamotif, c.i_product_status,
																	c.e_product_name as nama,b.v_product_retail as harga, g.e_product_statusname
																	from tr_product_motif a,tr_product_price b,tr_product c, tr_product_status g
																	where b.i_product=a.i_product and a.i_product_motif='00'
																	and a.i_product=c.i_product and c.i_product_status=g.i_product_status
																 	and b.i_price_group='$kdharga' 
																	order by a.i_product limit $num offset $offset",false);
		}		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset,$kdharga,$promo,$kdgroup)
    {
		if($offset=='' || $offset=='index')
			$offset=0;
		$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
		foreach($q->result() as $pro){
			$p		= $pro->f_all_product;
			$b		= $pro->f_all_baby;
			$r		= $pro->f_all_reguler;
			$tipe	= $pro->i_promo_type;
		}			
		if($p=='f'){
			if( ($tipe=='1') || ($tipe=='3')){
				if($b=='t'){
					if($kdgroup!='G0031'){
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tr_product c, tr_product_price d, tr_product_type e, tr_product_group f, tr_product_status g
										where c.i_product=a.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group
										and f.i_product_group='01' and c.i_product_status=g.i_product_status
										and upper(c.i_product) like '%$cari%'
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	order by a.i_product limit $num offset $offset" ,false);
					}else{
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tr_product c, tr_product_price d, tr_product_type e, tr_product_group f, tr_product_status g
										where c.i_product=a.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
										and upper(c.i_product) like '%$cari%'
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	order by a.i_product limit $num offset $offset" ,false);
					}
				}else{
					if($kdgroup!='G0031'){
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d, tr_product_type e, 
                    tr_product_group f, tr_product_status g
										where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
										and a.i_product=c.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type
										and e.i_product_group=f.i_product_group
										and f.i_product_group='01' and c.i_product_status=g.i_product_status
										and upper(c.i_product) like '%$cari%'
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
					}else{
						$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
										c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
										from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d, tr_product_type e, 
                    tr_product_group f, tr_product_status g
										where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
										and a.i_product=c.i_product and a.i_product_motif='00'
										and c.i_product_type=e.i_product_type and c.i_product_status=g.i_product_status
										and e.i_product_group=f.i_product_group
										and upper(c.i_product) like '%$cari%'
										and d.i_product=a.i_product and d.i_price_group='$kdharga'
									 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
					}
				}
			}else{
				if($kdgroup!='G0031'){
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
											a.e_product_motifname as namamotif, c.i_product_status,
											c.e_product_name as nama,b.v_unit_price as harga, g.e_product_statusname
											from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_type e, tr_product_group f, tr_product_status g
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and c.i_product_type=e.i_product_type
											and e.i_product_group=f.i_product_group
											and f.i_product_group='01' and a.i_product_motif='00'
											and upper(c.i_product) like '%$cari%'
											and a.i_product=c.i_product and c.i_product_status=g.i_product_status
										 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
				}else{
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
											a.e_product_motifname as namamotif, c.i_product_status,
											c.e_product_name as nama,b.v_unit_price as harga, g.e_product_statusname
											from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_type e, tr_product_group f, tr_product_status g
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and c.i_product_type=e.i_product_type and a.i_product_motif='00'
											and e.i_product_group=f.i_product_group and c.i_product_status=g.i_product_status
											and upper(c.i_product) like '%$cari%'
											and a.i_product=c.i_product
										 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
				}
			}
		}else{
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
										a.e_product_motifname as namamotif, c.i_product_status,
										c.e_product_name as nama,b.v_product_retail as harga, g.e_product_statusname
										from tr_product_motif a,tr_product_price b,tr_product c, tr_product_status g
										where b.i_product=a.i_product and a.i_product_motif='00'
										and a.i_product=c.i_product and c.i_product_status=g.i_product_status
										and upper(b.i_product) like '%$cari%'
								   	and b.i_price_group='$kdharga' order by a.i_product limit $num offset $offset",false);
		}		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproductupdate($cari,$num,$offset,$kdharga,$promo)
    {
		if($offset=='' || $offset=='index')
			$offset=0;
		$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
		foreach($q->result() as $pro){
			$p		= $pro->f_all_product;
			$b		= $pro->f_all_baby;
			$r		= $pro->f_all_reguler;
			$tipe	= $pro->i_promo_type;
		}			
		if($p=='f'){
			if( ($tipe=='1') || ($tipe=='3')){
				if($b=='t'){
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
									c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
									from tr_product_motif a,tr_product c, tr_product_price d, tr_product_type e, tr_product_group f, tr_product_status g
									where c.i_product=a.i_product
									and c.i_product_type=e.i_product_type
									and e.i_product_group=f.i_product_group
									and f.i_product_group='01' and c.i_product_status=g.i_product_status
									and upper(c.i_product) like '%$cari%'
									and d.i_product=a.i_product and d.i_price_group='$kdharga'
								 	order by a.i_product limit $num offset $offset" ,false);
				}else{
					$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, c.i_product_status,
									c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif, g.e_product_statusname
									from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d, tr_product_type e, 
                  tr_product_group f, tr_product_status g
									where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
									and a.i_product=c.i_product 
									and c.i_product_type=e.i_product_type
									and e.i_product_group=f.i_product_group
									and f.i_product_group='01' and c.i_product_status=g.i_product_status
									and upper(c.i_product) like '%$cari%'
									and d.i_product=a.i_product and d.i_price_group='$kdharga'
								 	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
				}
			}else{
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif, c.i_product_status,
									c.e_product_name as nama,b.v_unit_price as harga, g.e_product_statusname
									from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_type e, tr_product_group f, tr_product_status g
									where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
									and c.i_product_type=e.i_product_type
									and e.i_product_group=f.i_product_group
									and f.i_product_group='01' and c.i_product_status=g.i_product_status
									and upper(c.i_product) like '%$cari%'
									and a.i_product=c.i_product
							   	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
			}
/*
		}else if($b=='f'){
			if( ($tipe=='1') || ($tipe=='3')){
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama, d.v_product_retail as harga
								from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
								and d.i_product=a.i_product and d.i_price_group='$kdharga'
								and upper(b.i_product) like '%$cari%'
							  and b.i_promo='$promo'" ,false);
			}else{
				$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
											c.e_product_name as nama,b.v_unit_price as harga
											from tr_product_motif a,tm_promo_item b,tr_product c
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and a.i_product=c.i_product
											and upper(b.i_product) like '%$cari%'
									   	and b.i_promo='$promo'" 
										  ,false);
			}
*/
		}else{
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
										a.e_product_motifname as namamotif, c.i_product_status,
										c.e_product_name as nama,b.v_product_retail as harga, g.e_product_statusname
										from tr_product_motif a,tr_product_price b,tr_product c, tr_product_status g
										where b.i_product=a.i_product
										and a.i_product=c.i_product and c.i_product_status=g.i_product_status
										and upper(b.i_product) like '%$cari%'
								   	and b.i_price_group='$kdharga' order by a.i_product limit $num offset $offset",false);
		}		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacastore($num,$offset,$iarea)
    {
		$this->db->select("i_store, e_store_name from tr_store
						   where i_store in(select i_store from tr_area 
						   where i_area='$iarea' or i_area='00')
						   order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset,$promo,$c,$g,$type)
    {
		if($offset=='' || $offset=='index')
			$offset=0;
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group)
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customer y inner join tm_promo yy on(y.i_promo=yy.i_promo)
										where y.i_promo='$promo'
                    and x.f_approve='t' 
										and x.i_customer=y.i_customer 
										and x.i_area = '$iarea' limit $num offset $offset",false);
#                    and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t' 
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group)
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										where x.i_area = '$iarea'  and x.f_approve='t' limit $num offset $offset",false);
#										where x.i_area = '$iarea'  and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t' limit $num offset $offset",false);
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customergroup y inner join tm_promo p on (y.i_promo=p.i_promo)
										where y.i_promo='$promo' 
                    and x.f_approve='t'
										and x.i_customer_group=y.i_customer_group
										and x.i_area=y.i_area
										and x.i_area = '$iarea' limit $num offset $offset",false);
#                    and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t'
		}
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapromo($dspb,$num,$offset)
    {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$this->db->select(" * from (
													select distinct(i_promo) as tes,* from tm_promo where 
													f_all_area='t' and d_promo_start<='$dspb' and d_promo_finish>='$dspb' and f_all_reguler='f'
													union all
													select distinct(a.i_promo) as tes,a.* from tm_promo a
													inner join tm_promo_area b on (a.i_promo=b.i_promo 
													and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5'))
													where a.f_all_area='f' and d_promo_start<='$dspb' and d_promo_finish>='$dspb' and f_all_reguler='f'
													) as x
								order by x.i_promo",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function runningnumber($iarea,$thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPB'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospb
                            where i_modul='SPB'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<6){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
			  $nospb  ="SPB-".$thbl."-".$nospb;
			  return $nospb;
		  }else{
			  $nospb  ="000001";
			  $nospb  ="SPB-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPB','$iarea','$asal',1)");
			  return $nospb;
		  }
/*
#    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
#		  $row   	= $query->row();
#		  $thbl	= $row->c;
		  $th		= substr($thbl,0,2);
#		  $this->db->select(" max(substr(i_spb,10,6)) as max from tm_spb 
#				    			where substr(i_spb,5,2)='$th' and i_area='$iarea'", false);
		  $this->db->select(" count(i_spb) as max from tm_spb 
				    			where substr(i_spb,5,2)='$th' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospb  =$terakhir+1;
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<6){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
			  $nospb  ="SPB-".$thbl."-".$nospb;
			  return $nospb;
		  }else{
			  $nospb  ="000001";
			  $nospb  ="SPB-".$thbl."-".$nospb;
			  return $nospb;
		  }
*/
    }
	function runningnumbersjc($iarea){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_sjc,10,6)) as max from tm_sjc 
				  			where substr(i_sjc,5,2)='$th' and i_area='$iarea'
							and substr(i_sjc,1,3)='SJC'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nosjc  =$terakhir+1;
			settype($nosjc,"string");
			$a=strlen($nosjc);
			while($a<6){
			  $nosjc="0".$nosjc;
			  $a=strlen($nosjc);
			}
			$nosjc  ="SJC-".$thbl."-".$nosjc;
			return $nosjc;
		}else{
			$nosjc  ="000001";
			$nosjc  ="SJC-".$thbl."-".$nosjc;
			return $nosjc;
		}
    }
    function cari($iarea,$cari,$num,$offset)
    {
		$this->db->select(" * from tm_spb where upper(i_spb) like '%$cari%' and i_area='$iarea'
					order by i_spb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caristore($cari,$num,$offset)
    {
		$this->db->select(" * from tr_store where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'
							order by i_store",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset,$promo,$c,$g,$type)
    {
		if($offset=='' || $offset=='index')
			$offset=0;
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group)
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customer y inner join tm_promo yy on(y.i_promo=yy.i_promo)
										where y.i_promo='$promo' 
                    and x.f_approve='t'
										and x.i_customer=y.i_customer 
										and x.i_area = '$iarea' 
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
#                    and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t'
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group)
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										where x.i_area = '$iarea' 
                    and x.f_approve='t'
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
#                    and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t'
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line or x.i_price_group=c.i_price_group)
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customergroup y inner join tm_promo p on (y.i_promo=p.i_promo)
										where y.i_promo='$promo' 
                    and x.f_approve='t'
										and x.i_customer_group=y.i_customer_group
										and x.i_area=y.i_area
										and x.i_area = '$iarea' 
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
#                    and x.f_approve='t' and x.f_approve2='t' and x.f_approve3='t' and x.f_approve4='t'
		}
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caripromo($cari,$dspb,$num,$offset)
    {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$this->db->select(" * from (
							select distinct(i_promo) as tes,* from tm_promo where 
							f_all_area='t' and d_promo_start<='$dspb' and d_promo_finish>='$dspb' and f_all_reguler='f'
							and (upper(i_promo) like '%$cari%' or upper(e_promo_name) like '%$cari%')
							union all
							select distinct(a.i_promo) as tes,a.* from tm_promo a
							inner join tm_promo_area b on (a.i_promo=b.i_promo 
							and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5'))
							where a.f_all_area='f' and a.d_promo_start<='$dspb' and a.d_promo_finish>='$dspb' and a.f_all_reguler='f'
							and (upper(a.i_promo) like '%$cari%' or upper(a.e_promo_name) like '%$cari%')) as x
							order by x.i_promo",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();

			}
    }
	function bacaarea($num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5,$promo,$c,$g,$a)
    {
		if(($c=='f') && ($g=='f') && ($a=='t')){
			/*
			$query = $this->db->query(" select a.* 
										from tr_area a
										where a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5'",false);
			*/

			if($allarea=='t'){
				$this->db->select(" a.* from tr_area a order by a.i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" a.* from tr_area a where (upper(a.i_area) like '%$area1%' or upper(a.i_area) like '%$area2%' or upper(a.i_area) like '%$area3%' or upper(a.i_area) like '%$area4%' or upper(a.i_area) like '%$area5%') order by a.i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" a.* from tr_area a where a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5' order by a.i_area", false)->limit($num,$offset);
			}

			/* ////////////
			if($allarea=='t') {
				$query = $this->db->query("	select a.* from tr_area a ",false);				
			} else {
				$query = $this->db->query("	select a.* 
											from tr_area a
											where a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
											or a.i_area = '$area4' or a.i_area = '$area5'",false);				
			}
			*/
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customer a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
		}else if(($c=='t') && ($g=='f') && ($a=='t')){
			/*
			$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   or i_area = '$area4' or i_area = '$area5'",false);
			*/
			if($allarea=='t'){
				$this->db->select(" * from tr_area order by i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
			}
			/* ////////////
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);				
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);				
			}
			*/
		}else if(($c=='f') && ($g=='t') && ($a=='t')){
			/*
			$query = $this->db->query(" select a.* 
										from tr_area a
										where a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5'",false);
			*/
			if($allarea=='t'){
				$this->db->select(" * from tr_area order by i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
			}
			/* ////////////
			if($allarea=='t') {
				$query = $this->db->query("	select a.* from tr_area a ",false);				
			} else {
				$query = $this->db->query("	select a.* 
											from tr_area a
											where a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
											or a.i_area = '$area4' or a.i_area = '$area5'",false);
			}
			*/
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customergroup a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
		}else if($a=='f'){
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
									from tm_promo_area a, tr_area b
									where a.i_promo='$promo' and a.i_area=b.i_area
									and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
								   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
			if($allarea=='t'){
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where a.i_promo='$promo' and a.i_area=b.i_area order by a.i_area ", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where a.i_promo='$promo' and a.i_area=b.i_area and (upper(a.i_area) like '%$area1%' or upper(a.i_area) like '%$area2%' or upper(a.i_area) like '%$area3%' or upper(a.i_area) like '%$area4%' or upper(a.i_area) like '%$area5%') order by a.i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where a.i_promo='$promo' and a.i_area=b.i_area and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') order by a.i_area", false)->limit($num,$offset);
			}
			/* ////////////
			if($allarea=='t') {
				$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
							from tm_promo_area a, tr_area b
							where a.i_promo='$promo' and a.i_area=b.i_area ",false);				
			} else {
				$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
							from tm_promo_area a, tr_area b
							where a.i_promo='$promo' and a.i_area=b.i_area
							and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
							or a.i_area = '$area4' or a.i_area = '$area5')",false);					
			}
			*/
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5,$promo,$c,$g,$a)
    {
		if(($c=='f') && ($g=='f') && ($a=='t')){
			/*
			$query = $this->db->query("	select a.* 
										from tr_area a
										where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
			if($allarea=='t'){
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by a.i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by a.i_area", false)->limit($num,$offset);
			}
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customer a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
		}else if(($c=='t') && ($g=='f') && ($a=='t')){
			/*
			$query = $this->db->query("	select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
										and i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   	or i_area = '$area4' or i_area = '$area5'",false);
			*/
			if($allarea=='t'){
				$this->db->select(" * from tr_area (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
			}
		}else if(($c=='f') && ($g=='t') && ($a=='t')){
			/*
			$query = $this->db->query("	select a.* 
										from tr_area a
										where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
			if($allarea=='t'){
				$this->db->select(" * from tr_area (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
			}
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customergroup a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			*/
		}else if($a=='f'){
			/*
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
								from tm_promo_area a, tr_area b
								where a.i_promo='$promo' and a.i_area=b.i_area
								and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
							   	or a.i_area = '$area4' or a.i_area = '$area5')
								and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')",false);
			*/
			if($allarea=='t'){
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%') and a.i_promo='$promo' and a.i_area=b.i_area order by a.i_area ", false)->limit($num,$offset);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%') and a.i_promo='$promo' and a.i_area=b.i_area and (upper(a.i_area) like '%$area1%' or upper(a.i_area) like '%$area2%' or upper(a.i_area) like '%$area3%' or upper(a.i_area) like '%$area4%' or upper(a.i_area) like '%$area5%') order by a.i_area", false)->limit($num,$offset);
			}else{
				$this->db->select(" distinct(a.i_area) as i_area, b.* from tm_promo_area a, tr_area b where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%') and a.i_promo='$promo' and a.i_area=b.i_area and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') order by a.i_area", false)->limit($num,$offset);			
			}
		}	
		$query = $this->db->get();	
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasalesman($cari,$area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
		if($area1=='00'){
			$query = $this->db->select("i_salesman, e_salesman_name from tr_salesman
									               	where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
																	order by i_salesman",false)->limit($num,$offset);
		}else{
			$this->db->select(" i_salesman, e_salesman_name from tr_salesman
										              where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
														      or i_area = '$area4' or i_area = '$area5')
										              and (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')  
										              order by i_salesman",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                    DELETE FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if($row->i_trans!=''){
        return $row->i_trans;
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,$qsj,0,0,$qaw-$qsj,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
}
?>
