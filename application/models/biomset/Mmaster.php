<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }

  function bacaperiode($iperiode1, $iperiode2)
  {
    $query = $this->db->query(" select
                                  i_periode,
                                  i_area,
                                  e_area_name,
                                  v_biaya_kk,
                                  v_biaya_kb,
                                  v_biaya_bank,
                                  (v_biaya_kk + v_biaya_kb + v_biaya_bank) as v_biaya_total,
                                  v_omset,
                                  round( coalesce( ((v_biaya_kk + v_biaya_kb + v_biaya_bank) / nullif(v_omset, 0) * 100) , 0 ) , 3 ) as persen
                                  from (
                                  select i_periode, i_area, e_area_name, sum(v_biaya_kk) as v_biaya_kk, sum(v_biaya_kb) as v_biaya_kb, sum(v_biaya_bank) as v_biaya_bank, sum(v_omset) as v_omset
                                  from (

                                    select a.i_periode, a.i_area, b.e_area_name, sum(a.v_kk) as v_biaya_kk, 0 as v_biaya_kb, 0 as v_biaya_bank, 0 as v_omset
                                    from tm_kk a
                                    inner join tr_area b on(a.i_area=b.i_area)
                                    where a.i_periode between '$iperiode1' and '$iperiode2'
                                    and a.f_kk_cancel='f' and a.f_debet='t'
                                    and (a.i_coa like '610%' or a.i_coa like '620%' or a.i_coa like '800%')
                                    group by 1, 2, 3

                                    union all

                                    select a.i_periode, a.i_area, b.e_area_name, 0 as v_biaya_kk, sum(a.v_kb) as v_biaya_kb, 0 as v_biaya_bank, 0 as v_omset
                                    from tm_kb a
                                    inner join tr_area b on(a.i_area=b.i_area)
                                    where a.i_periode between '$iperiode1' and '$iperiode2'
                                    and a.f_kb_cancel='f' and a.f_debet='t'
                                    and (a.i_coa like '610%' or a.i_coa like '620%' or a.i_coa like '800%')
                                    group by 1, 2, 3

                                    union all

                                    select a.i_periode, a.i_area, b.e_area_name, 0 as v_biaya_kk, 0 as v_biaya_kb, sum(a.v_bank) as v_biaya_bank, 0 as v_omset
                                    from tm_kbank a
                                    inner join tr_area b on(a.i_area=b.i_area)
                                    where a.i_periode between '$iperiode1' and '$iperiode2'
                                    and a.f_kbank_cancel='f' and a.f_debet='t'
                                    and (a.i_coa like '610%' or a.i_coa like '620%' or a.i_coa like '800%')
                                    group by 1, 2, 3

                                    union all

                                    select to_char(a.d_nota,'yyyymm') i_periode, a.i_area, b.e_area_name, 0 as v_biaya_kk, 0 as v_biaya_kb, 0 as v_biaya_bank, sum(a.v_nota_netto) as v_omset
                                    from tm_nota a
                                    inner join tr_area b on(a.i_area=b.i_area)
                                    where to_char(a.d_nota,'yyyymm') between '$iperiode1' and '$iperiode2'
                                    and a.f_nota_cancel='f'
                                    group by 1, 2, 3

                                    ) x1
                                  group by 1, 2, 3
                                  order by 1, 2
                                )x2");

    return ($query->num_rows() > 0) ? $query->result() : 'zero';
  }
}
