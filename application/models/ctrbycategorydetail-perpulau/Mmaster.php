<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
    {
        parent::__construct();
    #$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl,$iproductcategory)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
      $this->db->select(" a.island, sum(volly) as volly, sum(volcy) as volcy, sum(vollm) as vollm, sum(volcm) as volcm
                        from(
                        select a.island, sum(a.vol) as volly, 0 as volcy, 0 as vollm, 0 as volcm 
                        from
                        (
                        select e.e_area_island as island, sum(c.n_deliver) as vol
                        from tr_product a, tr_product_category b, tm_nota_item c
                        left join tm_nota d on (d.i_nota=c.i_nota and d.i_area=c.i_area)
                        left join tr_area e on (c.i_area=e.i_area)
                        where b.i_product_category=a.i_product_category and c.i_product=a.i_product
                        and to_char(d.d_nota, 'yyyy') = '$prevth' and d.f_nota_cancel='f' and d.d_nota<='$prevdate'
                        and a.i_product_category='$iproductcategory'
                        and not d.i_nota isnull
                        group by e.e_area_island
                        ) as a
                        group by a.island
                        union all
                        select a.island, 0 as volly, sum(a.vol) as volcy, 0 as vollm, 0 as volcm 
                        from
                        (
                        select e.e_area_island as island, sum(c.n_deliver) as vol
                        from tr_product a, tr_product_category b, tm_nota_item c
                        left join tm_nota d on (d.i_nota=c.i_nota and d.i_area=c.i_area)
                        left join tr_area e on (c.i_area=e.i_area)
                        where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                        and to_char(d.d_nota, 'yyyy') = '$th' and d.f_nota_cancel='f' and d.d_nota<='$todate'
                        and a.i_product_category='$iproductcategory'
                        and not d.i_nota isnull
                        group by e.e_area_island
                        ) as a
                        group by a.island
                        union all
                        select a.island, 0 as volly, 0 as volcy, sum(a.vol) as vollm, 0 as volcm 
                        from
                        (
                        select e.e_area_island as island, sum(c.n_deliver) as vol
                        from tr_product a, tr_product_category b, tm_nota_item c
                        left join tm_nota d on (d.i_nota=c.i_nota and d.i_area=c.i_area)
                        left join tr_area e on (c.i_area=e.i_area)
                        where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                        and to_char(d.d_nota, 'yyyymm') = '$prevthbl' and d.f_nota_cancel='f' and d.d_nota<='$prevdate'
                        and a.i_product_category='$iproductcategory'
                        and not d.i_nota isnull
                        group by e.e_area_island
                        ) as a
                        group by a.island
                        union all
                        select a.island, 0 as volly, 0 as volcy, 0 as vollm, sum(a.vol) as volcm 
                        from
                        (
                        select e.e_area_island as island, sum(c.n_deliver) as vol
                        from tr_product a, tr_product_category b, tm_nota_item c
                        left join tm_nota d on (d.i_nota=c.i_nota and d.i_area=c.i_area)
                        left join tr_area e on (c.i_area=e.i_area)
                        where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                        and to_char(d.d_nota, 'yyyymm') = '$thbl' and d.f_nota_cancel='f' and d.d_nota<='$todate'
                        and a.i_product_category='$iproductcategory'
                        and not d.i_nota isnull
                        group by e.e_area_island
                        ) as a
                        group by a.island
                        )as a
                        group by a.island
                        order by a.island",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
  function nama($kode)
  {
    $nama='';
    $this->db->select("e_product_categoryname from tr_product_category where i_product_category='$kode'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      foreach($query->result() as $tmp){
        $nama=$tmp->e_product_categoryname;
      }
      return $nama;
    }
  }
function bacakategory($num,$offset,$cari)
  {
    $this->db->select("distinct(i_product_category),e_product_categoryname 
    from tr_product_category 
    where e_product_categoryname is not null and f_product_categorystatus='t'
    and (i_product_category like '%$cari%' or e_product_categoryname like '%$cari%' )", false)->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }
}
?>
