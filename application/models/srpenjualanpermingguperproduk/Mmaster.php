<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
      $this->db->select("	e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qspb, sum(vspb) as vspb, 
      sum(qtynota) as qnota, sum(vnota) as vnota from(
      select e_supplier_name, i_product, e_product_name, week, 0 as qtyspb, 0 as vspb, sum(qtynota) as qtynota, 
      sum(vgrnota-discitem) as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota) AS week , sum(b.n_deliver) as qtynota, 
      sum(b.n_deliver*b.v_unit_price) as vgrnota, (sum(b.n_deliver*b.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal as discitem
      from tm_nota a, tm_nota_item b, tr_product c, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area and a.i_sj=b.i_sj 
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discounttotal
      )as a
      GROUP BY a.e_supplier_name, a.i_product, a.e_product_name, week
      union all
      select e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qtyspb, sum(vgrspb-discitem) as vspb, 0 as qtynota, 
      0 as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb) AS week, sum(b.n_order) as qtyspb, 
      sum(b.n_order*b.v_unit_price) as vgrspb, 
      (sum(b.n_order*b.v_unit_price)/a.v_spb)*a.v_spb_discounttotal as discitem
      from tm_spb a, tm_spb_item b, tr_product c, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and a.f_spb_cancel='f' and a.i_area=b.i_area and a.i_spb=b.i_spb
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_product, b.e_product_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_product, c.e_product_name, week
      ORDER BY c.e_supplier_name, c.i_product, c.e_product_name, week",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function jmlminggu($iperiode)
    {
      $this->db->select("	count(distinct(week)) as jml from(
      select e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qtyspb, sum(vspb) as vspb, 
      sum(qtynota) as qtynota, sum(vnota) as vnota from(
      select e_supplier_name, i_product, e_product_name, week, 0 as qtyspb, 0 as vspb, sum(qtynota) as qtynota, 
      sum(vgrnota-discitem) as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota) AS week , sum(b.n_deliver) as qtynota, 
      sum(b.n_deliver*b.v_unit_price) as vgrnota, (sum(b.n_deliver*b.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal as discitem
      from tm_nota a, tm_nota_item b, tr_product c, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area and a.i_sj=b.i_sj 
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discounttotal
      )as a
      GROUP BY a.e_supplier_name, a.i_product, a.e_product_name, week
      union all
      select e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qtyspb, sum(vgrspb-discitem) as vspb, 0 as qtynota, 
      0 as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb) AS week, sum(b.n_order) as qtyspb, 
      sum(b.n_order*b.v_unit_price) as vgrspb, 
      (sum(b.n_order*b.v_unit_price)/a.v_spb)*a.v_spb_discounttotal as discitem
      from tm_spb a, tm_spb_item b, tr_product c, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and a.f_spb_cancel='f' and a.i_area=b.i_area and a.i_spb=b.i_spb
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_product, b.e_product_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_product, c.e_product_name, week
      )as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function minggu($iperiode)
    {
      $this->db->select("	week from(
      select e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qspb, sum(vspb) as vspb, 
      sum(qtynota) as qnota, sum(vnota) as vnota from(
      select e_supplier_name, i_product, e_product_name, week, 0 as qtyspb, 0 as vspb, sum(qtynota) as qtynota, 
      sum(vgrnota-discitem) as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota) AS week , sum(b.n_deliver) as qtynota, 
      sum(b.n_deliver*b.v_unit_price) as vgrnota, (sum(b.n_deliver*b.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal as discitem
      from tm_nota a, tm_nota_item b, tr_product c, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area and a.i_sj=b.i_sj 
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discounttotal
      )as a
      GROUP BY a.e_supplier_name, a.i_product, a.e_product_name, week
      union all
      select e_supplier_name, i_product, e_product_name, week, sum(qtyspb) as qtyspb, sum(vgrspb-discitem) as vspb, 0 as qtynota, 
      0 as vnota from(
      select d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb) AS week, sum(b.n_order) as qtyspb, 
      sum(b.n_order*b.v_unit_price) as vgrspb, 
      (sum(b.n_order*b.v_unit_price)/a.v_spb)*a.v_spb_discounttotal as discitem
      from tm_spb a, tm_spb_item b, tr_product c, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and a.f_spb_cancel='f' and a.i_area=b.i_area and a.i_spb=b.i_spb
      and b.i_product=c.i_product and c.i_supplier=d.i_supplier and d.i_supplier in ('SP002','SP004','SP021')
      GROUP BY d.e_supplier_name, b.i_product, c.e_product_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_product, b.e_product_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_product, c.e_product_name, week
      )as a
      Group by week Order by week",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' 
				                    or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
				                    order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
