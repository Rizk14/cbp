<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" d.i_customer, b.e_customer_name, c.e_city_name, a.i_salesman, sum(a.v_nota_gross) as nota, d.i_area_rayon, e.e_area_rayon_name,
                          sum(a.v_nota_netto) as bersih
                          from tm_nota a, tr_customer b, tr_city c, tr_customer_rayon d, tr_rayon e
                          where a.f_nota_cancel='f' and 
                          to_char(a.d_nota,'yyyymm')='$iperiode' and 
                          not a.i_nota isnull and
                          a.i_area='$iarea' and
                          a.i_customer=b.i_customer and 
                          a.i_customer=d.i_customer and 
                          d.i_area_rayon=e.i_area_rayon and 
                          b.i_city=c.i_city and
                          b.i_area=c.i_area
                          group by d.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, e.e_area_rayon_name
                          order by c.e_city_name ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
