<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomersalestype)
    {
		$this->db->select('i_customer_salestype, e_customer_salestypename')->from('tr_customer_salestype')->where('i_customer_salestype', $icustomersalestype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomersalestype, $ecustomersalestypename)
    {
    	$this->db->set(
    		array(
    			'i_customer_salestype' => $icustomersalestype,
    			'e_customer_salestypename' => $ecustomersalestypename
    		)
    	);
    	
    	$this->db->insert('tr_customer_salestype');
		#redirect('customersalestype/cform/');
    }
    function update($icustomersalestype, $ecustomersalestypename)
    {
    	$data = array(
               'i_customer_salestype' => $icustomersalestype,
               'e_customer_salestypename' => $ecustomersalestypename
            );
		$this->db->where('i_customer_salestype', $icustomersalestype);
		$this->db->update('tr_customer_salestype', $data); 
		#redirect('customersalestype/cform/');
    }
	
    public function delete($icustomersalestype) 
    {
		$this->db->query('DELETE FROM tr_customer_salestype WHERE i_customer_salestype=\''.$icustomersalestype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype order by i_customer_salestype", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
