<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($ispb, $iarea)
	{
		$this->db->select(" * from tm_spb 
				   left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where tm_spb.i_spb ='$ispb' and tm_spb.i_area='$iarea' order by tm_spb.i_area, tm_spb.i_spb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($ispb, $iarea)
	{
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
			                 	where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product 
												and a.i_product_motif=b.i_product_motif
				                order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function insertdetail(
		$ispb,
		$iarea,
		$iproduct,
		$iproductgrade,
		$eproductname,
		$norder,
		$vunitprice,
		$ndeliver,
		$iproductmotif,
		$nstock,
		$eremark,
		$i,
		$iproductstatus
	) {
		$this->db->set(
			array(
				'i_spb'				    => $ispb,
				'i_area'			    => $iarea,
				'i_product'			  => $iproduct,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'n_order'			    => $norder,
				'n_deliver'			  => $ndeliver,
				'n_stock'			    => $nstock,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
				'e_remark'	      => $eremark,
				'n_item_no'       => $i,
				'i_product_status' => $iproductstatus
			)
		);

		$this->db->insert('tm_spb_item');
	}
	function updateheader(
		$dspb,
		$icustomer,
		$ispbpo,
		$nspbtoplength,
		$isalesman,
		$ipricegroup,
		$dspbreceive,
		$fspbop,
		$ecustomerpkpnpwp,
		$fspbpkp,
		$fspbplusppn,
		$fspbplusdiscount,
		$fspbvalid,
		$nspbdiscount1,
		$nspbdiscount2,
		$nspbdiscount3,
		$vspbdiscount1,
		$vspbdiscount2,
		$vspbdiscount3,
		$vspbdiscounttotal,
		$vspb,
		$fspbconsigment,
		$ispb,
		$iarea,
		$istore,
		$istorelocation,
		$fspbstockdaerah,
		$fspbsiapnotagudang,
		$fspbcancel,
		$fspbsiapnotasales,
		$vspbdiscounttotalafter,
		$vspbafter
	) {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dspbupdate	= $row->c;
		if ($fspbstockdaerah == 'f') {
			$data = array(
				'd_spb'				            => $dspb,
				'i_customer'		          => $icustomer,
				'i_spb_po'			          => $ispbpo,
				'n_spb_toplength' 	      => $nspbtoplength,
				'i_salesman' 		          => $isalesman,
				'i_price_group' 	        => $ipricegroup,
				'd_spb_receive'		        => $dspb,
				'f_spb_op'			          => 't', #$fspbop,
				'e_customer_pkpnpwp'      => $ecustomerpkpnpwp,
				'f_spb_pkp'			          => $fspbpkp,
				'f_spb_plusppn'		        => $fspbplusppn,
				'f_spb_plusdiscount'      => $fspbplusdiscount,
				'f_spb_stockdaerah'	      => $fspbstockdaerah,
				'f_spb_valid' 		        => $fspbvalid,
				'f_spb_cancel' 		        => $fspbcancel,
				'n_spb_discount1' 	      => $nspbdiscount1,
				'n_spb_discount2' 	      => $nspbdiscount2,
				'n_spb_discount3' 	      => $nspbdiscount3,
				'v_spb_discount1'      	  => $vspbdiscount1,
				'v_spb_discount2' 	      => $vspbdiscount2,
				'v_spb_discount3' 	      => $vspbdiscount3,
				'v_spb_discounttotalafter' => $vspbdiscounttotalafter,
				'v_spb_after' 		        => $vspbafter,
				'f_spb_consigment' 	      => $fspbconsigment,
				'd_spb_update'		        => $dspbupdate,
				'i_store'			            => $istore,
				'i_store_location'	      => $istorelocation,
				'f_spb_siapnotagudang'	  => $fspbsiapnotagudang,
				'f_spb_siapnotasales'	    => $fspbsiapnotasales
			);
		} else {
			$data = array(
				'd_spb'				            => $dspb,
				'i_customer'		          => $icustomer,
				'i_spb_po'			          => $ispbpo,
				'n_spb_toplength' 	      => $nspbtoplength,
				'i_salesman' 		          => $isalesman,
				'i_price_group' 	        => $ipricegroup,
				'd_spb_receive'		        => $dspb,
				'f_spb_op'			          => $fspbop,
				'e_customer_pkpnpwp'      => $ecustomerpkpnpwp,
				'f_spb_pkp'			          => $fspbpkp,
				'f_spb_plusppn'		        => $fspbplusppn,
				'f_spb_plusdiscount'      => $fspbplusdiscount,
				'f_spb_stockdaerah'	      => $fspbstockdaerah,
				'f_spb_valid' 		        => 't',
				'f_spb_cancel' 		        => $fspbcancel,
				'n_spb_discount1' 	      => $nspbdiscount1,
				'n_spb_discount2' 	      => $nspbdiscount2,
				'n_spb_discount3' 	      => $nspbdiscount3,
				'v_spb_discount1' 	      => $vspbdiscount1,
				'v_spb_discount2' 	      => $vspbdiscount2,
				'v_spb_discount3' 	      => $vspbdiscount3,
				'v_spb_discounttotalafter' => $vspbdiscounttotalafter,
				'v_spb_after' 		        => $vspbafter,
				'f_spb_consigment' 	      => $fspbconsigment,
				'd_spb_update'		        => $dspbupdate,
				'i_store'			            => $istore,
				'i_store_location'	      => $istorelocation,
				'f_spb_siapnotagudang'	  => $fspbsiapnotagudang,
				'f_spb_siapnotasales'	    => $fspbsiapnotasales
			);
		}
		#			'f_spb_program' 	=> $fspbprogram,
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	function updateheadernsj(
		$ispb,
		$iarea,
		$istore,
		$istorelocation,
		$fspbstockdaerah,
		$fspbsiapnotagudang,
		$fspbcancel,
		$fspbvalid,
		$fspbsiapnotasales,
		$isj,
		$dsj
	) {
		$data = array(
			'i_store'				      => $istore,
			'i_store_location'	  => $istorelocation,
			'f_spb_stockdaerah' 	=> $fspbstockdaerah,
			'f_spb_valid' 			  => $fspbvalid,
			'f_spb_siapnotagudang' => $fspbsiapnotagudang,
			'f_spb_siapnotasales'	=> $fspbsiapnotasales,
			'f_spb_cancel' 			  => $fspbcancel
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	function langsungnota($ispb, $iarea)
	{
		$data = array(
			'f_spb_siapnotagudang' 		=> 't',
			'f_spb_siapnotasales' 		=> 'f',
			'f_spb_op' 		            => 'f',
			'f_spb_pemenuhan'         => 't'
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	function lansgungop($ispb, $iarea)
	{
		$data = array(
			'f_spb_pemenuhan'         => 'f'
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	public function deletedetail($iproduct, $iarea, $iproductgrade, $ispb, $iproductmotif)
	{
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
	}
	public function delete($ispb)
	{
		//		$this->db->query('DELETE FROM tm_spb WHERE i_spb=\''.$ispb.'\'');
		//		$this->db->query('DELETE FROM tm_spb_item WHERE i_spb=\''.$ispb.'\'');
		return TRUE;
	}
	function bacasemua($cari, $num, $offset)
	{
		$iuser = $this->session->userdata('user_id');
		$area1 = $this->session->userdata('i_area');
		$area2 = $this->session->userdata('i_area2');
		$area3 = $this->session->userdata('i_area3');
		$area4 = $this->session->userdata('i_area4');
		$area5 = $this->session->userdata('i_area5');
		$allarea	= $this->session->userdata('allarea');
		if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
			# and tm_spb.i_area=tr_salesman.i_area) 
			$this->db->select(" 	*,
									CASE
										WHEN n_print_gudang = 0 THEN 'Belum'
										ELSE 'Sudah ' || n_print_gudang::TEXT || 'X'
									END AS n_print_gudang
								FROM
									tm_spb
								INNER JOIN tr_customer ON
									(tm_spb.i_customer = tr_customer.i_customer)
								INNER JOIN tr_salesman ON
									(tm_spb.i_salesman = tr_salesman.i_salesman)
								INNER JOIN tr_customer_area ON
									(tm_spb.i_customer = tr_customer_area.i_customer)
								INNER JOIN tr_price_group ON
									(tm_spb.i_price_group = tr_price_group.i_price_group)
								INNER JOIN tr_product_group ON
									(tr_product_group.i_product_group = tm_spb.i_product_group)
								WHERE
									((NOT tm_spb.i_approve1 ISNULL
										AND NOT tm_spb.i_approve2 ISNULL
										AND tm_spb.i_store ISNULL
										AND tm_spb.i_store_location ISNULL
										AND f_spb_stockdaerah = 'f'
										AND tm_spb.f_spb_cancel = 'f')
									OR (NOT tm_spb.i_approve1 ISNULL
										AND NOT tm_spb.i_approve2 ISNULL
										AND tm_spb.i_store ISNULL
										AND tm_spb.i_store_location ISNULL
										AND f_spb_stockdaerah = 't'
										AND tm_spb.f_spb_cancel = 'f'
										AND tm_spb.i_area IN (
										SELECT
											i_area
										FROM
											tm_user_area
										WHERE
											i_user = '$iuser')
								)
								)
									AND (upper(tm_spb.i_spb) LIKE '%$cari%'
										OR upper(tm_spb.i_customer) LIKE '%$cari%')
									AND substring(tm_spb.i_customer, 3, 3)!= '000'
								ORDER BY
									tm_spb.i_spb", false)->limit($num, $offset);
		} else {
			# and tm_spb.i_area=tr_salesman.i_area) 
			$this->db->select(" * from tm_spb 
					inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
					inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
					inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
					inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
          inner join tr_product_group on(tr_product_group.i_product_group=tm_spb.i_product_group)
					where 
						  not tm_spb.i_approve1 isnull 
					  and not tm_spb.i_approve2 isnull 
					  and tm_spb.i_store isnull 
					  and tm_spb.i_store_location isnull
					  and f_spb_stockdaerah='t'
					  and tm_spb.f_spb_cancel='f'
					  and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
					  AND tm_spb.i_area IN (
						SELECT
							i_area
						FROM
							tm_user_area
						WHERE
							i_user = '$iuser')
						and substring(tm_spb.i_customer,3,3)!='000'
					order by tm_spb.i_spb
				", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaproduct($num, $offset, $kdharga)
	{
		$this->db->select("i_product, i_product_grade, e_product_name, v_product_retail
						   from tr_product_price 
						   where i_price_group = '$kdharga'
						   order by i_product, i_product_grade", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacastore($num, $offset, $area, $stockdaerah, $area1, $area2, $area3, $area4, $area5)
	{
		if ($stockdaerah == 'f') {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='00')
						   and a.i_store=b.i_store
						   order by a.i_store", false)->limit($num, $offset);
		} else {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area3' or i_area='$area4'))
						   and a.i_store=b.i_store
						   order by a.i_store", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomer($iarea, $num, $offset)
	{
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea'
							order by a.i_customer", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumber()
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_spb,10,6)) as max from tm_spb 
				  			where substr(i_spb,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nospb  = $terakhir + 1;
			settype($nospb, "string");
			$a = strlen($nospb);
			while ($a < 6) {
				$nospb = "0" . $nospb;
				$a = strlen($nospb);
			}
			$nospb  = "SPB-" . $thbl . "-" . $nospb;
			return $nospb;
		} else {
			$nospb  = "000001";
			$nospb  = "SPB-" . $thbl . "-" . $nospb;
			return $nospb;
		}
	}
	function cari($cari, $num, $offset)
	{
		$iuser = $this->session->userdata('user_id');
		$area1 = $this->session->userdata('i_area');
		$area2 = $this->session->userdata('i_area2');
		$area3 = $this->session->userdata('i_area3');
		$area4 = $this->session->userdata('i_area4');
		$area5 = $this->session->userdata('i_area5');
		$allarea	= $this->session->userdata('allarea');
		if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
			# and tm_spb.i_area=tr_salesman.i_area) 
			$this->db->select(" *, case when n_print_gudang = 0 then 'Belum' else 'Sudah ' || n_print_gudang::text || 'X' end as n_print_gudang from tm_spb 
						inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
						inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
						inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
            inner join tr_product_group on(tr_product_group.i_product_group=tm_spb.i_product_group)
					where 
					 ((not tm_spb.i_approve1 isnull 
					  and not tm_spb.i_approve2 isnull 
					  and tm_spb.i_store isnull 
					  and tm_spb.i_store_location isnull
					  and f_spb_stockdaerah='f'
					  and tm_spb.f_spb_cancel='f') 
					or (not tm_spb.i_approve1 isnull 
					  and not tm_spb.i_approve2 isnull 
					  and tm_spb.i_store isnull 
					  and tm_spb.i_store_location isnull
					  and tm_spb.f_spb_stockdaerah='t'
					  and tm_spb.f_spb_cancel='f'
					  AND tm_spb.i_area IN (
						SELECT
							i_area
						FROM
							tm_user_area
						WHERE
							i_user = '$iuser')
					  )
					 )
					  and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tm_spb.i_customer) ilike '%$cari%')
					order by tm_spb.i_spb", false)->limit($num, $offset);
		} else {
			# and tm_spb.i_area=tr_salesman.i_area) 
			$this->db->select(" * from tm_spb 
					inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
					inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
					inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
					inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
          inner join tr_product_group on(tr_product_group.i_product_group=tm_spb.i_product_group)
					where 
					      not tm_spb.i_approve1 isnull 
					  and not tm_spb.i_approve2 isnull 
					  and tm_spb.i_store isnull 
					  and tm_spb.i_store_location isnull
					  and f_spb_stockdaerah='t'
					  and tm_spb.f_spb_cancel='f'
					  and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tm_spb.i_customer) ilike '%$cari%')
					  AND tm_spb.i_area IN (
						SELECT
							i_area
						FROM
							tm_user_area
						WHERE
							i_user = '$iuser')
					  )
					order by tm_spb.i_spb
				", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caristore($cari, $num, $offset, $stockdaerah, $area1, $area2, $area3, $area4, $area5)
	{
		if ($stockdaerah == 'f') {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area' or i_area='00')
						   and a.i_store=b.i_store and upper(a.i_store) like '%cari%' 
						   or upper(a.e_store_name) like '%cari%' order by a.i_store", FALSE)->limit($num, $offset);
		} else {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area3' or i_area='$area4'))
						   and a.i_store=b.i_store and upper(a.i_store) like '%cari%' 
						   or upper(a.e_store_name) like '%cari%' order by a.i_store", FALSE)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomer($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.i_store_location, b.e_store_locationname from tr_store a, tr_store_location b 
					where a.i_store=b.i_store
					  and (upper(a.i_store) like '%$cari%' or upper(a.e_store_name) like '%$cari%'
					or upper(b.i_store_location) like '%$cari%' or upper(b.e_store_locationname) like '%$cari%') 
					order by a.i_store", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariproduct($cari, $num, $offset, $kdstore)
	{
		$this->db->select("  * from tr_product_price
					 where (upper(i_product) like '%$cari%' 
					or upper(e_product_name) like '%$cari%'
					or upper(i_product_grade) like '%$cari%')
					and i_product in(
								 select i_product from tm_ic where i_store = 'AA' or i_store='$kdstore')
					order by i_product, i_product_grade", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset)
	{
		$this->db->select("i_area, e_area_name from tr_area order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset)
	{
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumberbbk()
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_bbk,10,6)) as max from tm_bbk where substr(i_bbk,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nobbk  = $terakhir + 1;
			settype($nobbk, "string");
			$a = strlen($nobbk);
			while ($a < 6) {
				$nobbk = "0" . $nobbk;
				$a = strlen($nobbk);
			}
			$nobbk  = "BBK-" . $thbl . "-" . $nobbk;
			return $nobbk;
		} else {
			$nobbk  = "000001";
			$nobbk  = "BBK-" . $thbl . "-" . $nobbk;
			return $nobbk;
		}
	}
	function insertbbkheader($ispb, $dspb, $ibbk, $dbbk, $ibbktype, $eremark, $iarea, $isalesman)
	{
		$this->db->set(
			array(
				'i_bbk'					=> $ibbk,
				'i_bbk_type'			=> $ibbktype,
				'i_refference_document'	=> $ispb,
				'd_refference_document'	=> $dspb,
				'd_bbk'					=> $dbbk,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea,
				'i_salesman'			=> $isalesman
			)
		);

		$this->db->insert('tm_bbk');
	}
	function insertbbkdetail(
		$iproduct,
		$iproductgrade,
		$eproductname,
		$iproductmotif,
		$nquantity,
		$vunitprice,
		$ispb,
		$ibbk,
		$eremark,
		$dspb,
		$ibbktype,
		$istore,
		$istorelocation,
		$istorelocationbin
	) {
		$this->db->set(
			array(
				'i_bbk'				          => $ibbk,
				'i_bbk_type'			      => $ibbktype,
				'i_refference_document'	=> $ispb,
				'i_product'			        => $iproduct,
				'i_product_motif'		    => $iproductmotif,
				'i_product_grade'		    => $iproductgrade,
				'e_product_name'		    => $eproductname,
				'n_quantity'			      => $nquantity,
				'v_unit_price'			    => $vunitprice,
				'e_remark'			        => $eremark,
				'd_refference_document'	=> $dspb
				//				'i_store'				=> $istore,
				//				'i_store_location'		=> $istorelocation,
				//				'i_store_locationbin'	=> $istorelocationbin
			)
		);

		$this->db->insert('tm_bbk_item');
	}
	function runningnumbersj($iarea)
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_sj,9,6)) as max from tm_nota where substr(i_sj,4,2)='$th' and i_area_from='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nosj  = $terakhir + 1;
			settype($nosj, "string");
			$a = strlen($nosj);
			while ($a < 6) {
				$nosj = "0" . $nosj;
				$a = strlen($nosj);
			}
			$nosj  = "SJ-" . $thbl . "-" . $nosj;
			return $nosj;
		} else {
			$nosj  = "000001";
			$nosj  = "SJ-" . $thbl . "-" . $nosj;
			return $nosj;
		}
	}
	function insertsjheader(
		$ispb,
		$dspb,
		$isj,
		$dsj,
		$isjtype,
		$iarea,
		$isalesman,
		$icustomer,
		$nspbdiscount1,
		$nspbdiscount2,
		$nspbdiscount3,
		$vspbdiscount1,
		$vspbdiscount2,
		$vspbdiscount3,
		$vspbdiscounttotal,
		$vspbgross,
		$vspbnetto
	) {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjentry	= $row->c;
		$this->db->set(
			array(
				'i_sj'					=> $isj,
				'i_sj_type'				=> $isjtype,
				'i_spb'					=> $ispb,
				'd_spb'					=> $dspb,
				'd_sj'					=> $dsj,
				'i_area_from'			=> $iarea,
				'i_area_to'				=> $iarea,
				'i_salesman'			=> $isalesman,
				'i_refference_document'	=> $ispb,
				'i_customer'			=> $icustomer,
				'n_sj_discount1'		=> $nspbdiscount1,
				'n_sj_discount2'		=> $nspbdiscount2,
				'n_sj_discount3' 		=> $nspbdiscount3,
				'v_sj_discount1'		=> $vspbdiscount1,
				'v_sj_discount2'		=> $vspbdiscount2,
				'v_sj_discount3'		=> $vspbdiscount3,
				'v_sj_discounttotal'	=> $vspbdiscounttotal,
				'v_sj_gross'			=> $vspbgross,
				'v_sj_netto'			=> $vspbnetto,
				'd_sj_entry'			=> $dsjentry
			)
		);

		$this->db->insert('tm_nota');
	}
	function insertsjdetail(
		$iproduct,
		$iproductgrade,
		$iproductmotif,
		$eproductname,
		$norder,
		$ndeliver,
		$vunitprice,
		$ispb,
		$dspb,
		$isj,
		$isjtype,
		$dsj,
		$iarea,
		$isalesman,
		$istore,
		$istorelocation,
		$istorelocationbin
	) {
		$this->db->set(
			array(
				'i_sj'					=> $isj,
				'i_sj_type'				=> $isjtype,
				'd_sj'					=> $dsj,
				'i_area_from'			=> $iarea,
				'i_area_to'				=> $iarea,
				'i_refference_document'	=> $ispb,
				'i_product'				=> $iproduct,
				'i_product_motif'		=> $iproductmotif,
				'i_product_grade'		=> $iproductgrade,
				'e_product_name'		=> $eproductname,
				'n_quantity_order'		=> $norder,
				'n_quantity_deliver'	=> $ndeliver,
				'v_unit_price'			=> $vunitprice,
				'i_salesman'			=> $isalesman,
				'i_store'				=> $istore,
				'i_store_location'		=> $istorelocation,
				'i_store_locationbin'	=> $istorelocationbin
			)
		);

		$this->db->insert('tm_sj_item');
	}

	function updatespb($ispb, $iarea)
	{
		/* 		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dprint	= $row->c; */

		$this->db->query("update tm_spb set n_print_gudang = n_print_gudang+1 where i_spb='$ispb' AND i_area = '$iarea'");
	}
}
