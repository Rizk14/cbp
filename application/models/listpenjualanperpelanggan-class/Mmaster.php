<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, b.i_customer_class, d.e_customer_classname,  sum(a.v_nota_gross) as nota,
                          sum(a.v_nota_netto) as bersih
                          from tm_nota a, tr_customer b, tr_city c, tr_customer_class d
                          where a.f_nota_cancel='f' 
                          and to_char(a.d_nota,'yyyymm')='$iperiode' 
                          and not a.i_nota isnull and a.i_area='$iarea'
                          and a.i_customer=b.i_customer 
                          and b.i_city=c.i_city and b.i_area=c.i_area 
                          and b.i_customer_class=d.i_customer_class
                          group by a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, b.i_customer_class, d.e_customer_classname
                          order by d.e_customer_classname ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
