<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($inotapb,$icustomer)
    {
		$this->db->select("a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_notapb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_notapb ='$inotapb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($inotapb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_notapb_item a, tr_product_motif b
						 where a.i_notapb = '$inotapb' and a.i_customer='$icustomer' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_notapb'    => $inotapb,
      'i_area'      => $iarea,
      'i_spg'       => $ispg,
      'i_customer'  => $icustomer,
      'd_notapb'    => $dnotapb,
      'n_notapb_discount' => $nnotapbdiscount,
      'v_notapb_discount' => $vnotapbdiscount,
      'v_notapb_gross'    => $vnotapbgross,
      'f_notapb_cancel'   => 'f',
      'd_notapb_entry'    => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_notapb');
    }
    function insertdetail($inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_notapb'        => $inotapb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_notapb'        => $dnotapb,
          'i_product'       => $iproduct,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity'      => $nquantity,
          'v_unit_price'    => $vunitprice,
          'd_notapb_entry'  => $dentry,
          'n_item_no'       => $i,
          'e_product_name'  => $eproductname,
          'i_price_groupco' => $ipricegroupco,
          'e_remark'        => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_notapb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($iproduct, $iproductgrade, $inotapb, $iarea, $icustomer, $iproductmotif) 
    {
		  $this->db->query("DELETE FROM tm_notapb_item WHERE i_notapb='$inotapb' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_customer='$icustomer'");
    }
	
    public function deleteheader($xinotapb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_notapb WHERE i_notapb='$xinotapb' and i_area='$iarea' and i_customer='$icustomer'");
    }
    function bacaproduct($num,$offset,$cari,$cust)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, b.v_product_retail as harga, c.e_product_name as nama
						                    from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d
						                    where a.i_product=c.i_product and a.i_product=b.i_product 
						                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
						                    and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
                                order by c.i_product, a.e_product_motifname, b.i_price_group
                                limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariproduct($cari,$num,$offset)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								  a.e_product_motifname as namamotif, 
								  c.e_product_name as nama,c.v_product_retail as harga
								  from tr_product_motif a,tr_product c
								  where a.i_product=c.i_product
							     	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								  order by a.e_product_motifname asc limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		  if($allarea=='t') {
			  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
							     or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		  if($allarea=='t') {
			  $this->db->select("* from tr_area
                           where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                           order by i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select("* from tr_area where (i_area='$area1' or i_area='$area2' or i_area='$area3'
							     or i_area='$area4' or i_area='$area5')
                   and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                   order by i_area", false)->limit($num,$offset);			
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacaspg($num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		  if($allarea=='t') {
			  $this->db->select(" a.*, b.e_customer_name
                            from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            order by a.i_spg", false)->limit($num,$offset);
		  }else{
			  $this->db->select(" a.*, b.e_customer_name
                            from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3'
                            or a.i_area='$area4' or a.i_area='$area5')
                            order by a.i_spg", false)->limit($num,$offset);			
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function carispg($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		  if($allarea=='t') {
			  $this->db->select(" a.*, b.e_customer_name
                            from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or
                            upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                            order by a.i_spg", false)->limit($num,$offset);
		  }else{
			  $this->db->select(" a.*, b.e_customer_name
                            from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3'
                            or a.i_area='$area4' or a.i_area='$area5')
                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or
                            upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                            order by a.i_spg", false)->limit($num,$offset);			
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment
                                set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode,$q_aw,$q_ak)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi_consigment
                                (
                                  i_product,i_product_motif,i_product_grade,i_customer,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                  n_saldo_akhir,n_saldo_stockopname,f_mutasi_close, n_mutasi_git)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$icustomer','$emutasiperiode',$q_aw,0,0,$qsj,0,$q_ak-$qsj,0,'f',$qsj)
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic_consigment set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic_consigment
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$icustomer', '$eproductname', 0-$qsj, 't'
                                )
                              ",false);
    }
}
?>
