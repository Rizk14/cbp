<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }
  function bacaarea($cari, $num, $offset, $iuser)
  {
    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacano($no, $a, $b)
  {
    if ($a . $b >= "2204") {
      $this->db->select(" * from (
                            SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                            replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                            '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                            null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                            null as TanggalSSP, 
                            cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.11) as DPP, 
                            round((a.v_nota_netto/1.11)*0.11) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                            from tm_nota a, tm_spb c, tr_customer b
                            left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                            where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                            and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                            and length(trim(a.i_seri_pajak))<=6
                            union all
                            SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                            '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                            '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                            null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                            null as TanggalSSP, 
                            cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.11) as DPP, 
                            round((a.v_nota_netto/1.11)*0.11) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                            from tm_nota a, tm_spb c, tr_customer b
                            where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                            and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                            and length(trim(a.i_seri_pajak))<=6
                            union all
                            SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                            replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                            a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                            null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                            null as TanggalSSP, 
                            cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.11) as DPP, 
                            round((a.v_nota_netto/1.11)*0.11) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                            from tm_nota a, tm_spb c, tr_customer b
                            left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                            where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                            and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                            and length(trim(a.i_seri_pajak))>6
                            union all
                            SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                            '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                            a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                            null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                            null as TanggalSSP, 
                            cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.11) as DPP, 
                            round((a.v_nota_netto/1.11)*0.11) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                            from tm_nota a, tm_spb c, tr_customer b
                            where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                            and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                            and length(trim(a.i_seri_pajak))>6
                            ) as a
                            order by a.NoFaktur_Dokumen", false);
    } else {
      $this->db->select(" * from (
                              SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                              replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                              '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                              null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                              null as TanggalSSP, 
                              cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                              round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                              from tm_nota a, tm_spb c, tr_customer b
                              left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                              where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                              and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                              and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                              and length(trim(a.i_seri_pajak))<=6
                              union all
                              SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                              '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                              '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                              null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                              null as TanggalSSP, 
                              cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                              round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                              from tm_nota a, tm_spb c, tr_customer b
                              where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                              and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                              and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                              and length(trim(a.i_seri_pajak))<=6
                              union all
                              SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                              replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                              a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                              null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                              null as TanggalSSP, 
                              cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                              round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                              from tm_nota a, tm_spb c, tr_customer b
                              left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                              where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                              and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                              and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                              and length(trim(a.i_seri_pajak))>6
                              union all
                              SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                              '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                              a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                              null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                              null as TanggalSSP, 
                              cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                              round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                              from tm_nota a, tm_spb c, tr_customer b
                              where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                              and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                              and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                              and length(trim(a.i_seri_pajak))>6
                              ) as a
                              order by a.NoFaktur_Dokumen", false);
    }
    /* $this->db->select(" * from (
                        SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                        replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                        '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                        null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                        null as TanggalSSP, 
                        cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                        round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                        from tm_nota a, tm_spb c, tr_customer b
                        left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                        where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                        and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                        and length(trim(a.i_seri_pajak))<=6
                        union all
                        SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                        '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                        '010.900-$a.489'||a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                        null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                        null as TanggalSSP, 
                        cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                        round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                        from tm_nota a, tm_spb c, tr_customer b
                        where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                        and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                        and length(trim(a.i_seri_pajak))<=6
                        union all
                        SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                        replace(d.e_customer_pkpnpwp,'.','') as NPWP_NomorPaspor, d.e_customer_pkpname as NamaLawanTrasaksi, 
                        a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                        null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                        null as TanggalSSP, 
                        cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                        round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                        from tm_nota a, tm_spb c, tr_customer b
                        left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                        where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                        and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='t'
                        and length(trim(a.i_seri_pajak))>6
                        union all
                        SELECT 'A' as kodepajak, 2 as kodetransaksi, 1 as kodestatus, 1 as kodedokumen, 0 as Flag_VAT, 
                        '000000000000000' as NPWP_NomorPaspor, b.e_customer_name as NamaLawanTrasaksi, 
                        a.i_seri_pajak as NoFaktur_Dokumen, 0 as JenisDokumen, null as NomorFakturPengganti_Retur, 
                        null as Jenis_Dokumen_Dokumen_Pengganti_Retur, to_char(a.d_pajak, 'dd-mm-yyyy') as Tanggal_Faktur_Dokumen, 
                        null as TanggalSSP, 
                        cast('$b' as text)||'$b' as MasaPajak, '20'||'$a' as TahunPajak, 0 as Pembetulan, round(a.v_nota_netto/1.1) as DPP, 
                        round((a.v_nota_netto/1.1)*0.1) as PPN, 0 as PPnBM, a.i_area, a.d_nota, a.i_nota
                        from tm_nota a, tm_spb c, tr_customer b
                        where a.i_nota like '$no%' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and not a.i_faktur_komersial isnull and a.n_faktur_komersialprint>0 
                        and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and b.f_customer_pkp='f'
                        and length(trim(a.i_seri_pajak))>6
                        ) as a
                        order by a.NoFaktur_Dokumen",false); */
    $tes = $this->db->get();
    return $tes;
  }
}
