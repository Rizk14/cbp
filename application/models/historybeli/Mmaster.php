<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idtap,$ispb,$iarea) 
    {
			$this->db->query("update tm_dtap set f_dtap_cancel='t' where i_dtap='$idtap' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
						where a.i_supplier=b.i_supplier 
						and a.f_ttb_tolak='f'
						and not a.i_dtap isnull
						and (upper(a.i_dtap) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_supplier) like '%$cari%' 
						or upper(b.e_supplier_name) like '%$cari%')
						order by a.i_salesman, a.i_dtap desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
						where a.i_supplier=b.i_supplier 
						and a.f_ttb_tolak='f'
						and not a.i_dtap isnull
						and (upper(a.i_dtap) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_supplier) like '%$cari%' 
						  or upper(b.e_supplier_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_salesman, a.i_dtap desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
					where a.i_supplier=b.i_supplier and a.f_ttb_tolak='f'
					and not a.i_dtap isnull
					and (upper(a.i_dtap) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_supplier) like '%$cari%' 
					or upper(b.e_supplier_name) like '%$cari%')
					order by a.i_salesman, a.i_dtap desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
					where a.i_supplier=b.i_supplier 
					and a.f_ttb_tolak='f'
					and not a.i_dtap isnull
					and (upper(a.i_dtap) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_supplier) like '%$cari%' 
					  or upper(b.e_supplier_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_salesman, a.i_dtap desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iproduct,$isupplier,$cari,$num,$offset)
    {
		$this->db->select(" a.i_dtap, a.i_supplier, a.d_dtap, a.i_pajak, a.d_pajak, b.e_supplier_name,
												c.i_product, c.e_product_name, c.n_jumlah, c.v_netto
												from tm_dtap a, tr_supplier b, tm_dtap_item c
												where a.i_supplier=b.i_supplier and a.i_dtap=c.i_dtap and a.i_area=c.i_area
												and a.f_dtap_cancel='f' and a.i_supplier='$isupplier' and upper(c.i_product) like '%$iproduct%'
												ORDER BY a.d_dtap desc, a.i_dtap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
							where a.i_supplier=b.i_supplier 
							and a.f_ttb_tolak='f'
              and a.f_dtap_koreksi='f'
							and not a.i_dtap isnull
							and (upper(a.i_dtap) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_supplier) like '%$cari%' 
							  or upper(b.e_supplier_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_salesman, a.i_dtap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_supplier_name from tm_dtap a, tr_supplier b
							where a.i_supplier=b.i_supplier 
							and a.f_ttb_tolak='f'
              and a.f_dtap_koreksi='f'
							and not a.i_dtap isnull
							and (upper(a.i_dtap) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_supplier) like '%$cari%' 
							  or upper(b.e_supplier_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_salesman, a.i_dtap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
