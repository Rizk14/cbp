<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacabbmretur($dfrom,$dto,$iarea,$cari,$num,$offset)
    {
		$this->db->select(" i_bbm, i_area from tm_bbm where i_bbm like '%$cari%' and f_bbm_cancel='f' 
                        and substring(i_bbm,10,2)='$iarea' and
                        d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
                        d_bbm <= to_date('$dto','dd-mm-yyyy')
                        order by i_bbm desc", false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caribbmretur($dfrom,$dto,$iarea,$cari,$num,$offset)
    {
		$this->db->select("	i_bbm from tm_bbm where i_bbm like '%$cari%' and f_bbm_cancel='f' 
                        and substring(i_bbm,9,2)='$iarea' and
                        d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
                        d_bbm <= to_date('$dto','dd-mm-yyyy')
                        order by i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacabbmreturto($dfrom,$dto,$iarea,$cari,$num,$offset)
    {
		$this->db->select(" i_bbm, i_area from tm_bbm where i_bbm like '%$cari%' and f_bbm_cancel='f'
                        and substring(i_bbm,10,2)='$iarea' and
                        d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
                        d_bbm <= to_date('$dto','dd-mm-yyyy')
                        order by i_bbm desc", false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   
    function caribbmreturto($dfrom,$dto,$iarea,$cari,$num,$offset)
    {
		$this->db->select("	i_bbm from tm_bbm where i_bbm like '%$cari%' and f_bbm_cancel='f'
                        and substring(i_bbm,10,2)='$iarea' and
                        d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
                        d_bbm <= to_date('$dto','dd-mm-yyyy')
                        order by i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

   /* function bacabbmretur($cari,$iarea,$dfrom,$dto,$num,$offset)
    {
		  $this->db->select(" a.i_bbm, a.i_area, b.e_area_name from tm_bbm a, tr_area b
                          where (upper(a.i_bbm) like '%$cari%')
                          and a.i_area='$iarea' and a.i_area=b.i_area
                          and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_bbm", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caribbmretur($cari,$iarea,$dfrom,$dto,$num,$offset)
    {
		  $this->db->select(" a.i_bbm, a.i_area, b.e_area_name from tm_bbm a, tr_area b
                          where (upper(a.i_bbm) like '%$cari%')
                          and a.i_area='$iarea' and a.i_area=b.i_area
                          and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_bbm",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    } */
    function bacamaster($bbmreturfrom,$bbmreturto)
    {
		  $this->db->select("	a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                          a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name, b.i_customer, b.e_customer_name
                          from tm_bbm a, tr_area c, tr_salesman d, tm_ttbretur e, tr_customer b
					                where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                          and a.i_salesman=d.i_salesman
                          and a.i_refference_document=e.i_ttb and a.i_area=e.i_area 
                          and cast(to_char(a.d_refference_document,'yyyy') as numeric)=e.n_ttb_year
                          and e.i_customer=b.i_customer
							            and a.i_bbm >= '$bbmreturfrom' and a.i_bbm <= '$bbmreturto'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ibbm)
    {
		  $this->db->select(" a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                          a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name, b.i_customer, b.e_customer_name
                          from tm_bbm a, tr_area c, tr_salesman d, tm_ttbretur e, tr_customer b
					                where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                          and a.i_salesman=d.i_salesman
                          and a.i_refference_document=e.i_ttb and a.i_area=e.i_area 
                          and cast(to_char(a.d_refference_document,'yyyy') as numeric)=e.n_ttb_year
                          and e.i_customer=b.i_customer
							            and a.i_bbm = '$ibbm' and a.i_bbm_type='05'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ibbm)
    {
		  $this->db->select("	a.i_bbm, a.i_product, a.i_product_motif, a.i_product_grade, 
							          a.e_product_name, a.n_quantity as qty, a.v_unit_price, 
							          b.e_product_motifname, d.n_quantity,
							          d.i_product1, d.i_product1_motif, d.i_product1_grade, d.i_nota, c.i_ttb, c.n_ttb_year, c.i_area
							          from tm_bbm_item a, tr_product_motif b, tm_ttbretur c, tm_ttbretur_item d
					              where a.i_bbm = '$ibbm'
                        and a.i_product=b.i_product 
							          and a.i_product_motif=b.i_product_motif 
							          and a.i_product=d.i_product2
							          and a.i_bbm=c.i_bbm
							          and c.i_ttb=d.i_ttb
							          and c.i_area=d.i_area
							          order by a.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
