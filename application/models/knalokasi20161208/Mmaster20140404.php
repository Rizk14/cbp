<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name,
                               sum(c.v_sisa) as v_sisa from tm_dt a, tr_area b, tm_dt_item c
                             where a.i_area = b.i_area
                             and a.f_sisa = 't'
                             and a.i_dt=c.i_dt and a.i_area=c.i_area
                             and (upper(a.i_dt) like '%$cari%'
                               or upper(b.e_area_name) like '%$cari%'
                               or upper(a.i_area) like '%$cari%')
                             group by a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name
                             order by a.i_dt desc ",false)->limit($num,$offset);
               //and c.v_sisa<>c.v_jumlah
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($cari,$num,$offset)
    {
      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
               where a.i_customer=b.i_customer
               and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
               and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
               or upper(a.i_spb) like '%$cari%' or upper(a.i_nota) like '%$cari%')
               order by a.i_nota desc ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function insertheader(  $ipl,$idt,$iarea,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$ebankname,$vjumlah,$vlebih   )
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $tmp=$this->db->query("select i_pelunasan from tm_pelunasan where i_pelunasan='$ipl' and i_area='$iarea'", false);
    if($tmp->num_rows()>0){
        if($dcair!='' && $dgiro!=''){
           $this->db->query("update tm_pelunasan set i_jenis_bayar='$ijenisbayar',i_giro='$igiro',i_customer='$icustomer',d_giro='$dgiro',d_dt='$ddt',
                          d_bukti='$dbukti',d_cair='$dcair',e_bank_name='$ebankname',v_jumlah=$vjumlah,v_lebih=$vlebih, f_pelunasan_cancel='f',
                          d_update='$dentry'
                                where i_pelunasan='$ipl' and i_area='$iarea'");
        }elseif($dcair=='' && $dgiro!=''){
           $this->db->query("update tm_pelunasan set i_jenis_bayar='$ijenisbayar',i_giro='$igiro',i_customer='$icustomer',d_giro='$dgiro',d_dt='$ddt',
                          d_bukti='$dbukti',e_bank_name='$ebankname',v_jumlah=$vjumlah,v_lebih=$vlebih, f_pelunasan_cancel='f',d_update='$dentry'
                          where i_pelunasan='$ipl' and i_area='$iarea'");
        }elseif($dcair!='' && $dgiro==''){
           $this->db->query("update tm_pelunasan set i_jenis_bayar='$ijenisbayar',i_giro='$igiro',i_customer='$icustomer',d_dt='$ddt',
                          d_bukti='$dbukti',d_cair='$dcair',e_bank_name='$ebankname',v_jumlah=$vjumlah,v_lebih=$vlebih, f_pelunasan_cancel='f',
                          d_update='$dentry'
                          where i_pelunasan='$ipl' and i_area='$iarea'");
        }else{
           $this->db->query("update tm_pelunasan set i_jenis_bayar='$ijenisbayar',i_giro='$igiro',i_customer='$icustomer',d_dt='$ddt',
                          d_bukti='$dbukti',e_bank_name='$ebankname',v_jumlah=$vjumlah,v_lebih=$vlebih, f_pelunasan_cancel='f',d_update='$dentry'
                          where i_pelunasan='$ipl' and i_area='$iarea'");
        }
    }else{
        if($dcair!='' && $dgiro!=''){
           $this->db->query("insert into tm_pelunasan (i_pelunasan,i_dt,i_area,i_jenis_bayar,i_giro,i_customer,
                         d_giro,d_dt,d_bukti,d_cair,e_bank_name,v_jumlah,v_lebih,d_entry)
                          values
                        ('$ipl','$idt','$iarea','$ijenisbayar','$igiro','$icustomer','$dgiro','$ddt','$dbukti','$dcair','$ebankname',$vjumlah,$vlebih,'$dentry')");
        }elseif($dcair=='' && $dgiro!=''){
           $this->db->query("insert into tm_pelunasan (i_pelunasan,i_dt,i_area,i_jenis_bayar,i_giro,i_customer,d_giro,d_dt,d_bukti,
                         e_bank_name,v_jumlah,v_lebih,d_entry)
                         values
                       ('$ipl','$idt','$iarea','$ijenisbayar','$igiro','$icustomer','$dgiro','$ddt','$dbukti','$ebankname',$vjumlah,$vlebih,'$dentry')");
        }elseif($dcair!='' && $dgiro==''){
           $this->db->query("insert into tm_pelunasan (i_pelunasan,i_dt,i_area,i_jenis_bayar,i_giro,i_customer,
                         d_dt,d_bukti,d_cair,e_bank_name,v_jumlah,v_lebih,d_entry)
                          values
                        ('$ipl','$idt','$iarea','$ijenisbayar','$igiro','$icustomer','$ddt','$dbukti','$dcair','$ebankname',$vjumlah,$vlebih,'$dentry')");
        }else{
           $this->db->query("insert into tm_pelunasan (i_pelunasan,i_dt,i_area,i_jenis_bayar,i_giro,i_customer,
                         d_dt,d_bukti,e_bank_name,v_jumlah,v_lebih,d_entry)
                          values
                        ('$ipl','$idt','$iarea','$ijenisbayar','$igiro','$icustomer','$ddt','$dbukti','$ebankname',$vjumlah,$vlebih,'$dentry')");
        }
    }
  }
   function jmlasalkn(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->select("* from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function deleteheader(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->query("delete from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
   }
   function updategiro($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_giro set v_sisa=v_sisa-$pengurang+$asal, f_giro_use='t'
                    where i_giro='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where i_customer_groupbayar='$group'))");
    }
   function updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear)
    {
      $this->db->query("update tm_kum set v_sisa=v_sisa-$pengurang+$asal
                          where i_kum='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group')) and n_kum_year='$nkuyear'");
    }
   function updatekn($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_kn set v_sisa=v_sisa-$pengurang+$asal
                        where i_kn='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar
                        where i_customer_groupbayar='$group'))");
    }
   function updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal)
    {
      $this->db->query("update tm_pelunasan_lebih set v_lebih=0
                          where i_pelunasan='$egirobank' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group'))");
    }
  function updatesaldo($group,$icustomer,$pengurang)
    {
      $this->db->query("update tr_customer_groupar set v_saldo=v_saldo-$pengurang
                          where i_customer='$icustomer' and i_customer_groupar='$group'");
    }
   function insertdetail($ipl,$idt,$iarea,$inota,$dnota,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark)
  {
    $tmp=$this->db->query(" select i_pelunasan from tm_pelunasan_item
                            where i_pelunasan='$ipl' and i_area='$iarea' and i_nota='$inota'", false);
    if($tmp->num_rows()>0){
      $this->db->query("update tm_pelunasan_item set d_nota='$dnota',d_dt='$ddt',v_jumlah=$vjumlah,v_sisa=$vsisa,n_item_no=$i,
                        i_pelunasan_remark='$ipelunasanremark', e_remark='$eremark'
                        where i_pelunasan='$ipl' and i_area='$iarea' and i_nota='$inota'");
    }else{
        $this->db->query("insert into tm_pelunasan_item
                      (i_pelunasan,i_dt,i_area,i_nota,d_nota,d_dt,v_jumlah,v_sisa,n_item_no,i_pelunasan_remark,e_remark)
                      values
                      ('$ipl','$idt','$iarea','$inota','$dnota','$ddt',$vjumlah,$vsisa,$i,'$ipelunasanremark','$eremark')");
    }
  }
   function updatedt($idt,$iarea,$ddt,$inota,$vsisa)
    {
      $this->db->query("update tm_dt_item set v_sisa=$vsisa where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt' and i_nota='$inota'");
    }
   function updatenota($inota,$vsisa)
    {
      $this->db->query("update tm_nota set v_sisa=v_sisa-$vsisa where i_nota='$inota'");
    }
  function hitungsisadt($idt,$iarea,$ddt)
    {
      $this->db->select(" sum(v_sisa) as v_sisa from tm_dt_item
               where i_area='$iarea'
               and i_dt='$idt' and d_dt='$ddt'
               group by i_dt, i_area",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
            $jml=$row->v_sisa;
         }
         return $jml;
      }
  }
   function updatestatusdt($idt,$iarea,$ddt)
    {
      $this->db->query("update tm_dt set f_sisa='f' where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'");
    }
   function deletedetail($ipl,$idt,$iarea,$inota,$ddt)
    {
      $this->db->query("DELETE FROM tm_pelunasan_item WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'
                      and i_nota='$inota' and d_dt='$ddt'");
    }
   function bacacustomer($idt,$iarea,$num,$offset){
      $this->db->select(" distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.v_sisa>0
                     and a.i_dt = '$idt' and a.i_area = '$iarea'
                     order by a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caricustomer($cari,$idt,$iarea,$num,$offset){
      $this->db->select(" distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.v_sisa>0
                     and a.i_dt = '$idt' and a.i_area = '$iarea'
              and (upper(b.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                     order by a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagiro($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select(" a.* from tm_giro a, tr_customer_groupar b
                     where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_giro a, tr_customer_groupbayar b
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
#                    and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function carigiro($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_giro a, tr_customer_groupar b
							where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
							and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
              and (upper(a.i_giro) like '%$cari%') and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
							order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
   function bacakn($icustomer,$iarea,$num,$offset,$group,$xdbukti){
      /*$this->db->select(" a.* from tm_kn a, tr_customer_groupar b
                               where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0
                               order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      */

      $this->db->select(" a.* from tm_kn a, tr_customer_groupbayar b
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                     and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f'
                     order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);

      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carikn($cari,$icustomer,$iarea,$num,$offset,$group,$xdbukti){
      $this->db->select(" a.* from tm_kn a, tr_customer_groupar b
                               where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0 and d_kn<='$xdbukti'
                        and (upper(i_kn) like '%$cari%') and a.f_kn_cancel='f'
                               order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaku($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select("a.* from tm_kum a, tr_customer_groupar b
                         where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.d_kum<='$dbukti'
                         and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                         order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_kum a, tr_customer_groupbayar b
                             where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                             and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f'
                             order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function cariku($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_kum a, tr_customer_groupar b
					              where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
					              and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and (upper(a.i_kum) like '%$cari%') and a.d_kum<='$dbukti'
					              order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
   function bacaku2($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.* from tm_kum a
               where a.i_customer='$icustomer'
               and a.i_area='$iarea'
               and a.v_sisa>0
               and a.f_close='f'
               order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagirocek($num,$offset,$area){
    if($area=='00'||$area=='PB'){
      $this->db->select(" * from tr_jenis_bayar order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_jenis_bayar where i_jenis_bayar<>'05' order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function runningnumberpl($idt,$icustomer,$inotax){
    $this->db->select(" n_item_no from tm_dt_item
                        where i_dt='$idt' and i_customer='$icustomer' and i_nota='$inotax'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
      foreach($query->result() as $tmp){
        $nopl=$tmp->n_item_no;
        break;
      }
    }
    settype($nopl,"string");
      $a=strlen($nopl);
      while($a<2){
        $nopl="0".$nopl;
        $a=strlen($nopl);
      }
      $nopl  = $idt."-".$nopl;
      return $nopl;
  }
   function bacapelunasan($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan
               from tm_pelunasan_lebih a, tr_customer_groupbayar b
            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area ",FALSE)->limit($num,$offset);
#              and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caripelunasan($cari,$icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
                and (upper(a.i_pelunasan) like '%$cari%') 
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
  function cekpl($iarea,$ipl,$idt){
      $this->db->select(" i_pelunasan from tm_pelunasan where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        $this->db->select(" max(substring(i_pelunasan,9,2)) as no from tm_pelunasan where i_pelunasan like '$idt%' and i_area='$iarea'",FALSE);
        $quer = $this->db->get();
        if ($quer->num_rows() > 0){
          foreach($quer->result() as $tmp){
            $nopl=$tmp->no+1;
            break;
          }
        }
        settype($nopl,"string");
        $a=strlen($nopl);
        while($a<2){
          $nopl="0".$nopl;
          $a=strlen($nopl);
        }
        $nopl  = $idt."-".$nopl.substr($ipl,10,1);
        return $nopl;
      }else{
      return $ipl;
    }
   }
  function bacapl($iarea,$ipl,$idt){
    $year='20'.substr($idt,5,2);
    settype($year,"integer");

      $xdt=strtoupper($idt);
      $xpl=strtoupper($ipl);
      $this->db->select(" a.*, b.e_area_name, c.e_customer_name, d.e_jenis_bayarname, e.d_dt, c.e_customer_address, c.e_customer_city,
                        f.d_giro_duedate as d_giro_jt, f.d_giro_cair, g.d_kum
                             from tm_pelunasan a
                             inner join tr_area b on (a.i_area=b.i_area)
                             inner join tr_customer c on (a.i_customer=c.i_customer)
                             inner join tr_jenis_bayar d on (a.i_jenis_bayar=d.i_jenis_bayar)
                             inner join tm_dt e on (a.i_dt=e.i_dt and a.i_area=e.i_area)
                             left join tm_giro f on (a.i_giro=f.i_giro and a.i_area=f.i_area)
                             left join tm_kum g on (a.i_giro=g.i_kum and a.i_area=g.i_area and g.n_kum_year=$year)
                             where
                             upper(a.i_dt)='$xdt'
                             and upper(a.i_pelunasan)='$xpl' and upper(a.i_area)='$iarea'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function sisa($iarea,$ipl,$idt){
      $this->db->select(" sum(v_sisa)as sisa
                     from tm_dt_item
                     where i_area='$iarea'
                     and i_dt='$idt'",FALSE);
/*
      $this->db->select(" sum(v_sisa+v_jumlah)as sisa
                     from tm_pelunasan_item
                     where i_pelunasan='$ipl'
                     and i_area='$iarea'
                     and i_dt='$idt'",FALSE);
*/
      $query = $this->db->get();
      foreach($query->result() as $isi){
         return $isi->sisa;
      }
   }
   function bacadetailpl($iarea,$ipl,$idt){
      $this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_nota_netto as v_nota, g.e_pelunasan_remark from tm_pelunasan_item a
                inner join tm_nota b on (a.i_nota=b.i_nota)
            left join tr_pelunasan_remark g on(a.i_pelunasan_remark=g.i_pelunasan_remark)
               where a.i_pelunasan = '$ipl'
               and a.i_area='$iarea'
               and a.i_dt='$idt'
               order by a.i_pelunasan,a.i_area ",FALSE);
#
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacanota($iarea,$idt,$icustomer,$num,$offset,$group){
      $this->db->select(" a.i_dt, a.d_dt, a.v_jumlah, a.i_customer, c.d_nota, a.i_nota, c.v_sisa
                     from tm_dt_item a, tr_customer_groupbayar b, tm_nota c
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                     and a.i_dt='$idt' and a.i_nota=c.i_nota
                     and a.i_area='$iarea' and c.v_sisa>0
                     order by a.i_nota ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carinota($cari,$iarea,$idt,$icustomer,$num,$offset,$group){
      $this->db->select(" a.i_dt, a.d_dt, a.v_jumlah, a.i_customer, c.d_nota, a.i_nota, c.v_sisa
                        from tm_dt_item a, tr_customer_groupbayar b, tm_nota c
                             where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                             and a.i_dt='$idt' and a.i_nota=c.i_nota
                             and a.i_area='$iarea'

                             and (upper(a.i_nota) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          or upper(b.e_customer_name) like '%$cari%')
                           order by a.i_nota ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
   }

   function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5) {
      $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
      }

   function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
   {
      $this->db->select(" a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah,
                                    b.e_area_name, sum(c.v_sisa) as v_sisa from tm_dt a, tr_area b, tm_dt_item c
                                    where a.i_area=b.i_area
                                    and a.f_sisa = 't'
                                    and a.i_dt=c.i_dt and a.i_area=c.i_area
                                    and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
                                    and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                                    and upper(a.i_area)='$iarea'
                        and (upper(a.i_dt) like '%$cari%')
                                    group by a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name
                        order by a.i_dt, a.i_area, a.d_dt",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaremark($num,$offset) {
      $this->db->select("* from tr_pelunasan_remark order by i_pelunasan_remark", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
}
?>
