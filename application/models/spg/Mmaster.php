<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispg)
    {
		$this->db->select(" a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                        from tr_spg a, tr_customer b, tr_area c
                        where a.i_customer=b.i_customer and a.i_area=c.i_area
                        and a.i_spg = '$ispg' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($ispg,$espgname,$icustomer,$iarea)
    {
    	$this->db->set(
    		array(
    			 'i_spg'      => $ispg,
           'e_spg_name' => $espgname,
           'i_customer' => $icustomer,
           'i_area'     => $iarea
    		)
    	);
    	$this->db->insert('tr_spg');
    }
    function update($ispg,$espgname,$icustomer,$iarea)
    {
    	$data = array(
               'e_spg_name' => $espgname,
               'i_customer' => $icustomer,
               'i_area'     => $iarea
            );
  		$this->db->where('i_spg =', $ispg);
  		$this->db->update('tr_spg', $data); 
//		redirect('suppliergroup/cform/');
    }
	
    public function delete($icustomer) 
    {
  		$this->db->query("DELETE FROM tr_spg WHERE i_spg='$ispg'", false);
  		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
  		$this->db->select(" a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                          order by a.i_spg", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
                          or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%')
                          order by a.i_spg", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacapelanggan($num,$offset) {
		  $this->db->select(" a.*, b.e_area_name from tr_customer a, tr_area b where a.i_area = b.i_area
		                      order by i_area, e_customer_name asc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
	  }
    function caripelanggan($cari,$num,$offset) {
		$this->db->select(" a.*, b.e_area_name from tr_customer a, tr_area b where a.i_area = b.i_area and
		                    (upper(a.i_customer) ilike '%$cari%' or upper(a.e_customer_name) ilike '%$cari%')
                         order by a.i_area, a.e_customer_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacaarea($cari,$num,$offset)
    {
		  $this->db->select("i_area, e_area_name from tr_area where upper(i_area) like '%$cari%'
							or upper(e_area_name) like '%$cari%'order by i_area",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
