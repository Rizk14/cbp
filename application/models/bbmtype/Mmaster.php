<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibbmtype)
    {
		$this->db->select('i_bbm_type, e_bbm_typename')->from('tr_bbm_type')->where('i_bbm_type', $ibbmtype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($ibbmtype, $ebbmtypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
    			'i_bbm_type' 		=> $ibbmtype,
    			'e_bbm_typename' 	=> $ebbmtypename,
				'd_bbm_typeentry' 	=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_bbm_type');
		redirect('bbmtype/cform/');
    }
    function update($ibbmtype, $ebbmtypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
    	$data = array(
               'i_bbm_type' => $ibbmtype,
               'e_bbm_typename' => $ebbmtypename,
	       'd_bbm_typeupdate' => $dupdate
            );
		$this->db->where('i_bbm_type', $ibbmtype);
		$this->db->update('tr_bbm_type', $data); 
		redirect('bbmtype/cform/');
    }
	
    public function delete($ibbmtype) 
    {
		$this->db->query('DELETE FROM tr_bbm_type WHERE i_bbm_type=\''.$ibbmtype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_bbm_type, e_bbm_typename from tr_bbm_type order by i_bbm_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
