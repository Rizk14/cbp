<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_kn set f_kn_cancel='t' where i_kn='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
						where a.i_customer=b.i_customer
            and a.f_kn_cancel='f'
						and not a.i_kn isnull
						and (upper(a.i_kn) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_kn desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
						where a.i_customer=b.i_customer 
            and a.f_kn_cancel='f'
						and not a.i_kn isnull
						and (upper(a.i_kn) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_kn desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" a.*, b.e_customer_name from tm_kn a, tr_customer b
												where a.i_customer=b.i_customer 
                        and a.f_kn_cancel='f'
												and not a.i_kn isnull
												and (upper(a.i_kn) like '%$cari%'
												or upper(a.i_customer) like '%$cari%' 
												or upper(b.e_customer_name) like '%$cari%')
					              order by a.i_kn desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
												where a.i_customer=b.i_customer 
                        and a.f_kn_cancel='f'
												and not a.i_kn isnull
												and (upper(a.i_kn) like '%$cari%'
												or upper(a.i_customer) like '%$cari%' 
												or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_kn desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_kn a, tr_customer b
												where a.i_customer=b.i_customer 
                        and a.f_kn_cancel='f'
												and not a.i_kn isnull
												and (upper(a.i_kn) like '%$cari%'
												or upper(a.i_customer) like '%$cari%' 
												or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kn ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_kn a, tr_customer b
												where a.i_customer=b.i_customer 
                        and a.f_kn_cancel='f'
												and not a.i_kn isnull
												and (upper(a.i_kn) like '%$cari%'
												or upper(a.i_customer) like '%$cari%' 
												or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kn ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_kn a, tr_customer b
												where a.i_customer=b.i_customer 
                        and a.f_kn_cancel='f'
												and not a.i_kn isnull
												and (upper(a.i_kn) like '%$cari%'
												or upper(a.i_customer) like '%$cari%' 
												or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kn ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacabayar($ikn,$iarea)
  {
	  /*$this->db->select(" a.i_kn, a.d_kn, a.v_netto,  b.i_pelunasan, b.d_bukti, c.v_jumlah, c.i_nota, c.d_nota
                        from tm_pelunasan_item c, tm_kn a
                        left join tm_pelunasan b on(b.f_pelunasan_cancel='f' and b.f_giro_tolak='f' and b.f_giro_batal='f'
                        and a.i_area=b.i_area and a.i_kn=b.i_giro and a.d_kn=b.d_giro)
                        where a.i_kn='$ikn' and a.i_area='$iarea' and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area
                        order by b.d_bukti", false);
    select a.i_kn, a.d_kn, a.v_netto, b.i_pelunasan, b.d_bukti, b.v_jumlah, c.i_nota, c.d_nota
    from tm_kn a
    left join tm_pelunasan b on (a.i_area=b.i_area and a.i_kn=b.i_giro and a.i_customer=b.i_customer and
			    b.f_pelunasan_cancel='f')
    left join tm_pelunasan_item c on (a.i_area=c.i_area and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area)
    where
    a.i_kn='K0029005' and a.i_area='07'
    order by b.d_bukti
    */
    $this->db->select(" a.i_kn, a.d_kn, a.v_netto, b.i_pelunasan, b.d_bukti, c.v_jumlah, c.i_nota, c.d_nota
                        from tm_kn a
                        left join tm_pelunasan b on (a.i_area=b.i_area and a.i_kn=b.i_giro and a.i_customer=b.i_customer and
				                        b.f_pelunasan_cancel='f')
                        left join tm_pelunasan_item c on (b.i_pelunasan=c.i_pelunasan and a.i_area=c.i_area and b.i_area=c.i_area)
                        where
                        a.i_kn='$ikn' and a.i_area='$iarea'
                        order by b.d_bukti", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
