<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }

  function baca($iperiode)
  {
    $a     = substr($iperiode, 2, 2);
    $b     = substr($iperiode, 4, 2);
    $peri   = mbulan($b) . " - " . $a;
    $no   = 'FP-' . $a . $b . '-%';

    if ($iperiode > '202203') {
      $this->db->select(" substring(a.i_nota, 9, 2) as i_area, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.i_spb, a.d_spb, 
                          a.v_nota_netto, a.i_customer,b.e_customer_name, a.i_faktur_komersial, a.i_seri_pajak, a.i_salesman, a.v_nota_gross, 
                          a.v_nota_discounttotal, a.v_nota_netto, 
                          sum(c.v_dpp) as v_nota_dpp, sum(c.v_ppn) AS v_nota_ppn, b.f_customer_pkp, a.f_nota_cancel, a.d_nota_entry
                          from tm_nota a, tr_customer b, tm_nota_item c
                          where a.i_nota like '$no%' and a.i_customer=b.i_customer AND a.i_sj = c.i_sj AND a.i_area = c.i_area
                          GROUP BY a.i_nota, a.i_sj, a.d_sj, a.d_nota, a.d_jatuh_tempo, a.i_spb, a.d_spb, 
                          a.v_nota_netto, a.i_customer, a.i_faktur_komersial, a.i_seri_pajak, a.i_salesman, a.v_nota_gross, 
                          a.v_nota_discounttotal, a.v_nota_netto, b.f_customer_pkp, a.f_nota_cancel, a.i_area, b.e_customer_name
                          order by a.i_faktur_komersial, a.i_area, a.d_nota_entry ", false);
    } else {
      $this->db->select("	substring(a.i_nota, 9, 2) as i_area, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.i_spb, a.d_spb, 
                          a.v_nota_netto, a.i_customer,b.e_customer_name, a.i_faktur_komersial, a.i_seri_pajak, a.i_salesman, a.v_nota_gross, 
                          a.v_nota_discounttotal, a.v_nota_netto, cast(a.v_nota_netto/1.1 as integer) as v_nota_dpp, a.v_nota_ppn, b.f_customer_pkp, a.f_nota_cancel, a.d_nota_entry
                          from tm_nota a, tr_customer b 
                          where a.i_nota like '$no' and a.i_customer=b.i_customer
                          order by a.i_faktur_komersial, a.i_area,b.e_customer_name ", false);
      #                          order by a.d_nota, a.i_area, a.i_nota",false);
    }

    return $this->db->get();

    // if ($query->num_rows() > 0) {
    //   return $query->result();
    // }
  }

  function bacaexcel($iperiode, $istore, $cari)
  {
    $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ", false); #->limit($num,$offset);
    $query = $this->db->get();
    return $query;
    #		  if ($query->num_rows() > 0){
    #			  return $query->result();
    #		  }
  }
  function proses($iperiode, $iarea)
  {
    #      if($iarea=='00') $store='AA'; else $store=$iarea;
    $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
    $st = $query->row();
    $store = $st->i_store;
    $this->db->select("	* from tm_ic where i_store='$store'");
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $gie) {
        $this->db->select("	* from vmutasi where periode='$iperiode' and area='$iarea' and product='$gie->i_product'");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
          foreach ($query->result() as $vie) {
            if ($vie->pembelian == null) $vie->pembelian = 0;
            if ($vie->retur == null) $vie->retur = 0;
            if ($vie->bbm == null) $vie->bbm = 0;
            if ($vie->penjualan == null) $vie->penjualan = 0;
            if ($vie->bbk == null) $vie->bbk = 0;
            ###########
            $blawal = substr($iperiode, 4, 2) - 1;
            if ($blawal == 0) {
              $perawal = substr($iperiode, 0, 4) - 1;
              $perawal = $perawal . '12';
            } else {
              $perawal = substr($iperiode, 0, 4);
              $perawal = $perawal . (substr($iperiode, 4, 2) - 1);
            }
            #              $this->db->select("	* from tm_mutasi where e_mutasi_periode='$perawal' and i_store='$store' and i_product='$vie->product'");
            $this->db->select("	* from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
              foreach ($query->result() as $no) {
                $sawal = $no->n_saldo_awal; #stockopname;
              }
              ###########
              $this->db->query("	update tm_mutasi set 
              	                    n_saldo_awal=$sawal,
		                                n_mutasi_pembelian=$vie->pembelian,
		                                n_mutasi_returoutlet=$vie->retur,
		                                n_mutasi_bbm=$vie->bbm,
		                                n_mutasi_penjualan=$vie->penjualan,
		                                n_mutasi_returpabrik=0,
		                                n_mutasi_bbk=$vie->bbk,
		                                n_saldo_akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk)
		                                where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
              ###########
            } else {
              #                $query=$this->db->query(" select * from tm_stockopname_item 
              #                                          where to_char(d_stockopname,'yyyymm')='$perawal' and i_area='$iarea' and i_product='$vie->product'");
              $query = $this->db->query(" select * from tm_stockopname_item 
                                          where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$vie->product'");
              if ($query->num_rows() > 0) {
                foreach ($query->result() as $ni) {
                  $sawal = $ni->n_stockopname;
                }
                ###########
                $akhir = $sawal + ($vie->pembelian + $vie->retur + $vie->bbm) - ($vie->penjualan + $vie->bbk);
                if (substr($vie->product, 0, 1) == 'Z') $grade = 'B';
                else $grade = 'A';
                if ($store == 'AA') $loc = '01';
                else $loc = '00';
                $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
				                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
				                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
				                                                     n_mutasi_bbk,n_saldo_akhir)
				                                                     values
				                                                    ('$iperiode','$store','$vie->product','$grade','00','$loc','00',$sawal,
				                                                     $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir)");
                ###########
              }
            }
            $this->db->select("	* from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
            $que = $this->db->get();
            if ($que->num_rows() > 0) {
              foreach ($que->result() as $vei) {
                $query   = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row     = $query->row();
                if ($row->c == $iperiode) {
                  $this->db->query("	update tm_ic set 
                                        n_quantity_stock=$vei->n_saldo_akhir
                                        where i_store='$store' and i_product='$vei->i_product'");
                }
              }
            }
          }
        } else {
          $pembelian = 0;
          $retur = 0;
          $bbm = 0;
          $penjualan = 0;
          $bbk = 0;
          $blawal = substr($iperiode, 4, 2) - 1;
          if ($blawal == 0) {
            $perawal = substr($iperiode, 0, 4) - 1;
            $perawal = $perawal . '12';
          } else {
            $perawal = substr($iperiode, 0, 4);
            $perawal = $perawal . (substr($iperiode, 4, 2) - 1);
          }
          $this->db->select("	* from tm_mutasi where e_mutasi_periode='$perawal' and i_store='$store' and i_product='$gie->i_product'");
          $query = $this->db->get();
          if ($query->num_rows() > 0) {
            foreach ($query->result() as $no) {
              $sawal = $no->n_saldo_stockopname;
            }
          } else {
            #              $query=$this->db->query(" select * from tm_stockopname_item 
            #                                        where to_char(d_stockopname,'yyyymm')='$perawal' and i_area='$iarea' and i_product='$gie->i_product'");
            $query = $this->db->query(" select * from tm_stockopname_item 
                                          where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$gie->i_product'");
            if ($query->num_rows() > 0) {
              foreach ($query->result() as $ni) {
                $sawal = $ni->n_stockopname;
              }
            }
          }
          if (!isset($sawal)) $sawal = 0;
          $this->db->query("	update tm_mutasi set 
                                n_saldo_awal=$sawal,
                                n_mutasi_pembelian=$pembelian,
                                n_mutasi_returoutlet=$retur,
                                n_mutasi_bbm=$bbm,
                                n_mutasi_penjualan=$penjualan,
                                n_mutasi_returpabrik=0,
                                n_mutasi_bbk=$bbk,
                                n_saldo_akhir=$sawal+($pembelian+$retur+$bbm)-($penjualan+$bbk)
                                where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$gie->i_product'");
          $this->db->select("	* from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$gie->i_product'");
          $que = $this->db->get();
          if ($que->num_rows() > 0) {
            foreach ($que->result() as $vei) {
              $query   = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
              $row     = $query->row();
              if ($row->c == $iperiode) {
                $this->db->query("	update tm_ic set 
                                      n_quantity_stock=$vei->n_saldo_akhir
                                      where i_store='$store' and i_product='$vei->i_product'");
              }
            }
          }
          #####            
        }
      }
    }
  }
  function detail($iperiode, $iarea, $iproduct)
  {
    if ($iarea == '00') {
      $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and daerah='f' and product='$iproduct' and a.product=b.i_product", false);
    } else {
      $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and area='$iarea' and product='$iproduct'  and a.product=b.i_product", false);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
