<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($cari,$allarea,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto,$num,$offset)
    {
      if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
		    $this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
							              b.e_area_name, a.i_salesman, c.e_salesman_name
							              from tm_spb a, tr_area b, tr_salesman c
							              where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							              and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f' 
							              and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
							              and a.i_salesman=c.i_salesman
							              group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
							              order by a.i_area, a.i_salesman, c.e_salesman_name",false);#->limit($num,$offset);
      }else{
		    $this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
							              b.e_area_name, a.i_salesman, c.e_salesman_name
							              from tm_spb a, tr_area b, tr_salesman c
							              where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							              and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f' 
							              and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                                   and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
							              and a.i_salesman=c.i_salesman
							              group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
							              order by a.i_area, a.i_salesman, c.e_salesman_name",false);#->limit($num,$offset);
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
	      return $query->result();
      }
    }
    function cariperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
							b.e_area_name, a.i_salesman, c.e_salesman_name
							from tm_spb a, tr_area b, tr_salesman c
							where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
							and(upper(a.i_salesman) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%'
							or upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
							and a.i_area=b.i_area
							and a.i_salesman=c.i_salesman
							group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
							order by a.i_area, a.i_salesman",false);#->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperioderinci($isalesman,$esalesmanname,$dfrom,$dto,$num,$offset)
    {
		  $this->db->select(" d.i_product, d.e_product_name, sum(d.n_order) as n_order
					                from tm_spb a, tr_salesman c, tm_spb_item d
					                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
					                and a.f_spb_cancel='f' and a.i_salesman='$isalesman' and a.i_salesman=c.i_salesman
                          and a.i_spb=d.i_spb and a.i_area=d.i_area
					                group by d.i_product, d.e_product_name
					                order by d.i_product, d.e_product_name",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
	      return $query->result();
      }
    }

}
?>
