<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }

    function baca($ispb,$iarea)
    {
      $this->db->select(" a.e_remark1 AS emark1, a.*, e.e_price_groupname,
                       d.e_area_name, b.e_customer_name, b.e_customer_address, c.e_salesman_name, b.f_customer_first
                       from tm_spb a
                           inner join tr_customer b on (a.i_customer=b.i_customer)
                           inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                           inner join tr_customer_area d on (a.i_customer=d.i_customer)
                           left join tr_price_group e on (a.i_price_group=e.i_price_group)
                           where a.i_spb ='$ispb' and a.i_area='$iarea'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function bacadetail($ispb,$iarea,$ipricegroup)
    {
      $this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
                                    a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, b.e_product_motifname,
                                    c.v_product_retail as hrgnew, a.i_product_status
                        from tm_spb_item a, tr_product_motif b, tr_product_price c
                            where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                        and a.i_product=c.i_product and c.i_price_group='$ipricegroup' and a.i_product_grade=c.i_product_grade
                            order by a.n_item_no", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    /*
    function bacadetailnilaispb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a, tr_product_motif b, tr_product_price c
                 where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product=c.i_product and c.i_price_group='$ipricegroup' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a, tr_product_motif b, tr_product_price c
                 where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product=c.i_product and c.i_price_group='$ipricegroup' ", false);
    }
    */
    function bacadetailnilaispb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a
                 where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
                 where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function insertheader($ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman,
          $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
          $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
          $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
          $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
          $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold,$eremarkx)
    {
      $query   = $this->db->query("SELECT current_timestamp as c");
      $row     = $query->row();
      $dentry  = $row->c;
      $this->db->set(
         array(
         'i_spb'     => $ispb,
         'd_spb'     => $dspb,
         'i_customer'   => $icustomer,
         'i_area' => $iarea,
         'i_spb_po'  => $ispbpo,
         'n_spb_toplength'=> $nspbtoplength,
         'i_salesman'   => $isalesman,
         'i_price_group' => $ipricegroup,
         'd_spb_receive'   => $dspb,
         'f_spb_op'  => $fspbop,
         'e_customer_pkpnpwp' => $ecustomerpkpnpwp,
         'f_spb_pkp'    => $fspbpkp,
         'f_spb_plusppn'      => $fspbplusppn,
         'f_spb_plusdiscount' => $fspbplusdiscount,
         'f_spb_stockdaerah'  => $fspbstockdaerah,
         'f_spb_program'   => $fspbprogram,
         'f_spb_valid'     => $fspbvalid,
         'f_spb_siapnotagudang'  => $fspbsiapnotagudang,
         'f_spb_cancel'       => $fspbcancel,
         'n_spb_discount1'    => $nspbdiscount1,
         'n_spb_discount2'    => $nspbdiscount2,
         'n_spb_discount3'    => $nspbdiscount3,
         'v_spb_discount1'    => $vspbdiscount1,
         'v_spb_discount2'    => $vspbdiscount2,
         'v_spb_discount3'    => $vspbdiscount3,
         'v_spb_discounttotal'   => $vspbdiscounttotal,
         'v_spb'     => $vspb,
         'f_spb_consigment'   => $fspbconsigment,
         'd_spb_entry'     => $dentry,
         'i_spb_old'    => $ispbold,
         'i_product_group' => '02',
         'e_remark1'    => $eremarkx
         )
      );

      $this->db->insert('tm_spb');
    }
    function insertheadersjc($isjc, $dsjc, $ispb, $dspb, $icustomer, $iarea, $isalesman,
                      $fsjccancel, $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3,
                      $vsjcdiscount1, $vsjcdiscount2, $vsjcdiscount3, $vsjcgross,
                      $vsjcdiscounttotal, $vsjcnetto, $isjctype)
    {
      $query   = $this->db->query("SELECT current_timestamp as c");
      $row     = $query->row();
      $dentry  = $row->c;
      $this->db->set(
         array(
         'i_sjc'              => $isjc,
         'i_sjc_type'         => $isjctype,
         'd_sjc'              => $dsjc,
         'i_spb'              => $ispb,
         'd_spb'              => $dspb,
         'i_customer'         => $icustomer,
         'i_area'          => $iarea,
         'i_salesman'         => $isalesman,
         'f_sjc_cancel'          => $fsjccancel,
         'n_sjc_discount1'       => $nsjcdiscount1,
         'n_sjc_discount2'       => $nsjcdiscount2,
         'n_sjc_discount3'       => $nsjcdiscount3,
         'v_sjc_discount1'       => $vsjcdiscount1,
         'v_sjc_discount2'       => $vsjcdiscount2,
         'v_sjc_discount3'       => $vsjcdiscount3,
         'v_sjc_gross'        => $vsjcgross,
         'v_sjc_discounttotal'   => $vsjcdiscounttotal,
         'v_sjc_netto'        => $vsjcnetto,
         'd_sjc_entry'        => $dentry
         )
      );

      $this->db->insert('tm_sjc');
    }
    function insertdetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,$iproductmotif,$eremark,$i)
    {
      if($eremark=='') $eremark=null;
      $this->db->set(
         array(
               'i_spb'              => $ispb,
               'i_area'          => $iarea,
               'i_product'       => $iproduct,
          'i_product_status'=> $iproductstatus,
               'i_product_grade' => $iproductgrade,
               'i_product_motif' => $iproductmotif,
               'n_order'         => $norder,
               'n_deliver'       => $ndeliver,
               'v_unit_price'    => $vunitprice,
               'e_product_name'  => $eproductname,
               'e_remark'         => $eremark,
          'n_item_no'       => $i
         )
      );
      $this->db->insert('tm_spb_item');
    }
    function insertdetailsjc($isjc,$iarea,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nquantity,$vunitprice,$isjctype)
    {
      $this->db->set(
         array(
               'i_sjc'           => $isjc,
               'i_area'       => $iarea,
               'i_product'       => $iproduct,
               'i_product_grade' => $iproductgrade,
               'i_product_motif' => $iproductmotif,
               'e_product_name'  => $eproductname,
               'n_quantity'      => $nquantity,
               'v_unit_price'    => $vunitprice,
               'i_sjc_type'      => $isjctype
         )
      );
      $this->db->insert('tm_sjc_item');
    }
    function updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman,
                   $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                   $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                   $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
                   $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                   $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold,$eremark1)
    {
   $query      = $this->db->query("SELECT current_timestamp as c");
   $row        = $query->row();
   $dspbupdate = $row->c;
      $data = array( 'd_spb'     => $dspb,
         'i_customer'   => $icustomer,
         'i_spb_po'  => $ispbpo,
         'n_spb_toplength' => $nspbtoplength,
         'i_salesman'      => $isalesman,
         'i_price_group'   => $ipricegroup,
         'd_spb_receive'      => $dspb,
         'f_spb_op'     => $fspbop,
         'e_customer_pkpnpwp' => $ecustomerpkpnpwp,
         'f_spb_pkp'    => $fspbpkp,
         'f_spb_plusppn'      => $fspbplusppn,
         'f_spb_plusdiscount' => $fspbplusdiscount,
         'f_spb_stockdaerah'  => $fspbstockdaerah,
         'f_spb_program'   => $fspbprogram,
         'f_spb_valid'     => $fspbvalid,
         'f_spb_siapnotagudang'  => $fspbsiapnotagudang,
         'f_spb_cancel'       => $fspbcancel,
         'n_spb_discount1'    => $nspbdiscount1,
         'n_spb_discount2'    => $nspbdiscount2,
         'n_spb_discount3'    => $nspbdiscount3,
         'v_spb_discount1'    => $vspbdiscount1,
         'v_spb_discount2'    => $vspbdiscount2,
         'v_spb_discount3'    => $vspbdiscount3,
         'v_spb_discounttotal'   => $vspbdiscounttotal,
         'v_spb'     => $vspb,
         'f_spb_consigment'   => $fspbconsigment,
         'd_spb_update'    => $dspbupdate,
         'i_product_group' => '02',
         'i_spb_old'    => $ispbold,
         'e_remark1'    => $eremark1
            );
   $this->db->where('i_spb', $ispb);
   $this->db->where('i_area', $iarea);
   $this->db->update('tm_spb', $data);
    }
    function updateheadersjc($ispb, $iarea, $dsjc, $icustomer, $isalesman, $fsjccancel,
                      $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3, $vsjcdiscount1,
                      $vsjcdiscount2, $vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross,
                      $vsjcnetto, $isjc, $isjctype)
    {
      $query      = $this->db->query("SELECT current_timestamp as c");
      $row        = $query->row();
      $dsjcupdate = $row->c;
      $data = array(
         'd_sjc'              => $dsjc,
         'i_customer'         => $icustomer,
         'i_salesman'         => $isalesman,
         'f_sjc_cancel'          => $fsjccancel,
         'n_sjc_discount1'       => $nsjcdiscount1,
         'n_sjc_discount2'       => $nsjcdiscount2,
         'n_sjc_discount3'       => $nsjcdiscount3,
         'v_sjc_discount1'       => $vsjcdiscount1,
         'v_sjc_discount2'       => $vsjcdiscount2,
         'v_sjc_discount3'       => $vsjcdiscount3,
         'v_sjc_gross'        => $vsjcgross,
         'v_sjc_discounttotal'   => $vsjcdiscounttotal,
         'v_sjc_netto'        => $vsjcnetto,
         'd_sjc_update'       => $dsjcupdate
            );
      $this->db->where('i_spb', $ispb);
      $this->db->where('i_area', $iarea);
      $this->db->where('i_sjc', $isjc);
      $this->db->where('i_sjc_type', $isjctype);
      $this->db->update('tm_sjc', $data);
    }

   function uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2,$vspbdiscount3, $vspbdiscounttotal, $vspb)
    {
      $data = array(
         'v_spb_discount1'       => $vspbdiscount1,
         'v_spb_discount2'       => $vspbdiscount2,
         'v_spb_discount3'       => $vspbdiscount3,
         'v_spb_discounttotal'   => $vspbdiscounttotal,
         'v_spb'           => $vspb
            );
      $this->db->where('i_spb', $ispb);
      $this->db->where('i_area', $iarea);
      $this->db->update('tm_spb', $data);
    }

   function upheadsjc($ispb, $iarea, $isjc, $isjctype, $vsjcdiscount1, $vsjcdiscount2,$vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross, $vsjcnetto)
    {
      $data = array(
         'v_sjc_discount1'       => $vsjcdiscount1,
         'v_sjc_discount2'       => $vsjcdiscount2,
         'v_sjc_discount3'       => $vsjcdiscount3,
         'v_sjc_discounttotal'   => $vsjcdiscounttotal,
         'v_sjc_gross'        => $vsjcgross,
         'v_sjc_netto'        => $vsjcnetto,
            );
      $this->db->where('i_spb', $ispb);
      $this->db->where('i_area', $iarea);
      $this->db->where('i_sjc', $isjc);
      $this->db->where('i_sjc_type', $isjctype);
      $this->db->update('tm_sjc', $data);
    }

    public function deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif)
    {
      $this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
                           and i_product='$iproduct' and i_product_grade='$iproductgrade'
                           and i_product_motif='$iproductmotif'");
      return TRUE;
    }

    public function deletedetailsjc($isjc, $iarea, $isjctype, $iproduct, $iproductgrade, $iproductmotif)
    {
      $this->db->query("DELETE FROM tm_sjc_item WHERE i_sjc='$isjc' and i_area='$iarea' and i_sjc_type='$isjctype'
                           and i_product='$iproduct' and i_product_grade='$iproductgrade'
                           and i_product_motif='$iproductmotif'");
      return TRUE;
    }

    public function delete($ispb, $iarea)
    {
      $this->db->query("DELETE FROM tm_spb WHERE i_spb='$ispb' and i_area='$iarea'");
      $this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'");
      return TRUE;
    }
    function bacasemua($iarea)
    {
      $this->db->select("* from tm_spb where i_area='$iarea' order by i_spb desc",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaproduct($cari,$num,$offset,$kdharga)
    {
      if($offset=='')
         $offset=0;
      $query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
                          a.e_product_motifname as namamotif, c.i_product_status, e.e_product_statusname,
                          c.e_product_name as nama,b.v_product_retail as harga
                          from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d, tr_product_status e
                         where
                         d.i_product_type=c.i_product_type and d.i_product_group='02'
                         and b.i_product=a.i_product and a.i_product_motif='00'
                         and a.i_product=c.i_product and c.i_product_status=e.i_product_status
                         and b.i_price_group='$kdharga'
                         and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') limit $num offset $offset",false);

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacastore($num,$offset,$iarea)
    {
      $this->db->select("i_store, e_store_name from tr_store
                     where i_store in(select i_store from tr_area
                     where i_area='$iarea' or i_area='00')
                     order by i_store",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacacustomer($cari,$iarea,$num,$offset,$per)
    {
      $this->db->select(" * from tr_customer a
               left join tr_customer_pkp b on
               (a.i_customer=b.i_customer)
               left join tr_price_group c on
               (a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
               left join tr_customer_area d on
               (a.i_customer=d.i_customer)
               left join tr_customer_salesman e on
               (a.i_customer=e.i_customer and e.i_product_group='02' and e.e_periode='$per')
               left join tr_customer_discount f on
               (a.i_customer=f.i_customer) where a.i_area='$iarea'
          and a.f_approve='t' and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
               order by a.i_customer",false)->limit($num,$offset);
#          and a.f_approve='t' and a.f_approve2='t' and a.f_approve3='t' and a.f_approve4='t'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function runningnumber($iarea,$thbl){
      $th   = substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
        $this->db->select(" n_modul_no as max from tm_dgu_no
                          where i_modul='SPB'
                          and substr(e_periode,1,4)='$th'
                          and i_area='$iarea' for update", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           foreach($query->result() as $row){
             $terakhir=$row->max;
           }
           $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no
                            set n_modul_no=$nospb
                            where i_modul='SPB'
                            and substr(e_periode,1,4)='$th'
                            and i_area='$iarea'", false);
           settype($nospb,"string");
           $a=strlen($nospb);
           while($a<6){
             $nospb="0".$nospb;
             $a=strlen($nospb);
           }
           $nospb  ="SPB-".$thbl."-".$nospb;
           return $nospb;
        }else{
           $nospb  ="000001";
           $nospb  ="SPB-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                           values ('SPB','$iarea','$asal',1)");
           return $nospb;
        }
/*
        $th    = substr($thbl,0,2);
#       $this->db->select(" max(substr(i_spb,10,6)) as max from tm_spb
#                             where substr(i_spb,5,2)='$th' and i_area='$iarea'", false);
        $this->db->select(" count(i_spb) as max from tm_spb
                                where substr(i_spb,5,2)='$th' and i_area='$iarea'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           foreach($query->result() as $row){
             $terakhir=$row->max;
           }
           $nospb  =$terakhir+1;
           settype($nospb,"string");
           $a=strlen($nospb);
           while($a<6){
             $nospb="0".$nospb;
             $a=strlen($nospb);
           }
           $nospb  ="SPB-".$thbl."-".$nospb;
           return $nospb;
        }else{
           $nospb  ="000001";
           $nospb  ="SPB-".$thbl."-".$nospb;
           return $nospb;
        }
*/
    }
   function runningnumbersjc($iarea){
    $query  = $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
      $row     = $query->row();
      $thbl = $row->c;
      $th      = substr($thbl,0,2);
      $this->db->select(" max(substr(i_sjc,10,6)) as max from tm_sjc
                     where substr(i_sjc,5,2)='$th' and i_area='$iarea'
                     and substr(i_sjc,1,3)='SJC'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
           $terakhir=$row->max;
         }
         $nosjc  =$terakhir+1;
         settype($nosjc,"string");
         $a=strlen($nosjc);
         while($a<6){
           $nosjc="0".$nosjc;
           $a=strlen($nosjc);
         }
         $nosjc  ="SJC-".$thbl."-".$nosjc;
         return $nosjc;
      }else{
         $nosjc  ="000001";
         $nosjc  ="SJC-".$thbl."-".$nosjc;
         return $nosjc;
      }
    }
    function cari($iarea,$cari,$num,$offset)
    {
      $this->db->select(" * from tm_spb where upper(i_spb) like '%$cari%' and i_area='$iarea' order by i_spb",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function caristore($cari,$num,$offset)
    {
      $this->db->select(" * from tr_store where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'
                     order by i_store",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
      $this->db->select(" * from tr_customer a
               left join tr_customer_pkp b on
               (a.i_customer=b.i_customer)
               left join tr_price_group c on
               (a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
               left join tr_customer_area d on
               (a.i_customer=d.i_customer)
               left join tr_customer_salesman e on
               (a.i_customer=e.i_customer and e.i_product_group='02')
               left join tr_customer_discount f on
               (a.i_customer=f.i_customer) where a.i_area='$iarea'
          and a.f_approve='t' and
                     (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                     order by a.i_customer",false)->limit($num,$offset);
#                    (a.i_customer=f.i_customer) where a.i_area='$iarea'  and a.f_approve='t' and a.f_approve2='t' and a.f_approve3='t' and a.f_approve4='t' and
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cariproduct($cari,$kdharga,$num,$offset)
    {
      if($offset=='')
         $offset=0;
         $str  = "  select a.i_product as kode, a.i_product_motif as motif,
                     a.e_product_motifname as namamotif, c.i_product_status, e.e_product_statusname,
                     c.e_product_name as nama,b.v_product_retail as harga
                     from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d, tr_product_status e
                     where b.i_product=a.i_product and a.i_product_motif='00'
                     and d.i_product_type=c.i_product_type and d.i_product_group='02'
                     and a.i_product=c.i_product and c.i_product_status=e.i_product_status
                     and b.i_price_group='$kdharga'
                     and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
                     limit $num offset $offset";
      $query=$this->db->query($str,false);

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function bacaarea($num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5)
    {
      /* Disabled 13042011*/

      /*
      $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
      */

      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
         $this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
      }else{
         $this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5)
    {
      /* Disabled 13042011 */
      /*
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
               and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
               or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);

      */

      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
         $this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
      }else{
         $this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function productgroup()
    {
      $this->db->select("* from tr_product_group", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
  function bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
      $query = $this->db->select("distinct i_salesman, e_salesman_name from tr_customer_salesman
                                  where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
                                  and i_area='$iarea' and e_periode='$per' order by i_salesman",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
  function updatesjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
                                 $nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1,
                                   $vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto,$isjold)
    {
      $query      = $this->db->query("SELECT current_timestamp as c");
      $row        = $query->row();
      $dsjupdate  = $row->c;
      $this->db->set(
         array(
         'i_sj_old'        => $isjold,
         'i_spb'              => $ispb,
         'd_spb'              => $dspb,
         'd_sj'               => $dsj,
         'i_area'          => $iarea,
         'i_salesman'      => $isalesman,
         'i_customer'      => $icustomer,
         'n_nota_discount1'=> $nspbdiscount1,
         'n_nota_discount2'=> $nspbdiscount2,
         'n_nota_discount3'=> $nspbdiscount3,
         'v_nota_discount1'=> $vspbdiscount1,
         'v_nota_discount2'=> $vspbdiscount2,
         'v_nota_discount3'=> $vspbdiscount3,
         'v_nota_discounttotal'  => $vspbdiscounttotal,
         'v_nota_gross'    => $vspbgross,
         'v_nota_netto'    => $vspbnetto,
         'd_nota_update'      => $dsjupdate,
         'f_nota_cancel'      => 'f'
         )
      );

      $this->db->where('i_sj',$isj);
      $this->db->where('i_area',$iarea);
      $this->db->update('tm_nota');
    }
    function deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea)
   {
      $this->db->query("delete from tm_nota_item where i_sj='$isj' and i_area='$iarea'
                        and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'",false);
   }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri      = $this->db->query("SELECT i_trans FROM tm_ic_trans
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
        $row         = $queri->row();
      $query=$this->db->query("
                                    DELETE FROM tm_ic_trans
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if($row->i_trans!=''){
        return $row->i_trans;
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query("
                                UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query("
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
            return $query->result();
         }
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query   = $this->db->query("SELECT current_timestamp as c");
       $row    = $query->row();
       $now   = $row->c;
      $query=$this->db->query("
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location,
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction,
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin',
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
            $ada=true;
         }
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query("
                                UPDATE tm_mutasi
                                set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query("
                                insert into tm_mutasi
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,$qsj,0,0,$qaw-$qsj,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
            $ada=true;
         }
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query("
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query("
                                insert into tm_ic
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
}
?>
