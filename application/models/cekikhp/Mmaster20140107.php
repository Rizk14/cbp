<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1 = $this->session->userdata('i_area');
		$area2 = $this->session->userdata('i_area2');
		$area3 = $this->session->userdata('i_area3');
		$area4 = $this->session->userdata('i_area4');
		$area5 = $this->session->userdata('i_area5');
		$allarea = $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
					                where a.i_customer=b.i_customer 
					                and a.f_ttb_tolak='f'
					                and not a.i_nota isnull
					                and (upper(a.i_nota) like '%$cari%' 
					                or upper(a.i_spb) like '%$cari%' 
					                or upper(a.i_customer) like '%$cari%' 
					                or upper(b.e_customer_name) like '%$cari%')
					                order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
						              where a.i_customer=b.i_customer 
						              and a.f_ttb_tolak='f'
						              and not a.i_nota isnull
						              and (upper(a.i_nota) like '%$cari%' 
						                or upper(a.i_spb) like '%$cari%' 
						                or upper(a.i_customer) like '%$cari%' 
						                or upper(b.e_customer_name) like '%$cari%')
						              and (a.i_area='$area1' 
						              or a.i_area='$area2' 
						              or a.i_area='$area3' 
						              or a.i_area='$area4' 
						              or a.i_area='$area5')
						              order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						             or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
	    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
					               order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	* from(
														select a.d_bukti, a.i_bukti, b.e_ikhp_typename, a.i_coa, a.v_terima_tunai, a.v_terima_giro, 
															a.v_keluar_tunai, a.v_keluar_giro, c.e_area_name
														from tm_ikhp a, tr_ikhp_type b, tr_area c
														where
														a.i_ikhp_type=b.i_ikhp_type and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
														d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														d_bukti <= to_date('$dto','dd-mm-yyyy')
														union all
														select 	a.d_bukti, a.i_pelunasan as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
															a.v_jumlah as v_terima_tunai, 0 as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name
														from tm_pelunasan a, tr_area c
														where 
														a.i_jenis_bayar='02' and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
                            a.f_pelunasan_cancel='f' and 
														a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														a.d_bukti <= to_date('$dto','dd-mm-yyyy')
														union all
														select 	a.d_bukti, a.i_pelunasan as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
															0 as v_terima_tunai, a.v_jumlah as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name 
														from tm_pelunasan a, tr_area c
														where 
														a.i_jenis_bayar='01' and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
                            a.f_pelunasan_cancel='f' and 
														a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														a.d_bukti <= to_date('$dto','dd-mm-yyyy')
												) as x
												order by x.d_bukti, substr(x.i_bukti,1,7)",false)->limit($num,$offset);
#a.f_giro_tolak='f' and a.f_giro_batal='f' and
#a.f_giro_tolak='f' and a.f_giro_batal='f' and
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    // desta 28-12-2010
    function bacaperiodetanpalimit($iarea,$dfrom,$dto)
    {
	$this->db->select("	* from( select a.d_bukti, a.i_bukti, b.e_ikhp_typename, a.i_coa, a.v_terima_tunai, a.v_terima_giro, 
					a.v_keluar_tunai, a.v_keluar_giro, c.e_area_name, to_char(a.d_cek_ikhp,'yyyy-mm-dd') as d_cek_ikhp
					from tm_ikhp a, tr_ikhp_type b, tr_area c
					where
					a.i_ikhp_type=b.i_ikhp_type and
					a.i_area='$iarea' and
					a.i_area=c.i_area and
					d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
					d_bukti <= to_date('$dto','dd-mm-yyyy')
					union all
					
					select 	a.d_bukti, a.i_pelunasan as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
					a.v_jumlah as v_terima_tunai, 0 as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name, to_char(a.d_cek_ikhp,'yyyy-mm-dd') as d_cek_ikhp
					from tm_pelunasan a, tr_area c
					where 
					a.i_jenis_bayar='02' and
					a.i_area='$iarea' and
					a.i_area=c.i_area and
		      a.f_pelunasan_cancel='f' and 
					a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
					a.d_bukti <= to_date('$dto','dd-mm-yyyy')
					
					union all
					select 	b.d_dt as d_bukti, a.i_dt as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
						0 as v_terima_tunai, a.v_jumlah as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name, to_char(a.d_cek_ikhp,'yyyy-mm-dd') as d_cek_ikhp
					from tm_giro a, tr_area c, tm_dt b
					where
					a.i_area='$iarea' and a.i_dt=b.i_dt and a.i_area=b.i_area AND
					a.i_area=c.i_area and
					a.f_giro_tolak='f' and a.f_giro_batal='f' and a.f_giro_batal_input='f' and
					a.d_giro_terima >= to_date('$dfrom','dd-mm-yyyy') AND
					a.d_giro_terima <= to_date('$dto','dd-mm-yyyy')) as x

				order by x.d_bukti, substr(x.i_bukti,1,7)",false);
#a.f_giro_tolak='f' and a.f_giro_batal='f' and
#a.f_giro_tolak='f' and a.f_giro_batal='f' and
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacasaldo($iarea,$dfrom,$dto)
    {
	$ada	= false;
      	$i	= 0;
	while(!$ada)
	{
        	$i++;
		$tmp	= explode("-", $dfrom);
		$tahun	= $tmp[2];
		$bulan	= $tmp[1];
		$tanggal= $tmp[0];
		$dsaldo	= $tahun."/".$bulan."/".$tanggal;
		$dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
		$tmp	= explode("-", $dtos);
		$th	= $tmp[0];
		$bl	= $tmp[1];
		$dt	= $tmp[2];
		$dfrom	= $dt."-".$bl."-".$th;

		$this->db->select("	* from tm_ikhp_saldo where i_area='$iarea' and d_bukti = to_date('$dfrom','dd-mm-yyyy')",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$ada	= true;
			return $query->result();
		}

		if($i>365){
		  $ada=true;
		}
	}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		     }      
		    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
		    $dateTimeArr=getdate($dateTime);
		    $nosecmin = 0;
		    $min=$dateTimeArr['minutes'];
		    $sec=$dateTimeArr['seconds'];
		    if ($hr==0){$nosecmin += 1;}
		    if ($min==0){$nosecmin += 1;}
		    if ($sec==0){$nosecmin += 1;}
		    if ($nosecmin>2){     
				return(date("Y-m-d",$dateTime));
			} else {     
				return(date("Y-m-d G:i:s",$dateTime));
			}
	}
	
	function updateikhp($iarea,$dfrom,$dto,$user) {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mm:ss') as c");
		$row   	= $query->row();
		$dentry	= $row->c;

		$this->db->query(" UPDATE tm_ikhp SET i_cek_ikhp='$user', d_cek_ikhp='$dentry' WHERE  
				i_area='$iarea' AND
				d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
				d_bukti <= to_date('$dto','dd-mm-yyyy') ");

		$this->db->query(" UPDATE tm_pelunasan SET i_cek_ikhp='$user', d_cek_ikhp='$dentry' WHERE  
				i_area='$iarea' AND
				d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
				d_bukti <= to_date('$dto','dd-mm-yyyy') ");

		$this->db->query(" UPDATE tm_giro SET i_cek_ikhp='$user', d_cek_ikhp='$dentry' WHERE  
				i_area='$iarea' AND
				d_giro_terima >= to_date('$dfrom','dd-mm-yyyy') AND
				d_giro_terima <= to_date('$dto','dd-mm-yyyy') ");
	}
}
?>
