<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
//		$this->db->query('delete from tm_notakoreksi where i_nota=\''.$inota.'\' and i_area=\''.$iarea.'\'');
//		$this->db->query('delete from tm_notakoreksi_item where i_nota=\''.$inota.'\' and i_area=\''.$iarea.'\'');
		$this->db->query("update tm_notakoreksi set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
		$this->db->query("update tm_nota set f_nota_koreksi='f' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and not a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and not a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer and a.f_nota_koreksi='t'
							and not a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_nota) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
