<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
//		$this->db->query('delete from tm_notakoreksi where i_nota=\''.$inota.'\' and i_area=\''.$iarea.'\'');
//		$this->db->query('delete from tm_notakoreksi_item where i_nota=\''.$inota.'\' and i_area=\''.$iarea.'\'');
		$this->db->query("update tm_notakoreksi set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
		$this->db->query("update tm_nota set f_nota_koreksi='f' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and not a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and not a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
  {
/*
	  $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
						            where a.i_customer=b.i_customer and a.f_nota_koreksi='t'
						            and not a.i_nota isnull
						            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						            or upper(a.i_nota) like '%$cari%')
						            and a.i_area='$iarea' and
						            a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
						            a.d_nota <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_nota ",false)->limit($num,$offset);
*/
	  $this->db->select("	a.i_area, a.i_nota, a.d_nota as d_notay, b.d_nota as d_notax, a.i_salesman as i_salesmany, 
	                      b.i_salesman as i_salesmanx, a.i_customer as i_customery, b.i_customer as i_customerx, 
	                      a.v_nota_netto as v_nota_nettoy, b.v_nota_netto as v_nota_nettox, c.i_product as i_product, c.e_product_name,
	                      c.n_deliver as n_delivery, d.n_deliver as n_deliverx, a.e_alasan
	                      from tm_notakoreksi b, tm_nota a
	                      left join tm_nota_item c on(a.i_sj=c.i_sj and a.i_area=c.i_area )
	                      left join tm_notakoreksi_item d on (c.i_sj=d.i_sj and c.i_area=d.i_area and c.i_product=d.i_product
	                                                          and d.n_koreksi=a.n_koreksi)
						            where a.i_sj=b.i_sj and a.i_area=b.i_area
						            and a.f_nota_koreksi='t' and not a.i_nota isnull and b.n_koreksi=a.n_koreksi
						            and (upper(a.i_customer) like '%$cari%' or upper(a.i_nota) like '%$cari%')
						            and a.i_area='$iarea' and a.d_koreksi >= to_date('$dfrom','dd-mm-yyyy') 
						            AND a.d_koreksi <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_nota, c.i_product ",false)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
}
?>
