<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
  {
    parent::__construct();
	#$this->CI =& get_instance();
  }
  function baca()
  {
    $this->db->select("	i_product, i_product_grade, e_product_name, n_quantity_stock from tm_ic where i_store='AA' and i_store_location='01' 
                      order by i_product",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
 
  }
  
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store and b.i_store != 'AA'
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}else{
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
  	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
  	  $this->db->select("distinct on (a.i_store) a.i_store, c.i_store_location, a.i_area, b.e_store_name, c.e_store_locationname
                         from tr_area a, tr_store b, tr_store_location c
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                         and a.i_store=b.i_store and b.i_store=c.i_store
            						 order by a.i_store ", FALSE)->limit($num,$offset);
  	}else{
  	  $this->db->select("distinct on (a.i_store) a.i_store, c.i_store_location, a.i_area, b.e_store_name, c.e_store_locationname
                         from tr_area a, tr_store b, tr_store_location c
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
  						           and a.i_store=b.i_store and b.i_store=c.i_store and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
  						           or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
  	}
  	$query = $this->db->get();
  	if ($query->num_rows() > 0){
  	  return $query->result();
  	}
  }
  function bacaso($num,$offset,$iarea,$peri)
  {
	$this->db->select(" a.i_stockopname, a.i_store, a.i_store_location, c.e_store_name, b.e_store_locationname
                      from tm_stockopname a, tr_store_location b, tr_store c
                      where a.f_stockopname_cancel='f' and a.i_area = '$iarea' and to_char(a.d_stockopname,'yyyymm')='$peri' 
                      and a.i_store=c.i_store and a.i_store=b.i_store and a.i_store_location=b.i_store_location
                      order by a.i_stockopname desc", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
  }

  function updateohpusat()
  {
#  $query=$this->db->query("UPDATE tm_ic set n_quantity_stock=0 where i_store='AA'",false);
  $this->db->select(" * from f_stock_onhandpusat()", false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
      foreach($query->result() as $row){
      $iproduct       = $row->i_product;
      $iproductgrade  = $row->i_product_grade;
      $iproductmotif  = $row->i_product_motif;
	    $eproductname   = $row->e_product_name;
      $istore         = $row->i_store;
      $istorelocation = $row->i_store_location;
	    $istorelocationbin = $row->i_store_locationbin;
	    $qty              = $row->n_quantity_stock;

        $this->db->select(" * from tm_ic where i_product='$iproduct' and i_store='AA' and i_store_location='01' and i_product_motif='$iproductmotif'
                            and i_product_grade='$iproductgrade' and i_store_locationbin='00'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
/*	    echo "UPDATE tm_ic set n_quantity_stock=$qty, e_product_name='$eproductname'
            where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'".'<br>';*/
        $query=$this->db->query(" UPDATE tm_ic set n_quantity_stock=$qty, e_product_name='$eproductname'
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
        }else{
        $query=$this->db->query("INSERT INTO tm_ic (i_product, i_product_motif, i_product_grade, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, n_quantity_stock, f_product_active)
                                VALUES 
                                ('$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation',
                                  '$istorelocationbin', '$eproductname', $qty, 't')",false);
#        echo $iproduct.'  '.$iproductgrade.'  '.$iproductmotif.'<br>';
              }
#		return $query->result();
		}
	}
  }
}
?>
