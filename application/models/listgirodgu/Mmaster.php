<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($igiro,$ipv) 
    {
			$this->db->query("update tm_giro_dgu set f_giro_batal='t' WHERE i_giro='$igiro' and i_pv='$ipv'");
    }
    function bacasemua($cari, $num,$offset)
    {
			$this->db->select(" * from tm_giro_dgu a 
								inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
								where upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' or upper(a.i_supplier) like '%$cari%'
								order by a.i_giro, a.i_pv", false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
			$this->db->select(" * from tm_giro_dgu a 
								inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
								where upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' or upper(a.i_supplier) like '%$cari%'
								order by a.i_giro, a.i_pv", false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	* from tm_giro_dgu a 
								inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
								where (upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' or upper(a.i_supplier) like '%$cari%')
								and a.i_supplier='$isupplier' and
								a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_giro <= to_date('$dto','dd-mm-yyyy')
								ORDER BY a.i_giro ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	* from tm_giro_dgu a 
								inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
								where (upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' or upper(a.i_supplier) like '%$cari%')
								and a.i_supplier='$isupplier' and
								a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_giro <= to_date('$dto','dd-mm-yyyy')
								ORDER BY a.i_giro ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacasupplier($num,$offset)
    {
			$this->db->select(" * from tr_supplier",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function carisupplier($cari,$num,$offset)
    {
			$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
								order by i_supplier",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
