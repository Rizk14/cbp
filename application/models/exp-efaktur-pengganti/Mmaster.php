<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	function bacaperiode($dfrom, $dto)
	{
		$query = $this->db->query("	SELECT a.i_area, a.i_customer, b.e_customer_name, a.i_nota, a.d_nota, a.i_faktur_komersial, a.i_seri_pajak
									FROM tm_nota a
									INNER JOIN tr_customer b ON a.i_customer = b.i_customer AND a.i_area = b.i_area
									WHERE
									a.d_pajak >= to_date('$dfrom','dd-mm-yyyy')
									AND a.d_pajak <= to_date('$dto','dd-mm-yyyy')
									AND a.f_nota_cancel = 'f'
									ORDER BY a.i_faktur_komersial ", FALSE);

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaheader_newexc()
	{
		$this->db->select("	a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm') as masa_pajak, 
							to_char(a.d_pajak,'yyyy') as tahun_pajak, to_char(now(),'dd/mm/yyyy') as tgl_pajak, 
							sum(d.v_dpp) as dpp,
							sum(d.v_ppn) as ppn, 
							(100-((a.v_nota_netto/a.v_nota_gross)*100)) as diskon, c.e_customer_pkpnpwp, 
							case when b.f_customer_pkp = 'f' and (length(b.i_nik) = 16) then b.i_nik||'#NIK#NAMA#' else '' end
							|| case when b.f_customer_pkp = 'f' then b.e_customer_name /* else c.e_customer_pkpname */ end as e_customer_name,
							b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
							from tm_nota a, tr_customer b, tr_customer_pkp c, tm_nota_item d
							where a.f_nota_cancel='false' and b.i_customer=a.i_customer and 
							a.i_customer = c.i_customer and a.i_sj=d.i_sj and a.i_area=d.i_area
							and a.i_nota in (select i_nota from tt_efaktur)
							group by a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm'), to_char(a.d_pajak,'yyyy'), 
							to_char(a.d_pajak,'dd/mm/yyyy'), v_nota_netto, v_nota_gross, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress,b.f_customer_pkp
							order by a.i_faktur_komersial", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetail_newexc($dfrom, $dto, $inota)
	{
		$this->db->select(" 	a.i_nota,
								b.i_product,
								b.e_product_name,
								b.n_deliver,
								b.v_unit_price,
								b.v_dpp,
								b.v_ppn,
								b.v_nota_discount,
								b.v_netto,
								b.n_item_no,
								n_tax / 100 + 1 excl_divider,
								n_tax / 100 n_tax_val
							FROM
								tm_nota a
							LEFT JOIN tr_tax_amount c ON (a.d_sj BETWEEN d_start AND d_finish),
								tm_nota_item b
							WHERE
								a.f_nota_cancel = 'false'
								AND (d_pajak >= '$dfrom' AND d_pajak <= '$dto')
								AND a.i_nota = b.i_nota
								AND (a.i_nota = '$inota')
							ORDER BY b.n_item_no  ", false);
		return  $this->db->get();
	}

	function bacadetail($dfrom, $dto, $inota)
	{
		$this->db->select(" 	a.i_nota,
								a.i_product,
								a.e_product_name,
								a.n_deliver,
								a.v_dpp,
								a.v_ppn,
								(a.v_unit_price / excl_divider) AS v_unit_price,
								(a.n_deliver * a.v_unit_price) / excl_divider AS sub
							FROM
								(
									SELECT
										a.i_nota,
										b.i_product,
										b.e_product_name,
										b.n_deliver,
										b.v_unit_price,
										b.v_dpp,
										b.v_ppn,
										b.n_item_no,
										n_tax / 100 + 1 excl_divider,
										n_tax / 100 n_tax_val
									FROM
										tm_nota a
									LEFT JOIN tr_tax_amount c ON (a.d_sj BETWEEN d_start AND d_finish),
										tm_nota_item b
									WHERE
										a.f_nota_cancel = 'false'
										AND (d_pajak >= '$dfrom' AND d_pajak <= '$dto')
										AND a.i_nota = b.i_nota
										AND (a.i_nota = '$inota')
								) AS a
							ORDER BY a.n_item_no  ", false);
		return  $this->db->get();
		//   if ($query->num_rows() > 0){
		// 	  return $query->result();
		//   }
	}
	function bacaheader_unity($dfrom, $dto, $nppn)
	{
		$this->db->select(" * from f_vw_sales_tax_header('$dfrom','$dto','$nppn')", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetail_unity($dfrom, $dto, $nppn)
	{
		$this->db->select(" * from f_vw_sales_tax_detail('$dfrom','$dto','$nppn')", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	#	function bacaheader($dfrom, $dto, $fpengganti)
	function bacaheader($dfrom, $dto)
	{
		/*
		  $this->db->select("to_char(a.d_pajak,'mm') as masa_pajak, to_char(a.d_pajak,'yyyy') as tahun_pajak, 
		                    to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, floor(v_nota_netto/1.1) as dpp, floor((v_nota_netto/1.1)*0.1) as ppn, 
		                    (100-((v_nota_netto/v_nota_gross)*100)) as diskon, * 
		                    from tm_nota a, tr_customer b, tr_customer_pkp c
                        where a.f_nota_cancel='false' 
                        and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and a.i_customer = c.i_customer
                        order by a.i_faktur_komersial", false);
*/

		$this->db->select("a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm') as masa_pajak, 
		                     to_char(a.d_pajak,'yyyy') as tahun_pajak, to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, 
		                     floor((sum(d.v_unit_price*d.n_deliver)/1.1)-(((sum(d.v_unit_price*d.n_deliver)/1.1)*(100-((a.v_nota_netto/a.v_nota_gross)*100)))/100)) as dpp,
		                     floor(((sum(d.v_unit_price*d.n_deliver)/1.1)-(((sum(d.v_unit_price*d.n_deliver)/1.1)*(100-((a.v_nota_netto/a.v_nota_gross)*100)))/100))*0.1) as ppn, 
		                     (100-((a.v_nota_netto/a.v_nota_gross)*100)) as diskon, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
		                     b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
                         from tm_nota a, tr_customer b, tr_customer_pkp c, tm_nota_item d
                         where a.f_nota_cancel='false' and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and 
                         a.i_customer = c.i_customer and a.i_sj=d.i_sj and a.i_area=d.i_area
                         group by a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm'), to_char(a.d_pajak,'yyyy'), 
                         to_char(a.d_pajak,'dd/mm/yyyy'), v_nota_netto, v_nota_gross, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
		                     b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
                         order by a.i_faktur_komersial", false);
		#                        and a.f_pajak_pengganti='$fpengganti' and b.i_customer=a.i_customer
		#a.i_seri_pajak is not null and 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetail_old($dfrom, $dto, $fpengganti)
	{
		$this->db->select("b.n_deliver, b.v_unit_price, b.n_deliver * b.v_unit_price as sub, a.n_nota_discount1, a.n_nota_discount2, a.n_nota_discount3, 
                        round((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)) as dis1, 
                        round ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) as v_discount_product1,
                        round(
                        ((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) --v_discount_product1
                        * (a.n_nota_discount2/100) --%diskon
                        ) as disc2,

                        round(
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))- -- v_discount_product1
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * (a.n_nota_discount2/100)) --disc2
                        ) as v_discount2,

                        round(
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100)) --v_discount_product2
                        )* (a.n_nota_discount3/100) --%diskon
                        )as disc3,

                        round(
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100))
                        )- --v_discount_product2
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100)) --v_discount_product2
                        )* (a.n_nota_discount3/100)
                        ) as v_discount_product3,

                        a.*, (b.n_deliver * b.v_unit_price )as v_subtotal, a.i_nota, a.i_seri_pajak, b.i_product, b.*
                        from tm_nota  a, tm_nota_item b
                        where a.f_nota_cancel='false' 
                        and (d_pajak >='2015-06-01' and d_pajak <='2015-06-30')
                        and a.f_pajak_pengganti='false' and a.i_nota = b.i_nota
                        and (a.i_faktur_komersial ='FK-1506-17144' or a.i_faktur_komersial ='FK-1506-17145')
                        order by b.n_item_no", false);
		#a.i_seri_pajak is not null and 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function dateAdd($interval, $number, $dateTime)
	{
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr = getdate($dateTime);
		$yr = $dateTimeArr['year'];
		$mon = $dateTimeArr['mon'];
		$day = $dateTimeArr['mday'];
		$hr = $dateTimeArr['hours'];
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		switch ($interval) {
			case "s": //seconds
				$sec += $number;
				break;
			case "n": //minutes
				$min += $number;
				break;
			case "h": //hours
				$hr += $number;
				break;
			case "d": //days
				$day += $number;
				break;
			case "ww": //Week
				$day += ($number * 7);
				break;
			case "m": //similar result "m" dateDiff Microsoft
				$mon += $number;
				break;
			case "yyyy": //similar result "yyyy" dateDiff Microsoft
				$yr += $number;
				break;
			default:
				$day += $number;
		}
		$dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
		$dateTimeArr = getdate($dateTime);
		$nosecmin = 0;
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		if ($hr == 0) {
			$nosecmin += 1;
		}
		if ($min == 0) {
			$nosecmin += 1;
		}
		if ($sec == 0) {
			$nosecmin += 1;
		}
		if ($nosecmin > 2) {
			return (date("Y-m-d", $dateTime));
		} else {
			return (date("Y-m-d G:i:s", $dateTime));
		}
	}
}
