<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($cari,$allarea,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto,$num,$offset)
    {
      if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
        $this->db->select("	sum(v_nota) as v_nota, sum(x.v_nota_discounttotal) as v_nota_discounttotal,
                            sum(v_sj) as v_sj, sum(v_sj_discounttotal) as v_sj_discounttotal,
                            i_area, e_area_name, i_salesman, e_salesman_name
                            from (
                            select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                            0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and not i_nota isnull
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            union all
                            select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                            sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and i_nota isnull
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            ) as x
                            group by x.i_area, x.e_area_name, x.i_salesman, x.e_salesman_name 
                            order by x.i_area, x.i_salesman",false)->limit($num,$offset);
      }else{
        $this->db->select("	sum(v_nota) as v_nota, sum(x.v_nota_discounttotal) as v_nota_discounttotal,
                            sum(v_sj) as v_sj, sum(v_sj_discounttotal) as v_sj_discounttotal,
                            i_area, e_area_name, i_salesman, e_salesman_name
                            from (
                            select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                            0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and not i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            union all
                            select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                            sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            ) as x
                            group by x.i_area, x.e_area_name, x.i_salesman, x.e_salesman_name 
                            order by x.i_area, x.i_salesman",false)->limit($num,$offset);
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
	      return $query->result();
      }
    }
}
?>
