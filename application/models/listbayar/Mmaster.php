<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea)
    {
      $this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $area1= $this->session->userdata('i_area');
      $area2= $this->session->userdata('i_area2');
      $area3= $this->session->userdata('i_area3');
      $area4= $this->session->userdata('i_area4');
      $area5= $this->session->userdata('i_area5');
      $allarea= $this->session->userdata('allarea');
      if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
      {
         $this->db->select("  a.*, b.e_customer_name from tm_nota a, tr_customer b
                  where a.i_customer=b.i_customer
                  and a.f_ttb_tolak='f'
            and a.f_nota_cancel='f'
                  and not a.i_nota isnull
                  and (upper(a.i_nota) like '%$cari%'
                  or upper(a.i_spb) like '%$cari%'
                  or upper(a.i_customer) like '%$cari%'
                  or upper(b.e_customer_name) like '%$cari%')
                  order by a.i_nota desc",false)->limit($num,$offset);
      }else{
         $this->db->select("  a.*, b.e_customer_name from tm_nota a, tr_customer b
                  where a.i_customer=b.i_customer
                  and a.f_ttb_tolak='f'
            and a.f_nota_cancel='f'
                  and not a.i_nota isnull
                  and (upper(a.i_nota) like '%$cari%'
                    or upper(a.i_spb) like '%$cari%'
                    or upper(a.i_customer) like '%$cari%'
                    or upper(b.e_customer_name) like '%$cari%')
                  and (a.i_area='$area1'
                  or a.i_area='$area2'
                  or a.i_area='$area3'
                  or a.i_area='$area4'
                  or a.i_area='$area5')
                  order by a.i_nota desc",false)->limit($num,$offset);
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($cari,$num,$offset)
    {
      $area1= $this->session->userdata('i_area');
      $area2= $this->session->userdata('i_area2');
      $area3= $this->session->userdata('i_area3');
      $area4= $this->session->userdata('i_area4');
      $area5= $this->session->userdata('i_area5');
      $allarea= $this->session->userdata('allarea');
      if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
      {
      $this->db->select("  a.*, b.e_customer_name from tm_nota a, tr_customer b
               where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
          and a.f_nota_cancel='f'
               and not a.i_nota isnull
               and (upper(a.i_nota) like '%$cari%'
               or upper(a.i_spb) like '%$cari%'
               or upper(a.i_customer) like '%$cari%'
               or upper(b.e_customer_name) like '%$cari%')
               order by a.i_nota desc",FALSE)->limit($num,$offset);
      }else{
      $this->db->select("  a.*, b.e_customer_name from tm_nota a, tr_customer b
               where a.i_customer=b.i_customer
               and a.f_ttb_tolak='f'
          and a.f_nota_cancel='f'
               and not a.i_nota isnull
               and (upper(a.i_nota) like '%$cari%'
                 or upper(a.i_spb) like '%$cari%'
                 or upper(a.i_customer) like '%$cari%'
                 or upper(b.e_customer_name) like '%$cari%')
               and (a.i_area='$area1'
               or a.i_area='$area2'
               or a.i_area='$area3'
               or a.i_area='$area4'
               or a.i_area='$area5')
               order by a.i_nota desc",false)->limit($num,$offset);
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.f_ttb_tolak='f'
              and a.f_nota_cancel='f'
                     and not a.i_nota isnull
                     and (upper(a.i_nota) like '%$cari%'
                       or upper(a.i_spb) like '%$cari%'
                       or upper(a.i_customer) like '%$cari%'
                       or upper(b.e_customer_name) like '%$cari%')
                     and a.i_area='$iarea' and
                     a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                     a.d_nota <= to_date('$dto','dd-mm-yyyy')
                     ORDER BY a.i_nota ",false)->limit($num,$offset);
#                    and a.f_nota_koreksi='f'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.f_ttb_tolak='f'
              and a.f_nota_cancel='f'
                     and not a.i_nota isnull
                     and (upper(a.i_nota) like '%$cari%'
                       or upper(a.i_spb) like '%$cari%'
                       or upper(a.i_customer) like '%$cari%'
                       or upper(b.e_customer_name) like '%$cari%')
                     and a.i_area='$iarea' and
                     a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                     a.d_nota <= to_date('$dto','dd-mm-yyyy')
                     ORDER BY a.i_nota ",false)->limit($num,$offset);
#             and a.f_nota_koreksi='f'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $this->db->select("  a.*, b.e_customer_name from tm_nota a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.f_ttb_tolak='f'
              and a.f_nota_cancel='f'
                     and not a.i_nota isnull
                     and (upper(a.i_nota) like '%$cari%'
                       or upper(a.i_spb) like '%$cari%'
                       or upper(a.i_customer) like '%$cari%'
                       or upper(b.e_customer_name) like '%$cari%')
                     and a.i_area='$iarea' and
                     a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                     a.d_nota <= to_date('$dto','dd-mm-yyyy')
                     ORDER BY a.i_nota ",false)->limit($num,$offset);
#              and a.f_nota_koreksi='f'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
  function bacabayar($nota)
    {
     $this->db->select("* from 
                        (
                        select a.i_nota, a.d_nota, a.v_nota_netto, c.i_pelunasan, c.d_bukti, c.i_giro, c.e_bank_name, b.v_jumlah,
                        d.e_jenis_bayarname, d.i_jenis_bayar
                        from tm_nota a
                        left join tm_pelunasan_item b on(b.i_nota=a.i_nota)
                        inner join tm_pelunasan c on(b.i_pelunasan=c.i_pelunasan and c.f_pelunasan_cancel='f'
                        and c.f_giro_tolak='f' and c.f_giro_batal='f' and b.i_area=c.i_area)
                        left join tr_jenis_bayar d on(d.i_jenis_bayar=c.i_jenis_bayar)
                        where a.i_nota='$nota'
                        union all
                        select a.i_nota, a.d_nota, a.v_nota_netto, f.i_alokasi, f.d_alokasi, f.i_kbank, g.e_bank_name, e.v_jumlah,
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_nota a
                        left join tm_alokasi_item e on(e.i_nota=a.i_nota)
                        inner join tm_alokasi f on(e.i_alokasi=f.i_alokasi and f.f_alokasi_cancel='f' and e.i_area=f.i_area 
                        and e.i_kbank=f.i_kbank)
                        inner join tr_bank g on(f.i_coa_bank=g.i_coa)
                        where a.i_nota='$nota'
                        union all
                        select a.i_nota, a.d_nota, a.v_nota_netto, f.i_alokasi, f.d_alokasi, f.i_kn, '' as e_bank_name, e.v_jumlah,
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_nota a
                        left join tm_alokasikn_item e on(e.i_nota=a.i_nota)
                        inner join tm_alokasikn f on(e.i_alokasi=f.i_alokasi and f.f_alokasi_cancel='f' and e.i_area=f.i_area 
                        and e.i_kn=f.i_kn)
                        where a.i_nota='$nota' 
                        union all
                        select a.i_nota, a.d_nota, a.v_nota_netto, f.i_alokasi, f.d_alokasi, '' as i_kn, '' as e_bank_name, e.v_jumlah,
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_nota a
                        left join tm_alokasihl_item e on(e.i_nota=a.i_nota)
                        inner join tm_alokasihl f on(e.i_alokasi=f.i_alokasi and f.f_alokasi_cancel='f' and e.i_area=f.i_area)
                        where a.i_nota='$nota' 
                        union all
                        select a.i_nota, a.d_nota, a.v_nota_netto, f.i_alokasi, f.d_alokasi, f.i_kn, '' as e_bank_name, e.v_jumlah,
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_nota a
                        left join tm_alokasiknr_item e on(e.i_nota=a.i_nota)
                        inner join tm_alokasiknr f on(e.i_alokasi=f.i_alokasi and f.f_alokasi_cancel='f' and e.i_area=f.i_area 
                        and e.i_kn=f.i_kn)
                        where a.i_nota='$nota' 
                        )as c
                        order by c.d_bukti,c.i_pelunasan", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
