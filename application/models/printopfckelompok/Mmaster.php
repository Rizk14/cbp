<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaop($num,$offset)
    {
		$this->db->select("	i_op, i_area from tm_opfc order by i_op", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariop($cari,$num,$offset)
    {
		$this->db->select("	i_op, i_area from tm_opfc where i_op like '%$cari%' order by i_op",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacamaster($opfrom,$opto)
    {
		$this->db->select("	* from tm_opfc 
							inner join tr_supplier on (tm_opfc.i_supplier=tr_supplier.i_supplier)
							inner join tr_op_status on (tm_opfc.i_op_status=tr_op_status.i_op_status)
							inner join tr_area on (tm_opfc.i_area=tr_area.i_area)
							where tm_opfc.i_op >= '$opfrom' and tm_opfc.i_op <= '$opto' order by tm_opfc.i_op",false);
							
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($iop)
    {
      $reff='';
      $this->db->select(" i_reff from tm_opfc where tm_opfc.i_op = '$iop'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $reff=$tes->i_reff;
        }
		  }
      if(substr($reff,0,3)=='SPB'){
    		$this->db->select("a.*, d.e_product_motifname from tm_opfc_item a, tm_opfc c, tr_product_motif d where a.i_op='$iop'
                           and a.i_op=c.i_op and a.i_product=d.i_product and a.i_product_motif=d.i_product_motif
                           order by a.n_item_no",false);
      }else{
    		$this->db->select("a.*, d.e_product_motifname from tm_opfc_item a, tm_opfc c, tr_product_motif d where a.i_op='$iop'
                           and a.i_op=c.i_op and a.i_product=d.i_product and a.i_product_motif=d.i_product_motif
                           order by a.n_item_no",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
