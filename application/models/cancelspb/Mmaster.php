<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb,$area) 
    {
		$this->db->query("UPDATE tm_spb set f_spb_cancel='t' WHERE i_spb='$ispb' and i_area='$area'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer and a.i_nota is null and a.i_sj is null
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer and a.i_nota is null and a.i_sj is null
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer and a.i_nota is null and a.i_sj is null
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb desc ";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer and a.i_nota is null and a.i_sj is null
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb desc ";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		if ($cari == '') {
			$this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                          a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                          a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                          a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                          b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                          from tm_spb a 
													left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													, tr_area c
		                      where 
			                      a.i_area=c.i_area and	a.i_nota is null and a.i_sj is null and 				
			                      a.i_area='$iarea' and
			                      (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
			                      a.d_spb <= to_date('$dto','dd-mm-yyyy'))
		                      order by a.i_spb ",false)->limit($num,$offset);
		}else{
			$this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                          a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                          a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                          a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                          b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                          from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													, tr_area c
		                      where 
		                      a.i_area=c.i_area and	a.i_nota is null and a.i_sj is null and 				
		                      a.i_area='$iarea' and
		                      (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
		                      a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
					                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
					                order by a.i_spb ",false)->limit($num,$offset);
		}		
	
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiodeexport($iarea,$dfrom,$dto,$cari)
    {
		  if ($cari == '') {
			  $this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                            a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                            a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                            a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                            b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                            from tm_spb a 
													  left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													  left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													  left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													  , tr_area c
		                        where 
			                        a.i_area=c.i_area and	a.i_nota is null	and a.i_sj is null and 		
			                        a.i_area='$iarea' and
			                        (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
			                        a.d_spb <= to_date('$dto','dd-mm-yyyy'))
		                        order by a.i_spb ",false);
		  }else{
			  $this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                            a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                            a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                            a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                            b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                            from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													  left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													  left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													  , tr_area c
		                        where 
		                        a.i_area=c.i_area and	a.i_nota is null and a.i_sj is null and 				
		                        a.i_area='$iarea' and
		                        (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
		                        a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
					                  (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					                  or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
					  order by a.i_spb ",false);
		  }		
	
		  $query = $this->db->get();
      return $query;
    }
}
?>
