<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from
							(select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
							where f_posting = 't' and f_nota_koreksi='f' and f_close='f'
							union all
							select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
							where f_posting = 't' and f_close='f') as nota
							order by i_nota desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" * from
							(select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
							where f_posting = 't' and f_nota_koreksi='f' and (upper(i_nota) like '%$cari%') and f_close='f'
							union all
							select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
							where f_posting = 't' and (upper(i_nota) like '%$cari%') and f_close='f') as nota
							order by i_nota desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacanota($inota){
		$this->db->select(" * from 
							(
							select * from tm_nota 
							left join tm_promo on (tm_nota.i_spb_program=tm_promo.i_promo)
							inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
							inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
							inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
							inner join tm_spb on (tm_nota.i_spb=tm_spb.i_spb and tm_nota.i_area=tm_spb.i_area)
							where tm_nota.i_nota = '$inota' and tm_nota.f_posting = 't' and tm_nota.f_nota_koreksi='f' and f_close='f'
							union all 
							select * from tm_notakoreksi
							left join tm_promo on (tm_notakoreksi.i_spb_program=tm_promo.i_promo)
							inner join tr_customer on (tm_notakoreksi.i_customer=tr_customer.i_customer)
							inner join tr_salesman on (tm_notakoreksi.i_salesman=tr_salesman.i_salesman)
							inner join tr_customer_area on (tm_notakoreksi.i_customer=tr_customer_area.i_customer)
							inner join tm_spb on (tm_notakoreksi.i_spb=tm_spb.i_spb and tm_notakoreksi.i_area=tm_spb.i_area)
							where tm_notakoreksi.i_nota = '$inota' and tm_notakoreksi.f_posting = 't' and f_close='f'
							)
							as c",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadetailnota($inota){
		$this->db->select(" * from
							(select d.*, f.e_product_motifname from tm_nota_item d, tr_product_motif f
							where d.i_nota='$inota' and d.i_product=f.i_product and d.i_product_motif=f.i_product_motif
							union all
							select e.*, f.e_product_motifname from tm_notakoreksi_item e, tr_product_motif f
							where e.i_nota='$inota' and e.i_product=f.i_product and e.i_product_motif=f.i_product_motif) as nota",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader(	$inota,$iarea,$dnota )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$inota' and i_area='$iarea' and d_refference='$dnota'");
	}
	function deletetransitemdebet($accdebet,$inota,$dnota)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$accdebet' and i_refference='$inota' and d_refference='$dnota'");
	}
	function deletetransitemkredit($acckredit,$inota,$dnota)
    {
		$this->db->query("delete from tm_jurnal_transharianitem 
						  where i_coa='$acckredit' and i_refference='$inota' and d_refference='$dnota'");
	}
	function deletegldebet($accdebet,$inota,$iarea,$dnota)
    {
		$this->db->query("delete from tm_general_ledger 
						  where i_refference='$inota' and i_coa='$accdebet' and i_area='$iarea' and d_refference='$dnota'");
	}
	function deleteglkredit($acckredit,$inota,$iarea,$dnota)
    {
		$this->db->query("delete from tm_general_ledger
						  where i_refference='$inota' and i_coa='$acckredit' and i_area='$iarea' and d_refference='$dnota'");
	}
	function updatenota($inota,$iarea)
    {
		$this->db->query("update tm_nota set f_posting='f' where i_nota='$inota' and i_area='$iarea' and f_nota_koreksi='f'");
		$this->db->query("update tm_notakoreksi set f_posting='f' where i_nota='$inota' and i_area='$iarea'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
