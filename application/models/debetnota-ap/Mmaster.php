<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$iarea)
    {
		  $this->db->select(" * from tm_giro a
							  inner join tr_area on(a.i_area=tr_area.i_area)
							  inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							  where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function insert($isupplier,$idnap,$irefference,$ddnap,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
      if($ddnap!=''){
      	$this->db->set(
      		array(
				  'i_supplier'		=> $isupplier,
				  'i_dn_ap'			=> $idnap,
				  'i_refference' 	=> $irefference,
				  'd_dn_ap' 		=> $ddnap,
				  'd_refference'	=> $drefference,
				  'd_entry' 		=> $dentry,
				  'e_remark' 		=> $eremark,
				  'f_cetak' 		=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_dn_ap_year' 	=> $nknyear,
				  'v_netto' 		=> $vnetto,
				  'v_gross' 		=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			=> $vsisa

      		)
      	);
      }else{
      	$this->db->set(
      		array(
				  'i_supplier'			=> $isupplier,
				  'i_dn_ap'				=> $idnap,
				  'i_refference' 		=> $irefference,
				  'd_dn_ap' 			=> $ddnap,
				  'd_refference'		=> $drefference,
				  'd_entry' 			=> $dentry,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_dn_ap_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			=> $vsisa

      		)
      	);
      }
    	
    	$this->db->insert('tm_dn_ap');
    }

    function update($isupplier,$idnap,$irefference,$ddnap,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dupdate	= $row->c;
    	$this->db->set(
    		array(
				'i_supplier'	=> $isupplier,
				'i_dn_ap'		=> $idnap,
				'i_refference' 	=> $irefference,
				'd_dn_ap' 		=> $ddnap,
				'd_refference'	=> $drefference,
				'd_update' 		=> $dupdate,
				'e_remark' 		=> $eremark,
				'f_cetak' 		=> $fcetak,
				'f_masalah' 	=> $fmasalah,
				'f_insentif' 	=> $finsentif,
				'n_dn_ap_year' 	=> $nknyear,
				'v_netto' 		=> $vnetto,
				'v_gross' 		=> $vgross,
				'v_discount'    => $vdiscount,
				'v_sisa'	    => $vsisa

    		)
    	);
    	$this->db->where("i_dn_ap",$idnap);
    	$this->db->where("n_dn_ap_year",$nknyear);
    	$this->db->where("i_supplier",$isupplier);
    	$this->db->update('tm_dn_ap');
    }
    public function delete($igiro,$iarea) 
    {
		  $this->db->query("DELETE FROM tm_giro WHERE i_giro='$igiro' and i_area='$iarea'");
		  return TRUE;
    }
    function bacasemua()
    {
		  $this->db->select(' * from tm_giro a 
							  inner join tr_area on(a.i_area=tr_area.i_area)
							  inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							  order by a.i_area,a.i_giro', false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacasupplier($num,$offset)
    {
		  $this->db->select(" * from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplir_name from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%')
						     order by i_supplier ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

  	function runningnumberkn($th,$isupplier)
  	{
/*
		$pot=substr($th,2,2);
		$this->db->select(" trim(to_char(count(i_kn)+1,'000')) as no from tm_kn where n_kn_year=$th and i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
			  $kn="D".$iarea.$row->no.$pot;
			}
			return $kn;
		}else{
			$kn="D".$iarea."001".$pot;
			return $kn;
		}
*/
		$pot=substr($th,2,2);
		$this->db->select(" max(substring(i_dn_ap,4,3)) as no from tm_dn_ap where i_supplier='$isupplier' and substring(i_dn_ap,1,1)='D'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
        $nono=$row->no+1;			  
			}
      settype($nono,"string");
		  $a=strlen($nono);
		  while($a<3){
		    $nono="0".$nono;
		    $a=strlen($nono);
		  }
      $idnap="D"."00".$nono.$pot;
			return $idnap;
		}else{
			$idnap="D"."00"."001".$pot;
			return $idnap;
		}
	}
	function bacadnap($idnap,$nknyear,$isupplier)
	{
 		$this->db->select(" distinct (a.i_dn_ap||a.i_supplier),
              a.*, b.e_supplier_name
							from tm_dn_ap a, tr_supplier b
							where a.i_supplier=b.i_supplier 
							  and a.i_dn_ap='$idnap'
							  and a.n_dn_ap_year=$nknyear
							  and a.i_supplier='$isupplier'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
}
?>
