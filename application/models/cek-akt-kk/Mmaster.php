<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikk) 
    {
		$this->db->query("update tm_kk set f_kk_cancel='t' WHERE i_kk='$ikk' and i_periode='$iperiode' and i_area='$iarea' and f_posting='f' and f_close='f'");
    }
    function baca($ikk,$iperiode,$iarea)
    {
		$this->db->select("	a.i_kendaraan from tm_kk a 
							inner join tr_area b on(a.i_area=b.i_area)
							where a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_kendaraan;
			}
		}else{
			$xxx='';
		}

		if(trim($xxx)==''){
			$this->db->select("	a.*, b.e_area_name, '' as e_pengguna from tm_kk a, tr_area b 
								where a.i_area=b.i_area and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}else{
			$this->db->select("	a.*, b.e_area_name
													from tm_kk a, tr_area b, tr_kendaraan c
													where a.i_area=b.i_area and a.i_kendaraan=c.i_kendaraan and a.i_periode=c.i_periode
													and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
						where (upper(a.i_kk) like '%$cari%')
						and a.i_area=b.i_area
                				and a.f_kk_cancel='f'
						order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
						where (upper(a.i_kk) like '%$cari%')
						and a.i_area=b.i_area and a.f_kk_cancel='f'
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
						order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area and a.f_kk_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area and a.f_kk_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_kk_cancel='f'
							and a.i_area='$iarea' and
							a.d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kk <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_kk_cancel='f'
							and a.i_area='$iarea' and
							a.d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kk <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
/*
    function update($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,$ecoaname,$edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,$enamatoko,$epengguna,$ecek,$user)
    {
	$query 	= $this->db->query("SELECT current_timestamp as c");
	$row   	= $query->row();
	$dupdate= $row->c;
    	$this->db->set(array(
    'i_coa' => $icoa,
    'i_kendaraan' => $ikendaraan,
    'v_kk' => $vkk,
    'd_kk' => $dkk,
    'e_coa_name' => $ecoaname,
    'e_description' => $edescription,
    'e_jam_in' => $ejamin,
    'e_jam_out' => $ejamout,
    'n_km' => $nkm,
    'e_tempat' => $etempat,
    'f_debet' => $fdebet,
    'd_bukti' => $dbukti,
    'e_nama_toko' => $enamatoko,
    'e_pengguna' => $epengguna,
		'e_cek'	=> $ecek,
		'd_cek'	=> $dupdate,
		'i_cek'	=> $user
    	 ));
	$this->db->where('i_area',$iarea);
	$this->db->where('i_kk',$ikk);
	$this->db->where('i_periode',$iperiode);
  $this->db->update('tm_kk');
	
    }
*/
    function update($iarea,$dfrom,$dto,$user)
    {
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $dupdate= $row->c;
    	$this->db->set(array(
		  'd_cek'	=> $dupdate,
		  'i_cek'	=> $user
      	 ));
	    $query 	= $this->db->query("update tm_kk set d_cek='$dupdate', i_cek='$user' where f_close='f' and i_area='$iarea' and f_kk_cancel='f'
                                  and d_kk >= to_date('$dfrom','dd-mm-yyyy') AND d_kk <= to_date('$dto','dd-mm-yyyy')");
    }
	function bacacoa($num,$offset)
    {
		$this->db->select(" * from tr_coa where i_coa like '6%' order by i_coa",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakendaraan($area,$periode,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where a.i_area='$area' and a.i_periode='$periode'
							order by a.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikendaraan($area,$periode,$cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan a
					inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
					inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
					where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
					and a.i_area='$area' and a.i_periode='$periode'
					order by a.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
