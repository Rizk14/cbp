<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacasemua($iuser,$now, $dudet, $cari, $num,$offset)
    {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
    if($area1=='00'){
		$this->db->select(" a.d_sj, a.d_nota, a.i_sj, a.i_area, b.e_customer_name, c.e_area_name 
                        from tm_nota a, tr_customer b, tr_area c 
                        where a.i_customer=b.i_customer and a.i_area=c.i_area and a.f_ttb_tolak='f'
                        and (a.i_sj like '%-$now-%' or a.i_sj like '%-$dudet-%') and a.f_nota_cancel='f' 
                        and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                        or upper(a.i_sj) like '%$cari%' or upper(a.i_nota) like '%$cari%')
												order by a.d_sj desc ",false)->limit($num,$offset);
    }else{
		$this->db->select(" a.d_sj, a.d_nota, a.i_sj, a.i_area, b.e_customer_name, c.e_area_name 
                        from tm_nota a, tr_customer b, tr_area c 
                        where a.i_customer=b.i_customer and a.i_area=c.i_area and a.f_ttb_tolak='f'
                        and (a.i_sj like '%-$now-%' or a.i_sj like '%-$dudet-%') and a.f_nota_cancel='f' 
                        and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                        or upper(a.i_sj) like '%$cari%' or upper(a.i_nota) like '%$cari%')
                        and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')                         
												order by a.d_sj desc ",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
	function cari($cari,$num,$offset)
    {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
    if($area1=='00'){
		  	$this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
												  where a.i_customer=b.i_customer
												  and a.i_area=c.i_area and (trim(a.i_nota) = '' or a.i_nota isnull)
												  and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
												  and (upper(a.i_sj) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
												  or upper(c.e_area_name) ilike '%$cari%')
												  order by a.i_nota desc ",FALSE)->limit($num,$offset);
#and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
    }else{
		  	$this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
												  where a.i_customer=b.i_customer
												  and a.i_area=c.i_area and (trim(a.i_nota) = '' or a.i_nota isnull)
												  and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
												  and (a.i_area='$area1' or a.i_area='$area2' 
													  or a.i_area='$area3' or a.i_area='$area4' 
													  or a.i_area='$area5')
												  and (upper(a.i_sj) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
												  or upper(c.e_area_name) ilike '%$cari%')
												  order by a.i_nota desc ",FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacattb($iarea,$ittb,$tahun)
    {
		$this->db->select(" a.d_ttb, a.n_ttb_year, a.d_nota, a.i_nota, a.d_receive1, a.v_ttb_gross, a.i_area, a.n_ttb_discount1,
                        a.n_ttb_discount2, a.n_ttb_discount3, a.v_ttb_discount1, a.v_ttb_discount2, a.v_ttb_discount3,
                        a.i_salesman, a.v_ttb_discounttotal, a.f_ttb_plusppn, a.f_ttb_plusdiscount, a.v_ttb_netto, a.i_customer,
						            b.i_bbm,
						            c.e_area_name,
                        d.e_customer_name,
						            e.e_salesman_name,
                        f.e_customer_pkpnpwp
						            from tm_ttbtolak a 
						            ,tm_bbm b 
						            ,tr_area c 
						            ,tr_customer d 
						            ,tr_salesman e 
						            ,tr_customer_pkp f
						            where a.i_area = '$iarea' 
						            and a.i_ttb = '$ittb' 
						            and a.n_ttb_year=$tahun 
						            and a.i_ttb = b.i_refference_document
						            and a.i_area = c.i_area 
						            and a.i_customer = d.i_customer 
						            and a.i_customer = f.i_customer
						            and a.i_salesman = e.i_salesman",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacattbdetail($iarea,$ittb,$tahun,$koreksi,$nota)
    {
		if($koreksi=='t'){
			$this->db->select("	a.i_product,a.i_product_motif,c.e_product_name,a.v_unit_price, a.n_deliver, 
                          b.n_quantity, d.e_product_motifname, b.e_ttb_remark from tm_notakoreksi_item a
								inner join tr_product c on (c.i_product=a.i_product)
								inner join tr_product_motif d on (d.i_product_motif=a.i_product_motif and 
												d.i_product=a.i_product)
								left join tm_ttbtolak_item b on (b.i_ttb = '$ittb' and b.i_area ='$iarea' and b.n_ttb_year=$tahun and
												 				 b.i_product=a.i_product and b.i_product_motif=a.i_product_motif)
								where a.i_nota = '$nota' and a.i_area ='$iarea'
							   	order by a.n_item_no", false);
		}else{
			$this->db->select("	a.i_product,a.i_product_motif,c.e_product_name,a.v_unit_price, a.n_deliver, 
                          b.n_quantity, d.e_product_motifname, b.e_ttb_remark from tm_nota_item a
                          inner join tr_product c on (c.i_product=a.i_product)
                          inner join tr_product_motif d on (d.i_product_motif=a.i_product_motif and d.i_product=a.i_product)
                          left join tm_ttbtolak_item b on (b.i_ttb = '$ittb' and b.i_area ='$iarea' and b.n_ttb_year=$tahun and
                          b.i_product=a.i_product and b.i_product_motif=a.i_product_motif)
                          where a.i_nota = '$nota' and a.i_area ='$iarea'
                          order by a.n_item_no", false);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
	function baca($isj,$iarea)
    {
    $area1	= $this->session->userdata('i_area');
    if($area1=='00'){
		  $this->db->select("a.*, b.e_customer_name, c.e_customer_pkpnpwp, d.e_salesman_name, e.i_area, e.e_area_name, 
                         f.f_spb_plusppn, f.f_spb_plusdiscount
                         from tm_nota a 
			                   inner join tr_customer b on (a.i_customer=b.i_customer)
			                   inner join tr_customer_pkp c on (a.i_customer=c.i_customer)
			                   inner join tr_salesman d on (a.i_salesman=d.i_salesman)
			                   inner join tr_customer_area e on (a.i_customer=e.i_customer)
                         inner join tm_spb f on (a.i_spb=f.i_spb and a.i_area=f.i_area)
			                   where a.i_sj = '$isj' and a.i_area='$iarea'", false);
    }else{
		  $this->db->select("a.*, b.e_customer_name, c.e_customer_pkpnpwp, d.e_salesman_name, e.i_area, e.e_area_name, 
                         f.f_spb_plusppn, f.f_spb_plusdiscount
                         from tm_nota a
			                   inner join tr_customer b on (a.i_customer=b.i_customer)
			                   inner join tr_customer_pkp c on (a.i_customer=c.i_customer)
			                   inner join tr_salesman d on (a.i_salesman=d.i_salesman)
			                   inner join tr_customer_area e on (a.i_customer=e.i_customer)
                         inner join tm_spb f on (a.i_spb=f.i_spb and a.i_area=f.i_area)
			                   where a.i_sj = '$isj' and a.i_area='$iarea'", false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetail($isj,$iarea)
    {
    $area1	= $this->session->userdata('i_area');
    if($area1=='00'){
		  $this->db->select(" * from tm_nota_item
                          inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_nota_item.i_product_motif
			                                 and tr_product_motif.i_product=tm_nota_item.i_product)
                          where tm_nota_item.i_sj = '$isj' and tm_nota_item.i_area='$iarea'
                          order by tm_nota_item.i_product", false);
    }else{
		  $this->db->select("* from tm_nota_item
						     inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_nota_item.i_product_motif
													     and tr_product_motif.i_product=tm_nota_item.i_product)
						     where tm_nota_item.i_sj = '$isj' and tm_nota_item.i_area='$iarea'
						     order by tm_nota_item.i_product", false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function insertheader(	$iarea,$ittb,$dttb,$icustomer,$isalesman,$inota,$dnota,$nttbdiscount1,$nttbdiscount2,
							$nttbdiscount3,$vttbdiscount1,$vttbdiscount2,$vttbdiscount3,$fttbpkp,$fttbplusppn,
							$fttbplusdiscount,$vttbgross,$vttbdiscounttotal,$vttbnetto,$ettbremark,$fttbcancel,
							$dreceive1,$tahun,$isj)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_area'				=> $iarea,
					'i_ttb'					=> $ittb,
					'd_ttb'					=> $dttb,
					'i_customer'			=> $icustomer,
					'i_salesman'			=> $isalesman,
					'i_nota'				=> $inota,
					'd_nota'				=> $dnota,
					'n_ttb_discount1'		=> $nttbdiscount1,
					'n_ttb_discount2'		=> $nttbdiscount2,
					'n_ttb_discount3'		=> $nttbdiscount3,
					'v_ttb_discount1'		=> $vttbdiscount1,
					'v_ttb_discount2'		=> $vttbdiscount2,
					'v_ttb_discount3'		=> $vttbdiscount3,
					'f_ttb_pkp'				=> $fttbpkp,
					'f_ttb_plusppn'			=> $fttbplusppn,
					'f_ttb_plusdiscount'	=> $fttbplusdiscount,
					'v_ttb_gross'			=> $vttbgross,
					'v_ttb_discounttotal'	=> $vttbdiscounttotal,
					'v_ttb_netto'			=> $vttbnetto,
					'e_ttb_remark'			=> $ettbremark,
					'f_ttb_cancel'			=> $fttbcancel,
					'd_receive1'			=> $dreceive1,
					'd_entry'				=> $dentry,
					'n_ttb_year'			=> $tahun,
          'i_ttb_refference'=> $isj
    		)
    	);
    	$this->db->insert('tm_ttbtolak');
		$this->db->query("update tm_nota set f_ttb_tolak = 't' where i_nota='$inota'",false);
    }
	function insertdetail($iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver,$i,$isj)
    {
    	$this->db->set(
    		array(
					'i_area'    			=> $iarea,
					'i_ttb'			    	=> $ittb,
					'd_ttb'		    		=> $dttb,
					'i_product'		  	=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_quantity'  		=> $nquantity,
					'n_deliver'		  	=> $ndeliver,
					'v_unit_price'  	=> $vunitprice,
					'e_ttb_remark'  	=> $ettbremark,
					'n_ttb_year'	  	=> $tahun,
          'n_item_no'       => $i,
          'i_ttb_refference'=> $isj
    		)
    	);
    	
    	$this->db->insert('tm_ttbtolak_item');
    }
	function updateheader(	$ittb,$iarea,$tahun,$dttb,$dreceive1,$eremark,
							$nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
							$vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
							$vttbgross)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
			'd_ttb'					=> $dttb,
			'd_receive1'			=> $dreceive1,
			'e_ttb_remark'			=> $eremark,
			'd_update'				=> $dupdate,
			'n_ttb_discount1'		=> $nttbdiscount1,
			'n_ttb_discount2'		=> $nttbdiscount2,
			'n_ttb_discount3'		=> $nttbdiscount3,
			'v_ttb_discount1'		=> $vttbdiscount1,
			'v_ttb_discount2'		=> $vttbdiscount2,
			'v_ttb_discount3'		=> $vttbdiscount3,
			'v_ttb_gross'			=> $vttbgross,
			'v_ttb_discounttotal'	=> $vttbdiscounttotal,
			'v_ttb_netto'			=> $vttbnetto
							
    		)
    	);
		$this->db->where('i_ttb',$ittb);
		$this->db->where('i_area',$iarea);
		$this->db->where('n_ttb_year',$tahun);
    	$this->db->update('tm_ttbtolak');
    }
	function updatebbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea)
    {
    	$this->db->set(
    		array(
				'i_refference_document'	=> $ittb,
				'd_refference_document'	=> $dttb,
				'd_bbm'					=> $dbbm,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea
    		)
    	);
    	$this->db->where('i_bbm',$ibbm);
		$this->db->where('i_bbm_type',$ibbmtype);
    	$this->db->update('tm_bbm');
    }
	function insertbbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isalesman)
    {
    	$this->db->set(
    		array(
				'i_bbm'					=> $ibbm,
				'i_bbm_type'			=> $ibbmtype,
				'i_refference_document'	=> $ittb,
				'd_refference_document'	=> $dttb,
				'd_bbm'					=> $dbbm,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea,
				'i_salesman'			=> $isalesman
    		)
    	);
    	
    	$this->db->insert('tm_bbm');
    }
	function insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,$vunitprice,$ittb,$ibbm,$eremark,$dttb,$ibbmtype,$i)
    {
      $th=substr($dttb,0,4);
      $bl=substr($dttb,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_bbm'					=> $ibbm,
				'i_bbm_type'					=> $ibbmtype,
				'i_refference_document'	=> $ittb,
				'i_product'				=> $iproduct,
				'i_product_motif'		=> $iproductmotif,
				'i_product_grade'		=> $iproductgrade,
				'e_product_name'		=> $eproductname,
				'n_quantity'			=> $nquantity,
				'v_unit_price'			=> $vunitprice,
				'e_remark'				=> $eremark,
				'd_refference_document'	=> $dttb,
        'e_mutasi_periode'      => $pr,
        'n_item_no'             => $i
    		)
    	);
    	
    	$this->db->insert('tm_bbm_item');
    }
	function updatestockbbm($iproduct,$iproductgrade,$iproductmotif,$nquantity,$istore,$istorelocation,$istorelocationbin)
    {
    	$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$nquantity
						  where i_product='$iproduct' and i_product_motif='$iproductmotif'
						  and i_product_grade='$iproductgrade' and i_store='$istore'
						  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
    }
	public function deletedetail($iproduct, $iproductgrade, $ittb, $iproductmotif,$nquantity,$istore,
								 $istorelocation,$istorelocationbin,$tahun,$iarea) 
    {
		$this->db->query("DELETE FROM tm_ttbtolak_item WHERE i_ttb='$ittb' and i_area='$iarea'
						  and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						  and i_product_grade='$iproductgrade'");
		$this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'
						  and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						  and i_product_grade='$iproductgrade'");
		return TRUE;
    }
    function runningnumberbbm($thbl){
		  $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BBM'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobbm  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nobbm
                            where i_modul='BBM'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nobbm,"string");
			  $a=strlen($nobbm);
			  while($a<6){
			    $nobbm="0".$nobbm;
			    $a=strlen($nobbm);
			  }
        
			  $nobbm  ="BBM-".$thbl."-".$nobbm;
			  return $nobbm;
		  }else{
			  $nobbm  ="000001";
			  $nobbm  ="BBM-".$thbl."-".$nobbm;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BBM','00',$asal,1)");
			  return $nobbm;
		  }
/*
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_bbm,10,6)) as max from tm_bbm where substr(i_bbm,5,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<6){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="BBM-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="000001";
			  $noso  ="BBM-".$thbl."-".$noso;
			  return $noso;
		  }
*/
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                )
                              ",false);
    }
    function inserttrans44($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw, $tra
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,$qsj,0,0,0,$qsj,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj

                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $ndeliver, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $urut=null;
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
      if($queri->num_rows()>0){
        foreach($queri->result() as $tes){
          $urut=$tes->i_trans;
        }
      }
      $query=$this->db->query(" 
                                DELETE FROM tm_ic_trans 
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation'
                                and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      return $urut;
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
}
?>
