<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
//		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
//		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query("update tm_spmb set f_spmb_cancel='t' WHERE i_spmb='$ispmb'");
		return TRUE;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.i_area, a.i_sjp, a.d_sjp, a.d_sjp_receive, b.i_bapb, b.d_bapb,
                          b.n_bal, d.i_ekspedisi, e.e_ekspedisi
                          from tm_sjp a
                          left join tm_bapbsjp b on (a.i_bapb=b.i_bapb and a.i_area=b.i_area)
                          left join tm_bapbsjp_ekspedisi d on (a.i_bapb=d.i_bapb and b.i_bapb=d.i_bapb and a.i_area=d.i_area)
                          left join tr_ekspedisi e on (d.i_ekspedisi=e.i_ekspedisi)
                          where (upper(a.i_sjp) like '%$cari%' or upper(a.i_bapb) like '%$cari%')
                          and a.d_sjp >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sjp <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_area, a.i_sjp ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
