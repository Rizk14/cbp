<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
    parent::__construct();
  }
  function bacaperiode($dfrom,$dto,$num,$offset)
  {
		$this->db->select("	user_id, ip_address, to_char(waktu,'dd-mm-yyyy hh:mi:ss') as waktu, activity from dgu_log
												where to_char(waktu,'yyyy-mm-dd')>='$dfrom' and to_char(waktu,'yyyy-mm-dd')<='$dto'
												order by user_id, ip_address, waktu ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function cariperiode($dfrom,$dto,$num,$offset,$cari)
  {
		$this->db->select("	user_id, ip_address, to_char(waktu,'dd-mm-yyyy hh:mi:ss') as waktu, activity from dgu_log
												where to_char(waktu,'yyyy-mm-dd')>='$dfrom' and to_char(waktu,'yyyy-mm-dd')<='$dto'
                        and upper(activity) like '%CARD%'
												order by user_id, ip_address, waktu ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
