<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($isetor)
    {
		$this->db->select(" * from tr_customer_setor where i_setor = '$isetor' and f_show='t' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iarea,$icustomer,$ecustomersetor,$ecustomerrekening)
    {
		$fshow	= $icustomer!=''?'FALSE':'TRUE';
		
		$qdate	= $this->db->query("SELECT current_timestamp AS date ");
		$drow	= $qdate->row();
		$dentry	= $drow->date;

		$seq_tm	= $this->db->query(" SELECT i_setor FROM tr_customer_setor ORDER BY i_setor DESC LIMIT 1 ");
		if($seq_tm->num_rows() > 0 ) {
			$seqrow	= $seq_tm->row();
			$isetor	= $seqrow->i_setor+1;
		} else {
			$isetor	= 1;				
		}
						
    	$this->db->set(
    		array(
    		'i_setor' => $isetor,
    		'i_customer' => $icustomer,
			'i_area' => $iarea,
			'e_customer_setorname' => $ecustomersetor,
			'e_customer_setorrekening' => $ecustomerrekening,
			'f_show'=>$fshow,
			'd_entry'=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_customer_setor');

		if($icustomer!='') {
			$qowner	= $this->db->query(" select * from tr_customer_owner where i_customer=trim('$icustomer') ");
			if($qowner->num_rows()>0) {
				//$rowowner	= $qowner->row();
					$data2 = array(
						'e_customer_setor' => $ecustomersetor
					);
					$this->db->where('i_customer =', $icustomer);
					$this->db->update('tr_customer_owner', $data2); 					
			}
		}
		    	
		#redirect('customersetor/cform/');
    }
    function update($isetor,$iarea,$icustomer,$ecustomersetor,$ecustomerrekening)
    {
    	$data = array(
				'i_customer' => $icustomer,
				'i_area' => $iarea,
				'e_customer_setorname' => $ecustomersetor,
				'e_customer_setorrekening' => $ecustomerrekening,
				'f_show'=>'FALSE'
            );
		$this->db->where('i_setor =', $isetor);
		$this->db->update('tr_customer_setor', $data); 
		
		if($icustomer!='') {
			$qowner	= $this->db->query(" select * from tr_customer_owner where i_customer=trim('$icustomer') ");
			if($qowner->num_rows()>0) {
				//$rowowner	= $qowner->row();
					$data2 = array(
						'e_customer_setor' => $ecustomersetor
					);
					$this->db->where('i_customer =', $icustomer);
					$this->db->update('tr_customer_owner', $data2); 					
			}
		}
		#redirect('customersetor/cform/');
    }
	
    public function delete($isetor) 
    {
		$this->db->query("DELETE FROM tr_customer_setor WHERE i_setor='$isetor' and f_show='t' ", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_customer_setor where f_show='t' and (upper(i_customer) like '%$cari%' or upper(e_customer_setorname) like '%$cari%') order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tr_customer_setor where f_show='t' and (upper(i_customer) ilike '%$cari%' or upper(e_customer_setorname) ilike '%$cari%') order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapelanggan($num,$offset) {
		$this->db->select("* from tr_customer order by e_customer_name asc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function caripelanggan($cari,$num,$offset) {
		$this->db->select("* from tr_customer where (upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%') order by e_customer_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacaarea($area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select("	* from tr_area 
								order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("	* from tr_area 
								where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'
								order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($area1,$area2,$area3,$area4,$area5,$cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select("	i_area, e_area_name, i_store from tr_area 
								where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
								order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("	i_area, e_area_name, i_store from tr_area 
								where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
								and (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
								order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }		
}
?>
