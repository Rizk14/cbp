<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea, $itunai)
    {
		$this->db->select("	a.d_tunai, a.i_tunai, d.e_area_name, a.v_jumlah, a.v_sisa, a.i_area, a.i_salesman, e.e_salesman_name,
		                    a.i_customer, a.i_customer_groupar, c.e_customer_name, a.e_remark, a.i_rtunai
		      				from tm_tunai a
							left join tr_customer_salesman e on(a.i_customer=e.i_customer and a.i_area=e.i_area and a.i_salesman=e.i_salesman
 				 			and e.e_periode=to_char(a.d_tunai,'yyyymm'))
							left join tr_customer c on(a.i_customer=c.i_customer)
							left join tr_area d on(a.i_area=d.i_area)
							where a.i_tunai='$itunai' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($iarea,$ikum,$tahun)
    {
		$this->db->select(" i_kum from tm_kum where i_area='$iarea' and i_kum='$ikum' and n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			      => $iarea,
				'i_tunai'				    => $itunai,
				'i_customer'		    => $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'		    => $isalesman,
				'd_tunai'				    => $dtunai,
				'd_entry'			      => $dentry,
				'e_remark'			    => $eremark,
				'v_jumlah'			    => $vjumlah,
				'v_sisa'			      => $vsisa
    		)
    	);
    	
    	$this->db->insert('tm_tunai');
    }
    function update($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$eremark,$vjumlah,$vsisa,$xiarea)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			      => $iarea,
				'i_customer'		    => $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'		    => $isalesman,
				'd_tunai'				    => $dtunai,
				'd_update'		      => $dentry,
				'e_remark'			    => $eremark,
				'v_jumlah'			    => $vjumlah,
				'v_sisa'			      => $vsisa
    		)
    	);
    	$this->db->where('i_tunai',$itunai);
    	$this->db->where('i_area',$xiarea);
    	$this->db->update('tm_tunai');
    }
	function bacaarea($num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
  		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
		  $this->db->select(" * from tr_area 
							  where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
                          and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacacustomer($iarea,$per,$cari,$num,$offset){
		$this->db->select(" distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, 
					b.i_customer_groupar, 
					c.e_salesman_name, 
					c.i_salesman,
					d.e_customer_setor 

				from tr_customer a 
				left join tr_customer_groupar b on(a.i_customer=b.i_customer) 
				left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per') 
				left join tr_customer_owner d on(a.i_customer=d.i_customer)
				
				where a.i_area='$iarea' and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')
				order by a.i_customer ",FALSE)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function caricustomer($cari,$iarea,$num,$offset){
		$this->db->select(" a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
							left join tr_customer_groupar b on(a.i_customer=b.i_customer)
							left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
							left join tr_customer_owner d on(a.i_customer=d.i_customer)

						   	where a.i_area='$iarea' 
							and (upper(a.i_customer) like '%$cari%' or 
							upper(a.e_customer_name) like '%$cari%' or 
							upper(d.e_customer_setor) like '%$cari%' ) 
							order by a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
  function runningnumber($iarea,$thbl){
    $th   = substr($thbl,0,4);
    $asal=$thbl;
    $thbl=substr($thbl,2,2).substr($thbl,4,2);
    $this->db->select(" n_modul_no as max from tm_dgu_no
                      where i_modul='TN'
                      and substr(e_periode,1,4)='$th'
                      and i_area='$iarea' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
       foreach($query->result() as $row){
         $terakhir=$row->max;
       }
       $notn  =$terakhir+1;
    $this->db->query(" update tm_dgu_no
                        set n_modul_no=$notn
                        where i_modul='TN'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea'", false);
       settype($notn,"string");
       $a=strlen($notn);
       while($a<6){
         $notn="0".$notn;
         $a=strlen($notn);
       }
       $notn  ="TN-".$thbl."-".$notn;
       return $notn;
    }else{
       $notn  ="000001";
       $notn  ="TN-".$thbl."-".$notn;
       $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                          values ('TN','$iarea','$asal',1)");
       return $notn;
    }
  }
  function bacasalesman($iarea,$cari,$num,$offset)
    {
      $query = $this->db->select("distinct a.i_salesman, a.e_salesman_name from tr_salesman a
                                  where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                  and a.f_salesman_aktif='true'",false)->limit($num,$offset);
/*
      $query = $this->db->select("distinct a.i_salesman, a.e_salesman_name from tr_salesman a
                                  where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                  and a.i_area='$iarea' and a.f_salesman_aktif='true'",false)->limit($num,$offset);
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
