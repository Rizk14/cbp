<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$num,$offset,$dfrom,$dto)
    {
			if($area1=='00')
			{
			   $this->db->select(" a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, a.f_dt_cancel, a.n_print from tm_dt a, tr_area b
			                       where a.i_area=b.i_area and a.f_dt_cancel ='f'
			                       and (upper(a.i_dt) like '%$cari%') and a.d_dt >= to_date('$dfrom', 'dd-mm-yyyy') 
			                       and a.d_dt <= to_date('$dto', 'dd-mm-yyyy') and a.i_area='$iarea' order by a.i_dt DESC",false)->limit($num,$offset);
			}
			else
			{
			   $this->db->select(" a.*, c.* from tm_dt a, tr_area b, tm_pelunasan c
						                 where a.i_area=b.i_area and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                             or a.i_area='$area4' or a.i_area='$area5')
                             and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                             and a.i_area='$iarea' and a.f_dt_cancel ='f'
		                         and a.i_dt = c.i_dt and a.d_dt = c.d_dt and a.i_area = c.i_area
						                 and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						                 or upper(a.i_dt) like '%$cari%') order by a.i_dt DESC",false)->limit($num,$offset);
			}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($idt,$iarea)
    {
/*
		$this->db->select(" a.*, b.*, c.* from tm_dt a, tm_pelunasan b, tr_area c
		                    where a.i_dt = '$idt' and a.i_area='$iarea' and a.i_area = c.i_area
		                    and a.i_dt = b.i_dt and a.d_dt = b.d_dt and a.i_area = b.i_area
		                    order by a.i_dt desc",false);
*/
/*
		$this->db->select(" distinct on(a.i_dt) a.*, b.*, c.* from tm_dt a, tr_area b, tm_alokasi c, tm_alokasi_item d, tm_dt_item e
                        where a.i_dt = '$idt' and a.i_area='$iarea' and a.i_area = b.i_area and a.f_dt_cancel='f'
                        and a.i_area=c.i_area and a.i_dt=e.i_dt and a.i_area=e.i_area and a.d_dt=e.d_dt
                        and c.i_alokasi=d.i_alokasi and c.i_area=d.i_area and c.i_kbank=d.i_kbank and d.i_nota=e.i_nota 
                        and c.f_alokasi_cancel='f' order by a.i_dt desc",false);
*/                        
    $this->db->select(" * from (
                        select distinct on(a.i_dt) b.e_area_name, e.i_nota, a.i_dt, a.i_area, a.d_dt from tr_area b, tm_dt_item e, tm_dt a
                        where a.i_dt = '$idt' and a.i_area='$iarea' and a.i_area = b.i_area and a.f_dt_cancel='f'
                        and a.i_dt=e.i_dt and a.i_area=e.i_area and a.d_dt=e.d_dt
                        ) as a
                        left join tm_alokasi_item d on (d.i_nota=a.i_nota)
                        left join tm_alokasi c on (a.i_area=c.i_area and c.f_alokasi_cancel='f' and c.i_alokasi=d.i_alokasi and 
                        c.i_area=d.i_area and c.i_kbank=d.i_kbank)
                        left join tm_alokasikn_item f on (f.i_nota=a.i_nota)
                        left join tm_alokasikn e on (a.i_area=e.i_area and e.f_alokasi_cancel='f' and e.i_alokasi=f.i_alokasi and 
                        e.i_area=f.i_area and e.i_kn=f.i_kn)",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($idt,$iarea)
    {
      $this->db->select(" b.* from (
      SELECT a.*, d.v_jumlah as jumlahitem, d.i_alokasi, c.i_giro, c.d_alokasi, d.e_remark from ( 
      select a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo 
      from tm_dt_item a, tr_customer c, tm_nota b, tm_dt f 
      where a.i_dt='$idt' and a.i_area='$iarea' and f.f_dt_cancel='f' and a.i_dt=f.i_dt and a.i_area=f.i_area and a.d_dt=f.d_dt and a.i_nota=b.i_nota and a.i_customer=c.i_customer 
      ) as a 
      left join tm_alokasi_item d on (d.i_nota=a.i_nota) 
      left join tm_alokasi c on (a.i_area=c.i_area and c.f_alokasi_cancel='f' and c.i_alokasi=d.i_alokasi and c.i_area=d.i_area and c.i_kbank=d.i_kbank) 
      union all
      SELECT a.*, f.v_jumlah as jumlahitem, f.i_alokasi, e.i_kn, e.d_alokasi, f.e_remark from ( 
      select a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo 
      from tm_dt_item a, tr_customer c, tm_nota b, tm_dt f 
      where a.i_dt='$idt' and a.i_area='$iarea' and f.f_dt_cancel='f' and a.i_dt=f.i_dt and a.i_area=f.i_area and a.d_dt=f.d_dt and a.i_nota=b.i_nota and a.i_customer=c.i_customer 
      ) as a 
      inner join tm_alokasikn_item f on (f.i_nota=a.i_nota) 
      inner join tm_alokasikn e on (a.i_area=e.i_area and e.f_alokasi_cancel='f' and e.i_alokasi=f.i_alokasi and e.i_area=f.i_area and e.i_kn=f.i_kn)
      union all
      SELECT a.*, f.v_jumlah as jumlahitem, f.i_alokasi, e.i_kn, e.d_alokasi, f.e_remark from ( 
      select a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo 
      from tm_dt_item a, tr_customer c, tm_nota b, tm_dt f 
      where a.i_dt='$idt' and a.i_area='$iarea' and f.f_dt_cancel='f' and a.i_dt=f.i_dt and a.i_area=f.i_area and a.d_dt=f.d_dt and a.i_nota=b.i_nota and a.i_customer=c.i_customer 
      ) as a 
      inner join tm_alokasiknr_item f on (f.i_nota=a.i_nota) 
      inner join tm_alokasiknr e on (a.i_area=e.i_area and e.f_alokasi_cancel='f' and e.i_alokasi=f.i_alokasi and e.i_area=f.i_area and e.i_kn=f.i_kn) 
      ) as b
      order by b.n_item_no, b.d_alokasi, b.jumlahitem desc",false);
/* 
2016-11-08
    $this->db->select(" a.*, d.v_jumlah as jumlahitem, d.i_alokasi, c.i_giro, c.d_alokasi, d.e_remark from(
                        select a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo
                        from tm_dt_item a, tr_customer c, tm_nota b, tm_dt f
                        where a.i_dt='$idt' and a.i_area='$iarea' and f.f_dt_cancel='f' and a.i_dt=f.i_dt and a.i_area=f.i_area 
                        and a.d_dt=f.d_dt and a.i_nota=b.i_nota and a.i_customer=c.i_customer
                        ) as a
                        left join tm_alokasi_item d on (d.i_nota=a.i_nota)
                        left join tm_alokasi c on (a.i_area=c.i_area and c.f_alokasi_cancel='f' and c.i_alokasi=d.i_alokasi and 
                        c.i_area=d.i_area and c.i_kbank=d.i_kbank)
                        left join tm_alokasikn_item f on (f.i_nota=a.i_nota)
                        left join tm_alokasikn e on (a.i_area=e.i_area and e.f_alokasi_cancel='f' and e.i_alokasi=f.i_alokasi and 
                        e.i_area=f.i_area and e.i_kn=f.i_kn)
                        order by a.n_item_no",false);
*/
/*    
		$this->db->select(" a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo, d.v_jumlah as jumlahitem, d.*, e.*
                        from tm_dt_item a, tr_customer c, tm_nota b, tm_alokasi_item d, tm_alokasi e, tm_dt f
                        where a.i_dt='$idt' and a.i_area='$iarea' and f.f_dt_cancel='f' and a.i_dt=f.i_dt and a.i_area=f.i_area 
                        and a.d_dt=f.d_dt and d.i_alokasi = e.i_alokasi and e.i_kbank = d.i_kbank and d.i_area = e.i_area 
                        and a.i_area = d.i_area and a.i_nota=b.i_nota 
                        and a.i_nota = d.i_nota and a.i_customer=c.i_customer
                        order by b.d_nota, a.i_nota, a.n_item_no",false);
*/
/*
		$this->db->select("a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo, d.v_jumlah as jumlahitem, d.*, e.*, f.e_jenis_bayarname
		                   from tm_dt_item a, tr_customer c, tm_nota b, tm_pelunasan_item d, tm_pelunasan e, tr_jenis_bayar f
		                   where a.i_dt='$idt' and a.i_area='$iarea' and a.i_dt = d.i_dt 
		                   and d.i_dt = e.i_dt and e.i_pelunasan = d.i_pelunasan and d.d_dt = e.d_dt
		                   and a.d_dt = d.d_dt and a.i_area = d.i_area and a.i_nota=b.i_nota 
		                   and e.i_jenis_bayar = f.i_jenis_bayar
		                   and a.i_nota = d.i_nota and a.i_customer=c.i_customer
		                   order by b.d_nota, a.i_nota, a.n_item_no",false);
*/
/*
		$this->db->select(" 	a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo
					                from tm_dt_item a, tr_customer c, tr_customer_groupbayar d, tm_nota b
					                where a.i_dt='$idt' and a.i_area='$iarea'

                            and a.i_nota=b.i_nota 
						                and a.i_customer=c.i_customer
						                and d.i_customer=c.i_customer
					                order by a.n_item_no",false);
*/
/*				$this->db->select("a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo
					                from tm_dt_item a, tr_customer c, tm_nota b
					                where a.i_dt='$idt' and a.i_area='$iarea'
                            and a.i_nota=b.i_nota 
						                and a.i_customer=c.i_customer
					                order by a.n_item_no",false);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
				where a.i_area=b.i_area and a.f_sisa='t'
				and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
				or upper(a.i_dt) like '%$cari%')
				order by a.i_dt desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
