<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	  public function __construct()
    {
          parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$icustomer)
    {
      $this->db->select(" a.d_notapb, a.i_customer, b.e_customer_name, sum(c.n_quantity) as jumlah, a.n_notapb_discount,
                          sum(a.v_notapb_discount) as diskon, sum(c.n_quantity*c.v_unit_price) as kotor, a.i_area
                          from tm_notapb a, tr_customer b, tm_notapb_item c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_notapb <= to_date('$dto','dd-mm-yyyy')) and a.i_customer='$icustomer'
                          and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                          group by a.d_notapb, a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                          order by a.d_notapb, a.n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadiskon($dfrom,$dto,$icustomer)
    {
		  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                          where (d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                          and d_notapb <= to_date('$dto','dd-mm-yyyy')) and i_customer='$icustomer'
                          order by n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatotal($dfrom,$dto,$icustomer)
    {
		  $this->db->select(" distinct(a.i_customer), b.e_customer_name, sum(c.n_quantity) as totalpcs, a.n_notapb_discount,
                          sum(c.n_quantity*c.v_unit_price) as totalkotor, a.i_area
                          from tm_notapb a, tr_customer b, tm_notapb_item c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_notapb <= to_date('$dto','dd-mm-yyyy')) and a.i_customer='$icustomer'
                          and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                          group by a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                          order by a.n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function bacacustomer($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select(" a.*, b.e_customer_name, c.e_area_name
                            from tr_spg a, tr_customer b, tr_area c
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select(" a.*, b.e_customer_name, c.e_area_name
                            from tr_spg a, tr_customer b, tr_area c
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                            and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
						                or a.i_area = '$area4' or a.i_area = '$area5')", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function caricustomer($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("a.*, b.e_customer_name, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                          and (upper(a.i_customer) like '%$cari%'
                          or upper(b.e_customer_name) like '%$cari%') ", FALSE)->limit($num,$offset);
		  }else{
			  $this->db->select("a.*, b.e_customer_name, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area and
                          (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                          and (a.i_area = '$area1' or a.i_area = '$area2' 
    										  or a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5')", FALSE)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
