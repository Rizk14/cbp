<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibbk)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_bbkretur a, tr_supplier b
					              where a.i_supplier=b.i_supplier
					              and i_bbkretur ='$ibbk'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ibbk)
    {
			$this->db->select("a.*, b.e_product_motifname from tm_bbkretur_item a, tr_product_motif b
						             where a.i_bbkretur = '$ibbk' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						             order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updateheader($ibbkretur, $dbbkretur, $isupplier, $eremark, $vbbkretur)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'd_bbkretur'            => $dbbkretur,
			    'i_supplier' 	          => $isupplier,
          'e_remark'              => $eremark,
          'v_bbkretur'            => $vbbkretur,
          'd_update'              => $now
    		)
    	);
      $this->db->where("i_bbkretur",$ibbkretur);      
    	$this->db->update('tm_bbkretur');
    }
    function insertheader($ibbkretur, $dbbkretur, $isupplier, $eremark, $vbbkretur)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'i_bbkretur'            => $ibbkretur,
			    'd_bbkretur'            => $dbbkretur,
			    'i_supplier' 	          => $isupplier,
          'e_remark'              => $eremark,
          'v_bbkretur'            => $vbbkretur,
          'd_entry'               => $now
    		)
    	);
    	$this->db->insert('tm_bbkretur');
    }
    function updatedetail($ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl)
    {
    	$this->db->set(
    		array(
					'v_unit_price'		      => $vunitprice
    		)
    	);
    	$this->db->where('i_bbkretur',$ibbkretur);
    	$this->db->where('i_product',$iproduct);
    	$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->where('i_product_grade',$iproductgrade);
    	$this->db->update('tm_bbkretur_item');
    }
    function insertdetail($ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl, $idtap)
#    function insertdetail($ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl, $idtap)
    {
    	$this->db->set(
    		array(
					'i_bbkretur'            => $ibbkretur,
					'i_supplier'            => $isupplier,
					'i_product'	 	          => $iproduct,
					'i_product_grade'	      => $iproductgrade,
					'i_product_motif'	      => $iproductmotif,
					'n_quantity'		        => $nquantity,
					'v_unit_price'		      => $vunitprice,
					'e_product_name'	      => $eproductname,
					'e_remark'		          => $eremark,
					'i_dtap'		            => $idtap,
          'e_mutasi_periode'      => $thbl,
          'n_item_no'             => $i
    		)
    	);
    	
    	$this->db->insert('tm_bbkretur_item');
    }
    public function deletedetail($ibbkretur, $iproduct, $iproductmotif, $iproductgrade)
    {
		  $this->db->query("DELETE FROM tm_bbkretur_item WHERE i_bbkretur='$ibbkretur' and i_product='$iproduct' 
		                    and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'");
/*
      $this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$nquantityx
						    where i_product='$iproduct' and i_product_motif='$iproductmotif'
						    and i_product_grade='$iproductgrade' and i_store='$istore'
						    and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'");
*/
    }
	
    public function delete($ispmb) 
    {
#		  $this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
#		  $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$cari,$supp, $bulan, $tahun)
    {
		  if($offset=='')
			  $offset=0;
/*
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, 
						                    c.e_product_name as nama,c.v_product_mill as harga
						                    from tr_product_motif a,tr_product c, tr_supplier d
						                    where a.i_product=c.i_product and 
						                    (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
						                    and c.i_supplier='$supp' and c.i_supplier=d.i_supplier
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
*/
		  // ambil tgl terakhir di bln tsb
		  $tahunawal='2009';
		  $bulanawal='01';
			$timeStamp            =    mktime(0,0,0,$bulan,1,$tahun);    //Create time stamp of the first day from the give date.
			$firstDay            =     date('d',$timeStamp);    //get first day of the given month
			list($y,$m,$t)        =    explode('-',date('Y-m-t',$timeStamp)); //Find the last date of the month and separating it
			$lastDayTimeStamp    =    mktime(0,0,0,$m,$t,$y);//create time stamp of the last date of the give month
			$lastDay            =    date('d',$lastDayTimeStamp);// Find last day of the month
			
			/*echo " select distinct a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif,
						                    c.e_product_name as nama,f.v_pabrik as harga, e.i_pajak, e.i_dtap, e.d_dtap
						                    from tr_product_motif a,tr_product c, tr_supplier d, tm_dtap e, tm_dtap_item f
						                    where a.i_product=c.i_product and
						                    e.i_dtap=f.i_dtap and e.i_area=f.i_area and e.i_supplier=f.i_supplier and
						                    f.i_product=c.i_product and e.i_supplier=d.i_supplier and
						                    (upper(a.i_product) like UPPER('%$cari%') or upper(c.e_product_name) like UPPER('%$cari%') or upper(e.i_pajak) like UPPER('%$cari%'))
						                    AND e.d_dtap between '".$tahun."-".$bulan."-01' AND '".$tahun."-".$bulan."-".$lastDay."'
						                    
						                    and c.i_supplier='$supp' and c.i_supplier=d.i_supplier
                                order by e.d_dtap, a.i_product, a.e_product_motifname
                                limit $num offset $offset "; die(); */
		  
		  $query=$this->db->query(" select distinct a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif,
						                    c.e_product_name as nama,f.v_pabrik as harga, e.i_pajak, e.i_dtap, e.d_dtap
						                    from tr_product_motif a,tr_product c, tr_supplier d, tm_dtap e, tm_dtap_item f
						                    where a.i_product=c.i_product and
						                    e.i_dtap=f.i_dtap and e.i_area=f.i_area and e.i_supplier=f.i_supplier and
						                    f.i_product=c.i_product and e.i_supplier=d.i_supplier and
						                    (upper(a.i_product) like UPPER('%$cari%') or upper(c.e_product_name) like UPPER('%$cari%') 
						                    or upper(e.i_pajak) like UPPER('%$cari%'))
						                    AND e.d_dtap between '".$tahunawal."-".$bulanawal."-01' AND '".$tahun."-".$bulan."-".$lastDay."'
						                    and c.i_supplier='$supp' and c.i_supplier=d.i_supplier
                                order by e.d_dtap, a.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function runningnumber($thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BBR'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobbk  =$terakhir+1;
        $this->db->query("update tm_dgu_no 
                          set n_modul_no=$nobbk
                          where i_modul='BBR'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th'", false);
			  settype($nobbk,"string");
			  $a=strlen($nobbk);
			  while($a<4){
			    $nobbk="0".$nobbk;
			    $a=strlen($nobbk);
			  }
			  	$nobbk  ="BBK-".$thbl."-KR".$nobbk;
			  return $nobbk;
		  }else{
			  $nobbk  ="KR0001";
		  	$nobbk  ="BBK-".$thbl."-".$nobbk;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BBR','00','$asal',1)");
			  return $nobbk;
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacasupplier($num,$offset,$cari)
    {
			$this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
                          order by i_supplier", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$qbbk,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbk', '$now', 0, $qbbk, $q_ak-$qbbk, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_returpabrik=n_mutasi_returpabrik+$qbbk, n_saldo_akhir=n_saldo_akhir-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,0,0,$qbbk,0,$qbbk,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbbk)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', 0, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibbkretur,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
#and i_refference_document='$ibbk'
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ibbkretur', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_returpabrik=n_mutasi_returpabrik-$qbbk, n_saldo_akhir=n_saldo_akhir+$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
}
?>
