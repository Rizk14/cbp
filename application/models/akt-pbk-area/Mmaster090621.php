<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($ikbank,$iperiode,$iarea,$ibank,$icoabank)
    {

			$query=$this->db->query("	select a.*, b.e_area_name, '' as e_pengguna, e.i_bank, e.e_bank_name, e.i_coa as coabank 
			                          from tm_kbank a, tr_area b , tm_rv_item c, tm_rv d, tr_bank e
								                where a.i_area=b.i_area and a.i_periode='$iperiode' and a.i_kbank='$ikbank' and a.i_area='$iarea'
								                and a.i_kbank=c.i_kk and a.i_area=c.i_area_kb and c.i_rv_type='02'  and e.i_bank='$ibank'
								                and a.i_coa_bank='$icoabank'
								                and d.i_rv=c.i_rv and d.i_area=c.i_area and d.i_rv_type=c.i_rv_type and d.i_coa=e.i_coa",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacoa($num,$offset)
    {
		$this->db->select(" * from tr_coa where i_coa like '6%' order by i_coa",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricoa($cari,$num,$offset)
    {
		$this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) and upper(i_coa) like '6%'
												order by i_coa",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakendaraan($area,$periode,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where a.i_area='$area' and a.i_periode='$periode'
							order by a.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikendaraan($area,$periode,$cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan a
					inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
					inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
					where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
					and a.i_area='$area' and a.i_periode='$periode'
					order by a.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	function bacagiro($area,$xtgl,$num,$offset,$group){
		$this->db->select("a.* from (
						   select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
						   and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl' and not a.i_giro in(select i_giro from tm_kbank)
						   union all
						   select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
						   tm_rtunai_item d
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
						   and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
						   and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and not a.i_tunai in(select i_giro from tm_kbank)
						   union all
						   select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
						   and d_kum<='$xtgl' 
						   --and not a.i_kum in(select i_giro from tm_kbank)
						   )as a
						   order by a.tgl, a.bayar ",FALSE)->limit($num,$offset);
	   $query = $this->db->get();
	   if ($query->num_rows() > 0){
		  return $query->result();
	   }
	 }

	 function carigiro($cari,$area,$xtgl,$num,$offset,$group){
		$this->db->select(" a.* from (
						   select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
						   and (upper(a.i_giro) like '%$cari%')
						   and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl'
						   union all
						   select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
						   tm_rtunai_item d
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
						   and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
						   and (upper(a.i_tunai) like '%$cari%')
						   and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
						   union all
						   select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
						   where a.i_customer=b.i_customer and a.i_area='$area'
						   and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
						   and (upper(a.i_kum) like '%$cari%')
						   and d_kum<='$xtgl'
						   )as a
						   order by a.tgl, a.bayar ",FALSE)->limit($num,$offset);
	   $query = $this->db->get();
	   if ($query->num_rows() > 0){
		  return $query->result();
	   }
	 } 
    function insert($iarea,$ikb,$iperiode,$icoa,$ikendaraan,$vkb,$dkb,$ecoaname,$edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,$enamatoko,$epengguna,$ibukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'	=> $iarea,
				'i_kb'		=> $ikb,				
				'i_periode'	=> $iperiode,
				'i_kendaraan'	=> $ikendaraan,
				'i_coa'		=> $icoa,
				'v_kb'		=> $vkb,
				'i_bukti_pengeluaran'	=> $ibukti,
				'd_kb'		=> $dkb,
				'e_coa_name'	=> $ecoaname,
				'e_description'	=> $edescription,
				'e_jam_in'	=> $ejamin,
				'e_jam_out'	=> $ejamout,
				'n_km'		=> $nkm,
				'e_tempat'	=> $etempat,
				'd_entry'	=> $dentry,
				'd_bukti'	=> $dbukti,
				'f_debet'	=> $fdebet,
				'e_nama_toko'	=> $enamatoko,
				'e_pengguna'	=> $epengguna
    		)
    	);
    	$this->db->insert('tm_kb');
    }
    function update($iarea,$ikbank,$iperiode,$icoa,$vbank,$dbank,$ecoaname,$edescription,$fdebet,$irvtype,$icoabank,
                    $vbankold,$iareaold,$icoaold,$igiro)
    {
#####
      $irv='';
      $vrv=0;
#      $this->db->select(" i_rv, v_rv from tm_rv_item where i_kk='$ikbank' and i_area='$iarea' and i_rv_type='$irvtype'", false);
      $this->db->select(" i_rv, v_rv from tm_rv_item where i_kk='$ikbank' and i_area='00' and i_rv_type='$irvtype'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $irv=$row->i_rv;
			    $vrv=$row->v_rv;
			  }

#        $this->db->query(" update tm_rv set v_rv=v_rv-$vrv, i_area='$iarea'
#                           where i_rv='$irv' and i_area='$iareaold' and i_rv_type='$irvtype'", false);
        $this->db->query(" update tm_rv set v_rv=v_rv-$vrv
                           where i_rv='$irv' and i_area='00' and i_rv_type='$irvtype'", false);
#        $this->db->query(" delete from tm_rv_item where i_rv='$irv' and i_area='$iareaold' and i_kk='$ikbank' and i_rv_type='$irvtype'", false);
        $this->db->query(" delete from tm_rv_item where i_rv='$irv' and i_area='00' and i_kk='$ikbank' and i_rv_type='$irvtype'", false);
      	$this->db->set(
      		array(
			    'i_area'	            => '00',
			    'i_rv'	            	=> $irv,
			    'i_coa'              	=> $icoa,
			    'e_coa_name'	        => $ecoaname,
			    'v_rv'		            => $vbank,
			    'e_remark'    	      => $edescription,
			    'i_kk'                => $ikbank,
			    'i_rv_type'           => $irvtype,
			    'i_area_kb'           => $iarea,
			    'i_coa_bank'          => $icoabank
      		)
      	);
      	$this->db->insert('tm_rv_item');
		    $quer 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $quer->row();
		    $dupdate= $row->c;
      	$this->db->query(" update tm_rv set v_rv=v_rv+$vbank, d_update='$dupdate' where i_rv='$irv' and i_area='00' and i_rv_type='$irvtype'", false);
	    }
#####    
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
		    $this->db->set(
    		array(
				  'i_area'	     => $iarea,
				  'i_coa'		 => $icoa,
				  'v_bank'	     => $vbank,
				  'v_sisa'	     => $vbank,
				  'd_bank'	     => $dbank,
				  'e_coa_name'	 => $ecoaname,
				  'e_description'=> $edescription,
				  'd_update'	=> $dupdate,
				  'i_giro'		=> $igiro,
  			  	  'f_debet'	    => $fdebet
      		)
      	);
		    $this->db->where('i_kbank',$ikbank);
		    $this->db->where('i_periode',$iperiode);
		    $this->db->where('i_coa_bank',$icoabank);
       	$this->db->update('tm_kbank');
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ikbank' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ikbank' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ikbank' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ikbank' and i_area='$iareaold'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ikbank' and i_area='$iareaold'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ikbank' and i_area='$iareaold'");
#####
    }
	function runningnumberkb($th,$bl,$iarea)
	{
		$this->db->select(" max(substr(i_kb,9,5)) as max from tm_kb where substr(i_kb,4,2)='$th' and substr(i_kb,6,2)='$bl' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nogj  =$terakhir+1;
			settype($nogj,"string");
			$a=strlen($nogj);
			while($a<5){
			  $nogj="0".$nogj;
			  $a=strlen($nogj);
			}
			$nogj  ="kb-".$th.$bl."-".$nogj;
			return $nogj;
		}else{
			$nogj  ="00001";
			$nogj  ="kb-".$th.$bl."-".$nogj;
			return $nogj;
		}
    }
	function bacasaldo($area,$periode,$tanggal)
  {	    
		$tmp = explode("-", $tanggal);
		$thn	= $tmp[0];
		$bln	= $tmp[1];
		$tgl 	= $tmp[2];
		$dsaldo	= $thn."/".$bln."/".$tgl;
		$dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
		$tmp1 	= explode("-", $dtos,strlen($dtos));
		$th	= $tmp1[0];
		$bl	= $tmp1[1];
		$dt	= $tmp1[2];
		$dtos	= $th.$bl;
		$this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.2' ",false);
		$query = $this->db->get();
		$saldo=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$saldo=$row->v_saldo_awal;
			}
		}
		$this->db->select(" sum(v_kb) as v_kb from tm_kb
							where i_periode='$dtos' and i_area='$area'
							and d_kb<='$tanggal' and f_debet='t' and f_kb_cancel='f'",false);						 
		$query = $this->db->get();
		$kredit=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$kredit=$row->v_kb;
			}
		}
		$this->db->select(" sum(v_kb) as v_kb from tm_kb
							where i_periode='$dtos' and i_area='$area'
							and d_kb<='$tanggal' and f_debet='f' and f_kb_cancel='f'",false);							
		$query = $this->db->get();
		$debet=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$debet=$row->v_kb;
			}
		}
		$saldo=$saldo+$debet-$kredit;
		return $saldo;
  }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		     }      
		    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
		    $dateTimeArr=getdate($dateTime);
		    $nosecmin = 0;
		    $min=$dateTimeArr['minutes'];
		    $sec=$dateTimeArr['seconds'];
		    if ($hr==0){$nosecmin += 1;}
		    if ($min==0){$nosecmin += 1;}
		    if ($sec==0){$nosecmin += 1;}
		    if ($nosecmin>2){     
				return(date("Y-m-d",$dateTime));
			} else {     
				return(date("Y-m-d G:i:s",$dateTime));
			}
	}
	function area($iarea)
    {
		$this->db->select(" e_area_name from tr_area where i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
				$nama=$row->e_area_name;
				return $nama;
			}
		}
    }
	function bacakbgroup($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kb_group where upper(i_kb_group) like '%$cari%' or upper(e_kb_groupname) like '%$cari%' order by i_kb_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
###########posting##########
	function inserttransheader(	$inota,$iarea,$eremark,$fclose,$dkn )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose','$dkn','$dkn')");
	}
	function inserttransitemdebet($accdebet,$ikn,$namadebet,$fdebet,$fposting,$iarea,$eremark,$vjumlah,$dkn,$iarea)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry, i_area)
						  	  values
					  	 ('$accdebet','$ikn','$namadebet','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea')");
	}
	function inserttransitemkredit($acckredit,$ikn,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dkn,$iarea)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry, i_area)
						  	  values
					  	 ('$acckredit','$ikn','$namakredit','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea')");
	}
	function insertgldebet($accdebet,$ikn,$namadebet,$fdebet,$iarea,$vjumlah,$dkn,$eremark)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namadebet=str_replace("'","''",$namadebet);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ikn','$accdebet','$dkn','$namadebet','$fdebet',$vjumlah,'$iarea','$dkn','$eremark','$dentry')");
	}
	function insertglkredit($acckredit,$ikn,$namakredit,$fdebet,$iarea,$vjumlah,$dkn,$eremark)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$eremark=str_replace("'","''",$eremark);
		$namakredit=str_replace("'","''",$namakredit);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ikn','$acckredit','$dkn','$namakredit','$fdebet','$vjumlah','$iarea','$dkn','$eremark','$dentry')");
	}
	function updatekb($ikn,$iarea,$iperiode)
    {
		$this->db->query("update tm_kb set f_posting='t' where i_kb='$ikn' and i_area='$iarea' and i_periode='$iperiode'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
  }
###########end of posting##########
}
?>
