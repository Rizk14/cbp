<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacasupplier($num,$offset)
    {
	    $this->db->select("* from tr_supplier order by i_supplier", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%') order by i_supplier ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
#    function bacaperiode($isupp,$dto,$num,$offset,$cari,$nt,$jt)
    function bacaperiode($isupp,$dto,$cari,$nt,$jt)
    {
      if($isupp!='SEMUA'){
        if($nt!=''){
	        $this->db->select(" distinct(a.*), b.e_supplier_name
                              from tr_supplier b, tm_dtap a
                              left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                              left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                              and e.f_pelunasanap_cancel='f')
                              where a.i_supplier=b.i_supplier and not a.i_dtap isnull
                              and (upper(a.i_dtap) like '%$cari%' or upper(a.i_supplier) like '%$cari%' 
                              or upper(b.e_supplier_name) like '%$cari%')
                              and a.i_supplier='$isupp' and a.d_dtap <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 and a.f_dtap_cancel='f'
                              ORDER BY a.d_dtap",false);#->limit($num,$offset);
        }else{
	        $this->db->select(" distinct(a.*), b.e_supplier_name
                              from tr_supplier b, tm_dtap a
                              left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                              left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                              and e.f_pelunasanap_cancel='f')
                              where a.i_supplier=b.i_supplier and not a.i_dtap isnull
                              and (upper(a.i_dtap) like '%$cari%' or upper(a.i_supplier) like '%$cari%' 
                              or upper(b.e_supplier_name) like '%$cari%')
                              and a.i_supplier='$isupp' and a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 and a.f_dtap_cancel='f'
                              ORDER BY a.d_jatuh_tempo",false);#->limit($num,$offset);
        }
      }else{
        if($nt!=''){
	        $this->db->select(" distinct(a.*), b.e_supplier_name
                              from tr_supplier b, tm_dtap a
                              left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                              left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                              and e.f_pelunasanap_cancel='f')
                              where a.i_supplier=b.i_supplier and not a.i_dtap isnull
                              and (upper(a.i_dtap) like '%$cari%' or upper(a.i_supplier) like '%$cari%' 
                              or upper(b.e_supplier_name) like '%$cari%')
                              and a.d_dtap <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 and a.f_dtap_cancel='f'
                              ORDER BY a.i_supplier, a.d_dtap",false);#->limit($num,$offset);
        }else{
	        $this->db->select(" distinct(a.*), b.e_supplier_name
                              from tr_supplier b, tm_dtap a
                              left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                              left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                              and e.f_pelunasanap_cancel='f')
                              where a.i_supplier=b.i_supplier and not a.i_dtap isnull
                              and (upper(a.i_dtap) like '%$cari%' or upper(a.i_supplier) like '%$cari%' 
                              or upper(b.e_supplier_name) like '%$cari%')
                              and a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 
                              and a.f_dtap_cancel='f'
                              ORDER BY a.i_supplier, a.d_jatuh_tempo",false);#->limit($num,$offset);
        }
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							          where a.i_customer=b.i_customer 
							          and a.f_ttb_tolak='f'
                        and a.f_nota_koreksi='f'
							          and not a.i_nota isnull
							          and (upper(a.i_nota) like '%$cari%' 
							            or upper(a.i_spb) like '%$cari%' 
							            or upper(a.i_customer) like '%$cari%' 
							            or upper(b.e_customer_name) like '%$cari%')
							          and a.i_area='$iarea' and
							          a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_nota <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota,$area)
    {
		$this->db->select(" 	* from tm_nota_item
					inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_nota_item.i_product=tr_product_motif.i_product)
					where i_nota = '$inota' and i_area='$area' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
