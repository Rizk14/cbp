<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$istore,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name, c.v_product_retail from tm_mutasi a, tr_product b, tr_product_price c
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_product=c.i_product and c.i_price_group='00'
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name, c.v_product_retail from tm_mutasi a, tr_product b, tr_product_price c
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_product=c.i_product and c.i_price_group='00'
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }

   function proses($iperiode,$istore,$so)
   {
      if($istore=='AA')
      {
        $iarea='00';
      }
      else
      {
        $iarea=$istore;
      }
      $store=$istore;
		$this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='MTS'
                          and i_area='$istore' for update", false);
      $query = $this->db->get();
		if ($query->num_rows() > 0)
      {
        $this->db->query(" update tm_dgu_no 
                           set e_periode='$iperiode'
                           where i_modul='MTS' and i_area='$istore'", false);
		}
      else
      {
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode) 
                           values ('MTS','$istore','$iperiode')");
      }
      $soAkhir=$so;
      $query = $this->db->query("select * from tm_mutasi_header where i_store='$store' and e_mutasi_periode='$iperiode'");
      $st=$query->row();
      if($query->num_rows()>0)
      {
        $soAwal=$st->i_stockopname_awal;
      }
      if($store=='AA') $loc='01'; else $loc='00';
      $this->db->query(" delete from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store'");
      $query = $this->db->query(" select i_product from tm_stockopname_item where i_store='$store'
                                  and i_stockopname='$soAwal' group by i_product");
      if ($query->num_rows() > 0)
      {
         ### update ke mutasi header
         $emutasiperiode=$iperiode;
         $bldpn=substr($emutasiperiode,4,2)+1;
         if($bldpn==13)
         {
            $perdpn=substr($emutasiperiode,0,4)+1;
            $perdpn=$perdpn.'01';
         }
         else
         {
            $perdpn=substr($emutasiperiode,0,4);
            $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
         }
         $quer=$this->db->query(" select * from tm_mutasi_header
                                  where i_store='$store' and e_mutasi_periode='$emutasiperiode'
                                ",false);
         if($quer->num_rows()>0)
         {
            $que=$this->db->query(" UPDATE tm_mutasi_header
                                    set i_stockopname_akhir='$soAkhir'
                                    where i_store='$store' and e_mutasi_periode='$emutasiperiode'
                                   ",false);
         }
         else
         {
            $que=$this->db->query(" insert into tm_mutasi_header values
                                  ('$store','$emutasiperiode',null,'$soAkhir')
                                  ",false);
         }
         $quer=$this->db->query(" select * from tm_mutasi_header
                                  where i_store='$store' and e_mutasi_periode='$perdpn'
                                ",false);
         if($quer->num_rows()>0)
         {
            $que=$this->db->query(" UPDATE tm_mutasi_header
                                  set i_stockopname_awal='$soAkhir'
                                  where i_store='$store' and e_mutasi_periode='$perdpn'
                                ",false);
         }
         else
         {
            $que=$this->db->query(" insert into tm_mutasi_header values
                                  ('$store','$perdpn','$soAkhir',null)
                                  ",false);
         }
         ### end update ke mutasi header
			foreach($query->result() as $gie)
         {
            $que = $this->db->query("select	area,periode,product, penjualan, pembelian, bbm, bbk, retur 
                                     from vmutasi where periode='$iperiode' and (area='$iarea' or area='$store') and product='$gie->i_product'");
		      if ($que->num_rows() > 0)
            {
			      foreach($que->result() as $vie)
               {
                  if($vie->pembelian==null)$vie->pembelian=0;
                  if($vie->retur==null)$vie->retur=0;
                  if($vie->bbm==null)$vie->bbm=0;
                  if($vie->penjualan==null)$vie->penjualan=0;
                  if($vie->bbk==null)$vie->bbk=0;
                  $blawal=substr($iperiode,4,2)-1;
                  if($blawal==0)
                  {
                     $perawal=substr($iperiode,0,4)-1;
                     $perawal=$perawal.'12';
                  }
                  else
                  {
                     $perawal=substr($iperiode,0,4);
                     $perawal=$perawal.substr($iperiode,4,2)-1;;
                  }
                  $quer=$this->db->query(" select n_stockopname from tm_stockopname_item
                                           where i_stockopname='$soAwal' and i_area='$iarea' and i_product='$vie->product'");

                  if ($quer->num_rows() > 0)
                  {
                     foreach($quer->result() as $ni)
                     {
                        $sawal=$ni->n_stockopname;
                     }
			            $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
			            if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
			            if($store=='AA') $loc='01'; else $loc='00';
                     $opname=0;
                     $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                             where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product'");
                     if ($qur->num_rows() > 0)
                     {
                        foreach($qur->result() as $nu)
                        {
                           $opname=$nu->n_stockopname;
                        }
                     }
                     else
                     {
                        $opname==0;
                     }
			            $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                              i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                              n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                              n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                              values
			                              ('$iperiode','$store','$vie->product','$grade','00',
                                       '$loc','00',$sawal,
			                              $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname)");
                  }
                  else
                  {
                     $sawal=0;
                     $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
                     if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
                     if($store=='AA') $loc='01'; else $loc='00';
                     $opname=0;
                     $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                             where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product'");
                     if ($qur->num_rows() > 0)
                     {
                        foreach($qur->result() as $nu)
                        {
                           $opname=$nu->n_stockopname;
                        }
                     }
                     else
                     {
                        $opname==0;
                     }
                     $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                              n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                              n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                              values
			                              ('$iperiode','$store','$vie->product','$grade','00',
                                       '$loc','00',$sawal,
			                              $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname)");
                  }
                  $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                          from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
                  if ($qu->num_rows() > 0)
                  {
                     foreach($qu->result() as $vei)
                     {
                        $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                        $row   	= $querys->row();
                        if($row->c==$iperiode)
                        {
                           $this->db->query("update tm_ic set 
                                             n_quantity_stock=$vei->n_saldo_akhir
                                             where i_store='$store' and i_product='$vei->i_product'
                                             and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
/*
                           $query 	= $this->db->query("SELECT e_product_name from tr_product where i_product='$vei->i_product'");
                           $row   	= $query->row();
                           $query 	= $this->db->query("SELECT current_timestamp as c");
                           $row   	= $query->row();
                           $now	  = $row->c;
                           $query=$this->db->query(" INSERT INTO tm_ic_trans
                                                      (
                                                        i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                                        i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                                        n_quantity_in, n_quantity_out,
                                                        n_quantity_akhir, n_quantity_awal,i_trans
                                                       )
                                                   VALUES 
                                                      (
                                                        '$vei->i_product','$vei->i_product_grade','$vei->i_product_motif',
                                                        '$store','$loc','00', 
                                                        '$eproductname', 'UPDATE MUTASI', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                                       )
                                                   ",false);
*/
                        }
                     }
                  }
               }
            }
            else
            {
               $blawal=substr($iperiode,4,2)-1;
               if($blawal==0)
               {
                  $perawal=substr($iperiode,0,4)-1;
                  $perawal=$perawal.'12';
               }
               else
               {
                  $perawal=substr($iperiode,0,4);
                  $perawal=$perawal.substr($iperiode,4,2)-1;;
               }
               $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where i_stockopname='$soAwal' and i_area='$iarea' and i_product='$gie->i_product'");

               if ($quer->num_rows() > 0)
               {
                  foreach($quer->result() as $ni)
                  {
                     $stock=$ni->n_stockopname;
                  }
                  if($stock>0)
                  {
                     $sawal=$stock;
                     $akhir=$sawal;
                     if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
                     if($store=='AA') $loc='01'; else $loc='00';
                     $opname=0;
                     $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                             where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$gie->i_product'");
                     if ($qur->num_rows() > 0)
                     {
                        foreach($qur->result() as $nu)
                        {
                           $opname=$nu->n_stockopname;
                        }
                     }
                     else
                     {
                        $opname==0;
                     }
                     $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                              n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                              n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                              values
			                              ('$iperiode','$store','$gie->i_product','$grade','00',
                                       '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname)");
                  }
               }
               else
               {
               $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$gie->i_product'");
               if ($quer->num_rows() > 0)
               {
                  foreach($quer->result() as $ni)
                  {
                     $opname=$ni->n_stockopname;
                  }
                  if($stock>0)
                  {
                     if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
			            if($store=='AA') $loc='01'; else $loc='00';
			            $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                                                       n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                                                       n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                                                       values
			                                                      ('$iperiode','$store','$gie->i_product','$grade','00',
                                                             '$loc','00',0,0,0,0,0,0,0,0,$opname)");
                     }
                  }
               }
            }
         }
      }
      else
      {

      }
      $que = $this->db->query("select	area,periode,product, penjualan, pembelian, bbm, bbk, retur
                            from vmutasi where periode='$iperiode' and (area='$iarea' or area='$store') 
                            and product not in(select i_product from tm_ic where i_store='$store')");
      if ($que->num_rows() > 0)
      {
         foreach($que->result() as $vie)
         {
            if($vie->pembelian==null)$vie->pembelian=0;
            if($vie->retur==null)$vie->retur=0;
            if($vie->bbm==null)$vie->bbm=0;
            if($vie->penjualan==null)$vie->penjualan=0;
            if($vie->bbk==null)$vie->bbk=0;
            $blawal=substr($iperiode,4,2)-1;
            if($blawal==0)
            {
               $perawal=substr($iperiode,0,4)-1;
               $perawal=$perawal.'12';
            }
            else
            {
               $perawal=substr($iperiode,0,4);
               $perawal=$perawal.substr($iperiode,4,2)-1;;
            }
            $quer=$this->db->query("select * from tm_stockopname_item 
                                    where i_stockopname='$soAwal' and i_area='$iarea' and i_product='$vie->product'");
            if ($quer->num_rows() > 0)
            {
               foreach($quer->result() as $ni)
               {
                  $sawal=$ni->n_stockopname;
               }
               $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
               if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
               if($store=='AA') $loc='01'; else $loc='00';
               $opname=0;
               $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                       where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product'");
               if ($qur->num_rows() > 0)
               {
                  foreach($qur->result() as $nu)
                  {
                     $opname=$nu->n_stockopname;
                  }
               }
               else
               {
                  $opname==0;
               }
               $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                 i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                 n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                 n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
                                 values
                                 ('$iperiode','$store','$vie->product','$grade','00',
                                 '$loc','00',$sawal,
                                 $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname)");
            }
            else
            {
               $sawal=0;
               $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
               if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
               if($store=='AA') $loc='01'; else $loc='00';
               $opname=0;
               $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                       where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product'");
               if ($qur->num_rows() > 0)
               {
                  foreach($qur->result() as $nu)
                  {
                     $opname=$nu->n_stockopname;
                  }
               }
               else
               {
                  $opname==0;
               }
               $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                 i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                 n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                 n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
                                 values
                                 ('$iperiode','$store','$vie->product','$grade','00',
                                 '$loc','00',$sawal,
                                 $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname)");
            }
            $qu = $this->db->query("select * from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
            if ($qu->num_rows() > 0)
            {
               foreach($qu->result() as $vei)
               {
                  $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                  $row   	= $querys->row();
                  if($row->c==$iperiode)
                  {
                     $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                                       i_store_location, i_store_locationbin,f_product_active) 
                                       values
                                       ($vei->n_salselect	area,periode,product, penjualan, pembelian, bbm, bbk, retur
                                       from vmutasi where periode='$iperiode' and (area='$iarea' or area='$store') 
                                       and product not in(select i_product from tm_ic where i_store='$storedo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade','$store','$loc','00','t')");
                  }
               }
            }
         }
      }
   }   
  
   function detail($iperiode,$iarea,$iproduct,$iproductmotif,$iproductgrade)
    {
      if($iarea=='00')
      {
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and product='$iproduct'
                  and a.product=b.i_product order by dreff, ireff",false);
      }else{
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and area='$iarea' and product='$iproduct' 
                  and a.product=b.i_product and not a.ireff like 'BBM%' order by dreff, ireff",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct on (a.i_store) a.i_store, b.e_store_name 
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          group by a.i_store, b.e_store_name
                          order by a.i_store", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct on (a.i_store) a.i_store, b.e_store_name 
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                          or a.i_area = '$area4' or a.i_area = '$area5')
                          group by a.i_store, b.e_store_name order a.by i_store", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                 from tr_area a, tr_store b 
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                 from tr_area a, tr_store b
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaso($num,$offset,$area,$peri)
    {
		$this->db->select(" i_stockopname from tm_stockopname 
                        where f_stockopname_cancel='f' and i_area = '$area' and to_char(d_stockopname,'yyyymm')='$peri' 
                        order by i_stockopname desc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
