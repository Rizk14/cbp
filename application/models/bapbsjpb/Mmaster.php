<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct(){
		parent::__construct();
		#$this->CI =& get_instance();
	} 
	function bacasj2($num,$offset){
		$this->db->select("a.i_sjpb, a.d_sjpb, sum(b.n_deliver * b.v_unit_price) as v_sjpb from tm_sjpb a, tm_sjpb_item b
			where 
			a.i_sjpb = b.i_sjpb
			and a.i_area = b.i_area
			and a.i_bapb isnull 
			and a.f_sjpb_cancel = 'f' 
			and a.i_area_entry isnull 
			and a.d_sjpb >= '2019-05-01'
			group by a.i_sjpb, a.d_sjpb
			order by i_sjpb desc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function carisj($cari,$num,$offset){
		$this->db->select("a.i_sjpb, a.d_sjpb, sum(b.n_deliver * b.v_unit_price) as v_sjpb from tm_sjpb a, tm_sjpb_item b
			where 
			a.i_sjpb = b.i_sjpb
			and a.i_area = b.i_area
			and a.i_bapb isnull 
			and a.f_sjpb_cancel = 'f' 
			and a.i_area_entry isnull
			and a.d_sjpb >= '2019-05-01'
			and upper(a.i_sjpb) like '%$cari%' 
			group by a.i_sjpb, a.d_sjpb
			order by i_sjpb desc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	
	function runningnumber($iarea,$thbl){
		$th			= substr($thbl,0,2);
		$this->db->select(" max(substr(i_bapb,11,6)) as max from tm_bapbsjpb where substr(i_bapb,6,2)='$th' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$terakhir=$row->max;
			}
			$nobapb  =$terakhir+1;
			settype($nobapb,"string");
			$a=strlen($nobapb);
			while($a<6){
				$nobapb="0".$nobapb;
				$a=strlen($nobapb);
			}
			$nobapb  ="BAPB-".$thbl."-".$nobapb;
			return $nobapb;
		}else{
			$nobapb  ="000001";
			$nobapb  ="BAPB-".$thbl."-".$nobapb;
			return $nobapb;
		}
	}

	function insertheader($ibapb, $dbapb, $iarea, $vbapb){
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;

		$this->db->set(
			array(
				'i_bapb'			=> $ibapb,
				'd_bapb'			=> $dbapb,
				'i_area'			=> $iarea,
				'v_bapb'      => $vbapb,
				'd_entry'			=> $dentry
			)
		);
		$this->db->insert('tm_bapbsjpb');
	}
	function insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj){
		$this->db->query("DELETE FROM tm_bapbsjpb_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sjpb='$isj'");
		$this->db->set(
			array(
				'i_bapb'	=> $ibapb,
				'i_area'	=> $iarea,
				'i_sjpb'	 	=> $isj,
				'd_bapb' 	=> $dbapb,
				'd_sjpb'	 	=> $dsj,
				'e_remark'=> $eremark,
				'v_sjpb'    => $vsj
			)
		);
		$this->db->insert('tm_bapbsjpb_item');
	}
	function updatesj($ibapb,$isj,$iarea,$dbapb){
		$this->db->set(
			array(
				'i_bapb'	=> $ibapb,	
				'd_bapb' => $dbapb
			)
		);
		$this->db->where('i_sjpb',$isj);
		$this->db->where('i_area',$iarea);
		$this->db->update('tm_sjpb');
	}
	function updatesjb($ibapb,$iarea,$nilaitotal){
		$this->db->set(
			array(
				'v_bapb'	=> $nilaitotal
			)
		);
		$this->db->where('i_bapb',$ibapb);
		$this->db->where('i_area',$iarea);
		$this->db->update('tm_bapbsjpb');
	}
	function updatesjpb($ibapb,$iarea){

		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$d_update  = $row->c;

		$this->db->set(
			array(
				'd_update'	=> $d_update
			)
		);
		$this->db->where('i_bapb',$ibapb);
		$this->db->where('i_area',$iarea);
		$this->db->update('tm_bapbsjpb');
	}
	function baca($ibapb,$iarea)
	{
		$this->db->select(" * from tm_bapbsjpb where i_bapb ='$ibapb' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
	}

	function bacadetail($ibapb,$iarea)
	{
		$this->db->select("* from tm_bapbsjpb_item where i_bapb = '$ibapb' and i_area='$iarea' order by i_sjpb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}

	function deletedetail($ibapb, $iarea, $isj, $daer) 
	{
		$this->db->select(" * from tm_bapbsjpb_item where i_bapb = '$ibapb' and i_area='$iarea' and i_sjpb='$isj'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$this->db->query("update tm_sjpb set i_bapb=null and d_bapb=null WHERE i_sjpb='$isj' and i_area='$iarea'");
			}
		}

		$this->db->query("DELETE FROM tm_bapbsjpb_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sjpb='$isj'");

		$this->db->select(" sum(v_sjpb) as nilai from tm_bapbsjpb_item where i_bapb = '$ibapb'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$nilai = $row->nilai;
				$this->db->query("update tm_bapbsjpb set v_bapb=$nilai WHERE i_bapb = '$ibapb'");
			}
		}
	}

	function deleteitem($ibapb, $iarea) 
	{
		$this->db->select(" * from tm_bapbsjpb_item where i_bapb = '$ibapb' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$isj = $row->i_sjpb;
				$this->db->query("update tm_sjpb set i_bapb=null and d_bapb=null WHERE i_sjpb='$isj' and i_area='$iarea'");
			}
		}
		$this->db->query("DELETE FROM tm_bapbsjpb_item WHERE i_bapb='$ibapb' and i_area='$iarea' ");
	}


}
?>
