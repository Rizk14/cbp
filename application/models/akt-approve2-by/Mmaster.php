<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iarea,$iby) 
    {
		$this->db->query("update tm_by set f_by_cancel='t' WHERE i_by='$iby' and i_area='$iarea'");
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
    $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.i_by, a.d_by, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.v_by, a.v_sisa,
		                    d.e_jenisby_name, a.*
		                    from tm_by a, tr_area b, tr_customer c, tr_jenis_by d
		                    where (upper(i_by) like '%$cari%') and a.i_area = b.i_area and a.i_jenis_by= d.i_jenis_by and a.i_approve2 is null
		                    and a.i_customer = c.i_customer and a.i_area='$iarea' and a.f_by_cancel='f' and a.i_approve1 is not null and
		                    a.i_notapprove is null and a.d_by >= to_date('$dfrom','dd-mm-yyyy') AND a.d_by <= to_date('$dto ','dd-mm-yyyy')
		                    order by a.i_by DESC",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_by, a.d_by, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.v_by, a.v_sisa,
		                    d.e_jenisby_name
		                    from tm_by a, tr_area b, tr_customer c, tr_jenis_by d
		                    where (upper(i_by) like '%$cari%') and a.i_area = b.i_area and a.i_jenis_by= d.i_jenis_by
		                    and a.i_customer = c.i_customer and a.i_area='$iarea' and a.f_by_cancel='f' and
		                    a.d_by >= to_date('$dfrom','dd-mm-yyyy') AND a.d_by <= to_date('$dto ','dd-mm-yyyy')
		                    order by a.d_by DESC",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updateby($iarea,$iby,$vby,$ijenisby,$icustomer,$remark){
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;

      $query=$this->db->query("update tm_by set v_by=$vby, d_update='$dentry', i_jenis_by='$ijenisby', i_customer='$icustomer',
                               e_remark='$remark' where i_by='$iby' and i_area ='$iarea'",false);
    }
    function bacaby($iby,$iarea){
		  $query = $this->db->query("select a.i_by, a.d_by, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.v_by, a.v_sisa,
		                              d.e_jenisby_name, a.*
		                              from tm_by a, tr_area b, tr_customer c, tr_jenis_by d
		                              where a.i_area = b.i_area and a.i_jenis_by= d.i_jenis_by
		                              and a.i_customer = c.i_customer and a.i_area='$iarea' and a.i_by='$iby'");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }    
    }
	  function approve($iby,$iarea,$eapprove,$user){
		  $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;

      $query=$this->db->query("update tm_by set e_approve2='$eapprove', d_approve2='$dentry', i_approve2='$user'
                               where i_by='$iby' and i_area ='$iarea'",false);
    }     
	  function notapprove($iby,$iarea,$eapprove,$user){
		  $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;

      $query=$this->db->query("update tm_by set e_notapprove='$eapprove', d_notapprove='$dentry', i_notapprove='$user'
                               where i_by='$iby' and i_area ='$iarea'",false);
    }     
}
?>
