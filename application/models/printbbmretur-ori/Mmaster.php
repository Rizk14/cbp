<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
		$this->db->query('DELETE FROM tm_ttbretur WHERE i_ttb=\''.$iop.'\'');
		$this->db->query('DELETE FROM tm_ttbretur_item WHERE i_ttb=\''.$iop.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                        a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name 
                        from tm_bbm a, tr_area c, tr_salesman d
					              where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                        and a.i_salesman=d.i_salesman
					              and (upper(a.i_bbm) like '%$cari%' or upper(a.i_salesman) like '%$cari%' 
					              or upper(c.e_area_name) like '%$cari%' or upper(d.e_salesman_name) like '%$cari%') 
                        and (a.i_area='$area1' or a.i_area like '%$area2%'
					              or a.i_area like '%$area3%' or a.i_area like '%$area4%' or a.i_area like '%$area5%') 
                        order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ibbm)
    {
		  $this->db->select(" a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                          a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name, b.i_customer, b.e_customer_name
                          from tm_bbm a, tr_area c, tr_salesman d, tm_ttbretur e, tr_customer b
					                where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                          and a.i_salesman=d.i_salesman
                          and a.i_refference_document=e.i_ttb and a.i_area=e.i_area 
                          and cast(to_char(a.d_refference_document,'yyyy') as numeric)=e.n_ttb_year
                          and e.i_customer=b.i_customer
							            and a.i_bbm = '$ibbm' and a.i_bbm_type='05'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ibbm)
    {
		  $this->db->select(" a.i_bbm, a.i_product, a.i_product_motif, a.i_product_grade, 
							          a.e_product_name, a.n_quantity, a.v_unit_price, 
							          b.e_product_motifname, d.n_quantity,
							          d.i_product1, d.i_product1_motif, d.i_product1_grade, d.i_nota, c.i_ttb as i_ttb, c.n_ttb_year as year, c.i_area as i_area
							          from tm_bbm_item a, tr_product_motif b, tm_ttbretur c, tm_ttbretur_item d
							          where a.i_bbm = '$ibbm' 
							          and a.i_product=b.i_product 
							          and a.i_product_motif=b.i_product_motif 
							          and a.i_product=d.i_product2
							          and a.i_bbm=c.i_bbm
							          and c.i_ttb=d.i_ttb
							          and c.i_area=d.i_area
							          order by a.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
