<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icountry)
    {
		$this->db->select('i_country, e_country_name')->from('tr_country')->where('i_country', $icountry);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icountry, $ecountryname)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;	
    	$this->db->set(
    		array(
    			'i_country' => $icountry,
    			'e_country_name' => $ecountryname,
			'd_country_entry' => $dentry
    		)
    	);
    	
    	$this->db->insert('tr_country');
		#redirect('country/cform/');
    }
    function update($icountry, $ecountryname)
    {
	 	$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;	
    	$data = array(
               'i_country' => $icountry,
               'e_country_name' => $ecountryname,
	       'd_country_update' => $dupdate
            );
		$this->db->where('i_country', $icountry);
		$this->db->update('tr_country', $data); 
		#redirect('country/cform/');
    }
	
    public function delete($icountry) 
    {
		$this->db->query('DELETE FROM tr_country WHERE i_country=\''.$icountry.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_country, e_country_name from tr_country order by i_country", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
