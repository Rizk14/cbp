<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ikum,$nkumyear,$iarea) 
    {
//		$this->db->query("DELETE FROM tm_kum WHERE i_kum='$ikum' and n_kum_year='$nkumyear' and i_area='$iarea'");
			$this->db->query(" update tm_kum set f_kum_cancel='t', d_update=now() WHERE i_kum='$ikum' and n_kum_year='$nkumyear' and i_area='$iarea' ");
    }
    function bacasemua($num,$offset)
    {
		$this->db->select(" 
	         a.i_customer AS icustomer, a.d_kum AS dkum, a.f_kum_cancel AS fkumcancel, a.i_kum AS ikum, a.d_kum AS dkum, 
				   a.e_bank_name AS ebankname, c.e_customer_name AS ecustomername, e.e_customer_setor AS ecustomersetor,
				   a.v_jumlah AS vjumlah, a.v_sisa AS vsisa, a.f_close AS fclose, a.n_kum_year AS nkumyear, d.i_area AS iarea,
				   tm_dt.i_dt AS idt, tm_dt.d_dt AS ddt, tm_pelunasan.i_pelunasan AS ipelunasan, tm_pelunasan.i_giro AS igiro
				from tm_kum a
				   left join tr_customer c on(a.i_customer=c.i_customer)
				   left join tr_area d on(a.i_area=d.i_area)
				   left join tr_customer_owner e on(a.i_customer=e.i_customer)
				   left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro)
				   left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
				where 
				   (  (  tm_pelunasan.i_jenis_bayar!='02' and 
				         tm_pelunasan.i_jenis_bayar!='01' and 
				         tm_pelunasan.i_jenis_bayar!='04' and 
				         tm_pelunasan.i_jenis_bayar='03'
				      ) 
				     or 
				      (  (tm_pelunasan.i_jenis_bayar='03') is null
				      )
				       
				   )
				order by a.n_kum_year desc, a.i_kum", false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
	
		$this->db->select(" a.i_customer AS icustomer, 
					a.d_kum AS dkum, 
					a.f_kum_cancel AS fkumcancel, 
					a.i_kum AS ikum, 
					a.d_kum AS dkum, 
					a.e_bank_name AS ebankname,
					c.e_customer_name AS ecustomername,
					e.e_customer_setor AS ecustomersetor,
					a.v_jumlah AS vjumlah,
					a.v_sisa AS vsisa,
					a.f_close AS fclose,
					a.n_kum_year AS nkumyear,
					d.i_area AS iarea,
					tm_dt.i_dt AS idt, 
					tm_dt.d_dt AS ddt, 
					tm_pelunasan.i_pelunasan AS ipelunasan, 
					tm_pelunasan.i_giro AS igiro
									
					from tm_kum a
				
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							
					where upper(a.i_kum) like '%$cari%' and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null))
														
					order by a.n_kum_year desc, a.i_kum", false)->limit($num,$offset);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    /*function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {				
			$teksquery=" distinct a.i_customer AS icustomer, 
					a.d_kum AS dkum, 
					a.f_kum_cancel AS fkumcancel, 
					a.i_kum AS ikum, 
					a.d_kum AS dkum, 
					a.e_bank_name AS ebankname,
					c.e_customer_name AS ecustomername,
					e.e_customer_setor AS ecustomersetor,
					a.e_remark AS eremark,
					a.v_jumlah AS vjumlah,
					a.v_sisa AS vsisa,
					a.f_close AS fclose,
					a.n_kum_year AS nkumyear,
					d.i_area AS iarea,
					g.i_dt AS idt, 
					g.d_dt AS ddt, 
					f.i_giro AS igiro
					from tm_kum a
					left join tr_customer c on(a.i_customer=c.i_customer)
          left join tr_area d on(a.i_area=d.i_area)
          left join tr_customer_owner e on(a.i_customer=e.i_customer)
          left join tm_pelunasan f on(a.i_kum=f.i_giro and a.d_kum=f.d_giro and f.f_pelunasan_cancel='f' 
          and f.f_giro_tolak='f' and f.f_giro_batal='f' and a.i_area=f.i_area)
          left join tm_dt g on(f.i_dt=g.i_dt and f.d_dt=g.d_dt and f.i_area=g.i_area 
          and f.f_pelunasan_cancel='f' and f.f_giro_tolak='f' and f.f_giro_batal='f' and g.i_area=a.i_area)
					where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					((f.i_jenis_bayar!='02' and 
					f.i_jenis_bayar!='01' and 
					f.i_jenis_bayar!='04' and 
					f.i_jenis_bayar='03') or ((f.i_jenis_bayar='03') is null)) and 

					a.i_area='$iarea' and
					(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f'
					ORDER BY dkum, icustomer, ikum ";
			$this->db->select($teksquery,false)->limit($num,$offset);

			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }*/
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {				
			$teksquery=" distinct a.i_customer as icustomer, a.i_kum as ikum, a.d_kum as dkum, a.e_bank_name as ebankname,
                  a.i_area as iarea, a.n_kum_year as nkumyear, a.e_remark as eremark, a.f_kum_cancel as fkumcancel,
                  b.e_customer_name as ecustomername, c.i_customer_groupbayar, d.i_dt as idt, d.d_dt as ddt, d.i_giro as igiro,
                  d.i_customer, e.e_customer_setor as ecustomersetor, a.v_jumlah as vjumlah, a.v_sisa as vsisa, a.f_close as fclose
                  from tm_kum a
                  left join tr_customer b on (a.i_customer=b.i_customer)
                  left join tr_customer_groupbayar c on (a.i_customer=c.i_customer_groupbayar and b.i_customer=c.i_customer_groupbayar)
                  left join tm_pelunasan d on ((a.i_customer=d.i_customer or (a.i_customer=d.i_customer or a.i_area=d.i_area))
		                                            and a.i_kum=d.i_giro and a.d_kum=d.d_giro and d.f_pelunasan_cancel='f')
                  left join tr_customer_owner e on (a.i_customer=e.i_customer)
                  where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'  ";
					        if(!is_numeric($cari)){
						        $teksquery=$teksquery.")";
					        }else{
						        $teksquery=$teksquery."or a.v_jumlah=$cari)";

					        }
					        $teksquery=$teksquery." and a.f_kum_cancel='f'
                  and ((d.i_jenis_bayar!='02' and 
                  d.i_jenis_bayar!='01' and 
                  d.i_jenis_bayar!='04' and 
                  d.i_jenis_bayar='03') or ((d.i_jenis_bayar='03') is null)) and";
                  if($iarea=='NA'){
						$teksquery=$teksquery." a.i_area like '%%' ";
					}else{
						$teksquery=$teksquery." a.i_area='$iarea' ";
					}
                  $teksquery=$teksquery."and (a.d_kum >= to_date('$dfrom','dd-mm-yyyy')
                  and a.d_kum <= to_date('$dto','dd-mm-yyyy'))
                  ORDER BY dkum, icustomer, ikum ";
			$this->db->select($teksquery,false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();

			}
    }
    /*function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {					
			$teksquery=" distinct a.i_customer AS icustomer, 
					a.d_kum AS dkum, 
					a.f_kum_cancel AS fkumcancel, 
					a.i_kum AS ikum, 
					a.d_kum AS dkum, 
					a.e_bank_name AS ebankname,
					c.e_customer_name AS ecustomername,
					e.e_customer_setor AS ecustomersetor,
					a.e_remark AS eremark,
					a.v_jumlah AS vjumlah,
					a.v_sisa AS vsisa,
					a.f_close AS fclose,
					a.n_kum_year AS nkumyear,
					d.i_area AS iarea,
					tm_dt.i_dt AS idt, 
					tm_dt.d_dt AS ddt, 
					tm_pelunasan.i_giro AS igiro
					from tm_kum a
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.d_kum=tm_pelunasan.d_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
					where (upper(a.i_kum) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and 
					a.i_area='$iarea' and
					(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f'
					ORDER BY dkum, icustomer, ikum ";
					$this->db->select($teksquery,false)->limit($num,$offset); 

		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }*/
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {				
			$teksquery=" distinct a.i_customer as icustomer, a.i_kum as ikum, a.d_kum as dkum, a.e_bank_name as ebankname,
                  a.i_area as iarea, a.n_kum_year as nkumyear, a.e_remark as eremark, a.f_kum_cancel as fkumcancel,
                  b.e_customer_name as ecustomername, c.i_customer_groupbayar, d.i_dt as idt, d.d_dt as ddt, d.i_giro as igiro,
                  d.i_customer, e.e_customer_setor as ecustomersetor, a.v_jumlah as vjumlah, a.v_sisa as vsisa, a.f_close as fclose
                  from tm_kum a
                  left join tr_customer b on (a.i_customer=b.i_customer)
                  left join tr_customer_groupbayar c on (a.i_customer=c.i_customer_groupbayar and b.i_customer=c.i_customer_groupbayar)
                  left join tm_pelunasan d on ((a.i_customer=d.i_customer or (a.i_customer=d.i_customer or a.i_area=d.i_area))
		                                            and a.i_kum=d.i_giro and a.d_kum=d.d_giro and d.f_pelunasan_cancel='f')
                  left join tr_customer_owner e on (a.i_customer=e.i_customer)
                  where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'  ";
					        if(!is_numeric($cari)){
						        $teksquery=$teksquery.")";
					        }else{
						        $teksquery=$teksquery."or a.v_jumlah=$cari)";
					        }
					        $teksquery=$teksquery." and a.f_kum_cancel='f'
                  and ((d.i_jenis_bayar!='02' and 
                  d.i_jenis_bayar!='01' and 
                  d.i_jenis_bayar!='04' and 
                  d.i_jenis_bayar='03') or ((d.i_jenis_bayar='03') is null))
                  and a.i_area='$iarea'
                  and(a.d_kum >= to_date('$dfrom','dd-mm-yyyy')
                  and a.d_kum <= to_date('$dto','dd-mm-yyyy'))
                  ORDER BY dkum, icustomer, ikum ";
			$this->db->select($teksquery,false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
