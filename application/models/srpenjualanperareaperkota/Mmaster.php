<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
      $this->db->select("	e_supplier_name, i_area, e_area_name, e_city_name, bln, sum(target) as target, sum(qspb) as qspb, 
      sum(vspb) as vspb, sum(qnota) as qnota, sum(vnota) as vnota from(
      select a.e_supplier_name, a.i_area, e_area_name, e_city_name, bln, 0 as target, 0 as qspb, 0 as vspb, sum(qnota) as qnota, 
      sum(vnota) as vnota from(
      select c.e_supplier_name, a.i_area, b.e_area_name, e.e_city_name, to_char(a.d_nota,'yyyymm') AS bln, 0 as qspb, 0 as vspb, 
      sum (f.n_deliver) as qnota, 
      sum(f.n_deliver*f.v_unit_price)-(a.v_nota_discount*(sum(f.n_deliver*f.v_unit_price)/ a.v_nota_gross)) as vnota
      from tm_nota a, tr_area b, tr_customer d, tr_city e, tm_nota_item f, tr_product g, tr_supplier c
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and b.f_area_real='t'
      and a.i_area=b.i_area and a.i_customer=d.i_customer and d.i_area=e.i_area and d.i_city=e.i_city
      and g.i_supplier in ('SP002','SP004','SP021') and f.i_product=g.i_product and g.i_supplier=c.i_supplier and a.i_nota=f.i_nota 
      and a.i_area=f.i_area
      GROUP BY c.e_supplier_name, a.i_area, b.e_area_name, e.e_city_name, to_char(a.d_nota,'yyyymm'), a.v_nota_gross, a.v_nota_discount
      )as a
      GROUP BY a.e_supplier_name, a.i_area, a.e_area_name, a.e_city_name, bln
      union all
      select b.e_supplier_name, b.i_area, b.e_area_name, b.e_city_name, b.bln, 0 as target, sum(qspb) as qspb, sum(vspb) as vspb, 
      0 as qnota , 
      0 as vnota from(
      select  c.e_supplier_name, a.i_area, b.e_area_name, e.e_city_name, to_char(a.d_spb,'yyyymm') AS bln, sum(f.n_order) as qspb, 
      sum(f.n_order*f.v_unit_price)-(a.v_spb_discounttotal*(sum(f.n_order*f.v_unit_price)/ a.v_spb)) as vspb, 0 as qnota , 0 as vnota 
      from tm_spb a, tr_area b, tr_customer d, tr_city e, tm_spb_item f, tr_product g, tr_supplier c
      where to_char(a.d_spb,'yyyymm')='$iperiode' and f_spb_cancel='f' and b.f_area_real='t' and a.i_area=b.i_area 
      and a.i_customer=d.i_customer 
      and d.i_area=e.i_area 
      and d.i_city=e.i_city
      and g.i_supplier in ('SP002','SP004','SP021') and f.i_product=g.i_product and g.i_supplier=c.i_supplier and a.i_spb=f.i_spb 
      and a.i_area=f.i_area
      GROUP BY c.e_supplier_name, a.i_area, b.e_area_name, e.e_city_name, to_char(a.d_spb,'yyyymm'), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_area, b.e_area_name, b.e_city_name, bln
      )as c
      GROUP BY c.e_supplier_name, c.i_area, c.e_area_name, c.e_city_name, bln
      ORDER BY c.e_supplier_name, c.i_area, c.e_area_name, c.e_city_name, bln
      ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' 
				                    or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
				                    order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
