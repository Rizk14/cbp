<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasupplier($num,$offset)
    {
		  $this->db->select(" * from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

	  function carisupplier($cari,$num,$offset)
    {
		  $this->db->select(" * from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%') order by i_supplier ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiode($isupplier,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_supplier_name, c.e_jenis_bayarname 
                          from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                          where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                          and (upper(a.i_pelunasanap) like '%$cari%' 
                          or upper(a.i_supplier) like '%$cari%' 
                          or upper(b.e_supplier_name) like '%$cari%')
                          and a.i_supplier='$isupplier' 
                          ORDER BY a.i_pelunasanap ",false)->limit($num,$offset);
  #   						and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiodeperpages($isupplier,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_supplier_name, c.e_jenis_bayarname 
                          from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                          where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                          and (upper(a.i_pelunasanap) like '%$cari%' 
                          or upper(a.i_supplier) like '%$cari%' 
                          or upper(b.e_supplier_name) like '%$cari%')
                          and a.i_supplier='$isupplier' 
                          ORDER BY a.i_pelunasanap ",false)->limit($num,$offset);
  #             and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function cariperiode($iarea,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_supplier_name, c.e_jenis_bayarname 
                          from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                          where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                          and (upper(a.i_pelunasanap) like '%$cari%' 
                          or upper(a.i_supplier) like '%$cari%' 
                          or upper(b.e_supplier_name) like '%$cari%')
                          and a.i_supplier='$isupplier' 
                          ORDER BY a.i_pelunasanap ",false)->limit($num,$offset);
  #              and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
