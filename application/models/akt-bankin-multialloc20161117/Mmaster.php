<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
   function insertheader($ialokasi,$ikbank,$iarea,$icustomer,$dbank,$dalokasi,$ebankname, $vjumlah,$vlebih,$icoabank,$igiro)
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_alokasi 
                      (i_alokasi,i_kbank,i_area,i_customer,d_alokasi,e_bank_name,v_jumlah,v_lebih,d_entry,i_coa_bank,i_giro)
                      values
                      ('$ialokasi','$ikbank','$iarea','$icustomer','$dalokasi','$ebankname',$vjumlah,
                        $vlebih,'$dentry','$icoabank','$igiro')");
  }
   function deleteheader(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->query("delete from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
   }
  function updatebank($ikbank,$icoabank,$iarea,$pengurang)
    {
      $this->db->select(" v_sisa from tm_kbank where i_kbank='$ikbank' and i_coa_bank='$icoabank'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){
          $sisa=$xx->v_sisa-$pengurang;
          if($sisa<0){
            return false;
            break;
          }else{
            $this->db->query("update tm_kbank set v_sisa=v_sisa-$pengurang where i_kbank='$ikbank' and i_coa_bank='$icoabank'");
            return true;
          }
        }
      }else{
        return false;
      }
    }
  function updatesaldo($group,$icustomer,$pengurang)
    {
      $this->db->query("update tr_customer_groupar set v_saldo=v_saldo-$pengurang
                        where i_customer='$icustomer' and i_customer_groupar='$group'");
    }

   function insertdetail($ialokasi,$ikbank,$iarea,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$icoabank)
  {
    $tmp=$this->db->query(" select i_alokasi from tm_alokasi_item
                            where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                            and i_coa_bank='$icoabank'", false);
    if($tmp->num_rows()>0){
      $this->db->query("update tm_alokasi_item set d_nota='$dnota',v_jumlah=$vjumlah,v_sisa=$vsisa,n_item_no=$i,
                        e_remark='$eremark'
                        where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota' and i_kbank='$ikbank' 
                        and i_coa_bank='$icoabank'");
    }else{
        $this->db->query("insert into tm_alokasi_item
                      ( i_alokasi,i_kbank,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark,i_coa_bank)
                      values
                      ('$ialokasi','$ikbank','$iarea','$inota','$dnota',$vjumlah,$vsisa,$i,'$eremark','$icoabank')");
    }
  }
   function updatenota($inota,$vsisa)
    {
      $this->db->select(" v_sisa from tm_nota where i_nota='$inota'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){
          $sisa=$xx->v_sisa-$vsisa;
          if($sisa<0){
            return false;
            break;
          }else{
            $this->db->query("update tm_nota set v_sisa=v_sisa-$vsisa where i_nota='$inota'");
            return true;
          }
        }
      }else{
        return false;
      }
    }
   function deletedetail($ipl,$idt,$iarea,$inota,$ddt)
    {
      $this->db->query("DELETE FROM tm_pelunasan_item WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'
                      and i_nota='$inota' and d_dt='$ddt'");
    }
   function bacacustomer($iarea,$num,$offset){
      $this->db->select(" i_customer, e_customer_name, e_customer_address, e_customer_city from tr_customer where i_area = '$iarea' 
                          order by i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caricustomer($cari,$iarea,$num,$offset){
      if($cari=='sikasep'){
        $this->db->select(" i_customer, e_customer_name, e_customer_address, e_customer_city from tr_customer where i_area = '$iarea'
                            order by i_customer ",FALSE)->limit($num,$offset);      
      }else{
        $this->db->select(" i_customer, e_customer_name, e_customer_address, e_customer_city from tr_customer where i_area = '$iarea'
                            and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')
                            order by i_customer ",FALSE)->limit($num,$offset);      
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function runningnumberpl($iarea,$thbl){
      $th   = substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
      $this->db->select(" n_modul_no as max from tm_dgu_no
                        where i_modul='BAL'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea' for update", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
           $terakhir=$row->max;
         }
         $noal  =$terakhir+1;
         $this->db->query(" update tm_dgu_no
                          set n_modul_no=$noal
                          where i_modul='BAL'
                          and substr(e_periode,1,4)='$th'
                          and i_area='$iarea'", false);
         settype($noal,"string");
         $a=strlen($noal);
         while($a<5){
           $noal="0".$noal;
           $a=strlen($noal);
         }
         $noal  ="AL-".$thbl."-".$noal;
         return $noal;
      }else{
         $noal  ="00001";
         $noal  ="AL-".$thbl."-".$noal;
         $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                         values ('BAL','$iarea','$asal',1)");


         return $noal;
      }
    }
   function bacapelunasan($icustomer,$iarea,$num,$offset,$group){
      $this->db->select("a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                         a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
                         from tm_pelunasan_lebih a, tr_customer_groupar b
                         where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                         and a.v_lebih>0 and a.f_pelunasan_cancel='f'
                         group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caripelunasan($cari,$icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
                and (upper(a.i_pelunasan) like '%$cari%') 
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
  function bacapl($iarea,$ialokasi,$ikbank,$icoabank){
      $xkbank=strtoupper($ikbank);
      $xalokasi=strtoupper($ialokasi);
      $this->db->select("a.*, b.e_area_name, c.e_customer_name, e.d_bank, c.e_customer_address, c.e_customer_city
                         from tm_alokasi a
                         inner join tr_area b on (a.i_area=b.i_area)
                         inner join tr_customer c on (a.i_customer=c.i_customer)
                         inner join tm_kbank e on (a.i_kbank=e.i_kbank and a.i_area=e.i_area and a.i_coa_bank=e.i_coa_bank)
                         where
                         upper(a.i_kbank)='$xkbank' and upper(a.i_alokasi)='$xalokasi' and upper(a.i_area)='$iarea'
                         and a.i_coa_bank='$icoabank'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function sisa($iarea,$ialokasi,$ikbank){
      $sisa=0;
      $this->db->select(" sum(v_sisa)as sisa from tm_kbank where i_area='$iarea' and i_kbank='$ikbank'",FALSE);
      $query = $this->db->get();
      foreach($query->result() as $isi){
         $sisa=$isi->sisa;
      }
      return $sisa;
   }
   function bulat($iarea,$ialokasi,$ikbank){
      $bulat=0;
      $reff=$ialokasi.'|'.$ikbank;
      $this->db->select(" sum(v_mutasi_debet) as bulat from tm_general_ledger where i_refference='$reff' and i_area='$iarea'",FALSE);
      $query = $this->db->get();
      if($query->num_rows()>0){
        foreach($query->result() as $isi){
           $bulat=$isi->bulat;
        }
      }
      return $bulat;
   }
   function bacadetailpl($iarea,$ialokasi,$ikbank){
      $this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_nota_netto as v_nota from tm_alokasi_item a
                          inner join tm_nota b on (a.i_nota=b.i_nota)
                          where a.i_alokasi = '$ialokasi'
                          and a.i_area='$iarea'
                          and a.i_kbank='$ikbank'
                          order by a.i_alokasi,a.i_area ",FALSE);
#
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacanota($cari,$iarea,$icustomer,$num,$offset,$group){
      $this->db->select("c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                         from tr_customer_groupbayar b, tm_nota c
                         where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.v_sisa>0
                         and not c.i_nota is null and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')
                         and c.f_nota_cancel='f'
                         order by c.i_nota ",FALSE)->limit($num,$offset);
/*
      $this->db->select("c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                         from tr_customer_groupbayar b, tm_nota c
                         where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.i_area='$iarea' and c.v_sisa>0
                         and not c.i_nota is null and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')
                         and c.f_nota_cancel='f'
                         order by c.i_nota ",FALSE)->limit($num,$offset);
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carinota($cari,$iarea,$icustomer,$num,$offset,$group){
      $this->db->select("sc.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                         from tr_customer_groupbayar b, tm_nota c
                         where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.v_sisa>0
                         and not c.i_nota is null and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')
                         and c.f_nota_cancel='f'
                         order by c.i_nota ",FALSE)->limit($num,$offset);
/*
      $this->db->select("sc.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                         from tr_customer_groupbayar b, tm_nota c
                         where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.i_area='$iarea' and c.v_sisa>0
                         and not c.i_nota is null and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')
                         and c.f_nota_cancel='f'
                         order by c.i_nota ",FALSE)->limit($num,$offset);
*/
      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
   {
      $sm=PiutangDagangSementara;
      $this->db->select(" a.i_kbank, a.i_area, a.d_bank, a.v_bank, b.e_area_name, c.e_bank_name, a.v_sisa, a.i_coa_bank, a.e_description
                          from tm_kbank a, tr_area b, tr_bank c
                          where a.i_area=b.i_area and a.f_kbank_cancel='false'
                          and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') and a.f_debet=false
                          and a.d_bank <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0
                          and upper(a.i_area)='$iarea' and a.i_coa_bank=c.i_coa
                          and (upper(a.i_kbank) like '%$cari%') and a.i_coa='$sm'
                          order by a.i_kbank, a.d_bank",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
########## Posting ###########
	function jenisbayar($ipl,$iarea)
    {
		$this->db->select(" i_jenis_bayar from tm_alokasi where i_alokasi='$ipl' and i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_jenis_bayar;
			}
			return $xxx;
		}
    }
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ipelunasan,$iarea,$egirodescription,$fclose,$dbukti )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry,i_coa_bank)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry','$icoabank')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry,i_coa_bank)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry','$icoabank')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjumlah,'$iarea','$dbukti','$egirodescription','$dentry','$icoabank')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription,$icoabank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjumlah','$iarea','$dbukti','$egirodescription','$dentry','$icoabank')");
	}
	function updatepelunasan($ipl,$iarea,$idt)
    {
		$this->db->query("update tm_pelunasan set f_posting='t' where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
########## End of Posting ###########
   function bacagiro($icustomer,$iarea,$num,$offset,$group,$dbukti){
     $this->db->select("a.* from (
                        select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
                        where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                        and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                        and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
                        union all
                        select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
                        tm_rtunai_item d
                        where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                        and a.i_tunai=d.i_tunai and a.d_tunai<='$dbukti'
                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                        union all
                        select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
                        where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and d_kum<='$dbukti'
                        )as a
                        order by a.tgl, a.bayar ",FALSE)->limit($num,$offset);
/*
and not a.i_giro||a.i_area in (select i_giro||i_area from tm_alokasi where i_area='$iarea')
and not a.i_tunai||a.i_area in (select i_giro||i_area from tm_alokasi where i_area='$iarea')
and not a.i_kum||a.i_area in (select i_giro||i_area from tm_alokasi where i_area='$iarea')
*/
    $query = $this->db->get();
    if ($query->num_rows() > 0){
       return $query->result();
    }
  }
}
?>
