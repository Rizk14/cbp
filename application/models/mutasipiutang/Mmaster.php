<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
        
    function bacaperiode($iperiode)
    {

      $this->db->select(" b.i_area, c.e_area_name, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.penjualan) as penjualan, sum(b.pembayaran) as pembayaran, 
							sum(b.retur) as retur, sum(b.pembulatan) as pembulatan, sum(b.adminbank) as adminbank, sum(b.lainlain) as lainlain from (
								select x.i_area, 0 as v_saldo_awal, sum(x.penjualan) as penjualan, sum(x.pembayaran) as pembayaran, 
								sum(x.retur) as retur, sum(x.pembulatan) as pembulatan, sum(x.adminbank) as adminbank, sum(x.lainlain) as lainlain from(		
									
									-- ini nota / penjualan --
									select a.i_area, sum(a.v_nota_netto) as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_nota a
									where to_char(a.d_nota,'yyyymm') = '$iperiode'
									and a.f_nota_cancel = 'false'
									group by a.i_area

									UNION ALL
									-- ini alokasi bank masuk/pembayaran --- 
									select a.i_area, 0 as penjualan, sum(b.v_sisa) as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasi a, tm_alokasi_item b
									where a.i_alokasi = b.i_alokasi and a.i_kbank = b.i_kbank and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false'
									group by a.i_area

									UNION ALL
									-- ini KN RETUR -- 
									select a.i_area,  0 as penjualan, 0 as pembayaran, sum(b.v_sisa) as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasiknr a, tm_alokasiknr_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') >= '$iperiode'
									and a.f_alokasi_cancel = 'false' 
									group by a.i_area
									
									UNION ALL
									-- ini KN NON RETUR PEMBULATAN -- 
									select a.i_area,  0 as penjualan, 0 as pembayaran, 0 as retur, sum(b.v_sisa) as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar = '10'
									group by a.i_area

									UNION ALL
									-- ini KN Non RETUR ADMIN BANK --
									select a.i_area, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, sum(b.v_sisa) as adminbank, 0 as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar = '08'
									group by a.i_area

									UNION ALL
									-- ini kn non retur LAIN-LAIN -- 
									select a.i_area, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank,sum(b.v_sisa) as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar not in('08','10')
									group by a.i_area
								) as x
								group by x.i_area

								UNION ALL 

								-- ambil saldo awal--
								select a.i_area, a.v_saldo_awal, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
								from tm_mutasi_piutang a
								where e_mutasi_periode = '$iperiode'
								--group by a.i_area, a.e_mutasi_periode, a.v_saldo_awal

						) as b join tr_area c on(b.i_area = c.i_area)
						group by b.i_area, c.e_area_name
						order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function hitung($iperiode)
    {
		$this->db->select(" b.i_area, c.e_area_name, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.penjualan) as penjualan, sum(b.pembayaran) as pembayaran, 
							sum(b.retur) as retur, sum(b.pembulatan) as pembulatan, sum(b.adminbank) as adminbank, sum(b.lainlain) as lainlain, 
							sum(b.v_saldo_awal)+sum(b.penjualan)-sum(b.pembayaran)-sum(b.retur)-sum(b.pembulatan)-sum(b.adminbank)-sum(b.lainlain) as saldoakhir
							from (
								select x.i_area, 0 as v_saldo_awal, sum(x.penjualan) as penjualan, sum(x.pembayaran) as pembayaran, 
								sum(x.retur) as retur, sum(x.pembulatan) as pembulatan, sum(x.adminbank) as adminbank, sum(x.lainlain) as lainlain
								from(		
								
									-- ini nota / penjualan --
									select a.i_area, sum(a.v_nota_netto) as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_nota a
									where to_char(a.d_nota,'yyyymm') = '$iperiode'
									and a.f_nota_cancel = 'false'
									group by a.i_area

									UNION ALL
									-- ini alokasi bank masuk/pembayaran --- 
									select a.i_area, 0 as penjualan, sum(b.v_sisa) as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasi a, tm_alokasi_item b
									where a.i_alokasi = b.i_alokasi and a.i_kbank = b.i_kbank and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false'
									group by a.i_area

									UNION ALL
									-- ini KN RETUR -- 
									select a.i_area,  0 as penjualan, 0 as pembayaran, sum(b.v_sisa) as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasiknr a, tm_alokasiknr_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') >= '$iperiode'
									and a.f_alokasi_cancel = 'false' 
									group by a.i_area
									
									UNION ALL
									-- ini KN NON RETUR PEMBULATAN -- 
									select a.i_area,  0 as penjualan, 0 as pembayaran, 0 as retur, sum(b.v_sisa) as pembulatan, 0 as adminbank, 0 as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm-dd') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar = '10'
									group by a.i_area

									UNION ALL
									-- ini KN Non RETUR ADMIN BANK --
									select a.i_area, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, sum(b.v_sisa) as adminbank, 0 as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar = '08'
									group by a.i_area

									UNION ALL
									-- ini kn non retur LAIN-LAIN -- 
									select a.i_area, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank,sum(b.v_sisa) as lainlain
									from tm_alokasikn a, tm_alokasikn_item b
									where a.i_alokasi = b.i_alokasi and a.i_kn = b.i_kn and a.i_kn = b.i_kn and a.i_area = b.i_area
									and to_char(a.d_alokasi,'yyyymm') = '$iperiode'
									and a.f_alokasi_cancel = 'false' and a.i_jenis_bayar not in('08','10')
									group by a.i_area
								) as x
								group by x.i_area

							UNION ALL 

							-- ambil saldo awal--
							select a.i_area, a.v_saldo_awal, 0 as penjualan, 0 as pembayaran, 0 as retur, 0 as pembulatan, 0 as adminbank, 0 as lainlain
							from tm_mutasi_piutang a
							where e_mutasi_periode = '$iperiode'
							--group by a.i_area, a.e_mutasi_periode, a.v_saldo_awal

							) as b join tr_area c on(b.i_area = c.i_area)
							group by b.i_area, c.e_area_name
							order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }



    function insertmutasi($iarea,$saldoawal,$periode){
    	$this->i_area 			= $iarea;
    	$this->e_mutasi_periode = $periode;
    	$this->v_saldo_awal		= $saldoawal;

    	$this->db->insert('tm_mutasi_piutang',$this);
    }
    function updatemutasi($iarea,$saldoawal,$periode){
    	$this->v_saldo_awal = $saldoawal;
    	
    	$this->db->insert('tm_mutasi_piutang',$this,array('i_area' => $iarea, 'e_mutasi_periode' => $periode));
    }

    public function lihatperiode($periode)
	{   
	    $query = $this->db->get_where('tm_mutasi_piutang', array('e_mutasi_periode' => $periode));
	    if($query->num_rows()>0)
	    {
	      return 'true';
	    }
	    else
	    {
	      return 'false';
	    }
	}
}
?>
