<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$num,$offset,$cari)
    {
		    $this->db->select(" x.i_product, x.e_product_name, sum(x.n_saldo_awal) as saldo_awal, sum(x.n_deliver) as saldo_masuk, (sum(x.n_saldo_awal) - sum(x.n_deliver)) as saldo_akhir from(
				select a.i_product, b.e_product_name, a.n_saldo_awal, 0 as n_deliver
				from tm_saldoawal_fc a, tr_product b
				where a.i_product=b.i_product
				and a.e_periode='$iperiode'
				union all
				select a.i_product, a.e_product_name, 0 as n_saldo_awal, sum(a.n_deliver) as n_deliver
				from tm_dofc_item a, tm_dofc b
				where a.i_do = b.i_do and a.i_supplier = b.i_supplier
				and a.i_op = b.i_op
				and to_char(a.d_do,'YYYYMM')='$iperiode'
				and b.f_do_cancel = 'f'
				group by a.i_product, a.e_product_name
				) as x
				group by x.i_product, x.e_product_name
				order by x.i_product asc",false);//->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
      $query->free_result();
    }
    function detail($istorelocation,$iperiode,$iarea,$iproduct)
    {
		 $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name, a.urut,
	                        sum(a.in) as in, sum(a.out) as out, sum(a.git) as git, sum(a.gitpenjualan) as gitpenjualan
	                        FROM tr_product b, vmutasidetail a
	                        left join tm_nota_item c on c.i_sj=a.ireff and a.product=c.i_product
	                        left join tm_spb d on d.i_sj=c.i_sj
	                        left join tr_customer e on d.i_customer=e.i_customer 
	                        WHERE 
	                          b.i_product = a.product and a.loc='$istorelocation' AND
	                          a.periode='$iperiode' AND a.area='$iarea' AND a.product='$iproduct' 
	                        group by b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product,
	                                 e.e_customer_name, a.urut
	                        order by dreff, urut, ireff",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }

  function bacaarea($num,$offset,$iuser)
  {
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_area=b.i_store and b.i_store=c.i_store
                        and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
                        and not a.i_store in ('AA','PB') and c.i_store_location='00'
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
# and i_store_location='00'
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function cariarea($cari,$num,$offset,$iuser)
  {
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                         from tr_area a, tr_store b, tr_store_location c
                         where  a.i_area=b.i_store and b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						 		and (i_area in( select i_area from tm_user_area where i_user='$iuser') )
						 		and not a.i_store in ('AA','PB') and c.i_store_location='00' 
						 		order by a.i_store ", FALSE)->limit($num,$offset);
# and i_store_location='00'
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
}
?>
