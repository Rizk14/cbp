<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iperiode,$ikendaraan,$i_kendaraan_asuransi)
    {
    	if($i_kendaraan_asuransi!=NULL){
		$this->db->select("	* from tr_kendaraan a 
							          left join tr_area b on(a.i_area=b.i_area)
							          left join tr_kendaraan_jenis c on(a.i_kendaraan_jenis=c.i_kendaraan_jenis)
							          left join tr_kendaraan_bbm d on(a.i_kendaraan_bbm=d.i_kendaraan_bbm)
							          left join tr_kendaraan_item e on (e.i_kendaraan=a.i_kendaraan)
									  left join tr_kendaraan_asuransi f on (f.i_kendaraan_asuransi=e.i_kendaraan_asuransi)
									  left join tr_kendaraan_pengguna g on (g.i_kendaraan=a.i_kendaraan)
							          where g.i_periode='$iperiode' and a.i_kendaraan='$ikendaraan' and f.i_kendaraan_asuransi='$i_kendaraan_asuransi'",false);
										}else{
		$this->db->select("	* from tr_kendaraan a 
							          left join tr_area b on(a.i_area=b.i_area)
							          left join tr_kendaraan_jenis c on(a.i_kendaraan_jenis=c.i_kendaraan_jenis)
							          left join tr_kendaraan_bbm d on(a.i_kendaraan_bbm=d.i_kendaraan_bbm)
							          left join tr_kendaraan_item e on (e.i_kendaraan=a.i_kendaraan)
									  left join tr_kendaraan_asuransi f on (f.i_kendaraan_asuransi=e.i_kendaraan_asuransi)
									  left join tr_kendaraan_pengguna g on (g.i_kendaraan=a.i_kendaraan)
							          where g.i_periode='$iperiode' and a.i_kendaraan='$ikendaraan'",false);
										}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select(" * from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or e_area_name like '%$cari%'
							order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacajeniskendaraan($num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_jenis order by i_kendaraan_jenis",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carijeniskendaraan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_jenis 
							where upper(i_kendaraan_jenis) like '%$cari%' or e_kendaraan_jenis like '%$cari%'
							order by i_kendaraan_jenis",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacabbmkendaraan($num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_bbm order by i_kendaraan_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caribbmkendaraan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_bbm 
							where upper(i_kendaraan_bbm) like '%$cari%' or e_kendaraan_bbm like '%$cari%'
							order by i_kendaraan_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaasuransikendaraan($num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_asuransi order by i_kendaraan_asuransi",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariasuransikendaraan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_asuransi 
							where upper(i_kendaraan_asuransi) like '%$cari%' or e_kendaraan_asuransi like '%$cari%'
							order by i_kendaraan_asuransi",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cek($iperiode,$ikendaraan)
    {
		$this->db->select(" distinct tr_kendaraan.i_kendaraan from tr_kendaraan,tr_kendaraan_pengguna where tr_kendaraan_pengguna.i_periode='$iperiode' and tr_kendaraan.i_kendaraan='$ikendaraan'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insertheaderkendaraan($ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$dpajak)
    {
    	$this->db->set(
    		array(
				'i_kendaraan'					=> $ikendaraan,
				'i_area'	 					=> $iarea,
				'i_kendaraan_jenis'				=> $ikendaraanjenis,
				'i_kendaraan_bbm'				=> $ikendaraanbbm,
				'd_pajak'						=> $dpajak
    		)
    	);
    	
    	$this->db->insert('tr_kendaraan');
    }

    function insertheaderkendaraanpengguna($iperiode,$ikendaraan,$epengguna)
    {
    	$this->db->set(
    		array(
				'i_periode'						=> $iperiode,
				'i_kendaraan'					=> $ikendaraan,
				'e_pengguna'					=> $epengguna
    		)
    	);
    	
    	$this->db->insert('tr_kendaraan_pengguna');
    }

    function insertdetailkendaraan($ikendaraan,$e_pemilik_kendaraan,$e_posisi_kendaraan,
					$d_serah_terima_kendaraan,$e_merek_kendaraan,$e_jenis_kendaraan,$e_warna_kendaraan,$e_nomor_rangka,$e_nomor_mesin,
					$d_pajak_1_tahun,$d_pajak_5_tahun,$ikendaraanasuransi,$d_bayar_asuransi,$e_nomor_polisasuransi,$e_tlo,$d_periode_awalasuransi,$d_periode_akhirasuransi,
					$e_tahun,$e_desc)
    {
    	$this->db->set(
    		array(
				'i_kendaraan'					=> $ikendaraan,
				'e_pemilik_kendaraan'			=> $e_pemilik_kendaraan,
				'e_posisi_kendaraan'			=> $e_posisi_kendaraan,
				'd_serah_terima_kendaraan'		=> $d_serah_terima_kendaraan,
				'e_merek_kendaraan'				=> $e_merek_kendaraan,
				'e_jenis_kendaraan'				=> $e_jenis_kendaraan,
				'e_warna_kendaraan'				=> $e_warna_kendaraan,
				'e_nomor_rangka'				=> $e_nomor_rangka,
				'e_nomor_mesin'					=> $e_nomor_mesin,
				'd_pajak_1_tahun'				=> $d_pajak_1_tahun,
				'd_pajak_5_tahun'				=> $d_pajak_5_tahun,
				'i_kendaraan_asuransi'			=> $ikendaraanasuransi,
				'd_bayar_asuransi'				=> $d_bayar_asuransi,
				'e_nomor_polisasuransi'			=> $e_nomor_polisasuransi,
				'e_tlo'							=> $e_tlo,
				'd_periode_awalasuransi'		=> $d_periode_awalasuransi,
				'd_periode_akhirasuransi'		=> $d_periode_akhirasuransi,
				'e_tahun'						=> $e_tahun,
				'e_desc'						=> $e_desc
    		)
    	);
    	
    	$this->db->insert('tr_kendaraan_item');
    }
    function updateheaderkendaraan($ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$dpajak)
    {
    	$this->db->set(
    		array(
				'i_area'	 					=> $iarea,
				'i_kendaraan_jenis'				=> $ikendaraanjenis,
				'i_kendaraan_bbm'				=> $ikendaraanbbm,
				'd_pajak'						=> $dpajak
    		)
    	);
    	
    	$this->db->where("i_kendaraan",$ikendaraan);
    	$this->db->update('tr_kendaraan');
    }
    function updateheaderkendaraanpengguna($iperiode,$ikendaraan,$epengguna)
    {
    	$this->db->set(
    		array(

				'e_pengguna'					=> $epengguna
    		)
    	);
    	
    	$this->db->where("i_periode",$iperiode);
    	$this->db->where("i_kendaraan",$ikendaraan);
    	$this->db->update('tr_kendaraan_pengguna');
    }
    function updatedetailkendaraan($iperiode,$ikendaraan,$e_pemilik_kendaraan,$e_posisi_kendaraan,
					$d_serah_terima_kendaraan,$e_merek_kendaraan,$e_jenis_kendaraan,$e_warna_kendaraan,$e_nomor_rangka,$e_nomor_mesin,
					$d_pajak_1_tahun,$d_pajak_5_tahun,$ikendaraanasuransi,$d_bayar_asuransi,$e_nomor_polisasuransi,$e_tlo,$d_periode_awalasuransi,$d_periode_akhirasuransi,
					$e_tahun,$e_desc)
    {
    	$this->db->set(
    		array(
				'e_pemilik_kendaraan'			=> $e_pemilik_kendaraan,
				'e_posisi_kendaraan'			=> $e_posisi_kendaraan,
				'd_serah_terima_kendaraan'		=> $d_serah_terima_kendaraan,
				'e_merek_kendaraan'				=> $e_merek_kendaraan,
				'e_jenis_kendaraan'				=> $e_jenis_kendaraan,
				'e_warna_kendaraan'				=> $e_warna_kendaraan,
				'e_nomor_rangka'				=> $e_nomor_rangka,
				'e_nomor_mesin'					=> $e_nomor_mesin,
				'd_pajak_1_tahun'				=> $d_pajak_1_tahun,
				'd_pajak_5_tahun'				=> $d_pajak_5_tahun,
				'i_kendaraan_asuransi'			=> $ikendaraanasuransi,
				'd_bayar_asuransi'				=> $d_bayar_asuransi,
				'e_nomor_polisasuransi'			=> $e_nomor_polisasuransi,
				'e_tlo'							=> $e_tlo,
				'd_periode_awalasuransi'		=> $d_periode_awalasuransi,
				'd_periode_akhirasuransi'		=> $d_periode_akhirasuransi,
				'e_tahun'						=> $e_tahun,
				'e_desc'						=> $e_desc
    		)
    	);
    	$this->db->where("i_kendaraan",$ikendaraan);
    	$this->db->update('tr_kendaraan_item');
    }
}
?>
