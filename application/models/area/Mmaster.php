<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea)
    {
		$this->db->select("* from tr_area
					left join tr_area_type on (tr_area.i_area_type=tr_area_type.i_area_type)
					left join tr_store on (tr_area.i_store=tr_store.i_store)
				   	where tr_area.i_area = '$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iarea,$eareaname,$iareatype,$eareaaddress,
		       $eareacity,$eareaphone,$earearemark,$nareatoleransi,
		       $eareashortname,$istore)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_area (i_area,e_area_name,i_area_type,e_area_address,e_area_city,
				  e_area_shortname,e_area_phone,e_area_remark,d_area_entry,n_area_toleransi,i_store) 
				  values
				('$iarea','$eareaname','$iareatype','$eareaaddress','$eareacity',
				 '$eareashortname','$eareaphone','$earearemark','$dentry',$nareatoleransi,'$istore')");
		#redirect('area/cform/');
    }

    function update($iarea,$eareaname,$iareatype,$eareaaddress,
		       $eareacity,$eareaphone,$earearemark,$nareatoleransi,
		       $eareashortname,$istore)
    {
		$this->db->query("update tr_area set
				e_area_name='$eareaname',i_area_type='$iareatype',
				e_area_address='$eareaaddress',e_area_city='$eareacity',
				e_area_shortname='$eareashortname',e_area_phone='$eareaphone',
				e_area_remark='$earearemark',n_area_toleransi=$nareatoleransi,i_store='$istore'
				where i_area='$iarea'");
		#redirect('area/cform/');
    }
	
    public function delete($iarea) 
    {
		$this->db->query('DELETE FROM tr_area WHERE i_area=\''.$iarea.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_area
					left join tr_area_type on (tr_area.i_area_type=tr_area_type.i_area_type)
					left join tr_store on (tr_area.i_store=tr_store.i_store)
				   	where upper(tr_area.e_area_name) like '%$cari%' or upper(tr_area.i_area) like '%$cari%' or 
					upper(tr_area_type.e_area_typename) like '%$cari%' order by tr_area.i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaareatype($num,$offset)
    {
		$this->db->select("i_area_type, e_area_typename from tr_area_type order by i_area_type",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacastore($num,$offset)
    {
		$this->db->select("i_store, e_store_name from tr_store order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_area_typename from tr_area a, tr_area_type b
				   			where (upper(a.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%') 
							and a.i_area_type=b.i_area_type order by a.i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  
	function cariareatype($cari,$num,$offset)
    {
		$this->db->select("i_area_type, e_area_typename from tr_area_type 
				   where upper(e_area_typename) like '%$cari%' or upper(i_area_type) like '%$cari%' order by i_area_type", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function caristore($cari,$num,$offset)
    {
		$this->db->select("i_store, e_store_name from tr_store 
				   where upper(e_store_name) like '%$cari%' or upper(i_store) like '%$cari%' order by i_store", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
