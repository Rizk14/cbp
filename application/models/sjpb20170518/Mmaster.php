<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($isjp,$icustomer)
    {
		$this->db->select(" a.*, c.e_customer_name, b.e_area_name, b.i_store
                        from tm_sjpb a, tr_area b, tr_customer c
                        where a.i_area=b.i_area and a.i_customer=c.i_customer
                        and a.i_sjpb ='$isjp' ", false);#and substring(a.i_sj,9,2)='$iarea'", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($isj, $icustomer)
    {
		/*$this->db->select(" a.*, b.e_product_motifname, c.v_sjpb
                        from tm_sjpb_item a, tr_product_motif b, tm_sjpb c, tm_sjp_item d
                        where a.i_sjpb = '$isj' and a.i_product=b.i_product 
                        and a.i_sjpb=c.i_sjpb and a.i_area=c.i_area
                        and c.i_sjp=d.i_sjp and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false);*/
                        
		$this->db->select(" a.*, b.e_product_motifname, c.v_sjpb from tm_sjpb_item a 
		                    inner join tr_product_motif b on (a.i_product=b.i_product  
		                    and a.i_product_motif=b.i_product_motif) inner join tm_sjpb c 
		                    on (a.i_sjpb=c.i_sjpb and a.i_area=c.i_area) left join tm_sjp_item d
		                    on (a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
		                    and a.i_product=d.i_product and c.i_sjp=d.i_sjp and c.i_area=d.i_area )
		                    where a.i_sjpb ='$isj' order by a.n_item_no", false);                    
                        
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($ispb, $dspb, $iarea, $fop, $nprint)
    {
    	$this->db->set(
    		array(
			'i_spb'	=> $ispb,
			'd_spb'	=> $dspb,
			'i_area'	=> $iarea,
			'f_op'		=> 'f',
			'n_print'	=> 0
    		)
    	);
    	
    	$this->db->insert('tm_spb');
    }
    function insertdetail($ispb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	  = $row->c;
    	$this->db->set(
    		array(
					'i_spb'			=> $ispb,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_order'			=> $norder,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'e_remark'			=> $eremark,
          'd_sjpb_entry' => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_spb_item');
    }

    function updateheader($ispb, $dspb, $iarea)
    {
    	$this->db->set(
    		array(
			'd_spb'	=> $dspb,
			'i_area'	=> $iarea
    		)
    	);
    	$this->db->where('i_spb',$ispb);
    	$this->db->update('tm_spb');
    }

    public function deletedetail($iproduct, $iproductgrade, $ispb, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb'
										and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										and i_product_motif='$iproductmotif'");
		return TRUE;
    }
	
    public function delete($ispb) 
    {
		$this->db->query('DELETE FROM tm_spb WHERE i_spb=\''.$ispb.'\'');
		$this->db->query('DELETE FROM tm_spb_item WHERE i_spb=\''.$ispb.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spb order by i_spb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasjp($area1,$area2,$area3,$area4,$area5,$area,$num,$offset)
    {
		  if($offset=='') $offset=0;
		    $this->db->select(" distinct(a.*) from tm_sjp a, tm_sjp_item b, tm_spmb c
                            where a.i_area='$area' and a.i_spmb=c.i_spmb 
                            and c.f_spmb_consigment='t' and a.f_sjp_cancel='f'
                            and c.f_spmb_cancel='f' and b.n_saldo>0
                            and not a.d_sjp_receive is null
                            and a.i_sjp=b.i_sjp and a.i_area=b.i_area
                            and a.f_sjpb_close='0'
                            order by a.i_sjp limit $num offset $offset",false);
/*		    $this->db->select(" a.* from tm_sjp a, tm_spmb c where a.i_area='$area' and a.i_spmb=c.i_spmb 
                            and c.f_spmb_consigment='t' and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                            and not a.d_sjp_receive is null
                            order by a.i_sjp limit $num offset $offset",false);*/
		    /*$this->db->select(" a.* from tm_sjp a, tm_spmb c where a.i_area='$area' and a.i_spmb=c.i_spmb 
                            and c.f_spmb_consigment='t' and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                            and a.i_sjp not in (select i_sjp from tm_sjpb b where a.i_area=b.i_area)
                            and not a.d_sjp_receive is null
                            order by a.i_sjp limit $num offset $offset",false);*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisjp($cari,$area1,$area2,$area3,$area4,$area5,$area,$num,$offset)
    {
		  if($offset=='') $offset=0;
    		$area1	= $this->session->userdata('i_area');
			  $area2	= $this->session->userdata('i_area2');
			  $area3	= $this->session->userdata('i_area3');
			  $area4	= $this->session->userdata('i_area4');
			  $area5	= $this->session->userdata('i_area5');
		    $this->db->select(" distinct(a.*) from tm_sjp a, tm_sjp_item b, tm_spmb c
                            where a.i_area='$area' and a.i_spmb=c.i_spmb 
                            and c.f_spmb_consigment='t' and a.f_sjp_cancel='f'
                            and c.f_spmb_cancel='f' and b.n_saldo>0
                            and not a.d_sjp_receive is null
                            and a.i_sjp=b.i_sjp and a.i_area=b.i_area
                            and a.f_sjpb_close='0'
                            and a.i_sjp like '%$cari%' limit $num offset $offset",false);
			  /*$this->db->select("	a.* from tm_sjp a, tm_spmb c
                            where a.i_area='$area' and a.i_spmb=c.i_spmb and c.f_spmb_consigment='t' 
                            and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                            and not a.d_sjp_receive is null
                            and a.i_sjp like '%$cari%' limit $num offset $offset",false);*/
			  /*$this->db->select("	a.* from tm_sjp a, tm_spmb c
                            where a.i_area='$area' and a.i_spmb=c.i_spmb and c.f_spmb_consigment='t' 
                            and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                            and not a.d_sjp_receive is null
                            and a.i_sjp not in (select i_sjp from tm_sjpb b where a.i_area=b.i_area)
                            and a.i_sjp like '%$cari%' limit $num offset $offset",false);*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function product($sjp,$iarea)//ubah n_quantity_order jd n_quantity_receive
    {
	    $query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif, b.n_quantity_deliver,
								                  a.e_product_motifname as namamotif, b.n_saldo as n_qty,
								                  c.e_product_name as nama,b.v_unit_price as harga, b.i_product_grade as grade
								                  from tr_product_motif a,tr_product c, tm_sjp_item b
								                  where a.i_product=c.i_product 
								                  and b.i_product_motif=a.i_product_motif
								                  and c.i_product=b.i_product and b.n_saldo>0
								                  and b.i_sjp='$sjp' and i_area='$iarea' order by b.n_item_no",false);
/*	    $query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif, b.n_quantity_deliver,
								                  a.e_product_motifname as namamotif, b.n_quantity_receive as n_qty,
								                  c.e_product_name as nama,b.v_unit_price as harga, b.i_product_grade as grade
								                  from tr_product_motif a,tr_product c, tm_sjp_item b
								                  where a.i_product=c.i_product 
								                  and b.i_product_motif=a.i_product_motif
								                  and c.i_product=b.i_product
								                  and b.i_sjp='$sjp' and i_area='$iarea' order by b.n_item_no",false);*/
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
    function runningnumber(){
    	$query= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		  $row  = $query->row();
		  $thbl	= $row->c;
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_spb,11,6)) as max from tm_spb where substr(i_spb,6,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospb  =$terakhir+1;
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<6){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
			  $nospb  ="spb-".$thbl."-".$nospb;
			  return $nospb;
		  }else{
			  $nospb  ="000001";
			  $nospb  ="spb-".$thbl."-".$nospb;
			  return $nospb;
		  }
    }
    function cari($cari,$num,$offset)

    {
		$this->db->select(" * from tm_spb where upper(i_spb) like '%$cari%' 
					order by i_spb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
/*
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								              a.e_product_motifname as namamotif, 
								              c.e_product_name as nama,c.v_product_mill as harga
								              from tr_product_motif a,tr_product c
								              where a.i_product=c.i_product
							                 	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								              limit $num offset $offset",false);
*/
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								              a.e_product_motifname as namamotif, 
								              c.e_product_name as nama,b.v_unit_price as harga
								              from tr_product_motif a,tr_product c
								              where a.i_product=c.i_product
							                 	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								              limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($area1,$iuser,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select("	* from tr_area 
								order by i_area", false)->limit($num,$offset);
		}else{
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($area1,$iuser,$cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select("	i_area, e_area_name, i_store from tr_area 
								where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
								order by i_area ", FALSE)->limit($num,$offset);
		}else{
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumbersj($iareasj,$thbl)
    {
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
	    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SB'
                          and e_periode='$asal' 
                          and i_area='$iareasj' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nosj
                            where i_modul='SB'
                            and e_periode='$asal' 
                            and i_area='$iareasj'", false);
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
 		  	$nosj  ="SB-".$thbl."-".$iareasj.$nosj;
			  return $nosj;
		  }else{
			  $nosj  ="0001";
		  	$nosj  ="SB-".$thbl."-".$iareasj.$nosj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SB','$iareasj','$asal',1)");
			  return $nosj;
		  }
    }
    function insertsjpb($isjpb, $isjp, $icustomer, $iarea, $ispg, $dsjpb, $dsjp, $vsjpb)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjentry	= $row->c;
		  $this->db->set(
		  array(	  
      'i_sjpb'        => $isjpb,
	    'i_sjp'		      => $isjp,
	    'i_customer'    => $icustomer,
	    'i_area'        => $iarea,
	    'i_spg'         => $ispg,
		  'd_sjpb'		    => $dsjpb,
	    'd_sjp'		      => $dsjp,
	    'v_sjpb'      	=> $vsjpb,
	    'f_sjpb_cancel' => 'f',
	    'd_sjpb_entry'  => $dsjentry
		  )
    	);
    	$this->db->insert('tm_sjpb');
    }
    function insertsjheader2($ispb,$dspb,$isj,$dsj,$isjtype,$iareato,$iareafrom,$isalesman,$icustomer,
						   	$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1, 
							$vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto,$isjold,$daerah,$fentpusat,$iareareff)
    {
    $query 		= $this->db->query("SELECT f_customer_plusppn, f_customer_plusdiscount from tr_customer where i_customer='$icustomer'");
		$row   		= $query->row();
		$plusppn	= $row->f_customer_plusppn;
		$plusdiscount	= $row->f_customer_plusdiscount;
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjentry	= $row->c;

      		$this->db->set(
      			array(	  
          'i_sj'		        => $isj,
				  'i_sj_old'		    => $isjold,
				  'i_sj_type'   		=> $isjtype,
				  'i_spb'       		=> $ispb,
				  'd_spb'       		=> $dspb,
				  'd_sj'        		=> $dsj,
				  'i_area_from'		  => $iareafrom,
				  'i_area_to'		    => $iareato,
				  'i_salesman'		  => $isalesman,
				  'i_refference_document'	=> $ispb,
				  'i_customer'		  => $icustomer,
          'f_plus_ppn'      => $plusppn,
          'f_plus_discount' => $plusdiscount,
				  'n_sj_discount1'	=> $nspbdiscount1,
				  'n_sj_discount2'	=> $nspbdiscount2,
    	 	  'n_sj_discount3' 	=> $nspbdiscount3,
				  'v_sj_discount1'	=> $vspbdiscount1,
				  'v_sj_discount2'	=> $vspbdiscount2,
				  'v_sj_discount3'	=> $vspbdiscount3,
				  'v_sj_discounttotal'	=> $vspbdiscounttotal,
				  'v_sj_gross'		  => $vspbgross,
				  'v_sj_netto'		  => $vspbnetto,
				  'd_sj_entry'		  => $dsjentry,
				  'f_sj_cancel'		  => 'f',
          'f_sj_daerah'     => $daerah,
				  'f_entry_pusat'	  => $fentpusat,
				  'i_area_referensi'=> $iareareff
      		)
      	);
      	
      	$this->db->insert('tm_nota');
    }
    function insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                  			    $vunitprice,$isjpb,$iarea,$i,$dsjpb,$ipricegroup)
    {
    	$this->db->set(
    		array(
				'i_sjpb'			    => $isjpb,
				'i_area'	        => $iarea,
				'i_product'			  => $iproduct,
				'i_product_motif'	=> $iproductmotif,
				'i_product_grade'	=> $iproductgrade,
				'n_deliver'       => $ndeliver,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
        'd_sjpb'          => $dsjpb,
				'n_item_no'       => $i,
        'i_price_group'   => $ipricegroup
    		)
    	);
    	$this->db->insert('tm_sjpb_item');
    }
    function updatesjpdetail($isjp,$iarea,$iproduct,$iproductgrade,$iproductmotif,$ndeliver)
    {
	    $this->db->query(" update tm_sjp_item set n_saldo = n_saldo-$ndeliver
			                   where i_sjp='$isjp' and i_product='$iproduct' and i_product_grade='$iproductgrade'
			                   and i_product_motif='$iproductmotif' and i_area='$iarea' ",false);
    }
    function cekdaerah($ispb,$iarea)
		{
			$query=$this->db->query(" select f_spb_stockdaerah from tm_spb where i_spb='$ispb' and i_area='$iarea'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$stockdaerah=$qq->f_spb_stockdaerah;
				}
				return $stockdaerah;
			}
		}
    function cekkons($ispb,$iarea)
		{
			$query=$this->db->query(" select f_spb_consigment from tm_spb where i_spb='$ispb' and i_area='$iarea'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$consigment=$qq->f_spb_consigment;
				}
				return $consigment;
			}
		}

    function updatesjp($isjp,$iarea,$isjpb,$dsjpb)
	  {
		$this->db->query(" update tm_sjp set i_sjpb = '$isjpb',d_sjpb='$dsjpb' where i_sjp='$isjp' and i_area='$iarea'",false);
	  }
    function deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isjpb,$iarea,$ipricegroup)
	{
		$this->db->query("delete from tm_sjpb_item where i_sjpb='$isjpb' and i_area='$iarea'
                      and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'",false);
	}	
    function updatesjheader($isjpb,$dsjpb,$isjp,$dsjp,$iarea,$icustomer,$ispg,$vsjpb)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
      $row   		= $query->row();
      $dsjupdate	= $row->c;
    	$this->db->set(
    		array(
			        'i_sjp'     			=> $isjp,
			        'd_sjp'			      => $dsjp,
			        'd_sjpb'      		=> $dsjpb,
			        'i_area'      		=> $iarea,
			        'i_spg'     	  	=> $ispg,
			        'i_customer'	  	=> $icustomer,
			        'v_sjpb'      		=> $vsjpb,
			        'd_sjpb_update'		=> $dsjupdate,
			        'f_sjpb_cancel'		=> 'f'
    		)
    	);
    	$this->db->where('i_sjpb',$isjpb);
    	$this->db->where('i_area',$iarea);
    	$this->db->where('i_customer',$icustomer);
    	$this->db->update('tm_sjpb');
    }
    function updatesjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			     $vunitprice,$isjpb,$iarea,$i,$dsjpb,$ipricegroup)
    {
    	$this->db->set(
    		array(
			        'n_deliver'  			=> $ndeliver,
			        'v_unit_price'    => $vunitprice,
			        'i_price_group'		=> $ipricegroup
    		)
    	);
    	$this->db->where('i_sjpb',$isjpb);
    	$this->db->where('i_area',$iarea);
    	$this->db->where('i_product',$iproduct);
    	$this->db->where('i_product_grade',$iproductgrade);
    	$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_sjpb_item');
    }
    function searchsjheader($isj,$iarea,$isjtype,$daerah)
    {
		return $this->db->query(" SELECT * FROM tm_nota WHERE i_sj='$isj' AND i_area_to='$iarea' AND i_sj_type='$isjtype' AND f_sj_daerah='$daerah' ");
	}
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                )
                              ",false);
    }
    function inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw
                                )
                              ",false);
*/
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,0,0,$qsj,$qaw-$qsj,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', $now, $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                )
                              ",false);
    }
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,$qsj,0,0,0,$qsj,0,'f')
                              ",false);
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qsj, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                    DELETE FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function bacacustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" a.*, c.i_spg, c.e_spg_name from tr_customer a, tr_customer_consigment b, tr_spg c
                        where a.i_area='$iarea' and a.i_customer=b.i_customer and a.i_customer=c.i_customer
                        and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
				                order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($product,$num,$offset)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
					                      a.e_product_motifname as namamotif, b.v_product_retail as harga,
					                      c.e_product_name as nama
					                      from tr_product_motif a,tr_product c, tr_product_priceco b
					                      where a.i_product=c.i_product and a.i_product=b.i_product and a.i_product='$product'
                                order by c.i_product, a.e_product_motifname, b.i_price_group
                                limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
