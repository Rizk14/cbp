<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$iarea)
    {
     if($iarea=='NA'){

         $this->db->select(" c.e_area_name, sum(v_target) as v_target, b.i_area, sum(b.v_nota_netto) as v_nota_netto, 
                          sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from(
                          select v_target, i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler, 0 as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif,
                          0 as v_spb_gross, 0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif from tm_target
                          where i_periode='$iperiode'
                          union all
                          select 0 as v_target, i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and to_char(d_kn,'yyyymm')='$iperiode'

                          group by i_area
                          union all

                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area

                          ) as a
                          group by i_area
                          ) as b, tr_area c
                          where b.i_area=c.i_area
                          group by b.i_area, c.e_area_name
                          order by b.i_area",false); 

}else{
/*  
		  $this->db->select("	c.e_area_name, sum(v_target) as v_target, b.i_area, sum(b.v_nota_netto) as v_nota_netto, 
                          sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from(
                          select v_target, i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler, 0 as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif,
                          0 as v_spb_gross, 0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif from tm_target
                          where i_periode='$iperiode' and i_area='$iarea'
                          union all
                          select 0 as v_target, i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area
                          union all

                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area

                          ) as a
                          group by i_area
                          ) as b, tr_area c
                          where b.i_area=c.i_area
                          group by b.i_area, c.e_area_name
                          order by b.i_area",false);
*/
		  $this->db->select("	c.e_area_name, sum(v_target) as v_target, b.i_area, sum(b.v_nota_netto) as v_nota_netto, 
                          sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from(
                          select v_target, i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler, 0 as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif,
                          0 as v_spb_gross, 0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif from tm_target
                          where i_periode='$iperiode' and i_area='$iarea'
                          union all
                          select 0 as v_target, i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'

                          group by i_area
                          union all

                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area

                          ) as a
                          group by i_area
                          ) as b, tr_area c
                          where b.i_area=c.i_area
                          group by b.i_area, c.e_area_name
                          order by b.i_area",false); 
              }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacapersales($iperiode,$iarea)
    {
      $this->db->select("	c.e_salesman_name, d.e_area_name, b.i_area, b.i_salesman, sum(v_target) as v_target, 
                          sum(b.v_nota_netto) as v_nota_netto, sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select v_target, i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler, 
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif,
                          0 as v_spb_gross, 0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif from tm_target_itemsls
                          where i_periode='$iperiode' and i_area='$iarea'
                          union all
                          select 0 as v_target, i_area, i_salesman, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 
                          0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, 
                          sum(v_spb)-sum(v_spbdiscount) as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_docspb,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 
                          0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 
                          0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
                          union all
  
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_doc,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and i_area='$iarea' and to_char(d_kn,'yyyymm')='$iperiode'
                          group by i_area, i_salesman
    
                        ) as a
                          group by i_area, i_salesman
                        ) as b, tr_salesman c, tr_area d
                          where b.i_salesman=c.i_salesman and b.i_area=d.i_area
                          group by b.i_area, b.i_salesman, c.e_salesman_name, d.e_area_name
                          order by b.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
      }
    }
    function bacaarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
