<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icategory)
    {
		$this->db->select("a.i_product_category, a.e_product_categoryname, a.i_product_class, b.e_product_classname")->from('tr_product_category a, tr_product_class b')->where("i_product_category = '$icategory' and a.i_product_class=b.i_product_class");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($icategory, $ecategoryname, $iclass)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_product_category (i_product_category, e_product_categoryname, i_product_class, d_product_categoryentry) values ('$icategory', '$ecategoryname','$iclass', '$dentry')");
		redirect('category/cform/');
    }

    function update($icategory, $ecategoryname, $iclass)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_product_category set e_product_categoryname = '$ecategoryname', i_product_class = '$iclass', d_product_categoryupdate = '$dupdate' where i_product_category = '$icategory'");
		redirect('category/cform/');
    }
	
    public function delete($icategory) 
    {
		$this->db->query('DELETE FROM tr_product_category WHERE i_product_category=\''.$icategory.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.i_product_category, a.e_product_categoryname, a.i_product_class, a.d_product_categoryentry, b.e_product_classname from tr_product_category a, tr_product_class b where a.i_product_class=b.i_product_class and (upper(b.e_product_classname) like '%$cari%' or upper(a.i_product_category) like '%$cari%' or upper(a.e_product_categoryname) like '%$cari%') order by a.i_product_category",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacakelas($num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname from tr_product_class order by i_product_class",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_product_category, a.e_product_categoryname, a.i_product_class, a.d_product_categoryentry, b.e_product_classname from tr_product_category a, tr_product_class b where (upper(a.e_product_categoryname) like '%$cari%' or upper(a.i_product_category) like '%$cari%' or upper(b.e_product_classname) like '%$cari%' or upper(a.i_product_class) like '%$cari%') and a.i_product_class=b.i_product_class order by a.i_product_category",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carikelas($cari,$num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname from tr_product_class where upper(e_product_classname) like '%$cari%' or upper(i_product_class) like '%$cari%' order by i_product_class",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
