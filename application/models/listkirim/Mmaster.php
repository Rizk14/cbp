<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
//		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
//		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query("update tm_spmb set f_spmb_cancel='t' WHERE i_spmb='$ispmb'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							          where a.i_area=b.i_area and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_area_name from tm_spmb a, tr_area b
					where a.i_area=b.i_area and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
					where a.i_area=b.i_area and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%' or upper(a.i_spmb_old) like '%$cari%')
					order by a.i_spmb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		/*$this->db->select(" distinct(a.i_dkb), a.d_dkb, e.i_bapb, e.d_bapb, b.e_area_name, e.n_bal, e.i_customer, f.e_customer_name, h.e_ekspedisi
                        from tr_area b, tm_dkb a, tm_dkb_item c
                        left join tm_bapb_item d on(c.i_sj=d.i_sj)
                        left join tm_bapb e on(d.i_bapb=e.i_bapb and d.i_area=e.i_area)
                        left join tr_customer f on(e.i_customer=f.i_customer)
                        left join tm_dkb_ekspedisi g on(c.i_dkb=g.i_dkb and c.i_area=g.i_area)
                        left join tr_ekspedisi h on(g.i_ekspedisi=h.i_ekspedisi)
                        where a.i_area=b.i_area and a.f_dkb_batal='f' and a.i_dkb=c.i_dkb and a.i_area=c.i_area 
                        and (upper(a.i_dkb) like '%$cari%') and substring(a.i_dkb,10,2)='$iarea' and
                        a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dkb <= to_date('$dto','dd-mm-yyyy')
            						ORDER BY a.i_dkb ",false)->limit($num,$offset);*/
		$this->db->select(" a.i_sj, a.d_sj, b.i_dkb, b.d_dkb, c.i_bapb, c.d_bapb, d.e_area_name, c.n_bal,
                        a.i_customer, e.e_customer_name, g.e_ekspedisi
                        from tr_area d, tr_customer e, tm_nota a
                        left join tm_dkb b on (a.i_dkb=b.i_dkb)
                        left join tm_bapb c on (a.i_bapb=c.i_bapb)
                        left join tm_dkb_ekspedisi f on (a.i_dkb=f.i_dkb and a.i_area=f.i_area)
                        left join tr_ekspedisi g on(f.i_ekspedisi=g.i_ekspedisi)
                        where a.i_customer=e.i_customer and a.i_area=d.i_area and b.f_dkb_batal='f'
                        and substring(a.i_dkb,10,2)='$iarea' and (upper(a.i_dkb) like '%$cari%' or upper(a.i_sj) like '%$cari%')
                        and a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dkb <= to_date('$dto','dd-mm-yyyy')
                        order by a.i_area, a.i_sj ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_spmb a, tr_area b
						where a.i_area=b.i_area and a.f_spmb_cancel='f'
						and (upper(a.i_spmb) like '%$cari%')
						and a.i_area='$iarea' and
						a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
						a.d_spmb <= to_date('$dto','dd-mm-yyyy')
						ORDER BY b.e_area_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
