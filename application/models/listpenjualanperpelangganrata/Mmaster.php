<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
/*
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, sum(a.v_nota_gross) as nota
                          from tm_nota a, tr_customer b, tr_city c
                          where a.f_nota_cancel='f' and to_char(a.d_nota,'yyyymm')='$iperiode' and not a.i_nota isnull and a.i_area='$iarea'
                          and a.i_customer=b.i_customer and b.i_city=c.i_city and b.i_area=c.i_area
                          group by a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman
                          order by c.e_city_name ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
*/
    function bacaperiode($dfrom,$dto,$iarea,$interval)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
        $periode = $th.$bl;
			}
      if($iarea=='NA'){
      $sql=" * from crosstab
              ('SELECT f.e_customer_classname||b.i_customer||b.e_customer_name||b.e_customer_address||c.e_city_name||d.e_salesman_name 
              as komplit, f.e_customer_classname, b.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_salesman_name,
              to_number(to_char(a.d_nota, ''mm''),''99'') as bln, 
              sum(a.v_nota_gross) AS jumlah
              FROM tm_nota a, tr_customer b, tr_city c, tr_customer_salesman d, tm_spb e, tr_customer_class f
              WHERE a.f_nota_cancel = false AND b.i_customer = a.i_customer and b.i_customer_class = f.i_customer_class 
              AND NOT a.i_nota IS NULL and b.i_city=c.i_city and b.i_area=c.i_area and a.i_customer = d.i_customer and a.i_area = d.i_area 
              and d.e_periode =''$periode'' and a.i_spb = e.i_spb and a.i_area = e.i_area and e.i_product_group = d.i_product_group
              AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY f.e_customer_classname, b.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, 
              to_char(a.d_nota, ''mm''), d.e_salesman_name
              order by b.i_customer, b.e_customer_name, to_char(a.d_nota, ''mm''), d.e_salesman_name',
              'select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (komplit text, kelas text, kode text, nama text, alamat text, kota text, sales text,";
        switch ($bl){
        case '01' :
          $sql.="Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 
                 Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.="Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 
                 Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.="Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 
                 Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.="Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 
                 Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.="May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 
                 Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.="Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                 Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.="Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 
                 Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.="Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                 May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.="Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                 Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.="Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                 Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.="Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                 Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.="Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                 Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" order by kode, nama";
      }else{
        $sql=" * from crosstab
              ('SELECT f.e_customer_classname||b.i_customer||b.e_customer_name||b.e_customer_address||c.e_city_name||d.e_salesman_name 
              as komplit, f.e_customer_classname, b.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_salesman_name,
              to_number(to_char(a.d_nota, ''mm''),''99'') as bln, 
              sum(a.v_nota_gross) AS jumlah
              FROM tm_nota a, tr_customer b, tr_city c, tr_customer_salesman d, tm_spb e, tr_customer_class f
              WHERE a.f_nota_cancel = false AND b.i_customer = a.i_customer and a.i_area=''$iarea'' 
              and b.i_customer_class = f.i_customer_class 
              AND NOT a.i_nota IS NULL and b.i_city=c.i_city and b.i_area=c.i_area and a.i_customer = d.i_customer and a.i_area = d.i_area 
              and d.e_periode =''$periode'' and a.i_spb = e.i_spb and a.i_area = e.i_area and e.i_product_group = d.i_product_group
              AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY f.e_customer_classname, b.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, 
              to_char(a.d_nota, ''mm''), d.e_salesman_name
              order by b.i_customer, b.e_customer_name, to_char(a.d_nota, ''mm''), d.e_salesman_name',
              'select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (komplit text, kelas text, kode text, nama text, alamat text, kota text, sales text,";
        switch ($bl){
        case '01' :
          $sql.="Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 
                 Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.="Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 
                 Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.="Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 
                 Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.="Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 
                 Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.="May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 
                 Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.="Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                 Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.="Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 
                 Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.="Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                 May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.="Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                 Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.="Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                 Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.="Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                 Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.="Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                 Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" order by kode, nama";
      }
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
