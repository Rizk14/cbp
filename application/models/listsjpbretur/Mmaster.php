<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isjpbr, $iarea) 
    {
			$this->db->query("update tm_sjpbr set f_sjpbr_cancel='t' WHERE i_sjpbr='$isjpbr' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_to=b.i_area and a.i_sj_type='01'
								and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
	/*
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') and (a.i_area_to='$area1' or a.i_area_to='$area2' or a.i_area_to='$area3' or a.i_area_to='$area4'
						or a.i_area_to='$area5')
						order by a.i_sj desc",false)->limit($num,$offset);
	*/
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaperiode($iarea,$icustomer,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_customer_name
                          from tm_sjpbr a, tr_customer b
													where (upper(a.i_sjpbr) like '%$cari%') and
                          a.i_area='$iarea' and a.i_customer=b.i_customer and
													a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.i_sjpbr",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isjpbr,$iarea)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                        from tm_sjpbr a, tr_customer b, tr_area c, tr_spg d
						            where a.i_customer=b.i_customer and a.i_area=c.i_area and a.i_spg=d.i_spg
						            and a.i_sjpbr ='$isjpbr' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isjpbr, $iarea)
    {
		$this->db->select("a.i_sjpbr,a.d_sjpbr,a.i_area,a.i_product,a.i_product_grade,
                       a.i_product_motif,a.n_quantity_retur,a.n_quantity_receive,a.v_unit_price,
                       a.e_product_name,b.e_product_motifname,a.e_remark 
                       from tm_sjpbr_item a, tr_product_motif b
				               where a.i_sjpbr = '$isjpbr' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatesjheader($isj,$dsj,$iarea,$vsjpbr,$icustomer,$ispg)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjupdate= $row->c;
    	$this->db->set(
    		array(
				'v_sjpbr'         => $vsjpbr,
        'i_customer'      => $icustomer,
        'i_spg'           => $ispg,
        'd_sjpbr_update'  => $dsjupdate,
        'f_sjpbr_cancel'  => 'f'
    		)
    	);
    	$this->db->where('i_sjpbr',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_sjpbr');
    }
    public function deletesjdetail( $isj, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
	    $this->db->query("DELETE FROM tm_sjpbr_item WHERE i_sjpbr='$isj'
                        and i_area='$iarea'
									      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									      and i_product_motif='$iproductmotif'");
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                DELETE FROM tm_ic_trans 
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation'
                                and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if($row->i_trans!=''){
        return $row->i_trans;
      }else{
        return 1;
      }
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,$vunitprice,$isj,$dsj,$iarea,$eremark,$i)
    {
      $th=substr($dsj,0,4);
      $bl=substr($dsj,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_sjpbr'		          => $isj,
				'i_area'  	          => $iarea,
				'd_sjpbr'		          => $dsj,
				'i_product'       		=> $iproduct,
				'i_product_motif'   	=> $iproductmotif,
				'i_product_grade'   	=> $iproductgrade,
				'e_product_name'    	=> $eproductname,
				'n_quantity_retur'  	=> $nretur,
				'n_quantity_receive'	=> $nreceive,
				'v_unit_price'		    => $vunitprice,
        'e_remark'            => $eremark,
        'e_mutasi_periode'    => $pr,
        'n_item_no'           => $i
    		)
    	);
    	
    	$this->db->insert('tm_sjpbr_item');
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$trans)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      if($trans==''){
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                  )
                                ",false);
      }else{
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal, i_trans)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw, $trans
                                  )
                                ",false);
      }
    }
    function cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment
                                set n_mutasi_kepusat=n_mutasi_kepusat+$qsj, n_mutasi_git=n_mutasi_git+$qsj
                                , n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode,$emutasiperiodesj,$iarea)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                insert into tm_mutasi_consigment
                                (
                                  i_product,i_product_motif,i_product_grade,i_customer,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                  n_saldo_akhir,n_saldo_stockopname,f_mutasi_close, n_mutasi_git)
                                values
                                (
 '$iproduct','$iproductmotif','$iproductgrade','$icustomer','$emutasiperiode',0,0,0,0,$qsj,$qsj,0,'f',$qsj)
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$q_ak)
    {
      if( ($q_ak=='')||($q_ak==null) ) $q_ak=0;
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic_consigment set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$qsj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                insert into tm_ic_consigment
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$icustomer', '$eproductname', 0-$qsj, 't'
                                )
                              ",false);
    }
    function bacaproduct($num,$offset,$cari)
    {
			$this->db->select("	a.i_product as kode, a.e_product_name as nama, b.v_product_retail as harga, 
                          c.i_product_motif as motif, c.e_product_motifname as namamotif
                          from tr_product a, tr_product_price b, tr_product_motif c
                          where a.i_product=b.i_product and b.i_price_group='00'
                          and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                          and a.i_product=c.i_product ORDER BY a.e_product_name",false)->limit($num,$offset);
                          #and a.i_product_status<>'4'
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updatemutasi04($icustomer,$iproduct,$iproductgrade,$iproductmotif,$nasal,$emutasiperiode)
    {
      if( ($nasal=='')||($nasal==null) ) $nasal=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment set n_mutasi_kepusat=n_mutasi_kepusat-$nasal, 
                                n_saldo_akhir=n_saldo_akhir+$nasal, n_mutasi_git=n_mutasi_git-$nasal
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nasal)
    {
      if( ($nasal=='')||($nasal==null) ) $nasal=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic_consigment set n_quantity_stock=n_quantity_stock+$nasal
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                              ",false);
    }
    function insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                  			    $vunitprice,$isjpb,$iarea,$i,$dsjpb,$nreceive)
    {
    	$this->db->set(
    		array(
				'i_sjpb'			    => $isjpb,
				'i_area'	        => $iarea,
				'i_product'			  => $iproduct,
				'i_product_motif'	=> $iproductmotif,
				'i_product_grade'	=> $iproductgrade,
				'n_deliver'       => $ndeliver,
				'n_receive'       => $nreceive,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
        'd_sjpb'          => $dsjpb,
				'n_item_no'       => $i
    		)
    	);
    	$this->db->insert('tm_sjpb_item');
    }
}
?>
