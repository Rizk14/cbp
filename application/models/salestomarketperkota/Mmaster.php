<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacakota($num,$offset,$iarea)
    {
		  $this->db->select("	i_city, e_city_name from tr_city where i_area = '$iarea'
												  order by i_city",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carikota($iarea,$cari,$num,$offset)
    {
		  $this->db->select("	i_city, e_city_name from tr_city where i_area = '$iarea' 
                          and (upper(i_city) like '%$cari%' or upper(e_city_name) like '%$cari%')
												  order by i_city",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$icity,$dfrom,$dto,$num,$offset,$cari)
    {
	    if ($cari == '') {
		    $this->db->select("	distinct(a.i_product), a.e_product_name, sum(a.n_deliver) as total,
                            a.v_unit_price, sum(a.n_deliver*a.v_unit_price) as njual, a.i_area, b.i_city, b.e_city_name
                            from tm_nota_item a, tr_city b, tm_nota c, tr_customer d
                            where a.i_area=b.i_area and b.i_city=d.i_city
                            and a.i_nota=c.i_nota and c.i_customer=d.i_customer
                            and a.i_area='$iarea' and b.i_city='$icity'
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
                            and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            group by a.i_product, a.e_product_name, a.v_unit_price,
                            a.i_area, b.i_city, b.e_city_name
                            order by a.i_product",false)->limit($num,$offset);
	    }else{
		    $this->db->select("	distinct(a.i_product), a.e_product_name, sum(a.n_deliver) as total,
                            a.v_unit_price, sum(a.n_deliver*a.v_unit_price) as njual, a.i_area, b.i_city, b.e_city_name
                            from tm_nota_item a, tr_city b, tm_nota c, tr_customer d
                            where a.i_area=b.i_area and b.i_city=d.i_city
                            and a.i_nota=c.i_nota and c.i_customer=d.i_customer
                            and a.i_area='$iarea' and b.i_city='$icity'
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
                            and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                            group by a.i_product, a.e_product_name, a.v_unit_price,
                            a.i_area, b.i_city, b.e_city_name
                            order by a.i_product",false)->limit($num,$offset);
	    }		
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
}
?>
