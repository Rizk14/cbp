<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	x.i_op, x.d_op, x.i_supplier, x.i_area, x.e_supplier_name, x.e_area_name, x.i_op_old, d.i_do, d.d_do from
								(
								SELECT a.i_op, a.d_op, a.i_supplier, a.i_area, a.i_op_old, b.e_supplier_name, c.e_area_name
								FROM tm_op a, tr_supplier b, tr_area c
								WHERE 
								a.i_supplier = b.i_supplier AND
								a.i_area = c.i_area AND
								a.i_supplier='$isupplier' AND
								a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_op <= to_date('$dto','dd-mm-yyyy')
								) as x
							left join tm_do d on (x.i_op=d.i_op AND x.i_area=d.i_area)
							ORDER BY x.i_op ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	x.i_op, x.d_op, x.i_supplier, x.i_area, x.e_supplier_name, x.e_area_name, x.i_op_old, d.i_do, d.d_do from
								(
								SELECT a.i_op, a.d_op, a.i_supplier, a.i_area, a.i_op_old, b.e_supplier_name, c.e_area_name
								FROM tm_op a, tr_supplier b, tr_area c
								WHERE 
								a.i_supplier = b.i_supplier AND
								a.i_area = c.i_area AND
								a.i_supplier='$isupplier' AND
								a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_op <= to_date('$dto','dd-mm-yyyy') AND
								(upper(a.i_op) like '%$cari%' or upper(a.i_area) like '%$cari%')
								) as x
							left join tm_do d on (x.i_op=d.i_op AND x.i_area=d.i_area and (upper(d.i_do) like '%$cari%'))
							ORDER BY x.i_op ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select("	* FROM tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
							ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
