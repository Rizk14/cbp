<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacastock($iperiode)
  {
	  $this->db->select(" * from f_stock_rekapselisih_perarea_saldoakhir('$iperiode')",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacasales($iperiode)
  {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
    if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
		  $this->db->select(" distinct(a.i_salesman), a.i_area, b.e_salesman_name, e_periode, b.f_salesman_aktif, c.e_area_name
                          from tr_customer_salesman a, tr_salesman b, tr_area c
                          where a.i_salesman=b.i_salesman and b.f_salesman_as='f'
                          and a.i_area=c.i_area and b.f_salesman_aktif='t' and a.e_periode='$iperiode'
                          order by a.i_area, a.i_salesman",false);
    }else{
		  $this->db->select(" distinct(a.i_salesman), a.i_area, b.e_salesman_name, e_periode, b.f_salesman_aktif, c.e_area_name
                          from tr_customer_salesman a, tr_salesman b, tr_area c
                          where a.i_salesman=b.i_salesman and b.f_salesman_as='f'
                          and a.i_area=c.i_area and b.f_salesman_aktif='t' and a.e_periode='$iperiode' and
                          (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                          order by a.i_area, a.i_salesman",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacacash($iperiode)
  {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
    if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
      $this->db->select("	i_area, e_area_name, i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, sum(b.bayar+b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          union all
                          select a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.bayar+b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          ) as a 
                          group by i_area, e_area_name, i_salesman, e_salesman_name
                          order by a.i_area, a.i_salesman",false);
    }else{
      $this->db->select("	i_area, e_area_name, i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, sum(b.bayar+b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          union all
                          select a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.bayar+b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          ) as a 
                          group by i_area, e_area_name, i_salesman, e_salesman_name
                          order by a.i_area, a.i_salesman",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacacredit($iperiode)
  {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
    if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
      $this->db->select("	i_area, e_area_name, a.i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.sisa+b.bayar) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          union all
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          ) as a 
                          group by i_area, e_area_name, a.i_salesman, e_salesman_name
                          order by a.i_area, a.i_salesman",false);
    }else{
      $this->db->select("	i_area, e_area_name, a.i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.sisa+b.bayar) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          union all
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          ) as a 
                          group by i_area, e_area_name, a.i_salesman, e_salesman_name
                          order by a.i_area, a.i_salesman",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacapenjualan($iperiode)
  {
		$area1	= $this->session->userdata('i_area');
		$area2	= $this->session->userdata('i_area2');
		$area3	= $this->session->userdata('i_area3');
		$area4	= $this->session->userdata('i_area4');
		$area5	= $this->session->userdata('i_area5');
    if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
		    $this->db->select(" i_area, i_salesman, e_area_name, e_salesman_name, sum(v_target) as v_target, 
                            sum(v_spb) as v_spb, sum(v_nota) as v_nota, sum(v_retur) as v_retur from(
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, a.v_target, 0 as v_spb, 0 as v_nota, 
                            0 as v_retur
                            from tm_target_itemsls a, tr_area b, tr_salesman c 
                            where a.i_periode = '$iperiode' and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman
                            and b.f_area_real='t'
                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, sum(v_spb) as v_spb, 0 as v_nota, 0 as v_retur
                            from tm_spb a, tr_area b, tr_salesman c 
                            where to_char(d_spb,'yyyymm') = '$iperiode' and f_spb_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman and b.f_area_real='t'
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, 0 as v_spb, sum(v_nota_gross) as v_nota, 0 as v_retur
                            from tm_nota a, tr_area b, tr_salesman c 
                            where to_char(d_nota,'yyyymm') = '$iperiode' and f_nota_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman and b.f_area_real='t'
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name

                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, 0 as v_spb, 0 as v_nota, sum(a.v_netto) as v_retur
                            from tm_kn a, tr_area b, tr_salesman c 
                            where to_char(d_kn,'yyyymm') = '$iperiode' and f_kn_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman and b.f_area_real='t'
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
                            ) as x
                            group by x.i_area, x.i_salesman, x.e_area_name, x.e_salesman_name
                            order by x.i_area, x.i_salesman ",false);
    }else{
		    $this->db->select(" i_area, i_salesman, e_area_name, e_salesman_name, sum(v_target) as v_target, 
                            sum(v_spb) as v_spb, sum(v_nota) as v_nota, sum(v_retur) as v_retur from(
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, a.v_target, 0 as v_spb, 0 as v_nota, 
                            0 as v_retur
                            from tm_target_itemsls a, tr_area b, tr_salesman c 
                            where a.i_periode = '$iperiode' and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman
                            and b.f_area_real='t'
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, sum(v_spb) as v_spb, 0 as v_nota, 0 as v_retur
                            from tm_spb a, tr_area b, tr_salesman c 
                            where to_char(d_spb,'yyyymm') = '$iperiode' and f_spb_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman
                            and b.f_area_real='t'
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, 0 as v_spb, sum(v_nota_gross) as v_nota, 0 as v_retur
                            from tm_nota a, tr_area b, tr_salesman c 
                            where to_char(d_nota,'yyyymm') = '$iperiode' and f_nota_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman
                            and b.f_area_real='t'
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
                            union all
                            select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 
                            0 as v_target, 0 as v_spb, 0 as v_nota, sum(a.v_netto) as v_retur
                            from tm_kn a, tr_area b, tr_salesman c 
                            where to_char(d_kn,'yyyymm') = '$iperiode' and f_kn_cancel='f'
                            and a.i_area=b.i_area and a.i_area=c.i_area and a.i_salesman=c.i_salesman
                            and b.f_area_real='t'
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
                            ) as x
                            group by x.i_area, x.i_salesman, x.e_area_name, x.e_salesman_name
                            order by x.i_area, x.i_salesman ",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
