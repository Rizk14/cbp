<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select("data.i_area, data.i_city_status, e_area_name, e_city_statusname, e_product_categoryname, nota, bersih, e_customer_classname, grosir 
							from (
		  						select h.i_area, e.i_city_status, h.e_area_name, e.e_city_statusname , g.e_product_categoryname, count(distinct b.i_customer) as nota,
		  	             		sum(b.v_nota_netto) as bersih, k.e_customer_classname,  0 as grosir
							  	from tm_nota_item a
							  	left join tm_nota b using (i_nota)
								left join tr_customer c on c.i_customer=b.i_customer
								left join tr_area h on h.i_area=c.i_area
								left join tr_city d on d.i_city=c.i_city and d.i_area=c.i_area
								left join tr_city_status e on e.i_city_status=d.i_city_status
								left join tr_product f on f.i_product=a.i_product
								left join tr_product_category g on g.i_product_category=f.i_product_category
								left join tr_customer_class k on k.i_customer_class=c.i_customer_class
								where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.i_area='$iarea'
								group by e.i_city_status, g.e_product_categoryname, h.i_area, h.e_area_name, k.e_customer_classname, g.i_product_category
								union all
							select h.i_area, e.i_city_status, h.e_area_name, e.e_city_statusname , g.e_product_categoryname, 0 as nota,
		  	             		sum(b.v_nota_netto) as bersih, k.e_customer_classname, count(distinct b.i_customer) as grosir
							  	from tm_nota_item a
							  	left join tm_nota b using (i_nota)
								left join tr_customer c on c.i_customer=b.i_customer
								left join tr_area h on h.i_area=c.i_area
								left join tr_city d on d.i_city=c.i_city and d.i_area=c.i_area
								left join tr_city_status e on e.i_city_status=d.i_city_status
								left join tr_product f on f.i_product=a.i_product
								left join tr_product_category g on g.i_product_category=f.i_product_category
								left join tr_customer_class k on k.i_customer_class=c.i_customer_class
								where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.i_area='$iarea' and k.i_customer_class='3'
								group by e.i_city_status, g.e_product_categoryname, h.i_area, h.e_area_name, k.e_customer_classname, g.i_product_category
								)as data ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
