<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$iarea,$num,$offset)
    {
		$this->db->select(" sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                        sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                        a.i_product, a.e_product_name
                        from tm_op_item b
                        left join tm_do c on (b.i_op=c.i_op)
                        left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do and a.n_deliver>0)
                        where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and c.i_area='$iarea'
                        group by a.i_product, a.e_product_name, c.i_supplier
                        order by a.i_product",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$iarea,$cari,$num,$offset)
    {
			  $this->db->select(" sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                            sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                            a.i_product, a.e_product_name
                            from tm_op_item b
                            left join tm_do c on (b.i_op=c.i_op)
                            left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do and a.n_deliver>0)
                            where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                            and c.i_area='$iarea' and(upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                            group by a.i_product, a.e_product_name, c.i_supplier
                            order by a.i_product",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
