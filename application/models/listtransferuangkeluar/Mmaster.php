<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ikuk,$nkukyear) 
    {
#		$this->db->query("DELETE FROM tm_kuk WHERE i_kuk='$ikuk' and n_kuk_year='$nkukyear' ");
		$this->db->query("update tm_kuk set f_kuk_cancel='t' WHERE i_kuk='$ikuk' and n_kuk_year=$nkukyear ");
    }
    function bacasemua($num,$offset)
    {
		$this->db->select(" * from tm_kuk a
							inner join tr_supplier c on(a.i_supplier=c.i_supplier)
							order by a.n_kuk_year desc, a.i_kuk", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_kuk a
							inner join tr_supplier c on(a.i_supplier=c.i_supplier)
							where upper(a.i_kuk) like '%$cari%'
							order by a.n_kuk_year desc, a.i_kuk", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$teksquery=" distinct a.i_supplier,a.d_kuk, a.f_kuk_cancel, a.i_kuk, a.d_kuk, a.e_bank_name,
          b.e_supplier_name, a.e_remark,a.v_jumlah, a.v_sisa,
          a.f_close,a.n_kuk_year,c.i_pelunasanap, c.d_bukti, c.i_giro
          from tm_kuk a
          left join tr_supplier b on(a.i_supplier=b.i_supplier)
          left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' and
          ((c.i_jenis_bayar!='02' and 
          c.i_jenis_bayar!='01' and 
          c.i_jenis_bayar!='04' and 
          c.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)))
          where (upper(a.i_kuk) like '%$cari%' or upper(a.i_supplier) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and 
          a.i_supplier='$isupplier' and
          (a.d_kuk >= to_date('$dfrom','dd-mm-yyyy') and
          a.d_kuk <= to_date('$dto','dd-mm-yyyy'))
          ORDER BY a.d_kuk, a.i_supplier, a.i_kuk ";
			$this->db->select($teksquery,false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
			$teksquery=" distinct a.i_supplier,a.d_kuk, a.f_kuk_cancel, a.i_kuk, a.d_kuk, a.e_bank_name,
          b.e_supplier_name, a.e_remark,a.v_jumlah, a.v_sisa,
          a.f_close,a.n_kuk_year,c.i_pelunasanap, c.d_bukti, c.i_giro
          from tm_kuk a
          left join tr_supplier b on(a.i_supplier=b.i_supplier)
          left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f')
          where (upper(a.i_kuk) like '%$cari%' or upper(a.i_supplier) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
          ((c.i_jenis_bayar!='02' and 
          c.i_jenis_bayar!='01' and 
          c.i_jenis_bayar!='04' and 
          c.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)) and 
          a.i_supplier='$isupplier' and
          (a.d_kuk >= to_date('$dfrom','dd-mm-yyyy') and
          a.d_kuk <= to_date('$dto','dd-mm-yyyy')) and a.f_kuk_cancel='f'
          ORDER BY a.d_kuk, a.i_supplier, a.i_kuk ";
			$this->db->select($teksquery,false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select(" * from tr_supplier order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
