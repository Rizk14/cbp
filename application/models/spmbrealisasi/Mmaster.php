<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($ispmb, $area)
	{
		$this->db->select(" * from tm_spmb inner join tr_area on (tm_spmb.i_area=tr_area.i_area) where i_spmb ='$ispmb' and tm_spmb.i_area='$area'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($ispmb, $area)
	{
		/* Disabled 14042011
		  $this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
				     where a.i_spmb = '$ispmb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				     order by a.n_item_no", false);
		  */
		$this->db->select(" 		a.*,
									b.e_product_motifname,
									ctg.e_sales_categoryname 
								FROM
									tm_spmb_item a,
									tr_product_motif b,
									tr_product c
									LEFT JOIN tr_product_sales_category ctg ON c.i_sales_category = ctg.i_sales_category 
								WHERE
									a.i_spmb = '$ispmb'
									AND a.i_product = b.i_product
									AND a.i_product_motif = b.i_product_motif
									AND a.i_product = c.i_product 
								ORDER BY
									a.i_product asc ", false);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			$this->db->select("			a.*,
										b.e_product_motifname,
										ctg.e_sales_categoryname,
										0 as n_stock
									FROM
										tm_spmb_item a,
										tr_product_motif b,
										tr_product c
										LEFT JOIN tr_product_sales_category ctg ON c.i_sales_category = ctg.i_sales_category 
									WHERE
										a.i_spmb = '$ispmb'
										AND a.i_product = b.i_product
										AND a.i_product_motif = b.i_product_motif
										AND a.i_product = c.i_product 
									ORDER BY a.i_product asc ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return $query->result();
			}
		}
	}

	function bacaprint($ispmb)
	{
		$this->db->select(" * from tm_spmb
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
							where tm_spmb.i_spmb = '$ispmb'
							order by tm_spmb.i_spmb desc", false);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetailprint($ispmb)
	{
		$this->db->select(" 	*
							FROM
								tm_spmb_item
							INNER JOIN tr_product_motif ON (tm_spmb_item.i_product_motif = tr_product_motif.i_product_motif AND tm_spmb_item.i_product = tr_product_motif.i_product)
							WHERE
								i_spmb = '$ispmb' AND n_acc > 0
							ORDER BY
								tm_spmb_item.i_product ASC", false);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $nacc, $vunitprice, $iproductmotif, $nstock, $iarea, $i, $norder, $eremark, $nqtystock)
	{
		$this->db->set(
			array(
				'i_spmb'					=> $ispmb,
				'i_product'				=> $iproduct,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'n_acc' 					=> $nacc,
				'n_saldo'					=> $nstock,
				'n_stock'					=> $nqtystock,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
				'i_area'					=> $iarea,
				'n_item_no'       => $i,
				'e_remark'        => $eremark,
				'n_order'         => $norder
			)
		);

		$this->db->insert('tm_spmb_item');
	}
	function updatedetail($ispmb, $iproduct, $iproductgrade, $iproductmotif, $nstock, $iarea)
	{
		$this->db->set(
			array(
				'n_stock'					=> $nstock
			)
		);
		$this->db->where('i_spmb', $ispmb);
		$this->db->where('i_product', $iproduct);
		$this->db->where('i_product_grade', $iproductgrade);
		$this->db->where('i_product_motif', $iproductmotif);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spmb_item');
	}

	function updateheader($ispmb, $istore, $istorelocation, $fspmbcancel, $fspmbclose)
	{
		$data = array(
			'i_store'				=> $istore,
			'i_store_location'		=> $istorelocation,
			'f_spmb_close' 			=> $fspmbclose,
			'f_spmb_cancel' 		=> $fspmbcancel,
			'f_spmb_pemenuhan' 		=> 'f',
			'f_op' 					=> 't'
		);
		$this->db->where('i_spmb', $ispmb);
		$this->db->update('tm_spmb', $data);
	}

	function langsungclose($ispmb)
	{
		$data = array(
			'f_spmb_pemenuhan' => 't'
		);
		$this->db->where('i_spmb', $ispmb);
		$this->db->update('tm_spmb', $data);
	}

	public function deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif, $iarea)
	{
		$this->db->query("	DELETE FROM tm_spmb_item WHERE i_spmb='$ispmb' and i_area='$iarea'
					and i_product='$iproduct' and i_product_grade='$iproductgrade' 
					and i_product_motif='$iproductmotif'");
		return TRUE;
	}

	public function delete($ispmb)
	{
		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\'' . $ispmb . '\'');
		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\'' . $ispmb . '\'');
		return TRUE;
	}
	function bacasemua($cari, $num, $offset)
	{
		$this->db->select("	*, case when n_print = 0 then 'Belum' else 'Sudah ' || n_print::text || 'X' end as n_print from tm_spmb 
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area
							and (upper(tr_area.e_area_name) like '%$cari%' 
							or upper(tr_area.i_area) like '%$cari%'))
							where not tm_spmb.i_approve2 isnull
							and tm_spmb.i_store isnull and tm_spmb.i_store_location isnull
							and tm_spmb.f_spmb_cancel='f' and tm_spmb.f_spmb_close='f' and tm_spmb.f_spmb_acc='t'
							and upper(tm_spmb.i_spmb) like '%$cari%' and tm_spmb.i_store isnull
							order by tm_spmb.i_spmb desc", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaproduct($num, $offset, $kdharga)
	{
		$this->db->select("i_product, i_product_grade, e_product_name, v_product_retail
						   from tr_product_price 
						   where i_price_group = '$kdharga'
						   order by i_product, i_product_grade", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacastore($num, $offset, $area, $stockdaerah)
	{
		if ($stockdaerah == 'f') {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area' or i_area='00')
						   and a.i_store=b.i_store
						   order by a.i_store", false)->limit($num, $offset);
		} else {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area')
						   and a.i_store=b.i_store
						   order by a.i_store", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($iarea, $num, $offset)
	{
		$this->db->select(" * from tr_area a 
							left join tr_area_pkp b on
							(a.i_area=b.i_area) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
							left join tr_area_area d on
							(a.i_area=d.i_area) 
							left join tr_area_salesman e on
							(a.i_area=e.i_area)
							left join tr_area_discount f on
							(a.i_area=f.i_area) where a.i_area='$iarea'
							order by a.i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumber()
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_spmb,10,6)) as max from tm_spmb 
				  			where substr(i_spmb,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nospmb  = $terakhir + 1;
			settype($nospmb, "string");
			$a = strlen($nospmb);
			while ($a < 6) {
				$nospmb = "0" . $nospmb;
				$a = strlen($nospmb);
			}
			$nospmb  = "spmb-" . $thbl . "-" . $nospmb;
			return $nospmb;
		} else {
			$nospmb  = "000001";
			$nospmb  = "spmb-" . $thbl . "-" . $nospmb;
			return $nospmb;
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select("  *, case when n_print = 0 then 'Belum' else 'Sudah ' || n_print::text || 'X' end as n_print from tm_spmb 
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
							where not tm_spmb.i_approve2 isnull
							and tm_spmb.i_store isnull and tm_spmb.i_store_location isnull
							and tm_spmb.f_spmb_cancel='f' and tm_spmb.f_spmb_close='f' and tm_spmb.f_spmb_acc='t'
							and (upper(tm_spmb.i_spmb) ilike '%$cari%' or upper(tr_area.e_area_name) ilike '%$cari%' or upper(tm_spmb.i_spmb_old) ilike '%$cari%') order by tm_spmb.i_spmb desc ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caristore($cari, $num, $offset, $stockdaerah)
	{
		if ($stockdaerah == 'f') {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area' or i_area='00')
						   and a.i_store=b.i_store and upper(a.i_store) like '%$cari%' 
						   or upper(a.e_store_name) like '%$cari%' order by a.i_store", FALSE)->limit($num, $offset);
		} else {
			$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area')
						   and a.i_store=b.i_store and upper(a.i_store) like '%$cari%' 
						   or upper(a.e_store_name) like '%$cari%' order by a.i_store", FALSE)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.i_store_location, b.e_store_locationname from tr_store a, tr_store_location b 
					where a.i_store=b.i_store
					  and (upper(a.i_store) like '%$cari%' or upper(a.e_store_name) like '%$cari%'
					or upper(b.i_store_location) like '%$cari%' or upper(b.e_store_locationname) like '%$cari%') 
					order by a.i_store", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariproduct($cari, $num, $offset, $kdstore)
	{
		$this->db->select("  * from tr_product_price
					 where (upper(i_product) like '%$cari%' 
					or upper(e_product_name) like '%$cari%'
					or upper(i_product_grade) like '%$cari%')
					and i_product in(
								 select i_product from tm_ic where i_store = 'AA' or i_store='$kdstore')
					order by i_product, i_product_grade", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function updatespmb($ispmb)
	{
		/* $query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dprint	= $row->c; */

		$this->db->query("update tm_spmb set n_print=n_print+1 where i_spmb='$ispmb'");
	}
}
