<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($tahun,$bulan)
    {
      $tahunprev = intval($tahun) - 1;
      $tahunlalu = strval($tahunprev);
	    /*$this->db->select("	a.i_periode, a.e_area_name, sum(a.vnota)  as vnota, sum(qnota) as qnota from (	
	                        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_name, sum(a.v_nota_netto)  as vnota, 0 as qnota
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_name
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_name, 0 as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_name
                          )as a
                          group by a.i_periode, a.e_area_name
                          order by a.i_periode, a.e_area_name",false);*/

      $this->db->select(" nama , sum(round(nilai)) as nilai, sum(round(nilaiprev)) as nilaiprev from(
                              select 'GENDONGAN' as nama, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilai , 0 as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPG%' and 
                              to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull  
                              and a.i_area = b.i_area
                              union all 

                              select 'TAS' as Nama, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilai , 0 as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPT%' and 
                              to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                              union all 

                              select 'KASUR' as Nama, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilai , 0 as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPK%' and 
                              to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                              union all 

                              select 'BLANKET' as Nama, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilai , 0 as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPB%' and 
                              to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                              union all -- tahun sebelumnya -- 

                              select 'GENDONGAN' as nama, 0 as nilai , sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPG%' and 
                              to_char(b.d_nota,'yyyy') = '$tahunlalu' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull  
                              and a.i_area = b.i_area
                              union all 

                              select 'TAS' as Nama, 0 as nilai , sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPT%' and 
                              to_char(b.d_nota,'yyyy') = '$tahunlalu' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                              union all 

                              select 'KASUR' as Nama, 0 as nilai , sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPK%' and 
                              to_char(b.d_nota,'yyyy') = '$tahunlalu' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                              union all 

                              select 'BLANKET' as Nama, 0 as nilai , sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/ NULLIF(b.v_nota_gross,0))*b.v_nota_discounttotal))) as nilaiprev
                              from tm_nota_item a join tm_nota b on(a.i_nota = b.i_nota)
                              where a.i_product like 'TPB%' and 
                              to_char(b.d_nota,'yyyy') = '$tahunlalu' and to_char(b.d_nota,'mm') between '01' and '$bulan' and b.f_nota_cancel='f' and not b.i_nota isnull
                              and a.i_area = b.i_area
                          ) as a
                          group by a.nama",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
