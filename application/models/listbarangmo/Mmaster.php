<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacabarang($igroup)
    {
      $this->db->select("
       a.*, b.e_product_name, c.e_product_gradename, d.e_price_groupconame
		                      from tr_product b, tr_product_grade c, tr_product_priceco a, tr_price_groupco d 
		                      where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade and 
		                      a.i_price_groupco=d.i_price_groupco and d.i_price_groupco ='$igroup'
		                      order by b.e_product_name, a.i_price_group", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function bacagroup($num,$offset,$igroup,$status){
			if($status=='all'){
				$query = $this->db->select(" * from tr_price_groupco",false)->limit($num,$offset);
			}else{
				$query = $this->db->select(" * from tr_price_groupco where i_price_groupco = '$igroup'",false)->limit($num,$offset);
			}    
			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }}
?>
