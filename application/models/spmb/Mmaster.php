<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispmb)
    {
		$this->db->select("a.*, b.e_area_name from tm_spmb a, tr_area b
						   where a.i_area=b.i_area
						   and i_spmb ='$ispmb'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispmb)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
				   where a.i_spmb = '$ispmb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($ispmb, $dspmb, $iarea, $fop, $nprint, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'i_spmb'		=> $ispmb,
			'd_spmb'		=> $dspmb,
			'i_area'		=> $iarea,
			'f_op'			=> 'f',
			'n_print'		=> 0,
			'i_spmb_old'=> $ispmbold,
      'e_remark'  => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_spmb');
    }
    function insertdetail($ispmb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark,$iarea)
    {
    	$this->db->set(
    		array(
					'i_spmb'			=> $ispmb,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_order'			=> $norder,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'i_area'			=> $iarea,
					'e_remark'			=> $eremark
    		)
    	);
    	
    	$this->db->insert('tm_spmb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spmb_item WHERE i_spmb='$ispmb'
										and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										and i_product_motif='$iproductmotif'");
		return TRUE;
    }
	
    public function delete($ispmb) 
    {
		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_retail as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product
              and a.i_product_motif='00'
              limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function runningnumber($thbl){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPM'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospmb
                            where i_modul='SPM'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
		  	$nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPM','00','$asal',1)");
			  return $nospmb;
		  }
/*
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_spmb,11,6)) as max from tm_spmb 
				    			where substr(i_spmb,6,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }
*/
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product and a.i_product_motif='00'
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
