<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	public function delete($ispb, $iarea)
	{
		return TRUE;
	}
	function bacasemua($allarea, $cari, $num, $offset, $area, $iuser, $area1)
	{
		if (($area == '00')) {
			$this->db->select(" a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and a.i_approve2 isnull
								and not a.i_approve1 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and not a.i_cek isnull  
								and not a.i_customer like '%000'
								and (((a.f_spb_stockdaerah='t' or a.f_spb_stockdaerah='f')/* and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser'))*/)
								or (a.f_spb_stockdaerah='f' and not a.i_approve1 isnull)
								or (a.f_spb_stockdaerah='t' and (b.i_customer_status='4')))
								order by a.d_spb,a.i_area,a.i_spb", false)->limit($num, $offset);
			#								and not a.i_approve1 isnull
		} else {
			$this->db->select(" a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and a.i_approve1 isnull
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and not a.i_cek isnull  
								and a.f_spb_cancel='f'
								/*and not a.i_customer in(select i_customer from v_batasumur)*/
								and not a.i_customer like '%000' 
								and (b.i_customer_status<>'4' or b.i_customer_status isnull)
								and (/*a.f_spb_stockdaerah='t' and */a.i_area in ( select i_area from tm_user_area where i_user='$iuser'))
								order by a.d_spb,a.i_area,a.i_spb", false)->limit($num, $offset);
			# 								and b.i_customer_status<>'4'
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($area1, $area2, $area3, $area4, $area5, $allarea, $cari, $num, $offset)
	{
		$user = $this->session->userdata('user_id');

		if (($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
			$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve2 isnull 
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and not a.i_cek isnull  
							and not a.i_customer like '%000'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%' or upper(a.i_spb_old) like '%$cari' )
							and
						  ( (a.f_spb_stockdaerah='t' and a.i_area in (select i_area from tm_user_area where i_user = '$user'))
						  or	
						    (a.f_spb_stockdaerah='f' and not a.i_approve1 isnull)
              or
    					  (a.f_spb_stockdaerah='t' and (b.i_customer_status='4')))
							order by a.d_spb,a.i_area,a.i_spb ", false)->limit($num, $offset);
			#							and not a.i_approve1 isnull
		} else {
			$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve1 isnull 
							and a.i_approve2 isnull
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and not a.i_cek isnull  
							and not a.i_customer like '%000' and (b.i_customer_status<>'4' or b.i_customer_status isnull)
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%')
							and
							  ((a.f_spb_stockdaerah='t'
							  and a.i_area in (select i_area from tm_user_area where i_user = '$user'))
							  )
							order by a.d_spb,a.i_area,a.i_spb ", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function baca($ispb, $iarea)
	{
		$this->db->select(" * from tm_spb 
				   left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea' order by tm_spb.i_spb desc ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function detailcustomer($ispb, $area)
	{
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, b.d_signin, c.*, d.*, e.*, f.*
						from tm_spb a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e, tr_customer_groupar f
						where a.i_spb = '$ispb' and a.i_area='$area'
						and a.i_customer=b.i_customer and a.i_customer=f.i_customer
						and a.i_salesman=c.i_salesman
						and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
						and b.i_customer_class=d.i_customer_class
						order by a.i_spb desc", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacapiutang($ispb, $area)
	{
		$this->db->select(" i_customer from tm_spb where i_spb = '$ispb' and i_area='$area'", false);
		$quer = $this->db->get();
		$cust = '';
		$saldo = 0;
		if ($quer->num_rows() > 0) {
			foreach ($quer->result() as $rowi) {
				$cust = $rowi->i_customer;
			}
			$this->db->select(" sum(v_sisa) as sisa from tm_nota where i_customer = '$cust' and f_nota_cancel='f' and not i_nota isnull", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$saldo = $row->sisa;
				}
			}
		}
		return $saldo;
	}
	function bacadetail($ispb, $iarea)
	{
		$this->db->select("	a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   			where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product 
							and a.i_product_motif=b.i_product_motif
				   			order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function approve($ispb, $iarea, $eapprove2, $user, $dterima, $etelat)
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$data = array(
			'e_approve2'		=> $eapprove2,
			'd_approve2'		=> $dentry,
			'i_approve2'		=> $user,
			// 'd_terimaspb'		=> $dterima,
			'e_telat2'			=> $etelat
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	function notapprove($ispb, $iarea, $eapprove, $user)
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$data = array(
			'e_notapprove'		=> $eapprove,
			'd_notapprove'		=> $dentry,
			'i_notapprove'		=> $user
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
}
