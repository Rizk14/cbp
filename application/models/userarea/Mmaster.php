<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iuser,$iarea)
    {
		$this->db->select("* from tm_user_area 
					where tm_user_area.i_user = '$iuser'
					  and tm_user_area.i_area = '$iarea'
				   order by tm_user_area.i_user, tm_user_area.i_area", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iuser,$iarea)
    {
    	$this->db->query("insert into tm_user_area (i_user, i_area) values ('$iuser','$iarea')");
		  #redirect('userarea/cform/');
    }

    function update($iuser,$iarea)
    {
		$this->db->query("update tm_user_area set i_user = '$iuser', i_area = '$iarea' ");
		#redirect('userarea/cform/');
    }
	
    public function delete($iuser,$iarea) 
    {
		$this->db->query('DELETE FROM tm_user_area WHERE i_user=\''.$iuser.'\' and i_area=\''.$iarea.'\' ');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		    $this->db->select("* from tm_user_area where upper(tm_user_area.i_area) ilike '%$cari%' ",false)->limit($num,$offset);
       
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
      $this->db->select("* from tm_user_area where upper(tm_user_area.i_user) ilike '%$cari%' 
							or upper(tm_user_area.i_area) ilike '%$cari%'",false)->limit($num,$offset);
      
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
