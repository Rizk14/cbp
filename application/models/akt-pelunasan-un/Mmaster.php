<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tm_pelunasan where f_posting='t'
							order by i_pelunasan desc, i_area ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_giro where f_posting='t'
							and (upper(i_pelunasan) '%$cari%')
							order by i_pelunasan desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function sisa($ipl,$iarea,$idt){
		$this->db->select(" sum(v_sisa+v_jumlah)as sisa
							from tm_pelunasan_item
							where i_pelunasan='$ipl'
							and i_area='$iarea'
							and i_dt='$idt'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->sisa;
		}	
	}
	function jumlahbayar($ipl,$iarea,$idt){
		$this->db->select(" sum(v_jumlah)as jumlah
							from tm_pelunasan_item
							where i_pelunasan='$ipl'
							and i_area='$iarea'
							and i_dt='$idt'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->jumlah;
		}	
	}
	function bacapl($ipl,$iarea,$idt){
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, d.e_jenis_bayarname, e.d_dt, c.e_customer_address, c.e_customer_city
							from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							where a.i_area=b.i_area and a.i_customer=c.i_customer 
							and a.i_jenis_bayar=d.i_jenis_bayar 
							and a.i_dt=e.i_dt and a.i_area=e.i_area
							and upper(a.i_dt)='$idt' and upper(a.i_pelunasan)='$ipl' and upper(a.i_area)='$iarea'",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadetailpl($ipl,$iarea,$idt){
		$this->db->select(" * from tm_pelunasan_item
							where i_pelunasan = '$ipl' 
							and i_area='$iarea'
							and i_dt='$idt'
							order by i_pelunasan,i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader( $ipelunasan,$iarea,$dbukti )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$ipelunasan' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function deletetransitemdebet($accdebet,$ipelunasan,$dbukti)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$accdebet' and i_refference='$ipelunasan' and d_refference='$dbukti'");
	}
	function deletetransitemkredit($acckredit,$ipelunasan,$dbukti)
    {
		$this->db->query("delete from tm_jurnal_transharianitem 
						  where i_coa='$acckredit' and i_refference='$ipelunasan' and d_refference='$dbukti'");
	}
	function deletegldebet($accdebet,$ipelunasan,$iarea,$dbukti)
    {
		$this->db->query("delete from tm_general_ledger 
						  where i_refference='$ipelunasan' and i_coa='$accdebet' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function deleteglkredit($acckredit,$ipelunasan,$iarea,$dbukti)
    {
		$this->db->query("delete from tm_general_ledger
						  where i_refference='$ipelunasan' and i_coa='$acckredit' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function updatepelunasan($ipl,$iarea,$idt)
    {
		$this->db->query("update tm_pelunasan set f_posting='f' where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
