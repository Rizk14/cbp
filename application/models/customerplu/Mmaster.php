<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerplugroup,$icustomerplu)
    {
		$this->db->select("i_customer_plu, i_product, i_customer_plugroup, f_customer_pluaktif from tr_customer_plu where i_customer_plu = '$icustomerplu' and i_customer_plugroup = '$icustomerplugroup'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerplugroup, $icustomerplu, $iproduct)
    {
    	$this->db->set(
    		array(
    			'i_customer_plugroup' 	  => $icustomerplugroup,
    			'i_customer_plu' => $icustomerplu,
			'i_product' => $iproduct,
			'f_customer_pluaktif' => 'TRUE'
    		)
    	);
    	
    	$this->db->insert('tr_customer_plu');
		#redirect('customerplu/cform/');
    }
    function update($icustomerplugroup, $icustomerplu, $iproduct, $fcustomerpluaktif)
    {
		if($fcustomerpluaktif=='on'){
			$fcustomerpluaktif='TRUE';
		}else{
			$fcustomerpluaktif='FALSE';
		}
    	$data = array(
               'i_customer_plugroup' => $icustomerplugroup,
               'i_customer_plu' => $icustomerplu,
	       'i_product' => $iproduct,
	       'f_customer_pluaktif' => $fcustomerpluaktif
            );
		$this->db->where('i_customer_plu =', $icustomerplu);
		$this->db->where('i_customer_plugroup =', $icustomerplugroup);
		$this->db->update('tr_customer_plu', $data); 
		#redirect('customerplu/cform/');
    }
	
    public function delete($icustomerplugroup, $icustomerplu) 
    {
		$this->db->query("DELETE FROM tr_customer_plu WHERE i_customer_plu='$icustomerplu' and i_customer_plugroup='$icustomerplugroup'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("i_customer_plu, i_product, i_customer_plugroup, f_customer_pluaktif from tr_customer_plu where upper(i_customer_plu) like '%$cari%' or upper(i_product) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plu", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_customer_plu, i_product, i_customer_plugroup,f_customer_pluaktif from tr_customer_plu where upper(i_customer_plu) like '%$cari%' or upper(i_product) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plu", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaplugroup($cari, $num,$offset)
    {
		$this->db->select("	i_customer, e_customer_plugroupname, i_customer_plugroup from tr_customer_plugroup 
					where upper(i_customer) like '%$cari%' 
					or upper(e_customer_plugroupname) like '%$cari%' 
					or upper(i_customer_plugroup) like '%$cari%'", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
