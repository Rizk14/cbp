<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$fltr_area	= " ";
		} else {
			$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
		}
					
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area ".$fltr_area."
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%')
							order by a.i_spmb desc",false)->limit($num,$offset);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ispmb)
    {
		$this->db->select(" * from tm_spmb
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
							where tm_spmb.i_spmb = '$ispmb'
							order by tm_spmb.i_spmb desc",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ispmb)
    {
		$this->db->select(" * from tm_spmb_item
							inner join tr_product_motif on (tm_spmb_item.i_product_motif=tr_product_motif.i_product_motif
														and tm_spmb_item.i_product=tr_product_motif.i_product)
							where i_spmb = '$ispmb' order by tm_spmb_item.i_product asc",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$fltr_area	= " ";
		} else {
			$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
		}
					
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area ".$fltr_area."
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%')
							order by a.i_spmb desc",FALSE)->limit($num,$offset);
							
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
