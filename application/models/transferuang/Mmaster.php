<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea, $iku, $tahun)
    {
		$this->db->select("	* from tm_ku a
					left join tr_customer_salesman e on(a.i_customer=e.i_customer and a.i_area=e.i_area and a.i_salesman=e.i_salesman)
					left join tr_supplier b on(a.i_supplier=b.i_supplier)
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					where a.i_ku='$iku' and a.i_area='$iarea' and a.n_ku_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($iarea,$iku,$tahun)
    {
		$this->db->select(" i_ku from tm_ku where i_area='$iarea' and i_ku='$iku' and n_ku_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($iku,$dku,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
				    $isupplier,$isalesman,$eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			=> $iarea,
				'i_ku'				=> $iku,
				'i_customer'		=> $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'		=> $isalesman,
				'i_supplier'		=> $isupplier,
				'd_ku'				=> $dku,
				'd_entry'			=> $dentry,
				'e_bank_name'		=> $ebankname,
				'e_remark'			=> $eremark,
				'n_ku_year'			=> $tahun,
				'v_jumlah'			=> $vjumlah,
				'v_sisa'			=> $vsisa
    		)
    	);
    	
    	$this->db->insert('tm_ku');
    }
    function update($iku,$dku,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
				    $isupplier,$isalesman,$eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			=> $iarea,
				'i_ku'				=> $iku,
				'i_customer'		=> $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'		=> $isalesman,
				'i_supplier'		=> $isupplier,
				'd_ku'				=> $dku,
				'd_entry'			=> $dentry,
				'e_bank_name'		=> $ebankname,
				'e_remark'			=> $eremark,
				'n_ku_year'			=> $tahun,
				'v_jumlah'			=> $vjumlah,
				'v_sisa'			=> $vsisa
    		)
    	);
    	$this->db->where('i_ku',$iku);
    	$this->db->where('i_area',$iarea);
    	$this->db->where('n_ku_year',$tahun);
    	$this->db->update('tm_ku');
    }
	function bacaarea($num,$offset){
		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacacustomer($iarea,$num,$offset){
		$this->db->select(" a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman from tr_customer a
							left join tr_customer_groupar b on(a.i_customer=b.i_customer)
							left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
							where a.i_area='$iarea'
							order by a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function caricustomer($cari,$iarea,$num,$offset){
		$this->db->select(" a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman from tr_customer a
							left join tr_customer_groupar b on(a.i_customer=b.i_customer)
							left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
						   	where a.i_area='$iarea' 
							and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacasupplier($num,$offset){
		$this->db->select(" * from tr_supplier order by i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function carisupplier($cari,$num,$offset){
		$this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
							order by i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
}
?>
