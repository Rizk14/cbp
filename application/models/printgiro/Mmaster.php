<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($igiro,$iarea) 
    {
			$this->db->query("update tm_giro set f_giro_batal='t' WHERE i_giro='$igiro' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		/* Disabled 08042011
		$this->db->select("	* from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_giro <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
		*/
		
		$this->db->select("	* from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro)
							left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
							
							where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
							((tm_pelunasan.i_jenis_bayar!='02' and 
							tm_pelunasan.i_jenis_bayar!='03' and 
							tm_pelunasan.i_jenis_bayar!='04' and 
							tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							a.i_area='$iarea' and
							a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							a.d_giro <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);		
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cetakgiro($iarea,$dfrom,$dto,$cari)
    {	
		$this->db->select("	* from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro)
							left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
							
							where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
							((tm_pelunasan.i_jenis_bayar!='02' and 
							tm_pelunasan.i_jenis_bayar!='03' and 
							tm_pelunasan.i_jenis_bayar!='04' and 
							tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and							
							a.i_area='$iarea' and
							a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and
							a.d_giro <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.d_giro desc, a.i_giro desc",false);		
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }    
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	* from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_giro <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
