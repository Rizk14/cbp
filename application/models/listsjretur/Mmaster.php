<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj, $iarea) 
    {
			$this->db->query("update tm_sjr set f_sjr_cancel='t' WHERE i_sjr='$isj' and i_area='$iarea'");
#####
      $this->db->select(" * from tm_sjr where i_sjr='$isj' and i_area='$iarea'");
			$qry = $this->db->get();
			if ($qry->num_rows() > 0){
        foreach($qry->result() as $qyr){  
          $dsjr=$qyr->d_sjr;
        }
      }
      $th=substr($dsjr,0,4);
		  $bl=substr($dsjr,5,2);
		  $emutasiperiode=$th.$bl;
      $que = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$que->row();
      $istore=$st->i_store;
      if($istore=='AA'){
				$istorelocation		= '01';
			}else{
 				$istorelocation		= '00';
			}
			$istorelocationbin	= '00';
      $this->db->select(" * from tm_sjr_item where i_sjr='$isj' and i_area='$iarea' order by n_item_no");
			$qery = $this->db->get();
			if ($qery->num_rows() > 0){
        foreach($qery->result() as $qyre){  
          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                                        and i_product_motif='$qyre->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                                        order by d_transaction desc, i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $row   		= $queri->row();
            $que 	= $this->db->query("SELECT current_timestamp as c");
	          $ro 	= $que->row();
	          $now	 = $ro->c;
            $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$qyre->i_product','$qyre->i_product_grade','$qyre->i_product_motif',
                                '$istore','$istorelocation','$istorelocationbin', 
                                '$qyre->e_product_name', '$isj', '$now', $qyre->n_quantity_retur, 0, 
                                $row->n_quantity_akhir+$qyre->n_quantity_retur, $row->n_quantity_akhir
                              )
                             ",false);

            $this->db->query(" 
                              UPDATE tm_mutasi set n_mutasi_bbk=n_mutasi_bbk-$qyre->n_quantity_retur, 
                              n_mutasi_git=n_mutasi_git-$qyre->n_quantity_retur,
                              n_saldo_akhir=n_saldo_akhir+$qyre->n_quantity_retur
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                             ",false);
            $this->db->query(" 
                              UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qyre->n_quantity_retur
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif' and i_store='$istore' and i_store_location='$istorelocation' 
                              and i_store_locationbin='$istorelocationbin'
                             ",false);

          }
        }
      }
#####
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_to=b.i_area and a.i_sj_type='01'
								and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
	/*
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') and (a.i_area_to='$area1' or a.i_area_to='$area2' or a.i_area_to='$area3' or a.i_area_to='$area4'
						or a.i_area_to='$area5')
						order by a.i_sj desc",false)->limit($num,$offset);
	*/
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_sjr a, tr_area b
													where a.i_area=b.i_area
													and (upper(a.i_sjr) like '%$cari%' or upper(a.i_sjr_old) like '%$cari%')
													and a.i_area='$iarea' and
													a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sjr <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.i_sjr desc",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													where a.i_area_to=b.i_area and a.i_sj_type='01'
													and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
													and a.i_area_to='$iarea' and
													a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sj <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.i_sj desc",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
