<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    {
      if($dfrom){
        $tmp=explode("-", $dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2];
        $thbl=$th.$bl;
        $dfrom=$th."-".$bl."-".$hr;
      }

      if($dto){
        $tmpn=explode("-", $dto);
        $hri=$tmpn[0];
        $bln=$tmpn[1];
        $thn=$tmpn[2];
        $thnbln=$thn.$bln;
        $dto=$thn."-".$bln."-".$hri;
      }
      $this->load->database('101');
	    $this->db->select("	a.i_periode, sum(a.target) as target,  sum(a.spb) as spb, sum(a.sj) as sj, sum(a.nota)  as nota
                          from
                          (
                          SELECT i_periode, sum(v_target) as target, 0 as spb,0 as sj, 0 as nota
                          from tm_target where i_periode >= '$thbl' and i_periode <= '$thnbln' 
                          group by i_periode
                          union all
                          SELECT to_char(d_spb,'yyyymm') as i_periode, 0 as target, sum(v_spb_after) as spb, 0 as sj, 
                          0 as nota
                          from tm_spb where d_spb >= '$dfrom' and d_spb <= '$dto' and f_spb_cancel='f'
                          group by to_char(d_spb,'yyyymm')
                          union all
                			    SELECT to_char(d_sj,'yyyymm') as i_periode, 0 as target, 0 as spb, sum(v_nota_netto)  as sj,0 as nota 
                          from tm_nota where d_sj >= '$dfrom' and d_sj <= '$dto' and f_nota_cancel='f' 
                          group by to_char(d_sj,'yyyymm')
			                    union all
 			                    SELECT to_char(d_nota,'yyyymm') as i_periode, 0 as target, 0 as spb, 0 as sj, sum(v_nota_netto)  as nota
                          from tm_nota where d_nota >= '$dfrom' and d_nota <='$dto' and f_nota_cancel='f' and not i_nota isnull
                          group by to_char(d_nota,'yyyymm')
                          ) as a
                          group by a.i_periode
                          order by a.i_periode",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
      $this->load->database();
    }
    // function bacaspb($iperiode)
    // {
		//   $this->db->select("	sum(a.v_spb-v_spb_discounttotal) as v_spb_netto from tm_spb a
    //                       inner join tr_area b on(a.i_area=b.i_area and b.f_area_real='t')
    //                       where to_char(a.d_spb,'yyyymm') = '$iperiode' and a.f_spb_cancel='f'
    //                       group by b.i_area order by b.i_area",false);
		//   $query = $this->db->get();
		//   if ($query->num_rows() > 0){
		// 	  return $query->result();
		//   }
    // }
    function bacasj($iperiode)
    {
		  $this->db->select("	sum(v_nota_gross) as v_sj_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f'
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacanota($iperiode)
    {
		  $this->db->select("	sum(v_nota_netto) as v_nota_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f' and not a.i_nota isnull
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatarget($iperiode)
    {
		  $this->db->select("	a.v_target from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($iperiode)
    {
		  $this->db->select("	b.i_area from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetailnota($iarea,$period)
      {
       $this->db->select("  a.i_nota, a.d_nota, a.v_nota_netto, a.i_customer, b.e_customer_name,
                            b.e_customer_address, c.e_city_name, a.i_area, a.i_salesman, d.e_salesman_name
                            from tm_nota a, tr_customer b, tr_city c, tr_salesman d
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and b.i_city=c.i_city
                            and a.i_salesman=d.i_salesman
                            and b.i_area=c.i_area and a.i_area='$iarea' and a.i_nota like 'FP-$period-%'
                            order by a.i_salesman, a.i_nota", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
      }
    function bacadetailkn($iarea,$periode)
      {
       $this->db->select("  a.i_kn, a.d_kn, a.v_netto, a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_area
                            from tm_kn a, tr_customer b, tr_city c
                            where a.i_customer=b.i_customer and b.i_city=c.i_city and b.i_area=c.i_area
                            and a.i_area='$iarea' and to_char(a.d_kn,'yyyymm')='$periode'
                            order by a.i_kn", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
      }
}
?>
