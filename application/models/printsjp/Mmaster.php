<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset,$iuser)
    {
		$this->db->select(" a.*, c.e_area_name from tm_sjp a
                        inner join tr_area c on (a.i_area=c.i_area)
							          where(upper(a.i_sjp) like '%$cari%') and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
                        order by a.i_area, a.i_sjp desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($isj,$area)
    {
		$this->db->select(" tm_sjp.*, tr_area.e_area_name, tm_spmb.i_spmb  from tm_sjp
							          inner join tr_area on (tm_sjp.i_area=tr_area.i_area)
							          left join tm_spmb on (tm_spmb.i_spmb=tm_sjp.i_spmb and tm_spmb.i_area=tm_sjp.i_area)
							          where tm_sjp.i_sjp = '$isj' and tr_area.i_area='$area'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($isj,$area)
    {
		$this->db->select(" * from tm_sjp_item
							          inner join tr_product_motif on (tm_sjp_item.i_product_motif=tr_product_motif.i_product_motif
														          and tm_sjp_item.i_product=tr_product_motif.i_product)
							          where i_sjp = '$isj' and i_area='$area' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.*, c.e_area_name from tm_sjp a
                        inner join tr_area c on (a.i_area=c.i_area)
							          where (upper(a.i_sjp) like '%$cari%') 
							          order by a.i_sjp desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
