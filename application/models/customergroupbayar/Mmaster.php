<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomer)
    {
		$this->db->select(" a.i_customer, b.e_customer_name, a.i_customer_groupbayar as i_group
                        from tr_customer_groupbayar a, tr_customer b
                        where a.i_customer=b.i_customer and a.i_customer = '$icustomer' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomer, $ecustomername, $igroup)
    {
    	$this->db->set(
    		array(
    			'i_customer' 	  	        => $icustomer,
    			'i_customer_groupbayar'	  => $igroup
    		)
    	); 	
    	$this->db->insert('tr_customer_groupbayar');
    	$this->db->set(
    		array(
    			'i_customer' 	  	      => $icustomer,
    			'i_customer_groupar'	  => $igroup
    		)
    	); 	
    	$this->db->insert('tr_customer_groupar');
		  #redirect('customergroupbayar/cform/');
    }
    function update($icustomer, $ecustomername, $igroup)
    {
    	$data = array(
				'i_customer'              => $icustomer,
				'i_customer_groupbayar'   => $igroup
            );
		  $this->db->where('i_customer =', $icustomer);
		  $this->db->update('tr_customer_groupbayar', $data); 

    	$data = array(
				'i_customer'           => $icustomer,
				'i_customer_groupar'   => $igroup
            );
		  $this->db->where('i_customer =', $icustomer);
		  $this->db->update('tr_customer_groupar', $data);

		  #redirect('customergroupbayar/cform/');
    }
	
    public function delete($icustomer) 
    {
		  $this->db->query("DELETE FROM tr_customer_groupbayar WHERE i_customer='$icustomer'", false);
		  return TRUE;
		  $this->db->query("DELETE FROM tr_customer_groupar WHERE i_customer='$icustomer'", false);
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		if ($cari != "all")
			$this->db->select(" a.i_customer, a.i_customer_groupbayar as i_group
                        from tr_customer_groupbayar a
                        where upper(a.i_customer_groupbayar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                        order by a.i_customer", false)->limit($num,$offset);
        else
			$this->db->select(" a.i_customer, a.i_customer_groupbayar as i_group
                        from tr_customer_groupbayar a
                        order by a.i_customer", false)->limit($num,$offset);
        
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" a.i_customer, a.i_customer_groupbayar as i_group
                          from tr_customer_groupbayar a
                          where upper(a.i_customer_groupbayar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                          order by a.i_customer", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacapelanggan($num,$offset) {
		  $this->db->select(" * from tr_customer order by i_area, e_customer_name asc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
	  }
    function caripelanggan($cari,$num,$offset) {
		if ($cari != "all")
			$this->db->select("* from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by i_area, e_customer_name asc ",false)->limit($num,$offset);
		else
			$this->db->select(" * from tr_customer order by i_area, e_customer_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacagroup($num,$offset) {
		  $this->db->select(" i_customer as i_group, e_customer_name from tr_customer order by i_area, e_customer_name asc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
	  }
    function carigroup($cari,$num,$offset) {
		$this->db->select(" i_customer as i_group, e_customer_name from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by i_area, e_customer_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}		
}
?>
