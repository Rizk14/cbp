<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto,$group)
    { 

        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $thblfrom = $thnow.$bl;
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hri=$tmp[0];
        $bln=$tmp[1];
        $th=$tmp[2]-1;
        $thn = $tmp[2];
        $thblto = $thn.$bln;
        $dtoprev=$hri."-".$bln."-".$th;

      if($group=='NA'){
	    $sql=" a.i_periode ,sum(ob) as ob , sum(oa) as oa , sum(qty) as qty, sum(vnota) as vnota , sum(oaprev)as oaprev,sum(qtyprev) as qtyprev,sum(vnotaprev) as vnotaprev from(

  -- Hitung OB Group
    select substring(a.i_periode,5,2) as i_periode , count(ob) as ob, 0 as oa, 0 as qty, 0 as vnota , 0 as oaprev , 0 as qtyprev ,0 as vnotaprev from (
    SELECT distinct i_customer as ob, e_periode as i_periode,i_salesman
                          from tr_customer_salesman
                          where e_periode >='$thblfrom' and e_periode <='$thblto'
    ) as a
    group by a.i_periode 
    union all
  --Hitung OA
        select a.i_periode,0 as ob,count(oa) as oa, 0 as qty, 0 as vnota , 0 as oaprev , 0 as qtyprev ,0 as vnotaprev from (
        select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'mm') as i_periode
        from tm_nota a where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        ) as a
        group by a.i_periode
  union all
  --Hitung Qty 
        select to_char(a.d_nota,'mm') as i_periode,0 as ob,0 as oa,sum(c.n_deliver) as qty,0 as vnota,0 as oaprev,0 as qtyprev,0 as vnotaprev
        from tm_nota a,tm_nota_item c
        where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        group by to_char(a.d_nota,'mm')
  union all
  --Hitung Nota
        select to_char(a.d_nota,'mm') as i_periode,0 as ob,0 as oa , 0 as qty ,sum(a.v_nota_netto) as vnota,0 as oaprev , 0 as qtyprev ,0 as vnotaprev
        from tm_nota a
        where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull
        group by to_char(a.d_nota,'mm')
        union all
 --Hitung OA Prevth
        select a.i_periode,0 as ob,0 as oa, 0 as qty, 0 as vnota ,count(oa) as oaprev , 0 as qtyprev ,0 as vnotaprev from (
        select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'mm') as i_periode
        from tm_nota a where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        ) as a
        group by a.i_periode
  union all
  --Hitung Qty Prevth
        select to_char(a.d_nota,'mm') as i_periode,0 as ob,0 as oa,0 as qty,0 as vnota,0 as oaprev , sum(c.n_deliver) as qtyprev ,0 as vnotaprev
        from tm_nota a,tm_nota_item c
        where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        group by to_char(a.d_nota,'mm')
  union all
  --Hitung Nota Prevth
        select to_char(a.d_nota,'mm') as i_periode,0 as ob,0 as oa , 0 as qty ,0 as vnota,0 as oaprev , 0 as qtyprev ,sum(a.v_nota_netto) as vnotaprev
        from tm_nota a
        where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull
        group by to_char(a.d_nota,'mm')
        
 ) as a 
 group by a.i_periode 
 order by a.i_periode";
    }else{
      $sql ="a.i_periode , sum(oa) as oa , sum(qty) as qty, sum(vnota) as vnota , sum(oaprev)as oaprev,sum(qtyprev) as qtyprev,sum(vnotaprev) as vnotaprev from(
  --Hitung OA
        select a.i_periode,count(oa) as oa, 0 as qty, 0 as vnota , 0 as oaprev , 0 as qtyprev ,0 as vnotaprev from (
        select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'mm') as i_periode
        from tm_nota a where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        ) as a
        group by a.i_periode
  union all
  --Hitung Qty 
        select to_char(a.d_nota,'mm') as i_periode,0 as oa,sum(c.n_deliver) as qty,0 as vnota,0 as oaprev,0 as qtyprev,0 as vnotaprev
        from tm_nota a,tm_nota_item c
        where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        group by to_char(a.d_nota,'mm')
  union all
  --Hitung Nota
        select to_char(a.d_nota,'mm') as i_periode,0 as oa , 0 as qty ,sum(a.v_nota_netto) as vnota,0 as oaprev , 0 as qtyprev ,0 as vnotaprev
        from tm_nota a
        where (a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <=to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull
        group by to_char(a.d_nota,'mm')
        union all
 --Hitung OA Prevth
        select a.i_periode,0 as oa, 0 as qty, 0 as vnota ,count(oa) as oaprev , 0 as qtyprev ,0 as vnotaprev from (
        select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'mm') as i_periode
        from tm_nota a where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        ) as a
        group by a.i_periode
  union all
  --Hitung Qty Prevth
        select to_char(a.d_nota,'mm') as i_periode,0 as oa,0 as qty,0 as vnota,0 as oaprev , sum(c.n_deliver) as qtyprev ,0 as vnotaprev
        from tm_nota a,tm_nota_item c
        where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        group by to_char(a.d_nota,'mm')
  union all
  --Hitung Nota Prevth
        select to_char(a.d_nota,'mm') as i_periode,0 as oa , 0 as qty ,0 as vnota,0 as oaprev , 0 as qtyprev ,sum(a.v_nota_netto) as vnotaprev
        from tm_nota a
        where (a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull
        group by to_char(a.d_nota,'mm')
        
 ) as a 
 group by a.i_periode 
 order by a.i_periode";

    }
      $this->db->select($sql,FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaisland($tahun)
    {
	    $this->db->select("	a.i_periode, a.e_area_island, sum(a.vnota)  as vnota, sum(qnota) as qnota from (
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, sum(a.v_nota_netto)  as vnota, 0 as qnota
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          ) as a
                          group by a.i_periode, a.e_area_island
                          order by a.e_area_island ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaproductgroup()
    {
      $this->db->select(" * from tr_product_group",false);
    
      $query = $this->db->get();
    
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

}
?>
