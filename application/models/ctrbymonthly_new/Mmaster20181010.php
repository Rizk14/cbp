<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
    parent::__construct();
		#$this->CI =& get_instance();
  }
  function baca($dfrom,$dto,$group)
  { 

    $tmp=explode("-",$dfrom);
    $hr=$tmp[0];
    $bl=$tmp[1];
    $th=$tmp[2]-1;
    $thnow=$tmp[2];
    $thblfrom = $thnow.$bl;
    $dfromprev=$hr."-".$bl."-".$th;
    
        $tsasih = date('Y-m', strtotime('-6 month', strtotime($thblfrom))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
          $smn = explode("-", $tsasih);
          $thn = $smn[0];
          $bln = $smn[1];
        }
        $taunsasih = $thn.$bln;
        
        $tmp=explode("-",$dto);
        $hri=$tmp[0];
        $bln=$tmp[1];
        $th=$tmp[2]-1;
        $thn = $tmp[2];
        $thblto = $thn.$bln;
        $dtoprev=$hri."-".$bln."-".$th;

        if($group=='NA'){
         $sql=" a.i_periode, sum(ob) as ob, sum(oa) as oa, sum(qty) as qty, sum(vnota) as vnota, sum(oaprev)as oaprev, sum(qtyprev) as qtyprev, sum(vnotaprev) as vnotaprev from(
         --Hitung OA
         select a.i_periode, 0 as ob, count(oa) as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev from (
         select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode
         from tm_nota a, tr_customer b, tm_spb z where 
         (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
         and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='false'
         and not a.i_nota isnull 
         and a.i_customer=b.i_customer 
         and a.i_area=b.i_area 
         and b.i_customer_status<>'4' 
         and b.f_customer_aktif='true'
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         ) as a
         group by a.i_periode
         union all
         --Hitung Qty 
         select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, sum(c.n_deliver) as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev
         from tm_nota a, tm_nota_item c, tm_spb z
         where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
         and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='f' 
         and not a.i_nota isnull 
         and a.i_nota=c.i_nota 
         and a.i_area=c.i_area
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         group by to_char(a.d_nota, 'mm')
         union all
         --Hitung Nota
         select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, sum(a.v_nota_netto) as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev
         from tm_nota a, tm_spb z
         where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
         and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='f' 
         and not a.i_nota isnull
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         group by to_char(a.d_nota, 'mm')
         union all
         --Hitung OA Prevth
         select a.i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, count(oa) as oaprev, 0 as qtyprev, 0 as vnotaprev from (
         select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode
         from tm_nota a, tr_customer b, tm_spb z where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
         and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='false' 
         and a.i_customer=b.i_customer 
         and a.i_area=b.i_area 
         and b.i_customer_status<>'4' 
         and b.f_customer_aktif='true'
         and not a.i_nota isnull
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         ) as a
         group by a.i_periode
         union all
         --Hitung Qty Prevth
         select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, sum(c.n_deliver) as qtyprev, 0 as vnotaprev
         from tm_nota a, tm_nota_item c, tm_spb z
         where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='f' 
         and not a.i_nota isnull 
         and a.i_nota=c.i_nota 
         and a.i_area=c.i_area 
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         group by to_char(a.d_nota, 'mm')
         union all
         --Hitung Nota Prevth
         select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, sum(a.v_nota_netto) as vnotaprev
         from tm_nota a, tm_spb z
         where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
         and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
         and a.f_nota_cancel='f' 
         and not a.i_nota isnull
         and a.i_nota = z.i_nota
         and a.i_spb = z.i_spb
         and a.i_area = z.i_area
         and a.i_customer = z.i_customer
         and not z.i_nota isnull
         and not z.i_spb isnull
         and z.f_spb_cancel = 'f'
         -- and f_spb_consigment = 't'
         group by to_char(a.d_nota, 'mm')
         
         ) as a 
         group by a.i_periode 
         order by a.i_periode";
       }elseif($group=='MO'){
        $sql = " a.i_periode, sum(ob) as ob, sum(oa) as oa, sum(qty) as qty, sum(vnota) as vnota, sum(oaprev)as oaprev, sum(qtyprev) as qtyprev, sum(vnotaprev) as vnotaprev from(
        --Hitung OA
        select a.i_periode, 0 as ob, count(oa) as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev from (
        select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode
        from tm_nota a, tr_customer b, tm_spb z where 
        (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        and a.i_customer=b.i_customer 
        and a.i_area=b.i_area 
        and b.i_customer_status<>'4' 
        and b.f_customer_aktif='true'
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        ) as a
        group by a.i_periode
        union all
        --Hitung Qty 
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, sum(c.n_deliver) as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev
        from tm_nota a, tm_nota_item c, tm_spb z
        where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull 
        and a.i_nota=c.i_nota 
        and a.i_area=c.i_area
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        group by to_char(a.d_nota, 'mm')
        union all
        --Hitung Nota
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, sum(a.v_nota_netto) as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev
        from tm_nota a, tm_spb z
        where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        group by to_char(a.d_nota, 'mm')
        union all
        --Hitung OA Prevth
        select a.i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, count(oa) as oaprev, 0 as qtyprev, 0 as vnotaprev from (
        select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode
        from tm_nota a, tr_customer b, tm_spb z where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='false' 
        and a.i_customer=b.i_customer 
        and a.i_area=b.i_area 
        and b.i_customer_status<>'4' 
        and b.f_customer_aktif='true'
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        ) as a
        group by a.i_periode
        union all
        --Hitung Qty Prevth
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, sum(c.n_deliver) as qtyprev, 0 as vnotaprev
        from tm_nota a, tm_nota_item c, tm_spb z
        where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull 
        and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        group by to_char(a.d_nota, 'mm')
        union all
        --Hitung Nota Prevth
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, sum(a.v_nota_netto) as vnotaprev
        from tm_nota a, tm_spb z
        where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 't'
        group by to_char(a.d_nota, 'mm')
        
        ) as a 
        group by a.i_periode 
        order by a.i_periode
        ";
      }else{
        $sql ="a.i_periode, sum(ob) as ob, sum(oa) as oa, sum(qty) as qty, sum(vnota) as vnota, sum(oaprev)as oaprev, sum(qtyprev) as qtyprev, sum(vnotaprev) as vnotaprev, a.i_product_group from(
        --Hitung OA
        select a.i_periode, 0 as ob, count(oa) as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev, a.i_product_group from (
        select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode, z.i_product_group
        from tm_nota a, tr_customer b, tm_spb z where
        (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='false'
        and not a.i_nota isnull 
        and a.i_customer=b.i_customer 
        and a.i_area=b.i_area 
        and b.i_customer_status<>'4' 
        and b.f_customer_aktif='true'
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        ) as a
        group by a.i_periode, a.i_product_group
        union all
        --Hitung Qty 
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, sum(c.n_deliver) as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev, z.i_product_group
        from tm_nota a, tm_nota_item c, tm_spb z
        where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull 
        and a.i_nota=c.i_nota 
        and a.i_area=c.i_area
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        group by to_char(a.d_nota, 'mm'), z.i_product_group
        union all
        --Hitung Nota
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, sum(a.v_nota_netto) as vnota, 0 as oaprev, 0 as qtyprev, 0 as vnotaprev, z.i_product_group
        from tm_nota a, tm_spb z
        where (a.d_nota>=to_date('$dfrom', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dto', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        group by to_char(a.d_nota, 'mm'), z.i_product_group
        union all
        --Hitung OA Prevth
        select a.i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, count(oa) as oaprev, 0 as qtyprev, 0 as vnotaprev, a.i_product_group from (
        select distinct on (to_char(a.d_nota, 'yyyymm'), a.i_customer) a.i_customer as oa, to_char(a.d_nota, 'mm') as i_periode, z.i_product_group
        from tm_nota a, tr_customer b, tm_spb z where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='false' 
        and a.i_customer=b.i_customer 
        and a.i_area=b.i_area 
        and b.i_customer_status<>'4' 
        and b.f_customer_aktif='true'
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        ) as a
        group by a.i_periode, a.i_product_group
        union all
        --Hitung Qty Prevth
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, sum(c.n_deliver) as qtyprev, 0 as vnotaprev, z.i_product_group
        from tm_nota a, tm_nota_item c, tm_spb z
        where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull 
        and a.i_nota=c.i_nota 
        and a.i_area=c.i_area 
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        group by to_char(a.d_nota, 'mm'), z.i_product_group
        union all
        --Hitung Nota Prevth
        select to_char(a.d_nota, 'mm') as i_periode, 0 as ob, 0 as oa, 0 as qty, 0 as vnota, 0 as oaprev, 0 as qtyprev, sum(a.v_nota_netto) as vnotaprev, z.i_product_group
        from tm_nota a, tm_spb z
        where (a.d_nota>=to_date('$dfromprev', 'dd-mm-yyyy') 
        and a.d_nota <=to_date('$dtoprev', 'dd-mm-yyyy')) 
        and a.f_nota_cancel='f' 
        and not a.i_nota isnull
        and a.i_nota = z.i_nota
        and a.i_spb = z.i_spb
        and a.i_area = z.i_area
        and a.i_customer = z.i_customer
        and not z.i_nota isnull
        and not z.i_spb isnull
        and z.f_spb_cancel = 'f'
        and f_spb_consigment = 'f'
        group by to_char(a.d_nota, 'mm'), z.i_product_group
        
        ) as a where a.i_product_group = '$group'
        group by a.i_periode, a.i_product_group
        order by a.i_periode
        ";

      }
      $this->db->select($sql,FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
       return $query->result();
     }
   }
   function bacaisland($tahun)
   {
     $this->db->select("	a.i_periode, a.e_area_island, sum(a.vnota)  as vnota, sum(qnota) as qnota from (
      select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, sum(a.v_nota_netto)  as vnota, 0 as qnota
      from tm_nota a, tr_area b
      where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
      group by to_char(a.d_nota,'yyyy'), b.e_area_island
      union all
      select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 0  as vnota, sum(c.n_deliver) as qnota
      from tm_nota a, tr_area b, tm_nota_item c
      where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
      and a.i_sj=c.i_sj and a.i_area=c.i_area
      group by to_char(a.d_nota,'yyyy'), b.e_area_island
      ) as a
      group by a.i_periode, a.e_area_island
      order by a.e_area_island ",false);
     $query = $this->db->get();
     if ($query->num_rows() > 0){
       return $query->result();
     }
   }

   function bacaproductgroup()
   {
    $this->db->select(" * from tr_product_group",false);
    
    $query = $this->db->get();
    
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }

  function bacaob($dfrom,$dto,$group)
  { 
    $tmp=explode("-",$dfrom);
    $hr=$tmp[0];
    $bl=$tmp[1];
    $th=$tmp[2]-1;
    $thnow=$tmp[2];
    $thbl=$thnow."-".$bl;
    $dfromprev=$hr."-".$bl."-".$th;
        $tsasih = date('Y-m', strtotime('-24 month', strtotime($thbl))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
          $smn = explode("-", $tsasih);
          $thn = $smn[0];
          $bln = $smn[1];
        }
        $taunsasih = $thn.$bln;

        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnya=$tmp[2];
        $thblto=$thnya.$bl;
        $dtoprev=$hr."-".$bl."-".$th;


      /*$this->db->select(" count(a.ob) as ob from (
                          SELECT distinct i_customer as ob, e_periode,i_salesman
                          from tr_customer_salesman
                          where e_periode ='$thblto'
                          ) as a",false);*/
                          if($group == "NA"){
                            $sql = "count(ob) as ob from (
                            select distinct on (a.ob) a.ob as ob, a.i_area, a.e_area_name ,a.e_area_island , a.e_provinsi from (
                            select a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                            from tm_nota a , tr_area c
                            where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                            and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
                            union all
                            select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                            from tr_customer b, tr_area c
                            where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
                            ) as a 
                            ) as a";
                          }elseif($group == "MO"){
                            $sql = "count(ob) as ob from (
                            select distinct on (a.ob) a.ob as ob, a.i_area, a.e_area_name, a.e_area_island, a.e_provinsi from (
                            select a.i_customer as ob, a.i_area, c.e_area_name, c.e_area_island, c.e_provinsi 
                            from tm_nota a, tr_area c, tm_spb z
                            where to_char(a.d_nota, 'yyyymm')>='$taunsasih' and to_char(a.d_nota, 'yyyymm') <='$thblto' 
                            and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
                            and a.i_spb = z.i_spb
                            and a.i_area = z.i_area
                            and a.i_customer = z.i_customer
                            and not z.i_nota isnull
                            and not z.i_spb isnull
                            and z.f_spb_cancel = 'f'
                            and f_spb_consigment = 't'
                            and a.i_area='PB'
                            ) as a 
                            ) as a
                            ";
                          }elseif($group == "01"){
                            $sql = "count(ob) as ob, a.i_product_group from (
                            select distinct on (a.ob) a.ob as ob, a.i_area, a.e_area_name, a.e_area_island, a.e_provinsi, a.i_product_group from (
                            select a.i_customer as ob, a.i_area, c.e_area_name, c.e_area_island, c.e_provinsi, z.i_product_group
                            from tm_nota a, tr_area c, tm_spb z
                            where to_char(a.d_nota, 'yyyymm')>='$taunsasih' and to_char(a.d_nota, 'yyyymm') <='$thblto' 
                            and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
                            and a.i_spb = z.i_spb
                            and a.i_area = z.i_area
                            and a.i_customer = z.i_customer
                            and not z.i_nota isnull
                            and not z.i_spb isnull
                            and z.f_spb_cancel = 'f'
                            and f_spb_consigment = 'f'
                            union all
                            select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi, '' as i_product_group
                            from tr_customer b, tr_area c
                            where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
                            ) as a 
                            ) as a where a.i_product_group = '$group'
                            group by a.i_product_group
                            ";
                          }else{
                            $sql = "count(ob) as ob, a.i_product_group from (
                            select distinct on (a.ob) a.ob as ob, a.i_area, a.e_area_name, a.e_area_island, a.e_provinsi, a.i_product_group from (
                            select a.i_customer as ob, a.i_area, c.e_area_name, c.e_area_island, c.e_provinsi , z.i_product_group
                            from tm_nota a, tr_area c, tm_spb z
                            where to_char(a.d_nota, 'yyyymm')>='$taunsasih' and to_char(a.d_nota, 'yyyymm') <='$thblto' 
                            and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
                            and a.i_spb = z.i_spb
                            and a.i_area = z.i_area
                            and a.i_customer = z.i_customer
                            and not z.i_nota isnull
                            and not z.i_spb isnull
                            and z.f_spb_cancel = 'f'
                            and f_spb_consigment = 'f'
                            ) as a 
                            ) as a where a.i_product_group = '$group' 
                            group by a.i_product_group
                            ";
                          }
                          $this->db->select($sql,false);
                          $query = $this->db->get();
                          if ($query->num_rows() > 0){
                            return $query->result();
                          }
                        }

                      }
                      ?>
