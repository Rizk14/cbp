<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikb,$icoabank) 
    {
		$this->db->query("UPDATE tm_kbank 
						  SET f_kbank_cancel='t'
						  WHERE i_kbank='$ikb' AND i_periode='$iperiode' AND i_area='$iarea' AND f_posting='f' AND f_close='f' AND i_coa_bank='$icoabank'");
#####UnPosting      
      	$this->db->query("DELETE FROM th_jurnal_transharian
      	                  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("DELETE FROM th_jurnal_transharianitem
      	                  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("DELETE FROM th_general_ledger
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");

		#INSERT
      	$this->db->query("INSERT INTO th_jurnal_transharian SELECT * FROM tm_jurnal_transharian 
      	                  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("INSERT INTO th_jurnal_transharianitem SELECT * FROM tm_jurnal_transharianitem 
      	                  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("INSERT INTO th_general_ledger SELECT * FROM tm_general_ledger
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");

		#GET MUTASI DI TM GENERAL LEDGER
      	$quer = $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') AS periode 
                                  FROM tm_general_ledger
								  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
								  
		#UPDATE TO TM COA SALDO
  	  	if($quer->num_rows()>0){
        	foreach($quer->result() as $xx){
        	  	$this->db->query("UPDATE tm_coa_saldo 
				  				  SET v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
        	                      v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
        	                      v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
        	                      WHERE i_coa='$xx->i_coa' AND i_periode='$xx->periode'");
        	}
		}
		  
		#DELETE JURNAL 
      	$this->db->query("DELETE FROM tm_jurnal_transharian WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("DELETE FROM tm_jurnal_transharianitem WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
      	$this->db->query("DELETE FROM tm_general_ledger WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
		  
		#CEK PV ITEM 
		$quer = $this->db->query("SELECT i_pv, i_area, v_pv, i_pv_type, i_coa_bank FROM tm_pv_item
								  WHERE i_kk='$ikb' AND i_coa_bank='$icoabank' AND i_pv_type='02'");
		
		#UPDATE JUMLAH PV DAN PV CANCEL
  	  	if($quer->num_rows()>0){
      	  	foreach($quer->result() as $xx){
      	  	  	$this->db->query("UPDATE tm_pv SET v_pv=v_pv-$xx->v_pv
      	  	                      WHERE i_pv='$xx->i_pv' AND i_coa='$xx->i_coa_bank' AND i_pv_type='$xx->i_pv_type'");
#     	  	   	$this->db->query("delete from tm_pv_item where i_kk='$ikb' and i_pv='$xx->i_pv' and i_coa_bank='$xx->i_coa_bank' and i_pv_type='$xx->i_pv_type'");
      	  	  	$this->db->query("UPDATE tm_pv_item SET f_pv_cancel='t' WHERE i_kk='$ikb' AND i_pv='$xx->i_pv' AND i_coa_bank='$xx->i_coa_bank' AND i_pv_type='$xx->i_pv_type'");
      	  	}
		}
		
		#CEK RV ITEM
      	$quer 	= $this->db->query("SELECT i_rv, i_area, v_rv, i_rv_type, i_coa_bank FROM tm_rv_item
									WHERE i_kk='$ikb' AND i_coa_bank='$icoabank' AND i_rv_type='02'");
		
		#UPDATE JUMLAH RV DAN RV CANCEL
  	  	if($quer->num_rows()>0){
        	foreach($quer->result() as $xx){
        	  	$this->db->query("UPDATE tm_rv SET v_rv=v_rv-$xx->v_rv
        	                      WHERE i_rv='$xx->i_rv' AND i_coa='$xx->i_coa_bank' AND i_rv_type='$xx->i_rv_type'");
#       	   	$this->db->query("delete from tm_rv_item where i_kk='$ikb' and i_rv='$xx->i_rv' and i_coa_bank='$xx->i_coa_bank' and i_rv_type='$xx->i_rv_type'");
        	  	$this->db->query("UPDATE tm_rv_item SET f_rv_cancel='t' WHERE i_kk='$ikb' AND i_rv='$xx->i_rv' AND i_coa_bank='$xx->i_coa_bank' AND i_rv_type='$xx->i_rv_type'");
        	}
      	}
#####
    }
 /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area
                and a.f_kb_cancel='f'
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    	if($iarea == 'NA'){
		$this->db->select("	a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, a.v_sisa,
							          a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.i_coa_bank, c.i_bank,
							          b.e_area_name, a.i_coa_bank, c.i_bank, a.f_kbank_cancel, a.d_entry  from tm_kbank a, tr_area b, tr_bank c
							          where (upper(a.i_kbank) like '%$cari%') and a.i_coa_bank=c.i_coa
									  and a.i_area=b.i_area 
									  --and a.f_kbank_cancel='f'
							          and 
							          a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bank <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_kbank ",false);
    	}else{

		$this->db->select("	a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, a.v_sisa,
							          a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.i_coa_bank, c.i_bank,
							          b.e_area_name, a.i_coa_bank, c.i_bank, a.f_kbank_cancel, a.d_entry  from tm_kbank a, tr_area b, tr_bank c
							          where (upper(a.i_kbank) like '%$cari%') and a.i_coa_bank=c.i_coa
									  and a.i_area=b.i_area 
									  --and a.f_kbank_cancel='f'
							          and a.i_area='$iarea' and
							          a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bank <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_kbank ",false);
    	}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
