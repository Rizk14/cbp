<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	public function delete($ispb, $iarea, $eremark, $ireffadvo)
	{
		$this->db->query("UPDATE tm_spb set f_spb_cancel='t',e_batal='$eremark' WHERE i_spb='$ispb' and i_area='$iarea'");

		/* UPDATE STATUS SPB DI SALESFORCE (13 MEI 2023)  */
		$db2 = $this->load->database('salesforce', TRUE);
		$db2->query(" UPDATE tbl_spb SET f_spb_cancel = 't' WHERE i_spb = '$ireffadvo' AND i_company = '5' ");
		/* ******************************************** */

		return TRUE;
	}
	public function balik($ispb, $area)
	{
		$this->db->query("UPDATE tm_spb set i_approve1=NULL,i_approve2=NULL,d_approve1=NULL,d_approve2=NULL,e_approve1=NULL,e_approve2=NULL
		  					WHERE i_spb='$ispb' and i_area='$area'");
		return TRUE;
	}
	function updatebon($ispb, $area)
	{
		$this->db->query("UPDATE tm_notapb set i_spb=null, f_spb_rekap='f' WHERE i_spb='$ispb' and i_area='$area'");
	}
	function bacasemua($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($this->session->userdata('level') == '0') {
			$sql = "	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					  where a.i_customer=b.i_customer
					  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					  or upper(a.i_spb) like '%$cari%')
					  and a.i_area=c.i_area
					  order by a.i_spb";
		} else {
			$sql = "	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					  where a.i_customer=b.i_customer
					  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					  or upper(a.i_spb) like '%$cari%')
					  and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					  or a.i_area='$area5') and a.i_area=c.i_area
					  order by a.i_spb";
		}
		$this->db->select($sql, FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($this->session->userdata('level') == '0') {
			$sql = "	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					  where a.i_customer=b.i_customer
					  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					  or upper(a.i_spb) like '%$cari%')
					  and a.i_area=c.i_area
					  order by a.i_spb desc ";
		} else {
			$sql = "	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					  where a.i_customer=b.i_customer
					  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					  or upper(a.i_spb) like '%$cari%')
					  and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					  or a.i_area='$area5') and a.i_area=c.i_area
					  order by a.i_spb desc ";
		}
		$this->db->select($sql, FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($iarea, $dfrom, $dto, $offset, $cari)
	{
		if ($cari == '') {
			$sql = " a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman, a.i_spb_refference,
                a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname, d.v_nota_netto, d.i_nota, d.i_sj,
                to_char(d.d_nota,'dd-mm-yyyy') as d_nota, d.d_dkb , d.d_sj, a.d_approve1 as d_appsales, a.d_approve2 as d_appar, d.d_sj_receive, a.i_reff_advo
                from tm_spb a 
					      left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
					      left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
					      left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area 
                and x.i_customer like '%000')
					      , tr_area c
                where 
                a.i_area=c.i_area and ";
			if ($iarea != 'NA') {
				$sql .= " a.i_area='$iarea' and ";
			}
			$sql .= " (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) ";
			$sql .= " order by a.i_area, a.i_spb ";
			$this->db->select($sql, false)->limit($offset);
		} else {
			$sql = " a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                            a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                            a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,  a.i_spb_refference,
                            a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                            b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname, d.v_nota_netto, d.i_nota, d.i_sj,
                            to_char(d.d_nota,'dd-mm-yyyy') as d_nota, d.d_dkb , d.d_sj, a.d_approve1 as d_appsales, a.d_approve2 as d_appar, d.d_sj_receive, a.i_reff_advo
                            from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													  left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													  left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area 
													  and x.i_customer like '%000')
													  , tr_area c
		                        where 
		                        a.i_area=c.i_area and ";
			if ($iarea != 'NA') {
				$sql .= " a.i_area='$iarea' and ";
			}
			$sql .= " (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                  a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                  (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                  or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
            	    order by a.i_area, a.i_spb ";
			$this->db->select($sql, false)->limit($offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiodeexport($iarea, $dfrom, $dto, $cari)
	{
		if ($cari == '') {
			$this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                            a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                            a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,  a.i_spb_refference,
                            a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                            b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                            from tm_spb a 
													  left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													  left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													  left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													  , tr_area c
		                        where 
			                        a.i_area=c.i_area and					
			                        a.i_area='$iarea' and
			                        (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
			                        a.d_spb <= to_date('$dto','dd-mm-yyyy'))
		                        order by a.i_spb ", false);
		} else {
			$this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                            a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                            a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,  a.i_spb_refference,
                            a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                            b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                            from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
													  left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
													  left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
													  , tr_area c
		                        where 
		                        a.i_area=c.i_area and					
		                        a.i_area='$iarea' and
		                        (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
		                        a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
					                  (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					                  or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
					  order by a.i_spb ", false);
		}

		$query = $this->db->get();
		return $query;
	}

	function data($dfrom, $dto, $iarea)
	{
		if ($iarea == "NA") {
			return $this->db->query(" 	SELECT
											a.i_customer,
											a.f_spb_stockdaerah,
											a.d_spb,
											a.f_spb_cancel,
											a.i_approve1,
											a.i_notapprove,
											a.i_approve2,
											a.i_store,
											a.i_nota,
											a.f_spb_siapnotagudang,
											a.f_spb_op,
											a.f_spb_opclose,
											a.f_spb_siapnotasales,
											a.i_sj,
											a.v_spb,
											a.v_spb_discounttotal,
											a.i_spb,
											a.d_spb,
											a.i_salesman,
											a.i_area,
											a.i_spb_old,
											a.i_spb_program,
											a.i_product_group,
											a.i_spb_program,
											a.i_price_group,
											b.e_customer_name,
											c.e_area_name,
											d.i_dkb,
											x.e_customer_name AS xname,
											a.e_telat1 AS e_sales,
											a.e_telat2 AS e_keuangan,
											a.e_batal,
											d.e_telat1 AS e_sj,
											d.e_telat2 AS e_dkb,
											a.i_spb_refference,
											d.d_nota,
											d.v_nota_netto,
											d.d_sj,
											d.d_dkb,
											a.d_approve1,
											a.d_approve2,
											q.e_promo_name,
											a.e_remark1 AS keterangan,
											pg.e_product_groupname_short AS jenis
										FROM
											tm_spb a
										LEFT JOIN tm_promo q ON
											(a.i_spb_program = q.i_promo)
										LEFT JOIN tm_nota d ON
											(a.i_spb = d.i_spb
												AND a.i_area = d.i_area
												AND d.f_nota_cancel = 'f')
										LEFT JOIN tr_customer b ON
											(a.i_customer = b.i_customer
												AND a.i_area = b.i_area)
										LEFT JOIN tr_customer_tmp x ON
											(a.i_customer = x.i_customer
												AND a.i_spb = x.i_spb
												AND a.i_area = x.i_area
												AND x.i_customer LIKE '%000')
										LEFT JOIN tr_product_group pg ON (a.i_product_group = pg.i_product_group),
											tr_area c
										WHERE
											a.i_area = c.i_area
											AND (a.d_spb >= to_date('$dfrom', 'dd-mm-yyyy')
												AND a.d_spb <= to_date('$dto', 'dd-mm-yyyy'))
										ORDER BY
											a.i_area,
											a.i_spb ", FALSE);
		} else {
			return $this->db->query(" 	SELECT
											a.i_customer,
											a.f_spb_stockdaerah,
											a.d_spb,
											a.f_spb_cancel,
											a.i_approve1,
											a.i_notapprove,
											a.i_approve2,
											a.i_store,
											a.i_nota,
											a.f_spb_siapnotagudang,
											a.f_spb_op,
											a.f_spb_opclose,
											a.f_spb_siapnotasales,
											a.i_sj,
											a.v_spb,
											a.v_spb_discounttotal,
											a.i_spb,
											a.d_spb,
											a.i_salesman,
											a.i_area,
											a.i_spb_old,
											a.i_spb_program,
											a.i_product_group,
											a.i_spb_program,
											a.i_price_group,
											b.e_customer_name,
											c.e_area_name,
											d.i_dkb,
											x.e_customer_name AS xname,
											a.e_telat1 AS e_sales,
											a.e_telat2 AS e_keuangan,
											a.e_batal,
											d.e_telat1 AS e_sj,
											d.e_telat2 AS e_dkb,
											a.i_spb_refference,
											d.d_nota,
											d.v_nota_netto,
											d.d_sj,
											d.d_dkb,
											a.d_approve1,
											a.d_approve2,
											q.e_promo_name,
											a.e_remark1 AS keterangan,
											pg.e_product_groupname_short AS jenis
										FROM
											tm_spb a
										LEFT JOIN tm_promo q ON
											(a.i_spb_program = q.i_promo)
										LEFT JOIN tm_nota d ON
											(a.i_spb = d.i_spb
												AND a.i_area = d.i_area
												AND d.f_nota_cancel = 'f')
										LEFT JOIN tr_customer b ON
											(a.i_customer = b.i_customer
												AND a.i_area = b.i_area)
										LEFT JOIN tr_customer_tmp x ON
											(a.i_customer = x.i_customer
												AND a.i_spb = x.i_spb
												AND a.i_area = x.i_area
												AND x.i_customer LIKE '%000') 
										LEFT JOIN tr_product_group pg ON (a.i_product_group = pg.i_product_group),
											tr_area c
										WHERE
											a.i_area = c.i_area
											AND a.i_area = '$iarea'
											AND (a.d_spb >= to_date('$dfrom', 'dd-mm-yyyy')
												AND a.d_spb <= to_date('$dto', 'dd-mm-yyyy'))
										ORDER BY
											a.i_area,
											a.i_spb ", FALSE);
		}
	}
}
