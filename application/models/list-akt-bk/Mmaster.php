<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }
    public function delete($iperiode, $iarea, $ikb, $icoabank)
    {
        $this->db->query("UPDATE tm_kbank 
                          SET f_kbank_cancel='t' 
                          WHERE i_kbank='$ikb' AND i_periode='$iperiode' AND i_area='$iarea' AND f_posting='f' AND f_close='f' AND i_coa_bank='$icoabank'");

        #####UnPosting
        $this->db->query("DELETE FROM th_jurnal_transharian
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("DELETE FROM th_jurnal_transharianitem
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("DELETE FROM th_general_ledger
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");

        #INSERT 
        $this->db->query("INSERT INTO th_jurnal_transharian SELECT * FROM tm_jurnal_transharian
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("INSERT INTO th_jurnal_transharianitem SELECT * FROM tm_jurnal_transharianitem
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("INSERT INTO th_general_ledger SELECT * FROM tm_general_ledger
                          WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");

        #Kondisi Update di TM COA SALDO 
        $quer = $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') AS periode
                                  FROM tm_general_ledger
                                  WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        if ($quer->num_rows() > 0) {
            foreach ($quer->result() as $xx) {
                $this->db->query("UPDATE tm_coa_saldo 
                                  SET v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet,
                                  v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                                  v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                                  WHERE i_coa='$xx->i_coa' AND i_periode='$xx->periode'");
            }
        }

        $this->db->query("DELETE FROM tm_jurnal_transharian WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("DELETE FROM tm_jurnal_transharianitem WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");
        $this->db->query("DELETE FROM tm_general_ledger WHERE i_refference='$ikb' AND i_area='$iarea' AND i_coa_bank='$icoabank'");

        #UPDATE PV
        $quer = $this->db->query("SELECT i_pv, i_area, v_pv, i_pv_type, i_coa_bank FROM tm_pv_item
                                  WHERE i_kk='$ikb' AND i_coa_bank='$icoabank' AND i_pv_type='02'");
        if ($quer->num_rows() > 0) {
            foreach ($quer->result() as $xx) {
                $this->db->query("UPDATE tm_pv SET v_pv=v_pv-$xx->v_pv
                                  WHERE i_pv='$xx->i_pv' AND i_coa='$xx->i_coa_bank' AND i_pv_type='$xx->i_pv_type'");
                
                #UPDATE PV ITEM
#               $this->db->query("delete from tm_pv_item where i_kk='$ikb' and i_pv='$xx->i_pv' and i_coa_bank='$xx->i_coa_bank' and i_pv_type='$xx->i_pv_type'");
                $this->db->query("UPDATE tm_pv_item SET f_pv_cancel='t' 
                                  WHERE i_kk='$ikb' AND i_pv='$xx->i_pv' AND i_coa_bank='$xx->i_coa_bank' AND i_pv_type='$xx->i_pv_type'");
            }
        }

        #UPDATE RV 
        $quer = $this->db->query("SELECT i_rv, i_area, v_rv, i_rv_type, i_coa_bank FROM tm_rv_item
                                  WHERE i_kk='$ikb' AND i_coa_bank='$icoabank' AND i_rv_type='02'");
        if ($quer->num_rows() > 0) {
            foreach ($quer->result() as $xx) {
                $this->db->query("UPDATE tm_rv SET v_rv=v_rv-$xx->v_rv
                                  WHERE i_rv='$xx->i_rv' AND i_coa='$xx->i_coa_bank' AND i_rv_type='$xx->i_rv_type'");

                #UPDATE RV ITEM
#               $this->db->query("delete from tm_rv_item where i_kk='$ikb' and i_rv='$xx->i_rv' and i_coa_bank='$xx->i_coa_bank' and i_rv_type='$xx->i_rv_type'");
                $this->db->query("UPDATE tm_rv_item SET f_rv_cancel='t' 
                                  WHERE i_kk='$ikb' AND i_rv='$xx->i_rv' AND i_coa_bank='$xx->i_coa_bank' AND i_rv_type='$xx->i_rv_type'");
            }
        }
#####
    }
    /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
    if($area1=='00'){
    $this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
    where (upper(a.i_kb) like '%$cari%')
    and a.i_area=b.i_area
    and a.f_kb_cancel='f'
    order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
    }else{
    $this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
    where (upper(a.i_kb) like '%$cari%')
    and a.i_area=b.i_area and a.f_kb_cancel='f'
    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
    order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
    return $query->result();
    }
    }
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
    if($area1=='00'){
    $this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
    where (upper(a.i_area) like '%$cari%' or upper(a.i_kb) like '%$cari%')
    and a.i_area=b.i_area and a.f_kb_cancel='f'
    order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
    }else{
    $this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
    where (upper(a.i_kb) like '%$cari%')
    and a.i_area=b.i_area and a.f_kb_cancel='f'
    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
    order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
    return $query->result();
    }
    } */
    public function bacabank($num, $offset)
    {
        $this->db->select("* from tr_bank order by i_bank", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function caribank($cari, $num, $offset)
    {
        $this->db->select("i_bank, e_bank_name, i_coa from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')
	                     order by i_bank ", false)->limit($num, $offset);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function bacaperiode($icoabank, $dfrom, $dto, $ibank, $num, $offset, $cari)
    {
    if(is_numeric($cari)){
        $this->db->select("	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode,
		                    f_debet, f_posting, i_cek, e_area_name, i_bank, i_coa_bank, e_bank_name, d_entry
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank,
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting,
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name, a.d_entry
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02'
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%' or a.v_bank=$cari) and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank,
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting,
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name, a.d_entry
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02'
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%' or a.v_bank=$cari) and a.i_area=e.i_area
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank'
                  			) as a
                        ORDER BY i_kbank", false)->limit($num, $offset);
    }else{
        $this->db->select("	distinct on (i_kbank) vc, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_bank, v_sisa, i_periode,
		                    f_debet, f_posting, i_cek, e_area_name, i_bank, i_coa_bank, e_bank_name, d_entry
	                    	from(
                  			select x.i_rvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank,
                  			a.v_sisa, a.i_periode, a.f_debet, a.f_posting,
                  			a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name, a.d_entry
                  			from tr_area e, tm_kbank a
                  			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02'
                  			and b.i_coa_bank='$icoabank')
                  			left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                  			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv
                  			and x.i_coa_bank='$icoabank')
                  			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                  			where (upper(a.i_kbank) like '%$cari%' or upper(c.i_rv) like '%$cari%') and a.i_area=e.i_area
                  			and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                  			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank'
                  			union
                    		select x.i_pvb as vc, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank,
                    		a.v_sisa, a.i_periode, a.f_debet, a.f_posting,
                    		a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name, a.d_entry
                    		from tr_area e, tm_kbank a
                    		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02'
                    		and b.i_coa_bank='$icoabank')
                    		left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                    		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv
                    		and x.i_coa_bank='$icoabank')
                    		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                    		where (upper(a.i_kbank) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=e.i_area
                    		and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                    		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank'
                  			) as a
                        ORDER BY i_kbank", false)->limit($num, $offset);
    }
#where not vc isnull
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
    {
        $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
}