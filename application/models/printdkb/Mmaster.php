<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idkb) 
    {
//		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
//		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
//		return TRUE;
    }
    function bacasemua($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							          and substr(a.i_dkb,10,2)='$iarea' and 
							          a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_dkb desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							and substr(a.i_dkb,10,2)='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							          and substr(a.i_dkb,10,2)='$iarea' and 
							          a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($idkb,$iarea)
    {
		$this->db->select(" a.i_dkb, a.i_area, b.e_area_name, c.e_dkb_kirim, a.i_dkb_via, d.e_dkb_via,
                        a.e_sopir_name, a.v_dkb, a.d_dkb,  f.i_ekspedisi, f.e_ekspedisi
                        from tm_dkb a
				                inner join tr_area b on (a.i_area=b.i_area)
				                inner join tr_dkb_kirim c on (a.i_dkb_kirim=c.i_dkb_kirim)
				                inner join tr_dkb_via d on (a.i_dkb_via=d.i_dkb_via)
				                left join tm_dkb_ekspedisi e on (a.i_dkb=e.i_dkb and a.i_area=e.i_area)
                        left join tr_ekspedisi f on (e.i_ekspedisi=f.i_ekspedisi)
				                where a.i_dkb = '$idkb' and a.i_area='$iarea'
				                order by a.i_dkb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($idkb,$iarea)
    {
		$this->db->select(" a.*, b.i_customer, c.e_customer_name, c.e_customer_city, b.i_salesman, b.i_nota, d.n_spb_toplength, d.d_spb
					from tm_dkb_item a, tm_nota b, tr_customer c, tm_spb d
					where a.i_area=b.i_area 
						and a.i_sj=b.i_sj
						and a.i_dkb='$idkb'
						and a.i_area='$iarea'
            and b.i_spb=d.i_spb 
            and b.i_area=d.i_area
						and b.i_customer=c.i_customer
					order by a.i_sj desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
