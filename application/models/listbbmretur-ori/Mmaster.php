<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibbm,$ittb,$iarea,$tahun) 
    {
    	$this->db->set(
    		array(
					'i_bbm'			=> null,
					'd_bbm'			=> null,
					'd_receive2'=> null
    		)
    	);
			$this->db->where('i_area',$iarea);
			$this->db->where('i_ttb',$ittb);
			$this->db->where('n_ttb_year',$tahun);
			$this->db->update('tm_ttbretur');

    	$this->db->set(
    		array(
					'i_product2'					=> null,
					'i_product2_grade'		=> null,
					'i_product2_motif'		=> null,
					'n_quantity_receive'	=> null
    		)
    	);
    	$this->db->where('i_ttb',$ittb);
    	$this->db->where('n_ttb_year',$tahun);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_ttbretur_item');

		$this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_bbm='$ibbm'");
#		$this->db->query("DELETE FROM tm_bbm_item WHERE i_bbm='$ibbm'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
					                from tr_customer b, tm_ttbretur c, tm_bbm a
					                left join tm_kn d on (a.i_bbm=d.i_refference) 
					                where 
					                c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
					                a,i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
					                or upper(a.i_bbm) like '%$cari%') 
					                order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
							          from tm_bbm a, tr_customer b, tm_ttbretur c
							          where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
							          and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							          order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
							          from tr_customer b, tm_ttbretur c, tm_bbm a
							          left join tm_kn d on (a.i_bbm=d.i_refference) 
							          where 
							          c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
							          a.i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
							          or upper(a.i_bbm) like '%$cari%') 
							          and a.i_area='$iarea' and
							          a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbm desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
							          from tr_customer b, tm_ttbretur c, tm_bbm a
							          left join tm_kn d on (a.i_bbm=d.i_refference) 
							          where 
							          c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
							          a.i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
							          or upper(a.i_bbm) like '%$cari%') 
							          and a.i_area='$iarea' and
							          a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbm desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
