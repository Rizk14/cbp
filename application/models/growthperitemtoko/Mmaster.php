<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl,$icust)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
      if($icust=='all') $icust='';
	    $this->db->select("	a.i_product, a.e_product_name, a.e_customer_name, a.e_product_categoryname, sum(a.qnotaly) as qnotaly, 
	                        sum(a.vnotaly) as vnotaly, sum(a.qnotacy) as qnotacy, sum(a.vnotacy) as vnotacy, sum(a.qnotalm) as qnotalm, 
	                        sum(a.vnotalm) as vnotalm, sum(a.qnotacm) as qnotacm, sum(a.vnotacm) as vnotacm from (
                          SELECT b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname, sum(b.n_deliver) as qnotaly, 
                          sum(b.n_deliver*b.v_unit_price) as vnotaly, 0 as qnotacy, 0 as vnotacy, 0 as qnotalm, 0 as vnotalm, 0 as qnotacm, 
                          0 as vnotacm
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e
                          where to_char(a.d_nota, 'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and a.i_customer like '%$icust%' and a.i_sj=b.i_sj and a.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product=d.i_product and d.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname
                          union all
                          SELECT b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname, 0 as qnotaly, 0 as vnotaly, 
                          sum(b.n_deliver) as qnotacy, sum(b.n_deliver*b.v_unit_price) as vnotacy, 0 as qnotalm, 0 as vnotalm, 0 as qnotacm, 
                          0 as vnotacm
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e
                          where to_char(a.d_nota, 'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and a.i_customer like '%$icust%' and a.i_sj=b.i_sj and a.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product=d.i_product and d.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname
                          union all
                          SELECT b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname, 0 as qnotaly, 0 as vnotaly, 
                          0 as qnotacy, 0 as vnotacy, sum(b.n_deliver) as qnotalm, sum(b.n_deliver*b.v_unit_price) as vnotalm, 0 as qnotacm, 
                          0 as vnotacm
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e
                          where to_char(a.d_nota, 'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and a.i_customer like '%$icust%' and a.i_sj=b.i_sj and a.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product=d.i_product and d.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyymm'), b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname
                          union all
                     			SELECT b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname, 0 as qnotaly, 0 as vnotaly, 
                     			0 as qnotacy, 0 as vnotacy, 0 as qnotalm, 0 as vnotalm, sum(b.n_deliver) as qnotacm, 
                     			sum(b.n_deliver*b.v_unit_price) as vnotacm
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e
                          where to_char(a.d_nota, 'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and a.i_customer like '%$icust%' and a.i_sj=b.i_sj and a.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product=d.i_product and d.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyymm'), b.i_product, b.e_product_name, c.e_customer_name, e.e_product_categoryname
                        ) as a
                        group by a.i_product, a.e_product_name, a.e_customer_name, a.e_product_categoryname
                        order by a.e_customer_name, a.i_product",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacacustomer($num,$offset,$area,$cari)
    {
      $this->db->select(" a.*, b.e_area_name  
                          from tr_customer a, tr_area b 
                          where a.i_area='$area' and a.i_area=b.i_area and
                          (a.i_customer like '%$cari%' or a.e_customer_name like '%$cari%')
                          order by a.i_customer", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    
    function bacaarea($num,$offset,$iuser)
    {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    
    function bacatotal($todate,$prevdate,$th,$prevth,$bl,$icust)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	sum(custlm) as custlm, sum(custcm) as custcm, sum(custly) as custly, sum(custcy) as custcy from (
                          select count(a.i_customer) as custlm, 0 as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull and a.i_customer='$icust'
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, count(a.i_customer) as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull and a.i_customer='$icust'
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, 0 as custcm, sum(a.custly) as custly, 0 as custcy from
                          (
                          select count(a.i_customer) as custly from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull and a.i_customer='$icust'
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          union all
                          select 0 as custlm, 0 as custcm, 0 as custly, sum(a.custcy) as custcy from (
                          select count(a.i_customer) as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull and a.i_customer='$icust'
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          ) as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
