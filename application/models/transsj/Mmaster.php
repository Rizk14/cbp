<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$iarea,$num,$offset)
    {
		  $this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
						              where to_char(d_spb,'yyyymm')='$iperiode' and a.i_customer=b.i_customer
						              and i_area='$iarea' order by a.i_spb ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$iuser)
    {
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          and i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          group by a.i_store, b.e_store_name order by a.i_store", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$iuser)
    {
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by a.i_store ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
#		  if ($query->num_rows() > 0){
#			  return $query->result();
#		  }
    }
    function detail($iperiode,$iarea,$iproduct)
    {
      if($iarea=='00')
      {
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and daerah='f' and product='$iproduct' and a.product=b.i_product",false);
      }else{
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and area='$iarea' and product='$iproduct'  and a.product=b.i_product",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
