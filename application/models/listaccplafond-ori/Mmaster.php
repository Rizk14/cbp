<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_store_name as e_area_name from tm_spmb a, tr_store b
							          where a.i_area=b.i_store and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name  as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          group by a.i_store, b.e_store_name
                          order by a.i_store", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                          or a.i_area = '$area4' or a.i_area = '$area5')
                          group by a.i_store, b.e_store_name order by a.i_store", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b 
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($iperiodeawal,$iperiodeakhir)
    {
		$query = $this->db->query("select c.e_area_name, b.i_customer, b.n_customer_toplength, b.e_customer_name, 
                                  case when a.n_rata_telat is null then '99-Toko Baru'
                                  else 
                                    case when a.n_rata_telat<=7 then '01-Baik Sekali'
                                         when a.n_rata_telat<=14 then '02-Baik'
                                         when a.n_rata_telat<=21 then '03-Cukup' 
                                         when a.n_rata_telat<=30 then '04-Calon BL'
                                    else '05-Black List' end                                     
                                  end as e_kategori,
                                  case when a.n_rata_telat is null then
                                  0
                                  else
                                  case 
                                    when a.n_rata_telat<=7 then 2
                                    when a.n_rata_telat<=14 then 1.5
                                    when a.n_rata_telat<=21 then 1                                    
                                    when a.n_rata_telat<=30 then 0.5
                                  else 0 end 
                                  end as n_index,
                                a.*
                              from  tm_plafond a, tr_customer b, tr_area c
                              where a.i_customer_groupbayar = b.i_customer and a.i_area = c.i_area
                              and a.e_periode_awal ='$iperiodeawal' and a.e_periode_akhir='$iperiodeakhir' order by a.i_customer_groupbayar");
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
 	function bacano($iperiodeawal, $iperiodeakhir)
  {
    $this->db->select(" a.e_area_name, b.i_customer_groupbayar, c.e_customer_name , b.e_periode_awal, b.e_periode_akhir, b.i_kategori, b.e_kategori,
                        b.n_rata_telat, b.i_index, b.v_total_penjualan, b.v_max_penjualan, b.v_rata_penjualan, b.v_plafond as v_plafond_program, b.v_plafond_before, b.v_plafond_acc 
                        from tm_plafond b, tr_area a, tr_customer c
                        where a.i_area = b.i_area and b.e_periode_awal ='$iperiodeawal' and b.e_periode_akhir ='$iperiodeakhir'
                        and c.i_customer= b.i_customer_groupbayar",false);
    $tes=$this->db->get();
    return $tes;
  }
}
?>
