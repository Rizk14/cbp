<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproduct,$ipricegroup)
    {
		  $this->db->select(" a.*, b.e_product_name, c.e_product_gradename
                          from tr_product_priceco a, tr_product b, tr_product_grade c
                          where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade
                          and a.i_product='$iproduct' and a.i_price_group='$ipricegroup' ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }

    function update($iproduct,$eproductname,$iproductgrade,$ipricegroup,$vproductretail,$nmargin)
    {
		  $this->db->query(" update tr_product_priceco set v_product_retail = '$vproductretail', 
							           n_margin = '$nmargin'
				                 where i_product = '$iproduct' and i_price_group='$ipricegroup' ");
    }
	
    public function delete($iproduct,$ipricegroup)
    {
#		  $this->db->query('DELETE FROM tr_product_priceco WHERE i_product=\''.$iproduct.'\' and i_price_group=\''.$ipricegroup.'\'');
      $this->db->query("update tr_product_priceco set f_product_pricecocancel='t' 
                        WHERE i_product='$iproduct' and i_price_group='$ipricegroup'");
		  return TRUE;
      #redirect('listbrgkons/cform/');
    }
    
    function bacasemua($cari, $num,$offset)
    {
		  $this->db->select(" a.*, b.e_product_name, c.e_product_gradename, d.e_price_groupconame
                          from tr_product b, tr_product_grade c, tr_product_priceco a, tr_price_groupco d 
                          where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade and a.i_price_groupco=d.i_price_groupco
                          and (upper(a.i_product) like '%$cari%' 
                          or upper(b.e_product_name) = '%$cari%')
                          order by a.i_product", false)->limit($num,$offset);
#                          left join tr_price_groupco d on (a.i_price_groupco=d.i_price_groupco)
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" a.*, b.e_product_name, c.e_product_gradename
                          from tr_product_priceco a, tr_product b, tr_product_grade c
                          where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade
                          and (upper(a.i_product) like '%$cari%' 
                          or upper(b.e_product_name) = '%$cari%')
                          order by a.i_product", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
