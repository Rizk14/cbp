<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasemua($cari,$num,$offset,$dfrom,$dto)
    {
      $area	= $this->session->userdata('i_area');
		  $this->db->select(" a.*, b.e_customer_name, c.i_customer, c.i_sjpbr
                          from tm_sjbr a, tr_customer b, tm_sjpbr c
                          where c.i_customer=b.i_customer and a.i_area='$area'
                          and a.i_sjbr=c.i_sjbr and a.i_area=c.i_area
                          and a.f_sjbr_cancel='f'
                          and (upper(a.i_sjbr) like '%$cari%' or upper(c.i_customer) like '%$cari%'
                          or upper(b.e_customer_name) like '%$cari%')
                          and a.d_sjbr >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_sjbr <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_sjbr",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($isjbr)
    {
		  $this->db->select(" a.*, b.e_customer_name, b.e_customer_address, c.i_sjpbr, c.i_customer
                          from tm_sjbr a, tr_customer b, tm_sjpbr c
                          where a.i_sjbr = '$isjbr'
                          and c.i_customer=b.i_customer
                          and a.i_sjbr=c.i_sjbr and a.i_area=c.i_area
                          order by a.i_sjbr desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($isjbr)
    {
		  $this->db->select(" * from tm_sjbr_item
                          inner join tr_product on (tm_sjbr_item.i_product=tr_product.i_product)
                          inner join tr_product_motif on (tm_sjbr_item.i_product_motif=tr_product_motif.i_product_motif
                          and tm_sjbr_item.i_product=tr_product_motif.i_product)
                          where i_sjbr = '$isjbr' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.i_customer, c.i_sjpbr
                        from tm_sjbr a, tr_customer b, tm_sjpbr c
                        where c.i_customer=b.i_customer
                        and a.i_sjbr=c.i_sjbr and a.i_area=c.i_area
                        and (upper(a.i_sjbr) like '%$cari%' or upper(c.i_customer) like '%$cari%'
                        or upper(b.e_customer_name) like '%$cari%' or upper(c.i_sjpbr) like '%$cari%')
                        order by a.i_sjbr",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatesjbr($isjbr)
    {
      $this->db->query("   update tm_sjbr set n_print=n_print+1
                              where i_sjbr = '$isjbr'",false);
    }
}
?>
