<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    { 
    $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $tahun=$tmp[2];
        $th=$tmp[2]-1;
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $dtoprev=$hr."-".$bl."-".$th;

    $this->db->select(" a.group ,sum(a.totsales) as totsales, sum(ob) as ob, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_periode,sum(a.totsales) as totsales , a.group,sum(ob) as ob, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================
    
    --hitung totsales
    select '' as i_periode ,a.e_product_groupname as group, count(a.i_salesman) as totsales ,0 as ob ,0 as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct a.i_salesman , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where (a.d_nota >=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false'
    and b.i_product_group=c.i_product_group
    ) as a
    group by a.e_product_groupname

    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyymm') as i_periode, c.e_product_groupname as group,0 as totsales,0 as ob , 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev 
    from tm_nota a, tm_spb b, tr_product_group c
    where (a.d_nota >=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.i_product_group=c.i_product_group
    group by to_char(a.d_nota,'yyyymm'), c.e_product_groupname
    Union all

    --hitung qty group
    select to_char(a.d_nota,'yyyymm') as i_periode, c.e_product_groupname as group,0 as totsales,0 as ob ,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev
    from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
    where (a.d_nota >=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    group by to_char(a.d_nota,'yyyymm'), c.e_product_groupname
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group ,0 as totsales,0 as ob , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyymm') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where (a.d_nota >=to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')) and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.i_product_group=c.i_product_group
    --and a.i_customer in ('01155','01010')
    --order by a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname
    union all

    -- Hitung OB Group
    select a.i_periode , a.e_product_groupname as group ,0 as totsales,count(ob) as ob, 0 as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (a.i_customer)  a.i_customer as ob, to_char (a.d_nota,'yyyymm') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b, tr_product_group c
    where a.d_nota <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.i_product_group=c.i_product_group 
    order by a.i_customer 
    ) as a
    group by a.i_periode , a.e_product_groupname
    union all

    --========================================START OF PREV YEAR==================================================

   
    select to_char(a.d_nota,'yyyymm') as i_periode, c.e_product_groupname as group,0 as totsales,0 as ob ,0 as oa , 0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 0 as qnotaprev 
    from tm_nota a, tm_spb b, tr_product_group c
    where (a.d_nota >=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <= to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.i_product_group=c.i_product_group
    group by to_char(a.d_nota,'yyyymm'), c.e_product_groupname
    Union all
   
    select to_char(a.d_nota,'yyyymm') as i_periode, c.e_product_groupname as group,0 as totsales,0 as ob , 0 as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , sum(d.n_deliver) as qnotaprev
    from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
    where (a.d_nota >=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <= to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    group by to_char(a.d_nota,'yyyymm'), c.e_product_groupname
    union all 

    
    --hitung oa tahun sebelumnya
    select a.i_periode , a.e_product_groupname as group ,0 as totsales,0 as ob , 0 as oa ,0  as vnota, 0 as qnota , count(a.oa) as oaprev ,0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyymm') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where (a.d_nota >=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <= to_date('$dtoprev','dd-mm-yyyy')) and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false'
    and b.i_product_group=c.i_product_group
    ) as a
    group by a.i_periode , a.e_product_groupname

    --============================================END======================================

    ) as a
    group by a.i_periode, a.group

    ) as a
    group by a.group",false);  
/*$prevth=$tahun-1;
$this->db->select("a.group , sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_periode, a.group, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================

    -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev
    from tm_nota a, tm_spb b
    where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    group by to_char(a.d_nota,'yyyy')
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev 
    from tm_nota a, tm_spb b, tr_product_group c
    where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev
    from tm_nota a, tm_spb b, tm_nota_item c
    where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    group by to_char(a.d_nota,'yyyy')
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev
    from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
    where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where to_char(a.d_nota,'yyyy')='$tahun' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    ) as a
    group by a.i_periode 
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where to_char(a.d_nota,'yyyy')='$tahun' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    ) as a
    group by a.i_periode , a.e_product_groupname
    union all

    --=========================================END OF FIRST YEAR==================================================
    --========================================START OF PREV YEAR==================================================

    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 0 as qnotaprev
    from tm_nota a, tm_spb b
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    group by to_char(a.d_nota,'yyyy')
    union all
   
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 0 as qnotaprev 
    from tm_nota a, tm_spb b, tr_product_group c
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
    Union all
    
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , sum(c.n_deliver) as qnotaprev
    from tm_nota a, tm_spb b, tm_nota_item c
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    group by to_char(a.d_nota,'yyyy')
    union all
   
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , sum(d.n_deliver) as qnotaprev
    from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
    union all 

    --hitung oa mo tahun sebelumnya
    select a.i_periode , 'modern Outlet' as group , 0 as oa ,0  as vnota, 0 as qnota ,count(oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    ) as a
    group by a.i_periode 
    union all
    
    --hitung oa tahun sebelumnya
    select a.i_periode , a.e_product_groupname as group , 0 as oa ,0  as vnota, 0 as qnota , count(a.oa) as oaprev ,0 as vnotaprev , 0 as qnotaprev from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname
    from tm_nota a , tm_spb b , tr_product_group c where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    ) as a
    group by a.i_periode , a.e_product_groupname

    --============================================END OF THE PREV YEAR======================================

    ) as a
    group by a.i_periode, a.group

    ) as a
    group by a.group",false);*/
	   /* $this->db->select(" a.i_periode, a.group, sum(a.vnota)  as vnota, sum(qnota) as qnota from (	
	                        SELECT to_char(a.d_nota,'yyyy') as i_periode, 'Modern Outlet' as group, sum(a.v_nota_netto)  as vnota, 0 as qnota
                          from tm_nota a, tm_spb b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area  and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, sum(a.v_nota_netto)  as vnota, 
                          0 as qnota from tm_nota a, tm_spb b, tr_product_group c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area  and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          Union all
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'Modern Outlet' as group, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tm_spb b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area  and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0  as vnota, 
                          sum(d.n_deliver) as qnota from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
                          and a.i_sj=d.i_sj and a.i_area=d.i_area
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          ) as a
                          group by a.i_periode, a.group
                          order by a.i_periode, a.group",false); */
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacagroup($tahun)
    {
	    $this->db->select("	a.group from( 
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'Modern Outlet' as group, sum(a.v_nota_netto)  as vnota, 0 as qnota
                          from tm_nota a, tm_spb b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, sum(a.v_nota_netto)  as vnota, 
                          0 as qnota from tm_nota a, tm_spb b, tr_product_group c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          Union all
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'Modern Outlet' as group, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tm_spb b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0  as vnota, 
                          sum(d.n_deliver) as qnota from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
                          and a.i_sj=d.i_sj and a.i_area=d.i_area
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          ) as a
                          group by a.i_periode, a.group
                          order by a.i_periode, a.group",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
