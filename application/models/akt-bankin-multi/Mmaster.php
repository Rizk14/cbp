<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }

    public function baca($ikk, $iperiode, $iarea)
    {
        $this->db->select("	a.i_kendaraan from tm_kk a
							inner join tr_area b on(a.i_area=b.i_area)
							where a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $tmp) {
                $xxx = $tmp->i_kendaraan;
            }
        } else {
            $xxx = '';
        }

        if (trim($xxx) == '') {
            $this->db->select("	a.*, b.e_area_name, '' as e_pengguna from tm_kk a, tr_area b
								where a.i_area=b.i_area and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'", false);
        } else {
            $this->db->select("	a.*, b.e_area_name
													from tm_kk a, tr_area b, tr_kendaraan c
													where a.i_area=b.i_area and a.i_kendaraan=c.i_kendaraan and a.i_periode=c.i_periode
													and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'", false);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function baca_nilai($i_customer)
    {
        $thbln = date('ym');
        return $this->db->query("select (x.v_spb - x.diskon) as nilai_spb from(
  select sum(v_spb) as v_spb, sum(v_spb_discounttotal) as diskon from tm_spb where i_customer = '$i_customer' and i_spb like '%SPB-$thbln-%' and f_spb_cancel = 'f'
  ) as x");
    }
    public function bacaarea($num, $offset, $area1, $area2, $area3, $area4, $area5)
    {
        if ($area1 == '00') {
            $this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
        } else {
            $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function cariarea($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
    {
        if ($area1 == '00') {
            $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", false)->limit($num, $offset);
        } else {
            $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function bacacoa($num, $offset, $area)
    {
        if ($area != '00') {
            $this->db->select(" * from tr_coa where (not (i_coa like '" . Bank . "%')  and not (i_coa like '110-4%') order by i_coa", false)->limit($num, $offset);
        } else {
            $this->db->select(" * from tr_coa where (not (i_coa like '" . Bank . "%')  and not (i_coa like '110-4%')) order by i_coa", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function caricoa($cari, $num, $offset, $area)
    {
        if ($area != '00') {
            $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) or i_coa='900-0000' and
                    		  ((not (i_coa like '" . Bank . "%') ))
                    		  and i_coa<>i_coa_ledger  order by i_coa", false)->limit($num, $offset);
        } else {
            $this->db->select(" * from tr_coa where (upper(i_coa) like '%$cari%' or (upper(e_coa_name) like '%$cari%')) or i_coa='900-0000' and
		                      ((not (i_coa like '" . Bank . "%')))
              						 order by i_coa", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function bacagiro($area, $xtgl, $num, $offset, $group)
    {
        $this->db->select("a.* from (
                        select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
                        where a.i_customer=b.i_customer and a.i_area='$area'
                        and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                        and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl'
						and a.i_giro not in(select i_giro from tm_kbank where f_kbank_cancel='f')
                        union all
                        select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c,
                        tm_rtunai_item d
                        where a.i_customer=b.i_customer and a.i_area='$area'
                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                        and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                        and a.i_tunai not in(select i_giro from tm_kbank where f_kbank_cancel='f')
                        union all
                        select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a
                        left join tr_customer_groupar b on a.i_customer=b.i_customer
                        left join (
			                select i_giro from tm_kbank where f_kbank_cancel='f'
                        ) as c on a.i_kum = c.i_giro
                        where  a.i_area='$area'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and d_kum<='$xtgl'
                        and c.i_giro is null
						/*select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a
						left join tr_customer_groupar b on a.i_customer=b.i_customer
                        where a.i_area='$area'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and d_kum<='$xtgl'
                        and a.i_kum not in(select i_giro from tm_kbank where f_kbank_cancel='f')*/
                        )as a
                        order by a.tgl, a.bayar ", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function carigiro($cari, $area, $xtgl, $num, $offset, $group)
    {
        $this->db->select(" a.* from (
                        select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
                        where a.i_customer=b.i_customer and a.i_area='$area'
                        and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                        and (upper(a.i_giro) like '%$cari%')
                        and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl'
                        union all
                        select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c,
                        tm_rtunai_item d
                        where a.i_customer=b.i_customer and a.i_area='$area'
                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                        and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
                        and (upper(a.i_tunai) like '%$cari%')
                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                        union all
                        select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
                        where a.i_customer=b.i_customer and a.i_area='$area'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
						and (upper(a.i_kum) like '%$cari%')
                        and d_kum<='$xtgl'
                        )as a
                        order by a.tgl, a.bayar ", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function bacakendaraan($cari, $area, $periode, $num, $offset)
    {
        if ($cari == 'sikasep') {
            $this->db->select(" * from tr_kendaraan a
							            inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							            inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							            where a.i_area='$area' and a.i_periode='$periode'
							            order by a.i_kendaraan", false)->limit($num, $offset);
        } else {
            $this->db->select(" * from tr_kendaraan a
							            inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							            inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							            where a.i_area='$area' and a.i_periode='$periode'
                          and (upper(i_kendaraan) like '%$cari%' or upper(e_pengguna) like '%$cari%')
							            order by a.i_kendaraan", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function carikendaraan($area, $periode, $cari, $num, $offset)
    {
        $this->db->select(" * from tr_kendaraan a
					inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
					inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
					where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
					and a.i_area='$area' and a.i_periode='$periode'
					order by a.i_kendaraan", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
/*
function insert($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,$ecoaname,$edescription,$ejamin,$ejamout,$nkm,$etempat,$fdebet,$dbukti,$enamatoko,$epengguna,$ibukti)
 */
    public function insert($iareax, $ikbank, $iperiode, $icoa, $vkb, $dkb, $ecoaname, $edescription, $fdebet, $icoabank, $igiro)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $this->db->set(
            array(
                'i_area' => $iareax,
                'i_kbank' => $ikbank,
                'i_periode' => $iperiode,
                'i_coa' => $icoa,
                'v_bank' => $vkb,
                'v_sisa' => $vkb,
                'd_bank' => $dkb,
                'e_coa_name' => $ecoaname,
                'e_description' => $edescription,
                'd_entry' => $dentry,
                'f_debet' => $fdebet,
                'i_coa_bank' => $icoabank,
                'i_giro' => $igiro,
            )
        );
        $this->db->insert('tm_kbank');
    }
    public function update($iarea, $ikk, $iperiode, $icoa, $ikendaraan, $vkk, $dkk, $ecoaname, $edescription, $ejamin, $ejamout, $nkm, $etempat, $fdebet, $dbukti, $enamatoko, $epengguna, $ibukti)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dupdate = $row->c;
        $this->db->set(
            array(
                'i_kendaraan' => $ikendaraan,
                'i_coa' => $icoa,
                'v_kk' => $vkk,
                'i_bukti_pengeluaran' => $ibukti,
                'd_kk' => $dkk,
                'e_coa_name' => $ecoaname,
                'e_description' => $edescription,
                'e_jam_in' => $ejamin,
                'e_jam_out' => $ejamout,
                'n_km' => $nkm,
                'e_tempat' => $etempat,
                'd_update' => $dupdate,
                'd_bukti' => $dbukti,
                'f_debet' => $fdebet,
                'e_nama_toko' => $enamatoko,
                'e_pengguna' => $epengguna,
            )
        );
        $this->db->where('i_area', $iarea);
        $this->db->where('i_kk', $ikk);
        $this->db->where('i_periode', $iperiode);
        $this->db->update('tm_kk');
    }
#####
    public function insertrvb($irvb, $icoabank, $irv, $iarea, $irvtype)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $this->db->set(
            array(
                'i_rvb' => $irvb,
                'i_coa_bank' => $icoabank,
                'i_rv' => $irv,
                'i_area' => $iarea,
                'i_rv_type' => $irvtype,
                'd_entry' => $dentry,
            )
        );
        $this->db->insert('tm_rvb');
    }
    public function insertrv($irv, $iarea, $iperiode, $icoa, $drv, $tot, $eremark, $irvtype)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $this->db->set(
            array(
                'i_rv' => $irv,
                'i_area' => $iarea,
                'i_periode' => $iperiode,
                'i_coa' => $icoa,
                'd_rv' => $drv,
                'v_rv' => $tot,
                'd_entry' => $dentry,
                'i_rv_type' => $irvtype,
            )
        );
        $this->db->insert('tm_rv');
    }
    public function insertrvitem($irv, $iarea, $icoa, $ecoaname, $vrv, $edescription, $ikk, $irvtype, $iareax, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $this->db->set(
            array(
                'i_area' => $iarea,
                'i_rv' => $irv,
                'i_coa' => $icoa,
                'e_coa_name' => $ecoaname,
                'v_rv' => $vrv,
                'e_remark' => $edescription,
                'i_kk' => $ikk,
                'i_rv_type' => $irvtype,
                'i_area_kb' => $iareax,
                'i_coa_bank' => $icoabank,
            )
        );
        $this->db->insert('tm_rv_item');
    }
#####
    public function runningnumberrvb($th, $bl, $icoabank, $iarea)
    {
        $this->db->select(" max(substr(i_rvb,11,6)) as max
		                    from tm_rvb
		                    where substr(i_rvb,4,2)='$th' and substr(i_rvb,6,2)='$bl' and i_coa_bank='$icoabank'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $terakhir = $row->max;
            }
            $norvb = $terakhir + 1;
            settype($norvb, "string");
            $a = strlen($norvb);
            while ($a < 6) {
                $norvb = "0" . $norvb;
                $a = strlen($norvb);
            }
            $norvb = "RV-" . $th . $bl . "-" . $iarea . $norvb;
            return $norvb;
        } else {
            $norvb = "000001";
            $norvb = "RV-" . $th . $bl . "-" . $iarea . $norvb;
            return $norvb;
        }
    }
    public function runningnumberrv($th, $bl, $iarea, $irvtype)
    {
        $this->db->select(" max(substr(i_rv,11,6)) as max
		                    from tm_rv
		                    where substr(i_rv,4,2)='$th' and substr(i_rv,6,2)='$bl' and i_area='$iarea'
		                    and i_rv_type='$irvtype'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $terakhir = $row->max;
            }
            $norv = $terakhir + 1;
            settype($norv, "string");
            $a = strlen($norv);
            while ($a < 6) {
                $norv = "0" . $norv;
                $a = strlen($norv);
            }
            $norv = "RV-" . $th . $bl . "-" . $iarea . $norv;
            return $norv;
        } else {
            $norv = "000001";
            $norv = "RV-" . $th . $bl . "-" . $iarea . $norv;
            return $norv;
        }
    }
    public function runningnumberbank($th, $bl, $iarea, $icoabank)
    {
        $this->db->select(" max(substr(i_kbank,9,5)) as max from tm_kbank
		                    where substr(i_kbank,4,2)='$th' and substr(i_kbank,6,2)='$bl' and i_coa_bank='$icoabank'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $terakhir = $row->max;
            }
            $nogj = $terakhir + 1;
            settype($nogj, "string");
            $a = strlen($nogj);
            while ($a < 5) {
                $nogj = "0" . $nogj;
                $a = strlen($nogj);
            }
            $nogj = "BM-" . $th . $bl . "-" . $nogj;
            return $nogj;
        } else {
            $nogj = "00001";
            $nogj = "BM-" . $th . $bl . "-" . $nogj;
            return $nogj;
        }
    }
    public function bacasaldo($area, $tanggal, $icoabank)
    {
        $tmp = explode("-", $tanggal);
        $thn = $tmp[0];
        $bln = $tmp[1];
        $tgl = $tmp[2];
        $dsaldo = $thn . "/" . $bln . "/" . $tgl;
        $dtos = $this->mmaster->dateAdd("d", 1, $dsaldo);
        $tmp1 = explode("-", $dtos, strlen($dtos));
        $th = $tmp1[0];
        $bl = $tmp1[1];
        $dt = $tmp1[2];
        $dtos = $th . $bl;
        $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ", false);
#and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.1'
        $query = $this->db->get();
        $saldo = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $saldo = $row->v_saldo_awal;
            }
        }
/*
$this->db->select(" sum(x.v_bank) as v_bank from
(
select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
UNION ALL
select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
and b.i_periode='$dtos' and x.i_area='$area' and b.d_bank<='$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
) as x ",false);
 */
        $this->db->select("
		          sum(b.v_bank) as v_bank from tm_kbank b
							where b.i_periode='$dtos' and b.d_bank<='$tanggal' and b.f_debet='t' and b.i_coa_bank='$icoabank'
							and b.f_kbank_cancel='f'", false);
        $query = $this->db->get();
        $kredit = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $kredit = $row->v_bank;
            }
        }
        $this->db->select(" sum(b.v_bank) as v_bank from tm_kbank b
							where b.i_periode='$dtos' and b.d_bank<='$tanggal' and b.f_debet='f' and b.i_coa_bank='$icoabank'
							and b.f_kbank_cancel='f'", false);
        $query = $this->db->get();
        $debet = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $debet = $row->v_bank;
            }
        }

/*
$this->db->select(" sum(v_bank) as v_bank from tm_kbank a where a.i_periode='$dtos' and a.d_bank<='$tanggal'
and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa='$icoabank'
and a.v_bank not in (select b.v_kbank from tm_kbank b where b.d_bank = a.d_bank
and b.f_kbank_cancel='f' and b.f_debet='f' and b.i_coa_bank='$icoabank'
and b.i_periode='$dtos')",false);
$query = $this->db->get();
if ($query->num_rows() > 0){
foreach($query->result() as $row){
$debet=$debet+$row->v_bank;
}
}
 */

        if ($saldo == '' || $saldo == null) {
            $saldo = 0;
        }

        if ($debet == '' || $debet == null) {
            $debet = 0;
        }

        if ($kredit == '' || $kredit == null) {
            $kredit = 0;
        }

        $saldo = $saldo + $debet - $kredit;
        return $saldo;
    }
    public function dateAdd($interval, $number, $dateTime)
    {
        $dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
        $dateTimeArr = getdate($dateTime);
        $yr = $dateTimeArr['year'];
        $mon = $dateTimeArr['mon'];
        $day = $dateTimeArr['mday'];
        $hr = $dateTimeArr['hours'];
        $min = $dateTimeArr['minutes'];
        $sec = $dateTimeArr['seconds'];
        switch ($interval) {
            case "s": //seconds
                $sec += $number;
                break;
            case "n": //minutes
                $min += $number;
                break;
            case "h": //hours
                $hr += $number;
                break;
            case "d": //days
                $day += $number;
                break;
            case "ww": //Week
                $day += ($number * 7);
                break;
            case "m": //similar result "m" dateDiff Microsoft
                $mon += $number;
                break;
            case "yyyy": //similar result "yyyy" dateDiff Microsoft
                $yr += $number;
                break;
            default:
                $day += $number;
        }
        $dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
        $dateTimeArr = getdate($dateTime);
        $nosecmin = 0;
        $min = $dateTimeArr['minutes'];
        $sec = $dateTimeArr['seconds'];
        if ($hr == 0) {$nosecmin += 1;}
        if ($min == 0) {$nosecmin += 1;}
        if ($sec == 0) {$nosecmin += 1;}
        if ($nosecmin > 2) {
            return (date("Y-m-d", $dateTime));
        } else {
            return (date("Y-m-d G:i:s", $dateTime));
        }
    }
    public function area($iarea)
    {
        $this->db->select(" e_area_name from tr_area where i_area='$iarea'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $nama = $row->e_area_name;
                return $nama;
            }
        }
    }
    public function bacakgroup($cari, $num, $offset)
    {
        $this->db->select(" * from tr_kk_group where upper(i_kk_group) like '%$cari%' or upper(e_kk_groupname) like '%$cari%' order by i_kk_group", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function bacarv($irv, $iarea, $icoabank)
    {
        $this->db->select(" d.i_rvb as i_rv, a.d_rv, b.i_coa, b.e_remark, b.v_rv, c.e_bank_name, e.e_coa_name as nama_coa
		                    from tr_bank c, tm_rvb d, tm_rv a
		                    left join tm_rv_item b on(a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type= b.i_rv_type and b.f_rv_cancel=false)
                            left join tr_coa e  on (b.i_coa = e.i_coa)
		                    where a.i_area='$iarea' and d.i_rvb='$irv'
		                    and a.i_rv_type='02'
		                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_rv_type=d.i_rv_type and a.i_rv=d.i_rv
  	                    and d.i_coa_bank='$icoabank'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
    public function bacarvprint($icoabank, $cari, $area, $periode, $num, $offset)
    {
        if ($cari == 'sikasep') {
            $this->db->select(" distinct on (a.i_rv) *, a.v_rv as v_rv, d.i_rvb as i_rv from tr_bank c, tm_rvb d, tm_rv a
	  	                    left join tm_rv_item b on(a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type= b.i_rv_type)
	  	                    where a.i_area='$area' and a.i_periode='$periode' and a.i_rv_type='02'
	  	                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_rv_type=d.i_rv_type and a.i_rv=d.i_rv
	  	                    and d.i_coa_bank='$icoabank'", false)->limit($num, $offset);
        } else {
            $this->db->select(" distinct on (a.i_rv) *, a.v_rv as v_rv, d.i_rvb as i_rv from tr_bank c, tm_rvb d, tm_rv a
	  	                    left join tm_rv_item b on(a.i_rv=b.i_rv and a.i_area=b.i_area and a.i_rv_type= b.i_rv_type)
	  	                    where a.i_area='$area' and a.i_periode='$periode' and a.i_rv_type='02'
	  	                    and (upper(d.i_rvb) like '%$cari%')
	  	                    and a.i_coa=c.i_coa and a.i_area=d.i_area and a.i_rv_type=d.i_rv_type and a.i_rv=d.i_rv
	  	                    and d.i_coa_bank='$icoabank'", false)->limit($num, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
###########posting##########
    public function inserttransheader($inota, $iarea, $eremark, $fclose, $dkn, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $eremark = str_replace("'", "''", $eremark);
        $this->db->query("insert into tm_jurnal_transharian
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi,i_coa_bank)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose','$dkn','$dkn','$icoabank')");
    }
    public function inserttransitemdebet($accdebet, $ikn, $namadebet, $fdebet, $fposting, $iarea, $eremark, $vjumlah, $dkn, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $namadebet = str_replace("'", "''", $namadebet);
        $this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry, i_area,i_coa_bank)
						  	  values
					  	 ('$accdebet','$ikn','$namadebet','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea','$icoabank')");
    }
    public function inserttransitemkredit($acckredit, $ikn, $namakredit, $fdebet, $fposting, $iarea, $egirodescription, $vjumlah, $dkn, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $namakredit = str_replace("'", "''", $namakredit);
        $this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry, i_area,i_coa_bank)
						  	  values
					  	 ('$acckredit','$ikn','$namakredit','$fdebet','$fposting','$vjumlah','$dkn','$dkn','$dentry','$iarea','$icoabank')");
    }
    public function insertgldebet($accdebet, $ikn, $namadebet, $fdebet, $iarea, $vjumlah, $dkn, $eremark, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $eremark = str_replace("'", "''", $eremark);
        $namadebet = str_replace("'", "''", $namadebet);
        $this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ikn','$accdebet','$dkn','$namadebet','$fdebet',$vjumlah,'$iarea','$dkn','$eremark','$dentry','$icoabank')");
    }
    public function insertglkredit($acckredit, $ikn, $namakredit, $fdebet, $iarea, $vjumlah, $dkn, $eremark, $icoabank)
    {
        $query = $this->db->query("SELECT current_timestamp as c");
        $row = $query->row();
        $dentry = $row->c;
        $eremark = str_replace("'", "''", $eremark);
        $namakredit = str_replace("'", "''", $namakredit);
        $this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry,i_coa_bank)
						  	  values
					  	 ('$ikn','$acckredit','$dkn','$namakredit','$fdebet','$vjumlah','$iarea','$dkn','$eremark','$dentry','$icoabank')");
    }
    public function updatekk($ikn, $iarea, $iperiode)
    {
        $this->db->query("update tm_kk set f_posting='t' where i_kk='$ikn' and i_area='$iarea' and i_periode='$iperiode'");
    }
    public function updatesaldodebet($accdebet, $iperiode, $vjumlah)
    {
        $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
    }
    public function updatesaldokredit($acckredit, $iperiode, $vjumlah)
    {
        $this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
    }
    public function namaacc($icoa)
    {
        $this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $tmp) {
                $xxx = $tmp->e_coa_name;
            }
            return $xxx;
        }
    }
###########end of posting##########
    public function bacabank($num, $offset)
    {
        $this->db->select("* from tr_bank order by i_bank", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

     function cekrvb($irvb, $irvtype, $th, $icoabank)
    {
        $irbvcut=substr($irvb, 10,6);
        $this->db->select(" i_rvb from tm_rvb where i_rvb='$irvb' and i_rv_type='$irvtype' and substr(i_rvb,4,2)='$th' AND i_coa_bank = '$icoabank' ",false);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return "ada";
        } else {
            return "tidak ada";
        }
    }

    function getrv($irvb, $irvtype, $th, $icoabank)
    {
        $this->db->select(" i_rv from tm_rvb where i_rvb='$irvb' and i_rv_type='$irvtype' and substr(i_rvb,4,2)='$th' AND i_coa_bank = '$icoabank' ",false);
        $query = $this->db->get();
        return $query;
        /*if($query->num_rows()==0){
            return "tidak ada";
        } else {
            return "ada";
        }*/
    }

    function updaterv($irv, $irvtype, $iperiode, $tot)
    {
        $this->db->query("UPDATE tm_rv SET v_rv=v_rv+$tot, i_periode = '$iperiode' WHERE i_rv='$irv' AND i_rv_type='$irvtype' AND i_periode='$iperiode'");
    }

    function updatervb($irvb, $irvtype, $iperiode, $icoabank)
    {
        $this->db->query("UPDATE tm_rvb SET i_rvb = '$irvb' WHERE i_rvb = '$irvb' AND i_rv_type='$irvtype' AND i_coa_bank = '$icoabank' ");
    }
}