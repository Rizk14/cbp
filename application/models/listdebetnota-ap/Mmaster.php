<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idnap,$ndnyear,$isupplier) 
    {
		$this->db->query("update tm_dn_ap set f_dn_ap_cancel='t' WHERE i_dn_ap='$idnap' and n_dn_ap_year=$ndnyear and i_supplier='$isupplier'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name, 
            b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name 
				    from tm_kn a
				    inner join tr_customer b on (a.i_customer=b.i_customer)
				    inner join tr_area c on (a.i_area=c.i_area)
				    left join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    inner join tr_salesman e on (a.i_salesman=e.i_salesman)
				    where 
				    a.i_kn_type='03' 
				    and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
				     upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
				    order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*,
				b.e_supplier_name
			from 
				tm_dn_ap a 
				left join tr_supplier b on (a.i_supplier=b.i_supplier)
			where 
				a.i_supplier >= '$isupplier'
				and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
				and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
			order by 
				a.d_dn_ap",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("* from tr_supplier  order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%') order by i_supplier ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		/*$this->db->select("	* from tm_dn_ap a
			left join tr_supplier b using(i_supplier)
				where a.i_supplier='$isupplier' and
										  a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_dn_ap <= to_date('$dto','dd-mm-yyyy') ",false)->limit($num,$offset);*/
		$this->db->select("  
				a.*,
				b.e_supplier_name
			from 
				tm_dn_ap a 
				left join tr_supplier b on (a.i_supplier=b.i_supplier)
			where 
				a.i_supplier >= '$isupplier'
				and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
				and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
			order by 
				a.d_dn_ap", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	 
				a.*,
				b.e_supplier_name
			from 
				tm_dn_ap a 
				left join tr_supplier b on (a.i_supplier=b.i_supplier)
			where 
				a.i_supplier >= '$isupplier'
				and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
				and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
				and (a.i_supplier like '%$cari%' 
				or upper(b.e_supplier_name) like '%$cari%' 
				or upper(a.i_dn_ap) like '%$cari%' 
				or upper(a.i_refference) like '%$cari%')
			order by 
				a.d_dn_ap",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
