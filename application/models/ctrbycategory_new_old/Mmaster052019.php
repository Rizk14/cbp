<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    {
      $pecah1       = explode('-', $dfrom);
        $tgl1       = $pecah1[0];
        $bln1       = $pecah1[1];
        $tahun1     = $pecah1[2];
        $tahunprev1 = intval($tahun1) - 1;

      $pecah2       = explode('-', $dto);
        $tgl2       = $pecah2[0];
        $bln2       = $pecah2[1];
        $tahun2     = $pecah2[2];
        $tahunprev2 = intval($tahun2) - 1;

      $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
      $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;

      $this->db->select(" sum(a.oa) as oa, sum(a.oaprev) as oaprev, sum(a.ob) as ob, sum(a.vnota) as vnota, 
                          sum(a.vnotaprev) as vnotaprev, sum(a.qty) as qty, sum(a.qtyprev) as qtyprev, a.e_product_categoryname from(
                                select sum(z.oa) as oa, 0 as oaprev, sum(z.ob) as ob, sum(z.vnota) as vnota, 0 as vnotaprev, sum(z.qty) as qty, 0 as qtyprev, z.e_product_categoryname from(
                                  select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.e_product_categoryname from (
                                    select c.i_product_category, c.e_product_categoryname, 
                                    sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal)))  as vnota, sum(a.n_deliver) as qty
                                    from tm_nota_item a, tm_nota b, tr_product_category c, tr_product d
                                    where b.f_nota_cancel = 'f'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_product = d.i_product
                                    and d.i_product_category = c.i_product_category
                                    and not b.i_nota isnull
                                    and b.d_nota >= to_date('$dfrom','dd-mm-yyyy') and b.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    group by c.i_product_category,c.e_product_categoryname
                                  ) as a
                                  group by a.e_product_categoryname
                                  
                                  union all

                                  select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.e_product_categoryname from (
                                      select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, e.e_product_categoryname, e.i_product_category
                                      from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e 
                                      where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                        and a.f_nota_cancel='false'
                                        and a.i_nota = b.i_nota and a.i_area = b.i_area
                                        and a.i_customer = c.i_customer
                                        and b.i_product = d.i_product
                                        and d.i_product_category = e.i_product_category
                                        and not a.i_nota isnull
                                      group by e.i_product_category, e.e_product_categoryname,a.i_customer, to_char(a.d_nota,'yyyymm')
                                  ) as a
                                  group by a.e_product_categoryname

                                  union all

                                  select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.e_product_categoryname from (
                                        select distinct(a.i_customer) as ob, e.e_product_categoryname, e.i_product_category 
                                        from tm_nota a, tm_nota_item b, tr_customer c,tr_product d, tr_product_category e 
                                        where a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and a.i_nota = b.i_nota and a.i_area = b.i_area
                                    and b.i_product = d.i_product
                                    and d.i_product_category = e.i_product_category
                                    and not a.i_nota isnull
                                        group by e.i_product_category, e.e_product_categoryname,a.i_customer
                                  ) as a
                                  group by a.e_product_categoryname
                                ) as z
                                group by z.e_product_categoryname
                                
                                union all

                                ------------------------------------------- batas tahun lalu -----------------------------------------
                                select 0 as oa, sum(z.oaprev) as oaprev, 0 as ob, 0 as vnota, sum(z.vnotaprev) as vnotaprev, 0 as qty, sum(z.qtyprev) as qtyprev, z.e_product_categoryname from(
                                  select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.e_product_categoryname from (
                                    select c.i_product_category, c.e_product_categoryname, 
                                    sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal)))  as vnota, sum(a.n_deliver) as qty
                                    from tm_nota_item a, tm_nota b, tr_product_category c, tr_product d
                                    where b.f_nota_cancel = 'f'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_product = d.i_product
                                    and d.i_product_category = c.i_product_category
                                    and not b.i_nota isnull
                                    and b.d_nota >= to_date('$gabung1','dd-mm-yyyy') and b.d_nota <= to_date('$gabung2','dd-mm-yyyy')
                                    group by c.i_product_category,c.e_product_categoryname
                                  ) as a
                                  group by a.e_product_categoryname
                                  
                                  union all

                                  select count(a.oa) as oaprev, 0 as vnotaprev, 0 as qtyprev, a.e_product_categoryname from (
                                      select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, e.e_product_categoryname, e.i_product_category
                                      from tm_nota a, tm_nota_item b, tr_customer c, tr_product d, tr_product_category e 
                                      where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                        and a.f_nota_cancel='false'
                                        and a.i_nota = b.i_nota and a.i_area = b.i_area
                                        and a.i_customer = c.i_customer
                                        and b.i_product = d.i_product
                                        and d.i_product_category = e.i_product_category
                                        and not a.i_nota isnull
                                      group by e.i_product_category, e.e_product_categoryname,a.i_customer, to_char(a.d_nota,'yyyymm')
                                  ) as a
                                  group by a.e_product_categoryname

                                ) as z
                                group by z.e_product_categoryname
                              ) as a 
                              group by a.e_product_categoryname
                              order by a.e_product_categoryname ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
