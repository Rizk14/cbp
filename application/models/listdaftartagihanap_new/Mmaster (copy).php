<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idt,$iarea,$isupplier) 
    {
	      $sql	= " select i_supplier_group, i_supplier FROM TR_SUPPLIER where i_supplier = '$isupplier'";
	      $rs		= pg_query($sql);
	      while($row=pg_fetch_assoc($rs)){
		      $igroup   = $row['i_supplier_group'];
          if($igroup=='G0000'){
            $sql	= " select * from tm_dtap_item where i_dtap='$idt'";
            $rs		= pg_query($sql);
            while($row=pg_fetch_assoc($rs)){
	            $iap    = $row['i_do'];
              $sql2	  = "update tm_ap set f_ap_cancel='t' where i_ap='$iap' and i_supplier ='$isupplier'";
          		pg_query($sql2);
              }
            }
    		$this->db->query("update tm_dtap set f_dtap_cancel='t' WHERE i_dtap='$idt' and i_area='$iarea' and i_supplier='$isupplier'");

        $sql	= " select * from tm_dtap_item where i_dtap='$idt'";
        $rs		= pg_query($sql);
        while($row=pg_fetch_assoc($rs)){
          $ido        = $row['i_do'];
          $isupplier  = $row['i_supplier'];
          $iop        = $row['i_op'];
          $sql2	  = "update tm_do set i_faktur='' where i_do='$ido' and i_op='$iop' and i_supplier ='$isupplier'";
      		pg_query($sql2);
          }

      }
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							order by a.i_dtap desc",false)->limit($num,$offset);
		$query = $this->db->get(); 
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							order by a.i_dtap desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
    	if($isupplier!='AS'){
/*
		$this->db->select("
				distinct on(a.i_supplier, a.i_dtap) a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, f.v_sisa as sisa2,(a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa+f.v_sisa) as jumlah2 , e.d_alokasi as tgl1, g.d_alokasi as tgl2
				from  tr_area b, tr_supplier c, tm_dtap a

				left join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
				left join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier)

				left join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
				left join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier)

				where a.i_area=b.i_area and a.i_supplier=c.i_supplier
				and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
				and a.f_dtap_cancel = 'f'
				and a.i_supplier='$isupplier'

				ORDER BY a.i_supplier, a.i_dtap",false)->limit($num,$offset);
*/
		$this->db->select("  * from (
                        SELECT a.i_supplier, a.f_dtap_cancel, a.i_dtap, a.d_dtap, a.d_due_date, c.e_supplier_name, a.v_netto, a.v_sisa
                        from tr_supplier c, tm_dtap a
                        where a.i_supplier=c.i_supplier
                        and a.d_dtap >= to_date('$dfrom', 'dd-mm-yyyy') AND a.d_dtap <= to_date('$dto', 'dd-mm-yyyy')
                        and a.f_dtap_cancel = 'f' and a.i_supplier='$isupplier' and v_sisa>0
                        union all
                        SELECT a.i_supplier, 'f' as f_dtap_cancel, b.i_nota as i_dtap, b.d_nota as d_dtap, z.d_due_date, c.e_supplier_name, b.v_jumlah as v_netto, b.v_sisa
                        from tr_supplier c, tm_alokasi_bk a, tm_alokasi_bk_item b, tm_dtap z
                        where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                        and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
                        and a.f_alokasi_cancel = 'f' and a.i_supplier='$isupplier' 
                        and b.i_nota = z.i_dtap
                        and b.i_supplier = z.i_supplier
                        union all
                        SELECT a.i_supplier, 'f' as f_dtap_cancel, b.i_nota as i_dtap, b.d_nota as d_dtap, z.d_due_date, c.e_supplier_name, b.v_jumlah as v_netto, b.v_sisa
                        from tr_supplier c, tm_alokasi_kb a, tm_alokasi_kb_item b, tm_dtap z
                        where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                        and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
                        and a.f_alokasi_cancel = 'f' and a.i_supplier='$isupplier' 
                        and b.i_nota = z.i_dtap
                        and b.i_supplier = z.i_supplier
                        ) as a

                        where a.i_dtap||a.d_dtap||a.i_supplier not in(
                        SELECT b.i_nota||b.d_nota||a.i_supplier
                         from tr_supplier c, tm_alokasi_bk a, tm_alokasi_bk_item b
                         where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                         and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
                         and a.f_alokasi_cancel = 'f' and a.i_supplier='$isupplier' 
                         union all
                         SELECT b.i_nota||b.d_nota||a.i_supplier
                         from tr_supplier c, tm_alokasi_kb a, tm_alokasi_kb_item b
                         where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                         and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
                         and a.f_alokasi_cancel = 'f' and a.i_supplier='$isupplier' 
                        )

                        ORDER BY a.i_supplier, a.i_dtap",false)->limit($num,$offset);
		}else{
/*
			$this->db->select("
				distinct a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, f.v_sisa as sisa2,(a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa+f.v_sisa) as jumlah2 , e.d_alokasi as tgl1, g.d_alokasi as tgl2
				from  tr_area b, tr_supplier c, tm_dtap a
				left join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
				left join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier)
				left join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
				left join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier)
				where a.i_area=b.i_area and a.i_supplier=c.i_supplier
				and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
				and a.f_dtap_cancel = 'f'
				ORDER BY a.i_supplier, a.i_dtap",false);
*/
		$this->db->select("  * from (
                        SELECT a.i_supplier, a.f_dtap_cancel, a.i_dtap, a.d_dtap, a.d_due_date, c.e_supplier_name, a.v_netto, a.v_sisa
                        from tr_supplier c, tm_dtap a
                        where a.i_supplier=c.i_supplier
                        and a.d_dtap >= to_date('$dfrom', 'dd-mm-yyyy') AND a.d_dtap <= to_date('$dto', 'dd-mm-yyyy')
                        and a.f_dtap_cancel = 'f' and v_sisa>0
                        union all
                        SELECT a.i_supplier, 'f' as f_dtap_cancel, b.i_nota as i_dtap, b.d_nota as d_dtap, z.d_due_date, c.e_supplier_name, b.v_jumlah as v_netto, b.v_sisa
                        from tr_supplier c, tm_alokasi_bk a, tm_alokasi_bk_item b, tm_dtap z
                        where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                        and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
                        and a.f_alokasi_cancel = 'f'
                        and b.i_nota = z.i_dtap
                        and b.i_supplier = z.i_supplier
                        union all
                        SELECT a.i_supplier, 'f' as f_dtap_cancel, b.i_nota as i_dtap, b.d_nota as d_dtap, z.d_due_date, c.e_supplier_name, b.v_jumlah as v_netto, b.v_sisa
                        from tr_supplier c, tm_alokasi_kb a, tm_alokasi_kb_item b, tm_dtap z
                        where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                        and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
                        and a.f_alokasi_cancel = 'f'
                        and b.i_nota = z.i_dtap
                        and b.i_supplier = z.i_supplier
                        ) as a

                        where a.i_dtap||a.d_dtap||a.i_supplier not in(
                        SELECT b.i_nota||b.d_nota||a.i_supplier
                         from tr_supplier c, tm_alokasi_bk a, tm_alokasi_bk_item b
                         where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                         and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
                         and a.f_alokasi_cancel = 'f'
                         union all
                         SELECT b.i_nota||b.d_nota||a.i_supplier
                         from tr_supplier c, tm_alokasi_kb a, tm_alokasi_kb_item b
                         where a.i_alokasi=b.i_alokasi and a.i_supplier=c.i_supplier
                         and a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
                         and a.f_alokasi_cancel = 'f'
                        )

                        ORDER BY a.i_supplier, a.i_dtap",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							and a.i_supplier='$isupplier' and
							a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_dtap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select(" * from tr_supplier order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
