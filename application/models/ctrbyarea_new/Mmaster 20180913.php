<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto,$iproductgroup)
    {
      $pecah1       = explode('-', $dfrom);
        $tgl1       = $pecah1[0];
        $bln1       = $pecah1[1];
        $tahun1     = $pecah1[2];
        $thbl		= $tahun1.$bln1;
        $tahunprev1 = intval($tahun1) - 1;
        $thbln = $tahun1."-".$bln1;
                $tsasih = date('Y-m', strtotime('-24 month', strtotime($thbln))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
        $smn = explode("-", $tsasih);
        $thn = $smn[0];
        $bln = $smn[1];
        }
        $taunsasih = $thn.$bln;

      $pecah2       = explode('-', $dto);
        $tgl2       = $pecah2[0];
        $bln2       = $pecah2[1];
        $tahun2     = $pecah2[2];
        $thblto 	= $tahun2.$bln2;
        $tahunprev2 = intval($tahun2) - 1;

      $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
      $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;

      if($iproductgroup == "NA"){
/*
          $query = "select sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island , c.e_provinsi
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname , b.e_provinsi
                              from (
                                select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char(a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                                union all
                                
                                select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname , d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy'),d.e_provinsi

                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                union all

                                select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, '' as e_product_groupname,a.e_provinsi from (
                                    select distinct on (a.i_customer) a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                    from tm_nota a , tr_customer b , tr_area c
                                    where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                                    and a.f_nota_cancel='false'
                                    and a.i_customer=b.i_customer 
                                    and b.i_customer_status<>'4'
                                    and b.f_customer_aktif='true'
                                    and a.i_area=c.i_area
                                    and c.f_area_real='t'
                                    and not a.i_nota isnull
                                    ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island,a.e_provinsi
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname , b.e_provinsi
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname,b.e_provinsi from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                                union all
                                
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname,b.e_provinsi
                          
                          ) as c
                          group by c.e_provinsi ,c.i_area,c.e_area_name,c.e_area_island
                          order by c.e_area_island,c.e_provinsi ,c.i_area, c.e_area_name";
      $query = $this->db->query($query);
      if ($query->num_rows() > 0){
        return $query->result();
      }
*/
          $query = "select sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island , c.e_provinsi
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname , b.e_provinsi
                              from (
                                select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char(a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                union all
                                select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname , d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                union all

                                  select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, '' as e_product_groupname,a.e_provinsi from (
                                  select distinct on (x.ob) x.ob as ob, x.i_area, x.e_area_name ,x.e_area_island , x.e_provinsi from(
                                  select a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                  from tm_nota a , tr_area c
                                  where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                                  and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
                                  union all
                                  select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                  from tr_area c, tr_customer b 
                                  where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
                                  )
                                  as x
                                  order by x.ob
                                  ) as a
                                  group by a.i_area,a.e_area_name,a.e_area_island,a.e_provinsi

                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname , b.e_provinsi
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname,b.e_provinsi from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                union all
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname,b.e_provinsi
                          ) as c
                          group by c.e_provinsi ,c.i_area,c.e_area_name,c.e_area_island
                          order by c.e_area_island,c.e_provinsi ,c.i_area, c.e_area_name";
        $query = $this->db->query($query);
        if ($query->num_rows() > 0){
          return $query->result();
        }
      }elseif($iproductgroup == "MO"){
          $query = "select sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island , c.e_provinsi
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname , b.e_provinsi
                              from (
                                select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.f_spb_consigment = 't'
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char(a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                                union all
                                
                                select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname , d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.f_spb_consigment = 't'
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy'),d.e_provinsi

                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                union all

                                  select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, '' as e_product_groupname,a.e_provinsi from (
                                  select distinct on (x.ob) x.ob as ob, x.i_area, x.e_area_name ,x.e_area_island , x.e_provinsi from(
                                  select a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                  from tm_nota a , tr_area c, tm_spb d
                                  where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                                  and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
								                  and a.i_spb = d.i_spb and a.i_area = 'PB'
								                  and a.i_area = d.i_area
								                  and a.i_customer = d.i_customer
								                  and not d.i_spb isnull
								                  and not d.i_nota isnull
								                  and d.f_spb_consigment = 't'
                                  union all
                                  select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                  from tr_area c, tr_customer b 
                                  where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
                                  and b.i_customer like 'PB%'
                                  )
                                  as x
                                  order by x.ob
                                  ) as a
                                  group by a.i_area,a.e_area_name,a.e_area_island,a.e_provinsi
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname , b.e_provinsi
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname,b.e_provinsi from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and e.f_spb_consigment = 't'
                                    and a.i_area = e.i_area
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyymm'),d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                                union all
                                
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname,a.e_provinsi from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.f_spb_consigment = 't'
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,d.e_provinsi
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname,b.e_provinsi
                          
                          ) as c
                          group by c.e_provinsi ,c.i_area,c.e_area_name,c.e_area_island
                          order by c.e_area_island,c.e_provinsi ,c.i_area, c.e_area_name";
      $query = $this->db->query($query);
      if ($query->num_rows() > 0){
        return $query->result();
      }
      }else{
          $query = "select sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island , c.e_provinsi, c.i_product_group, c.e_product_groupname 
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname , b.e_provinsi, b.i_product_group
                              from (
                                select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi, a.i_product_group from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, d.e_provinsi,  e.i_product_group
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and not a.i_nota isnull and a.i_area = d.i_area
                                    and a.i_area = c.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                    and e.f_spb_cancel='false'
                                    and e.f_spb_consigment = 'false'
                                    and a.i_area=e.i_area and a.i_nota=e.i_nota and e.i_product_group=f.i_product_group
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi, a.i_product_group
                                
                                union all
                                
                                select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname ,a.e_provinsi, a.i_product_group from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname , d.e_provinsi , e.i_product_group
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.f_spb_consigment = 'f'
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                    group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy'),d.e_provinsi, e.i_product_group

                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi,a.i_product_group
                                union all

                                  select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname as e_product_groupname,a.e_provinsi, a.i_product_group from (
                                  select distinct on (x.ob) x.ob as ob, x.i_area, x.e_area_name ,x.e_area_island, x.e_product_groupname , x.e_provinsi, x.i_product_group  from(
                                  select a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island, e.e_product_groupname, c.e_provinsi, d.i_product_group  
                                  from tm_nota a , tr_area c, tm_spb d, tr_product_group e
                                  where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                                  and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
								                  and a.i_spb = d.i_spb --and a.i_area <> 'PB'
								                  and a.i_area = d.i_area
								                  and a.i_customer = d.i_customer
								                  and not d.i_spb isnull
								                  and not d.i_nota isnull
								                  and d.f_spb_consigment = 'f' and d.i_product_group=e.i_product_group
                                  union all
                                  select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi, '' as e_product_groupname, '' as i_product_group
                                  from tr_area c, tr_customer b 
                                  where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
                                  and b.i_customer not like 'PB%'
                                  )
                                  as x
                                  ) as a
                                  group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname, a.e_provinsi, a.i_product_group


                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname , b.e_provinsi, b.i_product_group
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname,b.e_provinsi, b.i_product_group from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname,a.e_provinsi, a.i_product_group from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi, e.i_product_group
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    
                                    and a.f_nota_cancel='false'
                                    and not a.i_nota isnull and a.i_area = d.i_area
                                    and a.i_area = c.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_customer_status<>'4'
                                    and c.f_customer_aktif='true'
                                    and e.f_spb_cancel='false'
                                    and e.f_spb_consigment = 'false'
                                    and a.i_area=e.i_area and a.i_nota=e.i_nota and e.i_product_group=f.i_product_group
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi,a.i_product_group
                                
                                union all
                                
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname,a.e_provinsi, a.i_product_group from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname,d.e_provinsi, e.i_product_group
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.f_spb_consigment = 'f'
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,d.e_provinsi, e.i_product_group
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname,a.e_provinsi,a.i_product_group
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname,b.e_provinsi,b.i_product_group
                          
                          ) as c where c.i_product_group = '$iproductgroup'
                          group by c.e_provinsi ,c.i_area,c.e_area_name,c.e_area_island, c.i_product_group, c.e_product_groupname 
                          order by c.e_area_island,c.e_provinsi ,c.i_area, c.e_area_name";
      $query = $this->db->query($query);
      if ($query->num_rows() > 0){
        return $query->result();
      }
      }
    }


    function bacaproductgroup()
    {
      $this->db->select(" * from tr_product_group",false);
    
      $query = $this->db->get();
    
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
