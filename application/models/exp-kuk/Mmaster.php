<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($datefrom,$dateto)
    {
		$this->db->select("	SELECT distinct a.i_supplier, a.d_kuk, a.f_kuk_cancel, a.i_kuk, a.d_kuk, a.e_bank_name, b.e_supplier_name,
                        a.e_remark, a.v_jumlah, a.v_sisa, a.f_close, a.n_kuk_year, c.i_pelunasanap, c.d_bukti, c.i_giro
                         from tm_kuk a
                         left join tr_supplier b on(a.i_supplier=b.i_supplier)
                         left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' and
                         ((c.i_jenis_bayar!='02' and 
                         c.i_jenis_bayar!='01' and 
                         c.i_jenis_bayar!='04' and 
                         c.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)))
                         where (a.d_kuk >= to_date('$datefrom', 'dd-mm-yyyy') and
                         a.d_kuk <= to_date('$dateto', 'dd-mm-yyyy'))
                         ORDER BY a.d_kuk, a.i_supplier, a.i_kuk",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "mm": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
}
?>
