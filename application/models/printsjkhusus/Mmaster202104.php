<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_op WHERE i_op=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_op_item WHERE i_op=\''.$iop.'\'');
			return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5,$iuser,$dfrom,$dto)
    {
      if($area1!='PB'){
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										and a.f_nota_cancel = 'f'
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser')) 
										
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										and a.f_nota_cancel = 'f'
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and (substring(a.i_sj,9,2)='$area1' or substring(a.i_sj,9,2) = '$area2' or substring(a.i_sj,9,2) = '$area3' 
										or substring(a.i_sj,9,2) = '$area4' or substring(a.i_sj,9,2) = '$area5' or substring(a.i_sj,9,2) = 'BK')
										 
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($isj,$area)
    {
		$this->db->select(" tm_nota.*, c.e_customer_name, c.e_customer_sendaddress, c.e_customer_city, c.f_customer_plusppn as f_plus_ppn,
							c.e_customer_phone, tr_area.e_area_name, tm_spb.i_spb_po, tm_spb.f_spb_consigment, tr_area.e_area_phone, d.e_customer_ownername
							, e.e_customer_pkpname, c.f_customer_pkp from tm_nota
							          inner join tr_customer c on (tm_nota.i_customer=c.i_customer)
							           inner join tr_customer_pkp e on (tm_nota.i_customer=e.i_customer)
							          inner join tr_area on (substring(tm_nota.i_sj,9,2)=tr_area.i_area)
							                inner join tr_customer_owner d on (tm_nota.i_customer=d.i_customer)
											left join tm_spb on (tm_spb.i_spb=tm_nota.i_spb and tm_spb.i_area=tm_nota.i_area)
							          where tm_nota.i_sj = '$isj' and substring(tm_nota.i_sj,9,2)='$area'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($isj,$area)
    {
      $cust='';
		  $tes=$this->db->query("select i_customer from tm_nota where i_sj = '$isj' and substring(i_sj,9,2)='$area'",false);
		  if ($tes->num_rows() > 0){
        foreach($tes->result() as $xx){
  			  $cust=$xx->i_customer;
        }
		  }
      $group='';
		  $que 	= $this->db->query(" select i_customer_plugroup from tr_customer_plugroup where i_customer='$cust'",false);
		  if($que->num_rows()>0){
        foreach($que->result() as $hmm){
          $group=$hmm->i_customer_plugroup;
        }
      }
      if($group==''){
		    $this->db->select(" * from tm_nota_item
							              inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
														              and tm_nota_item.i_product=tr_product_motif.i_product)
							              where i_sj = '$isj' and substring(i_sj,9,2)='$area' order by n_item_no",false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
		    $this->db->select(" a.i_sj, a.i_nota, a.i_product as product, a.i_product_grade, a.i_product_motif, a.n_deliver, a.v_unit_price,
                            a.e_product_name, a.i_area, a.d_nota, a.n_item_no, c.i_customer_plu, c.i_product from tm_nota_item a
							              inner join tr_product_motif b on (a.i_product_motif=b.i_product_motif and a.i_product=b.i_product)
                            left join tr_customer_plu c on (c.i_customer_plugroup='$group' and a.i_product=c.i_product) 
							              where i_sj = '$isj' and substring(i_sj,9,2)='$area' order by n_item_no",false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto)
    {
		if($area1!='PB'){
			$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										  where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										  and a.f_nota_cancel = 'f'
										  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										  or upper(a.i_sj) like '%$cari%')
										  and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser')) 
										  
													and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
													order by a.i_sj desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										  where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										  and a.f_nota_cancel = 'f'
										  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										  or upper(a.i_sj) like '%$cari%')
										  and (substring(a.i_sj,9,2)='$area1' or substring(a.i_sj,9,2) = '$area2' or substring(a.i_sj,9,2) = '$area3' 
										  or substring(a.i_sj,9,2) = '$area4' or substring(a.i_sj,9,2) = '$area5' or substring(a.i_sj,9,2) = 'BK')
										   
													and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
													order by a.i_sj desc",false)->limit($num,$offset);
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function updatesj($isj, $area)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dprint	= $row->c;
          $this->db->set(
      		array(
			  'd_sj_print'			=> $dprint
      		)
      	);
		  $this->db->where('i_sj', $isj);
		 // $this->db->where('i_area', $area);
		  $this->db->update('tm_nota'); 
    }
}
?>
