<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
		  $area2	= $this->session->userdata('i_area2');
		  $area3	= $this->session->userdata('i_area3');
		  $area4	= $this->session->userdata('i_area4');
		  $area5	= $this->session->userdata('i_area5');
		  $this->db->select("	a.*,b.e_salesman_name,c.v_target as v_target_area,d.e_area_name 
							            from tm_target_itemsls a, tr_salesman b, tm_target c, tr_area d
							            where a.i_periode = '$iperiode'
							            and a.i_area=c.i_area and a.i_periode=c.i_periode
							            and a.i_area=d.i_area
							            and a.i_salesman=b.i_salesman
                          and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
							            order by a.i_area, a.i_salesman",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
		  $area2	= $this->session->userdata('i_area2');
		  $area3	= $this->session->userdata('i_area3');
		  $area4	= $this->session->userdata('i_area4');
		  $area5	= $this->session->userdata('i_area5');
		  $this->db->select("	a.*, b.e_salesman_name, c.v_target as v_target_area,d.e_area_name 
							            from tm_target_itemsls a, tr_salesman b, tm_target c, tr_area d
							            where a.i_periode = '$iperiode'
							            and a.i_area=c.i_area and a.i_periode=c.i_periode
							            and a.i_area=d.i_area
							            and a.i_salesman=b.i_salesman
							            and(upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
							            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                          order by a.i_area, a.i_salesman ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
