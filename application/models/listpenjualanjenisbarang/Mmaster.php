<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
    $tahun = substr($iperiode,2,4);
		  $this->db->select(" tm_nota_item.i_product, tm_nota_item.e_product_name, tm_nota_item.i_product_motif, tm_nota_item.i_product_grade,
		                      tr_product_type.e_product_typename, sum(tm_nota_item.n_deliver) as n_deliver, 
		                      tr_harga_beli.v_product_mill, (tr_harga_beli.v_product_mill* sum(tm_nota_item.n_deliver)) as total_harga_beli,
		                      tm_nota_item.v_unit_price, (tm_nota_item.v_unit_price * sum(tm_nota_item.n_deliver)) as total_harga_jual
		                      FROM public.tm_nota, public.tm_nota_item, public.tr_harga_beli, public.tr_product_type, public.tr_product
		                      WHERE tm_nota.i_sj = tm_nota_item.i_sj AND  tm_nota.i_area = tm_nota_item.i_area AND
		                            tr_harga_beli.i_product = tm_nota_item.i_product AND 
		                            tr_harga_beli.i_product_grade = tm_nota_item.i_product_grade AND
		                            tm_nota.f_nota_cancel = FALSE AND tr_harga_beli.i_price_group = '00' AND 
		                            tm_nota.i_nota IS NOT NULL  AND tm_nota.i_nota LIKE 'FP-$tahun%' AND
		                            tr_product.i_product_type=tr_product_type.i_product_type AND tr_product.i_product=tm_nota_item.i_product
                          GROUP BY
                                tm_nota_item.i_product, tm_nota_item.e_product_name, tm_nota_item.i_product_motif, tm_nota_item.i_product_grade,
                                tr_product_type.e_product_typename, tr_harga_beli.v_product_mill, tm_nota_item.v_unit_price
                          ORDER BY tm_nota_item.i_product
		   ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
