<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tm_pelunasanap 
							inner join tr_supplier on (tr_supplier.i_supplier=tm_pelunasanap.i_supplier)
							where f_posting='f'
							order by i_pelunasanap desc, i_area ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_giro where f_posting='f'
							and (i_rv like '%$cari%' or i_giro like ''%$cari%)
							order by i_rv desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function sisa($ipl,$iarea,$dbukti){
		$this->db->select(" sum(v_sisa+v_jumlah)as sisa
							from tm_pelunasanap_item
							where i_pelunasanap='$ipl'
							and i_area='$iarea'
							and d_bukti='$dbukti'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->sisa;
		}	
	}
	function jumlahbayar($ipl,$iarea,$dbukti){
		$this->db->select(" sum(v_jumlah)as jumlah
							from tm_pelunasanap_item
							where i_pelunasanap='$ipl'
							and i_area='$iarea'
							and d_bukti='$dbukti'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->jumlah;
		}	
	}
	function bacapl($ipl,$iarea,$dbukti){
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name, d.e_jenis_bayarname, c.e_supplier_address, c.e_supplier_city
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier 
							and a.i_jenis_bayar=d.i_jenis_bayar 
							and a.d_bukti='$dbukti' and upper(a.i_pelunasanap)='$ipl' and upper(a.i_area)='$iarea'",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadetailpl($ipl,$iarea,$dbukti){
		$this->db->select(" * from tm_pelunasanap_item
							where i_pelunasanap = '$ipl' 
							and i_area='$iarea'
							and d_bukti='$dbukti'
							order by i_pelunasanap,i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ipelunasan,$iarea,$egirodescription,$fclose,$dbukti )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjumlah,'$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjumlah','$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function updatepelunasan($ipl,$iarea,$dbukti)
    {
		$this->db->query("update tm_pelunasanap set f_posting='t' where i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
