<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($m2,$mm,$num,$offset)
    {
		  $this->db->select(" * from tr_kendaraan
							  inner join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							  inner join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							  inner join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							  where (to_char(tr_kendaraan.d_pajak,'yyyymm')='$m2' or to_char(tr_kendaraan.d_pajak,'yyyymm')='$mm') and tr_kendaraan.i_periode='$mm'
							  order by tr_kendaraan.d_pajak desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
  		}
    }
    function cari($m2,$mm, $num, $offset)
    {
		  $this->db->select(" * from tr_kendaraan 
							            inner join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							            inner join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							            inner join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							            where upper(tr_kendaraan.i_kendaraan) like '%$cari%' 
							            and tr_kendaraan.i_periode='$iperiode' and to_char(tr_kendaraan.d_pajak,'yyyymm')='$m2' and tr_kendaraan.i_periode='$mm'
							            order by tr_kendaraan.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
