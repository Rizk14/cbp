<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num, $offset)
    {
		  $this->db->select(" a.i_product, a.i_price_group, a.e_product_name, a.v_product_retail, 
                          to_char(a.d_product_priceentry,'yyyy-mm-dd') as d_product_priceentry, 
                          to_char(a.d_product_priceupdate,'yyyy-mm-dd') as d_product_priceupdate
                          from tr_product_price a, tr_product b, tr_price_group c
                          where a.i_product=b.i_product and a.i_price_group=c.i_price_group
                          and b.f_product_pricelist='t'
                          and (a.i_product like '%$cari%' or a.e_product_name like '%$cari%')
          							  order by a.e_product_name, a.i_price_group",false)->limit($num,$offset);

		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
