<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibbktype)
    {
		$this->db->select('i_bbk_type, e_bbk_typename')->from('tr_bbk_type')->where('i_bbk_type', $ibbktype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($ibbktype, $ebbktypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
    			'i_bbk_type' 		=> $ibbktype,
    			'e_bbk_typename' 	=> $ebbktypename,
				'd_bbk_typeentry' 	=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_bbk_type');
		redirect('bbktype/cform/');
    }
    function update($ibbktype, $ebbktypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
    	$data = array(
               'i_bbk_type' => $ibbktype,
               'e_bbk_typename' => $ebbktypename,
	       'd_bbk_typeupdate' => $dupdate
            );
		$this->db->where('i_bbk_type', $ibbktype);
		$this->db->update('tr_bbk_type', $data); 
		redirect('bbktype/cform/');
    }
	
    public function delete($ibbktype) 
    {
		$this->db->query('DELETE FROM tr_bbk_type WHERE i_bbk_type=\''.$ibbktype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_bbk_type, e_bbk_typename from tr_bbk_type order by i_bbk_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
