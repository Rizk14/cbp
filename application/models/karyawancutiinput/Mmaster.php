<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iperiode,$ikendaraan,$i_kendaraan_asuransi)
    {
    	if($i_kendaraan_asuransi!=NULL){
		$this->db->select("	* from tr_kendaraan a 
							          left join tr_area b on(a.i_area=b.i_area)
							          left join tr_kendaraan_jenis c on(a.i_kendaraan_jenis=c.i_kendaraan_jenis)
							          left join tr_kendaraan_bbm d on(a.i_kendaraan_bbm=d.i_kendaraan_bbm)
							          left join tr_kendaraan_item e on (e.i_kendaraan=a.i_kendaraan)
									  left join tr_kendaraan_asuransi f on (f.i_kendaraan_asuransi=e.i_kendaraan_asuransi)
									  left join tr_kendaraan_pengguna g on (g.i_kendaraan=a.i_kendaraan)
							          where g.i_periode='$iperiode' and a.i_kendaraan='$ikendaraan' and f.i_kendaraan_asuransi='$i_kendaraan_asuransi'",false);
										}else{
		$this->db->select("	* from tr_kendaraan a 
							          left join tr_area b on(a.i_area=b.i_area)
							          left join tr_kendaraan_jenis c on(a.i_kendaraan_jenis=c.i_kendaraan_jenis)
							          left join tr_kendaraan_bbm d on(a.i_kendaraan_bbm=d.i_kendaraan_bbm)
							          left join tr_kendaraan_item e on (e.i_kendaraan=a.i_kendaraan)
									  left join tr_kendaraan_asuransi f on (f.i_kendaraan_asuransi=e.i_kendaraan_asuransi)
									  left join tr_kendaraan_pengguna g on (g.i_kendaraan=a.i_kendaraan)
							          where g.i_periode='$iperiode' and a.i_kendaraan='$ikendaraan'",false);
										}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select(" * from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or e_area_name like '%$cari%'
							order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
    function bacakaryawan($th_skrng,$num,$offset)
    {
		$this->db->select(" * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department) 
			   left join tr_area e on(a.i_area=e.i_area)
			   left join tr_karyawan_cuti f on(a.i_nik=f.i_nik) where f.v_saldo_cuti <> 0 and f.i_periode='$th_skrng' order by a.i_nik",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikaryawan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department) 
			   left join tr_area e on(a.i_area=e.i_area)
			   left join tr_karyawan_cuti f on(a.i_nik=f.i_nik)
			   where upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%' where f.v_saldo_cuti <> 0 order by a.i_nik",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function ceksaldocuti($i_nik)
    {
		$this->db->select(" distinct * from tr_karyawan_cuti where i_nik='$i_nik'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
        { return $query->row_array();
        }
        else {return NULL;}
    }

    function simpancutidetail($i_nik,$d_cuti_awal,$d_cuti_akhir,$v_jumlah_cuti,$e_alasan,$periode)
    {
    	$this->db->set(
    		array(

    			'i_nik'					=> $i_nik,
				'd_cuti_awal'			=> $d_cuti_awal,
				'd_cuti_akhir'			=> $d_cuti_akhir,
				'v_jumlah_cuti'			=> $v_jumlah_cuti,
				'e_alasan'				=> $e_alasan,
				'i_periode'				=> $periode
    		)
    	);
    	
    	$this->db->insert('tr_karyawan_cuti_item');
    }
    function updatekaryawancuti($i_nik,$v_saldo_akhir)
    {
    	$this->db->set(
    		array(
				
				'v_saldo_cuti'	=> $v_saldo_akhir
    		)
    	);
    	
    	$this->db->where("i_nik",$i_nik);
    	$this->db->update('tr_karyawan_cuti');
    }

}
?>
