<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	function baca($datefrom, $dateto)
	{
		$this->db->select("	distinct a.i_customer, a.d_kum, a.f_kum_cancel, a.i_kum, d.e_area_name,
                          a.d_kum, a.e_bank_name,c.e_customer_name, e.e_customer_setor, a.e_remark,
                          a.v_jumlah, a.v_sisa, a.f_close, a.n_kum_year, d.i_area, g.i_dt, g.d_dt, f.i_giro
                          from tm_kum a
                          left join tr_customer c on(a.i_customer=c.i_customer)
                          left join tr_area d on(a.i_area=d.i_area)
                          left join tr_customer_owner e on(a.i_customer=e.i_customer)
                          left join tm_pelunasan f on(a.i_kum=f.i_giro and a.d_kum=f.d_giro and f.f_pelunasan_cancel='f' 
                          and f.f_giro_tolak='f' and a.i_area=f.i_area)
                          left join tm_dt g on(f.i_dt=g.i_dt and f.d_dt=g.d_dt and f.i_area=g.i_area 
                          and f.f_pelunasan_cancel='f' and f.f_giro_tolak='f' and g.i_area=a.i_area)
                          where (upper(a.i_kum) like '%%') and
                          ((f.i_jenis_bayar!='02' and 
                          f.i_jenis_bayar!='01' and 
                          f.i_jenis_bayar!='04' and 
                          f.i_jenis_bayar='03') or ((f.i_jenis_bayar='03') is null)) and 
                          (a.d_kum >= to_date('$datefrom','dd-mm-yyyy') and
                          a.d_kum <= to_date('$dateto','dd-mm-yyyy')) and a.f_kum_cancel='f'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function dateAdd($interval, $number, $dateTime)
	{
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr = getdate($dateTime);
		$yr = $dateTimeArr['year'];
		$mon = $dateTimeArr['mon'];
		$day = $dateTimeArr['mday'];
		$hr = $dateTimeArr['hours'];
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		switch ($interval) {
			case "s": //seconds
				$sec += $number;
				break;
			case "n": //minutes
				$min += $number;
				break;
			case "h": //hours
				$hr += $number;
				break;
			case "d": //days
				$day += $number;
				break;
			case "ww": //Week
				$day += ($number * 7);
				break;
			case "mm": //similar result "m" dateDiff Microsoft
				$mon += $number;
				break;
			case "yyyy": //similar result "yyyy" dateDiff Microsoft
				$yr += $number;
				break;
			default:
				$day += $number;
		}
		$dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
		$dateTimeArr = getdate($dateTime);
		$nosecmin = 0;
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		if ($hr == 0) {
			$nosecmin += 1;
		}
		if ($min == 0) {
			$nosecmin += 1;
		}
		if ($sec == 0) {
			$nosecmin += 1;
		}
		if ($nosecmin > 2) {
			return (date("Y-m-d", $dateTime));
		} else {
			return (date("Y-m-d G:i:s", $dateTime));
		}
	}
}
