<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iarea,$iperiode,$num,$offset,$cari)
    {
      $this->db->select("	b.i_periode, a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, b.i_city, b.v_target, b.v_nota_grossinsentif, 
                          b.v_real_regularinsentif, b.v_real_regularnoninsentif, b.v_real_babyinsentif, b.v_real_babynoninsentif, 
                          b.v_retur_insentif, b.v_nota_grossnoninsentif, b.v_retur_noninsentif, b.v_spb_gross, e.e_city_name, b.v_nota_netto
                          from tr_area a 
                          inner join tm_target_itemkota b on(a.i_area=b.i_area and b.i_periode='$iperiode' and b.i_area='$iarea') 
                          left join tr_salesman c on(b.i_salesman=c.i_salesman) 
                          inner join tr_city e on(b.i_city=e.i_city and a.i_area=e.i_area) 
                          where a.f_area_real='t' 
                          order by e.e_city_name, i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
