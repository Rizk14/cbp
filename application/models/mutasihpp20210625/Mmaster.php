<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bacaperiode($iperiode)
    {

      $thn = substr($iperiode, 0, 4);
      $bln = substr($iperiode, 4, 2);
      $periodesebelum = date('Ym', strtotime('-1 month', strtotime($thn . '-' . $bln . '-01')));

      $hpp=$this->db->query("SELECT i_product, i_product_motif, i_product_grade, e_product_name, harga, awal, beli, akhir 
                             from f_mutasi_hpp('$periodesebelum', '$iperiode') ");//where i_product='IMK2700'");
                             //where i_product='IMK2700'");
                             //where i_product='TSS1107'");
                             //where i_product='STT6208'");
                             //where i_product='TPT1071'");
                             //where i_product='SKT2101'");
      if($hpp->num_rows()>0){
        $this->db->query("delete from tm_mutasi_hpp where e_periode='$iperiode'");
        $i=1;
//        $this->db->trans_begin();
        $jmlnota=0;
        $kode='';
        $kodenota='';
        foreach($hpp->result() as $row){
          if($row->awal==NULL){
            $row->awal=0;
          }
          if($row->beli==NULL){
            $row->beli=0;
          }
          if($row->akhir==NULL){
            $row->akhir=0;
          }
          $nota=$this->db->query("select i_product, sum(a.n_deliver) as jual
                                  from tm_nota_item a, tm_nota b where a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull 
                                  and to_char(b.d_nota,'yyyymm')='$iperiode' and b.f_nota_cancel='f'
                                  and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' and a.i_product_grade='$row->i_product_grade' 
                                  and a.e_product_name='$row->e_product_name'
                                  group by a.i_product");
          if($nota->num_rows()>0){
            foreach($nota->result() as $rownota){
              if($kodenota!=$rownota->i_product){
                $jmlnota=$rownota->jual;
              }
              if($rownota->jual==null){
                $rownota->jual=0;
              }
              if(($row->awal+$row->beli)==$row->akhir){
                  $data = array(
                      'e_periode'=> $iperiode,
                      'i_product' => $row->i_product,
                      'i_product_motif' => $row->i_product_motif,
                      'i_product_grade' => $row->i_product_grade,
                      'e_product_name' => $row->e_product_name,
                      'v_harga' => $row->harga,
                      'n_awal' => $row->awal,
                      'n_jual' => 0,
                      'n_beli' => $row->beli,
                      'n_akhir' => $row->akhir,
                  );
                  $this->db->insert('tm_mutasi_hpp', $data);
              }elseif(($row->awal+$row->beli)!=$row->akhir){
                  if(($row->awal+$row->beli)<$rownota->jual){
                    $data = array(
                        'e_periode'=> $iperiode,
                        'i_product' => $row->i_product,
                        'i_product_motif' => $row->i_product_motif,
                        'i_product_grade' => $row->i_product_grade,
                        'e_product_name' => $row->e_product_name,
                        'v_harga' => $row->harga,
                        'n_awal' => $row->awal,
                        'n_jual' => $row->awal+$row->beli,
                        'n_beli' => $row->beli,
                        'n_akhir' => $row->akhir,
                    );
                    $this->db->insert('tm_mutasi_hpp', $data);
                    $jmlnota=$jmlnota-($row->awal+$row->beli);
                  }else{
                    $data = array(
                        'e_periode'=> $iperiode,
                        'i_product' => $row->i_product,
                        'i_product_motif' => $row->i_product_motif,
                        'i_product_grade' => $row->i_product_grade,
                        'e_product_name' => $row->e_product_name,
                        'v_harga' => $row->harga,
                        'n_awal' => $row->awal,
                        'n_jual' => $jmlnota,
                        'n_beli' => $row->beli,
                        'n_akhir' => $row->akhir,
                    );
                    $this->db->insert('tm_mutasi_hpp', $data);
                    $jmlnota=0;
                  }
              }
              $kodenota=$rownota->i_product;
            }
          }else{
                $data = array(
                    'e_periode'=> $iperiode,
                    'i_product' => $row->i_product,
                    'i_product_motif' => $row->i_product_motif,
                    'i_product_grade' => $row->i_product_grade,
                    'e_product_name' => $row->e_product_name,
                    'v_harga' => $row->harga,
                    'n_awal' => $row->awal,
                    'n_jual' => 0,
                    'n_beli' => $row->beli,
                    'n_akhir' => $row->akhir,
                );
                $this->db->insert('tm_mutasi_hpp', $data);
          }
          $kode=$row->i_product;
        }
        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
        }else{
//          $this->db->trans_rollback();
          $this->db->trans_commit();
//          echo "Data sudah berhasil di Simpan";
          $data=$this->db->query("select e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_jual, n_akhir, v_harga
                                  from tm_mutasi_hpp where e_periode='$iperiode'");
          if ($data->num_rows() > 0){     
            return $data->result();
          }
        }
      }

/*
          $data=$this->db->query("select e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_jual, n_akhir, v_harga
                                  from tm_mutasi_hpp where e_periode='$iperiode'");
          if ($data->num_rows() > 0){     
            return $data->result();
          }
*/
    }

    public function findWithAttr($array, $attr, $value)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr] === $value) {
                return $i;
            }
        }

        return -1;
    }

    public function findWithAttr2($array, $attr1, $attr2, $value1, $value2)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr1] === $value1 && $array[$i][$attr2] === $value2) {
                return $i;
            }
        }

        return -1;
    }
}
