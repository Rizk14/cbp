<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($dfrom, $dto, $area, $cari, $num,$offset)
    {
		$area1 = $this->session->userdata('i_area');

		if($area == "NA"){
			$this->db->select("	a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
								a.f_spb_stockdaerah
								FROM tm_spb a
								INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
								INNER JOIN tr_area c on(a.i_area = c.i_area)
								LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
								WHERE
								f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
								AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
								AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
								AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
								AND a.f_spb_rekap = 'f'
								AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
								ORDER BY a.d_spb DESC ",false)->limit($num,$offset);
		}else{
			if($area1 == "00"){
				$this->db->select("	a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
									a.f_spb_stockdaerah
									FROM tm_spb a
									INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
									INNER JOIN tr_area c on(a.i_area = c.i_area)
									LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
									WHERE
									f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
									AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
									AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
									AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
									AND (f_spb_stockdaerah='t' OR f_spb_stockdaerah='f')
									AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
									AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
									AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
									ORDER BY a.d_spb DESC ",false)->limit($num,$offset);
			}else{
				$this->db->select("	a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
									a.f_spb_stockdaerah
									FROM tm_spb a
									INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
									INNER JOIN tr_area c on(a.i_area = c.i_area)
									LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
									WHERE
									f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
									AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
									AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
									AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
									AND f_spb_stockdaerah='t'
									AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
									AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
									AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
									ORDER BY a.d_spb DESC ",false)->limit($num,$offset);
			}
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$iuser)
    {
		  #$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $this->db->select('*')->from('tr_area');
		  $this->db->where("i_area in(select(i_area) from tm_user_area where i_user ='$iuser')");
		  $this->db->order_by('i_area');
		  $this->db->limit($num,$offset);

		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function updatespb($ispb,$area)
    {
        $this->db->query(" 	UPDATE tm_spb SET f_spb_siapnotagudang = FALSE, f_spb_siapnotasales = FALSE,
							i_store = NULL, i_store_location = NULL 
							WHERE i_spb = '$ispb' AND i_area='$area' ");

		$this->db->query(" 	UPDATE tm_spb_item SET n_deliver = NULL WHERE i_spb = '$ispb' AND i_area='$area' ");
    }
}
?>