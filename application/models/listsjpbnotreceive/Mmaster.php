<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_sjpb a, tr_area b
													where a.i_area=b.i_area and a.f_sjpb_cancel='f'
													and (upper(a.i_sjpb) like '%$cari%')
													and a.d_sjpb_receive is null
													ORDER BY a.i_sjpb DESC",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isjpb,$iarea)
    {
		$this->db->select("a.*, b.e_area_name 
                        from tm_sjpb a, tr_area b, tm_sjpb_item c
						            where a.i_area=b.i_area and a.i_sjpb=c.i_sjpb 
                        and a.i_area=c.i_area
						            and a.i_sjpb ='$isjpb' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($isjpb, $iarea)
    {
		$this->db->select("a.i_sjpb,a.d_sjpb,a.i_area,a.i_product,a.i_product_grade,a.i_product_motif,
                       a.n_receive,a.n_deliver,a.v_unit_price,a.e_product_name,
                       b.e_product_motifname from tm_sjpb_item a, tr_product_motif b
				               where a.i_sjpb = '$isjpb' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
