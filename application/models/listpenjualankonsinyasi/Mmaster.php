<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
  {
        parent::__construct();
  }
  function bacaperiode($iperiode)
  {
    $this->db->select(" distinct on (a.i_customer, a.n_notapb_discount) a.i_customer, b.e_customer_name, sum(c.n_quantity) as jumlah, 
                        a.n_notapb_discount, sum((a.n_notapb_discount*(c.n_quantity*c.v_unit_price))/100) as diskon, 
                        sum(c.n_quantity*c.v_unit_price) as kotor, a.i_area
                        from tm_notapb a, tr_customer b, tm_notapb_item c
                        where a.i_customer=b.i_customer and a.i_area=b.i_area
                        and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                        and a.f_spb_rekap='f' and a.i_cek is not null
                        group by a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                        order by a.i_customer, a.n_notapb_discount",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacadiskon($iperiode)
  {
	  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                        where to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and f_spb_rekap='f' and i_cek is not null
                        order by n_notapb_discount",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacaperioderinci($iperiode,$cust)
  {
    $this->db->select(" a.d_notapb, a.i_customer, b.e_customer_name, sum(c.n_quantity) as jumlah, 
                        a.n_notapb_discount, sum((a.n_notapb_discount*(c.n_quantity*c.v_unit_price))/100) as diskon, 
                        sum(c.n_quantity*c.v_unit_price) as kotor, a.i_area
                        from tm_notapb a, tr_customer b, tm_notapb_item c
                        where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_customer='$cust'
                        and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                        and a.f_spb_rekap='f' and a.i_cek is not null
                        group by a.d_notapb, a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                        order by a.d_notapb, a.i_customer, a.n_notapb_discount",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacaperioderincibrg($iperiode,$cust)
  {
    $this->db->select(" a.n_notapb_discount, c.i_product, c.e_product_name, c.v_unit_price, sum(c.n_quantity) as jumlah,
                        sum(c.n_quantity*c.v_unit_price) as kotor, b.e_customer_name
                        from tm_notapb a, tm_notapb_item c, tr_customer b
                        where a.i_customer='$cust' and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area and a.f_spb_rekap='f' 
                        and a.i_cek is not null and a.i_customer=b.i_customer
                        group by c.i_product, c.e_product_name, a.n_notapb_discount, c.v_unit_price, b.e_customer_name
                        order by a.n_notapb_discount, c.e_product_name",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacaperioderincitglbrg($iperiode,$cust)
  {
    $this->db->select(" to_char(a.d_notapb,'dd-mm-yyyy') as d_notapb, c.i_product, c.e_product_name, c.v_unit_price, 
                        sum(c.n_quantity) as jumlah, sum(c.n_quantity*c.v_unit_price) as kotor, b.e_customer_name
                        from tm_notapb a, tm_notapb_item c, tr_customer b
                        where a.i_customer='$cust' and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area and a.f_spb_rekap='f' 
                        and a.i_cek is not null and a.i_customer=b.i_customer
                        group by c.i_product, c.e_product_name, a.d_notapb, c.v_unit_price, b.e_customer_name
                        order by a.d_notapb, c.e_product_name",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
}
?>
