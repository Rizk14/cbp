<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb) 
    {
//		$this->db->query('DELETE FROM tm_spb WHERE i_spb=\''.$ispb.'\'');
//		$this->db->query('DELETE FROM tm_spb_item WHERE i_spb=\''.$ispb.'\'');
		return TRUE;
    }
    function bacasemua($iarea,$dfrom,$dto,$cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							          where a.i_customer=b.i_customer and f_ttb_tolak='f' and not a.i_nota isnull
							          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							          or upper(a.i_nota) like '%$cari%') and a.f_nota_cancel='f'
                        and a.i_area='$iarea' and 
												a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_nota desc",false)->limit($num,$offset);
#
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($inota)
    {
		$this->db->select(" * from tm_nota
          inner join tm_spb on (tm_nota.i_spb=tm_spb.i_spb and tm_nota.i_area=tm_spb.i_area)
					inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
					inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
					left join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
					where tm_nota.i_nota = '$inota'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota)
    {
		$this->db->select(" * from tm_nota_item
							inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
														and tm_nota_item.i_product=tr_product_motif.i_product)
							where tm_nota_item.i_nota = '$inota' order by tm_nota_item.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_nota
					              inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
					              inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
					              left join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
					              where (upper(tm_nota.i_customer) like '%$cari%' or upper(tr_customer.e_customer_name) like '%$cari%'
          							or upper(tm_nota.i_nota) like '%$cari%') and not tm_nota.i_nota isnull 
                        order by tm_nota.i_nota",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function updatenota($inota)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dprint	= $row->c;
		  $this->db->query("update tm_nota set d_nota_print='$dprint', n_print=n_print+1 where i_nota='$inota'");
    }
}
?>
