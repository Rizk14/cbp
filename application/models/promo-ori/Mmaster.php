<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
/*
{>select(" * from tr_promo_type", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
*/
    function baca($ipromo)
    {
			$this->db->select(" i_promo, i_promo_type, e_promo_name, d_promo, d_promo_start, d_promo_finish, n_promo_discount1, n_promo_discount2, f_all_product, 
											f_all_customer, f_customer_group, f_all_baby, f_all_reguler, f_all_area, i_price_group from tm_promo where i_promo ='$ipromo'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->row();
			}
    }
    function bacadetailp($ipromo)
    {
			$this->db->select(" a.i_promo, a.i_promo_type, a.i_product, a.i_product_grade, a.i_product_motif, a.e_product_name, a.v_unit_price, a.n_quantity_min, 
						 b.e_product_motifname from tm_promo_item a, tr_product_motif b
						 where a.i_promo = '$ipromo' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.i_product", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0)
			{
				return $query->result();
			}
    }
    function bacadetailc($ipromo)
    {
			$this->db->select(" i_promo, i_promo_type, i_customer, e_customer_name, e_customer_address, i_area
												from tm_promo_customer where i_promo = '$ipromo' order by i_customer", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetailg($ipromo)
    {
			$this->db->select(" distinct(i_customer_group) as i_customer_group, e_customer_groupname, i_promo
								from tm_promo_customergroup where i_promo = '$ipromo' order by i_customer_group", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetaila($ipromo)
    {
			$this->db->select(" i_promo, i_promo_type, i_area, e_area_name from tm_promo_area where i_promo = '$ipromo' order by i_area", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacajenis()
    {
			$this->db->select(" * from tr_promo_type", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function insertheader($ipromo,$dpromo,$ipromotype,$dpromostart,$dpromofinish,
						  $epromoname,$fallproduct,$fallcustomer,$fcustomergroup,
						  $npromodiscount1,$npromodiscount2,$fallbaby,$fallreguler,$fallarea,$ipricegroup)
    {
    	$this->db->set(
    		array(
			'i_promo'						=> $ipromo,
			'd_promo'						=> $dpromo,
			'i_promo_type'			=> $ipromotype,
			'd_promo_start'			=> $dpromostart,
			'd_promo_finish'		=> $dpromofinish,
			'e_promo_name'			=> $epromoname,
			'f_all_product'			=> $fallproduct,
			'f_all_baby'				=> $fallbaby,
			'f_all_reguler'			=> $fallreguler,
			'f_all_customer'		=> $fallcustomer,
			'f_all_area'				=> $fallarea,
			'f_customer_group'	=> $fcustomergroup,
			'n_promo_discount1'	=> $npromodiscount1,
			'n_promo_discount2'	=> $npromodiscount2,
      'i_price_group'      => $ipricegroup
    		)
    	);
    	$this->db->insert('tm_promo');
    }
    function insertdetailp($ipromo,$ipromotype,$iproduct,$iproductgrade,$eproductname,$nquantitymin,$vunitprice,$iproductmotif)
    {
		//$vunitprice2	= $vunitprice==''?'0':$vunitprice;
    	$this->db->set(
    		array(
					'i_promo'			=> $ipromo,
					'i_promo_type'		=> $ipromotype,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_quantity_min'	=> $nquantitymin,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname
    		)
    	);
    	
    	$this->db->insert('tm_promo_item');
    }
	function insertdetailc($ipromo,$ipromotype,$icustomer,$ecustomername,$ecustomeraddress,$iarea)
    {
    	$this->db->set(
    		array(
					'i_promo'			=> $ipromo,
					'i_promo_type'		=> $ipromotype,
					'i_customer'		=> $icustomer,
					'e_customer_name' 	=> $ecustomername,
  					'e_customer_address'=> $ecustomeraddress,
					'i_area'			=> $iarea
    		)
    	);
    	
    	$this->db->insert('tm_promo_customer');
    }
	function insertdetailg($ipromo,$ipromotype,$icustomergroup,$ecustomergroupname,$iarea)
    {
    	$this->db->set(
    		array(
					'i_promo'				=> $ipromo,
					'i_promo_type'			=> $ipromotype,
					'i_customer_group'		=> $icustomergroup,
					'e_customer_groupname' 	=> $ecustomergroupname,
					'i_area' 				=> $iarea
    		)
    	);
    	
    	$this->db->insert('tm_promo_customergroup');
    }
	function insertdetaila($ipromo,$ipromotype,$iarea,$eareaname)
    {
    	$this->db->set(
    		array(
					'i_promo'			=> $ipromo,
					'i_promo_type'=> $ipromotype,
					'i_area'			=> $iarea,
					'e_area_name'	=> $eareaname,
    		)
    	);
    	
    	$this->db->insert('tm_promo_area');
	}
	function updateheader($ipromo,$dpromo,$ipromotype,$dpromostart,$dpromofinish,
						  $epromoname,$fallproduct,$fallcustomer,$fcustomergroup,
						  $npromodiscount1,$npromodiscount2,$fallbaby,$fallreguler,$fallarea,$ipricegroup)
    {
    	$this->db->set(
    		array(
			'd_promo'						=> $dpromo,
			'i_promo_type'			=> $ipromotype,
			'd_promo_start'			=> $dpromostart,
			'd_promo_finish'		=> $dpromofinish,
			'e_promo_name'			=> $epromoname,
			'f_all_product'			=> $fallproduct,
			'f_all_baby'				=> $fallbaby,
			'f_all_reguler'			=> $fallreguler,
			'f_all_customer'		=> $fallcustomer,
			'f_all_area'				=> $fallarea,
			'f_customer_group'	=> $fcustomergroup,
			'n_promo_discount1'	=> $npromodiscount1,
			'n_promo_discount2'	=> $npromodiscount2,
      'i_price_group'     => $ipricegroup
    		)
    	);
    	$this->db->where('i_promo',$ipromo);  	
    	$this->db->update('tm_promo');
    }

    public function deletedetailp($ipromo,$iproduct,$iproductgrade,$iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_promo_item WHERE i_promo='$ipromo' 
										and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										and i_product_motif='$iproductmotif'");
    }
	public function deletedetailc($ipromo,$icustomer)
    {
		$this->db->query("DELETE FROM tm_promo_customer WHERE i_promo='$ipromo' and i_customer='$icustomer'");
    }
	public function deletedetailg($ipromo,$icustomergroup) 
    {
		$this->db->query("DELETE FROM tm_promo_customergroup WHERE i_promo='$ipromo' 
										and i_customer_group='$icustomergroup'");
    }	
	public function deletedetaila($ipromo,$iarea)
    {
		$this->db->query("DELETE FROM tm_promo_area WHERE i_promo='$ipromo' and i_area='$iarea'");
    }
    public function delete($ipromo) 
    {
		$this->db->query('DELETE FROM tm_promo WHERE i_promo=\''.$ipromo.'\'');
		$this->db->query('DELETE FROM tm_promo_item WHERE i_promo=\''.$ipromo.'\'');
		$this->db->query('DELETE FROM tm_promo_customer WHERE i_promo=\''.$ipromo.'\'');
		$this->db->query('DELETE FROM tm_promo_customergroup WHERE i_promo=\''.$ipromo.'\'');
    }
    function bacasemua()
    {
		$this->db->select(" i_promo, i_promo_type, e_promo_name, d_promo, d_promo_start, d_promo_finish, n_promo_discount1, n_promo_discount2, f_all_product, 
											f_all_customer, f_customer_group, f_all_baby, f_all_reguler, f_all_area, i_price_group from tm_promo order by i_promo desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_promo,9,5)) as max from tm_promo
				  			where substr(i_promo,4,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopromo  =$terakhir+1;
			settype($nopromo,"string");
			$a=strlen($nopromo);
			while($a<5){
			  $nopromo="0".$nopromo;
			  $a=strlen($nopromo);
			}
			$nopromo  ="PR-".$thbl."-".$nopromo;
			return $nopromo;
		}else{
			$nopromo  ="00001";
			$nopromo  ="PR-".$thbl."-".$nopromo;
			return $nopromo;
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_promo where upper(i_promo) like '%$cari%' 
					order by i_promo",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomer($num,$offset)
    {
		$this->db->select("* from tr_customer order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricustomer($cari,$num,$offset)
    {
		$this->db->select("* from tr_customer where upper(e_customer_name) like '%$cari%' or upper(i_customer) like '%$cari%' order by i_customer ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomergroup($num,$offset)
    {
		$this->db->select("* from tr_customer_group order by i_customer_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricustomergroup($cari,$num,$offset)
    {
		$this->db->select("* from tr_customer_group where upper(e_customer_groupname) like '%$cari%' or upper(i_customer_group) like '%$cari%' order by i_customer_group ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select("* from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacapricegroup($cari,$num,$offset)
    {
		$this->db->select("distinct(i_price_group) as i_price_group from tr_product_price 
                       where i_price_group like '%$cari%'
                       order by i_price_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
