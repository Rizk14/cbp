<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function bacasemua($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select(" a.*, b.e_area_name
							from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
							and substr(a.i_dkb,11,2)='$iarea' and 
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cari($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacaperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select("	a.*, b.e_area_name, b.e_area_address from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
							and substr(a.i_dkb,11,2)='$iarea' and 
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select("	a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function baca($idkb, $iarea)
	{
		$this->db->select(" a.i_dkb, a.i_area, b.e_area_name, c.e_dkb_kirim, a.i_dkb_via, d.e_dkb_via,
							a.e_sopir_name, a.v_dkb, a.d_dkb,  f.i_ekspedisi, f.e_ekspedisi, a.i_kendaraan, b.e_area_address
							from tm_dkb_sjp a
							inner join tr_area b on (a.i_area=b.i_area)
							inner join tr_dkb_kirim c on (a.i_dkb_kirim=c.i_dkb_kirim)
							inner join tr_dkb_via d on (a.i_dkb_via=d.i_dkb_via)
							left join tm_dkb_ekspedisi e on (a.i_dkb=e.i_dkb and a.i_area=e.i_area)
							left join tr_ekspedisi f on (e.i_ekspedisi=f.i_ekspedisi)
							where a.i_dkb = '$idkb' and a.i_area='$iarea'
							order by a.i_dkb desc", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetail($idkb, $iarea)
	{
		$this->db->select("  a.*, b.i_area, c.e_area_name, c.e_area_address, b.i_spmb, b.d_spmb
							from tm_dkb_sjp_item a, tm_sjp b, tr_area c
							where a.i_area=b.i_area
							AND a.i_area = c.i_area
							and a.i_sjp=b.i_sjp
							and a.i_dkb='$idkb'
							and a.i_area='$iarea'
							order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function updatedkb($idkb)
	{
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dprint	= $row->c;

		$this->db->query("update tm_dkb_sjp set n_print=n_print+1 where i_dkb='$idkb' ");
	}

	public function reprint($idkb, $iarea, $dfrom, $dto, $iareax, $folder)
	{
		$data = array(
			'n_print' => 0,
		);
		$this->db->where('i_dkb', $idkb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_dkb_sjp', $data);

		print "<script>alert(\"Nomor DKB : $idkb Bisa dicetak ulang, terimakasih.\");show(\"$folder/cform/view/" . $dfrom . "/" . $dto . "/" . $iareax . "\",\"#main\");</script>";
	}
}
