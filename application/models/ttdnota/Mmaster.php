<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$iarea,$num,$offset)
    {
		  $this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
						              where to_char(d_spb,'yyyymm')='$iperiode' and a.i_customer=b.i_customer
						              and i_area='$iarea' order by a.i_spb ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$cari)
    {
		  if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
			  $this->db->select("* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false)->limit($num,$offset);
		  }				
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
#		  if ($query->num_rows() > 0){
#			  return $query->result();
#		  }
    }
    function detail($iperiode,$iarea,$iproduct)
    {
      if($iarea=='00')
      {
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and daerah='f' and product='$iproduct' and a.product=b.i_product",false);
      }else{
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and area='$iarea' and product='$iproduct'  and a.product=b.i_product",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
