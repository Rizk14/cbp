<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dtap a, tr_area b
							where a.i_area=b.i_area and a.v_sisa>0 
							and (upper(a.i_dtap) like '%%' or upper(b.e_area_name) like '%%' or upper(a.i_area) like '%%') 
							order by a.i_dtap desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($isupplier,$dfrom,$dto, $cari, $num,$offset)
    {
		$this->db->select(" * from tm_dtap
                        where d_dtap >= to_date('$dfrom','dd-mm-yyyy') 
                        and d_dtap <= to_date('$dto','dd-mm-yyyy')
                        and v_sisa>0 and f_dtap_cancel='f'
                        and i_supplier='$isupplier'
                        order by i_dtap",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer
							and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%' or upper(a.i_nota) like '%$cari%')
							order by a.i_nota desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function insertheader(	$ipl,$iarea,$ijenisbayar,$igiro,$isupplier,$dgiro,$dbukti,$dcair,$ebankname,
							$vjumlah,$vlebih	)
    {
		if($dcair!='' && $dgiro!=''){
			$this->db->query("insert into tm_pelunasanap (i_pelunasanap,i_area,i_jenis_bayar,i_giro,i_supplier,
							  d_giro,d_bukti,d_cair,e_bank_name,v_jumlah,v_lebih)
						  	  values
						  	('$ipl','$iarea','$ijenisbayar','$igiro','$isupplier','$dgiro','$dbukti','$dcair','$ebankname',$vjumlah,$vlebih)");
		}elseif($dcair=='' && $dgiro!=''){
			$this->db->query("insert into tm_pelunasanap (i_pelunasanap,i_area,i_jenis_bayar,i_giro,i_supplier,d_giro,d_bukti,
							  e_bank_name,v_jumlah,v_lebih)
							  values
							('$ipl','$iarea','$ijenisbayar','$igiro','$isupplier','$dgiro','$dbukti','$ebankname',$vjumlah,$vlebih)");
		}elseif($dcair!='' && $dgiro==''){
			$this->db->query("insert into tm_pelunasanap (i_pelunasanap,i_area,i_jenis_bayar,i_giro,i_supplier,
							  d_bukti,d_cair,e_bank_name,v_jumlah,v_lebih)
						  	  values
						  	('$ipl','$iarea','$ijenisbayar','$igiro','$isupplier','$dbukti','$dcair','$ebankname',$vjumlah,$vlebih)");
		}else{
			$this->db->query("insert into tm_pelunasanap (i_pelunasanap,i_area,i_jenis_bayar,i_giro,i_supplier,
							  d_bukti,e_bank_name,v_jumlah,v_lebih)
						  	  values
						  	('$ipl','$iarea','$ijenisbayar','$igiro','$isupplier','$dbukti','$ebankname',$vjumlah,$vlebih)");
		}
    }
	function jmlasalkn(	$ipl,$dbukti,$iarea)
    {
		$this->db->select("* from tm_pelunasanap where i_pelunasanap='$ipl' and d_bukti='$dbukti' and i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function deleteheader(	$ipl,$dbukti,$iarea)
    {
		$this->db->query("delete from tm_pelunasanap where i_pelunasanap='$ipl' and d_bukti='$dbukti' and i_area='$iarea'",false);
	}
	function updategiro($igiro,$ipv,$pengurang,$asal)
    {
		$this->db->query("update tm_giro_dgu set v_sisa=v_sisa-$pengurang+$asal
						  where i_giro='$igiro' and i_pv='$ipv'");
    }
	function updatedn($iarea,$igiro,$pengurang,$asal)
    {
		$this->db->query("update tm_dn set v_sisa=v_sisa-$pengurang+$asal
						  where i_dn='$igiro' and i_area='$iarea'");
    }
	function updateku($iarea,$iku,$pengurang,$asal,$th)
    {
		$this->db->query("update tm_kuk set v_sisa=v_sisa-$pengurang+$asal
						  where i_kuk='$iku' and n_kuk_year='$th'");
/*
		$this->db->query("update tm_kuk set v_sisa=v_sisa-$pengurang+$asal
						  where i_kuk='$iku' and i_area='$iarea' and n_kuk_year='$th'");
*/
    }
	function insertdetail($ipl,$iarea,$inota,$dnota,$vjumlah,$vsisa,$dbukti)
    {
		$this->db->query("insert into tm_pelunasanap_item 
						  (i_pelunasanap,i_area,i_dtap,d_dtap,v_jumlah,v_sisa,d_bukti)
						  values
						  ('$ipl','$iarea','$inota','$dnota',$vjumlah,$vsisa,'$dbukti')");
    }
	function updatedtap($idtap,$iarea,$isupplier,$vsisa)
    {
		$this->db->query("update tm_dtap set v_sisa=$vsisa where i_dtap='$idtap' and i_supplier='$isupplier'");# and i_area='$iarea'");
    }
    function hitungsisapelunasan($ipl,$iarea,$dbukti)
    {
		$this->db->select(" sum(v_sisa) as v_sisa from tm_pelunasanap_item
							where i_area='$iarea' and i_pelunasanap='$ipl' and d_bukti='$dbukti'
							group by i_pelunasanap,i_area ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$jml=$row->v_sisa;
			}
			return $jml;
		}
    }
	function updatestatuspelunasan($ipl,$iarea,$dbukti)
    {
		$this->db->query("update tm_pelunasanap set f_close='t' where i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
    }
	public function deletedetail($ipl,$iarea,$inota,$dbukti) 
    {
		$this->db->query("DELETE FROM tm_pelunasanap_item WHERE i_pelunasanap='$ipl' and i_area='$iarea' and i_dtap='$inota' and d_bukti='$dbukti'");
    }
	function bacasupplier($cari,$num,$offset){
		$this->db->select(" i_supplier, e_supplier_name, e_supplier_address, e_supplier_city 
                        from tr_supplier
          							where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')
                        order by i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacagiro($isupplier,$num,$offset){
		$this->db->select(" * from tm_giro_dgu 
							where i_supplier = '$isupplier' 
							and (f_giro_tolak='f' or f_giro_batal='f')
							order by i_giro,i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadn($isupplier,$iarea,$num,$offset){
		$this->db->select(" * from tm_dn
							where i_supplier = '$isupplier' 
							and i_area='$iarea'
							and v_sisa>0
							order by i_dn,i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacaku($isupplier,$th,$num,$offset){
		$this->db->select(" * from tm_kuk 
							where i_supplier = '$isupplier' 
							and v_sisa=v_jumlah
							and f_close='f' and f_kuk_cancel='f'
							order by i_kuk,i_supplier ",FALSE)->limit($num,$offset);
#							and n_kuk_year=$th
/*
		$this->db->select(" * from tm_kuk 
							where i_supplier = '$isupplier' 
							and i_area='$iarea'
							and v_sisa>0
							and f_close='f'
							order by i_kuk,i_supplier ",FALSE)->limit($num,$offset);
*/		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
  }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function bacagirocek($num,$offset){
		$this->db->select(" * from tr_jenis_bayar where i_jenis_bayar <> '04' order by i_jenis_bayar ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadtap($cari,$iarea,$isupplier,$num,$offset){
		$this->db->select(" * from tm_dtap
							where i_supplier='$isupplier' and v_sisa>0 and (upper(i_dtap) like '%$cari%')
							order by i_dtap ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function runningnumberpl($thh,$iarea){
		$this->db->select(" max(substr(i_pelunasanap,1,10)) as max from tm_pelunasanap 
							where to_char(d_bukti,'yyyy')='$thh' and i_area='$iarea' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nopl  =$terakhir+1;
			settype($nopl,"string");
			$a=strlen($nopl);
			while($a<10){
			  $nopl="0".$nopl;
			  $a=strlen($nopl);
			}
			$nopl  = $nopl;
			return $nopl;
		}else{
			$nopl  ="0000000001";
			$nopl  =$nopl;
			return $nopl;
		}
    }
	function bacapelunasan($isupplier,$num,$offset,$cari){
		$this->db->select(" min(v_jumlah) as v_jumlah, min(v_lebih) as v_lebih,
								d_bukti,i_pelunasanap, i_supplier, i_area
							from tm_pelunasanap_lebih
							where i_supplier = '$isupplier'
							and v_lebih>0 and (upper(i_pelunasanap) like '%$cari%')
							group by i_pelunasanap, d_bukti, i_supplier, i_area",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacapl($iarea,$ipl,$dbukti){
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name, d.e_jenis_bayarname, c.e_supplier_address, c.e_supplier_city
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier 
							and a.i_jenis_bayar=d.i_jenis_bayar and a.d_bukti='$dbukti'
							and upper(a.i_pelunasanap)='$ipl' and upper(a.i_area)='$iarea'",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function sisa($iarea,$ipl,$dbukti){
		$this->db->select(" sum(v_sisa+v_jumlah)as sisa from tm_pelunasanap_item
							where i_pelunasanap='$ipl' and d_bukti='$dbukti' and i_area='$iarea' ",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->sisa;
		}	
	}
	function bacadetailpl($iarea,$ipl,$dbukti){
		$this->db->select(" * from tm_pelunasanap_item
							where i_pelunasanap = '$ipl' 
							and i_area='$iarea' and d_bukti='$dbukti'
							order by i_pelunasanap,i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
}
?>
