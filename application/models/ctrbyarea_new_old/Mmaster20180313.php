<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    {
      $pecah1       = explode('-', $dfrom);
        $tgl1       = $pecah1[0];
        $bln1       = $pecah1[1];
        $tahun1     = $pecah1[2];
        $tahunprev1 = intval($tahun1) - 1;

      $pecah2       = explode('-', $dto);
        $tgl2       = $pecah2[0];
        $bln2       = $pecah2[1];
        $tahun2     = $pecah2[2];
        $tahunprev2 = intval($tahun2) - 1;

      $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
      $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;

       /* $this->db->select(" sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island, c.e_product_groupname 
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char(a.d_nota,'yyyymm')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                                union all
                                
                                select 0 as oa, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where to_char(a.d_nota,'yyyy') = '$tahun1' and a.d_nota <= to_date('dfrom','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname

                                union all

                                select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                      select distinct(a.i_customer) as ob,  c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, to_char (a.d_nota,'yyyy') as i_periode 
                                      from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                      where (to_char(a.d_nota,'yyyy') = '$tahun1' and a.d_nota <= to_date('$dto','dd-mm-yyyy')) 
                                        and a.f_nota_cancel='false'
                                        and a.i_customer = c.i_customer
                                        and c.i_area = d.i_area
                                        and a.i_spb = e.i_spb
                                        and e.i_product_group = f.i_product_group
                                        and c.f_customer_aktif = 'true'
                                      group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyymm')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                                union all
                                
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname
                          
                          ) as c
                          group by c.i_area,c.e_area_name,c.e_area_island, c.e_product_groupname
                          order by c.e_area_island, c.i_area, c.e_area_name",false);*/

      $this->db->select("sum(c.oa) as oa,sum(c.oaprev) as oaprev, sum(c.ob) as ob, sum(c.vnota) as vnota, 
                            sum(c.vnotaprev) as vnotaprev, sum(c.qty) as qty, sum(c.qtyprev) as qtyprev, c.i_area, c.e_area_name, 
                            c.e_area_island, c.e_product_groupname 
                            from(
                              select sum(b.oa) as oa, 0 as oaprev,sum(b.ob) as ob, sum(b.vnota) as vnota, 0 as vnotaprev, sum(b.qty) as qty, 0 as qtyprev, b.i_area,b.e_area_name, b.e_area_island, b.e_product_groupname 
                              from (
                                select count(a.oa) as oa, 0 as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char(a.d_nota,'yyyymm')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                                union all
                                
                                select 0 as oa, 0 as ob, sum(a.vnota) as vnota, sum(a.qty) as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy')

                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                union all

                                select 0 as oa, count(a.ob) as ob, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                      select distinct(a.i_customer) as ob,  c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname, to_char (a.d_nota,'yyyy') as i_periode 
                                      from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                      where (to_char(a.d_nota,'yyyy') = '$tahun1' and a.d_nota <= to_date('$dto','dd-mm-yyyy')) 
                                        and a.f_nota_cancel='false'
                                        and a.i_customer = c.i_customer
                                        and c.i_area = d.i_area
                                        and a.i_spb = e.i_spb
                                        and e.i_product_group = f.i_product_group
                                        and c.f_customer_aktif = 'true'
                                        and not a.i_nota isnull
                                        and a.i_area = e.i_area
                                      group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyy')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              union all 
                              ------------------------------------------- tahun lalu -----------------------------------------------
                              select 0 as oa, sum(b.oa) as oaprev, 0 as ob, 0 as vnota, sum(b.vnota) as vnotaprev , 0 as qty, sum(b.qty) as qtyprev,  b.i_area , b.e_area_name , b.e_area_island,b.e_product_groupname from (
                                select count(a.oa) as oa, 0 as vnota, 0 as qty, a.i_area,a.e_area_name, a.e_area_island, a.e_product_groupname from (
                                  select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    
                                    and a.f_nota_cancel='false'
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname,to_char (a.d_nota,'yyyymm')
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                                union all
                                
                                select 0 as oaprev, sum(a.vnota) as vnotaprev, sum(a.qty) as qtyprev, a.i_area ,a.e_area_name , a.e_area_island, a.e_product_groupname from (
                                  select sum(b.n_deliver*b.v_unit_price)-(a.v_nota_discount*(sum(b.n_deliver*b.v_unit_price)/ a.v_nota_gross)) as vnota, sum(b.n_deliver) as qty,
                                    c.i_area , d.e_area_name, d.e_area_island, f.e_product_groupname
                                  from tm_nota a, tm_nota_item b, tr_customer c, tr_area d, tm_spb e, tr_product_group f
                                  where (a.d_nota >= to_date('$gabung1','dd-mm-yyyy') and a.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                                    and a.f_nota_cancel='false'
                                    and a.i_nota=b.i_nota and a.i_area=b.i_area
                                    and a.i_customer = c.i_customer
                                    and c.i_area = d.i_area
                                    and a.i_spb = e.i_spb
                                    and e.i_product_group = f.i_product_group
                                    and not a.i_nota isnull
                                    and a.i_area = e.i_area
                                  group by a.i_area, a.v_nota_discount,a.v_nota_gross,c.i_area, a.i_customer,d.e_area_name,d.e_area_island, f.e_product_groupname
                                ) as a
                                group by a.i_area,a.e_area_name,a.e_area_island, a.e_product_groupname
                                
                              ) as b
                              group by b.i_area,b.e_area_name,b.e_area_island, b.e_product_groupname
                          
                          ) as c
                          group by c.i_area,c.e_area_name,c.e_area_island, c.e_product_groupname
                          order by c.e_area_island, c.i_area, c.e_area_name",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
