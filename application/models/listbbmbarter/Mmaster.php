<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibbm,$iarea) 
    {
     /* $ittb=str_replace('%20','',$ittb);
    	$this->db->set(
    		array(
					'i_bbm'			=> null,
					'd_bbm'			=> null,
					'd_receive2'=> null
    		)
    	);
			$this->db->where('i_area',$iarea);
			$this->db->where('i_ttb',$ittb);
			$this->db->where('n_ttb_year',$tahun);
			$this->db->update('tm_ttbretur');

    	$this->db->set(
    		array(
					'i_product2'					=> null,
					'i_product2_grade'		=> null,
					'i_product2_motif'		=> null,
					'n_quantity_receive'	=> null
    		)
    	);
    	$this->db->where('i_ttb',$ittb);
    	$this->db->where('n_ttb_year',$tahun);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_ttbretur_item');*/

		$this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_bbm='$ibbm' and i_bbm_type='02'");
######
		  $ibbm=trim($ibbm);
		  $this->db->select(" b.e_product_name, a.d_bbm, b.n_quantity, b.i_product, b.i_product_grade, b.i_product_motif 
                          	from tm_bbm a, tm_bbm_item b WHERE a.i_bbm=b.i_bbm and a.i_bbm='$ibbm' and a.i_bbm_type='02'");
		  
		  $query = $this->db->get();
		  foreach($query->result() as $row){
			$jml    			= $row->n_quantity;
			$product 			= $row->i_product;
			$grade  			= $row->i_product_grade;
			$motif  			= $row->i_product_motif;
			$eproductname 		= $row->e_product_name;
        	$dbbm    			= $row->d_bbm;
        	$istore				= 'AA';
			$istorelocation		= '01';
			$istorelocationbin 	= '00';
        	$th 				= substr($dbbm,0,4);
			$bl 				= substr($dbbm,5,2);
			$emutasiperiode 	= $th.$bl;
			  
			$queri 	= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
										where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
										and i_store='$istore' and i_store_location='$istorelocation'
										and i_store_locationbin='$istorelocationbin' and i_refference_document='$ibbm'
										order by d_transaction desc, i_trans desc",false);
        
        if ($queri->num_rows() > 0){
			$row   		= $queri->row();
			$que 	= $this->db->query("SELECT current_timestamp as c");
			$ro 	= $que->row();
			$now	 = $ro->c;
          	$this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$product','$grade','$motif','$istore','$istorelocation','$istorelocationbin', 
                                '$eproductname', '$ibbm', '$now', 0, $jml, $row->n_quantity_akhir-$jml, $row->n_quantity_akhir
                              )
                           ",false);
        }
        if( ($jml!='') && ($jml!=0) ){
          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_returoutlet=n_mutasi_returoutlet-$jml, n_saldo_akhir=n_saldo_akhir-$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);
          $this->db->query(" 
                            UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                           ",false);
        }
		  }
######
#		$this->db->query("DELETE FROM tm_bbm_item WHERE i_bbm='$ibbm'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
					                from tr_customer b, tm_ttbretur c, tm_bbm a
					                left join tm_kn d on (a.i_bbm=d.i_refference) 
					                where 
					                c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
					                a,i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
					                or upper(a.i_bbm) like '%$cari%') 
					                order by a.i_bbm desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
							            from tm_bbm a, tr_customer b, tm_ttbretur c
							            where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
							            and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							            order by a.i_ttb desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($cari,$num,$offset,$iuser)
    {
        $sql = "* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')
        		and (i_area like '%$cari%' or e_area_name like '%$cari%') 
        		order by i_area";
      $this->db->select($sql, false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      	$thn = substr($dfrom,6,4);
      	if($iarea=='NA'){
      		$this->db->select(" a.*, b.e_customer_name , c.i_bbk , c.d_bbk
							from tr_customer b ,tm_bbm a
							left join tm_bbk c on (a.i_bbm=c.i_refference_document and a.i_area=c.i_area and a.i_salesman=c.i_salesman )
							where 
							a.i_area=b.i_area
							and a.i_supplier=b.i_customer
							--and a.f_bbm_cancel='f'
							and a.i_bbm_type='02'
							and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
							and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							order by i_bbm desc",false)->limit($num,$offset);
		    /*$this->db->select("	a.*, b.e_customer_name
								from tm_bbm a, tr_customer b
								where 
								a.i_area=b.i_area
								and a.i_supplier=b.i_customer
								--and a.f_bbm_cancel='f'
								and a.i_bbm_type='02'
								and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
								and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
								and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
								order by i_bbm desc",false)->limit($num,$offset);*/
		}else{
			  $this->db->select(" a.*, b.e_customer_name , c.i_bbk , c.d_bbk
							from tr_customer b ,tm_bbm a
							left join tm_bbk c on (a.i_bbm=c.i_refference_document and a.i_area=c.i_area and a.i_salesman=c.i_salesman )
							where 
							a.i_area=b.i_area
							and a.i_supplier=b.i_customer
							--and a.f_bbm_cancel='f'
							and a.i_bbm_type='02'
							and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							and a.i_area='$iarea'
							and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
							and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							order by i_bbm desc",false)->limit($num,$offset);
		    /*$this->db->select("	a.*, b.e_customer_name
								from tm_bbm a, tr_customer b
								where 
								a.i_area=b.i_area
								and a.i_supplier=b.i_customer
								--and a.f_bbm_cancel='f'
								and a.i_bbm_type='02'
								and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
								and a.i_area='$iarea'
								and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
								and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
								order by i_bbm desc",false)->limit($num,$offset);*/
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    	if($iarea=='NA'){
		    $this->db->select("	a.*, b.e_customer_name
								from tm_bbm a, tr_customer b
								where 
								a.i_area=b.i_area
								and a.i_supplier=b.i_customer
								--and a.f_bbm_cancel='f'
								and a.i_bbm_type='02'
								and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
								and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
								and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
								order by i_bbm desc",false)->limit($num,$offset);
		}else{
		    $this->db->select("	a.*, b.e_customer_name
								from tm_bbm a, tr_customer b
								where 
								a.i_area=b.i_area
								and a.i_supplier=b.i_customer
								--and a.f_bbm_cancel='f'
								and a.i_bbm_type='02'
								and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
								and a.i_area='$iarea'
								and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
								and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
								order by i_bbm desc",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ibbm)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
							from tr_customer b, tr_area c, tr_salesman d, tm_bbm a
							where 
							a.i_supplier=b.i_customer 
							and a.i_bbm_type='02'
							and a.i_area=c.i_area 
							and a.i_salesman =d.i_salesman 
							and a.i_bbm='$ibbm'
							order by a.i_bbm desc", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ibbm)
    {
		$this->db->select(" a.i_bbm, a.i_product as i_product2, a.i_product_motif as i_product2_motif, a.i_product_grade as i_product2_grade, 
							a.e_product_name as e_product2_name, a.n_quantity as n_quantity_receive, a.v_unit_price as v_unit_price, 
							b.e_product_motifname as e_product2_motifname
							from tm_bbm_item a, tr_product_motif b
							where a.i_bbm = '$ibbm' 
							and a.i_product=b.i_product 
							and a.i_product_motif=b.i_product_motif 
							order by a.i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
