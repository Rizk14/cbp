<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($num,$offset,$cari)
    {
	    $this->db->select("	a.*, c.e_customer_name 
	                        from tr_customer c, tm_adjmo a 
	                        where a.i_customer=c.i_customer and (upper(a.i_adj) like '%$cari%')
	                        and a.i_approve isnull and a.f_adj_cancel='f'
          							  order by a.d_adj, a.i_customer, a.i_adj desc ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($num,$offset,$cari)
    {
	    $this->db->select("	a.*, c.e_customer_name
	                        from tm_adjmo a, tr_customer c 
	                        where a.i_customer=c.i_customer and (upper(a.i_adj) like '%$cari%')
	                        and a.i_approve isnull and a.f_adj_cancel='f'	    
         							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($iadj,$icustomer)
    {
		$this->db->select("a.*, b.e_customer_name from tm_adjmo a, tr_customer b 
		                   where a.i_customer=b.i_customer
		                   and i_adj ='$iadj' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($iadj,$iarea)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_adjmmo_item a, tr_product_motif b
						 where a.i_adj = '$iadj' and i_customer='$icustomer' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function approve($iadj, $iarea, $user)
    {
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'YYYY-MM-DD') as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
          'i_approve'              => $user,
          'd_approve'              => $now
    		)
    	);
    	$this->db->where('i_adj',$iadj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_adjmo');
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttransbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbm,$q_in,$q_out,$qbm,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbm', '$now', $qbm, 0, $q_ak+$qbm, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qbm, n_saldo_akhir=n_saldo_akhir+$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,$qbm,0,0,0,$qbm,0,'f')
                              ",false);
    }

    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateicbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbm)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qbm, 't'
                                )
                              ",false);
    }
/*
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibm,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$ibm'
                                    order by d_transaction desc, i_trans desc",false);
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ibbk', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qbm, n_saldo_akhir=n_saldo_akhir-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
*/
	function runningnumberbbm($thbl,$iarea){
		$th	  = substr($thbl,2,2);
		$thbl = substr($thbl,2,4);
		$this->db->select(" max(substr(i_bbmadj,10,6)) as max from tm_bbmadj where substr(i_bbmadj,5,2)='$th' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nobbm  =$terakhir+1;
			settype($nobbm,"string");
			$a=strlen($nobbm);
			while($a<6){
			  $nobbm="0".$nobbm;
			  $a=strlen($nobbm);
			}
			$nobbm  ="BBM-".$thbl."-".$nobbm;
			return $nobbm;
		}else{
			$nobbm  ="000001";
			$nobbm  ="BBM-".$thbl."-".$nobbm;
			return $nobbm;
		}
  }
  function insertheaderbbm($ibbm,$iadj,$dadj,$iarea,$eremark)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c, to_char(current_timestamp,'yyyy-mm-dd') as d");
	    $row   	= $query->row();
	    $now	  = $row->c;
	    $tgl    = $row->d;
      $query=$this->db->query(" 
                                INSERT INTO tm_bbmadj (i_adj, d_adj, i_bbmadj, d_bbmadj, i_area, e_remark, d_entry)
                                VALUES 
                                ('$iadj','$dadj','$ibbm','$row->d','$iarea','$eremark', '$row->c')
                              ",false);
    }
    
  function insertdetailbbm($ibbm,$iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$nreceive,$vproductmill,$eremark,$eproductname,$dadj,$i)
  {
    $query 	= $this->db->query("SELECT current_timestamp as c, to_char(current_timestamp,'yyyymm') as d");
    $row   	= $query->row();
    $now	  = $row->d;
    $query=$this->db->query(" 
                              INSERT INTO tm_bbmadj_item (i_bbmadj, i_adj, i_area, i_product, i_product_motif, i_product_grade,
                              n_quantity, v_unit_price, e_remark, e_product_name, d_adj, e_mutasi_periode, n_item_no)
                              VALUES
                              ('$ibbm','$iadj','$iarea','$iproduct','$iproductmotif','$iproductgrade','$nreceive','$vproductmill','$eremark','$eproductname','$dadj','$now',$i)
                            ",false);
  }
	function runningnumberbbk($thbl,$iarea){
		$th	  = substr($thbl,2,2);
		$thbl = substr($thbl,2,4);
		$this->db->select(" max(substr(i_bbkadj,10,6)) as max from tm_bbkadj where substr(i_bbkadj,5,2)='$th' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nobbk  =$terakhir+1;
			settype($nobbk,"string");
			$a=strlen($nobbk);
			while($a<6){
			  $nobbk="0".$nobbk;
			  $a=strlen($nobbk);
			}
			$nobbk  ="BBK-".$thbl."-".$nobbk;
			return $nobbk;
		}else{
			$nobbk  ="000001";
			$nobbk  ="BBK-".$thbl."-".$nobbk;
			return $nobbk;
		}
  }
  function insertheaderbbk($ibbk,$iadj,$dadj,$iarea,$eremark)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c, to_char(current_timestamp,'yyyy-mm-dd') as d");
	    $row   	= $query->row();
	    $now	  = $row->c;
	    $tgl    = $row->d;
      $query=$this->db->query(" 
                                INSERT INTO tm_bbkadj (i_adj, d_adj, i_bbkadj, d_bbkadj, i_area, e_remark, d_entry)
                                VALUES 
                                ('$iadj','$dadj','$ibbk','$row->d','$iarea','$eremark', '$row->c')
                              ",false);
    }
    
  function insertdetailbbk($ibbk,$iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$nreceive,$vproductmill,$eremark,$eproductname,$dadj,$i)
  {
    $query 	= $this->db->query("SELECT current_timestamp as c, to_char(current_timestamp,'yyyymm') as d");
    $row   	= $query->row();
    $now	  = $row->c;
    $peri	  = $row->d;
    $query=$this->db->query(" 
                              INSERT INTO tm_bbkadj_item (i_bbkadj, i_adj, i_area, i_product, i_product_motif, i_product_grade,
                              n_quantity, v_unit_price, e_remark, e_product_name, d_adj, e_mutasi_periode, n_item_no)
                              VALUES
                              ('$ibbk','$iadj','$iarea','$iproduct','$iproductmotif','$iproductgrade','$nreceive','$vproductmill','$eremark','$eproductname','$dadj','$peri',$i)
                            ",false);
  }
    function inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$qbk,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbk', '$now', 0, $qbk, $q_ak-$qbk, $q_ak
                                )
                              ",false);
    }
    function updatemutasibbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qbk, n_saldo_akhir=n_saldo_akhir-$qbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,0,0,0,$qbk,0,0,'f')
                              ",false);
    }
    function updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbk,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbm)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', 0, 't'
                                )
                              ",false);
    }
}
?>
