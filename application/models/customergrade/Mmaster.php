<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomergrade)
    {
		$this->db->select('i_customer_grade, e_customer_gradename')->from('tr_customer_grade')->where('i_customer_grade', $icustomergrade);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomergrade, $ecustomergradename)
    {
    	$this->db->set(
    		array(
    			'i_customer_grade' => $icustomergrade,
    			'e_customer_gradename' => $ecustomergradename
    		)
    	);
    	
    	$this->db->insert('tr_customer_grade');
		#redirect('customergrade/cform/');
    }
    function update($icustomergrade, $ecustomergradename)
    {
    	$data = array(
               'i_customer_grade' => $icustomergrade,
               'e_customer_gradename' => $ecustomergradename
            );
		$this->db->where('i_customer_grade', $icustomergrade);
		$this->db->update('tr_customer_grade', $data); 
		#redirect('customergrade/cform/');
    }
	
    public function delete($icustomergrade) 
    {
		$this->db->query('DELETE FROM tr_customer_grade WHERE i_customer_grade=\''.$icustomergrade.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade order by i_customer_grade", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
