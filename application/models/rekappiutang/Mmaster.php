<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
/*
    function bacaperiode($iarea,$dto,$num,$offset,$cari,$nt,$jt)
    {
      if($nt!=''){
	      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
						      where a.i_customer=b.i_customer 
						      and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
                  and not a.i_nota isnull
						      and (upper(a.i_nota) like '%$cari%' 
						        or upper(a.i_spb) like '%$cari%' 
						        or upper(a.i_customer) like '%$cari%' 
						        or upper(b.e_customer_name) like '%$cari%')
						      and a.i_area='$iarea' 
                  and a.d_nota <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 
                  and a.f_nota_cancel='f'
						      ORDER BY a.d_nota ",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
						      where a.i_customer=b.i_customer 
						      and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
                  and not a.i_nota isnull
						      and (upper(a.i_nota) like '%$cari%' 
						        or upper(a.i_spb) like '%$cari%' 
						        or upper(a.i_customer) like '%$cari%' 
						        or upper(b.e_customer_name) like '%$cari%')
						      and a.i_area='$iarea' 
                  and a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 
                  and a.f_nota_cancel='f'
						      ORDER BY a.d_jatuh_tempo",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
*/
    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode,$num,$offset,$cari)
    {
		/*$sql = " i_area, sum(saldo) as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn, 
						sum(dpp) as dpp, sum(ppn) as ppn
						from(
                          select i_area, (sum(nota)+sum(dn))-(sum(pl)+sum(kn))as saldo, 0 as nota, 0 as dn, 0 as pl, 0 as kn,
                          0 as dpp, 0 as ppn from(
                          select i_area, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                          sum(dpp) as dpp, sum(ppn) as ppn
                          from(
                          select a.i_area ,sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          0 as dpp, 0 as ppn
                          from tm_nota a, tr_customer_groupar b
                          where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')< '$iperiode'
                          group by a.i_area
                          union all
                          select a.i_area, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn,
                          0 as dpp, 0 as ppn 
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '$iperiode'
                          and a.i_customer=b.i_customer group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn,
                          0 as dpp, 0 as ppn 
                          from tm_pelunasan a, tr_customer_groupar b
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' 
                          and to_char(a.d_bukti,'yyyymm')< '$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_customer=b.i_customer
                          group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          0 as dpp, 0 as ppn 
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '$iperiode'
                          and a.i_customer=b.i_customer group by a.i_area
                          ) as a group by i_area
                          ) as b group by i_area

                          union all

                          select i_area, 0 as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                         sum(dpp) as dpp, sum(ppn) as ppn
                         from(
                          select i_area, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                          sum(dpp) as dpp, sum(ppn) as ppn
                          from(
                          select a.i_area, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          sum(a.v_nota_netto)/1.1 AS dpp, sum(a.v_nota_netto)/11 AS ppn
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer AND b.i_customer = c.i_customer
                          and to_char(a.d_nota,'yyyymm')='$iperiode' AND c.f_customer_pkp = 't'
                          group by a.i_area
                          UNION ALL
                          select a.i_area, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          sum(a.v_nota_netto) as dpp, 0 as ppn
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer AND b.i_customer = c.i_customer
                          and to_char(a.d_nota,'yyyymm')='$iperiode'
                          group by a.i_area
                          
                          union all
                          select a.i_area, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn,
                          0 as dpp, 0 as ppn 
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and a.i_customer=b.i_customer 
                          and to_char(a.d_kn,'yyyymm')='$iperiode' group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn,
                          0 as dpp, 0 as ppn  
                          from tm_pelunasan a, tr_customer_groupar b
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and a.i_customer=b.i_customer 
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          sum(v_netto)/1.1 as dpp, sum(v_netto)/11 as ppn  
                          from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer
                          and b.i_customer=c.i_customer    
                          and to_char(a.d_kn,'yyyymm')='$iperiode' AND c.f_customer_pkp = 't' group by a.i_area
                          UNION
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          sum(v_netto) as dpp, 0 as ppn  
                          from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer
                          and b.i_customer=c.i_customer    
                          and to_char(a.d_kn,'yyyymm')='$iperiode' AND c.f_customer_pkp = 'f' group by a.i_area
                          
                          ) as a group by i_area
                          ) as b group by i_area
                          ) as c
                          where saldo<>0 or nota>0 or dn>0 or pl>0 or kn>0
                          group by i_area
                          order by i_area"; echo $sql; die(); */
      $this->db->select(" i_area, sum(saldo) as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn, 
						sum(dpp1) as dpp1, sum(ppn1) as ppn1, sum(dpp2) as dpp2, sum(ppn2) as ppn2
						from(
                          select i_area, (sum(nota)+sum(dn))-(sum(pl)+sum(kn))as saldo, 0 as nota, 0 as dn, 0 as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2 from(
                          select i_area, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                          sum(dpp1) as dpp1, sum(ppn1) as ppn1, sum(dpp2) as dpp2, sum(ppn2) as ppn2
                          from(
                          select a.i_area ,sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2
                          from tm_nota a, tr_customer_groupar b
                          where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')< '$iperiode'
                          group by a.i_area
                          union all
                          select a.i_area, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '$iperiode'
                          and a.i_customer=b.i_customer group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2
                          from tm_pelunasan a, tr_customer_groupar b
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' 
                          and to_char(a.d_bukti,'yyyymm')< '$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_customer=b.i_customer
                          group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '$iperiode'
                          and a.i_customer=b.i_customer group by a.i_area
                          ) as a group by i_area
                          ) as b group by i_area

                          union all

                          select i_area, 0 as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                         sum(dpp1) as dpp1, sum(ppn1) as ppn1, sum(dpp2) as dpp2, sum(ppn2) as ppn2
                         from(
                          select i_area, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn,
                          sum(dpp1) as dpp1, sum(ppn1) as ppn1, sum(dpp2) as dpp2, sum(ppn2) as ppn2
                          from(
                          select a.i_area, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          sum(a.v_nota_netto)/1.1 AS dpp1, sum(a.v_nota_netto)/11 AS ppn1, 0 as dpp2, 0 as ppn2
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer AND b.i_customer = c.i_customer
                          and to_char(a.d_nota,'yyyymm')='$iperiode' AND c.f_customer_pkp = 't'
                          group by a.i_area
                          UNION ALL
                          select a.i_area, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn,
                          sum(a.v_nota_netto) as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer AND b.i_customer = c.i_customer
                          and to_char(a.d_nota,'yyyymm')='$iperiode' AND c.f_customer_pkp = 'f'
                          group by a.i_area
                          
                          union all
                          select a.i_area, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2 
                          from tm_kn a, tr_customer_groupar b
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and a.i_customer=b.i_customer 
                          and to_char(a.d_kn,'yyyymm')='$iperiode' group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn,
                          0 as dpp1, 0 as ppn1, 0 as dpp2, 0 as ppn2 
                          from tm_pelunasan a, tr_customer_groupar b
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and a.i_customer=b.i_customer 
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_area
                          union all
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          0 as dpp1, 0 as ppn1, sum(v_netto)/1.1 as dpp2, sum(v_netto)/11 as ppn2  
                          from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer
                          and b.i_customer=c.i_customer    
                          and to_char(a.d_kn,'yyyymm')='$iperiode' AND c.f_customer_pkp = 't' group by a.i_area
                          UNION
                          select a.i_area, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn,
                          0 as dpp1, 0 as ppn1, sum(v_netto) as dpp2, 0 as ppn2  
                          from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer
                          and b.i_customer=c.i_customer    
                          and to_char(a.d_kn,'yyyymm')='$iperiode' AND c.f_customer_pkp = 'f' group by a.i_area
                          
                          ) as a group by i_area
                          ) as b group by i_area
                          ) as c
                          where saldo<>0 or nota>0 or dn>0 or pl>0 or kn>0
                          group by i_area
                          order by i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($icustomer,$iperiode,$num,$offset)
    {
      $this->db->select(" * from(
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_nota as tglbukti, a.i_nota as bukti, '' as jenis, '1. penjualan' as keterangan, a.v_nota_netto as debet, 
                          0 as kredit, 0 as dn, 0 as kn
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='$iperiode'
                          and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'kredit nota' as keterangan, 0 as debet, 0 as kredit, 0 as dn, 
                          v_netto as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_bukti as tglbukti, a.i_pelunasan as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 
                          0 as debet, a.v_jumlah as kredit, 0 as dn, 0 as kn
                          from tm_pelunasan a, tr_customer_groupar b, tr_customer c, tr_jenis_bayar d
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and to_char(d_bukti,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar 
                          and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'debet nota' as keterangan, 0 as debet, 0 as kredit, v_netto as dn, 
                          0 as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          ) as a order by area, groupar, tglbukti, keterangan, bukti",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
