<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function bacasemua($icust,$cari,$num,$offset,$dto,$no)
    {
      $this->db->select("   a.* from tm_nota a left join tr_customer b on(a.i_customer=b.i_customer and a.i_customer=b.i_customer)
                            where 
                            a.i_customer=b.i_customer 
                            and (a.n_print=0 or a.n_print isnull)
                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                            or upper(a.i_nota) like '%$cari%')
                            and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and a.i_customer='$icust' and a.f_lunas='f' and a.v_sisa>0
                            and f_nota_cancel='f'
						                order by a.i_nota desc",false)->limit($num,$offset);
    
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function baca($icust,$no,$dto)
    {
      $this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, b.d_signin, c.*, d.*, e.*, f.*
                             from tm_nota a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e, tr_customer_groupar f
                             where (a.i_customer='$icust' or a.i_customer in(select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icust'))
                             and a.i_customer=b.i_customer and a.i_customer=f.i_customer
                             and a.i_salesman=c.i_salesman
                             and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
                             and b.i_customer_class=d.i_customer_class  and a.f_lunas='f' and a.v_sisa>0
                             and a.f_nota_cancel='f'
                             and not a.i_nota is null
                             order by a.i_nota desc",false);

      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
      function hitung($icust)
    {
      $this->db->select(" sum(v_sisa) as total from tm_nota
                            where (i_customer='$icust' or 
                            i_customer in(select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icust'))
                            and f_nota_cancel='f'
                            ",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacacustomer($num,$offset,$iuser)
    {
		  $this->db->select(" distinct(a.*) from tr_customer a, tm_nota b
                          where 
                          a.i_area in(select i_area from tm_user_area where i_user='$iuser') 
                          and a.i_area=b.i_area
                          and a.i_customer=b.i_customer
                          and b.v_sisa<>0
                          and b.f_nota_cancel='f'
                          order by a.i_customer asc", false)->limit($num,$offset);
		  
      $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function caricustomer($cari,$num,$offset,$iuser)
    {
		  $this->db->select(" * from tr_customer 
                          where 
                          (upper(e_customer_name) like '%$cari%' or upper(i_customer) like '%$cari%') and
                          i_area in(select i_area from tm_user_area where i_user='$iuser') order by i_customer asc
                        ", FALSE)->limit($num,$offset);

		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function close($icust,$inota)
    {
      $this->db->query("   update tm_nota set n_print=n_print+1
                              where i_nota= '$inota' and i_customer = '$icust' ",false);
    $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
