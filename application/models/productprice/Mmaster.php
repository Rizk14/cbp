<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproduct,$iproductgrade)
    {
		$this->db->select(" * from tr_product_price 
					              LEFT JOIN tr_product_grade 
					                 ON (tr_product_price.i_product_grade=tr_product_grade.i_product_grade)
					              where tr_product_price.i_product = '$iproduct' 
					                and tr_product_price.i_product_grade = '$iproductgrade' order by tr_product_price.i_price_group", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$query = $this->db->get('tr_price_group');
		foreach ($query->result() as $row)
		{
			$tmp = $row->i_price_group;
			switch($tmp)
			{
			case '00':
				$margin=(100-$nproductmargin)/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '01':
				$margin=(100-($nproductmargin+5+3+9))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '02':
				$margin=(100-($nproductmargin+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '03':
				$margin=(100-($nproductmargin+5+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '04':
				$margin=(100-($nproductmargin+5+3+9+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '05':
				$margin=(100-($nproductmargin+5+3+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '06':
				$margin=(100-($nproductmargin+5+3+9+5+8))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case 'G0':
				$margin=(100-$nproductmargin)/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G2':
				$margin=(100-($nproductmargin+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G3':
				$margin=(100-($nproductmargin+5+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G5':
				$margin=(100-($nproductmargin+5+3+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			}
			$this->db->query("insert into tr_product_price (i_product, e_product_name, i_product_grade,
				  			  v_product_retail, v_product_mill, d_product_priceentry, i_price_group, 
							  n_product_margin) values
				    		  ('$iproduct','$eproductname','$iproductgrade',$vproductretail,$vproductmill, '$dentry',
							   '$tmp',$nproductmargin)");
		}
		redirect('productprice/cform/');
    }

    function update($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$query = $this->db->get('tr_price_group');
		foreach ($query->result() as $row)
		{
			$tmp = $row->i_price_group;
			switch($tmp)
			{
			case '00':
				$margin=(100-$nproductmargin)/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '01':
				$margin=(100-($nproductmargin+5+3+9))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '02':
				$margin=(100-($nproductmargin+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '03':
				$margin=(100-($nproductmargin+5+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '04':
				$margin=(100-($nproductmargin+5+3+9+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '05':
				$margin=(100-($nproductmargin+5+3+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case '06':
				$margin=(100-($nproductmargin+5+3+9+5+8))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				break;
			case 'G0':
				$margin=(100-$nproductmargin)/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G2':
				$margin=(100-($nproductmargin+5))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G3':
				$margin=(100-($nproductmargin+5+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			case 'G5':
				$margin=(100-($nproductmargin+5+3+3))/100;
				$vproductretail=round((($vproductmill*1.1/$margin)),-2);
				$vproductretail=$vproductretail*0.9;
				break;
			}
			$this->db->query("update tr_product_price set 
							  e_product_name = '$eproductname', 
							  v_product_retail = $vproductretail, 
							  v_product_mill = $vproductmill, 
							  d_product_priceupdate = '$dupdate', 
							  n_product_margin = $nproductmargin
							  where i_product = '$iproduct'
							  and i_product_grade = '$iproductgrade'
							  and i_price_group = '$tmp'
							");
		}
		redirect('productprice/cform/');
    }
	
    public function delete($iproduct,$iproductgrade) 
    {
		$this->db->query('DELETE FROM tr_product_price WHERE i_product=\''.$iproduct.'\' and i_product_grade=\''.$iproductgrade.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_product_price order by i_product, i_price_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cari($cari,$num,$offset)
    {
     $this->db->select("  * from tr_product_price 
					                where (upper(tr_product_price.i_product) ilike '%$cari%' 
					                or upper(tr_product_price.e_product_name) ilike '%$cari%')
                          order by i_product, i_price_group", false)->limit($num,$offset);

/*		$this->db->select(" * from tr_product_price 
					              LEFT JOIN tr_product ON (tr_product_price.i_product=tr_product.i_product)
					              LEFT JOIN tr_product_grade 
					                 ON (tr_product_price.i_product_grade=tr_product_grade.i_product_grade)
					              where upper(tr_product_price.i_product) like '%$cari%' 
					                 or upper(tr_product_price.e_product_name) = '%$cari%'
					                 order by tr_product_price.i_product, tr_product_price.i_price_group
				                ", false)->limit($num,$offset);
*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaproductgrade($num,$offset)
    {
		$this->db->select("i_product_grade, e_product_gradename from tr_product_grade order by i_product_grade",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproductgrade($cari,$num,$offset)
    {
		$this->db->select("i_product_grade, e_product_gradename from tr_product_grade where upper(e_product_gradename) like '%$cari%' or upper(i_product_grade) like '%$cari%' order by i_product_grade", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product order by i_product",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproduct($cari,$num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product where upper(e_product_name) ilike '%$cari%' or upper(i_product) ilike '%$cari%' order by i_product", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
