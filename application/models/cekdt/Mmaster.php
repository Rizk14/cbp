<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dt) like '%$cari%')
							          order by a.i_dt desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
						            where a.i_area=b.i_area 
						            and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						            or upper(a.i_dt) like '%$cari%')
						            order by a.i_dt desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$iuser)
    {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	  function cariarea($cari,$num,$offset,$iuser)
    {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_dt as i_pelunasan, a.*, b.e_area_name from tr_area b, tm_dt a
                        where a.i_area=b.i_area
							          and (upper(a.i_dt) like '%$cari%')
							          and a.i_area='$iarea'
							          and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
							          AND a.d_dt <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_dt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_dt as i_pelunasan,a.*, b.e_area_name from tr_area b, tm_dt a
							          where a.i_area=b.i_area 
							          and (upper(a.i_dt) like '%$cari%')
							          and a.i_area='$iarea'
							          and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') 
							          AND a.d_dt <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_dt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($idt,$iarea,$tgl)
    {
		$this->db->select("* from tm_dt 
				               inner join tr_area on (tm_dt.i_area=tr_area.i_area)
				               where tm_dt.i_dt ='$idt' and tm_dt.i_area='$iarea' and tm_dt.d_dt='$tgl'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($idt,$iarea,$tgl)
    {
		  $this->db->select("a.*, b.v_sisa as sisanota, b.d_jatuh_tempo, c.e_customer_name, c.e_customer_city
                         from tm_dt_item a 
				             	   inner join tm_nota b on (b.i_nota=a.i_nota)
					               inner join tr_customer c on (b.i_customer=c.i_customer)
						             where a.i_dt = '$idt' and a.i_area='$iarea' and a.d_dt='$tgl'
						     	       order by a.n_item_no", false);
# and b.i_area=a.i_area)
#                         inner join tr_customer_groupar d on (d.i_customer=c.i_customer)
#7okt2013                 inner join tr_customer_groupbayar d on (d.i_customer=c.i_customer)
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function updatecek($ecek,$user,$idt,$iarea,$ddt)
    	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$data = array(
			'e_cek'		=> $ecek,
			'd_cek'		=> $dentry,
			'i_cek'		=> $user
				 );
  	$this->db->where('d_dt', $ddt);
  	$this->db->where('i_dt', $idt);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_dt', $data);
    	}
}
?>
