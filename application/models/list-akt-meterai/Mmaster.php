<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ialokasi,$iarea) 
    {
		//* DELETE BANK MASUK (ALOKASI METERAI)
		if(substr($ialokasi,0,2)=="MT"){
			//* UnPosting      
			$this->db->query("	insert into th_jurnal_transharian select * from tm_jurnal_transharian 
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");
			
			$this->db->query("	insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");
			
			$this->db->query("	insert into th_general_ledger select * from tm_general_ledger
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");

			//* UPDATE tm_coa_saldo
			$quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
										from tm_general_ledger
										where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea'");
			if($quer->num_rows()>0){
				foreach($quer->result() as $xx){
					$this->db->query("	UPDATE tm_coa_saldo SET v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
										v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
										v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet-$xx->v_mutasi_kredit
										where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
				}
			}

			//* HAPUS tm_jurnal_transharian
			$this->db->query(" DELETE FROM tm_jurnal_transharian WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);

			//* HAPUS tm_jurnal_transharianitem
			$this->db->query(" DELETE FROM tm_jurnal_transharianitem WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);

			//* HAPUS tm_general_ledger
			$this->db->query(" DELETE FROM tm_general_ledger WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);
			//* END UnPosting

			//* BATAL ALOKASI MT
			$this->db->query(" UPDATE tm_alokasimt SET f_alokasi_cancel = 't' WHERE i_alokasi = '$ialokasi' AND i_area = '$iarea' ", false);

			//* GET nilai Meterai
			$data = $this->db->query(" 	SELECT i_kbank, i_area, i_nota, v_jumlah
										FROM tm_alokasimt_item
										WHERE i_alokasi = '$ialokasi' AND i_area = '$iarea' ");
			if($data->num_rows()>0){
				$vreturn = 0;
				foreach($data->result() AS $bank){
					$ikbank  = $bank->i_kbank;
					$iareakb = $bank->i_area;
					$vjumlah = $bank->v_jumlah;
					$inota   = $bank->i_nota;
					$vreturn += $vjumlah;

					//* UPDATE tm_nota (v_materai_sisa)
					$this->db->query(" UPDATE tm_nota SET v_materai_sisa = v_materai_sisa+$vjumlah WHERE i_nota = '$inota' ", FALSE);	
				}

				//* UPDATE tm_kbank
				$this->db->query(" UPDATE tm_kbank SET v_sisa = v_sisa+$vreturn WHERE i_kbank = '$ikbank' AND i_area = '$iareakb' ", FALSE);
			}
		}
		//* DELETE ALOKASI METERAI (PENYESUAIAN)
		else if(substr($ialokasi,0,2)=="AM"){
			//* UnPosting      
			$this->db->query("	insert into th_jurnal_transharian select * from tm_jurnal_transharian 
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");
			
			$this->db->query("	insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");
			
			$this->db->query("	insert into th_general_ledger select * from tm_general_ledger
								where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ");

			//* UPDATE tm_coa_saldo
			$quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
										from tm_general_ledger
										where substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea'");
			if($quer->num_rows()>0){
				foreach($quer->result() as $xx){
					$this->db->query("	UPDATE tm_coa_saldo SET v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
										v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
										v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
										where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
				}
			}

			//* HAPUS tm_jurnal_transharian
			$this->db->query(" DELETE FROM tm_jurnal_transharian WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);

			//* HAPUS tm_jurnal_transharianitem
			$this->db->query(" DELETE FROM tm_jurnal_transharianitem WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);

			//* HAPUS tm_general_ledger
			$this->db->query(" DELETE FROM tm_general_ledger WHERE substr(i_refference,1,13) = '$ialokasi' AND i_area = '$iarea' ",FALSE);
			//* END UnPosting

			//* BATAL ALOKASI MT
			$this->db->query(" UPDATE tm_meterai SET f_alokasi_cancel = 't' WHERE i_alokasi = '$ialokasi' AND i_area = '$iarea' ", false);

			//* GET nilai Meterai
			$data = $this->db->query(" 	SELECT i_area, i_nota, v_jumlah
										FROM tm_meterai_item
										WHERE i_alokasi = '$ialokasi' AND i_area = '$iarea' ");
			if($data->num_rows()>0){
				$vreturn = 0;
				foreach($data->result() AS $bank){
					$iareakb = $bank->i_area;
					$vjumlah = $bank->v_jumlah;
					$inota   = $bank->i_nota;
					
					//* UPDATE v_materai_sisa
					$this->db->query(" UPDATE tm_nota SET v_materai_sisa = v_materai_sisa+$vjumlah WHERE i_nota = '$inota' ", FALSE);	
				}
			}
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		if($iarea=="NA"){
			$this->db->select("	a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
								a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
									FROM tm_alokasimt a
									INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
									a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
								UNION ALL
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' as i_coa_bank
									FROM tm_meterai a
									INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									a.i_area, a.f_alokasi_cancel
								) AS a 
									WHERE
								a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%' ",false)->limit($num,$offset);
		}else{
			$this->db->select("	a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
								a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
									FROM tm_alokasimt a
									INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									AND a.i_area = '$iarea'
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
									a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
								UNION ALL
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' as i_coa_bank
									FROM tm_meterai a
									INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									AND a.i_area = '$iarea'
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah,
									a.i_area, a.f_alokasi_cancel
								) AS a 
									WHERE
								a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%'",false)->limit($num,$offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacatotal($iarea, $dfrom, $dto)
    {
		if($iarea=="NA"){
			$query = $this->db->query("SELECT	a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
								a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
									FROM tm_alokasimt a
									INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
									a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
								UNION ALL
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' as i_coa_bank
									FROM tm_meterai a
									INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									a.i_area, a.f_alokasi_cancel
								) AS a 
								WHERE
									a.f_alokasi_cancel = 'f' ");
		}else{
			$query = $this->db->query("SELECT	a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
								a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
									FROM tm_alokasimt a
									INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									AND a.i_area = '$iarea'
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
									a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
								UNION ALL
									SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
									(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' as i_coa_bank
									FROM tm_meterai a
									INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
									INNER JOIN tr_area d on (a.i_area = d.i_area)
									INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
									WHERE
									a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
									AND a.i_area = '$iarea'
									GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah,
									a.i_area, a.f_alokasi_cancel
								) AS a
								WHERE
									a.f_alokasi_cancel = 'f'");
		}

		// $query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
