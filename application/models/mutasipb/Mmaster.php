<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
  {
    parent::__construct();
	#$this->CI =& get_instance();
  }
  function baca($iperiode,$icustomer,$num,$offset,$cari)
  {
	$this->db->select("	a.*, b.e_product_name, c.e_customer_name from tm_mutasi_consigment a, tr_product b, tr_customer c
		                where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_customer=c.i_customer
      					    and a.i_customer='$icustomer' order by b.e_product_name ",false);#->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }
  function bacaexcel($iperiode,$istore,$cari)
  {
	$this->db->select("	a.*, b.e_product_name, c.v_product_retail from tm_mutasi a, tr_product b, tr_product_price c
		                where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_product=c.i_product and c.i_price_group='00'
			            and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
	$query = $this->db->get();
    return $query;
  }
  function proses($iperiode,$icustomer,$so)
  {
    $this->db->select(" i_customer from tr_mutasipb 
                        where i_customer='$icustomer' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      $this->db->query("update tr_mutasipb
                        set e_periode='$iperiode'
                        where i_customer='$icustomer'", false);
    }else{
      $this->db->query("insert into tr_mutasipb(i_customer, e_periode) 
                        values ('$icustomer','$iperiode')");
    }
    $soAkhir=$so;
    $query = $this->db->query(" select * from tm_mutasi_headerconsigment where i_customer='$icustomer' and e_mutasi_periode='$iperiode'");
    $st=$query->row();
    if($query->num_rows()>0)
    {
      $soAwal=$st->i_stockopname_awal;
    }else{
      $soAwal='';
    }
    $this->db->query(" delete from tm_mutasi_consigment where e_mutasi_periode='$iperiode' and i_customer='$icustomer'");
    $kuerys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_headerconsigment
                               where i_customer='$icustomer' and e_mutasi_periode='$iperiode'",false);
    if($kuerys->num_rows()>0)
    {
      $row   	= $kuerys->row();
      if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
      {
        $this->db->query(" update tm_ic_consigment set n_quantity_stock=0 where i_customer='$icustomer'");
      }else{
        $kuerys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
        $row   	= $kuerys->row();
        if($row->c==$iperiode)
        {
          $this->db->query(" update tm_ic_consigment set n_quantity_stock=0 where i_customer='$icustomer'");
        }
      }
    }
    $query = $this->db->query(" select distinct i_product from tm_sopb_item 
                                where i_customer='$icustomer' and (i_sopb='$soAwal' or i_sopb='$soAkhir')");
    if ($query->num_rows() > 0)
    {
      $emutasiperiode=$iperiode;
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
      }
      $quer=$this->db->query(" select * from tm_mutasi_headerconsigment
                               where i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'",false);
      if($quer->num_rows()>0)
      {
        $que=$this->db->query(" UPDATE tm_mutasi_headerconsigment
                                set i_stockopname_akhir='$soAkhir'
                                where i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'",false);
      }else{
        $que=$this->db->query("insert into tm_mutasi_headerconsigment values
                              ('$icustomer','$emutasiperiode',null,'$soAkhir')",false);
      }
      $quer=$this->db->query("select * from tm_mutasi_headerconsigment
                              where i_customer='$icustomer' and e_mutasi_periode='$perdpn'",false);
      if($quer->num_rows()>0)
      {
        $que=$this->db->query(" UPDATE tm_mutasi_headerconsigment
                                set i_stockopname_awal='$soAkhir'
                                where i_customer='$icustomer' and e_mutasi_periode='$perdpn'",false);
      }else{
        $que=$this->db->query(" insert into tm_mutasi_headerconsigment values
                              ('$icustomer','$perdpn','$soAkhir',null)",false);
      }
  		foreach($query->result() as $gie)
      {
        $que = $this->db->query("select	customer,periode,product,daripusat,darilang,penjualan,kepusat 
                                 from vmutasiconsigment where periode='$iperiode' and customer='$icustomer' and product='$gie->i_product'");
		    if ($que->num_rows() > 0)
        {
			    foreach($que->result() as $vie)
          {
            if($vie->daripusat==null)$vie->daripusat=0;
            if($vie->darilang==null)$vie->darilang=0;
            if($vie->penjualan==null)$vie->penjualan=0;
            if($vie->kepusat==null)$vie->kepusat=0;
            $blawal=substr($iperiode,4,2)-1;
            if($blawal==0)
            {
              $perawal=substr($iperiode,0,4)-1;
              $perawal=$perawal.'12';
            }else{
              $perawal=substr($iperiode,0,4);
              $perawal=$perawal.substr($iperiode,4,2)-1;;
            }
            $quer=$this->db->query("select n_sopb from tm_sopb_item
                                    where i_sopb='$soAwal' and i_customer='$icustomer' and i_product='$vie->product'");
            if ($quer->num_rows() > 0)
            {
              foreach($quer->result() as $ni)
              {
                $sawal=$ni->n_sopb;
              }
              $akhir=$sawal+($vie->daripusat+$vie->darilang)-($vie->penjualan+$vie->kepusat);
              if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
              $opname=0;
              $qur=$this->db->query("select n_sopb from tm_sopb_item 
                                     where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$vie->product'");
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_sopb;
                }
              }else{
                $opname==0;
              }
              $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                     where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$vie->product'");
              if ($cek->num_rows() == 0)
              {
			          $this->db->query("insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
	                                n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
	                                n_saldo_akhir,n_saldo_stockopname)
	                                values
	                                ('$iperiode','$icustomer','$vie->product','$grade','00',$sawal, $vie->daripusat,$vie->darilang,$vie->penjualan,
                                    $vie->kepusat,$akhir,$opname)");
              }
            }else{
              $sawal=0;
              $akhir=$sawal+($vie->daripusat+$vie->darilang)-($vie->penjualan+$vie->kepusat);
              if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
              $opname=0;
              $qur=$this->db->query("select n_sopb from tm_sopb_item 
                                     where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$vie->product'");
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_sopb;
                }
              }else{
                $opname==0;
              }
              $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                     where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$vie->product'");
              if ($cek->num_rows() == 0)
              {
                $this->db->query("insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
                                  n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                  n_saldo_akhir,n_saldo_stockopname)
                                  values
                                  ('$iperiode','$icustomer','$vie->product','$grade','00',
                                    $sawal,$vie->daripusat,$vie->darilang,$vie->penjualan,$vie->kepusat,$akhir,$opname)");
              }
            }
            $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                    from tm_mutasi_consigment where e_mutasi_periode='$iperiode' and i_customer='$icustomer' 
                                    and i_product='$vie->product'");
            if ($qu->num_rows() > 0)
            {
              foreach($qu->result() as $vei)
              {
                $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_headerconsigment
                                         where i_customer='$icustomer' and e_mutasi_periode='$iperiode'",false);
                if($querys->num_rows()>0)
                {
                  $row   	= $querys->row();
                  if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
                  {
                    $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                            where i_customer='$icustomer' and i_product='$vei->i_product'
                                            and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                    if($queryz->num_rows>0){
                      $this->db->query("update tm_ic_consigment set 
                                        n_quantity_stock=$vei->n_saldo_akhir
                                        where i_customer='$icustomer' and i_product='$vei->i_product' 
                                        and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                    }else{
                      $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer,
                                        f_product_active) 
                                        values
                                        ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                                        '$icustomer','t')");
                    }
                  }else{
                    $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                    $row   	= $querys->row();
                    if($row->c==$iperiode)
                    {
                      $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                              where i_customer='$icustomer' and i_product='$vei->i_product' 
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                      if($queryz->num_rows>0){
                        $this->db->query("update tm_ic_consigment set 
                                          n_quantity_stock=$vei->n_saldo_akhir
                                          where i_customer='$icustomer' and i_product='$vei->i_product'
                                          and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                      }else{
                        $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer, 
                                          f_product_active) 
                                          values
                                          ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                                          '$icustomer','t')");
                      }
                    }
                  }
                }
              }
            }
          }
        }else{
          $blawal=substr($iperiode,4,2)-1;
          if($blawal==0)
          {
            $perawal=substr($iperiode,0,4)-1;
            $perawal=$perawal.'12';
          }else{
            $perawal=substr($iperiode,0,4);
            $perawal=$perawal.substr($iperiode,4,2)-1;;
          }
          $quer=$this->db->query("select n_sopb from tm_sopb_item 
                                  where i_sopb='$soAwal' and i_customer='$icustomer' and i_product='$gie->i_product'");

          if ($quer->num_rows() > 0)
          {
            foreach($quer->result() as $ni)
            {
              $stock=$ni->n_sopb;
            }
            if($stock>0)
            {
              $sawal=$stock;
              $akhir=$sawal;
              if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
              $opname=0;
              $qur=$this->db->query(" select n_sopb from tm_sopb_item 
                                   where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$gie->i_product'");
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_sopb;
                }
              }else{
                $opname==0;
              }
              $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                     where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$gie->i_product'");
              if ($cek->num_rows() == 0)
              {
                $this->db->query("insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
                                  n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                  n_saldo_akhir,n_saldo_stockopname)
                                  values
                                  ('$iperiode','$icustomer','$gie->i_product','$grade','00',
                                    $sawal,0,0,0,0,$akhir,$opname)");
              }
            }
          }else{
            $quer=$this->db->query(" select n_sopb from tm_sopb_item 
                                     where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$gie->i_product'");
            if ($quer->num_rows() > 0)
            {
              foreach($quer->result() as $ni)
              {
                $opname=$ni->n_sopb;
              }
              if($opname>0)
              {
                if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
                $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                       where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$gie->i_product'");
                if ($cek->num_rows() == 0)
                {
                  $this->db->query("	insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
                                                           n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                                           n_saldo_akhir,n_saldo_stockopname)
                                                           values
                                                          ('$iperiode','$icustomer','$gie->i_product','$grade','00',
                                                           0,0,0,0,0,0,$opname)");
                }
              }
            }
          }
        }
#####Tambahan
        $qu = $this->db->query("select * from tm_mutasi_consigment where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$gie->i_product'");
        if ($qu->num_rows() > 0)
        {
          foreach($qu->result() as $vei)
          {
            $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_headerconsigment
                                       where i_customer='$icustomer' and e_mutasi_periode='$iperiode'",false);
            if($querys->num_rows()>0)
            {
              $row   	= $querys->row();
              if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
              {
                $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                            where i_customer='$icustomer' and i_product='$gie->i_product'
                                            and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                if($queryz->num_rows>0){
                  $this->db->query("update tm_ic_consigment set n_quantity_stock=$vei->n_saldo_akhir where i_customer='$icustomer' 
                                    and i_product='$gie->i_product' and i_product_motif='$vei->i_product_motif' 
                                    and i_product_grade='$vei->i_product_grade'");
                }else{
                  $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer,
                  f_product_active) 
                  values
                  ($vei->n_saldo_akhir,'$gie->i_product','$vei->i_product_motif','$vei->i_product_grade',
                  '$icustomer','t')");
                }
              }else{
                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row   	= $querys->row();
                if($row->c==$iperiode)
                {
                  $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                              where i_customer='$icustomer' and i_product='$gie->i_product'
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  if($queryz->num_rows>0){
                    $this->db->query("update tm_ic_consigment set n_quantity_stock=$vei->n_saldo_akhir where i_customer='$icustomer' 
                                      and i_product='$gie->i_product'
                                      and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  }else{
                    $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer,
                    f_product_active) 
                    values
                    ($vei->n_saldo_akhir,'$gie->i_product','$vei->i_product_motif','$vei->i_product_grade',
                    '$icustomer','t')");
                  }
                }
              }
            }
          }
        }
#####End Tambahan
      }
    }else{

    }
      
    $que = $this->db->query(" select customer,periode,product,daripusat,darilang,penjualan,kepusat
                              from vmutasiconsigment where periode='$iperiode' and customer='$icustomer'
                              and product not in(select i_product from tm_sopb_item where i_customer='$icustomer' and i_sopb='$soAwal')");
    if ($que->num_rows() > 0)
    {
      foreach($que->result() as $vie)
      {
        if($vie->daripusat==null)$vie->daripusat=0;
        if($vie->darilang==null)$vie->darilang=0;
        if($vie->penjualan==null)$vie->penjualan=0;
        if($vie->kepusat==null)$vie->kepusat=0;
        $blawal=substr($iperiode,4,2)-1;
        if($blawal==0)
        {
          $perawal=substr($iperiode,0,4)-1;
          $perawal=$perawal.'12';
        }else{
          $perawal=substr($iperiode,0,4);
          $perawal=$perawal.substr($iperiode,4,2)-1;;
        }
        $quer=$this->db->query("select * from tm_sopb_item 
                                where i_sopb='$soAwal' and i_customer='$icustomer' and i_product='$vie->product'");
        if ($quer->num_rows() > 0)
        {
          foreach($quer->result() as $ni)
          {
            $sawal=$ni->n_sopb;
          }
          $akhir=$sawal+($vie->daripusat+$vie->darilang)-($vie->penjualan+$vie->kepusat);
          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
          $opname=0;
          $qur=$this->db->query(" select n_sopb from tm_sopb_item 
                                  where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$vie->product'");
          if ($qur->num_rows() > 0)
          {
            foreach($qur->result() as $nu)
            {
              $opname=$nu->n_sopb;
            }
          }else{
            $opname==0;
          }
          $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                 where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$vie->product'");
          if ($cek->num_rows() == 0)
          {
            $this->db->query("insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
                             n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                             n_saldo_akhir,n_saldo_stockopname)
                             values
                             ('$iperiode','$icustomer','$vie->product','$grade','00',
                               $sawal,$vie->daripusat,$vie->darilang,$vie->penjualan,$vie->kepusat,$akhir,$opname)");
          }
        }else{
          $sawal=0;
          $akhir=$sawal+($vie->daripusat+$vie->darilang)-($vie->penjualan+$vie->kepusat);
          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
          $opname=0;
          $qur=$this->db->query(" select n_sopb from tm_sopb_item 
                                  where i_sopb='$soAkhir' and i_customer='$icustomer' and i_product='$vie->product'");
          if ($qur->num_rows() > 0)
          {
            foreach($qur->result() as $nu)
            {
              $opname=$nu->n_sopb;
            }
          }else{
            $opname==0;
          }
          $cek=$this->db->query("select i_product from tm_mutasi_consigment
                                 where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$vie->product'");
          if ($cek->num_rows() == 0)
          {
            $this->db->query("insert into tm_mutasi_consigment (e_mutasi_periode,i_customer,i_product,i_product_grade,i_product_motif,
                              n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                              n_saldo_akhir,n_saldo_stockopname)
                              values
                              ('$iperiode','$icustomer','$vie->product','$grade','00',
                                $sawal,$vie->daripusat,$vie->darilang,$vie->penjualan,$vie->kepusat,$akhir,$opname)");
          }
        }
        $qu = $this->db->query("select * from tm_mutasi_consigment where e_mutasi_periode='$iperiode' and i_customer='$icustomer' and i_product='$vie->product'");
        if ($qu->num_rows() > 0)
        {
          foreach($qu->result() as $vei)
          {
            $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_headerconsigment
                                     where i_customer='$icustomer' and e_mutasi_periode='$iperiode'",false);
            if($querys->num_rows()>0)
            {
              $row   	= $querys->row();
              if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
              {
                $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                            where i_customer='$icustomer' and i_product='$vei->i_product' and i_product_motif='$vei->i_product_motif' 
                                            and i_product_grade='$vei->i_product_grade'");
                if($queryz->num_rows>0){
                  $this->db->query("update tm_ic_consigment set n_quantity_stock=$vei->n_saldo_akhir where i_customer='$icustomer' 
                                    and i_product='$vei->i_product' and i_product_motif='$vei->i_product_motif' 
                                    and i_product_grade='$vei->i_product_grade'");
                }else{
                  $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer, 
                  f_product_active) 
                  values
                  ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                  '$icustomer','t')");
                }
              }else{
                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row   	= $querys->row();
                if($row->c==$iperiode)
                {
                  $queryz = $this->db->query("SELECT i_product from tm_ic_consigment
                                              where i_customer='$icustomer' and i_product='$vei->i_product' 
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  if($queryz->num_rows>0)
                  {
                    $this->db->query("update tm_ic_consigment set n_quantity_stock=$vei->n_saldo_akhir where i_customer='$icustomer' 
                                      and i_product='$vei->i_product' and i_product_motif='$vei->i_product_motif' 
                                      and i_product_grade='$vei->i_product_grade'");
                  }else{
                    $this->db->query("insert into tm_ic_consigment (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_customer, 
                    f_product_active) 
                    values
                    ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                    '$icustomer','t')");
                  }
                }
              }
            }
          }
        }
      }
    }
  } 
  function detail($iperiode,$icustomer,$iproduct,$iproductmotif,$iproductgrade)
  {
    $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.customer, a.periode, a.product, e.e_customer_name, 
                        sum(a.in) as in, sum(a.out) as out
                        FROM tr_product b, vmutasiconsigmentdetail a
                        left join tr_customer e on a.customer=e.i_customer 
                        WHERE 
                        b.i_product = a.product AND a.periode='$iperiode' AND a.customer='$icustomer' AND a.product='$iproduct' 
                        group by b.e_product_name, a.ireff, a.dreff, a.customer, a.periode, a.product, e.e_customer_name                            
                        order by dreff, ireff",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
	    return $query->result();
    }
  }
  function bacacustomer($cari,$num,$offset)
  {
	  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address
                        from tr_customer_consigment a, tr_customer b
                        where a.i_customer=b.i_customer and a.i_customer like '%$cari%'
                        order by a.i_customer", false)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    return $query->result();
	  }
  }
  function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                         from tr_area a, tr_store b 
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                         and a.i_store=b.i_store
						 order by a.i_store ", FALSE)->limit($num,$offset);
	}else{
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                         from tr_area a, tr_store b
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						 or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function bacaso($num,$offset,$icustomer,$peri)
  {
	$this->db->select(" i_sopb from tm_sopb
                        where f_sopb_cancel='f' and i_customer = '$icustomer' and to_char(d_sopb,'yyyymm')='$peri' 
                        order by i_sopb desc", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
  }
}
?>
