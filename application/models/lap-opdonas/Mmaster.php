<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$num,$offset)
    {
		$this->db->select(" sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                        sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                        a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                        from tm_op_item b
                        left join tm_do c on (b.i_op=c.i_op)
                        left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do and a.n_deliver>0)
                        inner join tr_supplier d on (a.i_supplier=d.i_supplier)
                        where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        group by a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                        order by a.i_product",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$cari,$num,$offset)
    {
			  $this->db->select(" sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                            sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                            a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                            from tm_op_item b
                            left join tm_do c on (b.i_op=c.i_op)
                            left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do and a.n_deliver>0)
                            inner join tr_supplier d on (a.i_supplier=d.i_supplier)
                            where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                            and(upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                            group by a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                            order by a.i_product",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier 
				     where upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%' order by i_supplier", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
