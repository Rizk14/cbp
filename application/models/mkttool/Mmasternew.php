<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproduct,$iproductgrade)
    {
		$this->db->select(" * from tr_product_price 
					              LEFT JOIN tr_product_grade 
					                 ON (tr_product_price.i_product_grade=tr_product_grade.i_product_grade)
					              where tr_product_price.i_product = '$iproduct' 
					                and tr_product_price.i_product_grade = '$iproductgrade' order by tr_product_price.i_price_group", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadata($user)
    {
		  $this->db->select(" a.*, b.e_salesman_name 
		  					 FROM tm_marketing_tool_tmp a  
							 INNER JOIN tr_salesman b on (a.i_salesman = b.i_salesman)
		  					 WHERE a.i_user = '$user' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
	function save($iloyal,$eremark1,$vestimasi,$isalesman,$icustomer,$iarea,$ecustomer,$jenis,$imkt,$jenispromo2,$diskon,$tandabukti,$remark2,$remark3,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$toollainnya){
		$tgl		= date('Y-m-d');
		$query   	= $this->db->query("SELECT current_timestamp as c");
    	$row     	= $query->row();
    	$dentry  	= $row->c;

		if($jenis == 1){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $tgl ,
				  'd_entry'           => $dentry ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_loyalitas_type'  => $iloyal ,
				  'e_tool_remark'     => $eremark1 ,
				  'v_biaya_eta'       => $vestimasi
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 2){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $tgl ,
				  'd_entry'           => $dentry ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_promo_type2'  	  => $jenispromo2 ,
				  'e_diskon_request'  => $diskon ,
				  'e_tanda_bukti'     => $tandabukti ,
				  'e_tool_remark'     => $remark2 ,
				  'v_biaya_eta'       => $vestimasi 
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 3){
			$this->db->set(
				array(
				  'i_mkt'             		=> $imkt ,
				  'd_mkt'             		=> $tgl ,
				  'd_entry'           		=> $dentry ,
				  'f_cancel'          		=> false ,
				  'i_salesman'        		=> $isalesman ,
				  'i_customer'        		=> $icustomer ,
				  'i_mkt_type'        		=> $jenis ,
				  'e_jenis_market'			=> $jenismarket,
				  'e_jenis_marketlainnya'	=> $toollainnya,
				  'n_panjang'				=> $panjang,
				  'n_lebar'					=> $lebar,
				  'n_tinggi'				=> $tinggi,
				  'n_ukuran_lainnya'		=> $ukuranlain,
				  'n_qty'					=> $jumqty,
				  'e_tool_remark'			=> $remark3,
				  'v_biaya_eta'				=> $vestimasi
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}
	}

    function insert_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
			$this->db->query("insert into tm_marketing_tool_tmp (i_user,i_salesman,i_customer,i_area,
									e_customer_name,i_mkt_type,d_entry) values
				          		  ('$user','$isalesman','$icustomer','$iarea','$ecustomer','$jenispromo','$dentry')");
    }
    function update_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;

		  $this->db->set(
			array(
				'i_salesman' 		=> $isalesman,
				'i_customer' 		=> $icustomer,
				'i_area' 			=> $iarea,
				'e_customer_name' 	=> $ecustomer,
				'i_mkt_type' 		=> $jenispromo,
				'd_entry' 			=> $dentry
			)
		);
		$this->db->where('i_user', $user);
		$this->db->update('tm_marketing_tool_tmp');
    }

	function runningnumber()
	{
	  $th		= date('Y');
	  $thbln	= date('Ym');
	  $thbl 	= substr($thbln, 2, 2) . substr($thbln, 4, 2);
	  $iarea 	= '00';
	  $this->db->select(" n_modul_no as max from tm_dgu_no
							where i_modul='MKT'
							and substr(e_periode,1,4)='$th' for update", false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0) {
		foreach ($query->result() as $row) {
		  $terakhir = $row->max;
		}
		$imkt  = $terakhir + 1;
		$this->db->query(" update tm_dgu_no
							  set n_modul_no=$imkt
							  where i_modul='MKT'
							  and substr(e_periode,1,4)='$th'", false);
		settype($imkt, "string");
		$a = strlen($imkt);
		while ($a < 6) {
		  $imkt = "0" . $imkt;
		  $a = strlen($imkt);
		}
		$imkt  = "MKT-" . $thbl . "-" . $imkt;
		return $imkt;
	  } else {
		$imkt  = "000001";
		$imkt  = "MKT-" . $thbl . "-" . $imkt;
		$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
							 values ('MKT','$iarea','$th',1)");
		return $imkt;
	  }
	}

	
    public function deletetmp($user) 
    {
		$this->db->query("DELETE FROM tm_marketing_tool_tmp WHERE i_user = '$user'");
		return TRUE;
    }
    
    function bacasadaya($cari, $num,$offset)
    {
		$this->db->select(" 	a.*,
								b.e_loyal,
								c.nama,
								d.e_namamkt,
								e.e_customer_name,
								f.e_salesman_name,
								CASE
									WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
									WHEN a.i_mkt_type = 2 THEN 'Promo'
									WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
								END AS marketing_type,
								CASE
									WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
									WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
									WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
								END AS promotype2
							FROM
								tm_marketing_tool a
							LEFT JOIN tr_mkttools_loyal b ON
								(a.i_loyalitas_type = b.i_loyal)
							LEFT JOIN tr_mkttools_promo c ON
								(trim(a.e_tanda_bukti) = trim(c.nama))
							LEFT JOIN tr_jns_mkttools d ON
								(a.e_jenis_market = d.i_jnsmkt)
							INNER JOIN tr_customer e ON
								(a.i_customer = e.i_customer)
							INNER JOIN tr_salesman f ON
								(a.i_salesman = f.i_salesman)
							WHERE
								a.f_cancel = 'f'
							ORDER BY
								a.i_mkt_type", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cari($cari,$num,$offset)
    {
     $this->db->select("  * from tr_product_price 
					                where (upper(tr_product_price.i_product) like '%$cari%' 
					                or upper(tr_product_price.e_product_name) = '%$cari%')
                          order by i_product, i_price_group", false)->limit($num,$offset);

/*		$this->db->select(" * from tr_product_price 
					              LEFT JOIN tr_product ON (tr_product_price.i_product=tr_product.i_product)
					              LEFT JOIN tr_product_grade 
					                 ON (tr_product_price.i_product_grade=tr_product_grade.i_product_grade)
					              where upper(tr_product_price.i_product) like '%$cari%' 
					                 or upper(tr_product_price.e_product_name) = '%$cari%'
					                 order by tr_product_price.i_product, tr_product_price.i_price_group
				                ", false)->limit($num,$offset);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacasalesman($num,$offset)
    {
		$this->db->select("i_salesman, e_salesman_name from tr_salesman where f_salesman_aktif = 't' order by i_salesman ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function carisalesman($cari,$num,$offset)
    {
		$this->db->select("i_salesman, e_salesman_name from tr_salesman where upper(i_salesman) like '%$cari%' or upper(e_salesman_name) like '%$cari%' order by i_salesman", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaloyal($num,$offset)
    {
		$this->db->select("i_loyal, e_loyal from tr_mkttools_loyal order by i_loyal ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacamkttools($num,$offset)
    {
		$this->db->select("i_jnsmkt, e_namamkt from tr_jns_mkttools order by i_jnsmkt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacamktpromo($num,$offset)
    {
		$this->db->select(" * from tr_mkttools_promo order by i_kode ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacacustomer($cari,$num,$offset,$periode,$salesman)
    {
		$this->db->select("	DISTINCT a.i_customer,
								a.e_customer_name,
								a.i_area
							FROM
								tr_customer a
							INNER JOIN tr_customer_salesman b ON
								(a.i_customer = b.i_customer)
							WHERE
								b.e_periode = '$periode' AND i_salesman = '$salesman'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		// echo $this->db->last_query();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproduct($cari,$num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product where upper(e_product_name) like '%$cari%' or upper(i_product) like '%$cari%' order by i_product", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
