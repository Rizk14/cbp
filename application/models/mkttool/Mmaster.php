<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacadata($user)
    {
		  $this->db->select(" a.*, b.e_salesman_name 
		  					 FROM tm_marketing_tool_tmp a  
							 INNER JOIN tr_salesman b on (a.i_salesman = b.i_salesman)
		  					 WHERE a.i_user = '$user' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
	function save($iloyal,$eremark1,$vestimasi,$isalesman,$icustomer,$iarea,$ecustomer,$jenis,$imkt,$jenispromo2,$diskon,$tandabukti,$remark2,$remark3,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$toollainnya,$vtarget){
		$tgl		= date('Y-m-d');
		$query   	= $this->db->query("SELECT current_timestamp as c");
    	$row     	= $query->row();
    	$dentry  	= $row->c;

		if($jenis == 1){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $tgl ,
				  'd_entry'           => $dentry ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_loyalitas_type'  => $iloyal ,
				  'e_tool_remark'     => $eremark1 ,
				  'v_biaya_eta'       => $vestimasi,
				  'i_approve_level'   => '6'
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 2){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $tgl ,
				  'd_entry'           => $dentry ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_promo_type2'  	  => $jenispromo2 ,
				  'e_diskon_request'  => $diskon ,
				  'e_tanda_bukti'     => $tandabukti ,
				  'e_tool_remark'     => $remark2 ,
				  'v_biaya_eta'       => $vestimasi,
				  'i_approve_level'   => '6',
				  'v_target'  	 	  => $vtarget
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 3){
			$this->db->set(
				array(
				  'i_mkt'             		=> $imkt ,
				  'd_mkt'             		=> $tgl ,
				  'd_entry'           		=> $dentry ,
				  'f_cancel'          		=> false ,
				  'i_salesman'        		=> $isalesman ,
				  'i_customer'        		=> $icustomer ,
				  'i_mkt_type'        		=> $jenis ,
				  'e_jenis_market'			=> $jenismarket,
				  'e_jenis_marketlainnya'	=> $toollainnya,
				  'n_panjang'				=> $panjang,
				  'n_lebar'					=> $lebar,
				  'n_tinggi'				=> $tinggi,
				  'n_ukuran_lainnya'		=> $ukuranlain,
				  'n_qty'					=> $jumqty,
				  'e_tool_remark'			=> $remark3,
				  'v_biaya_eta'				=> $vestimasi,
				  'i_approve_level'   		=> '6'
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}
	}

	function insert($imkt,$dmkt,$isalesman,$icustomer,$jenis,$dentry,$iarea,$iloyal,$eremark1,$vestimasi,$jenispromo2,$diskon,$tandabukti,$remark2,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$remark3,$realisai,$ipromo,$iapprove,$dapprove,$ilevel,$fmktapp,$toollainnya,$attach1,$attach2,$attach3,$attach4,$vtarget,$itype,$appr1,$appr2,$appr3,$appr4,$appr5,$dappr1,$dappr2,$dappr3,$dappr4,$dappr5){
		/* $tgl		= date('Y-m-d'); */
		$query   	= $this->db->query("SELECT current_timestamp as c");
    	$row     	= $query->row();
    	$dupdate  	= $row->c;
		$dapprove  	= $row->c;
		

		if($jenis == 1){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $dmkt ,
				  'd_entry'           => $dentry ,
				  'd_update'          => $dupdate ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_loyalitas_type'  => $iloyal ,
				  'e_tool_remark'     => $eremark1 ,
				  'v_biaya_eta'       => $vestimasi ,
				  'v_biaya_realisasi' => $realisai,
				  /* 'i_approve' 		  => $iapprove,
				  'd_approve' 		  => $dapprove, */
				  'i_approve_level'   => $ilevel,
				  'f_mkt_approve'	  => $fmktapp,
				  'f_mkt_approve'	  => $fmktapp,
				  'e_attch1'	  	  => $attach1,
				  'e_attch2'	  	  => $attach2,
				  'e_attch3'	  	  => $attach3,
				  'e_attch4'	  	  => $attach4,
				  'i_approve'		  => $appr1,
				  'i_approve2'		  => $appr2,
				  'i_approve3'		  => $appr3,
				  'i_approve4'		  => $appr4,
				  'i_approve5'		  => $appr5,
				  'd_approve'		  => $dappr1,
				  'd_approve2'		  => $dappr2,
				  'd_approve3'		  => $dappr3,
				  'd_approve4'		  => $dappr4,
				  'd_approve5'		  => $dappr5
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 2){
			$this->db->set(
				array(
				  'i_mkt'             => $imkt ,
				  'd_mkt'             => $dmkt ,
				  'd_entry'           => $dentry ,
				  'd_update'          => $dupdate ,
				  'f_cancel'          => false ,
				  'i_salesman'        => $isalesman ,
				  'i_customer'        => $icustomer ,
				  'i_mkt_type'        => $jenis ,
				  'i_promo_type2'  	  => $itype ,
				  'e_diskon_request'  => $diskon ,
				  'e_tanda_bukti'     => $tandabukti ,
				  'e_tool_remark'     => $remark2 ,
				  'v_biaya_eta'       => 0 ,
				  'i_promo'       	  => $ipromo,
				  /* 'i_approve' 		  => $iapprove,
				  'd_approve' 		  => $dapprove, */
				  'i_approve_level'   => $ilevel,
				  'v_biaya_realisasi' => 0,
				  'f_mkt_approve'	  => $fmktapp,
				  'e_attch1'	  	  => $attach1,
				  'e_attch2'	  	  => $attach2,
				  'e_attch3'	  	  => $attach3,
				  'e_attch4'	  	  => $attach4, 
				  'v_target'		  => $vtarget,
				  'v_biaya_eta'		  => $vestimasi,
				  'v_biaya_realisasi' => $realisai,
				  'i_approve'		  => $appr1,
				  'i_approve2'		  => $appr2,
				  'i_approve3'		  => $appr3,
				  'i_approve4'		  => $appr4,
				  'i_approve5'		  => $appr5,
				  'd_approve'		  => $dappr1,
				  'd_approve2'		  => $dappr2,
				  'd_approve3'		  => $dappr3,
				  'd_approve4'		  => $dappr4,
				  'd_approve5'		  => $dappr5
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}else if($jenis == 3){
			$this->db->set(
				array(
				  'i_mkt'             		=> $imkt ,
				  'd_mkt'             		=> $dmkt ,
				  'd_entry'           		=> $dentry ,
				  'd_update'          		=> $dupdate ,
				  'f_cancel'          		=> false ,
				  'i_salesman'        		=> $isalesman ,
				  'i_customer'        		=> $icustomer ,
				  'i_mkt_type'        		=> $jenis ,
				  'e_jenis_market'			=> $jenismarket,
				  'e_jenis_marketlainnya'	=> $toollainnya,
				  'n_panjang'				=> $panjang,
				  'n_lebar'					=> $lebar,
				  'n_tinggi'				=> $tinggi,
				  'n_ukuran_lainnya'		=> $ukuranlain,
				  'n_qty'					=> $jumqty,
				  'e_tool_remark'			=> $remark3,
				  'v_biaya_eta'				=> $vestimasi,
				  'v_biaya_realisasi' 		=> $realisai,
				  /* 'i_approve' 				=> $iapprove,
				  'd_approve' 				=> $dapprove, */
				  'i_approve_level' 		=> $ilevel,
				  'f_mkt_approve'	  		=> $fmktapp,
				  'e_attch1'	  	  		=> $attach1,
				  'e_attch2'	  	  		=> $attach2,
				  'e_attch3'	  	  		=> $attach3,
				  'e_attch4'	  	  		=> $attach4,
				  'i_approve'		  		=> $appr1,
				  'i_approve2'		  		=> $appr2,
				  'i_approve3'		  		=> $appr3,
				  'i_approve4'		  		=> $appr4,
				  'i_approve5'		  		=> $appr5,
				  'd_approve'		  		=> $dappr1,
				  'd_approve2'		  		=> $dappr2,
				  'd_approve3'		  		=> $dappr3,
				  'd_approve4'		  		=> $dappr4,
				  'd_approve5'		  		=> $dappr5
				)
			  );
			  $this->db->insert('tm_marketing_tool');
		}
	}

    function insert_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
			$this->db->query("insert into tm_marketing_tool_tmp (i_user,i_salesman,i_customer,i_area,
									e_customer_name,i_mkt_type,d_entry) values
				          		  ('$user','$isalesman','$icustomer','$iarea','$ecustomer','$jenispromo','$dentry')");
    }

    function insertnote($i_mkt,$dept,$user,$note,$roll,$level)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
		  $f_mkt_rollback = 1;
			$this->db->query("insert into tm_marketing_tool_note (i_mkt,i_dept,i_user,f_mkt_rollback,e_note,d_entry,i_level) 
							  values('$i_mkt','$dept','$user','$roll','$note','$dentry','$level')");
    }

    function update_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;

		  $this->db->set(
			array(
				'i_salesman' 		=> $isalesman,
				'i_customer' 		=> $icustomer,
				'i_area' 			=> $iarea,
				'e_customer_name' 	=> $ecustomer,
				'i_mkt_type' 		=> $jenispromo,
				'd_entry' 			=> $dentry
			)
		);
		$this->db->where('i_user', $user);
		$this->db->update('tm_marketing_tool_tmp');
    }

	function rejeckinaja($i_mkt,$note)
    {
		  $this->db->set(
			array(
				'f_reject' 		=> 't',
				'e_alasan' 		=> $note
			)
		);
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
    }


	function updateatach1($i_mkt,$fileName1)
    {

		  $this->db->set(
			array(
				'e_attch1' 		=> $fileName1,
			)
		);
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
    }

	function updateatach2($i_mkt,$fileName2)
    {

		  $this->db->set(
			array(
				'e_attch2' 		=> $fileName2,
			)
		);
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
		/* echo $this->db->last_query(); */
    }

	function updateatach3($i_mkt,$fileName3)
    {

		  $this->db->set(
			array(
				'e_attch3' 		=> $fileName3,
			)
		);
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
    }

	function updateatach4($i_mkt,$fileName4)
    {

		  $this->db->set(
			array(
				'e_attch4' 		=> $fileName4,
			)
		);
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
    }

	function approveas($imkt,$user,$level,$jenis,$dept,$jabatan)
    {
		  $tgl	 = date('Y-m-d');
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;

		if($jenis == '1'){//blok ini untuk approve as
			if($level <= 3){ //untuk departement 3 kebawah status approve mkt akan otomatis true dan proses dianggap selesai
				$status = true;
			}else{
				$status = false;
			}
		if($level <= 2){
			$this->db->set(
				array(
					'f_mkt_approve' 	=> $status,
					'i_approve' 		=> 'SYSTEM',
					'd_approve' 		=> $tgl,
					'i_approve_level' 	=> 0,
					'i_approve2' 		=> 'SYSTEM',
					'd_approve2' 		=> $tgl,
					'i_approve3' 		=> 'SYSTEM',
					'd_approve3' 		=> $tgl,
					'i_approve4' 		=> 'SYSTEM',
					'd_approve4' 		=> $tgl,
					'i_approve5' 		=> $user,
					'd_approve5' 		=> $tgl
				)
			);
		}else{
			$this->db->set(
				array(
					'f_mkt_approve' 	=> $status,
					'i_approve' 		=> $user,
					'd_approve' 		=> $tgl,
					'i_approve_level' 	=> $level
				)
			);
		}

		}else if($jenis == '2'){//blok ini untuk approve marketing
			$this->db->set(
				array(
					'f_mkt_approve' 	=> true,
					'i_approve2' 		=> $user,
					'd_approve2' 		=> $tgl
					// 'i_approve_level' 	=> $level
				)
				);
		}else if($jenis == '3'){
			if ($jabatan == '4' || $jabatan == '5'){//approve diarahkan ke RSM
				$this->db->set(
					array(
						'i_approve_level' 	=> $jabatan,
						'i_approve3' 		=> NULL,
						'd_approve3' 		=> NULL,
						'i_approve4' 		=> NULL,
						'd_approve4' 		=> NULL,
						'i_approve5' 		=> NULL,
						'd_approve5' 		=> NULL
					)
				);
			}else if ($jabatan == '3'){//approve diarahkan ke GM
				$this->db->select(" i_approve_level as level from tm_marketing_tool where i_mkt = '$imkt'",false);
				$query = $this->db->get();
				  if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
					  $apprlev 	= $row->level;
					}
				}
				if($apprlev == '5'){//dari marketing ke GM (RSM dilewat)
					$this->db->set(
						array(
							'i_approve3' 		=> 'SYSTEM',
							'd_approve3' 		=> $tgl,
							'i_approve_level' 	=> $jabatan
						)
					);
				}else if($apprlev == '4'){//dari marketing ke RSM
					$this->db->set(
						array(
							'i_approve3' 		=> $user,
							'd_approve3' 		=> $tgl,
							'i_approve_level' 	=> $jabatan
						)
					);
				}
			}else if ($jabatan == '2'){//approve direksi
				$this->db->select(" i_approve_level as level from tm_marketing_tool where i_mkt = '$imkt'",false);
				$query = $this->db->get();
				  if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
					  $apprlev 	= $row->level;
					}
				}
				if($apprlev == '5'){
					$this->db->set(
						array(
							'i_approve3' 		=> 'SYSTEM',
							'd_approve3' 		=> $tgl,
							'i_approve4' 		=> 'SYSTEM',
							'd_approve4' 		=> $tgl,
							'i_approve_level' 	=> $jabatan
						)
					);
				}else if($apprlev == '4'){
					$this->db->set(
						array(
							'i_approve3' 		=> $user,
							'd_approve3' 		=> $tgl,
							'i_approve4' 		=> 'SYSTEM',
							'd_approve4' 		=> $tgl,
							'i_approve_level' 	=> $jabatan
						)
					);
				}else if($apprlev == '3'){
					$this->db->set(
						array(
							'i_approve4' 		=> $user,
							'd_approve4' 		=> $tgl,
							'i_approve_level' 	=> $jabatan
						)
					);
				}
			}else if($jabatan == '0'){//approve untuk direksi
				$this->db->set(
					array(
				'i_approve5' 		=> $user,
				'd_approve5' 		=> $tgl,
				'i_approve_level' 	=> $jabatan
				)
			);
			}
		}
		$this->db->where('i_mkt', $imkt);
		$this->db->update('tm_marketing_tool');
    }


	function rollbackappr($i_mkt,$user,$dept,$level,$jabatan)
    {
		  $tgl	 = date('Y-m-d');
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;

		if($jabatan == 5){
			$this->db->set(
				array(
					'i_approve_level' 	=> 5,
					'i_approve3' 		=> NULL,
					'd_approve3' 		=> NULL,
					'i_approve4' 		=> NULL,
					'd_approve4' 		=> NULL,
					'i_approve5' 		=> NULL,
					'd_approve5' 		=> NULL
				)
			);
		}else if($jabatan == 4){
			$this->db->set(
				array(
					'i_approve_level' 	=> 4,
					'i_approve4' 		=> NULL,
					'd_approve4' 		=> NULL,
					'i_approve5' 		=> NULL,
					'd_approve5' 		=> NULL
				)
			);
		}else if($jabatan == 3){
			$this->db->set(
				array(
					'i_approve_level' 	=> 3,
					'i_approve5' 		=> NULL,
					'd_approve5' 		=> NULL
				)
			);
		}
		$this->db->where('i_mkt', $i_mkt);
		$this->db->update('tm_marketing_tool');
    }

    function delete($imkt)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
		  $this->db->set(
			array(
				'f_cancel' 		=> 't'
			)
		);
		$this->db->where('i_mkt', $imkt);
		$this->db->update('tm_marketing_tool');
    }

	function deletenote($i_mkt,$dept,$user)
    {
		$this->db->query("DELETE FROM tm_marketing_tool_note WHERE i_user = '$user' and i_mkt = '$i_mkt' and i_dept = '$dept'");
		return TRUE;
    }

	function runningnumber()
	{
	  $th		= date('Y');
	  $thbln	= date('Ym');
	  $thbl 	= substr($thbln, 2, 2) . substr($thbln, 4, 2);
	  $iarea 	= '00';
	  $this->db->select(" n_modul_no as max from tm_dgu_no
							where i_modul='MKT'
							and substr(e_periode,1,4)='$th' for update", false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0) {
		foreach ($query->result() as $row) {
		  $terakhir = $row->max;
		}
		$imkt  = $terakhir + 1;
		$this->db->query(" update tm_dgu_no
							  set n_modul_no=$imkt
							  where i_modul='MKT'
							  and substr(e_periode,1,4)='$th'", false);
		settype($imkt, "string");
		$a = strlen($imkt);
		while ($a < 6) {
		  $imkt = "0" . $imkt;
		  $a = strlen($imkt);
		}
		$imkt  = "MKT-" . $thbl . "-" . $imkt;
		return $imkt;
	  } else {
		$imkt  = "000001";
		$imkt  = "MKT-" . $thbl . "-" . $imkt;
		$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
							 values ('MKT','$iarea','$th',1)");
		return $imkt;
	  }
	}

	
    public function deletetmp($user) 
    {
		$this->db->query("DELETE FROM tm_marketing_tool_tmp WHERE i_user = '$user'");
		return TRUE;
    }

	public function deletemkt($i_mkt) 
    {
		$this->db->query("DELETE FROM tm_marketing_tool WHERE i_mkt = '$i_mkt'");
		return TRUE;
    }
    
    function bacasadaya($cari, $num,$offset,$tahun)
    {
		$this->db->select(" a.*,
								b.e_loyal,
								c.nama,
								d.e_namamkt,
								e.e_customer_name,
								f.e_salesman_name,
								CASE
									WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
									WHEN a.i_mkt_type = 2 THEN 'Promo'
									WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
								END AS marketing_type,
								CASE
									WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
									WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
									WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
						END AS promotype2,
								case
								when f_reject = 't' then 'Reject'
								when f_cancel = 't' then 'Batal'
								when i_approve_level = '6' then 'Input Sales'
								when i_approve_level = '5' and f_mkt_approve != 't' then 'Approve AS'
								when i_approve_level = '5' and f_mkt_approve = 't' then 'Approve AS & Marketing'
								when i_approve_level = '4' and f_mkt_approve = 't' then 'Approve RSM'
								when i_mkt_type != '2' and i_approve_level = '3' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve GM'

								when i_mkt_type != '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve Direksi'

								when i_mkt_type != '2' and i_approve_level = '0' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Sdh Approve Direksi'

								when i_mkt_type != '2' and i_approve_level <= '3' and f_mkt_approve = 't' and v_biaya_realisasi > 0 then 'Selesai'
								when i_mkt_type = '2' and i_approve_level = '3' and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve GM'
								when i_mkt_type = '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve Direksi'

								when i_mkt_type = '2' and i_approve_level = '0' and f_mkt_approve = 't' and (i_promo is null or i_promo = '' and v_biaya_realisasi = 0) then 'Sdh Approve Direksi'

								when i_mkt_type = '2' and i_approve_level <= '3' and f_mkt_approve = 't' and (i_promo is not null or i_promo != '' or v_biaya_realisasi is not null or v_biaya_realisasi > 0) then 'Selesai'
								when i_approve_level isnull and f_mkt_approve = 'f' then 'Input Sales'
								else '#N/A'
								end as e_status
							FROM
								tm_marketing_tool a
							LEFT JOIN tr_mkttools_loyal b ON
								(a.i_loyalitas_type = b.i_loyal)
							LEFT JOIN tr_mkttools_promo c ON
								(trim(a.e_tanda_bukti) = trim(c.nama))
							LEFT JOIN tr_jns_mkttools d ON
								(a.e_jenis_market = d.i_jnsmkt)
							INNER JOIN tr_customer e ON
								(a.i_customer = e.i_customer)
							INNER JOIN tr_salesman f ON
								(a.i_salesman = f.i_salesman)	
							WHERE 
								to_char(a.d_mkt,'yyyy') = '$tahun'	
							ORDER BY
								a.i_mkt desc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }


	function bacaexport($tahun)
    {
		return $this->db->query("  SELECT a.*,
								b.e_loyal,
								c.nama,
								d.e_namamkt,
								e.e_customer_name,
								f.e_salesman_name,
								CASE
									WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
									WHEN a.i_mkt_type = 2 THEN 'Promo'
									WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
								END AS marketing_type,
								CASE
									WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
									WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
									WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
						END AS promotype2,
								case
								/* when v_biaya_realisasi > 0 then 'Selesai LOU' */
								when f_reject = 't' then 'Reject'
								when f_cancel = 't' then 'Batal'
								when i_approve_level = '6' then 'Input Sales'
								when i_approve_level = '5' and f_mkt_approve != 't' then 'Approve AS'
								when i_approve_level = '5' and f_mkt_approve = 't' then 'Approve AS & Marketing'
								when i_approve_level = '4' and f_mkt_approve = 't' then 'Approve RSM'
								when i_mkt_type != '2' and i_approve_level = '3' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve GM'

								when i_mkt_type != '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve Direksi'

								when i_mkt_type != '2' and i_approve_level = '0' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Sdh Approve Direksi'

								when i_mkt_type != '2' and i_approve_level <= '3' and f_mkt_approve = 't' and v_biaya_realisasi > 0 then 'Selesai'
								when i_mkt_type = '2' and i_approve_level = '3' and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve GM'
								when i_mkt_type = '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve Direksi'

								when i_mkt_type = '2' and i_approve_level = '0' and f_mkt_approve = 't' and (i_promo is null or i_promo = '' and v_biaya_realisasi = 0) then 'Sdh Approve Direksi'

								when i_mkt_type = '2' and i_approve_level <= '3' and f_mkt_approve = 't' and (i_promo is not null or i_promo != '' or v_biaya_realisasi is not null or v_biaya_realisasi > 0) then 'Selesai'
								when i_approve_level isnull and f_mkt_approve = 'f' then 'Input Sales'
								else '#N/A'
								end as e_status,
								to_char(d_entry,'yyyy-mm-dd') AS entry,
								a.d_approve - to_char(d_entry,'yyyy-mm-dd')::date as entrytoas, 
								a.d_approve2 - a.d_approve as astomkt,
								a.d_approve3-a.d_approve2 as mkttorsm,
								a.d_approve4-a.d_approve3 as rsmtogm,
								a.d_approve5-a.d_approve4 as gmtodir
							FROM
								tm_marketing_tool a
							LEFT JOIN tr_mkttools_loyal b ON
								(a.i_loyalitas_type = b.i_loyal)
							LEFT JOIN tr_mkttools_promo c ON
								(trim(a.e_tanda_bukti) = trim(c.nama))
							LEFT JOIN tr_jns_mkttools d ON
								(a.e_jenis_market = d.i_jnsmkt)
							INNER JOIN tr_customer e ON
								(a.i_customer = e.i_customer)
							INNER JOIN tr_salesman f ON
								(a.i_salesman = f.i_salesman)	
							WHERE 
								to_char(a.d_mkt,'yyyy') = '$tahun'	
							ORDER BY
								a.i_mkt desc", false);
		/* $query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		} */
    }


	function bacasadayana($cari, $num,$offset, $tahun)
    {
		$this->db->select(" a.*,
								b.e_loyal,
								c.nama,
								d.e_namamkt,
								e.e_customer_name,
								f.e_salesman_name,
								CASE
									WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
									WHEN a.i_mkt_type = 2 THEN 'Promo'
									WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
								END AS marketing_type,
								CASE
									WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
									WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
									WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
						END AS promotype2,
								case
								/* when v_biaya_realisasi > 0 then 'Selesai LOU' */
								when f_reject = 't' then 'Reject'
								when f_cancel = 't' then 'Batal'
								when i_approve_level = '6' then 'Input Sales'
								when i_approve_level = '5' and f_mkt_approve != 't' then 'Approve AS'
								when i_approve_level = '5' and f_mkt_approve = 't' then 'Approve AS & Marketing'
								when i_approve_level = '4' and f_mkt_approve = 't' then 'Approve RSM'
								when i_mkt_type != '2' and i_approve_level = '3' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve GM'
								when i_mkt_type != '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Approve Direksi'
								when i_mkt_type != '2' and i_approve_level <= '3' and f_mkt_approve = 't' and v_biaya_realisasi > 0 then 'Selesai'

								when i_mkt_type != '2' and i_approve_level = '0' and f_mkt_approve = 't' and v_biaya_realisasi <= 0 then 'Sdh Approve Direksi'

								when i_mkt_type = '2' and i_approve_level = '3' and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve GM'
								when i_mkt_type = '2' and (i_approve_level < '3' and i_approve_level > '0') and f_mkt_approve = 't' and (i_promo is null or i_promo = '') then 'Approve Direksi'

								when i_mkt_type = '2' and i_approve_level = '0' and f_mkt_approve = 't' and (i_promo is null or i_promo = ''  and v_biaya_realisasi = 0) then 'Sdh Approve Direksi'

								when i_mkt_type = '2' and i_approve_level <= '3' and f_mkt_approve = 't' and (i_promo is not null or i_promo != '' or v_biaya_realisasi is not null or v_biaya_realisasi >0) then 'Selesai'
								when i_approve_level isnull and f_mkt_approve = 'f' then 'Input Sales'
								else '#N/A'
								end as e_status
							FROM
								tm_marketing_tool a
							LEFT JOIN tr_mkttools_loyal b ON
								(a.i_loyalitas_type = b.i_loyal)
							LEFT JOIN tr_mkttools_promo c ON
								(trim(a.e_tanda_bukti) = trim(c.nama))
							LEFT JOIN tr_jns_mkttools d ON
								(a.e_jenis_market = d.i_jnsmkt)
							INNER JOIN tr_customer e ON
								(a.i_customer = e.i_customer)
							INNER JOIN tr_salesman f ON
								(a.i_salesman = f.i_salesman)
							WHERE 
								to_char(a.d_mkt,'yyyy') = '$tahun' 	
								and (upper(a.i_mkt) like '%$cari%'	
								or upper(e.i_customer) like '%$cari%'	
								or upper(e.e_customer_name) like '%$cari%'
								or upper(a.i_salesman) like '%$cari%'
								or upper(f.e_salesman_name) like '%$cari%')
							ORDER BY
								a.i_mkt desc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }


	function bacaanalisa($imkt)
    {
		$this->db->select("
							n_month,
							sum(v_netto_first) as v_netto_first,
							sum(v_netto_mid) as v_netto_mid,
							sum(v_netto_last) as v_netto_last,
							sum(v_netto_last) - sum(v_netto_mid) as v_growth
							from
							(
							with mkt as (select i_customer, d_mkt from tm_marketing_tool where i_mkt='$imkt')
							select
							date_part('year', d_nota) as n_year,
							to_char(d_nota, 'mm') as n_month,
							case
							when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
							else b.i_customer_group
							end as i_customer,
							sum(a.v_nota_netto) as v_netto_first,
							0 as v_netto_mid,
							0 as v_netto_last
							from mkt, tm_nota a
							inner join tr_customer b on(a.i_customer=b.i_customer)
							where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 2
							and a.i_customer=mkt.i_customer
							and a.f_nota_cancel='f'
							group by 1, 2, 3
							union all
							select
							date_part('year', d_nota) as n_year,
							to_char(d_nota, 'mm') as n_month,
							case
							when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
							else b.i_customer_group
							end as i_customer,
							0 as v_netto_first,
							sum(a.v_nota_netto) as v_netto_mid,
							0 as v_netto_last
							from mkt, tm_nota a
							inner join tr_customer b on(a.i_customer=b.i_customer)
							where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 1
							and a.i_customer=mkt.i_customer
							and a.f_nota_cancel='f'
							group by 1, 2, 3
							union all
							select
							date_part('year', d_nota) as n_year,
							to_char(d_nota, 'mm') as n_month,
							case
							when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
							else b.i_customer_group
							end as i_customer,
							0 as v_netto_first,
							0 as v_netto_mid,
							sum(a.v_nota_netto) as v_netto_last
							from mkt, tm_nota a
							inner join tr_customer b on(a.i_customer=b.i_customer)
							where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 0
							and a.i_customer=mkt.i_customer
							and a.f_nota_cancel='f'
							group by 1, 2, 3
							union all
							select
							date_part('year', d_spb) as n_year,
							to_char(d_spb, 'mm') as n_month,
							case
							when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
							else b.i_customer_group
							end as i_customer,
							0 as v_netto_first,
							0 as v_netto_mid,
							sum(a.v_spb-a.v_spb_discounttotal) as v_netto_last
							from mkt, tm_spb a
							inner join tr_customer b on(a.i_customer=b.i_customer)
							where date_part('year', d_spb) = date_part('year', mkt.d_mkt) - 0
							and a.i_customer=mkt.i_customer
							and a.i_sj is null
							and a.f_spb_cancel='f'
							group by 1, 2, 3
							union all
							select date_part('year', mkt.d_mkt) n_year, to_char(DATE '1000-01-01' + (interval '1' month * generate_series(0, 11)), 'MM') as n_month, mkt.i_customer, 0 as v1, 0 as v2, 0 as v3 from mkt
							) x1
							group by 1
							order by 1", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaedit($imkt)
    {
		$this->db->select(" 	a.*,
								b.e_loyal,
								c.nama,
								d.e_namamkt,
								e.e_customer_name,
								f.e_salesman_name,
								coalesce(a.v_biaya_eta,0) as biaya,
								CASE
									WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
									WHEN a.i_mkt_type = 2 THEN 'Promo'
									WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
								END AS marketing_type,
								CASE
									WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
									WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
									WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
								END AS promotype2,
								to_char(a.d_mkt,'yyyy') as tahun
							FROM
								tm_marketing_tool a
							LEFT JOIN tr_mkttools_loyal b ON
								(a.i_loyalitas_type = b.i_loyal)
							LEFT JOIN tr_mkttools_promo c ON
								(trim(a.e_tanda_bukti) = trim(c.nama))
							LEFT JOIN tr_jns_mkttools d ON
								(a.e_jenis_market = d.i_jnsmkt)
							INNER JOIN tr_customer e ON
								(a.i_customer = e.i_customer)
							INNER JOIN tr_salesman f ON
								(a.i_salesman = f.i_salesman)
							WHERE a.i_mkt = '$imkt'	
							ORDER BY
								a.i_mkt_type", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

	function bacanote2($imkt)
    {
		$this->db->select("  	a.*,
								b.e_level,
								c.e_user_name
							from
								tm_marketing_tool_note a
							inner join tr_level b on
								(a.i_level = b.i_level)
							inner join tm_user c on
								(a.i_user = c.i_user)	
							WHERE 
								i_mkt = '$imkt'	
							ORDER BY
								a.i_dept", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }


	function bacapromo($num, $offset,$icustomer,$tahun)
	{
		$this->db->select("
								a.i_promo,
								a.e_promo_name,
								COALESCE(sum(c.v_nota_netto),0) AS total
							FROM
								tm_promo a
							INNER JOIN tm_spb b ON
								(a.i_promo = b.i_spb_program)
							INNER JOIN tm_nota c ON
								(b.i_sj = c.i_sj
									AND b.i_area = c.i_area AND c.i_customer = '$icustomer' AND c.f_nota_cancel = 'f')
							where to_char(a.d_promo,'yyyy') = '$tahun'		
							GROUP BY 1,2 order by i_promo", false)->limit($num, $offset);
		$query = $this->db->get();
		/* echo $this->db->last_query(); */
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

    function bacaheader($imkt)
    {
		$this->db->select(" 
								a.*,
								b.e_salesman_name,
								c.e_customer_name,
								c.i_area
							FROM
								tm_marketing_tool a
							INNER JOIN tr_salesman b ON
								(a.i_salesman = b.i_salesman)
							INNER JOIN tr_customer c ON
								(a.i_customer = c.i_customer)	
							WHERE
								i_mkt = '$imkt';", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    
    function cari($cari,$num,$offset)
    {
     $this->db->select("  * from tr_product_price 
					                where (upper(tr_product_price.i_product) like '%$cari%' 
					                or upper(tr_product_price.e_product_name) = '%$cari%')
                          order by i_product, i_price_group", false)->limit($num,$offset);

/*		$this->db->select(" * from tr_product_price 
					              LEFT JOIN tr_product ON (tr_product_price.i_product=tr_product.i_product)
					              LEFT JOIN tr_product_grade 
					                 ON (tr_product_price.i_product_grade=tr_product_grade.i_product_grade)
					              where upper(tr_product_price.i_product) like '%$cari%' 
					                 or upper(tr_product_price.e_product_name) = '%$cari%'
					                 order by tr_product_price.i_product, tr_product_price.i_price_group
				                ", false)->limit($num,$offset);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacasalesman($num,$offset)
    {
		$this->db->select("i_salesman, e_salesman_name from tr_salesman where f_salesman_aktif = 't' order by i_salesman ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function carisalesman($cari,$num,$offset,$salesman,$periode)
    {
	$this->db->select("
						DISTINCT
						b.i_salesman,
						b.e_salesman_name
						FROM
							tr_customer a
						INNER JOIN tr_customer_salesman b ON
							(a.i_customer = b.i_customer)
						WHERE
							b.e_periode = '$periode'/*  AND i_salesman = '$salesman' */
						AND upper(b.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%'", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricustomer($cari,$num,$offset,$salesman,$periode)
    {
	$this->db->select("	
					DISTINCT a.i_customer,
					a.e_customer_name,
					a.i_area
				FROM
					tr_customer a
				INNER JOIN tr_customer_salesman b ON
					(a.i_customer = b.i_customer)
				WHERE
					b.e_periode = '$periode' AND i_salesman = '$salesman'
				AND upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%'", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function caripromo($cari,$num,$offset,$icustomer)
    {
	$this->db->select("	
							a.i_promo,
							a.e_promo_name,
							COALESCE(sum(c.v_nota_netto),0) AS total
						FROM
							tm_promo a
						INNER JOIN tm_spb b ON
							(a.i_promo = b.i_spb_program)
						INNER JOIN tm_nota c ON
							(b.i_sj = c.i_sj
								AND b.i_area = c.i_area AND c.i_customer = '$icustomer' AND c.f_nota_cancel = 'f')
						WHERE 
							upper(a.i_promo) like '%$cari%' 
							or upper(a.e_promo_name) like '%$cari%'		
						GROUP BY 1,2 order by i_promo", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	

    function bacaloyal($num,$offset)
    {
		$this->db->select("i_loyal, e_loyal from tr_mkttools_loyal order by i_loyal ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacamkttools($num,$offset)
    {
		$this->db->select("i_jnsmkt, e_namamkt from tr_jns_mkttools order by i_jnsmkt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacamktpromo($num,$offset)
    {
		$this->db->select(" * from tr_mkttools_promo order by i_kode ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacacustomer($cari,$num,$offset,$periode,$salesman)
    {
		$this->db->select("	DISTINCT a.i_customer,
								a.e_customer_name,
								a.i_area
							FROM
								tr_customer a
							INNER JOIN tr_customer_salesman b ON
								(a.i_customer = b.i_customer)
							WHERE
								b.e_periode = '$periode' AND i_salesman = '$salesman'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		// echo $this->db->last_query();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproduct($cari,$num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product where upper(e_product_name) like '%$cari%' or upper(i_product) like '%$cari%' order by i_product", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	/* bagian print */

	function bacaprint($imkt){
		$query = $this->db->select("a.*, b.e_salesman_name, c.e_customer_name, c.i_area, c.e_customer_address,
			d.e_city_name, e.e_customer_classname, c.n_customer_toplength, c.i_price_group,
			coalesce(n_customer_discount1, 0) as n_customer_discount1,
			coalesce(n_customer_discount2, 0) as n_customer_discount2,
			coalesce(n_customer_discount3, 0) as n_customer_discount3/* , */
			/* g.v_target,  w.e_pic_name*/
			from tm_marketing_tool a
			inner join tr_salesman b on (a.i_salesman=b.i_salesman)
			inner join tr_customer c on (a.i_customer=c.i_customer)
			inner join tr_city d on (c.i_city=d.i_city and c.i_area=d.i_area)
			inner join tr_customer_class e on (c.i_customer_class=e.i_customer_class)
			inner join tr_customer_discount f on (a.i_customer=f.i_customer)
			/* left join tm_target_itemcustomer g on(a.i_customer=g.i_customer and to_char(a.d_mkt,'yyyymm')=g.i_periode) */
			left join tr_customer_area cr on(a.i_customer=cr.i_customer)
			/* left join tr_kp kp on(cr.i_area_kp=kp.i_kp) 
			left join tr_wilayah w on(kp.i_wilayah=w.i_wilayah)*/
			where a.i_mkt like '$imkt'", false)
		->get();

		return ($query->num_rows() > 0) ? $query->row() : 'zero';
	}

	function getyear($imkt){
		$query = $this->db->query("select date_part('year', d_mkt) - 2 as year1, date_part('year', d_mkt) - 1 as year2, date_part('year', d_mkt) as year3
			from tm_marketing_tool
			where i_mkt = '$imkt'");
		return ($query->num_rows() > 0) ? $query->row() : 'zero';
	}

	function nettolast3y($imkt){
		$query = $this->db->query("select
			n_month,
			sum(v_netto_first) as v_netto_first,
			sum(v_netto_mid) as v_netto_mid,
			sum(v_netto_last) as v_netto_last,
			sum(v_netto_last) - sum(v_netto_mid) as v_growth
			from
			(
			with mkt as (select i_customer, d_mkt from tm_marketing_tool where i_mkt='$imkt')
			select
			date_part('year', d_nota) as n_year,
			to_char(d_nota, 'mm') as n_month,
			case
			when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
			else b.i_customer_group
			end as i_customer,
			sum(a.v_nota_netto) as v_netto_first,
			0 as v_netto_mid,
			0 as v_netto_last
			from mkt, tm_nota a
			inner join tr_customer b on(a.i_customer=b.i_customer)
			where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 2
			and a.i_customer=mkt.i_customer
			and a.f_nota_cancel='f'
			group by 1, 2, 3
			union all
			select
			date_part('year', d_nota) as n_year,
			to_char(d_nota, 'mm') as n_month,
			case
			when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
			else b.i_customer_group
			end as i_customer,
			0 as v_netto_first,
			sum(a.v_nota_netto) as v_netto_mid,
			0 as v_netto_last
			from mkt, tm_nota a
			inner join tr_customer b on(a.i_customer=b.i_customer)
			where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 1
			and a.i_customer=mkt.i_customer
			and a.f_nota_cancel='f'
			group by 1, 2, 3
			union all
			select
			date_part('year', d_nota) as n_year,
			to_char(d_nota, 'mm') as n_month,
			case
			when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
			else b.i_customer_group
			end as i_customer,
			0 as v_netto_first,
			0 as v_netto_mid,
			sum(a.v_nota_netto) as v_netto_last
			from mkt, tm_nota a
			inner join tr_customer b on(a.i_customer=b.i_customer)
			where date_part('year', d_nota) = date_part('year', mkt.d_mkt) - 0
			and a.i_customer=mkt.i_customer
			and a.f_nota_cancel='f'
			group by 1, 2, 3
			union all
			select
			date_part('year', d_spb) as n_year,
			to_char(d_spb, 'mm') as n_month,
			case
			when b.i_customer_group = 'G0000' or trim(b.i_customer_group) = '' then b.i_customer
			else b.i_customer_group
			end as i_customer,
			0 as v_netto_first,
			0 as v_netto_mid,
			sum(a.v_spb-a.v_spb_discounttotal) as v_netto_last
			from mkt, tm_spb a
			inner join tr_customer b on(a.i_customer=b.i_customer)
			where date_part('year', d_spb) = date_part('year', mkt.d_mkt) - 0
			and a.i_customer=mkt.i_customer
			and a.i_sj is null
			and a.f_spb_cancel='f'
			group by 1, 2, 3
			union all
			select date_part('year', mkt.d_mkt) n_year, to_char(DATE '1000-01-01' + (interval '1' month * generate_series(0, 11)), 'MM') as n_month, mkt.i_customer, 0 as v1, 0 as v2, 0 as v3 from mkt
			) x1
			group by 1
			order by 1");
		return ($query->num_rows() > 0) ? $query->result() : 'zero';
	}

	function targetlast3y($imkt){
		$query = $this->db->query("select x1.i_customer, json_agg(x1.n_year order by x1.n_year) as n_year_arr, json_agg(v_target order by x1.n_year) as v_target_arr from (
			select a.*, sum(coalesce(/* b.v_target, */ 0)) v_target from (
			select a.i_customer, y.n_year
			from tm_marketing_tool a
			cross join(
			with t as (
			select to_char(d_mkt - interval '2 years', 'yyyymm') as year1, to_char(d_mkt,'yyyymm') as year2
			from tm_marketing_tool
			where i_mkt='$imkt'
			)
			select to_char(a,'yyyy') as n_year
			from t, generate_series(to_date(t.year1, 'yyyymm'), to_date(t.year2, 'yyyymm'), interval '1 year') as a
			) as y
			where a.i_mkt like '$imkt'
			) a
			/* left join tm_target_itemcustomer b on(a.i_customer=b.i_customer and a.n_year=substring(b.i_periode,1,4)) */
			group by 1, 2
			) x1
			group by 1");
		return ($query->num_rows() > 0) ? json_decode($query->row()->v_target_arr) : json_decode('[0,0,0]');
	}

	function biayath($imkt){
		$query = $this->db->query("select n_month, sum(v_biaya) as v_biaya, sum(v_biaya_realisasi) as v_biaya_realisasi from (

			with mkt as (select i_mkt, i_customer, d_mkt from tm_marketing_tool where i_mkt='$imkt')
			select to_char(a.d_mkt,'mm') n_month,
			a.v_biaya_eta as v_biaya,
			v_biaya_realisasi
			from mkt, tm_marketing_tool a
			where mkt.i_customer=a.i_customer
			and split_part(a.i_mkt,'-',2) = split_part(mkt.i_mkt,'-',2)
			and a.i_mkt_type != 2

			union

			select
			to_char(a.d_spb, 'mm') as n_month,
			0 as v_biaya,
			sum(a.vdis1 + a.vdis2 /* + a.vdis3 */) as v_biaya_realisasi
			from mkt, v_promo a
			where mkt.i_customer=a.i_customer
			and date_part('year',a.d_spb) = date_part('year',mkt.d_mkt)
			and (a.vdis1 + a.vdis2 /* + a.vdis3 */) > 0
			group by 1
			order by 1

			) as x1
			group by 1");
		
		return ($query->num_rows() > 0) ? $query->result() : 'zero';
	}

	function targetmlast3y($imkt){
		$query = $this->db->query("select x1.i_customer, json_agg(x1.n_year order by x1.n_year) as n_year_arr, json_agg(v_target order by x1.n_year) as v_target_arr from (
			select a.*, sum(coalesce(/* b.v_target,  */0)) v_target from (
			select a.i_customer, y.n_year
			from tm_marketing_tool a
			cross join(
			with t as (
			select to_char(d_mkt - interval '2 years', 'yyyymm') as year1, to_char(d_mkt,'yyyymm') as year2
			from tm_marketing_tool
			where i_mkt='$imkt'
			)
			select to_char(a,'yyyymm') as n_year
			from t, generate_series(to_date(t.year1, 'yyyymm'), to_date(t.year2, 'yyyymm'), interval '1 year') as a
			) as y
			where a.i_mkt like '$imkt'
			) a
			/* left join tm_target_itemcustomer b on(a.i_customer=b.i_customer and a.n_year=b.i_periode) */
			group by 1, 2
			) x1
			group by 1");
		return ($query->num_rows() > 0) ? json_decode($query->row()->v_target_arr) : json_decode('[0,0,0]');
	}

	function history($imkt){
		$query = $this->db->query("with mkt as (select i_mkt, i_customer, d_mkt from tm_marketing_tool where i_mkt='$imkt')
			select a.d_mkt, a.i_mkt as i_ref, a.i_customer, a.e_tool_remark, to_char(a.d_mkt,'mm') n_month,
			case
			when a.v_biaya_realisasi > 0 then a.v_biaya_realisasi
			when a.v_biaya_eta > 0 then a.v_biaya_eta
			else 0
			end as v_biaya
			from mkt, tm_marketing_tool a
			where a.i_customer=mkt.i_customer
			and split_part(a.i_mkt,'-',2) = split_part(mkt.i_mkt,'-',2)
			and ((a.i_mkt_type::int = 2 and a.i_promo_type2::int = 2) or (a.i_mkt_type::int != 2))
			and a.f_cancel='f'
			union
			select
			a.d_spb,
			a.i_spb_program,
			a.i_customer,
			concat(a.i_spb_program, ' - ', substring(split_part(a.e_promo_name, '||', 1), 0, 50)) as e_remark,
			to_char(a.d_spb, 'mm') as n_month,
			sum(a.vdis1 + a.vdis2 /* + a.vdis3 */) as v_biaya
			from mkt, v_promo a
			where date_part('year',a.d_spb) = date_part('year',mkt.d_mkt)
			and a.i_customer=mkt.i_customer
			and (a.vdis1 + a.vdis2 /* + a.vdis3 */) > 0
			group by 1, 2, 3, 4
			order by 1, 2");
		return ($query->num_rows() > 0) ? $query->result() : 'zero';
	}

}
?>
