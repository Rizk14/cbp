<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibapb, $iarea) 
    {
			$this->db->query("update tm_bapbsjp set f_bapb_cancel='t' WHERE i_bapb='$ibapb' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_bapbsjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_bapb) like '%$cari%')
							order by a.i_bapb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_bapbsjp a, tr_area b
					where a.i_area=b.i_area 
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_bapb) like '%$cari%')
					order by a.i_bapb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_bapbsjp a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_bapb) like '%$cari%')
							          and a.i_area='$iarea' and
							          a.d_bapb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bapb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bapb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_bapbsjp a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_bapb) like '%$cari%')
							          and a.i_area='$iarea' and
							          a.d_bapb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bapb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bapb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
