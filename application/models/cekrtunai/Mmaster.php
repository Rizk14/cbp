<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($irtunai,$iarea) 
    {
			$this->db->query(" update tm_rtunai set f_rtunai_cancel='t', d_update=now() WHERE i_rtunai='$irtunai' and i_area='$iarea' ");
##########
      $this->db->select("* from tm_rtunai_item WHERE i_rtunai='$irtunai' and i_area='$iarea' ");
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $itunai=$row->i_tunai;
			    $area=$row->i_area_tunai;
      		$this->db->query(" update tm_tunai set i_rtunai=null, i_area_rtunai=null, d_update=now() 
      		                   WHERE i_tunai='$itunai' and i_area='$area' ");
			  }
		  }
##########
    }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {				
			$teksquery="a.i_area as iarea, a.e_remark as eremark, a.f_rtunai_cancel as frtunaicancel,
                  a.i_rtunai as irtunai, a.v_jumlah as vjumlah, a.d_rtunai as drtunai, a.i_cek as cek, to_char(a.d_cek,'dd-mm-yyyy') as dcek
                  from tm_rtunai a
                  left join tr_area d on(a.i_area=d.i_area)
					        where (upper(a.i_rtunai) like '%$cari%' ";
					        if(!is_numeric($cari)){
						        $teksquery=$teksquery.")";
					        }else{
						        $teksquery=$teksquery."or a.v_jumlah=$cari)";

					        }
					        $teksquery=$teksquery." and a.f_rtunai_cancel='f'
                  and a.i_area='$iarea'
                  and(a.d_rtunai >= to_date('$dfrom','dd-mm-yyyy')
                  and a.d_rtunai <= to_date('$dto','dd-mm-yyyy'))
                  ORDER BY irtunai ";
			$this->db->select($teksquery,false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();

			}
    }
    function baca($iarea, $irtunai)
    {
		  $this->db->select("	a.d_rtunai, a.i_rtunai, d.e_area_name, a.v_jumlah, a.i_area, a.e_remark, a.i_cek, a.e_cek
		        from tm_rtunai a
					  left join tr_area d on(a.i_area=d.i_area)
					  where a.i_rtunai='$irtunai' and a.i_area='$iarea'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($iarea, $irtunai)
    {
		  $this->db->select("	a.*, b.e_area_name, to_char(c.d_tunai,'dd-mm-yyyy') as d_tunai, c.i_customer, c.e_remark, d.e_customer_name
		        from tm_rtunai_item a
					  left join tr_area b on(a.i_area_tunai=b.i_area),
					  tm_tunai c, tr_customer d
					  where a.i_rtunai='$irtunai' and a.i_area='$iarea' and c.i_customer=d.i_customer
					  and a.i_tunai=c.i_tunai and a.i_area_tunai=c.i_area
					  order by a.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function cek($ecek,$user,$irtunai,$iarea)
  	{
	    $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
	    $row   	= $query->row();
	    $dentry	= $row->c;
      		$data = array(
				    'e_cek'		=> $ecek,
				    'd_cek'		=> $dentry,
				    'i_cek'		=> $user
      				 );
     	$this->db->where('i_rtunai', $irtunai);
	    $this->db->where('i_area', $iarea);
	    $this->db->update('tm_rtunai', $data);
  	}
}
?>
