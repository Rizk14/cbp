<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
  {
    parent::__construct();
	#$this->CI =& get_instance();
  }
  function baca($istorelocation,$iperiode,$istore,$num,$offset,$cari)
  {
	$this->db->select("	a.*, b.e_product_name, c.v_product_retail from tm_mutasi a, tr_product b, tr_product_price c
		                where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_product=c.i_product and c.i_price_group='00'
      					    and i_store='$istore' and i_store_location='$istorelocation' order by b.e_product_name ",false);#->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }
  function bacaexcel($iperiode,$istore,$cari)
  {
	$this->db->select("	a.*, b.e_product_name, c.v_product_retail from tm_mutasi a, tr_product b, tr_product_price c
		                where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and a.i_product=c.i_product and c.i_price_group='00'
			            and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
	$query = $this->db->get();
    return $query;
  }
  function proses($iperiode,$istore,$so,$istorelocation)
  {
    if($istore=='AA')
    {
      $iarea='00';
    }else{
      $iarea=$istore;
    }
    $store=$istore;
    $this->db->select(" n_modul_no as max from tm_dgu_no 
                        where i_modul='MTS'
                        and i_area='$istore' for update", false); #and i_store_location='$istorelocation' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0)
    {
      $this->db->query("update tm_dgu_no 
                        set e_periode='$iperiode'
                        where i_modul='MTS' and i_area='$istore' and i_store_location='$istorelocation'", false);
    }else{
      $this->db->query("insert into tm_dgu_no(i_modul, i_area, e_periode, i_store_location) 
                        values ('MTS','$istore','$iperiode', '$istorelocation')");
    }
    $soAkhir=$so;
    $query = $this->db->query(" select * from tm_mutasi_header where i_store='$store' and i_store_location='$istorelocation' 
                                and e_mutasi_periode='$iperiode'");
    $st=$query->row();
    if($query->num_rows()>0)
    {
      $soAwal=$st->i_stockopname_awal;
    }else{
      $soAwal='';
    }
#    if($store=='AA') $loc='01'; else $loc='00';
    $loc=$istorelocation;
    $this->db->query(" delete from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_store_location='$loc'");

    $kuerys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_header
                               where i_store='$store' and e_mutasi_periode='$iperiode' and i_store_location='$loc'",false);
    if($kuerys->num_rows()>0)
    {
      $row   	= $kuerys->row();
      if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
      {
        $this->db->query(" update tm_ic set n_quantity_stock=0 where i_store='$store' and i_store_location='$loc'");
      }else{
        $kuerys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
        $row   	= $kuerys->row();
        if($row->c==$iperiode)
        {
          $this->db->query(" update tm_ic set n_quantity_stock=0 where i_store='$store' and i_store_location='$loc'");
        }
      }
    }
#    $query = $this->db->query(" select distinct i_product from tm_stockopname_item where i_store='$store' and i_stockopname='$soAwal'");
    $query = $this->db->query(" select distinct i_product from tm_stockopname_item 
                                where i_store='$store' and (i_stockopname='$soAwal' or i_stockopname='$soAkhir') and i_store_location='$loc'");
    if ($query->num_rows() > 0)
    {
      $emutasiperiode=$iperiode;
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
      }
      $quer=$this->db->query(" select * from tm_mutasi_header
                               where i_store='$store' and e_mutasi_periode='$emutasiperiode' and i_store_location='$loc'",false);
      if($quer->num_rows()>0)
      {
        $que=$this->db->query(" UPDATE tm_mutasi_header
                                set i_stockopname_akhir='$soAkhir'
                                where i_store='$store' and e_mutasi_periode='$emutasiperiode' and i_store_location='$loc'",false);
      }else{
        $que=$this->db->query("insert into tm_mutasi_header values
                              ('$store','$emutasiperiode',null,'$soAkhir','$loc')",false);
      }
      $quer=$this->db->query("select * from tm_mutasi_header
                              where i_store='$store' and e_mutasi_periode='$perdpn' and i_store_location='$loc'",false);
      if($quer->num_rows()>0)
      {
        $que=$this->db->query(" UPDATE tm_mutasi_header
                                set i_stockopname_awal='$soAkhir'
                                where i_store='$store' and e_mutasi_periode='$perdpn' and i_store_location='$loc'",false);
      }else{
        $que=$this->db->query(" insert into tm_mutasi_header values
                              ('$store','$perdpn','$soAkhir',null,'$loc')",false);
      }
  		foreach($query->result() as $gie)
      {
        if($store!='PB'){
          $que = $this->db->query("select	area,periode,product, penjualan, pembelian, bbm, bbk, retur, git, pesan, ketoko, daritoko 
                                   from vmutasi where periode='$iperiode' and (area='$iarea' or area='$store') and loc='$loc' and 
                                   product='$gie->i_product'");
        }else{
          $que = $this->db->query("select	area,periode,product, penjualan, pembelian, bbm, bbk, retur, git, pesan, ketoko, daritoko 
                                   from vmutasix where periode='$iperiode' and (area='$iarea' or area='$store') and loc='$loc' and
                                   product='$gie->i_product'");
        }
		    if ($que->num_rows() > 0)
        {
			    foreach($que->result() as $vie)
          {
            if($vie->pembelian==null)$vie->pembelian=0;
            if($vie->retur==null)$vie->retur=0;
            if($vie->bbm==null)$vie->bbm=0;
            if($vie->penjualan==null)$vie->penjualan=0;
            if($vie->bbk==null)$vie->bbk=0;
            if($vie->git==null)$vie->git=0;
            if($vie->ketoko==null)$vie->ketoko=0;
            if($vie->daritoko==null)$vie->daritoko=0;
            $blawal=substr($iperiode,4,2)-1;
            if($blawal==0)
            {
              $perawal=substr($iperiode,0,4)-1;
              $perawal=$perawal.'12';
            }else{
              $perawal=substr($iperiode,0,4);
              $perawal=$perawal.substr($iperiode,4,2)-1;;
            }
#            if($vie->product=='DGG4215' and $iarea=='PB'){
#              $quer=$this->db->query("xselect n_stockopname from tm_stockopname_item
#                                      where i_stockopname='$soAwal' and i_area='$iarea' 
#                                      and i_product='$vie->product' and i_store_location='$loc'");
#            }else{
              $quer=$this->db->query("select n_stockopname from tm_stockopname_item
                                      where i_stockopname='$soAwal' and i_area='$iarea' 
                                      and i_product='$vie->product' and i_store_location='$loc'");
#            }
            if ($quer->num_rows() > 0)
            {
              foreach($quer->result() as $ni)
              {
                $sawal=$ni->n_stockopname;
              }
              $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
              if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
#              if($store=='AA') $loc='01'; else $loc='00';
              $loc=$istorelocation;
              $opname=0;
              $qur=$this->db->query("select n_stockopname from tm_stockopname_item 
                                     where i_stockopname='$soAkhir' and i_area='$iarea' and i_store_location='$loc' and i_product='$vie->product'");
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_stockopname;
                }
              }else{
                $opname==0;
              }
              $cek=$this->db->query("select i_product from tm_mutasi 
                                     where e_mutasi_periode='$iperiode' and i_store='$store' and i_store_location='$loc' and i_product='$vie->product'");
              if ($cek->num_rows() == 0)
              {
			          $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
	                                i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
	                                n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
	                                n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
	                                values
	                                ('$iperiode','$store','$vie->product','$grade','00',
                                     '$loc','00',$sawal,
	                                $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
              }
            }else{
              $sawal=0;
              $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
              if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
#              if($store=='AA') $loc='01'; else $loc='00';
              $loc=$istorelocation;
              $opname=0;
#              if($vie->product=='DGG4215' and $iarea=='PB'){
#                $qur=$this->db->query(" xselect n_stockopname from tm_stockopname_item 
#                                        where i_stockopname='$soAkhir' and i_area='$iarea' and i_store_location='$loc' 
#                                        and i_product='$vie->product'");
#              }else{
                $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where i_stockopname='$soAkhir' and i_area='$iarea' and i_store_location='$loc' 
                                        and i_product='$vie->product'");
#              }
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_stockopname;
                }
              }else{
                $opname==0;
              }
#              if($vie->product=='DGG4215' and $store=='PB'){
#                $cek=$this->db->query(" xselect i_product from tm_mutasi 
#                                        where e_mutasi_periode='$iperiode' and i_store='$store' and i_store_location='$loc' 
#                                        and i_product='$vie->product'");
#              }else{
                $cek=$this->db->query(" select i_product from tm_mutasi 
                                        where e_mutasi_periode='$iperiode' and i_store='$store' and i_store_location='$loc' 
                                        and i_product='$vie->product'");
#              }
              if ($cek->num_rows() == 0)
              {
#                if($vie->product=='DGG4215' and $store=='PB'){
#                  $this->db->query("sinsert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
#                                    i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
#                                    n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
#                                    n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
#                                    values
#                                    ('$iperiode','$store','$vie->product','$grade','00',
#                                     '$loc','00',$sawal,
#                                      $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,$vie->git,
#                                      $vie->ketoko,$vie->daritoko)");
#                }else{
                  $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                    i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                    n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                    n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
                                    values
                                    ('$iperiode','$store','$vie->product','$grade','00',
                                     '$loc','00',$sawal,
                                      $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,$vie->git,
                                      $vie->ketoko,$vie->daritoko)");
#                }
              }
            }
            $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                    from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_store_location='$loc' and i_product='$vie->product'");
            if ($qu->num_rows() > 0)
            {
              foreach($qu->result() as $vei)
              {
                $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_header
                                         where i_store='$store' and i_store_location='$loc' and e_mutasi_periode='$iperiode'",false);
                if($querys->num_rows()>0)
                {
                  $row   	= $querys->row();
                  if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
                  {
                    $queryz = $this->db->query("SELECT i_product from tm_ic
                                            where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                            and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                    if($queryz->num_rows>0){
/*
                      if($vei->i_product=='DGG4111'){
                        $this->db->query("update tm_ic set 
                                          n_quantity_stock=$vei->n_saldo_akhir
                                          where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                          and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                      }else{
*/
                        $this->db->query("update tm_ic set 
                                          n_quantity_stock=$vei->n_saldo_akhir
                                          where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                          and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
#                      }
                    }else{
                      $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                                        i_store_location, i_store_locationbin,f_product_active) 
                                        values
                                        ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                                        '$store','$loc','00','t')");
                    }
                  }else{
                    $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                    $row   	= $querys->row();
                    if($row->c==$iperiode)
                    {
                      $queryz = $this->db->query("SELECT i_product from tm_ic
                                              where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                      if($queryz->num_rows>0){
                        $this->db->query("update tm_ic set 
                                          n_quantity_stock=$vei->n_saldo_akhir
                                          where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                          and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                      }else{
                        $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                                          i_store_location, i_store_locationbin,f_product_active) 
                                          values
                                          ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                                          '$store','$loc','00','t')");
                      }
                    }
                  }
                }
              }
            }
          }
        }else{
          $blawal=substr($iperiode,4,2)-1;
          if($blawal==0)
          {
            $perawal=substr($iperiode,0,4)-1;
            $perawal=$perawal.'12';
          }else{
            $perawal=substr($iperiode,0,4);
            $perawal=$perawal.substr($iperiode,4,2)-1;;
          }
          $quer=$this->db->query("select n_stockopname from tm_stockopname_item 
                                  where i_stockopname='$soAwal' and i_area='$iarea' and i_product='$gie->i_product' and i_store_location='$loc'");

          if ($quer->num_rows() > 0)
          {
            foreach($quer->result() as $ni)
            {
              $stock=$ni->n_stockopname;
            }
            if($stock>0)
            {
              $sawal=$stock;
              $akhir=$sawal;
              if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
#              if($store=='AA') $loc='01'; else $loc='00';
              $loc=$istorelocation;
              $opname=0;
              $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                   where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$gie->i_product' and i_store_location='$loc'");
              if ($qur->num_rows() > 0)
              {
                foreach($qur->result() as $nu)
                {
                  $opname=$nu->n_stockopname;
                }
              }else{
                $opname==0;
              }
              $cek=$this->db->query("select i_product from tm_mutasi 
                                     where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$gie->i_product' and i_store_location='$loc'");
              if ($cek->num_rows() == 0)
              {
                $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                  i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                  n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                  n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
                                  values
                                  ('$iperiode','$store','$gie->i_product','$grade','00',
                                   '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname,0,0,0)");
              }
            }
          }else{
            $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                     where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$gie->i_product' and i_store_location='$loc'");
            if ($quer->num_rows() > 0)
            {
              foreach($quer->result() as $ni)
              {
                $opname=$ni->n_stockopname;
              }
#              if($stock>0)
              if($opname>0)
              {
                if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
                #if($store=='AA') $loc='01'; else $loc='00';
                $loc=$istorelocation;
                $cek=$this->db->query("select i_product from tm_mutasi 
                                       where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$gie->i_product' and i_store_location='$loc'");
                if ($cek->num_rows() == 0)
                {
                  $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                           i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                           n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                           n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,
                                                           n_mutasi_ketoko,n_mutasi_daritoko)
                                                           values
                                                          ('$iperiode','$store','$gie->i_product','$grade','00',
                                                           '$loc','00',0,0,0,0,0,0,0,0,$opname,0,0,0)");
                }
              }
            }
          }
        }
#####Tambahan
        $qu = $this->db->query("select * from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$gie->i_product' and i_store_location='$loc'");
        if ($qu->num_rows() > 0)
        {
          foreach($qu->result() as $vei)
          {
            $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_header
                                       where i_store='$store' and e_mutasi_periode='$iperiode' and i_store_location='$loc'",false);
            if($querys->num_rows()>0)
            {
              $row   	= $querys->row();
              if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
              {
                $queryz = $this->db->query("SELECT i_product from tm_ic
                                            where i_store='$store' and i_product='$gie->i_product' and i_store_location='$loc'
                                            and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                if($queryz->num_rows>0){
                  $this->db->query("update tm_ic set n_quantity_stock=$vei->n_saldo_akhir where i_store='$store' and i_product='$gie->i_product'
                                    and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade' and i_store_location='$loc'");
                }else{
                  $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                  i_store_location, i_store_locationbin,f_product_active) 
                  values
                  ($vei->n_saldo_akhir,'$gie->i_product','$vei->i_product_motif','$vei->i_product_grade',
                  '$store','$loc','00','t')");
                }
              }else{
                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row   	= $querys->row();
                if($row->c==$iperiode)
                {
                  $queryz = $this->db->query("SELECT i_product from tm_ic
                                              where i_store='$store' and i_product='$gie->i_product' and i_store_location='$loc'
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  if($queryz->num_rows>0){
                    $this->db->query("update tm_ic set n_quantity_stock=$vei->n_saldo_akhir where i_store='$store' and i_product='$gie->i_product'
                                      and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade' and i_store_location='$loc'");
                  }else{
                    $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                    i_store_location, i_store_locationbin,f_product_active) 
                    values
                    ($vei->n_saldo_akhir,'$gie->i_product','$vei->i_product_motif','$vei->i_product_grade',
                    '$store','$loc','00','t')");
                  }
                }
              }
            }
          }
        }
#####End Tambahan
      }
    }else{

    }
    if($store!='PB'){
      $que = $this->db->query(" select area,periode,product, penjualan, pembelian, bbm, bbk, retur, git, ketoko, daritoko
                                from vmutasi where periode='$iperiode' and (area='$iarea' or area='$store') and loc='$loc' 
                                and product not in(select i_product from tm_stockopname_item where i_store='$store' and i_store_location='$loc' and i_stockopname='$soAwal')");
    }else{
      $que = $this->db->query(" select area,periode,product, penjualan, pembelian, bbm, bbk, retur, git, ketoko, daritoko
                                  from vmutasix where periode='$iperiode' and (area='$iarea' or area='$store') and loc='$loc' 
                                  and product not in(select i_product from tm_stockopname_item where i_store='$store' and i_store_location='$loc' and i_stockopname='$soAwal')");

    }
    if ($que->num_rows() > 0)
    {
      foreach($que->result() as $vie)
      {
        if($vie->pembelian==null)$vie->pembelian=0;
        if($vie->retur==null)$vie->retur=0;
        if($vie->bbm==null)$vie->bbm=0;
        if($vie->penjualan==null)$vie->penjualan=0;
        if($vie->bbk==null)$vie->bbk=0;
        if($vie->git==null)$vie->git=0;
        if($vie->ketoko==null)$vie->ketoko=0;
        if($vie->daritoko==null)$vie->daritoko=0;
        $blawal=substr($iperiode,4,2)-1;
        if($blawal==0)
        {
          $perawal=substr($iperiode,0,4)-1;
          $perawal=$perawal.'12';
        }else{
          $perawal=substr($iperiode,0,4);
          $perawal=$perawal.substr($iperiode,4,2)-1;;
        }
        $quer=$this->db->query("select * from tm_stockopname_item 
                                where i_stockopname='$soAwal' and i_area='$iarea' and i_product='$vie->product' and i_store_location='$loc'");
        if ($quer->num_rows() > 0)
        {
          foreach($quer->result() as $ni)
          {
            $sawal=$ni->n_stockopname;
          }
          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
          #if($store=='AA') $loc='01'; else $loc='00';
          $loc=$istorelocation;
          $opname=0;
          $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                  where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product' and i_store_location='$loc'");
          if ($qur->num_rows() > 0)
          {
            foreach($qur->result() as $nu)
            {
              $opname=$nu->n_stockopname;
            }
          }else{
            $opname==0;
          }
          $cek=$this->db->query("select i_product from tm_mutasi 
                                 where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product' and i_store_location='$loc'");
          if ($cek->num_rows() == 0)
          {
            $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                             i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                             n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                             n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
                             values
                             ('$iperiode','$store','$vie->product','$grade','00',
                             '$loc','00',$sawal,
                              $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
          }
        }else{
          $sawal=0;
          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
          #if($store=='AA') $loc='01'; else $loc='00';
          $loc=$istorelocation;
          $opname=0;
          $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                  where i_stockopname='$soAkhir' and i_area='$iarea' and i_product='$vie->product' and i_store_location='$loc'");
          if ($qur->num_rows() > 0)
          {
            foreach($qur->result() as $nu)
            {
              $opname=$nu->n_stockopname;
            }
          }else{
            $opname==0;
          }
          $cek=$this->db->query("select i_product from tm_mutasi 
                                 where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product' and i_store_location='$loc'");
          if ($cek->num_rows() == 0)
          {
            $this->db->query("insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                              i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                              n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                              n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,n_mutasi_daritoko)
                              values
                              ('$iperiode','$store','$vie->product','$grade','00',
                              '$loc','00',$sawal,
                              $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
          }
        }
        $qu = $this->db->query("select * from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product' and i_store_location='$loc'");
        if ($qu->num_rows() > 0)
        {
          foreach($qu->result() as $vei)
          {
            $querys=$this->db->query(" select i_stockopname_akhir from tm_mutasi_header
                                     where i_store='$store' and e_mutasi_periode='$iperiode' and i_store_location='$loc'",false);
            if($querys->num_rows()>0)
            {
              $row   	= $querys->row();
              if( (trim($row->i_stockopname_akhir)=='')||($row->i_stockopname_akhir==null) )
              {
                $queryz = $this->db->query("SELECT i_product from tm_ic
                                            where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                            and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                if($queryz->num_rows>0){
                  $this->db->query("update tm_ic set n_quantity_stock=$vei->n_saldo_akhir where i_store='$store' and i_product='$vei->i_product'
                                    and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade' and i_store_location='$loc'");
                }else{
                  $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                  i_store_location, i_store_locationbin,f_product_active) 
                  values
                  ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                  '$store','$loc','00','t')");
                }
              }else{
                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row   	= $querys->row();
                if($row->c==$iperiode)
                {
                  $queryz = $this->db->query("SELECT i_product from tm_ic
                                              where i_store='$store' and i_product='$vei->i_product' and i_store_location='$loc'
                                              and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  if($queryz->num_rows>0)
                  {
                    $this->db->query("update tm_ic set n_quantity_stock=$vei->n_saldo_akhir where i_store='$store' and i_product='$vei->i_product'
                                      and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade' and i_store_location='$loc'");
                  }else{
                    $this->db->query("insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                    i_store_location, i_store_locationbin,f_product_active) 
                    values
                    ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade',
                    '$store','$loc','00','t')");
                  }
                }
              }
            }
          }
        }
      }
    }
  } 
  function detail($loc,$iperiode,$iarea,$iproduct,$iproductmotif,$iproductgrade)
  {
#    if($iarea=='00')
    if($iarea=='AA')
    {
	    $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name, 
                          sum(a.in) as in, sum(a.out) as out, sum(a.git) as git
                          FROM tr_product b, vmutasidetail a
                          left join tm_nota_item c on c.i_sj=a.ireff and a.product=c.i_product
                          left join tm_spb d on d.i_sj=c.i_sj
                          left join tr_customer e on d.i_customer=e.i_customer 
                          WHERE 
                            b.i_product = a.product AND
                            a.periode='$iperiode' AND a.area='AA' AND a.product='$iproduct' 
                          and not a.ireff like 'BBM%' 
                          group by b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name
                          order by dreff, ireff",false);
    }elseif($iarea=='PB'){
	    $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name, 
                          sum(a.in) as in, sum(a.out) as out, sum(a.git) as git
                          FROM tr_product b, vmutasidetailx a
                          left join tm_nota_item c on c.i_sj=a.ireff and a.product=c.i_product
                          left join tm_spb d on d.i_sj=c.i_sj
                          left join tr_customer e on d.i_customer=e.i_customer 
                          WHERE 
                            b.i_product = a.product AND
                            a.periode='$iperiode' AND a.area='$iarea' AND a.product='$iproduct' and a.loc='$loc' 
                          and not a.ireff like 'BBM%' 
                          group by b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name
                          order by dreff, ireff",false);
    }else{
	    $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name, 
                          sum(a.in) as in, sum(a.out) as out, sum(a.git) as git
                          FROM tr_product b, vmutasidetail a
                          left join tm_nota_item c on c.i_sj=a.ireff and a.product=c.i_product
                          left join tm_spb d on d.i_sj=c.i_sj
                          left join tr_customer e on d.i_customer=e.i_customer 
                          WHERE 
                            b.i_product = a.product AND
                            a.periode='$iperiode' AND a.area='$iarea' AND a.product='$iproduct' and a.loc='$loc' 
                          and not a.ireff like 'BBM%' 
                          group by b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name
                          order by dreff, ireff",false);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
	    return $query->result();
    }
  }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}else{
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                         from tr_area a, tr_store b 
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                         and a.i_store=b.i_store
						 order by a.i_store ", FALSE)->limit($num,$offset);
	}else{
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                         from tr_area a, tr_store b
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						 or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function bacaso($num,$offset,$iarea,$peri)
  {
	$this->db->select(" i_stockopname from tm_stockopname 
                        where f_stockopname_cancel='f' and i_area = '$iarea' and to_char(d_stockopname,'yyyymm')='$peri' 
                        order by i_stockopname desc", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
  }
}
?>
