<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
                            where a.i_customer=b.i_customer
                            and a.i_kn_type='01' and a.v_sisa>0
                            and (upper(a.i_kn) like '%$cari%'
                            or upper(a.i_customer) like '%$cari%' 
                            or upper(b.e_customer_name) like '%$cari%')
                            order by a.i_kn",false)->limit($num,$offset);
		}else{
			$this->db->select("  a.*, b.e_customer_name from tm_kn a, tr_customer b
                           where a.i_customer=b.i_customer
                           and a.i_kn_type='01' and a.v_sisa>0
                           and (upper(a.i_kn) like '%$cari%'
                           or upper(a.i_customer) like '%$cari%' 
                           or upper(b.e_customer_name) like '%$cari%')
						          and (a.i_area='$area1' 
						          or a.i_area='$area2' 
						          or a.i_area='$area3' 
						          or a.i_area='$area4' 
						          or a.i_area='$area5')
						          order by a.i_kn",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
    $this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
                          where a.i_customer=b.i_customer
                          and a.i_kn_type='01' and a.v_sisa>0
                          and (upper(a.i_kn) like '%$cari%'
                          or upper(a.i_customer) like '%$cari%' 
                          or upper(b.e_customer_name) like '%$cari%')
                          order by a.i_kn",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_kn a, tr_customer b
                          where a.i_customer=b.i_customer
                          and a.i_kn_type='01' and a.v_sisa>0
                          and (upper(a.i_kn) like '%$cari%'
                          or upper(a.i_customer) like '%$cari%' 
                          or upper(b.e_customer_name) like '%$cari%')
					                and (a.i_area='$area1' 
					                or a.i_area='$area2' 
					                or a.i_area='$area3' 
					                or a.i_area='$area4' 
					                or a.i_area='$area5')
					                order by a.i_kn",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
        $sql = "* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area";
      $this->db->select($sql, false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
      $sql = "* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' and 
              i_area in ( select i_area from tm_user_area where i_user='$iuser') )";
      $this->db->select($sql, FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_kn a, tr_customer b
                        where a.i_customer=b.i_customer
                        and a.i_kn_type='01' and a.v_sisa>0 and a.f_kn_cancel='f'
                        and (upper(a.i_kn) like '%$cari%'
                        or upper(a.i_customer) like '%$cari%' 
                        or upper(b.e_customer_name) like '%$cari%')
                        and a.i_area='$iarea' order by a.i_kn ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiodeperpages($iarea,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_kn a, tr_customer b
                        where a.i_customer=b.i_customer
                        and a.i_kn_type='01' and a.v_sisa>0
                        and (upper(a.i_kn) like '%$cari%'
                        or upper(a.i_customer) like '%$cari%' 
                        or upper(b.e_customer_name) like '%$cari%')
                        and a.i_area='$iarea' order by a.i_kn ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_kn a, tr_customer b
                        where a.i_customer=b.i_customer
                        and a.i_kn_type='01' and a.v_sisa>0
                        and (upper(a.i_kn) like '%$cari%'
                        or upper(a.i_customer) like '%$cari%' 
                        or upper(b.e_customer_name) like '%$cari%')
                        and a.i_area='$iarea' order by a.i_kn",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
