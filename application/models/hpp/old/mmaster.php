<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer and a.i_customer=c.i_customer
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f' and a.i_customer=c.i_customer
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3'
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
  function bacaperiode($iperiode)
    {
    #delete from tm_stockopname_gabungan where e_periode='201706';
    #delete from tm_hpp where e_periode='201706';
/*
      if($iperiode>'201512'){
        $x=$this->db->query(" select a.* from (
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              and i_product='DLB2313'
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name 
                              from f_mutasi_stock_pusat_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull and a.i_product='DLB2313'
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name 
                              from f_mutasi_stock_mo_pb_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull  and a.i_product='DLB2313'
                              ) as a
                              ",false);# where a.i_product='DGA4301'
      }else{
        $x=$this->db->query(" SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all('$iperiode') where i_store<>'PB' and not e_product_name isnull 
                              and i_product='DLB2313'
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat
                              ('$iperiode') a where not a.e_product_name isnull and a.i_product='DLB2313'
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb
                              ('$iperiode') a where not a.e_product_name isnull and a.i_product='DLB2313'",false);
      }
*/
      if($iperiode>'201512'){
        $x=$this->db->query(" select a.* from (
                              SELECT n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              union all
                              SELECT n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name 
                              from f_mutasi_stock_pusat_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name 
                              from f_mutasi_stock_mo_pb_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull
                              ) as a
                              ",false);# where a.i_product='DGA4301'
      }else{
        $x=$this->db->query(" SELECT n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all('$iperiode') where i_store<>'PB' and not e_product_name isnull 
                              union all
                              SELECT n_saldo_stockopname, case when i_store_location='PB' 
                              then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb
                              ('$iperiode') a where not a.e_product_name isnull",false);
      }
      if ($x->num_rows() > 0){
        $this->db->query("delete from tm_stockopname_gabungan where e_periode='$iperiode'",false);
        $i=0;
        foreach($x->result() as $xx){
          $i++;
          $jml      = $xx->n_saldo_stockopname;#+$xx->n_mutasi_git+$xx->n_git_penjualan;
          $iproduct = $xx->i_product;
          $nama     = $xx->e_product_name;
          $grade    = $xx->i_product_grade;
          $motif    = $xx->i_product_motif;
          $qu=$this->db->query("select * from tm_stockopname_gabungan
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' 
                                and i_product_grade='$grade' ",false);
          if ($qu->num_rows() > 0){
            $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                              where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' and i_product_grade='$grade'",false);
          }else{
            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
            $row   	= $query->row();
            $entry	= $row->c;
            $this->db->query("insert into tm_stockopname_gabungan values
                              ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);#
          }
        }
/*
        if($iperiode>'201512'){
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name 
                                    from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
                                    where not a.e_product_name isnull and a.i_product='DLB2313'
                                    order by a.i_customer",false);# and a.i_product='DGA4301'
        }else{
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name from f_mutasi_stock_mo_cust_all('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }
*/
        if($iperiode>'201512'){
          $query=$this->db->query(" SELECT a.i_customer,  n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name 
                                    from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);# and a.i_product='DGA4301'
        }else{
          $query=$this->db->query(" SELECT a.i_customer, n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name from f_mutasi_stock_mo_cust_all('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $tmp){
            $i++;
            $customer = $tmp->i_customer;
            $iproduct = $tmp->i_product;
            $grade    = $tmp->i_product_grade;
            $motif    = $tmp->i_product_motif;
            $jml      = $tmp->n_saldo_stockopname;
            $nama     = $tmp->e_product_name;              
            $qu=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' and i_product_grade='$grade' ",false);
            if ($qu->num_rows() > 0){
              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' and i_product_grade='$grade' ",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);#
            }
          }
	      }
        $qie=$this->db->query("select b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name, sum(b.n_quantity) as n_quantity
                               from tm_notapb a, tm_notapb_item b
                               where a.i_notapb=b.i_notapb and a.i_area=b.i_area and a.i_customer=b.i_customer and
                               to_char(a.d_notapb,'yyyymm')='$iperiode' and a.f_notapb_cancel='f'
                               group by b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name",false);
                               # and b.i_product='DGA4301'
        if ($qie->num_rows() > 0){
          foreach($qie->result() as $mt){
            $i++;
            $iproduct = $mt->i_product;
            $grade    = $mt->i_product_grade;
            $motif    = $mt->i_product_motif;
            $jml      = $mt->n_quantity;
            $nama     = $mt->e_product_name;
            $qz=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' and i_product_grade='$grade' ",false);
            if ($qz->num_rows() > 0){

              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_motif='$motif' and i_product_grade='$grade' ",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);#
            }
          }
        }
        $this->db->query("delete from tm_hpp where e_periode='$iperiode'");
#############################################################################################################################################
        $qu=$this->db->query("select e_periode, i_product, 'A' as i_product_motif, '00' as i_product_grade, e_product_name, 
        sum(n_stockopname) as n_stockopname from tm_stockopname_gabungan where e_periode='$iperiode'
        group by e_periode, i_product, e_product_name order by i_product",false);
        if ($qu->num_rows() > 0){
          $tes=0;
          foreach($qu->result() as $xx){
            $tes++;
            $jml=$xx->n_stockopname;
            $xx->e_product_name=str_replace("'","''",$xx->e_product_name);
            $hrg=0;
            $jmlbeli=0;
            $qi=$this->db->query("select * from f_history_beli('$iperiode','$xx->i_product')",false);
            if ($qi->num_rows() > 0){
              $jmlx=$jml;
              foreach($qi->result() as $yy){
                $hrg=$yy->hargabeli;
                $hrgdisc=(($yy->hargabeli*75)/100);
                $jmlasal=$jml-$jmlbeli;
                $jmlbeli=$jmlbeli+$yy->beli;
                if($jml>=$jmlbeli){
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$yy->beli, n_opname_total=n_opname_total+$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
#                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
#                                       '$xx->i_product_grade', '$xx->e_product_name', 0, $yy->beli, $hrg, 'f',$xx->n_stockopname)",false);
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '00', 'A', '$xx->e_product_name', 0, 
                      $yy->beli, $hrg, 'f',$xx->n_stockopname)",false);
                  }
                }else{
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=n_opname_total+$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '00', 
                                            'A', '$xx->e_product_name', 0, $jmlasal, $hrg, 
                                            'f',$xx->n_stockopname)",false);
                  }
                  break;
                }
              }
#              if($tes==2)die;
            }else{
              $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                    where i_product='$xx->i_product' and i_price_group='00'",false);
              if ($qa->num_rows() > 0){
                foreach($qa->result() as $txt){
                  $pangaos=$txt->v_product_mill;
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=n_opname_total+$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos ",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0",false);
                    if ($qa->num_rows() == 0){
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '00', 
                                        'A', '$xx->e_product_name', 0, $jml, 0, 
                                        'f',$xx->n_stockopname)",false);
                    }
                  }
                }
              }else{
                $qa=$this->db->query("select * from tm_hpp
                                      where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0",false);
                if ($qa->num_rows() == 0){
                  $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '00', 
                                    'A', '$xx->e_product_name', 0, $jml, 0, 
                                    'f',$xx->n_stockopname)",false);
                }
              }
            }
            $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                  where i_product='$xx->i_product' and i_product_grade='A' 
                                  and i_price_group='00'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
#                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
#                                      and i_product_grade='$xx->i_product_grade' 
#                                      and i_product_motif='$xx->i_product_motif' and v_harga=$txt->v_product_mill",false);
                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
                                      and (v_harga=0 or v_harga isnull)",false);
#                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 

                if ($qx->num_rows() == 0){
                  $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                    where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)",false);
#                                    and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'
                }
              }
            }
#####yang ga ketemu harganya ambil dari master
            $qa=$this->db->query("select v_product_mill from tr_product where i_product='$xx->i_product'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
#                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
#                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 
#                                      and v_harga=$txt->v_product_mill",false);
                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
                                      and (v_harga=0 or v_harga isnull)",false);
#                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 
                if ($qx->num_rows() == 0){
                  $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                    where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)",false);
#                                    and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'
                }
              }
            }
##############################################
            $qz=$this->db->query("select i_product_a from tr_product_ab where i_product_b='$xx->i_product'",false);
            if ($qz->num_rows() > 0){
              foreach($qz->result() as $tx){
#                $qa=$this->db->query("select v_product_mill from tr_harga_beli
#                                      where i_product='$tx->i_product_a' and i_price_group='00'
#                                      and i_product_grade='$xx->i_product_grade'",false);
                $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                      where i_product='$tx->i_product_a' and i_price_group='00'",false);
                if ($qa->num_rows() > 0){
                  foreach($qa->result() as $txt){
                    $pangaos=(($txt->v_product_mill*75)/100);
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos ",false);
#                                          and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'
                    if ($qa->num_rows()==0){
                      $this->db->query("update tm_hpp set v_harga=$pangaos where e_periode='$iperiode' and i_product='$xx->i_product' 
                                        and (v_harga=0 or v_harga isnull)",false);
#                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'
                    }
                  }
                }
              }
            }
          } 
        }

        $qi=$this->db->query("select a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, sum(b.n_jumlah) as n_jumlah,
                              b.i_product_motif 
                              from tm_dtap a, tm_dtap_item b
                              where a.i_dtap=b.i_dtap and a.i_area=b.i_area and a.i_supplier=b.i_supplier 
                              and a.f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
                              group by a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, b.i_product_motif
                              order by a.d_dtap desc",false);# and b.i_product='DGA4301'
        if ($qi->num_rows() > 0){
          foreach($qi->result() as $yy){
            $yy->e_product_name=str_replace("'","''",$yy->e_product_name);
            $beli=$yy->n_jumlah;
            $qa=$this->db->query("select * from tm_hpp
                                  where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik",false);
#                                  and i_product_motif='$yy->i_product_motif' and i_product_grade='A'
            if ($qa->num_rows() > 0){
              $this->db->query("update tm_hpp set n_beli=n_beli+$beli, f_beli='t', v_harga=$yy->v_pabrik
                                where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik",false);
#                                and i_product_motif='$yy->i_product_motif' and i_product_grade='A'
            }else{
              $this->db->query("insert into tm_hpp values ('$iperiode', '$yy->i_product', '00', 'A', '$yy->e_product_name', $yy->n_jumlah, 
                                0, $yy->v_pabrik, 't', 0)",false);
            }
          }
        }
#############################################################################################################################################
        $xy=$this->db->query("select * from tm_hpp where e_periode='$iperiode' order by e_product_name, i_product");
        if ($xy->num_rows() > 0){
          return $xy->result();
        }
		  }
    }
/*
    function bacaperiode($iperiode)
    {
      if($iperiode>'201512'){
        $x=$this->db->query(" SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull",false);
      }else{
        $x=$this->db->query(" SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb
                              ('$iperiode') a where not a.e_product_name isnull",false);
      }
      if ($x->num_rows() > 0){
        $this->db->query("delete from tm_stockopname_gabungan where e_periode='$iperiode'",false);
        $i=0;
        foreach($x->result() as $xx){
          $i++;
          $jml      = $xx->n_saldo_stockopname;#+$xx->n_mutasi_git+$xx->n_git_penjualan;
          $iproduct = $xx->i_product;
          $nama     = $xx->e_product_name;
          $grade    = $xx->i_product_grade;
          $motif    = $xx->i_product_motif;
          $qu=$this->db->query("select * from tm_stockopname_gabungan
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                                and i_product_motif='$motif'",false);
          if ($qu->num_rows() > 0){
            $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                              where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                              and i_product_motif='$motif'",false);
          }else{
            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
            $row   	= $query->row();
            $entry	= $row->c;
            $this->db->query("insert into tm_stockopname_gabungan values
                              ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
          }
        }
        if($iperiode>'201512'){
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name 
                                    from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }else{
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name from f_mutasi_stock_mo_cust_all('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $tmp){
            $i++;
            $customer = $tmp->i_customer;
            $iproduct = $tmp->i_product;
            $grade    = $tmp->i_product_grade;
            $motif    = $tmp->i_product_motif;
            $jml      = $tmp->n_saldo_stockopname;
            $nama     = $tmp->e_product_name;              
            $qu=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                                  and i_product_motif='$motif'",false);
            if ($qu->num_rows() > 0){
              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                                and i_product_motif='$motif'",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
            }
          }
	      }
        $qie=$this->db->query("select b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name, sum(b.n_quantity) as n_quantity
                               from tm_notapb a, tm_notapb_item b
                               where a.i_notapb=b.i_notapb and a.i_area=b.i_area and a.i_customer=b.i_customer and
                               to_char(a.d_notapb,'yyyymm')='$iperiode' and a.f_notapb_cancel='f'                               
                               group by b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name",false);
        if ($qie->num_rows() > 0){
          foreach($qie->result() as $mt){
            $i++;
            $iproduct = $mt->i_product;
            $grade    = $mt->i_product_grade;
            $motif    = $mt->i_product_motif;
            $jml      = $mt->n_quantity;
            $nama     = $mt->e_product_name;
            $qz=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                                  and i_product_motif='$motif'",false);
            if ($qz->num_rows() > 0){
              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct' and i_product_grade='$grade' 
                                and i_product_motif='$motif'",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
            }
          }
        }
        $this->db->query("delete from tm_hpp where e_periode='$iperiode'");
#############################################################################################################################################
        $qu=$this->db->query("select * from tm_stockopname_gabungan where e_periode='$iperiode' ",false);
        if ($qu->num_rows() > 0){
          foreach($qu->result() as $xx){
            $jml=$xx->n_stockopname;
            $xx->e_product_name=str_replace("'","''",$xx->e_product_name);
            $hrg=0;
            $jmlbeli=0;
            $qi=$this->db->query("select * from f_history_beli('$iperiode','$xx->i_product')",false);
            if ($qi->num_rows() > 0){
              $jmlx=$jml;
              foreach($qi->result() as $yy){
                $hrg=$yy->hargabeli;
                $hrgdisc=(($yy->hargabeli*75)/100);
                $jmlasal=$jml-$jmlbeli;
                $jmlbeli=$jmlbeli+$yy->beli;
                if($jml>=$jmlbeli){
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$yy->beli, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                            '$xx->i_product_grade', '$xx->e_product_name', 0, $yy->beli, $hrg, 
                                            'f',$xx->n_stockopname)",false);
                  }
                }else{
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                            '$xx->i_product_grade', '$xx->e_product_name', 0, $jmlasal, $hrg, 
                                            'f',$xx->n_stockopname)",false);
                  }
                  break;
                }
              }
            }else{
              $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                    where i_product='$xx->i_product' and i_price_group='00'",false);
              if ($qa->num_rows() > 0){
                foreach($qa->result() as $txt){
                  $pangaos=$txt->v_product_mill;
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0
                                          and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                    if ($qa->num_rows() == 0){
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                        '$xx->i_product_grade', '$xx->e_product_name', 0, $jml, 0, 
                                        'f',$xx->n_stockopname)",false);
                    }
                  }
                }
              }else{
                $qa=$this->db->query("select * from tm_hpp
                                      where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0
                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                if ($qa->num_rows() == 0){
                  $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                    '$xx->i_product_grade', '$xx->e_product_name', 0, $jml, 0, 
                                    'f',$xx->n_stockopname)",false);
                }
              }
            }
            $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                  where i_product='$xx->i_product' and i_product_grade='$xx->i_product_grade' 
                                  and i_price_group='00'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
#                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
#                                      and i_product_grade='$xx->i_product_grade' 
#                                      and i_product_motif='$xx->i_product_motif' and v_harga=$txt->v_product_mill",false);
                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
                                      and i_product_grade='$xx->i_product_grade' 
                                      and i_product_motif='$xx->i_product_motif' and (v_harga=0 or v_harga isnull)",false);
                if ($qx->num_rows() == 0){
                  $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                    where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                    and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                }
              }
            }

#####yang ga ketemu harganya ambil dari master
            $qa=$this->db->query("select v_product_mill from tr_product where i_product='$xx->i_product'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
#                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
#                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 
#                                      and v_harga=$txt->v_product_mill",false);
                $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 
                                      and (v_harga=0 or v_harga isnull)",false);
                if ($qx->num_rows() == 0){
                  $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                    where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                    and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                }
              }
            }
##############################################

            $qz=$this->db->query("select i_product_a from tr_product_ab where i_product_b='$xx->i_product'",false);
            if ($qz->num_rows() > 0){
              foreach($qz->result() as $tx){
#                $qa=$this->db->query("select v_product_mill from tr_harga_beli
#                                      where i_product='$tx->i_product_a' and i_price_group='00'
#                                      and i_product_grade='$xx->i_product_grade'",false);
                $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                      where i_product='$tx->i_product_a' and i_price_group='00'",false);
                if ($qa->num_rows() > 0){
                  foreach($qa->result() as $txt){
                    $pangaos=(($txt->v_product_mill*75)/100);
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                          and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                    if ($qa->num_rows()==0){
                      $qx=$this->db->query("select * from tm_hpp where e_periode='$iperiode' and i_product='$xx->i_product' 
                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif' 
                                      and v_harga=$pangaos",false);
                      if ($qx->num_rows() == 0){
                        $this->db->query("update tm_hpp set v_harga=$pangaos
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                          and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                      }
                    }
                  }
                }
              }
            }
          } 
        }

        $qi=$this->db->query("select a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, sum(b.n_jumlah) as n_jumlah,
                              b.i_product_motif 
                              from tm_dtap a, tm_dtap_item b
                              where a.i_dtap=b.i_dtap and a.i_area=b.i_area and a.i_supplier=b.i_supplier 
                              and a.f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
                              group by a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, b.i_product_motif
                              order by a.d_dtap desc",false);
        if ($qi->num_rows() > 0){
          foreach($qi->result() as $yy){
            $yy->e_product_name=str_replace("'","''",$yy->e_product_name);
            $beli=$yy->n_jumlah;
            $qa=$this->db->query("select * from tm_hpp
                                  where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik
                                  and i_product_motif='$yy->i_product_motif' and i_product_grade='A'",false);
            if ($qa->num_rows() > 0){
              $this->db->query("update tm_hpp set n_beli=n_beli+$beli, f_beli='t', v_harga=$yy->v_pabrik
                                where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik
                                and i_product_motif='$yy->i_product_motif' and i_product_grade='A'",false);
            }else{
              $this->db->query("insert into tm_hpp values ('$iperiode', '$yy->i_product', '00', 'A', '$yy->e_product_name', $yy->n_jumlah, 
                                0, $yy->v_pabrik, 't', 0)",false);
            }
          }
        }
#############################################################################################################################################
        $xy=$this->db->query("select * from tm_hpp where e_periode='$iperiode' order by e_product_name, i_product");
        if ($xy->num_rows() > 0){
          return $xy->result();
        }
		  }
    }
*/
    function bacadetail($icustomer,$iperiode,$num,$offset)
    {
      $this->db->select(" * from(
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_nota as tglbukti, a.i_nota as bukti, '' as jenis, '1. penjualan' as keterangan, a.v_nota_netto as debet, 
                          0 as kredit, 0 as dn, 0 as kn
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='$iperiode'
                          and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'kredit nota' as keterangan, 0 as debet, 0 as kredit, 0 as dn, 
                          v_netto as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_bukti as tglbukti, a.i_pelunasan as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 
                          0 as debet, a.v_jumlah as kredit, 0 as dn, 0 as kn
                          from tm_pelunasan a, tr_customer_groupar b, tr_customer c, tr_jenis_bayar d
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and to_char(d_bukti,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar 
                          and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'debet nota' as keterangan, 0 as debet, 0 as kredit, v_netto as dn, 
                          0 as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          ) as a order by area, groupar, tglbukti, keterangan, bukti",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
