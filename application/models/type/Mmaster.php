<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($itype)
    {
		$this->db->select("a.*, b.e_product_groupname")->from('tr_product_type a, tr_product_group b')->where("a.i_product_type = '$itype' and a.i_product_group=b.i_product_group order by a.i_product_type");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($itype, $etypename, $igroup, $etypenameprint1, $etypenameprint2)
    {
		$this->db->query("insert into tr_product_type (i_product_type, e_product_typename, i_product_group, e_product_typenameprint1, e_product_typenameprint2) values ('$itype', '$etypename','$igroup', '$etypenameprint1', '$etypenameprint2')");
		redirect('type/cform/');
    }

    function update($itype, $etypename, $igroup,$etypenameprint1,$etypenameprint2)
    {
		$this->db->query("update tr_product_type set e_product_typename = '$etypename', i_product_group = '$igroup', e_product_typenameprint1 = '$etypenameprint1', e_product_typenameprint2 = '$etypenameprint2' where i_product_type = '$itype'");
		redirect('type/cform/');
    }
	
    public function delete($itype) 
    {
		$this->db->query('DELETE FROM tr_product_type WHERE i_product_type=\''.$itype.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.i_product_type, a.e_product_typename, a.i_product_group, a.e_product_typenameprint1, a.e_product_typenameprint2, b.e_product_groupname from tr_product_type a, tr_product_group b where a.i_product_group=b.i_product_group and (upper(b.e_product_groupname) like '%$cari%' or upper(a.i_product_type) like '%$cari%' or upper(a.e_product_typename) like '%$cari%') order by a.i_product_type",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacagroup($num,$offset)
    {
		$this->db->select("i_product_group, e_product_groupname")->from('tr_product_group')->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_product_type, a.e_product_typename, a.i_product_group, a.e_product_typenameprint1, a.e_product_typenameprint2, b.e_product_groupname from tr_product_type a, tr_product_group b where (upper(a.e_product_typename) like '%$cari%' or upper(a.i_product_type) like '%$cari%' or upper(b.e_product_groupname) like '%$cari%' ) and a.i_product_group=b.i_product_group order by a.i_product_type asc")->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carikelas($cari,$num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname from tr_product_class where upper(e_product_classname) like '%$cari%' or upper(i_product_class) like '%$cari%' ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function carigroup($cari,$num,$offset)
    {
		$this->db->select("a.i_product_group, a.e_product_groupname from tr_product_group a where upper(a.i_product_group) ilike '%$cari%'  or upper(a.e_product_groupname) ilike '%$cari%' order by a.i_product_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
