<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	 e_area_name, sum(nilaibaby) as nilaibaby, sum(nilaimoms) as nilaimoms from(
										
										select  a.f_spb_stockdaerah as pusat, c.e_area_name, b.e_product_groupname as moms, case when sum(a.v_spb) isnull then 0 else sum(a.v_spb) end as nilaimoms, 0 as nilaibaby
										from tm_spb a, tr_product_group b, tr_area c
										where a.f_spb_stockdaerah=false and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy')  
										and a.f_spb_cancel=false
										and a.i_product_group=b.i_product_group 
										and a.i_area=c.i_area 
										and not a.i_approve1 isnull
										and not a.i_approve2 isnull
										and a.i_store isnull
										and b.e_product_groupname='Moms Baby'
										group by a.f_spb_stockdaerah, a.i_area, b.e_product_groupname, c.e_area_name
									union all
										select a.f_spb_stockdaerah as pusat, c.e_area_name, b.e_product_groupname as babyjpy, 0 as nilaimoms, case when sum(a.v_spb) isnull then 0 else sum(a.v_spb) end as nilaibaby
										from tm_spb a, tr_product_group b, tr_area c
										where a.f_spb_stockdaerah=false and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy')  
										and a.f_spb_cancel=false
										and a.i_product_group=b.i_product_group 
										and a.i_area=c.i_area 
										and not a.i_approve1 isnull
										and not a.i_approve2 isnull
										and a.i_store isnull
										and b.e_product_groupname='Baby Joy'
										group by a.f_spb_stockdaerah, a.i_area, b.e_product_groupname, c.e_area_name
										)as a
										group by e_area_name
										order by e_area_name asc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
