<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset)
    {
		$this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
							b.e_area_name, a.i_customer, c.e_customer_name
							from tm_spb a, tr_area b, tr_customer c
							where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
							and a.i_area=b.i_area
							and a.i_customer=c.i_customer
							group by a.i_area, b.e_area_name, a.i_customer, c.e_customer_name 
							order by a.i_area, a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
							b.e_area_name, a.i_customer, c.e_customer_name
							from tm_spb a, tr_area b, tr_customer c
							where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
							and(upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%'
							or upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
							and a.i_area=b.i_area
							and a.i_customer=c.i_customer
							group by a.i_area, b.e_area_name, a.i_customer, c.e_customer_name 
							order by a.i_area, a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

}
?>
