<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
		$this->db->query('DELETE FROM tm_ttbretur WHERE i_ttb=\''.$iop.'\'');
		$this->db->query('DELETE FROM tm_ttbretur_item WHERE i_ttb=\''.$iop.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$iuser)
    {
		$this->db->select(" a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                        a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name 
                        from tm_bbm a, tr_area c, tr_salesman d
					              where a.i_bbm_type='02' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                        and a.i_salesman=d.i_salesman
					              and (upper(a.i_bbm) like '%$cari%' or upper(a.i_salesman) like '%$cari%' 
					              or upper(c.e_area_name) like '%$cari%' or upper(d.e_salesman_name) like '%$cari%') 
                        and (a.i_area in (select i_area from tm_user_area where i_user='$iuser') ) 
                        order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ibbm)
    {
		  $this->db->select(" a.*, b.e_customer_name from tm_bbm a , tr_customer b where a.i_bbm='$ibbm' and a.i_supplier=b.i_customer",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ibbm)
    {
		  $this->db->select("  a.i_bbm, a.i_product, a.i_product_motif, a.i_product_grade, 
								a.e_product_name, a.n_quantity as qty, a.v_unit_price, 
								b.e_product_motifname
								from tm_bbm_item a, tr_product_motif b
								where a.i_bbm = '$ibbm' 
								and a.i_product=b.i_product 
								and a.i_product_motif=b.i_product_motif 
								order by a.i_product",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
