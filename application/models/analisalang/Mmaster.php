<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }

  function bacaperiode($icustomer, $tahun)
  {
    /* BACA PENJUALAN RP (MIN, AVG, MAX) */
    return $this->db->query(" WITH raya AS (
                                  SELECT
                                    DISTINCT 
                                      y.bulan,
                                      bulan(y.bulan) AS eperiode,
                                      b.i_customer,
                                      b.e_customer_name,
                                      b.n_customer_toplength,
                                      b.d_signin,
                                      b.e_customer_address,
                                      b.f_customer_pkp,
                                      car.v_flapond
                                  FROM
                                    tr_customer b
                                    LEFT JOIN tm_nota a ON (a.i_customer = b.i_customer)
                                    INNER JOIN tr_customer_groupar car ON (car.i_customer = a.i_customer)
                                    CROSS JOIN (SELECT to_char(DATE '1001-01-01' + (INTERVAL '1' MONTH * generate_series(0, 11)), 'MM') AS bulan) y
                                  WHERE
                                    a.f_nota_cancel = 'f' AND NOT i_nota ISNULL
                                    AND to_char(a.d_nota, 'YYYY') = '$tahun'
                                )
                                SELECT
                                  y.i_customer,
                                  y.e_customer_name,
                                  y.n_customer_toplength,
                                  y.d_signin,
                                  y.e_customer_address,
                                  y.f_customer_pkp,
                                  y.v_flapond,
                                  jsonb_agg(y.eperiode ORDER BY y.bulan) AS bulan,
                                  jsonb_agg(COALESCE(x.v_nota_netto, 0) ORDER BY y.bulan) AS v_nota_netto,
                                  min(x.v_nota_netto) AS min,
                                  max(x.v_nota_netto) AS max,
                                  avg(x.v_nota_netto) AS avg
                                FROM
                                  (
                                    SELECT
                                      a.i_customer,
                                      b.e_customer_name,
                                      to_char(d_nota, 'MM') AS bulan,
                                      sum(v_nota_netto) AS v_nota_netto
                                    FROM
                                      tm_nota a
                                    INNER JOIN tr_customer b ON
                                      (b.i_customer = a.i_customer)
                                    WHERE
                                      f_nota_cancel = 'f'
                                      AND to_char(a.d_nota, 'YYYY') = '$tahun'
                                    GROUP BY
                                      1, 2, 3) AS x
                                  RIGHT JOIN raya y ON (x.i_customer = y.i_customer AND x.bulan = y.bulan)	
                                WHERE
                                  y.i_customer = '$icustomer'
                                GROUP BY 
                                  y.i_customer,
                                  y.e_customer_name,
                                  y.n_customer_toplength,
                                  y.v_flapond,
                                  y.d_signin,
                                  y.e_customer_address,
                                  y.f_customer_pkp ");
  }

  function bacaperiode_pernota($icustomer, $tahun)
  {
    /* BACA PENJUALAN RP PER NOTA (MIN & MAX)*/
    return $this->db->query(" SELECT
                                a.i_customer,
                                b.e_customer_name,
                                min(v_nota_netto) AS min_nota,
                                max(v_nota_netto) AS max_nota
                              FROM
                                tm_nota a
                              INNER JOIN tr_customer b ON
                                (b.i_customer = a.i_customer)
                              WHERE
                                f_nota_cancel = 'f'
                                AND to_char(a.d_nota, 'YYYY') = '$tahun'
                                AND a.i_customer = '$icustomer'
                              GROUP BY 1,2
                              LIMIT 1 ");
  }

  function baca_carabyr($icustomer, $tahun)
  {
    /* BACA CARA BAYAR TERBANYAK */
    return $this->db->query(" SELECT
                                i_customer,
                                jenis_byr,
                                count(jenis_byr) AS jml
                              FROM (
                                WITH mst AS (
                                  SELECT 'TRANSFER'::TEXT AS jenis_byr, i_area, i_customer, i_kum AS i_giro FROM tm_kum WHERE f_kum_cancel = 'f'
                                  UNION ALL 
                                  SELECT 'GIRO'::TEXT AS jenis_byr, i_area, i_customer, i_giro AS i_giro FROM tm_giro WHERE f_giro_batal = 'f'
                                  UNION ALL 
                                  SELECT 'TUNAI'::TEXT AS jenis_byr, i_area, i_customer, i_tunai AS i_giro FROM tm_tunai WHERE f_tunai_cancel = 'f'
                                )
                                SELECT DISTINCT 
                                  a.i_customer,
                                  d.jenis_byr,
                                  c.i_giro
                                FROM
                                  tm_nota a
                                  LEFT JOIN tm_alokasi_item b ON a.i_nota = b.i_nota
                                  INNER JOIN tm_alokasi c ON b.i_alokasi = c.i_alokasi AND b.i_coa_bank = c.i_coa_bank AND b.i_area = c.i_area AND c.f_alokasi_cancel = 'f'
                                  RIGHT JOIN mst d ON c.i_giro = d.i_giro
                                WHERE
                                  f_nota_cancel = 'f'
                                  /* AND to_char(a.d_nota, 'YYYY') = '$tahun' */  
                              ) x
                              WHERE i_customer = '$icustomer'
                              GROUP BY i_customer, jenis_byr
                              ORDER BY count(jenis_byr) DESC
                              LIMIT 1 ");
  }

  function baca_lamabyr($icustomer, $tahun)
  {
    /* BACA LAMA BAYAR (MIN, AVG, MAX) */
    return $this->db->query(" WITH raya AS (
                                SELECT
                                  DISTINCT 
                                    y.bulan,
                                    bulan(y.bulan) AS eperiode,
                                    b.i_area,
                                    b.i_customer,
                                    b.e_customer_name,
                                    b.n_customer_toplength,
                                    b.d_signin,
                                    b.e_customer_address,
                                    b.f_customer_pkp,
                                    car.v_flapond
                                FROM
                                  tr_customer b
                                  LEFT JOIN tm_nota a ON (a.i_customer = b.i_customer)
                                  INNER JOIN tr_customer_groupar car ON (car.i_customer = a.i_customer)
                                  CROSS JOIN (SELECT to_char(DATE '1001-01-01' + (INTERVAL '1' MONTH * generate_series(0, 11)), 'MM') AS bulan) y
                                WHERE
                                  a.f_nota_cancel = 'f' AND NOT i_nota ISNULL
                                  AND to_char(a.d_nota, 'YYYY') = '$tahun'
                              )
                              SELECT 
                                y.i_customer,
                                jsonb_agg(y.eperiode ORDER BY y.bulan) AS bulan,
                                jsonb_agg(lamabayar ORDER BY y.bulan) AS lama,
                                min(lamabayar) AS min_byr,
                                avg(lamabayar) AS avg_byr,
                                max(lamabayar) AS max_byr
                              FROM (
                                SELECT 
                                  bulan,
                                  x.i_customer,
                                  avg(lamabayar) AS lamabayar 
                                FROM (
                                  SELECT
                                    to_char(a.d_nota,'mm') AS bulan,
                                    a.i_customer,
                                    a.i_nota,
                                    avg(c.d_alokasi - a.d_nota) AS lamabayar
                                  FROM
                                    tm_nota a
                                    LEFT JOIN tm_alokasi_item b ON a.i_nota = b.i_nota
                                    INNER JOIN tm_alokasi c ON b.i_alokasi = c.i_alokasi AND b.i_coa_bank = c.i_coa_bank AND b.i_area = c.i_area AND c.f_alokasi_cancel = 'f'
                                  WHERE
                                    f_nota_cancel = 'f'
                                    AND to_char(a.d_nota, 'YYYY') = '$tahun'
                                    GROUP BY a.d_nota, a.i_customer,a.i_nota, a.i_area
                                ) x GROUP BY bulan,x.i_customer
                              ) a
                                RIGHT JOIN raya y ON (a.i_customer = y.i_customer AND a.bulan = y.bulan)	
                                WHERE y.i_customer = '$icustomer'
                              GROUP BY 
                                y.i_customer ");
  }

  function last_order($icustomer)
  {
    /* BACA ORDER TERAKHIR (NOTA) */
    return $this->db->query(" SELECT i_nota, d_nota, d_sj, v_nota_netto FROM tm_nota WHERE f_nota_cancel = 'f' 
                              AND NOT i_nota IS NULL AND i_customer = '$icustomer' ORDER BY d_sj DESC LIMIT 1 ");
  }

  function bacabulan()
  {
    /* BACA BULAN JAN - DES */
    return $this->db->query(" SELECT bulan(to_char(DATE '1001-01-01' + (INTERVAL '1' MONTH * generate_series(0, 11)), 'MM')) AS bulan", false);
  }

  function bacacustomer($cari, $num, $offset)
  {
    if ($cari == '') {
      $this->db->select(" * from tr_customer where f_customer_aktif ='t' and f_approve='t' order by i_customer", false)->limit($num, $offset);
    } else {
      $this->db->select(" * from tr_customer where f_customer_aktif ='t' and f_approve='t' and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by i_customer", false)->limit($num, $offset);
    }

    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
