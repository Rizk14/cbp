<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iikhp, $iarea) 
    {
			$this->db->query("update tm_ikhp set f_ikhp_cancel='t' WHERE i_ikhp='$iikhp' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_ikhp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_ikhp) like '%$cari%')
							order by a.i_ikhp",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_ikhp a, tr_area b
					where a.i_area=b.i_area 
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_ikhp) like '%$cari%')
					order by a.i_ikhp desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    if($cari==''){
		  $this->db->select("	a.*, b.e_area_name, c.e_ikhp_typename from tm_ikhp a, tr_area b, tr_ikhp_type c
							  where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							  and a.i_area='$iarea' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  order by a.d_bukti, a.i_ikhp ",false)->limit($num,$offset);
    }else{
		  $this->db->select("	a.*, b.e_area_name, c.e_ikhp_typename from tm_ikhp a, tr_area b, tr_ikhp_type c
							  where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							  and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%')
							  and a.i_area='$iarea' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  order by a.d_bukti, a.i_ikhp ",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		if($cari==''){
		  $this->db->select("	a.*, b.e_area_name, c.e_ikhp_typename  from tm_ikhp a, tr_area b, tr_ikhp_type c
							  where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							  and a.i_area='$iarea' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  order by a.d_bukti, a.i_ikhp ",false)->limit($num,$offset);
    }else{
		  $this->db->select("	a.*, b.e_area_name, c.e_ikhp_typename  from tm_ikhp a, tr_area b, tr_ikhp_type c
							  where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							  and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%' or a.i_ikhp=$cari)
							  and a.i_area='$iarea' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  order by a.d_bukti, a.i_ikhp ",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ikhp)
    {
		$this->db->select(" * from tm_ikhp a, tr_area b, tr_ikhp_type c 
                        where a.i_ikhp=$ikhp and a.i_area=b.i_area
                        and a.i_ikhp_type=c.i_ikhp_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function update($iikhp,$iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro,$ecek,$user)
    {
	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
	$row   	= $query->row();
	$dentry	= $row->c;

    	$this->db->set(
    		array(
		'e_cek'		=> $ecek,
		'd_cek'		=> $dentry,
		'i_cek'		=> $user
    		)
    	);
    	$this->db->where('i_ikhp',$iikhp);
    	$this->db->update('tm_ikhp');
    }
    function bacauraian($num,$offset)
    {
	$this->db->select("* from tr_ikhp_type order by i_ikhp_type", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
}
?>
