<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iicconvertion,$istore,$istorelocation,$istorelocationbin) 
    {
#  		$this->db->query('DELETE FROM tm_ic_convertion WHERE i_ic_convertion=\''.$iicconvertion.'\'');
#  		$this->db->query('DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion=\''.$iicconvertion.'\'');
#		  $this->db->query('DELETE FROM tm_bbk where i_refference_document =\''.$iicconvertion.'\'');
  		$this->db->query("update tm_ic_convertion set f_ic_convertioncancel='t' WHERE i_ic_convertion='$iicconvertion'");
  		$this->db->query("update tm_bbk set f_bbk_cancel='t' where i_refference_document ='$iicconvertion'");
		  $this->db->select(" d_refference_document from tm_bbk where i_refference_document ='$iicconvertion'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $dreff=$row->d_refference_document;
          $th=substr($dreff,0,4);
		      $bl=substr($dreff,5,2);
		      $emutasiperiode=$th.$bl;
        }
      }
		  $this->db->select(" * from tm_bbk_item where i_refference_document ='$iicconvertion'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$row->n_quantity
								    where i_product ='$row->i_product' and i_product_grade ='$row->i_product_grade'
								    and i_product_motif ='$row->i_product_motif' and i_store ='$istore' 
								    and i_store_location ='$istorelocation' and i_store_locationbin ='$istorelocationbin'");

          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                                        and i_product_motif='$row->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin' and i_refference_document='$iicconvertion'
                                        order by d_transaction desc, i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $rowtran= $queri->row();
            $nawal  = $rowtran->n_quantity_akhir;
          }else{
            $queri 		= $this->db->query("SELECT n_quantity_stock FROM tm_ic
                              where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                              and i_product_motif='$row->i_product_motif'
                              and i_store='$istore' and i_store_location='$istorelocation'
                              and i_store_locationbin='$istorelocationbin'",false);
            if ($queri->num_rows() > 0){
          	  $rowic   		= $queri->row();
              $nawal=$rowic->n_quantity_stock;
            }
          }
          $que 	= $this->db->query("SELECT current_timestamp as c");
          $ro 	= $que->row();
          $now	 = $ro->c;
          $this->db->query(" 
                            INSERT INTO tm_ic_trans
                            (
                              i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                              i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                              n_quantity_in, n_quantity_out,
                              n_quantity_akhir, n_quantity_awal)
                            VALUES 
                            (
                              '$row->i_product','$row->i_product_grade','$row->i_product_motif',
                              '$istore','$istorelocation','$istorelocationbin', 
                              '$row->e_product_name', '$iicconvertion', '$now', $row->n_quantity, 0, 
                              $nawal+$row->n_quantity, $nawal
                            )
                           ",false);

          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_bbk=n_mutasi_bbk-$row->n_quantity, 
                            n_saldo_akhir=n_saldo_akhir+$row->n_quantity
                            where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                            and i_product_motif='$row->i_product_motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);

			  }
		  }
  		$this->db->query("update tm_bbm set f_bbm_cancel='t' where i_refference_document ='$iicconvertion'");
		  $this->db->select(" d_refference_document from tm_bbm where i_refference_document ='$iicconvertion'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $dreff=$row->d_refference_document;
          $th=substr($dreff,0,4);
		      $bl=substr($dreff,5,2);
		      $emutasiperiode=$th.$bl;
        }
      }
		  $this->db->select(" * from tm_bbm_item where i_refference_document ='$iicconvertion'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock-$row->n_quantity
								    where i_product ='$row->i_product' and i_product_grade ='$row->i_product_grade'
								    and i_product_motif ='$row->i_product_motif' and i_store ='$istore' 
								    and i_store_location ='$istorelocation' and i_store_locationbin ='$istorelocationbin'");

          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                                        and i_product_motif='$row->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin' and i_refference_document='$iicconvertion'
                                        order by d_transaction desc, i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $rowtran= $queri->row();
            $nawal  = $rowtran->n_quantity_akhir;
          }else{
            $queri 		= $this->db->query("SELECT n_quantity_stock FROM tm_ic
                              where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                              and i_product_motif='$row->i_product_motif'
                              and i_store='$istore' and i_store_location='$istorelocation'
                              and i_store_locationbin='$istorelocationbin'",false);
            if ($queri->num_rows() > 0){
          	  $rowic   		= $queri->row();
              $nawal=$rowic->n_quantity_stock;
            }
          }
          $que 	= $this->db->query("SELECT current_timestamp as c");
          $ro 	= $que->row();
          $now	 = $ro->c;
          $this->db->query(" 
                            INSERT INTO tm_ic_trans
                            (
                              i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                              i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                              n_quantity_in, n_quantity_out,
                              n_quantity_akhir, n_quantity_awal)
                            VALUES 
                            (
                              '$row->i_product','$row->i_product_grade','$row->i_product_motif',
                              '$istore','$istorelocation','$istorelocationbin', 
                              '$row->e_product_name', '$iicconvertion', '$now', 0, $row->n_quantity, 
                              $nawal-$row->n_quantity, $nawal
                            )
                           ",false);

          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$row->n_quantity, 
                            n_saldo_akhir=n_saldo_akhir-$row->n_quantity
                            where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
                            and i_product_motif='$row->i_product_motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);

			  }
		  }
#		  $this->db->query('DELETE FROM tm_bbm_item where i_refference_document =\''.$iicconvertion.'\'');
    }
    function baca($iperiode)
    {
		$this->db->select(" a.i_product as ic_product, b.e_product_name as ic_product_name,
		                    a.i_product_motif as ic_product_motif, a.i_product_grade as ic_product_grade,
		                    a.f_ic_convertion, sum(a.n_ic_convertion) as ic_n_convertion,
		                    b.i_product as item_product, b.e_product_name as item_product_name,
		                    b.i_product_motif as item_product_motif, b.i_product_grade as item_product_grade,
		                    sum(b.n_ic_convertion) as item_n_convertion
		                    FROM tm_ic_convertion a, tm_ic_convertionitem b
		                    WHERE to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode'
		                    and a.i_ic_convertion = b.i_ic_convertion
		                    GROUP by a.i_product, b.e_product_name, a.i_product_motif, a.i_product_grade,
		                    a.f_ic_convertion, b.i_product, b.e_product_name, b.i_product_motif, b.i_product_grade
		                    ORDER by a.i_product asc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iperiode,$cari,$num,$offset)
    {
		$this->db->select(" * from tm_ic_convertion
                        where to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode' and
                        (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' or upper(i_ic_convertion) like '%$cari%')
                        order by i_ic_convertion desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
