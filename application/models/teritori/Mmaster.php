<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iteritori)
    {
		$this->db->select("a.*, b.e_country_name")->from('tr_teritori a, tr_country b')->where("i_teritori = '$iteritori' and a.i_country=b.i_country");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iteritori, $eteritoriname, $icountry)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_teritori (i_teritori, e_teritori_name, i_country, d_teritori_entry) values ('$iteritori', '$eteritoriname','$icountry', '$dentry')");
		#redirect('teritori/cform/');
    }

    function update($iteritori, $eteritoriname, $icountry)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_teritori set e_teritori_name = '$eteritoriname', i_country = '$icountry', d_teritori_update = '$dupdate' where i_teritori = '$iteritori'");
		#redirect('teritori/cform/');
    }
	
    public function delete($iteritori) 
    {
		$this->db->query('DELETE FROM tr_teritori WHERE i_teritori=\''.$iteritori.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.*, b.e_country_name from tr_teritori a, tr_country b where a.i_country=b.i_country and (upper(b.e_country_name) like '%$cari%' or upper(a.i_teritori) like '%$cari%' or upper(a.e_teritori_name) like '%$cari%') order by a.i_teritori",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacacountry($num,$offset)
    {
		$this->db->select("i_country, e_country_name from tr_country order by i_country",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.*, b.e_country_name from tr_teritori a, tr_country b where (upper(a.e_teritori_name) like '%$cari%' or upper(a.i_teritori) like '%$cari%' or upper(b.e_country_name) like '%$cari%' or upper(a.i_country) like '%$cari%') and a.i_country=b.i_country order by a.i_teritori",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricountry($cari,$num,$offset)
    {
		$this->db->select("i_country, e_country_name from tr_country where upper(e_country_name) like '%$cari%' or upper(i_country) like '%$cari%' order by i_country",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
