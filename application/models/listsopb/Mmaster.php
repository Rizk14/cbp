<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		  #$this->CI =& get_instance();
    }
    public function delete($isopb, $icustomer) 
    {
			$this->db->query("update tm_sopb set f_sopb_cancel='t' WHERE i_sopb='$isopb' and i_customer='$icustomer'");
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.* from tm_stockopname a, tr_area b 
				where a.i_stockopname like '%$cari%'
				and a.i_store = b.i_store
				and a.i_area=b.i_area
				and (b.i_area = '$area1' or b.i_area = '$area2' or
					 b.i_area = '$area3' or b.i_area = '$area4' or
					 b.i_area = '$area5') order by i_stockopname desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.* from tm_stockopname a, tr_area b 
				where a.i_stockopname like '%$cari%'
				and a.i_store = b.i_store
				and (b.i_area = '$area1' or b.i_area = '$area2' or
					 b.i_area = '$area3' or b.i_area = '$area4' or
					 b.i_area = '$area5') order by i_stockopname desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
#    function bacaperiode($iuser,$icustomer,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($iuser,$icustomer,$dfrom,$dto,$num,$offset,$cari)
    {
      $iarea  =$this->session->userdata("i_area");
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($iuser==''){
			  if($iarea=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		      $this->db->select(" a.*, b.e_customer_name from tm_sopb a, tr_customer b
							                where (a.i_sopb like '%$cari%' or a.i_customer like '%$cari%' or b.e_customer_name like '%$cari%')
							                and a.i_customer=b.i_customer and
							                a.d_sopb >= to_date('$dfrom','dd-mm-yyyy') AND
							                a.d_sopb <= to_date('$dto','dd-mm-yyyy')
							                order by a.i_area, i_customer, a.i_sopb ",false)->limit($num,$offset);
        }else{
		      $this->db->select(" a.*, b.e_customer_name from tm_sopb a, tr_customer b
							                where (a.i_sopb like '%$cari%' or a.i_customer like '%$cari%' or b.e_customer_name like '%$cari%')
							                and a.i_area = '$iarea'
                              and a.i_customer=b.i_customer and
							                a.d_sopb >= to_date('$dfrom','dd-mm-yyyy') AND
							                a.d_sopb <= to_date('$dto','dd-mm-yyyy')
							                order by a.i_area, i_customer, a.i_sopb ",false)->limit($num,$offset);
        }
      }else{
		    $this->db->select(" a.*, b.e_customer_name from tm_sopb a, tr_customer b
							              where (a.i_sopb like '%$cari%' or a.i_customer like '%$cari%' or b.e_customer_name like '%$cari%')
							              and a.i_customer = '$icustomer'
                            and a.i_customer=b.i_customer and
							              a.d_sopb >= to_date('$dfrom','dd-mm-yyyy') AND
							              a.d_sopb <= to_date('$dto','dd-mm-yyyy')
							              order by a.i_area, i_customer, a.i_sopb ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.* from tm_stockopname a, tr_area b 
							where a.i_stockopname like '%$cari%'
							and a.i_store = b.i_store
              and a.i_area = b.i_area
							and a.i_area='$iarea' and
							a.d_stockopname >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_stockopname <= to_date('$dto','dd-mm-yyyy')
							order by a.i_stockopname desc  ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
