<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$prevdate,$th,$prevth,$bl,$iuser)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("  i_area ,e_area_name ,i_salesman , e_salesman_name,
							 sum(a.vol) as vol , sum(a.ec) as ec , sum(a.oa)  as oa , sum(a.ob) as ob, 
							 sum(a.voltot) as voltot , sum(a.ectot) as ectot , sum(a.oatot)  as oatot , sum(a.obtot) as obtot, 
							 sum(volbefore) as volbefore , sum(ecbefore) as ecbefore , sum(oabefore) as oabefore ,sum(obbefore) as obbefore,
							 sum(volbeforetot) as volbeforetot , sum(ecbeforetot) as ecbeforetot , sum(oabeforetot) as oabeforetot ,sum(obbeforetot) as obbeforetot,
							 sum(v_target) as v_target , sum(v_spb) as v_spb, sum(v_nota) as v_nota , 
							 sum(v_targettot) as v_targettot , sum(v_spbtot) as v_spbtot, sum(v_notatot) as v_notatot,
							 sum(v_targetbefore) as v_targetbefore , sum(v_spbbefore) as v_spbbefore, sum(v_notabefore) as v_notabefore,
							 sum(v_targetbeforetot) as v_targetbeforetot , sum(v_spbbeforetot) as v_spbbeforetot, sum(v_notabeforetot) as v_notabeforetot
							 from (
							-------------------------------------------------jumlah bulan sekarang---------------
							select i_area ,e_area_name ,i_salesman , e_salesman_name ,
							sum(vol) as vol , 0 as ec , 0 as oa , 0 as ob, 
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from ( 
							select a.i_area ,d.e_area_name , b.i_salesman ,c.e_salesman_name ,sum(a.n_deliver) as vol from tm_nota_item a , tm_nota b , tr_salesman c , tr_area d
							where b.f_nota_cancel='false'
							and to_char(a.d_nota,'yyyymm')='$thbl'
							and a.i_nota=b.i_nota
							and a.i_area=b.i_area
							and a.i_area=d.i_area
							and b.i_salesman=c.i_salesman
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by b.i_salesman , a.i_area,c.e_salesman_name , d.e_area_name
							) as a group by i_salesman , i_area ,e_salesman_name, e_area_name

							union all 

							select i_area,e_area_name, i_salesman ,e_salesman_name  ,
							0 as vol ,count(ec) as ec, 0 as oa , 0 as ob, 
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT  distinct on (a.i_customer, to_char(a.d_nota,'yyyymmdd'))  a.d_nota as ec  ,a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b , tr_area c
							where to_char(a.d_nota,'yyyymm')='$thbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by  a.d_nota, a.i_customer , a.i_salesman , a.i_area,b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all 

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol  ,0 as ec ,count(oa) as oa, 0 as ob, 
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oa, a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from  tm_nota a ,tr_salesman b ,tr_area c 
							where to_char(a.d_nota,'yyyymm')='$thbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,count(ob) as ob ,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_area, a.i_customer)  a.i_customer as ob, a.i_salesman , a.i_area, b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b, tr_area c
							where to_char(a.d_nota,'yyyy') = '$th' 
							and to_char(a.d_nota,'yyyymm')<= '$thbl' 
							and a.f_nota_cancel='f' 
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyymm') , b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area ,e_salesman_name , e_area_name
							-----------------------------------------------------total bulannya-------------------
							union all 

							select i_area ,e_area_name ,i_salesman , e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa , 0 as ob,
							sum(voltot) as voltot, 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from ( 
							select a.i_area ,d.e_area_name , b.i_salesman ,c.e_salesman_name ,sum(a.n_deliver) as voltot from tm_nota_item a , tm_nota b , tr_salesman c , tr_area d
							where b.f_nota_cancel='false'
							and to_char(a.d_nota,'yyyy')='$th'
							and to_char(a.d_nota,'yyyymm')<='$thbl'
							and a.i_nota=b.i_nota
							and a.i_area=b.i_area
							and a.i_area=d.i_area
							and b.i_salesman=c.i_salesman
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by b.i_salesman , a.i_area,c.e_salesman_name , d.e_area_name
							) as a group by i_salesman , i_area ,e_salesman_name, e_area_name

							union all 

							select i_area,e_area_name, i_salesman ,e_salesman_name  ,
							0 as vol ,0 as ec, 0 as oa , 0 as ob, 
							0 as voltot , count(ectot) as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT  distinct on (a.i_customer,to_char(a.d_nota,'yyyymmdd'))  a.d_nota as ectot  ,a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b , tr_area c
							where to_char(a.d_nota,'yyyy')='$th'
							and to_char(a.d_nota,'yyyymm')<='$thbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by  a.d_nota, a.i_customer , a.i_salesman , a.i_area,b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all 

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol  ,0 as ec ,0 as oa, 0 as ob,
							0 as voltot , 0 as ectot , count(oatot) as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oatot, a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from  tm_nota a ,tr_salesman b ,tr_area c 
							where to_char(a.d_nota,'yyyy')='$th'
							and to_char(a.d_nota,'yyyymm')<='$thbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							 0 as vol , 0 as ec , 0 as oa ,0 as ob , 
							 0 as voltot , 0 as ectot , 0 as oatot , count(obtot) as obtot, 
							 0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							 0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot,
							 0 as v_target ,0 as v_spb,0 as v_nota,
							 0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_area, a.i_customer)  a.i_customer as obtot, a.i_salesman , a.i_area, b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b, tr_area c
							where to_char(a.d_nota,'yyyy') = '$th' 
							and to_char(a.d_nota,'yyyymm')<='$thbl'
							and a.f_nota_cancel='false' 
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyymm') , b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area ,e_salesman_name , e_area_name
							-----------------------------------------------------------------------tahun sebelumnya -------------------------------------------------
							union all

							select i_area ,e_area_name ,i_salesman , e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa , 0 as ob, 
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							sum(volbefore) as volbefore ,0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from( 
							select a.i_area ,d.e_area_name , b.i_salesman ,c.e_salesman_name ,sum(a.n_deliver) as volbefore from tm_nota_item a , tm_nota b , tr_salesman c , tr_area d
							where b.f_nota_cancel='false'
							and to_char(a.d_nota,'yyyymm')='$prevthbl'
							and a.i_nota=b.i_nota
							and a.i_area=b.i_area
							and a.i_area=d.i_area
							and b.i_salesman=c.i_salesman
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by b.i_salesman , a.i_area,c.e_salesman_name , d.e_area_name
							) as a group by i_salesman , i_area ,e_salesman_name, e_area_name

							union all 

							select i_area,e_area_name, i_salesman ,e_salesman_name  ,
							0 as vol ,0 as ec, 0 as oa , 0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore,count(ecbefore) as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (a.i_customer, to_char(a.d_nota,'yyyymmdd'))  a.d_nota as ecbefore  ,a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b , tr_area c
							where to_char(a.d_nota,'yyyymm')='$prevthbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by  a.d_nota, a.i_customer , a.i_salesman , a.i_area,b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all 

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol  ,0 as ec ,0 as oa, 0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore,count(oabefore) as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oabefore, a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from  tm_nota a ,tr_salesman b ,tr_area c 
							where to_char(a.d_nota,'yyyymm')='$prevthbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							 0 as vol , 0 as ec , 0 as oa ,0 as ob,
							 0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							 0 as volbefore, 0 as ecbefore, 0 as oabefore,count(obbefore)as obbefore,
							 0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot ,
							 0 as v_target ,0 as v_spb,0 as v_nota,
							 0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_area, a.i_customer)  a.i_customer as obbefore, a.i_salesman , a.i_area, b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b, tr_area c
							where to_char(a.d_nota,'yyyy') = '$prevth' 
							and to_char(a.d_nota,'yyyymm') <= '$prevthbl' 
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							and a.f_nota_cancel='f' 
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							group by a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyymm') , b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area ,e_salesman_name , e_area_name

							-------------------------------------------------------------total tahun sbelumnya --------------------------------
							union all

							select i_area ,e_area_name ,i_salesman , e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa , 0 as ob, 
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore ,0 as ecbefore, 0 as oabefore,0 as obbefore,
							sum(volbeforetot) as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from( 
							select a.i_area ,d.e_area_name , b.i_salesman ,c.e_salesman_name ,sum(a.n_deliver) as volbeforetot from tm_nota_item a , tm_nota b , tr_salesman c , tr_area d
							where b.f_nota_cancel='false'
							and (to_char(a.d_nota,'yyyy')='$prevth' 
							and to_char(a.d_nota,'yyyymm') <= '$prevthbl')
							and a.i_nota=b.i_nota
							and a.i_area=b.i_area
							and a.i_area=d.i_area
							and b.i_salesman=c.i_salesman
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by b.i_salesman , a.i_area,c.e_salesman_name , d.e_area_name
							) as a group by i_salesman , i_area ,e_salesman_name, e_area_name

							union all 

							select i_area,e_area_name, i_salesman ,e_salesman_name  ,
							0 as vol ,0 as ec, 0 as oa , 0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore,0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, count(ecbeforetot) as ecbeforetot, 0 as oabeforetot,0 as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (a.i_customer, to_char(a.d_nota,'yyyymmdd'))  a.d_nota as ecbeforetot  ,a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b , tr_area c
							where to_char(a.d_nota,'yyyy')='$prevth'
							and to_char(a.d_nota,'yyyymm')<='$prevthbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by  a.d_nota, a.i_customer , a.i_salesman , a.i_area,b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all 

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol  ,0 as ec ,0 as oa, 0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore,0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, count(oabeforetot) as oabeforetot,0 as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(
							SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oabeforetot, a.i_salesman , a.i_area , b.e_salesman_name , c.e_area_name from  tm_nota a ,tr_salesman b ,tr_area c 
							where to_char(a.d_nota,'yyyy')='$prevth'
							and to_char(a.d_nota,'yyyymm')<='$prevthbl'
							and a.f_nota_cancel='false'
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							) as a
							group by i_salesman , i_area,e_salesman_name, e_area_name

							union all

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,count(obbeforetot) as obbeforetot,
							0 as v_target ,0 as v_spb,0 as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from (
							SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_area, a.i_customer)  a.i_customer as obbeforetot, a.i_salesman , a.i_area, b.e_salesman_name , c.e_area_name from tm_nota a , tr_salesman b, tr_area c
							where to_char(a.d_nota,'yyyy') = '$prevth' 
							and to_char(a.d_nota,'yyyymm')<= '$prevthbl' 
							and a.f_nota_cancel='f' 
							and a.i_salesman=b.i_salesman
							and a.i_area=c.i_area
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyymm') , b.e_salesman_name, c.e_area_name
							) as a
							group by i_salesman , i_area ,e_salesman_name , e_area_name
							union all
                                                   ---------------------------------------menghitung nilai Target SPB NOTA tahun Sekarang----------

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot, 
							sum(v_target) as v_target , sum(v_spb) as v_spb,sum(v_nota) as v_nota,
							0 as v_targettot , 0 as v_spbtot,0 as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(

							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, a.v_target as v_target, 0 as v_spb, 0 as v_nota
							from tm_target_itemsls a, tr_area b, tr_salesman c 
							where a.i_periode = '$thbl' 
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_target, sum(v_spb-v_spb_discounttotal) as v_spb, 
							0 as v_nota
							from tm_spb a, tr_area b, tr_salesman c 
							where to_char(d_spb,'yyyymm') = '$thbl' and f_spb_cancel='f'
							and a.i_area=b.i_area
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_target, 0 as v_spb, 
							sum(v_nota_netto) as v_nota
							from tm_nota a, tr_area b, tr_salesman c 
							where to_char(d_nota,'yyyymm') = '$thbl' 
							and f_nota_cancel='f'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							) as a group by a.i_area, a.i_salesman, a.e_area_name, a.e_salesman_name

							union all

							-------------------menghitung total target SPB NOTA tahun sekarang-----------------

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot, 
							0 as v_target , 0 as v_spb,0 as v_nota ,
							sum(v_targettot) as v_targettot , sum(v_spbtot) as v_spbtot,sum(v_notatot) as v_notatot, 
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore ,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(

							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, sum(a.v_target) as v_targettot, 0 as v_spbtot, 0 as v_notatot
							from tm_target_itemsls a, tr_area b, tr_salesman c 
							where a.i_periode like  '%$th%' 
							and a.i_periode<='$thbl'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name 
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targettot, sum(v_spb-v_spb_discounttotal) as v_spbtot, 
							0 as v_notatot
							from tm_spb a, tr_area b, tr_salesman c 
							where to_char(d_spb,'yyyy') = '$th'
							and to_char(d_spb,'yyyymm') <= '$thbl'
							and f_spb_cancel='f'
							and a.i_area=b.i_area
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targettot, 0 as v_spbtot, 
							sum(v_nota_netto) as v_notatot
							from tm_nota a, tr_area b, tr_salesman c 
							where to_char(d_nota,'yyyy') = '$th' 
							and to_char(d_nota,'yyyymm') <= '$thbl' 
							and f_nota_cancel='f'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							) as a group by a.i_area, a.i_salesman, a.e_area_name, a.e_salesman_name

							union all
							
							 ---------------------------------------menghitung nilai Target SPB NOTA tahun sebelumnya----------

							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot, 
							0 as v_target , 0 as v_spb,0 as v_nota ,
							0 as v_targettot ,0 as v_spbtot,0 as v_notatot,
							sum(v_targetbefore) as v_targetbefore , sum(v_spbbefore) as v_spbbefore,sum(v_notabefore) as v_notabefore,
							0 as v_targetbeforetot , 0 as v_spbbeforetot,0 as v_notabeforetot from(

							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, a.v_target as v_targetbefore, 0 as v_spbbefore, 0 as v_notabefore
							from tm_target_itemsls a, tr_area b, tr_salesman c 
							where a.i_periode = '$prevthbl' 
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targetbefore, sum(v_spb-v_spb_discounttotal) as v_spbbefore, 
							0 as v_notabefore
							from tm_spb a, tr_area b, tr_salesman c 
							where to_char(d_spb,'yyyymm') = '$prevthbl' and f_spb_cancel='f'
							and a.i_area=b.i_area
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targetbefore, 0 as v_spbbefore, 
							sum(v_nota_netto) as v_notabefore
							from tm_nota a, tr_area b, tr_salesman c 
							where to_char(d_nota,'yyyymm') = '$prevthbl' 
							and f_nota_cancel='f'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							) as a group by a.i_area, a.i_salesman, a.e_area_name, a.e_salesman_name

							union all
							-----------------------------------menghitung total Target,SPB,NOTA tahun sebelumnya---------------- 
							select i_area , e_area_name ,i_salesman, e_salesman_name ,
							0 as vol , 0 as ec , 0 as oa ,0 as ob,
							0 as voltot , 0 as ectot , 0 as oatot , 0 as obtot, 
							0 as volbefore, 0 as ecbefore, 0 as oabefore,0 as obbefore,
							0 as volbeforetot, 0 as ecbeforetot, 0 as oabeforetot,0 as obbeforetot, 
							0 as v_target , 0 as v_spb,0 as v_nota ,
							0 as v_targettot ,0 as v_spbtot,0 as v_notatot,
							0 as v_targetbefore , 0 as v_spbbefore,0 as v_notabefore,
							sum(v_targetbeforetot) as v_targetbeforetot , sum(v_spbbeforetot) as v_spbbeforetot,sum(v_notabeforetot) as v_notabeforetot from(

							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, sum(a.v_target) as v_targetbeforetot, 0 as v_spbbeforetot, 0 as v_notabeforetot
							from tm_target_itemsls a, tr_area b, tr_salesman c 
							where a.i_periode like  '%$prevth%' 
							and a.i_periode<='$prevthbl'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name 
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targetbeforetot, sum(v_spb-v_spb_discounttotal) as v_spbbeforetot, 
							0 as v_notabeforetot
							from tm_spb a, tr_area b, tr_salesman c 
							where to_char(d_spb,'yyyy') = '$prevth'
							and to_char(d_spb,'yyyymm') <= '$prevthbl'
							and f_spb_cancel='f'
							and a.i_area=b.i_area
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							
							union all
							
							select a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name, 0 as v_targetbeforetot, 0 as v_spbbeforetot, 
							sum(v_nota_netto) as v_notabeforetot
							from tm_nota a, tr_area b, tr_salesman c 
							where to_char(d_nota,'yyyy') = '$prevth' 
							and to_char(d_nota,'yyyymm') <= '$prevthbl' 
							and f_nota_cancel='f'
							and a.i_area=b.i_area 
							and a.i_salesman=c.i_salesman 
							and b.f_area_real='t'
							and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
							group by a.i_area, a.i_salesman, b.e_area_name, c.e_salesman_name
							) as a group by a.i_area, a.i_salesman, a.e_area_name, a.e_salesman_name

							) as a
							group by i_salesman , i_area ,e_salesman_name , e_area_name order by  i_area,i_salesman 

						
						",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function user()
    {
        $this->db->select("  * from tm_user",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
