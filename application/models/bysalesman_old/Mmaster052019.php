<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$prevdate,$th,$prevth,$bl,$iuser,$akhir,$prevakhir)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;

	    $this->db->select(" a.i_area , b.e_area_name ,a.i_salesman , c.e_salesman_name ,sum(a.vtargetcoll) as vtargetcoll , sum(a.vrealisasicoll) as vrealisasicoll ,
sum(a.vtargetsls) as vtargetsls, sum(a.ob) as ob, sum(a.oa) as oa , sum(a.qty) as qty , sum(a.netsales) as netsales,
sum(a.vtargetcollprev) as vtargetcollprev, sum(a.vrealisasicollprev) as vrealisasicollprev , 
sum(a.vtargetslsprev) as vtargetslsprev,sum(a.obprev) as obprev, sum(a.oaprev) as oaprev , sum(a.qtyprev) as qtyprev , sum(a.netsalesprev) as netsalesprev from (
select i_area , i_salesman , sum(v_target_tagihan) as vtargetcoll , sum(v_realisasi_tagihan) as vrealisasicoll ,
0 as vtargetsls, 0 as ob, 0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev 
from  f_target_collection_rekapkodealokasi('$thbl','$akhir')
group by i_area, i_salesman
UNION ALL
select i_area , i_salesman , 0 as vtargetcoll , 0 as vtargetcoll , sum(v_target) as vtargetsls, 0 as ob, 0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev
from tm_target_itemsls
where  i_periode ='$thbl' 
group by i_area , i_salesman
UNION ALL
select i_area , i_salesman , 0 as vtargetcoll , 0 as vtargetcoll ,0 as vtargetsls, count(ob) as ob, 0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev from (
SELECT distinct on (a.i_customer)  a.i_customer as ob, a.i_salesman , a.i_area from tm_nota a 
where to_char(a.d_nota,'yyyymm')<= '$thbl' 
and a.f_nota_cancel='f' and not a.i_nota isnull
group by a.i_customer, a.i_area, a.i_salesman
) as a
group by i_salesman , i_area
UNION ALL
select i_area , i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,count(oa) as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev from(
SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oa, a.i_salesman , a.i_area from  tm_nota a 
where to_char(a.d_nota,'yyyymm')='$thbl'
and a.f_nota_cancel='false' and not a.i_nota isnull
) as a
group by i_salesman , i_area
UNION ALL
select a.i_area , b.i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,0 as oa ,sum(a.n_deliver) as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev
from tm_nota_item a , tm_nota b
where b.f_nota_cancel='false'
and to_char(a.d_nota,'yyyymm')='$thbl' and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
group by b.i_salesman , a.i_area
UNION ALL
select a.i_area, a.i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,0 as oa ,0 as qty ,sum(v_nota_netto) as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev
from tm_nota a
where to_char(d_nota,'yyyymm')='$thbl' 
and f_nota_cancel='f' and not i_nota isnull
group by a.i_area, a.i_salesman
UNION ALL
--================================================ PRev Year ==============================================
select i_area , i_salesman ,0 as vtargetcoll , 0 as vrealisasicoll , 0 as vtargetsls, 0 as ob, 0 as oa , 0 as qty , 0 as netsales, 
sum(v_target_tagihan) as vtargetcollprev, sum(v_realisasi_tagihan) as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev
from  f_target_collection_rekapkodealokasi('$prevthbl','$prevakhir')
group by i_area, i_salesman
UNION ALL
select i_area , i_salesman , 0 as vtargetcoll , 0 as vtargetcoll , 0 as vtargetsls, 0 as ob, 0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , sum(v_target) as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev
from tm_target_itemsls
where  i_periode ='$prevthbl' 
group by i_area , i_salesman
UNION ALL
select i_area , i_salesman , 0 as vtargetcoll , 0 as vtargetcoll ,0 as vtargetsls, 0 as ob, 0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,count(ob) as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev from (
SELECT distinct on (to_char(a.d_nota,'yyyy'), a.i_area, a.i_customer)  a.i_customer as ob, a.i_salesman , a.i_area from tm_nota a 
where to_char(a.d_nota,'yyyymm')<= '$prevthbl' 
and a.f_nota_cancel='f' and not a.i_nota isnull
group by a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyy')
) as a
group by i_salesman , i_area
UNION ALL
select i_area , i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,0 as oa , 0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, count(oa) as oaprev , 0 as qtyprev , 0 as netsalesprev from(
SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer as oa, a.i_salesman , a.i_area from  tm_nota a 
where to_char(a.d_nota,'yyyymm')='$prevthbl'
and a.f_nota_cancel='false' and not a.i_nota isnull
) as a
group by i_salesman , i_area
UNION ALL
select a.i_area , b.i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,0 as oa ,0 as qty , 0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , sum(a.n_deliver) as qtyprev , 0 as netsalesprev
from tm_nota_item a , tm_nota b
where b.f_nota_cancel='false'
and to_char(a.d_nota,'yyyymm')='$prevthbl' and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
group by b.i_salesman , a.i_area
UNION ALL
select a.i_area, a.i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,0 as oa ,0 as qty ,0 as netsales,
0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , sum(v_nota_netto) as netsalesprev
from tm_nota a
where to_char(d_nota,'yyyymm')='$prevthbl' 
and f_nota_cancel='f' and not i_nota isnull
group by a.i_area, a.i_salesman

) as a
left join tr_area b on (a.i_area=b.i_area and b.f_area_real='t' )
left join tr_salesman c on (a.i_salesman=c.i_salesman)
group by a.i_area , a.i_salesman,b.e_area_name ,c.e_salesman_name
order by i_area , i_salesman ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function user()
    {
        $this->db->select("  * from tm_user",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
