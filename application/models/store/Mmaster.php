<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($istore)
    {
		$query=$this->db->query("select i_store, e_store_name, to_char(d_store_register,'dd-mm-yyyy') as d_store_register 
						  from tr_store where i_store='$istore'");
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($istore, $estorename, $dstoreregister)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_store (i_store, e_store_name, d_store_register, d_store_entry) values ('$istore', '$estorename',to_date('$dstoreregister','dd-mm-yyyy'), '$dentry')");
		redirect('store/cform/');
    }
    function update($istore, $estorename, $dstoreregister)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_store set e_store_name = '$estorename', d_store_register = to_date('$dstoreregister','dd-mm-yyyy'), d_store_update = '$dupdate' where i_store = '$istore'");
		redirect('store/cform/');
    }
	
    public function delete($istore) 
    {
		$this->db->query('DELETE FROM tr_store WHERE i_store=\''.$istore.'\'');
		return TRUE;
    }
    function bacasemua($num,$offset)
    {
		$this->db->select("i_store, e_store_name, to_char(d_store_register,'dd-mm-yyyy') as d_store_register from tr_store order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_store, e_store_name, to_char(d_store_register,'dd-mm-yyyy') as d_store_register from tr_store where upper(e_store_name) like '%$cari%' or upper(i_store) like '%$cari%' order by i_store",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
