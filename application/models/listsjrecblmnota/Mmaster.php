<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj,$iarea) 
    {
			$this->db->query(" update tm_nota set f_nota_cancel='t' WHERE i_sj='$isj' and i_area='$iarea'");
			$this->db->query(" update tm_spb set f_spb_cancel='t' WHERE i_sj='$isj' and i_area='$iarea'");
    }
    function bacasemua($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
								where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
								and (upper(a.i_area_from) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.d_sj_receive",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name c.e_customer_name from tm_nota a, tr_area b, tr_customer c
						where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%') order by a.d_sj_receive",false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
						where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%')
						order by a.d_sj_receive",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function bacaarea($num,$offset,$area1,$iuser)
    {
			if($area1=='00'){
				$this->db->select(" * from tr_area order by i_area",false)->limit($num,$offset);;
			}else{
				$this->db->select(" * from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser') order by i_area",false)->limit($num,$offset);;
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$iuser)
    {
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')order by i_area",false)->limit($num,$offset);;
			}else{
				$query = $this->db->query("select * from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser')
											(upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')order by i_area",false)->limit($num,$offset);;
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($iarea,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
        if($iarea=='NA'){
			    $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
							  where a.i_area=b.i_area and a.i_customer=c.i_customer
							  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							  and not a.d_sj_receive is null and a.f_nota_cancel='f' and a.i_nota is null 
							  order by a.d_sj_receive, a.i_area",false)->limit($num,$offset);//and not a.d_sj_receive isnull and a.i_nota isnull  and a.f_nota_cancel='f'
        }else{
			    $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
							  where a.i_area=b.i_area and a.i_customer=c.i_customer
							  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							  and (substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') )
							  and not a.d_sj_receive isnull and a.i_nota isnull  and a.f_nota_cancel='f'
							   order by a.d_sj_receive ",false)->limit($num,$offset);
				}
      }else{
			  $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
													  where a.i_area=b.i_area and a.i_customer=c.i_customer
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
													  and (substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') ) and
													  not a.d_sj_receive isnull and a.i_nota isnull  and a.f_nota_cancel='f'
													    order by a.d_sj_receive",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
													where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
													and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
													and a.i_area_to='$iarea' and
													a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  order by a.d_sj_receive",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
