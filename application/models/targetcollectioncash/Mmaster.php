<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
		  $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, sum(b.bayar+b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 
                          0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.bayar+b.sisa) as totalnon, 
                          sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function simpan($iperiode)
    {
      $this->db->query("delete from tm_collection_cash where e_periode='$iperiode'");
      $this->db->select("	* from vcollectioncash",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $this->db->query("insert into tm_collection_cash values('$iperiode', '$row->i_nota', '$row->d_nota', '$row->d_jatuh_tempo', 
                            '$row->f_insentif', '$row->i_salesman', '$row->i_area', '$row->i_customer', $row->v_nota_netto, 
                            $row->bayar, $row->sisa)");
        }
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	x.i_area, x.e_area_name, x.i_customer, x.e_customer_name, x.i_salesman, x.e_salesman_name, x.i_nota, x.d_nota, 
                          sum(x.total) as total, sum(x.realisasi) as realisasi, sum(x.totalnon) as totalnon, 
                          sum(x.realisasinon) as realisasinon
                          from (
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota, 
                          sum(b.bayar+b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota, 
                          0 as total, 0 as realisasi, 
                          sum(b.bayar+b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota
                          ) as x                          
                          group by x.i_area, x.e_area_name, x.i_customer, x.e_customer_name, x.i_salesman, x.e_salesman_name, x.i_nota, 
                          x.d_nota
                          order by x.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)

    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
