<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	/* function bacasemua($iarea, $cari, $num, $offset)
	{
		if ($this->session->userdata('level') == '0') {
			$sql = " 	a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin,
					  a.e_product_name, a.n_quantity_stock, a.f_product_active, c.e_product_motifname, b.e_area_name 
					  from tm_ic a, tr_area b, tr_product_motif c 
					  where a.i_store=b.i_store and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
					  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
					  order by B.I_AREA, a.i_product";
		} else {
			$sql = " 	a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin,
					  a.e_product_name, a.n_quantity_stock, a.f_product_active, c.e_product_motifname, b.e_area_name
					  from tm_ic a, tr_area b, tr_product_motif c 
					  where a.i_store=b.i_store and b.i_area='$iarea'
					  and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
					  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
					  order by B.I_AREA, a.i_product";
		}
		$this->db->select($sql, false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($iarea, $cari, $num, $offset)
	{
		if ($this->session->userdata('level') == '0') {
			$sql = " 	a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin,
					a.e_product_name, a.n_quantity_stock, a.f_product_active, c.e_product_motifname, b.e_area_name
					from tm_ic a, tr_area b, tr_product_motif c 
					where a.i_store=b.i_store and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
					and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
					order by B.I_AREA, a.i_product";
		} else {
			$sql = " 	a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin,
					a.e_product_name, a.n_quantity_stock, a.f_product_active, c.e_product_motifname, b.e_area_name
					from tm_ic a, tr_area b, tr_product_motif c 
					where a.i_store=b.i_store and b.i_area='$iarea'
					and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
					and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
					order by B.I_AREA, a.i_product";
		}
		$this->db->select($sql, false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	} */
	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select(" distinct on (b.i_store) b.i_store, b.e_store_name, c.i_store_location, c.e_store_locationname, a.i_area
                          from tr_area a, tr_store b, tr_store_location c
                          where a.i_store=b.i_store and b.i_store=c.i_store and 
                          (a.i_area in (select i_area from tm_user_area where i_user='$iuser') )
                          order by b.i_store, c.i_store_location", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $iuser)
	{
		$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname, a.i_area
                           from tr_area a, tr_store b, tr_store_location c
                           where a.i_store=b.i_store and b.i_store=c.i_store 
                           	and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						            and (a.i_area in (select i_area from tm_user_area where i_user='$iuser') ) order by a.i_store ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($iarea, $istore, $istorelocation, $num, $offset, $cari)
	{
		$this->db->select("		a.i_product,
								a.i_product_motif,
								a.i_product_grade,
								a.i_store,
								a.i_store_location,
								a.i_store_locationbin,
								a.e_product_name,
								a.n_quantity_stock,
								a.f_product_active,
								c.e_product_motifname,
								b.e_store_name AS e_area_name,
								ctg.e_sales_categoryname 
							FROM
								tm_ic a,
								tr_store b,
								tr_product_motif c,
								tr_product d
								LEFT JOIN tr_product_sales_category ctg ON (d.i_sales_category = ctg.i_sales_category)
							WHERE
								a.i_store = b.i_store
								AND a.i_store = '$istore'
								AND a.i_store_location = '$istorelocation'
								AND a.i_product_motif = c.i_product_motif
								AND a.i_product = c.i_product
								AND a.i_product = d.i_product
								AND (upper(a.i_product) LIKE '%$cari%' OR upper(a.e_product_name) LIKE '%$cari%' OR upper(ctg.e_sales_categoryname) LIKE '%$cari%')
							ORDER BY
								a.e_product_name,
								a.i_product ", false); #->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	/* function cariperiode($iarea, $num, $offset, $cari)
	{
		$this->db->select("	a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin,
							a.e_product_name, a.n_quantity_stock, a.f_product_active, c.e_product_motifname, b.e_area_name
							from tm_ic a, tr_store b, tr_product_motif c 
							where a.i_store=b.i_store and a.i_store='$istore'
							and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
							and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							order by a.e_product_name,a.i_product ", false); #->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	} */
}
