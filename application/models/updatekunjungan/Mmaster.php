<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb,$area) 
    {
		$this->db->query("UPDATE tm_spb set f_spb_cancel='t' WHERE i_spb='$ispb' and i_area='$area'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb desc ";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb desc ";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function simpan($nlama,$dfrom,$dto)
    {
/*
#asalnya#
			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                from (
                                SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                UNION ALL
                                SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                b.i_customer, d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                UNION ALL
                                SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                b.i_customer, d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                ) as a
                                group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                order by a.i_area, a.i_salesman");
*/
			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                from (
                                SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel='f'
                                UNION ALL
                                SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                b.i_customer, d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel='f'
                                GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                UNION ALL
                                SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                b.i_customer, d.e_salesman_name, c.e_area_name
                                FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                WHERE 
                                a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                b.i_area = c.i_area AND
                                b.i_salesman = d.i_salesman AND
                                a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel='f' AND b.i_kunjungan_type='01' AND
                                b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                ) as a
                                group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                order by a.i_area, a.i_salesman");
		  if ($query->num_rows() > 0){
      	$tmp=explode("-",$dfrom);
        $th1=$tmp[0];
        $bl1=$tmp[1];
        $tmp=explode("-",$dto);
        $th2=$tmp[0];
        $iperiode=$th1.$bl1;
        $this->db->query("delete from tm_kunjungan where i_periode='$iperiode'");
			  foreach($query->result() as $row){
          $quer 	= $this->db->query("SELECT current_timestamp as c");
		      $raw   	= $quer->row();
		      $dproses= $raw->c;
          $this->db->query("insert into tm_kunjungan values('$iperiode','$row->i_area','$row->i_salesman',$nlama,
                            $row->jml,$row->jmlreal,'$dproses')");
        }
		  }
#####order
      $query=$this->db->query("	SELECT a.i_area, a.i_salesman, e.e_salesman_name, a.i_spb, a.d_spb, a.i_customer, 
                                c.i_kunjungan_type, c.f_kunjungan_realisasi, a.v_spb, a.v_spb_discounttotal
                                FROM tr_customer b, tr_salesman e, tm_spb a
                                left join tm_rrkh_item c on(a.i_area = c.i_area AND a.i_salesman = c.i_salesman AND c.d_rrkh = a.d_spb AND 
			                                    a.i_customer=c.i_customer AND c.i_kunjungan_type='01' AND 
			                                    c.f_kunjungan_realisasi='t' AND c.f_kunjungan_valid='t')
                                left join tm_rrkh d on(c.i_area=d.i_area AND c.i_salesman=d.i_salesman AND c.d_rrkh=d.d_rrkh AND
		                                       d.d_rrkh>='$dfrom' AND d.d_rrkh<='$dto' AND d.f_rrkh_cancel>='f')
                                WHERE 
                                a.i_customer=b.i_customer AND a.d_spb>='$dfrom' AND a.d_spb<='$dto' AND 
                                a.f_spb_cancel='f' AND a.i_salesman=e.i_salesman");
		  if ($query->num_rows() > 0){
      	$tmp=explode("-",$dfrom);
        $th1=$tmp[0];
        $bl1=$tmp[1];
        $tmp=explode("-",$dto);
        $th2=$tmp[0];
        $iperiode=$th1.$bl1;
        $this->db->query("delete from tm_kunjungan_item where i_periode='$iperiode'");
			  foreach($query->result() as $row){
          $quer 	= $this->db->query("SELECT current_timestamp as c");
		      $raw   	= $quer->row();
		      $dproses= $raw->c;
          if($row->i_kunjungan_type=='' && $row->f_kunjungan_realisasi==''){
            $this->db->query("insert into tm_kunjungan_item values('$iperiode','$row->i_area','$row->i_salesman','$row->i_customer',
                              '$row->e_salesman_name',
                              '$row->i_spb','$row->d_spb',null,null,$row->v_spb,
                              $row->v_spb_discounttotal)");
          }elseif($row->i_kunjungan_type!='' && $row->f_kunjungan_realisasi==''){
            $this->db->query("insert into tm_kunjungan_item values('$iperiode','$row->i_area','$row->i_salesman','$row->i_customer',
                              '$row->e_salesman_name',
                              '$row->i_spb','$row->d_spb','$row->i_kunjungan_type',null,$row->v_spb,
                              $row->v_spb_discounttotal)");
          }elseif($row->i_kunjungan_type=='' && $row->f_kunjungan_realisasi!=''){
            $this->db->query("insert into tm_kunjungan_item values('$iperiode','$row->i_area','$row->i_salesman','$row->i_customer',
                              '$row->e_salesman_name',
                              '$row->i_spb','$row->d_spb',null,'$row->f_kunjungan_realisasi',$row->v_spb,
                              $row->v_spb_discounttotal)");
          }else{
            $this->db->query("insert into tm_kunjungan_item values('$iperiode','$row->i_area','$row->i_salesman','$row->i_customer',
                              '$row->e_salesman_name',
                              '$row->i_spb','$row->d_spb','$row->i_kunjungan_type','$row->f_kunjungan_realisasi',$row->v_spb,
                              $row->v_spb_discounttotal)");
          }
        }
		  }
####
    }
    function bacaperiode($nlama,$dfrom,$dto)
    {
      $tmp=explode("-",$dfrom);
      $th1=$tmp[0];
      $bl1=$tmp[1];
      $hr1=$tmp[2];
      $tmp=explode("-",$dto);
      $th2=$tmp[0];
      $bl2=$tmp[1];
      $hr2=$tmp[2];
      $iperiode=$th1.$bl1;
      if( ($th1==$th2)&&($bl1==$bl2)&&($hr1=='01')&&($hr2=='28'||$hr2=='29'||$hr2=='30'||$hr2=='31') ){
        $query=$this->db->query("	select a.i_periode, a.i_area, a.i_salesman, a.n_jumlah_hari, a.n_jumlah_kunjungan as jml, 
                                  a.n_jumlah_order as jmlreal, b.e_salesman_name, c.e_area_name
                                  from tm_kunjungan a, tr_salesman b, tr_area c 
                                  where a.i_periode='$iperiode' and a.i_area=c.i_area and a.i_salesman=b.i_salesman
                                  order by a.i_area, a.i_salesman");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
/*
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
*/
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }
    function bacaorder($isalesman,$dfrom,$dto)
    {
      $tmp=explode("-",$dfrom);
      $th1=$tmp[2];
      $bl1=$tmp[1];
      $hr1=$tmp[0];
      $tmp=explode("-",$dto);
      $th2=$tmp[2];
      $bl2=$tmp[1];
      $hr2=$tmp[0];
      $iperiode=$th1.$bl1;
      if( ($th1==$th2)&&($bl1==$bl2)&&($hr1=='01')&&($hr2=='28'||$hr2=='29'||$hr2=='30'||$hr2=='31') ){
        $query=$this->db->query("	select a.d_spb, a.i_salesman, a.i_customer, a.i_area, sum(a.v_spb-a.v_discount) as v_spb, b.e_salesman_name,
                                  c.e_area_name, d.e_kunjungan_typename, e.e_customer_name, f.e_customer_classname
                                  from tr_salesman b, tr_area c, tr_customer e, tr_customer_class f, tm_kunjungan_item a
                                  left join tr_kunjungan_type d on(a.i_kunjungan_type=d.i_kunjungan_type)
                                  where a.i_periode='$iperiode' and a.i_area=c.i_area and a.i_salesman=b.i_salesman and a.i_area=b.i_area 
                                  and a.i_salesman='$isalesman' and e.i_customer_class=f.i_customer_class and e.i_customer=a.i_customer
                                  group by a.d_spb, a.i_salesman, a.i_customer, a.i_area, b.e_salesman_name, c.e_area_name,
                                  d.e_kunjungan_typename, e.e_customer_name, f.e_customer_classname
                                  order by a.i_customer, a.d_spb");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
/*
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
*/
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }
    function bacakunjungan($isalesman,$dfrom,$dto)
    {
      $tmp=explode("-",$dfrom);
      $th1=$tmp[2];
      $bl1=$tmp[1];
      $hr1=$tmp[0];
      $tmp=explode("-",$dto);
      $th2=$tmp[2];
      $bl2=$tmp[1];
      $hr2=$tmp[0];
      $iperiode=$th1.$bl1;
      if( ($th1==$th2)&&($bl1==$bl2)&&($hr1=='01')&&($hr2=='28'||$hr2=='29'||$hr2=='30'||$hr2=='31') ){
        $query=$this->db->query("	select a.*, c.e_salesman_name, d.e_customer_name, e.e_kunjungan_typename, f.e_area_name
                                  from tm_rrkh_item a, tm_rrkh b, tr_salesman c, tr_customer d, tr_kunjungan_type e, tr_area f
                                  where a.d_rrkh=b.d_rrkh and a.i_area=b.i_area and a.i_salesman=b.i_salesman and
                                  b.i_salesman=c.i_salesman and b.i_area=c.i_area and a.i_customer=d.i_customer and
                                  b.f_rrkh_cancel='f' and a.i_kunjungan_type='01' and a.f_kunjungan_valid='t' and
                                  a.i_kunjungan_type=e.i_kunjungan_type and a.i_salesman='$isalesman' and b.i_area=f.i_area and
                                  to_char(b.d_rrkh,'yyyymm')='$iperiode' order by a.d_rrkh");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
/*
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
*/
  			$query=$this->db->query("	select sum(jml) as jml, sum(jmlreal) as jmlreal, i_area, i_salesman, e_salesman_name, e_area_name
                                  from (
                                  SELECT 0 as jml, 0 as jmlreal, a.f_rrkh_cancel, a.d_rrkh, b.i_area, b.i_salesman, b.i_customer, 
                                  d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  UNION ALL
                                  SELECT count(*) as jml, 0 as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND b.f_kunjungan_valid='t' AND 
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  UNION ALL
                                  SELECT 0 as jml, count(*) as jmlreal, 'f' as f_rrkh_cancel, Null as d_rrkh, b.i_area, b.i_salesman,
                                  b.i_customer, d.e_salesman_name, c.e_area_name
                                  FROM tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d
                                  WHERE 
                                  a.i_area = b.i_area AND a.i_salesman = b.i_salesman AND a.d_rrkh = b.d_rrkh AND
                                  b.i_area = c.i_area AND
                                  b.i_salesman = d.i_salesman AND b.i_area = d.i_area AND
                                  a.d_rrkh>='$dfrom' AND a.d_rrkh<='$dto' AND a.f_rrkh_cancel>='f' AND b.i_kunjungan_type='01' AND
                                  b.f_kunjungan_realisasi='t' AND b.f_kunjungan_valid='t'
                                  GROUP BY b.i_area, b.i_salesman, b.i_customer, d.e_salesman_name, c.e_area_name
                                  ) as a
                                  group by a.i_area, a.i_salesman, e_salesman_name, e_area_name
                                  order by a.i_area, a.i_salesman");
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }
}
?>
