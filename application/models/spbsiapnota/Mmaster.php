<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispb,$iarea)
    {
		$this->db->select(" * from tm_spb 
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_store on (tm_spb.i_store=tr_store.i_store)
				   inner join tr_store_location on (tm_spb.i_store_location = tr_store_location.i_store_location)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispb,$iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product_motif=b.i_product_motif and a.i_product=b.i_product
				   order by a.i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function insertdetail($ispb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$ndeliver)
    {
    	$this->db->set(
    		array(
					'i_spb'				=> $ispb,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'n_order'			=> $norder,
					'n_deliver'			=> $ndeliver,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname
    		)
    	);
    	
    	$this->db->insert('tm_spb_item');
    }
    function updateheader($ispb,$fspbsiapnota,$iarea)
    {
    	$data = array(
			'f_spb_siapnotagudang'		=> $fspbsiapnota
		            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
    public function deletedetail($iproduct, $iproductgrade, $ispb) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb'
							and i_product='$iproduct' and i_product_grade='$iproductgrade'");
		return TRUE;
    }
    public function delete($ispb) 
    {
		$this->db->query('DELETE FROM tm_spb WHERE i_spb=\''.$ispb.'\'');
		$this->db->query('DELETE FROM tm_spb_item WHERE i_spb=\''.$ispb.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" distinct(b.i_spb) as no ,a.*, c.e_customer_name from tm_spb_item b, tm_spb a, tr_customer c
							where not a.i_approve1 isnull
							and not a.i_approve2 isnull
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_spb_op = 'f'
							and upper(a.i_spb) like '%$cari%' 
							and a.i_spb=b.i_spb and b.n_order>b.n_deliver
							and a.i_customer=c.i_customer
							order by a.i_spb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$kdharga)
    {
		$this->db->select("i_product, i_product_grade, e_product_name, v_product_retail
						   from tr_product_price 
						   where i_price_group = '$kdharga'
						   order by i_product, i_product_grade",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacastore($num,$offset,$area)
    {
		$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area' or i_area='00')
						   and a.i_store=b.i_store
						   order by a.i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_spb,10,6)) as max from tm_spb 
				  			where substr(i_spb,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nospb  =$terakhir+1;
			settype($nospb,"string");
			$a=strlen($nospb);
			while($a<6){
			  $nospb="0".$nospb;
			  $a=strlen($nospb);
			}
			$nospb  ="SPB-".$thbl."-".$nospb;
			return $nospb;
		}else{
			$nospb  ="000001";
			$nospb  ="SPB-".$thbl."-".$nospb;
			return $nospb;
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spb 
							inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer
							and (upper(tr_customer.e_customer_name) like '%$cari%' 
							or upper(tr_customer.i_customer) like '%$cari%'))
							inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
							inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
							inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
							where not tm_spb.i_approve1 isnull and not tm_spb.i_approve2 isnull
							and upper(tm_spb.i_spb) like '%$cari%' 
							order by tm_spb.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caristore($cari,$num,$offset)
    {
		$this->db->select("a.*, b.* from tr_store a, tr_store_location b
						   where a.i_store in(select i_store from tr_area 
						   where i_area='$area' or i_area='00')
						   and a.i_store=b.i_store and upper(a.i_store) like '%cari%' 
						   or upper(a.e_store_name) like '%cari%' order by a.i_store",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.i_store_location, b.e_store_locationname from tr_store a, tr_store_location b 
					where a.i_store=b.i_store
					  and (upper(a.i_store) like '%$cari%' or upper(a.e_store_name) like '%$cari%'
					or upper(b.i_store_location like '%$cari%' or upper(b.e_store_locationname) like '%$cari%') 
					order by a.i_store",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset,$kdstore)
    {
		$this->db->select("  * from tr_product_price
					 where (upper(i_product) like '%$cari%' 
					or upper(e_product_name) like '%$cari%'
					or upper(i_product_grade) like '%$cari%')
					and i_product in(
								 select i_product from tm_ic where i_store = 'AA' or i_store='$kdstore')
					order by i_product, i_product_grade",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
