<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }

    public function bacaperiode($dfrom, $dto)
    {
        $this->db->select(" a.i_coa_bank, a.e_bank_name, a.i_area, a.e_area_name, a.d_bank, a.i_reff, a.e_description, a.i_coa, a.e_coa_name,
                            a.v_debet, a.v_kredit
                            from(
                                /* BANK MASUK DEBET */
                                select	x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and a.i_coa_bank = b.i_coa_bank)
                                inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                                left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv and a.i_coa_bank = x.i_coa_bank)
                                left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                where 
                                a.i_area=e.i_area	and a.d_bank >= to_date('$dfrom','dd-mm-yyyy')
                                AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                                /*and a.i_coa_bank='110-20011' and d.i_bank ='00'*/ AND a.f_kbank_cancel='f'
                                UNION ALL
                                /* BANK KELUAR DEBET */
                                select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' and a.i_coa_bank <> b.i_coa_bank and a.i_coa_bank =b.i_coa)
                                inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                                left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv and a.i_coa_bank <> x.i_coa_bank)
                                left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                where 
                                a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') /*and a.i_coa='110-20011' and d.i_bank <>'00'*/
                                AND a.f_kbank_cancel='f'
                                UNION ALL
                                /* BANK KELUAR KREDIT */
                                select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name
                                from tr_area e, tm_kbank a
                                left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' and a.i_coa_bank = b.i_coa_bank)
                                inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                                left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv and a.i_coa_bank = x.i_coa_bank)
                                left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                where 
                                a.i_area=e.i_area and a.d_bank >= to_date('$dfrom','dd-mm-yyyy')
                                AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                                /*and a.i_coa_bank='110-20011' and d.i_bank ='00'*/ AND a.f_kbank_cancel='f'
                                UNION ALL
                                /* BANK KELUAR KREDIT */
                                select x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and a.i_coa_bank <> b.i_coa_bank and a.i_coa_bank = b.i_coa)
                                inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                                left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv and a.i_coa_bank <> x.i_coa_bank)
                                left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                where 
                                a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') /*and a.i_coa='110-20011' and d.i_bank <>'00'*/
                                AND a.f_kbank_cancel='f'
                            ) as a
                            ORDER BY e_bank_name, d_bank, f_debet, i_kbank ", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function baca_dkbank($dfrom, $dto, $ebankname)
    {
        return $this->db->query("SELECT sum(v_debet) AS v_debet, sum(v_kredit) AS v_kredit
                                    from(
                                        /* BANK MASUK DEBET */
                                        select	x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                        a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                        a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                        left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and a.i_coa_bank = b.i_coa_bank)
                                        inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                                        left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv and a.i_coa_bank = x.i_coa_bank)
                                        left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                        where 
                                        a.i_area=e.i_area	and a.d_bank >= to_date('$dfrom','dd-mm-yyyy')
                                        AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                                        /*and a.i_coa_bank='110-20011' and d.i_bank ='00'*/ AND a.f_kbank_cancel='f'
                                        UNION ALL
                                        /* BANK KELUAR DEBET */
                                        select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                        a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                        a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                        left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' and a.i_coa_bank <> b.i_coa_bank and a.i_coa_bank =b.i_coa)
                                        inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                                        left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv and a.i_coa_bank <> x.i_coa_bank)
                                        left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                        where 
                                        a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') /*and a.i_coa='110-20011' and d.i_bank <>'00'*/
                                        AND a.f_kbank_cancel='f'
                                        UNION ALL
                                        /* BANK KELUAR KREDIT */
                                        select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                        0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                        a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name
                                        from tr_area e, tm_kbank a
                                        left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' and a.i_coa_bank = b.i_coa_bank)
                                        inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                                        left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv and a.i_coa_bank = x.i_coa_bank)
                                        left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                        where 
                                        a.i_area=e.i_area and a.d_bank >= to_date('$dfrom','dd-mm-yyyy')
                                        AND a.d_bank <= to_date('$dto','dd-mm-yyyy')
                                        /*and a.i_coa_bank='110-20011' and d.i_bank ='00'*/ AND a.f_kbank_cancel='f'
                                        UNION ALL
                                        /* BANK KELUAR KREDIT */
                                        select x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description,
                                        0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank,
                                        a.i_coa_bank, d.e_bank_name, a.d_entry, b.e_coa_name from tr_area e, tm_kbank a
                                        left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and a.i_coa_bank <> b.i_coa_bank and a.i_coa_bank = b.i_coa)
                                        inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                                        left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv and a.i_coa_bank <> x.i_coa_bank)
                                        left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                                        where 
                                        a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') /*and a.i_coa='110-20011' and d.i_bank <>'00'*/
                                        AND a.f_kbank_cancel='f'
                                    ) as a
                                    WHERE e_bank_name = '$ebankname'
                                    GROUP BY e_bank_name ", false)->row();

        // $query = $this->db->get();
        // if ($query->num_rows() > 0) {
        //     return $query->result();
        // }
    }

    function bacakb($dfrom, $dto)
    {
        $this->db->select("	'KB' AS e_bank_name, 
                            * FROM (
                                SELECT
                                    x.e_coa_name,
                                    x.d_kb AS d_bank,
                                    x.d_bukti,
                                    CASE WHEN NOT i_rv ISNULL THEN c.i_rv 
                                    ELSE d.i_pv 
                                    END AS i_reff,
                                    x.i_kb,
                                    x.e_description,
                                    x.i_area,
                                    b.e_area_name,
                                    x.i_coa,
                                    x.d_entry,
                                    sum(x.debet) AS v_debet,
                                    sum(x.kredit) AS v_kredit
                                FROM
                                    (
                                        SELECT
                                            d_kb,
                                            d_bukti,
                                            i_kb,
                                            e_description,
                                            i_area,
                                            i_coa,
                                            e_coa_name,
                                            0 AS debet,
                                            v_kb AS kredit,
                                            d_entry
                                        FROM
                                            tm_kb
                                        WHERE
                                            d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                            AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                            AND f_debet = 't'
                                            AND f_kb_cancel = 'f'
                                    UNION ALL
                                        SELECT
                                            d_kb,
                                            d_bukti,
                                            i_kb,
                                            e_description,
                                            i_area,
                                            i_coa,
                                            e_coa_name,
                                            v_kb AS debet,
                                            0 AS kredit,
                                            d_entry
                                        FROM
                                            tm_kb
                                        WHERE
                                            d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                            AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                            AND f_debet = 'f'
                                            AND f_kb_cancel = 'f'
                                    ) AS x
                                INNER JOIN tr_area b ON (x.i_area = b.i_area)
                                LEFT JOIN tm_rv_item c ON (x.i_kb = c.i_kk AND x.i_area = c.i_area_kb)
                                LEFT JOIN tm_pv_item d ON (x.i_kb = d.i_kk AND x.i_area = d.i_area_kb)
                                GROUP BY
                                    x.e_coa_name,
                                    x.d_kb,
                                    x.i_kb,
                                    x.e_description,
                                    x.i_area,
                                    x.i_coa,
                                    b.e_area_name,
                                    x.d_bukti,
                                    c.i_rv,
                                    d.i_pv,
                                    x.d_entry
                            ) AS a
                            ORDER BY
                                a.d_bank, a.d_entry, a.i_reff ", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function baca_dkkb($dfrom, $dto)
    {
        return $this->db->query(" SELECT 'KB' AS e_bank_name, sum(v_debet) AS v_debet, sum(v_kredit) AS v_kredit
                                    FROM (
                                        SELECT
                                            x.e_coa_name,
                                            x.d_kb AS d_bank,
                                            x.d_bukti,
                                            CASE WHEN NOT i_rv ISNULL THEN c.i_rv 
                                            ELSE d.i_pv 
                                            END AS i_reff,
                                            x.i_kb,
                                            x.e_description,
                                            x.i_area,
                                            b.e_area_name,
                                            x.i_coa,
                                            x.d_entry,
                                            sum(x.debet) AS v_debet,
                                            sum(x.kredit) AS v_kredit
                                        FROM
                                            (
                                                SELECT
                                                    d_kb,
                                                    d_bukti,
                                                    i_kb,
                                                    e_description,
                                                    i_area,
                                                    i_coa,
                                                    e_coa_name,
                                                    0 AS debet,
                                                    v_kb AS kredit,
                                                    d_entry
                                                FROM
                                                    tm_kb
                                                WHERE
                                                    d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                                    AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                                    AND f_debet = 't'
                                                    AND f_kb_cancel = 'f'
                                            UNION ALL
                                                SELECT
                                                    d_kb,
                                                    d_bukti,
                                                    i_kb,
                                                    e_description,
                                                    i_area,
                                                    i_coa,
                                                    e_coa_name,
                                                    v_kb AS debet,
                                                    0 AS kredit,
                                                    d_entry
                                                FROM
                                                    tm_kb
                                                WHERE
                                                    d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                                    AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                                    AND f_debet = 'f'
                                                    AND f_kb_cancel = 'f'
                                            ) AS x
                                        INNER JOIN tr_area b ON (x.i_area = b.i_area)
                                        LEFT JOIN tm_rv_item c ON (x.i_kb = c.i_kk AND x.i_area = c.i_area_kb)
                                        LEFT JOIN tm_pv_item d ON (x.i_kb = d.i_kk AND x.i_area = d.i_area_kb)
                                        GROUP BY
                                            x.e_coa_name,
                                            x.d_kb,
                                            x.i_kb,
                                            x.e_description,
                                            x.i_area,
                                            x.i_coa,
                                            b.e_area_name,
                                            x.d_bukti,
                                            c.i_rv,
                                            d.i_pv,
                                            x.d_entry
                                    ) AS a ", false)->row();
        // $query = $this->db->get();
        // if ($query->num_rows() > 0) {
        //     return $query->result();
        // }
    }

    function bacakk($periode, $dfrom, $dto)
    {
        $coaku      = KasKecil;
        $kasbesar   = KasBesar;
        $bank       = Bank;

        $this->db->select("     coa.e_coa_name AS e_bank_name,
                                b.i_kk, b.i_area, e_area_name, b.d_kk, b.e_description, b.i_coa, b.e_coa_name, b.i_reff, b.d_entry,
                                sum(b.v_debet) AS v_debet,
                                sum(b.v_kredit) AS v_kredit
                            FROM
                                (
                                SELECT
                                    a.*,
                                    b.e_area_name,
                                    CASE
                                        WHEN NOT c.i_pv ISNULL THEN c.i_pv
                                        ELSE d.i_rv
                                    END AS i_reff
                                FROM
                                    (
                                    SELECT
                                        i_kb AS i_kk,
                                        d_kb AS d_kk,
                                        f_debet,
                                        e_description,
                                        '' AS i_kendaraan,
                                        0 AS n_km,
                                        0 AS v_debet,
                                        v_kb AS v_kredit,
                                        a.i_area,
                                        d_bukti,
                                        i_coa,
                                        a.e_coa_name,
                                        a.d_entry
                                    FROM
                                        tm_kb a
                                    INNER JOIN tr_area ON
                                        (a.i_area = tr_area.i_area)
                                    WHERE
                                        a.i_periode = '$periode'
                                        AND a.d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                        AND a.f_debet = 't'
                                        AND a.f_kb_cancel = 'f'
                                        AND a.i_coa IN(
                                        SELECT
                                            i_coa
                                        FROM
                                            tr_coa
                                        WHERE
                                            i_area <> ''
                                            AND e_coa_name LIKE '%Kas Kecil%')
                                        AND a.v_kb NOT IN (
                                        SELECT
                                            b.v_kk AS v_kb
                                        FROM
                                            tm_kk b
                                        WHERE
                                            b.d_kk = a.d_kb
                                            AND b.i_area = a.i_area
                                            AND b.f_kk_cancel = 'f'
                                            AND b.f_debet = 'f'
                                            AND b.i_coa LIKE '900-000%'
                                            AND b.i_periode = '$periode')
                                UNION ALL
                                    SELECT
                                        i_kbank AS i_kk,
                                        d_bank AS d_kk,
                                        f_debet,
                                        e_description,
                                        '' AS i_kendaraan,
                                        0 AS n_km,
                                        0 AS v_debet,
                                        v_bank AS v_kredit,
                                        a.i_area,
                                        d_bank AS d_bukti,
                                        i_coa,
                                        a.e_coa_name,
                                        a.d_entry
                                    FROM
                                        tm_kbank a
                                    INNER JOIN tr_area ON
                                        (a.i_area = tr_area.i_area)
                                    WHERE
                                        a.i_periode = '$periode'
                                        AND a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy')
                                        AND a.f_debet = 't'
                                        AND a.f_kbank_cancel = 'f'
                                        AND a.i_coa IN(
                                        SELECT
                                            i_coa
                                        FROM
                                            tr_coa
                                        WHERE
                                            i_area <> ''
                                            AND e_coa_name LIKE '%Kas Kecil%' )
                                        AND a.v_bank NOT IN (
                                        SELECT
                                            b.v_kk AS v_kb
                                        FROM
                                            tm_kk b
                                        WHERE
                                            b.d_kk = a.d_bank
                                            AND b.i_area = a.i_area
                                            AND b.f_kk_cancel = 'f'
                                            AND b.f_debet = 'f'
                                            AND b.i_coa LIKE '110-2%'
                                            AND b.i_periode = '$periode' )
                                UNION ALL
                                    SELECT
                                        a.i_kk AS i_kk,
                                        a.d_kk,
                                        a.f_debet,
                                        a.e_description,
                                        a.i_kendaraan,
                                        a.n_km,
                                        0 AS v_debet,
                                        a.v_kk AS v_kredit,
                                        a.i_area,
                                        d_bukti,
                                        i_coa,
                                        a.e_coa_name,
                                        a.d_entry
                                    FROM
                                        tm_kk a,
                                        tr_area b
                                    WHERE
                                        a.d_kk >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_kk <= to_date('$dto', 'dd-mm-yyyy')
                                        AND a.i_area = b.i_area
                                        AND a.f_kk_cancel = 'f' 
                                        AND a.f_debet = 't'
                                UNION ALL
                                    SELECT
                                        a.i_kk AS i_kk,
                                        a.d_kk,
                                        a.f_debet,
                                        a.e_description,
                                        a.i_kendaraan,
                                        a.n_km,
                                        a.v_kk AS v_debet,
                                        0 AS v_kredit,
                                        a.i_area,
                                        d_bukti,
                                        i_coa,
                                        a.e_coa_name,
                                        a.d_entry
                                    FROM
                                        tm_kk a,
                                        tr_area b
                                    WHERE
                                        a.d_kk >= to_date('$dfrom', 'dd-mm-yyyy')
                                        AND a.d_kk <= to_date('$dto', 'dd-mm-yyyy')
                                        AND a.i_area = b.i_area
                                        AND a.f_kk_cancel = 'f' 
                                        AND a.f_debet = 'f'
                            ) AS a
                                LEFT JOIN tr_area b ON
                                    (a.i_area = b.i_area)
                                LEFT JOIN tm_pv_item c ON
                                    (a.i_kk = c.i_kk
                                        AND a.i_area = c.i_area
                                        AND a.i_coa = c.i_coa)
                                LEFT JOIN tm_rv_item d ON
                                    (a.i_kk = d.i_kk
                                        AND a.i_area = d.i_area
                                        AND a.i_coa = d.i_coa) ) AS b
                                INNER JOIN tr_coa coa ON (b.i_area = coa.i_area AND coa.i_coa LIKE '110-12%')
                            GROUP BY coa.e_coa_name, b.i_kk, b.i_area, e_area_name, b.d_kk, b.e_description, b.i_coa, b.e_coa_name, b.d_entry, b.i_reff
                            ORDER BY
                                b.i_area,
                                b.d_kk,
                                b.d_entry,
                                b.i_reff,
                                b.i_kk ", FALSE);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    function baca_dkkk($periode, $dfrom, $dto, $ebankname)
    {
        $coaku      = KasKecil;
        $kasbesar   = KasBesar;
        $bank       = Bank;

        return $this->db->query("   SELECT coa.e_coa_name AS e_bank_name,
                                        sum(b.v_debet) AS v_debet,
                                        sum(b.v_kredit) AS v_kredit
                                    FROM
                                        (
                                        SELECT
                                            a.*,
                                            b.e_area_name,
                                            CASE
                                                WHEN NOT c.i_pv ISNULL THEN c.i_pv
                                                ELSE d.i_rv
                                            END AS i_reff
                                        FROM
                                            (
                                            SELECT
                                                i_kb AS i_kk,
                                                d_kb AS d_kk,
                                                f_debet,
                                                e_description,
                                                '' AS i_kendaraan,
                                                0 AS n_km,
                                                0 AS v_debet,
                                                v_kb AS v_kredit,
                                                a.i_area,
                                                d_bukti,
                                                i_coa,
                                                a.e_coa_name,
                                                a.d_entry
                                            FROM
                                                tm_kb a
                                            INNER JOIN tr_area ON
                                                (a.i_area = tr_area.i_area)
                                            WHERE
                                                a.i_periode = '$periode'
                                                AND a.d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                                AND a.d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                                AND a.f_debet = 't'
                                                AND a.f_kb_cancel = 'f'
                                                AND a.i_coa IN(
                                                SELECT
                                                    i_coa
                                                FROM
                                                    tr_coa
                                                WHERE
                                                    i_area <> ''
                                                    AND e_coa_name LIKE '%Kas Kecil%')
                                                AND a.v_kb NOT IN (
                                                SELECT
                                                    b.v_kk AS v_kb
                                                FROM
                                                    tm_kk b
                                                WHERE
                                                    b.d_kk = a.d_kb
                                                    AND b.i_area = a.i_area
                                                    AND b.f_kk_cancel = 'f'
                                                    AND b.f_debet = 'f'
                                                    AND b.i_coa LIKE '900-000%'
                                                    AND b.i_periode = '$periode')
                                        UNION ALL
                                            SELECT
                                                i_kbank AS i_kk,
                                                d_bank AS d_kk,
                                                f_debet,
                                                e_description,
                                                '' AS i_kendaraan,
                                                0 AS n_km,
                                                0 AS v_debet,
                                                v_bank AS v_kredit,
                                                a.i_area,
                                                d_bank AS d_bukti,
                                                i_coa,
                                                a.e_coa_name,
                                                a.d_entry
                                            FROM
                                                tm_kbank a
                                            INNER JOIN tr_area ON
                                                (a.i_area = tr_area.i_area)
                                            WHERE
                                                a.i_periode = '$periode'
                                                AND a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy')
                                                AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy')
                                                AND a.f_debet = 't'
                                                AND a.f_kbank_cancel = 'f'
                                                AND a.i_coa IN(
                                                SELECT
                                                    i_coa
                                                FROM
                                                    tr_coa
                                                WHERE
                                                    i_area <> ''
                                                    AND e_coa_name LIKE '%Kas Kecil%' )
                                                AND a.v_bank NOT IN (
                                                SELECT
                                                    b.v_kk AS v_kb
                                                FROM
                                                    tm_kk b
                                                WHERE
                                                    b.d_kk = a.d_bank
                                                    AND b.i_area = a.i_area
                                                    AND b.f_kk_cancel = 'f'
                                                    AND b.f_debet = 'f'
                                                    AND b.i_coa LIKE '110-2%'
                                                    AND b.i_periode = '$periode' )
                                        UNION ALL
                                            SELECT
                                                a.i_kk AS i_kk,
                                                a.d_kk,
                                                a.f_debet,
                                                a.e_description,
                                                a.i_kendaraan,
                                                a.n_km,
                                                0 AS v_debet,
                                                a.v_kk AS v_kredit,
                                                a.i_area,
                                                d_bukti,
                                                i_coa,
                                                a.e_coa_name,
                                                a.d_entry
                                            FROM
                                                tm_kk a,
                                                tr_area b
                                            WHERE
                                                a.d_kk >= to_date('$dfrom', 'dd-mm-yyyy')
                                                AND a.d_kk <= to_date('$dto', 'dd-mm-yyyy')
                                                AND a.i_area = b.i_area
                                                AND a.f_kk_cancel = 'f' 
                                                AND a.f_debet = 't'
                                        UNION ALL
                                            SELECT
                                                a.i_kk AS i_kk,
                                                a.d_kk,
                                                a.f_debet,
                                                a.e_description,
                                                a.i_kendaraan,
                                                a.n_km,
                                                a.v_kk AS v_debet,
                                                0 AS v_kredit,
                                                a.i_area,
                                                d_bukti,
                                                i_coa,
                                                a.e_coa_name,
                                                a.d_entry
                                            FROM
                                                tm_kk a,
                                                tr_area b
                                            WHERE
                                                a.d_kk >= to_date('$dfrom', 'dd-mm-yyyy')
                                                AND a.d_kk <= to_date('$dto', 'dd-mm-yyyy')
                                                AND a.i_area = b.i_area
                                                AND a.f_kk_cancel = 'f' 
                                                AND a.f_debet = 'f'
                                    ) AS a
                                        LEFT JOIN tr_area b ON
                                            (a.i_area = b.i_area)
                                        LEFT JOIN tm_pv_item c ON
                                            (a.i_kk = c.i_kk
                                                AND a.i_area = c.i_area
                                                AND a.i_coa = c.i_coa)
                                        LEFT JOIN tm_rv_item d ON
                                            (a.i_kk = d.i_kk
                                                AND a.i_area = d.i_area
                                                AND a.i_coa = d.i_coa) ) AS b
                                        INNER JOIN tr_coa coa ON (b.i_area = coa.i_area AND coa.i_coa LIKE '110-12%')
                                        WHERE
                                        coa.e_coa_name = '$ebankname'
                                    GROUP BY coa.e_coa_name ", FALSE)->row();
        // $query = $this->db->get();
        // if ($query->num_rows() > 0) {
        //     return $query->result();
        // }
    }

    public function bacasaldo($tanggal, $icoabank)
    {
        $tmp = explode("-", $tanggal);
        $thn = $tmp[0];
        $bln = $tmp[1];
        $tgl = $tmp[2];
        $dsaldo = $thn . "/" . $bln . "/" . $tgl;

        $dtos = $this->mmaster->dateAdd("d", 1, $dsaldo);
        $tmp1 = explode("-", $dtos, strlen($dtos));
        $th = $tmp1[0];
        $bl = $tmp1[1];
        $dt = $tmp1[2];
        $dtos = $th . $bl;

        $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ", false);

        $query = $this->db->get();
        $saldo = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $saldo = $row->v_saldo_awal;
            }
        }

        $this->db->select(" sum(x.v_bank) as v_bank from
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x ", false);
        $query = $this->db->get();
        $kredit = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $kredit = $row->v_bank;
            }
        }
        $this->db->select(" sum(x.v_bank) as v_bank from
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','yyyy-mm-dd')  and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','yyyy-mm-dd') and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x", false);
        $query = $this->db->get();
        $debet = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $debet = $row->v_bank;
            }
        }
        $saldo = $saldo + $debet - $kredit;
        return $saldo;
    }

    function bacasaldokas($periode, $dfrom, $dto)
    {
        $cek_saldo = $this->db->query("select v_saldo_awal from tm_coa_saldo where i_periode='$periode' and i_coa = '110-11000'");
        if ($cek_saldo->num_rows() > 0) {
            $saldo_awal = $cek_saldo->row();
            $saldo = $saldo_awal->v_saldo_awal;

            $cek_kedua = $this->db->query(" select sum(x.debet) as debet, sum(x.kredit) as kredit from(
                                            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, 0 as debet, v_kb as kredit from tm_kb where
                                            d_kb >= to_date('$periode', 'yyyymm') and d_kb < to_date('$dfrom', 'dd-mm-yyyy') and f_debet = 't' and f_kb_cancel = 'f'
                                            union all
                                            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, v_kb as debet,  0 as kredit from tm_kb where
                                            d_kb >= to_date('$periode', 'yyyymm') and d_kb < to_date('$dfrom', 'dd-mm-yyyy') and f_debet = 'f' and f_kb_cancel = 'f'
                                            ) as x");

            if ($cek_kedua->num_rows() > 0) {
                $cekk = $cek_kedua->row();

                $debet = $cekk->debet;
                $kredit = $cekk->kredit;
            } else {
                $debet = 0;
                $kredit = 0;
            }
            return $saldo = $saldo + $debet - $kredit;
        }
    }

    function bacasaldokk($area, $periode, $tanggal)
    {
        $this->db->select(" a.v_saldo_awal, a.i_coa from tm_coa_saldo a, tr_coa b
		where a.i_periode='$periode' 
		and a.i_coa = b.i_coa
		and b.i_area = '$area'
		and b.e_coa_name like '%Kas Kecil%'", false);
        $query = $this->db->get();
        $saldo = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $saldo = $row->v_saldo_awal;
                $coa_area = $row->i_coa;
            }
        }
        $this->db->select(" sum(v_kk) as v_kk from tm_kk
							  where i_periode='$periode' and i_area='$area'
							  and d_kk<'$tanggal' and f_debet='t' and f_kk_cancel='f'", false);
        $query = $this->db->get();
        $kredit = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $kredit = $row->v_kk;
            }
        }
        $this->db->select(" sum(v_kk) as v_kk from tm_kk
							  where i_periode='$periode' and i_area='$area'
							  and d_kk<'$tanggal' and f_debet='f' and f_kk_cancel='f'", false);
        $query = $this->db->get();
        $debet = 0;
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $debet = $row->v_kk;
            }
        }
        $coaku = KasKecil . $area;
        $kasbesar = KasBesar;
        $bank = Bank;
        $this->db->select(" sum(v_bank) as v_bank from tm_kbank a where a.i_periode='$periode' and a.i_area='$area' and a.d_bank<'$tanggal' 
		                      and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa='$coaku'
		                      and a.v_bank not in (select b.v_kk as v_bank from tm_kk b where b.d_kk = a.d_bank and b.i_area='$area' 
		                      and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' and b.i_periode='$periode')", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $debet = $debet + $row->v_bank;
            }
        }
        $this->db->select(" sum(v_kb) as v_kb from tm_kb a where a.i_periode='$periode' and a.i_area='$area' and a.d_kb<'$tanggal' 
		                      and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
		                      and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where b.d_kk = a.d_kb and b.i_area='$area' 
	                        and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar' 
	                        and b.i_periode='$periode')", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                //   $debet=$debet+$row->v_kb;
            }
        }
        $saldo = $saldo + $debet - $kredit;
        return $saldo;
    }

    public function dateAdd($interval, $number, $dateTime)
    {
        $dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
        $dateTimeArr = getdate($dateTime);
        $yr = $dateTimeArr['year'];
        $mon = $dateTimeArr['mon'];
        $day = $dateTimeArr['mday'];
        $hr = $dateTimeArr['hours'];
        $min = $dateTimeArr['minutes'];
        $sec = $dateTimeArr['seconds'];
        switch ($interval) {
            case "s": //seconds
                $sec += $number;
                break;
            case "n": //minutes
                $min += $number;
                break;
            case "h": //hours
                $hr += $number;
                break;
            case "d": //days
                $day += $number;
                break;
            case "ww": //Week
                $day += ($number * 7);
                break;
            case "m": //similar result "m" dateDiff Microsoft
                $mon += $number;
                break;
            case "yyyy": //similar result "yyyy" dateDiff Microsoft
                $yr += $number;
                break;
            default:
                $day += $number;
        }
        $dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
        $dateTimeArr = getdate($dateTime);
        $nosecmin = 0;
        $min = $dateTimeArr['minutes'];
        $sec = $dateTimeArr['seconds'];
        if ($hr == 0) {
            $nosecmin += 1;
        }
        if ($min == 0) {
            $nosecmin += 1;
        }
        if ($sec == 0) {
            $nosecmin += 1;
        }
        if ($nosecmin > 2) {
            return (date("Y-m-d", $dateTime));
        } else {
            return (date("Y-m-d G:i:s", $dateTime));
        }
    }

    public function namabank($icoabank)
    {
        $this->db->select("* from tr_bank where i_coa='$icoabank'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $xx) {
                return $xx->e_bank_name;
            }
        }
    }
}
