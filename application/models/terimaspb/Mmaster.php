<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari,$num,$offset,$user)
    {

      $this->db->select(" * from tm_spb 
                          inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
                          inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
                          inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
                          inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
                          where 
                          ((tm_spb.f_spb_stockdaerah='f'
                          and not tm_spb.i_approve1 is null
                          and not tm_spb.i_approve2 is null
                          and tm_spb.i_store isnull 
                          and tm_spb.i_store_location isnull
                          and tm_spb.d_spb_storereceive isnull)
                          or
                          (tm_spb.f_spb_stockdaerah='t'
                          and tm_spb.d_spb_storereceive isnull))
                          and tm_spb.f_spb_cancel='f'
                          and tm_spb.i_nota is null
                          and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tr_customer.e_customer_name) ilike '%$cari%')
                          and tm_spb.i_area in(select i_area from tm_user_area where i_user='$user')
                          order by tm_spb.i_area, tm_spb.i_spb", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
    $user=$this->session->userdata('user_id');
		$this->db->select(" 	* from tm_spb 
                          inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
                          inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
                          inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
                          inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
                          where 
                          ((tm_spb.f_spb_stockdaerah='f'
                          and not tm_spb.i_approve1 is null
                          and not tm_spb.i_approve2 is null
                          and tm_spb.i_store isnull 
                          and tm_spb.i_store_location isnull
                          and tm_spb.d_spb_storereceive isnull)
                          or
                          (tm_spb.f_spb_stockdaerah='t'
                          and tm_spb.d_spb_storereceive isnull))
                          and tm_spb.f_spb_cancel='f'
                          and tm_spb.i_nota is null
                          and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tr_customer.e_customer_name) ilike '%$cari%')
                          and tm_spb.i_area in(select i_area from tm_user_area where i_user='$user')
                          order by tm_spb.i_area, tm_spb.i_spb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatespb($ispb, $iarea)
    {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dspbstorereceive	= $row->c;
		$data = array(
		           'd_spb_storereceive' => $dspbstorereceive
		        );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
    function baca($ispb,$iarea)
    {
		$this->db->select(" * from tm_spb 
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispb,$iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
