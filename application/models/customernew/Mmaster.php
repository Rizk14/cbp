<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($ispb, $iarea)
	{
		$this->db->select(" a.*, tr_area.e_area_name, tr_shop_status.e_shop_status, tr_marriage.e_marriage, tr_jeniskelamin.e_jeniskelamin,
                        tr_religion.e_religion, tr_traversed.e_traversed, tr_customer_class.e_customer_classname, tr_city.e_city_name,
                        tr_paymentmethod.e_paymentmethod, tr_call.e_call, tr_customer_group.e_customer_groupname,
                        tr_customer_plugroup.e_customer_plugroupname, tr_customer_producttype.e_customer_producttypename,
                        tr_customer_specialproduct.e_customer_specialproductname, tr_customer_status.e_customer_statusname,
                        tr_customer_grade.e_customer_gradename, tr_customer_service.e_customer_servicename, tr_area.i_store,
                        tr_customer_salestype.e_customer_salestypename, tr_price_group.e_price_groupname, tr_retensi.e_retensi
												FROM tr_customer_tmp a
					              LEFT JOIN tr_city
					              ON (a.i_city = tr_city.i_city and a.i_area = tr_city.i_area)
					              LEFT JOIN tr_customer_group
					              ON (a.i_customer_group = tr_customer_group.i_customer_group)
					              LEFT JOIN tr_area
					              ON (a.i_area = tr_area.i_area)
					              LEFT JOIN tr_customer_status 
					              ON (a.i_customer_status = tr_customer_status.i_customer_status)
					              LEFT JOIN tr_customer_producttype
					              ON (a.i_customer_producttype = tr_customer_producttype.i_customer_producttype)
					              LEFT JOIN tr_customer_specialproduct
				              	ON (a.i_customer_specialproduct = tr_customer_specialproduct.i_customer_specialproduct)
					              LEFT JOIN tr_customer_grade
					              ON (a.i_customer_grade = tr_customer_grade.i_customer_grade)
					              LEFT JOIN tr_customer_service
					              ON (a.i_customer_service = tr_customer_service.i_customer_service)
					              LEFT JOIN tr_customer_salestype
					              ON (a.i_customer_salestype = tr_customer_salestype.i_customer_salestype)
					              LEFT JOIN tr_customer_class 
					              ON (a.i_customer_class=tr_customer_class.i_customer_class)
					              LEFT JOIN tr_shop_status 
					              ON (a.i_shop_status=tr_shop_status.i_shop_status)
					              LEFT JOIN tr_marriage 
					              ON (a.i_marriage=tr_marriage.i_marriage)
					              LEFT JOIN tr_jeniskelamin 
					              ON (a.i_jeniskelamin=tr_jeniskelamin.i_jeniskelamin)
					              LEFT JOIN tr_religion 
					              ON (a.i_religion=tr_religion.i_religion)
					              LEFT JOIN tr_traversed 
					              ON (a.i_traversed=tr_traversed.i_traversed)
					              LEFT JOIN tr_paymentmethod 
					              ON (a.i_paymentmethod=tr_paymentmethod.i_paymentmethod)
					              LEFT JOIN tr_call 
					              ON (a.i_call=tr_call.i_call)
					              LEFT JOIN tr_customer_plugroup
					              ON (a.i_customer_plugroup=tr_customer_plugroup.i_customer_plugroup)
					              LEFT JOIN tr_price_group
					              ON (a.i_price_group=tr_price_group.i_price_group)
					              LEFT JOIN tr_retensi
					              ON (a.i_retensi=tr_retensi.i_retensi)
					              where a.i_spb = '$ispb' and a.i_area='$iarea'
				", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function insert(
		$ispb,
		$icustomer,
		$iarea,
		$isalesman,
		$esalesmanname,
		$dsurvey,
		$nvisitperiod,
		$fcustomernew,
		$ecustomername,
		$ecustomeraddress,
		$ecustomersign,
		$ecustomerphone,
		$ert1,
		$erw1,
		$epostal1,
		$ecustomerkelurahan1,
		$ecustomerkecamatan1,
		$ecustomerkota1,
		$ecustomerprovinsi1,
		$efax1,
		$ecustomermonth,
		$ecustomeryear,
		$ecustomerage,
		$eshopstatus,
		$ishopstatus,
		$nshopbroad,
		$ecustomerowner,
		$ecustomerownerttl,
		$emarriage,
		$imarriage,
		$ejeniskelamin,
		$ijeniskelamin,
		$ereligion,
		$ireligion,
		$ecustomerowneraddress,
		$ecustomerownerphone,
		$ecustomerownerhp,
		$ecustomerownerfax,
		$ecustomerownerpartner,
		$ecustomerownerpartnerttl,
		$ecustomerownerpartnerage,
		$ert2,
		$erw2,
		$epostal2,
		$ecustomerkelurahan2,
		$ecustomerkecamatan2,
		$ecustomerkota2,
		$ecustomerprovinsi2,
		$ecustomersendaddress,
		$ecustomersendphone,
		$etraversed,
		$itraversed,
		$fparkir,
		$fkuli,
		$eekspedisi1,
		$eekspedisi2,
		$ert3,
		$erw3,
		$epostal3,
		$ecustomerkota3,
		$ecustomerprovinsi3,
		$ecustomerpkpnpwp,
		$fspbpkp,
		$ecustomernpwpname,
		$ecustomernpwpaddress,
		$ecustomerclassname,
		$icustomerclass,
		$epaymentmethod,
		$ipaymentmethod,
		$ecustomerbank1,
		$ecustomerbankaccount1,
		$ecustomerbankname1,
		$ecustomerbank2,
		$ecustomerbankaccount2,
		$ecustomerbankname2,
		$ekompetitor1,
		$ekompetitor2,
		$ekompetitor3,
		$nspbtoplength,
		$ncustomerdiscount,
		$epricegroupname,
		$ipricegroup,
		$nline,
		$fkontrabon,
		$ecall,
		$icall,
		$ekontrabonhari,
		$ekontrabonjam1,
		$ekontrabonjam2,
		$etagihhari,
		$etagihjam1,
		$etagihjam2,
		$icustomergroup,
		$icustomerplugroup,
		$icustomerproducttype,
		$icustomerspecialproduct,
		$icustomerstatus,
		$icustomergrade,
		$icustomerservice,
		$icustomersalestype,
		$ecustomerownerage,
		$ecustomerrefference,
		$iretensi,
		$icity,
		$ecustomercontact,
		$ecustomercontactgrade,
		$ecustomermail,
		$inik
	) {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry = $row->c;

		$ecustomername = str_replace("'", "''", $ecustomername);

		if ($nshopbroad == '') $nshopbroad = 0;
		$this->db->query("insert into tr_customer_tmp 
											values
											('$ispb','$icustomer','$iarea','$isalesman','$ipricegroup','$icustomerclass','$icustomerplugroup',
											 '$icustomergroup','$icustomerstatus','$icustomerproducttype','$icustomerspecialproduct',
											 '$icustomergrade','$icustomerservice','$icustomersalestype','$icity','$ishopstatus','$imarriage',
											 '$ijeniskelamin','$ireligion','$itraversed','$ipaymentmethod','$icall','$esalesmanname',
											 '$dsurvey',$nvisitperiod,'$fcustomernew','$ecustomername','$ecustomeraddress','$ecustomersign',
											 '$ecustomerphone','$ert1','$erw1','$epostal1','$ecustomerkelurahan1','$ecustomerkecamatan1',
											 '$ecustomerkota1','$ecustomerprovinsi1','$efax1','$ecustomermonth','$ecustomeryear','$ecustomerage',
											 '$nshopbroad','$ecustomerowner','$ecustomerownerttl','$ecustomerowneraddress','$ecustomerownerphone',
											 '$ecustomerownerhp','$ecustomerownerfax','$ecustomerownerpartner','$ecustomerownerpartnerttl',
											 '$ecustomerownerpartnerage','$ert2','$erw2','$epostal2','$ecustomerkelurahan2','$ecustomerkecamatan2',
											 '$ecustomerkota2','$ecustomerprovinsi2','$ecustomersendaddress','$ecustomersendphone','$fparkir','$fkuli',
											 '$eekspedisi1','$eekspedisi2','$ert3','$erw3','$epostal3','$ecustomerkota3','$ecustomerprovinsi3',
											 '$ecustomerpkpnpwp','$fspbpkp','$ecustomernpwpname','$ecustomernpwpaddress','$ecustomerbank1',
											 '$ecustomerbankaccount1','$ecustomerbankname1','$ecustomerbank2','$ecustomerbankaccount2',
											 '$ecustomerbankname2','$ekompetitor1','$ekompetitor2','$ekompetitor3',$nspbtoplength,'$ncustomerdiscount',
											 '$fkontrabon','$ekontrabonhari','$ekontrabonjam1','$ekontrabonjam2','$etagihhari','$etagihjam1',
											 '$etagihjam2','$dentry','$ecustomerownerage','f',null,null,null,'$ecustomercontact','$ecustomercontactgrade',
                       '$ecustomersendphone','$ecustomermail',
                       '$ecustomerrefference','f','t',null,null,null,'$iretensi','$inik')");
	}
	function update(
		$icustomer,
		$icustomerplugroup,
		$icity,
		$icustomergroup,
		$ipricegroup,
		$iarea,
		$icustomerstatus,
		$icustomerproducttype,
		$icustomerspecialproduct,
		$icustomergrade,
		$icustomerservice,
		$icustomersalestype,
		$icustomerclass,
		$icustomerstatuss,
		$ecustomername,
		$ecustomeraddress,
		$ecustomercity,
		$ecustomerpostal,
		$ecustomerphone,
		$ecustomerfax,
		$ecustomermail,
		$ecustomersendaddress,
		$ecustomerreceiptaddress,
		$ecustomerremark,
		$ecustomerpayment,
		$ecustomerpriority,
		$ecustomercontact,
		$ecustomercontactgrade,
		$ecustomerrefference,
		$ecustomerothersupplier,
		$fcustomerplusppn,
		$fcustomerplusdiscount,
		$fcustomerpkp,
		$fcustomeraktif,
		$fcustomertax,
		$ecustomerretensi,
		$ncustomertoplength,
		$fcustomercicil,
		$inik
	) {

		$ecustomername = str_replace("'", "''", $ecustomername);

		if ($fcustomerplusppn == 'on')
			$fcustomerplusppn = 'TRUE';
		else
			$fcustomerplusppn = 'FALSE';
		if ($fcustomerplusdiscount == 'on')
			$fcustomerplusdiscount = 'TRUE';
		else
			$fcustomerplusdiscount = 'FALSE';
		if ($fcustomerpkp == 'on')
			$fcustomerpkp = 'TRUE';
		else
			$fcustomerpkp = 'FALSE';
		if ($fcustomeraktif == 'on')
			$fcustomeraktif = 'TRUE';
		else
			$fcustomeraktif = 'FALSE';
		if ($fcustomertax == 'on')
			$fcustomertax = 'TRUE';
		else
			$fcustomertax = 'FALSE';
		if ($fcustomercicil == 'on')
			$fcustomercicil = 'TRUE';
		else
			$fcustomercicil = 'FALSE';
		$query  = $this->db->query("SELECT current_timestamp as c");
		$row    = $query->row();
		$dupdate = $row->c;
		$data = array(
			'i_customer_plugroup'	=> $icustomerplugroup,
			'i_city'		 	        => $icity,
			'i_customer_group'	 	=> $icustomergroup,
			'i_price_group' 		  => $ipricegroup,
			'i_area' 			        => $iarea,
			'i_customer_status' 	=> $icustomerstatus,
			'i_customer_producttype' 	  => $icustomerproducttype,
			'i_customer_specialproduct'	=> $icustomerspecialproduct,
			'i_customer_grade'	    => $icustomergrade,
			'i_customer_service' 		=> $icustomerservice,
			'i_customer_salestype' 	=> $icustomersalestype,
			'i_customer_class' 		  => $icustomerclass,
			'i_customer_status' 		=> $icustomerstatus,
			'e_customer_name' 		  => $ecustomername,
			'e_customer_address' 		=> $ecustomeraddress,
			'e_customer_city' 		  => $ecustomercity,
			'e_customer_postal' 		=> $ecustomerpostal,
			'e_customer_phone' 		  => $ecustomerphone,
			'e_customer_fax' 		    => $ecustomerfax,
			'e_customer_mail' 		  => $ecustomermail,
			'e_customer_sendaddress' => $ecustomersendaddress,
			'e_customer_receiptaddress'	=> $ecustomerreceiptaddress,
			'e_customer_remark' 		=> $ecustomerremark,
			'e_customer_payment' 		=> $ecustomerpayment,
			'e_customer_priority' 	=> $ecustomerpriority,
			'e_customer_contact' 		=> $ecustomercontact,
			'e_customer_contactgrade'	=> $ecustomercontactgrade,
			'e_customer_refference' 	=> $ecustomerrefference,
			'e_customer_othersupplier' => $ecustomerothersupplier,
			'f_customer_plusppn' 		  => $fcustomerplusppn,
			'f_customer_plusdiscount'	=> $fcustomerplusdiscount,
			'f_customer_pkp'	 	  => $fcustomerpkp,
			'f_customer_aktif' 		=> $fcustomeraktif,
			'f_customer_tax' 		  => $fcustomertax,
			'f_customer_cicil'	 	=> $fcustomercicil,
			'e_customer_retensi' 	=> $ecustomerretensi,
			'n_customer_toplength' => $ncustomertoplength,
			'd_customer_update'		=> $dupdate,
			'i_nik'					=> $inik
		);
		$this->db->where('i_customer =', $icustomer);
		$this->db->update('tr_customer', $data);
		redirect('customer/cform/');
	}

	public function delete($icustomer)
	{
		$this->db->query("DELETE FROM tr_customer WHERE i_customer='$icustomer'", false);
		return TRUE;
	}
	function bacasemua($cari, $num, $offset)
	{
		$this->db->select(" * from tr_customer where upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%' order by i_customer", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" * from tr_customer where upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%' order by i_customer", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaplugroup($num, $offset)
	{
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup order by i_customer_plugroup", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariplugroup($cari, $num, $offset)
	{
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup 
				   where upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plugroup", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacity($cari, $iarea, $num, $offset)
	{
		$this->db->select("i_city, e_city_name from tr_city where (upper(i_city) like '%$cari%' 
          							 or upper(e_city_name) like '%$cari%') and i_area='$iarea' order by i_city", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricity($cari, $num, $offset)
	{
		$this->db->select("i_city, e_city_name from tr_city 
				   where upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%' order by i_city", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomergroup($num, $offset)
	{
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group order by i_customer_group", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomergroup($cari, $num, $offset)
	{
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group 
				   where upper(e_customer_groupname) like '%$cari%' or upper(i_customer_group) like '%$cari%' order by i_customer_group", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacapricegroup($num, $offset)
	{
		$this->db->select("i_price_group, e_price_groupname, n_line from tr_price_group order by i_price_group", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caripricegroup($cari, $num, $offset)
	{
		$this->db->select("i_price_group, e_price_groupname, n_line from tr_price_group 
				   where upper(e_price_groupname) like '%$cari%' or upper(i_price_group) like '%$cari%' order by i_price_group", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $allarea, $iuser)
	{
		if ($allarea == 't') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num, $offset);
		}

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset, $allarea, $iuser)
	{
		if ($allarea == 't') {
			$this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num, $offset);
		}

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerstatus($num, $offset)
	{
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status order by i_customer_status", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerstatus($cari, $num, $offset)
	{
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status 
				   where upper(e_customer_statusname) like '%$cari%' or upper(i_customer_status) like '%$cari%' order by i_customer_status", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerproducttype($num, $offset)
	{
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype order by i_customer_producttype", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerproducttype($cari, $num, $offset)
	{
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype 
				   where upper(e_customer_producttypename) like '%$cari%' or upper(i_customer_producttype) like '%$cari%' order by i_customer_producttype", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerspecialproduct($icustomerproducttype, $num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query("select * from tr_customer_specialproduct 
					   where i_customer_producttype='$icustomerproducttype' 
					   order by i_customer_specialproduct 
					   limit $num offset $offset");
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerspecialproduct($cari, $num, $offset)
	{
		$this->db->select("i_customer_specialproduct, e_customer_specialproductname from tr_customer_specialproduct 
				   where upper(e_customer_specialproductname) like '%$cari%' or upper(i_customer_specialproduct) like '%$cari%' order by i_customer_specialproduct", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomergrade($num, $offset)
	{
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade order by i_customer_grade", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomergrade($cari, $num, $offset)
	{
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade 
				   where upper(e_customer_gradename) like '%$cari%' or upper(i_customer_grade) like '%$cari%' order by i_customer_grade", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerservice($num, $offset)
	{
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service order by i_customer_service", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerservice($cari, $num, $offset)
	{
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service 
				   where upper(e_customer_servicename) like '%$cari%' or upper(i_customer_service) like '%$cari%' order by i_customer_service", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomersalestype($num, $offset)
	{
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype order by i_customer_salestype", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomersalestype($cari, $num, $offset)
	{
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype 
				   where upper(e_customer_salestypename) like '%$cari%' or upper(i_customer_salestype) like '%$cari%' order by i_customer_salestype", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerclass($num, $offset)
	{
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class order by i_customer_class", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerclass($cari, $num, $offset)
	{
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class 
				   where upper(e_customer_classname) like '%$cari%' or upper(i_customer_class) like '%$cari%' order by i_customer_class", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacasalesman($num, $offset, $area, $cari)
	{
		$this->db->select("	* from tr_salesman where 
												(upper(i_salesman) like '%$cari%' 
												  or upper(e_salesman_name) like '%$cari%') and f_salesman_aktif='true' and i_area='$area'
												order by i_salesman", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacakota1($num, $offset, $area, $cari)
	{
		$this->db->select("	i_city, e_city_name from tr_city where i_area = '$area' 
                          and (upper(i_city) like '%$cari%' or upper(e_city_name) like '%$cari%')
												  order by i_city", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacakecamatan1($num, $offset, $area, $kota, $cari)
	{
		$this->db->select("	i_kecamatan, e_kecamatan_name from tr_kecamatan where i_area = '$area' 
                        and i_city='$kota' and (upper(e_kecamatan_name) like '%$cari%')
												order by i_kecamatan", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaretensi($num, $offset, $cari)
	{
		$this->db->select("	* from tr_retensi where (upper(i_retensi) like '%$cari%' or upper(e_retensi) like '%$cari%') 
                          order by i_retensi", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacashopstatus($num, $offset)
	{
		$this->db->select("	* from tr_shop_status order by i_shop_status", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacajeniskelamin($num, $offset)
	{
		$this->db->select("	* from tr_jeniskelamin order by i_jeniskelamin", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacareligion($num, $offset)
	{
		$this->db->select("	* from tr_religion order by i_religion", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacamarriage($num, $offset)
	{
		$this->db->select("	* from tr_marriage order by i_marriage", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacatraversed($num, $offset)
	{
		$this->db->select("	* from tr_traversed order by i_traversed", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacapaymentmethod($num, $offset)
	{
		$this->db->select("	* from tr_paymentmethod order by i_paymentmethod", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacall($num, $offset)
	{
		$this->db->select("	* from tr_call order by i_call", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaproduct($cari, $num, $offset, $kdharga)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select distinct a.i_product as kode, a.i_product_motif as motif,
									    a.e_product_motifname as namamotif, d.i_product_group, 
									    c.e_product_name as nama,b.v_product_retail as harga, c.i_product_status, e.e_product_statusname,
										ctg.e_sales_categoryname
									    from tr_product_motif a,tr_product_price b,tr_product c
										LEFT JOIN tr_product_sales_category ctg ON c.i_sales_category = ctg.i_sales_category, tr_product_type d, tr_product_status e
								      where 
								      b.i_product=a.i_product and c.i_product_status=e.i_product_status
								      and a.i_product=c.i_product and d.i_product_type=c.i_product_type
								      and b.i_price_group='$kdharga' and a.i_product_motif='00' and c.f_product_pricelist='t'
								      and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') limit $num offset $offset", false);
		#and d.i_product_type='09'
		#d.i_product_type=c.i_product_type and d.i_product_group='01'
		#								      and 
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumber($iarea, $thbl)
	{
		$th	= substr($thbl, 0, 4);
		$asal = $thbl;
		$thbl = substr($thbl, 2, 2) . substr($thbl, 4, 2);
		$this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPB'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nospb  = $terakhir + 1;
			$this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospb
                            where i_modul='SPB'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			settype($nospb, "string");
			$a = strlen($nospb);
			while ($a < 6) {
				$nospb = "0" . $nospb;
				$a = strlen($nospb);
			}
			$nospb  = "SPB-" . $thbl . "-" . $nospb;
			return $nospb;
		} else {
			$nospb  = "000001";
			$nospb  = "SPB-" . $thbl . "-" . $nospb;
			$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPB','$iarea','$asal',1)");
			return $nospb;
		}
	}
	function insertheader(
		$ispb,
		$dspb,
		$icustomer,
		$iarea,
		$ispbpo,
		$nspbtoplength,
		$isalesman,
		$ipricegroup,
		$dspbreceive,
		$fspbop,
		$ecustomerpkpnpwp,
		$fspbpkp,
		$fspbplusppn,
		$fspbplusdiscount,
		$fspbstockdaerah,
		$fspbprogram,
		$fspbvalid,
		$fspbsiapnotagudang,
		$fspbcancel,
		$nspbdiscount1,
		$nspbdiscount2,
		$nspbdiscount3,
		$vspbdiscount1,
		$vspbdiscount2,
		$vspbdiscount3,
		$vspbdiscounttotal,
		$vspb,
		$fspbconsigment,
		$ispbold,
		$eremarkx,
		$iproductgroup
	) {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->set(
			array(
				'i_spb'		=> $ispb,
				'd_spb'		=> $dspb,
				'i_customer'	=> $icustomer,
				'i_area'	=> $iarea,
				'i_spb_po'	=> $ispbpo,
				'n_spb_toplength' => $nspbtoplength,
				'i_salesman' 	=> $isalesman,
				'i_price_group' => $ipricegroup,
				'd_spb_receive'	=> $dspb,
				'f_spb_op'	=> $fspbop,
				'e_customer_pkpnpwp'	=> $ecustomerpkpnpwp,
				'f_spb_pkp'		=> $fspbpkp,
				'f_spb_plusppn'		=> $fspbplusppn,
				'f_spb_plusdiscount'	=> $fspbplusdiscount,
				'f_spb_stockdaerah'	=> $fspbstockdaerah,
				'f_spb_program' 	=> $fspbprogram,
				'f_spb_valid' 		=> $fspbvalid,
				'f_spb_siapnotagudang'	=> $fspbsiapnotagudang,
				'f_spb_cancel' 		=> $fspbcancel,
				'n_spb_discount1' 	=> $nspbdiscount1,
				'n_spb_discount2' 	=> $nspbdiscount2,
				'n_spb_discount3' 	=> $nspbdiscount3,
				'v_spb_discount1' 	=> $vspbdiscount1,
				'v_spb_discount2' 	=> $vspbdiscount2,
				'v_spb_discount3' 	=> $vspbdiscount3,
				'v_spb_discounttotal'	=> $vspbdiscounttotal,
				'v_spb' 		=> $vspb,
				'f_spb_consigment' 	=> $fspbconsigment,
				'd_spb_entry'		=> $dentry,
				'i_spb_old'		=> $ispbold,
				'i_product_group'	=> '01',
				'e_remark1'		=> $eremarkx
			)
		);

		$this->db->insert('tm_spb');
	}
	function insertdetail($ispb, $iarea, $iproduct, $iproductgrade, $eproductname, $norder, $ndeliver, $vunitprice, $iproductmotif, $eremark, $i, $iproductstatus)
	{
		if ($eremark == '') $eremark = null;
		$this->db->set(
			array(
				'i_spb'						=> $ispb,
				'i_area'      		=> $iarea,
				'i_product'   		=> $iproduct,
				'i_product_status' => $iproductstatus,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'n_order'     		=> $norder,
				'n_deliver'   		=> $ndeliver,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
				'e_remark'				=> $eremark,
				'n_item_no'       => $i
			)
		);
		$this->db->insert('tm_spb_item');
	}
	function bacaspb($ispb, $iarea)
	{
		#			$this->db->select("tm_spb.e_remark1 AS emark1, * from tm_spb 
		$this->db->select("tm_spb.e_remark1 AS emark1, tm_spb.i_spb, tm_spb.i_customer, tm_spb.i_salesman, tm_spb.i_price_group, 
                         tm_spb.i_nota, tm_spb.i_spb_program, tm_spb.i_spb_po, tm_spb.i_sj, tm_spb.i_store, tm_spb.i_store_location, 
                         tm_spb.d_spb, tm_spb.d_nota, tm_spb.d_spb_entry, tm_spb.d_spb_update, tm_spb.d_sj,
                         tm_spb.d_spb_delivery, tm_spb.d_spb_receive, tm_spb.d_spb_storereceive, tm_spb.d_spb_email, tm_spb.e_customer_pkpnpwp,
                         tm_spb.f_spb_op, tm_spb.f_spb_pkp, tm_spb.f_spb_plusppn, tm_spb.f_spb_plusdiscount, tm_spb.f_spb_stockdaerah, 
                         tm_spb.f_spb_program, tm_spb.f_spb_consigment, tm_spb.f_spb_valid, tm_spb.f_spb_siapnotagudang, tm_spb.f_spb_cancel,
                         tm_spb.n_spb_toplength, tm_spb.n_spb_discount1, tm_spb.n_spb_discount2, tm_spb.n_spb_discount3, tm_spb.v_spb_discount1, 
                         tm_spb.v_spb_discount2, tm_spb.v_spb_discount3, tm_spb.v_spb_discounttotal, tm_spb.v_spb_discounttotalafter,
                         tm_spb.v_spb, tm_spb.v_spb_after, tm_spb.i_approve1, tm_spb.i_approve2, tm_spb.d_approve1, tm_spb.d_approve2, 
                         tm_spb.e_approve1, tm_spb.e_approve2, tm_spb.i_area, tm_spb.n_spb_discount4, tm_spb.v_spb_discount4, tm_spb.i_notapprove,
                         tm_spb.d_notapprove, tm_spb.e_notapprove, tm_spb.f_spb_siapnotasales, tm_spb.i_spb_old, tm_spb.i_product_group, 
                         tm_spb.f_spb_opclose, tm_spb.f_spb_pemenuhan, tm_spb.n_print, tm_spb.i_cek, tm_spb.d_cek, tm_spb.e_cek
                         from tm_spb
						             left join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						             left join tr_customer_tmp on (tm_spb.i_spb=tr_customer_tmp.i_spb and tm_spb.i_area=tr_customer_tmp.i_area)
						             inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
						             left join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
						             inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
						             where tm_spb.i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($ispb, $iarea, $ipricegroup)
	{
		$this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
                          a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, 
                          b.e_product_motifname, c.v_product_retail as hrgnew, a.i_product_status, x.i_product_group
		                      from tm_spb_item a, tr_product_motif b, tr_product_price c, tm_spb x
						              where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif 
		                      and a.i_product=c.i_product and c.i_price_group='$ipricegroup'
		                      and a.i_spb=x.i_spb and a.i_area=x.i_area
						              order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetailnilaiorderspb($ispb, $iarea, $ipricegroup)
	{
		return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
												        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
	}

	function bacadetailnilaispb($ispb, $iarea, $ipricegroup)
	{
		return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a
			        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
	}
	function uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscounttotal, $vspb)
	{
		$data = array(
			'v_spb_discount1' 		=> $vspbdiscount1,
			'v_spb_discount2' 		=> $vspbdiscount2,
			'v_spb_discount3' 		=> $vspbdiscount3,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 				=> $vspb
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
	}
	function deleteheaderspb($ispb, $iarea)
	{
		$this->db->query(" delete from tm_spb where i_spb = '$ispb' and i_area='$iarea' ", false);
	}
	function deletedetailspb($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif)
	{
		$this->db->query(" delete from tm_spb_item where i_spb = '$ispb' and i_area='$iarea' 
												and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'", false);
	}
	function deleteheader($ispb, $iarea)
	{
		$this->db->query(" delete from tr_customer_tmp where i_spb = '$ispb' and i_area='$iarea' ", false);
	}
}
