<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($area,$cari, $num,$offset)
    {

		$this->db->select(" 	distinct(a.i_spb||a.i_area),a.*, b.e_customer_name,c.e_area_name
                          from tm_spb a, tr_customer b, tr_area c, 
                          tm_spb_item d left join tm_op e on(d.i_op=e.i_op and e.f_op_close='t')
                          where a.i_customer=b.i_customer 
                          and a.i_area=c.i_area
                          and a.f_spb_cancel='f' 
                          and a.f_spb_valid='f' 
                          and a.f_spb_siapnotagudang='f'
                          and a.f_spb_siapnotasales='f'
                          and a.f_spb_pemenuhan='t'
                          and a.i_store='AA'
                          and a.i_spb=d.i_spb and a.i_area=d.i_area
				                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                    or upper(a.i_spb) like '%$cari%')
				                  order by a.i_spb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($area,$cari,$num,$offset)
    {
		$this->db->select(" 	distinct(a.i_spb||a.i_area),a.*, b.e_customer_name,c.e_area_name
                          from tm_spb a, tr_customer b, tr_area c, 
                          tm_spb_item d left join tm_op e on(d.i_op=e.i_op and e.f_op_close='t')
                          where a.i_customer=b.i_customer 
                          and a.i_area=c.i_area
                          and a.f_spb_cancel='f' 
                          and a.f_spb_valid='f' 
                          and a.f_spb_siapnotagudang='f'
                          and a.f_spb_siapnotasales='f'
                          and a.f_spb_pemenuhan='t'
                          and a.i_store='AA'
                          and a.i_spb=d.i_spb and a.i_area=d.i_area
				                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                    or upper(a.i_spb) like '%$cari%')",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatespb($ispb, $iarea)
    {
		$data = array(
		           'f_spb_siapnotagudang' => 't'
		        );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
    function baca($ispb,$iarea)
    {
		$this->db->select(" * from tm_spb 
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispb,$iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
