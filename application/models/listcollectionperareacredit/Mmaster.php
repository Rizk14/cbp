<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$interval)
    {
      $perfrom=substr($dfrom,6,4).substr($dfrom,3,2);
      $perto  =substr($dto,6,4).substr($dto,3,2);
      $sql =" a.i_area, a.e_area_name, a.bln, sum(a.total) as total, sum(a.realisasi) as realisasi from( 
              select a.i_area, a.e_area_name, substring(b.e_periode, 5, 2) as bln, sum(b.sisa+b.bayar) as total, 
              sum(b.bayar) as realisasi 
              from tm_collection_credit b, tr_area a
              where a.i_area=b.i_area 
              and b.e_periode>='$perfrom' and b.e_periode<='$perto' 
              group by a.i_area, a.e_area_name, substring(b.e_periode, 5, 2)) as a 
              group by a.i_area, a.e_area_name, a.bln 
              order by a.i_area, a.e_area_name, a.bln";
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function sumperiode($dfrom,$dto,$interval)
    {
      $perfrom=substr($dfrom,6,4).substr($dfrom,3,2);
      $perto  =substr($dto,6,4).substr($dto,3,2);
      $sql =" a.bln, sum(a.total) as total from( 
              select a.i_area, a.e_area_name, substring(b.e_periode, 5, 2) as bln, sum(b.sisa+b.bayar) as total, 
              sum(b.bayar) as realisasi 
              from tm_collection_credit b, tr_area a
              where a.i_area=b.i_area 
              and b.e_periode>='$perfrom' and b.e_periode<='$perto' 
              group by a.i_area, a.e_area_name, substring(b.e_periode, 5, 2)) as a 
              group by a.bln ";
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
