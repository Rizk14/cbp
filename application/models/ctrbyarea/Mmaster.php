<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($tahun)
    {
	    $this->db->select("	a.i_periode, a.e_area_name, sum(a.vnota)  as vnota, sum(qnota) as qnota from (	
	                        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_name, sum(a.v_nota_gross)  as vnota, 0 as qnota
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_name
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_name, 0 as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_name
                          )as a
                          group by a.i_periode, a.e_area_name
                          order by a.i_periode, a.e_area_name",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
