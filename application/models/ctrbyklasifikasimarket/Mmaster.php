<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	a.e_customer_classname, sum(a.custly) as custly,  sum(a.notaly) as notaly, sum(a.custcy)  as custcy, sum(a.notacy) as notacy, 
                          sum(a.custlm) as custlm, sum(a.notalm) as notalm, sum(a.custcm) as custcm, sum(a.notacm) as notacm
                          from(
                          select  count(a.i_customer) as custly, sum(a.nota) as notaly, 0 as custcy, 0 as notacy, 0 as custlm, 0 as notalm, 
                          0 as custcm, 0 as notacm,  a.i_customer_class, a.e_customer_classname from
                          (
                          SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_customer)  a.i_customer, sum(a.v_nota_gross) as nota, b.i_customer_class, c.e_customer_classname
                          from tm_nota a, tr_customer b, tr_customer_class c
                          where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate'
                          and a.i_customer=b.i_customer
                          and c.i_customer_class=b.i_customer_class
                          group by a.i_customer, b.i_customer_class, c.e_customer_classname, to_char(a.d_nota,'yyyymm')
                          ) as a
                          group by i_customer_class, e_customer_classname
                          union all
                          select  0 as custly, 0 as notaly, count(a.i_customer) as custcy, sum(a.nota) as notacy, 0 as custlm, 0 as notalm, 
                          0 as custcm, 0 as notacm,  a.i_customer_class, a.e_customer_classname from
                          (
                          SELECT distinct on (to_char(a.d_nota,'yyyymm'), a.i_customer)  a.i_customer, sum(a.v_nota_gross) as nota, b.i_customer_class, c.e_customer_classname
                          from tm_nota a, tr_customer b, tr_customer_class c
                          where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate'
                          and a.i_customer=b.i_customer
                          and c.i_customer_class=b.i_customer_class
                          group by a.i_customer, b.i_customer_class, c.e_customer_classname, to_char(a.d_nota,'yyyymm')
                          ) as a
                          group by i_customer_class, e_customer_classname
                          union all
                          select  0 as custly, 0 as notaly, 0 as custcy, 0 as notacy, count(a.i_customer) as custlm, sum(a.nota) as notalm,
                          0 as custcm, 0 as notacm,  a.i_customer_class, a.e_customer_classname from
                          (
                          SELECT distinct on (a.i_customer)  a.i_customer, sum(a.v_nota_gross) as nota, b.i_customer_class, c.e_customer_classname
                          from tm_nota a, tr_customer b, tr_customer_class c
                          where to_char(a.d_nota,'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate'
                          and a.i_customer=b.i_customer
                          and c.i_customer_class=b.i_customer_class
                          group by a.i_customer, b.i_customer_class, c.e_customer_classname
                          ) as a
                          group by i_customer_class, e_customer_classname
                          union all
                          select  0 as custly, 0 as notaly, 0 as custcy, 0 as notacy, 0 as custlm, 0 as notalm,
                          count(a.i_customer) as custcm, sum(a.nota) as notacm,  a.i_customer_class, a.e_customer_classname from
                          (
                          SELECT distinct on (a.i_customer)  a.i_customer, sum(a.v_nota_gross) as nota, b.i_customer_class, c.e_customer_classname
                          from tm_nota a, tr_customer b, tr_customer_class c
                          where to_char(a.d_nota,'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate'
                          and a.i_customer=b.i_customer
                          and c.i_customer_class=b.i_customer_class
                          group by a.i_customer, b.i_customer_class, c.e_customer_classname
                          ) as a
                          group by i_customer_class, e_customer_classname
                          ) as a
                          group by i_customer_class, e_customer_classname
                          order by i_customer_class",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
