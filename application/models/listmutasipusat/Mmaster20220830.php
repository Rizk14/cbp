<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }
  function baca($istorelocation, $iperiode, $istore, $num, $offset, $cari)
  {
    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='MTS'
                          and i_area='$istore' for update", false); #and i_store_location='$istorelocation' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $this->db->query("update tm_dgu_no 
                          set e_periode='$iperiode'
                          where i_modul='MTS' and i_area='$istore' and i_store_location='$istorelocation'", false);
    } else {
      $this->db->query("insert into tm_dgu_no(i_modul, i_area, e_periode, i_store_location) 
                          values ('MTS','$istore','$iperiode', '$istorelocation')");
    }
    $query->free_result();
    if ($iperiode > '201512') {
      $sql = "";
      if ($iproductgroup == 'ALL') {
        $sql .= "   a.*,
                  n_mutasi_git AS n_saldo_git,
                  ctg.* 
                FROM
                  f_mutasi_stock_pusat_saldoakhir4('$iperiode') a
                  INNER JOIN tr_product b ON (a.i_product = b.i_product)
                  LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category) ";
        if ($iproductstatus != 'ALL') {
          $sql .= " where ctg.i_sales_category = '$iproductstatus'";
          // $sql .= " where substring(status,1,1)='$iproductstatus'";
        }
      } else {
        $sql .= "   a.*,
                  n_mutasi_git AS n_saldo_git,
                  ctg.* 
                FROM
                  f_mutasi_stock_pusat_saldoakhir4('$iperiode') a
                  INNER JOIN tr_product b ON (a.i_product = b.i_product)
                  LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
     		           where i_product_group='$iproductgroup' ";
        if ($iproductstatus != 'ALL') {
          $sql .= " and ctg.i_sales_category = '$iproductstatus'";
          // $sql .= " and substring(status,1,1)='$iproductstatus'";
        }
        $sql .= " ORDER BY i_product_grade, status, e_product_groupnameshort, e_product_name, i_product ;";
      }
      $this->db->select($sql, false);
    } else {
      $this->db->select(" *, n_mutasi_git as n_saldo_git from f_mutasi_stock_pusat('$iperiode') ", false); #->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
    $query->free_result();
  }
  function bacaexcel($iperiode, $istore, $cari)
  {
    if ($iperiode > '201512') {
      $this->db->select(" *, n_mutasi_git as n_saldo_git  from f_mutasi_stock_pusat_saldoakhir('$iperiode')", false); #->limit($num,$offset);
    } else {
      $this->db->select(" *, n_mutasi_git as n_saldo_git  from f_mutasi_stock_pusat('$iperiode')", false); #->limit($num,$offset);
    }
    $query = $this->db->get();
    return $query;
  }

  function bacatotal($iperiode, $iproductgroup, $iproductstatus)
  {
    $sql = $iproductstatus != 'ALL' ? "WHERE ctg.i_sales_category = '$iproductstatus'" : "";
    $sql2 = $iproductstatus != 'ALL' ? "AND ctg.i_sales_category = '$iproductstatus'" : "";
    /* $sql = $iproductstatus != 'ALL' ? "WHERE substring(status,1,1)='$iproductstatus'" : "";
    $sql2 = $iproductstatus != 'ALL' ? "AND substring(status,1,1)='$iproductstatus'" : ""; */

    if ($iproductgroup == 'ALL') {
      $query = $this->db->query("  SELECT
                                    ctg.i_sales_category,
                                    CASE WHEN ctg.e_sales_categoryname ISNULL THEN '-'
                                    ELSE ctg.e_sales_categoryname END AS e_sales_categoryname,
                                    sum(n_saldo_akhir) AS saldoakhir,
                                    sum(n_saldo_akhir * a.v_product_retail) AS rpsaldoakhir,
                                    sum(n_saldo_stockopname + n_mutasi_git + n_git_penjualan) AS saldostockopname,
                                    sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) * a.v_product_retail) AS rpstockopname,
                                    sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) AS selisih,
                                    sum(((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) * a.v_product_retail) AS rpselisih
                                  FROM
                                    f_mutasi_stock_pusat_saldoakhir4('$iperiode') a
                                    INNER JOIN tr_product b ON (a.i_product = b.i_product)
                                    LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
                                    $sql
                                  GROUP BY 1,ctg.i_sales_category 
                                  ORDER BY ctg.i_sales_category ");
    } else {
      $query = $this->db->query("  SELECT
                                    ctg.i_sales_category,
                                    CASE WHEN ctg.e_sales_categoryname ISNULL THEN '-'
                                    ELSE ctg.e_sales_categoryname END AS e_sales_categoryname,
                                    sum(n_saldo_akhir) AS saldoakhir,
                                    sum(n_saldo_akhir * a.v_product_retail) AS rpsaldoakhir,
                                    sum(n_saldo_stockopname + n_mutasi_git + n_git_penjualan) AS saldostockopname,
                                    sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) * a.v_product_retail) AS rpstockopname,
                                    sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) AS selisih,
                                    sum(((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) * a.v_product_retail) AS rpselisih
                                  FROM
                                    f_mutasi_stock_pusat_saldoakhir4('$iperiode') a
                                    INNER JOIN tr_product b ON (a.i_product = b.i_product)
                                    LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
                                  WHERE
                                    i_product_group='$iproductgroup'
                                    $sql2
                                  GROUP BY 1,ctg.i_sales_category 
                                  ORDER BY ctg.i_sales_category ");
    }

    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }

  function proses($iperiode, $iarea)
  {
    if ($iarea == '00') {
      $istore = 'AA';
    } else {
      $istore = $iarea;
    }
    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='MTS'
                          and i_area='$istore' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $this->db->query(" update tm_dgu_no 
                           set e_periode='$iperiode'
                           where i_modul='MTS' and i_area='$istore'", false);
    } else {
      $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode) 
                           values ('MTS','$istore','$iperiode')");
    }
    $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
    $st = $query->row();
    $store = $st->i_store;
    if ($store == 'AA') $loc = '01';
    else $loc = '00';
    $this->db->query("  delete from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store'");
    $query = $this->db->query(" select i_product from tm_ic where i_store='$store' group by i_product");
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $gie) {

        $que = $this->db->query("select area,periode,product,penjualan,pembelian,bbm,bbk,retur,git,pesan,ketoko,daritoko
                                   from vmutasi where periode='$iperiode' and area='$iarea' and product='$gie->i_product'");
        if ($que->num_rows() > 0) {
          foreach ($que->result() as $vie) {
            if ($vie->pembelian == null) $vie->pembelian = 0;
            if ($vie->retur == null) $vie->retur = 0;
            if ($vie->bbm == null) $vie->bbm = 0;
            if ($vie->penjualan == null) $vie->penjualan = 0;
            if ($vie->bbk == null) $vie->bbk = 0;
            if ($vie->git == null) $vie->git = 0;
            if ($vie->ketoko == null) $vie->ketoko = 0;
            if ($vie->daritoko == null) $vie->daritoko = 0;
            $blawal = substr($iperiode, 4, 2) - 1;
            if ($blawal == 0) {
              $perawal = substr($iperiode, 0, 4) - 1;
              $perawal = $perawal . '12';
            } else {
              $perawal = substr($iperiode, 0, 4);
              $perawal = $perawal . (substr($iperiode, 4, 2) - 1);
            }
            $quer = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$vie->product'");
            if ($quer->num_rows() > 0) {
              foreach ($quer->result() as $ni) {
                $sawal = $ni->n_stockopname;
              }
              $akhir = $sawal + ($vie->pembelian + $vie->retur + $vie->bbm) - ($vie->penjualan + $vie->bbk);
              if (substr($vie->product, 0, 1) == 'Z') $grade = 'B';
              else $grade = 'A';
              if ($store == 'AA') $loc = '01';
              else $loc = '00';
              $opname = 0;
              $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
              if ($qur->num_rows() > 0) {
                foreach ($qur->result() as $nu) {
                  $opname = $nu->n_stockopname;
                }
              }
              $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                           i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                           n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                           n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                           n_mutasi_daritoko)
                                                           values
                                                          ('$iperiode','$store','$vie->product','$grade','00',
                                                           '$loc','00',$sawal,
                                                           $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname
                                                           ,$vie->git,$vie->ketoko,$vie->daritoko)");
            } else {
              $sawal = 0;
              $akhir = $sawal + ($vie->pembelian + $vie->retur + $vie->bbm + $vie->daritoko) - ($vie->penjualan + $vie->bbk + $vie->ketoko);
              if (substr($vie->product, 0, 1) == 'Z') $grade = 'B';
              else $grade = 'A';
              if ($store == 'AA') $loc = '01';
              else $loc = '00';
              $opname = 0;
              $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
              if ($qur->num_rows() > 0) {
                foreach ($qur->result() as $nu) {
                  $opname = $nu->n_stockopname;
                }
              }
              $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                           i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                           n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                           n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                           n_mutasi_daritoko)
                                                           values
                                                          ('$iperiode','$store','$vie->product','$grade','00',
                                                           '$loc','00',$sawal,$vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,
                                                           $vie->bbk,$akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
            }
            if ($iperiode > '201512') {
              $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                         from f_mutasi_stock_pusat_saldoakhir('$iperiode') where i_product='$vie->product'");
            } else {
              $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                         from f_mutasi_stock_pusat('$iperiode') where i_product='$vie->product'");
            }
            if ($qu->num_rows() > 0) {
              foreach ($qu->result() as $vei) {
                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
                $row    = $querys->row();
                if ($row->c == $iperiode) {
                  $this->db->query("  update tm_ic set 
                                        n_quantity_stock=$vei->n_saldo_akhir
                                        where i_store='$store' and i_product='$vei->i_product'
                                        and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                }
              }
            }
          }
        } else {

          $blawal = substr($iperiode, 4, 2) - 1;
          if ($blawal == 0) {
            $perawal = substr($iperiode, 0, 4) - 1;
            $perawal = $perawal . '12';
          } else {
            $perawal = substr($iperiode, 0, 4);
            $perawal = $perawal . (substr($iperiode, 4, 2) - 1);
          }
          $quer = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                     where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$gie->i_product'");
          if ($quer->num_rows() > 0) {
            foreach ($quer->result() as $ni) {
              $stock = $ni->n_stockopname;
            }
            if ($stock > 0) {
              $sawal = $stock;
              $akhir = $sawal;
              if (substr($gie->i_product, 0, 1) == 'Z') $grade = 'B';
              else $grade = 'A';
              if ($store == 'AA') $loc = '01';
              else $loc = '00';
              $opname = 0;
              $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$gie->i_product'");
              if ($qur->num_rows() > 0) {
                foreach ($qur->result() as $nu) {
                  $opname = $nu->n_stockopname;
                }
              }
              $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                           i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                           n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                           n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
                                                           values
                                                          ('$iperiode','$store','$gie->i_product','$grade','00',
                                                           '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname)");
            }
          } else {
            $blawal = substr($perawal, 4, 2) - 1;
            if ($blawal == 0) {
              $perawalx = substr($perawal, 0, 4) - 1;
              $perawalx = $perawalx . '12';
            } else {
              $perawalx = substr($perawal, 0, 4);
              $perawalx = $perawalx . (substr($perawal, 4, 2) - 1);
            }
            $quer = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where e_mutasi_periode='$perawalx' and i_area='$iarea' and i_product='$gie->i_product'");
            if ($quer->num_rows() > 0) {
              foreach ($quer->result() as $ni) {
                $stock = $ni->n_stockopname;
              }
              if ($stock > 0) {
                $sawal = $stock;
                $akhir = $sawal;
                if (substr($gie->i_product, 0, 1) == 'Z') $grade = 'B';
                else $grade = 'A';
                if ($store == 'AA') $loc = '01';
                else $loc = '00';
                $opname = 0;
                $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                            where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$gie->i_product'");
                if ($qur->num_rows() > 0) {
                  foreach ($qur->result() as $nu) {
                    $opname = $nu->n_stockopname;
                  }
                }
                $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                             i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                             n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                             n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
                                                             values
                                                            ('$iperiode','$store','$gie->i_product','$grade','00',
                                                             '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname)");
              }
            }
          }
        }
      }
    }
    ######
    $que = $this->db->query("select area,periode,product,penjualan,pembelian,bbm,bbk,retur,git,pesan,ketoko,daritoko
                               from vmutasi where periode='$iperiode' and area='$iarea' 
                               and product not in(select i_product from tm_ic where i_store='$store')");
    if ($que->num_rows() > 0) {
      foreach ($que->result() as $vie) {
        if ($vie->pembelian == null) $vie->pembelian = 0;
        if ($vie->retur == null) $vie->retur = 0;
        if ($vie->bbm == null) $vie->bbm = 0;
        if ($vie->penjualan == null) $vie->penjualan = 0;
        if ($vie->bbk == null) $vie->bbk = 0;
        if ($vie->git == null) $vie->git = 0;
        if ($vie->ketoko == null) $vie->ketoko = 0;
        if ($vie->daritoko == null) $vie->daritoko = 0;
        $blawal = substr($iperiode, 4, 2) - 1;
        if ($blawal == 0) {
          $perawal = substr($iperiode, 0, 4) - 1;
          $perawal = $perawal . '12';
        } else {
          $perawal = substr($iperiode, 0, 4);
          $perawal = $perawal . (substr($iperiode, 4, 2) - 1);
        }
        $quer = $this->db->query(" select * from tm_stockopname_item 
                                   where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$vie->product'");
        if ($quer->num_rows() > 0) {
          foreach ($quer->result() as $ni) {
            $sawal = $ni->n_stockopname;
          }
          $akhir = $sawal + ($vie->pembelian + $vie->retur + $vie->bbm + $vie->daritoko) - ($vie->penjualan + $vie->bbk + $vie->ketoko);
          if (substr($vie->product, 0, 1) == 'Z') $grade = 'B';
          else $grade = 'A';
          if ($store == 'AA') $loc = '01';
          else $loc = '00';
          $opname = 0;
          $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                      where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
          if ($qur->num_rows() > 0) {
            foreach ($qur->result() as $nu) {
              $opname = $nu->n_stockopname;
            }
          }
          $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                       n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                       n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                       n_mutasi_daritoko)
                                                       values
                                                      ('$iperiode','$store','$vie->product','$grade','00',

                                                       '$loc','00',$sawal,
                                                       $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,
                                                       $vie->git,$vie->ketoko,$vie->daritoko)");
        } else {
          $sawal = 0;
          $akhir = $sawal + ($vie->pembelian + $vie->retur + $vie->bbm + $vie->daritoko) - ($vie->penjualan + $vie->bbk + $vie->ketoko);
          if (substr($vie->product, 0, 1) == 'Z') $grade = 'B';
          else $grade = 'A';
          if ($store == 'AA') $loc = '01';
          else $loc = '00';
          $opname = 0;
          $qur = $this->db->query(" select n_stockopname from tm_stockopname_item 
                                      where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
          if ($qur->num_rows() > 0) {
            foreach ($qur->result() as $nu) {
              $opname = $nu->n_stockopname;
            }
          }
          $this->db->query("  insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
                                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
                                                       n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
                                                       n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                       n_mutasi_daritoko)
                                                       values
                                                      ('$iperiode','$store','$vie->product','$grade','00',
                                                       '$loc','00',$sawal,$vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,
                                                       $akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
        }
        if ($iperiode > '201512') {
          $qu = $this->db->query("select * from f_mutasi_stock_pusat_saldoakhir('$iperiode') where i_product='$vie->product'");
        } else {
          $qu = $this->db->query("select * from f_mutasi_stock_pusat('$iperiode') where i_product='$vie->product'");
        }
        if ($qu->num_rows() > 0) {
          foreach ($qu->result() as $vei) {
            $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
            $row    = $querys->row();
            if ($row->c == $iperiode) {
              $this->db->query("  insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                                    i_store_location, i_store_locationbin,f_product_active) 
                                    values
                                    ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade','$store','$loc','00','t')");
            }
          }
        }
      }
    }
    ######
  }
  function detail($istorelocation, $iperiode, $iarea, $iproduct, $iproductgrade)
  {
    $this->db->select(" *
                   FROM vmutasidetailv2 a
                   WHERE a.periode='$iperiode' 
                     AND a.area='AA' 
                     AND a.product='$iproduct'
                     and a.i_product_grade='$iproductgrade' 
                    and a.loc='$istorelocation' 
                   order by dreff, urut, ireff", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacaarea($num, $offset, $iuser)
  {
    $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store and b.i_store='AA'
                        and i_area in ( select i_area from tm_user_area where i_user='$iuser')
                        order by b.i_store, c.i_store_location", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function cariarea($cari, $num, $offset, $iuser)
  {
    $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name 
                         from tr_area a, tr_store b 
                         where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                         and a.i_store=b.i_store and i_area in ( select i_area from tm_user_area where i_user='$iuser')
                         order by a.i_store ", FALSE)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
