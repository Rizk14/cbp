<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
      parent::__construct();
	#$this->CI =& get_instance();
  }
  function bacasaldo($iarea,$dfrom,$dto)
  {
		$ada		= false;
    $i=0;
		while(!$ada)
		{
      $i++;
			$tmp 		= explode("-", $dfrom);
			$tahun	= $tmp[2];
			$bulan	= $tmp[1];
			$tanggal= $tmp[0];
			$dsaldo	= $tahun."/".$bulan."/".$tanggal;
			$dtos		=$this->mmaster->dateAdd("d",-1,$dsaldo);
			$tmp 		= explode("-", $dtos);
			$th			= $tmp[0];
			$bl			= $tmp[1];
			$dt 		= $tmp[2];
			$dfrom	= $dt."-".$bl."-".$th;
			$this->db->select("	* from tm_ikhp_saldo where i_area='$iarea' and d_bukti = to_date('$dfrom','dd-mm-yyyy')",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				$ada	= true;
				return $query->result();
			}
      if($i>365){
        $ada=true;
      }
		}
  }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function eareaname($area) 
    {
		return $this->db->query(" SELECT e_area_name FROM tr_area WHERE i_area='$area' ");
	}
}
?>
