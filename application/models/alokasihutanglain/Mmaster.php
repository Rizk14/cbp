<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name,
                               sum(c.v_sisa) as v_sisa from tm_dt a, tr_area b, tm_dt_item c
                             where a.i_area = b.i_area
                             and a.f_sisa = 't'
                             and a.i_dt=c.i_dt and a.i_area=c.i_area
                             and (upper(a.i_dt) like '%$cari%'
                               or upper(b.e_area_name) like '%$cari%'
                               or upper(a.i_area) like '%$cari%')
                             group by a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name
                             order by a.i_dt desc ",false)->limit($num,$offset);
               //and c.v_sisa<>c.v_jumlah
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($cari,$num,$offset)
    {
      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
               where a.i_customer=b.i_customer
               and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
               and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
               or upper(a.i_spb) like '%$cari%' or upper(a.i_nota) like '%$cari%')
               order by a.i_nota desc ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
  function insertheader($ialokasi,$iarea,$icustomer,$dalokasi,$vjumlah,$vlebih,$ibank,$icoabank,$edescription,$idt,$areadt)
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_alokasihl (i_alokasi,i_area,i_customer,d_alokasi,v_jumlah,v_lebih,d_entry,i_coa_bank,i_kbank,e_description,i_dt,i_area_dt)
                          values
                        ('$ialokasi','$iarea','$icustomer','$dalokasi',$vjumlah,$vlebih,'$dentry','$icoabank','$ibank','$edescription', '$idt', '$areadt')");

    if($vlebih>0){
      $this->db->query("UPDATE tm_alokasihl_reff SET f_lebih_bayar='t' WHERE i_kbank='$ibank' AND i_area = '$iarea'");      
    }

  }
  function inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dbukti )
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $egirodescription=str_replace("'","''",$egirodescription);
    $this->db->query("insert into tm_jurnal_transharian 
             (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
                  values
               ('$ireff','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
  }
  function inserttranskredit($ikbank,$iarea,$dalokasi,$icoabank)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_jurnal_transharian
             (i_refference, i_area, d_entry,d_refference, d_mutasi, i_coa_bank)
                  values
               ('$ikbank','00','$dalokasi','$dalokasi','$dalokasi','$icoabank')");
  }

  function inserttransdebet($ikbank,$iarea,$dalokasi,$icoabank)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_jurnal_transharian
             (i_refference, i_area, d_entry,d_refference, d_mutasi, i_coa_bank)
                  values
               ('$ikbank','00','$dalokasi','$dalokasi','$dalokasi','$icoabank')");
  }
  function insertgldebet($acckredit,$ireff,$namadebet,$fdebet,$iarea,$vjumlah,$dalokasi,$egirodescription)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_general_ledger
             (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
                  values
               ('$ireff','$acckredit','$dalokasi','$namadebet','$fdebet',$vjumlah,'$iarea','$dalokasi','$egirodescription','$dentry')");
  }
  function insertglkredit($accdebet,$ireff,$namakredit,$fdebet,$iarea,$vjumlah,$dalokasi,$egirodescription)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_general_ledger
             (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
                  values
               ('$ireff','$accdebet','$dalokasi','$namakredit','$fdebet',$vjumlah,'$iarea','$dalokasi','$egirodescription','$dentry')");
  }
  function inserttransitemkredit($acckredit,$ireff,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dalokasi)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_jurnal_transharianitem
             (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
                  values
               ('$acckredit','$ireff','$namakredit','$fdebet','$fposting','$vjumlah','$dalokasi','$dalokasi','$dentry')");
  }
  function inserttransitemdebet($accdebet,$ireff,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dalokasi)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_jurnal_transharianitem
             (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
                  values
               ('$accdebet','$ireff','$namadebet','$fdebet','$fposting','$vjumlah','$dalokasi','$dalokasi','$dentry')");
  }
  function namaacc($icoa)
    {
    $this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      foreach($query->result() as $tmp)     
      {
        $xxx=$tmp->e_coa_name;
      }
      return $xxx;
    }
    }
   function jmlasalkn(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->select("* from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function deleteheader(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->query("delete from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
   }
   function updatebank($ikbank,$icoabank,$isupplier,$pengurang)
    {
      $this->db->query("update tm_kbank set v_sisa=v_sisa-$pengurang where i_kbank='$ikbank' and i_coa_bank='$icoabank'");
    }
   function updategiro($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_giro set v_sisa=v_sisa-$pengurang+$asal, f_giro_use='t'
                    where i_giro='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where i_customer_groupbayar='$group'))");
    }
   function updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear)
    {
      $this->db->query("update tm_kum set v_sisa=v_sisa-$pengurang+$asal
                          where i_kum='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group')) and n_kum_year='$nkuyear'");
    }
   function updatekn($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_kn set v_sisa=v_sisa-$pengurang+$asal
                        where i_kn='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar
                        where i_customer_groupbayar='$group'))");
    }
   function updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal)
    {
      $this->db->query("update tm_pelunasan_lebih set v_lebih=0
                          where i_pelunasan='$egirobank' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group'))");
    }
  function updatesaldo($group,$icustomer,$pengurang)
    {
      $this->db->query("update tr_customer_groupar set v_saldo=v_saldo-$pengurang
                          where i_customer='$icustomer' and i_customer_groupar='$group'");
    }
   function insertdetail($ialokasi,$iarea,$icustomer,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$ibank, $areabank)
  {
    $tmp=$this->db->query(" select i_alokasi from tm_alokasihl_item
                            where i_alokasi='$ialokasi' and i_area='$iarea' and i_nota='$inota'", false);
    if($tmp->num_rows()>0){
      $this->db->query("update tm_alokasihl_item set d_nota='$dnota',v_jumlah=$vjumlah,v_sisa=$vsisa,n_item_no=$i,
                        e_remark='$eremark'
                        where i_alokasi='$ialokasi' and i_customer='$icustomer' and i_nota='$inota'");

      $this->db->query("UPDATE tm_alokasihl_reff SET v_sisa = v_sisa-$vjumlah
                        where i_kbank in(SELECT i_kbank FROM tm_kbank WHERE i_kbank='$ibank' AND i_area='$areabank')");
    }else{
        $this->db->query("insert into tm_alokasihl_item
                      ( i_alokasi,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark)
                      values
                      ('$ialokasi','$iarea','$inota','$dnota',$vjumlah,$vsisa,$i,'$eremark')");

        $this->db->query("update tm_alokasihl_reff set v_sisa=v_sisa-$vjumlah
                        where i_kbank='$ibank' and i_area='$areabank' ");
    }
  }
   function updatedt($idt,$iarea,$ddt,$inota,$vsisa)
    {
      $this->db->query("update tm_dt_item set v_sisa=$vsisa where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt' and i_nota='$inota'");
    }
   function updatenota($inota,$vsisa)
    {
      $this->db->select(" v_sisa from tm_nota where i_nota='$inota'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){
          $sisa=$xx->v_sisa-$vsisa;
          if($sisa<0){
            return false;
            break;
          }else{
            $this->db->query("update tm_nota set v_sisa=v_sisa-$vsisa where i_nota='$inota'");
            return true;
          }
        }
      }else{
        return false;
      }
    }
  function hitungsisadt($idt,$iarea,$ddt)
    {
      $this->db->select(" sum(v_sisa) as v_sisa from tm_dt_item
               where i_area='$iarea'
               and i_dt='$idt' and d_dt='$ddt'
               group by i_dt, i_area",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
            $jml=$row->v_sisa;
         }
         return $jml;
      }
  }
   function updatestatusdt($idt,$isupplier,$ddt)
    {
      $this->db->query("update tm_dt set f_sisa='f' where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'");
    }
   function deletedetail($ipl,$idt,$iarea,$inota,$ddt)
    {
      $this->db->query("DELETE FROM tm_pelunasan_item WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'
                      and i_nota='$inota' and d_dt='$ddt'");
    }
   function bacasupplier($iarea,$num,$offset){
      $this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') order by i_supplier",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carisupplier($cari,$isupplier,$num,$offset){
      if($cari=='sikasep'){
        $this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') order by i_supplier ",FALSE)->limit($num,$offset);      
      }else{
        $this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') order by i_supplier ",FALSE)->limit($num,$offset);      
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagiro($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select(" a.* from tm_giro a, tr_customer_groupar b
                     where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_giro a, tr_customer_groupbayar b
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
#                    and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carisaldo($icoa,$iperiode)
  {
    $query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
    if ($query->num_rows() > 0)
    {
      $row = $query->row();
      return $row;
    } 
  }
	function carigiro($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_giro a, tr_customer_groupar b
							where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
							and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
              and (upper(a.i_giro) like '%$cari%') and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
							order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
##########
   function bacatunai($icustomer,$iarea,$num,$offset,$group,$dbukti){

     $coa='111.3'.$iarea;
     $this->db->select("a.*, c.d_rtunai, e.e_bank_name, e.i_coa, c.i_bank
                        from tm_tunai a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d, tr_bank e
                        where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                        and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$dbukti'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and c.i_bank=e.i_bank
                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                        ",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function caritunai($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
	  $coa='111.3'.$iarea;
    $this->db->select("a.*, c.d_rtunai, e.e_bank_name, e.i_coa, c.i_bank
                      from tm_tunai a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d, tr_bank e
                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                      and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                      and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$dbukti'
                      and a.v_sisa>0 and a.v_sisa=a.v_jumlah and c.i_bank=e.i_bank
                      and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and (upper(a.i_tunai) like '%$cari%')
                      ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
   function updatetunai($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_tunai set v_sisa=v_sisa-$pengurang+$asal
                    where i_tunai='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where i_customer_groupbayar='$group'))");
    }
##########
   function bacakn($icustomer,$iarea,$num,$offset,$group,$xdbukti){
      /*$this->db->select(" a.* from tm_kn a, tr_customer_groupar b
                               where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0
                               order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      */

      $this->db->select(" a.* from tm_kn a, tr_customer_groupbayar b
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                     and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f'
                     order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);

      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carikn($cari,$icustomer,$iarea,$num,$offset,$group,$xdbukti){
      $this->db->select(" a.* from tm_kn a, tr_customer_groupar b
                               where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0 and d_kn<='$xdbukti'
                        and (upper(i_kn) like '%$cari%') and a.f_kn_cancel='f'
                               order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaku($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select("a.* from tm_kum a, tr_customer_groupar b
                         where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.d_kum<='$dbukti'
                         and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                         order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_kum a, tr_customer_groupbayar b
                             where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                             and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f'
                             order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function cariku($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_kum a, tr_customer_groupar b
					              where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
					              and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and (upper(a.i_kum) like '%$cari%') and a.d_kum<='$dbukti'
					              order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
   function bacaku2($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.* from tm_kum a
               where a.i_customer='$icustomer'
               and a.i_area='$iarea'
               and a.v_sisa>0
               and a.f_close='f'
               order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagirocek($num,$offset,$area){
    if($area=='00'||$area=='PB'){
      $this->db->select(" * from tr_jenis_bayar order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_jenis_bayar where i_jenis_bayar<>'05' order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function runningnumberpl($iarea,$thbl){
      $th   = substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
      $this->db->select(" n_modul_no as max from tm_dgu_no
                        where i_modul='AHL'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea' for update", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
           $terakhir=$row->max;
         }
         $noal  =$terakhir+1;
         $this->db->query(" update tm_dgu_no
                          set n_modul_no=$noal
                          where i_modul='AHL'
                          and substr(e_periode,1,4)='$th'
                          and i_area='$iarea'", false);
         settype($noal,"string");
         $a=strlen($noal);
         while($a<5){
           $noal="0".$noal;
           $a=strlen($noal);
         }
         $noal  ="AH-".$thbl."-".$noal;
         return $noal;
      }else{
         $noal  ="00001";
         $noal  ="AH-".$thbl."-".$noal;
         $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                         values ('AHL','$iarea','$asal',1)");
         return $noal;
      }
    }
   function bacapelunasan($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan
               from tm_pelunasan_lebih a, tr_customer_groupbayar b
            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area ",FALSE)->limit($num,$offset);
#              and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caripelunasan($cari,$icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
                and (upper(a.i_pelunasan) like '%$cari%') 
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
  function cekpl($iarea,$ipl,$idt){
      $this->db->select(" i_pelunasan from tm_pelunasan where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        $this->db->select(" max(substring(i_pelunasan,9,2)) as no from tm_pelunasan where i_pelunasan like '$idt%' and i_area='$iarea'",FALSE);
        $quer = $this->db->get();
        if ($quer->num_rows() > 0){
          foreach($quer->result() as $tmp){
            $nopl=$tmp->no+1;
            break;
          }
        }
        settype($nopl,"string");
        $a=strlen($nopl);
        while($a<2){
          $nopl="0".$nopl;
          $a=strlen($nopl);
        }
        $nopl  = $idt."-".$nopl.substr($ipl,10,1);
        return $nopl;
      }else{
      return $ipl;
    }
   }
   function bacadt($iarea, $dfrom, $dto, $cari, $num, $offset, $iuser)
  {
    $query  = $this->db->select(" DISTINCT a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah FROM tm_dt a, tm_dt_item b, tm_nota c
                                  WHERE 
                                  a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt 
                                  AND b.i_nota=c.i_nota AND b.i_customer=c.i_customer AND b.i_area=c.i_area
                                  AND c.v_sisa>0 AND c.f_nota_cancel='f'
                                  AND a.d_dt >= '$dfrom' AND a.d_dt <= '$dto' 
                                  AND a.i_area IN(SELECT i_area FROM tm_user_area WHERE i_user='$iuser')
                                  AND (UPPER(a.i_dt) LIKE '%$cari%') 
                                  ORDER BY a.i_area, a.d_dt DESC", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacapl($ialokasi,$iarea){
      $this->db->select(" a.*, b.e_customer_name from tm_alokasihl a
                         inner join tr_customer b on (a.i_customer=b.i_customer)
                         where a.i_alokasi='$ialokasi' and a.i_area='$iarea'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function sisa($ialokasi,$iarea){
      $this->db->select(" sum(v_sisa)as sisa from tm_kbank where i_kbank='$ikbank'",FALSE);
      $query = $this->db->get();
      foreach($query->result() as $isi){
         return $isi->sisa;
      }
   }
   function bulat($isupplier,$ialokasi,$ikbank){
      $bulat=0;
      $reff=$ialokasi.'|'.$ikbank;
      $this->db->select(" sum(v_mutasi_debet) as bulat from tm_general_ledger where i_refference='$reff' and i_area='00'",FALSE);
      $query = $this->db->get();
      if($query->num_rows()>0){
        foreach($query->result() as $isi){
           $bulat=$isi->bulat;
        }
      }
      return $bulat;
   }
   function bacadetailpl($ialokasi,$iarea){
      $this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_nota_netto as v_nota
                          from tm_alokasihl_item a
                          inner join tm_nota b on (a.i_nota=b.i_nota)
                          where a.i_alokasi = '$ialokasi' and a.i_area='$iarea'
                          order by a.i_alokasi",FALSE);
#
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacanota($icustomer,$idt,$num,$offset,$group){
      $this->db->select(" a.*, b.* FROM tm_nota a,  tr_customer_groupbayar b WHERE  
                          a.i_customer = b.i_customer AND f_nota_cancel = 'f' AND a.v_sisa > 0 AND NOT a.i_nota IS NULL
                          AND b.i_customer_groupbayar = '$group'
                          AND a.i_nota IN(SELECT i_nota FROM tm_dt_item WHERE i_customer = '$icustomer' AND i_dt = '$idt')
                          ORDER BY i_nota ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carinota($icustomer,$num,$offset,$group,$cari){
      $this->db->select("a.*, b.* from tm_nota a,  tr_customer_groupbayar b where  
                                    a.i_customer=b.i_customer and f_nota_cancel='f' and a.v_sisa>0 and b.i_customer_groupbayar='$group'
                                    and (upper(a.i_nota) like '%$cari%')  and f_nota_cancel='f' ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
   }

  function bacabank($num,$offset,$iarea,$dalokasi,$sm){
    /*$this->db->select(" * from tm_kbank where f_kbank_cancel='false' and i_area='$iarea' and i_coa='$sm' and d_bank<='$dalokasi' 
                        order by i_kbank", false)->limit($num,$offset);*/

    $this->db->select(" b.i_area,  a.i_kbank, a.i_coa_bank, b.e_description, a.v_sisa, f_lebih_bayar
                        FROM tm_alokasihl_reff a
                        INNER JOIN tm_kbank b on(a.i_kbank=b.i_kbank AND a.i_area=b.i_area)
                        WHERE a.v_sisa>0  AND (a.i_area='$iarea' OR b.i_area='XX')
                        ORDER BY i_area, i_kbank", false)->limit($num,$offset);

    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }

   function bacaperiode($iperiode)
   {
/*
      $this->db->select(" a.i_kbank, a.i_area, a.d_bank, a.v_bank, b.e_area_name, c.e_bank_name, a.i_coa_bank, a.v_sisa 
                          from tm_kbank a, tr_area b, tr_bank c
                          where a.i_area=b.i_area and a.f_kbank_cancel='false'
                          and a.d_bank >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_bank <= to_date('$dto','dd-mm-yyyy')
                          and a.i_coa_bank=c.i_coa
                          and c.i_bank='$ibank'
                          and a.v_sisa>0
                          and a.i_coa like '%210-1%'",false)->limit($num,$offset);
*/
      //$hutanglain=HutangLain;
      $hutanglain=HutangLain;
      $this->db->select(" v_saldo_akhir from tm_coa_saldo where i_coa='$hutanglain' and i_periode='$iperiode'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacahlreff($iperiode)
   {
      $this->db->select(" * FROM tm_alokasihl_reff 
                          WHERE i_periode>='201901' AND i_periode<='$iperiode' 
                          ORDER BY i_periode DESC,i_area, i_kbank",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }

   function bacaremark($num,$offset) {
      $this->db->select("* from tr_pelunasan_remark order by i_pelunasan_remark", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function caribank($cari,$num,$offset,$iarea,$dalokasi,$sm){
      /*$this->db->select("* from tm_kbank where (upper(i_kbank) like '%$cari%' or upper(e_description) like '%$cari%')
                                    and  f_kbank_cancel='false' and i_coa='$sm' and d_bank<='$dalokasi' ", FALSE)->limit($num,$offset);*/

      $this->db->select(" b.i_area, a.i_kbank, a.i_coa_bank, b.e_description, a.v_sisa, f_lebih_bayar
                          FROM tm_alokasihl_reff a
                          INNER JOIN tm_kbank b on(a.i_kbank=b.i_kbank)
                          WHERE a.v_sisa>0 AND (UPPER(a.i_kbank) LIKE '%$cari%' OR UPPER(b.e_description) LIKE '%$cari%')
                          AND (a.i_area='$iarea' OR b.i_area='XX')
                          ORDER BY i_area, i_kbank ", FALSE)->limit($num,$offset);

      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
   function bacacustomer($cari,$iarea,$num,$offset){
      $this->db->select(" * from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                          and i_area='$iarea' order by i_customer",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caricustomer($cari,$iarea,$idt,$num,$offset){
      if($cari=='SIKASEP'){
        $this->db->select(" DISTINCT a.i_dt, a.d_dt, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_city
                            FROM tm_dt_item a
                            LEFT JOIN tm_dt b ON(a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt AND f_dt_cancel='f'), tr_customer c
                            WHERE 
                            a.i_customer=c.i_customer AND a.i_area = '$iarea' AND a.i_dt='$idt' ",FALSE)->limit($num,$offset);      
      }else{
        $this->db->select(" DISTINCT a.i_dt, a.d_dt, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_city
                            FROM tm_dt_item a
                            LEFT JOIN tm_dt b ON(a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt AND f_dt_cancel='f'), tr_customer c
                            WHERE 
                            a.i_customer=c.i_customer AND a.i_area = '$iarea' AND a.i_dt='$idt' 
                            AND (UPPER(a.i_customer) LIKE '%$cari%' OR UPPER(c.e_customer_name) LIKE '%$cari%' OR UPPER(a.i_dt) LIKE '%$cari%') ",FALSE)->limit($num,$offset);      
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
