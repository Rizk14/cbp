<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    		#$this->CI =& get_instance();
    }
    function baca($istockopname,$istore,$istorelocation)
    {
		  $this->db->select("a.i_stockopname, to_char(a.d_stockopname,'dd-mm-yyyy') as d_stockopname, 
						     a.i_store, a.i_store_location, a.i_area,
						     b.e_store_name, c.e_store_locationname 
						     from tm_stockopname a, tr_store b, tr_store_location c
						     where a.i_store=b.i_store
							   and a.i_store_location=c.i_store_location
							   and b.i_store=c.i_store
							   and a.i_stockopname = '$istockopname'
							   and a.i_store='$istore'
							   and a.i_store_location='$istorelocation'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($istockopname,$istore,$istorelocation)
    {
		  $this->db->select("	* from tm_stockopname_item 
							  inner join tr_product_motif on (tm_stockopname_item.i_product=tr_product_motif.i_product
															  and tm_stockopname_item.i_product_motif=tr_product_motif.i_product_motif)
						     	where tm_stockopname_item.i_stockopname = '$istockopname'
							  and tm_stockopname_item.i_store='$istore'
							  and tm_stockopname_item.i_store_location='$istorelocation'
						     	order by tm_stockopname_item.i_product", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function inseritemcustomer($periode,$icustomer,$vtarget,$ntarget,$isalesman,$iarea,$icity)
    {

      $que=$this->db->query(" SELECT *
                              FROM tm_target_itemcustomer 
                              WHERE i_periode = '$periode' AND i_customer = '$icustomer'", false);
        if($que->num_rows()>0){
          $this->db->query("DELETE FROM tm_target_itemcustomer where i_periode = '$periode' and i_customer = '$icustomer' ");
        }

          $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
		      $row   	= $query->row();
		      $dentry	= $row->c;
    	    $this->db->set(
    	    	array(
              'i_periode'   => $periode,
              'i_customer'  => $icustomer,
              'i_area'      => $iarea,
              'i_salesman'  => $isalesman,
              'i_city'      => $icity,
              'v_target'    => $vtarget,
              'n_target'    => $ntarget
            	
    	    	)
    	    );
        
    	    $this->db->insert('tm_target_itemcustomer');
      }

      function insert_target_sales($iperiode,$iarea,$isalesman,$vtarget,$nqty)
      {
          $que=$this->db->query(" SELECT *
                                  FROM tm_target_itemsls 
                                  WHERE i_periode = '$iperiode' AND i_area = '$iarea' AND i_salesman = '$isalesman'", false);
          if($que->num_rows()>0){
            $this->db->query("DELETE FROM tm_target_itemsls where i_periode = '$iperiode' and i_area = '$iarea' AND i_salesman = '$isalesman' ");
          }

            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
            $row   	= $query->row();
            $dentry	= $row->c;
            $this->db->set(
              array(
                'i_periode'   => $iperiode,
                'i_area'      => $iarea,
                'i_salesman'  => $isalesman,
                'v_target'    => $vtarget,
                'n_target'    => $nqty
                /* 'd_entry'     => $dentry */

              )
            );
            $this->db->insert('tm_target_itemsls');
        }


        function insert_target_kota($iperiode,$iarea,$isalesman,$vtarget,$nqty,$icity){
            $que=$this->db->query(" SELECT *
                                    FROM tm_target_itemkota 
                                    WHERE i_periode = '$iperiode' AND i_area = '$iarea' AND i_city = '$icity' and i_salesman = '$isalesman'", false);
              if($que->num_rows()>0){
              $this->db->query("DELETE FROM tm_target_itemkota where i_periode = '$iperiode' and i_area = '$iarea' AND i_city = '$icity' ");
              }

              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
              $dentry	= $row->c;
              $this->db->set(
              array(
                  'i_periode'   => $iperiode,
                  'i_area'      => $iarea,
                  'i_salesman'  => $isalesman,
                  'v_target'    => $vtarget,
                  'n_target'    => $nqty,
                  'i_city'      => $icity,
                  'd_entry'     => $dentry
              )
              );
              $this->db->insert('tm_target_itemkota');
        }

        function insert_targetheader($periode,$iarea, $vtarget, $nqty){
          $que=$this->db->query(" SELECT *
                                  FROM tm_target 
                                  WHERE i_periode = '$periode' AND i_area = '$iarea'", false);
            if($que->num_rows()>0){
            $this->db->query("DELETE FROM tm_target where i_periode = '$periode' and i_area = '$iarea'");
            }

            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
            $row   	= $query->row();
            $dentry	= $row->c;
            $this->db->set(
            array(
                'i_periode'   => $periode,
                'i_area'      => $iarea,
                'v_target'    => $vtarget,
                'n_target'    => $nqty,
                'd_entry'     => $dentry
            )
            );
            $this->db->insert('tm_target');
      }

    function updateheader($istockopname, $dstockopname, $istore, $istorelocation)
    {
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
    	$data = array(
               	'i_stockopname' 			=> $istockopname,
               	'd_stockopname' 			=> $dstockopname,
			       		'i_store' 						=> $istore,
			       		'i_store_location'		=> $istorelocation,
								'f_stockopname_cancel'=> 'f',
                'd_update'            => $dentry
            );
		$this->db->where('i_stockopname', $istockopname);
		$this->db->where('i_store', $istore);
		$this->db->where('i_store_location', $istorelocation);
		$this->db->update('tm_stockopname', $data); 
    }
   
    function delete($istockopname, $istore) 
    {
  		return TRUE;
    }
    function bacasemua()
    {
		  $this->db->select("	* from tm_stockopname order by i_stockopname desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaproduct($cari,$num,$offset,$istore,$istorelocation)
    {
		  $this->db->select(" a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
													where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
													and a.i_store='$istore' and a.i_store_location='$istorelocation'
													and a.i_product_motif=b.i_product_motif
													and a.i_product=b.i_product
													order by a.i_product, a.i_product_grade ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacastore($area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													from tr_store_location a, tr_store b, tr_area c
													where a.i_store = b.i_store and b.i_store=c.i_store
													and (c.i_area = '$area1' or c.i_area = '$area2' or
													 c.i_area = '$area3' or c.i_area = '$area4' or
													 c.i_area = '$area5')",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumber($iarea,$thbl){
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_stockopname,9,2)) as max from tm_stockopname 
													where substr(i_stockopname,4,2)='$th' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<2){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="SO-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="01";
			  $noso  ="SO-".$thbl."-".$noso;
			  return $noso;
		  }
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" * from tm_stockopname where upper(i_stockopname) like '%$cari%' or upper(i_store) like '%$cari%' or upper(i_store_location) like '%$cari%' order by i_stockopname",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select(" distinct(c.i_store) as i_store , b.i_store_location, b.e_store_locationname,a.e_store_name 
													from tr_store a, tr_store_location b, tr_area c
													where a.i_store=b.i_store and b.i_store=c.i_store
														and (upper(a.i_store) like '%$cari%' or upper(a.e_store_name) like '%$cari%'
													or upper(b.i_store_location) like '%$cari%' or upper(b.e_store_locationname) like '%$cari%')
													and (c.i_area = '$area1' or c.i_area = '$area2' or
														 c.i_area = '$area3' or c.i_area = '$area4' or
														 c.i_area = '$area5') 
													order by i_store",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariproduct($cari,$num,$offset,$istore,$istorelocation)
    {
		  $this->db->select(" a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
							  where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							  and a.i_store='$istore' and a.i_store_location='$istorelocation'
							  and a.i_product_motif=b.i_product_motif
							  and a.i_product=b.i_product
							  order by a.i_product, a.i_product_grade",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function getdata($periode,$icustomer)
    {
      $query=$this->db->query(" SELECT distinct a.i_customer, a.i_salesman, a.i_area, b.i_city 
                                FROM tr_customer_salesman a 
                                INNER JOIN tr_customer b ON (a.i_customer = b.i_customer)
                                WHERE e_periode = '$periode' AND a.i_customer = '$icustomer'",false);
      if ($query->num_rows() > 0){
				return $query->row();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ido,$q_in,$q_out,$qdo,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      if($qdo>$q_ak){
        $qtmp=$qdo-$q_ak;
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', $qtmp, 0, $q_ak+$qtmp, $q_ak
                                  )
                                ",false);
      }elseif($qdo<$q_ak){
        $qtmp=$q_ak-$qdo;
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', 0, $qtmp, $q_ak-$qtmp, $q_ak
                                  )
                                ",false);
      }else{
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', 0, 0, $q_ak, $q_ak
                                  )
                                ",false);
      }
    }
    function updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_saldo_stockopname=$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.(substr($emutasiperiode,4,2)+1);
      }
      $que=$this->db->query(" Select * from tm_mutasi 
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                            ",false);
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $gitasal=$row->n_mutasi_git;
          $gitpenjualanasal=$row->n_git_penjualan;
        }
        if($gitasal==null)$gitasal=0;
        if($gitpenjualanasal==null)$gitpenjualanasal=0;
      }else{
        $gitasal=0;
        $gitpenjualanasal=0;
      }
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_saldo_awal=$qdo, n_saldo_akhir=($qdo+$gitasal+$gitpenjualanasal+n_mutasi_pembelian+n_mutasi_returoutlet
                                +n_mutasi_bbm)-(n_mutasi_penjualan+n_mutasi_returpabrik+n_mutasi_bbk),n_mutasi_gitasal=$gitasal,
                                n_git_penjualanasal=$gitpenjualanasal
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$perdpn'
                              ",false);
    }
    function insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $ada=false;
      $tmp=substr($emutasiperiode,4,2);
      switch($tmp){
      case '01':
        $th=substr($emutasiperiode,0,4)-1;
        $per=$th.'12';
        break;
      case '02':
        $per=substr($emutasiperiode,0,4).'01';
        break;
      case '03':
        $per=substr($emutasiperiode,0,4).'02';
        break;
      case '04':
        $per=substr($emutasiperiode,0,4).'03';
        break;
      case '05':
        $per=substr($emutasiperiode,0,4).'04';
        break;
      case '06':
        $per=substr($emutasiperiode,0,4).'05';
        break;
      case '07':
        $per=substr($emutasiperiode,0,4).'06';
        break;
      case '08':
        $per=substr($emutasiperiode,0,4).'07';
        break;
      case '09':
        $per=substr($emutasiperiode,0,4).'08';
        break;
      case '10':
        $per=substr($emutasiperiode,0,4).'09';
        break;
      case '11':
        $per=substr($emutasiperiode,0,4).'10';
        break;
      case '12':
        $per=substr($emutasiperiode,0,4).'11';
        break;
      }
      $sal=0;
      $query=$this->db->query(" SELECT n_saldo_stockopname
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$per'
                              ",false);
      if ($query->num_rows() > 0){
        $isi=$query->row();
        $sal=$isi->n_saldo_stockopname;
				$ada=true;
			}
      if($sal==null){
        $sal=0;
      }
      if($ada){
        $query=$this->db->query(" 
                                  insert into tm_mutasi 
                                  (
                                    i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                    e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                    n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                  values
                                  ('$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation',
                                   '$istorelocationbin','$emutasiperiode',$sal,0,0,0,0,0,0,0,$qdo,'f')",false);
      }else{
        $query=$this->db->query(" 
                                  insert into tm_mutasi 
                                  (
                                    i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                    e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                    n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                  values
                                  ('$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation',
                                   '$istorelocationbin','$emutasiperiode',$qdo,0,0,0,0,0,0,0,$qdo,'f')",false);
      }
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.(substr($emutasiperiode,4,2)+1);
      }
      $que=$this->db->query(" Select * from tm_mutasi 
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                            ",false);
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $gitasal=$row->n_mutasi_git;
          $gitpenjualanasal=$row->n_git_penjualan;
        }
        if($gitasal==null)$gitasal=0;
        if($gitpenjualanasal==null)$gitpenjualanasal=0;
      }else{
        $gitasal=0;
        $gitpenjualanasal=0;
      }
      $this->db->query("delete from tm_mutasi 
                        where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                        and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                        and e_mutasi_periode='$perdpn' ");
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close,n_mutasi_git,
                                  n_mutasi_pesan,n_mutasi_ketoko,n_mutasi_daritoko,n_git_penjualan,n_mutasi_gitasal,n_git_penjualanasal)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation',
                                  '$istorelocationbin','$perdpn',$qdo,0,0,0,0,0,0,$qdo+$gitasal+$gitpenjualanasal,0,'f',0,0,0,0,0,
                                  $gitasal,$gitpenjualanasal)
                              ",false);
    }
    function updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qdo)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname',$qdo, 't'
                                )
                              ",false);
    }
    function namaprod($iproduct)
    {
      $nama='';
		  $this->db->select("e_product_name from tr_product
						             where i_product='$iproduct'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $nama=$tes->e_product_name;
        }
		  }
      return $nama;
    }
  }
?>
