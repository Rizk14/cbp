<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
  function baca($iperiode,$iarea,$num,$offset)
    {
		  $this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
						              where to_char(d_spb,'yyyymm')='$iperiode' and a.i_customer=b.i_customer
						              and i_area='$iarea' order by a.i_spb ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
  		}
    }
    function bacaperiode($iperiode){
		/*echo "select a.i_pelunasanap, a.i_supplier, s.e_supplier_name, a.i_area, date_part('month', a.d_bukti) as bulan,
                         date_part('year', a.d_bukti) as tahun, i.i_dtap, a.d_bukti, i.d_dtap, i.v_jumlah, i.v_sisa
                         FROM tm_pelunasanap_item i, tm_pelunasanap a, tr_supplier s
                         where to_char(a.d_bukti,'yyyymm')='$iperiode' and a.i_area = '$iarea'
                         and a.i_pelunasanap = i.i_pelunasanap and
                         a.i_supplier = s.i_supplier "; die(); */
    $query = $this->db->query("select a.i_pelunasanap, a.i_supplier, s.e_supplier_name, a.i_area, date_part('month', a.d_bukti) as bulan,
                               date_part('year', a.d_bukti) as tahun, i.i_dtap, a.d_bukti, i.d_dtap, i.v_jumlah, i.v_sisa
                               FROM tm_pelunasanap_item i, tm_pelunasanap a, tr_supplier s
                               where to_char(a.d_bukti,'yyyymm')='$iperiode' and a.i_area = '00'
                               and a.i_pelunasanap = i.i_pelunasanap and
                               a.i_supplier = s.i_supplier order by a.i_supplier, i.d_dtap");
    //$query    = $this->db->get();
      if($query->num_rows()>0){
      return $query->result();
      }
    }
  	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
