<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_ap WHERE i_ap=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_ap_item WHERE i_ap=\''.$iop.'\'');
    }
    function bacasemua($dfrom,$dto,$isupplier,$cari,$num,$offset)
    {
			$this->db->select(" * from v_list_do a
							left join tm_spb b on (b.i_spb=a.i_spb and b.i_area=a.i_spb_area)
							left join tm_spmb c on (c.i_spmb=a.i_spmb and c.i_area=a.i_spmb_area)
							where 
							a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy') and
							a.i_supplier='$isupplier' and
							upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%' or upper(a.i_spb) like '%$cari%'
							or upper(c.i_spmb_old) like '%$cari%' or upper(b.i_spb_old) like '%$cari%'
							order by i_do desc",false)->limit($num,$offset);

			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
       
     function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ido,$area)
    {
			$this->db->select(" * from tm_do
								inner join tr_supplier on (tm_do.i_supplier=tr_supplier.i_supplier)
								inner join tr_area on (tm_do.i_area=tr_area.i_area)
								where tm_do.i_do = '$ido' and tm_do.i_area='$area'
								order by tm_do.i_do desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetail($ido,$area)
    {
			$this->db->select(" * from tm_do_item
								inner join tr_product_motif on (tm_do_item.i_product_motif=tr_product_motif.i_product_motif
								and tm_do_item.i_product=tr_product_motif.i_product)
								where tm_do_item.i_do = '$ido' order by tm_do_item.i_do desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name, c.e_area_name from tm_ap a, tr_supplier b, tr_area c
							where a.i_supplier=b.i_supplier and a.i_area=c.i_area
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_ap) like '%$cari%' or upper(a.i_area) like '%$cari%' or upper(c.e_area_name) like '%$cari%')
							order by a.i_ap desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
