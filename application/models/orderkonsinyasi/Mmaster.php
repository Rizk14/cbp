<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iorderpb,$icustomer)
    {
		$this->db->select("a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_orderpb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_orderpb ='$iorderpb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($iorderpb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_orderpb_item a, tr_product_motif b
						 where a.i_orderpb = '$iorderpb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function insertheader($iorderpb, $dorderpb, $iarea, $ispg, $icustomer)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_orderpb'       => $iorderpb,
      'i_area'          => $iarea,
      'i_spg'           => $ispg,
      'i_customer'      => $icustomer,
      'd_orderpb'       => $dorderpb,
      'f_orderpb_cancel'=> 'f',
      'd_orderpb_entry' => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_orderpb');
    }
    function insertdetail($iorderpb,$iarea,$icustomer,$dorderpb,$iproduct,$iproductmotif,$iproductgrade,$nquantityorder,$nquantitystock,$i,$eproductname,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_orderpb'       => $iorderpb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_orderpb'       => $dorderpb,
          'i_product'       => $iproduct,
          'e_product_name'  => $eproductname,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity_order'=> $nquantityorder,
          'n_quantity_stock'=> $nquantitystock,
          'd_orderpb_entry' => $dentry,
          'e_remark'        => $eremark,
          'n_item_no'       => $i
    		)
    	);
    	
    	$this->db->insert('tm_orderpb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($iproduct, $iproductgrade, $iorderpb, $iarea, $icustomer, $iproductmotif,$i) 
    {
		  $this->db->query("DELETE FROM tm_orderpb_item WHERE i_orderpb='$iorderpb' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_customer='$icustomer' and i_area='$iarea' and n_item_no='$i'");
    }
	
    public function deleteheader($xiorderpb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_orderpb WHERE i_orderpb='$xiorderpb' and i_area='$iarea' and i_customer='$icustomer'");
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$cari)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select c.i_product as kode, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, c.e_product_name as nama
						                    from tr_product_motif a,tr_product c
						                    where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
							   or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);			
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
							   or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
							   or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);			
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
							   or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}

    }
    function bacastore($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													   c.i_area = '$area3' or c.i_area = '$area4' or
													   c.i_area = '$area5')", false)->limit($num,$offset);			
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(a.e_store_name) like '%$cari%'
                            or upper(b.i_store_location) like '%$cari%'
                            or upper(b.e_store_locationname) like '%$cari%')", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(a.e_store_name) like '%$cari%'
                            or upper(b.i_store_location) like '%$cari%'
                            or upper(b.e_store_locationname) like '%$cari%')
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													  c.i_area = '$area3' or c.i_area = '$area4' or
													  c.i_area = '$area5')", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumber($thbl){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='OPB'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noopb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$noopb
                            where i_modul='OPB'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($noopb,"string");
			  $a=strlen($noopb);
			  while($a<5){
			    $noopb="0".$noopb;
			    $a=strlen($noopb);
			  }
		  	$noopb  ="OPB-".$thbl."-".$noopb;
			  return $noopb;
		  }else{
			  $noopb  ="00001";
			  $noopb  ="OPB-".$thbl."-".$noopb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('OPB','00','$asal',1)");
			  return $noopb;
		  }
    }
}
?>
