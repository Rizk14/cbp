<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
    parent::__construct();
	  #$this->CI =& get_instance();
  }
    function bacaarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
 	function bacanoarea($area, $periode)
  {
    $this->db->select(" '04' as KODETRAN, 'CV BANGUN CITRA LESTARI (BCL)' as NAMADGU,
                      '31.659.080.1-421.000' as NPWPDGU, i_dtap as NODOK,d_dtap as TGLDOK, 
                      A.i_supplier as KODESUP, b.e_supplier_name as NAMASUP, i_pajak as NOSERI,
                      d_pajak as TGLPJK, (v_gross + v_ppn + v_discount) as GROSS, v_discount as POTONG, 
                      v_gross as DPP, v_ppn as PPN, 
                      v_netto as NET, '' as NAMAPKP, b.e_supplier_address as ALAMATPKP, 
                      b.e_supplier_npwp as NPWP

                      from tm_dtap a, tr_supplier b
                      where a.i_supplier = b.i_supplier and a.i_area='$area'
                      and to_char(d_dtap,'yyyymm')='$periode'
                      order by a.d_dtap",false);
    $tes=$this->db->get();
    return $tes;
  }
 	function bacano($periode)
  {
    $this->db->select(" '04' as KODETRAN, 'CV BANGUN CITRA LESTARI (BCL)' as NAMADGU,
                      '31.659.080.1-421.000' as NPWPDGU, i_dtap as NODOK,d_dtap as TGLDOK, 
                      A.i_supplier as KODESUP, b.e_supplier_name as NAMASUP, i_pajak as NOSERI,
                      d_pajak as TGLPJK, (v_gross + v_ppn + v_discount) as GROSS, v_discount as POTONG, 
                      v_gross as DPP, v_ppn as PPN, 
                      v_netto as NET, '' as NAMAPKP, b.e_supplier_address as ALAMATPKP, 
                      b.e_supplier_npwp as NPWP

                      from tm_dtap a, tr_supplier b
                      where a.i_supplier = b.i_supplier 
                      and to_char(d_dtap,'yyyymm')='$periode'
                      order by a.d_dtap",false);
    $tes=$this->db->get();
    return $tes;
  }
}
?>
