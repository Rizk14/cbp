<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icitystatus)
    {
		$this->db->select('i_city_status, e_city_statusname')->from('tr_city_status')->where('i_city_status', $icitystatus);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icitystatus, $ecitystatusname)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
    			'i_city_status' => $icitystatus,
    			'e_city_statusname' => $ecitystatusname,
			'd_city_statusentry' => $dentry
    		)
    	);
    	
    	$this->db->insert('tr_city_status');
		#redirect('citystatus/cform/');
    }
    function update($icitystatus, $ecitystatusname)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
    	$data = array(
               'i_city_status' => $icitystatus,
               'e_city_statusname' => $ecitystatusname,
	       'd_city_statusupdate' => $dupdate
            );
		$this->db->where('i_city_status', $icitystatus);
		$this->db->update('tr_city_status', $data); 
		#redirect('citystatus/cform/');
    }
	
    public function delete($icitystatus) 
    {
		$this->db->query('DELETE FROM tr_city_status WHERE i_city_status=\''.$icitystatus.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_city_status, e_city_statusname from tr_city_status order by i_city_status",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
