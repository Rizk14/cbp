<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tglfrom=$th.'-'.$bl.'-'.$dt;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tglto=$th.'-'.$bl.'-'.$dt;
			}

        $sql=" a.i_pelunasanap, a.i_supplier, c.e_supplier_name, a.i_area, to_char(a.d_bukti,'mm') as bln, 
               to_char(a.d_bukti,'yyyy') as thn, b.i_dtap, b.d_dtap, a.d_bukti, b.v_jumlah, b.v_sisa
               from tm_pelunasanap a, tm_pelunasanap_item b, tr_supplier c
               WHERE a.i_pelunasanap=b.i_pelunasanap AND a.d_bukti=b.d_bukti AND a.i_area=b.i_area and a.i_supplier = c.i_supplier
               AND a.d_bukti>='$tglfrom' AND a.d_bukti<='$tglto'
               order by a.d_bukti, a.i_supplier, a.i_pelunasanap, b.d_dtap              
             ";
		    $this->db->select($sql,false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
    }
}
?>
