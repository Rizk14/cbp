<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerspecialproduct)
    {
		$this->db->select('i_customer_specialproduct, e_customer_specialproductname')->from('tr_customer_specialproduct')->where('i_customer_specialproduct', $icustomerspecialproduct);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerspecialproduct, $ecustomerspecialproductname)
    {
    	$this->db->set(
    		array(
    			'i_customer_specialproduct' => $icustomerspecialproduct,
    			'e_customer_specialproductname' => $ecustomerspecialproductname
    		)
    	);
    	
    	$this->db->insert('tr_customer_specialproduct');
		#redirect('customerspecialproduct/cform/');
    }
    function update($icustomerspecialproduct, $ecustomerspecialproductname)
    {
    	$data = array(
               'i_customer_specialproduct' => $icustomerspecialproduct,
               'e_customer_specialproductname' => $ecustomerspecialproductname
            );
		$this->db->where('i_customer_specialproduct', $icustomerspecialproduct);
		$this->db->update('tr_customer_specialproduct', $data); 
		#redirect('customerspecialproduct/cform/');
    }
	
    public function delete($icustomerspecialproduct) 
    {
		$this->db->query('DELETE FROM tr_customer_specialproduct WHERE i_customer_specialproduct=\''.$icustomerspecialproduct.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_specialproduct, e_customer_specialproductname from tr_customer_specialproduct order by i_customer_specialproduct", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
