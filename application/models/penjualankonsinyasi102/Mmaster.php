<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($inotapb,$icustomer)
    {
		$this->db->select("a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_notapb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_notapb ='$inotapb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($inotapb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_notapb_item a, tr_product_motif b
						 where a.i_notapb = '$inotapb' and a.i_customer='$icustomer' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_notapb'    => $inotapb,
      'i_area'      => $iarea,
      'i_spg'       => $ispg,
      'i_customer'  => $icustomer,
      'd_notapb'    => $dnotapb,
      'n_notapb_discount' => $nnotapbdiscount,
      'v_notapb_discount' => $vnotapbdiscount,
      'v_notapb_gross'    => $vnotapbgross,
      'f_notapb_cancel'   => 'f',
      'd_notapb_entry'    => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_notapb');
    }
    function insertdetail($inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_notapb'        => $inotapb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_notapb'        => $dnotapb,
          'i_product'       => $iproduct,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity'      => $nquantity,
          'v_unit_price'    => $vunitprice,
          'd_notapb_entry'  => $dentry,
          'n_item_no'       => $i,
          'e_product_name'  => $eproductname,
          'i_price_groupco' => $ipricegroupco,
          'e_remark'        => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_notapb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($iproduct, $iproductgrade, $inotapb, $iarea, $icustomer, $iproductmotif) 
    {
		  $this->db->query("DELETE FROM tm_notapb_item WHERE i_notapb='$inotapb' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_customer='$icustomer'");
    }
	
    public function deleteheader($xinotapb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_notapb WHERE i_notapb='$xinotapb' and i_area='$iarea' and i_customer='$icustomer'");
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$cari,$cust)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, b.v_product_retail as harga,
						                    c.e_product_name as nama, e.n_quantity_stock as stock
						                    from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d, tm_ic_consigment e
						                    where a.i_product=c.i_product and a.i_product=b.i_product and c.i_product=e.i_product 
						                    and e.i_customer=d.i_customer
						                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
						                    and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
                                order by c.i_product, a.e_product_motifname, b.i_price_group
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function runningnumber($thbl){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPM'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospmb
                            where i_modul='SPM'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
		  	$nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPM','00','$asal',1)");
			  return $nospmb;
		  }
/*
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substring(i_spmb,11,6)) as max from tm_spmb 
				    			where substring(i_spmb,6,2)='$th' 
                  and substring(i_spmb,11,2)<>'ST' and substring(i_spmb,11,2)<>'KN'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }
*/
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
							   or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);			
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
							   or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
							   or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);			
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
							   or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}

    }
    function bacastore($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													   c.i_area = '$area3' or c.i_area = '$area4' or
													   c.i_area = '$area5')", false)->limit($num,$offset);			
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(a.e_store_name) like '%$cari%'
                            or upper(b.i_store_location) like '%$cari%'
                            or upper(b.e_store_locationname) like '%$cari%')", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(a.e_store_name) like '%$cari%'
                            or upper(b.i_store_location) like '%$cari%'
                            or upper(b.e_store_locationname) like '%$cari%')
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													  c.i_area = '$area3' or c.i_area = '$area4' or
													  c.i_area = '$area5')", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment
                                set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$emutasiperiode,$q_aw,$q_ak)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi_consigment
                                (
                                  i_product,i_product_motif,i_product_grade,i_customer,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,n_mutasi_kepusat,
                                  n_saldo_akhir,n_saldo_stockopname,f_mutasi_close, n_mutasi_git)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$icustomer','$emutasiperiode',$q_aw,0,0,$qsj,0,$q_ak-$qsj,0,'f',$qsj)
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic_consigment set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic_consigment
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$icustomer', '$eproductname', 0-$qsj, 't'
                                )
                              ",false);
    }
}
?>
