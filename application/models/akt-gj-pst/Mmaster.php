<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_general_jurnal a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_jurnal) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='f'
							order by a.i_jurnal desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" sa.*, b.e_area_name from tm_general_jurnal a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_jurnal) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='f'
							order by a.i_jurnal desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ijurnal)
    {
		$this->db->select(" * from tm_general_jurnal 
				   inner join tr_area on (tm_general_jurnal.i_area=tr_area.i_area)
				   where tm_general_jurnal.i_jurnal='$ijurnal'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ijurnal)
    {
		$this->db->select("	* from tm_general_jurnalitem 
							inner join tr_coa on(tm_general_jurnalitem.i_coa=tr_coa.i_coa)				
						   	where tm_general_jurnalitem.i_jurnal='$ijurnal'
						   	order by tm_general_jurnalitem.f_debet desc", false);//and i_supplier='$isupplier' 
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ijurnal,$iarea,$edescription,$fclose,$djurnal )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ijurnal','$iarea','$dentry','$edescription','$fclose','$djurnal','$djurnal')");
	}
	function inserttransitemdebet($acc,$ijurnal,$accname,$fdebet,$fposting,$iarea,$edescription,$vdebet,$djurnal)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acc','$ijurnal','$accname','$fdebet','$fposting','$vdebet','$djurnal','$djurnal','$dentry')");
	}
	function inserttransitemkredit($acc,$ijurnal,$accname,$fdebet,$fposting,$iarea,$edescription,$vkredit,$djurnal)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acc','$ijurnal','$accname','$fdebet','$fposting','$vkredit','$djurnal','$djurnal','$dentry')");
	}
	function insertgldebet($acc,$ijurnal,$accnamae,$fdebet,$iarea,$vdebet,$djurnal,$edescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ijurnal','$acc','$djurnal','$accnamae','$fdebet',$vdebet,'$iarea','$djurnal','$edescription','$dentry')");
	}
	function insertglkredit($acc,$ijurnal,$accname,$fdebet,$iarea,$vkredit,$djurnal,$edescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ijurnal','$acc','$djurnal','$accname','$fdebet','$vkredit','$iarea','$djurnal','$edescription','$dentry')");
	}
	function updatejurnal($ijurnal,$iarea)
    {
		$this->db->query("update tm_general_jurnal set f_posting='t' where i_jurnal='$ijurnal' and i_area='$iarea'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
