<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
		    $this->db->select("	a.*, b.e_area_name from tm_target a
                            inner join tr_area b on(a.i_area=b.i_area)
                            where a.i_periode = '$iperiode'  
                            and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
                            order by a.i_area",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, b.e_area_name from tm_target a
                            inner join tr_area b on(a.i_area=b.i_area)
                            where a.i_periode = '$iperiode'  
                            and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            order by a.i_area",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaspb($iperiode)
    {
		  $this->db->select("	sum(a.v_spb) as v_spb_gross from tm_spb a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_spb,'yyyymm') = '$iperiode' and a.f_spb_cancel='f'
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasj($iperiode)
    {
		  $this->db->select("	sum(v_nota_gross) as v_sj_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f'
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacanota($iperiode)
    {
		  $this->db->select("	sum(v_nota_gross) as v_nota_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f' and not a.i_nota isnull
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatarget($iperiode)
    {
		  $this->db->select("	a.v_target from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($iperiode)
    {
		  $this->db->select("	b.i_area from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetailnota($iarea,$period)
      {
       $this->db->select("  a.i_nota, a.d_nota, a.v_nota_gross, a.i_customer, b.e_customer_name,
                            b.e_customer_address, c.e_city_name, a.i_area, a.i_salesman, d.e_salesman_name
                            from tm_nota a, tr_customer b, tr_city c, tr_salesman d
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and b.i_city=c.i_city
                            and a.i_salesman=d.i_salesman
                            and b.i_area=c.i_area and a.i_area='$iarea' and a.i_nota like 'FP-$period-%'
                            order by a.i_salesman, a.i_nota", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
      }
    function bacadetailkn($iarea,$periode)
      {
       $this->db->select("  a.i_kn, a.d_kn, a.v_netto, a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_area
                            from tm_kn a, tr_customer b, tr_city c
                            where a.i_customer=b.i_customer and b.i_city=c.i_city and b.i_area=c.i_area
                            and a.i_area='$iarea' and to_char(a.d_kn,'yyyymm')='$periode'
                            order by a.i_kn", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
      }
}
?>
