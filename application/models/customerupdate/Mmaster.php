<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($ispb, $iarea)
	{
		$this->db->select(" a.i_spb, a.i_customer, a.i_area, a.i_salesman, a.i_price_group, a.i_customer_class,
							a.i_customer_plugroup,
							a.i_customer_group, a.i_customer_status, a.i_customer_producttype,
							a.i_customer_specialproduct,
							a.i_customer_grade, a.i_customer_service, a.i_customer_salestype, a.i_city, a.i_shop_status,
							a.i_marriage,
							a.i_jeniskelamin, a.i_religion, a.i_traversed, a.i_paymentmethod, a.i_call,
							a.e_salesman_name, a.d_survey,
							a.n_visit_period, a.f_customer_new, a.e_customer_name, a.e_customer_address,
							a.e_customer_sign,
							a.e_customer_phone, a.e_rt1, a.e_rw1, a.e_postal1, a.e_customer_kelurahan1,
							a.e_customer_kecamatan1,
							a.e_customer_kota1, a.e_customer_provinsi1, a.e_fax1, a.e_customer_month, a.e_customer_year,
							a.e_customer_age,
							a.n_shop_broad, a.e_customer_owner, a.e_customer_ownerttl, a.e_customer_owneraddress,
							a.e_customer_ownerphone,
							a.e_customer_ownerhp, a.e_customer_ownerfax, a.e_customer_ownerpartner,
							a.e_customer_ownerpartnerttl,
							a.e_customer_ownerpartnerage, a.e_rt2, a.e_rw2, a.e_postal2, a.e_customer_kelurahan2,
							a.e_customer_kecamatan2,
							a.e_customer_kota2, a.e_customer_provinsi2, a.e_customer_sendaddress, a.e_customer_sendphone,
							a.f_parkir,
							a.f_kuli, a.e_ekspedisi1, a.e_ekspedisi2, a.e_rt3, a.e_rw3, a.e_postal3, a.e_customer_kota3,
							a.e_customer_provinsi3,	a.e_customer_pkpnpwp, a.f_spb_pkp, a.e_customer_npwpname,
							a.e_customer_npwpaddress,
							a.e_customer_bank1,	a.e_customer_bankaccount1, a.e_customer_bankname1, a.e_customer_bank2, 
							a.e_customer_bankaccount2, a.e_customer_bankname2, a.e_kompetitor1,	a.e_kompetitor2,
							a.e_kompetitor3,
							a.n_spb_toplength, a.n_customer_discount,	a.f_kontrabon, a.e_kontrabon_hari,
							a.e_kontrabon_jam1,
							a.e_kontrabon_jam2, a.e_tagih_hari, a.e_tagih_jam1, a.e_tagih_jam2, a.d_customer_entry,
							a.e_customer_ownerage, a.e_customer_contact, a.e_customer_contactgrade, a.e_customer_mail,
							a.e_customer_refference,
							a.f_approve, a.i_approve, a.e_approve, a.d_approve, b.e_city_name, c.e_customer_groupname,
							d.e_area_name,
							e.e_customer_statusname, f.e_customer_producttypename, g.e_customer_specialproductname,
							h.e_customer_gradename,
							i.e_customer_servicename, j.e_customer_salestypename, k.e_customer_classname,
							l.e_shop_status, m.e_marriage,
							n.e_jeniskelamin, o.e_religion, p.e_traversed, q.e_paymentmethod, r.e_call,
							s.e_customer_plugroupname,
							t.e_price_groupname
							from tr_customer_tmp a
							LEFT JOIN tr_city b
							ON (a.i_city = b.i_city and a.i_area = b.i_area)
							LEFT JOIN tr_customer_group c
							ON (a.i_customer_group = c.i_customer_group)
							LEFT JOIN tr_area d
							ON (a.i_area = d.i_area)
							LEFT JOIN tr_customer_status e 
							ON (a.i_customer_status = e.i_customer_status)
							LEFT JOIN tr_customer_producttype f
							ON (a.i_customer_producttype = f.i_customer_producttype)
							LEFT JOIN tr_customer_specialproduct g
							ON (a.i_customer_specialproduct = g.i_customer_specialproduct)
							LEFT JOIN tr_customer_grade h
							ON (a.i_customer_grade = h.i_customer_grade)
							LEFT JOIN tr_customer_service i
							ON (a.i_customer_service = i.i_customer_service)
							LEFT JOIN tr_customer_salestype j
							ON (a.i_customer_salestype = j.i_customer_salestype)
							LEFT JOIN tr_customer_class k
							ON (a.i_customer_class=k.i_customer_class)
							LEFT JOIN tr_shop_status l 
							ON (a.i_shop_status=l.i_shop_status)
							LEFT JOIN tr_marriage m 
							ON (a.i_marriage=m.i_marriage)
							LEFT JOIN tr_jeniskelamin n
							ON (a.i_jeniskelamin=n.i_jeniskelamin)
							LEFT JOIN tr_religion o
							ON (a.i_religion=o.i_religion)
							LEFT JOIN tr_traversed p
							ON (a.i_traversed=p.i_traversed)
							LEFT JOIN tr_paymentmethod q
							ON (a.i_paymentmethod=q.i_paymentmethod)
							LEFT JOIN tr_call r
							ON (a.i_call=r.i_call)
							LEFT JOIN tr_customer_plugroup s
							ON (a.i_customer_plugroup=s.i_customer_plugroup)
							LEFT JOIN tr_price_group t
							ON (a.i_price_group=t.i_price_group)
							where a.i_spb = '$ispb' and a.i_area='$iarea'
				", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function insert($icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup, $iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, $icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, $ecustomername, $ecustomeraddress, $ecustomercity, $ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, $ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, $ecustomerpayment, $ecustomerpriority, $ecustomercontact, $ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, $fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp, $fcustomeraktif, $fcustomertax, $ecustomerretensi, $ncustomertoplength)
	{
		if ($fcustomerplusppn == 'on')
			$fcustomerplusppn = 'FALSE';
		else
			$fcustomerplusppn = 'FALSE';
		if ($fcustomerplusdiscount == 'on')
			$fcustomerplusdiscount = 'TRUE';
		else
			$fcustomerplusdiscount = 'FALSE';
		if ($fcustomerpkp == 'on')
			$fcustomerpkp = 'TRUE';
		else
			$fcustomerpkp = 'FALSE';
		if ($fcustomeraktif == 'on')
			$fcustomeraktif = 'TRUE';
		else
			$fcustomeraktif = 'FALSE';
		if ($fcustomertax == 'on')
			$fcustomertax = 'TRUE';
		else
			$fcustomertax = 'FALSE';
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry = $row->c;
		$this->db->set(
			array(
				'i_customer' 			=> $icustomer,
				'i_customer_plugroup' 		=> $icustomerplugroup,
				'i_city' 			=> $icity,
				'i_customer_group' 		=> $icustomergroup,
				'i_price_group' 		=> $ipricegroup,
				'i_area' 			=> $iarea,
				'i_customer_status' 		=> $icustomerstatus,
				'i_customer_producttype'	=> $icustomerproducttype,
				'i_customer_specialproduct' 	=> $icustomerspecialproduct,
				'i_customer_grade' 		=> $icustomergrade,
				'i_customer_service' 		=> $icustomerservice,
				'i_customer_salestype' 		=> $icustomersalestype,
				'i_customer_class' 		=> $icustomerclass,
				'i_customer_status' 		=> $icustomerstatus,
				'e_customer_name' 		=> $ecustomername,
				'e_customer_address' 		=> $ecustomeraddress,
				'e_customer_city' 		=> $ecustomercity,
				'e_customer_postal' 		=> $ecustomerpostal,
				'e_customer_phone' 		=> $ecustomerphone,
				'e_customer_fax' 		=> $ecustomerfax,
				'e_customer_mail' 		=> $ecustomermail,
				'e_customer_sendaddress' 	=> $ecustomersendaddress,
				'e_customer_receiptaddress' 	=> $ecustomerreceiptaddress,
				'e_customer_remark' 		=> $ecustomerremark,
				'e_customer_payment' 		=> $ecustomerpayment,
				'e_customer_priority' 		=> $ecustomerpriority,
				'e_customer_contact' 		=> $ecustomercontact,
				'e_customer_contactgrade' 	=> $ecustomercontactgrade,
				'e_customer_refference' 	=> $ecustomerrefference,
				'e_customer_othersupplier' 	=> $ecustomerothersupplier,
				'f_customer_plusppn' 		=> $fcustomerplusppn,
				'f_customer_plusdiscount' 	=> $fcustomerplusdiscount,
				'f_customer_pkp' 		=> $fcustomerpkp,
				'f_customer_aktif' 		=> $fcustomeraktif,
				'f_customer_tax' 		=> $fcustomertax,
				'e_customer_retensi' 		=> $ecustomerretensi,
				'n_customer_toplength' 		=> $ncustomertoplength,
				'd_customer_entry'		=> $dentry
			)
		);

		$this->db->insert('tr_customer');
	}

	function update($ispb, $icustomer, $iarea, $isalesman, $esalesmanname, $dsurvey, $nvisitperiod, $fcustomernew, $ecustomername, $ecustomeraddress, $ecustomersign, $ecustomerphone, $ert1, $erw1, $epostal1, $ecustomerkelurahan1, $ecustomerkecamatan1, $ecustomerkota1, $ecustomerprovinsi1, $efax1, $ecustomermonth, $ecustomeryear, $ecustomerage, $eshopstatus, $ishopstatus, $nshopbroad, $ecustomerowner, $ecustomerownerttl, $emarriage, $imarriage, $ejeniskelamin, $ijeniskelamin, $ereligion, $ireligion, $ecustomerowneraddress, $ecustomerownerphone, $ecustomerownerhp, $ecustomerownerfax, $ecustomerownerpartner, $ecustomerownerpartnerttl, $ecustomerownerpartnerage, $ert2, $erw2, $epostal2, $ecustomerkelurahan2, $ecustomerkecamatan2, $ecustomerkota2, $ecustomerprovinsi2, $ecustomersendaddress, $ecustomersendphone, $etraversed, $itraversed, $fparkir, $fkuli, $eekspedisi1, $eekspedisi2, $ert3, $erw3, $epostal3, $ecustomerkota3, $ecustomerprovinsi3, $ecustomerpkpnpwp, $fspbpkp, $ecustomernpwpname, $ecustomernpwpaddress, $ecustomerclassname, $icustomerclass, $epaymentmethod, $ipaymentmethod, $ecustomerbank1, $ecustomerbankaccount1, $ecustomerbankname1, $ecustomerbank2, $ecustomerbankaccount2, $ecustomerbankname2, $ekompetitor1, $ekompetitor2, $ekompetitor3, $nspbtoplength, $ncustomerdiscount, $epricegroupname, $ipricegroup, $nline, $fkontrabon, $ecall, $icall, $ekontrabonhari, $ekontrabonjam1, $ekontrabonjam2, $etagihhari, $etagihjam1, $etagihjam2, $icustomergroup, $icustomerplugroup, $icustomerproducttype, $icustomerspecialproduct, $icustomerstatus, $icustomergrade, $icustomerservice, $icustomersalestype, $ecustomerownerage, $ecustomerphone2, $ecustomercontact, $ecustomercontactgrade, $ecustomermail, $ecustomerrefference, $fspbplusppn, $fspbplusdiscount, $irefcode)
	{
		/* UPDATE tr_customer_tmp */
		$data = array(
			'i_customer' 	             => $icustomer,
			'i_salesman'                 => $isalesman,
			'i_price_group'              => $ipricegroup,
			'i_customer_class'           => $icustomerclass,
			'i_customer_plugroup'        => $icustomerplugroup,
			'i_customer_group'           => $icustomergroup,
			'i_customer_status'          => $icustomerstatus,
			'i_customer_producttype'     => $icustomerproducttype,
			'i_customer_specialproduct'  => $icustomerspecialproduct,
			'i_customer_grade'           => $icustomergrade,
			'i_customer_service'         => $icustomerservice,
			'i_customer_salestype'       => $icustomersalestype,
			#'i_city'                     => $icity,
			'i_shop_status'              => $ishopstatus,
			'i_marriage'                 => $imarriage,
			'i_jeniskelamin'             => $ijeniskelamin,
			'i_religion'                 => $ireligion,
			'i_traversed'                => $itraversed,
			'i_paymentmethod'            => $ipaymentmethod,
			'i_call'                     => $icall,
			'e_salesman_name'            => $esalesmanname,
			'd_survey'                   => $dsurvey,
			'n_visit_period'             => $nvisitperiod,
			'f_customer_new'             => $fcustomernew,
			'e_customer_name'            => $ecustomername,
			'e_customer_address'         => $ecustomeraddress,
			'e_customer_sign'            => $ecustomersign,
			'e_customer_phone'           => $ecustomerphone,
			'e_rt1'                      => $ert1,
			'e_rw1'                      => $erw1,
			'e_postal1'                  => $epostal1,
			'e_customer_kelurahan1'      => $ecustomerkelurahan1,
			'e_customer_kecamatan1'      => $ecustomerkecamatan1,
			'e_customer_kota1'           => $ecustomerkota1,
			'e_customer_provinsi1'       => $ecustomerprovinsi1,
			'e_fax1'                     => $efax1,
			'e_customer_month'           => $ecustomermonth,
			'e_customer_year'            => $ecustomeryear,
			'e_customer_age'             => $ecustomerage,
			'n_shop_broad'               => $nshopbroad,
			'e_customer_owner'           => $ecustomerowner,
			'e_customer_ownerttl'        => $ecustomerownerttl,
			'e_customer_owneraddress'    => $ecustomerowneraddress,
			'e_customer_ownerphone'      => $ecustomerownerphone,
			'e_customer_ownerhp'         => $ecustomerownerhp,
			'e_customer_ownerfax'        => $ecustomerownerfax,
			'e_customer_ownerpartner'    => $ecustomerownerpartner,
			'e_customer_ownerpartnerttl' => $ecustomerownerpartnerttl,
			'e_customer_ownerpartnerage' => $ecustomerownerpartnerage,
			'e_rt2'                      => $ert2,
			'e_rw2'                      => $erw2,
			'e_postal2'                  => $epostal2,
			'e_customer_kelurahan2'      => $ecustomerkelurahan2,
			'e_customer_kecamatan2'      => $ecustomerkecamatan2,
			'e_customer_kota2'           => $ecustomerkota2,
			'e_customer_provinsi2'       => $ecustomerprovinsi2,
			'e_customer_sendaddress'     => $ecustomersendaddress,
			'e_customer_sendphone'       => $ecustomersendphone,
			'f_parkir'                   => $fparkir,
			'f_kuli'                     => $fkuli,
			'e_ekspedisi1'               => $eekspedisi1,
			'e_ekspedisi2'               => $eekspedisi2,
			'e_rt3'                      => $ert3,
			'e_rw3'                      => $erw3,
			'e_postal3'                  => $epostal3,
			'e_customer_kota3'           => $ecustomerkota3,
			'e_customer_provinsi3'       => $ecustomerprovinsi3,
			'e_customer_pkpnpwp'         => $ecustomerpkpnpwp,
			'f_spb_pkp'                  => $fspbpkp,
			'e_customer_npwpname'        => $ecustomernpwpname,
			'e_customer_npwpaddress'     => $ecustomernpwpaddress,
			'e_customer_bank1'           => $ecustomerbank1,
			'e_customer_bankaccount1'    => $ecustomerbankaccount1,
			'e_customer_bankname1'       => $ecustomerbankname1,
			'e_customer_bank2'           => $ecustomerbank2,
			'e_customer_bankaccount2'    => $ecustomerbankaccount2,
			'e_customer_bankname2'       => $ecustomerbankname2,
			'e_kompetitor1'              => $ekompetitor1,
			'e_kompetitor2'              => $ekompetitor2,
			'e_kompetitor3'              => $ekompetitor3,
			'n_spb_toplength'            => $nspbtoplength,
			'n_customer_discount'        => $ncustomerdiscount,
			'f_kontrabon'                => $fkontrabon,
			'e_kontrabon_hari'           => $ekontrabonhari,
			'e_kontrabon_jam1'           => $ekontrabonjam1,
			'e_kontrabon_jam2'           => $ekontrabonjam2,
			'e_tagih_hari'               => $etagihhari,
			'e_tagih_jam1'               => $etagihjam1,
			'e_tagih_jam2'               => $etagihjam2,
			#'d_customer_entry'           => $dcustomerentry,
			'e_customer_ownerage'        => $ecustomerownerage,
			'e_customer_phone2'          => $ecustomerphone2,
			'e_customer_contact'         => $ecustomercontact,
			'e_customer_contactgrade'    => $ecustomercontactgrade,
			'e_customer_mail'            => $ecustomermail,
			'e_customer_refference'      => $ecustomerrefference
			#'f_customer_plusdiscount'    => $f_customer_plusdiscount,
			#'f_customer_plusppn'         => $f_customer_plusppn
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tr_customer_tmp', $data);

		/* UPDATE i_customer tm_spb */
		$data = array(
			'i_customer' 	=> $icustomer
		);
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);

		$query = $this->db->query("	SELECT a.*, b.e_paymentmethod, c.e_area_name from tr_customer_tmp a, tr_paymentmethod b, tr_area c
									where a.i_spb='$ispb' and a.i_area='$iarea' and a.i_paymentmethod=b.i_paymentmethod
									and a.i_area=c.i_area");

		if ($query->num_rows() > 0) {
			foreach ($query->result() as $x) {

				/* CEK i_customer di tr_customer */
				$this->db->select(" i_customer from tr_customer where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					/* $this->db->query(" 	INSERT INTO tr_customer (i_customer, i_customer_plugroup, i_customer_group, i_customer_status, i_customer_producttype, i_customer_specialproduct, 
										i_customer_grade, i_customer_service, i_customer_salestype, i_customer_class, i_city, i_price_group, i_area, i_approve1, e_customer_name, e_customer_address, e_customer_city, e_customer_postal,
										e_customer_phone, e_customer_fax, e_customer_sendaddress, e_customer_receiptaddress, e_customer_payment, e_approve1, f_customer_pkp, f_customer_aktif, f_customer_plusppn,
										f_customer_plusdiscount, n_customer_toplength, d_customer_entry, d_approve1, f_customer_first, f_approve, n_customer_toplength_print, d_signin, i_sps_code)
										VALUES ('$x->i_customer', '$x->i_customer_plugroup', '$x->i_customer_group', '$x->i_customer_status', '$x->i_customer_producttype', '$x->i_customer_specialproduct', '$x->i_customer_grade',
										'$x->i_customer_service', '$x->i_customer_salestype', '$x->i_customer_class', '$x->i_city', '$x->i_price_group', '$x->i_area', '$x->i_approve', '$x->e_customer_name', '$x->e_customer_address', 
										NULL, '$x->e_postal1', '$x->e_customer_phone', '$x->e_fax1', '$x->e_customer_sendaddress', '$x->e_customer_sendaddress', '$x->e_paymentmethod', '$x->e_approve', '$x->f_spb_pkp',
										't', '$fspbplusppn', '$fspbplusdiscount', '$x->n_spb_toplength', '$x->d_customer_entry', '$x->d_approve', 'f', 't', '$x->n_spb_toplength', '$x->d_survey', '$irefcode') ", FALSE); */

					$this->db->set(
						array(
							'i_customer'                    => $x->i_customer,
							'i_customer_plugroup'           => $x->i_customer_plugroup,
							'i_customer_group'              => $x->i_customer_group,
							'i_customer_status'             => $x->i_customer_status,
							'i_customer_producttype'        => $x->i_customer_producttype,
							'i_customer_specialproduct'     => $x->i_customer_specialproduct,
							'i_customer_grade'              => $x->i_customer_grade,
							'i_customer_service'            => $x->i_customer_service,
							'i_customer_salestype'          => $x->i_customer_salestype,
							'i_customer_class'              => $x->i_customer_class,
							'i_city'                        => $x->i_city,
							'i_price_group'                 => $x->i_price_group,
							'i_area'                        => $x->i_area,
							'i_approve1'                    => $x->i_approve,
							'e_customer_name'               => $x->e_customer_name,
							'e_customer_address'            => $x->e_customer_address,
							'e_customer_city'               => NULL,
							'e_customer_postal'             => $x->e_postal1,
							'e_customer_phone'              => $x->e_customer_phone,
							'e_customer_fax'                => $x->e_fax1,
							'e_customer_sendaddress'        => $x->e_customer_sendaddress,
							'e_customer_receiptaddress'     => $x->e_customer_sendaddress,
							'e_customer_payment'            => $x->e_paymentmethod,
							'e_approve1'                    => $x->e_approve,
							'f_customer_pkp'                => $x->f_spb_pkp,
							'f_customer_aktif'              => 't',
							'f_customer_plusppn'            => $fspbplusppn,
							'f_customer_plusdiscount'       => $fspbplusdiscount,
							'n_customer_toplength'          => $x->n_spb_toplength,
							'd_customer_entry'              => $x->d_customer_entry,
							'd_approve1'                    => $x->d_approve,
							'f_customer_first'              => 'f',
							'f_approve'                     => 't',
							'n_customer_toplength_print'    => $x->n_spb_toplength,
							'd_signin'                      => $x->d_survey,
							'i_sps_code'                    => $irefcode
						)
					);
					$this->db->insert('tr_customer');
				}

				/* INSERT tr_customer_area */
				$this->db->select(" i_customer from tr_customer_area where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_area (i_customer, i_area, e_area_name) VALUES ('$x->i_customer','$x->i_area','$x->e_area_name') ");
				}

				$dentry = $x->d_customer_entry;
				if ($dentry != '') {
					$tmp = explode("-", $dentry);
					$th = $tmp[0];
					$bl = $tmp[1];
					$hr = $tmp[0];
					$dentry = $th . "-" . $bl . "-" . $hr;
					$periode = $th . $bl;
				}

				/* INSERT CUSTOMER SALESMAN PER GROUP BARANG */
				$as = $this->db->query(" SELECT * FROM tr_product_group WHERE f_aktif = 't' ");
				foreach ($as->result() as $get) {
					$this->db->select(" i_customer from tr_customer_salesman where i_customer='$x->i_customer' and i_product_group='$get->i_product_group' and e_periode='$periode'");
					$que = $this->db->get();
					if ($que->num_rows() == 0) {
						$this->db->query(" 	INSERT INTO tr_customer_salesman (i_customer, i_salesman, i_area, e_salesman_name, i_product_group, e_periode)
											VALUES ('$x->i_customer','$x->i_salesman','$x->i_area','$x->e_salesman_name', '$get->i_product_group', '$periode') ");
					}
				}

				// $this->db->select(" i_customer from tr_customer_salesman where i_customer='$x->i_customer' and i_product_group='00' and e_periode='$periode'");
				// $que = $this->db->get();
				// if ($que->num_rows() == 0) {
				// 	$this->db->query(" 	INSERT INTO tr_customer_salesman (i_customer, i_salesman, i_area, e_salesman_name, i_product_group, e_periode)
				// 						VALUES ('$x->i_customer','$x->i_salesman','$x->i_area','$x->e_salesman_name', '00', '$periode') ");
				// }

				// $this->db->select(" i_customer from tr_customer_salesman where i_customer='$x->i_customer' and i_product_group='01' and e_periode='$periode'");
				// $que = $this->db->get();
				// if ($que->num_rows() == 0) {
				// 	$this->db->query(" 	INSERT INTO tr_customer_salesman (i_customer, i_salesman, i_area, e_salesman_name, i_product_group, e_periode)
				// 						VALUES ('$x->i_customer','$x->i_salesman','$x->i_area','$x->e_salesman_name', '01', '$periode') ");
				// }

				// $this->db->select(" i_customer from tr_customer_salesman where i_customer='$x->i_customer' and i_product_group='02' and e_periode='$periode'");
				// $que = $this->db->get();
				// if ($que->num_rows() == 0) {
				// 	$this->db->query(" 	INSERT INTO tr_customer_salesman (i_customer, i_salesman, i_area, e_salesman_name, i_product_group, e_periode)
				// 						VALUES ('$x->i_customer','$x->i_salesman','$x->i_area','$x->e_salesman_name', '02', '$periode') ");
				// }
				/* **************************************** */

				/* INSERT tr_customer_owner */
				$this->db->select(" i_customer from tr_customer_owner where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_owner (i_customer, e_customer_ownername, e_customer_owneraddress, e_customer_ownerphone)
										VALUES ('$x->i_customer','$x->e_customer_owner','$x->e_customer_owneraddress', '$x->e_customer_ownerphone') ");
				}

				/* INSERT tr_customer_discount */
				$this->db->select(" i_customer from tr_customer_discount where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_discount (i_customer, n_customer_discount1)
										VALUES ('$x->i_customer','$x->n_customer_discount') ");
				}

				/* INSERT tr_customer_groupar */
				$this->db->select(" i_customer from tr_customer_groupar where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_groupar (i_customer, i_customer_groupar)
										VALUES ('$x->i_customer','$x->i_customer') ");
				}

				/* INSERT tr_customer_groupbayar */
				$this->db->select(" i_customer from tr_customer_groupbayar where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_groupbayar (i_customer, i_customer_groupbayar)
										VALUES ('$x->i_customer','$x->i_customer') ");
				}

				/* INSERT tr_customer_pkp */
				$this->db->select(" i_customer from tr_customer_pkp where i_customer='$x->i_customer'");
				$que = $this->db->get();
				if ($que->num_rows() == 0) {
					$this->db->query(" 	INSERT INTO tr_customer_pkp (i_customer, e_customer_pkpname, e_customer_pkpaddress, e_customer_pkpnpwp)
										VALUES ('$x->i_customer','$x->e_customer_npwpname','$x->e_customer_npwpaddress', '$x->e_customer_pkpnpwp') ");
				}
			}
		}
	}

	public function delete($icustomer)
	{
		$this->db->query("DELETE FROM tr_customer WHERE i_customer='$icustomer'", false);
		return TRUE;
	}

	function bacasemua($cari, $num, $offset)
	{
		$this->db->select(" 	*
							FROM
								tr_customer_tmp a,
								tr_area c,
								tm_spb d
							WHERE
								a.i_area = c.i_area
								AND a.f_approve = 't'
								AND a.i_customer LIKE '%000'
								AND (a.i_spb LIKE '%$cari%' OR a.e_customer_name LIKE '%$cari%')
								AND a.i_spb = d.i_spb
								AND a.i_area = d.i_area
								AND a.i_customer = d.i_customer
								AND c.i_area = d.i_area
								AND d.f_spb_cancel = 'f'
							ORDER BY
								a.i_area,
								a.i_spb ", false)->limit($num, $offset);

		/* ORI COMMENT 20 APR 2023 (MUNCUL SEMUA TANPA ADA FILTER SPB PELANGGAN BARU YANG CANCEL) */
		/* $this->db->select(" * from tr_customer_tmp a, tr_area c
							where a.i_area=c.i_area and a.f_approve='t' and a.i_customer like '%000'
							and (a.i_spb like '%$cari%' or a.e_customer_name like '%$cari%')
							order by a.i_area, a.i_spb", false)->limit($num, $offset); */
		#and not a.i_approve_ar isnull and not a.i_approve isnull
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" * from tr_customer where upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%' order by i_customer", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaplugroup($num, $offset)
	{
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup order by i_customer_plugroup", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariplugroup($cari, $num, $offset)
	{
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup 
				   where upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plugroup", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacity($iarea, $num, $offset)
	{
		$this->db->select("i_city, e_city_name from tr_city where i_area='$iarea' order by i_city", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricity($cari, $num, $offset)
	{
		$this->db->select("i_city, e_city_name from tr_city 
				   where upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%' order by i_city", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacacustomergroup($num, $offset)
	{
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group order by i_customer_group", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomergroup($cari, $num, $offset)
	{
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group 
				   where upper(e_customer_groupname) like '%$cari%' or upper(i_customer_group) like '%$cari%' order by i_customergroup", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacapricegroup($num, $offset)
	{
		$this->db->select("i_price_group, e_price_groupname from tr_price_group order by i_price_group", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caripricegroup($cari, $num, $offset)
	{
		$this->db->select("i_price_group, e_price_groupname from tr_price_group 
				   where upper(e_price_groupname) like '%$cari%' or upper(i_price_group) like '%$cari%' order by i_price_group", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset)
	{
		$this->db->select("i_area, e_area_name from tr_area order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset)
	{
		$this->db->select("i_area, e_area_name from tr_area 
				   where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerstatus($num, $offset)
	{
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status order by i_customer_status", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerstatus($cari, $num, $offset)
	{
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status 
				   where upper(e_customer_statusname) like '%$cari%' or upper(i_customer_status) like '%$cari%' order by i_customer_status", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerproducttype($num, $offset)
	{
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype order by i_customer_producttype", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerproducttype($cari, $num, $offset)
	{
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype 
				   where upper(e_customer_producttypename) like '%$cari%' or upper(i_customer_producttype) like '%$cari%' order by i_customer_producttype", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerspecialproduct($icustomerproducttype, $num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query("select * from tr_customer_specialproduct 
					   where i_customer_producttype='$icustomerproducttype' 
					   order by i_customer_specialproduct 
					   limit $num offset $offset");
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerspecialproduct($cari, $num, $offset)
	{
		$this->db->select("i_customer_specialproduct, e_customer_specialproductname from tr_customer_specialproduct 
				   where upper(e_customer_specialproductname) like '%$cari%' or upper(i_customer_specialproduct) like '%$cari%' order by i_customer_specialproduct", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomergrade($num, $offset)
	{
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade order by i_customer_grade", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomergrade($cari, $num, $offset)
	{
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade 
				   where upper(e_customer_gradename) like '%$cari%' or upper(i_customer_grade) like '%$cari%' order by i_customer_grade", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerservice($num, $offset)
	{
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service order by i_customer_service", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerservice($cari, $num, $offset)
	{
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service 
				   where upper(e_customer_servicename) like '%$cari%' or upper(i_customer_service) like '%$cari%' order by i_customer_service", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomersalestype($num, $offset)
	{
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype order by i_customer_salestype", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomersalestype($cari, $num, $offset)
	{
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype 
				   where upper(e_customer_salestypename) like '%$cari%' or upper(i_customer_salestype) like '%$cari%' order by i_customer_salestype", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomerclass($num, $offset)
	{
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class order by i_customer_class", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomerclass($cari, $num, $offset)
	{
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class 
				   where upper(e_customer_classname) like '%$cari%' or upper(i_customer_class) like '%$cari%' order by i_customer_class", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaspb($ispb, $iarea)
	{
		$this->db->select(" tm_spb.e_remark1 AS emark1, * from tm_spb 
						 left join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						 left join tr_customer_tmp on (tm_spb.i_spb=tr_customer_tmp.i_spb and tm_spb.i_area=tr_customer_tmp.i_area)
						 inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
						 left join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
						 inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
						 where tm_spb.i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($ispb, $iarea, $ipricegroup)
	{
		$this->db->select("  a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, b.e_product_motifname, c.v_product_retail as hrgnew 
		                      from tm_spb_item a, tr_product_motif b, tr_product_price c
						              where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif 
		                      and a.i_product=c.i_product and c.i_price_group='$ipricegroup'
						              order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetailnilaiorderspb($ispb, $iarea, $ipricegroup)
	{
		return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
												        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
	}
}
