<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibbk) 
    {
  		$this->db->query("update tm_bbk set f_bbk_cancel='t' WHERE i_bbk='$ibbk' and i_bbk_type='03'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
					                from tr_customer b, tm_ttbretur c, tm_bbm a
					                left join tm_kn d on (a.i_bbm=d.i_refference) 
					                where 
					                c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
					                a,i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
					                or upper(a.i_bbm) like '%$cari%') 
					                order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
							          from tm_bbm a, tr_customer b, tm_ttbretur c
							          where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
							          and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							          order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name
							          from tm_bbk a, tr_customer b
					              where a.i_supplier=b.i_customer and a.i_bbk_type='03'
					              and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbk) like '%$cari%')
					              and a.d_bbk >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bbk <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
							          from tr_customer b, tm_ttbretur c, tm_bbm a
							          left join tm_kn d on (a.i_bbm=d.i_refference) 
							          where 
							          c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
							          a.i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
							          or upper(a.i_bbm) like '%$cari%') 
							          and a.i_area='$iarea' and
							          a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbm desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ibbk)
    {
		  $this->db->select(" a.*, b.e_customer_name from tm_bbk a, tr_customer b
					                where a.i_supplier=b.i_customer
					                and i_bbk ='$ibbk' and a.i_bbk_type='03'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ibbk)
    {
			$this->db->select("a.*, b.e_product_motifname from tm_bbk_item a, tr_product_motif b
						             where a.i_bbk = '$ibbk' and i_bbk_type='03' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						             order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
