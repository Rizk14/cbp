<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_pelunasanap a, tr_supplier b
							where a.f_posting='t' and a.i_supplier=b.i_supplier
							order by a.i_pelunasanap desc, a.i_area ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_pelunasanap a, tr_supplier b
							where a.f_posting='t' and a.i_supplier=b.i_supplier
							and (upper(a.i_pelunasanap) '%$cari%')
							order by a.i_pelunasanap desc, a.i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function sisa($ipl,$iarea,$dbukti){
		$this->db->select(" sum(v_sisa+v_jumlah)as sisa
							from tm_pelunasanap_item
							where i_pelunasanap='$ipl'
							and i_area='$iarea'
							and d_bukti='$dbukti'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->sisa;
		}	
	}
	function jumlahbayar($ipl,$iarea,$dbukti){
		$this->db->select(" sum(v_jumlah)as jumlah
							from tm_pelunasanap_item
							where i_pelunasanap='$ipl'
							and i_area='$iarea'
							and d_bukti='$dbukti'",FALSE);
		$query = $this->db->get();
		foreach($query->result() as $isi){			
			return $isi->jumlah;
		}	
	}
	function bacapl($ipl,$iarea,$dbukti){
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name, d.e_jenis_bayarname, c.e_supplier_address, c.e_supplier_city
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier 
							and a.i_jenis_bayar=d.i_jenis_bayar 
							and a.d_bukti='$dbukti' and upper(a.i_pelunasanap)='$ipl' and upper(a.i_area)='$iarea'",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadetailpl($ipl,$iarea,$dbukti){
		$this->db->select(" * from tm_pelunasanap_item
							where i_pelunasanap = '$ipl' 
							and i_area='$iarea'
							and d_bukti='$dbukti'
							order by i_pelunasanap,i_area ",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader( $ipelunasan,$iarea,$dbukti )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$ipelunasan' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function deletetransitemdebet($accdebet,$ipelunasan,$dbukti)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$accdebet' and i_refference='$ipelunasan' and d_refference='$dbukti'");
	}
	function deletetransitemkredit($acckredit,$ipelunasan,$dbukti)
    {
		$this->db->query("delete from tm_jurnal_transharianitem 
						  where i_coa='$acckredit' and i_refference='$ipelunasan' and d_refference='$dbukti'");
	}
	function deletegldebet($accdebet,$ipelunasan,$iarea,$dbukti)
    {
		$this->db->query("delete from tm_general_ledger 
						  where i_refference='$ipelunasan' and i_coa='$accdebet' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function deleteglkredit($acckredit,$ipelunasan,$iarea,$dbukti)
    {
		$this->db->query("delete from tm_general_ledger
						  where i_refference='$ipelunasan' and i_coa='$acckredit' and i_area='$iarea' and d_refference='$dbukti'");
	}
	function updatepelunasan($ipl,$iarea,$dbukti)
    {
		$this->db->query("update tm_pelunasanap set f_posting='f' where i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
