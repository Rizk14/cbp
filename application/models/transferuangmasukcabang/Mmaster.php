<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea, $ikum, $tahun)
    {
		$this->db->select("	* from tm_kum a
					left join tr_customer_salesman e on(a.i_customer=e.i_customer and a.i_area=e.i_area and a.i_salesman=e.i_salesman)
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					where a.i_kum='$ikum' and a.i_area='$iarea' and a.n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function update($ikum,$tahun,$icustomer,$icustomergroupar,$iareaasal)
    {
    	$this->db->set(
    		array(
				'i_customer'				=> $icustomer,
				'i_customer_groupar'=> $icustomergroupar
    		)
    	);
    	$this->db->where('i_kum',$ikum);
    	$this->db->where('i_area',$iareaasal);
    	$this->db->where('n_kum_year',$tahun);
    	$this->db->update('tm_kum');
    }
	function bacacustomer($iarea,$num,$offset){
		$this->db->select(" a.*, 
					b.i_customer_groupar, 
					c.e_salesman_name, 
					c.i_salesman,
					d.e_customer_setor 

				from tr_customer a 
				left join tr_customer_groupar b on(a.i_customer=b.i_customer) 
				left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area) 
				left join tr_customer_owner d on(a.i_customer=d.i_customer)
				
				where a.i_area='$iarea'
				order by a.i_customer ",FALSE)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function caricustomer($cari,$iarea,$num,$offset){
		$this->db->select(" a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
							left join tr_customer_groupar b on(a.i_customer=b.i_customer)
							left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
							left join tr_customer_owner d on(a.i_customer=d.i_customer)

						   	where a.i_area='$iarea' 
							and (upper(a.i_customer) like '%$cari%' or 
							upper(a.e_customer_name) like '%$cari%' or 
							upper(d.e_customer_setor) like '%$cari%' ) 
							order by a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
}
?>
