<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icitytype)
    {
		$this->db->select('i_city_type, e_city_typename')->from('tr_city_type')->where('i_city_type', $icitytype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icitytype, $ecitytypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
    			'i_city_type' 		=> $icitytype,
    			'e_city_typename' 	=> $ecitytypename,
			'd_city_typeentry' 	=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_city_type');
		#redirect('citytype/cform/');
    }
    function update($icitytype, $ecitytypename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
    	$data = array(
               'i_city_type' => $icitytype,
               'e_city_typename' => $ecitytypename,
	       'd_city_typeupdate' => $dupdate
            );
		$this->db->where('i_city_type', $icitytype);
		$this->db->update('tr_city_type', $data); 
		#redirect('citytype/cform/');
    }
	
    public function delete($icitytype) 
    {
		$this->db->query('DELETE FROM tr_city_type WHERE i_city_type=\''.$icitytype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_city_type, e_city_typename from tr_city_type order by i_city_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
