<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ipromotype)
    {
		$this->db->select('i_promo_type, e_promo_typename')->from('tr_promo_type')->where('i_promo_type', $ipromotype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($ipromotype, $epromotypename)
    {
    	$this->db->set(
    		array(
    			'i_promo_type' 		=> $ipromotype,
    			'e_promo_typename' 	=> $epromotypename
    		)
    	);
    	
    	$this->db->insert('tr_promo_type');
		redirect('promotype/cform/');
    }
    function update($ipromotype, $epromotypename)
    {
    	$data = array(
               'i_promo_type' => $ipromotype,
               'e_promo_typename' => $epromotypename
            );
		$this->db->where('i_promo_type', $ipromotype);
		$this->db->update('tr_promo_type', $data); 
		redirect('promotype/cform/');
    }
	
    public function delete($ipromotype) 
    {
		$this->db->query('DELETE FROM tr_promo_type WHERE i_promo_type=\''.$ipromotype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_promo_type, e_promo_typename from tr_promo_type order by i_promo_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
