<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerproducttype)
    {
		$this->db->select('i_customer_producttype, e_customer_producttypename')->from('tr_customer_producttype')->where('i_customer_producttype', $icustomerproducttype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerproducttype, $ecustomerproducttypename)
    {
    	$this->db->set(
    		array(
    			'i_customer_producttype' => $icustomerproducttype,
    			'e_customer_producttypename' => $ecustomerproducttypename
    		)
    	);
    	
    	$this->db->insert('tr_customer_producttype');
		#redirect('customerproducttype/cform/');
    }
    function update($icustomerproducttype, $ecustomerproducttypename)
    {
    	$data = array(
               'i_customer_producttype' => $icustomerproducttype,
               'e_customer_producttypename' => $ecustomerproducttypename
            );
		$this->db->where('i_customer_producttype', $icustomerproducttype);
		$this->db->update('tr_customer_producttype', $data); 
		#redirect('customerproducttype/cform/');
    }
	
    public function delete($icustomerproducttype) 
    {
		$this->db->query('DELETE FROM tr_customer_producttype WHERE i_customer_producttype=\''.$icustomerproducttype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype order by i_customer_producttype", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
