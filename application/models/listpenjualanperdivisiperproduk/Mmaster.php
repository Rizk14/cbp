<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($iperiode,$store)
  {
    $query = $this->db->query("select x.i_product, x.e_product_name, x.i_store, x.i_store_location, x.i_product_group, x.e_product_groupname, 
                        sum(x.jumlah) as jumlah, sum(x.sisasaldo) as sisasaldo, sum(x.git) as git 
                        from (
                        
                        select i.i_store, i.i_store_location, g.i_product, g.e_product_name, d.i_product_group, d.e_product_groupname,
                        sum(i.n_mutasi_penjualan) as jumlah, sum(i.n_saldo_akhir) as sisasaldo, sum((i.n_mutasi_git+n_git_penjualan)) as git
                        from tr_product_group d, tr_product g, tr_product_type h , tm_mutasi i
                        where i.e_mutasi_periode='$iperiode' and i.i_product=g.i_product and (i.i_store_location<>'PB' and i.i_store<>'PB')
                        and g.i_product_type = h.i_product_type and h.i_product_group = d.i_product_group and i.i_store = '$store'
                        group by i.i_store, i.i_store_location, d.i_product_group, d.e_product_groupname, g.i_product
                        
                        union all
                        
                        select i.i_store, i.i_store_location, g.i_product, g.e_product_name, 'PB' as i_product_group, 'Modern Outlet ' 
                        as e_product_groupname, sum(i.n_mutasi_penjualan) as jumlah, sum(i.n_saldo_akhir) as sisasaldo, 
                        sum((i.n_mutasi_git+n_git_penjualan)) as git
                        from tr_product_group d, tr_product g, tr_product_type h , tm_mutasi i
                        where i.e_mutasi_periode='$iperiode' and i.i_product=g.i_product and (i.i_store_location='PB' or i.i_store='PB')
                        and g.i_product_type = h.i_product_type and h.i_product_group = d.i_product_group and i.i_store = '$store'
                        group by i.i_store, i.i_store_location, d.i_product_group, d.e_product_groupname, g.i_product

                        ) x 
                        group by x.i_store, x.i_store_location, x.i_product_group, x.e_product_groupname, x.i_product, x.e_product_name
                        order by x.i_store, x.e_product_groupname, x.i_product 
                        ",false);

		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($iperiode)
  {
		$this->db->select(" distinct a.i_area, b.e_area_name, b.i_store
                        from vpenjualanperdivisi a, tr_area b
                        where a.i_area=b.i_area and to_char(a.d_doc, 'yyyymm')='$iperiode'
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($iperiode)
  {
		$this->db->select(" a.* from (
                        SELECT distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisi a 
                        inner join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where to_char(a.d_doc, 'yyyymm')='$iperiode'
                        union all
                        SELECT distinct a.i_product_group, 'Modern Outlet' as e_product_groupname
                        from vpenjualanperdivisi a 
                        where to_char(a.d_doc, 'yyyymm')='$iperiode'
                        and a.i_product_group not in (select i_product_group from tr_product_group)
                        ) as a
                        order by a.e_product_groupname",false);
/*
		$this->db->select(" distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisi a left join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        order by b.e_product_groupname",false);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
	  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	    $this->db->select(" distinct on (b.i_store) b.i_store, b.e_store_name, c.i_store_location, c.e_store_locationname, a.i_area
                          from tr_area a, tr_store b, tr_store_location c
                          where a.i_store=b.i_store and b.i_store=c.i_store and (a.i_area_type='00' or a.i_area_type='01' or 
                          a.i_area_type='02' or a.i_area_type='03')
                          order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	  }else{
	    $this->db->select(" distinct on (b.i_store) b.i_store, b.e_store_name, c.i_store_location, c.e_store_locationname, a.i_area
                          from tr_area a, tr_store b, tr_store_location c
                          where a.i_store=b.i_store and b.i_store=c.i_store and (a.i_area_type='00' or a.i_area_type='01' or 
                          a.i_area_type='02' or a.i_area_type='03')
                          and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                          or a.i_area = '$area4' or a.i_area = '$area5')
                          order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	  }
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    return $query->result();
	  }
    }
}
?>
