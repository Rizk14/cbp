<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bacaperiode($iperiode)
    {
      $thn = substr($iperiode, 0, 4);
      $bln = substr($iperiode, 4, 2);
      $periodesebelum = date('Ym', strtotime('-1 month', strtotime($thn . '-' . $bln . '-01')));
      $hpp=$this->db->query("SELECT i_product, i_product_motif, i_product_grade, e_product_name, harga, awal, beli, akhir 
                             from f_mutasi_hpp('$periodesebelum', '$iperiode') ");
                              //where i_product='SMB2575'");
                              // limit 150");
                              // where i_product='TPB1623'");
                              // where i_product='TPB3032'");
      if($hpp->num_rows()>0){
        $this->db->query("delete from tm_mutasi_hpp where e_periode='$iperiode'");
        $i=1;
//        $this->db->trans_begin();
        $kode='';
        $harga=0;
        $valid_baris = 0;
        $returmasuk=0;
        $returkeluar=0;
        $jual=0;
//        $lainkeluar=0;
        $a=0;
        foreach($hpp->result() as $row){
          $a++;
          if($row->awal==NULL)$row->awal=0;
          if($row->beli==NULL)$row->beli=0;
          if($row->akhir==NULL)$row->akhir=0;
          $awal=$row->awal;
          $akhir=$row->akhir;
          $beli=$row->beli; 
          if($kode!=$row->i_product) $valid_baris=0;
          $where = "";
          if ($valid_baris > 0) {
            $where = " Where baris >= '$valid_baris' "; 
          }
####### Transaksi
                                  $sql1="sSelect z.* from (";
                                  $sql2="Select z.* from (";
                                  $sql3="
                                  select x.*, ROW_NUMBER() OVER(order by i_product, i_product_motif, i_product_grade, e_product_name, tgl) as baris  from (
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, a.tgl, sum(a.returmasuk) as returmasuk, 
                                  sum(a.jual) as jual, sum(retursupp) as returkeluar,sum(lainkeluar) as lainkeluar 
                                  from (
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_nota as tgl, 0 as returmasuk, 
                                  sum(a.n_deliver) as jual, 0 as retursupp, 0 as lainkeluar
                                  from tm_nota_item a, tm_nota b where a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull 
                                  and to_char(b.d_nota,'yyyymm')='$iperiode' and b.f_nota_cancel='f' and a.i_product='$row->i_product' 
                                  and a.i_product_motif='$row->i_product_motif' and a.i_product_grade='$row->i_product_grade' 
                                  and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_nota
                                  union all
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbm as tgl, sum(a.n_quantity) as returmasuk, 
                                  0 as jual, 0 as retursupp, 0 as lainkeluar
                                  from tm_bbm_item a, tm_bbm b where a.i_bbm=b.i_bbm and to_char(b.d_bbm,'yyyymm')='$iperiode' and b.f_bbm_cancel='f' 
                                  and b.i_bbm_type='05' and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
                                  and a.i_product_grade='$row->i_product_grade' and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbm

                                  union all
	                                select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbkretur as tgl, 0 as returjual, 0 as jual, 
                                  sum(a.n_quantity) as retursupp, 0 as lainkeluar
	                                from tm_bbkretur_item a, tm_bbkretur b where a.i_bbkretur=b.i_bbkretur and to_char(b.d_bbkretur,'yyyymm')='$iperiode' and    
                                  b.f_bbkretur_cancel='f' and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
                                  and a.i_product_grade='$row->i_product_grade' and a.e_product_name='$row->e_product_name'
	                                group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbkretur      

                                  union all
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbk as tgl, 0 as returmasuk, 0 as jual, 
                                  0 as retursupp, sum(a.n_quantity) as lainkeluar
                                  from tm_bbk_item a, tm_bbk b where a.i_bbk=b.i_bbk and to_char(b.d_bbk,'yyyymm')='$iperiode' and b.f_bbk_cancel='f' 
                                  and b.i_bbk_type='03' and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
                                  and a.i_product_grade='$row->i_product_grade' and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbk
                                  ) as a
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, a.tgl
                                  order by a.i_product, a.tgl
                                  ) as x
                                  ) as z $where order by z.i_product, z.tgl
                                  ";
//          if($row->i_product=='TPB1623'){
//          if($valid_baris>0){
//            $sql=$sql1.$sql3;
//          }else{
            $sql=$sql2.$sql3;
//          }
          $nota=$this->db->query($sql);
          if($nota->num_rows()>0){
            foreach($nota->result() as $rownota){
              if($kode==''){
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $returkeluar=$returkeluar+$rownota->returkeluar;
                if((($awal+$beli+$returmasuk)-($jual))>=$akhir){
                  if((($awal+$beli+$returmasuk)-($jual+$returkeluar))>=$akhir){
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli,
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$jual,$returkeluar,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$jual, n_retur_keluar=$returkeluar, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=0;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                    $valid_baris=0;
                  }else{
                    $sisareturkeluar=$returkeluar;
                    do{
                      $sisareturkeluar--;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisareturkeluar))<$akhir);
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$jual,$sisareturkeluar,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$jual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=$returjual-$sisareturkeluar;
                    $valid_baris=$rownota->baris;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                    break;
                  }
                }else{
                  $sisajual=$jual;
                  do{
                    $sisajual--;
                  }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);

                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                  $sisajual=$jual-$sisajual;
                  $sisareturkeluar=$rownota->returkeluar;
                  $valid_baris=$rownota->baris;
                  $kode=$row->i_product;
                  $harga=$row->harga;
                  break;
                }
              }elseif($kode!=$row->i_product){
                $returmasuk=0;
                $jual=0;
                $returkeluar=0;
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $returkeluar=$returkeluar+$rownota->returkeluar;
                if(($awal+$beli+$returmasuk)-($jual)>=$akhir){
                  if(($awal+$beli+$returmasuk)-($jual+$returkeluar)>=$akhir){
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$jual,$returkeluar,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$jual, n_retur_keluar=$returkeluar, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=0;
                    $valid_baris=0;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                  }else{
                    $sisareturkeluar=$returkeluar;
                    do{
                      $sisareturkeluar--;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisareturkeluar))<$akhir);

                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$jual,0,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$jual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=$returkeluar-$sisareturkeluar;
                    $valid_baris=$rownota->baris;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                    break;
                  }
                }else{
                  $sisajual=$jual;
                  do{
                    $sisajual--;
                  }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);

                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                  $sisajual=$jual-$sisajual;
                  $sisareturkeluar=$rownota->returkeluar;
                  $valid_baris=$rownota->baris;
                  $kode=$row->i_product;
                  $harga=$row->harga;
                  break;
                }
              }elseif(($kode==$row->i_product) && ($harga==$row->harga)){
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $returkeluar=$returkeluar+$rownota->returkeluar;
                if(($awal+$beli+$returmasuk)-($sisajual)>=$akhir){
                  if(($awal+$beli+$returmasuk)-($sisajual+$sisareturkeluar)>=$akhir){
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,$sisareturkeluar,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, 
                                      n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=0;
                    $valid_baris=0;
                  }else{
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $valid_baris=0;
                  }
                  if(($awal+$beli+$returmasuk)-($jual)>=$akhir){
                    if(($awal+$beli+$returmasuk)-($jual+$returkeluar)>=$akhir){
                      $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                        n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                        values
                                        ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                        $returmasuk,0,$jual,$returkeluar,0,$row->akhir,$row->harga)
                                        ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                        Update set n_jual=$jual,n_retur_keluar=$returkeluar, n_retur_masuk=$returmasuk");
                      $sisajual=0;
                      $sisareturkeluar=0;
                      $valid_baris=0;
                      $kode=$row->i_product;
                      $harga=$row->harga;
                    }else{
                      $sisareturkeluar=$returkeluar;
                      do{
                        $sisareturkeluar--;
                      }while((($awal+$beli+$returmasuk)-($jual+$sisareturkeluar))<$akhir);

                      $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                        n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                        values
                                        ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                        $returmasuk,0,$jual,0,0,$row->akhir,$row->harga)
                                        ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                        Update set n_jual=$jual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, n_retur_masuk=$returmasuk");
                      $sisajual=0;
                      $sisareturkeluar=$returkeluar-$sisareturkeluar;
                      $valid_baris=$rownota->baris;
                      $kode=$row->i_product;
                      $harga=$row->harga;
                      break;
                    }
                  }else{
                    $sisajual=$jual;
                    do{
                      $sisajual--;
                    }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);

                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$sisajual, n_retur_masuk=$returmasuk");
                    $sisajual=$jual-$sisajual;
                    $valid_baris=$rownota->baris;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                    break;
                  }
                  $kode=$row->i_product;
                  $harga=$row->harga;
                }else{
                  $sesajual=$sisajual;
                  do{
                    $sisajual--;
                  }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                  $sisajual=$sesajual-$sisajual;
                  $valid_baris=$rownota->baris;
                  $kode=$row->i_product;
                  $harga=$row->harga;
                  break;
                }
              }elseif(($kode==$row->i_product) && ($harga!=$row->harga)){
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $returkeluar=$returkeluar+$rownota->returkeluar;
                if(($awal+$beli+$returmasuk)-($sisajual)>=$akhir){
                  if(($awal+$beli+$returmasuk)-($sisajual+$sisareturkeluar)>=$akhir){
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,$sisareturkeluar,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, 
                                      n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $sisareturkeluar=0;
                    $valid_baris=0;
                  }else{
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                    $sisajual=0;
                    $valid_baris=0;
                  }

                  if(($awal+$beli+$returmasuk)-($jual)>=$akhir){
                    if(($awal+$beli+$returmasuk)-($jual+$returkeluar)>=$akhir){
                      $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                        n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                        values
                                        ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                        $returmasuk,0,$rownota->jual,0,0,$row->akhir,$row->harga)
                                        ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                        Update set n_jual=tm_mutasi_hpp.n_jual+$rownota->jual, n_retur_masuk=$returmasuk");
                      $sisajual=0;
                      $sisareturkeluar=0;
                      $valid_baris=0;
                      $kode=$row->i_product;
                      $harga=$row->harga;
                    }else{
                      $sisareturkeluar=$returkeluar;
                      do{
                        $sisareturkeluar--;
                      }while((($awal+$beli+$returmasuk)-($jual+$sisareturkeluar))<$akhir);

                      $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                        n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                        values
                                        ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                        $returmasuk,0,$jual,$sisareturkeluar,0,$row->akhir,$row->harga)
                                        ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                        Update set n_jual=$jual, n_retur_keluar=tm_mutasi_hpp.n_retur_keluar+$sisareturkeluar, n_retur_masuk=$returmasuk");
                      $sisajual=0;
                      $sisareturkeluar=$returkeluar-$sisareturkeluar;
                      $valid_baris=$rownota->baris;
                      $kode=$row->i_product;
                      $harga=$row->harga;
                      break;
                    }
                  }else{
                    $sisajual=$jual;
                    do{
                      $sisajual--;
                    }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);
                    $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, 
                                      n_retur_masuk, n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                      values
                                      ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                      $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                      ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                      Update set n_jual=$sisajual, n_retur_masuk=$returmasuk");
                    $sisajual=$jual-$sisajual;
                    $valid_baris=$rownota->baris;
                    $kode=$row->i_product;
                    $harga=$row->harga;
                    break;
                  }
                  $kode=$row->i_product;
                  $harga=$row->harga;
                }else{
                  $sesajual=$sisajual;
                  do{
                    $sisajual--;
                  }while((($awal+$beli+$returmasuk)-($sisajual))<$akhir);
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$sisajual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_jual=tm_mutasi_hpp.n_jual+$sisajual, n_retur_masuk=$returmasuk");
                  $sisajual=$sesajual-$sisajual;
                  $valid_baris=$rownota->baris;
                  $kode=$row->i_product;
                  $harga=$row->harga;
                  break;
                }
              }
            }
          }else{
                $data = array(
                    'e_periode'=> $iperiode,
                    'i_product' => $row->i_product,
                    'i_product_motif' => $row->i_product_motif,
                    'i_product_grade' => $row->i_product_grade,
                    'e_product_name' => $row->e_product_name,
                    'v_harga' => $row->harga,
                    'n_awal' => $row->awal,
                    'n_beli' => $row->beli,
                    'n_retur_masuk' => 0,
                    'n_lain_masuk' => 0,
                    'n_jual' => 0,
                    'n_retur_keluar' => 0,
                    'n_lain_keluar' => 0,
                    'n_akhir' => $row->akhir,
                );
                $this->db->insert('tm_mutasi_hpp', $data);
          }
###### End Of Transaksi
          $kode=$row->i_product;
          $harga=$row->harga;
        }
//die;
        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
        }else{
//          $this->db->trans_rollback();
          $this->db->trans_commit();
//          echo "Data sudah berhasil di Simpan";
          $data=$this->db->query("select e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk, n_lain_masuk, 
                                  n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga
                                  from tm_mutasi_hpp where e_periode='$iperiode' ");//and (i_product='SBA1212' or i_product='TPT2871' or i_product='TPB1623')");
          if ($data->num_rows() > 0){     
            return $data->result();
          }
        }
      }

    }

    public function findWithAttr($array, $attr, $value)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr] === $value) {
                return $i;
            }
        }

        return -1;
    }

    public function findWithAttr2($array, $attr1, $attr2, $value1, $value2)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr1] === $value1 && $array[$i][$attr2] === $value2) {
                return $i;
            }
        }

        return -1;
    }
}
