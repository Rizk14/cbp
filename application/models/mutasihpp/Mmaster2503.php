<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bacaperiode($iperiode)
    {
      $thn = substr($iperiode, 0, 4);
      $bln = substr($iperiode, 4, 2);
      $periodesebelum = date('Ym', strtotime('-1 month', strtotime($thn . '-' . $bln . '-01')));
      $hpp=$this->db->query("SELECT i_product, i_product_motif, i_product_grade, e_product_name, harga, awal, beli, akhir 
                             from f_mutasi_hpp('$periodesebelum', '$iperiode') where i_product='TPT2871'");
      if($hpp->num_rows()>0){
        $this->db->query("delete from tm_mutasi_hpp where e_periode='$iperiode'");
        $i=1;
//        $this->db->trans_begin();
        $kode='';
        $harga=0;
        $rowmutasi=0;
        foreach($hpp->result() as $row){
          $rowmutasi++;
          if($row->awal==NULL)$row->awal=0;
          if($row->beli==NULL)$row->beli=0;
          if($row->akhir==NULL)$row->akhir=0;
          $awal=$row->awal;
          $akhir=$row->akhir;
          $beli=$row->beli;
####### Transaksi
          $nota=$this->db->query("select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, a.tgl, sum(a.returmasuk) as returmasuk, 
                                  sum(a.jual) as jual, sum(lainkeluar) as lainkeluar 
                                  from (
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_nota as tgl, 0 as returmasuk, 
                                  sum(a.n_deliver) as jual, 0 as lainkeluar
                                  from tm_nota_item a, tm_nota b where a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull 
                                  and to_char(b.d_nota,'yyyymm')='$iperiode' and b.f_nota_cancel='f' and a.i_product='$row->i_product' 
                                  and a.i_product_motif='$row->i_product_motif' and a.i_product_grade='$row->i_product_grade' 
                                  and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_nota
                                  union all
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbm as tgl, sum(a.n_quantity) as returmasuk, 
                                  0 as jual, 0 as lainkeluar
                                  from tm_bbm_item a, tm_bbm b where a.i_bbm=b.i_bbm and to_char(b.d_bbm,'yyyymm')='$iperiode' and b.f_bbm_cancel='f' 
                                  and b.i_bbm_type='05' and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
                                  and a.i_product_grade='$row->i_product_grade' and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbm
                                  union all
                                  select a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbk as tgl, 0 as returmasuk, 0 as jual, 
                                  sum(a.n_quantity) as lainkeluar
                                  from tm_bbk_item a, tm_bbk b where a.i_bbk=b.i_bbk and to_char(b.d_bbk,'yyyymm')='$iperiode' and b.f_bbk_cancel='f' 
                                  and b.i_bbk_type='03' and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
                                  and a.i_product_grade='$row->i_product_grade' and a.e_product_name='$row->e_product_name'
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, b.d_bbk
                                  ) as a
                                  group by a.i_product, a.i_product_motif, a.i_product_grade, a.e_product_name, a.tgl
                                  order by a.i_product, a.tgl");
          if($nota->num_rows()>0){
/*
e_periode='202101' and i_product='SMB2474'
*/
            $rowtrans=0;
            foreach($nota->result() as $rownota){
              $rowtrans++;
/* Awal */
              if($kode==''){
                $rowtranske=0;
                $sisajual=0;
                $sisaretur=0;
                $returmasuk=0;
                $jual=0;
                $lainkeluar=0;
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $lainkeluar=$lainkeluar+$rownota->lainkeluar;
                if((($awal+$beli+$returmasuk)-($jual+$lainkeluar))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$jual,0,$lainkeluar,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisajual=0;
                  $sisaretur=0;
                }elseif((($awal+$beli+$returmasuk)-($lainkeluar))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,0,0,$lainkeluar,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisajual=$jual;
                }elseif((($awal+$beli+$returmasuk)-($jual))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$jual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisaretur=$lainkeluar;
                }
                $rowtranske++;
                $kode=$row->i_product;
                $harga=$row->harga;
/* End Of Awal */
/* Kode Beda dengan kode sebelumnnya */
              }elseif(($kode!=$row->i_product)){
                $rowtranske=0;
                $sisajual=0;
                $sisaretur=0;
                $returmasuk=0;
                $jual=0;
                $lainkeluar=0;
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $lainkeluar=$lainkeluar+$rownota->lainkeluar;
                if((($awal+$beli+$returmasuk)-($jual+$lainkeluar))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$jual,0,$lainkeluar,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisajual=0;
                  $sisaretur=0;
                }elseif((($awal+$beli+$returmasuk)-($lainkeluar))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,0,0,$lainkeluar,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisajual=$jual;
                }elseif((($awal+$beli+$returmasuk)-($jual))>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$jual,0,0,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=tm_mutasi_hpp.n_jual+excluded.n_jual, 
                                    n_lain_keluar=tm_mutasi_hpp.n_lain_keluar+excluded.n_lain_keluar");
                  $sisaretur=$lainkeluar;
                }
                $rowtranske++;
                $kode=$row->i_product;
                $harga=$row->harga;
/* End Of Kode Beda dengan kode sebelumnnya */
/* Kode Sama dengan kode sebelumnnya dan Harga sama dengan harga sebelumnya */
              }elseif(($kode==$row->i_product) && ($harga==$row->harga)){
                $returmasuk=$returmasuk+$rownota->returmasuk;
                $jual=$jual+$rownota->jual;
                $lainkeluar=$lainkeluar+$rownota->lainkeluar;
                if(($awal+$beli+$returmasuk)-($jual+$lainkeluar)>=$akhir){
//var_dump($jual);
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$jual,0,$lainkeluar,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=$returmasuk, n_jual=$jual, 
                                    n_lain_keluar=$lainkeluar");
                  $sisajual=0;
                  $sisaretur=0;
                }else{
                  if(($awal+$beli+$returmasuk)-($jual+($lainkeluar-$rownota->lainkeluar))>=$akhir){
                    $sisajual=0;
                    $sisaretur=-1;
                    do{
                      $sisaretur++;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisaretur))>$akhir);
//var_dump($sisaretur);
                  }elseif(($awal+$beli+$returmasuk)-($lainkeluar+($jual-$rownota->jual))>=$akhir){
                    $sisajual=-1;
                    $sisaretur=0;
                    do{
                      $sisajual++;
                    }while((($awal+$beli+$returmasuk)-($lainkeluar+$sisajual))>$akhir);
//var_dump($sisajual);
                  }else{
                    $sisaretur=-1;
                    do{
                      $sisaretur++;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisaretur))>$akhir);
//var_dump($sisaretur);
                    $sisajual=-1;
                    do{
                      $sisajual++;
                    }while((($awal+$beli+$returmasuk)-($lainkeluar+$sisajual))>$akhir);
//var_dump($sisajual);
                  }
                $rowtranske++;                  
                }
//var_dump($rowtranske);
                $kode=$row->i_product;
                $harga=$row->harga;
/* End Of Kode Sama dengan kode sebelumnnya dan Harga sama dengan harga sebelumnya */
/* Kode Sama dengan kode sebelumnnya dan Harga beda dengan harga sebelumnya */
              }elseif(($kode==$row->i_product) && ($harga!=$row->harga)){
                if(($awal+$beli+$returmasuk)-($sisajual+$sisaretur)>=$akhir){
                  $this->db->query("insert into tm_mutasi_hpp (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk,
                                    n_lain_masuk, n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga) 
                                    values
                                    ('$iperiode','$row->i_product','$row->i_product_motif','$row->i_product_grade','$row->e_product_name',$awal,$beli,
                                    $returmasuk,0,$sisajual,0,$sisaretur,$row->akhir,$row->harga)
                                    ON CONFLICT (e_periode, i_product, i_product_motif, i_product_grade, e_product_name, v_harga) DO 
                                    Update set n_retur_masuk=tm_mutasi_hpp.n_retur_masuk+excluded.n_retur_masuk, n_jual=$sisajual, 
                                    n_lain_keluar=$sisaretur");
                  $sisajual=0;
                  $sisaretur=0;
                }else{
                  if(($awal+$beli+$returmasuk)-($jual+($lainkeluar-$rownota->lainkeluar))>=$akhir){
                    $sisajual=0;
                    $sisaretur=0;
                    do{
                      $sisaretur++;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisaretur))>$akhir);

                  }elseif(($awal+$beli+$returmasuk)-($lainkeluar+($jual-$rownota->jual))>=$akhir){
                    $sisajual=0;
                    $sisaretur=0;
                    do{
                      $sisajual++;
                    }while((($awal+$beli+$returmasuk)-($lainkeluar+$sisajual))>$akhir);
                  }else{
                    $sisajual=0;
                    $sisaretur=0;
                    do{
                      $sisaretur++;
                    }while((($awal+$beli+$returmasuk)-($jual+$sisaretur))>$akhir);
                    $sisajual=0;
                    $sisaretur=0;
                    do{
                      $sisajual++;
                    }while((($awal+$beli+$returmasuk)-($lainkeluar+$sisajual))>$akhir);
                  }
                }
                $rowtranske++;
                $kode=$row->i_product;
                $harga=$row->harga;
              }
/* End Of Kode Sama dengan kode sebelumnnya dan Harga beda dengan harga sebelumnya */
            }
          }else{
                $data = array(
                    'e_periode'=> $iperiode,
                    'i_product' => $row->i_product,
                    'i_product_motif' => $row->i_product_motif,
                    'i_product_grade' => $row->i_product_grade,
                    'e_product_name' => $row->e_product_name,
                    'v_harga' => $row->harga,
                    'n_awal' => $row->awal,
                    'n_beli' => $row->beli,
                    'n_retur_masuk' => 0,
                    'n_lain_masuk' => 0,
                    'n_jual' => 0,
                    'n_retur_keluar' => 0,
                    'n_lain_keluar' => 0,
                    'n_akhir' => $row->akhir,
                );
                $this->db->insert('tm_mutasi_hpp', $data);
          }
###### End Of Transaksi
          $rowtranske++;
          $kode=$row->i_product;
          $harga=$row->harga;
        }
die;
        if ($this->db->trans_status() === FALSE){
          $this->db->trans_rollback();
        }else{
//          $this->db->trans_rollback();
          $this->db->trans_commit();
//          echo "Data sudah berhasil di Simpan";
          $data=$this->db->query("select e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_retur_masuk, n_lain_masuk, 
                                  n_jual, n_retur_keluar, n_lain_keluar, n_akhir, v_harga
                                  from tm_mutasi_hpp where e_periode='$iperiode' and i_product='TPT2871'");
          if ($data->num_rows() > 0){     
            return $data->result();
          }
        }
      }

    }

    public function findWithAttr($array, $attr, $value)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr] === $value) {
                return $i;
            }
        }

        return -1;
    }

    public function findWithAttr2($array, $attr1, $attr2, $value1, $value2)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr1] === $value1 && $array[$i][$attr2] === $value2) {
                return $i;
            }
        }

        return -1;
    }
}
