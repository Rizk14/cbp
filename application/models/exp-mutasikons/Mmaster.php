<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
	 function bacacustomer($num,$offset) {
      $area1	= $this->session->userdata('i_area');
		  if($area1=='PB' or $area1=='00'){
		    $this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                        where a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                        where a.i_customer=b.i_customer and a.i_area='$area1' order by a.i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function caricustomer($cari,$num,$offset){
		  $this->db->select(" a.*, b.e_customer_name from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                            or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						                order by a.i_customer ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
