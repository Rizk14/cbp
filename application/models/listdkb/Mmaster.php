<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    public function delete($idkb, $iarea)
    {
      $this->db->query("update tm_dkb set f_dkb_batal='t' WHERE i_dkb='$idkb' and i_area='$iarea'");
      $que=$this->db->query("select i_sj, i_area from tm_dkb_item WHERE i_dkb='$idkb' and i_area='$iarea'");
        if ($que->num_rows() > 0){
           foreach($que->result() as $row){
          $this->db->query("update tm_nota set i_dkb=null, d_dkb=null WHERE i_sj='$row->i_sj' and i_area='$iarea'");
        }
        }
    }
    function bacasemua($cari, $num,$offset)
    {
        $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
                       where a.i_area=b.i_area
                       and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                       or upper(a.i_dkb) like '%$cari%')
                       order by a.i_dkb desc",false)->limit($num,$offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
    }
    function cari($cari,$num,$offset)
    {
        $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
                                       where a.i_area=b.i_area
                                       and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                                       or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
                                       order by a.i_dkb desc",FALSE)->limit($num,$offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $this->db->select("  a.i_dkb, a.d_dkb, b.e_area_name, a.e_sopir_name, a.i_kendaraan, sum(v_jumlah) as jumlah, a.f_dkb_batal, a.i_area from tr_area b, tm_dkb a, tm_dkb_item c
      where
      a.i_area = b.i_area
      and a.i_dkb = c.i_dkb
      and a.i_area = c.i_area
      and b.i_area = c.i_area
      and a.i_area = '$iarea'
      and a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
      a.d_dkb <= to_date('$dto','dd-mm-yyyy')
      and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
      or upper(a.i_dkb) like '%$cari%')
      group by a.i_dkb, a.d_dkb, b.e_area_name, a.e_sopir_name, a.i_kendaraan, a.f_dkb_batal, a.i_area
      order by a.i_dkb desc ",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $this->db->select("  a.*, b.e_area_name from tm_dkb a, tr_area b
                     where a.i_area=b.i_area
                     and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                     or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
                     and a.i_area='$iarea' and
                     a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
                     a.d_dkb <= to_date('$dto','dd-mm-yyyy')
                     order by a.i_dkb desc ",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
