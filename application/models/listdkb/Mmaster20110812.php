<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idkb, $iarea) 
    {
  		$this->db->query("update tm_dkb set f_dkb_batal='t' WHERE i_dkb='$idkb' and i_area='$iarea'");
  		$que=$this->db->query("select i_sj, i_area from tm_dkb_item WHERE i_dkb='$idkb' and i_area='$iarea'");
		  if ($que->num_rows() > 0){
			  foreach($que->result() as $row){
          $this->db->query("update tm_nota set i_dkb=null, d_dkb=null WHERE i_sj='$row->i_sj' and i_area='$iarea'");
        }
		  }
    }
    function bacasemua($cari, $num,$offset)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
							  where a.i_area=b.i_area 
							  and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							  or upper(a.i_dkb) like '%$cari%')
							  order by a.i_dkb desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_dkb a, tr_area b
													where a.i_area=b.i_area 
													and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
													or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
													order by a.i_dkb desc",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							          and substr(a.i_dkb,10,2)='$iarea' and 
							          a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
