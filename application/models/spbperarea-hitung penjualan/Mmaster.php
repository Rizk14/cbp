<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
		  $this->db->select("	a.i_area, a.e_area_name, v_target, v_nota_grossinsentif, v_real_regularinsentif, v_real_babyinsentif,
                          v_retur_insentif, v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross, to_char(d_process,'dd-mm-yyyy hh:mi:ss')
                          as d_process
                          from tr_area a
                          left join tm_target b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.i_periode='$iperiode'
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
