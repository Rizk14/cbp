<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igroup)
    {
		$this->db->select('i_product_group, e_product_groupname')->from('tr_product_group')->where('i_product_group', $igroup);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($igroup, $egroupname)
    {
    	$this->db->set(
    		array(
    			'i_product_group' => $igroup,
    			'e_product_groupname' => $egroupname
    		)
    	);
    	
    	$this->db->insert('tr_product_group');
		redirect('group/cform/');
    }
    function update($igroup, $egroupname)
    {
    	$data = array(
               'i_product_group' => $igroup,
               'e_product_groupname' => $egroupname
            );
		$this->db->where('i_product_group', $igroup);
		$this->db->update('tr_product_group', $data); 
		redirect('group/cform/');
    }
	
    public function delete($igroup) 
    {
		$this->db->query('DELETE FROM tr_product_group WHERE i_product_group=\''.$igroup.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select('i_product_group, e_product_groupname from tr_product_group order by i_product_group', false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
