<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

function baca($iarea, $itunai)
    {
    $this->db->select(" a.d_tunai, a.i_tunai, d.e_area_name, a.v_jumlah, a.v_sisa, a.i_area, a.i_salesman, e.e_salesman_name,
                        a.i_customer, a.i_customer_groupar, c.e_customer_name, a.e_remark, a.i_rtunai
          from tm_tunai a
          left join tr_customer_salesman e on(a.i_customer=e.i_customer and a.i_area=e.i_area and a.i_salesman=e.i_salesman
          and e.e_periode=to_char(a.d_tunai,'yyyymm'))
          left join tr_customer c on(a.i_customer=c.i_customer)
          left join tr_area d on(a.i_area=d.i_area) 
          where a.i_tunai='$itunai' and a.i_area='$iarea'",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->row();
    }
    }
    /* function bacadetail($iarea, $itunai)
    {
		$this->db->select("	a.* , b.i_rtunai, b.i_area, b.i_customer, b.i_salesman,b.e_remark, c.e_area_name, d.d_nota
                      from tm_tunai b 
                      left join tm_tunai_item a using (i_tunai) 
                      left join tr_area c on b.i_area=c.i_area 
                      left join tm_nota d on a.i_nota=d.i_nota
                      where b.i_tunai='$itunai' 
                      and b.i_area='$iarea' and d.i_area='$iarea' order by a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
    function bacadetail($iarea, $itunai)
    {
		$this->db->select("	a.* , b.i_rtunai, b.i_area, b.i_customer, b.i_salesman,b.e_remark, c.e_area_name, d.d_nota
                      from tm_tunai b 
                      left join tm_tunai_item a using (i_tunai) 
                      left join tr_area c on b.i_area=c.i_area 
                      left join tm_nota d on a.i_nota=d.i_nota
                      where b.i_tunai='$itunai' 
                      and b.i_area='$iarea' and d.i_area='$iarea' order by a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
      $jml = 0;
      foreach ($query->result() as $row) {
        $jml = $jml + $row->v_jumlah;
      }
      $data = array(
        'v_jumlah' => $jml,
        'v_sisa' => $jml
      );
      
      $this->db->where('i_tunai', $itunai);
      $this->db->where('i_area', $iarea);
      
      $this->db->update('tm_tunai', $data);
    
      
			return $query->result();
		}
    }
    function bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
      $query = $this->db->select("distinct a.i_salesman, a.e_salesman_name from tr_customer_salesman a, tr_salesman b
                                    where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                    and a.i_area='$iarea' and a.i_salesman = b.i_salesman
                                    and b.f_salesman_aktif='true' and a.e_periode='$per'",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
function bacanota($dtunai,$cari,$area,$num,$offset,$areax1,$areax2,$areax3,$areax4,$areax5,$icustomer)
  {
    if($areax1=='00'){
      $this->db->select(" a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.e_remark
                from tm_nota a 
              left join tr_area b using(i_area) 
              left join tr_customer c using (i_customer) where a.d_nota<='$dtunai' and a.v_sisa is not null 
              and a.i_customer='$icustomer' and a.i_area='$area' and a.f_nota_cancel='f'", false)->limit($num,$offset);
    }else{
      $this->db->select(" a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.e_remark
                from tm_nota a 
              left join tr_area b using(i_area) 
              left join tr_customer c using (i_customer) where a.d_nota<='$dtunai' and a.v_sisa is not null 
              and a.i_customer='$icustomer' and a.i_area='$area' and a.f_nota_cancel='f'", false)->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    } 
  }
     function carinota($cari,$iarea,$dtunai,$icustomer,$num,$offset,$group){
      $this->db->select(" a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.e_remark
                         from tm_nota a 
                         left join tr_area b using(i_area) 
                         left join tr_customer c using (i_customer)
                         left join tr_customer_groupbayar d on a.i_customer=d.i_customer
                         where a.d_nota<='$dtunai' and a.v_sisa is not null 
                         and a.i_customer='$icustomer' and a.i_area='$iarea' and a.f_nota_cancel='f'
                         and (upper(a.i_nota) like '%$cari%' or upper(a.i_customer) like '%$cari%') ",FALSE)->limit($num,$offset);
      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
      function bacacustomer($iarea,$per,$cari,$num,$offset){
    $this->db->select(" distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, 
          b.i_customer_groupar, 
          c.e_salesman_name, 
          c.i_salesman,
          d.e_customer_setor 

        from tr_customer a 
        left join tr_customer_groupar b on(a.i_customer=b.i_customer) 
        left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per') 
        left join tr_customer_owner d on(a.i_customer=d.i_customer)
        
        where a.i_area='$iarea' and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')
        order by a.i_customer ",FALSE)->limit($num,$offset);

    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    } 
  }

    function cek($iarea,$ikum,$tahun)
    {
		$this->db->select(" i_kum from tm_kum where i_area='$iarea' and i_kum='$ikum' and n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$eremark,$vjumlah,$vsisa)
    {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
      $this->db->set(
        array(
        'i_area'            => $iarea,
        'i_tunai'           => $itunai,
        'i_customer'        => $icustomer,
        'i_customer_groupar'=> $icustomergroupar,
        'i_salesman'        => $isalesman,
        'd_tunai'           => $dtunai,
        'd_entry'           => $dentry,
        'e_remark'          => $eremark,
        'v_jumlah'          => $vjumlah,
        'v_sisa'            => $vsisa
        )
      );
      
      $this->db->insert('tm_tunai');
    }
    function insertdetail($itunai,$iarea,$inota,$vjumlah,$i)
    {
		 	$this->db->set(
    		array(
				'i_tunai'			      => $itunai,
				'i_area'				    => $iarea,
				'i_nota' 				    => $inota,
				'v_jumlah'			    => $vjumlah,
				'n_item_no'         => $i
    		)
    	);
    	$this->db->insert('tm_tunai_item');
    }

    function updatedetail($itunai,$iarea,$inota,$vjumlah,$i)
    {
      $this->db->select("	v_jumlah from tm_tunai_item where i_tunai='$itunai' and i_area='$iarea' and i_nota='$inota'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
		    $this->db->set(
      		array(
				'i_tunai'           => $itunai,
        'i_area'            => $iarea,
        'i_nota'            => $inota,
        'v_jumlah'          => $vjumlah,
        'n_item_no'         => $i
      		)
      	);
      	$this->db->where('i_tunai',$itunai);
      	$this->db->where('i_area',$iarea);
      	$this->db->where('i_nota',$inota);

      	$this->db->update('tm_tunai_item');
      }else{
        $this->db->set(
      		array(
				   'i_tunai'         => $itunai,
          'i_area'           => $iarea,
          'i_nota'           => $inota,
          'v_jumlah'         => $vjumlah,
          'n_item_no'        => $i
      		)
      	);
      	$this->db->insert('tm_tunai_item');
      }
    }
    function deletedetail($itunai,$iarea,$inota)
    {
      $nilai=0;
      $this->db->select("	v_jumlah from tm_tunai_item 
                					where i_tunai='$itunai' and i_area='$iarea' and i_nota='$inota'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $xx){
          $nilai=$xx->v_jumlah;
			  }
      }
    	$this->db->query("update tm_tunai set v_jumlah=v_jumlah-$nilai where i_tunai='$itunai' and i_area='$iarea'");

    	$this->db->query("delete from tm_tunai_item 
    	                  where i_tunai='$itunai' and i_area='$iarea' and i_nota='$inota'");
    }
    function updatetunai($itunai,$iarea,$inota)
    {
      $this->db->set(
    		array(
				'i_area' 		=> $iarea,
				'i_tunai'  			  => $irtunai
    		)
    	);
    	$this->db->where('i_tunai',$itunai);
    	$this->db->where('i_area',$iareatunai);
    	$this->db->update('tm_tunai');
    }
    function update($itunai,$dtunai,$iarea,$eremark,$vjumlah)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    $this->db->set(
    		array(
        'i_tunai'           => $itunai,
				'i_area'			      => $iarea,
				'd_tunai'				    => $dtunai,
				'd_update'		      => $dentry,
				'e_remark'			    => $eremark,
				'v_jumlah'			    => $vjumlah,
				'v_sisa'            => $vjumlah
    		)
    	);
    	$this->db->where('i_tunai',$itunai);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_tunai');
    }
	function bacaarea($num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
  		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
		  $this->db->select(" * from tr_area 
							  where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
                          and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}

	function bacabank($num,$offset){
 		$this->db->select(" * from tr_bank order by i_bank ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}

 function runningnumber($iarea,$thbl){
    $th   = substr($thbl,0,4);
    $asal=$thbl;
    $thbl=substr($thbl,2,2).substr($thbl,4,2);
    $this->db->select(" n_modul_no as max from tm_dgu_no
                      where i_modul='TN'
                      and substr(e_periode,1,4)='$th'
                      and i_area='$iarea' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
       foreach($query->result() as $row){
         $terakhir=$row->max;
       }
       $notn  =$terakhir+1;
    $this->db->query(" update tm_dgu_no
                        set n_modul_no=$notn
                        where i_modul='TN'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea'", false);
       settype($notn,"string");
       $a=strlen($notn);
       while($a<6){
         $notn="0".$notn;
         $a=strlen($notn);
       }
       $notn  ="TN-".$thbl."-".$notn;
       return $notn;
    }else{
       $notn  ="000001";
       $notn  ="TN-".$thbl."-".$notn;
       $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                          values ('TN','$iarea','$asal',1)");
       return $notn;
    }
  }
	function bacatunai($drtunaix,$cari,$iarea,$num,$offset,$areax1,$areax2,$areax3,$areax4,$areax5)
  {
    if($areax1=='00'){
      $this->db->select(" a.*, b.i_customer, b.e_customer_name, c.e_area_name from tm_tunai a, tr_customer b, tr_area c 
	                        where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                          and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                          or upper(a.i_customer) like '%$cari%')
                          order by a.i_tunai ", false)->limit($num,$offset);
    }else{
      $this->db->select(" a.*, b.i_customer, b.e_customer_name, c.e_area_name from tm_tunai a, tr_customer b, tr_area c
	                        where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                          and (a.i_area='$areax1' or a.i_area='$areax2' or a.i_area='$areax3' or a.i_area='$areax4' 
                          or a.i_area='$areax5')
                          and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                          or upper(a.i_customer) like '%$cari%')
                          order by a.i_tunai ", false)->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }	
  }
}
?>
