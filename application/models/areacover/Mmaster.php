<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea,$iareacover)
    {
		$this->db->select("* from tr_area_cover 
					where tr_area_cover.i_area = '$iarea' and
					  tr_area_cover.i_area_cover = '$iareacover' and
					  tr_area_cover.f_ac_cancel='f'
				   order by tr_area_cover.i_area, tr_area_cover.i_area_cover", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iarea,$iareacover,$eareacovername)
    {
    	$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dareacoverentry= $row->c;
    	$this->db->query("insert into tr_area_cover (i_area,i_area_cover,e_area_cover_name,d_area_cover_entry,f_ac_cancel) values ('$iarea','$iareacover','$eareacovername','$dareacoverentry','t')");
		  #redirect('areacover/cform/');
    }

    function cancel($iarea,$iareacover) 
    {
		$this->db->query("update tr_area_cover set f_ac_cancel='f' where i_area='$iarea' and i_area_cover='$iareacover' ");
		return TRUE;
    }
   
    function bacasemua($cari, $num,$offset)
    {
		    $this->db->select("* from tr_area_cover where i_area like '%$cari%' 
		    					 and f_ac_cancel='t'
		    					order by tr_area_cover.i_area, tr_area_cover.i_area_cover",false)->limit($num,$offset);
       
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

/*	function bacaareacover($iarea,$num,$offset)
    {
		$this->db->select("i_area_cover from tr_area_cover where upper(tr_area_cover.i_area) like '%$iarea%'",false)->limit($num,$offset);
		$query2 = $this->db->get();
		if ($query2->num_rows() > 0){
			return $query2->result();
		}
    }
*/
    function cari($cari,$num,$offset)
    {
      $this->db->select("* from tr_area_cover where upper(tr_area_cover.i_area) like '%$cari%'",false)->limit($num,$offset);
      
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where i_area like '%$cari%' or e_area_name like '%$cari%' order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
