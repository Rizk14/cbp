<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacanilai($dfrom,$dto,$icoa,$ireff,$dreff,$fdebet,$iarea, $icoabank)
	{
		$ireff=trim($ireff);
		
		if($icoabank!=null){
		  $this->db->select("	* from(
		          select * from(
						  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, i_coa, i_refference, e_description, f_debet, v_mutasi_debet, 
						  v_mutasi_kredit, i_general_ledger, i_area, d_refference, e_coa_name, i_coa_bank
						  from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa<>'$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) as a
						  where 
						  a.i_refference in (
						  SELECT i_refference from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) and
						  a.i_coa_bank in (
						  SELECT i_coa_bank from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  )
						  union all						  
						  select * from(
						  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, i_coa, i_refference, e_description, f_debet, v_mutasi_debet, 
						  v_mutasi_kredit, i_general_ledger, i_area, d_refference, e_coa_name, i_coa_bank
						  from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa<>'$icoa' and f_debet<>'$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) as b
						  where 
						  b.i_refference in (
						  SELECT i_refference from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) and
						  b.i_coa_bank in (
						  SELECT i_coa_bank from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  )
						  )as c
						  order by c.d_mutasi, c.i_refference ",false);
		}else{
/*
		  $this->db->select("	* from(
						  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, i_coa, i_refference, e_description, f_debet, v_mutasi_debet, 
						  v_mutasi_kredit, i_general_ledger, i_area, d_refference, e_coa_name, i_coa_bank
						  from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa<>'$icoa' and f_debet<>'$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) as a
						  where 
						  a.i_refference in (
						  SELECT i_refference from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) and
						  a.e_description in (
						  SELECT e_description from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi <= to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  )
						  order by a.d_mutasi, a.i_refference ",false);
*/
		  $this->db->select("	* from(
		          select * from(
						  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, i_coa, i_refference, e_description, f_debet, v_mutasi_debet, 
						  v_mutasi_kredit, i_general_ledger, i_area, d_refference, e_coa_name, i_coa_bank
						  from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa<>'$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) as a
						  where 
						  a.i_refference in (
						  SELECT i_refference from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) and
						  a.e_description in (
						  SELECT e_description from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  )
						  union all						  
						  select * from(
						  SELECT to_char(d_mutasi, 'dd-mm-yyyy') as d_mutasi, i_coa, i_refference, e_description, f_debet, v_mutasi_debet, 
						  v_mutasi_kredit, i_general_ledger, i_area, d_refference, e_coa_name, i_coa_bank
						  from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy')
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa<>'$icoa' and f_debet<>'$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) as b
						  where 
						  b.i_refference in (
						  SELECT i_refference from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  ) and
						  b.e_description in (
						  SELECT e_description from tm_general_ledger 
						  where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						  and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						  and i_coa='$icoa' and f_debet='$fdebet' and trim(i_refference)='$ireff' and d_refference='$dreff' and i_area='$iarea'
						  )
						  )as c
						  order by c.d_mutasi, c.i_refference ",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacaflag($dfrom,$dto,$icoa)
	{
		$this->db->select("	i_refference,d_refference,i_area,f_debet,v_mutasi_debet,v_mutasi_kredit, i_general_ledger, i_coa_bank
		                    from tm_general_ledger 
						            where d_mutasi >= to_date('$dfrom', 'dd-mm-yyyy') 
						            and d_mutasi < to_date('$dto', 'dd-mm-yyyy') 
						            and i_coa='$icoa'
						            order by d_mutasi, i_refference ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacaperiode($dfrom,$dto,$icoafrom,$icoato)
    {
		$query=$this->db->query("	select i_coa, e_coa_name from tr_coa where i_coa>='$icoafrom' and i_coa<='$icoato'
            									order by i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($dfrom,$icoa)
    {
		$iperiode=substr($dfrom,6,4).substr($dfrom,3,2);
		$this->db->select("	v_saldo_awal from tm_coa_saldo
							where i_periode = '$iperiode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom =$th."-".$bl."-".$hr;
				$dstart=$th."-".$bl."-01";
			}
		$this->db->select("	sum(v_mutasi_debet) as v_mutasi_debet, sum(v_mutasi_kredit) as v_mutasi_kredit from tm_general_ledger
          							where i_coa='$icoa' and d_mutasi>='$dstart' and d_mutasi<'$dfrom' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$vmutasidebet = $tmp->v_mutasi_debet;
			$vmutasikredit= $tmp->v_mutasi_kredit;
		}
		if(!isset($sawal))$sawal=0;
		$sawal=$sawal+$vmutasidebet-$vmutasikredit;
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
}
?>
