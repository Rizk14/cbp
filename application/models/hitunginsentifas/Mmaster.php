<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaomset($iperiode)
    {
######insentif atas target omset & reguler & retur
/*
      $this->db->select("	a.i_area, a.i_as, a.e_area_name, c.i_salesman, c.e_salesman_name, v_target, v_nota_grossinsentif,
                          v_real_regularinsentif, v_real_babyinsentif, v_retur_insentif, 
                          v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross
                          from tr_area a
                          inner join tm_target_itemsls b on(a.i_area=b.i_area and b.i_periode='$iperiode') 
                          inner join tr_salesman c on(b.i_salesman=c.i_salesman)
                          where a.f_area_real='t' 
                          order by c.i_salesman,a.i_area",false);
*/
      $this->db->select("	distinct(a.*), b.e_salesman_name
                          from(
                          SELECT a.i_as as i_as, a.f_java, sum(v_target) as v_target, 
                          sum(v_nota_grossinsentif) as v_nota_grossinsentif, sum(v_real_regularinsentif) as v_real_regularinsentif, 
                          sum(v_real_babyinsentif) as v_real_babyinsentif, sum(v_retur_insentif) as v_retur_insentif, 
                          sum(v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(v_retur_noninsentif) as v_retur_noninsentif, 
                          sum(v_spb_gross) as v_spb_gross
                          from tr_area a
                          inner join tm_target b on(a.i_area=b.i_area and b.i_periode='$iperiode') 
                          where a.f_area_real='t' and not a.i_kp isnull
                          group by a.i_as, a.f_java
                          )as a, tr_salesman b
                          where a.i_as=b.i_salesman and b.i_periode_insentif='$iperiode'
                          order by a.i_as",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }

#		  $query = $this->db->get();
#		  if ($query->num_rows() > 0){
#			  return $query->result();
#		  }
    }
# and b.i_salesman='C4'
######insentif atas effective call & total call
    function bacaeffectivecall($iperiode)
    {
      $query=$this->db->query("	select a.i_periode, a.i_area, a.i_salesman, a.n_jumlah_hari, a.n_jumlah_kunjungan as jml, 
                                a.n_jumlah_order as jmlreal, b.e_salesman_name, c.e_area_name
                                from tm_kunjungan a, tr_salesman b, tr_area c 
                                where a.i_periode='$iperiode' and a.i_area=c.i_area and a.i_salesman=b.i_salesman
                                order by a.i_area, a.i_salesman");
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
######insentif atas collection
    function bacacollectioncash($iperiode)
    {
		  $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, sum(b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, sum(b.bayar) as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name                          
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_cash b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name                          
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
      if($row->realisasi==null || $row->realisasi=='')$row->realisasi=0;
      if($row->total!=0){
        $persen=number_format(($row->realisasi/$row->total)*100,2);
      }else{
        $persen='0.00';
      }
    }
    function bacacollectioncredit($iperiode)
    {
		  $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, sum(b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, sum(b.bayar) as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name                          
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name                          
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
      if($row->realisasi==null || $row->realisasi=='')$row->realisasi=0;
      if($row->total!=0){
        $persen=number_format(($row->realisasi/$row->total)*100,2);
      }else{
        $persen='0.00';
      }
    }
    function simpan($iperiode)
    {
      $this->db->query("delete from tm_insentif where e_periode='$iperiode'");
      $this->db->select("	* from vcollectioncr",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $this->db->query("insert into tm_insentif values('$iperiode', '$row->i_nota', '$row->d_nota', '$row->d_jatuh_tempo', 
                            '$row->f_insentif', '$row->i_salesman', '$row->i_area', '$row->i_customer', $row->v_nota_netto, 
                            $row->bayar, $row->sisa)");
        }
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	i_area, e_area_name, i_customer, e_customer_name, i_nota, d_nota, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, sum(b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, sum(b.bayar) as total, 0 as realisasi, 
                          0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, 0 as total, 0 as realisasi, 
                          sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, 0 as total, 0 as realisasi, 
                          sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          ) as a 
                          group by i_area, e_area_name, i_customer, e_customer_name, i_nota, d_nota
                          order by a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
