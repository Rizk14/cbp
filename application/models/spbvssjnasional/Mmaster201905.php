<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($thnsebelumnya,$thn)
    {
    //  $prevthbl=$prevth.$bl;
      //$thbl=$th.$bl;
      //if($icust=='all') $icust='';
	    $this->db->select("a.i_spb, a.d_spb, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.v_nota_netto, 
                  (a.d_sj)-(a.d_spb) as selisihsj, (a.d_nota)-(a.d_sj) as selisihnota,
                  a.i_customer, b.e_customer_name 
                  FROM tm_nota a, tr_customer b 
                  where a.i_customer = b.i_customer AND (
                  (to_char(a.d_spb, 'yyyy')>= '$thn' 
                  AND to_char(a.d_spb, 'yyyy') <= '$thn') OR
                  (to_char(a.d_sj, 'yyyy') >= '$thn' 
                  AND to_char(a.d_sj, 'yyyy') <= '$thn') OR (to_char(a.d_nota, 'yyyy') >= '$thn'
                  AND to_char(a.d_nota, 'yyyy') <= '$thn'))
                  AND a.f_nota_cancel='f'
                  ORDER BY a.d_spb ASC, a.i_spb ASC ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacacustomer($num,$offset,$area,$cari)
    {
      $this->db->select(" a.*, b.e_area_name  
                          from tr_customer a, tr_area b 
                          where a.i_area='$area' and a.i_area=b.i_area and
                          (a.i_customer like '%$cari%' or a.e_customer_name like '%$cari%')
                          order by a.i_customer", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    
    function bacaarea($num,$offset,$iuser)
    {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    
    function bacatotal($todate,$prevdate,$th,$prevth,$bl,$icust)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	sum(custlm) as custlm, sum(custcm) as custcm, sum(custly) as custly, sum(custcy) as custcy from (
                          select count(a.i_customer) as custlm, 0 as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, count(a.i_customer) as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, 0 as custcm, sum(a.custly) as custly, 0 as custcy from
                          (
                          select count(a.i_customer) as custly from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          union all
                          select 0 as custlm, 0 as custcm, 0 as custly, sum(a.custcy) as custcy from (
                          select count(a.i_customer) as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          ) as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
