<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj, $iarea) 
    {
			$this->db->query("update tm_sjr set f_sjr_cancel='t' WHERE i_sjr='$isj' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_to=b.i_area and a.i_sj_type='01'
								and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
	/*
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') and (a.i_area_to='$area1' or a.i_area_to='$area2' or a.i_area_to='$area3' or a.i_area_to='$area4'
						or a.i_area_to='$area5')
						order by a.i_sj desc",false)->limit($num,$offset);
	*/
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_sjr a, tr_area b
													where a.i_area=b.i_area
													and (upper(a.i_sjr) like '%$cari%' or upper(a.i_sjr_old) like '%$cari%')
													and a.i_area='$iarea' and
													a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sjr <= to_date('$dto','dd-mm-yyyy') and not a.d_sjr_receive is null
													ORDER BY a.i_sjr desc",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isjr,$iarea)
    {
		$this->db->select(" distinct(c.i_store), a.*, b.e_area_name 
                        from tm_sjr a, tr_area b, tm_sjr_item c
						            where a.i_area=b.i_area and a.i_sjr=c.i_sjr 
                        and a.i_area=c.i_area
						            and a.i_sjr ='$isjr' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isjr, $iarea)
    {
		$this->db->select("a.i_sjr,a.d_sjr,a.i_area,a.i_product,a.i_product_grade,a.i_product_motif,
                       a.n_quantity_receive,a.n_quantity_retur,a.v_unit_price,a.e_product_name,
                       a.i_store,a.i_store_location,a.i_store_locationbin,a.e_remark,
                       b.e_product_motifname from tm_sjr_item a, tr_product_motif b
				               where a.i_sjr = '$isjr' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatesjheader($isj,$iarea,$isjold,$dsj,$vsjnetto,$vsjrec)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjupdate= $row->c;
    	$this->db->set(
    		array(
				'i_sjr_old'	    => $isjold,
				'v_sjr'	        => $vsjnetto,
				'v_sjr_receive' => $vsjrec,
				'd_sjr_receive' => $dsj,
        'd_sjr_update'  => $dsjupdate

    		)
    	);
    	$this->db->where('i_sjr',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_sjr');
    }
    public function deletesjdetail($isj, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
	    $this->db->query("DELETE FROM tm_sjr_item WHERE i_sjr='$isj'
                        and i_area='$iarea'
									      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									      and i_product_motif='$iproductmotif'");
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                DELETE FROM tm_ic_trans 
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation'
                                and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      return $row->i_trans;
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
			                      $vunitprice,$isj,$dsj,$iarea, $istore,$istorelocation,$istorelocationbin,$eremark,$i)
    {
      $th=substr($dsj,0,4);
      $bl=substr($dsj,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_sjr'			          => $isj,
				'd_sjr'			          => $dsj,
				'i_area'		          => $iarea,
				'i_product'       		=> $iproduct,
				'i_product_motif'   	=> $iproductmotif,
				'i_product_grade'   	=> $iproductgrade,
				'e_product_name'    	=> $eproductname,
				'n_quantity_retur'  	=> $nretur,
				'n_quantity_receive'	=> $nreceive,
				'v_unit_price'		    => $vunitprice,
				'i_store'         		=> $istore,
				'i_store_location'	  => $istorelocation,
				'i_store_locationbin'	=> $istorelocationbin, 
        'e_remark'            => $eremark,
        'e_mutasi_periode'    => $pr,
        'n_item_no'           => $i
    		)
    	);
    	
    	$this->db->insert('tm_sjr_item');
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$trans)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw, $trans
                                )
                              ",false);
    }
    function cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,$qsj,0,0,0,$qsj,0,'f')
                              ",false);
    }
function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qsj, 't'
                                )
                              ",false);
    }
}
?>
