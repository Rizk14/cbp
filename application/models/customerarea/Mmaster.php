<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea,$icustomer)
    {
		$this->db->select("* from tr_customer_area 
					inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
					inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
					where tr_customer_area.i_area = '$iarea'
					  and tr_customer_area.i_customer = '$icustomer'
				   order by tr_customer_area.i_area, tr_customer_area.i_customer", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iarea, $icustomer, $eareaname)
    {
    	$this->db->query("insert into tr_customer_area (i_customer, i_area, e_area_name) values ('$icustomer','$iarea', '$eareaname')");
		  $this->db->query("update tr_customer set i_area='$iarea' where i_customer='$icustomer'");
		  #redirect('customerarea/cform/');
    }

    function update($iarea, $icustomer, $eareaname)
    {
		$this->db->query("update tr_customer_area set i_area = '$iarea', e_area_name='$eareaname' where i_customer = '$icustomer' ");
		$this->db->query("update tr_customer set i_area='$iarea' where i_customer='$icustomer'");
		#redirect('customerarea/cform/');
    }
	
    public function delete($iarea,$icustomer) 
    {
		$this->db->query('DELETE FROM tr_customer_area WHERE i_area=\''.$iarea.'\' and i_customer=\''.$icustomer.'\' ');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select("* from tr_customer_area 
						                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
						                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
						                      where upper(tr_customer_area.i_customer) like '%$cari%' or
						                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											  upper(tr_customer.e_customer_name) ilike '%$cari%'",false)->limit($num,$offset);
      }else{
        $this->db->select("* from tr_customer_area 
						                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
						                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
						                      where upper(tr_customer_area.i_customer) like '%$cari%' or
						                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											  upper(tr_customer.e_customer_name) ilike '%$cari%'
			                                  and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
			                                  or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function bacacustomer($num,$offset)
    {
		$this->db->select("i_customer, e_customer_name from tr_customer order by i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select("* from tr_customer_area 
						                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
						                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
						                      where upper(tr_customer_area.i_customer) like '%$cari%' or
						                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											  upper(tr_customer.e_customer_name) ilike '%$cari%'",false)->limit($num,$offset);
      }else{
        $this->db->select("* from tr_customer_area 
						                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
						                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
						                      where upper(tr_customer_area.i_customer) like '%$cari%' or
						                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											  upper(tr_customer.e_customer_name) ilike '%$cari%'
			                                  and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
			                                  or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caricustomer($cari,$num,$offset)
    {
		$this->db->select("i_customer, e_customer_name from tr_customer where upper(e_customer_name) ilike '%$cari%' or upper(i_customer) ilike '%$cari%' order by i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%' order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
