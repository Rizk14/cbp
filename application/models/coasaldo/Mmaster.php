<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iperiode,$icoa)
    {
		$this->db->select(" * from tm_coa_saldo where i_periode='$iperiode' and i_coa='$icoa'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($iperiode,$icoa)
    {
		$this->db->select(" i_coa from tm_coa_saldo where i_periode='$iperiode' and i_coa='$icoa'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($iperiode,$icoa,$ecoaname,$vsaldoawal,$vmutasidebet,$vmutasikredit,$vsaldoakhir,$ientry)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_periode'			=> $iperiode,
				'i_coa'				=> $icoa,
				'e_coa_name' 		=> $ecoaname,
				'v_saldo_awal'	 	=> $vsaldoawal,
				'v_mutasi_debet'	=> $vmutasidebet,
				'v_mutasi_kredit'	=> $vmutasikredit,
				'v_saldo_akhir'		=> $vsaldoakhir,
				'd_entry'			=> $dentry,
				'i_entry'			=> $ientry
    		)
    	);
    	
    	$this->db->insert('tm_coa_saldo');
    }
    function update($iperiode,$icoa,$ecoaname,$vsaldoawal,$vmutasidebet,$vmutasikredit,$vsaldoakhir,$ientry)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
				'e_coa_name' 		=> $ecoaname,
				'v_saldo_awal'	 	=> $vsaldoawal,
				'v_mutasi_debet'	=> $vmutasidebet,
				'v_mutasi_kredit'	=> $vmutasikredit,
				'v_saldo_akhir'		=> $vsaldoakhir,
				'd_update'			=> $dupdate,
				'i_update'			=> $ientry
    		)
    	);
    	$this->db->where("i_periode",$iperiode);
    	$this->db->where("i_coa",$icoa);
    	$this->db->update('tm_coa_saldo');
    }
	function bacacoa($num,$offset)
    {
		$this->db->select("* from tr_coa where f_coa_status='t' order by i_coa", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricoa($cari,$num,$offset)
    {
		$this->db->select("i_coa, e_coa_name from tr_coa where f_coa_status='t' and (upper(e_coa_name) like '%$cari%' or upper(i_coa) like '%$cari%')
						   order by i_coa ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
