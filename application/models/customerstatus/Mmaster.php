<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerstatus)
    {
		$this->db->select("i_customer_status, e_customer_statusname, n_customer_statusdown, n_customer_statusup, n_customer_statusindex from tr_customer_status where i_customer_status = '$icustomerstatus' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerstatus, $ecustomerstatusname, $ncustomerstatusdown,$ncustomerstatusup,$ncustomerstatusindex)
    {
    	$this->db->set(
    		array(
    			'i_customer_status'  	=> $icustomerstatus,
    			'e_customer_statusname'	=> $ecustomerstatusname,
			'n_customer_statusdown'	=> $ncustomerstatusdown,
			'n_customer_statusup'	=> $ncustomerstatusup,
			'n_customer_statusindex'=> $ncustomerstatusindex
    		)
    	);
    	
    	$this->db->insert('tr_customer_status');
		#redirect('customerstatus/cform/');
    }
    function update($icustomerstatus, $ecustomerstatusname, $ncustomerstatusdown, $ncustomerstatusup, $ncustomerstatusindex)
    {
    	$data = array(
               'i_customer_status' 	=> $icustomerstatus,
               'e_customer_statusname' 	=> $ecustomerstatusname,
	       'n_customer_statusdown' 	=> $ncustomerstatusdown,
	       'n_customer_statusup' 	=> $ncustomerstatusup,
	       'n_customer_statusindex'	=> $ncustomerstatusindex
            );
		$this->db->where('i_customer_status =', $icustomerstatus);
		$this->db->update('tr_customer_status', $data); 
		#redirect('customerstatus/cform/');
    }
	
    public function delete($icustomerstatus) 
    {
		$this->db->query("DELETE FROM tr_customer_status WHERE i_customer_status='$icustomerstatus'", false);
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_status, e_customer_statusname, n_customer_statusdown, n_customer_statusup, n_customer_statusindex from tr_customer_status 
				   order by i_customer_status", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
