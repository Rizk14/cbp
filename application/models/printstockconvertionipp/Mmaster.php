<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function bacasemua($cari,$num,$offset,$dfrom,$dto)
    {
      $this->db->select("  * from tm_ic_convertion
                        where d_ic_convertion >= to_date('$dfrom','dd-mm-yyyy')
                        and d_ic_convertion <= to_date('$dto','dd-mm-yyyy')
                        and (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
                        or upper(i_ic_convertion) like '%$cari%'
                        or upper(i_refference) like '%$cari%')
                        order by d_ic_convertion asc",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function baca($iicconvertion)
    {
      $this->db->select(" * from tm_ic_convertion
                        where i_ic_convertion='$iicconvertion'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacadetail($iicconvertion)
    {
      $this->db->select(" * from tm_ic_convertionitem
                         where i_ic_convertion='$iicconvertion'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
