<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
      parent::__construct();
  }
  public function delete($ispb,$iarea) 
  {
    return TRUE;
  }
  function bacasemua($cari, $num,$offset,$iuser)
  {
    $this->db->select(" * from v_list_sj_promo where i_area in ( select i_area from tm_user_area where i_user = '$iuser' ) ",false)->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }
  function cari($cari,$num,$offset,$iuser)
  {
    $this->db->select(" * from v_list_sj_promo
                      where i_area in ( select i_area from tm_user_area where i_user = '$iuser' )
                        and (upper(i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or 
                          upper(i_customer) like '%$cari%' or upper(i_sj) like '%$cari%' ) ",false)->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
  }

	function baca($ispb,$iarea)
    {
		$this->db->select(" * from tm_spb 
				   left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
           left join tm_nota nota on (nota.i_area=tm_spb.i_area and nota.i_spb = tm_spb.i_spb)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where tm_spb.i_spb ='$ispb' and tm_spb.i_area='$iarea' order by tm_spb.i_spb desc ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	function bacadetail($ispb,$iarea)
    {
		$this->db->select("	a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   			where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product 
							and a.i_product_motif=b.i_product_motif
				   			order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function simpan($ispb,$iarea,$user,$vsjgross, $vspbgross, $vsjdiscount, $vspbdiscount)
    {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    $vspbbersih = $vspbgross - $vspbdiscount;
    $nspbdiscount = ($vspbdiscount/$vspbgross)*100;
    $vsjbersih = $vsjgross - $vsjdiscount;
    $nsjdiscount = ($vsjdiscount/$vsjgross)*100;
    $data = array(
        'n_nota_discount1' => $nsjdiscount,
        'n_nota_discount2' => 0,
        'n_nota_discount3' => 0,
        'n_nota_discount4' => 0,
        'v_nota_discount1' => $vsjdiscount,
        'v_nota_discount2' => 0,
        'v_nota_discount3' => 0,
        'v_nota_discount4' => 0,
        'v_nota_discounttotal'		=> $vsjdiscount,
        'v_nota_gross'		=> $vsjgross,
        'v_nota_discount'		=> $vsjdiscount,
        'v_nota_netto'		=> $vsjbersih
           );
    $this->db->where('i_spb', $ispb);
    $this->db->where('i_area', $iarea);
    $this->db->update('tm_nota', $data); 
    $data = array(
        'n_spb_discount1' => $nspbdiscount,
        'n_spb_discount2' => 0,
        'n_spb_discount3' => 0,
        'n_spb_discount4' => 0,
        'v_spb_discount1' => $vspbdiscount,
        'v_spb_discount2' => 0,
        'v_spb_discount3' => 0,
        'v_spb_discount4' => 0,
        'v_spb_discounttotal' => $vspbdiscount,
        'v_spb_discounttotalafter' => $vsjdiscount,
        'v_spb_after'		=> $vsjbersih
           );
    $this->db->where('i_spb', $ispb);
    $this->db->where('i_area', $iarea);
    $this->db->update('tm_spb', $data); 
    }
	function sjapprove($ispb,$iarea,$eapprove,$user)
    {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'f_approve_cetak_sj'		=> '1'
    				 );
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		$this->db->update('tm_nota', $data); 
    }
}
?>
