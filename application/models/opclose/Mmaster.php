<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
    	$this->db->select("	distinct(z.i_op), z.d_op, z.i_area, z.i_reff, z.d_reff, z.i_do, z.d_do, z.e_supplier_name, z.e_area_name from (
                          select distinct(a.i_op), a.d_op, a.i_area, a.i_reff, a.d_reff, o.i_do, o.d_do, b.e_supplier_name, c.e_area_name 
                          from tm_op a left join tm_do o on (a.i_op=o.i_op)
                          ,tr_supplier b, tr_area c
					                where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
					                and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
					                or upper(a.i_op) like '%$cari%' or upper(a.i_reff) like '%$cari%')
                          union all
                          select distinct(a.i_op), a.d_op, a.i_area, a.i_reff, a.d_reff, o.i_do, o.d_do, b.e_supplier_name, c.e_area_name 
                          from tm_op a left join tm_do o on (a.i_op=o.i_op)
                          ,tr_supplier b, tr_area c
					                where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
					                and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
					                or upper(a.i_op) like '%$cari%' or upper(a.i_reff) like '%$cari%')
                          ) as z order by z.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, o.i_do, o.d_do, b.e_supplier_name, c.e_area_name 
							from tm_op a left join tm_do o on (a.i_op=o.i_op), tr_supplier b, tr_area c
							where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
							and (upper(c.e_area_name) ilike '%$cari%' or upper(b.e_supplier_name) ilike '%$cari%'
							or upper(a.i_op) ilike '%$cari%' or upper(a.i_reff) ilike '%$cari%' or upper(o.i_do) ilike '%$cari%')",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updateop($iop)
    {
		  $data = array(
		             'f_op_close' => 't'
		          );
		  $this->db->where('i_op', $iop);
		  $this->db->update('tm_op', $data); 
    }
    function updatespb($ireff,$iop,$iarea)
    {
	    $tmp=explode('-',$ireff);
	    if($tmp[0]=='SPB'){
        $this->db->query(" 	update tm_spb_item set i_op='$iop' where i_spb='$ireff' and i_area='$iarea' and n_order>n_deliver
                            and i_product in (select i_product from tm_op_item where i_op='$iop')");
        $this->db->query(" 	update tm_spb set f_spb_opclose='t' where i_spb='$ireff' and i_area='$iarea'");
	    }elseif($tmp[0]=='SPMB'){
        $this->db->query(" 	update tm_spmb_item set i_op='$iop' where i_spmb='$ireff' and n_order>n_deliver
                            and i_product in (select i_product from tm_op_item where i_op='$iop')");
		    $this->db->query(" 	update tm_spmb set f_spmb_opclose='t' where upper(i_spmb)='$ireff'",false);
	    }
    }
}
?>
