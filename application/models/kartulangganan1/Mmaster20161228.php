<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($tahun,$iarea)
    {	  $this->db->select(" distinct i_customer , e_customer_name, i_area from f_penjualan_tahunancustomer_perbarang ('$tahun') WHERE i_area in ( select i_area from tm_user_area where i_area='$iarea')order by i_customer ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
        function bacai($tahun,$i_area,$icustomer)
    {
		  $this->db->select(" * from f_penjualan_tahunancustomer_perbarang ('$tahun') where i_area='$i_area' and i_customer='$icustomer'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
     function bacadetail($icustomer)
    {
			$this->db->select(" a.i_city ,a.e_customer_name , a.e_customer_address ,a.e_customer_phone , a.n_customer_toplength, 
						b.e_customer_classname , 
						c.e_city_name , 
						d.e_customer_ownername from 
						tr_customer a, tr_customer_class b, tr_city c , tr_customer_owner d
						where a.i_customer_class=b.i_customer_class and a.i_city=c.i_city and a.i_customer=d.i_customer and a.i_customer='$icustomer'
						group by a.i_city ,a.e_customer_name , a.e_customer_address ,a.e_customer_phone , a.n_customer_toplength, 
						b.e_customer_classname , 
						c.e_city_name , 
						d.e_customer_ownername", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaqty($tahun,$icustomer,$i_area)
    {
		$this->db->select(" distinct b.i_product ,b.e_product_name , count(distinct(substring(a.i_nota,4,4))) as qty from tm_nota a, tm_nota_item b
							where a.i_customer='$icustomer'
							and a.i_sj=b.i_sj
							and a.i_area='$i_area'
							and to_char(a.d_nota,'yyyy')='$tahun'
							and a.f_nota_cancel='false'
							group by b.e_product_name , b.i_product order by i_product", false);
							$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
