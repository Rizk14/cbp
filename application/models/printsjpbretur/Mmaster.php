<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasemua($cari,$num,$offset,$dfrom,$dto)
    {
      $area	= $this->session->userdata('i_area');
		  $this->db->select(" a.*, b.e_customer_name
                          from tm_sjpbr a, tr_customer b
                          where a.i_customer=b.i_customer and a.i_area='$area'
                          and (upper(a.i_sjpbr) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          or upper(b.e_customer_name) like '%$cari%')
                          and a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($isjpbr)
    {
		  $this->db->select(" a.*, b.e_customer_name,b.e_customer_address
                          from tm_sjpbr a, tr_customer b
                          where a.i_sjpbr = '$isjpbr'
                          and a.i_customer=b.i_customer
                          order by a.i_sjpbr desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($isjpbr)
    {
		  $this->db->select(" 	* from tm_sjpbr_item
                          inner join tr_product on (tm_sjpbr_item.i_product=tr_product.i_product)
                          inner join tr_product_motif on (tm_sjpbr_item.i_product_motif=tr_product_motif.i_product_motif
                          and tm_sjpbr_item.i_product=tr_product_motif.i_product)
                          where i_sjpbr = '$isjpbr' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_sjpbr
                        from tm_sjpbr a, tr_customer b
                        where a.i_customer=b.i_customer
                        and (upper(a.i_sjpbr) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                        or upper(b.e_customer_name) like '%$cari%')",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatesjpbr($isjpbr)
    {
      $this->db->query("   update tm_sjpbr set n_print=n_print+1
                              where i_sjpbr = '$isjpbr'",false);
    }
}
?>
