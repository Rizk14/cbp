<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset,$dfrom,$dto)
    {
    if($iarea1=='00'){
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
	                      where a.i_customer=b.i_customer 
	                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
	                      or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
                        and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
					              order by a.i_faktur_komersial desc",false)->limit($num,$offset);
#and (a.n_print=0 or a.n_print isnull)
    }else{
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
	                      where a.i_customer=b.i_customer 
	                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
	                      or upper(a.i_faktur_komersial) like '%$cari%')
	                      and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' 
                        or a.i_area='$iarea5') and a.n_faktur_komersialprint>0
                        and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
              					order by a.i_faktur_komersial desc",false)->limit($num,$offset);
#and (a.n_print=0 or a.n_print isnull)
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($sjfrom,$sjto,$fakturfrom,$fakturto,$iarea)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, c.*, d.*, e.*
                        , p.*, o.e_customer_ownername
					              from tm_nota a, tr_customer b left join tr_customer_pkp p on(b.i_customer=p.i_customer)
                              left join tr_customer_owner o on(b.i_customer=o.i_customer), 
                              tr_salesman c, tr_customer_class d, tr_price_group e
					               where a.i_faktur_komersial >= '$fakturfrom' and a.i_faktur_komersial <= '$fakturto' 
                              and a.i_area='$iarea' and not a.i_faktur_komersial isnull
					               and a.i_customer=b.i_customer and a.i_salesman=c.i_salesman
					               and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
					               and b.i_customer_class=d.i_customer_class order by a.i_faktur_komersial",false);
#and a.i_sj>='$sjfrom' and a.i_sj<='$sjto'
#and (a.n_print=0 or a.n_print isnull)
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($isj,$inota,$iarea)
    {
		$this->db->select(" * from tm_nota_item
					              inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
					              and tm_nota_item.i_product=tr_product_motif.i_product)
					              where i_sj='$isj' and i_nota = '$inota' and i_area='$iarea' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' or a.i_area='$iarea5')
					order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($iarea,$fakturfrom,$fakturto,$iseri)
    {
	    $query=$this->db->query("select i_sj from tm_nota where i_faktur_komersial >= '$fakturfrom' and i_faktur_komersial <= '$fakturto' 
                              and i_area = '$iarea' and not i_faktur_komersial isnull and f_nota_cancel='f' order by i_faktur_komersial");
      if($query->num_rows()>0){
        settype($iseri,"string");
        $satu =substr($iseri,0,14);
        $dua  =substr($iseri,14,5);
        foreach($query->result() as $row){
          	$quer   = $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
          	$ro     = $quer->row();
          	$dentry = $ro->c;
          	settype($dua,"string");
          	$a=strlen($dua);
          	$dua=str_repeat("0",5-$a).$dua;
          	$iseri=$satu.$dua;
  	      	$this->db->query("update tm_nota set n_pajak_print=n_pajak_print+1, d_pajak_print='$dentry', d_pajak=d_nota, i_seri_pajak='$iseri'
              							where i_sj='$row->i_sj' and i_area = '$iarea'",false);
          	settype($dua,"integer");
          	$dua++;
        }
      }
    }
    function bacafakturfrom($num,$offset,$dfrom,$dto,$area,$cari,$to)
    {
      if($to==''){
			  $this->db->select(" a.* from tm_nota a
                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and a.i_area='$area' and not a.i_faktur_komersial isnull order by i_faktur_komersial", false)->limit($num,$offset);
      }else{
			  $this->db->select(" a.* from tm_nota a
                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and a.i_area='$area' and not a.i_faktur_komersial isnull 
                            and a.i_faktur_komersial <= '$to'
                            order by i_faktur_komersial", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacafakturto($num,$offset,$dfrom,$dto,$area,$cari,$from)
    {
      if($from==''){
			  $this->db->select(" a.* from tm_nota a
                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and a.i_area='$area' and not a.i_faktur_komersial isnull order by i_faktur_komersial", false)->limit($num,$offset);
      }else{
			  $this->db->select(" a.* from tm_nota a
                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                            and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                            and a.i_area='$area' and not a.i_faktur_komersial isnull 
                            and a.i_faktur_komersial >= '$from'
                            order by i_faktur_komersial", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
