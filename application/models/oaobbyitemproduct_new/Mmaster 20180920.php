<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    {
      $pecah1       = explode('-', $dfrom);
        $tgl1       = $pecah1[0];
        $bln1       = $pecah1[1];
        $tahun1     = $pecah1[2];
        $tahunprev1 = intval($tahun1) - 1;

      $pecah2       = explode('-', $dto);
        $tgl2       = $pecah2[0];
        $bln2       = $pecah2[1];
        $tahun2     = $pecah2[2];
        $tahunprev2 = intval($tahun2) - 1;

      $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
      $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;

      /*$sql=" z.i_product, sum(z.oa) as oa , sum(z.oaprev) as oaprev, z.e_product_name, sum(z.jml) as jml ,
                          sum(z.jmlprev) as jmlprev, sum(z.netitem) as netitem, sum(z.netitemprev) as netitemprev from (
                                select b.i_product, sum(b.oa) as oa , 0 as oaprev, b.e_product_name, sum(b.jml) as jml , 0 as jmlprev, sum(b.netitem) as netitem, 0 as netitemprev from (
                                  select  a.i_product, count(a.i_customer) as oa, a.e_product_name, sum(a.jml) as jml , 0 as netitem from (
                                    select distinct a.i_product, b.i_customer, c.e_product_name, sum(a.n_deliver) as jml
                                    from tm_nota_item a, tm_nota b, tr_product c
                                    where a.i_nota=b.i_nota and a.i_product = c.i_product
                                          and b.f_nota_cancel = 'false'
                                          and to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and to_char(now(),'mm')
                                    group by a.i_product, b.i_customer, c.e_product_name, b.i_nota
                                  )as a
                                  group by a.i_product,a.e_product_name 
                                  
                                  union all 

                                  select  a.i_product, 0 as oa, a.e_product_name, 0 as jml, sum(a.netitem) as netitem from (
                                    select a.i_product, c.e_product_name, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal))) as netitem
                                    from tm_nota_item a, tm_nota b , tr_product c
                                    where a.i_nota=b.i_nota and a.i_product = c.i_product
                                          and b.f_nota_cancel = 'false'
                                          and to_char(b.d_nota,'yyyy') = '$tahun' and to_char(b.d_nota,'mm') between '01' and to_char(now(),'mm')
                                    group by a.i_product, c.e_product_name
                                  )as a
                                  group by a.i_product, a.e_product_name
                                  
                                ) as b
                                group by b.i_product, b.e_product_name
                                

                                union all 

                                select b.i_product,0 as oa, sum(b.oaprev) as oaprev , b.e_product_name, 0 as jml, sum(b.jmlprev) as jmlprev , 0 as netitem, sum(b.netitemprev) as netitemprev from (
                                  select  a.i_product, count(a.i_customer) as oaprev, a.e_product_name, sum(a.jmlprev) as jmlprev , 0 as netitemprev from (
                                    select distinct a.i_product, b.i_customer, c.e_product_name, sum(a.n_deliver) as jmlprev
                                    from tm_nota_item a, tm_nota b, tr_product c
                                    where a.i_nota=b.i_nota and a.i_product = c.i_product
                                          and b.f_nota_cancel = 'false'
                                          and to_char(b.d_nota,'yyyy') = '$thprev' and to_char(b.d_nota,'mm') between '01' and '12'
                                    group by a.i_product, b.i_customer, c.e_product_name, b.i_nota
                                  )as a
                                  group by a.i_product,a.e_product_name 
                                  
                                  union all 

                                  select  a.i_product, 0 as oaprev, a.e_product_name, 0 as jmlprev, sum(a.netitemprev) as netitemprev from (
                                    select a.i_product, c.e_product_name, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal))) as netitemprev
                                    from tm_nota_item a, tm_nota b , tr_product c
                                    where a.i_nota=b.i_nota and a.i_product = c.i_product
                                          and b.f_nota_cancel = 'false'
                                          and to_char(b.d_nota,'yyyy') = '$thprev' and to_char(b.d_nota,'mm') between '01' and '12'
                                    group by a.i_product, c.e_product_name
                                  )as a
                                  group by a.i_product, a.e_product_name
                                  
                                ) as b
                                group by b.i_product, b.e_product_name
                                
                              ) as z
                              group by z.i_product, z.e_product_name
                              order by z.i_product;";*/

      $this->db->select(" z.i_product, sum(z.oa) as oa , sum(z.oaprev) as oaprev, sum(z.ob) as ob, z.e_product_name, sum(z.jml) as jml ,
sum(z.jmlprev) as jmlprev, sum(z.netitem) as netitem, sum(z.netitemprev) as netitemprev ,
(sum(z.netitem)/(select sum(v_nota_netto) as v_nota from tm_nota where d_nota >= to_date('$dfrom','dd-mm-yyyy') and d_nota <= to_date('$dto','dd-mm-yyyy') 
and not i_nota isnull and f_nota_cancel='false')*100) as ctrnetsales from (
                          
                          select b.i_product, sum(b.oa) as oa , 0 as oaprev, sum(b.ob) as ob, b.e_product_name, sum(b.jml) as jml , 0 as jmlprev, sum(b.netitem) as netitem, 0 as netitemprev from (
                              select  a.i_product, count(a.i_customer) as oa,0 as oaprev, 0 as ob, a.e_product_name, sum(a.jml) as jml , 0 as netitem from (
                                select distinct on (to_char(b.d_nota,'yyyymm'),b.i_customer) a.i_product, b.i_customer, c.e_product_name, 0 as jml
                                from tm_nota_item a, tm_nota b, tr_product c
                                where a.i_nota=b.i_nota and a.i_product = c.i_product
                                and b.f_nota_cancel = 'false'
                                and (b.d_nota >= to_date('$dfrom','dd-mm-yyyy') and b.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                and not b.i_nota isnull
                                group by a.i_product, b.i_customer, c.e_product_name, b.i_nota,b.d_nota
                              )as a
                              group by a.i_product,a.e_product_name 
                            
                            union all 

                              select  a.i_product, 0 as oa, 0 as oaprev, 0 as ob, a.e_product_name, sum(a.jml) as jml, sum(a.netitem) as netitem from (
                                select a.i_product, c.e_product_name, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal))) as netitem,sum(a.n_deliver) as jml
                                from tm_nota_item a, tm_nota b , tr_product c
                                where a.i_nota=b.i_nota and a.i_product = c.i_product
                                and b.f_nota_cancel = 'false'
                                and (b.d_nota >= to_date('$dfrom','dd-mm-yyyy') and b.d_nota <= to_date('$dto','dd-mm-yyyy'))
                                and not b.i_nota isnull
                                group by a.i_product, c.e_product_name
                              )as a
                              group by a.i_product, a.e_product_name
                          ) as b
                          group by b.i_product, b.e_product_name
                          -------------------------------------------tahun lalu-----------------------------------------------------------
                          union all 
                          -------------------------------------------tahun lalu ----------------------------------------------------------
                          select b.i_product,0 as oa, sum(b.oaprev) as oaprev , 0 as ob, b.e_product_name, 0 as jml, sum(b.jmlprev) as jmlprev , 0 as netitem, sum(b.netitemprev) as netitemprev from (
                            select  a.i_product, count(a.i_customer) as oaprev, a.e_product_name, 0 as jmlprev , 0 as netitemprev from (
                              select distinct on (to_char(b.d_nota,'yyyymm'),b.i_customer) a.i_product, b.i_customer, c.e_product_name, 0 as jmlprev
                              from tm_nota_item a, tm_nota b, tr_product c
                              where a.i_nota=b.i_nota and a.i_product = c.i_product
                              and b.f_nota_cancel = 'false'
                              and (b.d_nota >= to_date('$gabung1','dd-mm-yyyy') and b.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                              and not b.i_nota isnull
                              group by a.i_product, b.i_customer, c.e_product_name, b.i_nota,b.d_nota
                            )as a
                            group by a.i_product,a.e_product_name 
                            
                            union all 

                            select  a.i_product, 0 as oaprev, a.e_product_name, sum(a.jmlprev) as jmlprev, sum(a.netitemprev) as netitemprev from (
                              select a.i_product, c.e_product_name, sum((a.n_deliver*a.v_unit_price-(((a.n_deliver*a.v_unit_price)/b.v_nota_gross)*b.v_nota_discounttotal))) as netitemprev, sum(a.n_deliver) as jmlprev
                              from tm_nota_item a, tm_nota b , tr_product c
                              where a.i_nota=b.i_nota and a.i_product = c.i_product
                              and b.f_nota_cancel = 'false'
                              and (b.d_nota >= to_date('$gabung1','dd-mm-yyyy') and b.d_nota <= to_date('$gabung2','dd-mm-yyyy'))
                              and not b.i_nota isnull
                              group by a.i_product, c.e_product_name
                            )as a
                            group by a.i_product, a.e_product_name
                            
                          ) as b
                          group by b.i_product, b.e_product_name
                          
                              ) as z
                              group by z.i_product, z.e_product_name
                              order by ctrnetsales desc ,z.i_product",false);
      
      /*$this->db->select($sql,FALSE);*/
		  $query = $this->db->get();
      if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatotal($todate,$prevdate,$th,$prevth,$bl)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	sum(custlm) as custlm, sum(custcm) as custcm, sum(custly) as custly, sum(custcy) as custcy from (
                          select count(a.i_customer) as custlm, 0 as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, count(a.i_customer) as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, 0 as custcm, sum(a.custly) as custly, 0 as custcy from
                          (
                          select count(a.i_customer) as custly from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          union all
                          select 0 as custlm, 0 as custcm, 0 as custly, sum(a.custcy) as custcy from (
                          select count(a.i_customer) as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate' and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          ) as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaob($dfrom,$dto)
    { 
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $thbl=$thnow.$bl;
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnya=$tmp[2];
        $thblto=$thnya.$bl;
        $dtoprev=$hr."-".$bl."-".$th;

      $this->db->select(" count(a.ob) as ob from (
                          SELECT distinct i_customer as ob, e_periode ,i_salesman
                          from tr_customer_salesman
                          where e_periode ='$thblto'
                          ) as a",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
}
?>
