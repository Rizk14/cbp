<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function bacaperiode($dfrom, $dto, $num, $offset, $cari, $iarea)
	{
		$this->db->select("	a.*,
								b.e_area_name
							FROM
								tm_dkb_sjp a,
								tr_area b
							WHERE
								a.i_area = b.i_area
								AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
								AND a.i_approve1 ISNULL
								AND a.f_dkb_batal = 'f'
								AND substring(a.i_dkb, 11, 2)= '$iarea'
								AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
							ORDER BY
								a.i_dkb DESC ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	public function delete($idkb, $iarea)
	{
		$this->db->query("update tm_dkb set f_dkb_batal='t' WHERE i_dkb='$idkb' and i_area='$iarea'");
		return TRUE;
	}

	function bacasemua($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
							order by a.i_dkb desc", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cari($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
					where a.i_area=b.i_area 
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_dkb) like '%$cari%')
					order by a.i_dkb desc", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariperiode($dfrom, $dto, $num, $offset, $cari, $iarea)
	{
		$this->db->select("	a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%')
              and a.i_approve1 isnull
							and a.f_dkb_batal='f'
              and substring(a.i_dkb,10,2)='$iarea'
							and a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function baca($idkb, $iarea)
	{
		$this->db->select(" tm_dkb_sjp.i_dkb, tm_dkb_sjp.i_dkb_kirim, tm_dkb_sjp.i_dkb_via, tm_dkb_sjp.i_area, tm_dkb_sjp.i_ekspedisi, tm_dkb_sjp.d_dkb, /* tm_dkb_sjp.i_dkb_old, */
							tm_dkb_sjp.i_kendaraan, tm_dkb_sjp.e_sopir_name, tm_dkb_sjp.v_dkb, tm_dkb_sjp.f_dkb_batal, tm_dkb_sjp.d_entry, tm_dkb_sjp.d_update, tm_dkb_sjp.i_area,
							tr_area.e_area_name, tr_dkb_kirim.e_dkb_kirim, tr_dkb_via.e_dkb_via, tr_ekspedisi.e_ekspedisi 
							from tm_dkb_sjp 
							inner join tr_area on(tm_dkb_sjp.i_area=tr_area.i_area)
							inner join tr_dkb_kirim on(tm_dkb_sjp.i_dkb_kirim=tr_dkb_kirim.i_dkb_kirim)
							inner join tr_dkb_via on(tm_dkb_sjp.i_dkb_via=tr_dkb_via.i_dkb_via)
							left join tr_ekspedisi on(tm_dkb_sjp.i_ekspedisi=tr_ekspedisi.i_ekspedisi)
							where  tm_dkb_sjp.i_dkb ='$idkb'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}

	function bacadetail($idkb, $iarea)
	{
		$this->db->select(" * from tm_dkb_sjp_item where i_dkb = '$idkb' order by i_dkb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetailrunningjml($idkb, $iarea)
	{
		return $this->db->query(" select sum(v_jumlah) as v_total from tm_dkb_sjp_item where i_dkb='$idkb' and i_area='$iarea' ", false);
	}
	function bacadetailx($idkb, $iarea)
	{
		$this->db->select("a.*, b.e_ekspedisi from tm_dkb_ekspedisi a, tr_ekspedisi b where a.i_dkb = '$idkb' and a.i_area='$iarea' and a.i_ekspedisi = b.i_ekspedisi order by a.i_dkb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function updateheader($idkb, $iarea)
	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$iapprove = $this->session->userdata('user_id');
		$this->db->set(
			array(
				'i_approve1'  => $iapprove,
				'e_approve1'  => null,
				'd_approve1'  => $dentry
			)
		);
		$this->db->where('i_dkb', $idkb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_dkb_sjp');
	}

	function updatedetail($idkb, $iarea, $isjp, $ddkb, $fkirim)
	{
		$this->db->query("update tm_dkb_sjp_item set f_kirim='$fkirim' where i_sjp='$isjp' and i_area='$iarea' and i_dkb='$idkb'");
		/*$this->db->set(
    		array(
		    'f_kirim'	=> $fkirim
    		)
    	);
    	$this->db->where('i_sj',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->where('i_dkb',$idkb);
    	$this->db->update('tm_dkb_sjp_item');*/
	}

	function updatesjp($idkb, $isjp, $iarea, $ddkb)
	{
		$this->db->set(
			array(
				'i_dkb'	=> null,
				'd_dkb' => null
			)
		);
		$this->db->where('i_sjp', $isjp);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_sjp');
	}
	function customertodetail($isj, $dsj, $iarea)
	{
		return $this->db->query(" select b.i_customer, b.e_customer_name from tr_sj_type, tm_nota a left join tr_customer b on b.i_customer=a.i_customer 
				where tr_sj_type.i_sj_type='04' and a.i_sj='$isj' and a.d_sj='$dsj' and a.i_area='$iarea' ");
	}
}
