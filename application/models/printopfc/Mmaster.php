<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
		$this->db->query('DELETE FROM tm_op WHERE i_op=\''.$iop.'\'');
		$this->db->query('DELETE FROM tm_op_item WHERE i_op=\''.$iop.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_opfc a, tr_supplier b
							where a.i_supplier=b.i_supplier and f_op_cancel='f'
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_op) like '%$cari%')
							order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iop)
    {
		$this->db->select(" * from tm_opfc
							inner join tr_supplier on (tm_opfc.i_supplier=tr_supplier.i_supplier)
							inner join tr_op_status on (tm_opfc.i_op_status=tr_op_status.i_op_status)
							inner join tr_area on (tm_opfc.i_area=tr_area.i_area)
							where tm_opfc.i_op = '$iop' and f_op_cancel='f'
							order by tm_opfc.i_op desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($iop)
    {
    	$this->db->select(" a.* from tm_opfc_item a, tm_opfc c where a.i_op='$iop'
                           and a.i_op=c.i_op and f_op_cancel='f'
                           order by a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_opfc a, tr_supplier b
							where a.i_supplier=b.i_supplier and f_op_cancel='f'
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_op) like '%$cari%')
							order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
