<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasj($cari,$num,$offset,$iuser)
    {
		  $this->db->select("	i_sjp, i_area from tm_sjp where upper(i_sjp) like '%$cari%'
                          and (i_area in ( select i_area from tm_user_area where i_user='$iuser'))
                          order by i_sjp", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasj2($num,$offset,$area)
    {
		  $this->db->select("	i_sjp, i_area from tm_sjp where i_area='$area' order by i_sjp", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj($cari,$num,$offset,$iuser)
    {
		  $this->db->select("	i_sjp, i_area from tm_sjp where upper(i_sjp) like '%$cari%'
                          and (i_area in ( select i_area from tm_user_area where i_user='$iuser'))
                          order by i_sjp",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj2($cari,$num,$offset,$area)
    {
		  $this->db->select("	i_sjp, i_area from tm_sjp where upper(i_sjp) like '%$cari%'
                          and i_area='$area' order by i_sjp",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacamaster($sjfrom,$sjto,$area)
    {
		  $this->db->select("	tm_sjp.*, tr_area.e_area_name, tm_spmb.i_spmb  from tm_sjp
							            inner join tr_area on (tm_sjp.i_area=tr_area.i_area)
							            left join tm_spmb on (tm_spmb.i_spmb=tm_sjp.i_spmb and tm_spmb.i_area=tm_sjp.i_area)
						              where tm_sjp.i_sjp >= '$sjfrom' and tm_sjp.i_sjp <= '$sjto'
           							  and tr_area.i_area ='$area' order by tm_sjp.i_sjp",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($sj,$area)
    {
		  $this->db->select("	* from tm_sjp_item 
							            inner join tr_product_motif on (tm_sjp_item.i_product_motif=tr_product_motif.i_product_motif
							            and tm_sjp_item.i_product=tr_product_motif.i_product)
							            where tm_sjp_item.i_sjp = '$sj' and tm_sjp_item.i_area ='$area'
                          order by tm_sjp_item.i_sjp",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
