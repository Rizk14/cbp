<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	  public function __construct()
    {
          parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
      $this->db->select(" distinct(a.i_customer), b.e_customer_name, sum(c.n_quantity) as jumlah, a.n_notapb_discount,
                          sum(a.v_notapb_discount) as diskon, sum(c.n_quantity*c.v_unit_price) as kotor, a.i_area
                          from tm_notapb a, tr_customer b, tm_notapb_item c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                          and a.f_spb_rekap='f' and not a.i_cek is null and a.f_notapb_cancel='f'
                          group by a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                          order by a.i_customer, a.n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadiskon($iperiode)
    {
		  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                          where to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          order by n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumberspb($thbl,$namagrup,$iarea){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max, i_group from tm_dgu_nopb
                          where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
          $grup=$row->i_group;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_nopb 
                            set n_modul_no=$nospb
                            where i_modul='SPB' and i_group='$grup'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<4){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
		  	$nospb  ="SPB-".$thbl."-".$grup.$nospb;
			  return $nospb;
		  }else{
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                            and substr(e_periode,1,4)='$th'", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $grup=$row->i_group;
          }
			  }
			  $nospb  ="0001";
			  $nospb  ="SPB-".$thbl."-".$grup.$nospb;
        $this->db->query(" insert into tm_dgu_nopb(i_modul, i_area, i_group, e_periode, n_modul_no,e_group_name) 
                           values ('SPB','$iarea','$grup','$asal',1,'$namagrup')");
			  return $nospb;
		  }
    }
    function insertheader($ispb, $dspb, $iarea, $icustomer)
    {
      $this->db->select(" a.*, b.i_price_group as klp_harga
                          from tr_customer a, tr_price_group b
                          where (a.i_price_group=b.i_price_group or a.i_price_group=b.n_line)
                          and a.i_customer='$icustomer' and a.i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $ipricegroup  = $row->klp_harga;
          $fpkp         = $row->f_customer_pkp;
          $fppn         = $row->f_customer_plusppn;
          $top          = $row->n_customer_toplength;
			  }
      	$this->db->set(
      		array(
			        'i_spb'		              => $ispb,
			        'd_spb'		              => $dspb,
              'i_customer'            => $icustomer,
              'i_price_group'         => $ipricegroup,
              'i_product_group'       => '01',
              'i_salesman'            => 'TL',
              'f_spb_stockdaerah'     => 't',
              'f_spb_consigment'      => 't',
              'f_spb_pkp'             => $fpkp,
              'f_spb_plusppn'         => $fppn,
			        'i_area'		            => $iarea,
              'n_spb_toplength'       => $top,
              'n_spb_discount1'       => 0,
              'n_spb_discount2'       => 0,
              'n_spb_discount3'       => 0,
              'v_spb_discount1'       => 0,
              'v_spb_discount2'       => 0,
              'v_spb_discount3'       => 0,
              'v_spb_discounttotal'   => 0,
			        'n_print'		            => 0
      		)
      	);
      	$this->db->insert('tm_spb');
      }
    }
/*    function insertdetail($ispb,$iarea,$icustomer,$iperiode)
    {
      $que=$this->db->query(" 	select a.*, b.v_product_retail from tm_notapb_item a, tr_product_priceco b 
                                where a.i_product=b.i_product and b.i_price_group=a.i_price_groupco
                                and a.i_customer='$icustomer'
                                and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'");
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $query=$this->db->query(" 	select count(*) as brs from tm_spb_item where i_spb='$ispb'");
          if($query->num_rows()>0){
            $br=$query->row();
            $baris=$br->brs+1;
          }else{
            $baris=1;
          }
          $query=$this->db->query(" 	select a.* from tm_spb_item a
                                      where a.i_spb='$ispb' and a.i_product='$row->i_product' and
                                      a.i_product_grade='$row->i_product_grade' and a.i_product_motif='$row->i_product_motif'");
            if($query->num_rows()>0){
              $this->db->query(" 	update tm_spb_item set n_order=n_order+$row->n_quantity
                                  where i_spb='$ispb' and i_product='$row->i_product' and i_product_grade='$row->i_product_grade'
                                  and i_product_motif='$row->i_product_motif'");
            }else{
              $this->db->query(" 	insert into tm_spb_item values('$ispb','$row->i_product','$row->i_product_grade',
                                  '$row->i_product_motif',$row->n_quantity,null,null,$row->v_product_retail,
                                  '$row->e_product_name',null,'$row->i_area',null,$baris,'1')");
          }
        }
      }
    }*/
    function insertdetail($ispb,$iarea,$iproduct,$eproductname,$iproductgrade,$iproductmotif,$harga,$quantity,$x)
    {
      $this->db->query(" 	insert into tm_spb_item values('$ispb','$iproduct','$iproductgrade',
                          '$iproductmotif',$quantity,null,null,$harga,
                          '$eproductname',null,'$iarea',null,$x,'1')");
    }
    function updatespb($ispb,$iarea)
    {
      $query=$this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spb_item
                                where i_area = '$iarea' and i_spb='$ispb'");
      if($query->num_rows()>0){
        foreach($query->result() as $xx){
          $total=$xx->order;
        }
	        $this->db->query("update tm_spb set v_spb='$total' where i_spb='$ispb' and i_area='$iarea'");
      }
    }

    function updatebon($icustomer,$iperiode,$ispb)
    {
#      $query=$this->db->query("select i_notapb
#                        from tm_notapb 
#                        where i_customer='$icustomer'
#                        and to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
#                        order by i_notapb");
#      if($query->num_rows()>0){
#        foreach($query->result() as $xx){
#          $inotapb=$xx->i_notapb;
#        }
	        $this->db->query("update tm_notapb set i_spb='$ispb', f_spb_rekap='t' where i_customer='$icustomer'
                        and to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'");
#      }
    }
}
?>
