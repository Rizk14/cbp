<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_nota, a.i_spb, a.i_area, a.i_customer, a.i_salesman, a.i_dkb, a.d_spb, a.d_nota, 
							a.d_jatuh_tempo, a.d_dkb, a.d_nota_entry, a.v_nota_gross, a.v_nota_netto , a.i_nota_old,
							b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer
							and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_nota desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_spb, a.i_salesman, a.i_customer, a.i_price_group, a.i_nota, a.i_store, a.i_store_location,
              a.d_spb, a.d_nota, a.d_spbentry, a.d_sj, a.d_spb_receive, a.f_spb_op, a.f_spb_pkp, a.f_spb_plusdiscount,
              a.f_spb_stockdaerah, a.f_spb_program, a.f_spb_consigment, a.f_spb_valid, a.f_spb_siapnotagudang, a.f_spb_cancel
              a.n_spb_discount1, a.n_spb_discount2, a.n_spb_discount3, a.v_spb, a.v_spb_after, a.i_approve1, a.i_approve2,
              a.d_approve1, a.d_approve2, a.i_area, a.f_siapnotasales, a.i_spb_old, a.i_product_group, a.f_spb_opclose,
              a.f_spb_pemenuhan, a.i_cek, a.d_cek, a.e_remark1,
							b.e_customer_name from tm_spb a, tr_customer b
							where a.i_customer=b.i_customer and not a.i_approve1 isnull
							and not a.i_approve2 isnull and f_spb_siapnota = 't'
							and f_spb_cancel = 'f' and not i_store isnull and a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
								 or upper(a.i_spb) like '%$cari%') ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacattb($iarea,$ittb,$tahun)
    {
		/*echo "a.i_area, a.i_ttb, a.d_ttb, a.i_customer, a.i_salesman, a.i_bbm, a.d_bbm,
							a.n_ttb_discount1, a.n_ttb_discount2, a.n_ttb_discount3, a.v_ttb_discount1, a.v_ttb_discount2, a.v_ttb_discount3,
							a.v_ttb_gross, a.v_ttb_discounttotal, a.v_ttb_netto, a.v_ttb_sisa, a.f_ttb_cancel, a.f_ttb_pkp,
							a.f_ttb_plusdiscount, a.f_ttb_plusppn, a.d_receive1, a.e_ttb_remark, a.d_entry, a.n_ttb_year
							,b.i_bbm, d.e_customer_name 
							,c.i_area, c.e_area_name 
							,e.i_salesman 
							,e.e_salesman_name
							,f.e_customer_pkpnpwp
							,g.*
              ,h.*
							from tm_ttbretur a 
							left join tm_bbm b on (a.i_ttb = b.i_refference_document and a.i_salesman=b.i_salesman)
							inner join tr_area c on (a.i_area = c.i_area)
							inner join tr_customer d on (a.i_customer = d.i_customer)
							inner join tr_salesman e on (a.i_salesman = e.i_salesman)
							left join tr_customer_pkp f on (a.i_customer = f.i_customer)
							inner join tr_price_group g on (g.n_line=d.i_price_group or g.i_price_group=d.i_price_group)
              left join tr_alasan_retur h on (a.i_alasan_retur = h.i_alasan_retur)
							where a.i_area = '$iarea' 
							and a.i_ttb = '$ittb' 
							and a.n_ttb_year=$tahun"; die(); */
							
		$this->db->select(" a.i_area, a.i_ttb, a.d_ttb, a.i_customer, a.i_salesman, a.i_bbm, a.d_bbm, a.n_ttb_discount1, a.n_ttb_discount2, 
		          a.n_ttb_discount3, a.v_ttb_discount1, a.v_ttb_discount2, a.v_ttb_discount3, a.v_ttb_gross, a.v_ttb_discounttotal, 
		          a.v_ttb_netto, a.v_ttb_sisa, a.f_ttb_cancel, a.f_ttb_pkp, a.f_ttb_plusdiscount, a.f_ttb_plusppn, a.d_receive1, a.e_ttb_remark, 
		          a.d_entry, a.n_ttb_year, b.i_bbm, d.e_customer_name,c.i_area, c.e_area_name, e.i_salesman, e.e_salesman_name, 
		          f.e_customer_pkpnpwp, g.*,h.*
							from tm_ttbretur a 
							left join tm_bbm b on (a.i_ttb = b.i_refference_document and a.i_salesman=b.i_salesman and a.d_bbm=b.d_bbm)
							inner join tr_area c on (a.i_area = c.i_area)
							inner join tr_customer d on (a.i_customer = d.i_customer)
							inner join tr_salesman e on (a.i_salesman = e.i_salesman)
							left join tr_customer_pkp f on (a.i_customer = f.i_customer)
							inner join tr_price_group g on (g.n_line=d.i_price_group or g.i_price_group=d.i_price_group)
              left join tr_alasan_retur h on (a.i_alasan_retur = h.i_alasan_retur)
							where a.i_area = '$iarea' 
							and a.i_ttb = '$ittb' 
							and a.n_ttb_year=$tahun
							",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacattbdetail($iarea,$ittb,$tahun)
    {
		$this->db->select("	i_area, i_ttb, d_ttb, i_nota, d_nota, i_product1, i_product1_grade, i_product1_motif, i_product2, i_product2_grade,
                        e_product_name, v_unit_price, e_product_motifname, e_ttb_remark,
                        i_product2_motif,	n_quantity, n_quantity_receive, v_unit_price, n_ttb_year from tm_ttbretur_item
						   	inner join tr_product on (tr_product.i_product=tm_ttbretur_item.i_product1)
						   	inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_ttbretur_item.i_product1_motif
														and tr_product_motif.i_product=tm_ttbretur_item.i_product1)
						   	where tm_ttbretur_item.i_ttb = '$ittb' and tm_ttbretur_item.i_area ='$iarea' and tm_ttbretur_item.n_ttb_year=$tahun
						   	order by tm_ttbretur_item.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
	function baca($inota)
    {
		$this->db->select(" i_nota, i_spb, i_customer, i_salesman, i_dkb, d_spb, d_nota, d_jatuh_tempo, d_dkb, d_nota_update, f_plus_ppn, f_plus_discount, f_masalah, 						f_insentif, f_lunas, n_nota_discount1, n_nota_discount2, n_nota_discount3, v_nota_gross, v_nota_netto, v_sisa, f_ttb_tolak, f_nota_koreksi, i_area, 						f_cicil, f_posting, f_close, f_nota_cancel, i_nota_old, i_sj, d_sj, i_sj_old, f_pajak_pengganti from tm_nota 
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
				   where i_nota = '$inota'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetail($inota)
    {
		$this->db->select(" i_sj, i_nota, i_product, i_product_grade, i_product_motif, n_deliver, v_unit_price, e_product_name, i_area, d_nota, n_item_no from 								tm_nota_item
						   inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_nota_item.i_product_motif
													   and tr_product_motif.i_product=tm_nota_item.i_product)
						   where tm_nota_item.i_nota = '$inota'
						   order by tm_nota_item.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function insertheader(	$iarea,$ittb,$dttb,$icustomer,$isalesman,$nttbdiscount1,$nttbdiscount2,
							$nttbdiscount3,$vttbdiscount1,$vttbdiscount2,$vttbdiscount3,$fttbpkp,$fttbplusppn,
							$fttbplusdiscount,$vttbgross,$vttbdiscounttotal,$vttbnetto,$ettbremark,$fttbcancel,
							$dreceive1,$tahun,$ialasanretur,$ipricegroup,$inota)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_area'				      => $iarea,
					'i_ttb'					      => $ittb,
					'd_ttb'					      => $dttb,
					'i_customer'			    => $icustomer,
					'i_salesman'			    => $isalesman,
					'n_ttb_discount1'	    => $nttbdiscount1,
					'n_ttb_discount2'	    => $nttbdiscount2,
					'n_ttb_discount3'	    => $nttbdiscount3,
					'v_ttb_discount1'	    => $vttbdiscount1,
					'v_ttb_discount2'	    => $vttbdiscount2,
					'v_ttb_discount3'	    => $vttbdiscount3,
					'f_ttb_pkp'		    		=> $fttbpkp,
					'f_ttb_plusppn'		    => $fttbplusppn,
					'f_ttb_plusdiscount'  => $fttbplusdiscount,
					'v_ttb_gross'			    => $vttbgross,
					'v_ttb_discounttotal'	=> $vttbdiscounttotal,
					'v_ttb_netto'   			=> $vttbnetto,
					'v_ttb_sisa'		    	=> $vttbnetto,
					'e_ttb_remark'	  		=> $ettbremark,
					'f_ttb_cancel'	  		=> $fttbcancel,
					'd_receive1'		    	=> $dreceive1,
					'd_entry'				      => $dentry,
					'n_ttb_year'		    	=> $tahun,
          'i_alasan_retur'      => $ialasanretur,
          'i_price_group'       => $ipricegroup,
          'i_nota'              => $inota
    		)
    	);
    	$this->db->insert('tm_ttbretur');
    }
	function insertdetail($iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver,$i)
    {
    	$this->db->set(
    		array(
					'i_area'			      => $iarea,
					'i_ttb'	      			=> $ittb,
					'd_ttb'				      => $dttb,
					'i_product1'		    => $iproduct,
					'i_product1_grade'	=> $iproductgrade,
					'i_product1_motif'	=> $iproductmotif,
					'n_quantity'    		=> $nquantity,
					'v_unit_price'	  	=> $vunitprice,
					'e_ttb_remark'	  	=> $ettbremark,
					'n_ttb_year'		    => $tahun,
          'n_item_no'         => $i
    		)
    	);
    	$this->db->insert('tm_ttbretur_item');
    }
	function updatedetail($iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$nquantity,$vunitprice,$ettbremark,$tahun,$xtahun,$ndeliver,$i)
    {
/*      $query=$this->db->query("select * from tm_ttbretur_item where i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun
                               and i_product1='$iproduct' and i_product1_grade='$iproductgrade' and i_product1_motif='$iproductmotif'");*/
      $query=$this->db->query("select * from tm_ttbretur_item where i_ttb='$ittb' and i_area='$iarea'
                               and i_product1='$iproduct' and i_product1_grade='$iproductgrade' and i_product1_motif='$iproductmotif'");
      if($query->num_rows()==0){
        $this->db->set(
      		array(
					  'i_area'			      => $iarea,
					  'i_ttb'	      			=> $ittb,
					  'd_ttb'				      => $dttb,
					  'i_product1'		    => $iproduct,
					  'i_product1_grade'	=> $iproductgrade,
					  'i_product1_motif'	=> $iproductmotif,
					  'n_quantity'    		=> $nquantity,
					  'v_unit_price'	  	=> $vunitprice,
					  'e_ttb_remark'	  	=> $ettbremark,
					  'n_ttb_year'		    => $tahun,
            'n_item_no'         => $i
      		)
      	);
      	$this->db->insert('tm_ttbretur_item');
      }else{
      	$this->db->set(
      		array(
					  'd_ttb'				      => $dttb,
					  'n_quantity'    		=> $nquantity,
					  'v_unit_price'	  	=> $vunitprice,
					  'e_ttb_remark'	  	=> $ettbremark,
            'n_ttb_year'        => $tahun,
            'n_item_no'         => $i
      		)
      	);
        	$this->db->where('i_ttb',$ittb);
        	$this->db->where('i_area',$iarea);
        	$this->db->where('n_ttb_year',$xtahun);
        	$this->db->where('i_product1',$iproduct);
        	$this->db->where('i_product1_grade',$iproductgrade);
        	$this->db->where('i_product1_motif',$iproductmotif);
      	$this->db->update('tm_ttbretur_item');
      }
    }

  function updatebbm(	$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$vunitprice)
  {
    	$this->db->set(
    		array(
					'v_unit_price'	  	=> $vunitprice
    		)
    	);
    	$this->db->where('trim(i_refference_document)',$ittb);
    	$this->db->where('d_refference_document',$dttb);
    	$this->db->where('i_product',$iproduct);
    	$this->db->where('i_product_grade',$iproductgrade);
    	$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_bbm_item');
  }
	function updateheader(	$ittb,$iarea,$tahun,$xtahun,$dttb,$dreceive1,$eremark,
							            $nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
							            $vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
							            $vttbgross,$icustomer,$ibbm,$isalesman,$ialasanretur,$ipricegroup,$inota)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dupdate= $row->c;
    	$this->db->set(
    		array(
			'd_ttb'						=> $dttb,
			'd_receive1'			=> $dreceive1,
			'e_ttb_remark'		=> $eremark,
			'd_update'				=> $dupdate,
      'i_salesman'      => $isalesman,
			'n_ttb_discount1'	=> $nttbdiscount1,
			'n_ttb_discount2'	=> $nttbdiscount2,
			'n_ttb_discount3'	=> $nttbdiscount3,
			'v_ttb_discount1'	=> $vttbdiscount1,
			'v_ttb_discount2'	=> $vttbdiscount2,
			'v_ttb_discount3'	=> $vttbdiscount3,
			'v_ttb_gross'			=> $vttbgross,
			'v_ttb_discounttotal'	=> $vttbdiscounttotal,
			'v_ttb_netto'			=> $vttbnetto,
			'v_ttb_sisa'			=> $vttbnetto,
			'f_ttb_cancel'		=> 'f',
      'i_customer'      => $icustomer,
      'i_alasan_retur'  => $ialasanretur,
      'i_price_group'   => $ipricegroup,
      'n_ttb_year'      => $tahun,
      'i_nota'          => $inota
    		)
    	);
		  $this->db->where('i_ttb',$ittb);
		  $this->db->where('i_area',$iarea);
		  $this->db->where('n_ttb_year',$xtahun);
    	$this->db->update('tm_ttbretur');
      $query 	= $this->db->query("SELECT i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'");
		  $row   	= $query->row();
		  $icustomergroupar= $row->i_customer_groupar;
      $this->db->set(
    		array(
          'i_customer'        => $icustomer,
          'i_customer_groupar'=> $icustomergroupar,
					'v_gross'	  	      => $vttbgross,
					'v_discount'	      => $vttbdiscounttotal,
					'v_netto'	  	      => $vttbnetto,
					'v_sisa'	  	      => $vttbnetto
    		)
    	);
    	$this->db->where('i_refference',$ibbm);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_kn');
/*
      $this->db->set(
    		array(
          'i_customer'         => $icustomer
    		)
    	);
    	$this->db->where('i_bbm',$ibbm);
    	$this->db->where('i_bbm_type','05');
    	$this->db->update('tm_bbm');
*/
    }
	public function deletedetail($iarea, $ittb, $iproduct, $iproductgrade, $iproductmotif, $nttbyear)
    {
		$this->db->query("DELETE FROM tm_ttbretur_item WHERE i_ttb='$ittb'
						  and i_product1='$iproduct' and i_product1_motif='$iproductmotif' 
						  and i_product1_grade='$iproductgrade' and n_ttb_year=$nttbyear and i_area='$iarea'");
    }
	function bacaalasan($cari,$num,$offset)
    {
			$this->db->select(" * from tr_alasan_retur where upper(i_alasan_retur) like '%$cari%' or upper(e_alasan_returname)='%$cari%' order by i_alasan_retur", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$iuser)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$this->db->select(" i_area, e_area_name from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacanota($num,$offset,$iarea,$icust,$cari)
    {
			$this->db->select(" i_nota from tm_nota where i_area ='$iarea' and i_customer ='$icust' 
			                    and i_nota like '%$cari%'
			                    order by i_nota", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($iuser,$cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'  || $iuser=='arpusat4'){
			$this->db->select(" i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
	    }else{
			$this->db->select(" * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                        and i_area in ( select i_area from tm_user_area where i_user='$iuser') )", FALSE)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		  $this->db->select(" distinct on (a.i_customer)  * from tr_customer a 
							  left join tr_customer_pkp b on
							  (a.i_customer=b.i_customer) 
							  left join tr_price_group c on
							  (a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
							  left join tr_customer_area d on
							  (a.i_customer=d.i_customer) 
							  left join tr_customer_salesman e on
							  (a.i_customer=e.i_customer)
							  left join tr_customer_discount f on
							  (a.i_customer=f.i_customer) where a.i_area='$iarea'
							  order by a.i_customer",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" distinct on (a.i_customer) * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    /*function bacaproduct($num,$offset,$kdharga,$customer)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif, c.e_product_name as nama, b.v_product_retail as harga
									from tr_product_motif a,tr_product_price b,tr_product c
									where b.i_product=a.i_product and a.i_product_motif='00'
									  and a.i_product=c.i_product 
									  and b.i_price_group='$kdharga'
								    limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }*/

    function bacaproduct($num,$offset,$kdharga,$customer,$nota)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select distinct ON (e.i_product) e.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif, c.e_product_name as nama, b.v_product_retail as harga
									from tr_product_motif a,tr_product_price b,tr_product c, tm_nota d, tm_nota_item e
									where b.i_product=a.i_product and a.i_product_motif='00'
									  and a.i_product=c.i_product 
									  and c.i_product=e.i_product
									  AND d.i_nota = e.i_nota
									  and b.i_price_group='$kdharga'
									  and d.i_customer = '$customer'
									  and e.i_nota = '$nota'
								    limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$kdharga,$customer,$num,$offset)
    {
		if($offset=='')
			$offset=0;
			$query=$this->db->query("  select a.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif,
									c.e_product_name as nama, b.v_product_retail as harga					
									from tr_product_motif a,tr_product_price b,tr_product c
									where b.i_product=a.i_product and a.i_product_motif='00'
									  and a.i_product=c.i_product 
									  and b.i_price_group='$kdharga'
									and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
									limit $num offset $offset",false);
#and e.f_lunas='f'
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
