<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*,
							b.e_customer_name,c.e_area_name
							from tm_spb a, tr_customer b, tr_area c
							where a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.f_spb_cancel='f' 
							and a.f_spb_valid='f' 
							and a.f_spb_siapnotagudang='t'
							and a.f_spb_siapnotasales='f'
							and a.i_store='AA'
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
								 or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name 
							          from tm_spb a, tr_customer b, tr_area c
							          where a.i_customer=b.i_customer 
							          and a.i_area=c.i_area
							          and a.f_spb_cancel='f' 
							          and a.f_spb_valid='f' 
							          and a.f_spb_siapnotagudang='t'
							          and a.f_spb_siapnotasales='f'
							          and a.i_store='AA'
							          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
								        or upper(a.i_spb) like '%$cari%')",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatespb($ispb, $iarea)
    {
		$data = array(
		           'f_spb_siapnotasales' => 't',
               'f_spb_valid' => 't'
		        );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
    function baca($ispb,$iarea)
    {
		$this->db->select("* from tm_spb 
                       left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
			                 inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
			                 inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
			                 inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
			                 inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
			                 where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispb,$iarea,$ipricegroup)
    {
/*
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.n_item_no", false);
*/
    $this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
												a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark ,a.n_item_no, b.e_product_motifname, 
												c.v_product_retail as hrgnew, a.i_product_status
                        from tm_spb_item a, tr_product_motif b, tr_product_price c
				                where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif 
                        and a.i_product=c.i_product and c.i_price_group='$ipricegroup' and a.i_product_grade=c.i_product_grade
				                order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    public function deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
		return TRUE;
    }
#    function insertdetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,$iproductmotif,$eremark,$i,$iproductstatus)
    function updatedetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,$iproductmotif,$eremark,$i)
    {
	    if($eremark=='') $eremark=null;
    	$this->db->set(
    		array(
#					'i_spb'			      => $ispb,
#					'i_area'      		=> $iarea,
#					'i_product'   		=> $iproduct,
#					'i_product_status'=> $iproductstatus,
#					'i_product_grade'	=> $iproductgrade,
#					'i_product_motif'	=> $iproductmotif,
#					'n_order'		      => $norder,
					'n_deliver'   		=> $ndeliver
#					'v_unit_price'		=> $vunitprice,
#					'e_product_name'	=> $eproductname,
#					'e_remark'		    => $eremark,
#          'n_item_no'       => $i,
#          'i_product_status'=> $iproductstatus
    		)
    	);
		  $this->db->where('i_spb', $ispb);
		  $this->db->where('i_area', $iarea);
		  $this->db->where('i_product', $iproduct);
		  $this->db->where('i_product_motif', $iproductmotif);
		  $this->db->where('i_product_grade', $iproductgrade);
    	$this->db->update('tm_spb_item');
    }
    function updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						 $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						 $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$vspbdiscounttotalafter,$vspbafter)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dspbupdate	= $row->c;
      	$data = array(
			  'i_price_group'   	    	=> $ipricegroup,
			  'd_spb_receive'     			=> $dspb,
			  'n_spb_discount1' 		    => $nspbdiscount1,
			  'n_spb_discount2'     		=> $nspbdiscount2,
			  'n_spb_discount3' 		    => $nspbdiscount3,
			  'v_spb_discount1'      		=> $vspbdiscount1,
			  'v_spb_discount2' 		    => $vspbdiscount2,
			  'v_spb_discount3'      		=> $vspbdiscount3,
			  'v_spb_discounttotal'   	=> $vspbdiscounttotal,
			  'v_spb'        				    => $vspb,
			  'v_spb_discounttotalafter'=> $vspbdiscounttotalafter,
			  'v_spb_after'        			=> $vspbafter,
			  'd_spb_update'  			    => $dspbupdate
              );
		  $this->db->where('i_spb', $ispb);
		  $this->db->where('i_area', $iarea);
		  $this->db->update('tm_spb', $data); 
    }
    function bacapricegroup($cari,$num,$offset)
    {
			$this->db->select("	* from tr_price_group
								where upper(i_price_group) like '%$cari%' or upper(e_price_groupname) like '%$cari%'
								order by i_price_group", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
