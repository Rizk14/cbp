<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($istatus)
    {
		$this->db->select('i_product_status, e_product_statusname')->from('tr_product_status')->where('i_product_status', $istatus);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($istatus, $estatusname)
    {
    	$this->db->set(
    		array(
    			'i_product_status' => $istatus,
    			'e_product_statusname' => $estatusname
    		)
    	);
    	
    	$this->db->insert('tr_product_status');
		redirect('status/cform/');
    }
    function update($istatus, $estatusname)
    {
    	$data = array(
               'i_product_status' => $istatus,
               'e_product_statusname' => $estatusname
            );
		$this->db->where('i_product_status', $istatus);
		$this->db->update('tr_product_status', $data); 
		redirect('status/cform/');
    }
	
    public function delete($istatus) 
    {
		$this->db->query('DELETE FROM tr_product_status WHERE i_product_status=\''.$istatus.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select('i_product_status, e_product_statusname from tr_product_status order by i_product_status',false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
