<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacabank($num,$offset)
    {
		  $this->db->select("* from tr_bank order by i_bank", false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function caribank($cari,$num,$offset)
    {
	  $this->db->select("i_bank, e_bank_name, i_coa from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%') 
	                     order by i_bank ", FALSE)->limit($num,$offset);

		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($cari,$num,$offset)
    {
/*		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b, tr_customer_consigment c
                        where a.i_customer=b.i_customer and a.i_customer=c.i_customer and c.i_price_groupco ='14'
		                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
		                    or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
		                    and a.i_area='PB' and not a.i_faktur_komersial is null",FALSE)->limit($num,$offset);*/
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b, tr_customer_consigment c
                        where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
		                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
		                    or upper(a.i_nota) like '%$cari%') and a.n_faktur_komersialprint>0
		                    and a.i_area='PB' and not a.i_faktur_komersial is null",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapajak($inota, $iarea)
    {
		$this->db->select("a.*, b.e_customer_name,b.e_customer_address,b.f_customer_pkp,b.e_customer_city, c.*, d.*, e.*
                        , p.*, o.e_customer_ownername
					              from tm_nota a, tr_customer b left join tr_customer_pkp p on(b.i_customer=p.i_customer)
                        left join tr_customer_owner o on(b.i_customer=o.i_customer), 
                        tr_salesman c, tr_customer_class d, tr_price_group e
					              where a.i_nota = '$inota' and a.i_area='$iarea'
					              and a.i_customer=b.i_customer 
					              and a.i_salesman=c.i_salesman
					              and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
					              and b.i_customer_class=d.i_customer_class",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota,$iarea)
    {
		$this->db->select(" * from tm_nota_item
					inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_nota_item.i_product=tr_product_motif.i_product)
					where i_nota = '$inota' and i_area='$iarea' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacasaldo($tanggal,$icoabank)
  {	    
		$tmp = explode("-", $tanggal);
		$tgl	= $tmp[0];
		$bln	= $tmp[1];
		$thn 	= $tmp[2];
		$dsaldo	= $thn."/".$bln."/".$tgl;
		$dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
		$tmp1 	= explode("-", $dtos,strlen($dtos));
		$th	= $tmp1[0];
		$bl	= $tmp1[1];
		$dt	= $tmp1[2];
		$dtos	= $th.$bl;
		$this->db->select("v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ",false);
#and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.1'
		$query = $this->db->get();
		$saldo=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$saldo=$row->v_saldo_awal;
			}
		}
		$this->db->select("sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy') and b.f_debet='t' 
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL							
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy') and b.f_debet='t' 
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x ",false);
		$query = $this->db->get();
		$kredit=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$kredit=$row->v_bank;
			}
		}
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy')  and b.f_debet='f' 
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy') and b.f_debet='f' 
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x",false);
		$query = $this->db->get();
		$debet=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$debet=$row->v_bank;
			}
		}
		$saldo=$saldo+$debet-$kredit;
		return $saldo;
  }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		     }      
		    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
		    $dateTimeArr=getdate($dateTime);
		    $nosecmin = 0;
		    $min=$dateTimeArr['minutes'];
		    $sec=$dateTimeArr['seconds'];
		    if ($hr==0){$nosecmin += 1;}
		    if ($min==0){$nosecmin += 1;}
		    if ($sec==0){$nosecmin += 1;}
		    if ($nosecmin>2){     
				return(date("Y-m-d",$dateTime));
			} else {     
				return(date("Y-m-d G:i:s",$dateTime));
			}
	}
}
?>
