<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikk) 
    {
		$this->db->query("update tm_kk set f_kk_cancel='t' WHERE i_kk='$ikk' and i_periode='$iperiode' and i_area='$iarea' and f_posting='f' and f_close='f'");
    }
 /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area
                and a.f_kk_cancel='f'
								order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area and a.f_kk_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area and a.f_kk_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
								where (upper(a.i_kk) like '%$cari%')
								and a.i_area=b.i_area and a.f_kk_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kk, a.i_area, a.d_kk, a.i_coa, a.e_description, a.v_kk ,
							a.i_periode, a.f_debet, a.f_posting, a.i_cek,
							b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_kk_cancel='f'
							and a.i_area='$iarea' and
							a.d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kk <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kk, a.i_area, a.d_kk, a.i_coa, a.e_description, a.v_kk , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_kk_cancel='f'
							and a.i_area='$iarea' and
							a.d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kk <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
