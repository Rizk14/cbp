<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ikhp)
    {
		$this->db->select(" * from tm_ikhp a, tr_area b, tr_ikhp_type c 
                        where a.i_ikhp=$ikhp and a.i_area=b.i_area
                        and a.i_ikhp_type=c.i_ikhp_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($icoa)
    {
		$this->db->select(" i_coa from tr_coa where i_coa='$icoa'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro)
    {
    	$this->db->set(
    		array(
				'i_area'					=> $iarea,
				'd_bukti'					=> $dbukti,
				'i_bukti'					=> $ibukti,
				'i_coa'						=> $icoa,
				'i_ikhp_type'			=> $iikhptype,
				'v_terima_tunai'	=> $vterimatunai,
				'v_terima_giro'		=> $vterimagiro,
				'v_keluar_tunai'	=> $vkeluartunai,
				'v_keluar_giro'		=> $vkeluargiro
    		)
    	);
    	
    	$this->db->insert('tm_ikhp');
    }
    function update($iikhp,$iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro)
    {
    	$this->db->set(
    		array(
				'i_area'					=> $iarea,
				'd_bukti'					=> $dbukti,
				'i_bukti'					=> $ibukti,
				'i_coa'						=> $icoa,
				'i_ikhp_type'			=> $iikhptype,
				'v_terima_tunai'	=> $vterimatunai,
				'v_terima_giro'		=> $vterimagiro,
				'v_keluar_tunai'	=> $vkeluartunai,
				'v_keluar_giro'		=> $vkeluargiro
    		)
    	);
    	$this->db->where('i_ikhp',$iikhp);
    	$this->db->update('tm_ikhp');
    }
	function bacauraian($num,$offset)
    {
		$this->db->select("* from tr_ikhp_type order by i_ikhp_type", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariuraian($cari,$num,$offset)
    {
		$this->db->select("* from tr_ikhp_type where upper(e_ikhp_typename) like '%$cari%' order by i_ikhp_type ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
}
?>
