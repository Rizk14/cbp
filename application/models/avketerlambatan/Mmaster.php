<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
  function baca($iperiode,$iarea,$num,$offset)
    {
		  $this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
						              where to_char(d_spb,'yyyymm')='$iperiode' and a.i_customer=b.i_customer
						              and i_area='$iarea' order by a.i_spb ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
  		}
    }
    function bacaperiode($perawal, $perakhir){
/*
    $query = $this->db->query("select COUNT (x.i_customer) as jml_pelunasan, x.i_customer as icustomer, x.ecustomer as ecustomer, x.customer_entry as 
                               customer_entry, x.customer_top as customer_top, x.customer_class as customer_class, sum(x.telat_bayar) as keterlambatan, 
                               (sum(x.telat_bayar)/count(x.i_customer)) as telat_bayar 
                               from ( 
                                  select p.i_customer as i_customer, c.e_customer_name as ecustomer, c.d_customer_entry as customer_entry, 
                                  cc.e_customer_classname as customer_class, n_customer_toplength as customer_top, pi.i_nota as inota, 
                                  pi.d_nota as tgl_nota, n.i_sj as isj, 
                                  n.d_nota + (cast(n_customer_toplength as integer)) as djatuh_tempo, ct.n_toleransi_pusat, ct.n_toleransi_cabang, 

                                  case when substring(n.i_sj,9,2)='00' 
                                  then (n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_pusat as integer)) 
                                  when substring(n.i_sj,9,2)!='00' 
                                  then (n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_cabang as integer)) 
                                  end as jatuhtempo, 

                                  case when p.i_jenis_bayar='03'
                                  then x.d_kum 
                                  when p.i_jenis_bayar='01' 
                                  then g.d_giro_cair
                                  when p.i_jenis_bayar!='01' and p.i_jenis_bayar!='03' 
	                                  then p.d_bukti 
		                                  end as tgl_cair, 
		
			                                  case when p.i_jenis_bayar='03' 
			                                    then 
			                                      case when substring(n.i_sj,9,2)='00' 
				                                    then x.d_kum - ((n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_pusat as integer))) 
			                                    when substring(n.i_sj,9,2)!='00' then x.d_kum - ((n.d_nota + (cast(n_customer_toplength as integer))) + 
			                                    (cast(ct.n_toleransi_cabang as integer))) 
		                                  end 
	                                  when p.i_jenis_bayar='01' 
			                                  then 
				                                  case when substring(n.i_sj,9,2)='00' 
					                                  then g.d_giro_cair - ((n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_pusat as integer))) 	
					                                  when substring(n.i_sj,9,2)!='00' then g.d_giro_cair - ((n.d_nota + (cast(n_customer_toplength as integer))) + 
					                                  (cast(ct.n_toleransi_cabang as integer))) 
					                                  end 
			                                  when p.i_jenis_bayar!='01' and p.i_jenis_bayar!='03' 
			                                  then 
				                                  case when substring(n.i_sj,9,2)='00' 
				                                  then p.d_bukti - ((n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_pusat as integer))) 
			                                  when substring(n.i_sj,9,2)!='00' 
					                                  then p.d_bukti - ((n.d_nota + (cast(n_customer_toplength as integer))) + (cast(ct.n_toleransi_cabang as integer))) 
				                                  end 
	                                  end as telat_bayar, p.d_bukti, c.i_city as icity 
	
	                                  from tr_customer c, tr_city ct, tm_pelunasan_item pi, tm_nota n, tr_customer_class cc, tm_pelunasan p 
	                                  left join tm_kum x on(x.i_kum=p.i_giro and x.i_area=p.i_area and x.n_kum_year=cast(to_char(p.d_bukti,'yyyy') as integer))	
	                                  left join tm_giro g on(g.i_giro=p.i_giro and g.i_area=p.i_area) where p.i_customer = c.i_customer and c.i_city = ct.i_city 
		                                  and substring(p.i_customer,1,2) = ct.i_area and p.i_pelunasan = pi.i_pelunasan and pi.i_nota = n.i_nota  and n.i_customer =
		                                  p.i_customer and (to_char(p.d_bukti,'yyyymm') between '$perawal' and '$perakhir') 
		                                  and c.i_customer_class = cc.i_customer_class and (p.i_jenis_bayar ='03' or p.i_jenis_bayar='01' or p.i_jenis_bayar='02')
                                    			  														
		                                  ) as x 
		                                  group by i_customer, ecustomer, customer_entry, customer_top, customer_class 
		                                  order by x.ecustomer");
*/
    $query = $this->db->query("select COUNT (x.i_customer) as jml_alokasi, x.i_customer as icustomer, x.ecustomer as ecustomer, 
                               x.customer_entry as customer_entry, x.customer_top as customer_top, x.customer_class as customer_class, 
                               sum(x.telat_bayar) as keterlambatan, (sum(x.telat_bayar)/count(x.i_customer)) as telat_bayar from ( 
                               select p.i_customer as i_customer, c.e_customer_name as ecustomer, c.d_customer_entry as customer_entry, 
                               cc.e_customer_classname as customer_class, n_customer_toplength as customer_top, pi.i_nota as inota, 
                               pi.d_nota as tgl_nota, n.i_sj as isj, n.d_nota + (cast(n_customer_toplength as integer)) as djatuh_tempo, 
                               ct.n_toleransi_pusat, ct.n_toleransi_cabang, n.d_nota + (cast(n_customer_toplength as integer)) as 
                               jatuhtempo, p.d_alokasi as tgl_cair, p.d_alokasi - ((n.d_nota + (cast(n_customer_toplength as integer))) ) as 
                               telat_bayar, p.d_alokasi, c.i_city as icity from tr_customer c, tr_city ct, tm_alokasi_item pi, tm_nota n, 
                               tr_customer_class cc, tm_alokasi p left join tm_kum x on(x.i_kum=p.i_giro and x.i_area=p.i_area and 
                               x.n_kum_year=cast(to_char(p.d_alokasi,'yyyy') as integer)) left join tm_giro g on(g.i_giro=p.i_giro and 
                               g.i_area=p.i_area) where p.i_customer = c.i_customer and c.i_city = ct.i_city and substring(p.i_customer,1,2) 
                               = ct.i_area and p.i_alokasi = pi.i_alokasi and pi.i_nota = n.i_nota and n.i_customer = p.i_customer and 
                               (to_char(p.d_alokasi,'yyyymm') between '$perawal' and '$perakhir') and c.i_customer_class = 
                               cc.i_customer_class and p.f_alokasi_cancel='f') as x group by i_customer, ecustomer, customer_entry, 
                               customer_top, customer_class order by x.ecustomer");
      if($query->num_rows()>0){
      return $query->result();
      }
    }
  	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
