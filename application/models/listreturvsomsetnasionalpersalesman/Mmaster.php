<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$interval)
    {
      $sql =" a.kode, a.bln, sum(a.omset) as omset, sum(a.retur) as retur 
              from( 
              SELECT (a.i_area||'.'||b.i_salesman||'.'||b.e_salesman_name) as kode, 
              to_number(to_char(a.d_nota, 'mm'), '99') as bln, sum(a.v_nota_netto) AS omset, 0 as retur 
              FROM tm_nota a, tr_salesman b
              WHERE a.f_nota_cancel='f' AND NOT a.i_nota IS NULL and a.i_salesman=b.i_salesman
              AND (a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') AND a.d_nota <= to_date('$dto', 'dd-mm-yyyy')) 
              GROUP BY a.i_area, b.i_salesman, b.e_salesman_name, to_char(a.d_nota, 'mm') 
              union all 
              SELECT (a.i_area||'.'||b.i_salesman||'.'||b.e_salesman_name) as kode, 
              to_number(to_char(a.d_kn, 'mm'), '99') as bln, 0 as omset, sum(a.v_netto) AS jumlah 
              FROM tm_kn a, tr_salesman b
              WHERE a.f_kn_cancel='f' AND (a.d_kn >= to_date('$dfrom', 'dd-mm-yyyy') AND a.d_kn <= to_date('$dto', 'dd-mm-yyyy'))
              and a.i_salesman=b.i_salesman
              GROUP BY a.i_area, b.i_salesman, b.e_salesman_name, to_char(a.d_kn, 'mm') 
              ) as a 
              group by a.kode, a.bln
              order by a.kode, a.bln";
#and (a.i_area='27' or a.i_area='28')
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
