<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num, $offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							d.e_jenis_bayarname,e.d_dt
							from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							where 
							a.i_area=b.i_area
							and a.i_customer=c.i_customer
							and a.i_jenis_bayar=d.i_jenis_bayar
							and a.i_dt=e.i_dt and a.i_area=e.i_area
							and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%')
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_pelunasan a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							inner join tr_jenis_bayar on(a.i_jenis_bayar=tr_jenis_bayar.i_jenis_bayar)
							inner join tm_dt on(a.i_dt=tm_dt.i_dt and a.i_area=tm_dt.i_area)
							where upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%'
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city
							            from tm_alokasikn a, tr_area b, tr_customer c
							            where 
							            a.i_area=b.i_area and a.i_customer=c.i_customer and (upper(a.i_alokasi) like '%$cari%' 
							            or upper(a.i_customer) like '%$cari%') and a.i_area='$iarea' and a.f_alokasi_cancel='f' and
							            a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
							            ORDER BY a.d_alokasi, a.i_area, a.i_alokasi ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							  d.e_jenis_bayarname,e.d_dt
							  from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							  where 
							  a.i_area=b.i_area
							  and a.i_customer=c.i_customer
							  and a.i_jenis_bayar=d.i_jenis_bayar
							  and a.i_dt=e.i_dt and a.i_area=e.i_area and a.d_dt=e.d_dt
							  and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
							  and a.i_area='$iarea' and a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  ORDER BY a.d_bukti,a.i_area,a.i_pelunasan ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function sisa($iarea,$ipl,$idt){
	$this->db->select(" sum(v_sisa)as sisa
						from tm_dt_item
						where i_area='$iarea'
						and i_dt='$idt'",FALSE);
	$query = $this->db->get();
	foreach($query->result() as $isi){			
		return $isi->sisa;
	}	
   }
  function bacapl($iarea,$ialokasi,$ikn){
    $this->db->select(" a.*, b.e_area_name, c.e_customer_name, '' as e_jenis_bayarname,  '' as d_dt, c.e_customer_address, 
        c.e_customer_city, '' as d_giro_jt, '' as d_giro_cair, d.d_kn
				from tm_alokasikn a
				inner join tr_area b on (a.i_area=b.i_area)
				inner join tr_customer c on (a.i_customer=c.i_customer)
				inner join tm_kn d on (a.i_kn=d.i_kn and a.i_area=d.i_area)
				where a.i_alokasi='$ialokasi' and a.i_area='$iarea' and a.i_kn='$ikn'",FALSE);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }	
  }
  function bacadetailpl($iarea,$ialokasi,$ikn){
		$this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_nota_netto as v_nota, a.e_remark from tm_alokasikn_item a
				    inner join tm_nota b on (a.i_nota=b.i_nota)
					where a.i_alokasi = '$ialokasi' and a.i_area='$iarea' and a.i_kn='$ikn'
					order by a.i_alokasi,a.i_area ",FALSE);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
   function bacagirocek($num,$offset){
	$this->db->select(" * from tr_jenis_bayar order by i_jenis_bayar ",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
   function bacacustomer($idt,$iarea,$num,$offset){
	$this->db->select(" distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
						where a.i_customer=b.i_customer
						and a.v_sisa>0
						and a.i_dt = '$idt' and a.i_area = '$iarea'
						order by a.i_customer ",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
   function bacanota($iarea,$idt,$icustomer,$num,$offset,$group){
	$this->db->select(" a.* from tm_dt_item a, tr_customer b
						          where b.i_customer_group='$group' and a.i_customer=b.i_customer
						          and a.i_dt='$idt'
						          and a.i_area='$iarea' 
						          order by a.i_nota ",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
   function bacagiro($icustomer,$iarea,$num,$offset){
	$this->db->select(" * from tm_giro 
						where i_customer = '$icustomer' 
						and i_area='$iarea'
						and (f_giro_tolak='f' and f_giro_batal='f')
						order by i_giro,i_customer ",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
   function bacakn($icustomer,$iarea,$num,$offset){
	$this->db->select(" * from tm_kn
						where i_customer = '$icustomer' 
						and i_area='$iarea'
						and v_sisa>0
						order by i_kn,i_customer ",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}	
   }
	function bacaku($icustomer,$iarea,$num,$offset,$group){
		$this->db->select(" a.* from tm_kum a, tr_customer b
					where b.i_customer_group='$group' and a.i_customer=b.i_customer
					and a.i_area='$iarea'
					and a.v_sisa>0
					and a.f_close='f'
					order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacaku2($icustomer,$iarea,$num,$offset,$group){
		$this->db->select(" a.* from tm_kum a
					where a.i_customer='$icustomer'
					and a.i_area='$iarea'
					and a.v_sisa>0
					and a.f_close='f'
					order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacapelunasan($icustomer,$iarea,$num,$offset){
		$this->db->select(" i_dt, min(v_jumlah) as v_jumlah, min(v_lebih) as v_lebih, i_area,
						d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2)) as i_pelunasan
					from tm_pelunasan_lebih
					where i_customer = '$icustomer'
					and i_area='$iarea'
					and v_lebih>0
					group by i_dt, d_bukti, i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	
	function updatecek($ecek,$user,$ialokasi,$ikbank,$iarea)
    	{
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    		$data = array(
					'e_cek'		=> $ecek,
					'd_cek'		=> $dentry,
					'i_cek'		=> $user
    				 );
	    	$this->db->where('i_alokasi', $ialokasi);
	    	$this->db->where('i_kbank', $ikbank);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_alokasi', $data);
    	}
########## Posting ###########
	function jenisbayar($ipl,$iarea,$idt)
    {
		$this->db->select(" i_jenis_bayar from tm_pelunasan where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_jenis_bayar;
			}
			return $xxx;
		}
    }
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ipelunasan,$iarea,$egirodescription,$fclose,$dbukti )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjumlah,'$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjumlah','$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function updatepelunasan($ipl,$iarea,$idt)
    {
		$this->db->query("update tm_pelunasan set f_posting='t' where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
########## End of Posting ###########
}
?>
