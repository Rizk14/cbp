<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset,$iuser)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_bapb a
                        left join tr_customer b on (a.i_customer=b.i_customer and a.i_area=b.i_area)
                        inner join tr_area c on (a.i_area=c.i_area)
					              where 
					              (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					              or upper(a.i_bapb) like '%$cari%') and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                        order by a.i_bapb desc",false)->limit($num,$offset);		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ibapb,$area)
    {
		$this->db->select(" tm_bapb.*, tr_customer.e_customer_name, tr_customer.e_customer_address, tr_customer.e_customer_city
											  , tr_area.e_area_name from tm_bapb
							          left join tr_customer on (tm_bapb.i_customer=tr_customer.i_customer)
							          inner join tr_area on (tm_bapb.i_area=tr_area.i_area)
							          where tm_bapb.i_bapb = '$ibapb' and tm_bapb.i_area='$area'
							          order by tm_bapb.i_bapb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_bapb a
                        left join tr_customer b on (a.i_customer=b.i_customer and a.i_area=b.i_area)
                        inner join tr_area c on (a.i_area=c.i_area)
					              where 
					              (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					              or upper(a.i_bapb) like '%$cari%')and (a.i_area='$area1' or a.i_area = '$area2'
				                or a.i_area = '$area3' or a.i_area '$area4' or a.i_area = '$area5')
                        order by a.i_bapb desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
