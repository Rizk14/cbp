<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	public function delete($inota, $ispb, $iarea)
	{
		$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
	}
	function bacasemua($cari, $num, $offset)
	{
		$area1 = $this->session->userdata('i_area');
		$area2 = $this->session->userdata('i_area2');
		$area3 = $this->session->userdata('i_area3');
		$area4 = $this->session->userdata('i_area4');
		$area5 = $this->session->userdata('i_area5');
		$allarea = $this->session->userdata('allarea');
		if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc", false)->limit($num, $offset);
		} else {
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset)
	{
		$area1 = $this->session->userdata('i_area');
		$area2 = $this->session->userdata('i_area2');
		$area3 = $this->session->userdata('i_area3');
		$area4 = $this->session->userdata('i_area4');
		$area5 = $this->session->userdata('i_area5');
		$allarea = $this->session->userdata('allarea');
		if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc", FALSE)->limit($num, $offset);
		} else {
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') {
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num, $offset);
		} else {
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiodeperpages($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function baca($iarea, $dto)
	{
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($iperiode, $num, $offset, $cari)
	{
		/*
		                      union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_pelunasanap a
			                      where a.f_pelunasanap_cancel='f'
			                      and to_char(a.d_bukti,'yyyymm')<'$iperiode'
			                      and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
			                    union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_alokasi_bk a
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')<'$iperiode'
			                      group by a.i_supplier
  			                  union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_alokasi_kb a
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')<'$iperiode'
			                      group by a.i_supplier
*/
		/*$this->db->select(" i_supplier, sum(saldo)+sum(lain) as saldo, sum(nota) as nota, sum(pl) as pl from(
		                      select i_supplier, sum(nota) as saldo, 0 as nota , 0 as pl, 0 as lain from(
			                      select i_supplier, sum(a.v_sisa) as nota, 0 as pl
			                      from tm_dtap a
			                      where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')< '$iperiode' and a.v_sisa>0 
			                      group by a.i_supplier
		                      ) as a group by i_supplier
                      union all
                      select i_supplier, 0 as saldo, sum(nota) as nota, sum(pl) as pl, sum(lain) as lain from(
	                      select i_supplier, sum(nota) as nota, sum(pl) as pl, sum(lain) as lain from(
		                      select a.i_supplier, sum(a.v_netto) as nota, 0 as pl, 0 as lain
		                      from tm_dtap a
		                      where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
		                      group by a.i_supplier
	                      union all

		                      select a.i_supplier, 0 as nota, sum(a.v_jumlah) as pl, 0 as lain
		                      from tm_pelunasanap a
		                      where a.f_pelunasanap_cancel='f'
		                      and to_char(a.d_bukti,'yyyymm')='$iperiode'
		                      and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
		                    union all
			                      select a.i_supplier, 0 as nota, sum(b.v_jumlah) as pl, 0 as lain
			                      from tm_alokasi_bk a, tm_alokasi_bk_item b
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')='$iperiode' and a.i_alokasi=b.i_alokasi 
                            and a.i_kbank=b.i_kbank and a.i_supplier=b.i_supplier 
			                      group by a.i_supplier
			                  union all
                            select a.i_supplier, 0 as nota, sum(b.v_jumlah) as pl, 0 as lain
                            from tm_alokasi_bk a, tm_alokasi_bk_item b
                            where a.f_alokasi_cancel='f' and to_char(a.d_alokasi, 'yyyymm')<'$iperiode' and a.i_alokasi=b.i_alokasi 
                            and a.i_kbank=b.i_kbank and a.i_supplier=b.i_supplier and to_char(b.d_nota, 'yyyymm')= '$iperiode' 
                            group by a.i_supplier
			                  union all
			                      select a.i_supplier, 0 as nota, sum(b.v_jumlah) as pl, 0 as lain
			                      from tm_alokasi_kb a, tm_alokasi_kb_item b
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')='$iperiode' and a.i_alokasi=b.i_alokasi 
                            and a.i_kb=b.i_kb and a.i_supplier=b.i_supplier 
			                      group by a.i_supplier
			                  union all
                            select a.i_supplier, 0 as nota, sum(b.v_jumlah) as pl, 0 as lain
                            from tm_alokasi_kb a, tm_alokasi_kb_item b
                            where a.f_alokasi_cancel='f' and to_char(a.d_alokasi, 'yyyymm')<'$iperiode' and a.i_alokasi=b.i_alokasi 
                            and a.i_kb=b.i_kb and a.i_supplier=b.i_supplier and to_char(b.d_nota, 'yyyymm')= '$iperiode' 
                            group by a.i_supplier

	                      union all
		                      select a.i_supplier, 0 as nota, 0 as pl, sum(b.v_jumlah) as lain
		                      from tm_pelunasanap a, tm_pelunasanap_item b
		                      where a.f_pelunasanap_cancel='f' and a.i_pelunasanap=b.i_pelunasanap and a.d_bukti=b.d_bukti and a.i_area=b.i_area
		                      and to_char(a.d_bukti,'yyyymm')>='$iperiode' and to_char(b.d_dtap,'yyyymm')< '$iperiode'
		                      and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
		                    union all
			                      select a.i_supplier, 0 as nota, 0 as pl, sum(b.v_jumlah) as lain
			                      from tm_alokasi_bk a, tm_alokasi_bk_item b
			                      where a.f_alokasi_cancel='f' and to_char(a.d_alokasi,'yyyymm')>='$iperiode'
 		                        and a.i_alokasi=b.i_alokasi and a.i_kbank=b.i_kbank and a.i_supplier=b.i_supplier
	                          and to_char(b.d_nota,'yyyymm')< '$iperiode'
			                      group by a.i_supplier
			                  union all
			                      select a.i_supplier, 0 as nota, 0  as pl, sum(b.v_jumlah) as lain
			                      from tm_alokasi_kb a, tm_alokasi_kb_item b
			                      where a.f_alokasi_cancel='f' and to_char(a.d_alokasi,'yyyymm')>='$iperiode' 
			                      and a.i_alokasi=b.i_alokasi and a.i_kb=b.i_kb and a.i_supplier=b.i_supplier
			                      and to_char(b.d_nota,'yyyymm')< '$iperiode'
			                      group by a.i_supplier

	                      ) as a group by i_supplier
                      ) as b group by i_supplier
                      ) as c
                      where (saldo<>0 or nota>0 or pl>0)
                      and i_supplier<>'SP106'
                      group by i_supplier
                      order by i_supplier",false);#->limit($num,$offset);*/
		/*
      $this->db->select(" i_supplier, sum(saldo) as saldo, sum(nota) as nota, sum(pl) as pl from(
                          select i_supplier, sum(sisa) as saldo, 0 as nota,0 as pl from(
                          select i_supplier, sum(nota) as nota, sum(pl) as pl, sum(sisa) as sisa from(
                          select i_supplier, sum(a.v_netto) as nota, 0 as pl, sum(a.v_sisa) as sisa 
                          from tm_dtap a
                          where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')< '$iperiode'
                          group by a.i_supplier
                          union all
                          select a.i_supplier, 0 as nota, 0 as pl, sum(v_jumlah) as sisa
                          from tm_pelunasanap a
                          where a.f_pelunasanap_cancel='f'
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
                          ) as a group by i_supplier
                          ) as b group by i_supplier
                          union all
                          select i_supplier, 0 as saldo, sum(nota) as nota, sum(pl) as pl from(
                          select i_supplier, sum(nota) as nota, sum(pl) as pl, sum(sisa) as sisa from(
                          select a.i_supplier, sum(a.v_netto) as nota, 0 as pl, sum(a.v_sisa) as sisa
                          from tm_dtap a
                          where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
                          group by a.i_supplier
                          union all
                          select a.i_supplier, 0 as nota, sum(v_jumlah) as pl, 0 as sisa
                          from tm_pelunasanap a
                          where a.f_pelunasanap_cancel='f'
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
                          ) as a group by i_supplier
                          ) as b group by i_supplier
                          ) as c
                          where saldo<>0 or nota>0 or pl>0
                          group by i_supplier
                          order by i_supplier",false);#->limit($num,$offset);
*/
		$this->db->select(" 	d.*,
								CASE
									WHEN x.f_supplier_pkp = 't' THEN 'PKP'
									ELSE 'NON-PKP'
								END AS status
							FROM
								(
									SELECT
										i_supplier,
										sum(saldo)+ sum(lain) AS saldo,
										sum(dpp) AS dpp,
										sum(ppn) AS ppn,
										sum(nota) AS nota, /* DEBET */
										sum(pl) AS pl,
										sum(rt) AS rt/* KREDIT */
									FROM
										(
											/* BACA SALDO AWAL */
											SELECT
												i_supplier,
												sum(nota) AS saldo,
												0 AS dpp,
												0 AS ppn,
												0 AS nota ,
												0 AS pl,
												0 AS rt,
												0 AS lain
											FROM
												(
													SELECT
														i_supplier,
														sum(a.v_sisa) AS nota,
														0 AS pl
													FROM
														tm_dtap a
													WHERE
														f_dtap_cancel = 'f'
														AND to_char(a.d_dtap, 'yyyymm')< '$iperiode' /* and a.v_sisa>0 */
													GROUP BY
														a.i_supplier 
											) AS a
											GROUP BY
												i_supplier
											UNION ALL
											/* BACA HUTANG DAGANG (DPP & PPN) */
											SELECT
												a.i_supplier,
												0 AS saldo,
												sum(a.dpp) AS dpp,
												sum(a.ppn) AS ppn,
												0 AS nota ,
												0 AS pl,
												0 AS rt,
												0 AS lain
											FROM
												(
													SELECT
														a.i_supplier,
														b.e_supplier_name,
														CASE
															WHEN b.f_supplier_pkp = 't' THEN (sum(a.v_netto) - sum(a.v_ppn))
															ELSE (sum(a.v_netto))
														END AS dpp,
														CASE
															WHEN b.f_supplier_pkp = 't' THEN sum(a.v_ppn)
															ELSE 0
														END AS ppn,
														sum(a.v_netto) AS v_netto
													FROM
														tm_dtap a,
														tr_supplier b
													WHERE
														a.i_supplier = b.i_supplier
														AND to_char(a.d_dtap, 'yyyymm')= '$iperiode'
														AND a.f_dtap_cancel = 'f'
													GROUP BY
														a.i_supplier,
														b.e_supplier_name,
														b.f_supplier_pkp
											) AS a
											GROUP BY
												a.i_supplier,
												a.e_supplier_name
											UNION ALL
											SELECT
												i_supplier,
												0 AS saldo,
												0 AS dpp,
												0 AS ppn,
												sum(nota) AS nota, /* DEBET */
												sum(pl) AS pl,/* KREDIT */
												sum(rt) AS rt, /* RETUR */
												sum(lain) AS lain 
											FROM
												(
													SELECT
														i_supplier,
														sum(nota) AS nota,
														sum(pl) AS pl,
														sum(rt) AS rt,
														sum(lain) AS lain
													FROM
														(
															/* NOTA */
															SELECT
																a.i_supplier,
																sum(a.v_netto) AS nota,
																0 AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_dtap a
															WHERE
																f_dtap_cancel = 'f'
																AND to_char(a.d_dtap, 'yyyymm')= '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															/* PELUNASAN HUTANG */
															SELECT
																a.i_supplier,
																0 AS nota,
																sum(a.v_jumlah) AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_pelunasanap a
															WHERE
																a.f_pelunasanap_cancel = 'f'
																AND to_char(a.d_bukti, 'yyyymm')= '$iperiode'
																AND a.i_jenis_bayar <> '04'
																AND a.i_jenis_bayar <> '05'
															GROUP BY
																a.i_supplier
															UNION ALL
															/* ALOKASI HUTANG DIPERIODE YANG DIPILIH (BANK KELUAR) */
															SELECT
																a.i_supplier,
																0 AS nota,
																sum(b.v_jumlah) AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_alokasi_bk a,
																tm_alokasi_bk_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kbank = b.i_kbank
																AND a.i_supplier = b.i_supplier
															GROUP BY
																a.i_supplier
															UNION ALL
															/* ALOKASI HUTANG DARI PERIODE YANG DIPILIH KEBELAKANG (BANK KELUAR) */
															SELECT
																a.i_supplier,
																0 AS nota,
																sum(b.v_jumlah) AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_alokasi_bk a,
																tm_alokasi_bk_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')<'$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kbank = b.i_kbank
																AND a.i_supplier = b.i_supplier
																AND to_char(b.d_nota, 'yyyymm')= '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															/* ALOKASI HUTANG DARI PERIODE YANG DIPILIH (KAS KELUAR) */
															SELECT
																a.i_supplier,
																0 AS nota,
																sum(b.v_jumlah) AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_alokasi_kb a,
																tm_alokasi_kb_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kb = b.i_kb
																AND a.i_supplier = b.i_supplier
															GROUP BY
																a.i_supplier
															UNION ALL
															/* ALOKASI HUTANG DARI PERIODE YANG DIPILIH KEBELAKANG (KAS KELUAR) */
															SELECT
																a.i_supplier,
																0 AS nota,
																sum(b.v_jumlah) AS pl,
																0 AS rt,
																0 AS lain
															FROM
																tm_alokasi_kb a,
																tm_alokasi_kb_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')<'$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kb = b.i_kb
																AND a.i_supplier = b.i_supplier
																AND to_char(b.d_nota, 'yyyymm')= '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															/* RETUR DARI PERIODE YANG DIPILIH */
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																sum(b.v_jumlah) AS rt,
																0 AS lain
															FROM
																tm_alokasidn a,
																tm_alokasidn_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_dn = b.i_dn
															GROUP BY
																a.i_supplier
															UNION ALL
																/* RETUR DARI PERIODE YANG DIPILIH KEBELAKANG */
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																sum(b.v_jumlah) AS rt,
																0 AS lain
															FROM
																tm_alokasidn a,
																tm_alokasidn_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')<'$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_dn = b.i_dn
																AND to_char(b.d_nota, 'yyyymm')= '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															/* PELUNASAN HUTANG KE SUPP LUAR (BANK) LAIN */
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																0 AS rt,
																sum(b.v_jumlah) AS lain
															FROM
																tm_pelunasanap a,
																tm_pelunasanap_item b
															WHERE
																a.f_pelunasanap_cancel = 'f'
																AND a.i_pelunasanap = b.i_pelunasanap
																AND a.d_bukti = b.d_bukti
																AND a.i_area = b.i_area
																AND to_char(a.d_bukti, 'yyyymm')>= '$iperiode'
																AND to_char(b.d_dtap, 'yyyymm')< '$iperiode'
																AND a.i_jenis_bayar <> '04'
																AND a.i_jenis_bayar <> '05'
															GROUP BY
																a.i_supplier
															UNION ALL
																/* PELUNASAN HUTANG KE SUPP LUAR LAIN */
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																0 AS rt,
																sum(b.v_jumlah) AS lain
															FROM
																tm_alokasi_bk a,
																tm_alokasi_bk_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')>= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kbank = b.i_kbank
																AND a.i_supplier = b.i_supplier
																AND to_char(b.d_nota, 'yyyymm')< '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																0 AS rt,
																sum(b.v_jumlah) AS lain
															FROM
																tm_alokasi_kb a,
																tm_alokasi_kb_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')>= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_kb = b.i_kb
																AND a.i_supplier = b.i_supplier
																AND to_char(b.d_nota, 'yyyymm')< '$iperiode'
															GROUP BY
																a.i_supplier
															UNION ALL
															SELECT
																a.i_supplier,
																0 AS nota,
																0 AS pl,
																0 AS rt,
																sum(b.v_jumlah) AS lain
															FROM
																tm_alokasidn a,
																tm_alokasidn_item b
															WHERE
																a.f_alokasi_cancel = 'f'
																AND to_char(a.d_alokasi, 'yyyymm')>= '$iperiode'
																AND a.i_alokasi = b.i_alokasi
																AND a.i_dn = b.i_dn
																AND to_char(b.d_nota, 'yyyymm')< '$iperiode'
															GROUP BY
																a.i_supplier
													) AS a
													GROUP BY
														i_supplier
											) AS b
											GROUP BY
												i_supplier
									) AS c
									GROUP BY
										i_supplier
							) AS d
							INNER JOIN tr_supplier x ON
								(d.i_supplier = x.i_supplier)
							WHERE
								(saldo <> 0 OR nota>0 OR pl>0)
							ORDER BY
								d.i_supplier ", FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetail($isupplier, $iperiode, $num, $offset)
	{
		$this->db->select(" * from(
								select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_dtap as tglbukti, a.i_dtap as bukti, 
								'' as jenis, '1. pembelian' as keterangan, a.v_netto as debet, 0 as kredit
								from tm_dtap a, tr_supplier c
								where f_dtap_cancel='f' and a.i_supplier=c.i_supplier and to_char(a.d_dtap, 'yyyymm')='$iperiode'
								and a.i_supplier='$isupplier'
								
								union all

								select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_bukti as tglbukti, a.i_pelunasanap as bukti, 
								a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 0 as debet, a.v_jumlah as kredit
								from tm_pelunasanap a, tr_supplier c, tr_jenis_bayar d
								where a.f_pelunasanap_cancel='f' and to_char(d_bukti, 'yyyymm')='$iperiode'
								and a.i_supplier=c.i_supplier and a.i_jenis_bayar=d.i_jenis_bayar 
								and a.i_supplier='$isupplier'
								
								union all

								select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti, 
								'' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
								from tm_alokasi_bk a, tr_supplier c
								where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
								and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
								union all
								select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti,
								'' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
								from tm_alokasi_kb a, tr_supplier c
								where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
								and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
								
								union all

								select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti,
								'' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
								from tm_alokasidn a, tr_supplier c
								where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
								and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
                          	) as a order by supplier, tglbukti, keterangan, bukti", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
