<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$dfrom,$dto,$cari, $num,$offset)
    {
	$this->db->select(" a.d_spb, a.i_cek_cabang, a.d_cek_cabang, a.i_spb, a.i_spb_old, a.i_area, b.e_customer_name, b.e_customer_address, c.e_area_name 
					            from tm_spb a, tr_customer b, tr_area c
					            where 
					            a.i_customer=b.i_customer 
					            and a.i_area=c.i_area
					            and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
					            and a.i_notapprove isnull
					            and a.f_spb_cancel='f'
					            and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
					                 upper(a.i_customer) like '%$cari%')
					            and a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy'))
					            order by a.d_spb,a.i_area,a.i_spb LIMIT ".$num." OFFSET ".$offset,false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iarea,$dfrom,$dto,$cari,$num,$offset)
    {

		$this->db->select(" 	a.d_spb, a.i_cek_cabang, a.d_cek_cabang, a.i_spb, a.i_spb_old, a.i_area, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
    					and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and (upper(a.i_spb) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%' or 
							     upper(a.i_customer) ilike '%$cari%' or upper(a.i_spb_old) ilike '%$cari%')
							and a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy'))
							order by a.d_spb,a.i_area,a.i_spb",false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function baca($ispb, $iarea)
    {
		$this->db->select(" tm_spb.d_spb, tm_spb.i_cek_cabang, tm_spb.d_cek_cabang, tm_spb.i_spb, tm_spb.i_spb_old, tm_spb.i_area,
                        tr_price_group.e_price_groupname, tr_price_group.i_price_group, tm_promo.e_promo_name,
                        tm_spb.i_spb_program, tm_spb.v_spb, tm_spb.n_spb_discount1, tm_spb.n_spb_discount2, tm_spb.n_spb_discount4,
                        tm_spb.n_spb_discount3, tm_spb.v_spb_discount1, tm_spb.v_spb_discount2, tm_spb.v_spb_discount4,
                        tm_spb.v_spb_discount3, tr_customer_area.e_area_name, tr_customer.e_customer_name, tr_customer.e_customer_address,
                        tm_spb.i_customer, tm_spb.i_spb_po, tm_spb.f_spb_consigment, tm_spb.n_spb_toplength, tm_spb.f_spb_stockdaerah, 
                        tm_spb.f_spb_plusppn, tm_spb.f_spb_plusdiscount, tm_spb.f_spb_pkp, tm_spb.v_spb_discounttotal,tm_spb.i_salesman,
                        tr_salesman.e_salesman_name, tm_spb.v_spb_after, tm_spb.e_remark1, tm_spb.e_customer_pkpnpwp                    
           from tm_spb 
				   left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	function bacadetail($ispb, $iarea)
    {
		$this->db->select("  a.i_product, a.e_product_name, b.e_product_motifname, a.v_unit_price, a.n_order, a.i_product_motif 
                from tm_spb_item a, tr_product_motif b
					   		where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product 
							  and a.i_product_motif=b.i_product_motif
				   			order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function approve($ispb,$iarea,$ecek,$user)
    {
		
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'e_cek_cabang'  => $ecek,
					'd_cek_cabang'	=> $dentry,
					'i_cek_cabang'	=> $user
    				 );
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
    }
	function notapprove($ispb,$iarea,$eapprove,$user)
    {
		/*
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'e_notapprove'		=> $eapprove,
					'd_notapprove'		=> $dentry,
					'i_notapprove'		=> $user
    				 );
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
		*/ 
    }

   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function updateheader($ispb, $iarea,$ecek1,$user)
    {
      $daerah='';
      $this->db->select(" * from tm_spb where i_spb = '$ispb' and i_area='$iarea'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $tmp){
          $daerah=$tmp->f_spb_stockdaerah;
        }
        $query 		= $this->db->query("SELECT current_timestamp as c");
	      $row   		= $query->row();
	      $dspbupdate	= $row->c;
        if($daerah=='f'){
        	$data = array(
		      'e_cek_cabang' => $ecek1,
		      'd_cek_cabang' => $dspbupdate,
		      'i_cek_cabang' => $user
                );
		      $this->db->where('i_spb', $ispb);
		      $this->db->where('i_area', $iarea);
		      $this->db->update('tm_spb', $data); 
        }elseif($daerah=='t'){
        	$data = array(
		      'e_cek_cabang' => $ecek1,
		      'd_cek_cabang' => $dspbupdate,
		      'i_cek_cabang' => $user,
          'f_spb_valid'  => 't'
                );
		      $this->db->where('i_spb', $ispb);
		      $this->db->where('i_area', $iarea);
		      $this->db->update('tm_spb', $data); 
        }
      }
    }
    public function deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
		return TRUE;
    }
    function insertdetail($ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark,$i)
    {
    	if($eremark=='') $eremark=null;
    	$this->db->set(
    		array(
					'i_spb'			      => $ispb,
					'i_area'      		=> $iarea,
					'i_product'   		=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_order'     		=> $norder,
					'n_deliver'   		=> 0,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'e_remark'		    => $eremark,
          'n_item_no'       => $i
    		)
    	);
    	$this->db->insert('tm_spb_item');
    }
}
?>
