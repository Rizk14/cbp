<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($dfrom,$dto)
  {
    $tmp=explode('-',$dfrom);
    $th=$tmp[2];
    $this->db->select(" * from (
                        select * from crosstab
                        ('SELECT (a.i_product||e.i_product_group) as kode, a.i_product, d.i_supplier, i.e_supplier_name, e.i_product_group, 
                        a.e_product_name, b.e_area_name, g.e_product_groupname, 
                        ''BARU'' as kategori, a.i_area, sum(a.n_deliver) AS jumlah
                        FROM tm_nota c, tm_nota_item a, tr_area b, tr_product d, tr_product_type e, tr_product_group g, tm_spb h, 
                        tr_supplier i 
                        WHERE c.f_nota_cancel = false AND c.i_sj = a.i_sj AND c.i_area = a.i_area AND c.i_area = b.i_area 
                        AND c.i_spb=h.i_spb and c.i_area=h.i_area and h.f_spb_consigment=''f'' and d.i_supplier=i.i_supplier
                        AND NOT c.i_nota IS NULL AND a.i_product = d.i_product AND d.i_product_type = e.i_product_type 
                        AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
                        AND e.i_product_group = g.i_product_group 
                        AND ((to_char(d.d_product_register,''yyyy'')=''$th'') or (d.d_product_register isnull 
                        AND to_char(d.d_product_register,''yyyy'')=''$th''))
                        GROUP BY a.i_area, e.i_product_group, g.e_product_groupname, a.i_product, a.e_product_name, d.i_supplier, 
                        e_supplier_name, b.e_area_name 
                        order by g.e_product_groupname, a.i_product','select i_area from tr_area where f_area_real=''t'' order by i_area')
                        AS (kode text, i_product text, i_supplier text, e_supplier_name text, i_product_group text, e_product_name text, 
                        e_area_name text, e_product_groupname text, kategori text,
                        area_00 integer, area_01 integer, area_02 integer, area_06 integer, area_07 integer, area_08 integer, 
                        area_09 integer, area_10 integer, area_11 integer, area_12 integer, area_13 integer, area_16 integer, 
                        area_17 integer, area_18 integer, area_19 integer, area_20 integer, area_21 integer, area_22 integer, 
                        area_23 integer, area_25 integer, area_26 integer, area_27 integer, area_31 integer, area_32 integer, 
                        area_33 integer, area_40 integer, area_41 integer, area_43 integer, area_45 integer, area_XX integer)

                        union all

                        select * from crosstab
                        ('SELECT (a.i_product||e.i_product_group) as kode, a.i_product, d.i_supplier, i.e_supplier_name, e.i_product_group, 
                        a.e_product_name, b.e_area_name, g.e_product_groupname, 
                        ''LAMA'' as kategori, a.i_area, sum(a.n_deliver) AS jumlah
                        FROM tm_nota c, tm_nota_item a, tr_area b, tr_product d, tr_product_type e, tr_product_group g, tm_spb h, 
                        tr_supplier i 
                        WHERE c.f_nota_cancel = false AND c.i_sj = a.i_sj AND c.i_area = a.i_area AND c.i_area = b.i_area 
                        AND c.i_spb=h.i_spb and c.i_area=h.i_area and h.f_spb_consigment=''f''
                        AND NOT c.i_nota IS NULL AND a.i_product = d.i_product AND d.i_product_type = e.i_product_type 
                        AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
                        AND e.i_product_group = g.i_product_group and d.i_supplier=i.i_supplier
                        AND to_char(d.d_product_register,''yyyy'')<>''$th''
                        GROUP BY a.i_area, e.i_product_group, g.e_product_groupname, a.i_product, a.e_product_name,d.i_supplier, 
                        e_supplier_name,  b.e_area_name 
                        order by g.e_product_groupname, a.i_product','select i_area from tr_area where f_area_real=''t'' order by i_area')
                        AS (kode text, i_product text, i_supplier text, e_supplier_name text, i_product_group text, e_product_name text, 
                        e_area_name text, e_product_groupname text, kategori text,
                        area_00 integer, area_01 integer, area_02 integer, area_06 integer, area_07 integer, area_08 integer, 
                        area_09 integer, area_10 integer, area_11 integer, area_12 integer, area_13 integer, area_16 integer, 
                        area_17 integer, area_18 integer, area_19 integer, area_20 integer, area_21 integer, area_22 integer, 
                        area_23 integer, area_25 integer, area_26 integer, area_27 integer, area_31 integer, area_32 integer, 
                        area_33 integer, area_40 integer, area_41 integer, area_43 integer, area_45 integer, area_XX integer)

                        union all

                        select * from crosstab
                        ('SELECT (a.i_product||e.i_product_group) as kode, a.i_product, d.i_supplier, i.e_supplier_name, e.i_product_group, 
                        a.e_product_name, b.e_area_name, ''Modern Outlet'' as e_product_groupname, 
                        ''BARU'' as kategori, a.i_area, sum(a.n_deliver) AS jumlah
                        FROM tm_nota c, tm_nota_item a, tr_area b, tr_product d, tr_product_type e, tr_product_group g, tm_spb h, 
                        tr_supplier i 
                        WHERE c.f_nota_cancel = false AND c.i_sj = a.i_sj AND c.i_area = a.i_area AND c.i_area = b.i_area 
                        AND c.i_spb=h.i_spb and c.i_area=h.i_area and h.f_spb_consigment=''t''
                        AND NOT c.i_nota IS NULL AND a.i_product = d.i_product AND d.i_product_type = e.i_product_type 
                        AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
                        AND e.i_product_group = g.i_product_group and d.i_supplier=i.i_supplier
                        AND ((to_char(d.d_product_register,''yyyy'')=''$th'') or (d.d_product_register isnull 
                        AND to_char(d.d_product_register,''yyyy'')=''$th''))
                        GROUP BY a.i_area, e.i_product_group, g.e_product_groupname, a.i_product, a.e_product_name, d.i_supplier, 
                        e_supplier_name, b.e_area_name 
                        order by g.e_product_groupname, a.i_product','select i_area from tr_area where f_area_real=''t'' order by i_area')
                        AS (kode text, i_product text, i_supplier text, e_supplier_name text, i_product_group text, e_product_name text, 
                        e_area_name text, e_product_groupname text, kategori text, 
                        area_00 integer, area_01 integer, area_02 integer, area_06 integer, area_07 integer, area_08 integer, 
                        area_09 integer, area_10 integer, area_11 integer, area_12 integer, area_13 integer, area_16 integer, 
                        area_17 integer, area_18 integer, area_19 integer, area_20 integer, area_21 integer, area_22 integer, 
                        area_23 integer, area_25 integer, area_26 integer, area_27 integer, area_31 integer, area_32 integer, 
                        area_33 integer, area_40 integer, area_41 integer, area_43 integer, area_45 integer, area_XX integer)

                        union all

                        select * from crosstab
                        ('SELECT (a.i_product||e.i_product_group) as kode, a.i_product, d.i_supplier, i.e_supplier_name, e.i_product_group, 
                        a.e_product_name, b.e_area_name, ''Modern Outlet'' as e_product_groupname, 
                        ''LAMA'' as kategori, a.i_area, sum(a.n_deliver) AS jumlah
                        FROM tm_nota c, tm_nota_item a, tr_area b, tr_product d, tr_product_type e, tr_product_group g, tm_spb h, 
                        tr_supplier i 
                        WHERE c.f_nota_cancel = false AND c.i_sj = a.i_sj AND c.i_area = a.i_area AND c.i_area = b.i_area 
                        AND c.i_spb=h.i_spb and c.i_area=h.i_area and h.f_spb_consigment=''t''
                        AND NOT c.i_nota IS NULL AND a.i_product = d.i_product AND d.i_product_type = e.i_product_type 
                        AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
                        AND e.i_product_group = g.i_product_group and d.i_supplier=i.i_supplier
                        AND to_char(d.d_product_register,''yyyy'')<>''$th''
                        GROUP BY a.i_area, e.i_product_group, g.e_product_groupname, a.i_product, a.e_product_name, d.i_supplier, 
                        e_supplier_name, b.e_area_name 
                        order by g.e_product_groupname, a.i_product','select i_area from tr_area where f_area_real=''t'' order by i_area')
                        AS (kode text, i_product text, i_supplier text, e_supplier_name text, i_product_group text, e_product_name text, 
                        e_area_name text, e_product_groupname text, kategori text,
                        area_00 integer, area_01 integer, area_02 integer, area_06 integer, area_07 integer, area_08 integer, 
                        area_09 integer, area_10 integer, area_11 integer, area_12 integer, area_13 integer, area_16 integer, 
                        area_17 integer, area_18 integer, area_19 integer, area_20 integer, area_21 integer, area_22 integer, 
                        area_23 integer, area_25 integer, area_26 integer, area_27 integer, area_31 integer, area_32 integer, 
                        area_33 integer, area_40 integer, area_41 integer, area_43 integer, area_45 integer, area_XX integer)
                        ) as a
                        order by a.e_product_groupname, a.kategori, a.i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya()
  {
		$this->db->select(" i_area, e_area_name from tr_area where f_area_real='t' order by i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($iperiode)
  {
		$this->db->select(" distinct i_product, e_product_name, e_product_groupname from vpenjualanperproduk
                        where i_periode='$iperiode'
                        order by e_product_groupname, i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
