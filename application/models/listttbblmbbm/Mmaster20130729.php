<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($num,$offset,$cari)
    {
      $allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($allarea=='t' || $area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select("	a.i_area, a.i_ttb, a.d_ttb, a.i_customer, b.e_customer_name, a.d_receive1, a.f_ttb_cancel
                            from tm_ttbretur a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            and a.i_ttb not in (select i_refference_document from tm_bbm where a.i_ttb=tm_bbm.i_refference_document)
                            and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
                            order by a.d_ttb, a.i_area, a.i_ttb",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.i_area, a.i_ttb, a.d_ttb, a.i_customer, b.e_customer_name, a.d_receive1, a.f_ttb_cancel
                            from tm_ttbretur a, tr_customer b
                            where a.i_customer=b.i_customer and a.i_area=b.i_area
                            and a.i_ttb not in (select i_refference_document from tm_bbm where a.i_ttb=tm_bbm.i_refference_document)
                            and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            order by a.d_ttb, a.i_area, a.i_ttb",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
