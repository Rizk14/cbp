<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacado($num,$offset, $dfrom, $dto, $isupplier)
    {
		$tahun = date('Y');
		
    	if($isupplier == 'AS'){
		  $this->db->select(" distinct f.i_dtap, f.d_dtap, a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, 
			                      e.e_supplier_name, g.f_dtap_cancel, a.i_supplier, d.i_store, d.i_store_location, d.d_spmb from tr_supplier e,
                            tm_opfc b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
                            left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area),
                            tm_dofc a left join tm_dtap_item f on(a.i_do=f.i_do)
                            left join tm_dtap g on (f.i_dtap=g.i_dtap and f.i_area=g.i_area and f.i_supplier=g.i_supplier 
                            and g.f_dtap_cancel='f')
                            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
                            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier
                            and a.i_area <> '00'
                            and not d.i_spmb isnull
                             and a.i_do not in(select substr(e_remark, 12, 15) from tm_sjp_item where d_sjp >= to_date('01-01-$tahun','dd-mm-yyyy') and e_remark like '%Dari DO FC%' )
				                    order by a.i_supplier ", false)->limit($num,$offset);
    	}else{
		  $this->db->select(" distinct f.i_dtap, f.d_dtap, a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, 
			                      e.e_supplier_name, g.f_dtap_cancel, a.i_supplier, d.i_store, d.i_store_location, d.d_spmb from tr_supplier e,
                            tm_opfc b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
                            left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area),
                            tm_dofc a left join tm_dtap_item f on(a.i_do=f.i_do)
                            left join tm_dtap g on (f.i_dtap=g.i_dtap and f.i_area=g.i_area and f.i_supplier=g.i_supplier 
                            and g.f_dtap_cancel='f')
                            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
                            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and a.i_supplier='$isupplier'
                            and a.i_area <> '00'
                            and not d.i_spmb isnull
                           and a.i_do not in(select substr(e_remark, 12, 15) from tm_sjp_item where d_sjp >= to_date('01-01-$tahun','dd-mm-yyyy') and e_remark like '%Dari DO FC%' )
				                    order by a.i_supplier ", false)->limit($num,$offset);
    	}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier 
				     where upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%' order by i_supplier", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaheader($i_do, $i_supplier){
		  $this->db->select(" a.*, b.e_supplier_name from tm_do a, tr_supplier b where  a.i_supplier = b.i_supplier and a.f_do_export = 'f' and a.i_supplier = '$i_supplier' and i_do = '$i_do' order by a.i_do asc", FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($i_do, $i_supplier){
		  $this->db->select(" * from tm_do_item where i_do = '$i_do' and i_supplier = '$i_supplier'", FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function runningnumbersj($iarea,$thbl)
    {
		  $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SJP'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nosj
                            where i_modul='SJP'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
        
			  $nosj  ="SJP-".$thbl."-".$iarea.$nosj;
			  return $nosj;
		  }else{
			  $nosj  ="0001";
			  $nosj  ="SJP-".$thbl."-".$iarea.$nosj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SJP','$iarea','$asal',1)");
			  return $nosj;
		  }
    }
    function insertsjheader($ispmb,$dspmb,$isj,$dsj,$iarea,$vspbnetto,$isjold)
    {
    	    	echo $ispmb;
    	die();
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjentry	= $row->c;
    	$this->db->set(
    		array(
				'i_sjp'				=> $isj,
				'i_sjp_old'		=> $isjold,
				'i_spmb'			=> $ispmb,
				'd_spmb'			=> $dspmb,
				'd_sjp'				=> $dsj,
				'i_area'		  => $iarea,
				'v_sjp'		    => $vspbnetto,
				'd_sjp_entry'	=> $dsjentry,
				'f_sjp_cancel'=> 'f'
    		)
    	);
    	
    	$this->db->insert('tm_sjp');
    }
}
?>
