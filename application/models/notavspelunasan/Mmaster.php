<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, a.i_salesman, sum(a.v_nota_gross) as nota,
                          sum(a.v_nota_netto) as bersih, sum(d.v_jumlah) as bayar
                          from tr_customer b, tm_spb c, tm_nota a
                          left join tm_pelunasan_item d on (a.i_nota=d.i_nota)
                          left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_area=e.i_area and e.f_pelunasan_cancel='f' 
			                                            and e.f_giro_tolak='f' and e.f_giro_batal='f')
                          where a.f_nota_cancel='f' and to_char(a.d_nota,'yyyymm')='$iperiode' and not a.i_nota isnull and a.i_area='$iarea'
                          and a.i_customer=b.i_customer
                          and a.i_spb=c.i_spb and a.i_area=c.i_area and c.f_spb_consigment='t'
                          group by a.i_customer, b.e_customer_name, b.e_customer_address, a.i_salesman
                          order by a.i_customer ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
