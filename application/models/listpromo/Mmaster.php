<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ipromo) 
    {
			$this->db->query('DELETE FROM tm_promo WHERE i_promo=\''.$ipromo.'\'');
			$this->db->query('DELETE FROM tm_promo_item WHERE i_promo=\''.$ipromo.'\'');
			$this->db->query('DELETE FROM tm_promo_customer where i_promo =\''.$ipromo.'\'');
			$this->db->query('DELETE FROM tm_promo_customergroup where i_promo =\''.$ipromo.'\'');
			$this->db->query('DELETE FROM tm_promo_area where i_promo =\''.$ipromo.'\'');
    }
    function bacasemua($cari, $num,$offset)
    {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1!='00'){
			$query = $this->db->select(" * from (
																	select * from tm_promo where 
																	f_all_area='t'
																	and (upper(e_promo_name) like '%$cari%' or upper(i_promo) like '%$cari%')
																	union all
																	select a.* from tm_promo a
																	inner join tm_promo_area b on (a.i_promo=b.i_promo and a.i_promo_type=b.i_promo_type 
																	and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5')
																	and (upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'))
																	where a.f_all_area='f')
																	as x
																	order by x.i_promo desc
																	",false)->limit($num,$offset);
      }else{
			$query = $this->db->select("a.* from tm_promo a
																	where upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'
																	order by a.i_promo desc
																	",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->select(" * from (
																	select * from tm_promo where 
																	f_all_area='t'
																	and (upper(e_promo_name) like '%$cari%' or upper(i_promo) like '%$cari%')
																	union all
																	select a.* from tm_promo a
																	inner join tm_promo_area b on (a.i_promo=b.i_promo 
																	and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5')
																	and (upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'))
																	where a.f_all_area='f')
																	as x
																	order by x.i_promo desc
																	",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
