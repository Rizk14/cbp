<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproduct,$iproductgrade,$ipricegroup)
    {
		$this->db->select(" a.*, b.e_product_name
                        from tr_product_priceco a, tr_product b
                        where a.i_product=b.i_product
                        and a.i_product = '$iproduct' 
                        and a.i_product_grade = '$iproductgrade'
                        and a.i_price_group='$ipricegroup'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacakode()
    {
		  $this->db->select(" * from tr_price_group order by i_price_group", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function insert($iproduct,$ipricegroup,$iproductgrade,$vproductretail,$nmargin,$ipricegroupco)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
			$this->db->query("insert into tr_product_priceco (i_product, i_product_grade, i_price_group,
				        			  v_product_retail, n_margin, i_price_groupco, d_entry) values
				          		  ('$iproduct','$iproductgrade','$ipricegroup',$vproductretail,$nmargin,'$ipricegroupco','$dentry')");
#  		redirect('productpricemanual/cform/');
    }

    function update($iproduct,$ipricegroup,$iproductgrade,$vproductretail,$nmargin,$ipricegroupco)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dupdate= $row->c;
      $this->db->query("update tr_product_priceco set  
                        v_product_retail = $vproductretail,
                        n_margin = $nmargin, d_update='$dupdate'
                        where i_product = '$iproduct'
                        and i_product_grade = '$iproductgrade'
                        and i_price_group = '$ipricegroup'");
#  		redirect('productpricemanual/cform/');
    }
	
    public function delete($iproduct,$iproductgrade,$ipricegroup) 
    {
		  $this->db->query('DELETE FROM tr_product_priceco WHERE i_product=\''.$iproduct.'\' and i_product_grade=\''.$iproductgrade.'\'
                        and i_price_group=\''.$ipricegroup.'\'');
		  return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_product_name
                        from tr_product_priceco a, tr_product b
                        where a.i_product=b.i_product
                        and (upper(a.i_product) like '%$cari%' 
                        or upper(a.e_product_name) = '%$cari%')", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cari($cari,$num,$offset)
    {
     $this->db->select("a.*, b.e_product_name
                        from tr_product_priceco a, tr_product b
                        where a.i_product=b.i_product
                        and (upper(a.i_product) like '%$cari%' 
                        or upper(a.e_product_name) = '%$cari%')", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaproductgrade($num,$offset)
    {
		$this->db->select("i_product_grade, e_product_gradename from tr_product_grade order by i_product_grade",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproductgrade($cari,$num,$offset)
    {
		$this->db->select("i_product_grade, e_product_gradename from tr_product_grade where upper(e_product_gradename) like '%$cari%' or upper(i_product_grade) like '%$cari%' order by i_product_grade", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($cari,$num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product where (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%') order by i_product",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproduct($cari,$num,$offset)
    {
		$this->db->select("i_product, e_product_name from tr_product where upper(e_product_name) like '%$cari%' or upper(i_product) like '%$cari%' order by i_product", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapricegroupco($num,$offset,$cari)
    {
		$this->db->select("i_price_groupco, e_price_groupconame, n_margin from tr_price_groupco
		                   where (upper(i_price_groupco) like '%$cari%' or upper(e_price_groupconame) like '%$cari%') 
		                   order by i_price_groupco",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cekco($iproduct,$ipricegroup,$iproductgrade)
		{
		  $ada=true;
			$query=$this->db->query(" select i_product from tr_product_priceco
			                          where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_price_group='$ipricegroup'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$ada=false;
				}
			}
			return $ada;
		}
    function cekharga($iproduct,$ipricegroup,$iproductgrade)
		{
		  $ada=true;
			$query=$this->db->query(" select i_product from tr_product_price 
			                          where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_price_group='$ipricegroup'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$ada=false;
				}
			}
			return $ada;
		}
		function insertnet($iproduct,$ipricegroup,$iproductgrade,$eproductname,$vproductretail,$nmargin){
		  $que=$this->db->query(" select v_product_mill from tr_product_price 
		                            where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_price_group='00'",false);
		  if ($que->num_rows() > 0){
			  foreach($que->result() as $qq){
          $hrg=$qq->v_product_mill;
        }
		    $quer = $this->db->query("SELECT current_timestamp as c");
		    $row   = $quer->row();
		    $dentry= $row->c;
			  $this->db->query("insert into tr_product_price (i_product, i_product_grade, i_price_group, e_product_name, v_product_retail, 
			                    v_product_mill, d_product_priceentry, n_product_margin) values
				            		  ('$iproduct','$iproductgrade','$ipricegroup','$eproductname',$vproductretail,
				            		  $hrg,'$dentry',$nmargin)");
      }
		}
		function updatenet($iproduct,$ipricegroup,$iproductgrade,$eproductname,$vproductretail,$nmargin){
	    $quer = $this->db->query("SELECT current_timestamp as c");
	    $row   = $quer->row();
	    $dentry= $row->c;
		  $this->db->query("update tr_product_price set v_product_retail=$vproductretail, d_product_priceupdate='$dentry', 
		                    n_product_margin=$nmargin
		                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_price_group='$ipricegroup'");
    }
}
?>
