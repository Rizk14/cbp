<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer
							and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_nota desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_spb a, tr_customer b
							where a.i_customer=b.i_customer and not a.i_approve1 isnull
							and not a.i_approve2 isnull and f_spb_siapnota = 't'
							and f_spb_cancel = 'f' and not i_store isnull and a.i_nota isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
								 or upper(a.i_spb) like '%$cari%') ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacattb($iarea,$ittb,$tahun)
    {
		$this->db->select(" a.*
							,b.i_bbm 
							,c.* 
							,d.* 
							,e.e_salesman_name
							,f.* 
							,g.*
							from tm_ttbretur a 
							left join tm_bbm b on (a.i_ttb = b.i_refference_document)
							inner join tr_area c on (a.i_area = c.i_area)
							inner join tr_customer d on (a.i_customer = d.i_customer)
							inner join tr_salesman e on (a.i_salesman = e.i_salesman)
							left join tr_customer_pkp f on (a.i_customer = f.i_customer)
							inner join tr_price_group g on (g.n_line=d.i_price_group)
							where a.i_area = '$iarea' 
							and a.i_ttb = '$ittb' 
							and a.n_ttb_year=$tahun 
							",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacattbdetail($iarea,$ittb,$tahun)
    {
		$this->db->select("	* from tm_ttbretur_item
						   	inner join tr_product on (tr_product.i_product=tm_ttbretur_item.i_product1)
						   	inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_ttbretur_item.i_product1_motif
														and tr_product_motif.i_product=tm_ttbretur_item.i_product1)
							inner join tm_nota_item on (tm_nota_item.i_product_motif = tm_ttbretur_item.i_product1_motif
													and tm_nota_item.i_product = tm_ttbretur_item.i_product1
													and tm_nota_item.i_product_grade = tm_ttbretur_item.i_product1_grade
													and tm_nota_item.i_nota = tm_ttbretur_item.i_nota)
						   	where tm_ttbretur_item.i_ttb = '$ittb' and tm_ttbretur_item.i_area ='$iarea' and tm_ttbretur_item.n_ttb_year=$tahun
						   	order by tm_ttbretur_item.i_product1", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
	function baca($inota)
    {
		$this->db->select(" * from tm_nota 
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
				   where i_nota = '$inota'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetail($inota)
    {
		$this->db->select("* from tm_nota_item
						   inner join tr_product_motif on (tr_product_motif.i_product_motif=tm_nota_item.i_product_motif
													   and tr_product_motif.i_product=tm_nota_item.i_product)
						   where tm_nota_item.i_nota = '$inota'
						   order by tm_nota_item.i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function insertheader(	$iarea,$ittb,$dttb,$icustomer,$isalesman,$nttbdiscount1,$nttbdiscount2,
							$nttbdiscount3,$vttbdiscount1,$vttbdiscount2,$vttbdiscount3,$fttbpkp,$fttbplusppn,
							$fttbplusdiscount,$vttbgross,$vttbdiscounttotal,$vttbnetto,$ettbremark,$fttbcancel,
							$dreceive1,$tahun	)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_area'				=> $iarea,
					'i_ttb'					=> $ittb,
					'd_ttb'					=> $dttb,
					'i_customer'			=> $icustomer,
					'i_salesman'			=> $isalesman,
					'n_ttb_discount1'		=> $nttbdiscount1,
					'n_ttb_discount2'		=> $nttbdiscount2,
					'n_ttb_discount3'		=> $nttbdiscount3,
					'v_ttb_discount1'		=> $vttbdiscount1,
					'v_ttb_discount2'		=> $vttbdiscount2,
					'v_ttb_discount3'		=> $vttbdiscount3,
					'f_ttb_pkp'				=> $fttbpkp,
					'f_ttb_plusppn'			=> $fttbplusppn,
					'f_ttb_plusdiscount'	=> $fttbplusdiscount,
					'v_ttb_gross'			=> $vttbgross,
					'v_ttb_discounttotal'	=> $vttbdiscounttotal,
					'v_ttb_netto'			=> $vttbnetto,
					'v_ttb_sisa'			=> $vttbnetto,
					'e_ttb_remark'			=> $ettbremark,
					'f_ttb_cancel'			=> $fttbcancel,
					'd_receive1'			=> $dreceive1,
					'd_entry'				=> $dentry,
					'n_ttb_year'			=> $tahun
    		)
    	);
    	$this->db->insert('tm_ttbretur');
    }
	function insertdetail($iarea,$ittb,$dttb,$inota,$dnota,$iproduct,$iproductgrade,$iproductmotif,$nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver)
    {
    	$this->db->set(
    		array(
					'i_area'			=> $iarea,
					'i_ttb'				=> $ittb,
					'i_nota'			=> $inota,
					'd_ttb'				=> $dttb,
					'd_nota'			=> $dnota,
					'i_product1'		=> $iproduct,
					'i_product1_grade'	=> $iproductgrade,
					'i_product1_motif'	=> $iproductmotif,
					'n_quantity'		=> $nquantity,
					'v_unit_price'		=> $vunitprice,
					'e_ttb_remark'		=> $ettbremark,
					'n_ttb_year'		=> $tahun
    		)
    	);
    	
    	$this->db->insert('tm_ttbretur_item');
    }
	function updateheader(	$ittb,$iarea,$tahun,$dttb,$dreceive1,$eremark,
							$nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
							$vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
							$vttbgross)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
			'd_ttb'						=> $dttb,
			'd_receive1'			=> $dreceive1,
			'e_ttb_remark'		=> $eremark,
			'd_update'				=> $dupdate,
			'n_ttb_discount1'	=> $nttbdiscount1,
			'n_ttb_discount2'	=> $nttbdiscount2,
			'n_ttb_discount3'	=> $nttbdiscount3,
			'v_ttb_discount1'	=> $vttbdiscount1,
			'v_ttb_discount2'	=> $vttbdiscount2,
			'v_ttb_discount3'	=> $vttbdiscount3,
			'v_ttb_gross'			=> $vttbgross,
			'v_ttb_discounttotal'	=> $vttbdiscounttotal,
			'v_ttb_netto'			=> $vttbnetto,
			'v_ttb_sisa'			=> $vttbnetto,
			'f_ttb_cancel'		=> 'f'
    		)
    	);
		$this->db->where('i_ttb',$ittb);
		$this->db->where('i_area',$iarea);
		$this->db->where('n_ttb_year',$tahun);
    	$this->db->update('tm_ttbretur');
    }
	public function deletedetail($iarea, $ittb, $inota, $iproduct, $iproductgrade, $iproductmotif, $nttbyear)
    {
		$this->db->query("DELETE FROM tm_ttbretur_item WHERE i_ttb='$ittb'
						  and i_product1='$iproduct' and i_product1_motif='$iproductmotif' 
						  and i_product1_grade='$iproductgrade' and n_ttb_year=$nttbyear
						  and i_nota='$inota' and i_area='$iarea'");
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$this->db->select(" i_area, e_area_name from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select(" i_area, e_area_name from tr_area 
				where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'
				order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
			$this->db->select(" i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
	    }else{
			$this->db->select(" i_area, e_area_name from tr_area where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5') and
				upper(e_area_name) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);			
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$kdharga,$customer)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif, e.i_nota as nota, d.n_deliver as jml, 
									c.e_product_name as nama, d.v_unit_price as harga, e.d_nota,
									e.n_nota_discount1, e.n_nota_discount2, e.n_nota_discount3,
									e.f_plus_ppn, e.f_plus_discount
									from tr_product_motif a,tr_product_price b,tr_product c, tm_nota_item d, tm_nota e
									where b.i_product=a.i_product 
									  and a.i_product=c.i_product 
									  and b.i_price_group='$kdharga'
									  and d.i_product=c.i_product 
									  and d.i_product=a.i_product 
									  and d.i_product_motif=a.i_product_motif
									  and d.i_nota=e.i_nota
									  and e.i_customer='$customer'
									  and d.n_deliver>0
								    limit $num offset $offset",false);
#and e.f_lunas='f'
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$kdharga,$customer,$num,$offset)
    {
		if($offset=='')
			$offset=0;
			$query=$this->db->query("  select a.i_product as kode, a.i_product_motif as motif,
									a.e_product_motifname as namamotif, e.i_nota as nota, d.n_deliver as jml, 
									c.e_product_name as nama, d.v_unit_price as harga, e.d_nota,
									e.n_nota_discount1, e.n_nota_discount2, e.n_nota_discount3,
									e.f_plus_ppn, e.f_plus_discount
									from tr_product_motif a,tr_product_price b,tr_product c, tm_nota_item d, tm_nota e
									where b.i_product=a.i_product 
									  and a.i_product=c.i_product 
									  and b.i_price_group='$kdharga'
									  and d.i_product=c.i_product 
									  and d.i_product=a.i_product 
									  and d.i_product_motif=a.i_product_motif
									  and d.i_nota=e.i_nota
									  and e.i_customer='$customer'
									  and d.n_deliver>0
									and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
									limit $num offset $offset",false);
#and e.f_lunas='f'
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
