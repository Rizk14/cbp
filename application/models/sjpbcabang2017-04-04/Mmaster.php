<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($isjp,$icustomer)
    {
    $this->db->select(" a.*, c.e_customer_name, b.e_area_name, b.i_store
                        from tm_sjpb a, tr_area b, tr_customer c
                        where a.i_area=b.i_area and a.i_customer=c.i_customer
                        and a.i_sjpb ='$isjp' ", false);#and substring(a.i_sj,9,2)='$iarea'", false);
    $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->row();
      }
    }
    function bacadetail($isj, $icustomer)
    {
    /*$this->db->select(" a.*, b.e_product_motifname, c.v_sjpb
                        from tm_sjpb_item a, tr_product_motif b, tm_sjpb c, tm_sjp_item d
                        where a.i_sjpb = '$isj' and a.i_product=b.i_product 
                        and a.i_sjpb=c.i_sjpb and a.i_area=c.i_area
                        and c.i_sjp=d.i_sjp and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false);*/
                        
    $this->db->select(" a.*, b.e_product_motifname, c.v_sjpb from tm_sjpb_item a 
                        inner join tr_product_motif b on (a.i_product=b.i_product  
                        and a.i_product_motif=b.i_product_motif) inner join tm_sjpb c 
                        on (a.i_sjpb=c.i_sjpb and a.i_area=c.i_area) left join tm_sjp_item d
                        on (a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product=d.i_product and c.i_sjp=d.i_sjp and c.i_area=d.i_area )
                        where a.i_sjpb ='$isj' order by a.n_item_no", false);                    
                        
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
    }
    function bacaarea($num,$offset,$area1,$iuser)
    {
		  if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select(" a.i_area, a.e_area_name, a.i_store, b.i_store_location, b.i_store_locationbin
                            from tr_area a, tr_store_location b
                            where a.f_area_consigment='t' and a.i_store = b.i_store and b.i_store_location='PB'
                            order by i_area", false)->limit($num,$offset);
		  }else{
        $this->db->select("* from tr_area a, tr_store_location b
                           where 
                           a.i_area='PB' and a.f_area_consigment='t' and a.i_store = b.i_store
                           order by a.i_area", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($cari,$num,$offset,$area1,$iuser)
    {
		  if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("a.i_area, a.e_area_name, a.i_store, b.i_store_location, b.i_store_locationbin
                          from tr_area a, tr_store_location b
                          where a.f_area_consigment='t' and a.i_store = b.i_store and b.i_store_location='PB'
                          and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						              order by i_area ", FALSE)->limit($num,$offset);
		  }else{
        $this->db->select("i_area, e_area_name from tr_area where f_area_consigment='t' and  (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                       and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacacustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" a.*, c.i_spg, c.e_spg_name from tr_customer a, tr_customer_consigment b, tr_spg c
                        where b.i_area_real='$iarea' and a.i_customer=b.i_customer and a.i_customer=c.i_customer
                        and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
				                order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaproduct($istore,$num,$offset)
    {
		  if($offset=='')
		  $offset=0;
      if($istore=='PB'){
				$stquery = "a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c
                    where b.i_product_motif='00' and a.i_store_location='00' and 
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore'";
			}else{
				$stquery = "a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c
                    where b.i_product_motif='00' and a.i_store_location='00' and a.i_product = b.i_product 
                    and a.i_product = c.i_product and i_store ='$istore'";
			}
      $this->db->select($stquery,false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariproduct($cari,$istore,$icustomer,$num,$offset)
    {
		  if($offset=='')
		  $offset=0;
      if($cari == ''){
        $cari='all';
      }
			if ($cari != "all"){
        if($istore=='PB'){
				  $stquery = "e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c, tr_customer_consigment d, tr_product_priceco e
                    where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%') and b.i_product_motif='00' 
                    and a.i_store_location='00' and d.i_customer='$icustomer' and e.i_price_groupco=d.i_price_groupco 
                    and c.i_product=e.i_product and a.i_product=e.i_product and
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='PB'
                    group by e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active order by e.i_product
                    ";
			  }else{
				  $stquery = "e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c, tr_customer_consigment d, tr_product_priceco e
                    where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%') and b.i_product_motif='00' 
                    and a.i_store_location='00' and d.i_customer='$icustomer' and e.i_price_groupco=d.i_price_groupco 
                    and c.i_product=e.i_product and a.i_product=e.i_product and
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='PB'
                    group by e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active order by e.i_product
                    ";
			  }
			}
      else{
        if($istore=='PB'){
				  $stquery = "e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c, tr_customer_consigment d, tr_product_priceco e
                    where b.i_product_motif='00' and a.i_store_location='00' and d.i_customer='$icustomer' and e.i_price_groupco=d.i_price_groupco 
                    and c.i_product=e.i_product and a.i_product=e.i_product and
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore' 
                    group by e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active order by e.i_product
";
			  }else{
				  $stquery = "e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c, tr_customer_consigment d, tr_product_priceco e
                    where b.i_product_motif='00' and a.i_store_location='00' and d.i_customer='$icustomer' and e.i_price_groupco=d.i_price_groupco 
                    and c.i_product=e.i_product and a.i_product=e.i_product and
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore'
                    group by e.i_product, e.i_product_grade, a.i_product_motif, b.e_product_motifname, e.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active order by e.i_product
                    ";
			              
        }
      }
      $this->db->select($stquery,false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function runningnumbersj($iareasj,$thbl)
    {
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $tbl=substr($thbl,2,2).substr($thbl,4,2);
	    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SB'
                          and e_periode='$asal' 
                          and i_area='$iareasj' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nosj
                            where i_modul='SB'
                            and e_periode='$asal' 
                            and i_area='$iareasj'", false);
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
 		  	$nosj  ="SB-".$tbl."-".$iareasj.$nosj;
			  return $nosj;
		  }else{
			  $nosj  ="0001";
		  	$nosj  ="SB-".$tbl."-".$iareasj.$nosj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SB','$iareasj','$asal',1)");
			  return $nosj;
		  }
    }

    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin ='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                    DELETE FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin ='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function insertsjpb($isjpb, $icustomer, $iarea, $ispg, $dsjpb, $vsjpb)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjentry	= $row->c;
		  $area	    = $this->session->userdata('i_area');
      $user     = $this->session->userdata('user_id');
		  $this->db->set(
		  array(	  
      'i_sjpb'        => $isjpb,
	    'i_sjp'		      => '',
	    'i_customer'    => $icustomer,
	    'i_area'        => $iarea,
	    'i_spg'         => $ispg,
		  'd_sjpb'		    => $dsjpb,
	    'v_sjpb'      	=> $vsjpb,
	    'f_sjpb_cancel' => 'f',
	    'd_sjpb_entry'  => $dsjentry,
	    'i_entry'       => $user, 
	    'i_area_entry'  => $area
		  )
    	);
    	$this->db->insert('tm_sjpb');
    }
    function insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                			     $vunitprice,$isjpb,$iarea,$i,$dsjpb,$ipricegroup)
    {
    	$this->db->set(
    		array(
				'i_product'			  => $iproduct,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'e_product_name'	=> $eproductname,
				'n_deliver'       => $ndeliver,
				'v_unit_price'		=> $vunitprice,
				'i_sjpb'			    => $isjpb,
				'i_area'	        => $iarea,
				'n_item_no'       => $i,        
        'd_sjpb'          => $dsjpb,
        'i_price_group'   => $ipricegroup
    		)
    	);
    	$this->db->insert('tm_sjpb_item');
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw
                                )
                              ",false);
*/
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,0,0,$qsj,$qaw-$qsj,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
}

