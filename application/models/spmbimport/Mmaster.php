<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }

    public function insertheader($ispmb, $dspmb, $iarea, $ispmbold, $fretur)
    {
        $this->db->set(
            array(
                'i_spmb'        => $ispmb,
                'd_spmb'        => $dspmb,
                'i_area'        => $iarea,
                'i_spmb_old'    => $ispmbold,
                'f_retur'       => $fretur,
                'n_print'       => 0,
            )
        );

        $this->db->insert('tm_spmb');
    }

    public function insertdetail($ispmb, $iproduct, $eproductname, $norder, $iarea, $vunitprice, $eremark, $i)
    {
        $eproductname = strtoupper($eproductname);
        $iproduct = strtoupper($iproduct);
        $query = $this->db->query(" select i_product from tr_product
		                          where i_product='$iproduct'", false); # and upper(e_product_name) = '$eproductname'",false);
        if ($query->num_rows() > 0) {
            $this->db->set(
                array(
                    'i_spmb' => $ispmb,
                    'i_product' => $iproduct,
                    'i_product_grade' => 'A',
                    'i_product_motif' => '00',
                    'n_order' => $norder,
                    'e_product_name' => $eproductname,
                    'i_area' => $iarea,
                    'v_unit_price' => $vunitprice,
                    'e_remark' => $eremark,
                    'n_item_no' => $i,
                )
            );
            $this->db->insert('tm_spmb_item');
        } else {

            echo "<h1>Kode Barang " . $iproduct . " Belum ada di Master Barang !. Silahkan Input Dulu Kode Barang Tersebut. Terima Kasih ^_^</h1>";
            // die();
            // $this->db->set(
            //     array(
            //       'i_spmb'              => $ispmb,
            //       'i_product'            => $iproduct,
            //       'i_product_grade'    => 'A',
            //       'i_product_motif'    => '00',
            //       'n_order'              => $norder,
            //       'e_product_name'    => $eproductname,
            //       'i_area'              => $iarea,
            //       'v_unit_price'        => $vunitprice,
            //       'e_remark'            => $eremark,
            //       'n_item_no'            => $i
            //     )
            // );
            // $this->db->insert('ada_salah_kode');
        }
    }

    public function runningnumber($thbl, $iarea)
    {
        $th = substr($thbl, 0, 4);
        $asal = $thbl;
        $thbl = substr($thbl, 2, 2) . substr($thbl, 4, 2);
        $this->db->select(" n_modul_no as max from tm_dgu_no
                          where i_modul='SPM'
                          and substr(e_periode,1,4)='$th' for update", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $terakhir = $row->max;
            }
            $nospmb = $terakhir + 1;
            $this->db->query(" update tm_dgu_no
                            set n_modul_no=$nospmb
                            where i_modul='SPM'
                            and substr(e_periode,1,4)='$th' ", false);
            settype($nospmb, "string");
            $a = strlen($nospmb);
            while ($a < 6) {
                $nospmb = "0" . $nospmb;
                $a = strlen($nospmb);
            }
            $nospmb = "SPMB-" . $thbl . "-" . $nospmb;
            return $nospmb;
        } else {
            $nospmb = "000001";
            $nospmb = "SPMB-" . $thbl . "-" . $nospmb;
            $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                           values ('SPM','00','$asal',1)");
            return $nospmb;
        }
        /*

$th        = substr($thbl,0,2);
$this->db->select(" max(substr(i_spmb,11,6)) as max from tm_spmb where substr(i_spmb,6,2)='$th' ", false);
$query = $this->db->get();
if ($query->num_rows() > 0){
foreach($query->result() as $row){
$terakhir=$row->max;
}
$nospmb  =$terakhir+1;
settype($nospmb,"string");
$a=strlen($nospmb);
while($a<6){
$nospmb="0".$nospmb;
$a=strlen($nospmb);
}
$nospmb  ="SPMB-".$thbl."-".$nospmb;
return $nospmb;
}else{
$nospmb  ="000001";
$nospmb  ="SPMB-".$thbl."-".$nospmb;
return $nospmb;
}
 */
    }
}
