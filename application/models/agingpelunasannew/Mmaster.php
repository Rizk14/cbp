<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari,$djt)
    {
      $this->db->select(" i_customer, e_customer_name, sum(saldo) as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, 
	                        sum(kn) as kn from(
	                        select i_customer, e_customer_name, (sum(nota)+sum(dn))-(sum(pl)+sum(kn))as saldo, 0 as nota, 0 as dn, 0 as pl, 
	                        0 as kn from(
	                        select i_customer, e_customer_name, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 
	                        0 as kn 
	                        from tm_nota a, tr_customer_groupar b, tr_customer c
	                        where f_nota_cancel='f' and a.i_customer=b.i_customer and b.i_customer_groupar=c.i_customer
	                        and a.d_nota< '$dfrom' and a.i_area='$iarea' 
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn 
	                        from tm_kn a, tr_customer_groupar b, tr_customer c
	                        where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and b.i_customer_groupar=c.i_customer
	                        and a.d_kn>= '2000-03-01' and a.d_kn< '$dfrom'
	                        and a.i_customer=b.i_customer and a.i_area='$iarea' 
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn 
	                        from tm_pelunasan a, tr_customer_groupar b, tr_customer c
	                        where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' 
	                        and a.d_bukti>= '2000-03-01' and a.d_bukti< '$dfrom' and b.i_customer_groupar=c.i_customer
	                        and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_customer=b.i_customer and a.i_area='$iarea'
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn 
	                        from tm_kn a, tr_customer_groupar b, tr_customer c
	                        where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' 
	                        and a.d_kn>= '2000-03-01' and a.d_kn< '$dfrom' and b.i_customer_groupar=c.i_customer
	                        and a.i_customer=b.i_customer and a.i_area='$iarea' 
	                        group by b.i_customer_groupar, c.e_customer_name
	                        ) as a group by i_customer, e_customer_name
	                        ) as b group by i_customer, e_customer_name

	                        union all

	                        select i_customer, e_customer_name, 0 as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn 
	                        from(
	                        select i_customer, e_customer_name, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 
	                        0 as kn 
	                        from tm_nota a, tr_customer_groupar b, tr_customer c
	                        where f_nota_cancel='f' and a.i_customer=b.i_customer 
	                        and a.d_nota>= '$dfrom' and a.d_nota<= '$dto' and b.i_customer_groupar=c.i_customer
	                        and a.i_area='$iarea' 
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn 
	                        from tm_kn a, tr_customer_groupar b, tr_customer c
	                        where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and a.i_customer=b.i_customer 
	                        and a.d_kn>= '$dfrom' and a.d_kn<= '$dto' and a.i_area='$iarea' and b.i_customer_groupar=c.i_customer
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn 
	                        from tm_pelunasan a, tr_customer_groupar b, tr_customer c
	                        where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and a.i_customer=b.i_customer 
	                        and a.d_bukti>= '$dfrom' and a.d_bukti<= '$dto' and b.i_customer_groupar=c.i_customer
	                        and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_area='$iarea' 
	                        group by b.i_customer_groupar, c.e_customer_name
	                        union all
	                        select b.i_customer_groupar as i_customer, c.e_customer_name, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn 
	                        from tm_kn a, tr_customer_groupar b, tr_customer c
	                        where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer  
	                        and a.d_kn>= '$dfrom' and a.d_kn<= '$dto' and a.i_area='$iarea' and b.i_customer_groupar=c.i_customer
	                        group by b.i_customer_groupar, c.e_customer_name
	                        ) as a group by i_customer, e_customer_name
	                        ) as b group by i_customer, e_customer_name
	                        ) as c
	                        where saldo<>0 or nota>0 or dn>0 or pl>0 or kn>0
	                        group by i_customer, e_customer_name
	                        order by i_customer",false);#->limit($num,$offset);
 		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($icustomer,$djt,$num,$offset)
    {
      $this->db->select(" a.v_sisa, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.v_nota_netto, a.n_nota_toplength
                          from tm_nota a, tr_customer b
					                where a.i_customer=b.i_customer 
					                and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
                          and not a.i_nota isnull
				                  and a.i_customer='$icustomer' and a.v_sisa>0 
                          and a.f_nota_cancel='f'
                          order by a.i_nota",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
