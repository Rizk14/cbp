<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomer,$iareacover)
    {
		$this->db->select(" * from tr_customer_rayon a, tr_customer b 
							 where a.i_customer = '$icustomer' and b.i_area_rayon = '$iareacover' and
					  		 a.f_ar_cancel='f'
				   			 order by a.i_customer, a.i_area_rayon", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($icustomer,$iareacover,$eareacovername,$dareacoverentry)
    {
    	$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dareacoverentry= $row->c;
    	$this->db->query("insert into tr_customer_rayon (i_customer,i_area_rayon,d_area_rayon_entry,f_ar_cancel) values ('$icustomer','$iareacover'	,'$dareacoverentry','t')");
		  #redirect('arearayon/cform/');
    }

    function cancel($icustomer,$iareacover) 
    {
		$this->db->query("update tr_customer_rayon set f_ar_cancel='f' where i_customer='$icustomer' and i_area_rayon='$iareacover' ");
		return TRUE;
    }
   
    function bacasemua($cari, $num,$offset)
    {
		    $this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_area_rayon_name 
		    					from tr_customer_rayon a, tr_customer b, tr_area c, tr_rayon d
		    				    where b.i_customer=a.i_customer and b.i_area=c.i_area and a.i_customer like '%$cari%' and  a.i_area_rayon=d.i_area_rayon
		    					and a.f_ar_cancel='t'
		    					order by a.i_customer, a.i_area_rayon",false)->limit($num,$offset);
       
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacacustomer($num,$offset)
    {
		$this->db->select("i_customer, e_customer_name from tr_customer order by i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacarayon($num,$offset)
    {
		$this->db->select("i_area_rayon, e_area_rayon_name from tr_rayon order by i_area_rayon",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

/*	function bacaareacover($icustomer,$num,$offset)
    {
		$this->db->select("i_area_cover from tr_customer_rayon where upper(tr_customer_rayon.i_area) like '%$icustomer%'",false)->limit($num,$offset);
		$query2 = $this->db->get();
		if ($query2->num_rows() > 0){
			return $query2->result();
		}
    }
*/
    function cari($cari,$num,$offset)
    {
      $this->db->select("a.*, b.e_customer_name, c.e_area_name, d.e_area_rayon_name from tr_customer_rayon a, tr_customer b, tr_area c, tr_rayon d
      					where  a.i_customer=b.i_customer and b.i_area=c.i_area and a.i_area_rayon=d.i_area_rayon and
      					(upper(b .e_customer_name) like '%$cari%')
      					order by a.i_customer	",false)->limit($num,$offset);
      
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function caricustomer($cari,$num,$offset)
    {
		$this->db->select("i_customer, e_customer_name from tr_customer where (upper (i_customer) ilike '%$cari%' or upper (e_customer_name) ilike '%$cari%') order by i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
        function carirayon($cari,$num,$offset)
    {
		$this->db->select("i_area_rayon, e_area_rayon_name from tr_rayon where (upper (i_area_rayon) ilike '%$cari%' or upper (e_area_rayon_name) ilike '%$cari%') order by i_area_rayon",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>