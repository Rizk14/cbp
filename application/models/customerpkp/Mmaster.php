<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomer)
    {
		$this->db->select("i_customer, e_customer_pkpname, e_customer_pkpaddress, e_customer_pkpnpwp from tr_customer_pkp where i_customer = '$icustomer' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomer, $ecustomerpkpname, $ecustomerpkpaddress,$ecustomerpkpnpwp)
    {
    	$this->db->set(
    		array(
    			'i_customer' 	  	  => $icustomer,
    			'e_customer_pkpname' 	  => $ecustomerpkpname,
			'e_customer_pkpaddress'   => $ecustomerpkpaddress,
			'e_customer_pkpnpwp'	  => $ecustomerpkpnpwp
    		)
    	);
    	
    	$this->db->insert('tr_customer_pkp');
		#redirect('customerpkp/cform/');
    }
    function update($icustomer, $ecustomerpkpname, $ecustomerpkpaddress, $ecustomerpkpnpwp)
    {
    	$data = array(
               'i_customer' 		        => $icustomer,
               'e_customer_pkpname' 	  => $ecustomerpkpname,
	             'e_customer_pkpaddress' 	=> $ecustomerpkpaddress,
	             'e_customer_pkpnpwp' 	  => $ecustomerpkpnpwp
            );
		$this->db->where('i_customer =', $icustomer);
		$this->db->update('tr_customer_pkp', $data);

    $this->db->select("i_customer from tr_customer_tmp where i_customer='$icustomer'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      $data = array(
               'i_customer' 		        => $icustomer,
               'e_customer_npwpname' 	  => $ecustomerpkpname,
	             'e_customer_npwpaddress' => $ecustomerpkpaddress,
	             'e_customer_pkpnpwp' 	  => $ecustomerpkpnpwp
              );
      $this->db->where('i_customer', $icustomer);
      $this->db->update('tr_customer_tmp', $data);
    }else{
      $this->db->select("i_customer from tr_customer_tmpnonspb where i_customer='$icustomer'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        $data = array(
               'i_customer' 		        => $icustomer,
               'e_customer_npwpname' 	  => $ecustomerpkpname,
	             'e_customer_npwpaddress' => $ecustomerpkpaddress,
	             'e_customer_pkpnpwp' 	  => $ecustomerpkpnpwp
                );
        $this->db->where('i_customer', $icustomer);
        $this->db->update('tr_customer_tmpnonspb', $data);
      }
    }
 
		#redirect('customerpkp/cform/');
    }
	
    public function delete($icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer_pkp WHERE i_customer='$icustomer'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_customer, b.e_customer_name, a.e_customer_pkpname, a.e_customer_pkpaddress, a.e_customer_pkpnpwp
                        from tr_customer_pkp a, tr_customer b
                        where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_pkpname) like '%$cari%'
                        or upper(b.e_customer_name) like '%$cari%')
                        and a.i_customer=b.i_customer
                        order by a.i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_customer, b.e_customer_name, a.e_customer_pkpname, a.e_customer_pkpaddress, a.e_customer_pkpnpwp
                        from tr_customer_pkp a, tr_customer b
                        where (upper(a.i_customer) ilike '%$cari%' or upper(a.e_customer_pkpname) ilike '%$cari%'
                        or upper(b.e_customer_name) ilike '%$cari%')
                        and a.i_customer=b.i_customer
                        order by a.i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
