<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	  public function __construct()
    {
          parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
      $this->db->select(" distinct on (a.i_customer, a.n_notapb_discount) a.i_customer, b.e_customer_name, sum(c.n_quantity) as jumlah, 
                          a.n_notapb_discount, sum((a.n_notapb_discount*(c.n_quantity*c.v_unit_price))/100) as diskon, 
                          sum(c.n_quantity*c.v_unit_price) as kotor, a.i_area
                          from tm_notapb a, tr_customer b, tm_notapb_item c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                          and a.f_spb_rekap='f' and a.i_cek is not null and not a.i_cek=''
                          group by a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                          order by a.i_customer, a.n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadiskon($iperiode)
    {
		  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                          where to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and f_spb_rekap='f' and i_cek is not null
                          order by n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumberiks($thbl,$namagrup,$iarea,$enama_group){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_konsinyasi as max, i_group from tm_dgu_nopb
                          where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
          $grup=$row->i_group;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_nopb 
                            set n_modul_konsinyasi=$nospb
                            where i_modul='SPB' and i_group='$grup'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<6){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
		  	$nospb  ="IKS-".$thbl."-".$nospb;
		  	$query->free_result();
			  return $nospb;
		  }else{
/*
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup' 
                            and substr(e_periode,1,4)='$th'", false);
*/
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $grup=$row->i_group;
          }
			  }
			  $nospb  ="000001";
			  $nospb  ="IKS-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_nopb(i_modul, i_area, i_group, e_periode, n_modul_no,e_group_name,n_modul_konsinyasi) 
                           values ('SPB','$iarea','$grup','$asal',0,'$enama_group',1)");
        $query->free_result();
			  return $nospb;
		  }
    }
    function runningnumberspb($thbl,$namagrup,$iarea,$enama_group){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max, i_group from tm_dgu_nopb
                          where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
          $grup=$row->i_group;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_nopb 
                           set n_modul_no=$nospb
                           where i_modul='SPB' and i_group='$grup'
                           and substr(e_periode,1,4)='$th' ", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<4){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
		  	$nospb  ="SPB-".$thbl."-".$grup.$nospb;
		  	$query->free_result();
			  return $nospb;
		  }else{
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'", false);
/*
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                            and substr(e_periode,1,4)='$th'", false);
*/
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $grup=$row->i_group;
          }
			  }
			  $nospb  ="0001";
			  $nospb  ="SPB-".$thbl."-".$grup.$nospb;
        $this->db->query(" insert into tm_dgu_nopb(i_modul, i_area, i_group, e_periode, n_modul_no,e_group_name) 
                           values ('SPB','$iarea','$grup','$asal',1,'$enama_group')");
        $query->free_result();
			  return $nospb;
		  }
    }
    function insertheader($iks, $ispb, $dspb, $iarea, $icustomer)
    {
      $this->db->select(" a.*, b.i_price_group as klp_harga
                          from tr_customer a, tr_price_group b
                          where (a.i_price_group=b.i_price_group or a.i_price_group=b.n_line)
                          and a.i_customer='$icustomer' and a.i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $ipricegroup  = $row->klp_harga;
          $fpkp         = $row->f_customer_pkp;
          $fppn         = $row->f_customer_plusppn;
          $top          = $row->n_customer_toplength;
			  }
			  if($iarea=='PB'){
			    $lokasi='00';
			  }else{
			    $lokasi='PB';
			  }
			  $query->free_result();
      	$this->db->set(
      		array(
			        'i_konsinyasi'		      => $iks,
			        'i_spb'		              => $ispb,
			        'd_spb'		              => $dspb,
              'i_customer'            => $icustomer,
              'i_price_group'         => $ipricegroup,
              'i_store'               => $iarea,
              'i_store_location'      => $lokasi,
              'i_product_group'       => '01',
              'i_salesman'            => 'TL',
              'f_spb_stockdaerah'     => 't',
              'f_spb_consigment'      => 't',
              'f_spb_siapnotagudang'  => 't',
              'f_spb_valid'           => 't',
              'f_spb_pkp'             => $fpkp,
              'f_spb_plusppn'         => $fppn,
			        'i_area'		            => $iarea,
              'n_spb_toplength'       => $top,
              'n_spb_discount1'       => 0,
              'n_spb_discount2'       => 0,
              'n_spb_discount3'       => 0,
              'v_spb_discount1'       => 0,
              'v_spb_discount2'       => 0,
              'v_spb_discount3'       => 0,
              'v_spb_discounttotal'   => 0,
			        'n_print'		            => 0,
              'i_approve1'          => 'SYSTEM',
              'i_approve2'          => 'SYSTEM',
              'i_cek'               => 'SYSTEM',
              'f_spb_siapnotasales' => 't'
      		)
      	);
      	$this->db->insert('tm_spbkonsinyasi');
      }
    }
    function insertdetail($iks,$ispb,$iarea,$iproduct,$eproductname,$iproductgrade,$iproductmotif,$harga,$quantity,$x)
    {
      $this->db->query("insert into tm_spbkonsinyasi_item values('$iks','$ispb','$iproduct','$iproductgrade','$iproductmotif',
                                                        $quantity,null,null,
                                                        $harga,'$eproductname',null,'$iarea',null,$x,'1')");
    }
    function updatespb($iks,$ispb,$iarea)
    {
      $query=$this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spbkonsinyasi_item
                                where i_konsinyasi='$iks' and i_area = '$iarea' and i_spb='$ispb'");
      if($query->num_rows()>0){
        foreach($query->result() as $xx){
          $total=$xx->order;
        }
        $query->free_result();
	      $this->db->query("update tm_spbkonsinyasi set v_spb='$total' where i_konsinyasi='$iks' and i_spb='$ispb' and i_area='$iarea'");
      }
    }

    function updatebon($icustomer,$iperiode,$ispb)
    {
      $this->db->query("update tm_notapb set i_spb='$ispb', f_spb_rekap='t' where i_customer='$icustomer'
                        and to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and not i_cek is null and not d_cek is null");
    }
    function baca($ispb, $iarea)
    {
      $this->db->select(" a.e_remark1 AS emark1, a.*, e.e_price_groupname,
           d.e_area_name, b.e_customer_name, b.e_customer_address, c.e_salesman_name, b.f_customer_first
           from tm_spbkonsinyasi a
               inner join tr_customer b on (a.i_customer=b.i_customer)
               inner join tr_salesman c on (a.i_salesman=c.i_salesman)
               inner join tr_customer_area d on (a.i_customer=d.i_customer)
               left join tr_price_group e on (a.i_price_group=e.i_price_group)
               where a.i_spb ='$ispb' and a.i_area='$iarea'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function bacadetail($ispb, $iarea, $ipricegroup)
    {
      $this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
                          a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, b.e_product_motifname,
                          c.v_product_retail as hrgnew, a.i_product_status
                          from tm_spbkonsinyasi_item a, tr_product_motif b, tr_product_price c
                          where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                          and a.i_product=c.i_product and c.i_price_group='$ipricegroup' and c.i_product_grade='A'
                          order by a.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetailnilaispb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spbkonsinyasi_item a
                                where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spbkonsinyasi_item a
                                where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function updateheader($ispb, $iarea, $nspbdiscount1, $vspbdiscount1, $vspbdiscounttotal, $vspb)
    {
       $query      = $this->db->query("SELECT current_timestamp as c");
       $row        = $query->row();
       $dspbupdate = $row->c;
          $data = array( 
             'n_spb_discount1'    => $nspbdiscount1,
             'v_spb_discount1'    => $vspbdiscount1,
             'v_spb_discounttotal'=> $vspbdiscounttotal,
             'v_spb'              => $vspb,
             'd_spb_update'       => $dspbupdate,
             'i_product_group'    => '01'
                );
       $this->db->where('i_spb', $ispb);
       $this->db->where('i_area', $iarea);
       $this->db->update('tm_spbkonsinyasi', $data);
    }
}
?>
