<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		    #$this->CI =& get_instance();
    }

    function baca($icustomer)
    {
		$this->db->select(" * FROM tr_customer 
            LEFT JOIN  tr_customer_tmp
            ON (tr_customer_tmp.i_customer=tr_customer.i_customer)
            LEFT JOIN tr_shop_status 
            ON (tr_customer_tmp.i_shop_status=tr_shop_status.i_shop_status)
            LEFT JOIN tr_marriage 
            ON (tr_customer_tmp.i_marriage=tr_marriage.i_marriage)
            LEFT JOIN tr_jeniskelamin 
            ON (tr_customer_tmp.i_jeniskelamin=tr_jeniskelamin.i_jeniskelamin)
            LEFT JOIN tr_religion 
            ON (tr_customer_tmp.i_religion=tr_religion.i_religion)
            LEFT JOIN tr_traversed 
            ON (tr_customer_tmp.i_traversed=tr_traversed.i_traversed)
            LEFT JOIN tr_paymentmethod 
            ON (tr_customer_tmp.i_paymentmethod=tr_paymentmethod.i_paymentmethod)
            LEFT JOIN tr_call 
            ON (tr_customer_tmp.i_call=tr_call.i_call)
            LEFT JOIN tr_customer_plugroup
					  ON (tr_customer.i_customer_plugroup = tr_customer_plugroup.i_customer_plugroup)
					  LEFT JOIN tr_city
					  ON (tr_customer.i_city = tr_city.i_city and tr_customer.i_area = tr_city.i_area)
					  LEFT JOIN tr_customer_group
					  ON (tr_customer.i_customer_group = tr_customer_group.i_customer_group)
					  LEFT JOIN tr_price_group
					  ON (tr_customer.i_price_group=tr_price_group.n_line or tr_customer.i_price_group=tr_price_group.i_price_group)
					  LEFT JOIN tr_area
					  ON (tr_customer.i_area = tr_area.i_area)
					  LEFT JOIN tr_customer_status 
					  ON (tr_customer.i_customer_status = tr_customer_status.i_customer_status)
					  LEFT JOIN tr_customer_producttype
					  ON (tr_customer.i_customer_producttype = tr_customer_producttype.i_customer_producttype)
					  LEFT JOIN tr_customer_specialproduct
				  	ON (tr_customer.i_customer_specialproduct = tr_customer_specialproduct.i_customer_specialproduct)
					  LEFT JOIN tr_customer_grade
					  ON (tr_customer.i_customer_grade = tr_customer_grade.i_customer_grade)
					  LEFT JOIN tr_customer_service
					  ON (tr_customer.i_customer_service = tr_customer_service.i_customer_service)
					  LEFT JOIN tr_customer_salestype
					  ON (tr_customer.i_customer_salestype = tr_customer_salestype.i_customer_salestype)
					  LEFT JOIN tr_customer_class 
					  ON (tr_customer.i_customer_class=tr_customer_class.i_customer_class)
					  where tr_customer.i_customer = '$icustomer'
				", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert(
		$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
		$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
		$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
		$icustomerstatus, $ecustomername, $ecustomeraddress,$ecustomercity, 
		$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
		$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
		$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
		$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
		$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
		$fcustomertax, $ecustomerretensi, $ncustomertoplength, $fcustomercicil
		   )
    {
		if($fcustomerplusppn=='on')
			$fcustomerplusppn='TRUE';
		else
			$fcustomerplusppn='FALSE';
		if($fcustomerplusdiscount=='on')
			$fcustomerplusdiscount='TRUE';
		else
			$fcustomerplusdiscount='FALSE';
		if($fcustomerpkp=='on')
			$fcustomerpkp='TRUE';
		else
			$fcustomerpkp='FALSE';
		if($fcustomeraktif=='on')
			 $fcustomeraktif='TRUE';
		else
			$fcustomeraktif='FALSE';
		if($fcustomertax=='on')
			$fcustomertax='TRUE';
		else
			$fcustomertax='FALSE';
		if($fcustomercicil=='on')
			$fcustomercicil='TRUE';
		else
			$fcustomercicil='FALSE';
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
			'i_customer' 			=> $icustomer, 
			'i_customer_plugroup' 		=> $icustomerplugroup, 
			'i_city' 			=> $icity, 
			'i_customer_group'	 	=> $icustomergroup, 
			'i_price_group' 		=> $ipricegroup,
			'i_area'		 	=> $iarea, 
			'i_customer_status' 		=> $icustomerstatus, 
			'i_customer_producttype'	=> $icustomerproducttype, 
			'i_customer_specialproduct'	=> $icustomerspecialproduct, 
			'i_customer_grade' 		=> $icustomergrade, 
			'i_customer_service' 		=> $icustomerservice, 
			'i_customer_salestype' 		=> $icustomersalestype, 
			'i_customer_class' 		=> $icustomerclass, 
			'i_customer_status' 		=> $icustomerstatus, 
			'e_customer_name' 		=> $ecustomername, 
			'e_customer_address' 		=> $ecustomeraddress,
			'e_customer_city' 		=> $ecustomercity, 
			'e_customer_postal' 		=> $ecustomerpostal, 
			'e_customer_phone' 		=> $ecustomerphone, 
			'e_customer_fax' 		=> $ecustomerfax, 
			'e_customer_mail'	 	=> $ecustomermail, 
			'e_customer_sendaddress' 	=> $ecustomersendaddress, 
			'e_customer_receiptaddress'	=> $ecustomerreceiptaddress, 
			'e_customer_remark' 		=> $ecustomerremark, 
			'e_customer_payment' 		=> $ecustomerpayment, 
			'e_customer_priority' 		=> $ecustomerpriority, 
			'e_customer_contact' 		=> $ecustomercontact, 
			'e_customer_contactgrade' 	=> $ecustomercontactgrade, 
			'e_customer_refference' 	=> $ecustomerrefference, 
			'e_customer_othersupplier' 	=> $ecustomerothersupplier, 
			'f_customer_plusppn' 		=> $fcustomerplusppn, 
			'f_customer_plusdiscount' 	=> $fcustomerplusdiscount, 
			'f_customer_pkp' 		=> $fcustomerpkp,
			'f_customer_aktif' 		=> $fcustomeraktif, 
			'f_customer_tax' 		=> $fcustomertax,
			'f_customer_cicil'	 	=> $fcustomercicil,
			'f_customer_first'	 	=> 't',    
			'e_customer_retensi' 		=> $ecustomerretensi, 
			'n_customer_toplength' 		=> $ncustomertoplength,
			'd_customer_entry'		=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_customer');
		redirect('customer/cform/');
    }
    function update(
		$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
		$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
		$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
		$icustomerstatus, $ecustomername, $ecustomeraddress,$ecustomercity, 
		$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
		$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
		$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
		$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
		$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
		$fcustomertax, $ecustomerretensi, $ncustomertoplength, $fcustomercicil
		   )
    {
		if($fcustomerplusppn=='on')
			$fcustomerplusppn='TRUE';
		else
			$fcustomerplusppn='FALSE';
		if($fcustomerplusdiscount=='on')
			$fcustomerplusdiscount='TRUE';
		else
			$fcustomerplusdiscount='FALSE';
		if($fcustomerpkp=='on')
			$fcustomerpkp='TRUE';
		else
			$fcustomerpkp='FALSE';
		if($fcustomeraktif=='on')
			 $fcustomeraktif='TRUE';
		else
			$fcustomeraktif='FALSE';
		if($fcustomertax=='on')
			$fcustomertax='TRUE';
		else
			$fcustomertax='FALSE';
		if($fcustomercicil=='on')
			$fcustomercicil='TRUE';
		else
			$fcustomercicil='FALSE';
		$query  = $this->db->query("SELECT current_timestamp as c");
		$row    = $query->row();
		$dupdate= $row->c;
    	$data = array(
		'i_customer_plugroup' 		=> $icustomerplugroup, 
		'i_city'		 	=> $icity, 
		'i_customer_group'	 	=> $icustomergroup, 
		'i_price_group' 		=> $ipricegroup,
		'i_area' 			=> $iarea, 
		'i_customer_status' 		=> $icustomerstatus, 
		'i_customer_producttype' 	=> $icustomerproducttype, 
		'i_customer_specialproduct'	=> $icustomerspecialproduct, 
		'i_customer_grade'	 	=> $icustomergrade, 
		'i_customer_service' 		=> $icustomerservice, 
		'i_customer_salestype' 		=> $icustomersalestype, 
		'i_customer_class' 		=> $icustomerclass, 
		'i_customer_status' 		=> $icustomerstatus, 
		'e_customer_name' 		=> $ecustomername, 
		'e_customer_address' 		=> $ecustomeraddress,
		'e_customer_city' 		=> $ecustomercity, 
		'e_customer_postal' 		=> $ecustomerpostal, 
		'e_customer_phone' 		=> $ecustomerphone, 
		'e_customer_fax' 		=> $ecustomerfax, 
		'e_customer_mail' 		=> $ecustomermail, 
		'e_customer_sendaddress' 	=> $ecustomersendaddress, 
		'e_customer_receiptaddress'	=> $ecustomerreceiptaddress, 
		'e_customer_remark' 		=> $ecustomerremark, 
		'e_customer_payment' 		=> $ecustomerpayment, 
		'e_customer_priority' 		=> $ecustomerpriority, 
		'e_customer_contact' 		=> $ecustomercontact, 
		'e_customer_contactgrade' 	=> $ecustomercontactgrade, 
		'e_customer_refference' 	=> $ecustomerrefference, 
		'e_customer_othersupplier' 	=> $ecustomerothersupplier, 
		'f_customer_plusppn' 		=> $fcustomerplusppn, 
		'f_customer_plusdiscount'	=> $fcustomerplusdiscount, 
		'f_customer_pkp'	 	=> $fcustomerpkp,
		'f_customer_aktif' 		=> $fcustomeraktif, 
		'f_customer_tax' 		=> $fcustomertax, 
		'f_customer_cicil'	 	=> $fcustomercicil,  
		'e_customer_retensi' 		=> $ecustomerretensi, 
		'n_customer_toplength' 		=> $ncustomertoplength,
		'd_customer_update'		=> $dupdate
            );
		$this->db->where('i_customer =', $icustomer);
		$this->db->update('tr_customer', $data); 
		redirect('customer/cform/');
    }
	
    public function delete($icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer WHERE i_customer='$icustomer'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select("* from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')
                           order by i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select("* from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')
                           and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
    									     or i_area = '$area4' or i_area = '$area5') order by i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select("* from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')
                           order by i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select("* from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')
                           and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
    									     or i_area = '$area4' or i_area = '$area5') order by i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaplugroup($num,$offset)
    {
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup order by i_customer_plugroup",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariplugroup($cari,$num,$offset)
    {
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup 
				   where upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plugroup", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacity($iarea,$num,$offset)
    {
		$this->db->select("i_city, e_city_name from tr_city where i_area='$iarea' order by i_city",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricity($cari,$num,$offset)
    {
		$this->db->select("i_city, e_city_name from tr_city 
				   where upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%' order by i_city", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomergroup($num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group order by i_customer_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomergroup($cari,$num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group 
				   where upper(e_customer_groupname) like '%$cari%' or upper(i_customer_group) like '%$cari%' order by i_customer_group", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapricegroup($num,$offset)
    {
		$this->db->select("i_price_group, e_price_groupname, n_line from tr_price_group order by i_price_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caripricegroup($cari,$num,$offset)
    {
		$this->db->select("i_price_group, e_price_groupname, n_line from tr_price_group 
				   where upper(e_price_groupname) like '%$cari%' or upper(i_price_group) like '%$cari%' order by i_price_group", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area 
				   where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerstatus($num,$offset)
    {
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status order by i_customer_status", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerstatus($cari,$num,$offset)
    {
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status 
				   where upper(e_customer_statusname) like '%$cari%' or upper(i_customer_status) like '%$cari%' order by i_customer_status", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerproducttype($num,$offset)
    {
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype order by i_customer_producttype", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerproducttype($cari,$num,$offset)
    {
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype 
				   where upper(e_customer_producttypename) like '%$cari%' or upper(i_customer_producttype) like '%$cari%' order by i_customer_producttype", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerspecialproduct($icustomerproducttype,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query = $this->db->query("select * from tr_customer_specialproduct 
					   where i_customer_producttype='$icustomerproducttype' 
					   order by i_customer_specialproduct 
					   limit $num offset $offset");
		if ($query->num_rows() > 0){

			return $query->result();
		}
    }
    function caricustomerspecialproduct($cari,$num,$offset)
    {
		$this->db->select("i_customer_specialproduct, e_customer_specialproductname from tr_customer_specialproduct 
				   where upper(e_customer_specialproductname) like '%$cari%' or upper(i_customer_specialproduct) like '%$cari%' order by i_customer_specialproduct", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomergrade($num,$offset)
    {
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade order by i_customer_grade", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomergrade($cari,$num,$offset)
    {
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade 
				   where upper(e_customer_gradename) like '%$cari%' or upper(i_customer_grade) like '%$cari%' order by i_customer_grade", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerservice($num,$offset)
    {
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service order by i_customer_service", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerservice($cari,$num,$offset)
    {
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service 
				   where upper(e_customer_servicename) like '%$cari%' or upper(i_customer_service) like '%$cari%' order by i_customer_service", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomersalestype($num,$offset)
    {
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype order by i_customer_salestype", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomersalestype($cari,$num,$offset)
    {
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype 
				   where upper(e_customer_salestypename) like '%$cari%' or upper(i_customer_salestype) like '%$cari%' order by i_customer_salestype", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerclass($num,$offset)
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class order by i_customer_class", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerclass($cari,$num,$offset)
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class 
				   where upper(e_customer_classname) like '%$cari%' or upper(i_customer_class) like '%$cari%' order by i_customer_class", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
