<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$num,$offset,$dfrom,$dto)
    {
    if($area1=='00'){
		$this->db->select(" 	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer and (a.n_print=0 or a.n_print isnull)
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
          and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
          and a.i_area='$iarea'
					order by a.i_spb desc",false)->limit($num,$offset);
    }else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer and (a.n_print=0 or a.n_print isnull)
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
          and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
          and a.i_area='$iarea'
					order by a.i_spb desc",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($periode,$dfrom,$dto,$area)
    {
    $coaku=KasKecil.$area;
    $kasbesar=KasBesar;
    $bank=Bank;
    $this->db->select("	a.* from(
		                    select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk 
		                    from tm_kb a
		                    where a.i_periode='$periode' and a.i_area='$area' 
		                    and a.d_kb >= to_date('$dfrom','dd-mm-yyyy') and a.d_kb <= to_date('$dto','dd-mm-yyyy')
		                    and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
		                    and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where b.d_kk = a.d_kb and b.i_area='$area' 
		                    and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar')
		                    union all
		                    select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_bank as v_kk 
		                    from tm_kbank a
		                    where i_periode='$periode' and i_area='$area' 
		                    and d_bank >= to_date('$dfrom','dd-mm-yyyy') and d_bank <= to_date('$dto','dd-mm-yyyy')
		                    and f_debet='t' and f_kbank_cancel='f' and i_coa='$coaku'
		                    and a.v_bank not in (select b.v_kk as v_bank from tm_kk b where b.d_kk = a.d_bank and b.i_area='$area' 
		                    and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%')
		                    union all
                        select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk from tm_kk a, tr_area b
							          where a.d_kk >= to_date('$dfrom','dd-mm-yyyy') and a.d_kk <= to_date('$dto','dd-mm-yyyy')
							          and a.i_area=b.i_area and a.i_area='$area' and a.f_kk_cancel='f'
							          ) as a
							          order by a.d_kk, a.f_debet, a.i_kk ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jml($dfrom,$dto,$area)
    {
		  $this->db->select("	count(*) as jml from tm_kk a, tr_area b
							            where a.d_kk >= to_date('$dfrom','dd-mm-yyyy') 
							            and a.d_kk <= to_date('$dto','dd-mm-yyyy') 
							            and a.i_area=b.i_area and a.i_area='$area'
												  and a.f_kk_cancel='f'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $jum=$tes->jml;
        }
        return $jum;
		  }
    }
    function bacaname($area)
    {
		  $this->db->select("	* from tr_area where i_area='$area' ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
        return $query->result();
		  }
    }
    function cari($area1,$area2,$area3,$area4,$area5,$cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
					order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($area,$ispb)
    {
		$this->db->query("	update tm_spb set n_print=n_print+1 where i_spb = '$ispb' and i_area = '$area' ",false);
    }
    function bacasaldo($area,$periode,$tanggal)
    {
/*
      $tmp = explode("-", $tanggal);
		  $thn	= $tmp[0];
		  $bln	= $tmp[1];
		  $tgl 	= $tmp[2];
		  $dsaldo	= $thn."/".$bln."/".$tgl;
		  $dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
		  $tmp1 	= explode("-", $dtos,strlen($dtos));
		  $th	= $tmp1[0];
		  $bl	= $tmp1[1];$kasbesar=KasBesar;
    $bank=Bank;
		  $dt	= $tmp1[2];
#		  $dtos	= $th.$bl;
      $periode = $th.$bl;
*/
		  // $this->db->select(" v_saldo_awal from tm_coa_saldo
				// 			  where i_periode='$periode' and substr(i_coa,7,2)='$area' and substr(i_coa,1,6)='".KasKecil."'",false);
      if($periode>'201912'){
				$this->db->select(" v_saldo_awal from tm_coa_saldo
							  		where i_periode='$periode' and i_coa in(select i_coa from tr_coa where i_area = '$area' and i_coa like '110-12%') ",false);
			}else{
				$this->db->select(" v_saldo_awal from tm_coa_saldo
							  		where i_periode='$periode' and substr(i_coa,7,2)='$area' and substr(i_coa,1,6)='110-12' ",false);
			}
		  $query = $this->db->get();
		  $saldo=0;
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $saldo=$row->v_saldo_awal;
			  }
		  }
#		  echo 'awal  :'.$saldo.'<br>';
		  $this->db->select(" sum(v_kk) as v_kk from tm_kk
							  where i_periode='$periode' and i_area='$area'
							  and d_kk<'$tanggal' and f_debet='t' and f_kk_cancel='f'",false);
		  $query = $this->db->get();
		  $kredit=0;
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $kredit=$kredit+$row->v_kk;
			  }
		  }
#		  echo 'kredit:'.$kredit.'<br>';
		  $this->db->select(" sum(v_kk) as v_kk from tm_kk
							  where i_periode='$periode' and i_area='$area'
							  and d_kk<'$tanggal' and f_debet='f' and f_kk_cancel='f'",false);
		  $query = $this->db->get();
		  $debet=0;
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $debet=$debet+$row->v_kk;
			  }
		  }
#		  echo 'debet :'.$debet.'<br>';
		  $coaku=KasKecil.$area;
		  $kasbesar=KasBesar;
     	$bank=Bank;
		  $this->db->select(" sum(v_bank) as v_bank from tm_kbank a 
		                      where a.i_periode='$periode' and a.i_area='$area' and a.d_bank<'$tanggal' and a.f_debet='t' and 
		                      a.f_kbank_cancel='f' and a.i_coa='$coaku'
		                      and a.v_bank not in (select b.v_kk as v_bank from tm_kk b where b.d_kk = a.d_bank and b.i_area='$area' 
		                      and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' and b.i_periode='$periode')
		                      ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $debet=$debet+$row->v_bank;
			  }
		  }
#		  echo 'debet :'.$debet.'<br>';
		  $this->db->select(" sum(v_kb) as v_kb from tm_kb a where a.i_periode='$periode' and a.i_area='$area' and a.d_kb<'$tanggal' 
		                      and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
		                      and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where b.d_kk = a.d_kb and b.i_area='$area' 
		                      and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar' and b.i_periode='$periode')",false);							
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $debet=$debet+$row->v_kb;
			  }
		  }
#		  echo 'debet :'.$debet.'<br>';die;
		  $saldo=$saldo+$debet-$kredit;
      return $saldo;
    }
}
?>
