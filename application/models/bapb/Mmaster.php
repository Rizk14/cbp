<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibapb,$iarea)
    {
		$this->db->select(" tm_bapb.i_bapb, tm_bapb.i_dkb_kirim, tm_bapb.i_area, tm_bapb.d_bapb, tm_bapb.i_bapb_old,
							          tm_bapb.f_bapb_cancel, tm_bapb.n_bal, tr_customer.e_customer_name, tm_bapb.i_customer,
							          tr_area.e_area_name, tr_dkb_kirim.e_dkb_kirim, tm_bapb.v_bapb, tm_bapb.v_kirim
							          from tm_bapb 
							          inner join tr_area on(tm_bapb.i_area=tr_area.i_area)
							          left join tr_customer on(tm_bapb.i_customer=tr_customer.i_customer)
							          inner join tr_dkb_kirim on(tm_bapb.i_dkb_kirim=tr_dkb_kirim.i_dkb_kirim)
							          where tm_bapb.i_bapb ='$ibapb' and tm_bapb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ibapb,$iarea)
    {
		$this->db->select("* from tm_bapb_item where i_bapb = '$ibapb' and i_area='$iarea' order by i_bapb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetailx($ibapb,$iarea)
    {
		$this->db->select("a.*, b.e_ekspedisi from tm_bapb_ekspedisi a, tr_ekspedisi b
										   where a.i_bapb = '$ibapb' and a.i_area='$iarea' and a.i_ekspedisi = b.i_ekspedisi order by a.i_bapb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function customertodetail($isj,$dsj,$iarea)
    {
		return $this->db->query(" select b.i_customer, b.e_customer_name from tm_nota a 
                              inner join tr_customer b on b.i_customer=a.i_customer 
                              where a.i_sj='$isj' and a.d_sj='$dsj' and a.i_area='$iarea' ");
    }
    function insertheader($ibapb, $dbapb, $iarea, $idkbkirim, $icustomer, $nbal, $ibapbold, $vbapb, $vkirim)
    {
    	$this->db->set(
    		array(
			'i_bapb'		    => $ibapb,
			'd_bapb'		    => $dbapb,
			'i_dkb_kirim'	  => $idkbkirim,
			'i_area'		    => $iarea,
			'i_customer'	  => $icustomer,
			'n_bal'	    		=> $nbal,
			'i_bapb_old'  	=> $ibapbold,
			'v_bapb'      	=> $vbapb,
			'v_kirim'     	=> $vkirim
    		)
    	);
    	
    	$this->db->insert('tm_bapb');
    }
   function insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$i,$vsj)
   {
		$this->db->query("DELETE FROM tm_bapb_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sj='$isj'");
		$this->db->set(
			array(
						'i_bapb'  	=> $ibapb,
						'i_area'  	=> $iarea,
	 					'i_sj'	   	=> $isj,
	 					'd_bapb'   	=> $dbapb,
	 					'd_sj'  	 	=> $dsj,
	 					'e_remark'  => $eremark,
            'n_item_no' => $i,
	      		'v_sj'    	=> $vsj
			)
		);
		$this->db->insert('tm_bapb_item');
  }
	function updatesj($ibapb,$isj,$iarea,$dbapb)
    {
    	$this->db->set(
    		array(
			'i_bapb' => $ibapb,	
			'd_bapb' => $dbapb
    		)
    	);
    	$this->db->where('i_sj',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_nota');
    }
   function insertdetailekspedisi($ibapb,$iarea,$iekspedisi,$dbapb,$eremark)
   {
		$this->db->query("DELETE FROM tm_bapb_ekspedisi WHERE i_bapb='$ibapb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
		$this->db->set(
			array(
						'i_bapb'			=> $ibapb,
						'i_area'			=> $iarea,
	 					'i_ekspedisi'	=> $iekspedisi,
	 					'd_bapb' 			=> $dbapb,
						'e_remark'		=> $eremark
			)
		);
		$this->db->insert('tm_bapb_ekspedisi');
  }
    function updateheader($ispmb, $dspmb, $iarea, $ispmbold)
    {
    	$this->db->set(
    		array(
			'd_spmb'	=> $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	=> $iarea
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }
    public function deletedetail($ibapb, $iarea, $isj) 
    {
		$this->db->select(" v_sj from tm_bapb_item where i_bapb = '$ibapb' and i_area='$iarea' and i_sj='$isj'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
        $this->db->query("update tm_bapb set v_bapb=v_bapb WHERE i_bapb='$ibapb' and i_area='$iarea'");
				$this->db->query("update tm_nota set i_bapb=null and d_bapb=null WHERE i_sj='$isj' and i_area='$iarea'");
			}
		}
		$this->db->query("DELETE FROM tm_bapb_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sj='$isj'");
    }
	
    public function deletedetailekspedisi($ibapb, $iarea, $iekspedisi) 
    {
			$this->db->query("DELETE FROM tm_bapb_ekspedisi WHERE i_bapb='$ibapb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
    }

    public function deleteheader($ibapb, $iarea) 
    {
		$this->db->query("DELETE FROM tm_bapb WHERE i_bapb='$ibapb' and i_area='$iarea'");
    }

    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($iarea,$thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BAP'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobapb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nobapb
                            where i_modul='BAP'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nobapb,"string");
			  $a=strlen($nobapb);
			  while($a<4){
			    $nobapb="0".$nobapb;
			    $a=strlen($nobapb);
			  }
		  	$nobapb  ="BAP-".$thbl."-".$iarea.$nobapb;
			  return $nobapb;
		  }else{
			  $nobapb  ="0001";
		  	$nobapb  ="BAP-".$thbl."-".$iarea.$nobapb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BAP','$iarea','$asal',1)");
			  return $nobapb;
		  }
/*
			$th			= substr($thbl,0,2);
			$this->db->select(" max(substr(i_bapb,11,6)) as max from tm_bapb where substr(i_bapb,6,2)='$th' and i_area='$iarea'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				foreach($query->result() as $row){
					$terakhir=$row->max;
				}
				$nobapb  =$terakhir+1;
				settype($nobapb,"string");
				$a=strlen($nobapb);
				while($a<6){
					$nobapb="0".$nobapb;
					$a=strlen($nobapb);
				}
				$nobapb  ="BAPB-".$thbl."-".$nobapb;
				return $nobapb;
			}else{
				$nobapb  ="000001";
				$nobapb  ="BAPB-".$thbl."-".$nobapb;
				return $nobapb;
			}
*/
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$iuser)
    {
    	$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$iuser)
    {
   		$this->db->select("	* from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser') 
							and (upper(e_area_name) ilike '%$cari%' or i_area ilike '%$cari%')
							order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakirim($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
			$this->db->select("* from tr_dkb_kirim order by i_dkb_kirim",false)->limit($num,$offset);
    }else{
			$this->db->select("* from tr_dkb_kirim where i_dkb_kirim='1' order by i_dkb_kirim",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikirim($cari,$num,$offset)
    {
		$this->db->select("i_dkb_kirim, e_dkb_kirim from tr_dkb_kirim where upper(e_dkb_kirim) like '%$cari%' or upper(i_dkb_kirim) like '%$cari%' order by i_dkb_kirim ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacavia($num,$offset)
    {
		$this->db->select("* from tr_dkb_via order by i_dkb_via", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carivia($cari,$num,$offset)
    {
		$this->db->select("i_dkb_via, e_dkb_via from tr_dkb_via where upper(e_dkb_via) like '%$cari%' or upper(i_dkb_via) like '%$cari%' order by i_dkb_via ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaekspedisi($num,$offset)
    {
		$this->db->select("* from tr_ekspedisi order by i_ekspedisi", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariekspedisi($cari,$num,$offset)
    {
		$this->db->select("i_ekspedisi, e_ekspedisi from tr_ekspedisi where upper(e_ekspedisi) like '%$cari%' or upper(i_ekspedisi) like '%$cari%' order by i_ekspedisi ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacasj($cari,$customer,$iarea,$num,$offset,$iareasj,$area2,$area3,$area4,$area5)
  {
	  $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b 
		                    where a.i_bapb isnull and a.i_area='$iarea' and (upper(a.i_sj) like '%$cari%')
		                    and a.i_bapb is NULL and (substr(a.i_sj,9,2) like '%$iareasj%' or substr(a.i_sj,9,2) like '%$area2%' or 
                        substr(a.i_sj,9,2) like '%$area3%' or substr(a.i_sj,9,2) like '%$area4%' or substr(a.i_sj,9,2) like '%$area5%')
                        and a.i_customer='$customer'
		                    and a.i_customer=b.i_customer order by a.d_sj, a.i_sj", false)->limit($num,$offset);          
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function bacasj2($cari,$iarea,$num,$offset,$iareasj,$area2,$area3,$area4,$area5)
  {
		  $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b 
			                    where a.i_bapb isnull and a.i_area='$iarea' and (substr(a.i_sj,9,2) like '%$iareasj%' 
                          or substr(a.i_sj,9,2) like '%$area2%' or substr(a.i_sj,9,2) like '%$area3%' or substr(a.i_sj,9,2) like '%$area4%' 
                          or substr(a.i_sj,9,2) like '%$area5%') and (upper(a.i_sj) like '%$cari%')
			                    and a.i_customer=b.i_customer order by a.d_sj, a.i_sj", false)->limit($num,$offset);		  
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function carisj($customer,$iarea,$cari,$num,$offset)
    {
    $area1	= $this->session->userdata('i_area');
	  if($customer!=''){
      if($area1=='00'){
			/* Disabled 21042011
		    $this->db->select("a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$iarea' 
				    and a.i_customer='$customer' and a.i_bapb isnull and a.f_sj_daerah='f'
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);
			*/
		    $this->db->select("a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where (a.i_nota isnull or a.i_bapb isnull) and a.i_bapb isnull and a.i_area_to='$iarea' 
				    and a.i_customer='$customer' and a.f_sj_daerah='f'
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);			
#				    and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$iarea')
      }else{
		    $this->db->select("a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where (a.i_nota isnull or a.i_bapb isnull) and a.i_bapb isnull  and a.i_area_to='$iarea' 
				    and a.i_customer='$customer' and a.f_sj_daerah='t'
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);
#				    and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$iarea')
      }
	  }else{
      if($area1=='00'){
			/*
		    $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$iarea'
            and a.i_bapb isnull and a.f_sj_daerah='f' 
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);
			*/
		    $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where (a.i_nota isnull or a.i_bapb isnull) and a.i_bapb isnull  and a.i_area_to='$iarea'
				and a.f_sj_daerah='f' 
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);			
#				    and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$iarea')
      }else{
		    $this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b 
				    where (a.i_nota isnull or a.i_bapb isnull) and a.i_bapb isnull  and a.i_area_to='$iarea' 
                and a.f_sj_daerah='t'
		       	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_customer) like '%$cari%')
				    and a.i_customer=b.i_customer order by a.i_sj ", FALSE)->limit($num,$offset);
#				    and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$iarea')
      }
	  }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							where a.i_area='$iarea' and
							(upper(a.i_customer) ilike '%$cari%' or upper(a.e_customer_name) ilike '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
