<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }

    function runningnumber($iarea,$thbl){
        $th   = substr($thbl,0,4);
        $asal=$thbl;
        $thbl=substr($thbl,2,2).substr($thbl,4,2);
          $this->db->select(" n_modul_no as max from tm_dgu_no
                            where i_modul='SPB'
                            and substr(e_periode,1,4)='$th'
                            and i_area='$iarea' for update", false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
             foreach($query->result() as $row){
               $terakhir=$row->max;
             }
             $nospb  =$terakhir+1;
          $this->db->query(" update tm_dgu_no
                              set n_modul_no=$nospb
                              where i_modul='SPB'
                              and substr(e_periode,1,4)='$th'
                              and i_area='$iarea'", false);
             settype($nospb,"string");
             $a=strlen($nospb);
             while($a<6){
               $nospb="0".$nospb;
               $a=strlen($nospb);
             }
             $nospb  ="SPB-".$thbl."-".$nospb;
             return $nospb;
          }else{
             $nospb  ="000001";
             $nospb  ="SPB-".$thbl."-".$nospb;
          $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                             values ('SPB','$iarea','$asal',1)");
  
  
             return $nospb;
          }
      }

      function insertheader($ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman,
      $ipricegroup, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
      $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
      $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
      $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
      $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold,$eremarkx,$ispbadvo,$f_status_transfer, $i_product_group){
        
        $query   = $this->db->query("SELECT current_timestamp as c");
        $row     = $query->row();
        $dentry  = $row->c;
        $this->db->set(
            array(
            'i_spb'              => $ispb,
            'd_spb'              => $dspb,
            'i_customer'         => $icustomer,
            'i_area'             => $iarea,
            'i_spb_po'           => $ispbpo,
            'n_spb_toplength'    => $nspbtoplength,
            'i_salesman'         => $isalesman,
            'i_price_group'      => $ipricegroup,
            'd_spb_receive'      => $dspb,
            'f_spb_op'           => $fspbop,
            'e_customer_pkpnpwp' => $ecustomerpkpnpwp,
            'f_spb_pkp'          => $fspbpkp,
            'f_spb_plusppn'      => $fspbplusppn,
            'f_spb_plusdiscount' => $fspbplusdiscount,
            'f_spb_stockdaerah'  => $fspbstockdaerah,
            'f_spb_program'      => $fspbprogram,
            'f_spb_valid'        => $fspbvalid,
            'f_spb_siapnotagudang' => $fspbsiapnotagudang,
            'f_spb_cancel'       => $fspbcancel,
            'n_spb_discount1'    => $nspbdiscount1,
            'n_spb_discount2'    => $nspbdiscount2,
            'n_spb_discount3'    => $nspbdiscount3,
            'v_spb_discount1'    => $vspbdiscount1,
            'v_spb_discount2'    => $vspbdiscount2,
            'v_spb_discount3'    => $vspbdiscount3,
            'v_spb_discounttotal'=> $vspbdiscounttotal,
            'v_spb'              => $vspb,
            'f_spb_consigment'   => $fspbconsigment,
            'd_spb_entry'        => $dentry,
            'i_spb_old'          => $ispbold,
            'i_product_group'    => $i_product_group,
            'e_remark1'          => $eremarkx,
            'i_reff_advo'        => $ispbadvo,
            'f_status_transfer'  => $f_status_transfer
            )
        );
        $this->db->insert('tm_spb');
        }

        function insertdetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,$iproductmotif,$eremark,$i)
        {
          if($eremark=='') $eremark=null;
          $this->db->set(
             array(
                   'i_spb'           => $ispb,
                   'i_area'          => $iarea,
                   'i_product'       => $iproduct,
                   'i_product_status'=> $iproductstatus,
                   'i_product_grade' => $iproductgrade,
                   'i_product_motif' => $iproductmotif,
                   'n_order'         => $norder,
                   'n_deliver'       => $ndeliver,
                   'v_unit_price'    => $vunitprice,
                   'e_product_name'  => $eproductname,
                   'e_remark'        => $eremark,
              'n_item_no'            => $i
             )
          );
          $this->db->insert('tm_spb_item');
        }

        function updatevspb($ispb,$iarea,$total,$nspbdiscount1,$vspbdiscount1,$nspbdiscount2,$vspbdiscount2,$nspbdiscount3,$vspbdiscount3,$vspbdiscounttotal,$ispbadvo)
        {
          $data = array(  'v_spb'               => $total,
                          'n_spb_discount1'     => $nspbdiscount1,
                          'n_spb_discount2'     => $nspbdiscount2,
                          'n_spb_discount3'     => $nspbdiscount3,
                          'v_spb_discount1'     => $vspbdiscount1,
                          'v_spb_discount2'     => $vspbdiscount2,
                          'v_spb_discount3'     => $vspbdiscount3,
                          'v_spb_discounttotal' => $vspbdiscounttotal
                        );
       $this->db->where('i_spb', $ispb);
       $this->db->where('i_area', $iarea);
       $this->db->where('i_reff_advo', $ispbadvo);
       $this->db->update('tm_spb', $data);
        }


}
?>
