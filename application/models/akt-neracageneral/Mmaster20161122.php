<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacakas($periode)
	{

		$query=$this->db->query("select 'Kas & Bank' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								             from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								             and (b.i_coa like '110-1%' or b.i_coa like '110-2%')
								             and a.i_coa=b.i_coa",false);
    $bcajkt=BankPinjaman;
    $bank=Bank;
    $kas=Kas;
/*
	  $query=$this->db->query(" select 'Kas & Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                            sum(b.kredit) as kredit, 
                              (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                              select 0 as v_saldo_awal, sum(a.debet) as debet, sum(a.kredit) as kredit from (
                              select 0 as debet, sum(v_kb) as kredit from tm_kb where i_periode='$periode' and f_kb_cancel='f' 
                              and f_debet='t'
                              union all
                              select sum(v_kb) as debet, 0 as kredit from tm_kb where i_periode='$periode' and f_kb_cancel='f' 
                              and f_debet='f'
                              union all
                              select 0 as debet, sum(v_kk) as kredit from tm_kk where i_periode='$periode' and f_kk_cancel='f' 
                              and f_debet='t'
                              union all
                              select sum(v_kk) as debet, 0 as kredit from tm_kk where i_periode='$periode' and f_kk_cancel='f' 
                              and f_debet='f'
                              union all
                              select 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='t' and i_coa<>'$bcajkt' 
                              union all
                              select sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='f' and i_coa<>'$bcajkt') as a
                              union all
                              select sum(b.v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '$bank%'
                              and  b.i_coa<>'$bcajkt' and a.i_coa=b.i_coa
                              union all
                              select sum(b.v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '$kas%' and a.i_coa=b.i_coa) 
                              as b",false);

*//*
                              union all
                              select 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='t' and i_coa<>'$bcajkt'
                              union all
                              select sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='f' and i_coa<>'$bcajkt') as a

                              select sum(b.v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '111%' 
                              and  b.i_coa<>'$bcajkt' and a.i_coa=b.i_coa) as b",false);
*/
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacapiutang($periode)
	{
/*
		$query=$this->db->query("select 'Piutang Dagang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.2%' 
								 and a.i_coa=b.i_coa",false);
*/
		$query=$this->db->query("select 'Piutang Dagang' as ket, sum(v_nota_netto) as v_saldo_akhir
								             from tm_nota where to_char(d_nota,'yyyymm') = '$periode' and f_nota_cancel='f' and not i_nota isnull",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutanggiromundur($periode)
	{
/*
		$query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.1%' 
								 and a.i_coa=b.i_coa",false);
*/
		$query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(v_jumlah) as v_saldo_akhir
            								 from tm_giro where d_giro_cair isnull and v_jumlah=v_sisa and f_giro_batal='f' and f_giro_tolak='f'
            								 and to_char(d_giro,'yyyymm')<='$periode'",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapossementaradebet($periode)
	{
		$query=$this->db->query("select 'Pos Sementara Debet' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '119.100%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutangkaryawan($periode)
	{
    $PiutKaryawan=PiutKaryawan;
	  $query=$this->db->query(" select 'Piutang Karyawan' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                            sum(b.kredit) as kredit, (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                              select 0 as v_saldo_awal, sum(a.debet) as debet, sum(a.kredit) as kredit from (
                              select 0 as debet, sum(v_kb) as kredit from tm_kb where i_periode='$periode' and f_kb_cancel='f'
                              and i_coa='$PiutKaryawan' and f_debet='t'
                              union all
                              select sum(v_kb) as debet, 0 as kredit from tm_kb where i_periode='$periode' and f_kb_cancel='f' 
                              and i_coa='$PiutKaryawan' and f_debet='f'
                              union all
                              select 0 as debet, sum(v_kk) as kredit from tm_kk where i_periode='$periode' and f_kk_cancel='f' 
                              and i_coa='$PiutKaryawan' and f_debet='t'
                              union all
                              select sum(v_kk) as debet, 0 as kredit from tm_kk where i_periode='$periode' and f_kk_cancel='f' 
                              and i_coa='$PiutKaryawan' and f_debet='f'
                              union all
                              select 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and i_coa='$PiutKaryawan' and f_debet='t'
                              union all
                              select sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and i_coa='$PiutKaryawan' and f_debet='f') as a
                              union all
                              select sum(b.v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa='$PiutKaryawan' and a.i_coa=b.i_coa) 
                              as b",false);

/*
		$query=$this->db->query("select 'Piutang Karyawan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.4%' 
								 and a.i_coa=b.i_coa",false);
*/

		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel1($periode)
	{
		$query=$this->db->query("select 'Aktiva Tetap ' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa = '120-0000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva1($periode)
	{
		$query=$this->db->query("select 'Akum. Peny. Aktiva Tetap' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '130-0000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel2($periode)
	{
		$query=$this->db->query("select 'Aktiva Tetap Kelompok II' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '122.100%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva2($periode)
	{
		$query=$this->db->query("select 'Akum. Peny. Aktiva Tetap Kel. II' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '121.900%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasi($periode)
	{
		$query=$this->db->query("select 'Akum. Amortisasi B. Sewa' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '131.900%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayayangditangguhkan($periode)
	{
		$query=$this->db->query("select 'Biaya Yang Ditangguhkan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '132.100%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasiyangditangguhkan($periode)
	{
		$query=$this->db->query("select 'Akum. Amortisasi Biaya Yang Ditangguhkan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '132.900%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbank($periode)
	{

		$query=$this->db->query("select 'Hutang Bank' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa = '210-2000' 
								 and a.i_coa=b.i_coa",false);

/*
    $bcajkt=BankPinjaman;
	  $query=$this->db->query(" select 'Hutang Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                            sum(b.kredit) as kredit, 
                              ((sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit)) * -1 as v_saldo_akhir from (
                              select 0 as v_saldo_awal, sum(a.debet) as debet, sum(a.kredit) as kredit from (
                              select 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='t' and i_coa='$bcajkt'
                              union all
                              select sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' and f_kbank_cancel='f' 
                              and f_debet='f' and i_coa='$bcajkt') as a
                              union all
                              select sum(b.v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit
                              from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '111%' 
                              and  b.i_coa='$bcajkt' and a.i_coa=b.i_coa) as b",false);
*/
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutanglainlain($periode)
	{
		$query=$this->db->query("select 'Piutang Lain-Lain' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112.3%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayadibayardimuka($periode)
	{
		$query=$this->db->query("select 'Biaya Yang Dibayar Dimuka' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.2%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph21ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 21 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.301%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph22ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 22 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.302%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph23ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 23,26,4,2 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.303%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph25ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 25/29 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.304%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapersediaanbarangdagang($periode)
	{
		$query=$this->db->query("select 'Persediaan Barang Dagangan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa = '110-8000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangdagang($periode)
	{

		$query=$this->db->query("select 'Hutang Dagang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								             from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='210-1000' 
								             and a.i_coa=b.i_coa",false);

/*
		$query=$this->db->query("select 'Hutang Dagang' as ket, sum(v_gross) as v_saldo_akhir
								             from tm_dtap where to_char(d_dtap,'yyyymm') = '$periode' and f_dtap_cancel='f'",false);
*/
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacayhmdibayar($periode)
	{
		$query=$this->db->query("select 'Biaya YHM Dibayar' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='210-6000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph21($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 21 ' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.201%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph22($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 22' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.202%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph23($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh.Ps.23/26,4(2)' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.203%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph24($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 25/29' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.204%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakppn($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak Pertambahan Nilai (PPN)' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.205%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutanglain($periode)
	{
		$query=$this->db->query("select 'Hutang Lain Lain' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='210-5000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacauangmukaleasing($periode)
	{
		$query=$this->db->query("select 'Uang Muka Leasing' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='114.500' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapossementara($periode)
	{
		$query=$this->db->query("select 'Pos Sementara Kredit' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='214.109' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbankjangkapanjang($periode)
	{
		$query=$this->db->query("select 'Hutang Bank Jk. Panjang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='221.100' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacamodalyangdisetor($periode)
	{
		$query=$this->db->query("select 'Modal Yang Disetor' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='300-1000'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='300-2000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahantahunberjalan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan Tahun Berjalan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='300-3000' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahanbulanberjalan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan Bulan Berjalan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='312.300' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacamodal($periode)
	{
		$query=$this->db->query("select 'Modal' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where b.i_periode = '$periode' and a.i_coa like '3%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldogeneral
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
