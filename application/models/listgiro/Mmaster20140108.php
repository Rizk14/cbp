<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($igiro,$iarea) 
    {
			$this->db->query("update tm_giro set f_giro_batal_input='t' WHERE i_giro='$igiro' and i_area='$iarea'");
    }    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_giro, a.i_area, a.i_customer, a.i_rv, a.d_giro, a.d_rv, a.d_giro_duedate, a.d_giro_cair,
                        a.d_giro_tolak, a.e_giro_bank, a.f_giro_tolak, a.f_giro_cair, a.f_giro_batal, a.v_jumlah, a.v_sisa,
                        a.f_posting, a.f_giro_use from tm_giro a 
							          inner join tr_area on(a.i_area=tr_area.i_area)
							          inner join tr_customer on(a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
							          where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							          order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_giro, a.i_area, a.i_customer, a.i_rv, a.d_giro, a.d_rv, a.d_giro_duedate, a.d_giro_cair,
              a.d_giro_tolak, a.e_giro_bank, a.f_giro_tolak, a.f_giro_cair, a.f_giro_batal, a.v_jumlah, a.v_sisa,
              a.f_posting, a.f_giro_use from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
							where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
							order by a.i_area,a.i_giro", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($cari!=''){
        $query="distinct on (a.d_giro, a.i_giro) a.i_giro, tr_area.e_area_name, 
						    a.d_giro, 
						    a.i_rv, 
						    a.d_rv, a.d_giro_duedate,
						    tm_dt.i_dt, 
						    tm_dt.d_dt, 
						    tr_customer.i_customer, 
						    tr_customer.e_customer_name, 
						    a.e_giro_bank, 
						    a.v_jumlah, 
						    a.v_sisa, 
						    a.f_posting, 
						    a.f_giro_batal,	a.f_giro_tolak, a.f_giro_batal_input,
						    a.d_giro_cair,
						    a.i_area, tm_pelunasan.i_pelunasan, a.i_dt as xdt from tm_giro a 
                inner join tr_area on(a.i_area=tr_area.i_area)
							  inner join tr_customer on(a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
							  left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                          and tm_pelunasan.i_jenis_bayar='01' and tm_pelunasan.f_pelunasan_cancel='f')
							  left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							  where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'";
        if(is_numeric($cari)){
          $query.=" or a.v_jumlah=$cari) ";
        }else{
          $query.=" ) ";
        }
        $query.=" and a.i_area='$iarea' and a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and a.d_giro <= to_date('$dto','dd-mm-yyyy')
                ORDER BY a.d_giro, a.i_giro";
        $this->db->select($query,false)->limit($num,$offset);
      }else{
    	$this->db->select("	distinct on (a.d_giro, a.i_giro) a.i_giro, tr_area.e_area_name, 
						  a.d_giro, 
						  a.i_rv, 
						  a.d_rv, a.d_giro_duedate,
						  tm_dt.i_dt, 
						  tm_dt.d_dt, 
						  tr_customer.i_customer, 
						  tr_customer.e_customer_name, 
						  a.e_giro_bank, 
						  a.v_jumlah, 
						  a.v_sisa, 
						  a.f_posting, 
						  a.f_giro_batal,	a.f_giro_tolak, a.f_giro_batal_input,
						  a.d_giro_cair,
						  a.i_area, tm_pelunasan.i_pelunasan, a.i_dt as xdt from tm_giro a 
              inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
							left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area 
                                        and tm_pelunasan.i_jenis_bayar='01' and tm_pelunasan.f_pelunasan_cancel='f')
							left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							where a.i_area='$iarea' and a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and a.d_giro <= to_date('$dto','dd-mm-yyyy')
              ORDER BY a.d_giro, a.i_giro",false)->limit($num,$offset);
      }
#and tm_pelunasan.f_giro_tolak='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("		tr_area.e_area_name, 
						a.i_giro, 
						a.d_giro, 
						a.i_rv, 
						a.d_rv,  a.d_giro_duedate,
						tm_dt.i_dt, 
						tm_dt.d_dt, 
						tr_customer.i_customer, 
						tr_customer.e_customer_name, 
						a.e_giro_bank, 
						a.v_jumlah, 
						a.v_sisa, 
						a.f_posting, 
						a.f_giro_batal,	a.f_giro_tolak, a.f_giro_batal_input,
						a.i_area, tm_pelunasan.i_pelunasan, a.i_dt as xdt from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
							left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area and tm_pelunasan.i_jenis_bayar='01')
							left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%' or a.v_jumlah=$cari)
              and a.i_area='$iarea' and a.d_giro >= to_date('$dfrom','dd-mm-yyyy') and a.d_giro <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.d_giro, a.i_giro",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }    
}
?>
