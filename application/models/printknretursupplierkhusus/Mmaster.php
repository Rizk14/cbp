<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
      parent::__construct();
	#$this->CI =& get_instance();
  }
  function bacakn($num,$offset,$area,$cari)
  {
	  $this->db->select("	i_kn, d_kn, i_area from tm_kn 
                        where i_area='$area' and i_kn_type='01' and upper(i_kn) like '%$cari%'
                        order by i_kn", false)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacasemua($isupplier,$cari,$num,$offset,$dfrom,$dto)
  {
		$this->db->select(" a.*, b.e_supplier_name, b.i_supplier, b.e_supplier_name from tm_bbkretur a, tr_supplier b
                        where a.i_supplier=b.i_supplier
                        and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%' or upper(a.i_bbkretur) like '%$cari%')
                        and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') 
                        and a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
                        and a.i_supplier='$isupplier'
              					order by a.i_bbkretur desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select("	* FROM tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
							ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function baca($ibbk)
    {
		$this->db->select(" a.*, b.e_supplier_name from tm_bbkretur a, tr_supplier b
					              where a.i_supplier=b.i_supplier
					              and i_bbkretur ='$ibbk'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacaall($ibbkretur,$isupplier)
    {
		$this->db->select(" distinct a.*, b.e_supplier_name, b.e_supplier_address, b.f_supplier_ppn, e_supplier_city, b.e_supplier_npwp, d.i_pajak
		  					  ,d.v_netto , d.v_gross, d.v_discount
		  					  from tm_bbkretur a, tr_supplier b, tm_bbkretur_item c 
		  					  left join tm_dtap d on (c.i_dtap=d.i_dtap)
                              where a.i_supplier=b.i_supplier and a.i_bbkretur=c.i_bbkretur
                              and a.i_supplier='$isupplier' and a.i_bbkretur='$ibbkretur'
                              order by a.i_bbkretur", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ibbk)
    {
			$this->db->select("a.*, b.e_product_motifname from tm_bbkretur_item a, tr_product_motif b
						             where a.i_bbkretur = '$ibbk' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						             order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updateheader($ibbkretur, $dbbkretur, $isupplier, $ikn, $dkn, $vbbkretur)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array('i_kn'            => $ikn,
    			'd_kn'            => $dkn,
    			'v_bbkretur'            => $vbbkretur,
          		'd_update'              => $now
    		)
    	);
      $this->db->where("i_bbkretur",$ibbkretur);      
    	$this->db->update('tm_bbkretur');
    }
  function bacabbkretur($ibbkretur)
  {
	  $this->db->select("	i_bbkretur, d_bbkretur from tm_bbkretur 
          						  where i_bbkretur = '$ibbkretur'",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }

  function bacadetailbbkretur($ibbkretur)
  {
		$this->db->select("  * from tm_bbkretur_item
					  inner join tr_product_motif on (tm_bbkretur_item.i_product_motif=tr_product_motif.i_product_motif
					  and tm_bbkretur_item.i_product=tr_product_motif.i_product)
					  where i_bbkretur = '$ibbkretur' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
