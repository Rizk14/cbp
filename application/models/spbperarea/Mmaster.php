<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, c.v_target, a.i_area, b.e_area_name
							from tm_spb a, tr_area b, tm_target c
							where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
							and a.i_area=b.i_area and a.i_area=c.i_area and a.f_spb_cancel='f'
              and to_char(a.d_spb, 'yyyymm')=c.i_periode
							group by c.v_target, a.i_area, b.e_area_name
              order by a.i_area, b.e_area_name, c.v_target",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, c.v_target, a.i_area, b.e_area_name
							from tm_spb a, tr_area b, tm_target c
							where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
							and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
							and a.i_area=b.i_area and a.i_area=c.i_area and a.f_spb_cancel='f'
              and to_char(a.d_spb::timestamp with time zone, 'yyyymm'::text)=c.i_periode
							group by a.i_area, b.e_area_name, c.v_target 
              order by a.i_area, b.e_area_name, c.v_target ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
