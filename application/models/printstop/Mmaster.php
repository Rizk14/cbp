<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idkb) 
    {
//		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
//		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
//		return TRUE;
    }
    function bacasemua($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_stop a, tr_area b
										where a.i_area=b.i_area
										and (upper(a.i_stop) like '%$cari%') and a.i_area='$iarea' and 
										a.d_stop >= to_date('$dfrom','dd-mm-yyyy') AND f_stop_batal='f' and 
										a.d_stop <= to_date('$dto','dd-mm-yyyy')
							           order by a.i_stop desc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_stop a, tr_area b
										where a.i_area=b.i_area
										and (upper(a.i_stop) like '%$cari%') and a.i_area='$iarea' and 
										a.d_stop >= to_date('$dfrom','dd-mm-yyyy') AND f_stop_batal='f' and
										a.d_stop <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							          and substr(a.i_dkb,10,2)='$iarea' and 
							          a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name from tm_dkb a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($istop,$iarea)
    {
		$this->db->select(" a.* , b.e_area_name from tm_stop a
								inner join tr_area b on a.i_area=b.i_area
				                where a.i_stop = '$istop' and a.i_area='$iarea'
				                order by a.i_stop desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($istop,$iarea)
    {
		$this->db->select(" a.* , b.e_customer_name, e_supplier_name from tm_stop_item a
						left join tr_customer b on a.i_customer=b.i_customer
						inner join tr_supplier c on c.i_supplier=a.i_supplier
						where a.i_stop='$istop'
						and substr(a.i_stop,9,2)='$iarea'
            			order by a.i_op desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
