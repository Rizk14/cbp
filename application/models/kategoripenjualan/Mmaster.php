<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }

  function baca($isalescategory)
  {
    $this->db->select('i_sales_category, e_sales_categoryname')->from('tr_product_sales_category')->where('i_sales_category', $isalescategory);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->row();
    }
  }

  function insert($esalescategoryname)
  {
    $this->db->set(
      array(
        'e_sales_categoryname' => $esalescategoryname
      )
    );

    $this->db->insert('tr_product_sales_category');
  }

  function update($isalescategory, $esalescategoryname)
  {
    $data = array(
      'e_sales_categoryname' => $esalescategoryname
    );
    $this->db->where('i_sales_category', $isalescategory);
    $this->db->update('tr_product_sales_category', $data);
  }

  // public function delete($iseri)
  // {
  //   $this->db->query('DELETE FROM tr_product_seri WHERE i_product_seri=\'' . $iseri . '\'');
  //   return TRUE;
  // }

  function bacasemua($num, $offset, $cari)
  {
    $this->db->select("     i_sales_category,
                            e_sales_categoryname
                          FROM
                            tr_product_sales_category
                          WHERE
                            upper(e_sales_categoryname) like '%$cari%'
                          ORDER BY
                            i_sales_category ", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
