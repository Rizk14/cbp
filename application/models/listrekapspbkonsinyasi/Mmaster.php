<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	  public function __construct()
    {
          parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
      $this->db->select(" distinct(a.i_customer), b.e_customer_name, sum(c.n_quantity) as jumlah, a.n_notapb_discount,
                          sum(a.v_notapb_discount) as diskon, sum(a.v_notapb_gross) as kotor, a.i_area
                          from tm_notapb a, tr_customer b, tm_notapb_item c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.i_notapb=c.i_notapb and a.i_customer=c.i_customer and a.i_area=c.i_area
                          and a.f_spb_rekap='t' and not a.i_cek is null
                          and (not a.i_spb is null or trim(a.i_spb)!='')
                          group by a.i_customer, b.e_customer_name, a.n_notapb_discount, a.i_area
                          order by a.i_customer, a.n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadiskon($iperiode)
    {
		  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                          where to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          order by n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
