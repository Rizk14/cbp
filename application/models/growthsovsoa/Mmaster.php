<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	a.i_area, sum(a.custly) as custly,  sum(a.notaly) as notaly, sum(a.custcy)  as custcy, sum(a.notacy) as notacy, 
                          sum(a.custlm) as custlm, sum(a.notalm) as notalm, sum(a.custcm) as custcm, sum(a.notacm) as notacm
                          from
                          (
                          select count(a.i_customer) as custly, sum(a.nota) as notaly, 0 as custcy, 0 as notacy, 0 as custlm, 0 as notalm, 
                          0 as custcm, 0 as notacm, a.i_area from
                          (
                          SELECT distinct on (to_char(d_nota,'yyyymm'), i_area, i_customer)  i_customer, sum(v_nota_netto) as nota, i_area
                          from tm_nota where to_char(d_nota,'yyyy') = '$prevth' and f_nota_cancel='f' and d_nota<='$prevdate'
                          group by i_customer, i_area, to_char(d_nota,'yyyymm')
                          ) as a
                          group by i_area
                          union all
                          select 0 as custly, 0 as notaly, count(a.i_customer) as custcy, sum(a.nota) as notacy, 0 as custlm, 0 as notalm, 
                          0 as custcm, 0 as notacm, a.i_area from
                          (
                          SELECT distinct on (to_char(d_nota,'yyyymm'), i_area, i_customer)  i_customer, sum(v_nota_netto) as nota, i_area
                          from tm_nota where to_char(d_nota,'yyyy') = '$th' and f_nota_cancel='f' and d_nota<='$todate'
                          group by i_customer, i_area, to_char(d_nota,'yyyymm')
                          ) as a
                          group by i_area
                          union all
                          select 0 as custly, 0 as notaly, 0 as custcy, 0 as notacy, count(a.i_customer) as custlm, sum(a.nota) as notalm, 
                          0 as custcm, 0 as notacm, a.i_area from
                          (
                          SELECT distinct on (i_customer, i_area, to_char(d_nota,'yyyymm'))  i_customer, sum(v_nota_netto) as nota, i_area
                          from tm_nota where to_char(d_nota,'yyyymm') = '$prevthbl' and f_nota_cancel='f' and d_nota<='$prevdate'
                          group by i_customer, i_area, to_char(d_nota,'yyyymm')
                          ) as a
                          group by i_area
                          union all
                          select 0 as custly, 0 as notaly, 0 as custcy, 0 as notacy, 0 as custlm, 0 as notalm, count(a.i_customer) as custcm, 
                          sum(a.nota) as notacm, a.i_area from
                          (
                          SELECT distinct on (i_customer, i_area, to_char(d_nota,'yyyymm'))  i_customer, sum(v_nota_netto) as nota, i_area
                          from tm_nota where to_char(d_nota,'yyyymm') = '$thbl' and f_nota_cancel='f' and d_nota<='$todate'
                          group by i_customer, i_area, to_char(d_nota,'yyyymm')
                          ) as a
                          group by i_area
                          ) as a
                          group by a.i_area
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatotal($todate,$prevdate,$th,$prevth,$bl)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	sum(custlm) as custlm, sum(custcm) as custcm, sum(custly) as custly, sum(custcy) as custcy from (
                          select count(a.i_customer) as custlm, 0 as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$prevthbl' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, count(a.i_customer) as custcm, 0 as custly, 0 as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyymm') = '$thbl' and a.f_nota_cancel='f' and a.d_nota<='$todate' 
                          and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          union all
                          select 0 as custlm, 0 as custcm, sum(a.custly) as custly, 0 as custcy from
                          (
                          select count(a.i_customer) as custly from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$prevth' and a.f_nota_cancel='f' and a.d_nota<='$prevdate' and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          union all
                          select 0 as custlm, 0 as custcm, 0 as custly, sum(a.custcy) as custcy from (
                          select count(a.i_customer) as custcy from (
                          SELECT distinct on (a.i_customer, to_char(a.d_nota, 'yyyymm')) a.i_customer, to_char(a.d_nota, 'yyyymm') as peri
                          from tm_nota a
                          where to_char(a.d_nota, 'yyyy') = '$th' and a.f_nota_cancel='f' and a.d_nota<='$todate' and not a.i_nota isnull
                          group by a.i_customer, to_char(a.d_nota, 'yyyymm')
                          ) as a
                          group by peri
                          ) as a
                          ) as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
