<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
		  $this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
		  $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		  $this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							  where a.i_area=b.i_area and a.i_approve1 isnull and f_spmb_cancel='f' and f_spmb_acc='t'
							  and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								   or upper(a.i_spmb) like '%$cari%')
							  order by a.d_spmb, a.i_area, a.i_spmb ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area and a.i_approve1 isnull and f_spmb_cancel='f' and f_spmb_acc='t'
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%')
							order by a.d_spmb, a.i_area, a.i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function baca($ispmb)
    {
		$this->db->select(" * from tm_spmb 
				   inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
				   where i_spmb ='$ispmb'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	function bacadetail($ispmb)
    {
      /*
		$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
				   where a.i_spmb = '$ispmb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.i_product", false);
	   */
		$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
				   where a.i_spmb = '$ispmb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function approve($ispmb,$eapprove1,$user)
    {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'e_approve1'		=> $eapprove1,
					'd_approve1'		=> $dentry,
					'i_approve1'		=> $user
    				 );
    	$this->db->where('i_spmb', $ispmb);
		$this->db->update('tm_spmb', $data); 
    }
}
?>
