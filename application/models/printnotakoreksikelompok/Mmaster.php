<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacanotakoreksi($cari,$num,$offset)
    {
		$this->db->select("i_nota, i_area from tm_notakoreksi where i_nota like '%$cari%' and f_nota_cancel='f' order by i_nota", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   
    function carinotakoreksi($cari,$num,$offset)
    {
		$this->db->select("	i_nota from tm_notakoreksi where i_nota like '%$cari%' and f_nota_cancel='f'order by i_nota",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacanotakoreksito($cari,$num,$offset,$area)
    {
		$this->db->select("i_nota, i_area from tm_notakoreksi where i_nota like '%$cari%' and f_nota_cancel='f' and i_area='$area' order by i_nota", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   
    function carinotakoreksito($cari,$num,$offset,$area)
    {
		$this->db->select("	i_nota from tm_notakoreksi where i_nota like '%$cari%' and f_nota_cancel='f' and i_area='$area' order by i_nota",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacamaster($notakoreksifrom,$notakoreksito)
    {
      $this->db->select("	* from tm_notakoreksi 
                          inner join tr_customer on (tm_notakoreksi.i_customer=tr_customer.i_customer)
                          inner join tr_salesman on (tm_notakoreksi.i_salesman=tr_salesman.i_salesman)
                          left join tr_customer_pkp on (tm_notakoreksi.i_customer=tr_customer_pkp.i_customer)
                          where tm_notakoreksi.i_nota >= '$notakoreksifrom' and tm_notakoreksi.i_nota <= '$notakoreksito' order by tm_notakoreksi.i_nota",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($notakoreksi)
    {
		$this->db->select("	* from tm_notakoreksi_item 
							          inner join tr_product_motif on (tm_notakoreksi_item.i_product_motif=tr_product_motif.i_product_motif
					              and tm_notakoreksi_item.i_product=tr_product_motif.i_product)
					              where tm_notakoreksi_item.i_nota = '$notakoreksi' order by tm_notakoreksi_item.i_nota",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
