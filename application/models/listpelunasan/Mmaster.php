<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ipl,$iarea,$idt)
    {
		  $this->db->select(" * from tm_pelunasan WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'", false);
		  $query = $this->db->get();
		  foreach($query->result() as $row){
        $dpl 	= $row->d_bukti;
			  if($dpl!=''){
				  $tmp=explode("-",$dpl);
				  $th=$tmp[0];
				  $bl=$tmp[1];
				  $hr=$tmp[2];
			  }

        $dgiro 	= $row->d_giro;
			  if($dgiro!=''){
				  $tmp=explode("-",$dgiro);
				  $thx=$tmp[0];
				  $blx=$tmp[1];
				  $hrx=$tmp[2];
			  }

			  $vjml=$row->v_jumlah;
			  $vlbh=$row->v_lebih;
			  $vbyr=$vjml-$vlbh;
        $row->i_giro=trim($row->i_giro);
			  if($row->i_jenis_bayar=='01'){
#          $this->db->query("update tr_customer_groupar set v_saldo=v_saldo+$vbyr where i_customer='$row->i_customer'");
				  $this->db->query("update tm_giro set v_sisa=v_sisa+$vbyr where i_giro='$row->i_giro' and i_area='$iarea'");
				  $this->db->select(" v_jumlah, v_sisa from tm_giro WHERE i_giro='$row->i_giro' and i_area='$iarea'", false);  
				  $query2 = $this->db->get();
				  foreach($query2->result() as $row2){
					  $v_jum=$row2->v_jumlah;
					  $v_sisa=$row2->v_sisa;
					  if($v_jum == $v_sisa) {
						  $this->db->query("update tm_giro set f_giro_use = 'f' where i_giro='$row->i_giro' and i_area='$iarea'");
					  }
				  }
			  }elseif($row->i_jenis_bayar=='04'){
				  $this->db->query("update tm_kn set v_sisa=v_sisa+$vbyr where i_kn='$row->i_giro' and i_area='$iarea'");
        }elseif($row->i_jenis_bayar=='03'){
				  $this->db->query("update tm_kum set v_sisa=v_sisa+$vbyr where i_kum='$row->i_giro' and i_area='$iarea' and n_kum_year=$thx");
			  }elseif($row->i_jenis_bayar=='05'){
#				  $this->db->query("update tm_pelunasan_lebih set v_sisa=v_sisa+$vlbh where i_pelunasan ='$row->i_giro' and i_area = '$iarea'");
				  $this->db->query("update tm_pelunasan_lebih set v_lebih=v_lebih+$vbyr where i_pelunasan ='$row->e_bank_name' and i_area = '$iarea'");
			  }
			  $this->db->query("update tm_pelunasan_lebih set f_pelunasan_cancel='t' WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
        $this->db->query("update tr_customer_groupar set v_saldo=v_saldo+$vbyr where i_customer='$row->i_customer'");
		  }
		  $this->db->select(" * from tm_pelunasan_item WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'", false);
		  $query = $this->db->get();
		  foreach($query->result() as $row){
			  $vtmp=$row->v_jumlah;
#        $row->i_nota=trim($row->i_nota);
			  $this->db->query("update tm_nota set v_sisa=v_sisa+$vtmp where i_nota='$row->i_nota'");# and i_area='$iarea'");
#			  $this->db->query("update tm_dt_item set v_sisa=v_sisa+$vtmp where i_dt='$row->i_dt' and i_area='$iarea' and i_nota='$row->i_nota'");
			  $this->db->query("update tm_dt set f_sisa='t' where i_dt='$row->i_dt' and i_area='$iarea'");
			  $this->db->query("update tm_pelunasan_lebihitem set v_sisa=v_sisa+$vbyr where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt' and i_nota='$row->i_nota'");
		  }
      $query 	= $this->db->query("SELECT current_timestamp as c");
      $row   	= $query->row();
      $dupdate= $row->c;
		  $this->db->query("update tm_pelunasan set f_pelunasan_cancel='t', d_update='$dupdate'
                        WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
#		  $this->db->query("update tm_pelunasan_item set v_sisa=v_sisa+v_jumlah, v_jumlah=0 where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
    }

    function bacasemua($cari, $num, $offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							d.e_jenis_bayarname,e.d_dt
							from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							where 
							a.i_area=b.i_area
							and a.i_customer=c.i_customer
							and a.i_jenis_bayar=d.i_jenis_bayar
							and a.i_dt=e.i_dt and a.i_area=e.i_area
							and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%')
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_pelunasan a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							inner join tr_jenis_bayar on(a.i_jenis_bayar=tr_jenis_bayar.i_jenis_bayar)
							inner join tm_dt on(a.i_dt=tm_dt.i_dt and a.i_area=tm_dt.i_area)
							where upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%'
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							            d.e_jenis_bayarname,e.d_dt, a.i_cek
							            from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							            where 
							            a.i_area=b.i_area
							            and a.i_customer=c.i_customer and a.d_dt=e.d_dt
							            and a.i_jenis_bayar=d.i_jenis_bayar
							            and a.i_dt=e.i_dt and a.i_area=e.i_area
							            and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          or upper(a.i_pelunasan) like '%$cari%')
							            and a.i_area='$iarea' and
							            a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							            a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							            ORDER BY a.d_bukti,a.i_area,a.i_pelunasan ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							  d.e_jenis_bayarname,e.d_dt, a.i_cek
							  from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							  where 
							  a.i_area=b.i_area and a.d_dt=e.d_dt
							  and a.i_customer=c.i_customer
							  and a.i_jenis_bayar=d.i_jenis_bayar
							  and a.i_dt=e.i_dt and a.i_area=e.i_area
							  and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
							  and a.i_area='$iarea' and
							  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  ORDER BY a.d_bukti,a.i_area,a.i_pelunasan ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }

    }
}
?>
