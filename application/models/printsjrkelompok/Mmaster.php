<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasj($cari,$dfrom,$dto,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select("	i_sjr, i_area from tm_sjr where upper(i_sjr) like '%$cari%'
                          and (substring(i_sjr,9,2)='$area1' or substring(i_sjr,9,2)='$area2' or 
                          substring(i_sjr,9,2)='$area3' or substring(i_sjr,9,2)='$area4' or substring(i_sjr,9,2)='$area5') 
                          and d_sjr >= to_date('$dfrom','dd-mm-yyyy') and d_sjr <= to_date('$dto','dd-mm-yyyy')
                          order by i_sjr", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasj2($cari,$dfrom,$dto,$num,$offset,$area)
    {
		  $this->db->select("	i_sjr, i_area from tm_sjr where upper(i_sjr) like '%$cari%'
                          and (substring(i_sjr,9,2)='$area' or substring(i_sjr,9,2)='$area' or 
                          substring(i_srj,9,2)='$area' or substring(i_sjr,9,2)='$area' or substring(i_sjr,9,2)='$area')
                          and d_sjr >= to_date('$dfrom','dd-mm-yyyy') and d_sjr <= to_date('$dto','dd-mm-yyyy') order by i_sjr", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select("	i_sjr, substring(i_sjr,9,2) as i_area from tm_sjr where upper(i_sjr) like '%$cari%'
                          and (substring(i_sjr,9,2)='$area1' or substring(i_sjr,9,2)='$area2' or 
                          substring(i_sjr,9,2)='$area3' or substring(i_sjr,9,2)='$area4' or substring(i_sjr,9,2)='$area5') 
                          order by i_sjr",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj2($cari,$num,$offset,$area)
    {
		  $this->db->select("	i_sjr, substring(i_sjr,9,2) as i_area from tm_sjr where upper(i_sjr) like '%$cari%'
                          and substring(i_sjr,9,2)='$area' order by i_sjr",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacamaster($sjfrom,$sjto)
    {
		  $this->db->select("	tm_sjr.*, tr_customer.e_customer_name, tr_customer.e_customer_address, tr_customer.e_customer_city,
                          tr_customer.e_customer_phone, tr_area.e_area_name from tm_sjr
							            inner join tr_customer on (tm_sjr.i_customer=tr_customer.i_customer)
							            inner join tr_area on (substring(tm_sjr.i_sj,9,2)=tr_area.i_area)
							            where tm_sjr.i_sjr >= '$sjfrom' and tm_sjr.i_sjr <= '$sjto' order by tm_sjr.i_sjr",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($sj,$area)
    {
		  $this->db->select("	* from tm_sjr_item 
							            inner join tr_product_motif on (tm_sjr_item.i_product_motif=tr_product_motif.i_product_motif
							            and tm_sjr_item.i_product=tr_product_motif.i_product)
							            where tm_sjr_item.i_sjr = '$sj' and substring(tm_sjr_item.i_sjr,9,2)='$area'
                          order by tm_sjr_item.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
