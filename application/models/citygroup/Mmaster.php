<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icitygroup,$iarea)
    {
		$this->db->select("a.*, b.e_area_name")->from('tr_city_group a, tr_area b')->where("i_city_group = '$icitygroup' and a.i_area='$iarea' and a.i_area=b.i_area");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($icitygroup, $ecitygroupname, $iarea)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_city_group (i_city_group, e_city_groupname, i_area, d_city_groupentry) values ('$icitygroup', '$ecitygroupname','$iarea', '$dentry')");
	#	redirect('citygroup/cform/');
    }

    function update($icitygroup, $ecitygroupname, $iarea)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_city_group set e_city_groupname = '$ecitygroupname', d_city_groupupdate = '$dupdate' where i_city_group = '$icitygroup' and i_area = '$iarea'");
		#redirect('citygroup/cform/');
    }
	
    public function delete($icitygroup, $iarea) 
    {
		$this->db->query('DELETE FROM tr_city_group WHERE i_city_group=\''.$icitygroup.'\' and i_area=\''.$iarea.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.*, b.e_area_name from tr_city_group a, tr_area b where a.i_area=b.i_area and (upper(b.e_area_name) like '%$cari%' or upper(a.i_city_group) like '%$cari%' or upper(a.e_city_groupname) like '%$cari%') order by a.i_city_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.*, b.e_area_name from tr_city_group a, tr_area b where (upper(a.e_city_groupname) like '%$cari%' or upper(a.i_city_group) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_area) like '%$cari%') and a.i_area=b.i_area order by a.i_city_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%' order by i_area", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
