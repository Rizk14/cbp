<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
		  $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, sum(b.sisa+b.bayar) as total, sum(b.bayar) as realisasi, 0 as totalnon, 
                          0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.sisa+b.bayar) as totalnon, 
                          sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, a.e_salesman_name, 
                          a.i_nota, a.d_nota, 
                          sum(a.total) as total, sum(a.realisasi) as realisasi, sum(a.totalnon) as totalnon, 
                          sum(a.realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, f.i_salesman,  
                          f.e_salesman_name, i_nota, d_nota, 
                          sum(b.sisa+b.bayar) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, f.i_salesman, 
                          f.e_salesman_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, f.i_salesman, 
                          f.e_salesman_name, i_nota, d_nota, 
                          0 as total, 0 as realisasi, sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, f.i_salesman, 
                          f.e_salesman_name, i_nota, d_nota
                          ) as a 
                          group by a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, 
                          a.e_salesman_name, a.i_nota, a.d_nota
                          order by a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detailsales($iarea,$iperiode)
    {
      $this->db->select("	i_area, e_area_name, a.i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.sisa+b.bayar) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          union all
                          select a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name
                          ) as a 
                          group by i_area, e_area_name, a.i_salesman, e_salesman_name
                          order by a.i_area, a.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detaildivisi($iarea,$iperiode)
    {
      $this->db->select("	i_area, e_area_name, i_nota, i_product_group, e_product_groupname, d_jatuh_tempo, sum(total) as total, 
                          sum(realisasi) as realisasi, sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from
      (
      select a.i_area, a.e_area_name, b.i_nota, c.i_product_group, c.e_product_groupname, b.d_jatuh_tempo, 
      b.sisa+b.bayar as total, b.bayar as realisasi, 0 as totalnon, 0 as realisasinon
      from tr_area a
      left join tm_collection_credit b on(a.i_area=b.i_area)
      left join tm_spb e on (b.i_nota=e.i_nota and e.i_area=b.i_area)
      left join tr_product_group c on (c.i_product_group=e.i_product_group)
      where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
      union all
      select a.i_area, a.e_area_name, b.i_nota, c.i_product_group, c.e_product_groupname, b.d_jatuh_tempo, 0 as total, 0 as realisasi, 
      b.sisa+b.bayar as totalnon, b.bayar as realisasinon
      from tr_area a
      left join tm_collection_credit b on(a.i_area=b.i_area)
      left join tm_spb e on (b.i_nota=e.i_nota and e.i_area=b.i_area)
      left join tr_product_group c on (c.i_product_group=e.i_product_group)
      where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
      ) as a 
      group by i_area, e_area_name, a.i_nota, a.i_product_group, a.e_product_groupname, a.d_jatuh_tempo
      order by a.i_area, a.i_product_group, a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detailcetak($iarea,$iperiode)
    {
      $this->db->select("	e_periode, i_area, e_area_name, i_customer, e_customer_name, e_customer_classname, i_nota, d_nota, 
                          sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, sum(realisasinon) as realisasinon 
                          from(
                          select b.e_periode, a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota, sum(b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.e_periode, a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota, sum(b.bayar) as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.e_periode, a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota, 0 as total, 0 as realisasi, sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.e_periode, a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota, 0 as total, 0 as realisasi, sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_customer_class g on (e.i_customer_class=g.i_customer_class)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.e_periode, a.i_area, a.e_area_name, e.i_customer, e_customer_name, g.e_customer_classname, i_nota, 
                          d_nota
                          ) as a 
                          group by e_periode, i_area, e_area_name, i_customer, e_customer_name, e_customer_classname, i_nota, d_nota
                          order by a.i_area, a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cetaksales($iarea,$iperiode)
    {
      $this->db->select("	e_periode,i_area, e_area_name, i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.bayar) as total, 
                          0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          ) as a 
                          group by i_area, e_area_name, i_salesman, e_salesman_name, e_periode
                          order by a.i_area, a.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareas($iperiode)
  {
#	  $this->db->select("	i_area from tr_area where f_area_real = 't' order by i_area",false);
    $this->db->select(" distinct i_area from tm_collection_credit where e_periode='$iperiode' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
    function detailall($iperiode)
    {
      $this->db->select("	a.i_area, a.i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select b.i_area, e.i_salesman, e.e_salesman_name, sum(b.sisa+b.bayar) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tm_collection_credit b
                          inner join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.i_area, e.i_salesman, e_salesman_name
                          union all
                          select b.i_area, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tm_collection_credit b
                          inner join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.i_area, e.i_salesman, e_salesman_name
                          ) as a 
                          group by a.i_area, a.i_salesman, a.e_salesman_name
                          order by a.i_area, a.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
