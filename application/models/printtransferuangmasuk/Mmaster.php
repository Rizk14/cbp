<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ikum,$nkumyear,$iarea) 
    {
			//$this->db->query("DELETE FROM tm_kum WHERE i_kum='$ikum' and n_kum_year='$nkumyear' and i_area='$iarea'");
			$this->db->query("update tm_kum set f_kum_cancel='t' WHERE i_kum='$ikum' and n_kum_year='$nkumyear' and i_area='$iarea'");
    }
    function bacasemua($num,$offset)
    {
		/* Disabled 07042011
		$this->db->select(" * from tm_kum a
							left join tr_customer c on(a.i_customer=c.i_customer)
							left join tr_area d on(a.i_area=d.i_area)
							order by a.n_kum_year desc, a.i_kum", false)->limit($num,$offset);
		*/
					
		
		$this->db->select(" a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea,
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
								
				from tm_kum a
	
				left join tr_customer c on(a.i_customer=c.i_customer)
				left join tr_area d on(a.i_area=d.i_area)
				left join tr_customer_owner e on(a.i_customer=e.i_customer)
				left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area)
				left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)

				where 

				((tm_pelunasan.i_jenis_bayar!='02' and 
				tm_pelunasan.i_jenis_bayar!='01' and 
				tm_pelunasan.i_jenis_bayar!='04' and 
				tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null))
				and a.f_kum_cancel='f'
												
				order by a.n_kum_year desc, a.i_kum", false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
	
		$this->db->select(" a.i_customer AS icustomer, 
					a.d_kum AS dkum, 
					a.f_kum_cancel AS fkumcancel, 
					a.i_kum AS ikum, 
					a.d_kum AS dkum, 
					a.e_bank_name AS ebankname,
					c.e_customer_name AS ecustomername,
					e.e_customer_setor AS ecustomersetor,
					a.e_remark AS eremark,
					a.v_jumlah AS vjumlah,
					a.v_sisa AS vsisa,
					a.f_close AS fclose,
					a.n_kum_year AS nkumyear,
					d.i_area AS iarea,
					tm_dt.i_dt AS idt, 
					tm_dt.d_dt AS ddt, 
					tm_pelunasan.i_pelunasan AS ipelunasan, 
					tm_pelunasan.i_giro AS igiro
									
					from tm_kum a
				
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							
					where upper(a.i_kum) like '%$cari%' and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null))
					
					and a.f_kum_cancel='f'
														
					order by a.n_kum_year desc, a.i_kum", false)->limit($num,$offset);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {				

		$this->db->select("	a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea,
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
								
				from tm_kum a
				
				left join tr_customer c on(a.i_customer=c.i_customer)
				left join tr_area d on(a.i_area=d.i_area)
				left join tr_customer_owner e on(a.i_customer=e.i_customer)
				left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area)
				left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							
				where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%') and
				((tm_pelunasan.i_jenis_bayar!='02' and 
				tm_pelunasan.i_jenis_bayar!='01' and 
				tm_pelunasan.i_jenis_bayar!='04' and 
				tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and 
				a.i_area='$iarea' and
				(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
				a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f'

				ORDER BY a.d_kum desc, a.i_area,a.d_kum ",false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cetakku($iarea,$dfrom,$dto,$cari)
    {				

		$this->db->select("	a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea,
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
								
				from tm_kum a
				
				left join tr_customer c on(a.i_customer=c.i_customer)
				left join tr_area d on(a.i_area=d.i_area)
				left join tr_customer_owner e on(a.i_customer=e.i_customer)
				left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area)
				left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							
				where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%') and
				((tm_pelunasan.i_jenis_bayar!='02' and 
				tm_pelunasan.i_jenis_bayar!='01' and 
				tm_pelunasan.i_jenis_bayar!='04' and 
				tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and 
				a.i_area='$iarea' and
				(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
				a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f'

				ORDER BY a.d_kum desc, a.i_area,a.d_kum ",false);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }    
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {					

		$this->db->select("	a.i_customer AS icustomer, 
					a.d_kum AS dkum, 
					a.f_kum_cancel AS fkumcancel, 
					a.i_kum AS ikum, 
					a.d_kum AS dkum, 
					a.e_bank_name AS ebankname,
					c.e_customer_name AS ecustomername,
					e.e_customer_setor AS ecustomersetor,
					a.e_remark AS eremark,
					a.v_jumlah AS vjumlah,
					a.v_sisa AS vsisa,
					a.f_close AS fclose,
					a.n_kum_year AS nkumyear,
					d.i_area AS iarea,
					tm_dt.i_dt AS idt, 
					tm_dt.d_dt AS ddt, 
					tm_pelunasan.i_pelunasan AS ipelunasan, 
					tm_pelunasan.i_giro AS igiro
									
					from tm_kum a
					
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
							
					where (upper(a.i_kum) like '%$cari%') and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and							
					a.i_area='$iarea' and
					(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f'

					ORDER BY a.d_kum desc, a.i_area,a.d_kum ",false)->limit($num,$offset); 

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
