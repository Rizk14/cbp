<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_op WHERE i_op=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_op_item WHERE i_op=\''.$iop.'\'');
			return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto)
    {
		$this->db->select(" a.*, c.e_area_name from tm_sjr a, tr_area c
							          where substring(a.i_sjr,10,2)=c.i_area
							          and (upper(a.i_sjr) like '%$cari%')
							          and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2) = '$area2' or substring(a.i_sjr,10,2) = '$area3' 
							          or substring(a.i_sjr,10,2) = '$area4' or substring(a.i_sjr,10,2) = '$area5') 
												and a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjr <= to_date('$dto','dd-mm-yyyy')
												order by a.i_sjr desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($isj,$area)
    {
		  $this->db->select(" tm_sjr.*, tr_area.e_area_name
                          from tm_sjr
							            inner join tr_area on (substring(tm_sjr.i_sjr,10,2)=tr_area.i_area)
							            where tm_sjr.i_sjr = '$isj' and substring(tm_sjr.i_sjr,10,2)='$area'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($isj,$area)
    {
		  $this->db->select(" * from tm_sjr_item
							            inner join tr_product_motif on (tm_sjr_item.i_product_motif=tr_product_motif.i_product_motif
														            and tm_sjr_item.i_product=tr_product_motif.i_product)
							            where i_sjr = '$isj' and substring(i_sjr,10,2)='$area' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto)
    {
		  $this->db->select(" a.*, c.e_area_name from tm_sjr a, tr_area c
							            where substring(a.i_sjr,10,2)=c.i_area
							            and (upper(a.i_sjr) like '%$cari%') and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2) = '$area2' 
												  or substring(a.i_sjr,10,2) = '$area3' or substring(a.i_sjr,10,2) = '$area4' or substring(a.i_sjr,10,2) = '$area5')
												  and a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjr <= to_date('$dto','dd-mm-yyyy')
							            order by a.i_sjr desc",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
