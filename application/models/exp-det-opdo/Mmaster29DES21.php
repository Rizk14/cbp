<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
    function bacasupplier($num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier 
				     where upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%' order by i_supplier", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function esupname($supp) 
      {
		  return $this->db->query(" SELECT e_supplier_name FROM tr_supplier WHERE i_supplier='$supp' ");
	  }
	
	  function bacaperiode($isupplier, $dfrom, $dto,$num,$offset,$cari){
		if($isupplier == 'AS'){
		$this->db->select("a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
		(b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
				case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
				case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
				left join tm_op_item b on (a.i_op=b.i_op)
				inner join tr_supplier c on (a.i_supplier=c.i_supplier)
			where a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND a.d_op <= to_date('$dto','dd-mm-yyyy')
			and a.f_op_cancel='f' order by a.i_op",false)->limit($num,$offset);
		}else{
	
		$this->db->select("a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
		(b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
				case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
				case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
				left join tm_op_item b on (a.i_op=b.i_op)
				inner join tr_supplier c on (a.i_supplier=c.i_supplier)
			where a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND a.d_op <= to_date('$dto','dd-mm-yyyy')
			and a.i_supplier='$isupplier' and a.f_op_cancel='f' order by a.i_op",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
		return $query;
		}
	  }
}
?>
