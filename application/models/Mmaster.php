<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaspb($iperiode)
    {
		  $this->db->select("	sum(a.v_spb) as v_spb_gross from tm_spb a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_spb,'yyyymm') = '$iperiode' and a.f_spb_cancel='f'
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasj($iperiode)
    {
		  $this->db->select("	sum(v_nota_gross) as v_sj_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f'
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacanota($iperiode)
    {
		  $this->db->select("	sum(v_nota_gross) as v_nota_gross from tm_nota a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.f_nota_cancel='f' and not a.i_nota isnull
                          group by b.i_area order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacatarget($iperiode)
    {
		  $this->db->select("	a.v_target from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($iperiode)
    {
		  $this->db->select("	b.i_area from tm_target a
                          inner join tr_area b on(a.i_area=b.i_area)
                          where a.i_periode = '$iperiode'
                          order by b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function getmenu($username){
    $this->db->select("  a.*, b.i_menu as submenu, b.e_menu, b.e_menu_url, b.n_menu_level, b.i_parent, b.n_urut, case
                         when (b.f_aktif = 't' and a.f_status = 't') then true else false end as f_menu_aktif
                         from tm_user_menu a
                         inner join tr_menu b on (a.i_menu = b.i_menu)
                         where a.i_user = '$username' and b.f_aktif = 't' and a.f_status = 't'
                         order by b.n_urut asc",false);
        $query = $this->db->get();
        return (count($query->result()) > 0) ? $query->result() : 'zero';
     }
     function _taxvalue($dreference){
		$this->db->select("*, n_tax / 100 + 1 excl_divider, n_tax / 100 n_tax_val
			from tr_tax_amount
			where cast('$dreference' as date) between d_start and coalesce(d_finish,cast(now() as date))
			and f_active='t'",false);
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? json_encode($query->row()) : 10;
	}
}
?>
