<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacakaskecil($periode)
	{
		$kaskecil=KasKecil;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Kas Kecil' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$kaskecil%' and i_periode='$periode'",false);	  
	  }else{
	    $query=$this->db->query(" select 'Kas Kecil' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$kaskecil%' 
                                and i_periode='$periode'
                                union all
                                select 0 as v_saldo_awal, 0 as debet, sum(v_kk) as kredit from tm_kk where i_periode='$periode' 
                                and f_kk_cancel='f' and f_debet='t'
                                union all
                                select 0 as v_saldo_awal, sum(v_kk) as debet, 0 as kredit from tm_kk where i_periode='$periode' 
                                and f_kk_cancel='f' and f_debet='f')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacakasbesar($periode)
	{
		$kasbesar=KasBesar;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Kas Besar' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$kasbesar%' and i_periode='$periode'",false);	  
	  }else{
	    $query=$this->db->query(" select 'Kas Besar' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$kasbesar' 
                                and i_periode='$periode'
                                union all
                                select 0 as v_saldo_awal, 0 as debet, sum(v_kb) as kredit from tm_kb where i_periode='$periode' 
                                and f_kb_cancel='f' and f_debet='t'
                                union all
                                select 0 as v_saldo_awal, sum(v_kb) as debet, 0 as kredit from tm_kb where i_periode='$periode' 
                                and f_kb_cancel='f' and f_debet='f')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacabank($periode)
	{
    $bcajkt=BankPinjaman;
    $bank=Bank;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Bank' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$bank%' and i_coa<>'$bcajkt' and i_periode='$periode'",false);	  
	  }else{
	    $query=$this->db->query(" select 'Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacabankbcacmh($periode)
	{

    $bank=BCACMH;
    $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
    	$query=$this->db->query("select 'BCA Cimahi' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
	  							             from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
	  							             and (b.i_coa like '$bank%') and a.i_coa=b.i_coa",false);
	  }else{
	    $query=$this->db->query(" select 'BCA Cimahi' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
	  }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacabankbcajkt($periode)
	{
/*    $bank=BCAJKT;
    $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'BCA : 806 090 8070 BCA Jakarta' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								               and (b.i_coa like '$bank%') and a.i_coa=b.i_coa",false);
    }else{
	    $query=$this->db->query(" select 'Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
*/
	}
	function bacabankbcadago($periode)
	{
    $bank=BCADAGO;
    $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'BCA Dago' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								               and (b.i_coa like '$bank%') and a.i_coa=b.i_coa",false);
    }else{
	    $query=$this->db->query(" select 'BCA Dago' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacabankbri($periode)
	{
    $bank=BRIBDG;
    $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'BRI Bandung' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								               and (b.i_coa like '$bank%') and a.i_coa=b.i_coa",false);
    }else{
	    $query=$this->db->query(" select 'BRI Bandung' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacabankpermata($periode)
	{
    $bank=PERMATA;
    $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'Permata Bandung' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								               and (b.i_coa like '$bank%') and a.i_coa=b.i_coa",false);
    }else{
	    $query=$this->db->query(" select 'Permata Bandung' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                (sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit) as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_coa like '$bank%' 
                                and i_periode='$periode' and i_coa<>'$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank like '$bank%' and i_coa_bank<>'$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}

	function bacaasskendbyrdmk($periode)
	{
	  $ass='110-7300';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'Assuransi Kendaraan Dibayar Dimuka' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' 
								               and (b.i_coa like '$ass') and a.i_coa=b.i_coa",false);
	  }else{
		  $query=$this->db->query(" select 'Assuransi Kendaraan Dibayar Dimuka' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$ass%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacapiutang($periode)
	{
    $pj=PiutangDagang;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Piutang Dagang' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and not i_coa like '%SM' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Piutang Dagang' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and not i_coa like '%SM' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacapajakbyrdmk($periode)
	{
    $pj='110-6000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'PAJAK DIBAYAR DIMUKA' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'PAJAK DIBAYAR DIMUKA' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacasewabyrdmk($periode)
	{
    $pj='110-7200';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'SEWA DIBAYAR DIMUKA' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'SEWA DIBAYAR DIMUKA' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutanglain($periode)
	{
    if($periode=='201601'){
  		$query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '110-43%' 
								               and a.i_coa=b.i_coa",false);
    }else{
		  $query=$this->db->query("select 'Piutang Giro Mundur' as ket, sum(v_jumlah) as v_saldo_akhir
              								 from tm_giro where ((d_giro_cair isnull and v_jumlah=v_sisa) or to_char(d_giro_cair,'yyyymm')>'$periode')
              								 and f_giro_batal='f' and f_giro_batal_input='f' and f_giro_tolak='f' 
              								 and to_char(d_giro,'yyyymm')<='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangsementara($periode)
	{
	  $sem='210-11SM';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
  		$query=$this->db->query("select 'Hutang dagang Sementara' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
							                 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '$sem'
							                 and a.i_coa=b.i_coa",false);
	  }else{
		  $query=$this->db->query(" select 'Hutang dagang Sementara' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$sem%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
	}
		function bacapiutangsementara($periode)
	{
	  $sem='110-41SM';
 	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
   		$query=$this->db->query("select 'Piutang Sementara (mobil)' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
							                 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '$sem'
							                 and a.i_coa=b.i_coa",false);
	  }else{
		  $query=$this->db->query(" select 'Piutang Sementara (mobil)' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$sem%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
	}
		function bacapossementaradebet($periode)
	{
	  $sem='610-3000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'Pos Sementara Debet' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa like '$sem%' 
								               and a.i_coa=b.i_coa",false);
	  }else{
		  $query=$this->db->query(" select 'Pos Sementara Debet' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$sem%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapiutangkaryawan($periode)
	{
	  $pj=PiutKaryawan;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Piutang Karyawan' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and not i_coa like '%SM' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Piutang Karyawan' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and not i_coa like '%SM' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel1($periode)
	{
    $pj='120-0000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Aktiva Tetap' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Aktiva Tetap' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva1($periode)
	{
    $pj='130-0000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Akum. Peny. Aktiva Tetap' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Akum. Peny. Aktiva Tetap' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaaktivatetapkel2($periode)
	{
		$query=$this->db->query("select 'Aktiva Tetap Kelompok II' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '122.100%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumaktiva2($periode)
	{
		$query=$this->db->query("select 'Akum. Peny. Aktiva Tetap Kel. II' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '121.900%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasi($periode)
	{
		$query=$this->db->query("select 'Akum. Amortisasi B. Sewa' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '131.900%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayayangditangguhkan($periode)
	{
    $pj='120-xxxx';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Biaya Yang Ditangguhkan' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Biaya Yang Ditangguhkan' as ket, sum(v_mutasi_debet) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }

		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacaakumamortisasiyangditangguhkan($periode)
	{
    $pj='120-xxxx';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Akum. Amortisasi Biaya Yang Ditangguhkan' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Akum. Amortisasi Biaya Yang Ditangguhkan' as ket, sum(v_mutasi_debet) as v_saldo_akhir 
		                            from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbank($periode)
	{
	  $bcajkt=BankPinjaman;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query("select 'Hutang Bank' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								               from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and b.i_coa = '210-2000' 
								               and a.i_coa=b.i_coa",false);
    }else{
	    $query=$this->db->query(" select 'Hutang Bank' as ket, sum(b.v_saldo_awal) as v_saldo_awal, sum(b.debet) as debet, 
	                              sum(b.kredit) as kredit, 
                                ((sum(b.v_saldo_awal) + sum(b.debet)) - sum(b.kredit))*-1 as v_saldo_akhir from (
                                select sum(v_saldo_awal) as v_saldo_awal, 0 as debet, 0 as kredit from tm_coa_saldo where i_periode='$periode' 
                                and i_coa='$bcajkt'
                                union all                              
                                select 0 as v_saldo_awal, 0 as debet, sum(v_bank) as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='t' and i_coa_bank='$bcajkt'
                                union all
                                select 0 as v_saldo_awal, sum(v_bank) as debet, 0 as kredit from tm_kbank where i_periode='$periode' 
                                and f_kbank_cancel='f' and f_debet='f' and i_coa_bank='$bcajkt')as b",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacabiayadibayardimuka($periode)
	{
    $pj='110-7000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Biaya Yang Dibayar Dimuka' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Biaya Yang Dibayar Dimuka' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph21ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 21 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.301%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph22ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 22 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.302%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph23ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 23,26,4,2 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.303%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapajakpph25ydm($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Pajak PPh. Ps. 25/29 YDM' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '114.304%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapersediaanbarangdagang($periode)
	{
    $hpp=HPP;
		$query=$this->db->query(" select v_mutasi_debet as kredit from tm_general_ledger 
		                          where i_coa ='$hpp' and to_char(d_mutasi,'yyyymm')='$periode'",false);
		if ($query->num_rows() > 0){
		  $total=0;
			foreach($query->result() as $kotor){
			  $total=$total+$kotor->kredit;
			}
			if($total==0){
			  $query=$this->db->query("select * from tm_hpp where e_periode='$periode'",false);
		    if ($query->num_rows() > 0){
		      $total=0;
		      $grandtotal=0;
			    foreach($query->result() as $kotor){
			      $total=($kotor->n_opname_total)*$kotor->v_harga;
            $grandtotal=$grandtotal+$total;
			    }
			    return $grandtotal;
		    }
			}else{
  			return $total;
  	  }
		}else{
		  $query=$this->db->query("select * from tm_hpp where e_periode='$periode'",false);
		  if ($query->num_rows() > 0){
		    $total=0;
		    $grandtotal=0;
			  foreach($query->result() as $kotor){
			    $total=($kotor->n_opname_total)*$kotor->v_harga;
          $grandtotal=$grandtotal+$total;
			  }
			  return $grandtotal;
		  }
		}
	}
		function bacahutangdagang($periode)
	{
    $pj=HutangDagang;
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Hutang Dagang' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Hutang Dagang' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and not i_coa like '%SM' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacayhmdibayar($periode)
	{
/*
		$query=$this->db->query("select 'Biaya YHM Dibayar' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='210-6000'
								 and a.i_coa=b.i_coa",false);
*/
    $pj='210-6000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Biaya YHM Dibayar' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Biaya YHM Dibayar' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and not i_coa like '%SM' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajak($periode)
	{
/*
		$query=$this->db->query("select 'Hutang Pajak' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='210-4000' 
								 and a.i_coa=b.i_coa",false);
*/
    $pj='210-4000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Hutang Pajak' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Hutang Pajak' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and not i_coa like '%SM' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}

		function bacahutangpajakpph21($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 21 ' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.201%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph22($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 22' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.202%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph23($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh.Ps.23/26,4(2)' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.203%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakpph24($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak PPh. Ps. 25/29' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.204%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangpajakppn($periode)
	{
		$query=$this->db->query("select '&nbsp;&nbsp;&nbsp;Hutang Pajak Pertambahan Nilai (PPN)' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and i_coa_group='2' and b.i_coa like '213.205%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutanglain($periode)
	{
/*
		$query=$this->db->query("select 'Hutang Lain Lain' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='210-5000' 
								 and a.i_coa=b.i_coa",false);
*/
    $pj='210-5000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Hutang Lain Lain' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Hutang Lain Lain' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacauangmukaleasing($periode)
	{
    $pj='110-5002';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Uang Muka Pembelian (DP Leasing)' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Uang Muka Pembelian (DP Leasing)' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacapossementara($periode)
	{
/*
		$query=$this->db->query("select 'Pos Sementara Kredit' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='610-3000' 
								 and a.i_coa=b.i_coa",false);
*/
    $pj='610-3000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Pos Sementara Kredit' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Pos Sementara Kredit' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacahutangbankjangkapanjang($periode)
	{
/*
		$query=$this->db->query("select 'Hutang Bank Jk. Panjang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='220-0000' 
								 and a.i_coa=b.i_coa",false);
*/
    $pj='220-0000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Hutang Bank Jk. Panjang' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Hutang Bank Jk. Panjang' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacamodalyangdisetor($periode)
	{
/*
		$query=$this->db->query("select 'Modal Yang Disetor' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='300-1000'
								 and a.i_coa=b.i_coa",false);
*/
    $pj='300-1000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Modal Yang Disetor' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Modal Yang Disetor' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahan($periode)
	{
/*
		$query=$this->db->query("select 'Laba/Rugi Yang Ditahan' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where i_periode = '$periode' and a.i_coa='300-2000' 
								 and a.i_coa=b.i_coa",false);
*/
    $pj='300-2000';
	  if($periode=='201601' || $periode=='201602' || $periode=='201603' || $periode=='201604' || $periode=='201605'){
		  $query=$this->db->query(" select 'Laba/Rugi Yang Ditahan' as ket, v_saldo_akhir from tm_coa_saldogeneral
                                where i_coa like '$pj%' and i_periode='$periode'",false);	  
	  }else{
		  $query=$this->db->query(" select 'Laba/Rugi Yang Ditahan' as ket, sum(v_mutasi_kredit) as v_saldo_akhir from tm_general_ledger
                                where i_coa like '$pj%' and to_char(d_mutasi,'yyyymm')='$periode'",false);
    }
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
		function bacalabarugiyangditahantahunberjalan($periode)
	{
	  $tahun=substr($periode,0,4);
		$query=$this->db->query("select 'Laba/Rugi Tahun Berjalan' as ket, sum(v_lr) as v_saldo_akhir
            								 from tm_lr_general where i_periode < '$periode' and substring(i_periode,1,4)='$tahun'",false);
		if ($query->num_rows() > 0){
		  $tes=null;
		  foreach($query->result() as $x){
		    $tes=$x->v_saldo_akhir;
		  }
		  if($tes==null){
		    $query=$this->db->query("select 'Laba/Rugi Tahun Berjalan' as ket, sum(v_lr) as v_saldo_akhir
                 								 from tm_lr where i_periode < '$periode' and substring(i_periode,1,4)='$tahun'",false);
		    if ($query->num_rows() > 0){
			    return $query->result();
        }
		  }else{
  			return $query->result();
  		}
		}else{
		  $query=$this->db->query("select 'Laba/Rugi Tahun Berjalan' as ket, sum(v_lr) as v_saldo_akhir
              								 from tm_lr where i_periode < '$periode' and substring(i_periode,1,4)='$tahun'",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
      }
		}
	}
		function bacalabarugiyangditahanbulanberjalan($periode)
	{
		$query=$this->db->query("select 'Laba/Rugi Bulan Berjalan' as ket, v_lr as v_saldo_akhir
            								 from tm_lr_general where i_periode = '$periode'",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
   		$query=$this->db->query("select 'Laba/Rugi Bulan Berjalan' as ket, v_lr as v_saldo_akhir
              								 from tm_lr where i_periode = '$periode'",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }

		}
	}
		function bacamodal($periode)
	{
		$query=$this->db->query("select 'Modal' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldogeneral b where b.i_periode = '$periode' and a.i_coa like '3%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldogeneral
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
