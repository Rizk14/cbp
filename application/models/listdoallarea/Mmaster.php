<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
    }
    function bacasemua($iperiode, $cari, $num, $offset)
    {
      $this->db->select(" a.f_do_cancel,
                           a.i_do,
                           a.d_do,
                           a.i_op,
                           a.i_area,
                           c.i_spb,
                           c.i_spb_old,
                           d.i_spmb,
                           d.i_spmb_old,
                           e.e_supplier_name,
                           a.i_supplier
                        from tm_do a, tr_supplier e,
                           tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area)
                           left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
                        where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                           and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and
                           (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
                           or upper(a.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
                           or upper(d.i_spmb) like '%$cari%' or upper(c.i_spb) like '%$cari%'
                           or upper(d.i_spmb_old) like '%$cari%' or upper(c.i_spb_old) like '%$cari%')
                         order by e.e_supplier_name, a.i_do, a.i_area",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($iperiode, $cari, $num, $offset)
    {
      $this->db->select(" a.f_do_cancel,
                           a.i_do,
                           a.d_do,
                           a.i_op,
                           a.i_area,
                           c.i_spb,
                           c.i_spb_old,
                           d.i_spmb,
                           d.i_spmb_old,
                           e.e_supplier_name,
                           a.i_supplier
                        from tm_do a, tr_supplier e,
                           tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area)
                           left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
                        where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                           and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and
                           (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
                            or upper(a.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
                            or upper(d.i_spmb) like '%$cari%' or upper(c.i_spb) like '%$cari%'
                            or upper(d.i_spmb_old) like '%$cari%' or upper(c.i_spb_old) like '%$cari%')
                        order by e.e_supplier_name, a.i_do, a.i_area",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function baca($ido,$isupplier)
    {
      $this->db->select(" a.*, b.*, c.*
                     from tm_do a, tr_supplier b, tr_area c
                     where a.i_supplier=b.i_supplier
                     and a.i_area=c.i_area
                     and a.i_do ='$ido'
                     and a.i_supplier='$isupplier'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function bacadetail($ido,$isupplier)
    {
      $this->db->select(" a.*, b.e_product_motifname, c.n_order from tm_do_item a, tr_product_motif b, tm_op_item c
               where a.i_do = '$ido' and i_supplier='$isupplier' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
           and a.i_product=c.i_product and a.i_op=c.i_op
               order by a.i_product", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
