<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_supplier_name, b.e_supplier_address, b.e_supplier_city, c.e_jenis_bayarname
                          from tm_pelunasanap a, tr_supplier b, tr_jenis_bayar c
                          where 
                          a.i_supplier=b.i_supplier
                          and a.i_jenis_bayar=c.i_jenis_bayar
                          and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%'
                          or upper(a.i_supplier) like '%$cari%')
                          and a.i_supplier='$isupplier' and a.f_pelunasanap_cancel='f' and
                          a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
                          a.d_bukti <= to_date('$dto','dd-mm-yyyy')
                          order by a.d_bukti,a.i_supplier,a.i_pelunasanap ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_supplier_name, b.e_supplier_address, b.e_supplier_city, c.e_jenis_bayarname
                          from tm_pelunasanap a, tr_supplier b, tr_jenis_bayar c
                          where 
                          a.i_supplier=b.i_supplier
                          and a.i_jenis_bayar=c.i_jenis_bayar
                          and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%'
                          or upper(a.i_supplier) like '%$cari%')
                          and a.i_supplier='$isupplier' and a.f_pelunasanap_cancel='f' and
                          a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
                          a.d_bukti <= to_date('$dto','dd-mm-yyyy')
                          order by a.d_bukti,a.i_supplier,a.i_pelunasanap ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select(" * from tr_supplier order by i_supplier",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							  order by i_supplier",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function sisa($isupplier,$ipl){
	    $this->db->select(" sum(v_sisa)as sisa
						    from tm_dtap
						    where i_supplier='$isupplier'
						    and i_dtap='$ipl'",FALSE);
	    $query = $this->db->get();
	    foreach($query->result() as $isi){			
		    return $isi->sisa;
	    }	
   }
  function bacapl($isupplier,$ipl){
    $this->db->select(" a.*, b.e_supplier_name, c.e_jenis_bayarname, b.e_supplier_address, 
                        b.e_supplier_city, d.d_giro_duedate as d_giro_jt, d.d_giro_cair
                        from tm_pelunasanap a
                        inner join tr_supplier b on (a.i_supplier=b.i_supplier)
                        inner join tr_jenis_bayar c on (a.i_jenis_bayar=c.i_jenis_bayar)
                        left join tm_giro_dgu d on (a.i_giro=d.i_giro and a.i_supplier=d.i_supplier)
                        where  
                        upper(a.i_pelunasanap)='$ipl' and upper(a.i_supplier)='$isupplier'",FALSE);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }	
  }
  function bacadetailpl($isupplier,$ipl){
		$this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_netto as v_nota
                        from tm_pelunasanap_item a
                        inner join tm_dtap b on (a.i_dtap=b.i_dtap)
                        where a.i_pelunasanap = '$ipl' 
                        and b.i_supplier='$isupplier'
                        order by a.i_pelunasanap,b.i_supplier ",FALSE);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }	
   }
   function bacagirocek($num,$offset){
	  $this->db->select(" * from tr_jenis_bayar order by i_jenis_bayar ",FALSE)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }	
   }
   function bacagiro($icustomer,$iarea,$num,$offset){
	  $this->db->select(" * from tm_giro_dgu
                        where i_supplier = '$isupplier'
                        and (f_giro_tolak='f' and f_giro_batal='f')
                        order by i_giro,i_supplier ",FALSE)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }	
   }
	function bacaku($isupplier,$num,$offset){
		$this->db->select(" a.* from tm_kuk a, tr_supplier b
                        where a.i_supplier=b.i_supplier
                        and a.i_supplier='$isupplier'
                        and a.v_sisa>0
                        and a.f_close='f'
                        order by a.i_kuk,a.i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacaku2($isupplier,$num,$offset){
		$this->db->select(" a.* from tm_kuk a, tr_supplier b
                        where a.i_supplier=b.i_supplier
                        and a.i_supplier='$isupplier'
                        and a.v_sisa>0
                        and a.f_close='f'
                        order by a.i_kuk,a.i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacapelunasan($isupplier,$num,$offset)
  {
		$this->db->select(" i_dt, min(v_jumlah) as v_jumlah, min(v_lebih) as v_lebih, i_area,
						d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2)) as i_pelunasan
					from tm_pelunasan_lebih
					where i_customer = '$icustomer'
					and i_area='$iarea'
					and v_lebih>0
					group by i_dt, d_bukti, i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	
	function updatecek($ecek,$user,$ipl,$isupplier)
  {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    		$data = array(
					'e_cek'		=> $ecek,
					'd_cek'		=> $dentry,
					'i_cek'		=> $user
    				 );
   	$this->db->where('i_pelunasanap', $ipl);
   	$this->db->where('i_supplier', $isupplier);
		$this->db->update('tm_pelunasanap', $data);
   }
########## Posting ##########
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ipelunasan,$iarea,$egirodescription,$fclose,$dbukti )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjumlah,'$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjumlah,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjumlah','$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function updatepelunasan($ipl,$iarea,$dbukti)
    {
		$this->db->query("update tm_pelunasanap set f_posting='t' where i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}

########## End of Posting ##########
}
?>
