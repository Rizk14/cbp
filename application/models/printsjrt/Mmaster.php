<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto)
    {
		$this->db->select(" a.*, c.e_area_name from tm_sjrt a, tr_area c
							          where substring(a.i_sjr,10,2)=c.i_area
							          and (upper(a.i_sjr) like '%$cari%')
							          and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2) = '$area2' or substring(a.i_sjr,10,2) = '$area3' 
							          or substring(a.i_sjr,10,2) = '$area4' 
							          or substring(a.i_sjr,10,2) = '$area5') 
												and a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjr <= to_date('$dto','dd-mm-yyyy')
												order by a.i_sjr desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($isjr,$area)
    {
		$this->db->select(" *, tr_area.e_area_name from tm_sjrt
							          inner join tr_area on (tm_sjrt.i_area=tr_area.i_area)
							          where tm_sjrt.i_sjr = '$isjr' and tm_sjrt.i_area='$area'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($isjr,$area)
    {
		$this->db->select(" * from tm_sjrt_item
							          inner join tr_product_motif on (tm_sjrt_item.i_product_motif=tr_product_motif.i_product_motif
												and tm_sjrt_item.i_product=tr_product_motif.i_product)
							          where i_sjr = '$isjr' and i_area='$area' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto)
    {
		$this->db->select(" a.*, c.e_area_name from tm_sjrt a, tr_area c
							          where substring(a.i_sjr,10,2)=c.i_area
							          and (upper(a.i_sjr) like '%$cari%') and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2) = '$area2' 
												or substring(a.i_sjr,10,2) = '$area3' or substring(a.i_sjr,10,2) = '$area4' or substring(a.i_sjr,10,2) = '$area5')
												and a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjr <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_sjr desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
