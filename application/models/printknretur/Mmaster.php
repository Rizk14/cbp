<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
      parent::__construct();
	#$this->CI =& get_instance();
  }
  function bacakn($num,$offset,$area,$cari)
  {
	  $this->db->select("	i_kn, d_kn, i_area from tm_kn 
                        where i_area='$area' and i_kn_type='01' and upper(i_kn) like '%$cari%'
                        order by i_kn", false)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function bacasemua($iarea,$cari,$num,$offset,$dfrom,$dto)
  {
		$this->db->select(" a.i_kn, a.d_kn, a.i_area, c.e_area_name, b.e_customer_name from tm_kn a, tr_customer b, tr_area c
                        where a.i_customer=b.i_customer and a.i_area=c.i_area
                        and a.i_kn_type='01'
                        and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_kn) like '%$cari%')
                        and a.d_kn >= to_date('$dfrom','dd-mm-yyyy') 
                        and a.d_kn <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea'
              					order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function bacaarea($num,$offset)
  {
		$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function cariarea($cari,$num,$offset)
  {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function baca($ikn,$area)
  {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, c.*, d.*, e.*
					from tm_kn a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e
					where a.i_kn = '$ikn' and a.i_area='$area'
					and a.i_customer=b.i_customer
					and a.i_salesman=c.i_salesman
					and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
					and b.i_customer_class=d.i_customer_class
					order by a.i_kn desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacabbm($ibbm)
  {
	  $this->db->select("	i_bbm, d_bbm, i_refference_document from tm_bbm 
          						  where i_bbm = '$ibbm' and i_bbm_type='05'",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }

  function bacadetailbbm($ibbm)
  {
		$this->db->select(" * from tm_bbm_item
            					  where i_bbm = '$ibbm' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
