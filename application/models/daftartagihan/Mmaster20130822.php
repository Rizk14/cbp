<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($idt,$iarea,$tgl)
    {
		$this->db->select("* from tm_dt 
				               inner join tr_area on (tm_dt.i_area=tr_area.i_area)
				               where tm_dt.i_dt ='$idt' and tm_dt.i_area='$iarea' and tm_dt.d_dt='$tgl'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($idt,$iarea,$tgl)
    {
		  $this->db->select("a.*, b.v_sisa, b.d_jatuh_tempo, c.e_customer_name, c.e_customer_city
                         from tm_dt_item a 
				             	   inner join tm_nota b on (b.i_nota=a.i_nota)
					               inner join tr_customer c on (b.i_customer=c.i_customer)
                         inner join tr_customer_groupbayar d on (d.i_customer=c.i_customer)
						             where a.i_dt = '$idt' and a.i_area='$iarea' and a.d_dt='$tgl'
						     	       order by a.n_item_no", false);
# and b.i_area=a.i_area)
#                         inner join tr_customer_groupar d on (d.i_customer=c.i_customer)
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function insertheader($idt,$iarea,$ddt,$vjumlah,$fsisa)
    {
    	$this->db->set(
    		array(
					'i_dt'		=> $idt,
					'i_area'	=> $iarea,
					'd_dt'		=> $ddt,
					'v_jumlah'	=> $vjumlah,
					'f_sisa'	=> $fsisa
    		)
    	);
    	
    	$this->db->insert('tm_dt');
    }
    function insertdetail($idt,$ddt,$inota,$iarea,$dnota,$icustomer,$vsisa,$vjumlah,$i)
    {
    	$this->db->set(
    		array(
					'i_dt'				=> $idt,
					'd_dt'				=> $ddt,
					'i_nota'			=> $inota,
					'i_area'			=> $iarea,
					'd_nota'			=> $dnota,
					'i_customer'	=> $icustomer,
					'v_sisa'			=> $vsisa,
					'v_jumlah'		=> $vjumlah,
					'n_item_no'		=> $i
    		)
    	);
#      if($inota=='FP-1110-0304810'){
#        $this->db->insert('tm_dt_item');
#      }else{
   	 	  $this->db->insert('tm_dt_item');
#      }
    }
    function updateheader($idt,$iarea,$ddt,$vjumlah,$fsisa)
    {
		$this->db->query("delete from tm_dt where i_dt='$idt' and i_area='$iarea'");
		$this->db->set(
    		array(
					'i_dt'		=> $idt,
					'i_area'	=> $iarea,
					'd_dt'		=> $ddt,
					'v_jumlah'	=> $vjumlah,
					'f_sisa'	=> $fsisa
    		)
    	);
    	$this->db->insert('tm_dt');
    }

    public function deletedetail($idt,$ddt,$inota,$iarea,$vjumlah,$xddt) 
    {
		$this->db->set(
    		array(
					'v_jumlah'	=> $vjumlah
    		)
    	);
    	$this->db->where('i_dt',$idt);
    	$this->db->where('d_dt',$xddt);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_dt');
		$this->db->query("DELETE FROM tm_dt_item WHERE i_dt='$idt' and d_dt='$xddt' and i_nota='$inota' and i_area='$iarea'");
    }
	
	function uphead($iap,$isupplier,$iop,$iarea,$dap,$vapgross)
    {
    	$data = array(
					'i_ap'		=> $iap,
					'i_supplier'=> $isupplier,
					'i_op'		=> $iop,
					'i_area'	=> $iarea,
					'd_ap'		=> $dap,
					'v_ap_gross'=> $vapgross

            );
		$this->db->where('i_ap', $iap);
		$this->db->where('i_supplier', $isupplier);
		$this->db->update('tm_ap', $data); 
    }
    public function delete($idt,$ddt,$iarea) 
    {
		$this->db->query("update tm_dt set f_dt_cancel='t' WHERE i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",False);
    }
    function bacanota($area,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query	= $this->db->query("select a.i_nota, a.i_area, a.d_nota, a.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, 
                                a.d_jatuh_tempo, b.e_customer_city
                                from tm_nota a, tr_customer b, tr_customer_groupbayar c
                                where 
                                a.i_customer=c.i_customer and a.i_customer=b.i_customer and 
                                a.f_ttb_tolak='f' and 
                                a.f_nota_cancel='f' and
                                a.v_sisa>0 and
                                not (a.i_nota isnull or trim(a.i_nota)='') and 
                                (
                                (c.i_customer_groupbayar in(select i_customer_groupbayar from tr_customer_groupbayar 
                                where substring(i_customer,1,2)='$area'))
                                )
                                group by a.i_nota, a.i_area, a.d_nota, a.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo,
                                b.e_customer_city
                                order by a.i_customer, a.i_nota
                                limit $num offset $offset ",false);
/*
		$query	= $this->db->query("select a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, 
          a.d_jatuh_tempo, b.e_customer_city
					from tm_nota a, tr_customer b
					where 
					a.i_customer=b.i_customer and 
					a.f_ttb_tolak='f' and 
          a.f_nota_cancel='f' and
					a.i_area='$area' and  
					a.v_sisa>0 and
					not a.i_nota isnull
					group by a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo, b.e_customer_city
          limit $num offset $offset ",false);
*/
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carinota($cari,$area,$num,$offset)
    {
		if($offset=='')
			$offset=0;

		$query	= $this->db->query("select a.i_nota, a.i_area, a.d_nota, a.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, 
                                a.d_jatuh_tempo, b.e_customer_city
                                from tm_nota a, tr_customer b, tr_customer_groupbayar c
                                where 
                                a.i_customer=c.i_customer and a.i_customer=b.i_customer and 
                                a.f_ttb_tolak='f' and 
                                a.f_nota_cancel='f' and
                                a.v_sisa>0 and
                                not (a.i_nota isnull or trim(a.i_nota)='') and 
                                (
                                (c.i_customer_groupbayar in(select i_customer_groupbayar from tr_customer_groupbayar 
                                where substring(i_customer,1,2)='$area'))
                                )and 
					                      (upper(a.i_nota) like '%$cari%' or 
					                      a.i_nota_old like '%$cari%' or 
					                      upper(a.i_customer) like '%$cari%' or 
					                      upper(b.e_customer_name) like '%$cari%') 
					                      group by a.i_nota, a.i_area, a.d_nota, a.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo,
                                b.e_customer_city
                                order by a.i_customer, a.i_nota
					                      limit $num offset $offset ",false);
/*
		$query	= $this->db->query("select a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, 
          a.d_jatuh_tempo, b.e_customer_city
					from tm_nota a, tr_customer b 

					where 
					a.i_customer=b.i_customer and 
					a.f_ttb_tolak='f' and 
          a.f_nota_cancel='f' and
  				a.i_area='$area' and  
					a.v_sisa>0 and 
					not a.i_nota isnull and 

					(upper(a.i_nota) like '%$cari%' or 
					a.i_nota_old like '%$cari%' or 
					upper(b.i_customer) like '%$cari%' or 
					upper(b.e_customer_name) like '%$cari%') 
					
					group by a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo, b.e_customer_city
					limit $num offset $offset ",false);
*/
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$cari,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t'){
			$this->db->select("* from tr_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%')", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'", false)->limit($num,$offset);			
		}
		        
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($num,$offset,$cari,$area1,$area2,$area3,$area4,$area5,$allarea)
    {
		if($allarea=='t'){
			$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ", false)->limit($num,$offset);
		}else{
			$this->db->select(" * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' ", false)->limit($num,$offset);
		}
			        
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaop($num,$offset)
    {
		$this->db->select(" a.*, 'Lain-lain' as e_supplier_name, c.e_area_name 
							from tm_op a, tr_area c 
							where a.i_area=c.i_area and a.i_supplier='G0000'
							and a.f_op_cancel='f' 
							and a.f_op_close='f'",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_ap where upper(i_ap) like '%$cari%' or upper(i_supplier) like '%$cari%'
							order by i_ap",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')
							and i_supplier_group='G0000' order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query("  	select a.i_product as kode, 
									a.i_product_motif as motif,
									a.e_product_motifname as namamotif, 
									c.e_product_name as nama,
									c.v_product_mill as harga
									from tr_product_motif a, tr_product c
									where a.i_product=c.i_product
								   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariop($cari,$num,$offset)
    {
		$this->db->select(" * from tm_op where (upper(i_op) like '%$cari%' or upper(i_supplier) like '%$cari%'
							or upper(e_supplier_name) like '%$cari%') and a.i_supplier='G0000'
							and a.f_op_cancel='f' and substr(i_reff,1,4)='SPMB'
							and a.f_op_close='f'",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_op,9,4)) as max from tm_ap 
				  			where substr(i_ap,4,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$noop  =$terakhir+1;
			settype($noop,"string");
			$a=strlen($noop);
			while($a<4){
			  $noop="0".$noop;
			  $a=strlen($noop);
			}
			$noop  ="AP-".$thbl."-".$noop;
			return $noop;
		}else{
			$noop  ="0001";
			$noop  ="AP-".$thbl."-".$noop;
			return $noop;
		}
    }
/*
	function runningnumberbbm(){
	   	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_bbm,10,6)) as max from tm_bbm where substr(i_bbm,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$noso  =$terakhir+1;
			settype($noso,"string");
			$a=strlen($noso);
			while($a<6){
			  $noso="0".$noso;
			  $a=strlen($noso);
			}
			$noso  ="BBM-".$thbl."-".$noso;
			return $noso;
		}else{
			$noso  ="000001";
			$noso  ="BBM-".$thbl."-".$noso;
			return $noso;
		}
    }
	function insertbbmheader($iap,$dap,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea)
    {
    	$this->db->set(
    		array(
				'i_bbm'					=> $ibbm,
				'i_bbm_type'			=> $ibbmtype,
				'i_refference_document'	=> $iap,
				'd_refference_document'	=> $dap,
				'd_bbm'					=> $dbbm,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea
    		)
    	);
    	
    	$this->db->insert('tm_bbm');
    }
	function updatebbmheader($iap,$dap,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea)
    {
    	$this->db->set(
    		array(
				'i_refference_document'	=> $iap,
				'd_refference_document'	=> $dap,
				'd_bbm'					=> $dbbm,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea
    		)
    	);
    	$this->db->where('i_bbm',$ibbm);
		$this->db->where('i_bbm_type',$ibbmtype);
    	$this->db->update('tm_bbm');
    }
	function insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,$vunitprice,$iap,$ibbm,$eremark,$dap)
    {
    	$this->db->set(
    		array(
				'i_bbm'					=> $ibbm,
				'i_refference_document'	=> $iap,
				'i_product'				=> $iproduct,
				'i_product_motif'		=> $iproductmotif,
				'i_product_grade'		=> $iproductgrade,
				'e_product_name'		=> $eproductname,
				'n_quantity'			=> $nquantity,
				'v_unit_price'			=> $vunitprice,
				'e_remark'				=> $eremark,
				'd_refference_document'	=> $dap
    		)
    	);
    	
    	$this->db->insert('tm_bbm_item');
    }
*/
	function runningnumberdt($iarea,$thbl){
	  $th	 = substr($thbl,0,4);
    $asal=$thbl;
		$thn = substr($thbl,2,2);
    $thbl=substr($thbl,2,2).substr($thbl,4,2);
	  $this->db->select(" n_modul_no as max from tm_dgu_no 
                        where i_modul='DT'
                        and substr(e_periode,1,4)='$th' 
                        and i_area='$iarea' for update", false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
		    $terakhir=$row->max;
		  }
		  $nodt  =$terakhir+1;
      $this->db->query(" update tm_dgu_no 
                          set n_modul_no=$nodt
                          where i_modul='DT'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea'", false);
		  settype($nodt,"string");
		  $a=strlen($nodt);
		  while($a<4){
		    $nodt="0".$nodt;
		    $a=strlen($nodt);
		  }
	  	$nodt  =$nodt."-".$thn;
		  return $nodt;
	  }else{
		  $nodt  ="0001";
	  	$nodt  =$nodt."-".$thn;
      $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                         values ('DT','$iarea','$asal',1)");
		  return $nodt;
	  }
		
/*
    $th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_dt,1,4)) as max from tm_dt where substr(i_dt,6,2)='$th' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nodt  =$terakhir+1;
			settype($nodt,"string");
			$a=strlen($nodt);
			while($a<4){
			  $nodt="0".$nodt;
			  $a=strlen($nodt);
			}
			$nodt  =$nodt."-".$th;
			return $nodt;
		}else{
			$nodt  ="0001";
			$nodt  =$nodt."-".$th;
			return $nodt;
		}
*/
  }
}
?>
