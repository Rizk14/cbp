<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ialokasi,$idn,$iarea) 
    {
		  $this->db->query("UPDATE tm_alokasidn SET f_alokasi_cancel='t' WHERE i_dn='$idn' AND i_alokasi='$ialokasi'");

      $query 	= $this->db->query("SELECT current_timestamp as c");
      $row   	= $query->row();
      $ddelete= $row->c;
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
      
      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }
      $this->db->query("delete from tm_jurnal_transharian where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference LIKE '%$ialokasi%' and i_area='$iarea'");
#####
      $quer 	= $this->db->query(" SELECT i_nota, v_jumlah FROM tm_alokasidn_item
                                   WHERE i_dn='$idn' AND i_alokasi='$ialokasi'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          //! QUERY LAMA SEBELUM DIGABUNG ALOKASI METERAI (07 JUN 2021)
          // $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah WHERE i_nota='$xx->i_nota'");
          // $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");

          //* PENAMBAHAN & UPDATE 07 JUN 2021
          $this->db->query("UPDATE tm_dtap set v_sisa=v_sisa+$xx->v_jumlah WHERE i_dtap='$xx->i_nota'");
          $this->db->query("UPDATE tm_dn_ap set v_sisa=v_sisa+$xx->v_jumlah WHERE i_dn_ap='$idn'");
        }
      }
    }

    public function deletedetail($ialokasi,$ikbank,$iarea) 
    {
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference = '$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference = '$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference = '$ialokasi' and i_area='$iarea'");
      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }
      $this->db->query("delete from tm_jurnal_transharian where i_refference = '$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference = '$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference = '$ialokasi' and i_area='$iarea'");
#####
      $quer 	= $this->db->query("SELECT i_nota, v_jumlah, i_coa_bank, v_materai from tm_alokasi_item
                                  WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah, v_materai_sisa=v_materai_sisa+$xx->v_materai WHERE i_nota='$xx->i_nota'");
          $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah+$xx->v_materai WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");
        }
      }
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("* from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("* from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {

      if($isupplier == 'all'){
        $query = "  
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            (UPPER(a.i_alokasi) LIKE ('%$cari%')
            OR UPPER(a.i_supplier) LIKE ('%$cari%')
            OR UPPER(a.i_dn) LIKE ('%$cari%')
            OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
            AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }else{
        $query = " 
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            (UPPER(a.i_alokasi) LIKE ('%$cari%')
            OR UPPER(a.i_supplier) LIKE ('%$cari%')
            OR UPPER(a.i_dn) LIKE ('%$cari%')
            OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
            AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
            AND a.i_supplier = '$isupplier'
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }
		$this->db->select($query,false)->limit($num,$offset);
#and a.f_alokasi_cancel='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacatotal($isupplier,$dfrom,$dto)
    {
      if($isupplier == 'all'){
        $query = "  
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }else{
        $query = " 
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
            AND a.i_supplier = '$isupplier'
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }
    $this->db->select($query,false);
#and a.f_alokasi_cancel='f'
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
      if($isupplier == 'all'){
        $q = "  
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            (UPPER(a.i_alokasi) LIKE ('%$cari%')
            OR UPPER(a.i_supplier) LIKE ('%$cari%')
            OR UPPER(a.i_dn) LIKE ('%$cari%')
            OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
            AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }else{
        $q = " 
            a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
            a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
          FROM 
            tm_alokasidn a 
            INNER JOIN tr_area b ON (a.i_area = b.i_area)
            INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
          WHERE
            (UPPER(a.i_alokasi) LIKE ('%$cari%')
            OR UPPER(a.i_supplier) LIKE ('%$cari%')
            OR UPPER(a.i_dn) LIKE ('%$cari%')
            OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
            AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
            AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
            AND a.i_supplier = '$isupplier'
          ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
        ";
      }
		$this->db->select($q,false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
