<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	  public function __construct()
    {
          parent::__construct();
		  #$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
      $this->db->select(" a.i_area, a.i_spb, b.e_customer_name, a.v_spb, a.v_spb_discounttotal, (a.v_spb-a.v_spb_discounttotal) as jumlah
                          from tm_spbkonsinyasi a, tr_customer b
                          where a.i_customer=b.i_customer and a.i_area=b.i_area
                          and to_char(a.d_spb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.f_spb_cancel='f'
                          order by a.i_area, a.i_spb",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadiskon($iperiode)
    {
		  $this->db->select(" distinct(n_notapb_discount) as diskon from tm_notapb
                          where to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          order by n_notapb_discount",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumberiks($thbl,$namagrup,$iarea){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_konsinyasi as max, i_group from tm_dgu_nopb
                          where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
          $grup=$row->i_group;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_nopb 
                            set n_modul_konsinyasi=$nospb
                            where i_modul='SPB' and i_group='$grup'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<6){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
		  	$nospb  ="IKS-".$thbl."-".$nospb;
		  	$query->free_result();
			  return $nospb;
		  }else{
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                            and substr(e_periode,1,4)='$th'", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $grup=$row->i_group;
          }
			  }
			  $nospb  ="000001";
			  $nospb  ="IKS-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_nopb(i_modul, i_area, i_group, e_periode, n_modul_no,e_group_name,n_modul_konsinyasi) 
                           values ('SPB','$iarea','$grup','$asal',0,'$namagrup',1)");
        $query->free_result();
			  return $nospb;
		  }
    }
    function runningnumberspb($thbl,$namagrup,$iarea){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max, i_group from tm_dgu_nopb
                          where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
          $grup=$row->i_group;
			  }
			  $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_nopb 
                            set n_modul_no=$nospb
                            where i_modul='SPB' and i_group='$grup'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospb,"string");
			  $a=strlen($nospb);
			  while($a<4){
			    $nospb="0".$nospb;
			    $a=strlen($nospb);
			  }
		  	$nospb  ="SPB-".$thbl."-".$grup.$nospb;
		  	$query->free_result();
			  return $nospb;
		  }else{
        $this->db->select(" i_group from tm_dgu_nopb
                            where i_modul='SPB' and substr(e_group_name,1,3)='$namagrup'
                            and substr(e_periode,1,4)='$th'", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $grup=$row->i_group;
          }
			  }
			  $nospb  ="0001";
			  $nospb  ="SPB-".$thbl."-".$grup.$nospb;
        $this->db->query(" insert into tm_dgu_nopb(i_modul, i_area, i_group, e_periode, n_modul_no,e_group_name) 
                           values ('SPB','$iarea','$grup','$asal',1,'$namagrup')");
        $query->free_result();
			  return $nospb;
		  }
    }
    function insertheader($iks, $ispb, $dspb, $iarea, $icustomer)
    {
      $this->db->select(" a.*, b.i_price_group as klp_harga
                          from tr_customer a, tr_price_group b
                          where (a.i_price_group=b.i_price_group or a.i_price_group=b.n_line)
                          and a.i_customer='$icustomer' and a.i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $ipricegroup  = $row->klp_harga;
          $fpkp         = $row->f_customer_pkp;
          $fppn         = $row->f_customer_plusppn;
          $top          = $row->n_customer_toplength;
			  }
			  $query->free_result();
      	$this->db->set(
      		array(
			        'i_konsinyasi'		      => $iks,
			        'i_spb'		              => $ispb,
			        'd_spb'		              => $dspb,
              'i_customer'            => $icustomer,
              'i_price_group'         => $ipricegroup,
              'i_product_group'       => '01',
              'i_salesman'            => 'TL',
              'f_spb_stockdaerah'     => 't',
              'f_spb_consigment'      => 't',
              'f_spb_pkp'             => $fpkp,
              'f_spb_plusppn'         => $fppn,
			        'i_area'		            => $iarea,
              'n_spb_toplength'       => $top,
              'n_spb_discount1'       => 0,
              'n_spb_discount2'       => 0,
              'n_spb_discount3'       => 0,
              'v_spb_discount1'       => 0,
              'v_spb_discount2'       => 0,
              'v_spb_discount3'       => 0,
              'v_spb_discounttotal'   => 0,
			        'n_print'		            => 0
      		)
      	);
      	$this->db->insert('tm_spbkonsinyasi');
      }
    }
    function insertdetail($iks,$ispb,$iarea,$iproduct,$eproductname,$iproductgrade,$iproductmotif,$harga,$quantity,$x)
    {
      $this->db->query("insert into tm_spbkonsinyasi_item values('$iks','$ispb','$iproduct','$iproductgrade','$iproductmotif',
                                                        $quantity,null,null,
                                                        $harga,'$eproductname',null,'$iarea',null,$x,'1')");
    }
    function updatespb($iks,$ispb,$iarea)
    {
      $query=$this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spbkonsinyasi_item
                                where i_konsinyasi='$iks' and i_area = '$iarea' and i_spb='$ispb'");
      if($query->num_rows()>0){
        foreach($query->result() as $xx){
          $total=$xx->order;
        }
        $query->free_result();
	      $this->db->query("update tm_spbkonsinyasi set v_spb='$total' where i_konsinyasi='$iks' and i_spb='$ispb' and i_area='$iarea'");
      }
    }

    function updatebon($icustomer,$iperiode,$ispb)
    {
      $this->db->query("update tm_notapb set i_spb='$ispb', f_spb_rekap='t' where i_customer='$icustomer'
                        and to_char(d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                        and not i_cek is null and not d_cek is null");
    }
    function baca($ispb, $iarea)
    {
      $this->db->select(" a.e_remark1 AS emark1, a.*, e.e_price_groupname,
           d.e_area_name, b.e_customer_name, b.e_customer_address, c.e_salesman_name, b.f_customer_first
           from tm_spbkonsinyasi a
               inner join tr_customer b on (a.i_customer=b.i_customer)
               inner join tr_salesman c on (a.i_salesman=c.i_salesman)
               inner join tr_customer_area d on (a.i_customer=d.i_customer)
               left join tr_price_group e on (a.i_price_group=e.i_price_group)
               where a.i_spb ='$ispb' and a.i_area='$iarea'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function bacadetail($ispb, $iarea, $ipricegroup)
    {
      $this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
                          a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, b.e_product_motifname,
                          c.v_product_retail as hrgnew, a.i_product_status
                          from tm_spbkonsinyasi_item a, tr_product_motif b, tr_product_price c
                          where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                          and a.i_product=c.i_product and c.i_price_group='$ipricegroup'
                          order by a.n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetailnilaispb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spbkonsinyasi_item a
                                where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup)
    {
      return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spbkonsinyasi_item a
                                where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function updateheader($ispb, $iarea, $nspbdiscount1, $vspbdiscount1, $vspbdiscounttotal, $vspb)
    {
       $query      = $this->db->query("SELECT current_timestamp as c");
       $row        = $query->row();
       $dspbupdate = $row->c;
          $data = array( 
             'n_spb_discount1'    => $nspbdiscount1,
             'v_spb_discount1'    => $vspbdiscount1,
             'v_spb_discounttotal'=> $vspbdiscounttotal,
             'v_spb'              => $vspb,
             'd_spb_update'       => $dspbupdate,
             'i_product_group'    => '01'
                );
       $this->db->where('i_spb', $ispb);
       $this->db->where('i_area', $iarea);
       $this->db->update('tm_spbkonsinyasi', $data);
    }
#####
    function insert($ispb, $iarea)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $this->db->select(" * from tm_spbkonsinyasi where i_spb='$ispb' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $ispb                   = $row->i_spb;
          $icustomer              = $row->i_customer;
          $isalesman              = $row->i_salesman;
          $ipricegroup            = $row->i_price_group;
          $inota                  = $row->i_nota;
          $ispbprogram            = $row->i_spb_program;
          $ispbpo                 = $row->i_spb_po;
          $isj                    = $row->i_sj;
          $istore                 = $row->i_store;
          $istorelocation         = $row->i_store_location;
          $dspb                   = $row->d_spb;
          $dnota                  = $row->d_nota;
          $dspbentry              = $now;
          $dspbupdate             = null;
          $dsj                    = $row->d_sj;
          $dspbdelivery           = $row->d_spb_delivery;
          $dspbreceive            = $row->d_spb_receive;
          $dspbstorereceive       = $row->d_spb_storereceive;
          $dspbemail              = $row->d_spb_email;
          $ecustomerpkpnpwp       = $row->e_customer_pkpnpwp;
          $fspbop                 = $row->f_spb_op;
          $fspbpkp                = $row->f_spb_pkp;
          $fspbplusppn            = $row->f_spb_plusppn;
          $fspbplusdiscount       = $row->f_spb_plusdiscount;
          $fspbstockdaerah        = $row->f_spb_stockdaerah;
          $fspbprogram            = $row->f_spb_program;
          $fspbconsigment         = $row->f_spb_consigment;
          $fspbvalid              = $row->f_spb_valid;
          $fspbsiapnotagudang     = $row->f_spb_siapnotagudang;
          $fspbcancel             = $row->f_spb_cancel;
          $nspbtoplength          = $row->n_spb_toplength;
          $nspbdiscount1          = $row->n_spb_discount1;
          $nspbdiscount2          = $row->n_spb_discount2;
          $nspbdiscount3          = $row->n_spb_discount3;
          $vspbdiscount1          = $row->v_spb_discount1;
          $vspbdiscount2          = $row->v_spb_discount2;
          $vspbdiscount3          = $row->v_spb_discount3;
          $vspbdiscounttotal      = $row->v_spb_discounttotal;
          $vspbdiscounttotalafter = $row->v_spb_discounttotalafter;
          $vspb                   = $row->v_spb;
          $vspbafter              = $row->v_spb_after;
          $iapprove1              = $row->i_approve1;
          $iapprove2              = $row->i_approve2;
          $dapprove1              = $row->d_approve1;
          $dapprove2              = $row->d_approve2;
          $eapprove1              = $row->e_approve1;
          $eapprove2              = $row->e_approve2;
          $iarea                  = $row->i_area;
          $nspbdiscount4          = $row->n_spb_discount4;
          $vspbdiscount4          = $row->v_spb_discount4;
          $inotapprove            = $row->i_notapprove;
          $dnotapprove            = $row->d_notapprove;
          $enotapprove            = $row->e_notapprove;
          $fspbsiapnotasales      = $row->f_spb_siapnotasales;
          $ispbold                = $row->i_spb_old;
          $iproductgroup          = $row->i_product_group;
          $fspbopclose            = $row->f_spb_opclose;
          $fspbpemenuhan          = $row->f_spb_pemenuhan;
          $nprint                 = $row->n_print;
          $icek                   = $row->i_cek;
          $dcek                   = $row->d_cek;
          $ecek                   = $row->e_cek;
          $eremark1               = $row->e_remark1;
          $icekcabang             = $row->i_cek_cabang;
          $dcekcabang             = $row->d_cek_cabang;
          $ecekcabang             = $row->e_cek_cabang;
          $fspbrekap              = $row->f_spb_rekap;
          $ispmb                  = $row->i_spmb;
          $dpo                    = $row->d_po;
			  }
			  $query->free_result();
      	$this->db->set(
      		array(
          'i_spb'                    => $ispb,
          'i_customer'               => $icustomer,
          'i_salesman'               => $isalesman,
          'i_price_group'            => $ipricegroup,
          'i_nota'                   => $inota,
          'i_spb_program'            => $ispbprogram,
          'i_spb_po'                 => $ispbpo,
          'i_sj'                     => $isj,
          'i_store'                  => $istore,
          'i_store_location'         => $istorelocation,
          'd_spb'                    => $dspb,
          'd_nota'                   => $dnota,
          'd_spb_entry'              => $dspbentry,
          'd_spb_update'             => $dspbupdate,
          'd_sj'                     => $dsj,
          'd_spb_delivery'           => $dspbdelivery,
          'd_spb_receive'            => $dspbreceive,
          'd_spb_storereceive'       => $dspbstorereceive,
          'd_spb_email'              => $dspbemail,
          'e_customer_pkpnpwp'       => $ecustomerpkpnpwp,
          'f_spb_op'                 => $fspbop,
          'f_spb_pkp'                => $fspbpkp,
          'f_spb_plusppn'            => $fspbplusppn,
          'f_spb_plusdiscount'       => $fspbplusdiscount,
          'f_spb_stockdaerah'        => $fspbstockdaerah,
          'f_spb_program'            => $fspbprogram,
          'f_spb_consigment'         => $fspbconsigment,
          'f_spb_valid'              => $fspbvalid,
          'f_spb_siapnotagudang'     => $fspbsiapnotagudang,
          'f_spb_cancel'             => $fspbcancel,
          'n_spb_toplength'          => $nspbtoplength,
          'n_spb_discount1'          => $nspbdiscount1,
          'n_spb_discount2'          => $nspbdiscount2,
          'n_spb_discount3'          => $nspbdiscount3,
          'v_spb_discount1'          => $vspbdiscount1,
          'v_spb_discount2'          => $vspbdiscount2,
          'v_spb_discount3'          => $vspbdiscount3,
          'v_spb_discounttotal'      => $vspbdiscounttotal,
          'v_spb_discounttotalafter' => $vspbdiscounttotalafter,
          'v_spb'                    => $vspb,
          'v_spb_after'              => $vspbafter,
          'i_approve1'               => $iapprove1,
          'i_approve2'               => $iapprove2,
          'd_approve1'               => $dapprove1,
          'd_approve2'               => $dapprove2,
          'e_approve1'               => $eapprove1,
          'e_approve2'               => $eapprove2,
          'i_area'                   => $iarea,
          'n_spb_discount4'          => $nspbdiscount4,
          'v_spb_discount4'          => $vspbdiscount4,
          'i_notapprove'            => $inotapprove,
          'd_notapprove'            => $dnotapprove,
          'e_notapprove'            => $enotapprove,
          'f_spb_siapnotasales'      => $fspbsiapnotasales,
          'i_spb_old'                => $ispbold,
          'i_product_group'          => $iproductgroup,
          'f_spb_opclose'            => $fspbopclose,
          'f_spb_pemenuhan'          => $fspbpemenuhan,
          'n_print'                  => $nprint,
          'i_cek'                    => $icek,
          'd_cek'                    => $dcek,
          'e_cek'                    => $ecek,
          'e_remark1'                => $eremark1,
          'i_cek_cabang'             => $icekcabang,
          'd_cek_cabang'             => $dcekcabang,
          'e_cek_cabang'             => $ecekcabang,
          'f_spb_rekap'              => $fspbrekap,
          'i_spmb'                   => $ispmb,
          'd_po'                     => $dpo
      		)
      	);
      	$this->db->insert('tm_spb');
      }
      $this->db->select(" * from tm_spbkonsinyasi_item where i_spb='$ispb' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $ispb                   = $row->i_spb;
          $iproduct               = $row->i_product;
          $iproductgrade          = $row->i_product_grade;
          $iproductmotif          = $row->i_product_motif;
          $norder                 = $row->n_order;
          $ndeliver               = $row->n_deliver;
          $nstock                 = $row->n_stock;
          $vunitprice             = $row->v_unit_price;
          $eproductname           = $row->e_product_name;
          $iop                    = $row->i_op;
          $iarea                  = $row->i_area;
          $eremark                = $row->e_remark;
          $nitemno                = $row->n_item_no;
          $iproductstatus         = $row->i_product_status;

        	$this->db->set(
        		array(
              'i_spb'            => $ispb,
              'i_product'        => $iproduct,
              'i_product_grade'  => $iproductgrade,
              'i_product_motif'  => $iproductmotif,
              'n_order'          => $norder,
              'n_deliver'        => $ndeliver,
              'n_stock'          => $nstock,
              'v_unit_price'     => $vunitprice,
              'e_product_name'   => $eproductname,
              'i_op'             => $iop,
              'i_area'           => $iarea,
              'e_remark'         => $eremark,
              'n_item_no'        => $nitemno,
              'i_product_status' => $iproductstatus
        		)
        	);
        	$this->db->insert('tm_spb_item');
			  }
			  $query->free_result();
      }
    }
    function update($ispb, $iarea)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $this->db->select(" * from tm_spbkonsinyasi where i_spb='$ispb' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $ispb                   = $row->i_spb;
          $icustomer              = $row->i_customer;
          $isalesman              = $row->i_salesman;
          $ipricegroup            = $row->i_price_group;
          $inota                  = $row->i_nota;
          $ispbprogram            = $row->i_spb_program;
          $ispbpo                 = $row->i_spb_po;
          $isj                    = $row->i_sj;
          $istore                 = $row->i_store;
          $istorelocation         = $row->i_store_location;
          $dspb                   = $row->d_spb;
          $dnota                  = $row->d_nota;
          $dspbentry              = null;
          $dspbupdate             = $now;
          $dsj                    = $row->d_sj;
          $dspbdelivery           = $row->d_spb_delivery;
          $dspbreceive            = $row->d_spb_receive;
          $dspbstorereceive       = $row->d_spb_storereceive;
          $dspbemail              = $row->d_spb_email;
          $ecustomerpkpnpwp       = $row->e_customer_pkpnpwp;
          $fspbop                 = $row->f_spb_op;
          $fspbpkp                = $row->f_spb_pkp;
          $fspbplusppn            = $row->f_spb_plusppn;
          $fspbplusdiscount       = $row->f_spb_plusdiscount;
          $fspbstockdaerah        = $row->f_spb_stockdaerah;
          $fspbprogram            = $row->f_spb_program;
          $fspbconsigment         = $row->f_spb_consigment;
          $fspbvalid              = $row->f_spb_valid;
          $fspbsiapnotagudang     = $row->f_spb_siapnotagudang;
          $fspbcancel             = $row->f_spb_cancel;
          $nspbtoplength          = $row->n_spb_toplength;
          $nspbdiscount1          = $row->n_spb_discount1;
          $nspbdiscount2          = $row->n_spb_discount2;
          $nspbdiscount3          = $row->n_spb_discount3;
          $vspbdiscount1          = $row->v_spb_discount1;
          $vspbdiscount2          = $row->v_spb_discount2;
          $vspbdiscount3          = $row->v_spb_discount3;
          $vspbdiscounttotal      = $row->v_spb_discounttotal;
          $vspbdiscounttotalafter = $row->v_spb_discounttotalafter;
          $vspb                   = $row->v_spb;
          $vspbafter              = $row->v_spb_after;
          $iapprove1              = $row->i_approve1;
          $iapprove2              = $row->i_approve2;
          $dapprove1              = $row->d_approve1;
          $dapprove2              = $row->d_approve2;
          $eapprove1              = $row->e_approve1;
          $eapprove2              = $row->e_approve2;
          $iarea                  = $row->i_area;
          $nspbdiscount4          = $row->n_spb_discount4;
          $vspbdiscount4          = $row->v_spb_discount4;
          $inotapprove            = $row->i_notapprove;
          $dnotapprove            = $row->d_notapprove;
          $enotapprove            = $row->e_notapprove;
          $fspbsiapnotasales      = $row->f_spb_siapnotasales;
          $ispbold                = $row->i_spb_old;
          $iproductgroup          = $row->i_product_group;
          $fspbopclose            = $row->f_spb_opclose;
          $fspbpemenuhan          = $row->f_spb_pemenuhan;
          $nprint                 = $row->n_print;
          $icek                   = $row->i_cek;
          $dcek                   = $row->d_cek;
          $ecek                   = $row->e_cek;
          $eremark1               = $row->e_remark1;
          $icekcabang             = $row->i_cek_cabang;
          $dcekcabang             = $row->d_cek_cabang;
          $ecekcabang             = $row->e_cek_cabang;
          $fspbrekap              = $row->f_spb_rekap;
          $ispmb                  = $row->i_spmb;
          $dpo                    = $row->d_po;
			  }
			  $query->free_result();
      	$this->db->set(
      		array(
          'i_spb'                    => $ispb,
          'i_customer'               => $icustomer,
          'i_salesman'               => $isalesman,
          'i_price_group'            => $ipricegroup,
          'i_nota'                   => $inota,
          'i_spb_program'            => $ispbprogram,
          'i_spb_po'                 => $ispbpo,
          'i_sj'                     => $isj,
          'i_store'                  => $istore,
          'i_store_location'         => $istorelocation,
          'd_spb'                    => $dspb,
          'd_nota'                   => $dnota,
          'd_spb_entry'              => $dspbentry,
          'd_spb_update'             => $dspbupdate,
          'd_sj'                     => $dsj,
          'd_spb_delivery'           => $dspbdelivery,
          'd_spb_receive'            => $dspbreceive,
          'd_spb_storereceive'       => $dspbstorereceive,
          'd_spb_email'              => $dspbemail,
          'e_customer_pkpnpwp'       => $ecustomerpkpnpwp,
          'f_spb_op'                 => $fspbop,
          'f_spb_pkp'                => $fspbpkp,
          'f_spb_plusppn'            => $fspbplusppn,
          'f_spb_plusdiscount'       => $fspbplusdiscount,
          'f_spb_stockdaerah'        => $fspbstockdaerah,
          'f_spb_program'            => $fspbprogram,
          'f_spb_consigment'         => $fspbconsigment,
          'f_spb_valid'              => $fspbvalid,
          'f_spb_siapnotagudang'     => $fspbsiapnotagudang,
          'f_spb_cancel'             => $fspbcancel,
          'n_spb_toplength'          => $nspbtoplength,
          'n_spb_discount1'          => $nspbdiscount1,
          'n_spb_discount2'          => $nspbdiscount2,
          'n_spb_discount3'          => $nspbdiscount3,
          'v_spb_discount1'          => $vspbdiscount1,
          'v_spb_discount2'          => $vspbdiscount2,
          'v_spb_discount3'          => $vspbdiscount3,
          'v_spb_discounttotal'      => $vspbdiscounttotal,
          'v_spb_discounttotalafter' => $vspbdiscounttotalafter,
          'v_spb'                    => $vspb,
          'v_spb_after'              => $vspbafter,
          'i_approve1'               => $iapprove1,
          'i_approve2'               => $iapprove2,
          'd_approve1'               => $dapprove1,
          'd_approve2'               => $dapprove2,
          'e_approve1'               => $eapprove1,
          'e_approve2'               => $eapprove2,
          'i_area'                   => $iarea,
          'n_spb_discount4'          => $nspbdiscount4,
          'v_spb_discount4'          => $vspbdiscount4,
          'i_notapprove'            => $inotapprove,
          'd_notapprove'            => $dnotapprove,
          'e_notapprove'            => $enotapprove,
          'f_spb_siapnotasales'      => $fspbsiapnotasales,
          'i_spb_old'                => $ispbold,
          'i_product_group'          => $iproductgroup,
          'f_spb_opclose'            => $fspbopclose,
          'f_spb_pemenuhan'          => $fspbpemenuhan,
          'n_print'                  => $nprint,
          'i_cek'                    => $icek,
          'd_cek'                    => $dcek,
          'e_cek'                    => $ecek,
          'e_remark1'                => $eremark1,
          'i_cek_cabang'             => $icekcabang,
          'd_cek_cabang'             => $dcekcabang,
          'e_cek_cabang'             => $ecekcabang,
          'f_spb_rekap'              => $fspbrekap,
          'i_spmb'                   => $ispmb,
          'd_po'                     => $dpo
      		)
      	);
      	$this->db->where('i_spb',$ispb);
      	$this->db->where('i_area',$iarea);
      	$this->db->update('tm_spb');
      }
      $this->db->select(" * from tm_spbkonsinyasi_item where i_spb='$ispb' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $ispb                   = $row->i_spb;
          $iproduct               = $row->i_product;
          $iproductgrade          = $row->i_product_grade;
          $iproductmotif          = $row->i_product_motif;
          $norder                 = $row->n_order;
          $ndeliver               = $row->n_deliver;
          $nstock                 = $row->n_stock;
          $vunitprice             = $row->v_unit_price;
          $eproductname           = $row->e_product_name;
          $iop                    = $row->i_op;
          $iarea                  = $row->i_area;
          $eremark                = $row->e_remark;
          $nitemno                = $row->n_item_no;
          $iproductstatus         = $row->i_product_status;
			  }
			  $query->free_result();
      	$this->db->set(
      		array(
            'i_spb'            => $ispb,
            'i_product'        => $iproduct,
            'i_product_grade'  => $iproductgrade,
            'i_product_motif'  => $iproductmotif,
            'n_order'          => $norder,
            'n_deliver'        => $ndeliver,
            'n_stock'          => $nstock,
            'v_unit_price'     => $vunitprice,
            'e_product_name'   => $eproductname,
            'i_op'             => $iop,
            'i_area'           => $iarea,
            'e_remark'         => $eremark,
            'n_item_no'        => $nitemno,
            'i_product_status' => $iproductstatus
      		)
      	);
      	$this->db->where('i_spb',$ispb);
      	$this->db->where('i_area',$iarea);
      	$this->db->where('i_product',$iproduct);
      	$this->db->update('tm_spb_item');
      }
    }
#####
}
?>
