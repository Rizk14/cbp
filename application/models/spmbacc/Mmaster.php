<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($ispmb)
	{
		$this->db->select("a.*, b.e_area_name from tm_spmb a, tr_area b
						   where a.i_area=b.i_area
						   and i_spmb ='$ispmb'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($ispmb)
	{
		/* 
		$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
				   where a.i_spmb = '$ispmb' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.i_product", false);
	   */
		$this->db->select(" 	ctg.e_sales_categoryname,
								a.*,
								b.e_product_motifname
							FROM
								tm_spmb_item a,
								tr_product_motif b,
								tr_product prd,
								tr_product_sales_category ctg
							WHERE
								a.i_spmb = '$ispmb'
								AND a.i_product = b.i_product
								AND a.i_product_motif = b.i_product_motif
								AND a.i_product = prd.i_product
								AND prd.i_sales_category = ctg.i_sales_category
							ORDER BY
								a.n_item_no ", false);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function insertheader($ispmb, $dspmb, $iarea, $fop, $nprint, $ispmbold, $eremark)
	{
		$this->db->set(
			array(
				'i_spmb'		=> $ispmb,
				'd_spmb'		=> $dspmb,
				'i_area'		=> $iarea,
				'f_op'			=> 'f',
				'n_print'		=> 0,
				'i_spmb_old' => $ispmbold,
				'e_remark'  => $eremark
			)
		);

		$this->db->insert('tm_spmb');
	}
	function insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $norder, $nacc, $vunitprice, $iproductmotif, $eremark, $iarea, $i)
	{
		$this->db->set(
			array(
				'i_spmb' => $ispmb,
				'i_product' => $iproduct,
				'i_product_grade' => $iproductgrade,
				'i_product_motif' => $iproductmotif,
				'n_order' => $norder,
				'n_acc' => $nacc,
				'n_saldo' => $nacc,
				'v_unit_price' => $vunitprice,
				'e_product_name' => $eproductname,
				'i_area' => $iarea,
				'e_remark' => $eremark,
				'n_item_no' => $i
			)
		);

		$this->db->insert('tm_spmb_item');
	}

	function updateheader($ispmb, $ispmbold)
	{
		$this->db->set(
			array(
				'f_spmb_acc' => 't',
				'i_spmb_old' => $ispmbold
			)
		);
		$this->db->where('i_spmb', $ispmb);
		$this->db->update('tm_spmb');
	}
	public function deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif)
	{
		$this->db->query("DELETE FROM tm_spmb_item WHERE i_spmb='$ispmb'
								      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
								      and i_product_motif='$iproductmotif'");
		return TRUE;
	}

	public function delete($ispmb)
	{
		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\'' . $ispmb . '\'');
		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\'' . $ispmb . '\'');
		return TRUE;
	}
	function bacasemua()
	{
		$this->db->select("* from tm_spmb order by i_spmb desc", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaproduct($num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function runningnumber($thbl)
	{
		#    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		#		  $row   	= $query->row();
		#		  $thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_spmb,11,6)) as max from tm_spmb 
				    			where substr(i_spmb,6,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nospmb  = $terakhir + 1;
			settype($nospmb, "string");
			$a = strlen($nospmb);
			while ($a < 6) {
				$nospmb = "0" . $nospmb;
				$a = strlen($nospmb);
			}
			$nospmb  = "SPMB-" . $thbl . "-" . $nospmb;
			return $nospmb;
		} else {
			$nospmb  = "000001";
			$nospmb  = "SPMB-" . $thbl . "-" . $nospmb;
			return $nospmb;
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area and a.f_spmb_cancel='f' and a.f_spmb_acc='f'
                      		and a.i_approve2 isnull and (upper(a.i_spmb) ilike '%$cari%' or upper(b.e_area_name) ilike '%$cari%')", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariproduct($cari, $num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset)
	{
		$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset)
	{
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($num, $offset)
	{
		$this->db->select("	a.*, b.e_area_name from tm_spmb a, tr_area b
												where a.i_area=b.i_area and a.f_spmb_cancel='f' and f_spmb_acc='f'
                        and a.i_approve2 isnull
  											ORDER BY a.i_spmb ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariperiode($cari, $num, $offset)
	{
		$this->db->select("	a.*, b.e_area_name from tm_spmb a, tr_area b
												where a.i_area=b.i_area and a.f_spmb_cancel='f' and a.f_spmb_acc='f'
                        and a.i_approve2 isnull and upper(a.i_spmb) like '%$cari%'
  											ORDER BY a.i_spmb ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
