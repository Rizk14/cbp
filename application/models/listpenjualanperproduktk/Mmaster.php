<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($dfrom,$dto)
  {
    $tmp=explode('-',$dfrom);
    $th=$tmp[2];
    $this->db->select(" a.i_product, e.i_product_group, a.e_product_name, b.e_area_name, i.i_customer, i.e_customer_name, 
                        g.e_product_groupname, a.i_area, j.i_salesman, j.e_salesman_name, sum(a.n_deliver) AS jumlah 
                        FROM tm_nota c, tm_nota_item a, tr_area b, tr_product d, tr_product_type e, tr_product_group g, tm_spb h, 
                        tr_customer i, tr_salesman j
                        WHERE c.f_nota_cancel = false AND c.i_sj = a.i_sj AND c.i_area = a.i_area AND c.i_area = b.i_area AND c.i_spb=h.i_spb 
                        and c.i_area=h.i_area AND NOT c.i_nota IS NULL and c.i_salesman=j.i_salesman
                        AND a.i_product = d.i_product AND d.i_product_type = e.i_product_type 
                        AND (a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') 
                        AND a.d_nota <= to_date('$dto', 'dd-mm-yyyy')) AND e.i_product_group = g.i_product_group 
                        and c.i_customer=i.i_customer
                        GROUP BY a.i_area, e.i_product_group, g.e_product_groupname, a.i_product, a.e_product_name, b.e_area_name, 
                        i.i_customer, j.i_salesman, j.e_salesman_name
                        order by g.e_product_groupname, a.i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya()
  {
		$this->db->select(" i_area, e_area_name from tr_area where f_area_real='t' order by i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($iperiode)
  {
		$this->db->select(" distinct i_product, e_product_name, e_product_groupname from vpenjualanperproduk
                        where i_periode='$iperiode'
                        order by e_product_groupname, i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
