<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$iarea)
    {
    if($iarea!='00'){
      if($iperiode>'201512'){
        $this->db->select(" i_area, e_area_name, i_product, e_product_name, e_mutasi_periode, sum(n_saldo_awal) as n_saldo_awal, 
                              sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
		                          sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
		                          sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
		                          sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
		                          from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') where i_area = '$iarea'
		                          group by i_product, e_product_name, e_mutasi_periode, i_area, e_area_name
		                          order by i_area, e_area_name, i_product, e_product_name, e_mutasi_periode",false);
		  }else{
        $this->db->select(" i_area, e_area_name, i_product, e_product_name, e_mutasi_periode, sum(n_saldo_awal) as n_saldo_awal, 
                              sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
		                          sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
		                          sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
		                          sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
		                          from f_mutasi_stock_mo_cust_all('$iperiode') where i_area = '$iarea'
		                          group by i_product, e_product_name, e_mutasi_periode, i_area, e_area_name
		                          order by i_area, e_area_name, i_product, e_product_name, e_mutasi_periode",false);
      }
    }elseif($iarea=='00'||$iarea=='PB'){
      if($iperiode>'201512'){
        $this->db->select(" i_area, e_area_name, i_product, e_product_name, e_mutasi_periode, sum(n_saldo_awal) as n_saldo_awal, 
                              sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
		                          sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
		                          sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
		                          sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
		                          from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') 
		                          group by i_product, e_product_name, e_mutasi_periode, i_area, e_area_name
		                          order by i_area, i_product, e_product_name, e_mutasi_periode",false);
      }else{
        $this->db->select(" i_area, e_area_name, i_product, e_product_name, e_mutasi_periode, sum(n_saldo_awal) as n_saldo_awal, 
                              sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
		                          sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
		                          sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
		                          sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
		                          from f_mutasi_stock_mo_cust_all('$iperiode') 
		                          group by i_product, e_product_name, e_mutasi_periode, i_area, e_area_name
		                          order by i_area, i_product, e_product_name, e_mutasi_periode",false);
      }
    }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$icustomer,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi_consigment a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_customer='$icustomer' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }

    function proses($iperiode,$iarea)
    {
      if($iarea=='00'){
        $istore='AA';
      }else{
        $istore=$iarea;
      }
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='MTS'
                          and i_area='$istore' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
        $this->db->query(" update tm_dgu_no 
                           set e_periode='$iperiode'
                           where i_modul='MTS' and i_area='$istore'", false);
		  }else{
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode) 
                           values ('MTS','$istore','$iperiode')");
      }
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
      if($store=='AA') $loc='01'; else $loc='00';      
      $this->db->query("	delete from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store'");
      $query = $this->db->query(" select i_product from tm_ic where i_store='$store' group by i_product");
		  if ($query->num_rows() > 0){
#        $this->db->query("	delete from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store'");
			  foreach($query->result() as $gie){
                                          
          $que = $this->db->query("select	area,periode,product,penjualan,pembelian,bbm,bbk,retur,git,pesan,ketoko,daritoko
                                   from vmutasi where periode='$iperiode' and area='$iarea' and product='$gie->i_product'");
		      if ($que->num_rows() > 0){
			      foreach($que->result() as $vie)
            {
            	if($vie->pembelian==null)$vie->pembelian=0;
              if($vie->retur==null)$vie->retur=0;
              if($vie->bbm==null)$vie->bbm=0;
              if($vie->penjualan==null)$vie->penjualan=0;
              if($vie->bbk==null)$vie->bbk=0;
              if($vie->git==null)$vie->git=0;
              if($vie->ketoko==null)$vie->ketoko=0;
              if($vie->daritoko==null)$vie->daritoko=0;
              $blawal=substr($iperiode,4,2)-1;
              if($blawal==0){
                $perawal=substr($iperiode,0,4)-1;
                $perawal=$perawal.'12';
              }else{
                $perawal=substr($iperiode,0,4);
                $perawal=$perawal.substr($iperiode,4,2)-1;;
              }
              $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$vie->product'");
              if ($quer->num_rows() > 0){
                foreach($quer->result() as $ni)
                {
                  $sawal=$ni->n_stockopname;
                }
			          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm)-($vie->penjualan+$vie->bbk);
			          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
			          if($store=='AA') $loc='01'; else $loc='00';
                $opname=0;
                $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
                if ($qur->num_rows() > 0){
                  foreach($qur->result() as $nu){
                    $opname=$nu->n_stockopname;
                  }
                }
			          $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                                                     n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                           n_mutasi_daritoko)
			                                                     values
			                                                    ('$iperiode','$store','$vie->product','$grade','00',
                                                           '$loc','00',$sawal,
			                                                     $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname
                                                           ,$vie->git,$vie->ketoko,$vie->daritoko)");
              }else{
                $sawal=0;
			          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
			          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
			          if($store=='AA') $loc='01'; else $loc='00';
                $opname=0;
                $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
                if ($qur->num_rows() > 0){
                  foreach($qur->result() as $nu){
                    $opname=$nu->n_stockopname;
                  }
                }
			          $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                                                     n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                           n_mutasi_daritoko)
			                                                     values
			                                                    ('$iperiode','$store','$vie->product','$grade','00',
                                                           '$loc','00',$sawal,$vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,
                                                           $vie->bbk,$akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
              }
              $qu = $this->db->query("select i_product,i_product_motif,i_product_grade,n_saldo_akhir
                                      from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
		          if ($qu->num_rows() > 0)
              {
			          foreach($qu->result() as $vei)
                {
	                $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
	                $row   	= $querys->row();
                  if($row->c==$iperiode){
                    $this->db->query("	update tm_ic set 
                                        n_quantity_stock=$vei->n_saldo_akhir
                                        where i_store='$store' and i_product='$vei->i_product'
                                        and i_product_motif='$vei->i_product_motif' and i_product_grade='$vei->i_product_grade'");
                  }
                }
              }
            }
		      }else{

            $blawal=substr($iperiode,4,2)-1;
            if($blawal==0){
              $perawal=substr($iperiode,0,4)-1;
              $perawal=$perawal.'12';
            }else{
              $perawal=substr($iperiode,0,4);
              $perawal=$perawal.substr($iperiode,4,2)-1;;
            }
            $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                     where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$gie->i_product'");
            if ($quer->num_rows() > 0){
              foreach($quer->result() as $ni)
              {
                $stock=$ni->n_stockopname;
              }
              if($stock>0){
                $sawal=$stock;
			          $akhir=$sawal;
			          if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
			          if($store=='AA') $loc='01'; else $loc='00';
                $opname=0;
                $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                          where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$gie->i_product'");
                if ($qur->num_rows() > 0){
                  foreach($qur->result() as $nu){
                    $opname=$nu->n_stockopname;
                  }
                }
			          $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                                                     n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                                                     values
			                                                    ('$iperiode','$store','$gie->i_product','$grade','00',
                                                           '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname)");
              }
            }else{
              $blawal=substr($perawal,4,2)-1;
              if($blawal==0){
                $perawalx=substr($perawal,0,4)-1;
                $perawalx=$perawalx.'12';
              }else{
                $perawalx=substr($perawal,0,4);
                $perawalx=$perawalx.substr($perawal,4,2)-1;;
              }
              $quer=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                        where e_mutasi_periode='$perawalx' and i_area='$iarea' and i_product='$gie->i_product'");
              if ($quer->num_rows() > 0){
                foreach($quer->result() as $ni)
                {
                  $stock=$ni->n_stockopname;
                }
                if($stock>0){
                  $sawal=$stock;
			            $akhir=$sawal;
			            if(substr($gie->i_product,0,1)=='Z') $grade='B'; else $grade='A';
			            if($store=='AA') $loc='01'; else $loc='00';
                  $opname=0;
                  $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                            where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$gie->i_product'");
                  if ($qur->num_rows() > 0){
                    foreach($qur->result() as $nu){
                      $opname=$nu->n_stockopname;
                    }
                  }
			            $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
			                                                       i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
			                                                       n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
			                                                       n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname)
			                                                       values
			                                                      ('$iperiode','$store','$gie->i_product','$grade','00',
                                                             '$loc','00',$sawal,0,0,0,0,0,0,$akhir,$opname)");
                }
              }
            }
          }
        }
      }
######
      $que = $this->db->query("select	area,periode,product,penjualan,pembelian,bbm,bbk,retur,git,pesan,ketoko,daritoko
                               from vmutasi where periode='$iperiode' and area='$iarea' 
                               and product not in(select i_product from tm_ic where i_store='$store')");
      if ($que->num_rows() > 0){
        foreach($que->result() as $vie)
        {
        	if($vie->pembelian==null)$vie->pembelian=0;
          if($vie->retur==null)$vie->retur=0;
          if($vie->bbm==null)$vie->bbm=0;
          if($vie->penjualan==null)$vie->penjualan=0;
          if($vie->bbk==null)$vie->bbk=0;
          if($vie->git==null)$vie->git=0;
          if($vie->ketoko==null)$vie->ketoko=0;
          if($vie->daritoko==null)$vie->daritoko=0;
          $blawal=substr($iperiode,4,2)-1;
          if($blawal==0){
            $perawal=substr($iperiode,0,4)-1;
            $perawal=$perawal.'12';
          }else{
            $perawal=substr($iperiode,0,4);
            $perawal=$perawal.substr($iperiode,4,2)-1;;
          }
          $quer=$this->db->query(" select * from tm_stockopname_item 
                                   where e_mutasi_periode='$perawal' and i_area='$iarea' and i_product='$vie->product'");
          if ($quer->num_rows() > 0){
            foreach($quer->result() as $ni)
            {
              $sawal=$ni->n_stockopname;
            }
	          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
	          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
	          if($store=='AA') $loc='01'; else $loc='00';
            $opname=0;
            $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                      where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
            if ($qur->num_rows() > 0){
              foreach($qur->result() as $nu){
                $opname=$nu->n_stockopname;
              }
            }
	          $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
	                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
	                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
	                                                     n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,


                                                       n_mutasi_daritoko)
	                                                     values
	                                                    ('$iperiode','$store','$vie->product','$grade','00',

                                                       '$loc','00',$sawal,
	                                                     $vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,$akhir,$opname,
                                                       $vie->git,$vie->ketoko,$vie->daritoko)");
          }else{
            $sawal=0;
	          $akhir=$sawal+($vie->pembelian+$vie->retur+$vie->bbm+$vie->daritoko)-($vie->penjualan+$vie->bbk+$vie->ketoko);
	          if(substr($vie->product,0,1)=='Z') $grade='B'; else $grade='A';
	          if($store=='AA') $loc='01'; else $loc='00';
            $opname=0;
            $qur=$this->db->query(" select n_stockopname from tm_stockopname_item 
                                      where e_mutasi_periode='$iperiode' and i_area='$iarea' and i_product='$vie->product'");
            if ($qur->num_rows() > 0){
              foreach($qur->result() as $nu){
                $opname=$nu->n_stockopname;
              }
            }
	          $this->db->query("	insert into tm_mutasi (e_mutasi_periode,i_store,i_product,i_product_grade,i_product_motif,
	                                                     i_store_location,i_store_locationbin,n_saldo_awal,n_mutasi_pembelian,
	                                                     n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,n_mutasi_returpabrik,
	                                                     n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,n_mutasi_git,n_mutasi_ketoko,
                                                       n_mutasi_daritoko)
	                                                     values
	                                                    ('$iperiode','$store','$vie->product','$grade','00',
                                                       '$loc','00',$sawal,$vie->pembelian,$vie->retur,$vie->bbm,$vie->penjualan,0,$vie->bbk,
                                                       $akhir,$opname,$vie->git,$vie->ketoko,$vie->daritoko)");
          }
          $qu = $this->db->query("select * from tm_mutasi where e_mutasi_periode='$iperiode' and i_store='$store' and i_product='$vie->product'");
          if ($qu->num_rows() > 0)
          {
	          foreach($qu->result() as $vei)
            {
              $querys = $this->db->query("SELECT to_char(current_timestamp,'yyyymm') as c");
              $row   	= $querys->row();
              if($row->c==$iperiode){
                $this->db->query("	insert into tm_ic (n_quantity_stock, i_product, i_product_motif, i_product_grade, i_store, 
                                    i_store_location, i_store_locationbin,f_product_active) 
                                    values
                                    ($vei->n_saldo_akhir,'$vei->i_product','$vei->i_product_motif','$vei->i_product_grade','$store','$loc','00','t')");
              }
            }
          }
        }
      }
######
    }
    function detail($areanya,$iperiode,$iproduct)
    {
	    $this->db->select(" a.*, b.e_customer_name, c.e_product_name from vmutasiconsigmentdetail a , tr_customer b, tr_product c
	                        where substring(a.customer,1,2)='$areanya' and c.i_product=a.product and a.customer = b.i_customer and a.product='$iproduct'
	                        and a.periode='$iperiode' order by a.periode, a.urutan",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacacustomer($cari,$num,$offset,$icustomer)
  {
    $iarea=$this->session->userdata("i_area");
    if($iarea=='00'){
      if($icustomer==''){
        $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.i_spg, c.e_spg_name
                                      from tr_customer_consigment a, tr_customer b, tr_spg c
                                      where a.i_customer=b.i_customer and a.i_customer like '%$cari%' and b.i_customer=c.i_customer
                                      order by a.i_customer",false)->limit($num,$offset);
		  }else{
        $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.i_spg, c.e_spg_name
                            from tr_customer_consigment a, tr_customer b, tr_spg c
                            where a.i_customer=b.i_customer and a.i_customer='$icustomer' and a.i_customer like '%$cari%'
                            and b.i_customer=c.i_customer
                            order by a.i_customer", false)->limit($num,$offset);
      }
    }else{
      if($icustomer==''){
        $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.i_spg, c.e_spg_name
                                      from tr_customer_consigment a, tr_customer b, tr_spg c
                                      where a.i_customer=b.i_customer and a.i_customer like '%$cari%' and b.i_customer=c.i_customer
                                      and b.i_area='$iarea'
                                      order by a.i_customer",false)->limit($num,$offset);
		  }else{
        $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.i_spg, c.e_spg_name
                            from tr_customer_consigment a, tr_customer b, tr_spg c
                            where a.i_customer=b.i_customer and a.i_customer='$icustomer' and a.i_customer like '%$cari%'
                            and b.i_customer=c.i_customer and b.i_area='$iarea'
                            order by a.i_customer", false)->limit($num,$offset);
      }
    }
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    return $query->result();
	  }
  }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
