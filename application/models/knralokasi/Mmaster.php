<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name,
                               sum(c.v_sisa) as v_sisa from tm_dt a, tr_area b, tm_dt_item c
                             where a.i_area = b.i_area
                             and a.f_sisa = 't'
                             and a.i_dt=c.i_dt and a.i_area=c.i_area
                             and (upper(a.i_dt) like '%$cari%'
                               or upper(b.e_area_name) like '%$cari%'
                               or upper(a.i_area) like '%$cari%')
                             group by a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name
                             order by a.i_dt desc ",false)->limit($num,$offset);
               //and c.v_sisa<>c.v_jumlah
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($cari,$num,$offset)
    {
      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
               where a.i_customer=b.i_customer
               and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
               and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
               or upper(a.i_spb) like '%$cari%' or upper(a.i_nota) like '%$cari%')
               order by a.i_nota desc ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function insertheader($ialokasi,$ikn,$iarea,$icustomer,$dkn,$dalokasi,$vjumlahx,$vlebih)
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_alokasiknr (i_alokasi,i_kn,i_area,i_jenis_bayar,i_customer,d_alokasi,v_jumlah,v_lebih,d_entry)
                  values
                ('$ialokasi','$ikn','$iarea','04','$icustomer','$dalokasi',$vjumlahx,$vlebih,'$dentry')");
  }
  function insertheaderpembulatan($ialokasipembulatan,$ikn,$iarea,$ijenisbayar,$icustomer,$dkn,$dalokasi,$vsiso,$vlebih)
  {
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;
    $this->db->query("insert into tm_alokasiknr (i_alokasi,i_kn,i_area,i_jenis_bayar,i_customer,d_alokasi,v_jumlah,v_lebih,d_entry)
                  values
                ('$ialokasipembulatan','$ikn','$iarea','$ijenisbayar','$icustomer','$dalokasi',$vsiso,$vlebih,'$dentry')");
  }
   function jmlasalkn(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->select("* from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function deleteheader(  $ipl,$idt,$iarea,$ddt)
    {
      $this->db->query("delete from tm_pelunasan where i_pelunasan='$ipl' and i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'",false);
   }
   function updategiro($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_giro set v_sisa=v_sisa-$pengurang+$asal, f_giro_use='t'
                    where i_giro='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where i_customer_groupbayar='$group'))");
    }
   function updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear)
    {
      $this->db->query("update tm_kum set v_sisa=v_sisa-$pengurang+$asal
                          where i_kum='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group')) and n_kum_year='$nkuyear'");
    }
   function updatekn($group,$iarea,$igiro,$pengurang,$asal)
     {
      $this->db->select(" v_sisa from tm_kn where i_kn='$igiro'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){
          $sisa=$xx->v_sisa-$pengurang;
          if($sisa<0){
            return false;
            break;
          }else{
            $this->db->query("update tm_kn set v_sisa=v_sisa-$pengurang+$asal
                        where i_kn='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar
                        where i_customer_groupbayar='$group'))");
            return true;
          }
        }
      }else{
        return false;
      }
    }
   function updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal)
    {
      $this->db->query("update tm_pelunasan_lebih set v_lebih=0
                          where i_pelunasan='$egirobank' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where
                      i_customer_groupbayar='$group'))");
    }
  function updatesaldo($group,$icustomer,$pengurang)
    {
      $this->db->query("update tr_customer_groupar set v_saldo=v_saldo-$pengurang where i_customer='$icustomer' and i_customer_groupar='$group'");
    }
   function insertdetail($ialokasi,$ikn,$iarea,$inota,$dnota,$dkn,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark)
  {
        $this->db->query("insert into tm_alokasiknr_item
                      (i_alokasi,i_kn,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark)
                      values
                      ('$ialokasi','$ikn','$iarea','$inota','$dnota',$vjumlah,$vsisa,$i,'$eremark')");
  }
  function insertdetailpembulatan($ialokasipembulatan,$ikn,$iarea,$inota,$dnota,$dkn,$vsiso,$vsisa,$i,$ipelunasanremark,$eremark)
  {
        $this->db->query("insert into tm_alokasiknr_item
                      (i_alokasi,i_kn,i_area,i_nota,d_nota,v_jumlah,v_sisa,n_item_no,e_remark)
                      values
                      ('$ialokasipembulatan','$ikn','$iarea','$inota','$dnota',$vsiso,$vsisa,$i,'$eremark')");
  }
   function updatedt($idt,$iarea,$ddt,$inota,$vsisa)
    {
      $this->db->query("update tm_dt_item set v_sisa=$vsisa where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt' and i_nota='$inota'");
    }
   function updatenota($inota,$vsisa)
    {
      $this->db->select(" v_sisa from tm_nota where i_nota='$inota'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        foreach($query->result() as $xx){
          $sisa=$xx->v_sisa-$vsisa;
          if($sisa<0){
            return false;
            break;
          }else{
            $this->db->query("update tm_nota set v_sisa=v_sisa-$vsisa where i_nota='$inota'");
            return true;
          }
        }
      }else{
        return false;
      }
    }
    function updatenotapembulatan($inota,$vsiso)
    {
      $this->db->query("update tm_nota set v_sisa=v_sisa-$vsiso where i_nota='$inota'");
    }
  function hitungsisadt($idt,$iarea,$ddt)
    {
      $this->db->select(" sum(v_sisa) as v_sisa from tm_dt_item
               where i_area='$iarea'
               and i_dt='$idt' and d_dt='$ddt'
               group by i_dt, i_area",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
            $jml=$row->v_sisa;
         }
         return $jml;
      }
  }
   function updatestatusdt($idt,$iarea,$ddt)
    {
      $this->db->query("update tm_dt set f_sisa='f' where i_dt='$idt' and i_area='$iarea' and d_dt='$ddt'");
    }
   function deletedetail($ipl,$idt,$iarea,$inota,$ddt)
    {
      $this->db->query("DELETE FROM tm_pelunasan_item WHERE i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'
                      and i_nota='$inota' and d_dt='$ddt'");
    }
   function bacacustomer($ikn,$iarea,$num,$offset){
      $this->db->select(" b.* from tm_kn a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.v_sisa>0
                     and a.i_kn = '$ikn' and a.i_area = '$iarea'
                     order by a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caricustomer($cari,$ikn,$iarea,$num,$offset){
      $this->db->select(" b.* from tm_dt_item a, tr_customer b
                     where a.i_customer=b.i_customer
                     and a.v_sisa>0
                     and a.i_kn = '$ikn' and a.i_area = '$iarea'
              and (upper(b.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                     order by a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagiro($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select(" a.* from tm_giro a, tr_customer_groupar b
                     where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_giro a, tr_customer_groupbayar b
                     where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                     and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                     order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
#                    and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function carigiro($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_giro a, tr_customer_groupar b
							where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
							and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
              and (upper(a.i_giro) like '%$cari%') and not a.d_giro_cair isnull and a.d_giro_cair<='$dbukti'
							order by a.i_giro,a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
##########
   function bacatunai($icustomer,$iarea,$num,$offset,$group,$dbukti){

     $coa='111.3'.$iarea;
     $this->db->select("a.*, c.d_rtunai, e.e_bank_name, e.i_coa, c.i_bank
                        from tm_tunai a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d, tr_bank e
                        where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                        and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$dbukti'
                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and c.i_bank=e.i_bank
                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                        ",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function caritunai($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
	  $coa='111.3'.$iarea;
    $this->db->select("a.*, c.d_rtunai, e.e_bank_name, e.i_coa, c.i_bank
                      from tm_tunai a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d, tr_bank e
                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                      and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                      and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$dbukti'
                      and a.v_sisa>0 and a.v_sisa=a.v_jumlah and c.i_bank=e.i_bank
                      and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and (upper(a.i_tunai) like '%$cari%')
                      ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
   function updatetunai($group,$iarea,$igiro,$pengurang,$asal)
    {
      $this->db->query("update tm_tunai set v_sisa=v_sisa-$pengurang+$asal
                    where i_tunai='$igiro' and (i_area='$iarea' or i_customer in(select i_customer from tr_customer_groupbayar where i_customer_groupbayar='$group'))");
    }
##########
   function bacakn($icustomer,$iarea,$num,$offset,$group,$xdbukti){
      /*$this->db->select(" a.* from tm_kn a, tr_customer_groupar b
                               where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0
                               order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      */

      $this->db->select(" a.*, c.e_customer_name, c.e_customer_address, c.e_customer_city 
                          from tm_kn a, tr_customer_groupbayar b, tr_customer c
                          where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                          and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f' and a.i_customer=c.i_customer and a.i_kn_type='02'
                          order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);

      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carikn($cari,$icustomer,$iarea,$num,$offset,$group,$xdbukti){
      $this->db->select(" a.*, c.e_customer_name, c.e_customer_address, c.e_customer_city 
                          from tm_kn a, tr_customer_groupar b, tr_customer c
                          where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.v_sisa>0 and d_kn<='$xdbukti'
                          and (upper(i_kn) like '%$cari%') and a.f_kn_cancel='f' and a.i_customer=c.i_customer and a.i_kn_type='02'
                          order by a.i_kn,a.i_customer ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaku($icustomer,$iarea,$num,$offset,$group,$dbukti){
      $this->db->select("a.* from tm_kum a, tr_customer_groupar b
                         where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.d_kum<='$dbukti'
                         and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                         order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.* from tm_kum a, tr_customer_groupbayar b
                             where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                             and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f'
                             order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function cariku($cari,$icustomer,$iarea,$num,$offset,$group,$dbukti){
		$this->db->select(" a.* from tm_kum a, tr_customer_groupar b
					              where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
					              and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                        and (upper(a.i_kum) like '%$cari%') and a.d_kum<='$dbukti'
					              order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
   function bacaku2($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.* from tm_kum a
               where a.i_customer='$icustomer'
               and a.i_area='$iarea'
               and a.v_sisa>0
               and a.f_close='f'
               order by a.i_kum,a.i_customer",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacagirocek($num,$offset,$area){
    if($area=='00'||$area=='PB'){
      $this->db->select(" * from tr_jenis_bayar where i_jenis_bayar>'05' order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_jenis_bayar where i_jenis_bayar>'05' order by i_jenis_bayar ",FALSE)->limit($num,$offset);
    }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function runningnumberpl($iarea,$thbl){
      $th   = substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
      $this->db->select(" n_modul_no as max from tm_dgu_no
                        where i_modul='KAR'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea' for update", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
           $terakhir=$row->max;
         }
         $noal  =$terakhir+1;
         $this->db->query(" update tm_dgu_no
                          set n_modul_no=$noal
                          where i_modul='KAR'
                          and substr(e_periode,1,4)='$th'
                          and i_area='$iarea'", false);
         settype($noal,"string");
         $a=strlen($noal);
         while($a<5){
           $noal="0".$noal;
           $a=strlen($noal);
         }
         $noal  ="AKR-".$thbl."-".$noal;
         return $noal;
      }else{
         $noal  ="00001";
         $noal  ="AKR-".$thbl."-".$noal;
         $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                         values ('KAR','$iarea','$asal',1)");
         return $noal;
      }
    }
   function bacapelunasan($icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
/*
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan
               from tm_pelunasan_lebih a, tr_customer_groupbayar b
            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
               group by a.i_dt, a.d_bukti, a.i_area ",FALSE)->limit($num,$offset);
#              and a.i_area='$iarea'
*/
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function caripelunasan($cari,$icustomer,$iarea,$num,$offset,$group){
      $this->db->select(" a.i_dt, min(a.v_jumlah) as v_jumlah, min(a.v_lebih) as v_lebih, a.i_area, a.i_pelunasan,
                  a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2)) as i_pelunasan, a.i_customer
               from tm_pelunasan_lebih a, tr_customer_groupar b
            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
               and a.v_lebih>0 and a.f_pelunasan_cancel='f'
                and (upper(a.i_pelunasan) like '%$cari%') 
               group by a.i_dt, a.d_bukti, a.i_area, a.i_customer, a.i_pelunasan ",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
  function cekpl($iarea,$ipl,$idt){
      $this->db->select(" i_pelunasan from tm_pelunasan where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        $this->db->select(" max(substring(i_pelunasan,9,2)) as no from tm_pelunasan where i_pelunasan like '$idt%' and i_area='$iarea'",FALSE);
        $quer = $this->db->get();
        if ($quer->num_rows() > 0){
          foreach($quer->result() as $tmp){
            $nopl=$tmp->no+1;
            break;
          }
        }
        settype($nopl,"string");
        $a=strlen($nopl);
        while($a<2){
          $nopl="0".$nopl;
          $a=strlen($nopl);
        }
        $nopl  = $idt."-".$nopl.substr($ipl,10,1);
        return $nopl;
      }else{
      return $ipl;
    }
   }
  function bacapl($iarea,$ialokasi,$ikn){
      $this->db->select(" a.*, b.e_area_name, c.e_customer_name, d.e_jenis_bayarname, e.d_kn, c.e_customer_address, c.e_customer_city
                             from tm_alokasiknr a
                             inner join tr_area b on (a.i_area=b.i_area)
                             inner join tr_customer c on (a.i_customer=c.i_customer)
                             inner join tr_jenis_bayar d on (a.i_jenis_bayar=d.i_jenis_bayar)
                             inner join tm_kn e on (a.i_kn=e.i_kn and a.i_area=e.i_area)
                             where
                             upper(a.i_kn)='$ikn'
                             and upper(a.i_alokasi)='$ialokasi' and upper(a.i_area)='$iarea'",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function sisa($iarea,$ialokasi,$ikn){
      $this->db->select(" v_sisa as sisa
                         from tm_kn
                         where i_area='$iarea'
                         and i_kn='$ikn'",FALSE);
      $query = $this->db->get();
      foreach($query->result() as $isi){
         return $isi->sisa;
      }
   }
   function bacadetailpl($iarea,$ialokasi,$ikn){
      $this->db->select(" a.*, b.v_sisa as v_sisa_nota, b.v_nota_netto as v_nota
                          from tm_alokasiknr_item a
                          inner join tm_nota b on (a.i_nota=b.i_nota)
                          where a.i_alokasi = '$ialokasi'
                          and a.i_area='$iarea'
                          and a.i_kn='$ikn'
                          order by a.i_alokasi,a.i_area ",FALSE);
#
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacanota($iarea,$icustomer,$num,$offset,$group,$cari){
      $this->db->select("c.v_nota_netto as v_jumlah, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                         from tr_customer_groupbayar b, tm_nota c
                         where b.i_customer_groupbayar='$group' and c.i_customer=b.i_customer and c.v_sisa>0
                         and (upper(c.i_nota) like '%$cari%')
                         order by c.i_nota ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function carinota($cari,$icustomer,$num,$offset,$group){
      $this->db->select(" v.v_nota_netto as a.v_jumlah, a.i_customer, c.d_nota, a.i_nota, c.v_sisa
                          from tr_customer_groupbayar b, tm_nota c
                          where b.i_customer_groupbayar='$group' and c.i_customer=b.i_customer and c.v_sisa>0
                          and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')
                          order by c.i_nota ",FALSE)->limit($num,$offset);
#and a.i_area=c.i_area
      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
   {
      $this->db->select(" a.i_kn, a.i_area, a.d_kn, a.v_netto as v_jumlah, b.e_area_name, a.v_sisa, c.i_customer, c.e_customer_name, 
                          c.e_customer_address, c.e_customer_city 
                          from tm_kn a, tr_area b, tr_customer c
                          where a.i_area=b.i_area and a.i_area='$iarea' and a.v_sisa>0 and a.f_kn_cancel='f'
                          and a.d_kn >= to_date('$dfrom','dd-mm-yyyy') and a.d_kn <= to_date('$dto','dd-mm-yyyy')
                          and (upper(a.i_kn) like '%$cari%') and a.i_customer=c.i_customer and a.i_kn_type='01'
                          order by a.i_kn, a.i_area, a.d_kn",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
   function bacaremark($num,$offset) {
      $this->db->select("* from tr_pelunasan_remark order by i_pelunasan_remark", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
########## Posting ###########
	function jenisbayar($ipl,$iarea)
    {
		$this->db->select(" i_jenis_bayar from tm_alokasi where i_alokasi='$ipl' and i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_jenis_bayar;
			}
			return $xxx;
		}
    }
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$ipelunasan,$iarea,$egirodescription,$fclose,$dbukti )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjuml,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjuml','$dbukti','$dbukti','$dentry')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjuml,$dbukti)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjuml','$dbukti','$dbukti','$dentry')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjuml,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjuml,'$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjuml,$dbukti,$egirodescription)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$egirodescription=str_replace("'","''",$egirodescription);
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjuml','$iarea','$dbukti','$egirodescription','$dentry')");
	}
	function updatepelunasan($ipl,$iarea,$idt)
    {
		$this->db->query("update tm_pelunasan set f_posting='t' where i_pelunasan='$ipl' and i_area='$iarea' and i_dt='$idt'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjuml)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjuml, v_saldo_akhir=v_saldo_akhir+$vjuml
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjuml)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjuml, v_saldo_akhir=v_saldo_akhir-$vjuml
						          where i_coa='$acckredit' and i_periode='$iperiode'");
	}
########## End of Posting ###########
}
?>
