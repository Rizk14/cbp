<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function insertheader($ido,$isupplier,$iop,$iarea,$ddo,$vdogross)
    {
    $query   = $this->db->query("SELECT current_timestamp as c");
    $row     = $query->row();
    $dentry  = $row->c;
		$this->db->set(
    		array(
			'i_do'					=> $ido,
			'i_supplier'		=> $isupplier,
			'i_op'					=> $iop,
			'i_area'				=> $iarea,
			'd_do'					=> $ddo, 
			'v_do_gross'		=> $vdogross,
			'd_entry'       => $dentry
    		)
    	);
    	$this->db->insert('tm_do');
    }

    function updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliver)
    {
    	$this->db->set(
    		array(
					'n_delivery'			=> $ndeliver
    		)
    	);
    	$this->db->where('i_op',$iop);
    	$this->db->where('i_product',$iproduct);
    	$this->db->where('i_product_grade',$iproductgrade);
    	$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_op_item');
    }
    function updatespbdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliver)
    {
		$this->db->select(" i_reff, i_area from tm_op
							where i_op='$iop'", false);
		$query = $this->db->get();
		if($query->num_rows>0){
			foreach($query->result() as $row){
				$spb =$row->i_reff;
				$area=$row->i_area;
			}
			$this->db->query("update tm_spb_item set n_deliver=n_deliver+$ndeliver, n_stock=n_stock+$ndeliver
							  where i_spb='$spb' and i_area='$area' and i_product='$iproduct' and i_product_grade='$iproductgrade'
							  and i_product_motif='$iproductmotif'");
		}
    }
    
    function updateheader($ido,$isupplier,$iop,$iarea,$ddo,$vdogross)
    {
		$data = array(
			'i_do'					=> $ido,
			'i_supplier'		=> $isupplier,
			'i_op'					=> $iop,
			'i_area'				=> $iarea,
			'd_do'					=> $ddo, 
			'v_do_gross'		=> $vdogross
    		);
		$this->db->where('i_do',$ido);
		$this->db->where('i_supplier', $isupplier);
    	$this->db->update('tm_do',$data);
    }
     
    function insertdetail($ido,$isupplier,$iop,$iproduct,$iproductmotif,$iproductgrade,$ndeliv,$vmill,$eproduct,$nitem)
    {
		  $this->db->set(
      		array(
			  'i_do'					  => $ido,
			  'i_supplier'			=> $isupplier,
			  'i_product'				=> $iproduct,
			  'i_product_grade'	=> $iproductgrade,
			  'i_product_motif'	=> $iproductmotif,
			  'e_product_name'	=> $eproduct,
			  'n_deliver'				=> $ndeliv,
			  'v_product_mill'	=> $vmill,
			  'i_op'					  => $iop,
			  'n_item_no'					  => $nitem
      		)
      	);
      	$this->db->insert('tm_do_item');
    }
	function updateopitem($iop,$iproduct,$iproductgrade,$iproductmotif,$jumlah,$ido)
    {
		$this->db->query("	update tm_ic set n_quantity_stock=n_quantity_stock+$jumlah
						 	where i_product='$iproduct'
							and i_product_grade='$iproductgrade'
							and i_product_motif='$iproductmotif'
							and i_store='AA'
							and i_store_location='01'
							and i_store_locationbin='00'", false);

		$query=$this->db->query("select * from tm_op_item 
								 where i_op='$iop' 
								 and i_product='$iproduct'
								 and i_product_grade='$iproductgrade'
								 and i_product_motif='$iproductmotif'", false);
		foreach ($query->result() as $row)
		{
		   	$jmltmp=$row->n_delivery;
			$jmlop =$row->n_order;
		}
		if(($jmltmp+$jumlah)<=$jmlop){
			$this->db->set(
				array(
				'n_delivery'	=> $jumlah
				)
			);
			$this->db->where('i_op',$iop);
			$this->db->where('i_product',$iproduct);
			$this->db->where('i_product_grade',$iproductgrade);
			$this->db->where('i_product_motif',$iproductmotif);
			$this->db->update('tm_op_item');
		}else{
			return false;
		}
    }
     function updatendeliver($ido,$iproduct,$ndeliv,$ddo,$emutasiperiode){
    	$this->db->set(
				array(
				'n_deliver'	=> $ndeliv,
				'e_mutasi_periode' => $emutasiperiode,
				'd_do' => $ddo
				)
			);
			$this->db->where('i_do',$ido);
			$this->db->where('i_product',$iproduct);
			$this->db->update('tm_do_item');
    }
    function updatevdo($ido,$vdo){
    	$this->db->set(
				array(
				'v_do_gross'	=> $vdo
				)
			);
			$this->db->where('i_do',$ido);
			#$this->db->where('i_product',$iproduct);
			$this->db->update('tm_do');
    }
	function cekadaop($iop,$iproduct,$iproductmotif,$iproductgrade,$iarea)
	{
		$que=$this->db->query("select * from tm_op where i_op_old like '%$iop%' and i_area='$iarea'", false);
		if($que->num_rows()>0){
			foreach($que->result() as $tmp){
				$query=$this->db->query("select i_op from tm_op_item 
										 where i_op='$tmp->i_op'and i_product='$iproduct' 
										 and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'", false);
				return $query->num_rows();
			}
		}
	}
	function cekdataitem($ido,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$thbl)
	{
		$query=$this->db->query("select i_do from tm_do_item 
								 where i_do='$ido' and i_supplier='$isupplier'
								 and i_product='$iproduct' and i_product_grade='$iproductgrade' 
								 and i_product_motif='$iproductmotif'", false);
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
			$query=$this->db->query("select i_do from tm_do_item 
									 where i_do='$ido' and i_supplier='$isupplier'
									 and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									 and i_product_motif='$iproductmotif'", false);
			return $query->num_rows();
		}
	}
	function cekdata($ido,$isupplier,$thbl)
	{
		$query=$this->db->query("select i_do from tm_do where i_do='$ido' and i_supplier='$isupplier'", false);
		if($query->num_rows()>0){
			return $query->num_rows();
		}else{
			$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
			$query=$this->db->query("select i_do from tm_do where i_do='$ido' and i_supplier='$isupplier'", false);
			return $query->num_rows();
		}
	}
    function inserttmp($ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
						  $vproductmill,$iproductmotif,$isupplier,$ddo,$iarea,$iop)
    {
		$this->db->select(" n_delivery from tt_do where i_do='$ido' and i_product='$iproduct'
							and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif' 
							and i_area='$iarea' and i_op='$iop'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$this->db->query("	update tt_do set n_delivery=n_delivery+$jumlah
						 	where i_do='$ido' and i_product='$iproduct'
							and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif' 
							and i_area='$iarea' and i_op='$iop'", false);			
		}else{
			$this->db->set(
				array(
				'i_do'					=> $ido,
				'i_supplier'			=> $isupplier,
				'i_product'				=> $iproduct,
				'i_product_grade'		=> $iproductgrade,
				'i_product_motif'		=> $iproductmotif,
				'e_product_name'		=> $eproductname,
				'n_delivery'			=> $jumlah,
				'v_product_mill'		=> $vproductmill,
				'd_do'					=> $ddo,
				'i_area'				=> $iarea,
				'i_op'					=> $iop
				)
			);
			$this->db->insert('tt_do');
		}
    }
	function cekbbm($ido,$ibbmtype,$iarea,$isupplier,$ddo)
	{
		$query=$this->db->query("select i_bbm from tm_bbm 
								 where trim(i_refference_document)='$ido' and i_bbm_type='$ibbmtype' 
								 and i_area='$iarea' and i_supplier='$isupplier' and d_refference_document='$ddo'", false);
		if($query->num_rows()>0){
			foreach($query->result() as $row){
			  $no=$row->i_bbm;
			}
			return $no;
		}
	}
	function updatebbmheader($ido,$ddo,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isupplier)
    {
    	$this->db->set(
    		array(
				'i_refference_document'	=> $ido,
				'd_refference_document'	=> $ddo,
				'd_bbm'					=> $dbbm,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea,
				'i_supplier'			=> $isupplier
    		)
    	);
    	$this->db->where('i_bbm',$ibbm);
		$this->db->where('i_bbm_type',$ibbmtype);
    	$this->db->update('tm_bbm');
    }
	function insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,$vunitprice,$ido,$ibbm,$eremark,$ddo)
    {
		$this->db->select(" n_quantity from tm_bbm_item where i_bbm='$ibbm' and i_product='$iproduct' and i_refference_document='$ido'
							and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			$this->db->query("	update tm_bbm_item set n_quantity=n_quantity+$nquantity
						 	where i_bbm='$ibbm' and i_product='$iproduct' and i_refference_document='$ido'
							and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif' ", false);			
		}else{
      $th=substr($ddo,0,4);
      $bl=substr($ddo,5,2);
      $pr=$th.$bl;
			$this->db->set(
				array(
					'i_bbm'					=> $ibbm,
					'i_refference_document'	=> $ido,
					'i_product'				=> $iproduct,
					'i_product_motif'		=> $iproductmotif,
					'i_product_grade'		=> $iproductgrade,
					'e_product_name'		=> $eproductname,
					'n_quantity'			=> $nquantity,
					'v_unit_price'			=> $vunitprice,
					'e_remark'				=> $eremark,
					'd_refference_document'	=> $ddo,
          'e_mutasi_periode'      => $pr
				)
			);
			$this->db->insert('tm_bbm_item');
		}
    }
	
	function insertbbkheader($ispb,$dspb,$ibbk,$dbbk,$ibbktype,$eremark,$iarea,$isupplier)
    {
    	$this->db->set(
    		array(
				'i_bbk'					=> $ibbk,
				'i_bbk_type'			=> $ibbktype,
				'i_refference_document'	=> $ispb,
				'd_refference_document'	=> $dspb,
				'd_bbk'					=> $dbbk,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea,
				'i_supplier'			=> $isupplier
    		)
    	);
    	
    	$this->db->insert('tm_bbk');
    }
	function insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,
							 $vunitprice,$ispb,$ibbk,$eremark,$dspb,$ibbktype,
							 $istore,$istorelocation,$istorelocationbin)
    {
      $th=substr($dspb,0,4);
      $bl=substr($dspb,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_bbk'					=> $ibbk,
				'i_bbk_type'			=> $ibbktype,
				'i_refference_document'	=> $ispb,
				'i_product'				=> $iproduct,
				'i_product_motif'		=> $iproductmotif,
				'i_product_grade'		=> $iproductgrade,
				'e_product_name'		=> $eproductname,
				'n_quantity'			=> $nquantity,
				'v_unit_price'			=> $vunitprice,
				'e_remark'				=> $eremark,
				'd_refference_document'	=> $dspb,
        'e_mutasi_periode'      => $pr
    		)
    	);
    	
    	$this->db->insert('tm_bbk_item');
    }
	function updatebbkheader($ido,$ddo,$ibbk,$dbbk,$ibbktype,$eremark,$iarea,$isupplier)
    {
    	$this->db->set(
    		array(
				'i_refference_document'	=> $ido,
				'd_refference_document'	=> $ddo,
				'd_bbk'					=> $dbbk,
				'e_remark'				=> $eremark,
				'i_area'				=> $iarea
    		)
    	);
    	$this->db->where('i_bbk',$ibbk);
		$this->db->where('i_bbk_type',$ibbktype);
		$this->db->where('i_supplier',$isupplier);
    	$this->db->update('tm_bbk');
    }
	function cekbbk($ido,$ibbktype,$iarea,$isupplier,$ddo)
	{
		$query=$this->db->query("select i_bbk from tm_bbk 
								 where trim(i_refference_document)='$ido' and i_bbk_type='$ibbktype' 
								 and i_area='$iarea' and i_supplier='$isupplier' and d_refference_document='$ddo'", false);
		if($query->num_rows()>0){
			foreach($query->result() as $row){
			  $no=$row->i_bbk;
			}
			return $no;
		}
	}
	function cekono($iop,$iarea)
	{
		$query= $this->db->query("select i_op from tm_op where i_op_old like '%$iop%' and i_area='$iarea'");
		if($query->num_rows()>0){
			foreach($query->result() as $row){
			  $no=$row->i_op;
			}
			return $no;
		}
	}
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,
													$ido,$q_in,$q_out,$qdo,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ido', '$now', $qdo, 0, $q_ak+$qdo, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_pembelian=n_mutasi_pembelian+$qdo, n_saldo_akhir=n_saldo_akhir+$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin',
																	'$emutasiperiode',0,$qdo,0,0,0,0,0,$qdo,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qdo)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin',
																	'$eproductname',$qdo, 't'
                                )
                              ",false);
    }
  function bacasupplier($cari,$num,$offset)
  {
    $this->db->select(" i_supplier, e_supplier_name from tr_supplier
          where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
			    order by i_supplier",false)->limit($num,$offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
	    return $query->result();
    }
  }
  function bacaarea($iuser,$num,$offset)
    {
	$this->db->select("	* , case when i_area ='00' then '51' when i_area ='01' then '50' when i_area ='02' then '49' 
		 when i_area ='03' then '48'  when i_area ='04' then '59' else 'xx' end as i_customer from tr_area
		where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
		order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacado($icustomer,$yest,$today)
    {
	$this->db->select(" * from dblink('host=192.168.0.7 user=dedy password=dedyalamsyah dbname=sysdbtirai', 
			'select a.i_customer, ''DO-''||to_char(c.d_do,''yymm'')||''-''||substring(c.i_do_code,4,6) as n_do, c.d_do, b.i_do,
			''OP-''||to_char(a.d_op,''yymm'')||''-''||a.i_op_ref as i_op,c.i_do_code,round(sum(b.v_do_gross * b.n_deliver)) as v_do
			from tm_op a
			inner join tm_do_item b on a.i_op = b.i_op
			inner join tm_do c on c.i_do = b.i_do
			where  c.d_do >= ''$yest''  and c.d_do  <= ''$today'' and a.i_customer=''$icustomer''
			and c.f_do_cancel =''f'' and a.f_op_cancel =''f'' and a.i_op_ref is not null
			group by a.i_customer,c.d_do,b.i_do,c.i_do_code,a.d_op,a.i_op_ref') as get_op 
			(i_customer varchar(5),n_do varchar(15) , d_do date, i_do varchar(15),i_op varchar(15), i_do_code varchar(15),v_do numeric)
			 where n_do not in (select i_do from tm_do where d_do >= '$yest' and d_do <= '$today')
			order by i_do", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
     function bacadoitem($ido,$iproduct)
    {
	$this->db->select("	* from dblink('host=192.168.0.7 user=dedy password=dedyalamsyah dbname=sysdbtirai', 
						'select a.i_do_code AS iopcode, substring(b.i_product,1,7) AS iproduct, c.e_product_motifname AS motifname, 
						b.n_deliver AS qty FROM tm_do_item b
						RIGHT JOIN tm_do a ON a.i_do=b.i_do
						INNER JOIN tr_product_motif c ON trim(c.i_product_motif)=trim(b.i_product)
						INNER JOIN tr_product_base d ON trim(d.i_product_base)=trim(c.i_product)
						INNER JOIN tm_op e ON e.i_op=b.i_op
						WHERE a.i_do_code=''$ido'' AND substring(b.i_product,1,7) =''$iproduct'' AND a.f_do_cancel=''f''
						GROUP BY a.d_do, a.i_do_code,b.i_product, c.e_product_motifname, b.n_deliver') as get_op 
						(i_do varchar(15),i_product varchar(7) , e_product_name varchar(255), n_deliver integer)", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacatotal($ido)
	{
		$query= $this->db->query("select sum(v_product_mill * n_deliver) as v_do from tm_do_item where i_do ='$ido'");

		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacaop($iop)
	{
		$query= $this->db->query("select * from tm_op_item where i_op ='$iop' and (n_delivery < n_order or n_delivery is null)");

		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
}
?>
