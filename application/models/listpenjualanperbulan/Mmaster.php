<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
/*
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, sum(a.v_nota_gross) as nota
                          from tm_nota a, tr_customer b, tr_city c
                          where a.f_nota_cancel='f' and to_char(a.d_nota,'yyyymm')='$iperiode' and not a.i_nota isnull and a.i_area='$iarea'
                          and a.i_customer=b.i_customer and b.i_city=c.i_city and b.i_area=c.i_area
                          group by a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman
                          order by c.e_city_name ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
*/
    function bacaperiode($dfrom,$dto,$iarea,$interval)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
      if($iarea=='NA'){

         $sql=" b.kode, b.nama, b.alamat, b.kota, b.area, b.jenis, b.iarea, b.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '02' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '03' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '04' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '05' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '06' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '07' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '08' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '09' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '10' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '11' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '12' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        }

        $sql.="from (select a.kode, a.nama, a.alamat, a.kota, a.area, a.jenis, a.iarea, a.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '02' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '03' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '04' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '05' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '06' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '07' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '08' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '09' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '10' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '11' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '12' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        }

        $sql.=" from ( select kode, nama, alamat, kota, area, jenis, iarea, icity, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_number(to_char(a.d_nota, ''mm''),''99'') as bln, 
              sum(a.v_nota_netto) AS jumlah
              FROM tm_nota a, tr_customer b, tr_city c, tr_area d, tr_customer_class e
              WHERE a.f_nota_cancel = false AND b.i_customer = a.i_customer and a.f_nota_cancel=''f'' 
              AND NOT a.i_nota IS NULL and b.i_city=c.i_city and b.i_area=c.i_area and b.i_area=d.i_area
              AND b.i_customer_class=e.i_customer_class
              AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_char(a.d_nota, ''mm'')
              order by a.i_customer, b.e_customer_name, to_char(a.d_nota, ''mm'')','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (kode text, nama text, alamat text, kota text, area text, jenis text, iarea text, icity text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 
                  Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.=" Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 
                  Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.=" Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 
                  Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.=" Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 
                  Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.=" May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 
                  Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.=" Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                  Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.=" Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 
                  Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.=" Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                  May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.=" Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                  Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.=" Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                  Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.=" Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                  Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.=" Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                  Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" ) as a
                group by a.iarea, a.icity, a.nama, a.kode, a.alamat, a.kota, a.area, a.jenis";
        $sql.=" ) as b
                  group by b.iarea, b.icity, b.nama, b.kode, b.alamat, b.kota, b.area, b.jenis
                  order by b.iarea, b.icity, b.kode, b.nama, b.alamat, b.kota, b.area, b.jenis";
#                order by a.kode, a.nama, a.alamat, a.kota, a.sales, a.area, a.jenis";
      }else{
        $sql=" b.kode, b.nama, b.alamat, b.kota, b.area, b.jenis, b.iarea, b.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '02' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '03' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '04' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '05' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '06' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '07' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '08' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '09' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '10' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '11' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        case '12' :
          $sql.=" sum(b.notajannet) as notajannet, sum(b.notafebnet) as notafebnet, sum(b.notamarnet) as notamarnet, 
                  sum(b.notaaprnet) as notaaprnet, sum(b.notamaynet) as notamaynet, sum(b.notajunnet) as notajunnet, 
                  sum(b.notajulnet) as notajulnet, sum(b.notaaugnet) as notaaugnet, sum(b.notasepnet) as notasepnet, 
                  sum(b.notaoctnet) as notaoctnet, sum(b.notanovnet) as notanovnet, sum(b.notadesnet) as notadesnet ";
          break;
        }


        $sql.="from (select a.kode, a.nama, a.alamat, a.kota, a.area, a.jenis, a.iarea, a.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '02' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '03' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '04' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '05' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '06' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '07' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '08' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '09' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '10' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '11' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        case '12' :
          $sql.=" sum(a.Jan) as notajannet, sum(a.Feb) as notafebnet, sum(a.Mar) as notamarnet, sum(a.Apr) as notaaprnet, 
                  sum(a.May) as notamaynet, sum(a.Jun) as notajunnet, sum(a.Jul) as notajulnet, sum(a.Aug) as notaaugnet, 
                  sum(a.Sep) as notasepnet, sum(a.Oct) as notaoctnet, sum(a.Nov) as notanovnet, sum(a.Des) as notadesnet ";
          break;
        }

        $sql.=" from ( select kode, nama, alamat, kota, area, jenis, iarea, icity, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_number(to_char(a.d_nota, ''mm''),''99'') as bln, 
              sum(a.v_nota_netto) AS jumlah
              FROM tm_nota a, tr_customer b, tr_city c, tr_area d, tr_customer_class e
              WHERE a.f_nota_cancel = false AND b.i_customer = a.i_customer and a.f_nota_cancel=''f'' 
              AND NOT a.i_nota IS NULL and b.i_city=c.i_city and b.i_area=c.i_area and b.i_area=d.i_area
              AND b.i_customer_class=e.i_customer_class AND a.i_area=''$iarea''
              AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_char(a.d_nota, ''mm'')
              order by a.i_customer, b.e_customer_name, to_char(a.d_nota, ''mm'')','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (kode text, nama text, alamat text, kota text, area text, jenis text, iarea text, icity text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 
                  Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.=" Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 
                  Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.=" Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 
                  Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.=" Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 
                  Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.=" May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 
                  Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.=" Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                  Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.=" Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 
                  Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.=" Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                  May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.=" Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                  Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.=" Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                  Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.=" Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                  Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.=" Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                  Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" ) as a
                group by a.iarea, a.icity, a.nama, a.kode, a.alamat, a.kota, a.area, a.jenis";
        $sql.=" ) as b
                  group by b.iarea, b.icity, b.nama, b.kode, b.alamat, b.kota, b.area, b.jenis
                  order by b.iarea, b.icity, b.kode, b.nama, b.alamat, b.kota, b.area, b.jenis";
/*
        $sql=" a.nama, a.kode, a.alamat, a.kota, a.sales, a.area, a.jenis, a.iarea, a.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, 
                  sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, 
                  sum(a.Nov) as notanov, sum(a.Des) as notades ";
          break;
        case '02' :
          $sql.=" sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, 
                  sum(a.Jul) as notajul, sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, 
                  sum(a.Des) as notades, sum(a.Jan) as notajan ";
          break;
        case '03' :
          $sql.=" sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, 
                  sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, 
                  sum(a.Jan) as notajan, sum(a.Feb) as notafeb ";
          break;
        case '04' :
          $sql.=" sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, 
                  sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, 
                  sum(a.Feb) as notafeb, sum(a.Mar) as notamar ";
          break;
        case '05' :
          $sql.=" sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, 
                  sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, 
                  sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr ";
          break;
        case '06' :
          $sql.=" sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, 
                  sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, 
                  sum(a.Apr) as notaapr, sum(a.May) as notamay ";
          break;
        case '07' :
          $sql.=" sum(a.Jul) as notajul, sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, 
                  sum(a.Des) as notades, sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, 

                  sum(a.May) as notamay, sum(a.Jun) as notajun ";
          break;
        case '08' :
          $sql.=" sum(a.Aug) as notaaug, sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, 

                  sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, 
                  sum(a.Jun) as notajun, sum(a.Jul) as notajul ";
          break;
        case '09' :
          $sql.=" sum(a.Sep) as notasep, sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, 
                  sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, 
                  sum(a.Jul) as notajul, sum(a.Aug) as notaaug ";
          break;
        case '10' :
          $sql.=" sum(a.Oct) as notaoct, sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, sum(a.Feb) as notafeb, 
                  sum(a.Mar) as notamar, sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, 
                  sum(a.Aug) as notaaug, sum(a.Sep) as notasep ";
          break;
        case '11' :
          $sql.=" sum(a.Nov) as notanov, sum(a.Des) as notades, sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, 
                  sum(a.Apr) as notaapr, sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, 
                  sum(a.Sep) as notasep, sum(a.Oct) as notaoct ";
          break;
        case '12' :
          $sql.=" sum(a.Des) as notades, sum(a.Jan) as notajan, sum(a.Feb) as notafeb, sum(a.Mar) as notamar, sum(a.Apr) as notaapr, 
                  sum(a.May) as notamay, sum(a.Jun) as notajun, sum(a.Jul) as notajul, sum(a.Aug) as notaaug, sum(a.Sep) as notasep, 

                  sum(a.Oct) as notaoct, sum(a.Nov) as notanov ";
          break;
        }

        $sql.=" from ( select nama, kode, alamat, kota, sales, area, jenis, iarea, icity, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT b.e_customer_name, a.i_customer, b.e_customer_address, c.e_city_name, a.i_salesman, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_number(to_char(a.d_nota, ''mm''),''99'') as bln, 
              sum(a.v_nota_gross) AS jumlah
              FROM tm_nota a, tr_customer b, tr_city c, tr_area d, tr_customer_class e
              WHERE a.f_nota_cancel = false AND b.i_customer = a.i_customer and a.f_nota_cancel=''f'' 
              AND NOT a.i_nota IS NULL and b.i_city=c.i_city and b.i_area=c.i_area and b.i_area=d.i_area
              AND b.i_customer_class=e.i_customer_class AND a.i_area=''$iarea''
              AND (a.d_nota >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_nota <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY b.e_customer_name, a.i_customer, b.e_customer_address, c.e_city_name, a.i_salesman, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_char(a.d_nota, ''mm'')
              order by a.i_customer, b.e_customer_name, to_char(a.d_nota, ''mm'')','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (nama text, kode text, alamat text, kota text, sales text, area text, jenis text, iarea text, icity text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 
                  Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.=" Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 
                  Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.=" Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 

                  Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.=" Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 

                  Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.=" May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 

                  Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.=" Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                  Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.=" Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 

                  Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.=" Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                  May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.=" Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                  Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.=" Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                  Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.=" Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                  Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.=" Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                  Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" ) as a
                group by a.iarea, a.icity, a.nama, a.kode, a.alamat, a.kota, a.sales, a.area, a.jenis
                order by a.iarea, a.icity, a.kode, a.nama, a.alamat, a.kota, a.sales, a.area, a.jenis";
*/
      }
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
