<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iap,$isupplier)
    {
	$this->db->select(" * from tm_bbm 
			   inner join tr_supplier on (tm_bbm.i_supplier=tr_supplier.i_supplier)
			   inner join tr_area on (tm_bbm.i_area=tr_area.i_area)
			   where tm_bbm.i_ap ='$iap'", false);// and tm_bbm.i_supplier='$isupplier'", false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->row();
	}
    }
    function bacadetail($iap,$isupplier)
    {
	$this->db->select(" a.*, b.e_product_motifname from tm_bbm_item a, tr_product_motif b
			   where a.i_ap = '$iap' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
			   order by a.i_product", false);//and i_supplier='$isupplier' 
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function insertheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_ap'		=> $iap,
					'i_supplier'=> $isupplier,
					'i_op'		=> $iop,
					'i_area'	=> $iarea,
					'd_ap'		=> $dap,
					'v_ap_gross'=> $vapgross
    		)
    	);
    	
    	$this->db->insert('tm_bbm');
		$this->db->query("update tm_op set f_op_close='t' where i_op='$iop'");
    }
    function insertdetail($iap,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$vproductmill)
    {
    	$this->db->set(
    		array(
					'i_ap'				=> $iap,
					'i_supplier'		=> $isupplier,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'e_product_name'	=> $eproductname,
					'n_receive'			=> $nreceive,
					'v_product_mill'	=> $vproductmill
    		)
    	);
    	
    	$this->db->insert('tm_bbm_item');
    }
    function updateheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross)
    {
    	$data = array(
					'i_ap'		=> $iap,
					'i_supplier'=> $isupplier,
					'i_op'		=> $iop,
					'i_area'	=> $iarea,
					'd_ap'		=> $dap,
					'v_ap_gross'=> $vapgross
		             );
	$this->db->where('i_ap', $iap);
	$this->db->update('tm_bbm', $data); 
    }

    public function deletedetail($iproduct, $iproductgrade, $iap, $isupplier, $iproductmotif) 
    {
	$delete_spb = $this->db->query("DELETE FROM tm_bbm_item WHERE i_ap='$iap' 
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");//and i_supplier='$isupplier'
	return TRUE;
    }
	
	function uphead($iap,$isupplier,$iop,$iarea,$dap,$vapgross)
    {
    	$data = array(
					'i_ap'		=> $iap,
					'i_supplier'=> $isupplier,
					'i_op'		=> $iop,
					'i_area'	=> $iarea,
					'd_ap'		=> $dap,
					'v_ap_gross'=> $vapgross

            );
	$this->db->where('i_ap', $iap);
	$this->db->where('i_supplier', $isupplier);
	$this->db->update('tm_bbm', $data); 
    }

    public function delete($iap,$isupplier,$iop) 
    {
		$this->db->query('DELETE FROM tm_bbm WHERE i_ap=\''.$iap.'\' and i_supplier=\''.$isupplier.'\'');
		$this->db->query('DELETE FROM tm_bbm_item WHERE i_ap=\''.$iap.'\' and i_supplier=\''.$isupplier.'\'');
		$this->db->query("updates tm_op set f_op_close='f' WHERE i_op='$iop' and i_supplier='G0000' ",False);
    }
    function bacaproduct($op,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif, 
									c.e_product_name as nama, c.v_product_mill as harga 
									from tr_product_motif a, tm_op_item b, tr_product c 
									where a.i_product=c.i_product 
									and b.i_op='$op' and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select(" * from tr_supplier where i_supplier_group='G0000'",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaop($num,$offset)
    {
		$this->db->select(" a.*, 'Lain-lain' as e_supplier_name, c.e_area_name 
							from tm_op a, tr_area c 
							where a.i_area=c.i_area and a.i_supplier='G0000'
							and a.f_op_cancel='f' 
							and a.f_op_close='f'",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_bbm where upper(i_ap) like '%$cari%' or upper(i_supplier) like '%$cari%'
							order by i_ap",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')
							and i_supplier_group='G0000' order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query("  	select a.i_product as kode, 
									a.i_product_motif as motif,
									a.e_product_motifname as namamotif, 
									c.e_product_name as nama,
									c.v_product_mill as harga
									from tr_product_motif a, tr_product c
									where a.i_product=c.i_product
								   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariop($cari,$num,$offset)
    {
		$this->db->select(" * from tm_op where (upper(i_op) like '%$cari%' or upper(i_supplier) like '%$cari%'
							or upper(e_supplier_name) like '%$cari%') and a.i_supplier='G0000'
							and a.f_op_cancel='f' and substr(i_reff,1,4)='SPMB'
							and a.f_op_close='f'",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_op,9,4)) as max from tm_bbm 
				  			where substr(i_ap,4,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$noop  =$terakhir+1;
			settype($noop,"string");
			$a=strlen($noop);
			while($a<4){
			  $noop="0".$noop;
			  $a=strlen($noop);
			}
			$noop  ="AP-".$thbl."-".$noop;
			return $noop;
		}else{
			$noop  ="0001";
			$noop  ="AP-".$thbl."-".$noop;
			return $noop;
		}
    }
}
?>
