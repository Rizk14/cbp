<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikb) 
    {
		$this->db->query("update tm_kbank set f_kbank_cancel='t' WHERE i_kbank='$ikb' and i_periode='$iperiode' and i_area='$iarea' and f_posting='f' and f_close='f'");
#####UnPosting      
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ikb' and i_area='$iarea'");

      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ikb' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }

      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ikb' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ikb' and i_area='$iarea'");
#####
    }
 /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area
                and a.f_kb_cancel='f'
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
	function bacabank($num,$offset)
    {
		  $this->db->select("* from tr_bank order by i_bank", false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function caribank($cari,$num,$offset)
    {
	  $this->db->select("i_bank, e_bank_name, i_coa from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%') 
	                     order by i_bank ", FALSE)->limit($num,$offset);

		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($icoabank,$dfrom,$dto)
    {
		$this->db->select("	a.i_kbank, h.i_rvb, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, f.i_alokasi,e.d_alokasi,a.v_sisa, 
							f.i_nota,f.v_jumlah, a.i_periode, 
							a.f_debet, a.f_posting, a.i_cek, g.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name,e.d_entry
							from 
							tm_kbank a
							left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and a.i_coa_bank=b.i_coa_bank)
							left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
							left join tr_bank d on(a.i_coa=d.i_coa)
							left join tm_alokasi e on(a.i_kbank=e.i_kbank and a.i_area=e.i_area and e.f_alokasi_cancel='f' and a.i_coa_bank=e.i_coa_bank)
							left join tm_alokasi_item f on(e.i_alokasi=f.i_alokasi and e.i_area=f.i_area and e.i_kbank=f.i_kbank)
							left join tm_rvb h on(b.i_rv=h.i_rv and b.i_area=h.i_area and b.i_coa_bank=h.i_coa_bank),
							tr_area g
							where 
							a.i_area=g.i_area 
							and a.f_kbank_cancel='f'
							and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') 
							AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
							and a.i_coa_bank='$icoabank'
							ORDER BY a.i_kbank",false);
		
		/*$this->db->select("	a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, a.i_periode, a.f_debet, a.f_posting, 
	                        a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name
	                        from tr_area e, tm_kbank a
	                        left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02')
	                        left join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
	                        left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
	                        where a.i_area=e.i_area and a.f_kbank_cancel='f'
	                        and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
	                        and (a.i_coa_bank='$icoabank' or c.i_coa='$icoabank')
	                        ORDER BY a.i_kbank",false);*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
