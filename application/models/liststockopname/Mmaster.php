<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($istockopname, $istore) 
    {
			$this->db->query("update tm_stockopname set f_stockopname_cancel='t' WHERE i_stockopname='$istockopname' and i_store='$istore'");
//			$this->db->query("DELETE FROM tm_stockopname_item WHERE i_stockopname='$istockopname' and i_store='$istore'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.* from tm_stockopname a, tr_area b 
				where a.i_stockopname like '%$cari%'
				and a.i_store = b.i_store
				and a.i_area=b.i_area
				and (b.i_area = '$area1' or b.i_area = '$area2' or
					 b.i_area = '$area3' or b.i_area = '$area4' or
					 b.i_area = '$area5') order by i_stockopname desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		$this->db->select(" a.* from tm_stockopname a, tr_area b 
				where a.i_stockopname like '%$cari%'
				and a.i_store = b.i_store
				and (b.i_area = '$area1' or b.i_area = '$area2' or
					 b.i_area = '$area3' or b.i_area = '$area4' or
					 b.i_area = '$area5') order by i_stockopname desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.* from tm_stockopname a, tr_area b 
							where a.i_stockopname like '%$cari%'
							and a.i_store = b.i_store
							and a.i_area = b.i_area
							and a.i_area='$iarea' and
							a.d_stockopname >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_stockopname <= to_date('$dto','dd-mm-yyyy')
							order by a.i_stockopname desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.* from tm_stockopname a, tr_area b 
							where a.i_stockopname like '%$cari%'
							and a.i_store = b.i_store
              and a.i_area = b.i_area
							and a.i_area='$iarea' and
							a.d_stockopname >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_stockopname <= to_date('$dto','dd-mm-yyyy')
							order by a.i_stockopname desc  ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
