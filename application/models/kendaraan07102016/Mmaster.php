<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iperiode,$ikendaraan)
    {
		$this->db->select("	* from tr_kendaraan a 
							          inner join tr_area b on(a.i_area=b.i_area)
							          inner join tr_kendaraan_jenis c on(a.i_kendaraan_jenis=c.i_kendaraan_jenis)
							          inner join tr_kendaraan_bbm d on(a.i_kendaraan_bbm=d.i_kendaraan_bbm)
							          where a.i_periode='$iperiode' and a.i_kendaraan='$ikendaraan'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select(" * from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or e_area_name like '%$cari%'
							order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacajeniskendaraan($num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_jenis order by i_kendaraan_jenis",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carijeniskendaraan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_jenis 
							where upper(i_kendaraan_jenis) like '%$cari%' or e_kendaraan_jenis like '%$cari%'
							order by i_kendaraan_jenis",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacabbmkendaraan($num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_bbm order by i_kendaraan_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caribbmkendaraan($cari,$num,$offset)
    {
		$this->db->select(" * from tr_kendaraan_bbm 
							where upper(i_kendaraan_bbm) like '%$cari%' or e_kendaraan_bbm like '%$cari%'
							order by i_kendaraan_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cek($iperiode,$ikendaraan)
    {
		$this->db->select(" i_kendaraan from tr_kendaraan where i_periode='$iperiode' and i_kendaraan='$ikendaraan'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($iperiode,$ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$epengguna,$dpajak)
    {
    	$this->db->set(
    		array(
				'i_periode'			=> $iperiode,
				'i_kendaraan'		=> $ikendaraan,
				'i_area'	 		=> $iarea,
				'i_kendaraan_jenis'	=> $ikendaraanjenis,
				'i_kendaraan_bbm'	=> $ikendaraanbbm,
				'e_pengguna'		=> $epengguna,
				'd_pajak'			=> $dpajak
    		)
    	);
    	
    	$this->db->insert('tr_kendaraan');
    }
    function update($iperiode,$ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$epengguna,$dpajak)
    {
    	$this->db->set(
    		array(
				'i_area'	 		=> $iarea,
				'i_kendaraan_jenis'	=> $ikendaraanjenis,
				'i_kendaraan_bbm'	=> $ikendaraanbbm,
				'e_pengguna'		=> $epengguna,
				'd_pajak'			=> $dpajak
    		)
    	);
    	
    	$this->db->where("i_periode",$iperiode);
    	$this->db->where("i_kendaraan",$ikendaraan);
    	$this->db->update('tr_kendaraan');
    }
}
?>
