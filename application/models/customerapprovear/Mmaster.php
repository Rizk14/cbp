<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispb,$iarea)
    {
			$this->db->select(" * FROM tr_customer_tmp a
					  LEFT JOIN tr_city
					  ON (a.i_city = tr_city.i_city and a.i_area = tr_city.i_area)
					  LEFT JOIN tr_customer_group
					  ON (a.i_customer_group = tr_customer_group.i_customer_group)
					  LEFT JOIN tr_area
					  ON (a.i_area = tr_area.i_area)
					  LEFT JOIN tr_customer_status 
					  ON (a.i_customer_status = tr_customer_status.i_customer_status)
					  LEFT JOIN tr_customer_producttype
					  ON (a.i_customer_producttype = tr_customer_producttype.i_customer_producttype)
					  LEFT JOIN tr_customer_specialproduct
				  	ON (a.i_customer_specialproduct = tr_customer_specialproduct.i_customer_specialproduct)
					  LEFT JOIN tr_customer_grade
					  ON (a.i_customer_grade = tr_customer_grade.i_customer_grade)
					  LEFT JOIN tr_customer_service
					  ON (a.i_customer_service = tr_customer_service.i_customer_service)
					  LEFT JOIN tr_customer_salestype
					  ON (a.i_customer_salestype = tr_customer_salestype.i_customer_salestype)
					  LEFT JOIN tr_customer_class 
					  ON (a.i_customer_class=tr_customer_class.i_customer_class)
					  LEFT JOIN tr_shop_status 
					  ON (a.i_shop_status=tr_shop_status.i_shop_status)
					  LEFT JOIN tr_marriage 
					  ON (a.i_marriage=tr_marriage.i_marriage)
					  LEFT JOIN tr_jeniskelamin 
					  ON (a.i_jeniskelamin=tr_jeniskelamin.i_jeniskelamin)
					  LEFT JOIN tr_religion 
					  ON (a.i_religion=tr_religion.i_religion)
					  LEFT JOIN tr_traversed 
					  ON (a.i_traversed=tr_traversed.i_traversed)
					  LEFT JOIN tr_paymentmethod 
					  ON (a.i_paymentmethod=tr_paymentmethod.i_paymentmethod)
					  LEFT JOIN tr_call 
					  ON (a.i_call=tr_call.i_call)
					  LEFT JOIN tr_customer_plugroup
					  ON (a.i_customer_plugroup=tr_customer_plugroup.i_customer_plugroup)
					  LEFT JOIN tr_price_group
					  ON (a.i_price_group=tr_price_group.i_price_group)
					  where a.i_spb = '$ispb' and a.i_area='$iarea'
				", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->row();
			}
    }
    function insert(
		$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
		$iarea, $icustomerproducttype, $icustomerspecialproduct, 
		$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
		$icustomerstatus, $ecustomername, $ecustomeraddress,$ecustomercity, 
		$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
		$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
		$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
		$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
		$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
		$fcustomertax, $ecustomerretensi, $ncustomertoplength
		   )
    {
		if($fcustomerplusppn=='on')
			$fcustomerplusppn='TRUE';
		else
			$fcustomerplusppn='FALSE';
		if($fcustomerplusdiscount=='on')
			$fcustomerplusdiscount='TRUE';
		else
			$fcustomerplusdiscount='FALSE';
		if($fcustomerpkp=='on')
			$fcustomerpkp='TRUE';
		else
			$fcustomerpkp='FALSE';
		if($fcustomeraktif=='on')
			 $fcustomeraktif='TRUE';
		else
			$fcustomeraktif='FALSE';
		if($fcustomertax=='on')
			$fcustomertax='TRUE';
		else
			$fcustomertax='FALSE';
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
			'i_customer' 			=> $icustomer, 
			'i_customer_plugroup' 		=> $icustomerplugroup, 
			'i_city' 			=> $icity, 
			'i_customer_group' 		=> $icustomergroup, 
			'i_price_group' 		=> $ipricegroup,
			'i_area' 			=> $iarea, 
			'i_customer_status' 		=> $icustomerstatus, 
			'i_customer_producttype'	=> $icustomerproducttype, 
			'i_customer_specialproduct' 	=> $icustomerspecialproduct, 
			'i_customer_grade' 		=> $icustomergrade, 
			'i_customer_service' 		=> $icustomerservice, 
			'i_customer_salestype' 		=> $icustomersalestype, 
			'i_customer_class' 		=> $icustomerclass, 
			'i_customer_status' 		=> $icustomerstatus, 
			'e_customer_name' 		=> $ecustomername, 
			'e_customer_address' 		=> $ecustomeraddress,
			'e_customer_city' 		=> $ecustomercity, 
			'e_customer_postal' 		=> $ecustomerpostal, 
			'e_customer_phone' 		=> $ecustomerphone, 
			'e_customer_fax' 		=> $ecustomerfax, 
			'e_customer_mail' 		=> $ecustomermail, 
			'e_customer_sendaddress' 	=> $ecustomersendaddress, 
			'e_customer_receiptaddress' 	=> $ecustomerreceiptaddress, 
			'e_customer_remark' 		=> $ecustomerremark, 
			'e_customer_payment' 		=> $ecustomerpayment, 
			'e_customer_priority' 		=> $ecustomerpriority, 
			'e_customer_contact' 		=> $ecustomercontact, 
			'e_customer_contactgrade' 	=> $ecustomercontactgrade, 
			'e_customer_refference' 	=> $ecustomerrefference, 
			'e_customer_othersupplier' 	=> $ecustomerothersupplier, 
			'f_customer_plusppn' 		=> $fcustomerplusppn, 
			'f_customer_plusdiscount' 	=> $fcustomerplusdiscount, 
			'f_customer_pkp' 		=> $fcustomerpkp,
			'f_customer_aktif' 		=> $fcustomeraktif, 
			'f_customer_tax' 		=> $fcustomertax, 
			'e_customer_retensi' 		=> $ecustomerretensi, 
			'n_customer_toplength' 		=> $ncustomertoplength,
			'd_customer_entry'		=> $dentry
    		)
    	);
    	
    	$this->db->insert('tr_customer');
    }
    function update($ispb, $iarea, $iapprove, $eapprove, $fapprove, $fparkir, $fkuli, $fkontrabon)
    {
		  $query  = $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row    = $query->row();
		  $dapprove= $row->c;
      	$data = array(
		          'e_approve_ar' 	=> $eapprove, 
		          'i_approve_ar' 	=> $iapprove,
		          'd_approve_ar'	=> $dapprove,
              'f_parkir'      => $fparkir,
              'f_kuli'        => $fkuli,
              'f_kontrabon'   => $fkontrabon
  #            'f_approve'   => $fapprove
                      );
		  $this->db->where('i_spb', $ispb);
		  $this->db->where('i_area', $iarea);
		  $this->db->update('tr_customer_tmp', $data); 

      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
		  if($fapprove=='t'){
      	$data = array(
				    'e_approve2'		=> $eapprove,
				    'd_approve2'		=> $dentry,
				    'i_approve2'		=> $iapprove
      				 );
      }else{
      	$data = array(
				    'e_notapprove'		=> $eapprove,
				    'd_notapprove'		=> $dentry,
				    'i_notapprove'		=> $iapprove
      				 );
      }
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		  $this->db->update('tm_spb', $data); 
    }
    public function delete($icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer WHERE i_customer='$icustomer'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_customer_tmp a, tr_area c
												where a.i_area=c.i_area and a.f_approve='t' 
                        and (a.i_approve <>'' or not a.i_approve isnull)
                        and (a.i_approve_ar='' or a.i_approve_ar isnull)
                        and (a.i_spb like '%$cari%' or a.e_customer_name like '%$cari%')
												order by a.i_area, a.i_spb", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tr_customer where upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%' order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaplugroup($num,$offset)
    {
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup order by i_customer_plugroup",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariplugroup($cari,$num,$offset)
    {
		$this->db->select("i_customer_plugroup, e_customer_plugroupname from tr_customer_plugroup 
				   where upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer_plugroup", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacity($iarea,$num,$offset)
    {
		$this->db->select("i_city, e_city_name from tr_city where i_area='$iarea' order by i_city",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricity($cari,$num,$offset)
    {
		$this->db->select("i_city, e_city_name from tr_city 
				   where upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%' order by i_city", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomergroup($num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group order by i_customer_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomergroup($cari,$num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group 
				   where upper(e_customer_groupname) like '%$cari%' or upper(i_customer_group) like '%$cari%' order by i_customergroup", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapricegroup($num,$offset)
    {
		$this->db->select("i_price_group, e_price_groupname from tr_price_group order by i_price_group",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caripricegroup($cari,$num,$offset)
    {
		$this->db->select("i_price_group, e_price_groupname from tr_price_group 
				   where upper(e_price_groupname) like '%$cari%' or upper(i_price_group) like '%$cari%' order by i_price_group", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area 
				   where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerstatus($num,$offset)
    {
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status order by i_customer_status", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerstatus($cari,$num,$offset)
    {
		$this->db->select("i_customer_status, e_customer_statusname from tr_customer_status 
				   where upper(e_customer_statusname) like '%$cari%' or upper(i_customer_status) like '%$cari%' order by i_customer_status", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerproducttype($num,$offset)
    {
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype order by i_customer_producttype", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerproducttype($cari,$num,$offset)
    {
		$this->db->select("i_customer_producttype, e_customer_producttypename from tr_customer_producttype 
				   where upper(e_customer_producttypename) like '%$cari%' or upper(i_customer_producttype) like '%$cari%' order by i_customer_producttype", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerspecialproduct($icustomerproducttype,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query = $this->db->query("select * from tr_customer_specialproduct 
					   where i_customer_producttype='$icustomerproducttype' 
					   order by i_customer_specialproduct 
					   limit $num offset $offset");
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerspecialproduct($cari,$num,$offset)
    {
		$this->db->select("i_customer_specialproduct, e_customer_specialproductname from tr_customer_specialproduct 
				   where upper(e_customer_specialproductname) like '%$cari%' or upper(i_customer_specialproduct) like '%$cari%' order by i_customer_specialproduct", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomergrade($num,$offset)
    {
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade order by i_customer_grade", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomergrade($cari,$num,$offset)
    {
		$this->db->select("i_customer_grade, e_customer_gradename from tr_customer_grade 
				   where upper(e_customer_gradename) like '%$cari%' or upper(i_customer_grade) like '%$cari%' order by i_customer_grade", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerservice($num,$offset)
    {
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service order by i_customer_service", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerservice($cari,$num,$offset)
    {
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service 
				   where upper(e_customer_servicename) like '%$cari%' or upper(i_customer_service) like '%$cari%' order by i_customer_service", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomersalestype($num,$offset)
    {
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype order by i_customer_salestype", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomersalestype($cari,$num,$offset)
    {
		$this->db->select("i_customer_salestype, e_customer_salestypename from tr_customer_salestype 
				   where upper(e_customer_salestypename) like '%$cari%' or upper(i_customer_salestype) like '%$cari%' order by i_customer_salestype", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomerclass($num,$offset)
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class order by i_customer_class", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomerclass($cari,$num,$offset)
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class 
				   where upper(e_customer_classname) like '%$cari%' or upper(i_customer_class) like '%$cari%' order by i_customer_class", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaspb($ispb,$iarea)
    {
			$this->db->select(" tm_spb.e_remark1 AS emark1, * from tm_spb 
						 left join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						 left join tr_customer_tmp on (tm_spb.i_spb=tr_customer_tmp.i_spb and tm_spb.i_area=tr_customer_tmp.i_area)
						 inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
						 left join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
						 inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
						 where tm_spb.i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->row();
			}
    }
    function bacadetail($ispb,$iarea,$ipricegroup)
    {
			$this->db->select("  a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark as ket,a.n_item_no, b.e_product_motifname, c.v_product_retail as hrgnew 
		                      from tm_spb_item a, tr_product_motif b, tr_product_price c
						              where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif 
		                      and a.i_product=c.i_product and c.i_price_group='$ipricegroup'
						              order by a.n_item_no", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup)
    {
			return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
												        where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
}
?>
