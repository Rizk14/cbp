<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
/*
    function bacaperiode($iperiode,$iarea)
    {
		  $this->db->select(" a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman, sum(a.v_nota_gross) as nota
                          from tm_nota a, tr_customer b, tr_city c
                          where a.f_nota_cancel='f' and to_char(a.d_nota,'yyyymm')='$iperiode' and not a.i_nota isnull and a.i_area='$iarea'
                          and a.i_customer=b.i_customer and b.i_city=c.i_city and b.i_area=c.i_area
                          group by a.i_customer, b.e_customer_name, b.e_customer_address, c.e_city_name, a.i_salesman
                          order by c.e_city_name ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
*/
    function bacaperiode($dfrom,$dto,$iarea,$interval)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
      if($iarea=='NA'){
        $sql=" a.area, a.iarea, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 
                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes ";
          break;
        case '02' :
          $sql.=" sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan ";
          break;
        case '03' :
          $sql.=" sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb ";
          break;
        case '04' :
          $sql.=" sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar ";
          break;
        case '05' :
          $sql.=" sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr ";
          break;
        case '06' :
          $sql.=" sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay ";
          break;
        case '07' :
          $sql.=" sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 
                  sum(a.May) as spbmay, sum(a.Jun) as spbjun ";
          break;
        case '08' :
          $sql.=" sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 
                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul ";
          break;
        case '09' :
          $sql.=" sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug ";
          break;
        case '10' :
          $sql.=" sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, 
                  sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep ";
          break;
        case '11' :
          $sql.=" sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct ";
          break;
        case '12' :
          $sql.=" sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 
                  sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, 
                  sum(a.Oct) as spboct, sum(a.Nov) as spbnov ";
          break;
        }

        $sql.=" from ( select area, iarea, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT d.e_area_name, a.i_area, to_number(to_char(a.d_spb, ''mm''),''99'') as bln, sum(a.v_spb) AS jumlah
              FROM tm_spb a, tr_area d
              WHERE a.f_spb_cancel = false AND a.i_area=d.i_area 
              AND (a.d_spb >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_spb <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY d.e_area_name, a.i_area,
              to_char(a.d_spb, ''mm'')
              order by a.i_area','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (area text, iarea text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, 
                  Oct numeric, Nov numeric, Des numeric) ";
          break;
        case '02' :
          $sql.=" Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, 
                  Nov numeric, Des numeric, Jan numeric) ";
          break;
        case '03' :
          $sql.=" Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, 
                  Des numeric, Jan numeric, Feb numeric) ";
          break;
        case '04' :
          $sql.=" Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, 
                  Jan numeric, Feb numeric, Mar numeric) ";
          break;
        case '05' :
          $sql.=" May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, 
                  Feb numeric, Mar numeric, Apr numeric) ";
          break;
        case '06' :
          $sql.=" Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, 
                  Mar numeric, Apr numeric, May numeric) ";
          break;
        case '07' :
          $sql.=" Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, 
                  Apr numeric, May numeric, Jun numeric) ";
          break;
        case '08' :
          $sql.=" Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, 
                  May numeric, Jun numeric, Jul numeric) ";
          break;
        case '09' :
          $sql.=" Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, 
                  Jun numeric, Jul numeric, Aug numeric) ";
          break;
        case '10' :
          $sql.=" Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, 
                  Jul numeric, Aug numeric, Sep numeric) ";
          break;
        case '11' :
          $sql.=" Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, 
                  Aug numeric, Sep numeric, Oct numeric) ";
          break;
        case '12' :
          $sql.=" Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, 
                  Sep numeric, Oct numeric, Nov numeric) ";
          break;
        }
        $sql.=" ) as a
                group by a.area, a.iarea
                order by a.iarea";
      }else{
        $sql=" a.area, a.iarea, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 
                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes ";
          break;
        case '02' :
          $sql.=" sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan ";
          break;
        case '03' :
          $sql.=" sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb ";
          break;
        case '04' :
          $sql.=" sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar ";
          break;
        case '05' :
          $sql.=" sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr ";
          break;
        case '06' :
          $sql.=" sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay ";
          break;
        case '07' :
          $sql.=" sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 
                  sum(a.May) as spbmay, sum(a.Jun) as spbjun ";
          break;
        case '08' :
          $sql.=" sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 
                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul ";
          break;
        case '09' :
          $sql.=" sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug ";
          break;
        case '10' :
          $sql.=" sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, 
                  sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep ";
          break;
        case '11' :
          $sql.=" sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct ";
          break;
        case '12' :
          $sql.=" sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 

                  sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, 
                  sum(a.Oct) as spboct, sum(a.Nov) as spbnov ";
          break;
        }

        $sql.=" from ( select area, iarea, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT d.e_area_name, a.i_area, to_number(to_char(a.d_spb, ''mm''),''99'') as bln, sum(a.v_spb) AS jumlah
              FROM tm_spb a, tr_area d
              WHERE a.f_spb_cancel = false AND a.i_area=d.i_area and a.i_area=''$iarea''
              AND (a.d_spb >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_spb <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY d.e_area_name, a.i_area,
              to_char(a.d_spb, ''mm'')
              order by a.i_area','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as
              (area text, iarea text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, 
                  Oct numeric, Nov numeric, Des numeric) ";
          break;
        case '02' :
          $sql.=" Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, 
                  Nov numeric, Des numeric, Jan numeric) ";
          break;
        case '03' :
          $sql.=" Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, 
                  Des numeric, Jan numeric, Feb numeric) ";
          break;
        case '04' :
          $sql.=" Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, 
                  Jan numeric, Feb numeric, Mar numeric) ";
          break;
        case '05' :
          $sql.=" May numeric, Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, 
                  Feb numeric, Mar numeric, Apr numeric) ";
          break;
        case '06' :
          $sql.=" Jun numeric, Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, 
                  Mar numeric, Apr numeric, May numeric) ";
          break;
        case '07' :
          $sql.=" Jul numeric, Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, 
                  Apr numeric, May numeric, Jun numeric) ";
          break;
        case '08' :
          $sql.=" Aug numeric, Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, 
                  May numeric, Jun numeric, Jul numeric) ";
          break;
        case '09' :
          $sql.=" Sep numeric, Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, 
                  Jun numeric, Jul numeric, Aug numeric) ";
          break;
        case '10' :
          $sql.=" Oct numeric, Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, 
                  Jul numeric, Aug numeric, Sep numeric) ";
          break;
        case '11' :
          $sql.=" Nov numeric, Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, 
                  Aug numeric, Sep numeric, Oct numeric) ";
          break;
        case '12' :
          $sql.=" Des numeric, Jan numeric, Feb numeric, Mar numeric, Apr numeric, May numeric, Jun numeric, Jul numeric, Aug numeric, 
                  Sep numeric, Oct numeric, Nov numeric) ";
          break;
        }
        $sql.=" ) as a
                group by a.area, a.iarea
                order by a.iarea";
/*
        $sql=" a.nama, a.kode, a.alamat, a.kota, a.area, a.jenis, a.iarea, a.icity, ";
        switch ($bl){
        case '01' :
          $sql.=" sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 
                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes ";
          break;
        case '02' :
          $sql.=" sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan ";
          break;
        case '03' :
          $sql.=" sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb ";
          break;
        case '04' :
          $sql.=" sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 

                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar ";
          break;
        case '05' :
          $sql.=" sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 

                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 
                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr ";
          break;
        case '06' :
          $sql.=" sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, 
                  sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay ";
          break;
        case '07' :
          $sql.=" sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, 
                  sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 
                  sum(a.May) as spbmay, sum(a.Jun) as spbjun ";
          break;
        case '08' :
          $sql.=" sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, 
                  sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, 

                  sum(a.Jun) as spbjun, sum(a.Jul) as spbjul ";
          break;
        case '09' :
          $sql.=" sum(a.Sep) as spbsep, sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, 

                  sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, 
                  sum(a.Jul) as spbjul, sum(a.Aug) as spbaug ";
          break;
        case '10' :
          $sql.=" sum(a.Oct) as spboct, sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, 

                  sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, 
                  sum(a.Aug) as spbaug, sum(a.Sep) as spbsep ";
          break;
        case '11' :
          $sql.=" sum(a.Nov) as spbnov, sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, 
                  sum(a.Apr) as spbapr, sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, 
                  sum(a.Sep) as spbsep, sum(a.Oct) as spboct ";
          break;
        case '12' :
          $sql.=" sum(a.Des) as spbdes, sum(a.Jan) as spbjan, sum(a.Feb) as spbfeb, sum(a.Mar) as spbmar, sum(a.Apr) as spbapr, 
                  sum(a.May) as spbmay, sum(a.Jun) as spbjun, sum(a.Jul) as spbjul, sum(a.Aug) as spbaug, sum(a.Sep) as spbsep, 
                  sum(a.Oct) as spboct, sum(a.Nov) as spbnov ";
          break;
        }

        $sql.=" from ( select nama, kode, alamat, kota, area, jenis, iarea, icity, ";
        switch ($bl){
        case '01' :
          $sql.=" Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des ";
          break;
        case '02' :
          $sql.=" Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan ";
          break;
        case '03' :
          $sql.=" Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb ";
          break;
        case '04' :
          $sql.=" Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar ";
          break;
        case '05' :
          $sql.=" May, Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr ";
          break;
        case '06' :
          $sql.=" Jun, Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May ";
          break;
        case '07' :
          $sql.=" Jul, Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun ";
          break;
        case '08' :
          $sql.=" Aug, Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul ";
          break;
        case '09' :
          $sql.=" Sep, Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug ";
          break;
        case '10' :
          $sql.=" Oct, Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep ";
          break;
        case '11' :
          $sql.=" Nov, Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct ";
          break;
        case '12' :
          $sql.=" Des, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov ";
          break;
        }
        $sql.=" from crosstab
              ('SELECT b.e_customer_name, a.i_customer, b.e_customer_address, c.e_city_name, d.e_area_name, e.e_customer_classname, 
              a.i_area, b.i_city, to_number(to_char(a.d_spb, ''mm''),''99'') as bln, sum(a.v_spb) AS jumlah
              FROM tm_spb a, tr_customer b, tr_city c, tr_area d, tr_customer_class e
              WHERE a.f_spb_cancel = false AND b.i_customer = a.i_customer and a.f_spb_cancel=''f'' 
              AND b.i_city=c.i_city and b.i_area=c.i_area and b.i_area=d.i_area
              AND b.i_customer_class=e.i_customer_class AND a.i_area=''$iarea''
              AND (a.d_spb >= to_date(''$dfrom'',''dd-mm-yyyy'') AND a.d_spb <= to_date(''$dto'',''dd-mm-yyyy''))
              GROUP BY b.e_customer_name, a.i_customer, b.e_customer_address, c.e_city_name, d.e_area_name, 
              e.e_customer_classname, a.i_area, b.i_city,
              to_char(a.d_spb, ''mm'')
              order by a.i_customer, b.e_customer_name, to_char(a.d_spb, ''mm'')','select (SELECT EXTRACT(MONTH FROM date_trunc(''month'', 
              ''$tgl''::date)::date + s.a * ''1 month''::interval))
              from generate_series(0, 11) as s(a)')
              as

              (nama text, kode text, alamat text, kota text, area text, jenis text, iarea text, icity text,";
        switch ($bl){
        case '01' :
          $sql.=" Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, 

                  Oct integer, Nov integer, Des integer) ";
          break;
        case '02' :
          $sql.=" Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, 

                  Nov integer, Des integer, Jan integer) ";
          break;
        case '03' :
          $sql.=" Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, 
                  Des integer, Jan integer, Feb integer) ";
          break;
        case '04' :
          $sql.=" Apr integer, May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, 
                  Jan integer, Feb integer, Mar integer) ";
          break;
        case '05' :
          $sql.=" May integer, Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, 
                  Feb integer, Mar integer, Apr integer) ";
          break;
        case '06' :
          $sql.=" Jun integer, Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, 
                  Mar integer, Apr integer, May integer) ";
          break;
        case '07' :
          $sql.=" Jul integer, Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, 
                  Apr integer, May integer, Jun integer) ";
          break;
        case '08' :
          $sql.=" Aug integer, Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, 
                  May integer, Jun integer, Jul integer) ";
          break;
        case '09' :
          $sql.=" Sep integer, Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, 
                  Jun integer, Jul integer, Aug integer) ";
          break;
        case '10' :
          $sql.=" Oct integer, Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, 
                  Jul integer, Aug integer, Sep integer) ";
          break;
        case '11' :
          $sql.=" Nov integer, Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, 
                  Aug integer, Sep integer, Oct integer) ";
          break;
        case '12' :
          $sql.=" Des integer, Jan integer, Feb integer, Mar integer, Apr integer, May integer, Jun integer, Jul integer, Aug integer, 
                  Sep integer, Oct integer, Nov integer) ";
          break;
        }
        $sql.=" ) as a
                group by a.iarea, a.icity, a.nama, a.kode, a.alamat, a.kota, a.area, a.jenis
                order by a.iarea, a.icity, a.kode, a.nama, a.alamat, a.kota, a.area, a.jenis";
*/
      }
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
