<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibbm,$ittb,$iarea,$tahun) 
    {
    	$this->db->set(
    		array(
					'i_bbm'			=> null,
					'd_bbm'			=> null,
					'd_receive2'=> null
    		)
    	);
			$this->db->where('i_area',$iarea);
			$this->db->where('i_ttb',$ittb);
			$this->db->where('n_ttb_year',$tahun);
			$this->db->update('tm_ttbretur');

    	$this->db->set(
    		array(
					'i_product2'					=> null,
					'i_product2_grade'		=> null,
					'i_product2_motif'		=> null,
					'n_quantity_receive'	=> null
    		)
    	);
    	$this->db->where('i_ttb',$ittb);
    	$this->db->where('n_ttb_year',$tahun);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_ttbretur_item');

		$this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_bbm='$ibbm'");
#		$this->db->query("DELETE FROM tm_bbm_item WHERE i_bbm='$ibbm'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
					                from tr_customer b, tm_ttbretur c, tm_bbm a
					                left join tm_kn d on (a.i_bbm=d.i_refference) 
					                where 
					                c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
					                a,i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
					                or upper(a.i_bbm) like '%$cari%') 
					                order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
							          from tm_bbm a, tr_customer b, tm_ttbretur c
							          where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
							          and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							          order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($num,$offset,$cari)
    {
      $allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($allarea=='t' || $area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select("	a.*, c.i_customer, b.e_customer_name,
                            a.i_refference_document as i_ttb, a.d_refference_document as d_ttb
                            from tm_bbm a, tr_customer b, tm_ttbretur c
                            where c.i_customer=b.i_customer and a.i_area=c.i_area
                            and c.i_ttb=a.i_refference_document and a.f_bbm_cancel='f'
                            and a.i_bbm not in (select i_refference from tm_kn where a.i_bbm=tm_kn.i_refference and a.i_area=tm_kn.i_area)
                            and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							              order by a.i_area, a.i_bbm",false)->limit($num,$offset);
                            /*a.*, c.i_customer, b.e_customer_name, a.i_refference_document as i_ttb, a.d_refference_document as d_ttb, a.i_area
                            from tm_bbm a, tr_customer b, tm_ttbretur c
                            where a.f_kn='f' and c.i_customer=b.i_customer and a.f_bbm_cancel='f'
                            and a.i_refference_document=c.i_ttb and a.i_area=c.i_area and a.d_refference_document=c.d_ttb
                            and (c.i_customer like '%%' or upper(b.e_customer_name) like '%%' or upper(a.i_bbm) like '%%')
                            order by a.i_bbm*/
      }else{
		    $this->db->select("	a.*, c.i_customer, b.e_customer_name,
                            a.i_refference_document as i_ttb, a.d_refference_document as d_ttb
                            from tm_bbm a, tr_customer b, tm_ttbretur c
                            where c.i_customer=b.i_customer and a.i_area=c.i_area
                            and c.i_ttb=a.i_refference_document and a.f_bbm_cancel='f'
                            and a.i_bbm not in (select i_refference from tm_kn where a.i_bbm=tm_kn.i_refference and a.i_area=tm_kn.i_area)
                            and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							              and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
							              order by a.i_area, a.i_bbm ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
