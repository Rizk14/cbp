<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iadj,$iarea)
    {
		$this->db->select("a.*, b.e_area_name from tm_adj a, tr_area b 
		                   where a.i_area=b.i_area
		                   and i_adj ='$iadj' and a.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($iadj,$iarea)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_adj_item a, tr_product_motif b
						 where a.i_adj = '$iadj' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updateheader($iadj, $iarea, $dadj, $istockopname, $eremark, $istore, $istorelocation)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'd_adj'		              => $dadj,
			    'i_stockopname'         => $istockopname,
          'i_store'               => $istore, 
          'i_store_location'      => $istorelocation,			    
          'e_remark'              => $eremark,
          'd_update'              => $now
    		)
    	);
    	$this->db->where('i_adj',$iadj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_adj');
    }
    function insertheader($iadj, $iarea, $dadj, $istockopname, $eremark, $istore, $istorelocation)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'i_adj'		              => $iadj,
			    'i_area'	              => $iarea,
			    'd_adj'		              => $dadj,
			    'i_stockopname'         => $istockopname,
          'i_store'               => $istore, 
          'i_store_location'      => $istorelocation,			    
          'e_remark'              => $eremark,
          'd_entry'               => $now
    		)
    	);
    	$this->db->insert('tm_adj');
    }
    function insertdetail($iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i)
    {
    	$this->db->set(
    		array(
					'i_adj'	   	            => $iadj,
					'i_area'                => $iarea,
					'i_product'	 	          => $iproduct,
					'i_product_grade'	      => $iproductgrade,
					'i_product_motif'	      => $iproductmotif,
					'n_quantity'		        => $nquantity,
					'e_product_name'	      => $eproductname,
					'e_remark'		          => $eremark,
          'n_item_no'             => $i
    		)
    	);
    	$this->db->insert('tm_adj_item');
    }
    function updatedetail($iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i)
    {
    	$this->db->set(
    		array(
					'n_quantity'		        => $nquantity,
					'e_product_name'	      => $eproductname,
					'e_remark'		          => $eremark,
          'n_item_no'             => $i
    		)
    	);
    	$this->db->where('i_adj',$iadj);
    	$this->db->where('i_area',$iarea);
    	$this->db->where('i_product',$iproduct);
    	$this->db->where('i_product_grade',$iproductgrade);
    	$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_adj_item');
    }
    public function deletedetail($iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade)
    {
		  $this->db->query("DELETE FROM tm_adj_item WHERE i_adj='$iadj' and i_area='$iarea' and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'");
    }
	
    public function delete($ibm)
    {
		  $this->db->query("sUpdate tm_bm set f_bm_cancel='t' WHERE i_bm='$ibm'");
#		  $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($store,$loc,$num,$offset,$cari)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, d.i_product_grade as grade, 
						                    c.e_product_name as nama,c.v_product_mill as harga
						                    from tr_product_motif a,tr_product c, tm_ic d
						                    where a.i_product=c.i_product and c.i_product=d.i_product and d.i_store='$store'
						                    and i_store_location='$loc'
						                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function runningnumber($thbl,$iarea){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='ADJ'
                          and i_area='$iarea'
                          and substring(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noadj  =$terakhir+1;
        $this->db->query("update tm_dgu_no 
                          set n_modul_no=$noadj
                          where i_modul='ADJ'
                          and i_area='$iarea'
                          and substring(e_periode,1,4)='$th'", false);
			  settype($noadj,"string");
			  $a=strlen($noadj);
			  while($a<6){
			    $noadj="0".$noadj;
			    $a=strlen($noadj);
			  }
			  	$noadj  ="ADJ-".$thbl."-".$noadj;
			  return $noadj;
		  }else{
			  $noadj  ="000001";
		  	$noadj  ="ADJ-".$thbl."-".$noadj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('ADJ','$iarea','$asal',1)");
			  return $noadj;
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$istore,$istorelocation,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product and d.i_store='$istore'
                                and i_store_location='$istorelocation'
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomer($num,$offset,$cari)
    {
			$this->db->select(" * from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                          order by i_customer", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttransbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibm,$q_in,$q_out,$qbm,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibm', '$now', $qbm, 0, $q_ak+$qbm, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasibmelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qbm, n_saldo_akhir=n_saldo_akhir+$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibmelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,$qbm,0,0,0,$qbm,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateicbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbm)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qbm, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibm,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$ibm'
                                    order by d_transaction desc, i_trans desc",false);
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ibbk', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qbm, n_saldo_akhir=n_saldo_akhir-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}else{
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
	if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
	  $this->db->select("distinct on (a.i_store) a.i_store, c.i_store_location, a.i_area, b.e_store_name, c.e_store_locationname
                       from tr_area a, tr_store b, tr_store_location c
                       where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                       and a.i_store=b.i_store and b.i_store=c.i_store
          						 order by a.i_store ", FALSE)->limit($num,$offset);
	}else{
	  $this->db->select("distinct on (a.i_store) a.i_store, c.i_store_location, a.i_area, b.e_store_name, c.e_store_locationname
                       from tr_area a, tr_store b, tr_store_location c
                       where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						           and a.i_store=b.i_store and b.i_store=c.i_store and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						           or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
	}
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function bacaso($num,$offset,$iarea)
  {
	$this->db->select(" a.i_stockopname, a.i_store, a.i_store_location, c.e_store_name, b.e_store_locationname
                      from tm_stockopname a, tr_store_location b, tr_store c
                      where a.f_stockopname_cancel='f' and a.i_area = '$iarea' 
                      and a.i_store=c.i_store and a.i_store=b.i_store and a.i_store_location=b.i_store_location
                      order by a.i_stockopname desc", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
  }
}
?>
