<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dto,$num,$offset,$cari,$nt,$jt)
    {
      if($nt=='qqq'){
	      $this->db->select(" a.* from (
	                          select distinct(a.i_customer) i_customer, a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
	                          b.n_customer_toplength
                            from tm_nota a
                            inner join tr_customer b on (a.i_customer=b.i_customer)
                            inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                            where a.d_nota <= to_date('$dto','dd-mm-yyyy') and a.i_area='$iarea' and (upper(a.i_nota) like '%$cari%' 
                            or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                            or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                            union all
                            select distinct(a.i_customer) i_customer, a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
                            b.n_customer_toplength
                            from tm_nota a
                            inner join tr_customer b on (a.i_customer=b.i_customer)
                            inner join tr_customer_consigment d on (a.i_customer=d.i_customer and d.i_area_real='$iarea')
                            inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                            where a.d_nota <= to_date('$dto','dd-mm-yyyy') and (upper(a.i_nota) like '%$cari%' 
                            or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                            or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                            ) as a
                            order by a.i_customer ",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.* from (
	                          select distinct(a.i_customer) i_customer, a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
	                          b.n_customer_toplength
                            from tm_nota a
                            inner join tr_customer b on (a.i_customer=b.i_customer)
                            inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                            where a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') 
                            and a.i_area='$iarea' and (upper(a.i_nota) like '%$cari%' or upper(a.i_spb) like '%$cari%' 
                            or upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                            and a.f_nota_cancel='f' and a.v_sisa>0
                            union all
                            select distinct(a.i_customer) i_customer, a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
                            b.n_customer_toplength
                            from tm_nota a
                            inner join tr_customer b on (a.i_customer=b.i_customer)
                            inner join tr_customer_consigment d on (a.i_customer=d.i_customer and d.i_area_real='$iarea')
                            inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                            where a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and (upper(a.i_nota) like '%$cari%' 
                            or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                            or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                            ) as a
                            order by a.i_customer",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($icustomer,$isalesman,$dto,$nt,$jt)
    {
    if($nt=='qqq'){
		  $this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					                from tm_nota a, tr_customer b
					                where a.i_customer = '$icustomer' and a.d_nota<='$dto' and a.v_sisa>0
					                and a.i_customer=b.i_customer and a.i_salesman='$isalesman'
					                order by a.i_nota desc",false);
    }else{
		  $this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					                from tm_nota a, tr_customer b
					                where a.i_customer = '$icustomer' and a.d_jatuh_tempo<='$dto' and a.v_sisa>0
					                and a.i_customer=b.i_customer and a.i_salesman='$isalesman'
					                order by a.i_nota desc",false);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota,$area)
    {
		$this->db->select(" 	* from tm_nota_item
					inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_nota_item.i_product=tr_product_motif.i_product)
					where i_nota = '$inota' and i_area='$area' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacaopname($icustomer,$isalesman,$dto,$nt,$jt,$num,$offset)
  {
    if($nt=='qqq'){
	  $this->db->select(" i_customer, i_salesman, i_nota, d_nota, d_jatuh_tempo, v_nota_netto, v_sisa
                        from tm_nota
                        where i_customer='$icustomer' and i_salesman='$isalesman'
                        and v_sisa>0 and f_nota_cancel='f' and d_nota<='$dto'
                        order by i_nota", false)->limit($num,$offset);
    }else{
	  $this->db->select(" i_customer, i_salesman, i_nota, d_nota, d_jatuh_tempo, v_nota_netto, v_sisa
                        from tm_nota
                        where i_customer='$icustomer' and i_salesman='$isalesman'
                        and v_sisa>0 and f_nota_cancel='f' and d_jatuh_tempo<='$dto'
                        order by i_nota", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
