<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($isjp,$iarea)
    {
		$this->db->select(" a.i_nota, a.f_plus_ppn, a.d_sj, a.d_spb, a.i_sj, a.i_area, a.i_spb, a.d_sj_print, a.i_sj_old, a.v_nota_netto, a.i_customer,
                        a.i_dkb, c.e_customer_name, b.e_area_name from tm_nota a, tr_area b, tr_customer c
						   					where a.i_area=b.i_area and a.i_customer=c.i_customer
						   					and a.i_sj ='$isjp' ", false);#and substring(a.i_sj,9,2)='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isj, $iarea)
    {
		$this->db->select(" a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname, d.v_unit_price as harga,
                        a.v_unit_price, a.n_deliver, d.n_order, a.i_product_grade, d.n_order as n_qty, c.d_sj_print
                        from tm_nota_item a, tr_product_motif b, tm_nota c, tm_spb_item d
                        where a.i_sj = '$isj' and a.i_product=b.i_product 
                        and a.i_sj=c.i_sj and a.i_area=c.i_area
                        and c.i_spb=d.i_spb and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($ispb, $dspb, $iarea, $fop, $nprint)
    {
    	

    	$this->db->set(
    		array(
			'i_spb'	=> $ispb,
			'd_spb'	=> $dspb,
			'i_area'	=> $iarea,
			'f_op'		=> 'f',
			'n_print'	=> 0
    		)
    	);
    	
    	$this->db->insert('tm_spb');
    }
    function insertdetail($ispb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark)
    {


    	$this->db->set(
    		array(
					'i_spb'			=> $ispb,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_order'			=> $norder,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'e_remark'			=> $eremark
    		)
    	);
    	
    	$this->db->insert('tm_spb_item');
    }

    function updateheader($ispb, $dspb, $iarea)
    {
    	$this->db->set(
    		array(
			'd_spb'	=> $dspb,
			'i_area'	=> $iarea
    		)
    	);
    	$this->db->where('i_spb',$ispb);
    	$this->db->update('tm_spb');
    }

    public function deletedetail($iproduct, $iproductgrade, $ispb, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb'
										and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										and i_product_motif='$iproductmotif'");
		return TRUE;
    }
	
    public function delete($ispb) 
    {
		$this->db->query('DELETE FROM tm_spb WHERE i_spb=\''.$ispb.'\'');
		$this->db->query('DELETE FROM tm_spb_item WHERE i_spb=\''.$ispb.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spb order by i_spb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaspb($area1,$area2,$area3,$area4,$area5,$area,$num,$offset)
    {
		if($offset=='') $offset=0;
			if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
				$this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
									where a.i_customer=b.i_customer and a.i_area=c.i_area
									and a.i_nota isnull
									and not a.i_store isnull 
									and a.f_spb_cancel = 'f' 
									and a.i_area='$area'
									and (
										(	not a.i_approve1 isnull 
											and not a.i_approve2 isnull 
											and a.f_spb_siapnotagudang = 't'
											and a.f_spb_siapnotasales = 't')
                      and 
                        ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                           or
                          (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										)
									and a.i_sj isnull order by a.i_spb limit $num offset $offset",false);
# and a.f_spb_valid = 't'
			}else{
				$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
									where a.i_customer=b.i_customer and a.i_area=c.i_area and
									a.i_area='$area'
									and a.i_nota isnull
									and not a.i_approve1 isnull 
									and not a.i_approve2 isnull 
									and not a.i_store isnull 
									and a.f_spb_cancel = 'f' 
									and (
											a.f_spb_stockdaerah = 't'
										)
									and a.i_sj isnull order by a.i_spb limit $num offset $offset",false);
# and a.f_spb_valid = 't'
			}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carispb($cari,$area1,$area2,$area3,$area4,$area5,$area,$num,$offset)
    {
		if($offset=='') $offset=0;
  		$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
//									and a.i_area='$area1'
				$this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
									where a.i_customer=b.i_customer and a.i_area=c.i_area
									and a.i_nota isnull
									and not a.i_store isnull 
									and a.f_spb_cancel = 'f' 
									and a.i_area='$area'
									and  (
										    (	not a.i_approve1 isnull 
											    and not a.i_approve2 isnull 
											    and a.f_spb_siapnotagudang = 't'
											    and a.f_spb_siapnotasales = 't')
                          and 
                            ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                               or
                              (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										    )
									and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
									and a.i_sj isnull order by a.i_spb limit $num offset $offset",false);
# and a.f_spb_valid = 't'
			}else{
				$this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
									where a.i_customer=b.i_customer and a.i_area=c.i_area and
									a.i_area='$area'
									and a.i_nota isnull
									and not a.i_approve1 isnull 
									and not a.i_approve2 isnull 
									and not a.i_store isnull 
									and a.f_spb_cancel = 'f' 
									and (
											a.f_spb_stockdaerah = 't'
										)
									and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
									and a.i_sj isnull order by a.i_spb limit $num offset $offset",false);
# and a.f_spb_valid = 't'
			}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function product($spb,$iarea)
    {
	    $query=$this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif, b.n_deliver,
								                  a.e_product_motifname as namamotif, b.n_order as n_qty,
								                  c.e_product_name as nama,b.v_unit_price as harga, b.i_product_grade as grade
								                  from tr_product_motif a,tr_product c, tm_spb_item b
								                  where a.i_product=c.i_product 
								                  and b.i_product_motif=a.i_product_motif
								                  and c.i_product=b.i_product
								                  and b.i_spb='$spb' and i_area='$iarea' order by b.n_item_no",false);
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
    function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_spb,11,6)) as max from tm_spb 
				  			where substr(i_spb,6,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nospb  =$terakhir+1;
			settype($nospb,"string");
			$a=strlen($nospb);
			while($a<6){
			  $nospb="0".$nospb;
			  $a=strlen($nospb);
			}
			$nospb  ="spb-".$thbl."-".$nospb;
			return $nospb;
		}else{
			$nospb  ="000001";
			$nospb  ="spb-".$thbl."-".$nospb;
			return $nospb;
		}
    }
    function cari($cari,$num,$offset)

    {
		$this->db->select(" * from tm_spb where upper(i_spb) like '%$cari%' 
					order by i_spb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
/*
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								              a.e_product_motifname as namamotif, 
								              c.e_product_name as nama,c.v_product_mill as harga
								              from tr_product_motif a,tr_product c
								              where a.i_product=c.i_product
							                 	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								              limit $num offset $offset",false);
*/
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								              a.e_product_motifname as namamotif, 
								              c.e_product_name as nama,b.v_unit_price as harga
								              from tr_product_motif a,tr_product c
								              where a.i_product=c.i_product
							                 	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								              limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($iuser,$num,$offset)
    {
		$this->db->select("	* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("* from tr_area where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function runningnumbersj($iarea,$thbl,$kons)
    {
#      $area1	= $this->session->userdata('i_area');
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
      if($kons=='t' && $iarea!='PB'){
		    $this->db->select(" n_modul_no as max from tm_dgu_no 
                            where i_modul='SJ'
                            and e_periode='$asal' 
                            and i_area='BK' for update", false);
      }else{
		    $this->db->select(" n_modul_no as max from tm_dgu_no 
                            where i_modul='SJ'
                            and e_periode='$asal' 
                            and i_area='$iarea' for update", false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        if($kons=='t' && $iarea!='PB'){
          $this->db->query(" update tm_dgu_no 
                              set n_modul_no=$nosj
                              where i_modul='SJ'
                              and e_periode='$asal' 
                              and i_area='BK'", false);
        }else{
          $this->db->query(" update tm_dgu_no 
                              set n_modul_no=$nosj
                              where i_modul='SJ'
                              and e_periode='$asal' 
                              and i_area='$iarea'", false);
        }
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
        if($kons=='t' && $iarea!='PB'){
          $nosj  ="SJ-".$thbl."-BK".$nosj;
        }else{
  		  	$nosj  ="SJ-".$thbl."-".$iarea.$nosj;
        }
			  return $nosj;
		  }else{
			  $nosj  ="0001";
        if($kons=='t' && $iarea!='PB'){
          $nosj  ="SJ-".$thbl."-BK".$nosj;
          $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                             values ('SJ','BK','$asal',1)");
        }else{
          $nosj  ="SJ-".$thbl."-".$iarea.$nosj;
          $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                             values ('SJ','$iarea','$asal',1)");
        }
			  return $nosj;
		  }
    }
    function insertsjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
						   	$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1, 
							$vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto,$isjold,$fentpusat,$iareareff)
    {
      $query 		= $this->db->query("SELECT f_customer_plusppn, f_customer_plusdiscount from tr_customer where i_customer='$icustomer'");
		  $row   		= $query->row();
		  $plusppn	= $row->f_customer_plusppn;
		  $plusdiscount	= $row->f_customer_plusdiscount;
      $que      = $this->db->query("SELECT f_spb_consigment from tm_spb where i_spb='$ispb' and i_area='$iarea'");
      $row      = $que->row();
      $kons     = $row->f_spb_consigment;
      if ($kons=='t'){
        $dkb    ='SYSTEM';
        $ddkb   =$dsj;
      } else {
        $dkb    =NULL;
        $ddkb   =NULL;
      }
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjentry	= $row->c;
		  if($iarea=='PB'){
		    $this->db->set(
		      array(	  
            'i_sj'            		=> $isj,
	          'i_sj_old'		        => $isjold,
	          'i_spb'           		=> $ispb,
	          'd_spb'           		=> $dspb,
	          'd_sj'            		=> $dsj,
	          'd_sj_receive'        => $dsj,
	          'i_area'		          => $iarea,
	          'i_salesman'      		=> $isalesman,
	          'i_customer'      		=> $icustomer,
            'f_plus_ppn'          => $plusppn,
            'f_plus_discount'     => $plusdiscount,
	          'n_nota_discount1'  	=> $nspbdiscount1,
	          'n_nota_discount2'  	=> $nspbdiscount2,
	          'n_nota_discount3'   	=> $nspbdiscount3,
	          'v_nota_discount1'  	=> $vspbdiscount1,
	          'v_nota_discount2'  	=> $vspbdiscount2,
	          'v_nota_discount3'  	=> $vspbdiscount3,
	          'v_nota_discounttotal'=> $vspbdiscounttotal,
            'v_nota_discount'     => $vspbdiscounttotal,
	          'v_nota_gross'		    => $vspbgross,
	          'v_nota_netto'    		=> $vspbnetto,
	          'd_sj_entry'      		=> $dsjentry,
            'i_dkb'               => $dkb,
            'd_dkb'               => $ddkb,
	          'f_nota_cancel'		    => 'f'
		      )
      	);
      }else{
		    $this->db->set(
		      array(	  
            'i_sj'            		=> $isj,
	          'i_sj_old'		        => $isjold,
	          'i_spb'           		=> $ispb,
	          'd_spb'           		=> $dspb,
	          'd_sj'            		=> $dsj,
	          'i_area'		          => $iarea,
	          'i_salesman'      		=> $isalesman,
	          'i_customer'      		=> $icustomer,
            'f_plus_ppn'          => $plusppn,
            'f_plus_discount'     => $plusdiscount,
	          'n_nota_discount1'  	=> $nspbdiscount1,
	          'n_nota_discount2'  	=> $nspbdiscount2,
	          'n_nota_discount3'   	=> $nspbdiscount3,
	          'v_nota_discount1'  	=> $vspbdiscount1,
	          'v_nota_discount2'  	=> $vspbdiscount2,
	          'v_nota_discount3'  	=> $vspbdiscount3,
	          'v_nota_discounttotal'=> $vspbdiscounttotal,
            'v_nota_discount'     => $vspbdiscounttotal,
	          'v_nota_gross'		    => $vspbgross,
	          'v_nota_netto'    		=> $vspbnetto,
	          'd_sj_entry'      		=> $dsjentry,
            'i_dkb'               => $dkb,
            'd_dkb'               => $ddkb,
	          'f_nota_cancel'		    => 'f'
		      )
      	);
      }
    	$this->db->insert('tm_nota');
    }
    function insertsjheader2($ispb,$dspb,$isj,$dsj,$isjtype,$iareato,$iareafrom,$isalesman,$icustomer,
						   	$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1, 
							$vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto,$isjold,$daerah,$fentpusat,$iareareff,$ntop)
    {
      $query 		= $this->db->query("SELECT f_customer_plusppn, f_customer_plusdiscount from tr_customer where i_customer='$icustomer'");
		  $row   		= $query->row();
		  $plusppn	= $row->f_customer_plusppn;
		  $plusdiscount	= $row->f_customer_plusdiscount;
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjentry	= $row->c;
      if($iarea=='PB'){
     		$this->db->set(
		    array('i_sj'		=> $isj,
              'i_sj_old'		=> $isjold,
              'i_sj_type'		=> $isjtype,
              'i_spb'		=> $ispb,
              'd_spb'		=> $dspb,
              'd_sj'		=> $dsj,
              'd_sj_receive'  => $dsj,
              'i_area_from'		=> $iareafrom,
              'i_area_to'		=> $iareato,
              'i_salesman'		  => $isalesman,
              'i_refference_document'	=> $ispb,
              'i_customer'		  => $icustomer,
              'f_plus_ppn'      => $plusppn,
              'f_plus_discount' => $plusdiscount,
              'n_nota_toplength'=> $ntop,
              'n_sj_discount1'	=> $nspbdiscount1,
              'n_sj_discount2'	=> $nspbdiscount2,
        	 	  'n_sj_discount3' 	=> $nspbdiscount3,
              'v_sj_discount1'	=> $vspbdiscount1,
              'v_sj_discount2'	=> $vspbdiscount2,
              'v_sj_discount3'	=> $vspbdiscount3,
              'v_sj_discounttotal'	=> $vspbdiscounttotal,
              'v_sj_gross'		=> $vspbgross,
              'v_sj_netto'		=> $vspbnetto,
              'd_sj_entry'		=> $dsjentry,
              'f_sj_cancel'		=> 'f',
              'f_sj_daerah'       	=> $daerah,
              'f_entry_pusat'	=> $fentpusat,
              'i_area_referensi'	=> $iareareff
      		)
      	);
      }else{
    		$this->db->set(
			  array('i_sj'		=> $isj,
	            'i_sj_old'		=> $isjold,
	            'i_sj_type'		=> $isjtype,
	            'i_spb'		=> $ispb,
	            'd_spb'		=> $dspb,
	            'd_sj'		=> $dsj,
	            'i_area_from'		=> $iareafrom,
	            'i_area_to'		=> $iareato,
	            'i_salesman'		  => $isalesman,
	            'i_refference_document'	=> $ispb,
	            'i_customer'		  => $icustomer,
              'f_plus_ppn'      => $plusppn,
              'f_plus_discount' => $plusdiscount,
              'n_nota_toplength'=> $ntop,
	            'n_sj_discount1'	=> $nspbdiscount1,
	            'n_sj_discount2'	=> $nspbdiscount2,
        	 	  'n_sj_discount3' 	=> $nspbdiscount3,
	            'v_sj_discount1'	=> $vspbdiscount1,
	            'v_sj_discount2'	=> $vspbdiscount2,
	            'v_sj_discount3'	=> $vspbdiscount3,
	            'v_sj_discounttotal'	=> $vspbdiscounttotal,
	            'v_sj_gross'		=> $vspbgross,
	            'v_sj_netto'		=> $vspbnetto,
	            'd_sj_entry'		=> $dsjentry,
	            'f_sj_cancel'		=> 'f',
              'f_sj_daerah'       	=> $daerah,
	            'f_entry_pusat'	=> $fentpusat,
	            'i_area_referensi'	=> $iareareff
      		)
      	);
      }
      $this->db->insert('tm_nota');
    }
    function insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                  			    $vunitprice,$isj,$iarea,$i)
    {
    	$query=$this->db->query(" select i_product_category, i_product_class from tr_product where i_product='$iproduct'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$i_productcategory	=$qq->i_product_category;
					$i_productclass	=$qq->i_product_class;
				}
		$query2=$this->db->query(" select a.i_product_category, a.e_product_categoryname, b.i_product_class, b.e_product_classname from tr_product_category a, tr_product_class b 
									where a.i_product_category='$i_productcategory' and b.i_product_class='$i_productclass' ",false);
			if ($query2->num_rows() > 0){
				foreach($query2->result() as $oo){
					$i_product_category		=$oo->i_product_category;
					$e_product_categoryname	=$oo->e_product_categoryname;
					$i_product_class		=$oo->i_product_class;
					$e_product_classname	=$oo->e_product_classname;
				}
			}

    	$this->db->set(
    		array(
				'i_sj'			     	 => $isj,
				'i_area'	         	 => $iarea,
				'i_product'			 	 => $iproduct,
				'i_product_motif'	 	 => $iproductmotif,
				'i_product_grade'	 	 => $iproductgrade,
				'e_product_name'	 	 => $eproductname,
				'n_deliver'       	 	 => $ndeliver,
				'v_unit_price'		 	 => $vunitprice,
				'i_product_category' 	 => $i_product_category,
				'e_product_categoryname' => $e_product_categoryname,
				'i_product_class' 		 => $i_product_class,
				'e_product_classname' 	 => $e_product_classname,
				'n_item_no'       	 	 => $i
    		)
    	);
    	
    	$this->db->insert('tm_nota_item');
    	}
    }    
    function updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea,$vunitprice)
    {
	    $this->db->query(" update tm_spb_item set n_deliver = $ndeliver
			                   where i_spb='$ispb' and i_product='$iproduct' and i_product_grade='$iproductgrade'
			                   and i_product_motif='$iproductmotif' and i_area='$iarea' and v_unit_price=$vunitprice ",false);
    }
    function cekdaerah($ispb,$iarea)
		{
			$query=$this->db->query(" select f_spb_stockdaerah from tm_spb where i_spb='$ispb' and i_area='$iarea'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$stockdaerah=$qq->f_spb_stockdaerah;
				}
				return $stockdaerah;
			}
		}
    function cekkons($ispb,$iarea)
		{
		  $consigment='f';
			$query=$this->db->query(" select f_spb_consigment from tm_spb where i_spb='$ispb' and i_area='$iarea'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$consigment=$qq->f_spb_consigment;
				}
			}
			return $consigment;
		}
    function updatespb($ispb,$iarea,$isj,$dsj)
	  {
		$this->db->query(" update tm_spb set i_sj = '$isj',d_sj='$dsj' where i_spb='$ispb' and i_area='$iarea'",false);
	  }
    function updatedkb($vsjnetto,$isj,$iarea)
	  {
			$query=$this->db->query(" select a.v_jumlah, b.i_dkb from tm_dkb_item a, tm_dkb b 
                                where a.i_dkb=b.i_dkb and a.i_area=b.i_area and b.f_dkb_batal='f' 
                                and a.i_sj='$isj' and a.i_area='$iarea'",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$jml=$qq->v_jumlah;
          $dkb=$qq->i_dkb;
				}
		    $this->db->query(" update tm_dkb set v_dkb = (v_dkb-$jml)+$vsjnetto where i_dkb='$dkb' and i_area='$iarea'",false);
		    $this->db->query(" update tm_dkb_item set v_jumlah = '$vsjnetto' where i_dkb='$dkb' and i_sj='$isj' and i_area='$iarea'",false);
			}
	  }
    function deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea)
	{
		$this->db->query("delete from tm_nota_item where i_sj='$isj' and i_area='$iarea' 
                      and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'",false);
	}	
    function updatesjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
					                 	$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1, 
							              $vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto,$isjold)
    {
      $query 		= $this->db->query("SELECT f_customer_plusppn, f_customer_plusdiscount from tr_customer where i_customer='$icustomer'");
		  $row   		= $query->row();
		  $plusppn	= $row->f_customer_plusppn;
		  $plusdiscount	= $row->f_customer_plusdiscount;
      $query 		= $this->db->query("SELECT current_timestamp as c");
      $row   		= $query->row();
      $dsjupdate	= $row->c;
    	$this->db->set(
    		array(
#			'i_sj_old'      	=> $isjold,
			'i_spb'     			=> $ispb,
			'd_spb'			      => $dspb,
			'd_sj'	      		=> $dsj,
			'i_area'      		=> $iarea,
			'i_salesman'	  	=> $isalesman,
			'i_customer'	  	=> $icustomer,
      'f_plus_ppn'      => $plusppn,
      'f_plus_discount' => $plusdiscount,
			'n_nota_discount1'=> $nspbdiscount1,
			'n_nota_discount2'=> $nspbdiscount2,
 			'n_nota_discount3'=> $nspbdiscount3,
			'v_nota_discount1'=> $vspbdiscount1,
 			'v_nota_discount2'=> $vspbdiscount2,
 			'v_nota_discount3'=> $vspbdiscount3,
 			'v_nota_discounttotal'	=> $vspbdiscounttotal,
 			'v_nota_discount'	=> $vspbdiscounttotal,
			'v_nota_gross'		=> $vspbgross,
			'v_nota_netto'		=> $vspbnetto,
			'd_sj_update' 		=> $dsjupdate,
			'f_nota_cancel'		=> 'f'
    		)
    	);

    	$this->db->where('i_sj',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_nota');
    }
    function searchsjheader($isj,$iarea,$isjtype,$daerah)
    {
		return $this->db->query(" SELECT * FROM tm_nota WHERE i_sj='$isj' AND i_area_to='$iarea' AND i_sj_type='$isjtype' AND f_sj_daerah='$daerah' ");
	}
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                )
                              ",false);
*/    }
    function inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw
                                )
                              ",false);
*/
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_git_penjualan=n_git_penjualan+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_saldo_akhir=n_saldo_akhir-$qsj, n_git_penjualan=n_git_penjualan+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
*/
/*
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj,
                                n_git_penjualan=n_git_penjualan+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
*/
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close,n_git_penjualan)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,0,0,0,0,0,'f',$qsj) ",false);
/*
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close,n_git_penjualan)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,$qsj,0,0,$qaw-$qsj,0,'f',$qsj)
                              ",false);
*/
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '00', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
/*
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', $now, $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                )
                              ",false);
    }
*/
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,$qsj,0,0,0,$qsj,0,'f')
                              ",false);
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '00', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qsj, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
#and i_refference_document='$isj'
      if ($queri->num_rows() > 0){
#       return $query->result();
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 && $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$isj', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
/*
        $query=$this->db->query(" 
                                      DELETE FROM tm_ic_trans 
                                      where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                      and i_store='$istore' and i_store_location='$istorelocation'
                                      and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                                ",false);
*/
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_git_penjualan=n_git_penjualan-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_saldo_akhir=n_saldo_akhir+$qsj, n_git_penjualan=n_git_penjualan-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
*/
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }

    function ceksj($ispb,$iarea)
    {
		  $this->db->select("* from tm_nota where i_spb='$ispb' and i_area='$iarea'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return true;
		  }else{
			  return false;
		  }
    }
}
?>
