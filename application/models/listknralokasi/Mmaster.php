<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	public function delete($ialokasi, $iarea, $ikn, $ijenisbayar)
	{
		$this->db->select(" * from tm_alokasiknr WHERE i_alokasi='$ialokasi' and i_area='$iarea' and i_kn='$ikn'", false);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$dalokasi 	= $row->d_alokasi;
			if ($dalokasi != '') {
				$tmp = explode("-", $dalokasi);
				$th = $tmp[0];
				$bl = $tmp[1];
				$hr = $tmp[2];
			}
			$vjml = $row->v_jumlah;
			$vlbh = $row->v_lebih;
			$vbyr = $vjml - $vlbh;
			#####UnPosting
			$this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                          where i_refference='$ialokasi' and i_area='$iarea'");
			$this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                          where i_refference='$ialokasi' and i_area='$iarea'");
			$this->db->query("insert into th_general_ledger select * from tm_general_ledger
                          where i_refference='$ialokasi' and i_area='$iarea'");
			$quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                    from tm_general_ledger
                                    where i_refference='$ialokasi' and i_area='$iarea'");
			if ($quer->num_rows() > 0) {
				foreach ($quer->result() as $xx) {
					$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                              v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                              v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                              where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
				}
			}
			$this->db->query("delete from tm_jurnal_transharian where i_refference='$ialokasi' and i_area='$iarea'");
			$this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ialokasi' and i_area='$iarea'");
			$this->db->query("delete from tm_general_ledger where i_refference='$ialokasi' and i_area='$iarea'");
			#####
			if ($ijenisbayar == 10 && $vjml > 0 && $vjml <= 100) {
				$this->db->query("update tm_kn set v_sisa=v_sisa+0 where i_kn='$row->i_kn' and i_area='$iarea' and i_kn_type='01'");
			} else {
				$this->db->query("update tm_kn set v_sisa=v_sisa+$vjml where i_kn='$row->i_kn' and i_area='$iarea' and i_kn_type='01'");
			}
			$this->db->query("update tm_alokasiknr_lebih set f_alokasi_cancel='t' WHERE i_alokasi='$ialokasi' and i_area='$iarea' and i_kn='$ikn'");
			$this->db->query("update tr_customer_groupar set v_saldo=v_saldo+$vjml where i_customer='$row->i_customer'");
		}
		$this->db->select(" * from tm_alokasiknr_item WHERE i_alokasi='$ialokasi' and i_area='$iarea' and i_kn='$ikn'", false);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$vtmp = $row->v_jumlah;
			$this->db->query("update tm_nota set v_sisa=v_sisa+$vtmp where i_nota='$row->i_nota'");
			$this->db->query("update tm_alokasiknr_lebihitem set v_sisa=v_sisa+$vtmp where i_alokasi='$ialokasi' and i_area='$iarea' and i_kn='$ikn' and i_nota='$row->i_nota'");
		}
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate = $row->c;
		$this->db->query("update tm_alokasiknr set f_alokasi_cancel='t', d_update='$dupdate' 
		                    WHERE i_alokasi='$ialokasi' and i_area='$iarea' and i_kn='$ikn'");
	}

	function bacasemua($cari, $num, $offset)
	{
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							d.e_jenis_bayarname,e.d_dt
							from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							where 
							a.i_area=b.i_area
							and a.i_customer=c.i_customer
							and a.i_jenis_bayar=d.i_jenis_bayar
							and a.i_dt=e.i_dt and a.i_area=e.i_area
							and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%')
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" * from tm_pelunasan a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							inner join tr_jenis_bayar on(a.i_jenis_bayar=tr_jenis_bayar.i_jenis_bayar)
							inner join tm_dt on(a.i_dt=tm_dt.i_dt and a.i_area=tm_dt.i_area)
							where upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%'
							order by a.d_bukti,a.i_area,a.i_pelunasan", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		if ($iarea == 'NA') {
			$this->db->select("	a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,
								a.v_jumlah, sum(f.v_jumlah) AS bayar, a.v_lebih, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
								a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn
							    from tm_alokasiknr a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e, tm_alokasiknr_item f
							    where
							    a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
							    and a.i_area=e.i_area and a.i_kn=e.i_kn and e.i_kn_type='01'
								AND a.i_alokasi = f.i_alokasi AND a.i_kn = f.i_kn AND a.i_area = f.i_area
							    and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          		or upper(a.i_alokasi) like '%$cari%') and
							    a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
								GROUP BY a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,b.e_area_name, a.v_lebih, 
								c.e_customer_name, c.e_customer_address, c.e_customer_city, a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn,a.v_jumlah
							    ORDER BY a.i_area, a.i_alokasi, a.d_alokasi ", false)->limit($num, $offset);
		} else {
			$this->db->select("	a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,
								a.v_jumlah, sum(f.v_jumlah) AS bayar, a.v_lebih, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
								a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn
								from tm_alokasiknr a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e, tm_alokasiknr_item f
								where
								a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
								and a.i_area=e.i_area and a.i_kn=e.i_kn and e.i_kn_type='01'
								AND a.i_alokasi = f.i_alokasi AND a.i_kn = f.i_kn AND a.i_area = f.i_area
								and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
								or upper(a.i_alokasi) like '%$cari%') and
								a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_alokasi <= to_date('$dto','dd-mm-yyyy') AND a.i_area = '$iarea'
								GROUP BY a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,b.e_area_name, a.v_lebih, 
								c.e_customer_name, c.e_customer_address, c.e_customer_city, a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn,a.v_jumlah
								ORDER BY a.i_area, a.i_alokasi, a.d_alokasi", false)->limit($num, $offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacatotal($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		if ($iarea == 'NA') {
			$this->db->select("	a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,
								a.v_jumlah, sum(f.v_jumlah) AS bayar, a.v_lebih, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
								a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn
							    from tm_alokasiknr a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e, tm_alokasiknr_item f
							    where
							    a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
							    and a.i_area=e.i_area and a.i_kn=e.i_kn and e.i_kn_type='01'
								AND a.i_alokasi = f.i_alokasi AND a.i_kn = f.i_kn AND a.i_area = f.i_area
							    and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                          		or upper(a.i_alokasi) like '%$cari%') and
							    a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
								GROUP BY a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,b.e_area_name, a.v_lebih, 
								c.e_customer_name, c.e_customer_address, c.e_customer_city,a.i_jenis_bayar,  d.e_jenis_bayarname, e.d_kn,a.v_jumlah
							    ORDER BY a.i_area, a.i_alokasi, a.d_alokasi ", false);
		} else {
			$this->db->select("	a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,
								a.v_jumlah, sum(f.v_jumlah) AS bayar, a.v_lebih, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
								a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn
								from tm_alokasiknr a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e, tm_alokasiknr_item f
								where
								a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
								and a.i_area=e.i_area and a.i_kn=e.i_kn and e.i_kn_type='01'
								AND a.i_alokasi = f.i_alokasi AND a.i_kn = f.i_kn AND a.i_area = f.i_area
								and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
								or upper(a.i_alokasi) like '%$cari%') and
								a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_alokasi <= to_date('$dto','dd-mm-yyyy') AND a.i_area = '$iarea'
								GROUP BY a.i_kn, a.i_alokasi, a.d_alokasi, a.i_area, a.i_customer, a.f_alokasi_cancel, i_cek, d_cek,b.e_area_name, a.v_lebih, 
								c.e_customer_name, c.e_customer_address, c.e_customer_city, a.i_jenis_bayar, d.e_jenis_bayarname, e.d_kn,a.v_jumlah
								ORDER BY a.i_area, a.i_alokasi, a.d_alokasi", false);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		if ($iarea == 'NA') {
			$this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
							  	d.e_jenis_bayarname,e.d_dt, a.i_cek
							  	from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
							  	where 
							  	a.i_area=b.i_area and a.d_dt=e.d_dt
							  	and a.i_customer=c.i_customer
							  	and a.i_jenis_bayar=d.i_jenis_bayar
							  	and a.i_dt=e.i_dt and a.i_area=e.i_area
							  	and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
							  	and
							  	a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							  	a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							  	ORDER BY a.d_bukti,a.i_area,a.i_pelunasan ", false)->limit($num, $offset);
		} else {
			$this->db->select("	a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city,
								d.e_jenis_bayarname,e.d_dt, a.i_cek
								from tm_pelunasan a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_dt e
								where 
								a.i_area=b.i_area and a.d_dt=e.d_dt
								and a.i_customer=c.i_customer
								and a.i_jenis_bayar=d.i_jenis_bayar
								and a.i_dt=e.i_dt and a.i_area=e.i_area
								and (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
								and a.i_area='$iarea' and
								a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_bukti <= to_date('$dto','dd-mm-yyyy')
								ORDER BY a.d_bukti,a.i_area,a.i_pelunasan ", false)->limit($num, $offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
