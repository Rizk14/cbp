<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea, $irtunai)
    {
		$this->db->select("	a.d_rtunai, a.i_rtunai, d.e_area_name, a.v_jumlah, a.i_area, a.e_remark, a.i_cek, a.i_bank, b.e_bank_name
		      from tm_rtunai a
		      left join tr_bank b on(a.i_bank=b.i_bank)
					left join tr_area d on(a.i_area=d.i_area)
					where a.i_rtunai='$irtunai' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		  }
    }
    #function bacadetail($iarea, $irtunai)
    function bacadetail($iarea,$dfrom,$dto)
    {
		/*$this->db->select("	a.*,e.v_jumlah AS nnota,b.e_area_name, to_char(c.d_tunai,'dd-mm-yyyy') as d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name
                        from tm_rtunai_item a
                        left join tr_area b on(a.i_area_tunai=b.i_area)
                        left join tm_tunai_item e on(a.i_tunai=e.i_tunai and b.i_area=e.i_area),
                        tm_tunai c, tr_customer d
                        where a.i_rtunai='$irtunai' and a.i_area='$iarea' and c.i_customer=d.i_customer
                        and a.i_tunai=c.i_tunai and a.i_area_tunai=c.i_area
                        group by a.i_rtunai, a.i_area, a.i_tunai,a.i_area_tunai,e.v_jumlah, b.e_area_name,c.d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name
                        order by a.n_item_no ",false);*/
    if($iarea=='NA'){
    $this->db->select(" a.*,e.v_jumlah AS nnota,b.e_area_name, to_char(c.d_tunai,'dd-mm-yyyy') as d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name,
                        c.d_entry
                        from tm_rtunai_item a
                        left join tr_area b on(a.i_area_tunai=b.i_area)
                        left join tm_tunai_item e on(a.i_tunai=e.i_tunai and b.i_area=e.i_area) 
                        left join tm_rtunai f on(a.i_rtunai=f.i_rtunai and a.i_area=f.i_area),
                        tm_tunai c, tr_customer d
                        where c.i_customer=d.i_customer
                        and a.i_tunai=c.i_tunai and a.i_area_tunai=c.i_area  
                        and(f.d_rtunai >= to_date('$dfrom','dd-mm-yyyy')
                        and f.d_rtunai <= to_date('$dto','dd-mm-yyyy'))
                        group by a.i_rtunai, a.i_area, a.i_tunai,a.i_area_tunai,e.v_jumlah, b.e_area_name,c.d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name,c.d_entry
                        order by a.i_area, a.i_rtunai ",false);
    }else{
    $this->db->select(" a.*,e.v_jumlah AS nnota,b.e_area_name, to_char(c.d_tunai,'dd-mm-yyyy') as d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name,
                        c.d_entry
                        from tm_rtunai_item a 
                        left join tr_area b on(a.i_area_tunai=b.i_area)
                        left join tm_tunai_item e on(a.i_tunai=e.i_tunai and b.i_area=e.i_area)
                        left join tm_rtunai f on(a.i_rtunai=f.i_rtunai and a.i_area=f.i_area),  
                        tm_tunai c, tr_customer d 
                        where a.i_area='$iarea' and c.i_customer=d.i_customer
                        and a.i_tunai=c.i_tunai and a.i_area_tunai=c.i_area
                        and(f.d_rtunai >= to_date('$dfrom','dd-mm-yyyy')
                        and f.d_rtunai <= to_date('$dto','dd-mm-yyyy'))
                        group by a.i_rtunai, a.i_area, a.i_tunai,a.i_area_tunai,e.v_jumlah, b.e_area_name,c.d_tunai,e.i_nota, c.i_customer, c.e_remark, d.e_customer_name,c.d_entry
                        order by a.i_rtunai,a.n_item_no ",false);
    }

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacabukti($iarea,$dfrom,$dto,$num,$offset)
    {       
      $this->db->select("a.*, d.*
                  from tm_rtunai a
                  left join tr_area d on(a.i_area=d.i_area)
                  where a.f_rtunai_cancel='f'
                  and(a.d_rtunai >= to_date('$dfrom','dd-mm-yyyy')
                  and a.d_rtunai <= to_date('$dto','dd-mm-yyyy'))
                  and a.i_area='$iarea'
                  ORDER BY i_rtunai ",false)->limit($num,$offset);
      
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();

      }
    }
    function cek($iarea,$ikum,$tahun)
    {
		$this->db->select(" i_kum from tm_kum where i_area='$iarea' and i_kum='$ikum' and n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($irtunai,$drtunai,$iarea,$eremark,$vjumlah,$ibank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			      => $iarea,
				'i_rtunai'				  => $irtunai,
				'd_rtunai'				  => $drtunai,
				'd_entry'			      => $dentry,
				'e_remark'			    => $eremark,
				'v_jumlah'			    => $vjumlah,
				'i_bank'            => $ibank
    		)
    	);
    	$this->db->insert('tm_rtunai');
    }
    function insertdetail($irtunai,$iarea,$itunai,$iareatunai,$vjumlah,$i)
    {
		 	$this->db->set(
    		array(
				'i_area'			      => $iarea,
				'i_rtunai'				  => $irtunai,
				'i_tunai' 				  => $itunai,
				'i_area_tunai'      => $iareatunai,
				'v_jumlah'			    => $vjumlah,
				'n_item_no'         => $i
    		)
    	);
    	$this->db->insert('tm_rtunai_item');
    }

    function updatedetail($irtunai,$iarea,$xiarea,$itunai,$iareatunai,$vjumlah,$i)
    {
      $this->db->select("	v_jumlah from tm_rtunai_item 
                					where i_rtunai='$irtunai' and i_area='$xiarea' and i_tunai='$itunai' and i_area_tunai='$iareatunai'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
		    $this->db->set(
      		array(
				  'i_area'			      => $iarea,
				  'i_rtunai'				  => $irtunai,
				  'i_tunai' 				  => $itunai,
				  'i_area_tunai'      => $iareatunai,
				  'v_jumlah'			    => $vjumlah,
				  'n_item_no'         => $i
      		)
      	);
      	$this->db->where('i_rtunai',$irtunai);
      	$this->db->where('i_area',$xiarea);
      	$this->db->where('i_tunai',$itunai);
      	$this->db->where('i_area_tunai',$iareatunai);
      	$this->db->update('tm_rtunai_item');
      }else{
        $this->db->set(
      		array(
				  'i_area'			      => $iarea,
				  'i_rtunai'				  => $irtunai,
				  'i_tunai' 				  => $itunai,
				  'i_area_tunai'      => $iareatunai,
				  'v_jumlah'			    => $vjumlah,
				  'n_item_no'         => $i
      		)
      	);
      	$this->db->insert('tm_rtunai_item');
      }
    }
    function deletedetail($irtunai,$iarea,$itunai,$iareatunai)
    {
      $nilai=0;
      $this->db->select("	v_jumlah from tm_rtunai_item 
                					where i_rtunai='$irtunai' and i_area='$iarea' and i_tunai='$itunai' and i_area_tunai='$iareatunai'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $xx){
          $nilai=$xx->v_jumlah;
			  }
      }
    	$this->db->query("update tm_rtunai set v_jumlah=v_jumlah-$nilai where i_rtunai='$irtunai' and i_area='$iarea'");
    	
    	$this->db->query("update tm_tunai set v_sisa=v_sisa+$nilai, i_area_rtunai=null, i_rtunai=null
    	                  where i_tunai='$itunai' and i_area='$iareatunai'");
    	
#      $this->db->set(
#    		array(
#				'i_area_rtunai' 		=> null,
#				'i_rtunai'  			  => null
#    		)
#    	);
#    	$this->db->where('i_tunai',$itunai);
#    	$this->db->where('i_area',$iareatunai);
#    	$this->db->update('tm_tunai');
    	$this->db->query("delete from tm_rtunai_item 
    	                  where i_rtunai='$irtunai' and i_area='$iarea' and i_tunai='$itunai' and i_area_tunai='$iareatunai'");
    }
    function updatetunai($irtunai,$iarea,$itunai,$iareatunai,$vjumlah)
    {
      $this->db->query("update tm_tunai set i_area_rtunai='$iarea', i_rtunai='$irtunai', v_sisa=v_sisa-$vjumlah 
                        where i_tunai='$itunai' and i_area='$iareatunai'");
/*    
      $this->db->set(
    		array(
				'i_area_rtunai' 		=> $iarea,
				'i_rtunai'  			  => $irtunai
				'v_sisa'            => v_sisa-$vjumlah
    		)
    	);
    	$this->db->where('i_tunai',$itunai);
    	$this->db->where('i_area',$iareatunai);
    	$this->db->update('tm_tunai');
*/
    }
    function updatetunaix($irtunai,$iarea,$itunai,$iareatunai,$vjumlah)
    {
      $this->db->query("update tm_tunai set v_sisa=v_sisa+$vjumlah where i_tunai='$itunai' and i_area='$iareatunai'");
    }
    function update($irtunai,$drtunai,$iarea,$xiarea,$eremark,$vjumlah,$ibank)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			      => $iarea,
				'i_rtunai'				  => $irtunai,
				'd_rtunai'				  => $drtunai,
				'd_update'		      => $dentry,
				'e_remark'			    => $eremark,
				'v_jumlah'			    => $vjumlah,
				'i_bank'            => $ibank
    		)
    	);
    	$this->db->where('i_rtunai',$irtunai);
    	$this->db->where('i_area',$xiarea);
    	$this->db->update('tm_rtunai');
    }
	function bacaarea($num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
  		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
	  $area	= $this->session->userdata('i_area');
		$iuser   = $this->session->userdata('user_id');
		if($area=='00'){
		  $this->db->select(" * from tr_area 
							  where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
    }else{
      $this->db->select(" * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
                          and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}

	function bacabank($num,$offset){
 		$this->db->select(" * from tr_bank order by i_bank ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}

  function runningnumber($iarea,$thbl){
    $th   = substr($thbl,0,4);
    $asal=$thbl;
    $thbl=substr($thbl,2,2).substr($thbl,4,2);
    $this->db->select(" n_modul_no as max from tm_dgu_no
                      where i_modul='RTN'
                      and substr(e_periode,1,4)='$th'
                      and i_area='$iarea' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
       foreach($query->result() as $row){
         $terakhir=$row->max;
       }
       $nortn  =$terakhir+1;
    $this->db->query(" update tm_dgu_no
                        set n_modul_no=$nortn
                        where i_modul='RTN'
                        and substr(e_periode,1,4)='$th'
                        and i_area='$iarea'", false);
       settype($nortn,"string");
       $a=strlen($nortn);
       while($a<5){
         $nortn="0".$nortn;
         $a=strlen($nortn);
       }
       $nortn  ="RTN-".$thbl."-".$nortn;
       return $nortn;
    }else{
       $nortn  ="00001";
       $nortn  ="RTN-".$thbl."-".$nortn;
       $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                          values ('RTN','$iarea','$asal',1)");
       return $nortn;
    }
  }
	function bacatunai($drtunaix,$cari,$iarea,$num,$offset,$areax1,$areax2,$areax3,$areax4,$areax5)
  {
    if($areax1=='00'){
      $this->db->select(" a.*, b.i_customer, b.e_customer_name, c.e_area_name from tm_tunai a, tr_customer b, tr_area c 
	                        where (a.i_rtunai isnull or a.v_sisa>0) and a.d_tunai<='$drtunaix'
                          and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                          and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                          or upper(a.i_customer) like '%$cari%')
                          order by a.i_tunai ", false)->limit($num,$offset);
    }else{
      $this->db->select(" a.*, b.i_customer, b.e_customer_name, c.e_area_name from tm_tunai a, tr_customer b, tr_area c
	                        where (a.i_rtunai isnull or a.v_sisa>0) and a.d_tunai<='$drtunaix'
                          and (a.i_area='$areax1' or a.i_area='$areax2' or a.i_area='$areax3' or a.i_area='$areax4' 
                          or a.i_area='$areax5')
                          and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                          and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                          or upper(a.i_customer) like '%$cari%')
                          order by a.i_tunai ", false)->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->result();
    }	
  }
}
?>
