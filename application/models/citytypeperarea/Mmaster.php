<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icitytypeperarea,$iarea,$icitytype)
    {
		$this->db->select("a.*, b.e_area_name, c.e_city_typename")->from('tr_city_typeperarea a, tr_area b, tr_city_type c')->where("a.i_city_typeperarea = '$icitytypeperarea' and a.i_area = '$iarea' and a.i_city_type = '$icitytype' and a.i_area=b.i_area and a.i_city_type=c.i_city_type");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($icitytypeperarea,$ecitytypeperareaname,$iarea,$icitytype)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_city_typeperarea (i_city_typeperarea, e_city_typeperareaname, i_area, i_city_type, d_city_typeperareaentry) values ('$icitytypeperarea', '$ecitytypeperareaname','$iarea','$icitytype', '$dentry')");
		#redirect('citytypeperarea/cform/');
    }

    function update($icitytypeperarea,$ecitytypeperareaname,$iarea,$icitytype)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_city_typeperarea set e_city_typeperareaname = '$ecitytypeperareaname', d_city_typeperareaupdate = '$dupdate' where i_city_typeperarea = '$icitytypeperarea' and i_area='$iarea' and i_city_type='$icitytype'");
		#redirect('citytypeperarea/cform/');
    }
	
    public function delete($icitytypeperarea,$iarea,$icitytype) 
    {
		$this->db->query('DELETE FROM tr_city_typeperarea WHERE i_city_typeperarea=\''.$icitytypeperarea.'\' and i_area=\''.$iarea.'\' and i_city_type=\''.$icitytype.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, 
							tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type and (upper 
							(a.i_city_typeperarea) ilike '%$cari%' or upper (a.e_city_typeperareaname) ilike '%$cari%' 
							or upper(b.e_area_name) ilike '%$cari%' or upper (c.e_city_typename) ilike '%$cari%') 
							order by a.i_city_typeperarea ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacitytype($num,$offset)
    {
		$this->db->select("i_city_type, e_city_typename from tr_city_type order by i_city_type", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, 
							tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type and (upper 
							(a.i_city_typeperarea) ilike '%$cari%' or upper (a.e_city_typeperareaname) ilike '%$cari%' 
							or upper(b.e_area_name) ilike '%$cari%' or upper (c.e_city_typename) ilike '%$cari%') 
							order by a.i_city_typeperarea ", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricitytype($cari,$num,$offset)
    {
		$this->db->select("i_city_type, e_city_typename from tr_city_type
				   where i_city_type ilike '%$cari%' or e_city_typename ilike '%$cari%' order by i_city_type", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area
				   where i_area like '%$cari%' or e_area_name like '%$cari%' order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
