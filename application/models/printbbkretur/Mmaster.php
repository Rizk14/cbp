<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
	    $this->db->select("	a.*, b.e_supplier_name
						              from tm_bbkretur a, tr_supplier b
				                  where a.i_supplier=b.i_supplier
				                  and (b.i_supplier like '%$cari%' or upper(b.e_supplier_name) like '%$cari%' 
				                  or upper(a.i_bbkretur) like '%$cari%')
				                  and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') 
				                  AND a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
						              order by a.i_bbkretur ",false)->limit($num,$offset);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
    function baca($ibbkretur)
    {
		$this->db->select(" a.*, b.e_supplier_name
												from tm_bbkretur a, tr_supplier b
							          where a.i_supplier=b.i_supplier and a.i_bbkretur='$ibbkretur'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ibbkretur)
    {
	    $this->db->select(" a.i_bbkretur, c.d_bbkretur, a.i_product as product, a.i_product_grade, a.i_product_motif, a.n_quantity,
	                        a.v_unit_price, b.e_product_motifname, a.e_product_name, a.i_product
	                        from tm_bbkretur_item a, tr_product_motif b, tm_bbkretur c
						              where c.i_bbkretur = '$ibbkretur' and c.i_bbkretur=a.i_bbkretur
						              and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif 
						              order by a.n_item_no",false);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
}
?>
