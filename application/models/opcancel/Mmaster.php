<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		// $this->db->select(" 	a.*, b.e_supplier_name from tm_op a, tr_supplier b
		// 			where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
		// 			and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
		// 			or upper(a.i_op) like '%$cari%')
		// 			order by a.i_op desc",false)->limit($num,$offset);

		$this->db->select(" * from (
        	select a.*, b.e_supplier_name from tm_op a, tr_supplier b
			where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
			and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
			or upper(a.i_op) like '%$cari%')
			order by a.i_op desc
        ) as a
        where a.i_op not in (select i_op from tm_do where f_do_cancel = 'f') order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from (
					        	select a.*, b.e_supplier_name from tm_op a, tr_supplier b
								where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
								and (upper(a.i_reff) ilike '%$cari%' or upper(b.e_supplier_name) ilike '%$cari%' or upper(a.i_op) ilike '%$cari%')
								order by a.i_op desc
					        ) as a
					        where a.i_op not in (select i_op from tm_do where f_do_cancel = 'f') order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updateop($iop)
    {
		$data = array(
		           'f_op_cancel' => 't'
		        );
		$this->db->where('i_op', $iop);
		$this->db->update('tm_op', $data); 
    }
}
?>
