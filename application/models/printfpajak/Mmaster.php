<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset,$dfrom,$dto)
    {
    if($iarea1=='00'){
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
	                      where a.i_customer=b.i_customer 
	                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
	                      or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
                        and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
					              order by a.i_faktur_komersial desc",false)->limit($num,$offset);
#and (a.n_print=0 or a.n_print isnull)
    }else{
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
	                      where a.i_customer=b.i_customer 
	                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
	                      or upper(a.i_faktur_komersial) like '%$cari%')
	                      and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' 
                        or a.i_area='$iarea5') and a.n_faktur_komersialprint>0
                        and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
              					order by a.i_faktur_komersial desc",false)->limit($num,$offset);
    }
#and (a.n_print=0 or a.n_print isnull)
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($inota,$iarea)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.f_customer_pkp,b.e_customer_city, c.*, d.*, e.*
                        , p.*, o.e_customer_ownername
					              from tm_nota a, tr_customer b left join tr_customer_pkp p on(b.i_customer=p.i_customer)
                        left join tr_customer_owner o on(b.i_customer=o.i_customer), 
                        tr_salesman c, tr_customer_class d, tr_price_group e
					              where a.i_nota = '$inota' and a.i_area='$iarea'
					              and a.i_customer=b.i_customer 
					              and a.i_salesman=c.i_salesman
					              and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
					              and b.i_customer_class=d.i_customer_class",false);
#and (a.n_print=0 or a.n_print isnull) and a.i_customer=o.i_customer
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota,$iarea)
    {
		$this->db->select(" * from tm_nota_item
					inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_nota_item.i_product=tr_product_motif.i_product)
					where i_nota = '$inota' and i_area='$iarea' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' or a.i_area='$iarea5')
					order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($iarea,$inota,$iseri,$dpajak)
    {
      $query  = $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
      $row    = $query->row();
      $dentry = $row->c;
		  $this->db->query("	update tm_nota set n_pajak_print=n_pajak_print+1, i_seri_pajak='$iseri', d_pajak='$dpajak', d_pajak_print='$dentry'
            							where i_nota = '$inota' and i_area = '$iarea' ",false);
    }
    function bacafaktur($num,$offset,$dfrom,$dto,$area,$cari)
    {
			$this->db->select(" a.* from tm_nota a
                          where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%' 
                          or upper(a.i_nota) like '%$cari%') and a.n_faktur_komersialprint>0
                          and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                          and a.i_area='$area' and not a.i_faktur_komersial isnull order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
