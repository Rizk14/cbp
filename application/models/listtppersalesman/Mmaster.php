<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function simpan($iperiode)
    {
######  per area
      $this->db->select("	i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan 
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          sum(v_gross) as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, sum(v_gross) as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          sum(v_gross) as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00' and f_insentif='t'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, sum(v_gross) as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00' and f_insentif='f'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, sum(v_gross) as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00' and f_insentif='t'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, sum(v_gross) as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          sum(v_gross) as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00' and f_insentif='f'
                          group by i_area
                          ) as a
                          group by i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
		      $quer 	= $this->db->query("SELECT current_timestamp as c");
		      $raw   	= $quer->row();
		      $dproses= $raw->c;

          $this->db->query("update tm_target set v_real_insentif=$row->v_nota_grossinsentif,
                            v_real_regularinsentif=$row->v_nota_regulerinsentif,
                            v_real_babyinsentif=$row->v_nota_babyinsentif,
                            v_real_noninsentif=$row->v_nota_grossnoninsentif,
                            v_real_regularnoninsentif=$row->v_nota_regulernoninsentif,
                            v_real_babynoninsentif=$row->v_nota_babynoninsentif,
                            v_retur_insentif=$row->v_retur_insentif,
                            v_retur_noninsentif=$row->v_retur_noninsentif,
                            v_spb_gross=$row->v_spb_gross,
                            v_spb_netto=$row->v_spb_netto,
                            v_nota_gross=$row->v_nota_gross,
                            v_nota_netto=$row->v_nota_netto,
                            v_nota_grossinsentif=$row->v_nota_grossinsentif,
                            v_nota_nettoinsentif=$row->v_nota_nettoinsentif,
                            v_nota_grossnoninsentif=$row->v_nota_grossnoninsentif,
                            v_nota_nettononinsentif=$row->v_nota_nettononinsentif,
                            d_process='$dproses'
                            where i_periode='$iperiode' and i_area='$row->i_area'");
        }
		  }
##### per sales
      $this->db->select("	i_area, i_salesman, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, i_salesman, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 
                          0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, 
                          sum(v_spb)-sum(v_spbdiscount) as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan 
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          sum(v_gross) as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, sum(v_gross) as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          sum(v_gross) as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00' and f_insentif='t'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, sum(v_gross) as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group<>'00' and f_insentif='f'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, sum(v_gross) as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00' and f_insentif='t'
                          group by i_area, i_salesman
                          union all
                          select i_area, i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, sum(v_gross) as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          sum(v_gross) as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where i_product_group='00' and f_insentif='f'
                          group by i_area, i_salesman
                          ) as a
                          group by i_area, i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $quer 	= $this->db->query("SELECT current_timestamp as c");
		      $raw   	= $quer->row();
		      $dproses= $raw->c;
          $this->db->query("update tm_target_itemsls set v_real_insentif=$row->v_nota_grossinsentif,
                            v_real_regularinsentif=$row->v_nota_regulerinsentif,
                            v_real_babyinsentif=$row->v_nota_babyinsentif,
                            v_real_noninsentif=$row->v_nota_grossnoninsentif,
                            v_real_regularnoninsentif=$row->v_nota_regulernoninsentif,
                            v_real_babynoninsentif=$row->v_nota_babynoninsentif,
                            v_retur_insentif=$row->v_retur_insentif,
                            v_retur_noninsentif=$row->v_retur_noninsentif,
                            v_spb_gross=$row->v_spb_gross,
                            v_spb_netto=$row->v_spb_netto,
                            v_nota_gross=$row->v_nota_gross,
                            v_nota_netto=$row->v_nota_netto,
                            v_nota_grossinsentif=$row->v_nota_grossinsentif,
                            v_nota_nettoinsentif=$row->v_nota_nettoinsentif,
                            v_nota_grossnoninsentif=$row->v_nota_grossnoninsentif,
                            v_nota_nettononinsentif=$row->v_nota_nettononinsentif
                            where i_periode='$iperiode' and i_area='$row->i_area' and i_salesman='$row->i_salesman'");
        }
		  }
##### per nota
      $this->db->query("delete from tm_target_itemnota where i_periode='$iperiode'");
      $this->db->select("	to_char(a.d_doc,'yyyymm') as i_periode, a.i_area, a.i_salesman, a.i_customer, c.i_city_type,
                          c.i_city_typeperarea, c.i_city_group, b.i_city, a.i_doc as i_nota, a.i_docspb as i_spb, a.d_doc as d_nota, 
                          a.d_docspb as d_spb, a.e_remark, a.f_masalah, a.f_insentif, a.v_netto
                          from vpenjualan a
                          inner join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
                          inner join tr_city c on(b.i_city=c.i_city and a.i_area=c.i_area)
                          inner join tr_area d on(a.i_area=d.i_area)
                          where to_char(a.d_doc,'yyyymm')='$iperiode'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $this->db->query("insert into tm_target_itemnota values('$iperiode', '$row->i_area', '$row->i_salesman', '$row->i_customer',
                            '$row->i_city_type', '$row->i_city_typeperarea', '$row->i_city_group', '$row->i_city', '$row->i_nota', 
                            '$row->i_spb', '$row->d_nota', '$row->d_spb', '$row->e_remark', '$row->f_masalah', '$row->f_insentif', 
                            $row->v_netto)");
        }
		  }
##### per kota
      $this->db->select("	i_area, i_city, i_salesman, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select c.i_area, c.i_city, a.i_salesman, sum(a.v_netto) as v_nota_netto, sum(a.v_gross) as v_nota_gross, 
                          0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, sum(a.v_spb) as v_spb_gross, 
                          sum(a.v_spb)-sum(a.v_spbdiscount) as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area, c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 
                          sum(a.v_gross) as v_nota_grossinsentif, sum(a.v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 
                          0 as v_nota_nettononinsentif, 0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 
                          0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 
                          0 as v_spb_netto, sum(a.v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.f_insentif='t')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city,a.i_salesman
                          union all
                          select c.i_area, c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, sum(a.v_gross) as v_nota_grossnoninsentif, sum(a.v_netto) as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(a.v_kn) as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.f_insentif='f')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area, c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          sum(a.v_gross) as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto, 0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group<>'00')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area,c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, sum(a.v_gross) as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group='00')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area,c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(a.v_gross) as v_nota_baby, 
                          sum(a.v_gross) as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group<>'00' and a.f_insentif='t')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area, c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          sum(a.v_gross) as v_nota_baby, 0 as v_nota_babyinsentif, sum(a.v_gross) as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group<>'00' and a.f_insentif='f')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area,c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(a.v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, sum(a.v_gross) as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group='00' and a.f_insentif='t')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          union all
                          select c.i_area, c.i_city, a.i_salesman, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,sum(a.v_gross) as v_nota_baby, 
                          0 as v_nota_babyinsentif, sum(a.v_gross) as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          sum(a.v_gross) as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from tr_city c, tr_customer b
                          left join vpenjualan a on(a.i_customer=b.i_customer and to_char(a.d_doc,'yyyymm')='$iperiode'
                                                    and a.i_product_group='00' and a.f_insentif='f')
                          where b.i_city=c.i_city and b.i_area=c.i_area
                          group by c.i_area, c.i_city, a.i_salesman
                          ) as a
                          group by i_area, i_city, i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $this->db->select("i_area from tm_target_itemkota where 
                             i_periode='$iperiode' and i_area='$row->i_area' and i_city='$row->i_city' and i_salesman='$row->i_salesman'");
          $cek = $this->db->get();
     		  if ($cek->num_rows() > 0){
            $quer 	= $this->db->query("SELECT current_timestamp as c");
            $raw   	= $quer->row();
            $dproses= $raw->c;
            $this->db->query("update tm_target_itemkota set v_real_insentif=$row->v_nota_grossinsentif,
                              v_real_regularinsentif=$row->v_nota_regulerinsentif,
                              v_real_babyinsentif=$row->v_nota_babyinsentif,
                              v_real_noninsentif=$row->v_nota_grossnoninsentif,
                              v_real_regularnoninsentif=$row->v_nota_regulernoninsentif,
                              v_real_babynoninsentif=$row->v_nota_babynoninsentif,
                              v_retur_insentif=$row->v_retur_insentif,
                              v_retur_noninsentif=$row->v_retur_noninsentif,
                              v_spb_gross=$row->v_spb_gross,
                              v_spb_netto=$row->v_spb_netto,
                              v_nota_gross=$row->v_nota_gross,
                              v_nota_netto=$row->v_nota_netto,
                              v_nota_grossinsentif=$row->v_nota_grossinsentif,
                              v_nota_nettoinsentif=$row->v_nota_nettoinsentif,
                              v_nota_grossnoninsentif=$row->v_nota_grossnoninsentif,
                              v_nota_nettononinsentif=$row->v_nota_nettononinsentif,
                              d_process='$dproses'
                              where i_periode='$iperiode' and i_area='$row->i_area' and i_city='$row->i_city' 
                              and i_salesman='$row->i_salesman'");
          }elseif($row->i_salesman!='' || $row->i_salesman==null){
            $quer 	= $this->db->query("SELECT current_timestamp as c");
            $raw   	= $quer->row();
            $dproses= $raw->c;
            $this->db->query("insert into tm_target_itemkota values('$iperiode', '$row->i_area', '$row->i_salesman', '$row->i_city',
                              0, $row->v_nota_grossinsentif, $row->v_nota_regulerinsentif, $row->v_nota_babyinsentif,
                              $row->v_nota_grossnoninsentif, $row->v_nota_regulernoninsentif, $row->v_nota_babynoninsentif,
                              $row->v_retur_insentif, $row->v_retur_noninsentif, $row->v_spb_gross, $row->v_spb_netto,
                              $row->v_nota_gross, $row->v_nota_netto, $row->v_nota_grossinsentif, $row->v_nota_nettoinsentif,
                              $row->v_nota_grossnoninsentif, $row->v_nota_nettononinsentif, '$dproses', '$dproses')");
          }
        }
		  }
##### hitung retur
      $this->db->query("delete from tm_target_itemretur where i_periode='$iperiode'");
      $this->db->select("	to_char(a.d_doc,'yyyymm') as i_periode, a.i_area, a.i_salesman, a.i_customer, c.i_city_type,
                          c.i_city_typeperarea, c.i_city_group, b.i_city, a.i_kn, a.d_kn, 
                          a.e_remark, a.f_masalah, a.f_insentif, a.v_kn
                          from vpenjualan a
                          inner join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
                          inner join tr_city c on(b.i_city=c.i_city and a.i_area=c.i_area)
                          inner join tr_area d on(a.i_area=d.i_area)
                          where to_char(a.d_kn,'yyyymm')='$iperiode'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
          $this->db->query("insert into tm_target_itemretur values('$iperiode', '$row->i_area', '$row->i_salesman', '$row->i_customer',
                            '$row->i_city_type', '$row->i_city_typeperarea', '$row->i_city_group', '$row->i_city', '$row->i_kn', 
                            '$row->d_kn', '$row->e_remark', '$row->f_masalah', '$row->f_insentif', $row->v_kn)");
        }
		  }
    }
    function baca($iperiode)
    {
		  $this->db->select("	a.i_area, a.e_area_name, v_target, v_nota_grossinsentif, v_real_regularinsentif, v_real_babyinsentif,
                          v_retur_insentif, v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross, 
                          to_char(d_process,'dd-mm-yyyy hh:mi:ss')
                          as d_process
                          from tr_area a
                          left join tm_target b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.i_periode='$iperiode'
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacapersales($iperiode,$iarea)
    {
      $this->db->select("	a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, v_target, v_nota_grossinsentif,
                          v_real_regularinsentif, v_real_babyinsentif, v_retur_insentif, 
                          v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross
                          from tr_area a 
                          inner join tm_target_itemsls b on(a.i_area=b.i_area and b.i_periode='$iperiode' and b.i_area='$iarea') 
                          inner join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area)
                          where a.f_area_real='t' 
                          order by c.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacapernota($iperiode,$iarea)
    {
      $this->db->select("	a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, b.i_nota, b.d_nota, b.v_netto, 
                          b.i_customer, d.e_customer_name, d.e_customer_address, e.e_city_name
                          from tr_area a 
                          inner join tm_target_itemnota b on(a.i_area=b.i_area and b.i_periode='$iperiode' and b.i_area='$iarea') 
                          inner join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area)
                          inner join tr_customer d on(b.i_customer=d.i_customer)
                          inner join tr_city e on(b.i_city=e.i_city and a.i_area=e.i_area)
                          where a.f_area_real='t' 
                          order by b.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperkota($iperiode,$iarea)
    {
      $this->db->select("	a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, b.i_city, b.v_target, b.v_nota_grossinsentif, 
                          b.v_real_regularinsentif, b.v_real_babyinsentif, b.v_retur_insentif, b.v_nota_grossnoninsentif,
                          b.v_retur_noninsentif, b.v_spb_gross, e.e_city_name 
                          from tr_area a 
                          inner join tm_target_itemkota b on(a.i_area=b.i_area and b.i_periode='$iperiode' and b.i_area='$iarea') 
                          left join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area) 
                          inner join tr_city e on(b.i_city=e.i_city and a.i_area=e.i_area) 
                          where a.f_area_real='t' 
                          order by e.e_city_name, i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaretur($iperiode,$iarea)
    {
      $this->db->select("	a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, b.i_kn, b.d_kn, b.v_netto, 
                          b.i_customer, d.e_customer_name, d.e_customer_address, e.e_city_name
                          from tr_area a 
                          inner join tm_target_itemretur b on(a.i_area=b.i_area and b.i_periode='$iperiode' and b.i_area='$iarea') 
                          inner join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area)
                          inner join tr_customer d on(b.i_customer=d.i_customer)
                          inner join tr_city e on(b.i_city=e.i_city and a.i_area=e.i_area)
                          where a.f_area_real='t' 
                          order by c.i_salesman,b.d_kn, b.i_kn",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaallsales($iperiode)
    {
      $this->db->select("	a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, v_target, v_nota_grossinsentif,
                          v_real_regularinsentif, v_real_babyinsentif, v_retur_insentif, 
                          v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross
                          from tr_area a 
                          inner join tm_target_itemsls b on(a.i_area=b.i_area and b.i_periode='$iperiode') 
                          inner join tr_salesman c on(b.i_salesman=c.i_salesman)
                          where a.f_area_real='t' 
                          order by a.i_area, c.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	i_area, e_area_name, i_customer, e_customer_name, i_nota, d_nota, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, sum(b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, sum(b.bayar) as total, 
                          0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, 0 as total, 0 as realisasi, 
                          sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, i_nota, d_nota, 0 as total, 0 as realisasi, 
                          sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, i_nota, d_nota
                          ) as a 
                          group by i_area, e_area_name, i_customer, e_customer_name, i_nota, d_nota
                          order by a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
