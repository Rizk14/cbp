<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
        $this->db->select("	b.i_periode, a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, v_target, v_nota_grossinsentif,
                            v_real_regularinsentif, v_real_regularnoninsentif, v_real_babyinsentif, v_real_babynoninsentif, v_retur_insentif, 
                            v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross, v_spb_netto, v_nota_netto
                            from tr_area a 
                            inner join tm_target_itemsls b on(a.i_area=b.i_area and b.i_periode='$iperiode') 
                            inner join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area)
                            where a.f_area_real='t' and (upper(b.i_salesman) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')
                            order by a.i_area,c.i_salesman",false);
      }else{
        $this->db->select("	b.i_periode, a.i_area, a.e_area_name, c.i_salesman, c.e_salesman_name, v_target, v_nota_grossinsentif,
                            v_real_regularinsentif, v_real_regularnoninsentif, v_real_babyinsentif, v_real_babynoninsentif, v_retur_insentif, 
                            v_nota_grossnoninsentif, v_retur_noninsentif, v_spb_gross, v_spb_netto, v_nota_netto
                            from tr_area a 
                            inner join tm_target_itemsls b on(a.i_area=b.i_area and b.i_periode='$iperiode') 
                            inner join tr_salesman c on(b.i_salesman=c.i_salesman and b.i_area=c.i_area)
                            where a.f_area_real='t' and (upper(b.i_salesman) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            order by a.i_area,c.i_salesman",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
		    $this->db->select("	a.*, b.e_salesman_name, c.v_target as v_target_area,d.e_area_name 
							              from tm_target_itemsls a, tr_salesman b, tm_target c, tr_area d
							              where a.i_periode = '$iperiode'
							              and a.i_area=c.i_area and a.i_periode=c.i_periode
							              and a.i_area=d.i_area
							              and a.i_salesman=b.i_salesman
							              and(upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
							              order by a.i_area, a.i_salesman ",false)->limit($num,$offset);
      }else{
        $this->db->select("	a.*, b.e_salesman_name, c.v_target as v_target_area,d.e_area_name 
							              from tm_target_itemsls a, tr_salesman b, tm_target c, tr_area d
							              where a.i_periode = '$iperiode'
							              and a.i_area=c.i_area and a.i_periode=c.i_periode
							              and a.i_area=d.i_area
							              and a.i_salesman=b.i_salesman
							              and(upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            order by a.i_area, a.i_salesman ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
