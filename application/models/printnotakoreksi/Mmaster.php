<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_nota) like '%$cari%')
							order by a.i_nota desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($inota)
    {
		$this->db->select(" * from tm_notakoreksi
							inner join tr_customer on (tm_notakoreksi.i_customer=tr_customer.i_customer)
							inner join tr_salesman on (tm_notakoreksi.i_salesman=tr_salesman.i_salesman)
							left join tr_customer_pkp on (tm_notakoreksi.i_customer=tr_customer_pkp.i_customer)
							where tm_notakoreksi.i_nota = '$inota'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($inota)
    {
		$this->db->select(" * from tm_notakoreksi_item
							inner join tr_product_motif on (tm_notakoreksi_item.i_product_motif=tr_product_motif.i_product_motif
														and tm_notakoreksi_item.i_product=tr_product_motif.i_product)
							where tm_notakoreksi_item.i_nota = '$inota' order by tm_notakoreksi_item.i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_notakoreksi a, tr_customer b
							where a.i_customer=b.i_customer
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
