<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproduct)
    {
		$this->db->select(" * FROM tr_product
					LEFT JOIN tr_product_base
					ON (tr_product.i_product_base = tr_product_base.i_product_base)
					LEFT JOIN tr_product_motif
					ON (tr_product.i_product_motif = tr_product_motif.i_product_motif)
					where tr_product.i_product = '$iproduct' ");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($iproduct, $eproductname, $iproductbase,$iproductmotif)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_product (i_product, e_product_name, i_product_base, i_product_motif, d_product_entry) values ('$iproduct', '$eproductname','$iproductbase','$iproductmotif', '$dentry')");
		redirect('product/cform/');
    }

    function update($iproduct, $eproductname, $iproductbase, $iproductmotif)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_product set e_product_name = '$eproductname', i_product_base = '$iproductbase', i_product_motif = '$iproductmotif', d_product_update = '$dupdate' where i_product = '$iproduct'");
		redirect('product/cform/');
    }
	
    public function delete($iproduct) 
    {
		$this->db->query('DELETE FROM tr_product WHERE i_product=\''.$iproduct.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * FROM tr_product
					LEFT JOIN tr_product_base
					ON (tr_product.i_product_base = tr_product_base.i_product_base)
					LEFT JOIN tr_product_motif
					ON (tr_product.i_product_motif = tr_product_motif.i_product_motif) 
					order by tr_product.i_product ",false
				);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaproductbase($num,$offset)
    {
		$this->db->select("i_product_base, e_product_basename from tr_product_base order by i_product_base",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaproductmotif($num,$offset)
    {
		$this->db->select("i_product_motif, e_product_motifname from tr_product_motif order by i_product_motif",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_product, a.e_productname, a.i_product_base, a.d_productentry, b.e_product_basename from tr_product a, tr_product_base b where (upper(a.e_productname) like '%$cari%' or upper(a.i_product) like '%$cari%' or upper(b.e_product_basename) like '%$cari%' or upper(a.i_product_base) like '%$cari%') and a.i_product_base=b.i_product_base order by a.i_product ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproductbase($cari,$num,$offset)
    {
		$this->db->select("i_product_base, e_product_basename from tr_product_base where upper(e_product_basename) like '%$cari%' or upper(i_product_base) like '%$cari%' order by i_product_base", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariproductmotif($cari,$num,$offset)
    {
		$this->db->select("i_product_motif, e_product_motifname from tr_product_motif where upper(e_product_motifname) like '%$cari%' or upper(i_product_motif) like '%$cari%' order by i_product_motif ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
