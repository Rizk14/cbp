<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bacaperiode($iperiode)
    {

      $thn = substr($iperiode, 0, 4);
      $bln = substr($iperiode, 4, 2);
      $periodesebelum = date('Ym', strtotime('-1 month', strtotime($thn . '-' . $bln . '-01')));
      $data=$this->db->query("select e_periode, i_product, i_product_motif, i_product_grade, e_product_name, n_awal, n_beli, n_jual, n_akhir, v_harga
                              from tm_mutasi_hpp where e_periode='$iperiode'");
      if ($data->num_rows() > 0){     
        return $data->result();
      }
    }

    public function findWithAttr($array, $attr, $value)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr] === $value) {
                return $i;
            }
        }

        return -1;
    }

    public function findWithAttr2($array, $attr1, $attr2, $value1, $value2)
    {
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i][$attr1] === $value1 && $array[$i][$attr2] === $value2) {
                return $i;
            }
        }

        return -1;
    }
}
