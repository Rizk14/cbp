<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
  }
  function bacaperiode($iperiode)
  {
    $this->db->select(" x.i_area, x.e_area_name, x.i_product_group, x.e_product_groupname, sum(x.jumlah) as jumlah, 
                        sum(x.sisasaldo) as sisasaldo, sum(x.git) as git
                        from (
                        select a.i_area, b.e_area_name, d.i_product_group, d.e_product_groupname, 
                        sum(e.n_deliver*j.v_product_retail) as jumlah,
                        0 as sisasaldo, 0 as git
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_nota_item e, tr_product g, tr_product_type h, 
                        tr_product_price j 
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area
                        and j.i_product=g.i_product and j.i_price_group='00'
                        and a.i_area=b.i_area and to_char(a.d_nota, 'yyyymm')='$iperiode' and not a.i_nota is null 
                        and not a.i_sj like '%-%-00%' and h.i_product_group=d.i_product_group and a.f_nota_cancel='f' 
                        and c.f_spb_consigment='f' and e.i_product=g.i_product 
                        and g.i_product_type=h.i_product_type and a.i_sj=e.i_sj and a.i_area=e.i_area 
                        group by a.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name 
     
                        union all
                        select b.i_area, b.e_area_name, d.i_product_group, d.e_product_groupname, 0 as jumlah, 
                        sum(i.n_saldo_akhir*j.v_product_retail) as sisasaldo, 
                        sum((i.n_mutasi_git+n_git_penjualan)*j.v_product_retail) as git 
                        from tr_area b, tr_product_group d, tr_product g, tr_product_type h, tm_mutasi i, tr_product_price j 
                        where 
                        b.i_area=i.i_store and 
                        d.i_product_group=h.i_product_group and
                        g.i_product=i.i_product and i.i_store_location<>'PB' and i.i_store<>'PB' AND
                        h.i_product_type=g.i_product_type and
                        i.e_mutasi_periode='$iperiode' and 
                        j.i_product=g.i_product and j.i_price_group='00'
                        group by b.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name, b.i_store
      
                        union all 
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, 'Modern Outlet ' as e_product_groupname, 
                        sum(e.n_deliver*j.v_product_retail) as jumlah,
                        0 as sisasaldo, 0 as git
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_nota_item e, tr_product g, tr_product_type h, 
                        tr_product_price j 
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area
                        and j.i_product=g.i_product and j.i_price_group='00'
                        and a.i_area=b.i_area and to_char(a.d_nota, 'yyyymm')='$iperiode' and not a.i_nota is null 
                        and not a.i_sj like '%-%-00%' and h.i_product_group=d.i_product_group and a.f_nota_cancel='f' 
                        and c.f_spb_consigment='t' and e.i_product=g.i_product 
                        and g.i_product_type=h.i_product_type and a.i_sj=e.i_sj and a.i_area=e.i_area 
                        group by a.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name

                        union all
                        select b.i_area, b.e_area_name, 'PB' as i_product_group, 'Modern Outlet ' as e_product_groupname, 0 as jumlah, 
                        sum(i.n_saldo_akhir*j.v_product_retail) as sisasaldo, 
                        sum((i.n_mutasi_git+n_git_penjualan)*j.v_product_retail) as git 
                        from tr_area b, tr_product_group d, tr_product g, tr_product_type h, tm_mutasi i, tr_product_price j 
                        where 
                        b.i_area=i.i_store and 
                        d.i_product_group=h.i_product_group and
                        g.i_product=i.i_product and
                        h.i_product_type=g.i_product_type and
                        i.e_mutasi_periode='$iperiode' and (i.i_store_location='PB' or i.i_store='PB') and
                        j.i_product=g.i_product and j.i_price_group='00'
                        group by b.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name
                        ) x 
                        group by x.i_area, x.e_area_name, x.i_product_group, x.e_product_groupname
                        order by x.i_area, x.e_product_groupname
                        ",false);

  	$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($iperiode)
  {
		$this->db->select(" distinct a.i_area, b.e_area_name
                        from vpenjualanperdivisi a, tr_area b
                        where a.i_area=b.i_area and to_char(a.d_doc,'yyyymm')='$iperiode'
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($iperiode)
  {
		$this->db->select(" a.* from (
                        SELECT distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisi a
                        inner join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where to_char(a.d_doc,'yyyymm')='$iperiode'
                        union all
                        SELECT distinct a.i_product_group, 'Modern Outlet' as e_product_groupname
                        from vpenjualanperdivisi a 
                        where to_char(a.d_doc,'yyyymm')='$iperiode'
                        and a.i_product_group not in (select i_product_group from tr_product_group)
                        ) as a
                        order by a.e_product_groupname",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
