<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ialokasi,$ikbank,$iarea,$isupplier) 
    {
		$this->db->query("update tm_alokasi_bk set f_alokasi_cancel='t' WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi'  and i_supplier='$isupplier'");
#####UnPosting      
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ialokasi' and i_area='$iarea'");

      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }

      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ialokasi' and i_area='$iarea'");
#####
      $quer 	= $this->db->query("SELECT i_nota, v_jumlah, i_coa_bank from tm_alokasi_bk_item
                                  WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_supplier='$isupplier'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("UPDATE tm_dtap set v_sisa=v_sisa+$xx->v_jumlah WHERE i_dtap='$xx->i_nota' and i_supplier = '$isupplier'");
          $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");
        }
      }
    }
    function bacasupplier($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_supplier  order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function carisupplier($cari,$num,$offset,$iuser)
    {
		  $this->db->select("* from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%')
						     order by i_supplier ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($isupplier,$dfrom,$dto,$offset,$cari)
    // function bacaperiode($iarea,$dfrom,$dto,$offset,$cari)
    {
		// $this->db->select("	a.i_alokasi, a.i_kbank, a.i_supplier, b.e_supplier_name, a.d_alokasi, 
		//                     a.v_jumlah, a.v_lebih
		//                     from tm_alokasi_bk a, tr_supplier b
		// 					          where (upper(a.i_alokasi) like '%$cari%') 
		// 					          and a.i_supplier=b.i_supplier and a.f_alokasi_cancel='f'
		// 					          and a.i_supplier='$isupplier' and
		// 					          a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
		// 					          a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
    //                     ORDER BY a.i_alokasi ",false)->limit($num,$offset);
                        
//----------------------------------------------------------------------------------------------------------------------------//
if ($cari == '') {
  $sql =" a.i_alokasi, a.i_kbank, a.i_supplier, b.e_supplier_name, a.d_alokasi, 
          a.v_jumlah, a.v_lebih
          from tm_alokasi_bk a, tr_supplier b
          where a.i_supplier=b.i_supplier and a.f_alokasi_cancel='f' and";
  if($isupplier!='AS'){
    $sql .=" a.i_supplier='$isupplier' and ";
  }
  $sql .=" a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
          a.d_alokasi <= to_date('$dto','dd-mm-yyyy') ";
  $sql .=" ORDER BY a.i_alokasi ";
  $this->db->select($sql,false)->limit($offset);
}else{
  $sql =" a.i_alokasi, a.i_kbank, a.i_supplier, b.e_supplier_name, a.d_alokasi, 
          a.v_jumlah, a.v_lebih
          from tm_alokasi_bk a, tr_supplier b
          where a.i_supplier=b.i_supplier and a.f_alokasi_cancel='f' and";
  if($isupplier!='AS'){
    $sql .=" a.i_supplier='$isupplier' and ";
  }
  $sql .= " (a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
            a.d_alokasi <= to_date('$dto','dd-mm-yyyy')) AND
            (upper(a.i_alokasi) like '%$cari%')
            ORDER BY a.i_alokasi ";
  $this->db->select($sql,false)->limit($offset);
}
//----------------------------------------------------------------------------------------------------------------------------//                        
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
