<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($dfrom,$dto)
  {
    $this->db->select(" x.i_area, x.e_area_name, x.i_product_group, x.e_product_groupname, sum(x.jumlah) as jumlah, sum(n_nota) as n_nota, 
                        sum(x.n_spb) as n_spb 
                        from (
                        select a.i_area, b.e_area_name, d.i_product_group, d.e_product_groupname, 0 as n_nota, sum(f.n_order) as n_spb
                        , 0 as jumlah
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_spb_item f, tr_product g, tr_product_type h
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area and a.i_area=b.i_area
                        and a.d_nota>='$dfrom' and a.d_nota<='$dto' and not a.i_nota is null
                        and a.f_nota_cancel='f' and c.f_spb_consigment='f' and c.i_spb=f.i_spb and c.i_area=f.i_area
                        and h.i_product_group=d.i_product_group and f.i_product=g.i_product and g.i_product_type=h.i_product_type
                        group by a.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, d.i_product_group, d.e_product_groupname, sum(e.n_deliver) as n_nota, 0 as n_spb
                        , sum(e.n_deliver*e.v_unit_price) as jumlah
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_nota_item e, tr_product g, tr_product_type h
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area and a.i_area=b.i_area
                        and a.d_nota>='$dfrom' and a.d_nota<='$dto' and not a.i_nota is null
                        and h.i_product_group=d.i_product_group and a.f_nota_cancel='f' and c.f_spb_consigment='f'
                        and e.i_product=g.i_product and g.i_product_type=h.i_product_type
                        and a.i_sj=e.i_sj and a.i_area=e.i_area
                        group by a.i_area, d.i_product_group, d.e_product_groupname, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, 'Modern Outlet ' as e_product_groupname, 0 as n_nota, 
                        sum(f.n_order) as n_spb, 0 as jumlah
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_spb_item f, tr_product g, tr_product_type h
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area and a.i_area=b.i_area
                        and a.d_nota>='$dfrom' and a.d_nota<='$dto' and not a.i_nota is null
                        and h.i_product_group=d.i_product_group and a.f_nota_cancel='f' and c.f_spb_consigment='t'
                        and c.i_spb=f.i_spb and c.i_area=f.i_area
                        and f.i_product=g.i_product and g.i_product_type=h.i_product_type
                        group by a.i_area, d.i_product_group, e_product_groupname, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, 'Modern Outlet ' as e_product_groupname, 
                        sum(e.n_deliver) as n_nota, 0 as n_spb, sum(e.n_deliver*e.v_unit_price) as jumlah
                        from tm_nota a, tr_area b, tr_product_group d, tm_spb c, tm_nota_item e, tr_product g, tr_product_type h
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area and a.i_area=b.i_area
                        and a.d_nota>='$dfrom' and a.d_nota<='$dto' and not a.i_nota is null
                        and h.i_product_group=d.i_product_group and a.f_nota_cancel='f' and c.f_spb_consigment='t'
                        and e.i_product=g.i_product and g.i_product_type=h.i_product_type
                        and a.i_sj=e.i_sj and a.i_area=e.i_area
                        group by a.i_area, d.i_product_group, e_product_groupname, b.e_area_name
                        ) x
                        group by x.i_area, x.e_area_name, x.i_product_group, x.e_product_groupname
                        order by x.i_area, x.e_product_groupname  ",false);
#                        , sum(n_nota) as n_nota, sum(n_spb) as n_spb 

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($dfrom,$dto)
  {
		$this->db->select(" distinct a.i_area, b.e_area_name
                        from vpenjualanperdivisi a, tr_area b
                        where a.i_area=b.i_area and a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($dfrom,$dto)
  {
		$this->db->select(" a.* from (
                        SELECT distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisi a 
                        inner join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        union all
                        SELECT distinct a.i_product_group, 'Modern Outlet' as e_product_groupname
                        from vpenjualanperdivisi a 
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        and a.i_product_group not in (select i_product_group from tr_product_group)
                        ) as a
                        order by a.e_product_groupname",false);
/*
		$this->db->select(" distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisi a left join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        order by b.e_product_groupname",false);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
