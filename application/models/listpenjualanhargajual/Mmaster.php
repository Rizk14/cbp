<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
    $tahun = substr($iperiode,2,4);
		  $this->db->select(" a.i_product, a.e_product_name, a.i_product_motif, a.i_product_grade, sum(a.n_deliver) as n_deliver, b.i_price_group, 
		                      a.v_unit_price, (a.v_unit_price * sum(a.n_deliver)) as total_harga_jual                           
		                      from tm_nota_item a, tr_customer b, tm_nota c, tr_product_price d
                          where a.i_nota like 'FP-$tahun%'
                          and a.i_nota = c.i_nota and a.i_area = c.i_area and b.i_customer = c.i_customer
                          and a.i_product = d.i_product and a.i_product_grade = d.i_product_grade 
                          and b.i_price_group = d.i_price_group                          
                          group by a.i_product, a.e_product_name, a.i_product_motif, a.i_product_grade, b.i_price_group, a.v_unit_price
                          order by a.i_product
		   ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
