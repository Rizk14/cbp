<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from
							(select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
							where f_posting = 'f' and f_nota_koreksi='f'
							union all
							select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
							where f_posting = 'f') as nota
							order by i_nota desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" * from
							(select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
							where f_posting = 'f' and f_nota_koreksi='f' and (upper(i_nota) like '%$cari%')
							union all
							select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
							where f_posting = 'f' and (upper(i_nota) like '%$cari%')) as nota
							order by i_nota desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacanota($inota){
		$this->db->select(" * from 
							(
							select * from tm_nota 
							left join tm_promo on (tm_nota.i_spb_program=tm_promo.i_promo)
							inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
							inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
							inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
							left join tm_spb on (tm_nota.i_spb=tm_spb.i_spb and tm_nota.i_area=tm_spb.i_area)
							where tm_nota.i_nota = '$inota' and tm_nota.f_posting = 'f' and tm_nota.f_nota_koreksi='f'
							union all 
							select * from tm_notakoreksi
							left join tm_promo on (tm_notakoreksi.i_spb_program=tm_promo.i_promo)
							inner join tr_customer on (tm_notakoreksi.i_customer=tr_customer.i_customer)
							inner join tr_salesman on (tm_notakoreksi.i_salesman=tr_salesman.i_salesman)
							inner join tr_customer_area on (tm_notakoreksi.i_customer=tr_customer_area.i_customer)
							inner join tm_spb on (tm_notakoreksi.i_spb=tm_spb.i_spb and tm_notakoreksi.i_area=tm_spb.i_area)
							where tm_notakoreksi.i_nota = '$inota' and tm_notakoreksi.f_posting = 'f'
							)
							as c",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacadetailnota($inota){
		$this->db->select(" * from
							(select d.*, f.e_product_motifname from tm_nota_item d, tr_product_motif f
							where d.i_nota='$inota' and d.i_product=f.i_product and d.i_product_motif=f.i_product_motif
							union all
							select e.*, f.e_product_motifname from tm_notakoreksi_item e, tr_product_motif f
							where e.i_nota='$inota' and e.i_product=f.i_product and e.i_product_motif=f.i_product_motif) as nota",FALSE);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function inserttransheader(	$inota,$iarea,$eremark,$fclose,$dnota )
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharian 
						 (i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
						  	  values
					  	 ('$inota','$iarea','$dentry','$eremark','$fclose','$dnota','$dnota')");
	}
	function inserttransitemdebet($accdebet,$ipelunasan,$namadebet,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dnota)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dnota','$dnota','$dentry')");
	}
	function inserttransitemkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$fposting,$iarea,$egirodescription,$vjumlah,$dnota)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
						 (i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry)
						  	  values
					  	 ('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dnota','$dnota','$dentry')");
	}
	function insertgldebet($accdebet,$ipelunasan,$namadebet,$fdebet,$iarea,$vjumlah,$dnota,$eremark)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$accdebet','$dnota','$namadebet','$fdebet',$vjumlah,'$iarea','$dnota','$eremark','$dentry')");
	}
	function insertglkredit($acckredit,$ipelunasan,$namakredit,$fdebet,$iarea,$vjumlah,$dnota,$eremark)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$this->db->query("insert into tm_general_ledger
						 (i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry)
						  	  values
					  	 ('$ipelunasan','$acckredit','$dnota','$namakredit','$fdebet','$vjumlah','$iarea','$dnota','$eremark','$dentry')");
	}
	function updatenota($inota,$iarea)
    {
		$this->db->query("update tm_nota set f_posting='t' where i_nota='$inota' and i_area='$iarea' and f_nota_koreksi='f'");
		$this->db->query("update tm_notakoreksi set f_posting='t' where i_nota='$inota' and i_area='$iarea'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
