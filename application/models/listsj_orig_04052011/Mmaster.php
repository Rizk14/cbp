<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj, $iarea) 
    {
			$this->db->query("update tm_nota set f_sj_cancel='t' WHERE i_sj='$isj' and i_sj_type='04' and i_area_from='$iarea'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_from=b.i_area and a.i_sj_type='04'
								and (upper(a.i_area_from) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_from=b.i_area and a.i_sj_type='04'
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_from=b.i_area and a.i_sj_type='04'
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													  where a.i_area_to=b.i_area and a.i_sj_type='04'
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
													  and a.i_area_from='$iarea'

													  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy')
													  and a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
//                            and a.f_sj_daerah='f' and
/*
			  $this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													  where a.i_area_from=b.i_area and a.i_sj_type='04'
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
													  and a.i_area_to='$iarea'
                            and a.f_sj_daerah='f' and
													  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  ORDER BY a.i_sj ",false)->limit($num,$offset);
*/
      }else{
			  $this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													  where a.i_area_from=b.i_area and a.i_sj_type='04'
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
													  and a.i_area_to='$iarea'
                            and a.f_sj_daerah='t' and
													  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													where a.i_area_from=b.i_area and a.i_sj_type='04'
													and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
													and a.i_area_to='$iarea' and
													a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sj <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.d_sj, a.i_sj ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
