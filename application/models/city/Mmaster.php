<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($icity, $iarea)
	{
		$this->db->select("a.*, b.e_area_name, c.e_city_typename, d.e_city_typeperareaname, e.e_city_groupname, f.e_city_statusname")->from('tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, tr_city_group e, tr_city_status f')->where("a.i_city = '$icity' and a.i_area = '$iarea' and a.i_area=b.i_area and a.i_city_type=c.i_city_type and a.i_city_typeperarea=d.i_city_typeperarea and a.i_area=d.i_area and a.i_city_group=e.i_city_group and a.i_city_status= f.i_city_status and b.i_area=e.i_area");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}

	function insert(
		$icity,
		$ecityname,
		$iarea,
		$icitytype,
		$icitytypeperarea,
		$icitygroup,
		$icitystatus,
		$ntoleransipusat,
		$ntoleransicabang
	) {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry = $row->c;
		$this->db->query("insert into tr_city (i_city, i_area, i_city_type, i_city_typeperarea, i_city_group,
				  i_city_status, e_city_name, d_city_entry, n_toleransi_pusat, n_toleransi_cabang) values ('$icity', '$iarea', '$icitytype',
				  '$icitytypeperarea', '$icitygroup', '$icitystatus', '$ecityname','$dentry', $ntoleransipusat, $ntoleransicabang)");
		#redirect('city/cform/');
	}

	function update(
		$icity,
		$ecityname,
		$iarea,
		$icitytype,
		$icitytypeperarea,
		$icitygroup,
		$icitystatus,
		$ntoleransipusat,
		$ntoleransicabang
	) {
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dupdate = $row->c;
		$this->db->query("update tr_city set i_city_type = '$icitytype', i_city_typeperarea = '$icitytypeperarea', 
				              i_city_group = '$icitygroup', i_city_status = '$icitystatus', e_city_name = '$ecityname',
				              d_city_update = '$dupdate', n_toleransi_pusat=$ntoleransipusat, n_toleransi_cabang=$ntoleransicabang 
                      where i_city='$icity' and i_area='$iarea' ");
		#redirect('city/cform/');
	}

	public function delete($icity, $iarea)
	{
		$this->db->query('DELETE FROM tr_city WHERE i_city=\'' . $icity . '\' and i_area=\'' . $iarea . '\'');
	}

	function bacasemua($cari, $num, $offset)
	{
		$this->db->select("a.*, b.e_area_name, c.e_city_typename, d.e_city_typeperareaname, e.e_city_groupname, f.e_city_statusname from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, tr_city_group e, tr_city_status f where a.i_area=b.i_area and a.i_city_type=c.i_city_type and a.i_area=d.i_area and a.i_city_typeperarea=d.i_city_typeperarea and a.i_city_group=e.i_city_group and a.i_city_status= f.i_city_status and b.i_area=e.i_area and (upper(a.e_city_name) like '%$cari%' or upper(a.i_city) like '%$cari%') order by b.i_area,a.i_city", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacaarea($num, $offset)
	{
		$this->db->select(" a.e_area_name, b.e_city_typename, c.* from tr_area a, tr_city_type b, tr_city_typeperarea c
				    where a.i_area=c.i_area and b.i_city_type=c.i_city_type
				    order by a.i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacacitystatus($num, $offset)
	{
		$this->db->select("i_city_status, e_city_statusname from tr_city_status order by i_city_status", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacacitygroup($iarea, $num, $offset)
	{
		$this->db->select("i_city_group, e_city_groupname from tr_city_group where i_area='$iarea'
				   order by i_area, e_city_groupname ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cari($cari, $num, $offset)
	{
		$this->db->select("a.*, b.e_area_name, c.e_city_typename, d.e_city_typeperareaname, e.e_city_groupname, f.e_city_statusname from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, tr_city_group e, tr_city_status f where a.i_area=b.i_area and a.i_city_type=c.i_city_type and a.i_area=d.i_area and a.i_city_typeperarea=d.i_city_typeperarea and a.i_city_group=e.i_city_group and a.i_city_status= f.i_city_status and b.i_area=e.i_area and (upper(a.e_city_name) ilike '%$cari%' or upper(a.i_city) ilike '%$cari%') order by b.i_area,a.i_city", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset)
	{
		$this->db->select(" a.e_area_name, b.e_city_typename, c.* from tr_area a, tr_city_type b, tr_city_typeperarea c
				    where a.i_area=c.i_area and b.i_city_type=c.i_city_type
				    and ( upper(a.e_area_name) ilike '%$cari%' or upper(a.i_area) ilike '%$cari%'
					  or upper(c.e_city_typeperareaname) ilike '%$cari%') 
				    order by a.i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricitygroup($cari, $iarea, $num, $offset)
	{
		$this->db->select("i_city_group, e_city_groupname from tr_city_group where i_area='$iarea' 
				   and upper(i_city_group) like '%$cari%' or upper(e_city_groupname) like '%$cari%'
				   order by i_area, e_city_groupname", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricitystatus($cari, $num, $offset)
	{
		$this->db->select("i_city_status, e_city_statusname from tr_city_status 
				   where upper(i_city_status) like '%$cari%' or upper(e_city_statusname) like '%$cari%'
				   order by i_city_status", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function runningnumber($iarea)
	{
		$no = $this->db->query(" SELECT CAST (substring(i_city,2,6) AS int) + 1 AS next_num FROM tr_city WHERE i_area = '$iarea' ORDER BY i_city DESC LIMIT 1 ")->row()->next_num;

		settype($no, "string");
		$a = strlen($no);
		while ($a < 6) {
			$no = "0" . $no;
			$a = strlen($no);
		}

		return $num = "K" . $no;
	}
}
