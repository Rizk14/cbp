<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl,$iarea)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
	    $this->db->select("	a.eareaname, a.i_customer, a.namapel, sum(a.notaly) as notaly, sum(a.volly) as volly, sum(a.volnew) as volnew,
                          sum(a.notacy) as notacy, sum(a.volcy) as volcy,
                          sum(a.notalm) as notalm, sum(a.vollm) as vollm,
                          sum(a.notacm) as notacm, sum(a.volcm) as volcm, a.iarea as iarea, a.namapel as namapel
                          from
                          (
                          select  c.e_area_name as eareaname, a.i_area as iarea, a.i_customer, b.e_customer_name as namapel, 
                          sum(round(d.n_deliver*d.v_unit_price-(((d.n_deliver*d.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal))) as notaly, sum(d.n_deliver) as volly, 0 as notacy, 
                          sum(d.n_deliver) as volnew, 0 as volcy, 0 as notalm , 0 as vollm, 0 as notacm, 0 as volcm
                          from tm_nota a, tr_customer b, tr_area c, tm_nota_item d
                          where 
                          a.i_customer=b.i_customer
                          and a.i_area=c.i_area
                          and a.i_nota=d.i_nota and a.i_sj=d.i_sj and a.i_area=d.i_area
                          and to_char(a.d_nota,'yyyy') = '$prevth' and f_nota_cancel='f' and a.d_nota<='$prevdate' and a.i_area='$iarea'  
                          group by a.i_customer, a.i_area, b.e_customer_name, c.e_area_name
                          union all
                          select  c.e_area_name as eareaname ,a.i_area as iarea, a.i_customer, b.e_customer_name as namapel, 0 as notaly,
                          0 as volly, sum(round(d.n_deliver*d.v_unit_price-(((d.n_deliver*d.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal))) as notacy,
                          0 as volnew, sum(d.n_deliver) as volcy, 0 as notalm , 0 as vollm, 0 as notacm, 0 as volcm
                          from tm_nota a, tr_customer b, tr_area c, tm_nota_item d
                          where 
                          a.i_customer=b.i_customer 
                          and a.i_area=c.i_area
                          and a.i_nota=d.i_nota and a.i_sj=d.i_sj and a.i_area=d.i_area
                          and to_char(a.d_nota,'yyyy') = '$th' and f_nota_cancel='f' and a.d_nota<='$todate' and a.i_area='$iarea' 
                          group by a.i_customer, a.i_area, b.e_customer_name, c.e_area_name
                          union all

                          select  c.e_area_name as eareaname ,a.i_area as iarea, a.i_customer, b.e_customer_name as namapel, 0 as notaly,
                          0 as volly, 0 as notacy, 0 as volnew, 0 as volcy, sum(round(d.n_deliver*d.v_unit_price-(((d.n_deliver*d.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal))) as notalm, 
                          sum(d.n_deliver) as vollm, 0 as notacm, 0 as volcm
                          from tm_nota a, tr_customer b, tr_area c, tm_nota_item d
                          where 
                          a.i_customer=b.i_customer 
                          and a.i_area=c.i_area
                          and a.i_nota=d.i_nota and a.i_sj=d.i_sj and a.i_area=d.i_area
                          and to_char(a.d_nota,'yyyymm') = '$prevthbl' and f_nota_cancel='f' and a.d_nota<='$prevdate' and a.i_area='$iarea' 
                          group by a.i_customer, a.i_area, b.e_customer_name, c.e_area_name
                          union all

                          select  c.e_area_name as eareaname ,a.i_area as iarea, a.i_customer, b.e_customer_name as namapel, 0 as notaly,
                          0 as volly, 0 as notacy,
                          0 as volnew, 0 as volcy, 0 as notalm , 0 as vollm, 
                          sum(round(d.n_deliver*d.v_unit_price-(((d.n_deliver*d.v_unit_price)/a.v_nota_gross)*a.v_nota_discounttotal))) as notacm, sum(d.n_deliver) as volcm
                          from tm_nota a, tr_customer b, tr_area c, tm_nota_item d
                          where 
                          a.i_customer=b.i_customer 
                          and a.i_area=c.i_area
                          and a.i_nota=d.i_nota and a.i_sj=d.i_sj and a.i_area=d.i_area
                          and to_char(a.d_nota,'yyyymm') = '$thbl' and f_nota_cancel='f' and a.d_nota<='$todate' and a.i_area='$iarea' 
                          group by a.i_customer, a.i_area, b.e_customer_name, c.e_area_name
                          ) as a
                          group by a.i_customer, a.namapel, a.eareaname, a.iarea
                          order by notaly desc LIMIT 10",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset,$iuser)
    {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    
}
?>
