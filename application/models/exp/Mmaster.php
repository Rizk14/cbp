<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaekspedisiarea($iekspedisi, $iarea)
    {
		return $this->db->query("select a.*, b.e_area_name, b.i_area from tr_ekspedisi a, tr_area b where a.i_ekspedisi = '$iekspedisi' and a.i_area='$iarea' and a.i_area=b.i_area");
    }

    function baca($iekspedisi,$iarea)
    {
		$this->db->select(" a.* from tr_ekspedisi a where a.i_ekspedisi = '$iekspedisi' ");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    
    function insert($iekspedisi,$eekspedisiname,$iarea,$eekspedisiaddress,$eekspedisicity,$eekspedisifax,$eekspedisiphone)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;	
		$this->db->query("insert into tr_ekspedisi (i_ekspedisi, e_ekspedisi, i_area, e_ekspedisi_address,
				  			   e_ekspedisi_city, e_ekspedisi_fax, e_ekspedisi_phone,
	  						   d_entry, d_update) 
				  values 
							  ('$iekspedisi', '$eekspedisiname','$iarea', '$eekspedisiaddress',
							       '$eekspedisicity','$eekspedisifax','$eekspedisiphone',
							       '$dentry','$dentry')");
		redirect('exp/cform/');
    }

    function update($iekspedisi,$eekspedisiname,$iarea,$eekspedisiaddress,$eekspedisicity,$eekspedisifax,$eekspedisiphone)
    {

		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("update tr_ekspedisi set e_ekspedisi = '$eekspedisiname', 
							 i_area = '$iarea', 			
							 e_ekspedisi_address = '$eekspedisiaddress', 
							 e_ekspedisi_city = '$eekspedisicity', 
							 e_ekspedisi_phone='$eekspedisiphone',
							 e_ekspedisi_fax='$eekspedisifax',
							 d_update='$dentry'
				  where i_ekspedisi = '$iekspedisi' ");
		redirect('exp/cform/');
    }
	
    public function delete($iekspedisi,$iarea)
    {
		  $this->db->query('DELETE FROM tr_ekspedisi WHERE i_ekspedisi=\''.$iekspedisi.'\'');
		  return TRUE;
    }
    
    function bacasemua($area, $cari, $num,$offset)
    {
		  $this->db->select("* from tr_ekspedisi where i_area='$area' 
                         and (upper(i_ekspedisi) like '%$cari%' or upper(e_ekspedisi) like '%$cari%')
                         order by i_ekspedisi", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }else{
        return 1;
      }
    }
    
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.* from tr_ekspedisi a where (upper(a.i_ekspedisi) like '%$cari%' or 
									upper(a.e_ekspedisi) like '%$cari%' or 
									upper(a.e_ekspedisi_city) like '%$cari%' or upper(a.e_ekspedisi_phone) like '%$cari%') order by a.i_ekspedisi ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5)
    {
		if($allarea=='t'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false)->limit($num,$offset);			
		}
				
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$allarea,$area1,$area2,$area3,$area4,$area5)
  {
		if($allarea=='t'){
			$this->db->select("* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                         order by i_area", false)->limit($num,$offset);
		}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
			$this->db->select("* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                         order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area 
                         where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
                         and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false)->limit($num,$offset);			
		}
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
