<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb, $iarea) 
    {
//		$this->db->query("DELETE FROM tm_spb WHERE i_spb='$ispb' and i_area='$iarea'");
//		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua($iuser,$area1,$area2,$area3,$area4,$area5,$allarea,$cari, $num,$offset)
    {
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{

				$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve1 isnull 
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%')
							and
							  ((a.f_spb_stockdaerah='t' and not a.i_approve2 isnull
							  and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser')))
							or	
							  (a.f_spb_stockdaerah='f' and not a.i_cek isnull))
							order by a.d_spb,a.i_area,a.i_spb",false)->limit($num,$offset);
		}else{
				$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve1 isnull 
							and not a.i_approve2 isnull  
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%')
							and
							  ((a.i_area in ( select i_area from tm_user_area where i_user='$iuser')))
							  
							order by a.d_spb,a.i_area,a.i_spb",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($area1,$area2,$area3,$area4,$area5,$allarea,$cari,$num,$offset)
    {
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
				$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve1 isnull 
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%')
							and
							  ((a.f_spb_stockdaerah='t' and not a.i_approve2 isnull
							  and (a.i_area='$area1' or a.i_area='$area2' 
								or a.i_area='$area3' or a.i_area='$area4' 
								or a.i_area='$area5'))
							or	
							  (a.f_spb_stockdaerah='f' and not a.i_cek isnull and a.i_approve1 isnull))
							order by a.d_spb,a.i_area,a.i_spb",false)->limit($num,$offset);
		}else{
				$this->db->select(" 	a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and a.i_approve1 isnull 
							and not a.i_approve2 isnull  
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%')
							and
							  ((a.f_spb_stockdaerah='t' and not a.i_approve2 isnull
							  and (a.i_area='$area1' or a.i_area='$area2' 
								or a.i_area='$area3' or a.i_area='$area4' 
								or a.i_area='$area5'))
							  )
              order by a.d_spb,a.i_area,a.i_spb",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function baca($ispb, $iarea)
    {
		$this->db->select(" * from tm_spb 
				   left join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	function bacadetail($ispb, $iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
					   		where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_product=b.i_product 
							and a.i_product_motif=b.i_product_motif
				   			order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
/*
	function approve($ispb,$iarea,$eapprove1,$user)
    {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'e_approve1'		=> $eapprove1,
					'd_approve1'		=> $dentry,
					'i_approve1'		=> $user
    				 );
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
*/
	function approve($ispb,$iarea,$eapprove1,$user)
    {
      if($iarea=='01' || $iarea=='02'){
        $this->db->select(" a.f_spb_stockdaerah, a.i_customer, b.i_customer_status
                            from tm_spb a, tr_customer b 
                            where a.i_spb = '$ispb' and a.i_area='$iarea' and a.i_customer=b.i_customer", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $row){
            $daerah=$row->f_spb_stockdaerah;
            $custom=$row->i_customer_status;
            $kode  =$row->i_customer;
          }
        }
        if($daerah=='f'){
          if($custom!='4'){
            $this->db->select(" v_flapond, v_saldo from tr_customer_groupar where i_customer = '$kode'", false);
	          $que = $this->db->get();
	          if ($que->num_rows() > 0){
		          foreach($que->result() as $ro){
                $flapond=$ro->v_flapond;
                $saldo  =$ro->v_saldo;
              }
            }
            if($flapond>0){
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		          $row   	= $query->row();
		          $dentry	= $row->c;
            	$data = array(
				          'e_approve2'		=> 'SYSTEM',
				          'd_approve2'		=> $dentry,
				          'i_approve2'		=> 'SYSTEM PLAFOND',
				          'e_approve1'		=> $eapprove1,
				          'd_approve1'		=> $dentry,
				          'i_approve1'		=> $user
            				 );
            	$this->db->where('i_spb', $ispb);
            	$this->db->where('i_area', $iarea);
		          $this->db->update('tm_spb', $data);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		          $row   	= $query->row();
		          $dentry	= $row->c;
            	$data = array(
				          'e_approve1'		=> $eapprove1,
				          'd_approve1'		=> $dentry,
				          'i_approve1'		=> $user
            				 );
            	$this->db->where('i_spb', $ispb);
            	$this->db->where('i_area', $iarea);
		          $this->db->update('tm_spb', $data);
            }
          }else{
            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		        $row   	= $query->row();
		        $dentry	= $row->c;
          	$data = array(
				        'e_approve1'		=> $eapprove1,
				        'd_approve1'		=> $dentry,
				        'i_approve1'		=> $user
          				 );
          	$this->db->where('i_spb', $ispb);
          	$this->db->where('i_area', $iarea);
		        $this->db->update('tm_spb', $data);
          }
        }else{
          $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		      $row   	= $query->row();
		      $dentry	= $row->c;
        	$data = array(
				      'e_approve1'		=> $eapprove1,
				      'd_approve1'		=> $dentry,
				      'i_approve1'		=> $user
        				 );
        	$this->db->where('i_spb', $ispb);
        	$this->db->where('i_area', $iarea);
		      $this->db->update('tm_spb', $data);
        }
      }else{
		    $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		    $row   	= $query->row();
		    $dentry	= $row->c;
      	$data = array(
				    'e_approve1'		=> $eapprove1,
				    'd_approve1'		=> $dentry,
				    'i_approve1'		=> $user
      				 );
      	$this->db->where('i_spb', $ispb);
      	$this->db->where('i_area', $iarea);
		    $this->db->update('tm_spb', $data);
      }
    }
	function notapprove($ispb,$iarea,$eapprove,$user)
    {
		$query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$data = array(
					'e_notapprove'		=> $eapprove,
					'd_notapprove'		=> $dentry,
					'i_notapprove'		=> $user
    				 );
    	$this->db->where('i_spb', $ispb);
    	$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
}
?>
