<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Closing KAS dan BANK tiap tanggal 04
 */

class Mclosing extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	/*----------  Hitung Ulang Transaksi Kas Kecil dan Update tm_coa_saldo  ----------*/

	public function kaskecil($sekarang,$sebelumnya)
	{
		$this->db->select('a.*, b.i_area');
		$this->db->from('tm_coa_saldo a');
		$this->db->join('tr_coa b','b.i_coa = a.i_coa');
		$this->db->where('a.i_periode', $sebelumnya);
		$this->db->like('a.i_coa', KasKecil, 'after');
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $key) {
				/*$iarea = substr($key->i_coa, 7,2);*/
				$querysaldo = $this->db->query("
					SELECT
					    sum(saldo) AS saldo,
					    sum(masuk) AS masuk,
					    sum(keluar) AS keluar,
					    sum(saldo) + sum(masuk) - sum(keluar) AS saldoakhir
					FROM
					    (
					    SELECT
					        v_saldo_awal AS saldo,
					        0 AS masuk,
					        0 AS keluar
					    FROM
					        tm_coa_saldo
					    WHERE
					        i_periode = '$sebelumnya'
					        AND i_coa = '$key->i_coa'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        sum(v_kk) AS masuk,
					        0 AS masuk
					    FROM
					        tm_kk tk
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kk_cancel = 'f'
					        AND f_debet = 'f'
					        AND i_area = '$key->i_area'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        0 AS masuk,
					        sum(v_kk) AS keluar
					    FROM
					        tm_kk tk
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kk_cancel = 'f'
					        AND f_debet = 't'
					        AND i_area = '$key->i_area') AS x
				", FALSE);
				if ($querysaldo->num_rows()>0) {
					foreach ($querysaldo->result() as $row) {
						$this->db->query("
							UPDATE 
								tm_coa_saldo
							SET 
								v_saldo_awal = $row->saldo,
								v_mutasi_debet = $row->masuk,
								v_mutasi_kredit = $row->keluar,
								v_saldo_akhir = $row->saldoakhir,
								i_update = 'Program',
								d_update = now()
							WHERE 
								i_periode = '$sebelumnya'
								AND i_coa = '$key->i_coa'
						", FALSE);
					}
				}
			}
		}
	}

	/*----------  Hitung Ulang Transaksi Kas Besar dan Update tm_coa_saldo  ----------*/
	
	public function kasbesar($sekarang,$sebelumnya)
	{
		$this->db->select('*');
		$this->db->from('tm_coa_saldo');
		$this->db->where('i_periode', $sebelumnya);
		$this->db->like('i_coa', KasBesar, 'after');
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $key) {
				$querysaldo = $this->db->query("
					SELECT
					    sum(saldo) AS saldo,
					    sum(masuk) AS masuk,
					    sum(keluar) AS keluar,
					    sum(saldo) + sum(masuk) - sum(keluar) AS saldoakhir
					FROM
					    (
					    SELECT
					        v_saldo_awal AS saldo,
					        0 AS masuk,
					        0 AS keluar
					    FROM
					        tm_coa_saldo
					    WHERE
					        i_periode = '$sebelumnya'
					        AND i_coa = '$key->i_coa'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        sum(v_kb) AS masuk,
					        0 AS masuk
					    FROM
					        tm_kb
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kb_cancel = 'f'
					        AND f_debet = 'f'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        0 AS masuk,
					        sum(v_kb) AS keluar
					    FROM
					        tm_kb
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kb_cancel = 'f'
					        AND f_debet = 't') AS x
				", FALSE);
				if ($querysaldo->num_rows()>0) {
					foreach ($querysaldo->result() as $row) {
						$this->db->query("
							UPDATE 
								tm_coa_saldo
							SET 
								v_saldo_awal = $row->saldo,
								v_mutasi_debet = $row->masuk,
								v_mutasi_kredit = $row->keluar,
								v_saldo_akhir = $row->saldoakhir,
								i_update = 'Program',
								d_update = now()
							WHERE 
								i_periode = '$sebelumnya'
								AND i_coa = '$key->i_coa'
						", FALSE);
					}
				}
			}
		}
	}

	/*----------  Hitung Ulang Transaksi Bank dan Update tm_coa_saldo  ----------*/
	
	public function bank($sekarang,$sebelumnya)
	{
		$this->db->select('*');
		$this->db->from('tm_coa_saldo');
		$this->db->where('i_periode', $sebelumnya);
		$this->db->like('i_coa', Bank, 'after');
		$query = $this->db->get();
		if ($query->num_rows()>0) {
			foreach ($query->result() as $key) {
				$querysaldo = $this->db->query("
					SELECT
					    sum(saldo) AS saldo,
					    sum(masuk) AS masuk,
					    sum(keluar) AS keluar,
					    sum(saldo) + sum(masuk) - sum(keluar) AS saldoakhir
					FROM
					    (
					    SELECT
					        v_saldo_awal AS saldo,
					        0 AS masuk,
					        0 AS keluar
					    FROM
					        tm_coa_saldo
					    WHERE
					        i_periode = '$sebelumnya'
					        AND i_coa = '$key->i_coa'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        sum(v_bank) AS masuk,
					        0 AS masuk
					    FROM
					        tm_kbank
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kbank_cancel = 'f'
					        AND f_debet = 'f'
					UNION ALL
					    SELECT
					        0 AS saldo,
					        0 AS masuk,
					        sum(v_bank) AS keluar
					    FROM
					        tm_kbank
					    WHERE
					        i_periode = '$sebelumnya'
					        AND f_kbank_cancel = 'f'
					        AND f_debet = 't') AS x
				", FALSE);
				if ($querysaldo->num_rows()>0) {
					foreach ($querysaldo->result() as $row) {
						$this->db->query("
							UPDATE 
								tm_coa_saldo
							SET 
								v_saldo_awal = $row->saldo,
								v_mutasi_debet = $row->masuk,
								v_mutasi_kredit = $row->keluar,
								v_saldo_akhir = $row->saldoakhir,
								i_update = 'Program',
								d_update = now()
							WHERE 
								i_periode = '$sebelumnya'
								AND i_coa = '$key->i_coa'
						", FALSE);
					}
				}
			}
		}
	}

	/*----------  Update Semua Saldo Akhir Bulan Sebelumnya Jadi Bulan Sekarang  ----------*/
	
	public function closingall($sekarang,$sebelumnya)
	{
		$query = $this->db->query("
			SELECT * FROM tm_coa_saldo WHERE i_periode = '$sebelumnya'
		", FALSE);
		if ($query->num_rows()>0) {
			foreach ($query->result() as $key) {
				$this->db->query("
	            INSERT
	                INTO
	                tm_coa_saldo (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry, i_entry, e_coa_name)
	            VALUES ($sekarang, '$key->i_coa', $key->v_saldo_akhir, 0, 0, $key->v_saldo_akhir, now(),'Program','$key->e_coa_name')
	            ON
	                CONFLICT (i_periode, i_coa) DO
	            UPDATE
	                SET
	                    v_saldo_awal 	= $key->v_saldo_akhir,
	                    v_mutasi_debet 	= 0,
	                    v_mutasi_kredit = 0,
	                    v_saldo_akhir  	= $key->v_saldo_akhir,
	                    d_entry  		= now(),
	                    i_entry  		= 'Program',
	                    e_coa_name  	= '$key->e_coa_name'
	        ", FALSE);
			}
		}
	}

	/*----------  Update Periode Closing ----------*/

	public function updateperiode($sekarang,$sebelumnya)
	{
		$this->db->query("
			UPDATE 
				tm_periode
			SET 
				i_periode = '$sekarang'
		", FALSE);

		// $this->db->query("
		// 	UPDATE 
		// 		tm_periode_kk
		// 	SET 
		// 		i_periode = '$sekarang'
		// ", FALSE);

		$hariini  = date('Y-m-d');
		$tanggal1 = date('Y-m').'-01';
		$this->db->query("
			UPDATE 
				tm_closing_kas_bank
			SET 
				d_closing_kb		= '$hariini',
				d_open_kb			= '$tanggal1',
				d_closing_kbin		= '$hariini',
				d_open_kbin			= '$tanggal1',
				d_closing_kbank		= '$hariini',
				d_open_kbank		= '$tanggal1',
				d_closing_kbankin	= '$hariini',
				d_open_kbankin		= '$tanggal1',
				d_closing_kk		= '$hariini',
				d_open_kk			= '$tanggal1',
				d_closing_kkin		= '$hariini',
				d_open_kkin			= '$tanggal1'
		", FALSE);
	}
}

?>