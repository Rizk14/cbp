<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ialokasi,$ikbank,$iarea) 
    {
		  $this->db->query("update tm_alokasi set f_alokasi_cancel='t' WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }
      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ialokasi' and i_area='$iarea'");
#####
      $quer 	= $this->db->query("SELECT i_nota, v_jumlah, i_coa_bank from tm_alokasi_item
                                  WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah WHERE i_nota='$xx->i_nota'");
          $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");
        }
      }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto)
    {
/*
		$this->db->select("	a.i_alokasi, a.i_kbank, a.i_area, b.e_area_name, a.d_alokasi, a.i_customer, c.e_customer_name, 
		                    a.v_jumlah, a.v_lebih, a.f_alokasi_cancel, a.i_coa_bank
		                    from tm_alokasi a, tr_area b, tr_customer c
							          where a.i_customer=c.i_customer
							          and a.i_area=b.i_area and a.i_area='$iarea' and
							          a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_alokasi, a.i_kbank",false);
*/
#and a.f_alokasi_cancel='f'
		$this->db->select("	e.i_nota, e.d_nota, c.e_customer_name, e.v_nota_netto, a.d_alokasi, a.i_giro, d.v_jumlah, f.e_bank_name, 
		                    a.d_alokasi-d.d_nota as hari_bayar
		                    from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank f
		                    where a.i_customer=c.i_customer and a.i_area=b.i_area and a.i_area='$iarea' and a.i_alokasi=d.i_alokasi and 
		                    a.i_area=d.i_area and a.i_kbank=d.i_kbank and d.i_nota=e.i_nota and a.i_coa_bank=f.i_coa and not e.i_nota isnull 
		                    and e.f_nota_cancel='f' and a.f_alokasi_cancel='f' and 
		                    a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
		                    ORDER BY c.e_customer_name, a.i_alokasi, e.i_nota",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
