<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ialokasi,$ikbank,$iarea) 
    {
		  $this->db->query("update tm_alokasi set f_alokasi_cancel='t' WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ialokasi' and i_area='$iarea'");
      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }
      $this->db->query("delete from tm_jurnal_transharian where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ialokasi' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$ialokasi' and i_area='$iarea'");
#####
      $quer 	= $this->db->query("SELECT i_nota, v_jumlah, i_coa_bank from tm_alokasi_item
                                  WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah WHERE i_nota='$xx->i_nota'");
          $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");
        }
      }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
//     function bacaperiode($iperiode)
//     {
// /*
// 		$this->db->select("	a.i_alokasi, a.i_kbank, a.i_area, b.e_area_name, a.d_alokasi, a.i_customer, c.e_customer_name, 
// 		                    a.v_jumlah, a.v_lebih, a.f_alokasi_cancel, a.i_coa_bank
// 		                    from tm_alokasi a, tr_area b, tr_customer c
// 							          where a.i_customer=c.i_customer
// 							          and a.i_area=b.i_area and a.i_area='$iarea' and
// 							          a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
// 							          a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
// 							          ORDER BY a.i_alokasi, a.i_kbank",false);
// */
// #and a.f_alokasi_cancel='f'
// // 		$this->db->select("c.i_kbank, c.d_bank, c.i_coa_bank, c.v_bank as jum_bm, a.i_alokasi, b.d_alokasi, a.i_nota, a.v_jumlah as jum_alokasi, 
// // a.v_sisa as jum_nota, (a.v_sisa - a.v_jumlah) as sisa, a.d_nota, b.d_entry, c.i_area
// // from tm_kbank c 
// // left join tm_alokasi b on (b.i_kbank=c.i_kbank and b.i_coa_bank=c.i_coa_bank and b.f_alokasi_cancel='f' and b.i_alokasi like '%-$iperiode-%')
// // left join tm_alokasi_item a on (a.i_alokasi=b.i_alokasi and a.i_kbank=b.i_kbank and a.i_area=b.i_area)
// // where c.i_kbank like '%-$iperiode-%' and c.f_kbank_cancel='f' and c.i_coa='110-41SM'
// // group by c.i_kbank, c.i_coa_bank, c.v_bank,a.i_alokasi, a.i_nota, a.d_nota, a.v_jumlah, b.d_alokasi, a.v_sisa, b.d_entry
// // order by c.i_kbank, c.i_coa_bank, c.v_bank",false);
// // WAH Penambahan a.e_remark
// $this->db->select("c.i_kbank, c.d_bank, c.i_coa_bank, c.v_bank as jum_bm, a.i_alokasi, b.d_alokasi, a.i_nota, a.v_jumlah as jum_alokasi, 
// a.v_sisa as jum_nota, (a.v_sisa - a.v_jumlah) as sisa, a.d_nota, b.d_entry, c.i_area, a.e_remark, d.i_customer
// from tm_kbank c 
// left join tm_alokasi b on (b.i_kbank=c.i_kbank and b.i_coa_bank=c.i_coa_bank and b.f_alokasi_cancel='f' and b.i_alokasi like '%-$iperiode-%')
// left join tm_alokasi_item a on (a.i_alokasi=b.i_alokasi and a.i_kbank=b.i_kbank and a.i_area=b.i_area)
// left join tm_nota d on(a.i_nota = d.i_nota and a.i_area = d.i_area and b.i_area = d.i_area)
// where c.i_kbank like '%-$iperiode-%' and c.f_kbank_cancel='f' and c.i_coa='110-41SM'
// group by c.i_kbank, c.i_coa_bank, c.v_bank,a.i_alokasi, a.i_nota, a.d_nota, a.v_jumlah, b.d_alokasi, a.v_sisa, b.d_entry, a.e_remark, d.i_customer
// order by c.i_kbank, c.i_coa_bank, c.v_bank",false);

// 		$query = $this->db->get();
// 		if ($query->num_rows() > 0){
// 			return $query->result();
// 		}
//     }
    function bacaperiode($iarea,$dfrom,$dto)
    {
      /*ORI DGU (24 MEI 2021) 
      if($iarea == 'NA'){
        $this->db->select("
        x.* from (
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
          a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name
          from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank f, tm_kbank g
          where a.i_customer=c.i_customer
          and a.i_area=b.i_area 
          and a.i_alokasi=d.i_alokasi 
          and a.i_area=d.i_area 
          and a.i_kbank=d.i_kbank 
          and d.i_nota=e.i_nota 
          and a.i_coa_bank=f.i_coa 
          and not e.i_nota isnull 
          and e.f_nota_cancel='f' 
          and a.f_alokasi_cancel='f' 
          and a.i_kbank = g.i_kbank
          and a.i_area = g.i_area
          and a.i_coa_bank = g.i_coa_bank
          and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
          AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
          union all
          select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
          a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name
          from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank_old f, tm_kbank g
          where a.i_customer=c.i_customer
          and a.i_area=b.i_area 
          and a.i_alokasi=d.i_alokasi 
          and a.i_area=d.i_area 
          and a.i_kbank=d.i_kbank 
          and d.i_nota=e.i_nota 
          and a.i_coa_bank=f.i_coa 
          and not e.i_nota isnull 
          and e.f_nota_cancel='f' 
          and a.f_alokasi_cancel='f' 
          and a.i_kbank = g.i_kbank
          and a.i_area = g.i_area
          and a.i_coa_bank = g.i_coa_bank
          and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
          AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
          ) as x
          ORDER BY x.e_customer_name, x.i_alokasi, x.i_nota
          ",false);
      }else{

        $this->db->select("
        x.* from(
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
          a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name
          from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank_old f, tm_kbank g
          where a.i_customer=c.i_customer
          and a.i_area=b.i_area 
          and a.i_alokasi=d.i_alokasi 
          and a.i_area=d.i_area 
          and a.i_kbank=d.i_kbank 
          and d.i_nota=e.i_nota 
          and a.i_coa_bank=f.i_coa 
          and not e.i_nota isnull 
          and e.f_nota_cancel='f' 
          and a.f_alokasi_cancel='f' 
          and a.i_kbank = g.i_kbank
          and a.i_area = g.i_area
          and a.i_coa_bank = g.i_coa_bank
          and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
          AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
          and a.i_area = '$iarea'
          union all
          select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
          a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name
          from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank f, tm_kbank g
          where a.i_customer=c.i_customer
          and a.i_area=b.i_area 
          and a.i_alokasi=d.i_alokasi 
          and a.i_area=d.i_area 
          and a.i_kbank=d.i_kbank 
          and d.i_nota=e.i_nota 
          and a.i_coa_bank=f.i_coa 
          and not e.i_nota isnull 
          and e.f_nota_cancel='f' 
          and a.f_alokasi_cancel='f' 
          and a.i_kbank = g.i_kbank
          and a.i_area = g.i_area
          and a.i_coa_bank = g.i_coa_bank
          and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
          AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
          and a.i_area = '$iarea'
          ) as x
          ORDER BY x.e_customer_name, x.i_alokasi, x.i_nota
          ",false);
      }

      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    } */
    /* COPY DARI PRS 24 MEI 2021 */
    if($iarea == 'NA'){
      $this->db->select("
      x.* from (
      select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and a.i_kbank=d.i_kbank 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        and a.i_area = g.i_area
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
        union all
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank_old f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and a.i_kbank=d.i_kbank 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        and a.i_area = g.i_area
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
        UNION ALL          
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, '' AS i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasihl a, tr_area b, tr_customer c, tm_alokasihl_item d, tm_nota e, tr_bank f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        /* and a.i_area = g.i_area */
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')  
        ) as x
        ORDER BY x.e_customer_name, x.i_alokasi, x.i_nota
        ",false);
    }else{

      $this->db->select("
      x.* from(
      select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank_old f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and a.i_kbank=d.i_kbank 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        and a.i_area = g.i_area
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
        and a.i_area = '$iarea'
        union all
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, a.i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasi a, tr_area b, tr_customer c, tm_alokasi_item d, tm_nota e, tr_bank f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and a.i_kbank=d.i_kbank 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        and a.i_area = g.i_area
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
        and a.i_area = '$iarea'
        UNION ALL          
        select a.i_kbank,b.i_area, g.d_bank, g.v_bank as jum_bm, a.i_alokasi, a.d_alokasi, e.i_nota, a.i_customer, e.d_nota, d.v_jumlah as jum_alokasi, d.v_sisa as jum_nota, (d.v_sisa - d.v_jumlah) as sisa,
        a.d_entry, d.e_remark, '' AS i_giro, f.e_bank_name, a.d_alokasi-d.d_nota as hari_bayar, c.e_customer_name, e.i_salesman,
        a.d_alokasi - (d.d_nota + c.n_customer_toplength*'1 day' :: interval)  as hari2
        from tm_alokasihl a, tr_area b, tr_customer c, tm_alokasihl_item d, tm_nota e, tr_bank f, tm_kbank g
        where a.i_customer=c.i_customer
        and a.i_area=b.i_area 
        and a.i_alokasi=d.i_alokasi 
        and a.i_area=d.i_area 
        and d.i_nota=e.i_nota 
        and a.i_coa_bank=f.i_coa 
        and not e.i_nota isnull 
        and e.f_nota_cancel='f' 
        and a.f_alokasi_cancel='f' 
        and a.i_kbank = g.i_kbank
        /* and a.i_area = g.i_area */
        and a.i_coa_bank = g.i_coa_bank
        and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
        AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')  
        and a.i_area = '$iarea'
        ) as x
        ORDER BY x.e_customer_name, x.i_alokasi, x.i_nota
        ",false);
    }
    $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
  }
?>
