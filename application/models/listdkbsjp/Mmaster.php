<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
   public function __construct()
   {
      parent::__construct();
      #$this->CI =& get_instance();
   }
   public function delete($idkb, $iarea)
   {
      $this->db->query("update tm_dkb_sjp set f_dkb_batal='t' WHERE i_dkb='$idkb' and i_area='$iarea'");
      $que = $this->db->query("select i_sjp, i_area from tm_dkb_sjp_item WHERE i_dkb='$idkb' and i_area='$iarea'");
      if ($que->num_rows() > 0) {
         foreach ($que->result() as $row) {
            $this->db->query("update tm_sjp set i_dkb=null, d_dkb=null WHERE i_sjp='$row->i_sjp' and i_area='$iarea'");
         }
      }
   }
   function bacasemua($cari, $num, $offset)
   {
      $this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
                       where a.i_area=b.i_area
                       and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                       or upper(a.i_dkb) like '%$cari%')
                       order by a.i_dkb desc", false)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
   function cari($cari, $num, $offset)
   {
      $this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
                                       where a.i_area=b.i_area
                                       and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                                       or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
                                       order by a.i_dkb desc", FALSE)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
   function bacaarea($num, $offset, $iuser)
   {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }

   function cariarea($cari, $num, $offset, $iuser)
   {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						       and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
   function bacaperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
   {
      if ($iarea == "NA") {
         $this->db->select("     a.i_dkb,
                                 a.d_dkb,
                                 b.e_area_name,
                                 a.e_sopir_name,
                                 a.i_kendaraan,
                                 sum(v_jumlah) AS jumlah,
                                 a.f_dkb_batal,
                                 a.i_area
                              FROM
                                 tr_area b,
                                 tm_dkb_sjp a,
                                 tm_dkb_sjp_item c
                              WHERE
                                 a.i_area = b.i_area
                                 AND a.i_dkb = c.i_dkb
                                 AND a.i_area = c.i_area
                                 AND b.i_area = c.i_area
                                 AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                 AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                              GROUP BY
                                 a.i_dkb,
                                 a.d_dkb,
                                 b.e_area_name,
                                 a.e_sopir_name,
                                 a.i_kendaraan,
                                 a.f_dkb_batal,
                                 a.i_area
                              ORDER BY
                                 a.i_dkb DESC ", false)->limit($num, $offset);
      } else {
         $this->db->select("     a.i_dkb,
                                 a.d_dkb,
                                 b.e_area_name,
                                 a.e_sopir_name,
                                 a.i_kendaraan,
                                 sum(v_jumlah) AS jumlah,
                                 a.f_dkb_batal,
                                 a.i_area
                              FROM
                                 tr_area b,
                                 tm_dkb_sjp a,
                                 tm_dkb_sjp_item c
                              WHERE
                                 a.i_area = b.i_area
                                 AND a.i_dkb = c.i_dkb
                                 AND a.i_area = c.i_area
                                 AND b.i_area = c.i_area
                                 AND a.i_area = '$iarea'
                                 AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                 AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                              GROUP BY
                                 a.i_dkb,
                                 a.d_dkb,
                                 b.e_area_name,
                                 a.e_sopir_name,
                                 a.i_kendaraan,
                                 a.f_dkb_batal,
                                 a.i_area
                              ORDER BY
                                 a.i_dkb DESC ", false)->limit($num, $offset);
      }

      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
   function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
   {
      $this->db->select("  a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
                     where a.i_area=b.i_area
                     and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
                     or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
                     and a.i_area='$iarea' and
                     a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
                     a.d_dkb <= to_date('$dto','dd-mm-yyyy')
                     order by a.i_dkb desc ", false)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
}
