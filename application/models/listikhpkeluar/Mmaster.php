<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iikhp, $iarea) 
    {
			$this->db->query("update tm_ikhp set f_ikhp_cancel='t' WHERE i_ikhp='$iikhp' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
							a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_ikhp) like '%$cari%')
							order by a.i_ikhp desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
					a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b
					where a.i_area=b.i_area 
					and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
					or upper(a.i_ikhp) like '%$cari%')
					order by a.i_ikhp desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    if($cari==''){
		  $this->db->select("	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai, a.i_cek,
								          a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name, c.e_ikhp_typename 
								          from tm_ikhp a, tr_area b, tr_ikhp_type c
							            where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							            and a.i_area='$iarea' and 
							            not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							            a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							            a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							            order by a.d_bukti desc, a.i_ikhp desc ",false)->limit($num,$offset);
    }else{
		  $this->db->select("	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai, a.i_cek,
								          a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name, c.e_ikhp_typename 
								          from tm_ikhp a, tr_area b, tr_ikhp_type c
							            where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							            and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%'
							            or a.v_keluar_tunai like '%$cari%' or a.v_keluar_giro like '%$cari%')
							            and a.i_area='$iarea' and
							            not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							            a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							            a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							            order by a.d_bukti desc, a.i_ikhp desc ",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		if($cari==''){
		  $this->db->select("	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai, a.i_cek,
								          a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name, c.e_ikhp_typename  
								          from tm_ikhp a, tr_area b, tr_ikhp_type c
							            where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							            and a.i_area='$iarea' and
  						            not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							            a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							            a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							            order by a.d_bukti desc, a.i_ikhp desc ",false)->limit($num,$offset);
    }else{
		  $this->db->select("	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai, a.i_cek,
								          a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name, c.e_ikhp_typename  
								          from tm_ikhp a, tr_area b, tr_ikhp_type c
							            where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							            and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%' 
							            or a.i_ikhp=$cari)
							            and a.i_area='$iarea' and
							            not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							            a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							            a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							            order by a.d_bukti desc, a.i_ikhp desc ",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
