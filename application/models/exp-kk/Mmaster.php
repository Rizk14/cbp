<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($datefrom,$dateto)
    {
		$this->db->select("	* from tm_kk 
							inner join tr_area on (tm_kk.i_area=tr_area.i_area)
							where tm_kk.d_kk >= '$datefrom' and tm_kk.d_kk <= '$dateto' 
							order by tm_kk.i_kk",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function bacasaldo($area,$periode,$tanggal)
    {
#		$this->db->select(" v_saldo_awal from tm_coa_saldo
#							where i_periode='$periode' and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.2' ",false);
#		$query = $this->db->get();
#		$saldo=0;
#		if ($query->num_rows() > 0){
#			foreach($query->result() as $row){
#				$saldo=$row->v_saldo_awal;
#			}
#		}
#####
	  $this->db->select(" v_saldo_awal from tm_coa_saldo
						  where i_periode='$periode' and substr(i_coa,7,2)='$area' and substr(i_coa,1,6)='".KasKecil."'",false);
	  $query = $this->db->get();
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $saldo=$row->v_saldo_awal;
		  }
	  }
	  $this->db->select(" sum(v_kk) as v_kk from tm_kk
						  where i_periode='$periode' and i_area='$area'
						  and d_kk<'$tanggal' and f_debet='t' and f_kk_cancel='f'",false);
	  $query = $this->db->get();
	  $kredit=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $kredit=$row->v_kk;
		  }
	  }
	  $this->db->select(" sum(v_kk) as v_kk from tm_kk
						  where i_periode='$periode' and i_area='$area'
						  and d_kk<'$tanggal' and f_debet='f' and f_kk_cancel='f'",false);
	  $query = $this->db->get();
	  $debet=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $debet=$row->v_kk;
		  }
	  }
	  $coaku=KasKecil.$area;
	  $this->db->select(" sum(v_bank) as v_bank from tm_kbank where i_periode='$periode' and i_area='$area' and d_bank<'$tanggal' 
	                      and f_debet='t' and f_kbank_cancel='f' and i_coa='$coaku'",false);							
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $debet=$debet+$row->v_bank;
		  }
	  }
	  $this->db->select(" sum(v_kb) as v_kb from tm_kb where i_periode='$periode' and i_area='$area' and d_kb<'$tanggal' 
	                      and f_debet='t' and f_kb_cancel='f' and i_coa='$coaku'",false);							
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $debet=$debet+$row->v_kb;
		  }
	  }
	  $saldo=$saldo+$debet-$kredit;
#####		
		return $saldo;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
