<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj,$iarea,$area1) 
    {
			$this->db->query(" update tm_nota set f_sj_cancel='t' WHERE i_sj='$isj' and i_sj_type='04' and i_area_from='$area1' and i_area_to='$iarea' ");
			return TRUE;
    }
    function bacasemua($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_from=b.i_area and a.i_sj_type='04'
								and (upper(a.i_area_from) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_from=b.i_area and a.i_sj_type='04'
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_from=b.i_area and a.i_sj_type='04'
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
       $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%') and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
							              where a.i_area=b.i_area
							              and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							              and substring(a.i_sj,9,2)='$iarea'
							              and a.d_sj >= to_date('$dfrom','dd-mm-yyyy')
							              and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							              order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      }else{
			  $this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
													  where a.i_area=b.i_area
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
													  and substring(a.i_sj,9,2)='$iarea' and
													  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isj,$iarea)
    {
		$this->db->select(" a.d_sj, a.d_spb, a.i_sj, a.i_area, a.i_spb, a.i_sj_old, a.v_nota_netto, 
                        b.e_area_name from tm_nota a, tr_area b
						   					where a.i_area=b.i_area 
						   					and a.i_sj ='$isj' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isj, $iarea)
    {
		$this->db->select(" a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname,
                        a.v_unit_price, a.n_deliver, d.n_order
                        from tm_nota_item a, tr_product_motif b, tm_nota c, tm_spb_item d
                        where a.i_sj = '$isj' and a.i_product=b.i_product 
                        and a.i_sj=c.i_sj and a.i_area=c.i_area
                        and c.i_spb=d.i_spb and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false); 
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumbersj($iarea,$thbl)
    {
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SJT'
                          and e_periode='$asal' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nosj
                            where i_modul='SJT'
                            and e_periode='$asal' 
                            and i_area='$iarea'", false);
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
			  	$nosj  ="SJT-".$thbl."-".$iarea.$nosj;
			  return $nosj;
		  }else{
			  $nosj  ="0001";
			  	$nosj  ="SJT-".$thbl."-".$iarea.$nosj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SJT','$iarea','$asal',1)");
			  return $nosj;
		  }
    }
    function insertsjtheader($isj,$dsj,$iarea,$isjt,$dsjt,$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1,
                             $vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto)
    {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjentry	= $row->c;
		$this->db->set(
		array(	  
    'i_sj'            		=> $isj,
	  'i_sjt'           		=> $isjt,
	  'd_sjt'           		=> $dsjt,
	  'd_sj'            		=> $dsj,
	  'i_area'		          => $iarea,
	  'n_nota_discount1'  	=> $nspbdiscount1,
	  'n_nota_discount2'  	=> $nspbdiscount2,
	  'n_nota_discount3'   	=> $nspbdiscount3,
	  'v_nota_discount1'  	=> $vspbdiscount1,
	  'v_nota_discount2'  	=> $vspbdiscount2,
	  'v_nota_discount3'  	=> $vspbdiscount3,
	  'v_nota_discounttotal'=> $vspbdiscounttotal,
	  'v_nota_gross'		    => $vspbgross,
	  'v_nota_netto'    		=> $vspbnetto,
	  'd_sjt_entry'      		=> $dsjentry,
	  'f_sjt_cancel'		    => 'f'
		)
  	);
  	$this->db->insert('tm_sjt');
    }
    function updatesjheader($isj,$iarea,
					                 	$nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1, 
							              $vspbdiscount2,$vspbdiscount3,$vspbdiscounttotal,$vspbgross,$vspbnetto)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
      $row   		= $query->row();
      $dsjupdate	= $row->c;
    	$this->db->set(
    		array(
			    'n_nota_discount1'=> $nspbdiscount1,
			    'n_nota_discount2'=> $nspbdiscount2,
     			'n_nota_discount3'=> $nspbdiscount3,
			    'v_nota_discount1'=> $vspbdiscount1,
     			'v_nota_discount2'=> $vspbdiscount2,
     			'v_nota_discount3'=> $vspbdiscount3,
     			'v_nota_discounttotal'	=> $vspbdiscounttotal,
			    'v_nota_gross'		=> $vspbgross,
			    'v_nota_netto'		=> $vspbnetto,
			    'd_nota_update'		=> $dsjupdate,
			    'f_nota_cancel'		=> 'f'
    		)
    	);

    	$this->db->where('i_sj',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_nota');
    }
}
?>
