<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($th,$prevth,$group)
    {
	    $this->db->select(" a.group , a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_customer_class, a.e_customer_classname, a.i_periode, a.group, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================

    -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev ,
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname
    union all

    --=========================================END OF FIRST YEAR==================================================
    --========================================START OF PREV YEAR==================================================

        -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev ,
    sum(c.n_deliver) as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    sum(d.n_deliver) as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa sebelumnya
    select a.i_periode , a.e_product_groupname as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname

    --============================================END OF THE PREV YEAR======================================

    ) as a
    group by a.i_periode, a.group, a.i_customer_class, a.e_customer_classname

    ) as a
    where a.group = '$group'
    group by a.group , a.i_customer_class, a.e_customer_classname
    order by a.group, a.e_customer_classname",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacagroup($th,$prevth)
    {
        $this->db->select("a.group from (select a.group , a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_customer_class, a.e_customer_classname, a.i_periode, a.group, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================

    -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev ,
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname
    union all

    --=========================================END OF FIRST YEAR==================================================
    --========================================START OF PREV YEAR==================================================

        -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev ,
    sum(c.n_deliver) as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    sum(d.n_deliver) as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa sebelumnya
    select a.i_periode , a.e_product_groupname as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname

    --============================================END OF THE PREV YEAR======================================

    ) as a
    group by a.i_periode, a.group, a.i_customer_class, a.e_customer_classname

    ) as a
    group by a.group , a.i_customer_class, a.e_customer_classname
    order by a.group) as a group by a.group
    order by a.group",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
     function bacaclass($th,$prevth)
    {
        $this->db->select("a.e_customer_classname from (select a.group , a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_customer_class, a.e_customer_classname, a.i_periode, a.group, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================

    -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev ,
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname
    union all

    --=========================================END OF FIRST YEAR==================================================
    --========================================START OF PREV YEAR==================================================

        -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev ,
    sum(c.n_deliver) as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    sum(d.n_deliver) as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa sebelumnya
    select a.i_periode , a.e_product_groupname as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname

    --============================================END OF THE PREV YEAR======================================

    ) as a
    group by a.i_periode, a.group, a.i_customer_class, a.e_customer_classname

    ) as a
    group by a.group , a.i_customer_class, a.e_customer_classname
    order by a.group) as a group by a.e_customer_classname
    order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacanas($th,$prevth)
    {
        $this->db->select("a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (select a.group , a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from (
    select a.i_customer_class, a.e_customer_classname, a.i_periode, a.group, sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , sum(qnotaprev) as qnotaprev from ( 

    --=================================START FIRST YEAR=================================================

    -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev ,
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 
    0 as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$th' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa 
    select a.i_periode , a.e_product_groupname as group , count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$th' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname
    union all

    --=========================================END OF FIRST YEAR==================================================
    --========================================START OF PREV YEAR==================================================

        -- Hitung Jum Rp.Nota MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
    and a.i_customer=c.i_customer
    and c.i_customer_class=d.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.i_customer_class, d.e_customer_classname
    union all
    
    -- Hitung Jum Rp.Nota Per Group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
    0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname
    Union all

    --hitung qty MO
    SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev ,
    sum(c.n_deliver) as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname
    union all

    --hitung qty group
    select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group,0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
    sum(d.n_deliver) as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
    where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
    and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
    and a.i_sj=d.i_sj and a.i_area=d.i_area
    and a.i_customer=e.i_customer
    and e.i_customer_class=f.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, e.i_customer_class, f.e_customer_classname
    union all

    --hitung oa mo
    select a.i_periode , 'modern Outlet' as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
    a.i_customer_class, a.e_customer_classname  from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname 
    from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), d.i_customer_class, e.e_customer_classname, a.i_customer, c.e_product_groupname
    ) as a
    group by a.i_periode, a.i_customer_class, a.e_customer_classname 
    union all

    --hitung oa sebelumnya
    select a.i_periode , a.e_product_groupname as group , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
    a.i_customer_class, a.e_customer_classname from (
    select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , c.e_product_groupname,
    d.i_customer_class, e.e_customer_classname from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
    where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
    and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='false'
    and b.i_product_group=c.i_product_group
    and a.i_customer=d.i_customer
    and d.i_customer_class=e.i_customer_class
    group by to_char(a.d_nota,'yyyy'), c.e_product_groupname, d.i_customer_class, e.e_customer_classname, a.i_customer
    ) as a
    group by a.i_periode , a.e_product_groupname, a.i_customer_class, a.e_customer_classname

    --============================================END OF THE PREV YEAR======================================

    ) as a
    group by a.i_periode, a.group, a.i_customer_class, a.e_customer_classname

    ) as a
    group by a.group , a.i_customer_class, a.e_customer_classname
    order by a.group) as a 
    group by a.i_customer_class, a.e_customer_classname
    order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
}
?>
