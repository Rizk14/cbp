<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto,$group)
    {
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $thbl=$thnow.$bl;
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tehabeel=$thnow."-".$bl;
        $tsasih = date('Y-m', strtotime('-24 month', strtotime($tehabeel))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
        $smn = explode("-", $tsasih);
        $thn = $smn[0];
        $bln = $smn[1];
        }
        $taunsasih = $hr."-".$bln."-".$thn;

        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnya=$tmp[2];
        $thblto=$thnya.$bl;
        $dtoprev=$hr."-".$bl."-".$th;

        if ($group=="NA") {
            $this->db->select(" a.i_customer_class, a.e_customer_classname,sum(ob) as ob, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , 
                            sum(qnotaprev) as qnotaprev from (select * from f_sales_report_klasifikasi_new('$dfrom','$dto','$dfromprev','$dtoprev','$taunsasih')
                            group by grup , i_customer_class, e_customer_classname, i_product_group,ob, oa, vnota, qnota, oaprev, qnotaprev, vnotaprev
                            order by grup) as a 
                            group by a.i_customer_class, a.e_customer_classname
                            order by a.e_customer_classname",false);            
        }else{
	        $this->db->select(" a.i_customer_class, a.e_customer_classname,sum(ob) as ob, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , 
sum(qnotaprev) as qnotaprev, a.i_product_group as group from (
select a.i_customer_class, a.e_customer_classname, a.group as grup, a.i_product_group, sum(ob) as ob , sum(oa) as oa ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(oaprev) as oaprev ,sum(vnotaprev) as vnotaprev , 
sum(qnotaprev) as qnotaprev from ( 

--=================================Outlet Binaan=================================================
select a.e_product_groupname as group ,a.i_product_group, count(a.ob) as ob ,0 as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
0 as qnotaprev ,a.i_customer_class, a.e_customer_classname from (
SELECT distinct on(a.ob) * from (
SELECT distinct on(b.i_customer) b.i_customer as ob, a.i_area, c.e_area_name ,e.i_customer_class, e.e_customer_classname, f.e_product_groupname, 
f.i_product_group
from tm_nota b, tm_spb a , tr_area c, tr_customer d, tr_customer_class e, tr_product_group f
where b.d_nota>=to_date('$taunsasih','dd-mm-yyyy') and b.d_nota<=to_date('$dto','dd-mm-yyyy') and a.i_spb=b.i_spb and a.i_area=b.i_area
and b.f_nota_cancel='f' and a.i_customer = d.i_customer and d.i_customer_class = e.i_customer_class
and a.i_product_group = f.i_product_group and a.i_area=c.i_area and not b.i_nota isnull
group by b.i_customer, a.i_area, c.e_area_name,e.i_customer_class, 
e.e_customer_classname,f.e_product_groupname, f.i_product_group
union all
select b.i_customer as ob, b.i_area, c.e_area_name ,e.i_customer_class, e.e_customer_classname, 'Dialogue Baby Bedding' as e_product_groupname, 
'01' as i_product_group
from tr_customer b, tr_area c, tr_customer_class e
where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
and b.i_customer_class = e.i_customer_class
)as a
)as a
group by a.e_product_groupname,a.i_product_group,  a.i_customer_class, a.e_customer_classname
Union All
--=================================START FIRST YEAR=================================================

-- Hitung Jum Rp.Nota MO
SELECT 'modern Outlet' as group, 'MO' as i_product_group,  0 as ob , 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
where a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota<=to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
and a.i_customer=c.i_customer
and c.i_customer_class=d.i_customer_class
group by c.i_customer_class, d.e_customer_classname
union all

-- Hitung Jum Rp.Nota Per Group
select c.e_product_groupname as group, c.i_product_group, 0 as ob , 0 as oa ,sum(a.v_nota_netto)  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
where a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota<=to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
group by c.e_product_groupname, c.i_product_group, d.i_customer_class, e.e_customer_classname
Union all

--hitung qty MO
SELECT 'modern Outlet' as group, 'MO' as i_product_group,0 as ob , 0 as oa , 0  as vnota, sum(c.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev ,
0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
where a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota<=to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
group by d.i_customer_class, e.e_customer_classname
union all

--hitung qty group
select c.e_product_groupname as group, c.i_product_group,0 as ob , 0 as oa , 0  as vnota, sum(d.n_deliver) as qnota ,0 as oaprev , 0 as vnotaprev , 
0 as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
where a.d_nota>=to_date('$dfrom','dd-mm-yyyy') and a.d_nota<=to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
and a.i_sj=d.i_sj and a.i_area=d.i_area and a.i_customer=e.i_customer and e.i_customer_class=f.i_customer_class
group by c.e_product_groupname, c.i_product_group, e.i_customer_class, f.e_customer_classname
union all

--hitung oa mo
select 'modern Outlet' as group , 'MO' as i_product_group, 0 as ob ,count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
a.i_customer_class, a.e_customer_classname  from (
select distinct on (a.i_customer, to_char(a.d_nota,'yyyymm')) a.i_customer as oa , c.e_product_groupname, d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm') as periode
from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e where a.d_nota>=to_date('$dfrom','dd-mm-yyyy')
and a.d_nota<=to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='false'
and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
and d.i_customer_status<>'4' and d.f_customer_aktif='true'
group by d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm'), a.i_customer, c.e_product_groupname
) as a
group by a.i_customer_class, a.e_customer_classname, a.periode
union all

--hitung oa 
select a.e_product_groupname as group ,a.i_product_group, 0 as ob ,count(a.oa) as oa ,0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
a.i_customer_class, a.e_customer_classname from (
select distinct on (a.i_customer, to_char(a.d_nota,'yyyymm')) a.i_customer as oa , c.e_product_groupname, c.i_product_group, 
d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm') as periode from tm_nota a , tm_spb b , tr_product_group c , 
tr_customer d , tr_customer_class e 
where a.d_nota>=to_date('$dfrom','dd-mm-yyyy') 
and a.d_nota<=to_date('$dto','dd-mm-yyyy') 
and a.f_nota_cancel='false'
and not a.i_nota isnull 
and a.i_area=b.i_area 
and a.i_nota=b.i_nota 
and b.f_spb_cancel='false' 
and b.f_spb_consigment='false'
and b.i_product_group=c.i_product_group 
and a.i_customer=d.i_customer 
and d.i_customer_class=e.i_customer_class
and d.i_customer_status<>'4' 
and d.f_customer_aktif='true'
and c.i_product_group = '$group'
group by c.e_product_groupname,c.i_product_group, to_char(a.d_nota,'yyyymm'),  d.i_customer_class, e.e_customer_classname, a.i_customer
) as a
group by a.e_product_groupname,a.i_product_group,  a.i_customer_class, a.e_customer_classname, a.periode
union all

--=========================================END OF FIRST YEAR==================================================
--========================================START OF PREV YEAR==================================================

-- Hitung Jum Rp.Nota MO
SELECT 'modern Outlet' as group, 'MO' as i_product_group, 0 as ob , 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
0 as qnotaprev, c.i_customer_class, d.e_customer_classname from tm_nota a, tm_spb b, tr_customer c, tr_customer_class d
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
and a.i_customer=c.i_customer
and c.i_customer_class=d.i_customer_class
group by c.i_customer_class, d.e_customer_classname
union all

-- Hitung Jum Rp.Nota Per Group
select c.e_product_groupname as group, c.i_product_group, 0 as ob , 0 as oa ,0 as vnota, 0 as qnota ,0 as oaprev , sum(a.v_nota_netto) as vnotaprev , 
0 as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tr_customer d, tr_customer_class e
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
group by c.e_product_groupname, c.i_product_group, d.i_customer_class, e.e_customer_classname
Union all

--hitung qty MO
SELECT 'modern Outlet' as group, 'MO' as i_product_group,0 as ob , 0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev ,
sum(c.n_deliver) as qnotaprev, d.i_customer_class, e.e_customer_classname from tm_nota a, tm_spb b, tm_nota_item c, tr_customer d, tr_customer_class e
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
group by d.i_customer_class, e.e_customer_classname
union all

--hitung qty group
select c.e_product_groupname as group, c.i_product_group,0 as ob , 0 as oa , 0  as vnota, 0 as qnota ,0 as oaprev , 0 as vnotaprev , 
sum(d.n_deliver) as qnotaprev, e.i_customer_class, f.e_customer_classname from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d, tr_customer e, tr_customer_class f
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
and a.i_sj=d.i_sj and a.i_area=d.i_area and a.i_customer=e.i_customer and e.i_customer_class=f.i_customer_class
group by c.e_product_groupname,c.i_product_group, e.i_customer_class, f.e_customer_classname
union all

--hitung oa mo
select 'modern Outlet' as group , 'MO' as i_product_group, 0 as ob , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev, 
a.i_customer_class, a.e_customer_classname  from (
select distinct on (a.i_customer, to_char(a.d_nota,'yyyymm')) a.i_customer as oa , c.e_product_groupname, d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm') as periode 
from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , tr_customer_class e 
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') and a.f_nota_cancel='false'
and not a.i_nota isnull and a.i_area=b.i_area and a.i_nota=b.i_nota and b.f_spb_cancel='false' and b.f_spb_consigment='true'
and a.i_customer=d.i_customer
and d.i_customer_class=e.i_customer_class
and d.i_customer_status<>'4' and d.f_customer_aktif='true'
group by d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm'), a.i_customer, c.e_product_groupname
) as a
group by a.i_customer_class, a.e_customer_classname, a.periode
union all

--hitung oa sebelumnya
select a.e_product_groupname as group ,a.i_product_group, 0 as ob , 0 as oa ,0  as vnota, 0 as qnota ,count(a.oa) as oaprev , 0 as vnotaprev , 0 as qnotaprev ,
a.i_customer_class, a.e_customer_classname from (
select distinct on (a.i_customer, to_char(a.d_nota,'yyyymm')) a.i_customer as oa , c.e_product_groupname, c.i_product_group,
d.i_customer_class, e.e_customer_classname, to_char(a.d_nota,'yyyymm') as periode from tm_nota a , tm_spb b , tr_product_group c , tr_customer d , 
tr_customer_class e 
where a.d_nota>=to_date('$dfromprev','dd-mm-yyyy') 
and a.d_nota<=to_date('$dtoprev','dd-mm-yyyy') 
and a.f_nota_cancel='false'
and not a.i_nota isnull 
and a.i_area=b.i_area 
and a.i_nota=b.i_nota 
and b.f_spb_cancel='false' 
and b.f_spb_consigment='false'
and b.i_product_group=c.i_product_group 
and a.i_customer=d.i_customer 
and d.i_customer_class=e.i_customer_class
and d.i_customer_status<>'4' 
and d.f_customer_aktif='true'
and c.i_product_group = '$group'
group by c.e_product_groupname, c.i_product_group, to_char(a.d_nota,'yyyymm'), d.i_customer_class, e.e_customer_classname, a.i_customer
) as a
group by a.e_product_groupname, a.i_product_group, a.periode, a.i_customer_class, a.e_customer_classname

--============================================END OF THE PREV YEAR======================================

) as a
group by a.group, a.i_product_group, a.i_customer_class, a.e_customer_classname
) as a
where a.i_product_group = '$group'
group by a.i_customer_class, a.e_customer_classname, a.i_product_group
order by a.e_customer_classname",false);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          return $query->result();
        }

    }
    function bacaob($dfrom,$dto,$group)
    { 
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $thbl=$thnow."-".$bl;
        $dfromprev=$hr."-".$bl."-".$th;
        $tsasih = date('Y-m', strtotime('-24 month', strtotime($thbl))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
        $smn = explode("-", $tsasih);
        $thn = $smn[0];
        $bln = $smn[1];
        }
        $taunsasih = $thn.$bln;

        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnya=$tmp[2];
        $thblto=$thnya.$bl;
        $dtoprev=$hr."-".$bl."-".$th;


      /*$this->db->select(" count(a.ob) as ob from (
                          SELECT distinct i_customer as ob, e_periode,i_salesman
                          from tr_customer_salesman
                          where e_periode ='$thblto'
                          ) as a",false);*/
      $this->db->select(" count(ob) as ob from (
 select distinct on (a.ob) a.ob as ob, a.i_area, a.e_area_name ,a.e_area_island , a.e_provinsi from (
 select a.i_customer as ob, a.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
                                  from tm_nota a , tr_area c
                                  where to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                                  and a.f_nota_cancel='false' and a.i_area=c.i_area and c.f_area_real='t' and not a.i_nota isnull
 union all
 select b.i_customer as ob, b.i_area, c.e_area_name ,c.e_area_island , c.e_provinsi 
 from tr_customer b, tr_area c
 where b.i_customer_status<>'4' and b.f_customer_aktif='true' and b.i_area=c.i_area and c.f_area_real='t'
 ) as a 
 ) as a
                        ",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacagroup($th,$prevth,$todate,$prevdate)
    {
        $this->db->select("a.group from (select * from f_sales_report_klasifikasi('$th','$prevth','$todate','$prevdate') 
                           order by a.grup) as a 
                           group by a.grup  
                           order by a.grup",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
     function bacaclass($dfrom,$dto)
    {
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $dtoprev=$hr."-".$bl."-".$th;
        $this->db->select("a.e_customer_classname from (select * from f_sales_report_klasifikasi_new('$dfrom','$dto','$dfromprev','$dtoprev') 
                            order by grup) as a 
                            group by a.e_customer_classname
                            order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacanas($th,$prevth,$todate,$prevdate)
    {
        $this->db->select(" a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , 
                            sum(qnotaprev) as qnotaprev from (select * from f_sales_report_klasifikasi('$th','$prevth','$todate','$prevdate')
                            group by grup , i_customer_class, e_customer_classname,i_product_group,ob,oa,vnota,qnota,oaprev,vnotaprev,qnotaprev
                            order by grup) as a 
                            group by a.i_customer_class, a.e_customer_classname
                            order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacaproductgroup()
    {
      $this->db->select(" * from tr_product_group",false);
    
      $query = $this->db->get();
    
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
