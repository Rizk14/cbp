<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($djt,$num,$offset)
    {
      $this->db->select(" a.i_area, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.i_sj, a.v_sisa as v_sisa, a.v_nota_netto as v_nota_netto, 
                          a.i_customer, b.e_customer_name, a.f_insentif, a.f_masalah, a.e_remark
                          from tm_nota a, tr_customer b, tr_customer_groupar c
				                  where a.i_customer=b.i_customer and a.i_customer=c.i_customer
				                  and a.f_ttb_tolak='f'
                          and not a.i_nota isnull
				                  and a.v_sisa>0 and a.f_nota_cancel='f'
				                  ORDER BY a.i_customer, a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($icustomergroupar,$djt,$num,$offset)
    {
      $this->db->select(" a.i_customer, a.v_sisa, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.v_nota_netto, a.n_nota_toplength
                          from tm_nota a, tr_customer b, tr_customer_groupar c
					                where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
					                and a.f_ttb_tolak='f' 
                          and not a.i_nota isnull
				                  and c.i_customer_groupar='$icustomergroupar' and a.v_sisa>0 
                          and a.f_nota_cancel='f'
                          order by a.i_nota",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
