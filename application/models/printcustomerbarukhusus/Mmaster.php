<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$cari,$num,$offset,$dfrom,$dto)
    {
    if($iarea=='NA'){
		$this->db->select(" a.*, b.d_spb from tr_customer_tmp a, tm_spb b
							where (upper(a.i_customer) like '%000' and (upper(a.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%') and a.i_spb=b.i_spb and a.i_area=b.i_area
							and b.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and b.d_spb <= to_date('$dto','dd-mm-yyyy'))
							order by a.i_spb desc",false)->limit($num,$offset);
    }else{
		$this->db->select(" a.*, b.d_spb from tr_customer_tmp a, tm_spb b
							where (upper(a.i_customer) like '%000' or upper(a.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%') and a.i_spb=b.i_spb and a.i_area=b.i_area
							and b.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
							and b.d_spb <= to_date('$dto','dd-mm-yyyy')
							and a.i_area='$iarea' order by a.i_spb desc",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacalang($ispb,$area)
    {
		  $this->db->select(" a.*, b.e_area_name, c.e_customer_classname, d.e_paymentmethod, e.n_spb_discount1, e.n_spb_discount2
                          	,e.n_spb_discount3, e.n_spb_discount4, e.i_price_group, f.e_salesman_name
                          	from tr_area b, tr_customer_tmp a
                          	inner join tr_customer_class c on (c.i_customer_class=a.i_customer_class)
                          	inner join tr_paymentmethod d on(d.i_paymentmethod=a.i_paymentmethod)
                          	inner join tm_spb e on(a.i_spb=e.i_spb and a.i_area=e.i_area)
                          	inner join tr_salesman f on(a.i_salesman=f.i_salesman)
                          	where a.i_spb = '$ispb' and a.i_area='$area' and a.i_area=b.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ispb,$area)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, c.*, d.*, e.*, f.* 
					              from tm_spb a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e, tr_customer_groupar f
					              where a.i_spb = '$ispb' and a.i_area='$area' 
					              and a.i_customer=b.i_customer and a.i_customer=f.i_customer
					              and a.i_salesman=c.i_salesman
					              and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
					              and b.i_customer_class=d.i_customer_class
					              order by a.i_spb desc",false);
#and (a.n_print=0 or a.n_print isnull)
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ispb,$area)
    {
		$this->db->select(" 	* from tm_spb_item
					inner join tr_product on (tm_spb_item.i_product=tr_product.i_product)
					inner join tr_product_motif on (tm_spb_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_spb_item.i_product=tr_product_motif.i_product)
					where i_spb = '$ispb' and i_area='$area' order by n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$iuser,$iarea,$num,$offset)
    {
		if($iarea=='NA'){
			$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
								where a.i_customer=b.i_customer
								and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
								or upper(a.i_spb) like '%$cari%')
								order by a.i_spb desc",FALSE)->limit($num,$offset);
		}else{
			$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
								where a.i_customer=b.i_customer
								and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
								or upper(a.i_spb) like '%$cari%')
								and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
								order by a.i_spb desc",FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$area1,$iuser)
    {
		if($area1=='00'/* or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'*/){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$area1,$iuser)
    {
		if($area1=='00'/* or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'*/){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area in(select i_area from tm_user_area where i_area='$iarea')) order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($area,$ispb)
    {
		$this->db->query("	update tm_spb set n_print=n_print+1 
          							where i_spb = '$ispb' and i_area = '$area' ",false);
    }
}
?>
