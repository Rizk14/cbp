<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ittb,$iarea,$tahun,$inota) 
    {
/*
		$this->db->query("DELETE FROM tm_ttbtolak WHERE i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun");
		$this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
		$this->db->query("DELETE FROM tm_bbm WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
		$this->db->query("DELETE FROM tm_ttbtolak_item WHERE i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun");
		$this->db->query("update tm_nota set f_ttb_tolak='f' WHERE i_nota='$inota' ",False);
*/
		$this->db->query("update tm_ttbtolak set f_ttb_cancel='t' WHERE i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun");
//		$this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
//		$this->db->query("DELETE FROM tm_bbm WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
//		$this->db->query("DELETE FROM tm_ttbtolak_item WHERE i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun");
		$this->db->query("update tm_nota set f_ttb_tolak='f' WHERE i_nota='$inota' ",False);
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.f_nota_koreksi 
							from tm_ttbtolak a, tr_customer b, tr_area c, tm_nota d
							where a.i_nota=d.i_nota and a.i_area=c.i_area and a.i_area=d.i_area
							and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, d.e_area_name, c.f_nota_koreksi
							from tm_ttbtolak a, tr_customer b, tm_nota c, tr_area d
							where a.i_nota=c.i_nota and a.i_customer=b.i_customer 
							and a.i_area=d.i_area and a.i_area=c.i_area
							and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
        $sql = "* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area";
      $this->db->select($sql, FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
      $sql = "* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' and 
              i_area in ( select i_area from tm_user_area where i_user='$iuser') )";
      $this->db->select($sql, FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name, c.e_area_name, d.f_nota_koreksi 
							from tm_ttbtolak a, tr_customer b, tr_area c, tm_nota d
							where a.i_nota=d.i_nota and a.i_area=c.i_area and a.i_area=d.i_area
							and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_ttb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name, c.e_area_name, d.f_nota_koreksi 
							from tm_ttbtolak a, tr_customer b, tr_area c, tm_nota d
							where a.i_nota=d.i_nota and a.i_area=c.i_area and a.i_area=d.i_area
							and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_ttb desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
