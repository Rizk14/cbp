<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bacaperiode($iuser, $num, $offset, $cari)
    {
        $query = $this->db->select("    a.*, b.e_area_name, c.f_spmb_consigment, c.f_retur, dkb.i_approve1, a.i_approve as approvefa
                                        from tm_sjp a
                                        left join tm_dkb_sjp dkb on a.i_dkb = dkb.i_dkb and a.i_area = dkb.i_area, tr_area b, tm_spmb c
                                        where a.i_area=b.i_area and a.i_spmb=c.i_spmb
                                        and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
                                        and (a.i_area in (select i_area from tm_user_area where i_user = '$iuser' ))
                                        and a.i_approve is null and a.f_sjp_cancel='f' 
                                        AND (NOT a.i_dkb ISNULL AND a.i_dkb <> '')
                                        ORDER BY a.i_sjp desc ", false)->limit($num, $offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function baca($isjp, $iarea)
    {
        $this->db->select(" distinct(c.i_store), c.i_store_location, a.*, b.e_area_name, s1.n_tolerance, s2.f_retur 
                            from tm_sjp a, tr_area b, tm_sjp_item c, tm_dkb_sjp d1, tm_send_tolerance s1, tm_spmb s2
                            where 
                            a.i_area=b.i_area and a.i_sjp=c.i_sjp
                            and a.i_area=c.i_area and a.f_sjp_cancel='f'
                            AND a.i_dkb = d1.i_dkb AND d1.i_dkb_via = s1.i_send_type AND d1.i_area = s1.i_area 
                            AND a.i_spmb = s2.i_spmb AND a.i_area = s2.i_area 
                            and a.i_sjp ='$isjp' and a.i_area='$iarea' ", false);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }

    public function bacadetail($isjp, $iarea, $ispmb)
    {
        $this->db->select(" a.i_sjp,a.d_sjp,a.i_area,a.n_quantity_order,
                            a.i_product,a.i_product_grade,a.i_product_motif,a.n_quantity_receive,
                            a.n_quantity_deliver,a.v_unit_price,a.e_product_name,a.i_store,
                            a.i_store_location,a.i_store_locationbin,a.e_remark,
                            b.e_product_motifname from tm_sjp_item a, tr_product_motif b
                            where a.i_sjp = '$isjp' and a.i_area='$iarea'
                            and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                            order by a.n_item_no", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {

            $total = 0;
            foreach ($query->result() as $row) {
                $total = $total + ($row->n_quantity_deliver * $row->v_unit_price);
            }

            $this->db->query("update tm_sjp set v_sjp = '$total' where i_sjp = '$isjp' and i_area = '$iarea'");

            return $query->result();
        }
    }

    public function cari($cari, $num, $offset)
    {
        $iuser  = $this->session->userdata('user_id');
        $this->db->select(" a.*, b.e_area_name, c.f_spmb_consigment, c.f_retur, dkb.i_approve1, a.i_approve as approvefa
                            from tm_sjp a
                            left join tm_dkb_sjp dkb on a.i_dkb = dkb.i_dkb and a.i_area = dkb.i_area, tr_area b, tm_spmb c
                            where a.i_area=b.i_area and a.i_spmb=c.i_spmb
                            and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
                            and (a.i_area in (select i_area from tm_user_area where i_user = '$iuser' ))
                            and a.i_approve is null and a.f_sjp_cancel='f' 
                            AND (NOT a.i_dkb ISNULL AND a.i_dkb <> '')
                            ORDER BY a.i_sjp desc   ", false)->limit($num, $offset);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function approve($isj, $iarea, $dapprovefa)
    {
        $iuser      = $this->session->userdata('user_id');

        // $query      = $this->db->query("SELECT current_timestamp as c");
        // $row        = $query->row();
        // $dapprove   = $row->c;

        $this->db->set(
            array(
                'i_approve' => $iuser,
                'd_approve' => $dapprovefa,
            )
        );
        $this->db->where('i_sjp', $isj);
        $this->db->update('tm_sjp');
    }
}
