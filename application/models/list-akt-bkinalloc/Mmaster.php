<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }
  public function delete($ialokasi, $ikbank, $iarea)
  {
    $this->db->query("update tm_alokasi set f_alokasi_cancel='t' WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");

    //* PENAMBAHAN 07 JUN 2021
    $query   = $this->db->query("SELECT current_timestamp as c");
    $row     = $query->row();
    $ddelete = $row->c;

    #####UnPosting
    $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$ialokasi' and i_area='$iarea'");
    $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$ialokasi' and i_area='$iarea'");
    $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$ialokasi' and i_area='$iarea'");

    $quer   = $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$ialokasi' and i_area='$iarea'");
    if ($quer->num_rows() > 0) {
      foreach ($quer->result() as $xx) {
        $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
      }
    }
    $this->db->query("delete from tm_jurnal_transharian where i_refference='$ialokasi' and i_area='$iarea'");
    $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$ialokasi' and i_area='$iarea'");
    $this->db->query("delete from tm_general_ledger where i_refference='$ialokasi' and i_area='$iarea'");
    #$this->db->query("delete from tm_alokasi_item where i_alokasi='$ialokasi' and i_area='$iarea'");
    #$this->db->query("delete from tm_alokasikn_item where i_alokasi='$ialokasi' and i_area='$iarea'");

    #####
    $quer   = $this->db->query("SELECT i_nota, v_jumlah, i_coa_bank, v_materai from tm_alokasi_item WHERE i_kbank='$ikbank' and i_alokasi='$ialokasi' and i_area='$iarea'");
    if ($quer->num_rows() > 0) {
      foreach ($quer->result() as $xx) {
        //! QUERY LAMA SEBELUM DIGABUNG ALOKASI METERAI (07 JUN 2021)
        // $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah WHERE i_nota='$xx->i_nota'");
        // $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");

        //* PENAMBAHAN & UPDATE 07 JUN 2021
        $this->db->query("UPDATE tm_nota set v_sisa=v_sisa+$xx->v_jumlah, v_materai_sisa=v_materai_sisa+$xx->v_materai WHERE i_nota='$xx->i_nota'");
        $this->db->query("UPDATE tm_kbank set v_sisa=v_sisa+$xx->v_jumlah+$xx->v_materai WHERE i_kbank='$ikbank' and i_coa_bank='$xx->i_coa_bank'");
        $this->db->query("UPDATE tm_alokasimt SET f_alokasi_cancel = 't', d_update = '$ddelete' WHERE i_alokasi_reff = '$ialokasi' and i_area='$iarea' ");
      }
    }
  }
  function bacaarea($num, $offset, $iuser)
  {
    $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }

  function cariarea($cari, $num, $offset, $iuser)
  {
    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacaperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
  {
    if ($iarea == "NA") {
      $this->db->select("	a.i_alokasi, a.i_kbank, a.i_area, b.e_area_name, a.d_alokasi, a.i_customer, c.e_customer_name, 
                            a.v_jumlah, a.v_lebih, a.f_alokasi_cancel, a.i_coa_bank
                            from tm_alokasi a, tr_area b, tr_customer c
                            where (upper(a.i_alokasi) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' or upper(a.i_kbank) like '%$cari%') and a.i_customer=c.i_customer
                            and a.i_area=b.i_area and
                            a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
                            ORDER BY a.i_kbank, a.i_alokasi ", false)->limit($num, $offset);
    } else {
      $this->db->select("	a.i_alokasi, a.i_kbank, a.i_area, b.e_area_name, a.d_alokasi, a.i_customer, c.e_customer_name, 
                            a.v_jumlah, a.v_lebih, a.f_alokasi_cancel, a.i_coa_bank
                            from tm_alokasi a, tr_area b, tr_customer c
                            where (upper(a.i_alokasi) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' or upper(a.i_kbank) like '%$cari%') and a.i_customer=c.i_customer
                            and a.i_area=b.i_area and a.i_area='$iarea' and
                            a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_alokasi <= to_date('$dto','dd-mm-yyyy')
                            ORDER BY a.i_kbank, a.i_alokasi ", false)->limit($num, $offset);
    }

    #and a.f_alokasi_cancel='f'
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
  {
    $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek,
							a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							where (upper(a.i_kb) like '%$cari%')
							and a.i_area=b.i_area and a.f_kb_cancel='f'
							and a.i_area='$iarea' and
							a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kb <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_kb ", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
