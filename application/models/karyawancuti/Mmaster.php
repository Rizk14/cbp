<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($i_nik,$th_skrng)
    {
		$this->db->select(" * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_area e on(a.i_area=e.i_area)
			   left join tr_karyawan_cuti f on(a.i_nik=f.i_nik)
			   where a.i_nik='$i_nik' and f.i_periode='$th_skrng'
			", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($i_nik,$th_skrng)
    {
		$this->db->select(" * from tr_karyawan_cuti_item where i_nik='$i_nik' and i_periode='$th_skrng'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($i_nik, $i_department, $i_area, $i_karyawan_status, 
					$d_kontrak_ke1_awal, $d_kontrak_ke1_akhir, $d_kontrak_ke2_awal, $d_kontrak_ke2_akhir, 
					$d_kontrak_ke3_awal, $d_kontrak_ke3_akhir, $d_join_date, $e_nomor_bpjs_ketenagakerjaan,
					$e_nomor_bpjs_kesehatan, $e_nomor_npwp, $e_sim_a,$e_sim_b,$e_sim_c,$e_karyawan_jabatan,$e_karyawan_penempatan,
					$e_karyawan_daerah,$v_sisa_cuti)
    {
    	$query  = $this->db->query("SELECT current_timestamp as c");
    	$row    = $query->row();
    	$dentry = $row->c;
    	
		    	$this->db->set(
		    		array(
						'i_nik'								=> $i_nik,
						'i_department'	 					=> $i_department,
						'i_area'							=> $i_area,
						'i_karyawan_status'					=> $i_karyawan_status,
						'd_kontrak_ke1_awal'				=> $d_kontrak_ke1_awal,
						'd_kontrak_ke1_akhir'				=> $d_kontrak_ke1_akhir,
						'd_kontrak_ke2_awal'				=> $d_kontrak_ke2_awal,
						'd_kontrak_ke2_akhir'	 			=> $d_kontrak_ke2_akhir,
						'd_kontrak_ke3_awal'				=> $d_kontrak_ke3_awal,
						'd_kontrak_ke3_akhir'				=> $d_kontrak_ke3_akhir,
						'd_join_date'						=> $d_join_date,
						'e_nomor_bpjs_ketenagakerjaan'		=> $e_nomor_bpjs_ketenagakerjaan,
						'e_nomor_bpjs_kesehatan'	 		=> $e_nomor_bpjs_kesehatan,
						'e_nomor_npwp'						=> $e_nomor_npwp,
						'e_sim_a'							=> $e_sim_a,
						'e_sim_b'							=> $e_sim_b,
						'e_sim_c'							=> $e_sim_c,
						'e_karyawan_jabatan'	 			=> $e_karyawan_jabatan,
						'e_karyawan_penempatan'				=> $e_karyawan_penempatan,
						'e_karyawan_daerah'					=> $e_karyawan_daerah,
						'v_sisa_cuti'						=> $v_sisa_cuti,
						'd_entry'							=> $dentry
		    			)
    				);
    	
    	$this->db->insert('tr_karyawan');
    }
    function insertdetail($i_nik, $e_nama_karyawan_lengkap, $e_nama_karyawan_panggilan, $e_jenis_kelamin, 
					$e_tempat_lahir, $d_lahir, $e_agama, $e_no_kk, 
					$e_no_ktp, $e_alamat_ktp, $e_alamat_sekarang, $e_jurusan,
					$e_asal_universitas_sekolah, $e_pendidikan_terakhir, $e_status_pernikahan,$e_nama_pasangan,$e_alamat_pasangan,$e_nomor_telepon)
    {
    	$query  = $this->db->query("SELECT current_timestamp as c");
    	$row    = $query->row();
    	$d_entry_karyawan_item = $row->c;

			    	$this->db->set(
			    		array(
							'i_nik'								=> $i_nik,
							'e_nama_karyawan_lengkap'	 		=> $e_nama_karyawan_lengkap,
							'e_nama_karyawan_panggilan'			=> $e_nama_karyawan_panggilan,
							'e_jenis_kelamin'					=> $e_jenis_kelamin,
							'e_tempat_lahir'					=> $e_tempat_lahir,
							'd_lahir'							=> $d_lahir,
							'e_agama'	 						=> $e_agama,
							'e_no_kk'							=> $e_no_kk,
							'e_no_ktp'							=> $e_no_ktp,
							'e_alamat_ktp'						=> $e_alamat_ktp,
							'e_alamat_sekarang'					=> $e_alamat_sekarang,
							'e_jurusan'	 						=> $e_jurusan,
							'e_asal_universitas_sekolah'		=> $e_asal_universitas_sekolah,
							'e_pendidikan_terakhir'				=> $e_pendidikan_terakhir,
							'e_status_pernikahan'				=> $e_status_pernikahan,
							'e_nama_pasangan'					=> $e_nama_pasangan,
							'e_alamat_pasangan'	 				=> $e_alamat_pasangan,
							'e_nomor_telepon'					=> $e_nomor_telepon,
							'd_entry_karyawan_item'				=> $d_entry_karyawan_item
			    			)
			    		);
    	
    	$this->db->insert('tr_karyawan_item');
    }
    function updateheader(
		    $i_nik,$v_cuti_lebaran,$v_saldo_cuti,$th_skrng
  		   )
    {
    	$query  = $this->db->query("SELECT current_timestamp as c");
    	$row    = $query->row();
    	$d_update_entry = $row->c;
		 
      	$data = array(
						'v_cuti_lebaran'				=> $v_cuti_lebaran,
						'v_saldo_cuti'					=> $v_saldo_cuti
              );

		  $this->db->where('i_nik', $i_nik);
		  $this->db->where('i_periode', $th_skrng);
		  $this->db->update('tr_karyawan_cuti', $data); 
    }
    function updatedetail(
		   						$i_nik,$d_cuti_awal,$d_cuti_akhir,$e_alasan,$v_jumlah_cuti,$th_skrng
  		   				 )
    {

    	$query  = $this->db->query("SELECT current_timestamp as c");
    	$row    = $query->row();
    	$d_update_entry_item = $row->c;
		 
      	$data = array(
							'd_cuti_awal'	 		=> $d_cuti_awal,
							'd_cuti_akhir'			=> $d_cuti_akhir,
							'e_alasan'				=> $e_alasan,
							'v_jumlah_cuti'			=> $v_jumlah_cuti
              );

		  $this->db->where('i_nik', $i_nik);
		  $this->db->where('i_periode', $th_skrng);
		  $this->db->update('tr_karyawan_cuti_item', $data); 
    }
	
    function delete($iproduct) 
    {
		  $this->db->query("DELETE FROM tr_product WHERE i_product='$iproduct'", false);
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_karyawan_cuti e on(a.i_nik=e.i_nik) 
			   where e.i_periode='2016' and (upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%') order by a.i_nik asc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_karyawan_cuti e on(a.i_nik=e.i_nik)
			   where e.i_periode='2016' and (upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%') order by a.i_nik asc", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caridepartment($cari,$num,$offset)
    {
		$query = $this->db->get();
		$this->db->select(" * from tr_department 
				   where upper(e_department_name) like '%$cari%' or upper(i_department) like '%$cari%'", FALSE)->limit($num,$offset);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadepartment($num,$offset)
    {
		$this->db->select(" * from tr_department order by i_department",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariaarea($cari,$num,$offset)
    {
		$query = $this->db->get();
		$this->db->select(" * from tr_area 
				   where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%'", FALSE)->limit($num,$offset);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset)
    {
		$this->db->select(" * from tr_area order by i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carikaryawanstatus($cari,$num,$offset)
    {
		$query = $this->db->get();
		$this->db->select(" * from tr_karyawan_status 
				   where upper(e_karyawan_status) like '%$cari%' or upper(i_karyawan_status) like '%$cari%'", FALSE)->limit($num,$offset);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacakaryawanstatus($num,$offset)
    {
		$this->db->select(" * from tr_karyawan_status order by i_karyawan_status",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jumlahcutiambil($i_nik,$th_skrng)
    {
    	$this->db->select("sum(v_jumlah_cuti) as jumlah_cuti from tr_karyawan_cuti_item where i_nik='$i_nik' and i_periode='$th_skrng'", false);
    	$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}

    }

     function ceksaldocuti($i_nik)
    {
		$this->db->select(" distinct * from tr_karyawan_cuti where i_nik='$i_nik'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0)
        { return $query->row_array();
        }
        else {return NULL;}
    }

    function simpancutidetail($i_nik,$d_cuti_awal,$d_cuti_akhir,$v_jumlah_cuti,$e_alasan,$periode)
    {
    	$this->db->set(
    		array(

    			'i_nik'					=> $i_nik,
				'd_cuti_awal'			=> $d_cuti_awal,
				'd_cuti_akhir'			=> $d_cuti_akhir,
				'v_jumlah_cuti'			=> $v_jumlah_cuti,
				'e_alasan'				=> $e_alasan,
				'i_periode'				=> $periode
    		)
    	);
    	
    	$this->db->insert('tr_karyawan_cuti_item');
    }

    function updatekaryawancuti($i_nik,$v_saldo_akhir)
    {
    	$this->db->set(
    		array(
				
				'v_saldo_cuti'	=> $v_saldo_akhir
    		)
    	);
    	
    	$this->db->where("i_nik",$i_nik);
    	$this->db->update('tr_karyawan_cuti');
    }
}
?>
