<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function insert_xlang($nodok,$tgldok,$wila,$nolang,$nama,$alamat,$kota,$kodepos,$nmmilik,$almmilik
                         ,$npwp,$telepon,$fax,$telepon2,$jenis,$jenis2,$discount,$bayar,$pkp,$pkp2
                         ,$kodesales,$tgldaftar,$cperson,$email,$adamemo,$memo,$nonpwpbaru,$plusppn
                         ,$plusdisc,$npwpbaru)
    {
      $mem=$this->db->escape($memo);
      $mem=$this->db->escape_str($mem);
      $kata=$mem;
      for($i=128;$i<=255;$i++){
        $teks=strstr($mem,chr($i));
        if($teks!=''){
          $tmp=explode(chr($i),$mem);
          $kata='';
          for($j=0;$j<count($tmp);$j++){
            $kata=$kata.$tmp[$j];
          }
        }
      }
      $kata=str_replace('"\"','',$kata);
#      echo 'kata = '.$kata.'<br>';
#      $mem=str_replace('�', '', $kata);
#      echo 'mem  = '.$mem.'<br>';
      $mem=$kata;
 		  $this->db->select(" nodok from tt_xlang where nodok='$nodok' and tgldok='$tgldok' and wila='$wila'", false);
		  $query = $this->db->get();
		  if($query->num_rows==0){
	      $this->db->query("insert into tt_xlang
                          values
                         ('$nodok','$tgldok','$wila','$nolang','$nama','$alamat','$kota','$kodepos',
                          '$nmmilik','$almmilik','$npwp','$telepon','$fax','$telepon2','$jenis','$jenis2',
                          '$discount','$bayar','$pkp','$pkp2','$kodesales','$tgldaftar','$cperson','$email',
                          '$adamemo','$mem','$nonpwpbaru','$plusppn','$plusdisc','$npwpbaru')");
      }
    }

    function insertheader( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						               $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						               $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                           $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						               $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						               $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                           $eremarkx,$iproductgroup)
    {
		    $query 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $query->row();
		    $dentry	= $row->c;
        	$this->db->set(
        		array(
			    'i_spb'		=> $ispb,
			    'd_spb'		=> $dspb,
			    'i_customer'	=> $kodelang,
			    'i_area'	=> $iarea,
			    'i_spb_po'	=> $po,
			    'n_spb_toplength'=> $top,
			    'i_salesman' 	=> $isalesman,
			    'i_price_group' => $ipricegroup,
			    'd_spb_receive'	=> $dspb,
			    'f_spb_op'	=> $fspbop,
			    'e_customer_pkpnpwp'	=> $npwp,
			    'f_spb_pkp'		=> $pkp,
			    'f_spb_plusppn'		=> $plusppn,
			    'f_spb_plusdiscount'	=> $plusdisc,
			    'f_spb_stockdaerah'	=> $fspbstockdaerah,
			    'f_spb_program' 	=> $fspbprogram,
			    'f_spb_valid' 		=> $fspbvalid,
			    'f_spb_siapnotagudang'	=> $fspbsiapnotagudang,
			    'f_spb_cancel' 		=> $fspbcancel,
			    'n_spb_discount1' 	=> $nspbdiscount1,
			    'n_spb_discount2' 	=> $nspbdiscount2,
			    'n_spb_discount3' 	=> $nspbdiscount3,
			    'v_spb_discount1' 	=> $vspbdiscount1,
			    'v_spb_discount2' 	=> $vspbdiscount2,
			    'v_spb_discount3' 	=> $vspbdiscount3,
			    'v_spb_discounttotal'	=> $vspbdiscounttotal,
			    'v_spb' 		=> $vspb,
			    'f_spb_consigment' 	=> $fspbconsigment,
			    'd_spb_entry'		=> $dentry,
			    'i_spb_old'		=> $ispbold,
			    'i_product_group'	=> $iproductgroup,
			    'e_remark1'		=> $eremarkx
        		)
        	);
        	
        	$this->db->insert('tm_spb');
#      }
    }
    function insertheaderpromo( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						               $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						               $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                           $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						               $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						               $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                           $eremarkx,$iproductgroup,$ipromo)
    {
		    $query 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $query->row();
		    $dentry	= $row->c;
        	$this->db->set(
        		array(
			    'i_spb'		              => $ispb,
			    'd_spb'		              => $dspb,
			    'i_customer'	          => $kodelang,
			    'i_area'	              => $iarea,
			    'i_spb_po'	            => $po,
			    'n_spb_toplength'       => $top,
			    'i_salesman' 	          => $isalesman,
			    'i_price_group'         => $ipricegroup,
			    'd_spb_receive'	        => $dspb,
			    'f_spb_op'	            => $fspbop,
			    'e_customer_pkpnpwp'	  => $npwp,
			    'f_spb_pkp'		          => $pkp,
			    'f_spb_plusppn'		      => $plusppn,
			    'f_spb_plusdiscount'	  => $plusdisc,
			    'f_spb_stockdaerah'	    => $fspbstockdaerah,
			    'f_spb_program'        	=> $fspbprogram,
			    'f_spb_valid'        		=> $fspbvalid,
			    'f_spb_siapnotagudang'	=> $fspbsiapnotagudang,
			    'f_spb_cancel' 		      => $fspbcancel,
			    'n_spb_discount1' 	    => $nspbdiscount1,
			    'n_spb_discount2' 	    => $nspbdiscount2,
			    'n_spb_discount3' 	    => $nspbdiscount3,
			    'v_spb_discount1' 	    => $vspbdiscount1,
			    'v_spb_discount2' 	    => $vspbdiscount2,
			    'v_spb_discount3'      	=> $vspbdiscount3,
			    'v_spb_discounttotal'	  => $vspbdiscounttotal,
			    'v_spb' 		            => $vspb,
			    'f_spb_consigment' 	    => $fspbconsigment,
			    'd_spb_entry'		        => $dentry,
			    'i_spb_old'		          => $ispbold,
			    'i_product_group'	      => $iproductgroup,
			    'e_remark1'		          => $eremarkx,
          'i_spb_program'         => $ipromo
        		)
        	);
        	
        	$this->db->insert('tm_spb');
    }
    function insertheaderkons( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						                   $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						                   $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                               $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						                   $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						                   $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                               $eremarkx,$iproductgroup)
    {
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $dentry	= $row->c;
      if($iarea=='PB'){
        $istore='PB';
        $istorelocation='00';
      }else{
        $istore=$iarea;
        $istorelocation='PB';
      }
      	$this->db->set(
      		array(
		    'i_spb'		            => $ispb,
		    'd_spb'		            => $dspb,
		    'i_customer'	        => $kodelang,
		    'i_area'	            => $iarea,
		    'i_spb_po'	          => $po,
		    'n_spb_toplength'     => $top,
		    'i_salesman' 	        => $isalesman,
		    'i_price_group'       => $ipricegroup,
		    'd_spb_receive'	      => $dspb,
		    'f_spb_op'	          => $fspbop,
		    'e_customer_pkpnpwp'	=> $npwp,
		    'f_spb_pkp'		        => $pkp,
		    'f_spb_plusppn'		    => $plusppn,
		    'f_spb_plusdiscount'	=> $plusdisc,
		    'f_spb_stockdaerah'	  => $fspbstockdaerah,
		    'f_spb_program' 	    => $fspbprogram,
		    'f_spb_valid' 		    => $fspbvalid,
		    'f_spb_siapnotagudang'=> $fspbsiapnotagudang,
		    'f_spb_cancel' 		    => $fspbcancel,
		    'n_spb_discount1' 	  => $nspbdiscount1,
		    'n_spb_discount2' 	  => $nspbdiscount2,
		    'n_spb_discount3' 	  => $nspbdiscount3,
		    'v_spb_discount1' 	  => $vspbdiscount1,
		    'v_spb_discount2' 	  => $vspbdiscount2,
		    'v_spb_discount3' 	  => $vspbdiscount3,
		    'v_spb_discounttotal'	=> $vspbdiscounttotal,
		    'v_spb' 		          => $vspb,
		    'f_spb_consigment' 	  => $fspbconsigment,
		    'd_spb_entry'		      => $dentry,
		    'i_spb_old'		        => $ispbold,
		    'i_product_group'	    => $iproductgroup,
		    'e_remark1'		        => $eremarkx,
        'i_store'             => $istore,
        'i_store_location'    => $istorelocation,
        'i_approve1'          => 'SYSTEM',
        'i_approve2'          => 'SYSTEM',
        'i_cek'               => 'SYSTEM',
        'f_spb_siapnotasales' => 't'
      		)
      	);
      	$this->db->insert('tm_spb');
    }
    function updateheader( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						               $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						               $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                           $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						               $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						               $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                           $eremarkx,$iproductgroup)
    {
    		$query 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $query->row();
		    $dupdate= $row->c;
        	$this->db->set(
        		array(
			    'i_spb'		            => $ispb,
			    'd_spb'		            => $dspb,
			    'i_customer'	        => $kodelang,
			    'i_area'	            => $iarea,
			    'i_spb_po'	          => $po,
			    'n_spb_toplength'     => $top,
			    'i_salesman' 	        => $isalesman,
			    'i_price_group'       => $ipricegroup,
			    'd_spb_receive'	      => $dspb,
			    'f_spb_op'	          => $fspbop,
			    'e_customer_pkpnpwp'	=> $npwp,
			    'f_spb_pkp'		        => $pkp,
			    'f_spb_plusppn'		    => $plusppn,
			    'f_spb_plusdiscount'	=> $plusdisc,
			    'f_spb_stockdaerah'	  => $fspbstockdaerah,
			    'f_spb_program' 	    => $fspbprogram,
			    'f_spb_valid' 		    => $fspbvalid,
			    'f_spb_siapnotagudang'=> $fspbsiapnotagudang,
			    'f_spb_cancel' 		    => $fspbcancel,
			    'n_spb_discount1' 	  => $nspbdiscount1,
			    'n_spb_discount2' 	  => $nspbdiscount2,
			    'n_spb_discount3'   	=> $nspbdiscount3,
			    'v_spb_discount1' 	  => $vspbdiscount1,
			    'v_spb_discount2'    	=> $vspbdiscount2,
			    'v_spb_discount3' 	  => $vspbdiscount3,
			    'v_spb_discounttotal'	=> $vspbdiscounttotal,
			    'v_spb' 		          => $vspb,
			    'f_spb_consigment' 	  => $fspbconsigment,
			    'd_spb_entry'		      => $dupdate,
			    'i_spb_old'		        => $ispbold,
			    'i_product_group'	    => $iproductgroup,
			    'e_remark1'		        => $eremarkx
        		)
        	);
        	
		  $this->db->where('i_spb',$ispb);
		  $this->db->where('i_area', $iarea);
    	$this->db->update('tm_spb');
    }
    function updateheaderpromo( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						               $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						               $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                           $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						               $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						               $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                           $eremarkx,$iproductgroup,$ipromo)
    {
    		$query 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $query->row();
		    $dupdate= $row->c;
        	$this->db->set(
        		array(
			    'i_spb'		=> $ispb,
			    'd_spb'		=> $dspb,
			    'i_customer'	=> $kodelang,
			    'i_area'	=> $iarea,
			    'i_spb_po'	=> $po,
			    'n_spb_toplength'=> $top,
			    'i_salesman' 	=> $isalesman,
			    'i_price_group' => $ipricegroup,
			    'd_spb_receive'	=> $dspb,
			    'f_spb_op'	=> $fspbop,
			    'e_customer_pkpnpwp'	=> $npwp,
			    'f_spb_pkp'		=> $pkp,
			    'f_spb_plusppn'		=> $plusppn,
			    'f_spb_plusdiscount'	=> $plusdisc,
			    'f_spb_stockdaerah'	=> $fspbstockdaerah,
			    'f_spb_program' 	=> $fspbprogram,
			    'f_spb_valid' 		=> $fspbvalid,
			    'f_spb_siapnotagudang'	=> $fspbsiapnotagudang,
			    'f_spb_cancel' 		=> $fspbcancel,
			    'n_spb_discount1' 	=> $nspbdiscount1,
			    'n_spb_discount2' 	=> $nspbdiscount2,
			    'n_spb_discount3' 	=> $nspbdiscount3,
			    'v_spb_discount1' 	=> $vspbdiscount1,
			    'v_spb_discount2' 	=> $vspbdiscount2,
			    'v_spb_discount3' 	=> $vspbdiscount3,
			    'v_spb_discounttotal'	=> $vspbdiscounttotal,
			    'v_spb' 		=> $vspb,
			    'f_spb_consigment' 	=> $fspbconsigment,
			    'd_spb_entry'		=> $dupdate,
			    'i_spb_old'		=> $ispbold,
			    'i_product_group'	=> $iproductgroup,
			    'e_remark1'		=> $eremarkx,
          'i_spb_program'         => $ipromo
        		)
        	);
        	
		  $this->db->where('i_spb',$ispb);
		  $this->db->where('i_area', $iarea);
    	$this->db->update('tm_spb');
    }
    function updateheaderkons( $ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
						               $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
						               $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                           $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						               $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						               $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                           $eremarkx,$iproductgroup)
    {
    		$query 	= $this->db->query("SELECT current_timestamp as c");
		    $row   	= $query->row();
		    $dupdate= $row->c;
        	$this->db->set(
        		array(
			    'i_spb'		=> $ispb,
			    'd_spb'		=> $dspb,
			    'i_customer'	=> $kodelang,
			    'i_area'	=> $iarea,
			    'i_spb_po'	=> $po,
			    'n_spb_toplength'=> $top,
			    'i_salesman' 	=> $isalesman,
			    'i_price_group' => $ipricegroup,
			    'd_spb_receive'	=> $dspb,
			    'f_spb_op'	=> $fspbop,
			    'e_customer_pkpnpwp'	=> $npwp,
			    'f_spb_pkp'		=> $pkp,
			    'f_spb_plusppn'		=> $plusppn,
			    'f_spb_plusdiscount'	=> $plusdisc,
			    'f_spb_stockdaerah'	=> $fspbstockdaerah,
			    'f_spb_program' 	=> $fspbprogram,
#			    'f_spb_valid' 		=> $fspbvalid,
#			    'f_spb_siapnotagudang'	=> $fspbsiapnotagudang,
			    'f_spb_cancel' 		=> $fspbcancel,
			    'n_spb_discount1' 	=> $nspbdiscount1,
			    'n_spb_discount2' 	=> $nspbdiscount2,
			    'n_spb_discount3' 	=> $nspbdiscount3,
			    'v_spb_discount1' 	=> $vspbdiscount1,
			    'v_spb_discount2' 	=> $vspbdiscount2,
			    'v_spb_discount3' 	=> $vspbdiscount3,
			    'v_spb_discounttotal'	=> $vspbdiscounttotal,
			    'v_spb' 		=> $vspb,
			    'f_spb_consigment' 	=> $fspbconsigment,
			    'd_spb_entry'		=> $dupdate,
			    'i_spb_old'		=> $ispbold,
			    'i_product_group'	=> $iproductgroup,
			    'e_remark1'		=> $eremarkx
        		)
        	);
        	
		  $this->db->where('i_spb',$ispb);
		  $this->db->where('i_area', $iarea);
    	$this->db->update('tm_spb');
    }
  function insertdetail($ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,$vunitprice,
                        $iproductmotif,$eremark,$i)
  {
    if($i<=1){
      $this->db->query(" delete from tm_spb_item 
                         where i_spb='$ispb' and i_area='$iarea'", false);
    }
#    $que=$this->db->query(" select i_spb from tm_spb_item 
#                            where i_spb='$ispb' and i_area='$iarea' and i_product='$iproduct' and i_product_motif='00'", false);
#    if($que->num_rows()>0){
#      $que=$this->db->query(" select i_spb from tm_spb_item 
#                              where i_spb='$ispb' and i_area='$iarea' and i_product='$iproduct' and i_product_motif='01'", false);
#      if($que->num_rows()>0){
#        $iproductmotif='02';
#      }else{
#        $iproductmotif='01';
#      }




#    }
  	if($eremark=='') $eremark=null;
  	$this->db->set(
  		array(
				'i_spb'						=> $ispb,
				'i_area'      		=> $iarea,
				'i_product'   		=> $iproduct,
				'i_product_status'=> $iproductstatus,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'n_order'     		=> $norder,
#				'n_deliver'   		=> $ndeliver,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
				'e_remark'				=> $eremark,
				'n_item_no'       => $i
  		)
  	);
  	$this->db->insert('tm_spb_item');
  }

	function cekadaspb($ispb,$iarea)
	{
		$que=$this->db->query("select * from tm_spb where i_spb='$ispb' and i_area='$iarea'", false);
		return $que->num_rows();
	}

  function runningnumber($iarea,$thbl,$nodok){
    $ada=false;
    for($i=0;$i<strlen($nodok);$i++){
      $teks=strtoupper(substr($nodok,$i,1));
      if($teks=='A'||$teks=='B'||$teks=='C'||$teks=='D'||$teks=='E'||$teks=='F'||$teks=='G'||$teks=='H'||$teks=='I'||
         $teks=='J'||$teks=='K'||$teks=='L'||$teks=='M'||$teks=='N'||$teks=='O'||$teks=='P'||$teks=='Q'||$teks=='R'||
         $teks=='S'||$teks=='T'||$teks=='U'||$teks=='V'||$teks=='W'||$teks=='X'||$teks=='Y'||$teks=='Z'){
        $ada=true;
        break;
      }
    }
    if(!$ada){
      $nospb=intval($nodok);
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,4);
	    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPB'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $row){
		      $terakhir=$row->max;
		    }
        if($nospb<=$terakhir){
		      settype($nospb,"string");
		      $a=strlen($nospb);
		      while($a<6){
		        $nospb="0".$nospb;
		        $a=strlen($nospb);
		      }
		      $nospb  ="SPB-".$thbl."-".$nospb;
        }else{

          $this->db->query(" update tm_dgu_no 
                              set n_modul_no=$nodok
                              where i_modul='SPB'
                              and substr(e_periode,1,4)='$th' 
                              and i_area='$iarea'", false);

          $nospb  ="SPB-".$thbl."-".$nodok;
        }
		    return $nospb;
	    }else{
		    $nospb  =$nodok;
		    $nospb  ="SPB-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPB','$iarea','$asal',1)");
		    return $nospb;
	    }
    }else{
      $nospb = $nodok;
      $th	   = substr($thbl,0,4);
      $asal  = $thbl;
      $thbl  = substr($thbl,2,4);
      $nospb = "SPB-".$thbl."-".$nospb;
      return $nospb;
	  }
	}
  function insert_lang(
									 $nodok,$wila,$nolang,$nama,$alamat,$kota,$kodepos,
                   $nmmilik,$almmilik,$npwp,$telepon,$fax,$telepon2,$jenis,
                   $jenis2,$discount,$bayar,$pkp,$pkp2,$kodesales,$tgldaftar,
                   $cperson,$email,$adamemo,$memo,$nonpwpbaru,$plusppn,$plusdisc,
                   $npwpbaru,$thbl,$ipricegroup
	   							)
  {
    settype($nodok,"string");
    $a=strlen($nodok);
    while($a<6){
      $nodok="0".$nodok;
      $a=strlen($nodok);
    }
    $ispb	=$this->mmaster->runningnumber($wila,$thbl,$nodok);
	  $que=$this->db->query("select i_spb from tr_customer_tmp 
                           where i_spb='$ispb' and i_area='$wila'", false);
    if($que->num_rows()==0){
	    $query = $this->db->query("SELECT current_timestamp as c");
	    $row   = $query->row();
	    $dentry= $row->c;
      $nolang= $wila.$nolang;
      $kata=$memo;
      for($i=128;$i<=255;$i++){
        $teks=strstr($memo,chr($i));
        if($teks!=''){
          $tmp=explode(chr($i),$memo);
          $kata='';
          for($j=0;$j<count($tmp);$j++){
            $kata=$kata.$tmp[$j];
          }
        }
      }
      $kata=str_replace('"\"','',$kata);
      $memo=$kata;
      if($nonpwpbaru!=''){
	      $this->db->query("insert into tr_customer_tmp 
                          (i_spb, i_area, i_customer, e_customer_name, e_customer_address, 
                           e_customer_kota1, e_postal1, e_customer_owner, e_customer_owneraddress,
                           e_customer_pkpnpwp, 
                           e_customer_phone, e_fax1, e_customer_phone2, i_customer_class, n_customer_discount,
                           n_spb_toplength, f_spb_pkp, i_salesman, d_customer_entry, e_customer_contact,
                           e_customer_mail,
                           e_customer_refference, f_customer_plusppn, f_customer_plusdiscount,i_price_group)
										      values
										      ('$ispb','$wila','$nolang','$nama','$alamat','$kota','$kodepos',
                           '$nmmilik','$almmilik','$nonpwpbaru','$telepon','$fax','$telepon2','$jenis',
                            $discount,$bayar,'$pkp','$kodesales','$tgldaftar',
                           '$cperson','$email','$memo','$plusppn','$plusdisc','$ipricegroup')");
      }else{
	      $this->db->query("insert into tr_customer_tmp 
                          (i_spb, i_area, i_customer, e_customer_name, e_customer_address, 
                           e_customer_kota1, e_postal1, e_customer_owner, e_customer_owneraddress, e_customer_pkpnpwp,
                           e_customer_phone, e_fax1, e_customer_phone2, i_customer_class, n_customer_discount,
                           n_spb_toplength, f_spb_pkp, i_salesman, d_customer_entry, e_customer_contact,
                           e_customer_mail,
                           e_customer_refference, f_customer_plusppn, f_customer_plusdiscount,i_price_group)
										      values
										      ('$ispb','$wila','$nolang','$nama','$alamat','$kota','$kodepos',
                           '$nmmilik','$almmilik','$npwp','$telepon','$fax','$telepon2','$jenis',
                            $discount,$bayar,'$pkp','$kodesales','$tgldaftar',
                           '$cperson','$email','$memo','$plusppn','$plusdisc','$ipricegroup')");
      }    
    }
  }
}
?>
