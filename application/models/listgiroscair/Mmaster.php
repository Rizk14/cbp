<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    public function delete($igiro,$iarea)
    {
         $this->db->query("update tm_giro set f_giro_batal='t' WHERE i_giro='$igiro' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" * from tm_giro a
                     inner join tr_area on(a.i_area=tr_area.i_area)
                     inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                     where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                     order by a.i_area,a.i_giro", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($cari,$num,$offset)
    {
      $this->db->select(" * from tm_giro a
                     inner join tr_area on(a.i_area=tr_area.i_area)
                     inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                     where upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                     order by a.i_area,a.i_giro", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
    if($cari!=''){
      $quer= "distinct on (a.d_giro, a.i_giro) a.i_giro, tr_area.e_area_name,
              a.d_giro, a.d_giro_cair,
              a.i_rv,
              a.d_rv,
              tm_dt.i_dt,
              tm_dt.d_dt,
              tr_customer.i_customer,
              tr_customer.e_customer_name,
              a.e_giro_bank,
              a.v_jumlah,
              a.v_sisa,
              a.f_posting,
              a.f_giro_batal,
              a.i_area, tm_pelunasan.i_pelunasan from tm_giro a
                 inner join tr_area on(a.i_area=tr_area.i_area)
                 inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                 left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area 
                                       and a.v_jumlah=tm_pelunasan.v_jumlah)
                 left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=tm_pelunasan.i_area)
                 where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%' ";
        if(is_numeric($cari)){
          $quer.=" or a.v_jumlah=$cari) ";
        }else{
          $quer.=" ) ";
        }
        $quer.=" and 
                 ((tm_pelunasan.i_jenis_bayar!='02' and
                 tm_pelunasan.i_jenis_bayar!='03' and
                 tm_pelunasan.i_jenis_bayar!='04' and
                 tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and
                 a.i_area='$iarea' and
                 (a.d_giro_cair >= to_date('$dfrom','dd-mm-yyyy') and
                 a.d_giro_cair <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='t'
                 order by a.d_giro desc, a.i_giro desc";
        $this->db->select($quer,false)->limit($num,$offset);
      }else{
      $this->db->select(" distinct on (a.d_giro, a.i_giro) a.i_giro, tr_area.e_area_name,
                  a.d_giro, a.d_giro_cair,
                  a.i_rv,
                  a.d_rv,
                  tm_dt.i_dt,
                  tm_dt.d_dt,
                  tr_customer.i_customer,
                  tr_customer.e_customer_name,
                  a.e_giro_bank,
                  a.v_jumlah,
                  a.v_sisa,
                  a.f_posting,
                  a.f_giro_batal,
                  a.i_area, tm_pelunasan.i_pelunasan from tm_giro a
                     inner join tr_area on(a.i_area=tr_area.i_area)
                     inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                     left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area 
                                           and a.v_jumlah=tm_pelunasan.v_jumlah)
                     left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=tm_pelunasan.i_area)
                     where ((tm_pelunasan.i_jenis_bayar!='02' and
                     tm_pelunasan.i_jenis_bayar!='03' and
                     tm_pelunasan.i_jenis_bayar!='04' and
                     tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and
                     a.i_area='$iarea' and
                     (a.d_giro_cair >= to_date('$dfrom','dd-mm-yyyy') and
                     a.d_giro_cair <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='t'
                     order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($cari!=''){
        $this->db->select(" distinct a.i_giro, tr_area.e_area_name,
                    a.d_giro, a.d_giro_cair,
                    a.i_rv,
                    a.d_rv,
                    tm_dt.i_dt,
                    tm_dt.d_dt,
                    tr_customer.i_customer,
                    tr_customer.e_customer_name,
                    a.e_giro_bank,
                    a.v_jumlah,
                    a.v_sisa,
                    a.f_posting,
                    a.f_giro_batal,
                    a.i_area, tm_pelunasan.i_pelunasan from tm_giro a
                       inner join tr_area on(a.i_area=tr_area.i_area)
                       inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                       left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area and a.v_jumlah=tm_pelunasan.v_jumlah)
                       left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=tm_pelunasan.i_area)

                       where ( upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.v_jumlah=$cari) and
                       ((tm_pelunasan.i_jenis_bayar!='02' and
                       tm_pelunasan.i_jenis_bayar!='03' and
                       tm_pelunasan.i_jenis_bayar!='04' and
                       tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and
                       a.i_area='$iarea' and
                       (a.d_giro_cair >= to_date('$dfrom','dd-mm-yyyy') and
                       a.d_giro_cair <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='t'
                       order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }else{
        $this->db->select(" distinct a.i_giro, tr_area.e_area_name,
                      a.d_giro, a.d_giro_cair,
                      a.i_rv,
                      a.d_rv,
                      tm_dt.i_dt,
                      tm_dt.d_dt,
                      tr_customer.i_customer,
                      tr_customer.e_customer_name,
                      a.e_giro_bank,
                      a.v_jumlah,
                      a.v_sisa,
                      a.f_posting,
                      a.f_giro_batal,
                      a.i_area, tm_pelunasan.i_pelunasan from tm_giro a
                         inner join tr_area on(a.i_area=tr_area.i_area)
                         inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                         left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro and a.i_area=tm_pelunasan.i_area and a.v_jumlah=tm_pelunasan.v_jumlah)
                         left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_dt.i_area=tm_pelunasan.i_area)

                         where ( upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') and
                         ((tm_pelunasan.i_jenis_bayar!='02' and
                         tm_pelunasan.i_jenis_bayar!='03' and
                         tm_pelunasan.i_jenis_bayar!='04' and
                         tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and
                         a.i_area='$iarea' and
                         (a.d_giro_cair >= to_date('$dfrom','dd-mm-yyyy') and
                         a.d_giro_cair <= to_date('$dto','dd-mm-yyyy')) and a.f_giro_cair='t'
                         order by a.d_giro desc, a.i_giro desc",false)->limit($num,$offset);
      }      
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacagiro($igiro,$iarea)
    {
      $this->db->select(" * from tm_giro a
                     inner join tr_area on(a.i_area=tr_area.i_area)
                     inner join tr_customer on(a.i_customer=tr_customer.i_customer)
                     where a.i_giro='$igiro' and a.i_area='$iarea'",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }

    function bacadetailpl($iarea,$ipl,$idt){
      $this->db->select(" a.*, b.v_nota_netto as v_nota from tm_pelunasan_item a
                inner join tm_nota b on (a.i_nota=b.i_nota and a.i_area=b.i_area)

               where a.i_pelunasan = '$ipl'
               and a.i_area='$iarea'
               and a.i_dt='$idt'
               order by a.i_pelunasan,a.i_area ",FALSE);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
