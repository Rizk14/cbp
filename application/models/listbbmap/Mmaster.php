<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iap,$isupplier,$iop) 
    {
			$this->db->query("update tm_ap set f_ap_cancel='t' WHERE i_ap='$iap' and i_supplier='$isupplier'");
			$this->db->query("update tm_op set f_op_close='f' WHERE i_op='$iop' and i_supplier='G0000' ",False);
######
		  $iap=trim($iap);
		  $this->db->select(" e_product_name, d_ap, n_receive, i_product, i_product_grade, i_product_motif from tm_ap_item
          							  WHERE i_ap='$iap' and i_supplier='$isupplier'");
		  $query = $this->db->get();
		  foreach($query->result() as $row){
			  $jml    = $row->n_receive;
			  $product= $row->i_product;
			  $grade  = $row->i_product_grade;
			  $motif  = $row->i_product_motif;
			  $eproductname = $row->e_product_name;
        $dap    = $row->d_ap;
			  $this->db->query("update tm_op_item set n_delivery=n_delivery-$jml WHERE i_op='$iop'
						    	        and i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'");

				$query=$this->db->query(" select i_area, i_reff from tm_op where i_op='$iop' ",false);
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						if(substr($row->i_reff,0,3)=='SPB'){
							$ispb=$row->i_reff;
              $iarea=$row->i_area;
							$this->db->query("update tm_spb_item set n_stock=n_stock-$jml where i_spb='$ispb' and i_area='$iarea' and 
                                i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'");
						}else if(substr($row->i_reff,0,4)=='SPMB'){
							$ispmb=$row->i_reff;
              $iarea=$row->i_area;
		          $this->db->query("update tm_spmb_item set n_stock=n_stock-$jml where i_spmb='$ispmb' and i_area='$iarea' and 
                                i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'");
						}
					}
				}
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
        $th=substr($dap,0,4);
			  $bl=substr($dap,5,2);
			  $emutasiperiode=$th.$bl;

			  $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$iap'
                                    order by d_transaction desc, i_trans desc",false);
        if ($queri->num_rows() > 0){
      	  $row   		= $queri->row();
          $que 	= $this->db->query("SELECT current_timestamp as c");
	        $ro 	= $que->row();
	        $now	 = $ro->c;
          $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$product','$grade','$motif','$istore','$istorelocation','$istorelocationbin', 
                                '$eproductname', '$iap', '$now', 0, $jml, $row->n_quantity_akhir-$jml, $row->n_quantity_akhir
                              )
                           ",false);
        }
        if( ($jml!='') && ($jml!=0) ){
          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_pembelian=n_mutasi_pembelian-$jml, n_saldo_akhir=n_saldo_akhir-$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);
          $this->db->query(" 
                            UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                           ",false);
        }

		  }
		  $this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_refference_document='$iap'");#" and i_supplier='$isupplier'");
######
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
      if($isupplier=='AS'){
			  $this->db->select("	a.i_ap, a.i_supplier, a.i_op, a.i_area, a.d_ap, a.v_ap_gross,
													  a.i_op, a.f_ap_cancel, b.e_supplier_name
													  from tm_ap a, tr_supplier b
													  where a.i_supplier=b.i_supplier 
													  and (upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')
													  and a.d_ap >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_ap <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_ap desc ",false)->limit($num,$offset);
      }else{
			  $this->db->select("	a.i_ap, a.i_supplier, a.i_op, a.i_area, a.d_ap, a.v_ap_gross,
													  a.i_op, a.f_ap_cancel, b.e_supplier_name
													  from tm_ap a, tr_supplier b
													  where a.i_supplier=b.i_supplier 
													  and (upper(a.i_ap) like '%$cari%' or upper(a.i_op) like '%$cari%')
													  and a.i_supplier='$isupplier' and
													  a.d_ap >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_ap <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_supplier, a.i_ap desc ",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacasupplier($num,$offset)
    {
			$this->db->select(" * from tr_supplier",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function carisupplier($cari,$num,$offset)
    {
			$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
								order by i_supplier",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
