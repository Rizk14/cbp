<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_orderpb, a.d_orderpb, a.i_spg, b.e_spg_name, a.i_customer, c.e_customer_name, a.f_orderpb_cancel
                        from tm_orderpb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and ((upper(a.i_orderpb) like '%$cari%') or (upper(a.i_spg) like '%$cari%')
                        or (upper(b.e_spg_name) like '%$cari%') or (upper(c.e_customer_name) like '%$cari%')) and
                        a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_orderpb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_orderpb, a.d_orderpb, a.i_spg, b.e_spg_name, a.i_customer, c.e_customer_name, a.f_orderpb_cancel
                        from tm_orderpb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and ((upper(a.i_orderpb) like '%$cari%') or (upper(a.i_spg) like '%$cari%')
                        or (upper(b.e_spg_name) like '%$cari%') or (upper(c.e_customer_name) like '%$cari%')) and
                        a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_orderpb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iorderpb,$icustomer)
    {
		$this->db->select("a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_orderpb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_orderpb ='$iorderpb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($iorderpb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_orderpb_item a, tr_product_motif b
            						  where a.i_orderpb = '$iorderpb' and a.i_customer='$icustomer' and a.i_product=b.i_product and 
            						  a.i_product_motif=b.i_product_motif order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
	
    public function deleteheader($xiorderpb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_orderpb WHERE i_orderpb='$xiorderpb' and i_area='$iarea' and i_customer='$icustomer'");
    }

    function insertheader($iorderpb, $dorderpb, $iarea, $ispg, $icustomer)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_orderpb'       => $iorderpb,
      'i_area'          => $iarea,
      'i_spg'           => $ispg,
      'i_customer'      => $icustomer,
      'd_orderpb'       => $dorderpb,
      'f_orderpb_cancel'=> 'f',
      'd_orderpb_entry' => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_orderpb');
    }

    public function deletedetail($iproduct, $iproductgrade, $xiorderpb, $iarea, $icustomer, $iproductmotif) 
    {
		  $this->db->query("DELETE FROM tm_orderpb_item WHERE i_orderpb='$xiorderpb' and i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'");
    }

    function insertdetail($iorderpb,$iarea,$icustomer,$dorderpb,$iproduct,$eproductname,$iproductmotif,$iproductgrade,$nquantity,$nstock,$eremark,$i)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_orderpb'       => $iorderpb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_orderpb'       => $dorderpb,
          'i_product'       => $iproduct,
          'e_product_name'  => $eproductname,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity_order'=> $nquantity,
          'n_quantity_stock'=> $nstock,
          'd_orderpb_entry' => $dentry,
          'e_remark'        => $eremark,
          'n_item_no'       => $i
    		)
    	);	
    	$this->db->insert('tm_orderpb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    function bacaperiode($dfrom,$dto,$num,$offset,$cari,$area,$area2,$area3,$area4,$area5)
    {
      if($area=='PB'){
		    $this->db->select("	a.i_orderpb, a.d_orderpb, a.i_spg, b.e_spg_name, a.i_customer, c.e_customer_name, a.f_orderpb_cancel
                            from tm_orderpb a, tr_spg b, tr_customer c
                            where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                            and ((upper(a.i_orderpb) like '%$cari%') or (upper(a.i_spg) like '%$cari%')
                            or (upper(b.e_spg_name) like '%$cari%') or (upper(c.e_customer_name) like '%$cari%')) and
                            a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
						                ORDER BY a.i_orderpb ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.i_orderpb, a.d_orderpb, a.i_spg, b.e_spg_name, a.i_customer, c.e_customer_name, a.f_orderpb_cancel
                            from tm_orderpb a, tr_spg b, tr_customer c
                            where a.i_spg=b.i_spg and a.i_customer=c.i_customer and (a.i_area='$area' or a.i_area='$area2' or 
                            a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            and ((upper(a.i_orderpb) like '%$cari%') or (upper(a.i_spg) like '%$cari%')
                            or (upper(b.e_spg_name) like '%$cari%') or (upper(c.e_customer_name) like '%$cari%')) and
                            a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
						                ORDER BY a.i_orderpb ",false)->limit($num,$offset);
      }
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
    function cariperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_orderpb, a.d_orderpb, a.i_spg, b.e_spg_name, a.i_customer, c.e_customer_name, a.f_orderpb_cancel
                        from tm_orderpb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and ((upper(a.i_orderpb) like '%$cari%') or (upper(a.i_spg) like '%$cari%')
                        or (upper(b.e_spg_name) like '%$cari%') or (upper(c.e_customer_name) like '%$cari%')) and
                        a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_orderpb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaproduct($num,$offset,$cari)
    {
      if($offset=='')
        $offset=0;
      $query=$this->db->query(" select c.i_product as kode, a.i_product_motif as motif,
                                a.e_product_motifname as namamotif, c.e_product_name as nama
                                from tr_product_motif a,tr_product c
                                where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
