<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select(" * from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							  order by i_supplier",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function cariarea($cari,$num,$offset)
    {
      $this->db->select(" * from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%') order by i_supplier ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.i_dtap, a.d_dtap, a.d_due_date, a.i_supplier, b.e_supplier_name, a.i_area,
                          a.v_discount, a.v_ppn, a.v_netto, a.v_sisa, a.f_dtap_cancel
                          from tm_dtap a, tr_supplier b
                          where a.i_supplier=b.i_supplier
                          and (upper(a.i_dtap) like '%$cari%')
                          and a.i_supplier='$isupplier' and
                          a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
                          a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                          order by a.d_dtap, a.i_dtap ",false)->limit($num,$offset);
  #   						and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiodeperpages($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select(" a.i_dtap, a.d_dtap, a.d_due_date, a.i_supplier, b.e_supplier_name,
                          a.v_discount, a.v_ppn, a.v_netto, a.v_sisa, a.f_dtap_cancel
                          from tm_dtap a, tr_supplier b
                          where a.i_supplier=b.i_supplier
                          and (upper(a.i_dtap) like '%$cari%')
                          and a.i_supplier='$isupplier' and
                          a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
                          a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                          order by a.d_dtap, a.i_dtap ",false)->limit($num,$offset);
  #             and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.i_dtap, a.d_dtap, a.d_due_date, a.i_supplier, b.e_supplier_name,
                          a.v_discount, a.v_ppn, a.v_netto, a.v_sisa, a.f_dtap_cancel
                          from tm_dtap a, tr_supplier b
                          where a.i_supplier=b.i_supplier
                          and (upper(a.i_dtap) like '%$cari%')
                          and a.i_supplier='$isupplier' and
                          a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
                          a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                          order by a.d_dtap, a.i_dtap ",false)->limit($num,$offset);
  #              and a.f_nota_koreksi='f'
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  function bacabayar($nota,$area,$supp)
    {
#	  $this->db->select(" a.i_dtap, a.d_dtap, a.v_netto, c.i_pelunasanap, c.d_bukti, c.i_giro, c.e_bank_name, b.v_jumlah, 
#                        d.e_jenis_bayarname, d.i_jenis_bayar
#                        from tm_dtap a
#                        left join tm_pelunasanap_item b on(b.i_dtap=a.i_dtap and a.i_area=b.i_area)
#                        inner join tm_pelunasanap c on(b.i_pelunasanap=c.i_pelunasanap and c.f_pelunasanap_cancel='f' 
#                        and b.i_area=c.i_area and a.i_supplier=c.i_supplier)
#                        left join tr_jenis_bayar d on(d.i_jenis_bayar=c.i_jenis_bayar)
#                        where a.i_dtap='$nota' and a.i_area='$area' and a.i_supplier='$supp' order by c.i_pelunasanap", false);
    $nota=str_replace('%20',' ',$nota);
	  $this->db->select(" a.* from (
	                      select a.i_dtap, a.d_dtap, a.v_netto, c.i_pelunasanap, c.d_bukti, c.i_giro, c.e_bank_name, b.v_jumlah, 
                        d.e_jenis_bayarname, d.i_jenis_bayar
                        from tm_dtap a
                        left join tm_pelunasanap_item b on(b.i_dtap=a.i_dtap and a.i_area=b.i_area)
                        inner join tm_pelunasanap c on(b.i_pelunasanap=c.i_pelunasanap and c.f_pelunasanap_cancel='f' 
                        and b.i_area=c.i_area and a.i_supplier=c.i_supplier)
                        left join tr_jenis_bayar d on(d.i_jenis_bayar=c.i_jenis_bayar)
                        where a.i_dtap='$nota' and a.i_area='$area' and a.i_supplier='$supp'
                        union all
                        SELECT a.i_dtap, a.d_dtap, a.v_netto, c.i_alokasi, c.d_alokasi, '' as i_giro, '' as e_bank_name, b.v_jumlah, 
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_dtap a
                        left join tm_alokasi_kb_item b on(b.i_nota=a.i_dtap)
                        inner join tm_alokasi_kb c on(b.i_alokasi=c.i_alokasi and c.f_alokasi_cancel='f' and b.i_kb=c.i_kb and 
                        a.i_supplier=c.i_supplier)
                        where a.i_dtap='$nota' and a.i_area='$area' and a.i_supplier='$supp' 
                        union all
                        SELECT a.i_dtap, a.d_dtap, a.v_netto, c.i_alokasi, c.d_alokasi, '' as i_giro, '' as e_bank_name, b.v_jumlah, 
                        '' as e_jenis_bayarname, '' as i_jenis_bayar
                        from tm_dtap a
                        left join tm_alokasi_bk_item b on(b.i_nota=a.i_dtap)
                        inner join tm_alokasi_bk c on(b.i_alokasi=c.i_alokasi and c.f_alokasi_cancel='f' and b.i_kbank=c.i_kbank 
                        and a.i_supplier=c.i_supplier)
                        where a.i_dtap='$nota' and a.i_area='$area' and a.i_supplier='$supp'
                        ) as a
                        order by a.i_pelunasanap", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
