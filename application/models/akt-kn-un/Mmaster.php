<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
							from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
							where a.i_customer=b.i_customer 
							  and b.i_customer=d.i_customer
							  and a.i_salesman=e.i_salesman
							  and a.i_area=c.i_area 
							  and a.f_posting = 't' and a.f_close='f'
							order by a.i_kn desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
							from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
							where a.i_customer=b.i_customer 
							  and b.i_customer=d.i_customer
							  and a.i_salesman=e.i_salesman
							  and a.i_area=c.i_area 
							  and a.f_posting = 't' and a.f_close='f'
							and (upper(a.i_kn) like '%$cari%')
							order by a.i_kn desc ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakn($ikn,$iarea,$nknyear)
	{
 		$this->db->select(" a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
							from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
							where a.i_customer=b.i_customer 
							  and b.i_customer=d.i_customer
							  and a.i_salesman=e.i_salesman
							  and a.i_area=c.i_area 
							  and a.i_kn='$ikn'
							  and a.n_kn_year=$nknyear
							  and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader(	$ikn,$iarea,$dkn )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$ikn' and i_area='$iarea' and d_refference='$dkn'");
	}
	function deletetransitemdebet($accdebet,$ikn,$dkn)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$accdebet' and i_refference='$ikn' and d_refference='$dkn'");
	}
	function deletetransitemkredit($acckredit,$ikn,$dkn)
    {
		$this->db->query("delete from tm_jurnal_transharianitem 
						  where i_coa='$acckredit' and i_refference='$ikn' and d_refference='$dkn'");
	}
	function deletegldebet($accdebet,$ikn,$iarea,$dkn)
    {
		$this->db->query("delete from tm_general_ledger 
						  where i_refference='$ikn' and i_coa='$accdebet' and i_area='$iarea' and d_refference='$dkn'");
	}
	function deleteglkredit($acckredit,$ikn,$iarea,$dkn)
    {
		$this->db->query("delete from tm_general_ledger
						  where i_refference='$ikn' and i_coa='$acckredit' and i_area='$iarea' and d_refference='$dkn'");
	}
	function updatekn($ikn,$iarea,$nknyear)
    {
		$this->db->query("update tm_kn set f_posting='f' where i_kn='$ikn' and i_area='$iarea' and n_kn_year=$nknyear");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
