<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$isupplier)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
    $this->db->select("a.*, b.e_supplier_name from tm_op a, tr_supplier b
                        where a.i_supplier=b.i_supplier AND a.i_supplier ='$isupplier' 
                        and a.f_op_cancel = 'f'
                        AND d_op >= to_date('$dfrom','dd-mm-yyyy') AND d_op <= to_date('$dto','dd-mm-yyyy')",false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
    function bacasupplier($cari,$num,$offset)
    {
    $this->db->select(" * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
                      order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier
							where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')
							order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($iop)
    {
      $reff='';
      $this->db->select(" i_reff from tm_op where tm_op.i_op = '$iop'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $reff=$tes->i_reff;
        }
		  }
      if(substr($reff,0,3)=='SPB'){
    		$this->db->select("a.*, b.e_remark, d.e_product_motifname, i_reff from tm_op_item a, tm_spb_item b, tm_op c, tr_product_motif d where a.i_op='$iop'
                           and a.i_op=c.i_op and a.i_product=d.i_product and a.i_product_motif=d.i_product_motif
                           and a.i_op=b.i_op and c.i_reff=b.i_spb and c.i_area=b.i_area 
                           and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product_grade=b.i_product_grade order by a.n_item_no",false);
      }else{
    		$this->db->select("a.*, b.e_remark, d.e_product_motifname, i_reff from tm_op_item a, tm_spmb_item b, tm_op c, tr_product_motif d where a.i_op='$iop'
                           and a.i_op=c.i_op and a.i_product=d.i_product and a.i_product_motif=d.i_product_motif
                           and a.i_op=b.i_op and c.i_reff=b.i_spmb and c.i_area=b.i_area 
                           and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product_grade=b.i_product_grade order by a.n_item_no",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function bacaheader($i_op)
    {
        $this->db->select(" * from tm_op inner join tr_supplier on 
                           (tm_op.i_supplier=tr_supplier.i_supplier) inner join tr_op_status 
                           on (tm_op.i_op_status=tr_op_status.i_op_status) inner join tr_area 
                           on (tm_op.i_area=tr_area.i_area) where tm_op.i_op = '$i_op'
                           order by tm_op.i_op desc",false);
/*    		$this->db->select("s.e_supplier_name, s.e_supplier_address, s.e_supplier_city, o.i_op,
    		                   i_reff, o.n_top_length
    		                   from tm_op o, tr_supplier s
    		                   where o.i_op = '$iop' and o.i_supplier=s.i_supplier
    		                   ",false);*/
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
