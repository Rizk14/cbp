<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproductbase)
    {
		$this->db->select(" * FROM tr_product
			    LEFT JOIN tr_product_class
			    ON (tr_product.i_product_class = tr_product_class.i_product_class)
			    LEFT JOIN tr_product_category
			    ON (tr_product.i_product_category = tr_product_category.i_product_category)
			    LEFT JOIN tr_product_type
			    ON (tr_product.i_product_type = tr_product_type.i_product_type)
				  LEFT JOIN tr_product_group
				  ON (tr_product_group.i_product_group = tr_product_type.i_product_group) 
			    LEFT JOIN tr_product_status
			    ON (tr_product.i_product_status = tr_product_status.i_product_status)
			    LEFT JOIN tr_supplier
			    ON (tr_product.i_supplier = tr_supplier.i_supplier)
			    where tr_product.i_product = '$iproductbase'
			", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cari_brg($nbrg){
		  return $this->db->query(" SELECT * FROM tr_product WHERE i_product_status='2' and i_product='$nbrg' ");
	  }
    function insert(
		$iproduct, $iproductsupplier, $isupplier, $iproductstatus, 
		$iproducttype, $iproductcategory, $iproductclass, $iproductgroup,$eproductname,
		$eproductsuppliername, $vproductretail, $vproductmill, $fproductpricelist,
		$dproductstopproduction, $dproductregister
		   )
    {
		if($fproductpricelist=='on')
			$fproductpricelist='TRUE';
		else
			$fproductpricelist='FALSE';
		if($dproductregister!=''){	
			$tmp=explode("-",$dproductregister);
			$t=$tmp[2];
			$b=$tmp[1];
			$h=$tmp[0];
			$dproductregister=$t."-".$b."-".$h." 00:00:00";
		}else{
			$dproductregister=NULL;
		}
		if($dproductstopproduction!=''){	
			$tmp=explode("-",$dproductstopproduction);
			$t=$tmp[2];
			$b=$tmp[1];
			$h=$tmp[0];
			$dproductstopproduction=$t."-".$b."-".$h." 00:00:00";
		}else{
			$dproductstopproduction=NUll;
		}
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
    	$this->db->set(
    		array(
			'i_product'		          => $iproduct, 
			'i_product_supplier'		=> $iproductsupplier, 
			'i_supplier'      			=> $isupplier, 
			'i_product_status'   		=> $iproductstatus, 
			'i_product_type'    		=> $iproducttype, 
			'i_product_category'		=> $iproductcategory, 
			'i_product_class'   		=> $iproductclass,
			'e_product_name'		    => $eproductname, 
			'e_product_suppliername'=> $eproductsuppliername, 
			'v_product_retail'  		=> $vproductretail, 
			'v_product_mill'		    => $vproductmill, 
			'f_product_pricelist'		=> $fproductpricelist, 
			'd_product_stopproduction'	=> $dproductstopproduction, 
			'd_product_register'		=> $dproductregister, 
			'd_product_entry'		    => $dentry
    		)
    	);
    	$this->db->insert('tr_product');
#		redirect('productbase/cform/');
    }
    function insertmotif($iproductmotif, $eproductmotifname,$iproduct, $eproductname)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
      $query = $this->db->query("select * from tr_product_motif 
                                where i_product='$iproduct' and i_product_motif='$iproductmotif'");
      if($query->num_rows()>0){
		    $this->db->set(
			    array(
				    'i_product'				    => $iproduct,
				    'i_product_motif' 		=> $iproductmotif,
				    'e_product_motifname' => $eproductmotifname,
				    'd_product_motifentry'=> $dentry
			    )
		    );
		    $this->db->where('i_product',$iproduct);
		    $this->db->where('i_product_motif',$iproductmotif);
		    $this->db->update('tr_product_motif');
      }else{
		    $this->db->set(
			    array(
				    'i_product'				=> $iproduct,
				    'i_product_motif' 		=> $iproductmotif,
				    'e_product_motifname' 	=> $eproductmotifname,
				    'd_product_motifentry'	=> $dentry
			    )
		    );
		    $this->db->insert('tr_product_motif');
      }
    }
    function insertprice($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill)
    {
		  $query = $this->db->query("SELECT current_timestamp as c");
		  $row   = $query->row();
		  $dentry= $row->c;
		  $query = $this->db->get('tr_price_group');
		  foreach ($query->result() as $row)
		  {
			  $tmp = $row->i_price_group;
			  switch($tmp)
			  {
			  case '00':
				  $margin=(100-$nproductmargin)/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case '01':
				  $margin=(100-($nproductmargin+5+3+9))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case '02':
				  $margin=(100-($nproductmargin+5))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case '03':
				  $margin=(100-($nproductmargin+5+3))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case '04':
				  $margin=(100-($nproductmargin+5+3+9+5))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case '05':
				  $margin=(100-($nproductmargin+5+3+3))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  break;
			  case 'G0':
				  $margin=(100-$nproductmargin)/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  $vproductretail=$vproductretail*0.9;
				  break;
			  case 'G2':
				  $margin=(100-($nproductmargin+5))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  $vproductretail=$vproductretail*0.9;
				  break;
			  case 'G3':
				  $margin=(100-($nproductmargin+5+3))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  $vproductretail=$vproductretail*0.9;
				  break;
			  case 'G5':
				  $margin=(100-($nproductmargin+5+3+3))/100;
				  $vproductretail=round((($vproductmill*1.1/$margin)),-2);
				  $vproductretail=$vproductretail*0.9;
				  break;
			  }
        $query=$this->db->query("select * from tr_product_price 
                                 where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_price_group='$tmp'");
        if($query->num_rows()>0){
			    $this->db->query("update tr_product_price set e_product_name='$eproductname', 
                            v_product_retail=$vproductretail, v_product_mill=$vproductmill, 
                            d_product_priceentry='$dentry', n_product_margin=$nproductmargin
                            where i_product='$iproduct' and i_price_group='$tmp' and i_product_grade='$iproductgrade'");
        }else{
			    $this->db->query("insert into tr_product_price (i_product, e_product_name, i_product_grade,
				      			  v_product_retail, v_product_mill, d_product_priceentry, i_price_group, 
							      n_product_margin) values
				        		  ('$iproduct','$eproductname','$iproductgrade',$vproductretail,$vproductmill, '$dentry',
							       '$tmp',$nproductmargin)");
        }
		  }
    }
    function update(
		    $iproduct, $iproductsupplier, $isupplier, $iproductstatus, 
		    $iproducttype, $iproductcategory, $iproductclass, $iproductgroup, $eproductname,
		    $eproductsuppliername, $vproductretail, $vproductmill, $fproductpricelist,
		    $dproductstopproduction, $dproductregister
  		   )
    {
		  if($fproductpricelist=='on')
			  $fproductpricelist='TRUE';
		  else
			  $fproductpricelist='FALSE';
		  if($dproductregister!=''){	
			  if(strlen($dproductregister)==10){
				  $tmp=explode("-",$dproductregister);
				  $t=$tmp[2];
				  $b=$tmp[1];
				  $h=$tmp[0];
				  $dproductregister=$t."-".$b."-".$h." 00:00:00";
			  }
		  }else{
			  $dproductregister=NULL;
		  }
		  if($dproductstopproduction!=''){	
			  if(strlen($dproductstopproduction)==10){
				  $tmp=explode("-",$dproductstopproduction);
				  $t=$tmp[2];
				  $b=$tmp[1];
				  $h=$tmp[0];
				  $dproductstopproduction=$t."-".$b."-".$h." 00:00:00";
			  }
		  }else{
			  $dproductstopproduction=NUll;
		  }
		  $query  = $this->db->query("SELECT current_timestamp as c");
		  $row    = $query->row();
		  $dupdate= $row->c;
      	$data = array(
			  'i_product'		=> $iproduct, 
			  'i_product_supplier'		=> $iproductsupplier, 
			  'i_supplier'			=> $isupplier, 
			  'i_product_status'		=> $iproductstatus, 
			  'i_product_type'		=> $iproducttype, 
			  'i_product_category'		=> $iproductcategory, 
			  'i_product_class'		=> $iproductclass, 
			  'e_product_name'		=> $eproductname, 
			  'e_product_suppliername'	=> $eproductsuppliername, 
			  'v_product_retail'		=> $vproductretail, 
			  'v_product_mill'		=> $vproductmill, 
			  'f_product_pricelist'		=> $fproductpricelist, 
			  'd_product_stopproduction'	=> $dproductstopproduction, 
			  'd_product_register'		=> $dproductregister, 
			  'd_product_update'		=> $dupdate
              );
		  $this->db->where('i_product =', $iproduct);
		  $this->db->update('tr_product', $data); 
    	$data = array(
                    'v_product_mill'		=> $vproductmill
                   );
		  $this->db->where('i_product =', $iproduct);
		  $this->db->update('tr_product_price', $data); 
    }
	
    function delete($iproduct) 
    {
		  $this->db->query("DELETE FROM tr_product WHERE i_product='$iproduct'", false);
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from tr_product where i_product_status='2' 
                        and (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%')
          							order by i_product", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tr_product where i_product_status='2' 
                        and (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%')
							          order by i_product", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacagroup($num,$offset)
    {
		  $this->db->select("i_product_group, e_product_groupname from tr_product_group order by i_product_group",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carigroup($cari,$num,$offset)
    {
		$this->db->select("i_product_group, e_product_groupname from tr_product_group 
				   where upper(e_product_groupname) like '%$cari%' or upper(i_product_group) like '%$cari%' order by i_product_group", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select("i_supplier, e_supplier_name from tr_supplier order by i_supplier", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select("i_supplier, e_supplier_name from tr_supplier 
				   where upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%' order by i_supplier", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproductstatus($num,$offset)
    {
		$this->db->select("i_product_status, e_product_statusname from tr_product_status order by i_product_status", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproductstatus($cari,$num,$offset)
    {
		$this->db->select("i_product_status, e_product_statusname from tr_product_status 
				   where upper(e_product_statusname) like '%$cari%' or upper(i_product_status) like '%$cari%' order by i_product_status", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproducttype($iproductgroup,$num,$offset)
    {
		$this->db->select("i_product_type, e_product_typename from tr_product_type 
				   where i_product_group='$iproductgroup' order by i_product_type",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproducttype($iproductgroup,$cari,$num,$offset)
    {
		$this->db->select("i_product_type, e_product_typename from tr_product_type 
				   where (upper(e_product_typename) like '%$cari%' or upper(i_product_type) like '%$cari%') and i_product_group='$iproductgroup'
				   order by i_product_type", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproductcategory($iproductclass,$num,$offset)
    {
		$this->db->select("i_product_category, e_product_categoryname from tr_product_category 
				where i_product_class='$iproductclass' order by i_product_category",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproductcategory($iproductclass,$cari,$num,$offset)
    {
		$this->db->select("i_product_category, e_product_categoryname from tr_product_category 
				   where (upper(e_product_categoryname) like '%$cari%' or upper(i_product_category) like '%$cari%')
				   and i_product_class = '$iproductclass' order by i_product_category", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproductclass($num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname from tr_product_class order by i_product_class",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproductclass($cari,$num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname from tr_product_class 
				   where upper(e_product_classname) like '%$cari%' or upper(i_product_class) like '%$cari%' order by i_product_class", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
