<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($icustomer)
    {
      $sql1="select a.*, tr_area.e_area_name, tr_shop_status.e_shop_status, tr_marriage.e_marriage, tr_jeniskelamin.e_jeniskelamin,
             tr_religion.e_religion, tr_traversed.e_traversed, tr_customer_class.e_customer_classname, 
             tr_paymentmethod.e_paymentmethod, tr_call.e_call, tr_customer_group.e_customer_groupname,
             tr_customer_plugroup.e_customer_plugroupname, tr_customer_producttype.e_customer_producttypename,
             tr_customer_specialproduct.e_customer_specialproductname, tr_customer_status.e_customer_statusname,
             tr_customer_grade.e_customer_gradename, tr_customer_service.e_customer_servicename, tr_area.i_store,
             tr_customer_salestype.e_customer_salestypename, tr_price_group.e_price_groupname, tr_price_group.n_line,
             tr_customer_discount.n_customer_discount1,tr_customer_discount.n_customer_discount2 , tr_city.e_city_name, tr_customer_pkp.e_customer_pkpnpwp as pkpnpwp,
             tr_customer_pkp.e_customer_pkpname as namapkp, tr_customer_pkp.e_customer_pkpaddress as almtpkp,
             x.d_survey, x.n_visit_period, x.f_customer_new, x.e_rt1, x.e_customer_sign, x.e_rw1, x.e_postal1, x.e_fax1,
             x.e_customer_month, x.e_customer_age, x.e_customer_year, x.n_shop_broad, x.i_shop_status, x.e_customer_kelurahan1,
             x.e_customer_kecamatan1, x.e_customer_kota1, x.e_customer_provinsi1, x.e_customer_ownerttl, x.e_customer_owner,
             x.e_customer_ownerage, x.i_marriage, x.i_jeniskelamin, x.i_religion, x.e_rt2, x.e_rw2, x.e_customer_owneraddress,
             x.e_postal2, x.e_customer_ownerphone, x.e_customer_ownerfax, x.e_customer_ownerhp, x.e_customer_ownerpartnerttl,
             x.e_customer_ownerpartner, x.e_customer_ownerpartnerage, x.e_customer_kelurahan2, x.e_customer_kecamatan2,
             x.e_customer_kota2, x.e_customer_provinsi2, x.e_rt3, x.e_rw3, x.e_postal3, x.e_customer_sendphone, x.i_traversed,
             x.f_parkir, x.f_kuli, x.e_ekspedisi1, x.e_ekspedisi2, x.e_customer_kota3, x.e_customer_provinsi3,
             x.e_customer_pkpnpwp, x.e_customer_npwpname, x.e_customer_npwpaddress, x.i_paymentmethod, x.e_customer_bank1,
             x.e_customer_bankaccount1, x.e_customer_bankname1, x.e_customer_bank2, x.e_customer_bankaccount2, x.e_customer_bankname2, 
             x.e_kompetitor1, x.e_kompetitor2, x.e_kompetitor3, x.n_customer_discount, x.f_kontrabon, x.i_call, x.e_kontrabon_hari, 
             x.e_tagih_hari, x.e_kontrabon_jam1, x.e_tagih_jam1, x.e_kontrabon_jam2, x.e_tagih_jam2, x.e_customer_mail,
             z.i_salesman, z.e_salesman_name, tr_price_group.e_price_groupname, tr_customer_owner.e_customer_ownername as ownername,
             tr_customer_owner.e_customer_owneraddress as owneraddress, tr_customer_owner.e_customer_ownerphone as ownerphone
				     fROM tr_customer a
             LEFT JOIN tr_customer_salesman z
             ON (a.i_customer = z.i_customer) ";
      $sql2="LEFT JOIN tr_city
             ON (a.i_city = tr_city.i_city and a.i_area = tr_city.i_area)
             LEFT JOIN tr_customer_group
             ON (a.i_customer_group = tr_customer_group.i_customer_group)
             LEFT JOIN tr_customer_discount
             ON (a.i_customer = tr_customer_discount.i_customer)
             LEFT JOIN tr_area
             ON (a.i_area = tr_area.i_area)
             LEFT JOIN tr_customer_status 
             ON (a.i_customer_status = tr_customer_status.i_customer_status)
             LEFT JOIN tr_customer_producttype
             ON (a.i_customer_producttype = tr_customer_producttype.i_customer_producttype)
             LEFT JOIN tr_customer_specialproduct
             ON (a.i_customer_specialproduct = tr_customer_specialproduct.i_customer_specialproduct)
             LEFT JOIN tr_customer_grade
             ON (a.i_customer_grade = tr_customer_grade.i_customer_grade)
             LEFT JOIN tr_customer_service
             ON (a.i_customer_service = tr_customer_service.i_customer_service)
             LEFT JOIN tr_customer_salestype
             ON (a.i_customer_salestype = tr_customer_salestype.i_customer_salestype)
             LEFT JOIN tr_customer_class 
             ON (a.i_customer_class=tr_customer_class.i_customer_class)
             LEFT JOIN tr_shop_status 
             ON (x.i_shop_status=tr_shop_status.i_shop_status)
             LEFT JOIN tr_marriage 
             ON (x.i_marriage=tr_marriage.i_marriage)
             LEFT JOIN tr_jeniskelamin 
             ON (x.i_jeniskelamin=tr_jeniskelamin.i_jeniskelamin)
             LEFT JOIN tr_religion 
             ON (x.i_religion=tr_religion.i_religion)
             LEFT JOIN tr_traversed 
             ON (x.i_traversed=tr_traversed.i_traversed)
             LEFT JOIN tr_paymentmethod 
             ON (x.i_paymentmethod=tr_paymentmethod.i_paymentmethod)
             LEFT JOIN tr_call 
             ON (x.i_call=tr_call.i_call)
             LEFT JOIN tr_customer_plugroup
             ON (a.i_customer_plugroup=tr_customer_plugroup.i_customer_plugroup)
             LEFT JOIN tr_price_group
             ON (a.i_price_group=tr_price_group.i_price_group or a.i_price_group=tr_price_group.n_line)
	           LEFT JOIN tr_customer_pkp
	           ON (a.i_customer=tr_customer_pkp.i_customer)
	           LEFT JOIN tr_customer_owner
	           ON (a.i_customer=tr_customer_owner.i_customer)
             where a.i_customer = '$icustomer'";

      $cektmp=$this->db->query("select i_customer from tr_customer_tmp where i_customer='$icustomer'");
      if($cektmp->num_rows() > 0){
        $sql=$sql1." LEFT JOIN tr_customer_tmp x
                     ON (a.i_customer = x.i_customer) ".$sql2;
      }else{
        $sql=$sql1." LEFT JOIN tr_customer_tmpnonspb x
                     ON (a.i_customer = x.i_customer) ".$sql2;
      }
		  $query=$this->db->query($sql);
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function update($icustomer,$fpareto)
    {
		  if($fpareto=='t')
			  $fpareto='TRUE';
		  else
			  $fpareto='FALSE';
    	$data = array(
                    'f_pareto' 		=> $fpareto
                   );
      $this->db->where('i_customer', $icustomer);
      $this->db->update('tr_customer', $data); 
    }
	
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select("a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                           where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                           and a.i_customer=b.i_customer
                           order by a.i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select("a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b
                           where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                           and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
    									     or a.i_area = '$area4' or a.i_area = '$area5')
                           and a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $this->db->select(" a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                           where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                           and a.i_customer=b.i_customer
                           order by a.i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select(" a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b
                           where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                           and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
    									     or a.i_area = '$area4' or a.i_area = '$area5')
                           and a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
