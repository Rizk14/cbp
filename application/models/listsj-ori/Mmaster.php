<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj,$iarea) 
    {
			$this->db->query(" update tm_nota set f_nota_cancel='t' WHERE i_sj='$isj' and i_area='$iarea'");
			$this->db->query(" update tm_spb set f_spb_cancel='t' WHERE i_sj='$isj' and i_area='$iarea'");
#####
      $this->db->select(" * from tm_nota where i_sj='$isj' and i_area='$iarea'");
			$qry = $this->db->get();
			if ($qry->num_rows() > 0){
        foreach($qry->result() as $qyr){  
          $dsj=$qyr->d_sj;
        }
      }
      $th=substr($dsj,0,4);
		  $bl=substr($dsj,5,2);
		  $emutasiperiode=$th.$bl;
      $query=$this->db->query(" select f_spb_consigment from tm_spb where i_sj='$isj' and i_area='$iarea'",false);
      $consigment='f';
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$consigment=$qq->f_spb_consigment;
				}
			}
      $iareasj= substr($isj,8,2);
      if($iareasj=='BK')$iareasj=$iarea;
      $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
      $st=$que->row();
      $istore=$st->i_store;
      if($istore=='AA'){
				$istorelocation		= '01';
			}else{
        if($consigment=='t')
          $istorelocation		= 'PB';
        else
  				$istorelocation		= '00';
			}
			$istorelocationbin	= '00';
      $this->db->select(" * from tm_nota_item where i_sj='$isj' and i_area='$iarea' order by n_item_no");
			$qery = $this->db->get();
			if ($qery->num_rows() > 0){
        foreach($qery->result() as $qyre){  
          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                                        and i_product_motif='$qyre->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                                        order by d_transaction desc, i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $row   		= $queri->row();
            $que 	= $this->db->query("SELECT current_timestamp as c");
	          $ro 	= $que->row();
	          $now	 = $ro->c;
            $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$qyre->i_product','$qyre->i_product_grade','$qyre->i_product_motif',
                                '$istore','$istorelocation','$istorelocationbin', 
                                '$qyre->e_product_name', '$isj', '$now', $qyre->n_deliver, 0, $row->n_quantity_akhir+$qyre->n_deliver, 
                                $row->n_quantity_akhir
                              )
                             ",false);

            $this->db->query(" 
                              UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qyre->n_deliver, 
                              n_saldo_akhir=n_saldo_akhir+$qyre->n_deliver
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                             ",false);
            $this->db->query(" 
                              UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qyre->n_deliver
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif' and i_store='$istore' and i_store_location='$istorelocation' 
                              and i_store_locationbin='$istorelocationbin'
                             ",false);

          }
        }
      }
#####
    }
    function bacasemua($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
								where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
								and (upper(a.i_area_from) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name c.e_customer_name from tm_nota a, tr_area b, tr_customer c
						where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
						where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%')
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
							where a.i_area=b.i_area and a.i_customer=c.i_customer
							and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							and (substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') )
							and a.d_sj >= to_date('$dfrom','dd-mm-yyyy')
							and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      }else{
			  $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
													  where a.i_area=b.i_area and a.i_customer=c.i_customer
													  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
													  and (substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') ) and
													  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													  a.d_sj <= to_date('$dto','dd-mm-yyyy')
													  order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
													where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
													and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
													and a.i_area_to='$iarea' and
													a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sj <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.d_sj, a.i_sj ",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
