<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ipricegroup)
    {
		$this->db->select('i_price_group, e_price_groupname')->from('tr_price_group')->where('i_price_group', $ipricegroup);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($ipricegroup, $epricegroupname)
    {
    	$this->db->set(
    		array(
    			'i_price_group' => $ipricegroup,
    			'e_price_groupname' => $epricegroupname
    		)
    	);
    	
    	$this->db->insert('tr_price_group');
		#redirect('pricegroup/cform/');
    }
    function update($ipricegroup, $epricegroupname)
    {
    	$data = array(
               'i_price_group' => $ipricegroup,
               'e_price_groupname' => $epricegroupname
            );
		$this->db->where('i_price_group', $ipricegroup);
		$this->db->update('tr_price_group', $data); 
		#redirect('pricegroup/cform/');
    }
	
    public function delete($ipricegroup) 
    {
		$this->db->query('DELETE FROM tr_price_group WHERE i_price_group=\''.$ipricegroup.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_price_group, e_price_groupname from tr_price_group order by i_price_group", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
