<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function baca($idkb, $iarea)
	{
		$this->db->select(" tm_dkb_sjp.i_dkb, 
							tm_dkb_sjp.i_dkb_kirim, tm_dkb_sjp.i_dkb_via, tm_dkb_sjp.i_area, tm_dkb_sjp.i_ekspedisi, tm_dkb_sjp.d_dkb, 
							tm_dkb_sjp.i_kendaraan, tm_dkb_sjp.e_sopir_name, tm_dkb_sjp.v_dkb, tm_dkb_sjp.f_dkb_batal, 
							tm_dkb_sjp.d_entry, to_char(tm_dkb_sjp.d_entry,'yyyy-mm-dd') as tglentry, tm_dkb_sjp.d_update, tm_dkb_sjp.i_approve1,
							tr_area.e_area_name, tr_dkb_kirim.e_dkb_kirim, tr_dkb_via.e_dkb_via, tr_ekspedisi.e_ekspedisi
							from tm_dkb_sjp 
							inner join tr_area on(tm_dkb_sjp.i_area=tr_area.i_area)
							inner join tr_dkb_kirim on(tm_dkb_sjp.i_dkb_kirim=tr_dkb_kirim.i_dkb_kirim)
							inner join tr_dkb_via on(tm_dkb_sjp.i_dkb_via=tr_dkb_via.i_dkb_via)
							left join tr_ekspedisi on(tm_dkb_sjp.i_ekspedisi=tr_ekspedisi.i_ekspedisi)
							where  tm_dkb_sjp.i_dkb ='$idkb' ", false); # and tm_dkb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function bacadetail($idkb, $iarea)
	{
		$this->db->select("	a.i_dkb, a.d_dkb, a.i_area, a.i_sjp, a.d_sjp, sum(c.n_quantity_deliver) AS n_deliver, 
							sum(d.v_product_retail) AS jumlah03, a.v_jumlah, a.e_remark, a.n_koli, a.n_ikat, a.n_kardus, b.e_telat2
							from tm_dkb_sjp_item a
							INNER JOIN tm_sjp b ON a.i_sjp = b.i_sjp AND a.i_area = b.i_area 
							INNER JOIN tm_sjp_item c ON b.i_sjp = c.i_sjp AND b.i_area = c.i_area
							INNER JOIN tr_product_price d ON c.i_product = d.i_product AND c.i_product_grade  = d.i_product_grade AND d.i_price_group = '03'
							where a.i_dkb='$idkb'
							GROUP BY a.i_dkb, a.d_dkb, a.i_area, a.i_sjp, a.d_sjp, b.e_telat2, 
							a.e_remark, a.n_koli, a.n_ikat, a.n_kardus ", false); # and i_area='$iarea' order by i_dkb", false);
		/*$this->db->select(" a.*, b.i_nota
                        from tm_dkb_sjp_item a, tm_nota b where a.i_dkb='$idkb'
                        and a.i_dkb=b.i_dkb and a.i_sjp=b.i_sj and a.i_area=b.i_area",false);*/ # and i_area='$iarea' order by i_dkb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetailrunningjml($idkb, $iarea)
	{
		return $this->db->query(" select sum(v_jumlah) as v_total from tm_dkb_sjp_item where i_dkb='$idkb' and i_area='$iarea' ", false);
	}
	function customertodetail($isj, $dsj, $iarea)
	{
		return $this->db->query(" select b.i_customer, b.e_customer_name 
                              from tm_nota a
                              inner join tr_customer b on b.i_customer=a.i_customer 
                              where a.i_sjp='$isjp' and a.d_sj='$dsj' and a.i_area='$iarea'");
	}
	function insertheader($idkb, $ddkb, $iareasj, $idkbkirim, $idkbvia, $ikendaraan, $esopirname, $vdkb, $idkbold)
	{
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;

		$this->db->set(
			array(
				'i_dkb'		    => $idkb,
				'i_dkb_kirim'	=> $idkbkirim,
				'i_dkb_via'	  	=> $idkbvia,
				'i_area'	    => $iareasj,
				'd_dkb'		    => $ddkb,
				'i_kendaraan'	=> $ikendaraan,
				'e_sopir_name' 	=> $esopirname,
				'v_dkb'		    => $vdkb,
				'f_dkb_batal'	=> 'f',
				'd_entry'	    => $dentry
			)
		);
		$this->db->insert('tm_dkb_sjp');
	}

	function insertdetail($idkb, $iareasj, $isjp, $ddkb, $dsjp, $vjumlah, $eremark, $i, $nkoli, $nikat, $nkardus)
	{
		if ($eremark == '') $eremark = null;
		$this->db->query("DELETE FROM tm_dkb_sjp_item WHERE i_dkb='$idkb' and i_area='$iareasj' and i_sjp='$isjp'");
		$this->db->set(
			array(
				'i_dkb'	  	=> $idkb,
				'i_area'  	=> $iareasj,
				'i_sjp'	   	=> $isjp,
				'd_dkb'   	=> $ddkb,
				'd_sjp'  	=> $dsjp,
				'v_jumlah'	=> $vjumlah,
				'e_remark'	=> $eremark,
				'n_item_no' => $i,
				'n_koli' 	=> $nkoli,
				'n_ikat' 	=> $nikat,
				'n_kardus' 	=> $nkardus
			)
		);
		$this->db->insert('tm_dkb_sjp_item');
	}

	function updatesjp($idkb, $isj, $iareasj, $ddkb, $etelat)
	{
		$this->db->set(
			array(
				'i_dkb'		=> $idkb,
				'd_dkb' 	=> $ddkb,
				'e_telat2' 	=> $etelat
			)
		);
		$this->db->where('i_sjp', $isj);
		$this->db->where('i_area', $iareasj);
		$this->db->update('tm_sjp');
	}

	function updateheader($ispmb, $dspmb, $iarea, $ispmbold)
	{
		$this->db->set(
			array(
				'd_spmb'	=> $dspmb,
				'i_spmb_old' => $ispmbold,
				'i_area'	=> $iarea
			)
		);
		$this->db->where('i_spmb', $ispmb);
		$this->db->update('tm_spmb');
	}

	public function deletedetail($idkb, $iarea, $isjp)
	{
		$this->db->select("v_jumlah from tm_dkb_sjp_item
        						     where i_dkb = '$idkb' and i_area='$iarea' and i_sjp='$isjp'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$this->db->query("update tm_dkb_sjp set v_dkb=v_dkb WHERE i_dkb='$idkb' and i_area='$iarea'");
				$this->db->query("update tm_sjp set i_dkb=null, d_dkb=null WHERE i_sjp='$isjp' and i_area='$iarea'");
			}
		}
		$this->db->query("DELETE FROM tm_dkb_sjp_item WHERE i_dkb='$idkb' and i_area='$iarea' and i_sjp='$isjp'");
	}

	/* public function deletedetail2($idkb, $iarea, $isj, $iareasj) 
    {
      $iareaxx = $this->session->userdata('i_area');
      if($iareaxx=='00') $daer='f';
      if($iareaxx!='00') $daer='t';
		  $this->db->select(" v_jumlah from tm_dkb_sjp_item
						     where i_dkb = '$idkb' and i_area_referensi='$iarea' and i_sjp='$isjp'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $this->db->query("update tm_dkb set v_dkb=v_dkb-$row->v_jumlah WHERE i_dkb='$idkb' and i_area_referensi='$iarea'");
				  $this->db->query("update tm_nota set i_dkb=null WHERE i_sj_type='04' and i_sjp='$isjp' and i_area_referensi='$iareasj' and f_sj_daerah='$daer'");
			  }
		  }
		  $this->db->query("DELETE FROM tm_dkb_sjp_item WHERE i_dkb='$idkb' and i_area_referensi='$iarea' and i_sjp='$isjp'");
	} */

	public function deletedetail2($idkb, $iarea, $isjp, $iareasjp)
	{
		$iareaxx = $this->session->userdata('i_area');

		if ($iareaxx == '00') $daer = 'f';
		if ($iareaxx != '00') $daer = 't';

		$this->db->select(" v_jumlah from tm_dkb_sjp_item
						    where i_dkb = '$idkb' and i_area='$iarea' and i_sjp='$isjp'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$this->db->query("update tm_dkb_sjp set v_dkb=v_dkb-$row->v_jumlah WHERE i_dkb='$idkb' and i_area='$iarea'");
				$this->db->query("update tm_sjp set i_dkb=null, d_dkb=null WHERE i_sjp='$isjp' and i_area='$iarea'");
			}
		}
		$this->db->query("DELETE FROM tm_dkb_sjp_item WHERE i_dkb='$idkb' and i_area='$iarea' and i_sjp='$isjp'");
	}

	public function deleteheader($idkb, $iarea)
	{
		$this->db->query("DELETE FROM tm_dkb_sjp WHERE i_dkb='$idkb' and i_area='$iarea'");
	}

	function bacasemua()
	{
		$this->db->select(" * from tm_spmb order by i_spmb desc", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaproduct($num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumber($iarea, $thbl)
	{
		$th		= substr($thbl, 0, 4);
		$asal 	= $thbl;
		$thbl 	= substr($thbl, 2, 2) . substr($thbl, 4, 2);

		$this->db->select(" n_modul_no as max from tm_dgu_no 
							where i_modul='DKP'
							and substr(e_periode,1,4)='$th' 
							and i_area='$iarea' for update", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nodkb  = $terakhir + 1;
			$this->db->query(" 	update tm_dgu_no 
								set n_modul_no=$nodkb
								where i_modul='DKP'
								and substr(e_periode,1,4)='$th' 
								and i_area='$iarea'", false);
			settype($nodkb, "string");
			$a = strlen($nodkb);
			while ($a < 4) {
				$nodkb = "0" . $nodkb;
				$a = strlen($nodkb);
			}
			$nodkb  = "DKBP-" . $thbl . "-" . $iarea . $nodkb;
			return $nodkb;
		} else {
			$nodkb  = "0001";
			$nodkb  = "DKBP-" . $thbl . "-" . $iarea . $nodkb;
			$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('DKP','$iarea','$asal',1)");
			return $nodkb;
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariproduct($cari, $num, $offset)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select(" 	*
							FROM
								tr_area
							WHERE
								i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser')
								AND i_area IN (SELECT i_store FROM tr_store WHERE f_aktif = 't')
							ORDER BY
								i_area ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select(" 	*
							FROM
								tr_area
							WHERE
								i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser')
								AND i_area IN (SELECT i_store FROM tr_store WHERE f_aktif = 't')
								AND (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
							ORDER BY
								i_area ", false)->limit($num, $offset);

		/* $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset); */
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacakirim($num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		// if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
		// 	$this->db->select("* from tr_dkb_kirim order by i_dkb_kirim", false)->limit($num, $offset);
		// } else {
		$this->db->select("* from tr_dkb_kirim where i_dkb_kirim='2' order by i_dkb_kirim", false)->limit($num, $offset);
		// }
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function carikirim($cari, $num, $offset)
	{
		$this->db->select("i_dkb_kirim, e_dkb_kirim from tr_dkb_kirim where upper(e_dkb_kirim) like '%$cari%' or upper(i_dkb_kirim) like '%$cari%' order by i_dkb_kirim ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacavia($num, $offset, $iarea)
	{
		$this->db->select(" 	a.*
							FROM
								tr_dkb_via a
								INNER JOIN tm_send_tolerance b ON a.i_dkb_via = b.i_send_type 
							WHERE
								b.i_area = '$iarea'
							ORDER BY
								i_dkb_via ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function carivia($cari, $num, $offset, $iarea)
	{
		$this->db->select(" 	a.*
							FROM
								tr_dkb_via a
								INNER JOIN tm_send_tolerance b ON a.i_dkb_via = b.i_send_type 
							WHERE
								b.i_area = '$iarea'
								AND upper(e_dkb_via) like '%$cari%' or upper(i_dkb_via) like '%$cari%'
							ORDER BY
								i_dkb_via ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaekspedisi($area, $num, $offset)
	{
		$this->db->select("* from tr_ekspedisi
                       where i_area='$area' order by i_ekspedisi", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariekspedisi($cari, $num, $offset)
	{
		$this->db->select("i_ekspedisi, e_ekspedisi from tr_ekspedisi where upper(e_ekspedisi) like '%$cari%' or upper(i_ekspedisi) like '%$cari%' order by i_ekspedisi ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacasjp($ddkbx, $cari, $area, $num, $offset)
	{
		if ($cari == '') {
			$this->db->select("	i_sjp, d_sjp, a.i_area, b.e_area_name, v_sjp
								FROM tm_sjp a
								INNER JOIN tr_area b ON a.i_area = b.i_area 
								WHERE
								a.i_area = '$area' AND d_sjp <= '$ddkbx'
								AND i_dkb IS NULL AND f_sjp_cancel='f'
								ORDER BY d_sjp DESC ", false)->limit($num, $offset);
		} else {
			$this->db->select("	i_sjp, d_sjp, a.i_area, b.e_area_name, v_sjp
								FROM tm_sjp a
								INNER JOIN tr_area b ON a.i_area = b.i_area 
								WHERE
								a.i_area = '$area' AND d_sjp <= '$ddkbx'
								AND i_dkb IS NULL AND f_sjp_cancel='f'
								AND (upper(a.i_sjp) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' 
								OR upper(a.i_spmb) LIKE '%$cari%' OR upper(a.i_area) LIKE '%$cari%')
								ORDER BY d_sjp DESC ", false)->limit($num, $offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function insertdetailekspedisi($idkb, $iarea, $iekspedisi, $ddkb, $eremark, $i)
	{
		$this->db->query("DELETE FROM tm_dkb_ekspedisi WHERE i_dkb='$idkb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
		$this->db->set(
			array(
				'i_dkb'			=> $idkb,
				'i_area'		=> $iarea,
				'i_ekspedisi'	=> $iekspedisi,
				'd_dkb' 		=> $ddkb,
				'e_remark'		=> $eremark,
				'n_item_no'   	=> $i
			)
		);
		$this->db->insert('tm_dkb_ekspedisi');
	}
	public function deletedetailekspedisi($idkb, $iarea, $iekspedisi)
	{
		$this->db->query("DELETE FROM tm_dkb_ekspedisi WHERE i_dkb='$idkb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
	}
	function bacadetailx($idkb, $iarea)
	{
		$this->db->select("	a.*, b.e_ekspedisi from tm_dkb_ekspedisi a, tr_ekspedisi b
							where a.i_dkb = '$idkb' and a.i_area='$iarea' and a.i_ekspedisi = b.i_ekspedisi order by a.i_dkb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function get_toleransi($iarea, $idkbvia)
	{
		return $this->db->query(" SELECT COALESCE (n_tolerance, 0) AS n_tolerance FROM tm_send_tolerance WHERE i_area = '$iarea' AND i_send_type = '$idkbvia' ");
	}
}
