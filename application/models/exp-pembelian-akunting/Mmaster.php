<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
        $query = $this->db->query(" SELECT
                                        i_supplier,
                                        e_supplier_name,
                                        i_dtap AS nofaktur,
                                        d_dtap AS tglfaktur,
                                        d_due_date AS jatuhtempo,
                                        d_pajak,
                                        v_gross,
                                        v_discount,
                                        v_netto,
                                        round(v_netto / ((tr_tax_amount.n_tax / 100)+ 1)) AS v_dpp,
                                        (v_netto - round(v_netto / ((tr_tax_amount.n_tax / 100)+ 1))) AS v_ppn,
                                        tr_supplier.f_supplier_pkp,
                                        v_sisa,
                                        n_supplier_toplength,
                                        e_area_name,
                                        tm_dtap.i_pajak 
                                    FROM
                                        tm_dtap
                                    LEFT JOIN tr_supplier
                                            USING (i_supplier)
                                    INNER JOIN tr_tax_amount ON
                                        tm_dtap.d_dtap BETWEEN tr_tax_amount.d_start AND tr_tax_amount.d_finish
                                    INNER JOIN tr_area ON
                                        (tm_dtap.i_area = tr_area.i_area)
                                    WHERE
                                        f_dtap_cancel = 'f'
                                        AND to_char(d_dtap, 'yyyymm')= '$iperiode'
                                    ORDER BY
                                        i_supplier,
                                        i_dtap ", false);

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }
}
