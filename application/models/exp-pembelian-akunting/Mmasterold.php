<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
        $sql="i_supplier, e_supplier_name, i_dtap as nofaktur, d_dtap as tglfaktur, d_due_date as jatuhtempo, 
              d_pajak, v_gross, v_discount,  v_netto,round (v_netto/1.1) as v_dpp,  (v_netto-round (v_netto/1.1)) as v_ppn
              from tm_dtap left join tr_supplier using (i_supplier) 
              where f_dtap_cancel='f' and to_char(d_dtap,'yyyymm')='$iperiode' order by i_supplier, i_dtap";
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
