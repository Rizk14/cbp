<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$num,$offset,$dfrom,$dto)
    {
			if($area1=='00')
			{
			   $this->db->select(" a.* from tm_dt a, tr_area b
						                 where a.i_area=b.i_area and a.f_dt_cancel ='f'
						                 and (upper(a.i_dt) like '%$cari%') and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                             and a.i_area='$iarea' order by a.i_dt DESC",false)->limit($num,$offset);
			}
			else
			{
			   $this->db->select(" a.* from tm_dt a, tr_area b
						                 where a.i_area=b.i_area and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                             or a.i_area='$area4' or a.i_area='$area5')
                             and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                             and a.i_area='$iarea' and a.f_dt_cancel ='f'
						                 
						                 and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						                 or upper(a.i_dt) like '%$cari%') order by a.i_dt DESC",false)->limit($num,$offset);
			}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($idt,$iarea)
    {
		$this->db->select(" * from tm_dt
				                inner join tr_area on (tm_dt.i_area=tr_area.i_area)
				                where tm_dt.i_dt = '$idt' and tm_dt.i_area='$iarea'
				                order by tm_dt.i_dt desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($idt,$iarea)
    {
/*
		$this->db->select(" 	a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo
					                from tm_dt_item a, tr_customer c, tr_customer_groupbayar d, tm_nota b
					                where a.i_dt='$idt' and a.i_area='$iarea'
                            and a.i_nota=b.i_nota 
						                and a.i_customer=c.i_customer
						                and d.i_customer=c.i_customer
					                order by a.n_item_no",false);
*/
		$this->db->select(" 	a.*, c.e_customer_name, c.e_customer_city, b.d_jatuh_tempo, b.v_materai_sisa
					                from tm_dt_item a, tr_customer c, tm_nota b
					                where a.i_dt='$idt' and a.i_area='$iarea'
                            and a.i_nota=b.i_nota 
						                and a.i_customer=c.i_customer
					                order by a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
				where a.i_area=b.i_area and a.f_sisa='t'
				and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
				or upper(a.i_dt) like '%$cari%')
				order by a.i_dt desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
