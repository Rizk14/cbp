<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaspb($cari,$iarea,$dfrom,$dto,$num,$offset)
    {
		  $this->db->select(" a.i_spb, a.i_area, a.i_customer, b.e_customer_name from tm_spb a, tr_customer b
										      where (upper(a.i_spb) like '%$cari%') and a.i_customer=b.i_customer
										      and a.i_area='$iarea ' and (a.n_print=0 or a.n_print isnull)
                          and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_spb", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carispb($cari,$iarea,$dfrom,$dto,$num,$offset)
    {
		  $this->db->select("	a.i_spb, a.i_area, a.i_customer, b.e_customer_name from tm_spb a, tr_customer b
										      where (upper(a.i_spb) like '%$cari%') and a.i_customer=b.i_customer
										      and a.i_area='$iarea ' and (a.n_print=0 or a.n_print isnull)
                          and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
                          order by a.i_spb",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacamaster($area,$spbfrom,$spbto)
    {
		  $this->db->select("	a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.f_customer_pkp, c.*, d.*, e.*, f.* 
							  from tm_spb a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e, tr_customer_groupar f
							  where a.i_spb >= '$spbfrom' and a.i_spb <= '$spbto' and a.i_area = '$area' 
							  and a.i_customer=b.i_customer and (a.n_print=0 or a.n_print isnull)
							  and a.i_salesman=c.i_salesman and a.i_area=c.i_area and a.i_customer=f.i_customer
							  and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
							  and b.i_customer_class=d.i_customer_class",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($area,$spb)
    {
		  $this->db->select("	* from tm_spb_item
          inner join tr_product on (tm_spb_item.i_product=tr_product.i_product)
					inner join tr_product_motif on (tm_spb_item.i_product_motif=tr_product_motif.i_product_motif
					and tm_spb_item.i_product=tr_product_motif.i_product)
					where i_spb = '$spb' and i_area='$area' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							     order by i_area ", FALSE)->limit($num,$offset);
		  }else{
			  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function close($area,$spbfrom,$spbto)
    {
		  $this->db->query("	update tm_spb set n_print=n_print+1 
            							where i_spb >= '$spbfrom' and i_spb <= '$spbto' and i_area = '$area' ",false);
    }
}
?>
