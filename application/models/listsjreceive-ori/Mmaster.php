<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }
    function baca($isjp,$iarea)
    {
		$this->db->select(" a.i_nota, a.f_plus_ppn, a.d_sj, a.d_spb, a.i_sj, a.i_area, a.i_spb, a.i_sj_old, a.v_nota_netto, a.i_customer,
                        a.i_dkb, c.e_customer_name, b.e_area_name, a.d_sj_receive, a.e_sj_receive
                        from tm_nota a, tr_area b, tr_customer c
						   					where a.i_area=b.i_area and a.i_customer=c.i_customer
						   					and a.i_sj ='$isjp' ", false);#and substring(a.i_sj,9,2)='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isj, $iarea)
    {
		$this->db->select(" a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname, d.v_unit_price as harga,
                        a.v_unit_price, a.n_deliver, d.n_order, a.i_product_grade, d.n_order as n_qty
                        from tm_nota_item a, tr_product_motif b, tm_nota c, tm_spb_item d
                        where a.i_sj = '$isj' and a.i_product=b.i_product 
                        and a.i_sj=c.i_sj and a.i_area=c.i_area
                        and c.i_spb=d.i_spb and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }    
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iuser,$iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  $query = $this->db->select("a.*, b.e_area_name from tm_nota a, tr_area b
														      where a.i_area=b.i_area and a.d_sj_receive is not null
															    and (upper(a.i_sj) like '%$cari%')
															    and (a.i_area in (select i_area from tm_user_area where i_user='$iuser'))
															    and a.i_area='$iarea' and (a.d_sj>='$dfrom' and a.d_sj<='$dto')
															    ORDER BY a.i_sj desc",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaproduct($num,$offset,$cari)
    {
			$this->db->select("	a.i_product as kode, a.e_product_name as nama, b.v_product_retail as harga, 
                          c.i_product_motif as motif, c.e_product_motifname as namamotif
                          from tr_product a, tr_product_price b, tr_product_motif c
                          where a.i_product=b.i_product and b.i_price_group='00'
                          and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                          and a.i_product=c.i_product ORDER BY a.e_product_name",false)->limit($num,$offset); 
                          #and a.i_product_status<>'4'
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
