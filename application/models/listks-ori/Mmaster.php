<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iicconvertion,$istore,$istorelocation,$istorelocationbin) 
    {
		$this->db->query('DELETE FROM tm_ic_convertion WHERE i_ic_convertion=\''.$iicconvertion.'\'');
		$this->db->query('DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion=\''.$iicconvertion.'\'');
		$this->db->query('DELETE FROM tm_bbk where i_refference_document =\''.$iicconvertion.'\'');
		$this->db->select(" * from tm_bbk_item where i_refference_document ='$iicconvertion'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$row->n_quantity
								  where i_product ='$row->i_product' and i_product_grade ='$row->i_product_grade'
								  and i_product_motif ='$row->i_product_motif' and i_store ='$istore' 
								  and i_store_location ='$istorelocation' and i_store_locationbin ='$istorelocationbin'");
			}
		}
		$this->db->query('DELETE FROM tm_bbk_item where i_refference_document =\''.$iicconvertion.'\'');
		$this->db->query('DELETE FROM tm_bbm where i_refference_document =\''.$iicconvertion.'\'');
		$this->db->select(" * from tm_bbm_item where i_refference_document ='$iicconvertion'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock-$row->n_quantity
								  where i_product ='$row->i_product' and i_product_grade ='$row->i_product_grade'
								  and i_product_motif ='$row->i_product_motif' and i_store ='$istore' 
								  and i_store_location ='$istorelocation' and i_store_locationbin ='$istorelocationbin'");
			}
		}
		$this->db->query('DELETE FROM tm_bbm_item where i_refference_document =\''.$iicconvertion.'\'');

		return TRUE;
    }
    function baca($iperiode,$cari,$num,$offset)
    {
		$this->db->select(" * from tm_ic_convertion
                        where to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode' and
                        (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' or upper(i_ic_convertion) like '%$cari%')
                        order by i_ic_convertion desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iperiode,$cari,$num,$offset)
    {
		$this->db->select(" * from tm_ic_convertion
                        where to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode' and
                        (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' or upper(i_ic_convertion) like '%$cari%')
                        order by i_ic_convertion desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
