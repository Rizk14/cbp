<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
   public function __construct()
   {
      parent::__construct();
      #$this->CI =& get_instance();
   }

   function bacasemua_v1($iarea, $datefrom, $toto) /* ORI */
   {
      if ($iarea != 'NA') {
         $this->db->select("  a.i_spb,
		                     a.d_spb,
		                     a.d_approve2,
		                     a.i_customer,
		                     a.e_customer_name,
		                     a.e_customer_classname,
		                     a.n_customer_toplength,
		                     a.i_salesman,
		                     a.e_salesman_name,
		                     a.i_product,
		                     a.e_product_name,
		                     a.e_product_seriname,
		                     a.kategory,
		                     a.subkategori,
		                     a.v_unit_price,
		                     a.n_order,
		                     a.n_deliver,
		                     a.v_spb_gross as spbkotor,
		                     a.n_spb_discount1,
		                     a.n_spb_discount2,
		                     a.n_spb_discount3,
		                     a.v_spb_disc1,
		                     a.v_spb_disc2,
		                     a.v_spb_disc3,
		                     (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
		                     a.i_nota,
		                     a.d_nota,
		                     a.notakotor,
		                     a.n_nota_discount1,
		                     a.n_nota_discount2,
		                     a.n_nota_discount3,
		                     a.v_nota_disc1,
		                     a.v_nota_disc2,
		                     a.v_nota_disc3,
		                     (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
		                     a.i_supplier,
		                     a.e_supplier_name,
		                     a.i_price_group,
		                     a.i_sj,
		                     a.d_sj,
		                     a.e_store_name,
		                     a.i_city,
		                     a.d_sj_receive,
		                     a.e_product_typename,
		                     a.e_provinsi,
		                     a.i_store,
		                     a.i_approve1,
		                     a.i_approve2,
		                     a.f_spb_stockdaerah,
		                     a.i_dkb,
		                     a.f_spb_siapnotasales,
		                     a.f_spb_siapnotagudang,
		                     a.i_notapprove,
		                     a.f_spb_opclose,
		                     a.f_spb_cancel
		                  from
		                     (
		                     select
		                        a.f_spb_cancel,
		                        a.f_spb_opclose,
		                        a.i_notapprove,
		                        a.i_spb,
		                        a.d_spb,
		                        a.d_approve2,
		                        a.i_customer,
		                        b.e_customer_name,
		                        c.e_customer_classname,
		                        b.n_customer_toplength,
		                        a.i_salesman,
		                        d.e_salesman_name,
		                        e.i_product,
		                        e.e_product_name,
		                        g.e_product_seriname,
		                        i.e_product_classname as kategory,
		                        h.e_product_categoryname as subkategori,
		                        e.v_unit_price,
		                        e.n_order,
		                        e.n_deliver,
		                        e.v_unit_price * e.n_order as v_spb_gross,
		                        a.n_spb_discount1,
		                        a.n_spb_discount2,
		                        a.n_spb_discount3,
		                        (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_spb_disc1,
		                        (e.n_order * e.v_unit_price * (1::numeric(20,
		                        2) - a.n_spb_discount1 / 100::numeric(20,
		                        2)) * (a.n_spb_discount2 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_spb_disc2,
		                        (e.n_order * e.v_unit_price * (1::numeric(20,
		                        2) - a.n_spb_discount1 / 100::numeric(20,
		                        2)) * (1::numeric(20,
		                        2) - a.n_spb_discount2 / 100::numeric(20,
		                        2)) * (a.n_spb_discount3 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_spb_disc3,
		                        j.i_nota,
		                        j.d_nota,
		                        k.n_deliver * k.v_unit_price as notakotor,
		                        j.n_nota_discount1,
		                        j.n_nota_discount2,
		                        j.n_nota_discount3,
		                        (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_nota_disc1,
		                        (k.n_deliver * k.v_unit_price * (1::numeric(20,
		                        2) - j.n_nota_discount1 / 100::numeric(20,
		                        2)) * (j.n_nota_discount2 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_nota_disc2,
		                        (k.n_deliver * k.v_unit_price * (1::numeric(20,
		                        2) - j.n_nota_discount1 / 100::numeric(20,
		                        2)) * (1::numeric(20,
		                        2) - j.n_nota_discount2 / 100::numeric(20,
		                        2)) * (j.n_nota_discount3 / 100::numeric(20,
		                        2)))::numeric(20,
		                        2) as v_nota_disc3,
		                        f.i_supplier,
		                        l.e_supplier_name,
		                        b.i_price_group,
		                        j.i_sj,
		                        j.d_sj,
		                        m.e_store_name,
		                        b.i_city,
		                        j.d_sj_receive,
		                        o.e_product_typename,
		                        q.e_provinsi,
		                        a.i_store,
		                        a.i_approve1,
		                        a.i_approve2,
		                        a.f_spb_stockdaerah,
		                        j.i_dkb,
		                        a.f_spb_siapnotasales,
		                        a.f_spb_siapnotagudang
		                     from
		                        tm_spb a
		                     inner join tr_customer b on
		                        (a.i_customer = b.i_customer)
		                     inner join tr_customer_class c on
		                        (b.i_customer_class = c.i_customer_class)
		                     inner join tr_salesman d on
		                        (a.i_salesman = d.i_salesman)
		                     inner join tm_spb_item e on
		                        (a.i_spb = e.i_spb
		                           and a.i_area = e.i_area)
		                     inner join tr_product f on
		                        (e.i_product = f.i_product)
		                     left join tr_product_seri g on
		                        (f.i_product_seri = g.i_product_seri)
		                     left join tr_product_category h on
		                        (f.i_product_category = h.i_product_category)
		                     left join tr_product_class i on
		                        (h.i_product_class = i.i_product_class)
		                     left join tm_nota j on
		                        (a.i_nota = j.i_nota
		                           and a.i_sj = j.i_sj
		                           and a.i_area = j.i_area)
		                     left join tm_nota_item k on
		                        (j.i_nota = k.i_nota
		                           and e.i_product = k.i_product)
		                     inner join tr_supplier l on
		                        (f.i_supplier = l.i_supplier)
		                     inner join tr_store m on
		                        (a.i_store = m.i_store)
		                     inner join tr_store_location n on
		                        (m.i_store = n.i_store
		                           and a.i_store_location = n.i_store_location)
		                     inner join tr_product_type o on
		                        (f.i_product_type = o.i_product_type)
		                     inner join tr_city p on
		                        (b.i_area = p.i_area
		                           and b.i_city = p.i_city)
		                     inner join tr_area q on
		                        (j.i_area = q.i_area)
		                     where
		                        j.d_nota >= '$datefrom'
		                        and j.d_nota <= '$toto' ) as a
		                  order by
		                     a.i_nota
		                  ", false);
      } else {
         $this->db->select("  a.i_spb,
		                       a.d_spb,
		                       a.d_approve2,
		                       a.i_customer,
		                       a.e_customer_name,
		                       a.e_customer_classname,
		                       a.n_customer_toplength,
		                       a.i_salesman,
		                       a.e_salesman_name,
		                       a.i_product,
		                       a.e_product_name,
		                       a.e_product_seriname,
		                       a.kategory,
		                       a.subkategori,
		                       a.v_unit_price,
		                       a.n_order,
		                       a.n_deliver,
		                       a.v_spb_gross as spbkotor,
		                       a.n_spb_discount1,
		                       a.n_spb_discount2,
		                       a.n_spb_discount3,
		                       a.v_spb_disc1,
		                       a.v_spb_disc2,
		                       a.v_spb_disc3,
		                       (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
		                       a.i_nota,
		                       a.d_nota,
		                       a.notakotor,
		                       a.n_nota_discount1,
		                       a.n_nota_discount2,
		                       a.n_nota_discount3,
		                       a.v_nota_disc1,
		                       a.v_nota_disc2,
		                       a.v_nota_disc3,
		                       (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
		                       a.i_supplier,
		                       a.e_supplier_name,
		                       a.i_price_group,
		                       a.i_sj,
		                       a.d_sj,
		                       a.e_store_name,
		                       a.i_city,
		                       a.d_sj_receive,
		                       a.e_product_typename,
		                       a.e_provinsi,
		                       a.i_store,
		                       a.i_approve1,
		                       a.i_approve2,
		                       a.f_spb_stockdaerah,
		                       a.i_dkb,
		                       a.f_spb_siapnotasales,
		                       a.f_spb_siapnotagudang,
		                       a.i_notapprove,
		                       a.f_spb_opclose,
		                       a.f_spb_cancel
		                    from
		                       (
		                       select
		                          a.f_spb_cancel,
		                          a.f_spb_opclose,
		                          a.i_notapprove,
		                          a.i_spb,
		                          a.d_spb,
		                          a.d_approve2,
		                          a.i_customer,
		                          b.e_customer_name,
		                          c.e_customer_classname,
		                          b.n_customer_toplength,
		                          a.i_salesman,
		                          d.e_salesman_name,
		                          e.i_product,
		                          e.e_product_name,
		                          g.e_product_seriname,
		                          i.e_product_classname as kategory,
		                          h.e_product_categoryname as subkategori,
		                          e.v_unit_price,
		                          e.n_order,
		                          e.n_deliver,
		                          e.v_unit_price * e.n_order as v_spb_gross,
		                          a.n_spb_discount1,
		                          a.n_spb_discount2,
		                          a.n_spb_discount3,
		                          (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_spb_disc1,
		                          (e.n_order * e.v_unit_price * (1::numeric(20,
		                          2) - a.n_spb_discount1 / 100::numeric(20,
		                          2)) * (a.n_spb_discount2 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_spb_disc2,
		                          (e.n_order * e.v_unit_price * (1::numeric(20,
		                          2) - a.n_spb_discount1 / 100::numeric(20,
		                          2)) * (1::numeric(20,
		                          2) - a.n_spb_discount2 / 100::numeric(20,
		                          2)) * (a.n_spb_discount3 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_spb_disc3,
		                          j.i_nota,
		                          j.d_nota,
		                          k.n_deliver * k.v_unit_price as notakotor,
		                          j.n_nota_discount1,
		                          j.n_nota_discount2,
		                          j.n_nota_discount3,
		                          (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_nota_disc1,
		                          (k.n_deliver * k.v_unit_price * (1::numeric(20,
		                          2) - j.n_nota_discount1 / 100::numeric(20,
		                          2)) * (j.n_nota_discount2 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_nota_disc2,
		                          (k.n_deliver * k.v_unit_price * (1::numeric(20,
		                          2) - j.n_nota_discount1 / 100::numeric(20,
		                          2)) * (1::numeric(20,
		                          2) - j.n_nota_discount2 / 100::numeric(20,
		                          2)) * (j.n_nota_discount3 / 100::numeric(20,
		                          2)))::numeric(20,
		                          2) as v_nota_disc3,
		                          f.i_supplier,
		                          l.e_supplier_name,
		                          b.i_price_group,
		                          j.i_sj,
		                          j.d_sj,
		                          m.e_store_name,
		                          b.i_city,
		                          j.d_sj_receive,
		                          o.e_product_typename,
		                          q.e_provinsi,
		                          a.i_store,
		                          a.i_approve1,
		                          a.i_approve2,
		                          a.f_spb_stockdaerah,
		                          j.i_dkb,
		                          a.f_spb_siapnotasales,
		                          a.f_spb_siapnotagudang
		                       from
		                          tm_spb a
		                       inner join tr_customer b on
		                          (a.i_customer = b.i_customer)
		                       inner join tr_customer_class c on
		                          (b.i_customer_class = c.i_customer_class)
		                       inner join tr_salesman d on
		                          (a.i_salesman = d.i_salesman)
		                       inner join tm_spb_item e on
		                          (a.i_spb = e.i_spb
		                             and a.i_area = e.i_area)
		                       inner join tr_product f on
		                          (e.i_product = f.i_product)
		                       left join tr_product_seri g on
		                          (f.i_product_seri = g.i_product_seri)
		                       left join tr_product_category h on
		                          (f.i_product_category = h.i_product_category)
		                       left join tr_product_class i on
		                          (h.i_product_class = i.i_product_class)
		                       left join tm_nota j on
		                          (a.i_nota = j.i_nota
		                             and a.i_sj = j.i_sj
		                             and a.i_area = j.i_area)
		                       left join tm_nota_item k on
		                          (j.i_nota = k.i_nota
		                             and e.i_product = k.i_product)
		                       inner join tr_supplier l on
		                          (f.i_supplier = l.i_supplier)
		                       inner join tr_store m on
		                          (a.i_store = m.i_store)
		                       inner join tr_store_location n on
		                          (m.i_store = n.i_store
		                             and a.i_store_location = n.i_store_location)
		                       inner join tr_product_type o on
		                          (f.i_product_type = o.i_product_type)
		                       inner join tr_city p on
		                          (b.i_area = p.i_area
		                             and b.i_city = p.i_city)
		                       inner join tr_area q on
		                          (j.i_area = q.i_area)
		                       where
		                          j.d_nota >= '$datefrom'
		                          and j.d_nota <= '$toto' and a.i_area = '$iarea' ) as a
		                    order by
		                       a.i_nota
		                    ", false);
      }
   }

   function bacasemua_v2($iarea, $datefrom, $toto) /* INC BACA ALOKASI UNTUK CARI LAMA BAYAR */
   {
      if ($iarea != 'NA') { // CEK DATA PER AREA 
         return $this->db->query(" SELECT 
										0 as nol,
										a.i_spb,
										a.d_spb,
										a.d_approve2,
										a.i_customer,
										a.e_customer_name,
										a.e_customer_classname,
										a.n_customer_toplength,
										a.i_salesman,
										a.e_salesman_name,
										a.i_product,
										a.e_product_name,
										a.e_product_seriname,
										a.kategory,
										a.subkategori,
										a.v_unit_price,
										a.n_order,
										a.n_deliver,
										a.v_spb_gross as spbkotor,
										a.n_spb_discount1,
										a.n_spb_discount2,
										a.n_spb_discount3,
										a.v_spb_disc1,
										a.v_spb_disc2,
										a.v_spb_disc3,
										(a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
										a.i_nota,
										a.d_nota,
										a.notakotor,
										a.n_nota_discount1,
										a.n_nota_discount2,
										a.n_nota_discount3,
										a.v_nota_disc1,
										a.v_nota_disc2,
										a.v_nota_disc3,
										(a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
										a.i_supplier,
										a.e_supplier_name,
										a.i_price_group,
										a.i_sj,
										a.d_sj,
										a.e_store_name,
										a.i_city,
										a.d_sj_receive,
										a.e_product_typename,
										a.e_provinsi,
										a.i_store,
										a.i_approve1,
										a.i_approve2,
										a.f_spb_stockdaerah,
										a.i_dkb,
										a.f_spb_siapnotasales,
										a.f_spb_siapnotagudang,
										a.i_notapprove,
										a.f_spb_opclose,
										a.f_spb_cancel,
										a.i_spb_refference,
										a.i_spb_program,
										a.e_promo_name,
										a.d_spb_entry,
										coalesce(rata, 0) / notac1 as sttstelat,
										case
											when is_normal = 'f' then 'Melebihi Rata-rata Keterlambatan'
											else ''
										end as statusss,
										a.e_alasan_lanjut
                                       from
                                          (
                                          select
                                             a.f_spb_cancel,
                                             a.f_spb_opclose,
                                             a.i_notapprove,
                                             a.i_spb,
                                             a.d_spb,
                                             a.d_approve2,
                                             a.i_customer,
                                             b.e_customer_name,
                                             c.e_customer_classname,
                                             b.n_customer_toplength,
                                             a.i_salesman,
                                             d.e_salesman_name,
                                             e.i_product,
                                             e.e_product_name,
                                             g.e_product_seriname,
                                             i.e_product_classname as kategory,
                                             h.e_product_categoryname as subkategori,
                                             e.v_unit_price,
                                             e.n_order,
                                             e.n_deliver,
                                             e.v_unit_price * e.n_order as v_spb_gross,
                                             a.n_spb_discount1,
                                             a.n_spb_discount2,
                                             a.n_spb_discount3,
                                             (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc1,
                                             (e.n_order * e.v_unit_price * (1::numeric(20,
                                             2) - a.n_spb_discount1 / 100::numeric(20,
                                             2)) * (a.n_spb_discount2 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc2,
                                             (e.n_order * e.v_unit_price * (1::numeric(20,
                                             2) - a.n_spb_discount1 / 100::numeric(20,
                                             2)) * (1::numeric(20,
                                             2) - a.n_spb_discount2 / 100::numeric(20,
                                             2)) * (a.n_spb_discount3 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc3,
                                             j.i_nota,
                                             j.d_nota,
                                             k.n_deliver * k.v_unit_price as notakotor,
                                             j.n_nota_discount1,
                                             j.n_nota_discount2,
                                             j.n_nota_discount3,
                                             (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc1,
                                             (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                             2) - j.n_nota_discount1 / 100::numeric(20,
                                             2)) * (j.n_nota_discount2 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc2,
                                             (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                             2) - j.n_nota_discount1 / 100::numeric(20,
                                             2)) * (1::numeric(20,
                                             2) - j.n_nota_discount2 / 100::numeric(20,
                                             2)) * (j.n_nota_discount3 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc3,
                                             f.i_supplier,
                                             l.e_supplier_name,
                                             b.i_price_group,
                                             j.i_sj,
                                             j.d_sj,
                                             m.e_store_name,
                                             b.i_city,
                                             j.d_sj_receive,
                                             o.e_product_typename,
                                             q.e_provinsi,
                                             a.i_store,
                                             a.i_approve1,
                                             a.i_approve2,
                                             a.f_spb_stockdaerah,
                                             j.i_dkb,
                                             a.f_spb_siapnotasales,
                                             a.f_spb_siapnotagudang,
                                             a.i_spb_refference,
                                             a.i_spb_program,
                                             r.e_promo_name,
                                             a.d_spb_entry,
                                             case
                                                when a.v_spb > s.v_saldo then 'OVER PLAFOND'
                                                else ''
                                             end as sttsplafond,
                                             (u.d_alokasi - j.d_jatuh_tempo) as rata,
                                             v.nota_count as notac1,
                                             v.is_avg_normal as is_normal,
                                             j.e_alasan_lanjut
                                          from
                                             tm_spb a
                                          inner join tr_customer b on
                                             (a.i_customer = b.i_customer)
                                          inner join tr_customer_class c on
                                             (b.i_customer_class = c.i_customer_class)
                                          inner join tr_salesman d on
                                             (a.i_salesman = d.i_salesman)
                                          inner join tm_spb_item e on
                                             (a.i_spb = e.i_spb
                                                and a.i_area = e.i_area)
                                          inner join tr_product f on
                                             (e.i_product = f.i_product)
                                          left join tr_product_seri g on
                                             (f.i_product_seri = g.i_product_seri)
											LEFT JOIN tr_product_class i ON
												(f.i_product_class = i.i_product_class)
											LEFT JOIN tr_product_category h ON
												(i.i_product_class = h.i_product_class AND f.i_product_category = h.i_product_category)
                                          left join tm_nota j on
                                             (a.i_nota = j.i_nota
                                                and a.i_sj = j.i_sj
                                                and a.i_area = j.i_area)
                                          left join tm_nota_item k on
                                             (j.i_nota = k.i_nota
                                                and e.i_product = k.i_product)
                                          inner join tr_supplier l on
                                             (f.i_supplier = l.i_supplier)
                                          left join tr_store m on
                                             (a.i_store = m.i_store)
                                          left join tr_store_location n on
                                             (m.i_store = n.i_store
                                                and a.i_store_location = n.i_store_location)
                                          inner join tr_product_type o on
                                             (f.i_product_type = o.i_product_type)
                                          inner join tr_city p on
                                             (b.i_area = p.i_area
                                                and b.i_city = p.i_city)
                                          inner join tr_area q on
                                             (a.i_area = q.i_area)
                                          left join tm_promo r on
                                             (a.i_spb_program = r.i_promo)
                                          left join tr_customer_groupar s on
                                             (a.i_customer = s.i_customer)
                                          left join tm_alokasi_item t on
                                             (j.i_nota = t.i_nota)
                                          left join tm_alokasi u on
                                             (t.i_alokasi = u.i_alokasi
                                                and t.i_area = u.i_area)
                                          left join (
                                             select
                                                a.i_spb,
                                                a.i_customer,
                                                rata.e_customer_name,
                                                a.i_area,
                                                overfl.v_flapond,
                                                overfl.v_spb,
                                                overfl.v_saldo,
                                                coalesce(rata.n_customer_toplength, 9999) n_customer_toplength,
                                                coalesce(rata.sumketerlambatan, 9999) sumketerlambatan,
                                                coalesce(rata.nota_count, 1) nota_count,
                                                coalesce(rata.rata_keterlambatan, 9999) rata_keterlambatan,
                                                overfl.is_normal is_flapond_normal,
                                                coalesce(rata.is_normal, false) is_avg_normal ,
                                                case
                                                   when overfl.is_normal = 't'
                                                      and rata.is_normal = 't' then true
                                                      else false
                                                   end as all_normal
                                                from
                                                   tm_spb a
                                                left join(/*over flapond*/
                                                   select
                                                      i_spb,
                                                      i_customer,
                                                      v_flapond,
                                                      v_spb,
                                                      v_saldo,
                                                      case
                                                         when v_saldo > 0 then true
                                                         else false
                                                      end as is_normal
                                                   from
                                                      (
                                                      select
                                                         *,
                                                         v_flapond-(piutang + sum(v_spb) over(partition by i_customer
                                                      order by
                                                         i_customer,
                                                         id)) as v_saldo
                                                      from
                                                         (
                                                         select
                                                            row_number() over(partition by a.i_customer
                                                         order by
                                                            replace(a.i_spb, substring(i_spb, 10, 2), '00'),
                                                            a.i_customer,
                                                            a.d_spb) id,
                                                            case
                                                               when substring(i_spb, 10, 2) != '' then replace(a.i_spb, substring(i_spb, 10, 2), '00')
                                                            end as id_spb,
                                                            a.i_spb,
                                                            a.i_customer,
                                                            c.v_flapond,
                                                            coalesce(b.piutang, 0) piutang,
                                                            case
                                                               when (a.v_spb_after = '0'
                                                                  or a.v_spb_after is null) then sum(v_spb-v_spb_discounttotal)
                                                               else a.v_spb_after
                                                            end as v_spb,
                                                            a.d_spb
                                                         from
                                                            tm_spb a
                                                         left join (
                                                            select
                                                               i_customer,
                                                               i_area,
                                                               sum(v_sisa) as piutang
                                                            from
                                                               tm_nota
                                                            where
                                                               f_nota_cancel = 'f'
                                                               and i_nota is not null
                                                            group by
                                                               1,
                                                               2) b on
                                                            (a.i_customer = b.i_customer
                                                               and a.i_area = b.i_area)
                                                         left join tr_customer_groupar c on
                                                            (a.i_customer = c.i_customer
                                                               and a.i_area = substring(c.i_customer, 1, 2))
                                                         where
                                                            a.f_spb_cancel = 'f'
                                                            and a.d_spb >= '$datefrom'
                                                            and a.d_spb <= '$toto'
                                                            and a.i_area = '$iarea'
                                                            and a.i_nota is null
                                                            and substring(a.i_customer, 3, 3) != '000'
                                                         group by
                                                            2,
                                                            3,
                                                            4,
                                                            5,
                                                            6,
                                                            a.i_area
                                                         order by
                                                            a.i_customer,
                                                            a.d_spb,
                                                            2 ) x1 ) x2
                                                   order by
                                                      2,
                                                      1 ) overfl on
                                                   (a.i_customer = overfl.i_customer
                                                      and a.i_area = substring(overfl.i_customer, 1, 2)
                                                         and a.i_spb = overfl.i_spb)
                                                left join (
                                                   select
                                                      i_customer,
                                                      e_customer_name,
                                                      n_customer_toplength,
                                                      sumketerlambatan,
                                                      nota_count,
                                                      rata_keterlambatan,
                                                      case
                                                         when n_customer_toplength = 0 then false
                                                         when (n_customer_toplength >= 30
                                                            and n_customer_toplength <= 35)
                                                         and rata_keterlambatan <= 50 then true
                                                         when n_customer_toplength = 45
                                                         and rata_keterlambatan <= 60 then true
                                                         when n_customer_toplength = 60
                                                         and rata_keterlambatan <= 70 then true
                                                         else false
                                                      end as is_normal
                                                   from
                                                      (
                                                      select
                                                         i_customer,
                                                         e_customer_name,
                                                         n_customer_toplength,
                                                         sum(sumketerlambatan) sumketerlambatan,
                                                         sum(nota_count) nota_count,
                                                         round((sum(sumketerlambatan)/ sum(nota_count))) rata_keterlambatan
                                                      from
                                                         (
                                                         select
                                                            row_number() over(partition by i_customer
                                                         order by
                                                            substring(i_nota, 1, 7) desc) as rownumber,
                                                            i_customer,
                                                            e_customer_name,
                                                            n_customer_toplength ,
                                                            substring(i_nota, 1, 7) inota ,
                                                            sum(1) as nota_count ,
                                                            sum(d_alokasi-d_sj_receive-n_customer_toplength) as sumketerlambatan
                                                         from
                                                            (
                                                            select
                                                               a.i_customer,
                                                               c.e_customer_name,
                                                               a.i_nota,
                                                               c.n_customer_toplength,
                                                               a.v_sisa,
                                                               b.d_alokasi,
                                                               case
                                                                  when a.d_sj_receive is null then a.d_nota
                                                                  when a.d_sj_receive is not null then a.d_sj_receive
                                                               end as d_sj_receive
                                                            from
                                                               tm_nota a
                                                            inner join (
                                                               select
                                                                  pli.i_alokasi,
                                                                  pli.i_area,
                                                                  pli.i_nota,
                                                                  pli.v_jumlah,
                                                                  pl.i_giro,
                                                                  pl.d_alokasi
                                                               from
                                                                  tm_alokasi_item pli
                                                               inner join tm_alokasi pl on
                                                                  (pli.i_alokasi = pl.i_alokasi
                                                                     and pli.i_area = pl.i_area) ) b on
                                                               (a.i_nota = b.i_nota
                                                                  and a.i_area = b.i_area)
                                                            left join tr_customer c on
                                                               (a.i_customer = c.i_customer)
                                                            where
                                                               f_nota_cancel = 'f'
                                                               and c.f_customer_aktif = 't'
                                                            order by
                                                               1,
                                                               3 ) x1
                                                         group by
                                                            2,
                                                            3,
                                                            4,
                                                            5 ) x2
                                                      where
                                                         rownumber <= 6
                                                      group by
                                                         1,
                                                         2,
                                                         3 ) x3 ) rata on
                                                   (a.i_customer = rata.i_customer
                                                      and a.i_area = substring(rata.i_customer, 1, 2))
                                                where
                                                   a.f_spb_cancel = 'f'
                                                   and a.d_spb >= '$datefrom'
                                                   and a.d_spb <= '$toto'
                                                   and a.i_area = '$iarea'
                                                   and a.i_customer not like '%000' order by a.i_spb, a.i_area) v on
                                             (a.i_spb = v.i_spb
                                                and a.i_area = v.i_area)
                                          where
                                             a.d_spb >= '$datefrom'
                                             and a.d_spb <= '$toto'
                                             and a.i_area = '$iarea' order by a.i_spb, a.i_area) as a
                                       order by
                                          a.i_nota
                                     ", false);
      } else {
         return $this->db->query(" SELECT
									0 as nol,
                                    a.i_spb,
                                    a.d_spb,
                                    a.d_approve2,
                                    a.i_customer,
                                    a.e_customer_name,
                                    a.e_customer_classname,
                                    a.n_customer_toplength,
                                    a.i_salesman,
                                    a.e_salesman_name,
                                    a.i_product,
                                    a.e_product_name,
                                    a.e_product_seriname,
                                    a.kategory,
                                    a.subkategori,
                                    a.v_unit_price,
                                    a.n_order,
                                    a.n_deliver,
                                    a.v_spb_gross as spbkotor,
                                    a.n_spb_discount1,
                                    a.n_spb_discount2,
                                    a.n_spb_discount3,
                                    a.v_spb_disc1,
                                    a.v_spb_disc2,
                                    a.v_spb_disc3,
                                    (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
                                    a.i_nota,
                                    a.d_nota,
                                    a.notakotor,
                                    a.n_nota_discount1,
                                    a.n_nota_discount2,
                                    a.n_nota_discount3,
                                    a.v_nota_disc1,
                                    a.v_nota_disc2,
                                    a.v_nota_disc3,
                                    (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
                                    a.i_supplier,
                                    a.e_supplier_name,
                                    a.i_price_group,
                                    a.i_sj,
                                    a.d_sj,
                                    a.e_store_name,
                                    a.i_city,
                                    a.d_sj_receive,
                                    a.e_product_typename,
                                    a.e_provinsi,
                                    a.i_store,
                                    a.i_approve1,
                                    a.i_approve2,
                                    a.f_spb_stockdaerah,
                                    a.i_dkb,
                                    a.f_spb_siapnotasales,
                                    a.f_spb_siapnotagudang,
                                    a.i_notapprove,
                                    a.f_spb_opclose,
                                    a.f_spb_cancel,
                                    a.i_spb_refference,
                                    a.i_spb_program,
                                    a.e_promo_name,
                                    a.d_spb_entry,
                                    coalesce(rata, 0) / notac1 as sttstelat,
                                    case
                                       when is_normal = 'f' then 'Melebihi Rata-rata Keterlambatan'
                                       else ''
                                    end as statusss,
                                    a.e_alasan_lanjut
                                 from
                                    (
                                    select
                                       a.f_spb_cancel,
                                       a.f_spb_opclose,
                                       a.i_notapprove,
                                       a.i_spb,
                                       a.d_spb,
                                       a.d_approve2,
                                       a.i_customer,
                                       b.e_customer_name,
                                       c.e_customer_classname,
                                       b.n_customer_toplength,
                                       a.i_salesman,
                                       d.e_salesman_name,
                                       e.i_product,
                                       e.e_product_name,
                                       g.e_product_seriname,
                                       i.e_product_classname as kategory,
                                       h.e_product_categoryname as subkategori,
                                       e.v_unit_price,
                                       e.n_order,
                                       e.n_deliver,
                                       e.v_unit_price * e.n_order as v_spb_gross,
                                       a.n_spb_discount1,
                                       a.n_spb_discount2,
                                       a.n_spb_discount3,
                                       (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc1,
                                       (e.n_order * e.v_unit_price * (1::numeric(20,
                                       2) - a.n_spb_discount1 / 100::numeric(20,
                                       2)) * (a.n_spb_discount2 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc2,
                                       (e.n_order * e.v_unit_price * (1::numeric(20,
                                       2) - a.n_spb_discount1 / 100::numeric(20,
                                       2)) * (1::numeric(20,
                                       2) - a.n_spb_discount2 / 100::numeric(20,
                                       2)) * (a.n_spb_discount3 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc3,
                                       j.i_nota,
                                       j.d_nota,
                                       k.n_deliver * k.v_unit_price as notakotor,
                                       j.n_nota_discount1,
                                       j.n_nota_discount2,
                                       j.n_nota_discount3,
                                       (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc1,
                                       (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                       2) - j.n_nota_discount1 / 100::numeric(20,
                                       2)) * (j.n_nota_discount2 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc2,
                                       (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                       2) - j.n_nota_discount1 / 100::numeric(20,
                                       2)) * (1::numeric(20,
                                       2) - j.n_nota_discount2 / 100::numeric(20,
                                       2)) * (j.n_nota_discount3 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc3,
                                       f.i_supplier,
                                       l.e_supplier_name,
                                       b.i_price_group,
                                       j.i_sj,
                                       j.d_sj,
                                       m.e_store_name,
                                       b.i_city,
                                       j.d_sj_receive,
                                       o.e_product_typename,
                                       q.e_provinsi,
                                       a.i_store,
                                       a.i_approve1,
                                       a.i_approve2,
                                       a.f_spb_stockdaerah,
                                       j.i_dkb,
                                       a.f_spb_siapnotasales,
                                       a.f_spb_siapnotagudang,
                                       a.i_spb_refference,
                                       a.i_spb_program,
                                       r.e_promo_name,
                                       a.d_spb_entry,
                                       case
                                          when a.v_spb > s.v_saldo then 'OVER PLAFOND'
                                          else ''
                                       end as sttsplafond,
                                       (u.d_alokasi - j.d_jatuh_tempo) as rata,
                                       v.nota_count as notac1,
                                       v.is_avg_normal as is_normal,
                                       j.e_alasan_lanjut
                                    from
                                       tm_spb a
                                    inner join tr_customer b on
                                       (a.i_customer = b.i_customer)
                                    inner join tr_customer_class c on
                                       (b.i_customer_class = c.i_customer_class)
                                    inner join tr_salesman d on
                                       (a.i_salesman = d.i_salesman)
                                    inner join tm_spb_item e on
                                       (a.i_spb = e.i_spb
                                          and a.i_area = e.i_area)
                                    inner join tr_product f on
                                       (e.i_product = f.i_product)
                                    left join tr_product_seri g on
                                       (f.i_product_seri = g.i_product_seri)
									LEFT JOIN tr_product_class i ON
										(f.i_product_class = i.i_product_class)
									LEFT JOIN tr_product_category h ON
										(i.i_product_class = h.i_product_class AND f.i_product_category = h.i_product_category)
                                    left join tm_nota j on
                                       (a.i_nota = j.i_nota
                                          and a.i_sj = j.i_sj
                                          and a.i_area = j.i_area)
                                    left join tm_nota_item k on
                                       (j.i_nota = k.i_nota
                                          and e.i_product = k.i_product)
                                    inner join tr_supplier l on
                                       (f.i_supplier = l.i_supplier)
                                    left join tr_store m on
                                       (a.i_store = m.i_store)
                                    left join tr_store_location n on
                                       (m.i_store = n.i_store
                                          and a.i_store_location = n.i_store_location)
                                    inner join tr_product_type o on
                                       (f.i_product_type = o.i_product_type)
                                    inner join tr_city p on
                                       (b.i_area = p.i_area
                                          and b.i_city = p.i_city)
                                    inner join tr_area q on
                                       (a.i_area = q.i_area)
                                    left join tm_promo r on
                                       (a.i_spb_program = r.i_promo)
                                    left join tr_customer_groupar s on
                                       (a.i_customer = s.i_customer)
                                    left join tm_alokasi_item t on
                                       (j.i_nota = t.i_nota)
                                    left join tm_alokasi u on
                                       (t.i_alokasi = u.i_alokasi
                                          and t.i_area = u.i_area)
                                    left join (
                                       select
                                          a.i_spb,
                                          a.i_customer,
                                          rata.e_customer_name,
                                          a.i_area,
                                          overfl.v_flapond,
                                          overfl.v_spb,
                                          overfl.v_saldo,
                                          coalesce(rata.n_customer_toplength, 9999) n_customer_toplength,
                                          coalesce(rata.sumketerlambatan, 9999) sumketerlambatan,
                                          coalesce(rata.nota_count, 1) nota_count,
                                          coalesce(rata.rata_keterlambatan, 9999) rata_keterlambatan,
                                          overfl.is_normal is_flapond_normal,
                                          coalesce(rata.is_normal, false) is_avg_normal ,
                                          case
                                             when overfl.is_normal = 't'
                                                and rata.is_normal = 't' then true
                                                else false
                                             end as all_normal
                                          from
                                             tm_spb a
                                          left join(/*over flapond*/
                                             select
                                                i_spb,
                                                i_customer,
                                                v_flapond,
                                                v_spb,
                                                v_saldo,
                                                case
                                                   when v_saldo > 0 then true
                                                   else false
                                                end as is_normal
                                             from
                                                (
                                                select
                                                   *,
                                                   v_flapond-(piutang + sum(v_spb) over(partition by i_customer
                                                order by
                                                   i_customer,
                                                   id)) as v_saldo
                                                from
                                                   (
                                                   select
                                                      row_number() over(partition by a.i_customer
                                                   order by
                                                      replace(a.i_spb, substring(i_spb, 10, 2), '00'),
                                                      a.i_customer,
                                                      a.d_spb) id,
                                                      case
                                                         when substring(i_spb, 10, 2) != '' then replace(a.i_spb, substring(i_spb, 10, 2), '00')
                                                      end as id_spb,
                                                      a.i_spb,
                                                      a.i_customer,
                                                      c.v_flapond,
                                                      coalesce(b.piutang, 0) piutang,
                                                      case
                                                         when (a.v_spb_after = '0'
                                                            or a.v_spb_after is null) then sum(v_spb-v_spb_discounttotal)
                                                         else a.v_spb_after
                                                      end as v_spb,
                                                      a.d_spb
                                                   from
                                                      tm_spb a
                                                   left join (
                                                      select
                                                         i_customer,
                                                         i_area,
                                                         sum(v_sisa) as piutang
                                                      from
                                                         tm_nota
                                                      where
                                                         f_nota_cancel = 'f'
                                                         and i_nota is not null
                                                      group by
                                                         1,
                                                         2) b on
                                                      (a.i_customer = b.i_customer
                                                         and a.i_area = b.i_area)
                                                   left join tr_customer_groupar c on
                                                      (a.i_customer = c.i_customer
                                                         and a.i_area = substring(c.i_customer, 1, 2))
                                                   where
                                                      a.f_spb_cancel = 'f'
                                                      and a.d_spb >= '$datefrom'
                                                      and a.d_spb <= '$toto'
                                                      and a.i_nota is null
                                                      and substring(a.i_customer, 3, 3) != '000'
                                                   group by
                                                      2,
                                                      3,
                                                      4,
                                                      5,
                                                      6,
                                                      a.i_area
                                                   order by
                                                      a.i_customer,
                                                      a.d_spb,
                                                      2 ) x1 ) x2
                                             order by
                                                2,
                                                1 ) overfl on
                                             (a.i_customer = overfl.i_customer
                                                and a.i_area = substring(overfl.i_customer, 1, 2)
                                                   and a.i_spb = overfl.i_spb)
                                          left join (
                                             select
                                                i_customer,
                                                e_customer_name,
                                                n_customer_toplength,
                                                sumketerlambatan,
                                                nota_count,
                                                rata_keterlambatan,
                                                case
                                                   when n_customer_toplength = 0 then false
                                                   when (n_customer_toplength >= 30
                                                      and n_customer_toplength <= 35)
                                                   and rata_keterlambatan <= 50 then true
                                                   when n_customer_toplength = 45
                                                   and rata_keterlambatan <= 60 then true
                                                   when n_customer_toplength = 60
                                                   and rata_keterlambatan <= 70 then true
                                                   else false
                                                end as is_normal
                                             from
                                                (
                                                select
                                                   i_customer,
                                                   e_customer_name,
                                                   n_customer_toplength,
                                                   sum(sumketerlambatan) sumketerlambatan,
                                                   sum(nota_count) nota_count,
                                                   round((sum(sumketerlambatan)/ sum(nota_count))) rata_keterlambatan
                                                from
                                                   (
                                                   select
                                                      row_number() over(partition by i_customer
                                                   order by
                                                      substring(i_nota, 1, 7) desc) as rownumber,
                                                      i_customer,
                                                      e_customer_name,
                                                      n_customer_toplength ,
                                                      substring(i_nota, 1, 7) inota ,
                                                      sum(1) as nota_count ,
                                                      sum(d_alokasi-d_sj_receive-n_customer_toplength) as sumketerlambatan
                                                   from
                                                      (
                                                      select
                                                         a.i_customer,
                                                         c.e_customer_name,
                                                         a.i_nota,
                                                         c.n_customer_toplength,
                                                         a.v_sisa,
                                                         b.d_alokasi,
                                                         case
                                                            when a.d_sj_receive is null then a.d_nota
                                                            when a.d_sj_receive is not null then a.d_sj_receive
                                                         end as d_sj_receive
                                                      from
                                                         tm_nota a
                                                      inner join (
                                                         select
                                                            pli.i_alokasi,
                                                            pli.i_area,
                                                            pli.i_nota,
                                                            pli.v_jumlah,
                                                            pl.i_giro,
                                                            pl.d_alokasi
                                                         from
                                                            tm_alokasi_item pli
                                                         inner join tm_alokasi pl on
                                                            (pli.i_alokasi = pl.i_alokasi
                                                               and pli.i_area = pl.i_area) ) b on
                                                         (a.i_nota = b.i_nota
                                                            and a.i_area = b.i_area)
                                                      left join tr_customer c on
                                                         (a.i_customer = c.i_customer)
                                                      where
                                                         f_nota_cancel = 'f'
                                                         and c.f_customer_aktif = 't'
                                                      order by
                                                         1,
                                                         3 ) x1
                                                   group by
                                                      2,
                                                      3,
                                                      4,
                                                      5 ) x2
                                                where
                                                   rownumber <= 6
                                                group by
                                                   1,
                                                   2,
                                                   3 ) x3 ) rata on
                                             (a.i_customer = rata.i_customer
                                                and a.i_area = substring(rata.i_customer, 1, 2))
                                          where
                                             a.f_spb_cancel = 'f'
                                             and a.d_spb >= '$datefrom'
                                             and a.d_spb <= '$toto'
                                             and a.i_customer not like '%000' order by a.i_spb, a.i_area) v on
                                       (a.i_spb = v.i_spb
                                          and a.i_area = v.i_area)
                                    where
                                       a.d_spb >= '$datefrom'
                                       and a.d_spb <= '$toto'
                                       order by a.i_spb, a.i_area ) as a
                                 order by
                                    a.i_nota
                                 ", false);
      }
   }

   /* BIKIN ULANG 06 APR 2023 */
   function bacasemua($iarea, $datefrom, $toto) /* EXC BACA ALOKASI UNTUK CARI LAMA BAYAR */
   {
      $sql = $iarea != "NA" ? " AND spb.i_area = '$iarea' " : "";

      return $this->db->query("  SELECT
                                    0 as nol,
                                    a.*,
                                    a.v_spb_gross as spbkotor,
                                    (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
                                    (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
                                    0 as sttstelat,
                                    case
                                       when a.is_normal = 'f' then 'Melebihi Rata-rata Keterlambatan'
                                       else ''
                                    end as statusss,
                                    a.e_alasan_lanjut,
                                    CASE
                                       WHEN f_spb_cancel = 't' THEN 'Batal'
                                       WHEN i_approve1 IS NULL AND i_notapprove IS NULL THEN 'Sales'
                                       WHEN i_approve1 IS NULL AND NOT i_notapprove IS NULL THEN 'Reject (sls)'
                                       WHEN NOT i_approve1 IS NULL AND i_approve2 IS NULL AND i_notapprove IS NULL THEN 'Keuangan'
                                       WHEN NOT i_approve1 IS NULL AND i_approve2 IS NULL AND NOT i_notapprove IS null THEN 'Reject (ar)'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND i_store IS NULL THEN 'Gudang'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 'f' AND f_spb_op = 'f' THEN 'Pemenuhan SPB'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 'f' AND f_spb_op = 't' AND f_spb_opclose = 'f' THEN 'Proses OP'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 'f' AND f_spb_siapnotasales = 'f' AND f_spb_opclose = 't' THEN 'OP Close'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 't' AND f_spb_siapnotasales = 'f' THEN 'Siap SJ (sales)'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 't' AND f_spb_siapnotasales = 't' AND i_sj IS NULL THEN 'Siap SJ'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND i_dkb IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 't' AND f_spb_siapnotasales = 't' AND NOT i_sj IS NULL THEN 'Siap DKB'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_dkb IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 'f' AND f_spb_siapnotagudang = 't' AND f_spb_siapnotasales = 't' AND NOT i_sj IS NULL THEN 'Siap Nota'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 't' AND i_sj IS NULL THEN 'Siap SJ'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND i_dkb IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 't' AND NOT i_sj IS NULL THEN 'Siap DKB'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_dkb IS NULL AND NOT i_store IS NULL AND i_nota IS NULL AND f_spb_stockdaerah = 't' AND NOT i_sj IS NULL THEN 'Siap Nota'
                                       WHEN NOT i_approve1 IS NULL AND NOT i_approve2 IS NULL AND NOT i_store IS NULL AND NOT i_nota IS NULL THEN 'Sudah dinotakan'
                                       WHEN NOT i_nota IS NULL THEN 'Sudah dinotakan'
                                    ELSE 'Unknown' END AS status
                                 FROM (
                                    SELECT 
                                       spb.*,
                                       nt.i_dkb,
                                       cst.e_customer_name,
                                       ccl.e_customer_classname,
                                       cst.n_customer_toplength,
                                       sls.e_salesman_name, 
                                       spbdet.i_product,
                                       prd.e_product_name,
                                       prdtyp.e_product_typename,
                                       sri.e_product_seriname,
                                       cls.e_product_classname AS kategory,
                                       ctg.e_product_categoryname AS subkategori,
                                       spbdet.n_order,
                                       nti.n_deliver,
                                       spbdet.v_unit_price,
                                       COALESCE (spbdet.n_order,0) AS n_spb_order,
                                       COALESCE (spbdet.n_deliver,0) AS n_spb_deliver,
                                       ((spbdet.n_order * spbdet.v_unit_price)) AS v_spb_gross,
                                       ((spbdet.n_order * spbdet.v_unit_price) * spb.n_spb_discount1 / 100) AS v_spb_disc1,
                                       (((spbdet.n_order * spbdet.v_unit_price)-((spbdet.n_order * spbdet.v_unit_price) * spb.n_spb_discount1 / 100)) * spb.n_spb_discount2 / 100) AS v_spb_disc2,
                                       (((spbdet.n_order * spbdet.v_unit_price)-((spbdet.n_order * spbdet.v_unit_price) * spb.n_spb_discount1 / 100)-(((spbdet.n_order * spbdet.v_unit_price)-((spbdet.n_order * spbdet.v_unit_price) * spb.n_spb_discount1 / 100)) * spb.n_spb_discount2 / 100)) * spb.n_spb_discount3 / 100) AS v_spb_disc3,
                                       nt.d_nota,
                                       COALESCE(round((nti.n_deliver * nti.v_unit_price)),0) AS notakotor,
                                       nt.n_nota_discount1,
                                       nt.n_nota_discount2,
                                       nt.n_nota_discount3,
                                       COALESCE(round((nti.n_deliver * nti.v_unit_price) * nt.n_nota_discount1 / 100),0) AS v_nota_disc1,
                                       COALESCE(round(round((nti.n_deliver * nti.v_unit_price)-((nti.n_deliver * nti.v_unit_price) * nt.n_nota_discount1 / 100)) * nt.n_nota_discount2 / 100),0) AS v_nota_disc2,
                                       COALESCE(round(round((nti.n_deliver * nti.v_unit_price)-round((nti.n_deliver * nti.v_unit_price) * nt.n_nota_discount1 / 100)-(((nti.n_deliver * nti.v_unit_price)-((nti.n_deliver * nti.v_unit_price) * nt.n_nota_discount1 / 100)) * nt.n_nota_discount2 / 100)) * nt.n_nota_discount3 / 100),0) AS v_nota_disc3,
                                       prd.i_supplier,
                                       spp.e_supplier_name,
                                       CASE WHEN spb.f_spb_stockdaerah = 't' THEN 'CABANG' ELSE 'PUSAT' END AS pemenuhan,
                                       cst.i_city,
                                       nt.d_sj_receive,
                                       ctg.e_product_categoryname,
                                       ar.e_provinsi,
                                       nt.e_alasan_lanjut,
                                       prm.e_promo_name,
                                       case
                                          when spb.v_spb > cstar.v_saldo then 'OVER PLAFOND'
                                          else ''
                                       end as sttsplafond,
                                       xx.nota_count as notac1,
                                       xx.is_normal
                                    FROM
                                       tm_spb spb
                                       INNER JOIN tm_spb_item spbdet ON spb.i_spb = spbdet.i_spb AND spb.i_area = spbdet.i_area
                                       LEFT JOIN tm_nota nt ON spb.i_spb = nt.i_spb AND spb.i_area = nt.i_area AND nt.f_nota_cancel = 'f'
                                       LEFT JOIN tm_nota_item nti ON nt.i_sj = nti.i_sj AND nt.i_area = nti.i_area AND spbdet.i_product = nti.i_product
                                       INNER JOIN tr_customer cst ON spb.i_customer = cst.i_customer AND spb.i_area = cst.i_area
                                       INNER JOIN tr_customer_class ccl ON cst.i_customer_class = ccl.i_customer_class
                                       INNER JOIN tr_customer_groupar cstar on (cst.i_customer = cstar.i_customer)
                                       INNER JOIN tr_salesman sls ON spb.i_salesman = sls.i_salesman
                                       INNER JOIN tr_product prd ON spbdet.i_product = prd.i_product
                                       INNER JOIN tr_product_type prdtyp ON prd.i_product_type = prdtyp.i_product_type
                                       LEFT JOIN tr_product_seri sri ON prd.i_product_seri = sri.i_product_seri
                                       LEFT JOIN tr_product_class cls ON prd.i_product_class = cls.i_product_class
                                       LEFT JOIN tr_product_category ctg ON cls.i_product_class = ctg.i_product_class AND prd.i_product_category = ctg.i_product_category 
                                       LEFT JOIN tr_supplier spp ON prd.i_supplier = spp.i_supplier
                                       INNER JOIN tr_area ar ON cst.i_area = ar.i_area 
                                       LEFT JOIN tm_promo prm ON spb.i_spb_program = prm.i_promo 
                                       LEFT JOIN f_spb_ovr('$datefrom', '$toto', '$iarea') xx ON (spb.i_spb = xx.i_spb and spb.i_area = xx.i_area)
                                    WHERE 
                                       spb.d_spb BETWEEN to_date('$datefrom','dd-mm-yyyy') AND to_date('$toto','dd-mm-yyyy')
                                       AND spb.f_spb_cancel = 'f'
                                       $sql
                                    ORDER BY
                                       spb.i_spb,
                                       spb.i_area
                                 ) a
                                 ORDER BY a.i_nota ");
   }

   function bacaarea($num, $offset, $iuser)
   {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }

   function cariarea($cari, $num, $offset, $iuser)
   {
      $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
         return $query->result();
      }
   }
}
