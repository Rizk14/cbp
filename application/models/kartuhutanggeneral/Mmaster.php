<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jumsaldoawal($iperiode)
    {
		$this->db->select(" SUM(v_saldo_awal) as jumsaldoawal from tm_kh_general where e_periode='$iperiode' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jumdebet($iperiode)
    {
		$this->db->select(" SUM(v_debet) as jumdebet from tm_kh_general where e_periode='$iperiode' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jumkredit($iperiode)
    {
		$this->db->select(" SUM(v_kredit) as jumkredit from tm_kh_general where e_periode='$iperiode' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function jumsaldoakhir($iperiode)
    {
		$this->db->select(" SUM(v_saldo_akhir) as jumsaldoakhir from tm_kh_general where e_periode='$iperiode' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode,$num,$offset,$cari)
    {
/*
		                      union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_pelunasanap a
			                      where a.f_pelunasanap_cancel='f'
			                      and to_char(a.d_bukti,'yyyymm')<'$iperiode'
			                      and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
			                    union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_alokasi_bk a
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')<'$iperiode'
			                      group by a.i_supplier
  			                  union all
			                      select a.i_supplier, 0 as nota, sum(v_jumlah) as pl
			                      from tm_alokasi_kb a
			                      where a.f_alokasi_cancel='f'
			                      and to_char(a.d_alokasi,'yyyymm')<'$iperiode'
			                      group by a.i_supplier
*/    
      $this->db->select(" a.*, b.e_supplier_name from tm_kh_general a, tr_supplier b
                          where a.e_periode='$iperiode' and a.i_supplier=b.i_supplier
                          and (v_saldo_awal<>0 or v_debet<>0 or v_kredit<>0 or v_saldo_akhir<>0)
                          order by b.i_supplier",false);#->limit($num,$offset);
/*
      $this->db->select(" i_supplier, sum(saldo) as saldo, sum(nota) as nota, sum(pl) as pl from(
                          select i_supplier, sum(sisa) as saldo, 0 as nota,0 as pl from(
                          select i_supplier, sum(nota) as nota, sum(pl) as pl, sum(sisa) as sisa from(
                          select i_supplier, sum(a.v_netto) as nota, 0 as pl, sum(a.v_sisa) as sisa 
                          from tm_dtap a
                          where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')< '$iperiode'
                          group by a.i_supplier
                          union all
                          select a.i_supplier, 0 as nota, 0 as pl, sum(v_jumlah) as sisa
                          from tm_pelunasanap a
                          where a.f_pelunasanap_cancel='f'
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
                          ) as a group by i_supplier
                          ) as b group by i_supplier
                          union all
                          select i_supplier, 0 as saldo, sum(nota) as nota, sum(pl) as pl from(
                          select i_supplier, sum(nota) as nota, sum(pl) as pl, sum(sisa) as sisa from(
                          select a.i_supplier, sum(a.v_netto) as nota, 0 as pl, sum(a.v_sisa) as sisa
                          from tm_dtap a
                          where f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
                          group by a.i_supplier
                          union all
                          select a.i_supplier, 0 as nota, sum(v_jumlah) as pl, 0 as sisa
                          from tm_pelunasanap a
                          where a.f_pelunasanap_cancel='f'
                          and to_char(a.d_bukti,'yyyymm')='$iperiode'
                          and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' group by a.i_supplier
                          ) as a group by i_supplier
                          ) as b group by i_supplier
                          ) as c
                          where saldo<>0 or nota>0 or pl>0
                          group by i_supplier
                          order by i_supplier",false);#->limit($num,$offset);
*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function bacadetail($isupplier,$iperiode,$num,$offset)
    {
      $this->db->select(" * from(
                          select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_dtap as tglbukti, a.i_dtap as bukti, 
                          '' as jenis, '1. pembelian' as keterangan, a.v_netto as debet, 0 as kredit
                          from tm_dtap a, tr_supplier c
                          where f_dtap_cancel='f' and a.i_supplier=c.i_supplier and to_char(a.d_dtap, 'yyyymm')='$iperiode'
                          and a.i_supplier='$isupplier'
                          union all
                          select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_bukti as tglbukti, a.i_pelunasanap as bukti, 
                          a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 0 as debet, a.v_jumlah as kredit
                          from tm_pelunasanap a, tr_supplier c, tr_jenis_bayar d
                          where a.f_pelunasanap_cancel='f' and to_char(d_bukti, 'yyyymm')='$iperiode'
                          and a.i_supplier=c.i_supplier and a.i_jenis_bayar=d.i_jenis_bayar 
                          and a.i_supplier='$isupplier'
                          
                          union all
                          select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti, 
                          '' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
                          from tm_alokasi_bk a, tr_supplier c
                          where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
                          and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
                          union all
                          select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti,
                          '' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
                          from tm_alokasi_kb a, tr_supplier c
                          where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
                          and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
                          ) as a order by supplier, tglbukti, keterangan, bukti",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
