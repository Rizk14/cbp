<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($drrkh,$isalesman,$iarea)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_salesman_name from tm_rrkh a, tr_area b, tr_salesman c
						   where a.i_area=b.i_area and a.i_salesman=c.i_salesman
						   and a.d_rrkh ='$drrkh' and a.i_salesman='$isalesman' and a.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($drrkh,$isalesman,$iarea)
    {
		$this->db->select(" a.*, b.e_kunjungan_typename, c.e_customer_name, d.e_city_name 
												from tr_kunjungan_type b, tr_customer c, tm_rrkh_item a
                                    left join tr_city d on(a.i_city=d.i_city and a.i_area=d.i_area)
												where a.d_rrkh = '$drrkh' and a.i_salesman='$isalesman' and a.i_area='$iarea'
												and a.i_kunjungan_type=b.i_kunjungan_type and a.i_customer=c.i_customer
												order by a.d_rrkh, a.i_area, a.i_salesman, a.i_customer", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($isalesman, $drrkh, $iarea,$drec1)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
		  $userid = $this->session->userdata("user_id");
      if($drec1!=''){
      		$this->db->set(
      			array(
			  'i_salesman'=> $isalesman,
			  'd_rrkh'=> $drrkh,
			  'i_area'=> $iarea,
			  'd_entry'=> $dentry,
			  'i_entry'=> $userid,
        'd_receive1'=> $drec1));
      	$this->db->insert('tm_rrkh');
      }else{
      		$this->db->set(
      			array(
			  'i_salesman'=> $isalesman,
			  'd_rrkh'=> $drrkh,
			  'i_area'=> $iarea,
			  'd_entry'=> $dentry,
			  'i_entry'=> $userid));
      	$this->db->insert('tm_rrkh');
      }
    }
    function insertdetail($isalesman,$drrkh,$iarea,$icustomer,$ikunjungantype,$icity,$fkunjunganrealisasi,$fkunjunganvalid,$eremark,$i)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$userid = $this->session->userdata("user_id");
		if($eremark=='') $eremark=null;
    		$this->db->set(
    		array('d_rrkh'        => $drrkh,
			'i_salesman'            => $isalesman,
			'i_area'                => $iarea,
			'i_customer'            => $icustomer,
			'i_kunjungan_type'      => $ikunjungantype,
			'i_city'                => $icity,
			'f_kunjungan_realisasi'	=> $fkunjunganrealisasi,
			'f_kunjungan_valid'     => $fkunjunganvalid,
			'e_remark'              => $eremark,
			'f_tagihan'             => $ftagihan,
			'i_nota'          	    => $inota,
			'e_cash'          	    => $ecash,
			'e_giro'          	    => $egiro,
			'e_ku'            		  => $eku,
			'd_entry'               => $dentry,
			'i_entry'               => $userid,
      'n_item_no'             => $i
    		)
    	);
    	
    	$this->db->insert('tm_rrkh_item');
    }
    function updateheader($drrkh,$isalesman,$iarea,$dreceive1)
    {
    	$data	= array('d_receive1'=>$dreceive1);
    	$this->db->update('tm_rrkh',$data,array('i_area'=>$iarea,'i_salesman'=>$isalesman,'d_rrkh'=>$drrkh));
    }
    public function deleteheader($drrkh, $isalesman, $iarea) 
    {
		$this->db->query("DELETE FROM tm_rrkh WHERE d_rrkh='$drrkh'
										and i_salesman='$isalesman' 
										and i_area='$iarea'");
    }
    public function deletedetail($icustomer, $isalesman, $drrkh, $iarea) 
    {
		$this->db->query("DELETE FROM tm_rrkh_item WHERE d_rrkh='$drrkh'
										and i_customer='$icustomer' and i_salesman='$isalesman' 
										and i_area='$iarea'");
    }
    function bacasemua()
    {
		$this->db->select(" * from tm_rrkh order by d_rrkh, i_area, i_salesman desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($num,$offset,$area)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select * from(
                              select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer a, tr_city b where a.i_area='$area'
                              and a.i_area=b.i_area and a.i_city=b.i_city
                              union all
                              select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer_tmp a 
                              left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                              where a.i_area='$area' and a.i_customer like '%000'
                              ) as a
                              order by a.i_customer limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($thbl){
#    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
#		  $row   	= $query->row();
#		  $thbl	= $row->c;
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_rrkh,11,6)) as max from tm_rrkh 
				    			where substr(i_rrkh,6,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $norrkh  =$terakhir+1;
			  settype($norrkh,"string");
			  $a=strlen($norrkh);
			  while($a<6){
			    $norrkh="0".$norrkh;
			    $a=strlen($norrkh);
			  }
			  $norrkh  ="rrkh-".$thbl."-".$norrkh;
			  return $norrkh;
		  }else{
			  $norrkh  ="000001";
			  $norrkh  ="rrkh-".$thbl."-".$norrkh;
			  return $norrkh;
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_rrkh where upper(i_rrkh) like '%$cari%' 
					order by i_rrkh",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$num,$offset,$area)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select * from(
                                select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer a, tr_city b where a.i_area='$area'
                                and a.i_area=b.i_area and a.i_city=b.i_city 
                                and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                union all
                                select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer_tmp a 
                                left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                                where a.i_area='$area' and (a.i_customer like '%000' or upper(a.e_customer_name) like '%$cari%')
                                ) as a
                                order by a.i_customer desc limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacasalesman($num,$offset,$area)
    {
		$this->db->select("distinct(i_salesman),e_salesman_name from tr_customer_salesman where i_area='$area' order by i_salesman",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisalesman($cari,$num,$offset,$area)
    {
		$this->db->select("	distinct(i_salesman),e_salesman_name from tr_customer_salesman where i_area='$area' and 
 												(upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
												order by i_salesman",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacity($num,$offset,$area)
    {
		$this->db->select("i_city, e_city_name from tr_city where i_area='$area' order by e_city_name",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricity($cari,$num,$offset,$area)
    {
		$this->db->select("	i_city, e_city_name from tr_city
												where i_area='$area' and (upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%')
												order by e_city_name",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacakunjungan($num,$offset)
    {
		$this->db->select("i_kunjungan_type, e_kunjungan_typename from tr_kunjungan_type order by i_kunjungan_type",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carikunjungan($cari,$num,$offset)
    {
		$this->db->select("	i_kunjungan_type, e_kunjungan_typename from tr_kunjungan_type
												where upper(e_kunjungan_typename) like '%$cari%' or upper(i_kunjungan_type) like '%$cari%'
												order by i_kunjungan_type",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
