<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispb,$iarea)
    {
		$this->db->select(" * from tm_spb 
				   inner join tm_promo on (tm_spb.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer)
				   inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group)
				   where i_spb ='$ispb' and tm_spb.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispb,$iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b
				   where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
				   order by a.i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
						 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb, 
						 $fspbconsigment, $ispbprogram, $ispbold)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_spb'					=> $ispb,
			'd_spb'					=> $dspb,
			'i_customer'			=> $icustomer,
			'i_area'				=> $iarea,
			'i_spb_po'				=> $ispbpo,
			'i_spb_program'			=> $ispbprogram,
			'n_spb_toplength' 		=> $nspbtoplength,
			'i_salesman' 			=> $isalesman,
			'i_price_group' 		=> $ipricegroup,
			'd_spb_receive'			=> $dspb,
			'f_spb_op'				=> $fspbop,
			'e_customer_pkpnpwp'	=> $ecustomerpkpnpwp,
			'f_spb_pkp'				=> $fspbpkp,
			'f_spb_plusppn'			=> $fspbplusppn,
			'f_spb_plusdiscount'	=> $fspbplusdiscount,
			'f_spb_stockdaerah'		=> $fspbstockdaerah,
			'f_spb_program' 		=> $fspbprogram,
			'f_spb_valid' 			=> $fspbvalid,
			'f_spb_siapnotagudang'	=> $fspbsiapnota,
			'f_spb_cancel' 			=> $fspbcancel,
			'n_spb_discount1' 		=> $nspbdiscount1,
			'n_spb_discount2' 		=> $nspbdiscount2,
			'n_spb_discount3' 		=> $nspbdiscount3,
			'n_spb_discount4' 		=> $nspbdiscount4,
			'v_spb_discount1' 		=> $vspbdiscount1,
			'v_spb_discount2' 		=> $vspbdiscount2,
			'v_spb_discount3' 		=> $vspbdiscount3,
			'v_spb_discount4' 		=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 				=> $vspb,
			'f_spb_consigment' 		=> $fspbconsigment,
			'd_spb_entry'			=> $dentry,
			'i_spb_old'				=> $ispbold,
			'i_product_group'		=> '01'
    		)
    	);
    	
    	$this->db->insert('tm_spb');
    }
    function insertheadersjc($isjc, $dsjc, $ispb, $dspb, $icustomer, $iarea, $isalesman, 
							 $fsjccancel, $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3, 
							 $vsjcdiscount1, $vsjcdiscount2, $vsjcdiscount3, $vsjcgross,
							 $vsjcdiscounttotal, $vsjcnetto, $isjctype)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_sjc'					=> $isjc,
			'i_sjc_type'			=> $isjctype,
			'd_sjc'					=> $dsjc,
			'i_spb'					=> $ispb,
			'd_spb'					=> $dspb,
			'i_customer'			=> $icustomer,
			'i_area'				=> $iarea,
			'i_salesman' 			=> $isalesman,
			'f_sjc_cancel' 			=> $fsjccancel,
			'n_sjc_discount1' 		=> $nsjcdiscount1,
			'n_sjc_discount2' 		=> $nsjcdiscount2,
			'n_sjc_discount3' 		=> $nsjcdiscount3,
			'v_sjc_discount1' 		=> $vsjcdiscount1,
			'v_sjc_discount2' 		=> $vsjcdiscount2,
			'v_sjc_discount3' 		=> $vsjcdiscount3,
			'v_sjc_gross'			=> $vsjcgross,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_netto'			=> $vsjcnetto,
			'd_sjc_entry'			=> $dentry
    		)
    	);
    	
    	$this->db->insert('tm_sjc');
    }
    function insertdetail($ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark)
    {
	if($eremark=='0') $eremark=null;
    	$this->db->set(
    		array(
					'i_spb'				=> $ispb,
					'i_area'			=> $iarea,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_order'			=> $norder,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'e_remark'			=> $eremark
    		)
    	);
    	$this->db->insert('tm_spb_item');
    }
    function insertdetailsjc($isjc,$iarea,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nquantity,$vunitprice,$isjctype)
    {
    	$this->db->set(
    		array(
					'i_sjc'				=> $isjc,
					'i_area'			=> $iarea,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'e_product_name'	=> $eproductname,
					'n_quantity'		=> $nquantity,
					'v_unit_price'		=> $vunitprice,
					'i_sjc_type'		=> $isjctype
    		)
    	);
    	$this->db->insert('tm_sjc_item');
    }
    function updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
						 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold)
    {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dspbupdate	= $row->c;
    	$data = array(
			'd_spb'			=> $dspb,
			'i_customer'		=> $icustomer,
			'i_spb_po'		=> $ispbpo,
			'n_spb_toplength' 	=> $nspbtoplength,
			'i_salesman' 		=> $isalesman,
			'i_price_group' 	=> $ipricegroup,
			'd_spb_receive'		=> $dspb,
			'f_spb_op'		=> $fspbop,
			'e_customer_pkpnpwp'	=> $ecustomerpkpnpwp,
			'f_spb_pkp'		=> $fspbpkp,
			'f_spb_plusppn'		=> $fspbplusppn,
			'f_spb_plusdiscount'	=> $fspbplusdiscount,
			'f_spb_stockdaerah'	=> $fspbstockdaerah,
			'f_spb_program' 	=> $fspbprogram,
			'f_spb_valid' 		=> $fspbvalid,
			'f_spb_siapnotagudang'	=> $fspbsiapnota,
			'f_spb_cancel' 		=> $fspbcancel,
			'n_spb_discount1' 	=> $nspbdiscount1,
			'n_spb_discount2' 	=> $nspbdiscount2,
			'n_spb_discount3' 	=> $nspbdiscount3,
			'n_spb_discount4' 	=> $nspbdiscount4,
			'v_spb_discount1' 	=> $vspbdiscount1,
			'v_spb_discount2' 	=> $vspbdiscount2,
			'v_spb_discount3' 	=> $vspbdiscount3,
			'v_spb_discount4' 	=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 		=> $vspb,
			'f_spb_consigment' 	=> $fspbconsigment,
			'd_spb_update'		=> $dspbupdate,
			'f_spb_cancel'		=> 'f',
			'i_notapprove'		=> null,
			'd_notapprove'		=> null,
			'i_spb_old'		=> $ispbold
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }
    function updateheadersjc($ispb, $iarea, $dsjc, $icustomer, $isalesman, $fsjccancel,
							 $nsjcdiscount1, $nsjcdiscount2, $nsjcdiscount3, $vsjcdiscount1, 
							 $vsjcdiscount2, $vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross, 
							 $vsjcnetto, $isjc, $isjctype)
    {
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dsjcupdate	= $row->c;
    	$data = array(
			'd_sjc'					=> $dsjc,
			'i_customer'			=> $icustomer,
			'i_salesman' 			=> $isalesman,
			'f_sjc_cancel' 			=> $fsjccancel,
			'n_sjc_discount1' 		=> $nsjcdiscount1,
			'n_sjc_discount2' 		=> $nsjcdiscount2,
			'n_sjc_discount3' 		=> $nsjcdiscount3,
			'v_sjc_discount1' 		=> $vsjcdiscount1,
			'v_sjc_discount2' 		=> $vsjcdiscount2,
			'v_sjc_discount3' 		=> $vsjcdiscount3,
			'v_sjc_gross'			=> $vsjcgross,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_netto'			=> $vsjcnetto,
			'd_sjc_update'			=> $dsjcupdate
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->where('i_sjc', $isjc);
		$this->db->where('i_sjc_type', $isjctype);
		$this->db->update('tm_sjc', $data); 
    }

	function uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb)
    {
    	$data = array(
			'v_spb_discount1' 		=> $vspbdiscount1,
			'v_spb_discount2' 		=> $vspbdiscount2,
			'v_spb_discount3' 		=> $vspbdiscount3,
			'v_spb_discount4' 		=> $vspbdiscount4,
			'v_spb_discounttotal'	=> $vspbdiscounttotal,
			'v_spb' 				=> $vspb
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data); 
    }

	function upheadsjc(	$ispb, $iarea, $isjc, $isjctype, $vsjcdiscount1, $vsjcdiscount2,
						$vsjcdiscount3, $vsjcdiscounttotal, $vsjcgross, $vsjcnetto)
    {
    	$data = array(
			'v_sjc_discount1' 		=> $vsjcdiscount1,
			'v_sjc_discount2' 		=> $vsjcdiscount2,
			'v_sjc_discount3' 		=> $vsjcdiscount3,
			'v_sjc_discounttotal'	=> $vsjcdiscounttotal,
			'v_sjc_gross'			=> $vsjcgross,
			'v_sjc_netto'			=> $vsjcnetto,
            );
		$this->db->where('i_spb', $ispb);
		$this->db->where('i_area', $iarea);
		$this->db->where('i_sjc', $isjc);
		$this->db->where('i_sjc_type', $isjctype);
		$this->db->update('tm_sjc', $data); 
    }

    public function deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
    }

    public function deletedetailsjc($isjc, $iarea, $isjctype, $iproduct, $iproductgrade, $iproductmotif) 
    {
		$this->db->query("DELETE FROM tm_sjc_item WHERE i_sjc='$isjc' and i_area='$iarea' and i_sjc_type='$isjctype'
									and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									and i_product_motif='$iproductmotif'");
		return TRUE;
    }
	
    public function delete($ispb, $iarea) 
    {
		$this->db->query("DELETE FROM tm_spb WHERE i_spb='$ispb' and i_area='$iarea'");
		$this->db->query("DELETE FROM tm_spb_item WHERE i_spb='$ispb' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua($iarea)
    {
		$this->db->select("* from tm_spb where i_area='$iarea' order by i_spb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$kdharga,$promo,$tipe)
    {
		if($offset=='')
			$offset=0;
		$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
		foreach($q->result() as $pro){
			$p	= $pro->f_all_product;
			$tipe	= $pro->i_promo_type;
		}			
		if($p=='f'){
			if($tipe=='1'){
				$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
								c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif
								from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
								and d.i_product=a.i_product and d.i_price_group='$kdharga'
							   	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
			}else{
				$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif,
								c.e_product_name as nama,b.v_unit_price as harga
								from tr_product_motif a,tm_promo_item b,tr_product c
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product
							   	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
			}
		}else{
				$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif,
								c.e_product_name as nama,b.v_product_retail as harga
								from tr_product_motif a,tr_product_price b,tr_product c
								where b.i_product=a.i_product
								and a.i_product=c.i_product
							   	and b.i_price_group='$kdharga' order by a.i_product limit $num offset $offset",false);
		}	
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function cariproductupdate($cari,$num,$offset,$kdharga,$promo)
    {
		if($offset=='')
			$offset=0;
		$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
		foreach($q->result() as $pro){
			$p	= $pro->f_all_product;
			$tipe	= $pro->i_promo_type;
		}			
		if($p=='f'){
			if($tipe=='1'){
				$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif, 
								c.e_product_name as nama, d.v_product_retail as harga,a.e_product_motifname as namamotif
								from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
								and d.i_product=a.i_product and d.i_price_group='$kdharga'
							   	and b.i_promo='$promo' order by a.i_product limit $num offset $offset" ,false);
			}else{
				$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif,
								c.e_product_name as nama,b.v_unit_price as harga
								from tr_product_motif a,tm_promo_item b,tr_product c
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product and upper(c.i_product) like '%$cari%'
							   	and b.i_promo='$promo' order by a.i_product limit $num offset $offset",false);
			}
		}else{
			$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif,
							c.e_product_name as nama,b.v_product_retail as harga
							from tr_product_motif a,tr_product_price b,tr_product c
							where b.i_product=a.i_product
							and a.i_product=c.i_product and upper(c.i_product) like '%$cari%'
						   	and b.i_price_group='$kdharga' order by a.i_product limit $num offset $offset",false);
		}		if ($query->num_rows() > 0){
		return $query->result();
		}
    }
    function bacastore($num,$offset,$iarea)
    {
		$this->db->select("i_store, e_store_name from tr_store
						   where i_store in(select i_store from tr_area 
						   where i_area='$iarea' or i_area='00')
						   order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset,$promo,$c,$g,$type)
    {
		if($offset=='')
			$offset=0;
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customer y inner join tm_promo yy on(y.i_promo=yy.i_promo)
										where y.i_promo='$promo' 
										and x.i_customer=y.i_customer 
										and x.i_area = '$iarea' limit $num offset $offset",false);
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										where x.i_area = '$iarea' limit $num offset $offset",false);
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customergroup y inner join tm_promo p on (y.i_promo=p.i_promo)
										where y.i_promo='$promo' 
										and x.i_customer_group=y.i_customer_group
										and x.i_area=y.i_area
										and x.i_area = '$iarea' limit $num offset $offset",false);
		}
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapromo($dspb,$num,$offset)
    {
		$this->db->select(" * from tm_promo where d_promo_start<='$dspb' and d_promo_finish>='$dspb' 
							order by i_promo",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($iarea){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_spb,10,6)) as max from tm_spb 
				  			where substr(i_spb,5,2)='$th' and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nospb  =$terakhir+1;
			settype($nospb,"string");
			$a=strlen($nospb);
			while($a<6){
			  $nospb="0".$nospb;
			  $a=strlen($nospb);
			}
			$nospb  ="SPB-".$thbl."-".$nospb;
			return $nospb;
		}else{
			$nospb  ="000001";
			$nospb  ="SPB-".$thbl."-".$nospb;
			return $nospb;
		}
    }
	function runningnumbersjc($iarea){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_sjc,10,6)) as max from tm_sjc 
				  			where substr(i_sjc,5,2)='$th' and i_area='$iarea'
							and substr(i_sjc,1,3)='SJC'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nosjc  =$terakhir+1;
			settype($nosjc,"string");
			$a=strlen($nosjc);
			while($a<6){
			  $nosjc="0".$nosjc;
			  $a=strlen($nosjc);
			}
			$nosjc  ="SJC-".$thbl."-".$nosjc;
			return $nosjc;
		}else{
			$nosjc  ="000001";
			$nosjc  ="SJC-".$thbl."-".$nosjc;
			return $nosjc;
		}
    }
    function cari($iarea,$cari,$num,$offset)
    {
		$this->db->select(" * from tm_spb where upper(i_spb) like '%$cari%' and i_area='$iarea'
					order by i_spb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caristore($cari,$num,$offset)
    {
		$this->db->select(" * from tr_store where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'
							order by i_store",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset,$promo,$c,$g,$type)
    {
		if($offset=='')
			$offset=0;
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customer y inner join tm_promo yy on(y.i_promo=yy.i_promo)
										where y.i_promo='$promo' 
										and x.i_customer=y.i_customer 
										and x.i_area = '$iarea' 
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										where x.i_area = '$iarea' 
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select * 
										from tr_customer x 
										left join tr_customer_pkp b on
										(x.i_customer=b.i_customer) 
										left join tr_price_group c on
										(x.i_price_group=c.n_line) 
										left join tr_customer_area d on
										(x.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(x.i_customer=e.i_customer)
										left join tr_customer_discount f on
										(x.i_customer=f.i_customer)
										, tm_promo_customergroup y inner join tm_promo p on (y.i_promo=p.i_promo)
										where y.i_promo='$promo' 
										and x.i_customer_group=y.i_customer_group
										and x.i_area=y.i_area
										and x.i_area = '$iarea' 
										and (x.i_customer like '%$cari%' or x.e_customer_name like '%$cari%')
										limit $num offset $offset",false);
		}
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$kdharga,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query("  select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,b.v_product_retail as harga
								from tr_product_motif a,tr_product_price b,tr_product c
								where b.i_product=a.i_product
								and a.i_product=c.i_product
							   	and b.i_price_group='$kdharga' 
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caripromo($cari,$dspb,$num,$offset)
    {
		$this->db->select(" 	* from tm_promo where d_promo_start<='$dspb' and d_promo_finish>='$dspb' 
					and (upper(i_promo) like '%$cari%' or upper(e_promo_name) like '%$cari%')						
					order by i_promo",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5,$promo,$c,$g)
    {
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customer a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   or i_area = '$area4' or i_area = '$area5'",false);
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customergroup a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
		}		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$promo,$c,$g)
    {
		if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customer a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
		}else if(($c=='t') && ($g=='f')){
			$query = $this->db->query("	select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
										and i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   	or i_area = '$area4' or i_area = '$area5'",false);
		}else if(($c=='f') && ($g=='t')){
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customergroup a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
		}		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
