<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
		$this->db->query('DELETE FROM tm_ttbretur WHERE i_ttb=\''.$iop.'\'');
		$this->db->query('DELETE FROM tm_ttbretur_item WHERE i_ttb=\''.$iop.'\'');
		return TRUE;
    }
    function bacasemua($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5,$user)
    {
/* 		$this->db->select(" a.i_bbm, a.i_refference_document, a.d_refference_document, a.i_area, a.i_salesman, a.i_supplier,
                        a.d_bbm, a.e_remark, c.e_area_name, d.e_salesman_name 
                        from tm_bbm a, tr_area c, tr_salesman d
					              where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                        and a.i_salesman=d.i_salesman
					              and (upper(a.i_bbm) like '%$cari%' or upper(a.i_salesman) like '%$cari%' 
					              or upper(c.e_area_name) like '%$cari%' or upper(d.e_salesman_name) like '%$cari%') 
                        and (a.i_area='$area1' or a.i_area like '%$area2%'
					              or a.i_area like '%$area3%' or a.i_area like '%$area4%' or a.i_area like '%$area5%') 
						order by a.i_bbm desc",false)->limit($num,$offset); */
		$this->db->select(" a.i_area , a.i_ttb, d_ttb, b.e_salesman_name 
							from tm_ttbretur a 
							inner join tr_salesman b on a.i_salesman = b.i_salesman 
							where a.i_area in(select i_area from tm_user_area  where i_user = '$user')",false)->limit($num,$offset);				


						
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ittb)
    {
		  $this->db->select("	  a.i_ttb,
								  a.i_area,
								  a.i_salesman,
								  a.d_ttb,
								  e.e_alasan_returname as e_remark,
								  b.e_area_name,
								  c.e_salesman_name,
								  a.i_customer,
								  d.e_customer_name
							  from
								  tm_ttbretur a
							  inner join tr_area b on
								  (a.i_area = b.i_area)
							  inner join tr_salesman c on
								  (a.i_salesman = c.i_salesman)
							  inner join tr_customer d on
								  (a.i_customer = d.i_customer)
							  inner join tr_alasan_retur e on
								  (a.i_alasan_retur = e.i_alasan_retur)
							  where
								  a.i_ttb = '$ittb'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ittb)
    {
		  $this->db->select("		  a.i_ttb,
									  a.i_product1 as i_product,
									  a.i_product1_motif as i_product_motif,
									  a.i_product2_grade as i_product_grade,
									  d.e_product_name,
									  a.n_quantity as qty,
									  a.v_unit_price,
									  b.e_product_motifname,
									  a.n_quantity,
									  a.i_product1,
									  a.i_product1_motif,
									  a.i_product1_grade,
									  a.i_ttb as i_ttb,
									  a.n_ttb_year as year,
									  a.i_area as i_area,
									  a.e_ttb_remark
	  							from 
									  tm_ttbretur_item a
									  inner join tr_product_motif b on(a.i_product1 = b.i_product)
									  inner join tm_ttbretur c on(a.i_ttb = c.i_ttb)
									  inner join tr_product d on(a.i_product1 = d.i_product)
	  							where a.i_ttb = '$ittb'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
