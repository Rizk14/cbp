<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($isalesman, $iarea)
    {
		$this->db->select("a.*, b.e_area_name from tr_salesman a, tr_area b where a.i_salesman = '$isalesman' and a.i_area='$iarea' and a.i_area=b.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($isalesman,$esalesmanname,$iarea,$esalesmanaddress,
		       $esalesmancity,$esalesmanpostal,$esalesmanphone,
		       $fsalesmanaktif)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;	
		$this->db->query("insert into tr_salesman (i_salesman, e_salesman_name, i_area, e_salesman_address,
				  			   e_salesman_city, e_salesman_postal, e_salesman_phone,
	  						   d_salesman_entry, f_salesman_aktif) 
				  values 
							  ('$isalesman', '$esalesmanname','$iarea', '$esalesmanaddress',
							       '$esalesmancity','$esalesmanpostal','$esalesmanphone',
							       '$dentry','TRUE')");
		#redirect('salesman/cform/');
    }

    function update($isalesman,$esalesmanname,$iarea,$esalesmanaddress,
		    $esalesmancity,$esalesmanpostal,$esalesmanphone,
		    $fsalesmanaktif)
    {
		if($fsalesmanaktif=='on'){
			$fsalesmanaktif='TRUE';
		}elseif($fsalesmanaktif=='off'){
			$fsalesmanaktif='FALSE';
		}
		if($fsalesmanaktif!='FALSE'){
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("update tr_salesman set e_salesman_name = '$esalesmanname', 
							 i_area = '$iarea',
							 e_salesman_address = '$esalesmanaddress', 
							 e_salesman_city = '$esalesmancity', 
							 e_salesman_postal='$esalesmanpostal',
							 e_salesman_phone='$esalesmanphone',
							 f_salesman_aktif='$fsalesmanaktif'
				  where i_salesman = '$isalesman'");
		}else{
		$this->db->query("update tr_salesman set e_salesman_name = '$esalesmanname', 
							 i_area = '$iarea',
							 e_salesman_address = '$esalesmanaddress', 
							 e_salesman_city = '$esalesmancity', 
							 e_salesman_postal='$esalesmanpostal',
							 e_salesman_phone='$esalesmanphone',
							 f_salesman_aktif='$fsalesmanaktif'
				  where i_salesman = '$isalesman'");
		}
		#redirect('salesman/cform/');
    }
	
    public function delete($isalesman,$iarea) 
    {
		$this->db->query("UPDATE tr_salesman SET f_salesman_aktif='0' WHERE i_salesman=\''.$isalesman.'\'");
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.*, b.e_area_name from tr_salesman a, tr_area b where a.i_area=b.i_area and (upper(b.e_area_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%' or upper(a.e_salesman_name) like '%$cari%') order by a.i_salesman", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaarea($num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.*, b.e_area_name from tr_salesman a, tr_area b where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%' or upper(b.e_area_name) like '%$cari%') and a.i_area=b.i_area order by a.i_salesman",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
