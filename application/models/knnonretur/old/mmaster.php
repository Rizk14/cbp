<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$iarea)
    {
		$this->db->select(" * from tm_giro a
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			=> $iarea,
				'i_kn'				=> $ikn,
				'i_customer' 		=> $icustomer,
				'i_refference' 	=> $irefference,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman' 		=> $isalesman,
				'i_kn_type' 		=> $ikntype,
				'd_kn' 				=> $dkn,
				'd_refference'		=> $drefference,
//				'i_pajak' 			=> ,
//				'd_pajak' 			=> ,
//				'd_cetak' 			=> ,
//				'd_cetak2' 			=> ,
//				'd_update' 			=> ,
				'd_entry' 			=> $dentry,
				'e_remark' 			=> $eremark,
				'f_cetak' 			=> $fcetak,
				'f_masalah' 		=> $fmasalah,
				'f_insentif' 		=> $finsentif,
				'n_kn_year' 		=> $nknyear,
				'v_netto' 			=> $vnetto,
				'v_gross' 			=> $vgross,
				'v_discount' 		=> $vdiscount,
				'v_sisa'			=> $vsisa

    		)
    	);
    	
    	$this->db->insert('tm_kn');
    }
    function update($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate	= $row->c;
    	$this->db->set(
    		array(
				'i_area'				=> $iarea,
				'i_kn'					=> $ikn,
				'i_customer' 		=> $icustomer,
				'i_refference' 	=> $irefference,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman' 		=> $isalesman,
				'i_kn_type' 		=> $ikntype,
				'd_kn' 					=> $dkn,
				'd_refference'	=> $drefference,
				'd_update' 			=> $dupdate,
				'e_remark' 			=> $eremark,
				'f_cetak' 			=> $fcetak,
				'f_masalah' 		=> $fmasalah,
				'f_insentif' 		=> $finsentif,
				'n_kn_year' 		=> $nknyear,
				'v_netto' 			=> $vnetto,
				'v_gross' 			=> $vgross,
				'v_discount' 		=> $vdiscount,
				'v_sisa'				=> $vsisa,
				'f_kn_cancel'		=> 'f'

    		)
    	);
    	$this->db->where("i_kn",$ikn);
    	$this->db->where("n_kn_year",$nknyear);
    	$this->db->where("i_area",$iarea);
    	$this->db->update('tm_kn');
    }
    public function delete($igiro,$iarea) 
    {
		$this->db->query("DELETE FROM tm_giro WHERE i_giro='$igiro' and i_area='$iarea'");
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select(' * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							order by a.i_area,a.i_giro', false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset)
    {
		$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset)
    {
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						   order by i_area ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacabbm($iarea,$num,$offset)
    {
		$this->db->select(" a.i_bbm, a.i_area, a.i_salesman, d.e_salesman_name, c.e_area_name, b.i_customer, e.e_customer_name, e.e_customer_address,
							b.v_ttb_gross, b.v_ttb_discounttotal, b.v_ttb_netto, v_ttb_sisa, a.d_bbm, f.i_customer_groupar
							from tm_bbm a, tr_salesman d, tm_ttbretur b, tr_area c, tr_customer e, tr_customer_groupar f
							where a.i_bbm_type='05' and a.i_area='$iarea' 
							and a.i_salesman=d.i_salesman 
							and a.i_bbm=b.i_bbm and a.i_area=b.i_area
							and a.i_area=c.i_area
							and b.i_customer=e.i_customer
							and e.i_customer=f.i_customer and a.i_bbm not in(select i_refference from tm_kn)
							order by a.i_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_groupar f on
							(a.i_customer=f.i_customer)  
							left join tr_customer_discount g on
							(a.i_customer=g.i_customer) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer) 
							where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_groupar f on
							(a.i_customer=f.i_customer)  
							left join tr_customer_discount g on
							(a.i_customer=g.i_customer) 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumberkn($th,$iarea)
	{
    $pot=substr($th,2,2);
    $this->db->select("	count(i_kn) as jml from tm_kn where n_kn_year=$nknyear and i_area='$iarea' and substring(i_kn,1,2)='KP'",false);
    $query = $this->db->get();
    $jml=0;
    if ($query->num_rows() > 0){
	    foreach($query->result() as x){
	      $jml=$x->jml;
	    }
	  }
	  if($jml>332){
  		$this->db->select(" substring(i_kn,5,2) as no from tm_kn where n_kn_year=$th and i_area='$iarea' and substring(i_kn,1,2)='KP' order by xmin::text::bigint desc",false);
    }else{
   		$this->db->select(" max(substring(i_kn,5,2)) as no from tm_kn where n_kn_year=$th and i_area='$iarea' and substring(i_kn,1,2)='KP'",false);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
			  if($row->no==99){
			    $nono='A1';
			  }elseif(ord(substr($row->no,0,1))>57 && ord(substr($row->no,1,1))<57){
			    $x=substr($row->no,1,1)+1;
			    $nono=chr(ord(substr($row->no,0,1))).$x;
			  }elseif(ord(substr($row->no,0,1))>57 && ord(substr($row->no,0,1))<90 && ord(substr($row->no,1,1))==57){
			    $nono=chr(ord(substr($row->no,0,1))+1).'1';
######			  
			  }elseif(ord(substr($row->no,0,1))==90 && ord(substr($row->no,1,1))==57){
			    $nono='AA';  
			  }elseif(ord(substr($row->no,0,1))>57 && ord(substr($row->no,0,1))<90 && ord(substr($row->no,1,1))>57 && ord(substr($row->no,1,1))<90){
			    $nono=substr($row->no,0,1).chr(ord(substr($row->no,1,1))+1);
			  }elseif(ord(substr($row->no,0,1))>57 && ord(substr($row->no,0,1))<90 && ord(substr($row->no,1,1))==90){
			    $nono=chr(ord(substr($row->no,0,1))+1).'A';
######			    
			  }else{
          $nono=$row->no+1;			  
			  }
			  break;
			}
      settype($nono,"string");
		  $a=strlen($nono);
		  while($a<2){
		    $nono="0".$nono;
		    $a=strlen($nono);
		  }
      $kn="KP".$iarea.$nono.$pot;
			return $kn;
		}else{
			$kn="KP".$iarea."01".$pot;
			return $kn;
		}
	}
	function bacakn($ikn,$nknyear,$iarea)
	{
 		$this->db->select("	distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman),
              a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name from tm_kn a
							left join tr_customer_groupar d on (a.i_customer=d.i_customer)
							inner join tr_customer b on (a.i_customer=b.i_customer)
							inner join tr_area c on (a.i_area=c.i_area)
							inner join tr_salesman e on (a.i_salesman=e.i_salesman)
							where a.i_kn_type='02' 
							  and a.i_kn='$ikn'
							  and a.n_kn_year=$nknyear
							  and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
}
?>
