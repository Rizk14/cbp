<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$iarea,$ikb) 
    {
		#UPDATE KB 
		$this->db->query("UPDATE tm_kb 
						  SET f_kb_cancel='t' 
						  WHERE i_kb='$ikb' AND i_periode='$iperiode' AND i_area='$iarea' AND f_posting='f' AND f_close='f'");
#####UnPosting      
		$this->db->query("DELETE FROM th_jurnal_transharian
						  WHERE i_refference='$ikb' AND i_area='$iarea'");
		$this->db->query("DELETE FROM th_jurnal_transharianitem
						  WHERE i_refference='$ikb' AND i_area='$iarea'");
		$this->db->query("DELETE FROM th_general_ledger
						  WHERE i_refference='$ikb' AND i_area='$iarea'");
#INSERT
		$this->db->query("INSERT INTO th_jurnal_transharian SELECT * FROM tm_jurnal_transharian 
      	                  WHERE i_refference='$ikb' AND i_area='$iarea'");
      	$this->db->query("INSERT INTO th_jurnal_transharianitem SELECT * FROM tm_jurnal_transharianitem 
      	                  WHERE i_refference='$ikb' AND i_area='$iarea'");
      	$this->db->query("INSERT INTO th_general_ledger SELECT * FROM tm_general_ledger
                          WHERE i_refference='$ikb' AND i_area='$iarea'");

#SELECT MUTASI DI TM GENERAL LEDGER
      	$quer = $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  FROM tm_general_ledger
								  WHERE i_refference='$ikb' AND i_area='$iarea'");
#UPDATE TM_COA_SALDO
  	  	if($quer->num_rows()>0){
        	foreach($quer->result() as $xx){
        	  	$this->db->query("UPDATE tm_coa_saldo 
				  				  SET v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
        	                      v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
        	                      v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
        	                      WHERE i_coa='$xx->i_coa' AND i_periode='$xx->periode'");
        	}
      	}

#DELETE TM JURNAL TRANSHARIAN
      	$this->db->query("DELETE FROM tm_jurnal_transharian WHERE i_refference='$ikb' AND i_area='$iarea'");
      	$this->db->query("DELETE FROM tm_jurnal_transharianitem WHERE i_refference='$ikb' AND i_area='$iarea'");
      	$this->db->query("DELETE FROM tm_general_ledger WHERE i_refference='$ikb' AND i_area='$iarea'");

		#SELECT NOMOR PV
		$quer 	= $this->db->query("SELECT i_pv, i_area, v_pv, i_pv_type FROM tm_pv_item
      	                            WHERE i_kk='$ikb' AND i_area='$iarea' AND i_pv_type='01'");
		
		#UPDATE PV 
		if($quer->num_rows()>0){
      	  	foreach($quer->result() as $xx){
      	  	  	$this->db->query("UPDATE tm_pv SET v_pv=v_pv-$xx->v_pv
      	  	  	                  WHERE i_pv='$xx->i_pv' AND i_area='$xx->i_area' AND i_pv_type='$xx->i_pv_type'");
					  
				#$this->db->query("delete from tm_pv_item where i_kk='$ikb' and i_pv='$xx->i_pv' and i_area='$xx->i_area' and i_pv_type='$xx->i_pv_type'");
				
				$this->db->query("UPDATE tm_pv_item SET f_pv_cancel='t' 
					  WHERE i_kk='$ikb' AND i_pv='$xx->i_pv' AND i_pv_type='$xx->i_pv_type'");	
			}
		}
		
		#SELECT NOMOR RV
      	$quer 	= $this->db->query("SELECT i_rv, i_area, v_rv, i_rv_type FROM tm_rv_item
      	                            WHERE i_kk='$ikb' AND i_area='$iarea' AND i_rv_type='01'");
		
		#UPDATE RV
		if($quer->num_rows()>0){
      	  	foreach($quer->result() as $xx){
      	  	  	$this->db->query("UPDATE tm_rv SET v_rv=v_rv-$xx->v_rv
      	  	  	                  WHERE i_rv='$xx->i_rv' AND i_area='$xx->i_area' AND i_rv_type='$xx->i_rv_type'");
					  
				#$this->db->query("delete from tm_rv_item where i_kk='$ikb' and i_rv='$xx->i_rv' and i_area='$xx->i_area' and i_rv_type='$xx->i_rv_type'");
				
				$this->db->query("UPDATE tm_rv_item SET f_rv_cancel='t' 
                                  WHERE i_kk='$ikb' AND i_rv='$xx->i_rv' AND i_rv_type='$xx->i_rv_type'");
			}
      	}
#####
    }
 /*   function bacasemua($area1, $area2, $area3, $area4, $area5, $cari, $num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area
                and a.f_kb_cancel='f'
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } 
    function cari($area1, $area2, $area3, $area4, $area5, $cari,$num,$offset)
    {
		if($area1=='00'){
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_area) like '%$cari%' or upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								order by a.i_area, a.i_jurnal desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" a.*, b.e_area_name from tm_kb a, tr_area b
								where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area and a.f_kb_cancel='f'
								and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
								order by a.i_area, a.i_kb desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	* from (
    		                    select c.i_rv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.f_kb_cancel,
							              b.e_area_name from tm_kb a, tr_area b, tm_rv_item c
							              where a.i_kb=c.i_kk and c.i_rv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_rv) like '%$cari%') 
										  and a.i_area=b.i_area 
										  --and a.f_kb_cancel='f' 
										  and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              union all
							              select c.i_pv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.f_kb_cancel,
							              b.e_area_name from tm_kb a, tr_area b, tm_pv_item c
							              where a.i_kb=c.i_kk and c.i_pv_type='01'
										  and (upper(a.i_kb) like '%$cari%' or upper(c.i_pv) like '%$cari%') and a.i_area=b.i_area 
										  --and a.f_kb_cancel='f' 
										  and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              ) as a ORDER BY a.i_kb ",false);
							              #->limit($num,$offset);
							              #and a.i_area=c.i_area
      }else{
		    $this->db->select("	* from (
    		                    select c.i_rv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.f_kb_cancel,
							              b.e_area_name from tm_kb a, tr_area b, tm_rv_item c
							              where a.i_kb=c.i_kk and c.i_rv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_rv) like '%$cari%')
										  and a.i_area=b.i_area 
										  --and a.f_kb_cancel='f'
							              and a.i_area='$iarea' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							              a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              union all
							              select c.i_pv as vc, a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb ,
							              a.i_periode, a.f_debet, a.f_posting, a.i_cek, a.f_kb_cancel,
							              b.e_area_name from tm_kb a, tr_area b, tm_pv_item c
							              where a.i_kb=c.i_kk and c.i_pv_type='01'
							              and (upper(a.i_kb) like '%$cari%' or upper(c.i_pv) like '%$cari%')
										  and a.i_area=b.i_area 
										  --and a.f_kb_cancel='f'
							              and a.i_area='$iarea' and
							              a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							              a.d_kb <= to_date('$dto','dd-mm-yyyy')
							              ) as a ORDER BY a.i_kb ",false);
							              #->limit($num,$offset);
							              #and a.i_area=c.i_area
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek, a.f_kb_cancel,
							    a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							    where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area 
								--and a.f_kb_cancel='f' 
								and
							    a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_kb <= to_date('$dto','dd-mm-yyyy')
							    ORDER BY a.i_kb ",false);
							    #->limit($num,$offset);
		  }else{
		    $this->db->select("	a.i_kb, a.i_area, a.d_kb, a.i_coa, a.e_description, a.v_kb , a.i_cek, a.f_kb_cancel,
							    a.i_periode, a.f_debet, a.f_posting, b.e_area_name from tm_kb a, tr_area b
							    where (upper(a.i_kb) like '%$cari%')
								and a.i_area=b.i_area 
								--and a.f_kb_cancel='f'
							    and a.i_area='$iarea' and
							    a.d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_kb <= to_date('$dto','dd-mm-yyyy')
							    ORDER BY a.i_kb ",false);
							    #->limit($num,$offset);
		  }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
