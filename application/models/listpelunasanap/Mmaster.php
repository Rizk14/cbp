<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ipl,$iarea,$dbukti,$thku) 
    {
			$this->db->select(" * from tm_pelunasanap WHERE i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'", false);
			$query = $this->db->get();
			foreach($query->result() as $row){
				$isupplier=$row->i_supplier;
				$vjml=$row->v_jumlah;
				$vlbh=$row->v_lebih;
				$vbyr=$vjml-$vlbh;
#				echo $vjml.'  '.$vlbh.'  '.$vbyr;die;
				if($row->i_jenis_bayar=='01'){
					$this->db->query("update tm_giro_dgu set v_sisa=v_sisa+$vbyr where i_giro='$row->i_giro' and i_supplier='$isupplier'");
				}elseif($row->i_jenis_bayar=='04'){
					$this->db->query("update tm_dn set v_sisa=v_sisa+$vbyr where i_dn='$row->i_giro' and i_area='$iarea'");
				}elseif($row->i_jenis_bayar=='03'){
					$this->db->query(" update tm_kuk set v_sisa=v_sisa+$vbyr where i_kuk='$row->i_giro' and n_kuk_year='$thku'");
				}
				$this->db->query("update tm_pelunasanap_lebih set f_pelunasanap_cancel='t' WHERE i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
			}
			$this->db->select(" * from tm_pelunasanap_item WHERE i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'", false);
			$query = $this->db->get();
			foreach($query->result() as $row){
				$vtmp=$row->v_jumlah;
				$this->db->query("update tm_dtap set v_sisa=v_sisa+$vtmp where i_dtap='$row->i_dtap' and i_supplier='$isupplier'");
			}
			$this->db->query("update tm_pelunasanap set f_pelunasanap_cancel='t' WHERE i_pelunasanap='$ipl' and i_area='$iarea' and d_bukti='$dbukti'");
    }
    function bacasemua($cari, $num, $offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name, c.e_supplier_address, c.e_supplier_city,
							d.e_jenis_bayarname
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where 
							a.i_area=b.i_area
							and a.i_supplier=c.i_supplier
							and a.i_jenis_bayar=d.i_jenis_bayar
							and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_supplier) like '%$cari%')
							order by a.i_area,a.i_pelunasanap", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_pelunasan a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
							inner join tr_jenis_bayar on(a.i_jenis_bayar=tr_jenis_bayar.i_jenis_bayar)
							inner join tm_dtap on(a.i_dt=tm_dt.i_dt and a.i_area=tm_dt.i_area)
							where upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_supplier) like '%$cari%'
							order by a.i_area,a.i_dt", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.e_supplier_address, c.e_supplier_city,
							d.e_jenis_bayarname
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where 
							a.i_area=b.i_area
							and a.i_supplier=c.i_supplier
							and a.i_jenis_bayar=d.i_jenis_bayar
							and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_supplier) like '%$cari%')
							and a.i_supplier='$isupplier' and
							a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_supplier, a.i_pelunasanap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.e_supplier_address, c.e_supplier_city,
							d.e_jenis_bayarname
							from tm_pelunasanap a, tr_area b, tr_supplier c, tr_jenis_bayar d
							where 
							a.i_area=b.i_area
							and a.i_supplier=c.i_supplier
							and a.i_jenis_bayar=d.i_jenis_bayar
							and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%' 
							or upper(a.i_supplier) like '%$cari%')
							and a.i_supplier='$isupplier' and
							a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_bukti <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_pelunasanap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
