<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispmb)
    {
		  $this->db->select(" distinct(a.*), c.e_area_name
                          from tm_spmb a, tm_spmb_item b, tr_area c
                          where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                          and not a.i_approve2 is null
                          and not a.i_store is null
                          and b.n_deliver<b.n_saldo
                          and a.i_spmb='$ispmb'
                          and a.i_area=c.i_area
                          order by a.i_spmb ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($ispmb)
    {
		  $this->db->select(" a.i_spmb, a.d_spmb, a.i_area, b.i_product, b.e_product_name, b.n_order, b.n_acc, b.n_saldo
                          from tm_spmb a, tm_spmb_item b
                          where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                          and not a.i_approve2 is null and not a.i_store is null
                          and b.n_deliver<b.n_saldo and a.i_spmb='$ispmb'
                          order by b.n_item_no ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$num,$offset,$cari)
    {
			  $this->db->select("	distinct(a.i_spmb), a.d_spmb, a.i_area, a.f_spmb_consigment, a.f_spmb_cancel, c.e_area_name
                            from tm_spmb a, tm_spmb_item b, tr_area c
                            where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                            and not a.i_approve2 is null
                            and not a.i_store is null
                            and b.n_deliver<b.n_saldo
                            and (upper(a.i_spmb) like '%$cari%')
                            and a.i_area='$iarea'
                            and a.i_area=c.i_area
                            order by a.i_spmb DESC",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$num,$offset,$cari)
    {
			$this->db->select("	distinct(a.i_spmb), a.d_spmb, a.i_area, a.f_spmb_consigment, a.f_spmb_cancel, c.e_area_name
                          from tm_spmb a, tm_spmb_item b, tr_area c
                          where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                          and not a.i_approve2 is null
                          and not a.i_store is null
                          and b.n_deliver<b.n_saldo
                          and (upper(a.i_spmb) like '%$cari%')
                          and a.i_area='$iarea'
                          and a.i_area=c.i_area
                          order by a.i_spmb DESC",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
