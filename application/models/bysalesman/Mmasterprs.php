<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto,$th,$prevth,$bl,$bulan,$tahun,$iuser,$akhir,$prevakhir,$last)
    {
       $thbl=$th.$bl;
       $thblto=$tahun.$bulan;

        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $thun=$tmp[2]-1;
        $thnow=$tmp[2];
        $thblFROM = $thnow."-".$bl;
        $dfromprev=$hr."-".$bl."-".$thun;
        //$dfromprev=$hr."-".$bl."-".$th;

        $tsasih = date('Y-m', strtotime('-24 month', strtotime($thblFROM))); //tambah tanggal sebanyak 6 bulan
        if($tsasih!=''){
        $smn = explode("-", $tsasih);
        $yer = $smn[0];
        $mon = $smn[1];
        }
        $taunsasih = $yer.$mon;
      
        $tmp=explode("-",$dto);
        $hri=$tmp[0];
        $bln=$tmp[1];
        $thun=$tmp[2]-1;
        $thn = $tmp[2];
        $thblto = $thn.$bln;

        //u tahun bukan kabisat
        $temptahunprev2 = $thun % 4;
        if($temptahunprev2<>0 && $hri==29 && $bln==2){
            $hri = $hri - 1;
        }
        $dtoprev=$hri."-".$bln."-".$thun;
/*SELECT i_area , i_salesman , sum(v_target_tagihan) AS vtargetcoll , sum(v_realisasi_tagihan) AS vrealisasicoll ,
0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev 
FROM  f_target_collection_rekapkodealokasi('$thbl','$akhir')
GROUP BY i_area, i_salesman
UNION ALL*/
      $sql=" d.i_area , d.e_area_name ,a.i_salesman , c.e_salesman_name ,sum(a.vtargetcoll) AS vtargetcoll , sum(a.vrealisasicoll) AS vrealisasicoll ,
            sum(a.vtargetsls) AS vtargetsls, sum(a.ob) AS ob, sum(a.oa) AS oa , sum(a.qty) AS qty , sum(a.netsales) AS netsales, 
            sum(a.oaprev) AS oaprev , sum(a.qtyprev) AS qtyprev , sum(a.netsalesprev) AS netsalesprev FROM ( ";

	$start = $month = strtotime($dfrom);
	$end = strtotime($dto);
	$i=0;
while($month < $end)
{	
     $oke=date('m', $month);
     $month = strtotime("+1 month", $month);
     $date = date ("Y-m-d", strtotime("+1 month", strtotime($month)));
     $periode=$th.$oke;

     if($periode!=''){
				$th=substr($periode,0,4);
				$bl=substr($periode,4,2);
				$prevth  =$th-1;
				$prevdate=$prevth.$bl;
			}		
			if($bl <9 ){
          		$bln = $bl+1;
          		$bln = '0'.$bln;
          		$akhir = $th.'-'.$bln.'-01';
          		$prevakhir = $prevth.'-'.$bln.'-01';
        	}elseif($bl >=9 && $bl <12){
          		$bln = $bl+1;
          		$akhir = $th.'-'.$bln.'-01';
          		$prevakhir = $prevth.'-'.$bln.'-01';
        	}elseif($bl ==12){
          		$thn = $th+1;
          		$akhir = $thn.'-01-01';
          		$prevakhir = $prevth.'-01-01';
        	}
    $sql .="  SELECT i_area , i_salesman , sum(v_target_tagihan) AS vtargetcoll , sum(v_realisasi_tagihan) AS vrealisasicoll ,
              0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev 
              FROM  f_target_collection_rekapkodealokasi('$periode','$akhir')
              GROUP BY i_area, i_salesman

              UNION ALL";
}

    /*$sql .="

    SELECT i_area , i_salesman , sum(v_target_tagihan) AS vtargetcoll , sum(v_realisasi_tagihan) AS vrealisasicoll ,
    0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
    0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev 
    FROM  f_target_collection_rekapkodealokasi('$thblto','$last')
    GROUP BY i_area, i_salesman
    UNION ALL";*/


    $sql .="  SELECT i_area , i_salesman , 0 AS vtargetcoll , 0 AS vtargetcoll , sum(v_target) AS vtargetsls, 0 AS ob, 0 AS oa ,
              0 AS qty , 0 AS netsales, 0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 
              0 AS qtyprev , 0 AS netsalesprev
              FROM tm_target_itemsls
              WHERE  i_periode >='$thbl' and i_periode <='$thblto' 
              GROUP BY i_area , i_salesman

              UNION ALL

              SELECT i_area , i_salesman,0 AS vtargetcoll , 0 AS vtargetcoll ,0 AS vtargetsls, count(ob) AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM (
                SELECT distinct on (a.ob) a.ob AS ob , a.i_salesman , a.i_area FROM (
                    SELECT  a.i_customer AS ob ,a.i_salesman,a.i_area
                    FROM tm_nota a 
                    WHERE to_char(a.d_nota,'yyyymm')>='$taunsasih' and to_char(a.d_nota,'yyyymm') <='$thblto' 
                    and a.f_nota_cancel='false'
                    and not a.i_nota isnull
                    
                    UNION ALL

                    SELECT a.i_customer AS ob ,b.i_salesman , a.i_area FROM tr_customer a 
                    LEFT JOIN tr_customer_salesman b on(a.i_customer=b.i_customer and a.i_area=b.i_area) 
                    WHERE a.i_customer_status <> '4' and a.f_customer_aktif='true'
                ) AS a
                ORDER BY a.ob
              ) AS a
              GROUP BY i_area , i_salesman 

              UNION ALL

              /************HITUNG OA***********/
              select i_area , i_salesman,0 as vtargetcoll,0 as vtargetcoll ,0 as vtargetsls, 0 as ob,count(oa) as oa , 0 as qty , 0 as netsales,
              0 as vtargetcollprev, 0 as vrealisasicollprev , 0 as vtargetslsprev,0 as obprev, 0 as oaprev , 0 as qtyprev , 0 as netsalesprev 
              from (SELECT DISTINCT ON(to_char(d_nota,'yyyymm'),a.i_salesman,a.i_area,a.i_customer) a.i_customer as oa, a.i_salesman , a.i_area 
              from  tm_nota a join tr_customer b on a.i_customer = b.i_customer
              where (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy'))
              and a.f_nota_cancel='false' 
              and not a.i_nota isnull
              and b.f_customer_aktif='true'
              and b.i_customer_status<>'4'
              ) as a 
              group by i_salesman , i_area

              UNION ALL

              /************HITUNG QTY*********/
              SELECT a.i_area , b.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,sum(a.n_deliver) AS qty , 0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
              FROM tm_nota_item a , tm_nota b
              WHERE b.f_nota_cancel='false'
              and (b.d_nota >= to_date('$dfrom','dd-mm-yyyy') and b.d_nota <= to_date('$dto','dd-mm-yyyy'))
              and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
              GROUP BY b.i_salesman , a.i_area

              UNION ALL

              SELECT a.i_area, a.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty ,sum(v_nota_netto) AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
              FROM tm_nota a
              WHERE (d_nota >= to_date('$dfrom','dd-mm-yyyy') and d_nota <= to_date('$dto','dd-mm-yyyy'))
              and f_nota_cancel='f' and not i_nota isnull
              GROUP BY a.i_area, a.i_salesman

              UNION ALL

              /*================================================ PRev Year ==============================================*/
              /*****HITUNG OA SEBELUMNYA*******/
              SELECT i_area , i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa , 0 AS qty , 0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, count(oa) AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM(
                  SELECT distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area,a.i_salesman) a.i_customer AS oa, a.i_salesman , a.i_area 
                  FROM tm_nota a ,tr_customer b
                  WHERE (a.d_nota >= to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <= to_date('$dtoprev','dd-mm-yyyy'))
                  and a.f_nota_cancel='false' and not a.i_nota isnull
                  and a.i_customer=b.i_customer and a.i_area=b.i_area
                  and b.f_customer_aktif='true'
                  and b.i_customer_status<>'4'
                ) AS a
                GROUP BY i_salesman , i_area
              
              UNION ALL
              
              /*****HITUNG QTY SEBELUMNYA*******/
              SELECT a.i_area , b.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty , 0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , sum(a.n_deliver) AS qtyprev , 0 AS netsalesprev
              FROM tm_nota_item a , tm_nota b
              WHERE b.f_nota_cancel='false'
              and (b.d_nota >= to_date('$dfromprev','dd-mm-yyyy') and b.d_nota <= to_date('$dtoprev','dd-mm-yyyy')) 
              and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
              GROUP BY b.i_salesman , a.i_area
              
              UNION ALL
              
              SELECT a.i_area, a.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty ,0 AS netsales,
              0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , sum(v_nota_netto) AS netsalesprev
              FROM tm_nota a
              WHERE (a.d_nota >= to_date('$dfromprev','dd-mm-yyyy') and a.d_nota <= to_date('$dtoprev','dd-mm-yyyy'))
              and f_nota_cancel='f' and not i_nota isnull
              GROUP BY a.i_area, a.i_salesman
) AS a
LEFT JOIN tr_area b on (a.i_area=b.i_area and b.f_area_real='t' )
LEFT JOIN tr_salesman c on (a.i_salesman=c.i_salesman)
INNER JOIN tr_area_mapping d ON (b.i_area = d.i_area_mapping)
GROUP BY d.i_area , a.i_salesman,d.e_area_name ,c.e_salesman_name
ORDER BY d.i_area , i_salesman ";

/*
	    $this->db->SELECT(" a.i_area , b.e_area_name ,a.i_salesman , c.e_salesman_name ,sum(a.vtargetcoll) AS vtargetcoll , sum(a.vrealisasicoll) AS vrealisasicoll ,
sum(a.vtargetsls) AS vtargetsls, sum(a.ob) AS ob, sum(a.oa) AS oa , sum(a.qty) AS qty , sum(a.netsales) AS netsales,
sum(a.vtargetcollprev) AS vtargetcollprev, sum(a.vrealisasicollprev) AS vrealisasicollprev , 
sum(a.vtargetslsprev) AS vtargetslsprev,sum(a.obprev) AS obprev, sum(a.oaprev) AS oaprev , sum(a.qtyprev) AS qtyprev , sum(a.netsalesprev) AS netsalesprev FROM (
SELECT i_area , i_salesman , sum(v_target_tagihan) AS vtargetcoll , sum(v_realisasi_tagihan) AS vrealisasicoll ,
0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev 
FROM  f_target_collection_rekapkodealokasi('$thbl','$akhir')
GROUP BY i_area, i_salesman
UNION ALL
SELECT i_area , i_salesman , 0 AS vtargetcoll , 0 AS vtargetcoll , sum(v_target) AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
FROM tm_target_itemsls
WHERE  i_periode ='$thbl' 
GROUP BY i_area , i_salesman
UNION ALL
SELECT i_area , i_salesman , 0 AS vtargetcoll , 0 AS vtargetcoll ,0 AS vtargetsls, count(ob) AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM (
SELECT distinct on (a.i_customer)  a.i_customer AS ob, a.i_salesman , a.i_area FROM tm_nota a 
WHERE to_char(a.d_nota,'yyyymm')<= '$thbl' 
and a.f_nota_cancel='f' and not a.i_nota isnull
GROUP BY a.i_customer, a.i_area, a.i_salesman
) AS a
GROUP BY i_salesman , i_area
UNION ALL
SELECT i_area , i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,count(oa) AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM(
SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer AS oa, a.i_salesman , a.i_area FROM  tm_nota a 
WHERE to_char(a.d_nota,'yyyymm')='$thbl'
and a.f_nota_cancel='false' and not a.i_nota isnull
) AS a
GROUP BY i_salesman , i_area
UNION ALL
SELECT a.i_area , b.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,sum(a.n_deliver) AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
FROM tm_nota_item a , tm_nota b
WHERE b.f_nota_cancel='false'
and to_char(a.d_nota,'yyyymm')='$thbl' and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
GROUP BY b.i_salesman , a.i_area
UNION ALL
SELECT a.i_area, a.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty ,sum(v_nota_netto) AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
FROM tm_nota a
WHERE to_char(d_nota,'yyyymm')='$thbl' 
and f_nota_cancel='f' and not i_nota isnull
GROUP BY a.i_area, a.i_salesman
UNION ALL
--================================================ PRev Year ==============================================
SELECT i_area , i_salesman ,0 AS vtargetcoll , 0 AS vrealisasicoll , 0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales, 
sum(v_target_tagihan) AS vtargetcollprev, sum(v_realisasi_tagihan) AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
FROM  f_target_collection_rekapkodealokasi('$prevthbl','$prevakhir')
GROUP BY i_area, i_salesman
UNION ALL
SELECT i_area , i_salesman , 0 AS vtargetcoll , 0 AS vtargetcoll , 0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , sum(v_target) AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev
FROM tm_target_itemsls
WHERE  i_periode ='$prevthbl' 
GROUP BY i_area , i_salesman
UNION ALL
SELECT i_area , i_salesman , 0 AS vtargetcoll , 0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob, 0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,count(ob) AS obprev, 0 AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM (
SELECT distinct on (to_char(a.d_nota,'yyyy'), a.i_area, a.i_customer)  a.i_customer AS ob, a.i_salesman , a.i_area FROM tm_nota a 
WHERE to_char(a.d_nota,'yyyymm')<= '$prevthbl' 
and a.f_nota_cancel='f' and not a.i_nota isnull
GROUP BY a.i_customer, a.i_area, a.i_salesman,to_char(a.d_nota,'yyyy')
) AS a
GROUP BY i_salesman , i_area
UNION ALL
SELECT i_area , i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa , 0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, count(oa) AS oaprev , 0 AS qtyprev , 0 AS netsalesprev FROM(
SELECT  distinct on (to_char(a.d_nota,'yyyymm'),a.i_customer,a.i_area) a.i_customer AS oa, a.i_salesman , a.i_area FROM  tm_nota a 
WHERE to_char(a.d_nota,'yyyymm')='$prevthbl'
and a.f_nota_cancel='false' and not a.i_nota isnull
) AS a
GROUP BY i_salesman , i_area
UNION ALL
SELECT a.i_area , b.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty , 0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , sum(a.n_deliver) AS qtyprev , 0 AS netsalesprev
FROM tm_nota_item a , tm_nota b
WHERE b.f_nota_cancel='false'
and to_char(a.d_nota,'yyyymm')='$prevthbl' and a.i_nota=b.i_nota and a.i_area=b.i_area and not b.i_nota isnull
GROUP BY b.i_salesman , a.i_area
UNION ALL
SELECT a.i_area, a.i_salesman,0 AS vtargetcoll,0 AS vtargetcoll ,0 AS vtargetsls, 0 AS ob,0 AS oa ,0 AS qty ,0 AS netsales,
0 AS vtargetcollprev, 0 AS vrealisasicollprev , 0 AS vtargetslsprev,0 AS obprev, 0 AS oaprev , 0 AS qtyprev , sum(v_nota_netto) AS netsalesprev
FROM tm_nota a
WHERE to_char(d_nota,'yyyymm')='$prevthbl' 
and f_nota_cancel='f' and not i_nota isnull
GROUP BY a.i_area, a.i_salesman

) AS a
LEFT JOIN tr_area b on (a.i_area=b.i_area and b.f_area_real='t' )
LEFT JOIN tr_salesman c on (a.i_salesman=c.i_salesman)
GROUP BY a.i_area , a.i_salesman,b.e_area_name ,c.e_salesman_name
ORDER BY i_area , i_salesman ",false); */
		  $this->db->SELECT($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function user()
    {
        $this->db->SELECT("  * FROM tm_user",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
