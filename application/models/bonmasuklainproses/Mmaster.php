<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacabmnota($num,$offset,$cari){
      if($offset=='')$offset=0;
			if($cari=='' or $cari=='sikasep'){
        return false;
			}else{
			  $query=$this->db->query(" select substring(a.i_nota,9,2) as i_area, a.i_bmnota, to_char(a.d_bmnota,'dd-mm-yyyy') as d_bmnota, 
			                            a.i_nota, to_char(c.d_nota,'dd-mm-yyyy') as d_nota, b.e_area_name 
			                            from tm_bmnota a, tr_area b, tm_nota c
			                            where a.f_bmnota_cancel='f' and a.i_bmnota like '%$cari%' and a.i_spbnew isnull and a.i_nota=c.i_nota
			                            and substring(a.i_nota,9,2)=b.i_area order by a.i_bmnota, a.i_nota limit $num offset $offset
			                          ");
			  return $query->result();
			}
    }
    function bacadetail($inota)
    {
			$this->db->select("a.*, b.e_product_motifname from tm_nota_item a, tr_product_motif b
						             where a.i_nota = '$inota' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						             order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updateheader($ibm, $dbm, $eremark)
    {
    	$this->db->set(
    		array(
			'd_bm'	    => $dbm,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_bm',$ibm);
    	$this->db->update('tm_bm');
    }
    function insertheader($ibm, $inota, $dbm, $eremark)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'i_bmnota'              => $ibm,
			    'd_bmnota'              => $dbm,
			    'i_nota'                => $inota,
          'e_remark'              => $eremark,
          'd_entry'               => $now
    		)
    	);
    	$this->db->insert('tm_bmnota');
    }
    function updatenotabatal($inota, $user)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
    	$this->db->set(
    		array(
			    'i_notabatal_approve'   => $user,
			    'd_notabatal_approve'   => $now
    		)
    	);
    	$this->db->where('i_nota',$inota);
    	$this->db->update('tm_notabatal');
    }
    function insertdetail($ibm,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i)
    {
    	$this->db->set(
    		array(
					'i_bmnota' 	            => $ibm,
					'i_product'	 	          => $iproduct,
					'i_product_grade'	      => $iproductgrade,
					'i_product_motif'	      => $iproductmotif,
					'n_quantity'		        => $nquantity,
					'e_product_name'	      => $eproductname,
					'e_remark'		          => $eremark,
          'n_item_no'             => $i
    		)
    	);
    	$this->db->insert('tm_bmnota_item');
    }
    public function deletedetail($iproduct, $iproductgrade, $ibm, $iproductmotif, $nquantityx, $istore, $istorelocation, $istorelocationbin)
    {
		  $this->db->query("DELETE FROM tm_bm_item WHERE i_bm='$ibm' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif'");
    }
    public function delete($ibm) 
    {
		  $this->db->query("sUpdate tm_bm set f_bm_cancel='t' WHERE i_bm='$ibm'");
#		  $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$cari)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, 
						                    c.e_product_name as nama,c.v_product_mill as harga
						                    from tr_product_motif a,tr_product c
						                    where a.i_product=c.i_product and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumberspb($iarea,$thbl){
      $th   = substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
        $this->db->select(" n_modul_no as max from tm_dgu_no
                          where i_modul='SPB'
                          and substr(e_periode,1,4)='$th'
                          and i_area='$iarea' for update", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           foreach($query->result() as $row){
             $terakhir=$row->max;
           }
           $nospb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no
                            set n_modul_no=$nospb
                            where i_modul='SPB'
                            and substr(e_periode,1,4)='$th'
                            and i_area='$iarea'", false);
           settype($nospb,"string");
           $a=strlen($nospb);
           while($a<6){
             $nospb="0".$nospb;
             $a=strlen($nospb);
           }
           $nospb  ="SPB-".$thbl."-".$nospb;
           return $nospb;
        }else{
           $nospb  ="000001";
           $nospb  ="SPB-".$thbl."-".$nospb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                           values ('SPB','$iarea','$asal',1)");
           return $nospb;
        }
    }
    
      function runningnumbersj($iarea,$thbl,$kons)
    {
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
      if($kons=='t' && $iarea!='PB'){
		    $this->db->select(" n_modul_no as max from tm_dgu_no 
                            where i_modul='SJ'
                            and e_periode='$asal' 
                            and i_area='BK' for update", false);
      }else{
		    $this->db->select(" n_modul_no as max from tm_dgu_no 
                            where i_modul='SJ'
                            and e_periode='$asal' 
                            and i_area='$iarea' for update", false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj  =$terakhir+1;
        if($kons=='t' && $iarea!='PB'){
          $this->db->query(" update tm_dgu_no 
                              set n_modul_no=$nosj
                              where i_modul='SJ'
                              and e_periode='$asal' 
                              and i_area='BK'", false);
        }else{
          $this->db->query(" update tm_dgu_no 
                              set n_modul_no=$nosj
                              where i_modul='SJ'
                              and e_periode='$asal' 
                              and i_area='$iarea'", false);
        }
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
        if($kons=='t' && $iarea!='PB'){
          $nosj  ="SJ-".$thbl."-BK".$nosj;
        }else{
  		  	$nosj  ="SJ-".$thbl."-".$iarea.$nosj;
        }
			  return $nosj;
		  }else{
			  $nosj  ="0001";
        if($kons=='t' && $iarea!='PB'){
          $nosj  ="SJ-".$thbl."-BK".$nosj;
          $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                             values ('SJ','BK','$asal',1)");
        }else{
          $nosj  ="SJ-".$thbl."-".$iarea.$nosj;
          $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                             values ('SJ','$iarea','$asal',1)");
        }
			  return $nosj;
		  }
    }

    function runningnumberdkb($iarea,$thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='DKB'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nodkb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nodkb
                            where i_modul='DKB'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nodkb,"string");
			  $a=strlen($nodkb);
			  while($a<4){
			    $nodkb="0".$nodkb;
			    $a=strlen($nodkb);
			  }
		  	$nodkb  ="DKB-".$thbl."-".$iarea.$nodkb;
			  return $nodkb;
		  }else{
			  $nodkb  ="0001";
		  	$nodkb  ="DKB-".$thbl."-".$iarea.$nodkb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('DKB','$iarea','$asal',1)");
			  return $nodkb;
		  }
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomer($num,$offset,$cari)
    {
			$this->db->select(" * from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                          order by i_customer", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibm,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibm', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_git_penjualan=n_git_penjualan+$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close,n_git_penjualan)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,0,0,0,0,0,0,'f',$qsj) ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '00', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibm,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$ibm'
                                    order by i_trans desc",false);
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ibbk', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qbm, n_saldo_akhir=n_saldo_akhir-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbm)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function carinota($ibmnota)
    {
      $inota='';
			$this->db->select("i_nota from tm_bmnota where i_bmnota = '$ibmnota'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  foreach($query->result() as $xx){
			    $inota=$xx->i_nota;
			  }
				 
			}
			return $inota;
    }
    function spb($ibmnota,$inota,$thbl,$dprocess)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
			$this->db->select("i_spb, i_area from tm_nota where i_nota = '$inota'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  foreach($query->result() as $yy){
     			$this->db->query("delete from tt_spb where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
     			$this->db->query("delete from tt_spb_item where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_spb select * from tm_spb where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_spb_item select * from tm_spb_item where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
  			  $ispb	=$this->mmaster->runningnumberspb($yy->i_area,$thbl);
     			$this->db->query("update tt_spb set i_spb='$ispb', f_spb_stockdaerah='f', d_spb='$dprocess', i_sj=null, d_sj=null, i_nota=null, 
     			                  d_nota=null where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
     			$this->db->query("update tt_spb_item set i_spb='$ispb' where i_spb = '$yy->i_spb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_spb select * from tt_spb where i_spb = '$ispb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_spb_item select * from tt_spb_item where i_spb = '$ispb' and i_area='$yy->i_area'", false);
          $this->db->query("update tm_bmnota set i_spbnew='$ispb', d_spbnew='$dprocess', d_process='$now' where i_bmnota='$ibmnota'", false);
			  }
			  return $ispb;
			}
    }
    function sj($ibmnota,$inota,$thbl,$dprocess)
    {
			$this->db->select(" a.i_sj, a.i_area, b.f_spb_consigment from tm_nota a, tm_spb b 
			                    where a.i_nota = '$inota' and a.i_spb=b.i_spb and a.i_area=b.i_area", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  foreach($query->result() as $yy){
			    $kons=$yy->f_spb_consigment;
     			$this->db->query("delete from tt_nota where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
     			$this->db->query("delete from tt_nota_item where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_nota select * from tm_nota where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_nota_item select * from tm_nota_item where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
  			  $isj	=$this->mmaster->runningnumbersj('00',$thbl,$kons);
     			$this->db->query("update tt_nota set i_sj='$isj', d_sj='$dprocess', d_nota=null, i_dkb=null, d_spb='$dprocess' where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
     			$this->db->query("update tt_nota_item set i_sj='$isj', i_nota=null, d_nota=null where i_sj = '$yy->i_sj' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_nota select * from tt_nota where i_sj = '$isj' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_nota_item select * from tt_nota_item where i_sj = '$isj' and i_area='$yy->i_area'", false);
          $this->db->query("update tm_bmnota set i_sjnew='$isj', d_sjnew='$dprocess' where i_bmnota='$ibmnota'", false);
          $this->db->select(" i_spbnew, substring(i_nota,9,2) as i_area from tm_bmnota where i_bmnota='$ibmnota'", false);
			    $que = $this->db->get();
			    if ($que->num_rows() > 0){
			      foreach($que->result() as $xx){
              $this->db->query("update tm_nota set i_spb='$xx->i_spbnew' where i_sj='$isj'", false);
              $this->db->query("update tm_spb set i_sj='$isj', d_sj='$dprocess' where i_spb='$xx->i_spbnew' and i_area='$xx->i_area'", false);
			      }
			    }
			  }
			  return $query->result();
			}
    }
    function nosj($ibmnota,$inota,$thbl,$dprocess)
    {
			$this->db->select(" a.i_sj, a.i_area, b.f_spb_consigment from tm_nota a, tm_spb b 
			                    where a.i_nota = '$inota' and a.i_spb=b.i_spb and a.i_area=b.i_area", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  foreach($query->result() as $yy){
			    $kons=$yy->f_spb_consigment;
  			  $isj	=$this->mmaster->runningnumbersj('00',$thbl,$kons);
			  }
			  return $isj;
			}
    }

    function dkb($ibmnota,$inota,$thbl,$dprocess)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry 	= $row->c;
			$this->db->select(" i_dkb, i_area from tm_nota where i_nota = '$inota'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  foreach($query->result() as $yy){
     			$this->db->query("delete from tt_dkb where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("delete from tt_dkb_item where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("delete from tt_dkb_ekspedisi where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);

     			$this->db->query("insert into tt_dkb select * from tm_dkb where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_dkb_item select * from tm_dkb_item where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tt_dkb_ekspedisi select * from tm_dkb_ekspedisi where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
  			  $idkb	=$this->mmaster->runningnumberdkb('00',$thbl);
     			$this->db->query("update tt_dkb set i_dkb='$idkb', d_dkb='$dprocess', d_entry='$dentry' where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("update tt_dkb_item set i_dkb='$idkb', d_dkb='$dprocess' where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("update tt_dkb_ekspedisi set i_dkb='$idkb', d_dkb='$dprocess' where i_dkb = '$yy->i_dkb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_dkb select * from tt_dkb where i_dkb = '$idkb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_dkb_item select * from tt_dkb_item where i_dkb = '$idkb' and i_area='$yy->i_area'", false);
     			$this->db->query("insert into tm_dkb_ekspedisi select * from tt_dkb_ekspedisi where i_dkb = '$idkb' and i_area='$yy->i_area'", false);
          $this->db->query("update tm_bmnota set i_dkbnew='$idkb', d_dkbnew='$dprocess' where i_bmnota='$ibmnota'", false);
          $this->db->select(" i_spbnew, i_sjnew, i_dkbnew, d_dkbnew, substring(i_nota,9,2) as i_area from tm_bmnota where i_bmnota='$ibmnota'", false);
			    $que = $this->db->get();
			    if ($que->num_rows() > 0){
			      foreach($que->result() as $xx){
              $this->db->query("update tm_nota set i_nota=null, f_nota_cancel='f', i_dkb='$idkb', d_dkb='$xx->d_dkbnew',
                                i_seri_pajak=null, i_faktur_komersial=null, d_pajak=null, n_faktur_komersialprint=0, n_pajak_print=0,
                                d_pajak_print=null
                                where i_sj='$xx->i_sjnew'", false);
			      }
			    }
			  }
			  return $idkb;
			}
    }

    function bacasjdetail($isj)
    {
			$this->db->select(" * from tm_nota_item where i_sj = '$isj'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
			  return $query->result();
			}
    }
}
?>
