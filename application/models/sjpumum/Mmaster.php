<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function runningnumbersj($iarea, $thbl)
	{
		$th	= substr($thbl, 0, 4);
		$asal = $thbl;
		$thbl = substr($thbl, 2, 2) . substr($thbl, 4, 2);
		$this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SJU'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$nosj  = $terakhir + 1;
			$this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nosj
                            where i_modul='SJU'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			settype($nosj, "string");
			$a = strlen($nosj);
			while ($a < 4) {
				$nosj = "0" . $nosj;
				$a = strlen($nosj);
			}

			$nosj  = "SJP-" . $thbl . "-" . $iarea . $nosj;
			return $nosj;
		} else {
			$nosj  = "0001";
			$nosj  = "SJP-" . $thbl . "-" . $iarea . $nosj;
			$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SJU','$iarea','$asal',1)");
			return $nosj;
		}
	}

	function insertheader($isj,  $isender, $esender, $esendercompany, $erecipient, $erecipientcompany, $dsjp, $iarea, $eremarkhead, $iapprove)
	{
		return $this->db->query(
			"INSERT INTO tm_sjp_umum
			(i_sjp, i_area, d_sjp, i_sender, e_sender, e_sender_company, e_recipient, e_recipient_company, e_remark, i_approve)
			VALUES('$isj', '$iarea', '$dsjp',  '$isender', '$esender', '$esendercompany', '$erecipient', '$erecipientcompany', '$eremarkhead', '$iapprove')
			RETURNING id"
		);
	}

	function insertdetail($id, $isj, $iarea, $dsjp, $iproduct, $eproductname, $qty, $satuan, $eremark, $i)
	{
		$this->db->set(
			array(
				'id_sjp'			=> $id,
				'i_product'			=> $iproduct,
				'e_product_name'	=> $eproductname,
				'n_quantity'		=> $qty,
				'n_satuan'			=> $satuan,
				'e_remark'			=> $eremark,
				'n_item_no'         => $i
			)
		);
		$this->db->insert('tm_sjp_umum_item');
	}

	function updatedetail($id, $idproduct, $isj, $iproduct, $eproductname, $qty, $satuan, $eremark, $i)
	{
		$this->db->set(
			array(
				'n_quantity_receive' => $qty,
			)
		);
		$this->db->where('id', $idproduct);
		$this->db->update('tm_sjp_umum_item');
	}

	function updateheader($id, $isjp, $esender, $esendercompany, $erecipient, $erecipientcompany, $dsjp, $iarea, $eremarkhead)
	{
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate	= $row->c;

		$this->db->set(
			array(
				'i_sjp'					=> $isjp,
				'i_area'				=> $iarea,
				'd_sjp'					=> $dsjp,
				'e_sender'				=> $esender,
				'e_sender_company'		=> $esendercompany,
				'e_recipient'			=> $erecipient,
				'e_recipient_company'	=> $erecipientcompany,
				'e_remark'				=> $eremarkhead,
				'd_sjp_update'			=> $dupdate,
				'd_notapprove'			=> null,
			)
		);
		$this->db->where('id', $id);
		$this->db->update('tm_sjp_umum');

		$this->db->query("delete from tm_sjp_umum_item where id_sjp='$id'");
	}

	function baca($id)
	{
		$this->db->select(" id, i_sjp, a.i_area, b.e_area_name, to_char(d_sjp, 'dd-mm-yyyy') as d_sjp, to_char(d_sjp_receive , 'dd-mm-yyyy') as d_sjp_receive, e_sender, e_sender_company, 
							e_recipient, e_recipient_company, e_remark , a.d_approve, a.d_notapprove, a.e_receive, u1.e_user_name
							from tm_sjp_umum a 
							inner join tr_area b on (a.i_area = b.i_area) /*Sesuaikan Dengan Relasi Program*/
							left join tm_user u1 on a.i_approve = u1.i_user
							where id = '$id' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}

	function bacadetail($id)
	{
		$this->db->select(" id, i_product, e_product_name, n_quantity, n_quantity_receive , n_satuan, e_remark from tm_sjp_umum_item where id_sjp = '$id' order by n_item_no ", false);

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function fprinted($id)
	{
		$this->db->query(" UPDATE tm_sjp_umum SET n_print = n_print+1, f_printed='t' WHERE id = '$id' ");
	}

	public function reprint($id, $isjp, $folder)
	{
		$this->db->query(" UPDATE tm_sjp_umum SET f_printed='f' WHERE id = '$id' ");

		print "<script>alert(\"Nomor SJP : $isjp Bisa dicetak ulang, terimakasih.\");show(\"$folder/cform/index/\",\"#main\");</script>";
	}

	function approve($id)
	{
		$query 		= $this->db->query("SELECT current_timestamp as c");
		$row   		= $query->row();
		$dapprove	= $row->c;

		$iuser = $this->session->userdata("user_id");

		$this->db->query(" UPDATE tm_sjp_umum SET d_approve = '$dapprove' WHERE id = '$id' ");
	}

	function reject($id, $eremark)
	{
		$query 			= $this->db->query("SELECT current_timestamp as c");
		$row   			= $query->row();
		$dnotapprove 	= $row->c;

		$iuser = $this->session->userdata("user_id");

		$this->db->query(" UPDATE tm_sjp_umum SET d_notapprove = '$dnotapprove', e_remark_notapprove = '$eremark' WHERE id = '$id' ");
	}

	function receive($id, $eremark)
	{
		$query 			= $this->db->query("SELECT current_timestamp as c");
		$row   			= $query->row();
		$dsjpreceive 	= $row->c;

		$iuser = $this->session->userdata("user_id");

		$this->db->query(" UPDATE tm_sjp_umum SET d_sjp_receive = '$dsjpreceive', e_receive = '$eremark' WHERE id = '$id' ");
	}

	function bacasemua($cari, $num, $offset, $iuser)
	{
		if ($offset == '')
			$offset = 0;

		$this->db->select(" id, i_sjp, l1.e_area_name, to_char(d_sjp, 'mm-dd-yyyy') as d_sjp,  to_char(d_sjp_receive, 'mm-dd-yyyy') as d_sjp_receive, l2.e_sender, l3.e_recipient, a.e_remark, f_sjp_cancel,
							case when f_sjp_cancel = true then 'Batal' else 'Aktif' end as f_sjp_cancel_name, f_printed, 
							case when n_print = 0 then 'Belum' else 'Sudah ' || n_print::text || 'X' end as n_print,
							case 	
								when f_sjp_cancel = true then 'Batal'
								when d_approve is null and d_notapprove is null then 'Menunggu Approval'
								when d_approve is null and d_notapprove is not null then 'Rejected [' || coalesce(e_remark_notapprove, '') || ']'
								when d_approve is not null and d_sjp_receive is null then 'Approved'
								when d_sjp_receive is not null then 'Received'
							end as status_sjp, a.i_approve, a.d_approve, a.d_notapprove, a.e_receive/* , a.d_sjp_receive */
							from tm_sjp_umum a 
							inner join tr_area b on (a.i_area = b.i_area) /*Sesuaikan Dengan Relasi Program*/
							,lateral (select b.i_area || ' - ' || b.e_area_name as e_area_name) l1
							,lateral (select coalesce(e_sender,'') || case when e_sender is null and e_sender_company is null then '' else ' - ' || coalesce(a.e_sender_company,'') end as e_sender) l2
							,lateral (select coalesce(e_recipient ,'') || case when e_recipient  is null and e_recipient_company  is null then '' else ' - ' || coalesce(a.e_recipient_company ,'') end as e_recipient) l3
							WHERE (a.i_sender = '$iuser' OR a.i_approve = '$iuser')
							and (upper(i_sjp) LIKE '%$cari%' OR upper(l2.e_sender) LIKE '%$cari%') /* AND a.i_area in (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser') */
							ORDER BY d_sjp DESC 
							LIMIT $num OFFSET $offset ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacustomer($num, $offset, $area)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select * from(
                              select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer a, tr_city b where a.i_area='$area'
                              and a.i_area=b.i_area and a.i_city=b.i_city and a.f_customer_aktif = 't'
                              union all
                              select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer_tmp a 
                              left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                              where a.i_area='$area' and a.i_customer like '%000'
                              ) as a
                              order by a.e_customer_name limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function runningnumber($thbl)
	{
		#    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		#		  $row   	= $query->row();
		#		  $thbl	= $row->c;
		$th		= substr($thbl, 0, 2);
		$this->db->select(" max(substr(i_rrkh,11,6)) as max from tm_rrkh 
				    			where substr(i_rrkh,6,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$terakhir = $row->max;
			}
			$norrkh  = $terakhir + 1;
			settype($norrkh, "string");
			$a = strlen($norrkh);
			while ($a < 6) {
				$norrkh = "0" . $norrkh;
				$a = strlen($norrkh);
			}
			$norrkh  = "rrkh-" . $thbl . "-" . $norrkh;
			return $norrkh;
		} else {
			$norrkh  = "000001";
			$norrkh  = "rrkh-" . $thbl . "-" . $norrkh;
			return $norrkh;
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select(" * from tm_rrkh where upper(i_rrkh) like '%$cari%' 
					order by i_rrkh", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricustomer($cari, $num, $offset, $area)
	{
		if ($offset == '')
			$offset = 0;
		$query = $this->db->query(" select * from(
                                select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer a, tr_city b where a.i_area='$area'
                                and a.i_area=b.i_area and a.i_city=b.i_city and a.f_customer_aktif = 't'
                                and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                union all
                                select a.i_customer, a.e_customer_name, b.i_city, b.e_city_name from tr_customer_tmp a 
                                left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                                where a.i_area='$area' and a.i_customer like '%000' 
                                and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                ) as a
                                order by a.i_customer desc limit $num offset $offset", false);
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $allarea, $iuser)
	{
		if ($allarea == 't') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num, $offset);
		}

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset, $allarea, $iuser)
	{
		if ($allarea == 't') {
			$this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num, $offset);
		}

		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacasalesman($num, $offset, $area)
	{
		$iuser   = $this->session->userdata('user_id');
		$sales = '';
		$query = $this->db->query("	select i_salesman from tm_user_area where i_area='$area' and i_user='$iuser'", false);
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				if ($row->i_salesman != null) {
					$sales = $row->i_salesman;
				}
			}
			if ($sales == '')
				$this->db->select(" * from tr_salesman where i_area='$area' and f_salesman_aktif='t'", false)->limit($num, $offset);
			else
				$this->db->select("	* from tr_salesman where /* i_area='$area' and  */i_salesman='$sales' and f_salesman_aktif='t'", false)->limit($num, $offset);
		} else {
			$this->db->select(" * from tr_salesman where i_area='$area' and f_salesman_aktif='t'", false)->limit($num, $offset);
		}



		#		$this->db->select(" distinct(i_salesman),e_salesman_name from tr_customer_salesman 
		#                        where i_area='$area' order by i_salesman",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function carisalesman($cari, $num, $offset, $area)
	{
		$this->db->select("	distinct(i_salesman),e_salesman_name from tr_customer_salesman where /* i_area='$area' and */ 
 												(upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
												order by i_salesman", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacacity($num, $offset, $area)
	{
		$this->db->select("i_city, e_city_name from tr_city where i_area='$area' order by e_city_name", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caricity($cari, $num, $offset, $area)
	{
		$this->db->select("	i_city, e_city_name from tr_city
												where i_area='$area' and (upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%')
												order by e_city_name", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacakunjungan($num, $offset)
	{
		$this->db->select("i_kunjungan_type, e_kunjungan_typename from tr_kunjungan_type order by i_kunjungan_type", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function carikunjungan($cari, $num, $offset)
	{
		$this->db->select("	i_kunjungan_type, e_kunjungan_typename from tr_kunjungan_type
												where upper(e_kunjungan_typename) like '%$cari%' or upper(i_kunjungan_type) like '%$cari%'
												order by i_kunjungan_type", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
