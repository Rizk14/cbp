<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	// function bacaperiode($dfrom, $dto)
	// {
	// 	$perfrom = substr($dfrom, 6, 4) . substr($dfrom, 3, 2);
	// 	$perto  = substr($dto, 6, 4) . substr($dto, 3, 2);
	// 	$sql = " b.i_area, a.e_area_shortname, b.i_customer, c.e_customer_name, to_char(b.d_kn,'dd-mm-yyyy') as d_kn, b.i_kn,
	//   		  b.d_pajak,
	//           b.i_salesman, d.e_customer_pkpnpwp, b.v_gross, b.v_discount, b.v_netto, b.f_kn_cancel,b.e_remark, (n_tax / 100 + 1) excl_divider, n_tax / 100 n_tax_val 
	//           from tm_kn b, tr_area a, tr_customer c, tr_customer_pkp d, tr_tax_amount xx
	//           where a.i_area=b.i_area and b.i_customer=c.i_customer and b.i_customer=d.i_customer
	//           and b.d_pajak between d_start and d_finish
	//           and to_char(b.d_kn,'yyyymm') >='$perfrom' and to_char(b.d_kn,'yyyymm') <='$perto' 
	//           order by a.i_area,  b.i_kn";
	// 	$this->db->select($sql, false);
	// 	$query = $this->db->get();
	// 	if ($query->num_rows() > 0) {
	// 		return $query->result();
	// 	}
	// }
	function bacaperiode($dfrom, $dto)
	{
		$perfrom = substr($dfrom, 6, 4) . substr($dfrom, 3, 2) . substr($dfrom, 0, 2);
		$perto  = substr($dto, 6, 4) . substr($dto, 3, 2) . substr($dto, 0, 2);
		$sql = " b.i_area, a.e_area_shortname, b.i_customer, c.e_customer_name, to_char(b.d_kn,'dd-mm-yyyy') as d_kn, b.i_kn,
              b.i_salesman, d.e_customer_pkpnpwp, b.v_gross, b.v_discount, b.v_netto, b.f_kn_cancel, b.d_pajak
              from tm_kn b, tr_area a, tr_customer c, tr_customer_pkp d
              where a.i_area=b.i_area and b.i_customer=c.i_customer and b.i_customer=d.i_customer
              and to_char(b.d_kn,'yyyymmdd') >='$perfrom' and to_char(b.d_kn,'yyyymmdd') <='$perto' 
              order by a.i_area,  b.i_kn";
		$this->db->select($sql, false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') { # or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') { # or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num, $offset);
		} else {
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
