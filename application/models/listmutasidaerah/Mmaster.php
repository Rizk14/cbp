<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }
  function baca($istorelocation, $iperiode, $istore, $num, $offset, $cari)
  {
    $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='MTS'
                          and i_area='$istore' for update", false); #and i_store_location='$istorelocation' for update", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $this->db->query("update tm_dgu_no 
                          set e_periode='$iperiode'
                          where i_modul='MTS' and i_area='$istore' and i_store_location='$istorelocation'", false);
    } else {
      $this->db->query("insert into tm_dgu_no(i_modul, i_area, e_periode, i_store_location) 
                          values ('MTS','$istore','$iperiode', '$istorelocation')");
    }
    $query->free_result();
    if ($iperiode > '201512') {
      $this->db->select("	  a.e_product_groupname,
                            a.i_product,
                            i_product_grade,
                            sum(n_saldo_awal) AS n_saldo_awal,
                            sum(n_mutasi_pembelian) AS n_mutasi_pembelian,
                            sum(n_mutasi_returoutlet) AS n_mutasi_returoutlet,
                            sum(n_mutasi_bbm) AS n_mutasi_bbm,
                            sum(n_mutasi_penjualan) AS n_mutasi_penjualan,
                            sum(n_mutasi_bbk) AS n_mutasi_bbk,
                            sum(n_saldo_akhir) AS n_saldo_akhir,
                            sum(n_mutasi_ketoko) AS n_mutasi_ketoko,
                            sum(n_mutasi_daritoko) AS n_mutasi_daritoko,
                            sum(n_mutasi_git) AS n_saldo_git,
                            e_mutasi_periode,
                            i_store,
                            sum(n_git_penjualan) AS n_git_penjualan,
                            sum(n_git_penjualanasal) AS n_git_penjualanasal,
                            sum(n_mutasi_gitasal) AS n_mutasi_gitasal,
                            sum(n_saldo_stockopname) AS n_saldo_stockopname,
                            a.e_product_name,
                            i_store_location,
                            sum(adjus) AS adjustment,
                            ctg.e_sales_categoryname 
                          FROM
                            f_mutasi_stock_daerah_saldoakhir2('$iperiode', '$istore', '$istorelocation') a
                            LEFT JOIN tr_product b ON a.i_product = b.i_product 
                            LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
                          GROUP BY
                            a.i_product,
                            i_product_grade,
                            e_product_groupname,
                            e_mutasi_periode,
                            i_store,
                            a.e_product_name,
                            i_store_location,
                            ctg.e_sales_categoryname 
                          ORDER BY
                            e_product_groupname,
                            e_product_name,
                            i_product ", false); #->limit($num,$offset);
    } else {
      $this->db->select("	e_product_groupname, i_product, i_product_grade,sum(n_saldo_awal) as n_saldo_awal, 
                            sum(n_mutasi_pembelian) as n_mutasi_pembelian,
                            sum(n_mutasi_returoutlet) as n_mutasi_returoutlet, sum(n_mutasi_bbm) as n_mutasi_bbm,
                            sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
                            sum(n_mutasi_bbk) as n_mutasi_bbk, sum(n_saldo_akhir) as n_saldo_akhir, sum(n_mutasi_ketoko) as n_mutasi_ketoko, 
                            sum(n_mutasi_daritoko) as n_mutasi_daritoko, sum(n_mutasi_git) as n_saldo_git, e_mutasi_periode, i_store,
                            sum(n_git_penjualan) as n_git_penjualan, sum(n_git_penjualanasal) as n_git_penjualanasal,
                            sum(n_mutasi_gitasal) as n_mutasi_gitasal,
                            sum(n_saldo_stockopname) as n_saldo_stockopname, e_product_name, i_store_location
                            from f_mutasi_stock_daerah('$iperiode','$istore','$istorelocation')
                            group by i_product, i_product_grade, e_product_groupname, e_mutasi_periode, i_store, e_product_name, 
                                     i_store_location
                            order by e_product_groupname, e_product_name, i_product ", false); #->limit($num,$offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
    $query->free_result();
  }

  function bacatotal($istorelocation, $iperiode, $istore)
  {
    $query = $this->db->query("  SELECT
                                  ctg.i_sales_category,
                                  CASE WHEN ctg.e_sales_categoryname ISNULL THEN '-'
                                  ELSE ctg.e_sales_categoryname END AS e_sales_categoryname,
                                  sum(n_saldo_akhir) AS saldoakhir,
                                  sum(n_saldo_akhir * a.v_product_retail) AS rpsaldoakhir,
                                  sum(n_saldo_stockopname + n_mutasi_git + n_git_penjualan) AS saldostockopname,
                                  sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) * a.v_product_retail) AS rpstockopname,
                                  sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) AS selisih,
                                  sum(((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) * a.v_product_retail) AS rpselisih
                                FROM
                                  f_mutasi_stock_daerah_saldoakhir2('$iperiode','$istore','$istorelocation') a
                                  LEFT JOIN tr_product b ON a.i_product = b.i_product
                                  LEFT JOIN tr_product_price ppr ON a.i_product = ppr.i_product AND ppr.i_price_group = '00'
                                  LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
                                GROUP BY ctg.i_sales_category, ctg.e_sales_categoryname
                                ORDER BY 1 ");
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }

  function detail($istorelocation, $iperiode, $iarea, $iproduct)
  {
    $this->db->select("	b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product, e.e_customer_name, a.urut,
		                sum(a.in) as in, sum(a.out) as out, sum(a.git) as git, sum(a.gitpenjualan) as gitpenjualan
		                FROM tr_product b, vmutasidetail_new a
		                left join tm_nota_item c on c.i_sj=a.ireff and a.product=c.i_product
		                left join tm_spb d on d.i_sj=c.i_sj
		                left join tr_customer e on d.i_customer=e.i_customer 
		                WHERE 
		                  b.i_product = a.product and a.loc='$istorelocation' AND
		                  a.periode='$iperiode' AND a.area='$iarea' AND a.product='$iproduct' 
		                group by b.e_product_name, a.ireff, a.dreff, a.area, a.periode, a.product,
		                         e.e_customer_name, a.urut
		                order by dreff, urut, ireff", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacaexcel($iperiode, $istore, $cari)
  {
    $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ", false); #->limit($num,$offset);
    $query = $this->db->get();
    return $query;
  }

  function bacaarea($num, $offset, $iuser)
  {
    $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_area=b.i_store and b.i_store=c.i_store
                        and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
                        and not a.i_store in ('AA','PB') and c.i_store_location='00'
                        order by b.i_store, c.i_store_location", false)->limit($num, $offset);
    # and i_store_location='00'
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function cariarea($cari, $num, $offset, $iuser)
  {
    $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                         from tr_area a, tr_store b, tr_store_location c
                         where  a.i_area=b.i_store and b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						 		and (i_area in( select i_area from tm_user_area where i_user='$iuser') )
						 		and not a.i_store in ('AA','PB') and c.i_store_location='00' 
						 		order by a.i_store ", FALSE)->limit($num, $offset);
    # and i_store_location='00'
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
