<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacasemua($area1,$area2,$area3,$area4,$area5,$cari, $num,$offset,$dfrom,$dto)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
					              where a.i_customer=c.i_customer and a.i_area=b.i_area and (upper(a.i_nota) like '%$cari%' or upper(a.i_nota_old) like '%$cari%')
                        and not a.i_faktur_komersial isnull and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
          							order by a.i_faktur_komersial",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
	function cari($cari,$num,$offset,$dfrom,$dto)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
					              where a.i_customer=c.i_customer and a.i_area=b.i_area and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                        and not a.i_faktur_komersial isnull and (upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' 
                				or upper(a.i_nota) like '%$cari%' or upper(a.i_nota_old) like '%$cari%')
                				order by a.i_faktur_komersial",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function delete($inota,$area) 
    {
		$this->db->query("update tm_nota set i_faktur_komersial=NULL, i_seri_pajak=NULL, d_pajak=NULL where i_nota='$inota' and i_area='$area'");
    }
}
?>
