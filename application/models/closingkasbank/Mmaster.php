<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        #$this->CI =& get_instance();
    }

    public function get_closingperiode()
    {
        $this->db->select('*');
        $this->db->from('tm_periode');
        return $this->db->get();
    }

    public function get_closing_kasbank()
    {
        $this->db->select('*');
        $this->db->from('tm_closing_kas_bank');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function cekclosing($dateclosing, $dateopen)
    {
        $this->db->select('*');
        $this->db->from('tm_closing_kas_bank');
        return $this->db->get();
    }

    public function closing($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kb'      => $dateclosing,
            'd_open_kb'         => $dateopen,
            'd_closing_kbin'    => $dateclosing,
            'd_open_kbin'       => $dateopen,
            'd_closing_kbank'   => $dateclosing,
            'd_open_kbank'      => $dateopen,
            'd_closing_kbankin' => $dateclosing,
            'd_open_kbankin'    => $dateopen,
            'd_closing_kk'      => $dateclosing,
            'd_open_kk'         => $dateopen,
            'd_closing_kkin'    => $dateclosing,
            'd_open_kkin'       => $dateopen,
        );
        $this->db->insert('tm_closing_kas_bank', $data);
    }

    public function closeperiodeall($closingperiode)
    {
        $this->db->query(" UPDATE tm_periode SET i_periode = '$closingperiode' ", FALSE);
    }

    public function closingkb($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kb'        => $dateclosing,
            'd_open_kb'            => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }

    public function closingkbin($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kbin'    => $dateclosing,
            'd_open_kbin'        => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }

    public function closingkbank($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kbank'    => $dateclosing,
            'd_open_kbank'        => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }

    public function closingkbankin($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kbankin'    => $dateclosing,
            'd_open_kbankin'    => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }

    public function closingkk($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kk'        => $dateclosing,
            'd_open_kk'            => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }

    public function closingkkin($dateclosing, $dateopen)
    {
        $data = array(
            'd_closing_kkin'    => $dateclosing,
            'd_open_kkin'        => $dateopen,
        );
        $this->db->update('tm_closing_kas_bank', $data);
    }
}
