<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

	function cekheaderspmb($ispmb,$iarea){
		$this->db->select(" * from tm_spmb where i_spmb='$ispmb' 
								 and i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}	
	}
	
	function updateheaderspmb($ispmb,$iarea,$now){
		$query=$this->db->query("update tm_spmb set d_update = '$now' where i_spmb = '$ispmb'
								 and i_area = '$iarea'");
	}
	
    function insertheaderspmb($ispmb, $dspmb, $istore, $istorelocation, $iarea, $iapprove2, $dapprove2, $eremark, $now)
    {
		$this->db->set(
    		array(
			  'i_spmb'    			=> $ispmb,
			  'i_area'    			=> $iarea,
			  'd_spmb'    			=> $dspmb,
			  'f_op'      			=> 'false',
			  'n_print'   			=> 0,
			  'f_spmb_acc'      => 'true',
			  'i_approve2'			=> $iapprove2,
			  'd_approve2'			=> $dapprove2,
			  'e_remark'		    => $eremark,
			  'd_entry'				=> $now
			)
    	);
    	$this->db->insert('tm_spmb');
    }
	
	function insertdetailspmb($eproduct,$ispmb,$iproduct,$iproductmotif,$iarea,$nsaldo,$nacc,$norder,$vunitprice,$item,$eremark)
    {
			$this->db->set(
    		array(
			  'i_spmb'    			=> $ispmb,
			  'i_product'    		=> $iproduct,
			  'i_product_motif' => $iproductmotif,
			  'i_product_grade' => 'A',
			  'e_product_name'	=> $eproduct,
			  'n_saldo'				  => $nsaldo,
			  'n_acc' 				  => $nacc,
			  'n_order' 				  => $norder,
			  'v_unit_price'		=> $vunitprice,
			  'e_remark'			  => $eremark,
			  'i_area'    			=> $iarea,
			  'n_item_no'			  => $item
			)
    	);
    	$this->db->insert('tm_spmb_item');
		}
	function inserttempspmb($ispmb,$iproduct,$iproductmotif,$iarea)
    {
  	$konek 	= "host=192.168.0.101 user=postgres dbname=omiland_tk port=5432 password=d14logueGU";
		$db    	= pg_connect($konek);

		$sql	= "insert into temp_transfer_spmb (i_spmb,i_product, i_product_motif,i_product_grade, i_area)VALUES 
				  ('$ispmb','$iproduct','$iproductmotif','A','$iarea')";
		pg_query($sql);
    }
}
?>
