<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$isupplier,$num,$offset)
    {
		/*$this->db->select(" a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
    (b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
        case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
        case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
        left join tm_op_item b on (a.i_op=b.i_op)
        inner join tr_supplier c on (a.i_supplier=c.i_supplier)
      where to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
      and a.i_supplier='$isupplier' and a.f_op_cancel='f' order by a.i_op
  ",false)->limit($num,$offset);*/
      $this->db->select(" DISTINCT
                a.i_op,
              	a.i_area,
              	a.i_reff,
              	a.d_reff,
              	a.e_op_remark,
              	a.d_op,
              	b.i_product,
              	b.e_product_name,
              	b.v_product_mill,
              	b.n_order,
              	(b.n_order * b.v_product_mill) AS rpop,
              	e.e_supplier_name,
              	CASE WHEN 
              		b.n_delivery isnull THEN 0
              	ELSE
              		b.n_delivery
              	END,
              	(b.n_delivery * b.v_product_mill) AS rpdo,
              	CASE WHEN 
              		b.n_delivery isnull THEN b.n_order
              	ELSE
              		b.n_order - b.n_delivery 
              	END AS pending
              FROM 
              	tm_op a
              	LEFT JOIN tm_op_item b ON (a.i_op = b.i_op)
              	LEFT JOIN tm_do c ON (a.i_op = c.i_op AND a.i_area = c.i_area AND a.i_supplier = c.i_supplier)
              	LEFT JOIN tm_do_item d ON (c.i_do = d.i_do AND b.i_product = d.i_product)
              	INNER JOIN tr_supplier e ON (a.i_supplier = e.i_supplier AND a.i_supplier = c.i_supplier)
              WHERE 
              	to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND to_char(c.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND a.i_supplier = '$isupplier'
              	AND a.f_op_cancel = 'f'
              	AND c.f_do_cancel = 'f'
              ORDER BY b.i_product
      ", FALSE)->limit($num, $offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$isupplier,$cari,$num,$offset)
    {
			  /*$this->db->select(" a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
            (b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
            case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
            case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
            left join tm_op_item b on (a.i_op=b.i_op)
            inner join tr_supplier c on (a.i_supplier=c.i_supplier)
            where to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
            and(upper(b.i_product) like '%$cari%' or upper(b.e_product_name) like '%$cari%')
            and a.i_supplier='$isupplier' and a.f_op_cancel='f' order by a.i_op",false)->limit($num,$offset);*/

        $this->db->select(" DISTINCT a.i_op,
              	a.i_area,
              	a.i_reff,
              	a.d_reff,
              	a.e_op_remark,
              	a.d_op,
              	b.i_product,
              	b.e_product_name,
              	b.v_product_mill,
              	b.n_order,
              	(b.n_order * b.v_product_mill) AS rpop,
              	e.e_supplier_name,
              	CASE WHEN 
              		b.n_delivery isnull THEN 0
              	ELSE
              		b.n_delivery
              	END,
              	(b.n_delivery * b.v_product_mill) AS rpdo,
              	CASE WHEN 
              		b.n_delivery isnull THEN b.n_order
              	ELSE
              		b.n_order - b.n_delivery 
              	END AS pending
              FROM 
              	tm_op a
              	LEFT JOIN tm_op_item b ON (a.i_op = b.i_op)
              	LEFT JOIN tm_do c ON (a.i_op = c.i_op AND a.i_area = c.i_area AND a.i_supplier = c.i_supplier)
              	LEFT JOIN tm_do_item d ON (c.i_do = d.i_do AND b.i_product = d.i_product)
              	INNER JOIN tr_supplier e ON (a.i_supplier = e.i_supplier AND a.i_supplier = c.i_supplier)
              WHERE 
              	to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND to_char(c.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND a.i_supplier = '$isupplier'
              	AND a.f_op_cancel = 'f'
                AND c.f_do_cancel = 'f'
                AND (UPPER(b.i_product) ILIKE '%$cari%' OR UPPER(b.e_product_name) ILIKE '%$cari%')
              ORDER BY b.i_product",FALSE)->limit($num, $offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisupplier($cari,$num,$offset)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier 
				     where upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%' order by i_supplier", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
