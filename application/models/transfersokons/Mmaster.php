<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    		#$this->CI =& get_instance();
    }
    function baca($istockopname,$icustomer,$iarea)
    {
		  $this->db->select("i_sopb, to_char(d_sopb,'dd-mm-yyyy') as d_sopb 
                         from tm_sopb
                         where i_sopb = '$istockopname'
                         and i_customer='$icustomer' and i_area='$iarea'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($istockopname,$icustomer)
    {
		  $this->db->select("	* from tm_sopb_item 
							  inner join tr_product_motif on (tm_sopb_item.i_product=tr_product_motif.i_product
															  and tm_sopb_item.i_product_motif=tr_product_motif.i_product_motif)
						     	where tm_sopb_item.i_sopb = '$istockopname'
							  and tm_sopb_item.i_customer='$icustomer'
						     	order by tm_sopb_item.i_product", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacacust($ispg)
    {
      // $this->db->select("	i_customer from tr_spg where i_spg = UPPER('$ispg')", false);
      $this->db->select("	i_customer from tr_customer ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function insertheader($istockopname, $dstockopname, $icustomer, $iarea, $ispg)
    {
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
    			'i_sopb' 		    => $istockopname,
    			'd_sopb' 		    => $dstockopname,
					'i_customer' 		=> $icustomer,
					'i_area'				=> $iarea,
					'i_spg'				  => $ispg,
        	'd_entry'       => $dentry
    		)
    	);
    	$this->db->insert('tm_sopb');
### update ke mutasi header
      $emutasiperiode='20'.substr($istockopname,3,4);
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13){
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
      }
      $query=$this->db->query(" select * from tm_mutasi_headerconsigment
                                where i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if($query->num_rows()>0){
        $this->db->query("   
                                UPDATE tm_mutasi_headerconsigment
                                set i_stockopname_akhir='$istockopname'
                                where i_customer='$icustomer' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      }else{
        $this->db->query("   
                                insert into tm_mutasi_headerconsigment values
                                ('$icustomer','$emutasiperiode',null,'$istockopname')
                                ",false);
      }
      $query=$this->db->query(" select * from tm_mutasi_headerconsigment
                                where i_customer='$icustomer' and e_mutasi_periode='$perdpn'
                              ",false);
      if($query->num_rows()>0){
        $this->db->query("   
                                UPDATE tm_mutasi_headerconsigment
                                set i_stockopname_awal='$istockopname'
                                where i_customer='$icustomer' and e_mutasi_periode='$perdpn'
                              ",false);
      }else{
        $this->db->query("   
                                insert into tm_mutasi_headerconsigment values
                                ('$icustomer','$perdpn','$istockopname',null)
                                ",false);
      }
### end update ke mutasi header
    }
    function insertdetail(	$iproduct, $iproductgrade, $eproductname, $nstockopname, $istockopname, 
							$icustomer, $iproductmotif, $dstockopname, $iarea, $i)
    {
#      $th=substr($dstockopname,0,4);
#      $bl=substr($dstockopname,5,2);
#      $pr=$th.$bl;
      $pr='20'.substr($istockopname,3,4);
    	$this->db->set(
    		array(
    	  'i_sopb'         			=> $istockopname,
				'd_sopb'      			  => $dstockopname,
				'i_customer'		      => $icustomer,
				'i_product' 			    => $iproduct,
				'i_product_grade'		  => $iproductgrade,
				'e_product_name'		  => $eproductname,
				'i_product_motif'		  => $iproductmotif,
				'n_sopb' 		  => $nstockopname,
				'i_area'				      => $iarea,
        'e_mutasi_periode'    => $pr,
        'n_item_no'           => $i
    		)
    	);
    	
    	$this->db->insert('tm_sopb_item');
    }
    function updateheader($istockopname, $dstockopname, $icustomer)
    {
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
    	$data = array(
               	'i_sopb'         			=> $istockopname,
               	'd_sopb'         			=> $dstockopname,
			       		'i_customer'					=> $icustomer,
								'f_sopb_cancel'       => 'f',
                'd_update'            => $dentry
            );
		$this->db->where('i_sopb', $istockopname);
		$this->db->where('i_customer', $icustomer);
		$this->db->update('tm_sopb', $data); 
    }
   /* public function deletedetail( $iproduct, $iproductgrade, $istockopname, 
				       			  $icustomer, $iproductmotif) 
    {
		  $this->db->query("	DELETE FROM tm_stockopname_item WHERE i_stockopname='$istockopname'
							  and i_product_motif='$iproductmotif'
							  and i_product='$iproduct' 
							  and i_product_grade='$iproductgrade'
							  and i_customer='$icustomer'");
  		return TRUE;
    }
    public function delete($istockopname, $icustomer) 
    {
  		return TRUE;
    }*/
    function bacasemua()
    {
		  $this->db->select("	* from tm_sopb order by i_sopb desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
   /* function bacaproduct($cari,$num,$offset,$icustomer)
    {
		  $this->db->select(" a.*,b.e_product_motifname from tm_ic_consigment a, tr_product_motif b
													where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
													and a.i_customer='$icustomer'
													and a.i_product_motif=b.i_product_motif
													and a.i_product=b.i_product
													order by a.i_product, a.i_product_grade ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacastore($area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													from tr_store_location a, tr_store b, tr_area c
													where a.i_store = b.i_store and b.i_store=c.i_store
													and (c.i_area = '$area1' or c.i_area = '$area2' or
													 c.i_area = '$area3' or c.i_area = '$area4' or
													 c.i_area = '$area5')",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }*/
    function runningnumber($icustomer,$thbl){
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_sopb,9,2)) as max from tm_sopb 
													where substr(i_sopb,4,2)='$th' and i_customer='$icustomer'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<2){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="SO-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="01";
			  $noso  ="SO-".$thbl."-".$noso;
			  return $noso;
		  }
    }
    function cari($cari,$num,$offset)
    {
		  $this->db->select(" * from tm_sopb where upper(i_sopb) like '%$cari%' or upper(i_customer) like '%$cari%' order by i_stockopname",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    /*function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select(" distinct(c.i_store) as i_store , b.i_store_location, b.e_store_locationname,a.e_store_name 
													from tr_store a, tr_store_location b, tr_area c
													where a.i_store=b.i_store and b.i_store=c.i_store
														and (upper(a.i_store) like '%$cari%' or upper(a.e_store_name) like '%$cari%'
													or upper(b.i_store_location) like '%$cari%' or upper(b.e_store_locationname) like '%$cari%')
													and (c.i_area = '$area1' or c.i_area = '$area2' or
														 c.i_area = '$area3' or c.i_area = '$area4' or
														 c.i_area = '$area5') 
													order by i_store",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariproduct($cari,$num,$offset,$icustomer)
    {
		  $this->db->select(" a.*,b.e_product_motifname from tm_ic_consigment a, tr_product_motif b
							  where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							  and a.i_customer='$icustomer'
							  and a.i_product_motif=b.i_product_motif
							  and a.i_product=b.i_product
							  order by a.i_product, a.i_product_grade",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }*/
    function qic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
   /* function inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$ido,$q_in,$q_out,$qdo,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      if($qdo>$q_ak){
        $qtmp=$qdo-$q_ak;
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', $q_in+$qtmp, $q_out, $q_ak+$qtmp, $q_aw
                                  )
                                ",false);
      }elseif($qdo<$q_ak){
        $qtmp=$q_ak-$qdo;
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', $q_in, $q_out+$qtmp, $q_ak-$qtmp, $q_aw
                                  )
                                ",false);
      }else{
        $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ido', '$now', $q_in, $q_out, $q_ak, $q_aw
                                  )
                                ",false);
      }
    }*/
    function updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment
                                set n_saldo_stockopname=$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
      }
      $query=$this->db->query(" 
                                UPDATE tm_mutasi_consigment
                                set n_saldo_awal=$qdo, n_saldo_akhir=($qdo+n_mutasi_daripusat+n_mutasi_darilang)-
                                (n_mutasi_penjualan+n_mutasi_kepusat)
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer' and e_mutasi_periode='$perdpn'
                              ",false);
    }
    function insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qdo,$emutasiperiode)
    {
      /*$query=$this->db->query(" 
                                insert into tm_mutasi_consigment
                                (
                                  i_product, i_product_motif, i_product_grade, i_customer, e_mutasi_periode, n_saldo_awal,
                                  n_mutasi_daripusat, n_mutasi_darilang, n_mutasi_penjualan, n_mutasi_kepusat, n_saldo_akhir,
                                  n_saldo_stockopname, n_mutasi_git, f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$icustomer',
                                  '$emutasiperiode',0,0,0,0,0,0,$qdo,0,'f')
                              ",false);
      $bldpn=substr($emutasiperiode,4,2)+1;
      if($bldpn==13)
      {
        $perdpn=substr($emutasiperiode,0,4)+1;
        $perdpn=$perdpn.'01';
      }else{
        $perdpn=substr($emutasiperiode,0,4);
        $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
      }
      $query=$this->db->query(" 
                                insert into tm_mutasi_consigment
                                (
                                  i_product, i_product_motif, i_product_grade, i_customer, e_mutasi_periode, n_saldo_awal,
                                  n_mutasi_daripusat, n_mutasi_darilang, n_mutasi_penjualan, n_mutasi_kepusat, n_saldo_akhir,
                                  n_saldo_stockopname, n_mutasi_git, f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$icustomer','$perdpn',
                                  0,0,0,0,0,0,0,0,'f')
                              ",false);*/
      $ada=false;
      $tmp=substr($emutasiperiode,4,2);
      switch($tmp){
      case '01':
        $th=substr($emutasiperiode,0,4)-1;
        $per=$th.'12';
        break;
      case '02':
        $per=substr($emutasiperiode,0,4).'01';
        break;
      case '03':
        $per=substr($emutasiperiode,0,4).'02';
        break;
      case '04':
        $per=substr($emutasiperiode,0,4).'03';
        break;
      case '05':
        $per=substr($emutasiperiode,0,4).'04';
        break;
      case '06':
        $per=substr($emutasiperiode,0,4).'05';
        break;
      case '07':
        $per=substr($emutasiperiode,0,4).'06';
        break;
      case '08':
        $per=substr($emutasiperiode,0,4).'07';
        break;
      case '09':
        $per=substr($emutasiperiode,0,4).'08';
        break;
      case '10':
        $per=substr($emutasiperiode,0,4).'09';
        break;
      case '11':
        $per=substr($emutasiperiode,0,4).'10';
        break;
      case '12':
        $per=substr($emutasiperiode,0,4).'11';
        break;
      }
      $sal=0;
      $query=$this->db->query(" SELECT n_saldo_stockopname
                                from tm_mutasi_consigment
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                                and e_mutasi_periode='$per'
                              ",false);
      if ($query->num_rows() > 0){
        $isi=$query->row();
        $sal=$isi->n_saldo_stockopname;
				$ada=true;
			}
      if($sal==null){
        $sal=0;
      }
      if($ada){
        $query=$this->db->query(" 
                                  insert into tm_mutasi_consigment 
                                  (
                                    i_product,i_product_motif,i_product_grade,i_customer,
                                    e_mutasi_periode,n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,
                                    n_mutasi_kepusat,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                  values
                                  ('$iproduct','$iproductmotif','$iproductgrade','$icustomer',
                                   '$emutasiperiode',$sal,0,0,0,0,0,$qdo,'f')",false);
      }else{
        $query=$this->db->query(" 
                                  insert into tm_mutasi_consigment
                                  (
                                    i_product,i_product_motif,i_product_grade,i_customer,
                                    e_mutasi_periode,n_saldo_awal,n_mutasi_daripusat,n_mutasi_darilang,n_mutasi_penjualan,
                                    n_mutasi_kepusat,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                  values
                                  ('$iproduct','$iproductmotif','$iproductgrade','$icustomer',
                                   '$emutasiperiode',$sal,0,0,0,0,0,$qdo,'f')",false);
      }
    }
    function updateic4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$qdo,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic_consigment set n_quantity_stock=$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_customer='$icustomer'
                              ",false);
    }
    function insertic4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$qdo)
    {
      $query=$this->db->query(" 
                                insert into tm_ic_consigment
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$icustomer', '$eproductname',$qdo, 't'
                                )
                              ",false);
    }
   /* function namaprod($iproduct)
    {
      $nama='';
		  $this->db->select("e_product_name from tr_product
						             where i_product='$iproduct'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $nama=$tes->e_product_name;
        }
		  }
      return $nama;
    }*/
}
?>
