<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		    #$this->CI =& get_instance();
    }

    function baca($user)
    {
		$this->db->select('i_host, e_printer_host, e_printer_uri')->from('tm_user')->where('i_user', $user);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function update($ihost,$eprinterhost,$eprinteruri,$user)
    {
    	$data = array(
               'e_printer_host'	=> $eprinterhost,
               'e_printer_uri' 	=> $eprinteruri
//              ,      'i_host'   			=> $ihost
            );
		$this->db->where('i_user', $user);
		$this->db->update('tm_user', $data); 
		$sess=array('printerhost'=>$eprinterhost, 'printeruri'=>$eprinteruri, 'ihost'=>$ihost);
#		$this->CI->session->set_userdata($sess);
		$this->session->set_userdata($sess);
    }
	
    function bacahost($num,$offset,$cari)
    {
		$sql=" * from tr_host where upper(e_host_name) like '%$cari%' or e_ip_address like '%$cari%' order by e_ip_address";
		$this->db->select($sql,false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
