<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaap($num,$offset)
    {
			$this->db->select("	i_ap, i_area from tm_ap order by i_ap", false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaap2($num,$offset,$area)
    {
			$this->db->select("	i_ap, i_area from tm_ap where i_area='$area' order by i_ap", false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariap($cari,$num,$offset)
    {
			$this->db->select("	i_ap, i_area from tm_ap where i_ap like '%$cari%' order by i_ap",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariap2($cari,$num,$offset)
    {
			$this->db->select("	i_ap, i_area from tm_ap where i_ap like '%$cari%' order by i_ap",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacamaster($apfrom,$apto)
    {
			$this->db->select(" * from tm_ap
								inner join tr_supplier on (tm_ap.i_supplier=tr_supplier.i_supplier)
								inner join tr_area on (tm_ap.i_area=tr_area.i_area)
								where tm_ap.i_ap >= '$apfrom' and tm_ap.i_ap <= '$apto'
								order by tm_ap.i_ap desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetail($ap)
    {
			$this->db->select(" * from tm_ap_item
								inner join tr_product_motif on (tm_ap_item.i_product_motif=tr_product_motif.i_product_motif
															and tm_ap_item.i_product=tr_product_motif.i_product)
								where tm_ap_item.i_ap = '$ap' order by tm_ap_item.i_ap desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
