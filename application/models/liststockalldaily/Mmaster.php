<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($tgl,$iperiode)
    {
 		  $this->db->select("	i_product, e_product_name, sum(satu) as satu, sum(dua) as dua, sum(tiga) as tiga, sum(empat) as empat, 
 		  sum(lima) as lima, sum(tujuh) as tujuh, sum(lapan) as lapan, sum(lalan) as lalan, sum(luluh) as luluh, sum(welas) as welas, 
 		  sum(walas) as walas, sum(juhlas) as juhlas, sum(walu) as walu, sum(luji) as luji, sum(AA) as AA, sum(PB) as PB from (
      select i_product, e_product_name, satu, dua, tiga, empat, lima, tujuh, lapan, lalan, luluh, welas, walas, juhlas, walu, luji, AA, PB 
      from crosstab(
      'SELECT i_product, e_product_name, i_store, sum(qty) as qty from f_all_saldoakhir_daily(''$iperiode'',''$tgl'') 
      group by i_product, e_product_name, i_store',
      'select distinct (i_store) as i_store from f_all_saldoakhir_daily(''$iperiode'',''$tgl'') order by i_store'
      )as
      (i_product text, e_product_name text, satu integer, dua integer, tiga integer, empat integer, lima integer, tujuh integer, 
      lapan integer, lalan integer, luluh integer, welas integer, walas integer, juhlas integer, walu integer, luji integer, AA integer, 
      PB integer)
      ) as a
      group by i_product, e_product_name
      order by i_product, e_product_name",false);#->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
      $query->free_result();
    }

    function bacastore($tgl,$iperiode)
  {
	  $this->db->select(" distinct (i_store) as i_store from f_all_saldoakhir_daily('$iperiode','$tgl') order by i_store", false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
}
?>
