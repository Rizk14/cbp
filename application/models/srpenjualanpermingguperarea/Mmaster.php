<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
      $this->db->select("	e_supplier_name, i_area, e_area_name, week, sum(qspb) as qspb, sum(vspb) as vspb, sum(qnota) as qnota, 
      sum(vnota) as vnota from(
      select a.e_supplier_name, a.i_area, e_area_name, week, 0 as qspb, 0 as vspb, sum(qnota) as qnota, sum(vnota) as vnota from(
      select d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota) AS week , 0 as qspb, 0 as vspb, 
      sum(c.n_deliver) as qnota, 
      sum(c.n_deliver*c.v_unit_price)-(a.v_nota_discount*(sum(c.n_deliver*c.v_unit_price)/ a.v_nota_gross)) as vnota
      from tm_nota a, tr_area b, tm_nota_item c, tr_product e, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and b.f_area_real='t'  
      and a.i_area=b.i_area and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier 
      and a.i_sj=c.i_sj and a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discount
      )as a
      GROUP BY a.e_supplier_name, a.i_area, a.e_area_name, week
      union all
      select b.e_supplier_name, b.i_area, b.e_area_name, b.week, sum(qspb) as qspb, sum(vspb) as vspb, 0 as qnota, 0 as vnota from(
      select  d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', d_spb) AS week, sum(c.n_order) as qspb, 
      sum(c.n_order*c.v_unit_price)-(a.v_spb_discounttotal*(sum(c.n_order*c.v_unit_price)/ a.v_spb)) as vspb, 0 as qnota, 0 as vnota 
      from tm_spb a, tr_area b, tm_spb_item c, tr_product e, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and f_spb_cancel='f' and b.f_area_real='t' and a.i_area=b.i_area 
      and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier and a.i_spb=c.i_spb and
      a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_area, b.e_area_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_area, c.e_area_name, week
      ORDER BY c.e_supplier_name, c.i_area, c.e_area_name, week;
      ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function jmlminggu($iperiode)
    {
      $this->db->select("	count(distinct(week)) as jml from(
      select e_supplier_name, i_area, e_area_name, week, sum(qspb) as qspb, sum(vspb) as vspb, sum(qnota) as qnota, 
      sum(vnota) as vnota from(
      select a.e_supplier_name, a.i_area, e_area_name, week, 0 as qspb, 0 as vspb, sum(qnota) as qnota, sum(vnota) as vnota from(
      select d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota) AS week , 0 as qspb, 0 as vspb, 
      sum(c.n_deliver) as qnota, 
      sum(c.n_deliver*c.v_unit_price)-(a.v_nota_discount*(sum(c.n_deliver*c.v_unit_price)/ a.v_nota_gross)) as vnota
      from tm_nota a, tr_area b, tm_nota_item c, tr_product e, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and b.f_area_real='t'  
      and a.i_area=b.i_area and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier 
      and a.i_sj=c.i_sj and a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discount
      )as a
      GROUP BY a.e_supplier_name, a.i_area, a.e_area_name, week
      union all
      select b.e_supplier_name, b.i_area, b.e_area_name, b.week, sum(qspb) as qspb, sum(vspb) as vspb, 0 as qnota, 0 as vnota from(
      select  d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', d_spb) AS week, sum(c.n_order) as qspb, 
      sum(c.n_order*c.v_unit_price)-(a.v_spb_discounttotal*(sum(c.n_order*c.v_unit_price)/ a.v_spb)) as vspb, 0 as qnota, 0 as vnota 
      from tm_spb a, tr_area b, tm_spb_item c, tr_product e, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and f_spb_cancel='f' and b.f_area_real='t' and a.i_area=b.i_area 
      and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier and a.i_spb=c.i_spb and
      a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_area, b.e_area_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_area, c.e_area_name, week
      )as a",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function minggu($iperiode)
    {
      $this->db->select("	week from(
      select e_supplier_name, i_area, e_area_name, week, sum(qspb) as qspb, sum(vspb) as vspb, sum(qnota) as qnota, 
      sum(vnota) as vnota from(
      select a.e_supplier_name, a.i_area, e_area_name, week, 0 as qspb, 0 as vspb, sum(qnota) as qnota, sum(vnota) as vnota from(
      select d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota) AS week , 0 as qspb, 0 as vspb, 
      sum(c.n_deliver) as qnota, 
      sum(c.n_deliver*c.v_unit_price)-(a.v_nota_discount*(sum(c.n_deliver*c.v_unit_price)/ a.v_nota_gross)) as vnota
      from tm_nota a, tr_area b, tm_nota_item c, tr_product e, tr_supplier d
      where to_char(a.d_nota,'yyyymm')='$iperiode' and f_nota_cancel='f' and not a.i_nota isnull and b.f_area_real='t'  
      and a.i_area=b.i_area and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier 
      and a.i_sj=c.i_sj and a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_nota), a.v_nota_gross, a.v_nota_discount
      )as a
      GROUP BY a.e_supplier_name, a.i_area, a.e_area_name, week
      union all
      select b.e_supplier_name, b.i_area, b.e_area_name, b.week, sum(qspb) as qspb, sum(vspb) as vspb, 0 as qnota, 0 as vnota from(
      select  d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', d_spb) AS week, sum(c.n_order) as qspb, 
      sum(c.n_order*c.v_unit_price)-(a.v_spb_discounttotal*(sum(c.n_order*c.v_unit_price)/ a.v_spb)) as vspb, 0 as qnota, 0 as vnota 
      from tm_spb a, tr_area b, tm_spb_item c, tr_product e, tr_supplier d
      where to_char(a.d_spb,'yyyymm')='$iperiode' and f_spb_cancel='f' and b.f_area_real='t' and a.i_area=b.i_area 
      and e.i_supplier in ('SP002','SP004','SP021') and c.i_product=e.i_product and e.i_supplier=d.i_supplier and a.i_spb=c.i_spb and
      a.i_area=c.i_area
      GROUP BY d.e_supplier_name, a.i_area, b.e_area_name, date_trunc('week', a.d_spb), a.v_spb, a.v_spb_discounttotal
      )as b
      GROUP BY b.e_supplier_name, b.i_area, b.e_area_name, week
      )as c
      GROUP BY c.e_supplier_name, c.i_area, c.e_area_name, week
      )as a
      Group by week Order by week",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' 
				                    or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
				                    order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
