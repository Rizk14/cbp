<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacakas($periode)
	{
		$query=$this->db->query("select 'Kas' as ket, sum(b.v_saldo_awal) as v_saldo_awal
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '111%'
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacapiutang($periode)
	{
		$query=$this->db->query("select 'Piutang Dagang' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='1' and b.i_coa like '112%' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacahutangusaha($periode)
	{
		$query=$this->db->query("select 'Hutang Usaha' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='2' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacamodal($periode)
	{
		$query=$this->db->query("select 'Modal' as ket, sum(b.v_saldo_akhir) as v_saldo_akhir
								 from tr_coa a, tm_coa_saldo b where i_periode = '$periode' and i_coa_group='3' 
								 and a.i_coa=b.i_coa",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function bacacoa($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa order by i_coa limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricoa($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" 	select * from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'
									limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function saldoawal($periode,$icoa)
    {
		$this->db->select("	v_saldo_awal from tm_coa_saldo
							where i_periode = '$periode'
							and i_coa='$icoa' ",false);
		$query = $this->db->get();
		foreach($query->result() as $tmp){
			$sawal= $tmp->v_saldo_awal;
		}
		return $sawal;		
    }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s":
		        $sec += $number;
		        break;
		    case "n":
		        $min += $number;
		        break;
		    case "h":
		        $hr += $number;
		        break;
		    case "d":
		        $day += $number;
		        break;
		    case "ww":
		        $day += ($number * 7);
		        break;
		    case "m": 
		        $mon += $number;
		        break;
		    case "yyyy": 
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		}      
	    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
	    $dateTimeArr=getdate($dateTime);
	    $nosecmin = 0;
	    $min=$dateTimeArr['minutes'];
	    $sec=$dateTimeArr['seconds'];
	    if ($hr==0){$nosecmin += 1;}
	    if ($min==0){$nosecmin += 1;}
	    if ($sec==0){$nosecmin += 1;}
	    if ($nosecmin>2){     
			return(date("Y-m-d",$dateTime));
		} else {     
			return(date("Y-m-d G:i:s",$dateTime));
		}
	}
	function NamaBulan($bln){
		switch($bln){
			case "01" 	:
				$NMbln = "Januari";
				break;
			case "02" 	:
				$NMbln = "Februari";
				break;
			case "03" 	:
				$NMbln = "Maret";
				break;
			case "04" 	:
				$NMbln = "April";
				break;
			case "05" 	:
				$NMbln = "Mei";
				break;
			case "06" 	:
				$NMbln = "Juni";
				break;
			case "07" 	:
				$NMbln = "Juli";
				break;
			case "08" 	:
				$NMbln = "Agustus";
				break;
			case "09" 	:
				$NMbln = "September";
				break;
			case "10" 	:
				$NMbln = "Oktober";
				break;
			case "11" 	:
				$NMbln = "November";
				break;
			case "12"  	:
				$NMbln = "Desember";
				break;
		}
		return ($NMbln);
	}
}
?>
