<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($isupplier)
	{
		$this->db->select(" * FROM tr_supplier LEFT JOIN tr_supplier_group
							ON (tr_supplier.i_supplier_group = tr_supplier_group.i_supplier_group)
							where tr_supplier.i_supplier = '$isupplier'
				", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row();
		}
	}
	function insert(
		$isupplier,
		$isuppliergroup,
		$esuppliername,
		$esupplieraddress,
		$esuppliercity,
		$esupplierpostalcode,
		$esupplierphone,
		$esupplierfax,
		$esupplierownername,
		$esupplierowneraddress,
		$esuppliernpwp,
		$esupplierphone2,
		$esuppliercontact,
		$esupplieremail,
		$nsupplierdiscount,
		$nsupplierdiscount2,
		$nsuppliertoplength,
		$fsupplierpkp,
		$fsupplierppn,
		$paymenttype,
		$bankname,
		$accountnumber,
		$accountname
	) {
		if ($fsupplierpkp == 'on')
			$fsupplierpkp = 'TRUE';
		else
			$fsupplierpkp = 'FALSE';
		if ($fsupplierppn == 'on')
			$fsupplierppn = 'TRUE';
		else
			$fsupplierppn = 'FALSE';
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry = $row->c;
		$this->db->set(
			array(
				'i_supplier'				=> $isupplier,
				'i_supplier_group'			=> $isuppliergroup,
				'e_supplier_name'			=> $esuppliername,
				'e_supplier_address'		=> $esupplieraddress,
				'e_supplier_city'			=> $esuppliercity,
				'e_supplier_postalcode'		=> $esupplierpostalcode,
				'e_supplier_phone'			=> $esupplierphone,
				'e_supplier_fax'			=> $esupplierfax,
				'e_supplier_ownername'		=> $esupplierownername,
				'e_supplier_owneraddress'	=> $esupplierowneraddress,
				'e_supplier_npwp'			=> $esuppliernpwp,
				'e_supplier_phone2'			=> $esupplierphone2,
				'e_supplier_contact'		=> $esuppliercontact,
				'e_supplier_email'			=> $esupplieremail,
				'n_supplier_discount'		=> $nsupplierdiscount,
				'n_supplier_discount2'		=> $nsupplierdiscount2,
				'n_supplier_toplength'		=> $nsuppliertoplength,
				'f_supplier_pkp'			=> $fsupplierpkp,
				'f_supplier_ppn'			=> $fsupplierppn,
				'd_supplier_entry'			=> $dentry,
				'payment_type'				=> $paymenttype,
				'bank_name'					=> $bankname,
				'account_number'			=> $accountnumber,
				'account_name'				=> $accountname
			)
		);

		$this->db->insert('tr_supplier');
		//		redirect('supplier/cform/');
	}
	function update(
		$isupplier,
		$isuppliergroup,
		$esuppliername,
		$esupplieraddress,
		$esuppliercity,
		$esupplierpostalcode,
		$esupplierphone,
		$esupplierfax,
		$esupplierownername,
		$esupplierowneraddress,
		$esuppliernpwp,
		$esupplierphone2,
		$esuppliercontact,
		$esupplieremail,
		$nsupplierdiscount,
		$nsupplierdiscount2,
		$nsuppliertoplength,
		$fsupplierpkp,
		$fsupplierppn,
		$paymenttype,
		$bankname,
		$accountnumber,
		$accountname
	) {
		if ($fsupplierpkp == 'on')
			$fsupplierpkp = 'TRUE';
		else
			$fsupplierpkp = 'FALSE';
		if ($fsupplierppn == 'on')
			$fsupplierppn = 'TRUE';
		else
			$fsupplierppn = 'FALSE';
		$query  = $this->db->query("SELECT current_timestamp as c");
		$row    = $query->row();
		$dupdate = $row->c;
		$data = array(
			'i_supplier_group'			=> $isuppliergroup,
			'e_supplier_name'			=> $esuppliername,
			'e_supplier_address'		=> $esupplieraddress,
			'e_supplier_city'			=> $esuppliercity,
			'e_supplier_postalcode'		=> $esupplierpostalcode,
			'e_supplier_phone'			=> $esupplierphone,
			'e_supplier_fax'			=> $esupplierfax,
			'e_supplier_ownername'		=> $esupplierownername,
			'e_supplier_owneraddress'	=> $esupplierowneraddress,
			'e_supplier_npwp'			=> $esuppliernpwp,
			'e_supplier_phone2'			=> $esupplierphone2,
			'e_supplier_contact'		=> $esuppliercontact,
			'e_supplier_email'			=> $esupplieremail,
			'n_supplier_discount'		=> $nsupplierdiscount,
			'n_supplier_discount2'		=> $nsupplierdiscount2,
			'n_supplier_toplength'		=> $nsuppliertoplength,
			'f_supplier_pkp'			=> $fsupplierpkp,
			'f_supplier_ppn'			=> $fsupplierppn,
			'd_supplier_update'			=> $dupdate,
			'payment_type'				=> $paymenttype,
			'bank_name'					=> $bankname,
			'account_number'			=> $accountnumber,
			'account_name'				=> $accountname
		);
		$this->db->where('i_supplier =', $isupplier);
		$this->db->update('tr_supplier', $data);
		//		redirect('supplier/cform/');
	}

	public function delete($isupplier)
	{
		$this->db->query("DELETE FROM tr_supplier WHERE i_supplier='$isupplier'", false);
		return TRUE;
	}
	function bacasemua($cari, $num, $offset)
	{
		$this->db->select("* from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%' order by i_supplier", false); #->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cari($cari, $num, $offset)
	{
		$this->db->select("* from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%' order by i_supplier", false); #->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacagroup($cari, $num, $offset)
	{
		$this->db->select("i_supplier_group, e_supplier_groupname from tr_supplier_group order by i_supplier_group ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function carigroup($cari, $num, $offset)
	{
		$this->db->select("i_supplier_group, e_supplier_groupname from tr_supplier_group 
				   where upper(e_supplier_groupname) like '%$cari%' or upper(i_supplier_group) like '%$cari%' order by i_supplier_group", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
