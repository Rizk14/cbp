<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaperiode($isupplier,$dfrom,$dto,$cari)
    {
		    $this->db->select("	z.i_supplier, z.e_supplier_name, z.d_dtap, z.i_dtap, z.v_netto-z.v_ppn as dpp, z.v_ppn, z.v_netto, 
                            z.d_due_date, z.tanggalbayar,z.bankname, z.umurhutang, z.bukti, z.v_sisa from (
                                    select a.i_supplier, b.e_supplier_name, a.d_dtap, a.i_dtap, a.v_netto-a.v_ppn as dpp, a.v_ppn, a.v_netto, a.d_due_date, 
                                    d.d_alokasi as tanggalbayar,d.e_bank_name as bankname, d.d_alokasi-a.d_dtap as umurhutang, g.i_pvb as bukti, a.v_sisa
                                    from tm_dtap a, tr_supplier b, tm_kbank c, tm_alokasi_bk d, tm_alokasi_bk_item e, tm_pv_item f, tm_pvb g 
                                    where a.d_dtap between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') 
                                    and a.i_supplier = '$isupplier'  
                                    and d.f_alokasi_cancel = 'false'
                                    and c.f_kbank_cancel = 'false'
                                    and a.i_supplier = b.i_supplier
                                    and a.i_dtap = e.i_nota and a.i_supplier = d.i_supplier
                                    and e.i_alokasi = d.i_alokasi and e.i_kbank = d.i_kbank and e.i_supplier = d.i_supplier and e.i_coa_bank = d.i_coa_bank
                                    and d.i_kbank = c.i_kbank
                                    and f.i_pv = g.i_pv
                                    and e.i_kbank = f.i_kk

                                    union all

                                    select a.i_supplier, b.e_supplier_name, a.d_dtap, a.i_dtap, a.v_netto-a.v_ppn as dpp, a.v_ppn, a.v_netto, a.d_due_date, 
                                    d.d_alokasi as tanggalbayar, 'KAS' as bankname, d.d_alokasi-a.d_dtap as umurhutang,f.i_pv as bukti, a.v_sisa
                                    from tm_dtap a, tr_supplier b, tm_kb c, tm_alokasi_kb d, tm_alokasi_kb_item e, tm_pv_item f 
                                    where a.d_dtap between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
                                    and a.i_supplier = '$isupplier' 
                                    and d.f_alokasi_cancel = 'false'
                                    and c.f_kb_cancel = 'false'
                                    and a.i_supplier = b.i_supplier
                                    and a.i_dtap = e.i_nota and a.i_supplier = e.i_supplier
                                    and e.i_alokasi = d.i_alokasi and e.i_kb = d.i_kb and e.i_supplier = d.i_supplier
                                    and c.i_kb = d.i_kb
                                    and e.i_kb = f.i_kk

                            ) as z
                            order by z.i_supplier, z.d_dtap",false);
    		$query = $this->db->get();
    		if ($query->num_rows() > 0){
    			return $query->result();
    		}
    }
    
    function bacasupplier($num,$offset)
    {
    		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
    		$query = $this->db->get();
    		if ($query->num_rows() > 0){
    			return $query->result();
		    }
    }

    function carisupplier($cari,$num,$offset)
    {
    		$this->db->select("	* FROM tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
    							ORDER BY i_supplier ",false)->limit($num,$offset);
    		$query = $this->db->get();
    		if ($query->num_rows() > 0){
    			return $query->result();
    		}
    }
}
?>
