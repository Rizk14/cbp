<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerbank,$ecustomerbankname)
    {
		$this->db->select('i_customer, e_customer_bankname, e_customer_bankaccount')->from('tr_customer_bank')->where('i_customer', $icustomerbank)->where('e_customer_bankname', $ecustomerbankname);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerbank, $ecustomerbankname, $ecustomerbankaccount)
    {
    	$this->db->set(
    		array(
    			'i_customer' => $icustomerbank,
    			'e_customer_bankname' => $ecustomerbankname,
			'e_customer_bankaccount' => $ecustomerbankaccount
    		)
    	);
    	
    	$this->db->insert('tr_customer_bank');
		#redirect('customerbank/cform/');
    }
    function update($icustomerbank, $ecustomerbankname, $ecustomerbankaccount)
    {
    	$data = array(
               'i_customer' => $icustomerbank,
               'e_customer_bankname' => $ecustomerbankname,
	       'e_customer_bankaccount' => $ecustomerbankaccount
            );
		$this->db->where('i_customer', $icustomerbank);
		$this->db->update('tr_customer_bank', $data); 
		#redirect('customerbank/cform/');
    }
	
    public function delete($icustomerbank,$ecustomerbankname) 
    {
		  $this->db->query("DELETE FROM tr_customer_bank WHERE i_customer='$icustomerbank' and e_customer_bankname='$ecustomerbankname'");
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		  $this->db->select("i_customer, e_customer_bankname, e_customer_bankaccount from tr_customer_bank where upper(i_customer) like '%$cari%' or upper(e_customer_bankname) like '%$cari%' order by i_customer", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_customer, e_customer_bankname, e_customer_bankaccount from tr_customer_bank where upper(i_customer) ilike '%$cari%' or upper(e_customer_bankname) ilike '%$cari%' order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
