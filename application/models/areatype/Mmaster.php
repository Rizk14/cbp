<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iareatype)
    {
		$this->db->select('i_area_type, e_area_typename')->from('tr_area_type')->where('i_area_type', $iareatype);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iareatype, $eareatypename)
    {
    	$this->db->set(
    		array(
    			'i_area_type' => $iareatype,
    			'e_area_typename' => $eareatypename
    		)
    	);
    	
    	$this->db->insert('tr_area_type');
		#redirect('areatype/cform/');
    }
    function update($iareatype, $eareatypename)
    {
    	$data = array(
               'i_area_type' => $iareatype,
               'e_area_typename' => $eareatypename
            );
		$this->db->where('i_area_type', $iareatype);
		$this->db->update('tr_area_type', $data); 
		#redirect('areatype/cform/');
    }
	
    public function delete($iareatype) 
    {
		$this->db->query('DELETE FROM tr_area_type WHERE i_area_type=\''.$iareatype.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_area_type, e_area_typename from tr_area_type order by i_area_type",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
