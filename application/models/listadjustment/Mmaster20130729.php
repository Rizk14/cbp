<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ittb,$iarea,$tahun) 
    {
		$this->db->query("update tm_ttbretur set f_ttb_cancel='t' WHERE i_ttb='$ittb' and i_area='$iarea' and n_ttb_year=$tahun");
		$this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
		$this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_refference_document='$ittb' and to_char(d_refference_document,'yyyy')='$tahun'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.*, b.e_customer_name, c.e_area_name, d.e_alasan_returname from tr_customer b, tr_area c, tm_ttbretur a
                  left join tr_alasan_retur d on(a.i_alasan_retur=d.i_alasan_retur)
							    where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							    or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							    and a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_ttb, a.i_area, a.i_ttb desc ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, b.e_customer_name, c.e_area_name, d.e_alasan_returname from tr_customer b, tr_area c, tm_ttbretur a
                  left join tr_alasan_retur d on(a.i_alasan_retur=d.i_alasan_retur)
							    where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							    or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							    order by a.i_ttb desc ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							    where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							    or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							    and a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_ttb, a.i_area, a.i_ttb desc ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							    where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							    or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_ttb <= to_date('$dto','dd-mm-yyyy')
							    order by a.i_ttb desc ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
