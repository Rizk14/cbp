<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iadj,$iarea) 
    {
  		$this->db->query("update tm_adj set f_adj_cancel='t' WHERE i_adj='$iadj' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.*, c.e_area_name from tr_area c, tm_adj a
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, c.e_area_name from tr_area c, tm_adj a
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.*, c.e_area_name from tm_adj a, tr_area c
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, c.e_area_name from tm_adj a, tr_area c
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
