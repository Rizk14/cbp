<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($tahun)
    {
	    $this->db->select(" a.i_periode, a.group, sum(a.vnota)  as vnota, sum(qnota) as qnota from (	
	                        SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, sum(a.v_nota_gross)  as vnota, 0 as qnota
                          from tm_nota a, tm_spb b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, sum(a.v_nota_gross)  as vnota, 
                          0 as qnota from tm_nota a, tm_spb b, tr_product_group c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          Union all
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tm_spb b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0  as vnota, 
                          sum(d.n_deliver) as qnota from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
                          and a.i_sj=d.i_sj and a.i_area=d.i_area
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          ) as a
                          group by a.i_periode, a.group
                          order by a.i_periode, a.group",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacagroup($tahun)
    {
	    $this->db->select("	a.group from( 
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, sum(a.v_nota_gross)  as vnota, 0 as qnota
                          from tm_nota a, tm_spb b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, sum(a.v_nota_gross)  as vnota, 
                          0 as qnota from tm_nota a, tm_spb b, tr_product_group c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          Union all
                          SELECT to_char(a.d_nota,'yyyy') as i_periode, 'modern Outlet' as group, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tm_spb b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='t'  and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy')
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, c.e_product_groupname as group, 0  as vnota, 
                          sum(d.n_deliver) as qnota from tm_nota a, tm_spb b, tr_product_group c, tm_nota_item d
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area 
                          and a.i_nota=b.i_nota and b.f_spb_cancel='f' and b.f_spb_consigment='f' and b.i_product_group=c.i_product_group  
                          and a.i_sj=d.i_sj and a.i_area=d.i_area
                          group by to_char(a.d_nota,'yyyy'), c.e_product_groupname
                          ) as a
                          group by a.i_periode, a.group
                          order by a.i_periode, a.group",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
