<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ido,$isupplier,$op) 
    {
		  $ido=trim($ido);
		  $this->db->select(" n_deliver, i_product, i_product_grade, i_product_motif from tm_do_item
							  WHERE i_do='$ido' and i_supplier='$isupplier'");
		  $query = $this->db->get();
		  foreach($query->result() as $row){
			  $jml=$row->n_deliver;
			  $product=$row->i_product;
			  $grade=$row->i_product_grade;
			  $motif=$row->i_product_motif;
			  $this->db->query("update tm_op_item set n_delivery=n_delivery-$jml WHERE i_op='$op'
						    	        and i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'");
		  }
      $bb='';
		  $query=$this->db->query(" select i_bbm from tm_bbm WHERE i_refference_document='$ido' and i_supplier='$isupplier'");
		  foreach($query->result() as $raw){
			  $bb=$raw->i_bbm;
		  }
		  $this->db->query("update tm_bbm set f_bbm_cancel='t' WHERE i_refference_document='$ido' and i_supplier='$isupplier'");
		  $this->db->query("update tm_op set f_op_close='f' WHERE i_op='$op'");
		  $this->db->query("UPDATE tm_do set f_do_cancel='t' WHERE i_do='$ido' and i_supplier='$isupplier'");
######
      $this->db->select(" * from tm_do where i_do='$ido' and i_supplier='$isupplier'");
			$qry = $this->db->get();
			if ($qry->num_rows() > 0){
        foreach($qry->result() as $qyr){  
          $ddo=$qyr->d_do;
        }
      }
      $th=substr($ddo,0,4);
		  $bl=substr($ddo,5,2);
		  $emutasiperiode=$th.$bl;
      $istore='AA';
			$istorelocation='01';
			$istorelocationbin='00';
      $this->db->select(" * from tm_do_item where i_do='$ido' and i_supplier='$isupplier' order by n_item_no");
			$qery = $this->db->get();
			if ($qery->num_rows() > 0){
        foreach($qery->result() as $qyre){  
          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                                        and i_product_motif='$qyre->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin' and i_refference_document='$ido'
                                        order by d_transaction desc, i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $row   		= $queri->row();
            $que 	= $this->db->query("SELECT current_timestamp as c");
	          $ro 	= $que->row();
	          $now	 = $ro->c;
            $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$qyre->i_product','$qyre->i_product_grade','$qyre->i_product_motif',
                                '$istore','$istorelocation','$istorelocationbin', 
                                '$qyre->e_product_name', '$ido', '$now', 0, $qyre->n_deliver, $row->n_quantity_akhir-$qyre->n_deliver, 
                                $row->n_quantity_akhir
                              )
                             ",false);
          }
          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_pembelian=n_mutasi_pembelian-$qyre->n_deliver, 
                            n_saldo_akhir=n_saldo_akhir-$qyre->n_deliver
                            where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                            and i_product_motif='$qyre->i_product_motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);
          $this->db->query(" 
                            UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qyre->n_deliver
                            where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                            and i_product_motif='$qyre->i_product_motif' and i_store='$istore' and i_store_location='$istorelocation' 
                            and i_store_locationbin='$istorelocationbin'
                           ",false);
        }
      }
######
		  return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" * from v_list_dofc where 
							upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
							order by i_do desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from v_list_dofc a
							where 
							upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
							order by i_do desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
	    if ($cari == '') {
			$sql = " a.i_do, a.d_do, a.i_op, a.f_do_cancel, a.i_area, e.e_supplier_name, a.i_supplier
            from tm_dofc a, tr_supplier e
            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
            and a.i_supplier=e.i_supplier and a.i_area='$iarea' 
            order by a.d_do";
			//echo $sql; die();
		    $this->db->select($sql,false)->limit($num,$offset);
	    }else{
		    $this->db->select("	DISTINCT b.i_area, b.i_do, b.f_do_cancel, b.d_do, b.i_op, b.i_supplier, a.e_supplier_name
                                      FROM tm_dofc b
                                    JOIN tr_supplier a ON a.i_supplier = b.i_supplier
                                WHERE b.f_do_cancel = 'false' 
                                and 
                                    upper(b.i_supplier) like '%$cari%' or upper(a.e_supplier_name) like '%$cari%'
                                    or upper(b.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
                                and b.i_area='$iarea'
                                ORDER BY b.i_do, b.d_do, b.i_op, b.i_supplier, a.e_supplier_name",false)->limit($num,$offset);
	    }		
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
}
?>
