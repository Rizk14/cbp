<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacasemua($dfrom,$dto,$iarea,$cari, $num,$offset)
    {
	    $this->db->select(" a.*, b.e_customer_name from tm_ttbtolak a, tr_customer b
                         where a.i_customer=b.i_customer and a.i_nota not in(select i_nota from tm_nota where f_nota_koreksi='f')
                         and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                         or upper(a.i_ttb) like '%$cari%' or upper(a.i_nota) like '%$cari%') and
                   			 a.i_area='$iarea' and
			                   (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and
			                   a.d_nota <= to_date('$dto','dd-mm-yyyy'))
                    		 order by a.i_ttb desc ",false)->limit($num,$offset);

	    $query = $this->db->get();

	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
    
	function cari($dfrom,$dto,$iarea,$cari,$num,$offset)
    {
	    $this->db->select(" a.*, b.e_customer_name from tm_ttbtolak a, tr_customer b
                          where a.i_customer=b.i_customer and 
			  a.i_nota not in(select i_nota from tm_notakoreksi) and 
			  (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%' or upper(a.i_nota) like '%$cari%') and
			a.i_area='$iarea' and
			(a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and
			a.d_nota <= to_date('$dto','dd-mm-yyyy')) order by a.i_ttb desc ",FALSE)->limit($num,$offset);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }
	function bacanota($inota, $iarea)
    {
		$this->db->select(" * from tm_nota 
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
				   left join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
				   where i_nota = '$inota' and tm_nota.i_area='$iarea' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetailnota($inota,$iarea)
    {
		$this->db->select(" * from 
							(
							select distinct a.*, b.e_product_motifname, a.n_deliver as del from tm_nota_item a, tr_product_motif b
							where a.i_nota = '$inota' and a.i_area='$iarea'
							  and b.i_product_motif=a.i_product_motif
							  and b.i_product=a.i_product
							  and a.i_product not in (select i_product from tm_ttbtolak_item 
										where i_ttb in (select i_ttb from tm_ttbtolak where i_nota='$inota' and i_area='$iarea'))
							union all
							select distinct a.*, b.e_product_motifname, a.n_deliver-c.n_quantity as del
							from tm_nota_item a, tr_product_motif b, tm_ttbtolak_item c
							where a.i_nota = '$inota' and a.i_area='$iarea'
							  and b.i_product_motif=a.i_product_motif
							  and b.i_product=a.i_product
							  and c.i_product=a.i_product
							  and c.i_product_grade=a.i_product_grade
							  and c.i_product_motif=a.i_product_motif
							  and a.i_product in (select i_product from tm_ttbtolak_item 
										where i_ttb in (select i_ttb from tm_ttbtolak where i_nota='$inota' and i_area='$iarea'))
							) as sel
							order by i_product", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacanotakoreksi($inota,$ispb,$iarea)
    {
		$this->db->select(" * from tm_nota
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
				   left join tr_customer_pkp on (tm_nota.i_customer=tr_customer_pkp.i_customer)
				   where i_nota = '$inota' and tm_nota.i_area='$iarea' and tm_nota.f_nota_koreksi='t'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	function bacadetailnotakoreksi($inota,$iarea)
    {
		$this->db->select(" a.*, b.e_product_motifname, a.n_deliver as del from tm_nota_item a, tr_product_motif b
							where a.i_nota = '$inota' and a.i_area = '$iarea'
							  and b.i_product_motif=a.i_product_motif
							  and b.i_product=a.i_product
							order by a.i_product", false);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumber($area,$thbl){
#  	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
#		$row   	= $query->row();
#		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_nota,11,6)) as max from tm_nota_koreksi  
        				  			where substr(i_nota,4,2)='$th' and i_area='$area'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nonota  =$terakhir+1;
			settype($nonota,"string");
			$a=strlen($nonota);
			while($a<6){
			  $nonota="0".$nonota;
			  $a=strlen($nonota);
			}
			$nonota  ="FP-".$thbl."-".$area.$nonota;
			return $nonota;
		}else{
			$nonota  ="000001";
			$nonota  ="FP-".$thbl."-".$area.$nonota;
			return $nonota;
		}
    }
	function insertheader($inota)
	{
		$this->db->query("insert into tm_notakoreksi select * from tm_nota where i_nota='$inota'");
		$this->db->query("update tm_nota set f_nota_koreksi='t' where i_nota='$inota'");
		$this->db->query("update tm_notakoreksi set f_ttb_tolak='f' where i_nota='$inota'");
	}
	function updatenotakoreksi(	$inota,$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,$icustomer,
								$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,$vnotadiscounttotal,
								$vnotagross,$vnotanetto,$fnotaplusppn,$fnotaplusdiscount,$nnotatoplength)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dupdate= $row->c;
    	$this->db->set(
    		array(
			'i_customer'			    => $icustomer,
			'n_nota_discount1'		=> $nnotadiscount1,
			'n_nota_discount2'		=> $nnotadiscount2,
			'n_nota_discount3'		=> $nnotadiscount3,
			'v_nota_discount1'		=> $vnotadiscount1,
			'v_nota_discount2'		=> $vnotadiscount2,
			'v_nota_discount3'		=> $vnotadiscount3,
			'v_nota_discounttotal'=> $vnotadiscounttotal,
			'v_nota_netto'			  => $vnotanetto,
			'v_nota_gross'  			=> $vnotagross,
			'v_sisa'				      => $vnotanetto,
			'f_plus_ppn'    			=> $fnotaplusppn,
			'f_plus_discount' 		=> $fnotaplusdiscount,
			'n_nota_toplength'		=> $nnotatoplength,
			'd_nota_update'		  	=> $dupdate
    		)
    	);
		  $this->db->where('i_nota',$inota);
    	$this->db->update('tm_notakoreksi');
    }
	function insertdetail($inota)
    {
    	$this->db->query("insert into tm_notakoreksi_item select * from tm_nota_item where i_nota='$inota'");
    }
	function updatedetailnotakoreksi($inota,$iproduct,$iproductgrade,$eproductname,$ndeliver,$vunitprice,$iproductmotif)
    {
    	$this->db->set(
    		array(
					'i_nota'		    	=> $inota,
					'i_product'		  	=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_deliver'		  	=> $ndeliver,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname
    		)
    	);
		$this->db->where('i_nota',$inota);
		$this->db->where('i_product',$iproduct);
		$this->db->where('i_product_grade',$iproductgrade);
		$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_notakoreksi_item');
    }
	function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_pkp b on
							(a.i_customer=b.i_customer) 
							left join tr_price_group c on
							(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							left join tr_customer_salesman e on
							(a.i_customer=e.i_customer)
							left join tr_customer_discount f on
							(a.i_customer=f.i_customer) where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}

    }
	function updatespb(	$vspbdiscounttotalafter,$vspbafter,$ispb,$iarea)
  {  
	  $query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
		$this->db->set(
    		array(
					'v_spb_discounttotalafter'=> $vspbdiscounttotalafter,
					'v_spb_after'			      	=> $vspbafter
			)
		);
		$this->db->where('i_spb',$ispb);
		$this->db->where('i_area',$iarea);
    $this->db->update('tm_spb');		
	}
	function updatespbdetail($ispb,$iarea,$iproduct,$iproductgrade,$ndeliver,$iproductmotif)
  {
  	$this->db->set(
  		array(
				'n_deliver'			=> $ndeliver
  		)
  	);
	  $this->db->where('i_spb',$ispb);
	  $this->db->where('i_area',$iarea);
	  $this->db->where('i_product',$iproduct);
	  $this->db->where('i_product_grade',$iproductgrade);
	  $this->db->where('i_product_motif',$iproductmotif);
  	$this->db->update('tm_spb_item');
  }
}
?>
