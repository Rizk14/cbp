<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}

	function baca($dfrom, $dto)
	{
		return 	$this->db->query(" 	SELECT * FROM (
										SELECT
											x.e_coa_name,
											x.d_kb,
											x.d_bukti,
											CASE WHEN NOT i_rv ISNULL THEN c.i_rv 
											ELSE d.i_pv 
											END AS i_reff,
											x.i_kb,
											x.e_description,
											x.i_area || ' - ' || b.e_area_name AS i_area,
											x.i_coa,
											x.d_entry,
											sum(x.debet) AS debet,
											sum(x.kredit) AS kredit
										FROM
											(
												SELECT
													d_kb,
													d_bukti,
													i_kb,
													e_description,
													i_area,
													i_coa,
													e_coa_name,
													0 AS debet,
													v_kb AS kredit,
													d_entry
												FROM
													tm_kb
												WHERE
													d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
													AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
													AND f_debet = 't'
													AND f_kb_cancel = 'f'
											UNION ALL
												SELECT
													d_kb,
													d_bukti,
													i_kb,
													e_description,
													i_area,
													i_coa,
													e_coa_name,
													v_kb AS debet,
													0 AS kredit,
													d_entry
												FROM
													tm_kb
												WHERE
													d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
													AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
													AND f_debet = 'f'
													AND f_kb_cancel = 'f'
											) AS x
										INNER JOIN tr_area b ON (x.i_area = b.i_area)
										LEFT JOIN tm_rv_item c ON (x.i_kb = c.i_kk AND x.i_area = c.i_area_kb)
										LEFT JOIN tm_pv_item d ON (x.i_kb = d.i_kk AND x.i_area = d.i_area_kb)
										GROUP BY
											x.e_coa_name,
											x.d_kb,
											x.i_kb,
											x.e_description,
											x.i_area,
											x.i_coa,
											b.e_area_name,
											x.d_bukti,
											c.i_rv,
											d.i_pv,
											x.d_entry
									) AS a
									ORDER BY
										a.d_entry, a.i_reff, a.i_kb
										/* a.d_kb, a.d_entry, a.i_reff */ ");
	}

	function dateAdd($interval, $number, $dateTime)
	{
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr = getdate($dateTime);
		$yr = $dateTimeArr['year'];
		$mon = $dateTimeArr['mon'];
		$day = $dateTimeArr['mday'];
		$hr = $dateTimeArr['hours'];
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		switch ($interval) {
			case "s": //seconds
				$sec += $number;
				break;
			case "n": //minutes
				$min += $number;
				break;
			case "h": //hours
				$hr += $number;
				break;
			case "d": //days
				$day += $number;
				break;
			case "ww": //Week
				$day += ($number * 7);
				break;
			case "m": //similar result "m" dateDiff Microsoft
				$mon += $number;
				break;
			case "yyyy": //similar result "yyyy" dateDiff Microsoft
				$yr += $number;
				break;
			default:
				$day += $number;
		}
		$dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
		$dateTimeArr = getdate($dateTime);
		$nosecmin = 0;
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		if ($hr == 0) {
			$nosecmin += 1;
		}
		if ($min == 0) {
			$nosecmin += 1;
		}
		if ($sec == 0) {
			$nosecmin += 1;
		}
		if ($nosecmin > 2) {
			return (date("Y-m-d", $dateTime));
		} else {
			return (date("Y-m-d G:i:s", $dateTime));
		}
	}
	function bacasaldo($area, $periode, $tanggal)
	{
		$this->db->select(" v_saldo_awal from tm_coa_saldo
							  where i_periode='$periode' and substr(i_coa,7,2)='$area' and substr(i_coa,1,6)='" . KasKecil . "'", false);
		$query = $this->db->get();
		$saldo = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$saldo = $row->v_saldo_awal;
			}
		}
		$this->db->select(" sum(v_kb) as v_kb from tm_kb
							  where i_periode='$periode' and i_area='$area'
							  and d_kb<'$tanggal' and f_debet='t' and f_kb_cancel='f'", false);
		$query = $this->db->get();
		$kredit = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$kredit = $row->v_kb;
			}
		}
		$this->db->select(" sum(v_kb) as v_kb from tm_kb
							  where i_periode='$periode' and i_area='$area'
							  and d_kb<'$tanggal' and f_debet='f' and f_kb_cancel='f'", false);
		$query = $this->db->get();
		$debet = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$debet = $row->v_kb;
			}
		}
		$coaku = KasKecil . $area;
		$kasbesar = KasBesar;
		$bank = Bank;
		$this->db->select(" sum(v_bank) as v_bank from tm_kbank a where a.i_periode='$periode' and a.i_area='$area' and a.d_bank<'$tanggal' 
		                      and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa='$coaku'
		                      and a.v_bank not in (select b.v_kk as v_bank from tm_kk b where b.d_kk =a.d_bank and b.i_area='$area' 
		                      and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' and b.i_periode='$periode')", false);
		/*
		  $this->db->select(" sum(v_bank) as v_bank from tm_kbank where i_periode='$periode' and i_area='$area' and d_bank<'$tanggal' 
		                      and f_debet='t' and f_kbank_cancel='f' and i_coa='$coaku'",false);							
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$debet = $debet + $row->v_bank;
			}
		}
		$this->db->select(" sum(v_kb) as v_kb from tm_kb a where a.i_periode='$periode' and a.i_area='$area' and a.d_kb<'$tanggal' 
		                      and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
		                      and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where b.d_kk =a.d_kb and b.i_area='$area' 
	                        and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar' 
	                        and b.i_periode='$periode')", false);
		/*
		  $this->db->select(" sum(v_kb) as v_kb from tm_kb where i_periode='$periode' and i_area='$area' and d_kb<'$tanggal' 
		                      and f_debet='t' and f_kb_cancel='f' and i_coa='$coaku'",false);							
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$debet = $debet + $row->v_kb;
			}
		}
		$saldo = $saldo + $debet - $kredit;

		return $saldo;
	}
	function bacaarea($num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') {
			$this->db->select("* from tr_area order by i_area", false)->limit($num, $offset);
		} else {
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariarea($cari, $num, $offset, $area1, $area2, $area3, $area4, $area5)
	{
		if ($area1 == '00') {
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num, $offset);
		} else {
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num, $offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function eareaname($area)
	{
		return $this->db->query(" SELECT e_area_name FROM tr_area WHERE i_area='$area' ");
	}
}
