<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iopstatus)
    {
		$this->db->select('i_op_status, e_op_statusname')->from('tr_op_status')->where('i_op_status', $iopstatus);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iopstatus, $eopstatusname)
    {
    	$this->db->set(
    		array(
    			'i_op_status' => $iopstatus,
    			'e_op_statusname' => $eopstatusname
    		)
    	);
    	
    	$this->db->insert('tr_op_status');
		redirect('opstatus/cform/');
    }
    function update($iopstatus, $eopstatusname)
    {
    	$data = array(
               'i_op_status' => $iopstatus,
               'e_op_statusname' => $eopstatusname
            );
		$this->db->where('i_op_status', $iopstatus);
		$this->db->update('tr_op_status', $data); 
		redirect('opstatus/cform/');
    }
	
    public function delete($iopstatus) 
    {
		$this->db->query('DELETE FROM tr_op_status WHERE i_op_status=\''.$iopstatus.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select('i_op_status, e_op_statusname from tr_op_status order by i_op_status',false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
