<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iperiode,$ikendaraan) 
    {
		$this->db->query("DELETE FROM tr_kendaraan WHERE i_kendaraan='$ikendaraan'");
		$this->db->query("DELETE FROM tr_kendaraan_pengguna WHERE i_periode='$iperiode' and i_kendaraan='$ikendaraan'");
		$this->db->query("DELETE FROM tr_kendaraan_item WHERE i_kendaraan='$ikendaraan'");
    }
    function bacasemua($iperiode, $cari, $num, $offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("tr_kendaraan.i_kendaraan,tr_kendaraan.i_kendaraan_jenis,tr_kendaraan.i_kendaraan_bbm,tr_kendaraan.i_area,tr_kendaraan.d_pajak,tr_kendaraan_pengguna.e_pengguna,tr_kendaraan_pengguna.i_periode,tr_kendaraan_item.i_kendaraan_asuransi,e_merek_kendaraan,
							 e_jenis_kendaraan,e_tlo,e_nomor_polisasuransi,tr_kendaraan_asuransi.e_kendaraan_asuransi,tr_area.e_area_name
							  from tr_kendaraan
							  	left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							  	left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							  	left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							  	left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
							  	left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 							  	left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
							  	where tr_kendaraan_pengguna.i_periode='$iperiode'
							  	order by tr_kendaraan_pengguna.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
    }else{
		  $this->db->select("tr_kendaraan.i_kendaraan,tr_kendaraan.i_kendaraan_jenis,tr_kendaraan.i_kendaraan_bbm,tr_kendaraan.i_area,tr_kendaraan.d_pajak,tr_kendaraan_pengguna.e_pengguna,tr_kendaraan_pengguna.i_periode,tr_kendaraan_item.i_kendaraan_asuransi,e_merek_kendaraan,
							 e_jenis_kendaraan,e_tlo,e_nomor_polisasuransi,tr_kendaraan_asuransi.e_kendaraan_asuransi,tr_area.e_area_name
		  					  left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							  left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							  left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							  left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
							  left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 							  left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
							  where tr_kendaraan_pengguna.i_periode='$iperiode' and (tr_kendaraan.i_area = '$area1' or tr_kendaraan.i_area = '$area2' or tr_kendaraan.i_area = '$area3'
						      or tr_kendaraan.i_area = '$area4' or tr_kendaraan.i_area = '$area5')
							  order by tr_kendaraan_pengguna.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iperiode, $cari, $num, $offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select(" * from tr_kendaraan 
							            left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							            left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							            left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							            left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
							            left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 										left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
							            where upper(tr_kendaraan.i_kendaraan) like '%$cari%' 
							            and tr_kendaraan_pengguna.i_periode='$iperiode'
							            order by tr_kendaraan_pengguna.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
    }else{
		  $this->db->select(" * from tr_kendaraan 
							            left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							            left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							            left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							            left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
							            left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 										left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
							            where upper(tr_kendaraan.i_kendaraan) like '%$cari%' 
							            and tr_kendaraan_pengguna.i_periode='$iperiode' and (tr_kendaraan.i_area = '$area1' or tr_kendaraan.i_area = '$area2' or tr_kendaraan.i_area = '$area3'
       									  or tr_kendaraan.i_area = '$area4' or tr_kendaraan.i_area = '$area5')
							            order by tr_kendaraan_pengguna.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
