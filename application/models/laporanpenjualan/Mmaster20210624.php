<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom, $dto)
    {
      $dfrom=substr($dfrom,6,4).'-'.substr($dfrom,3,2).'-'.substr($dfrom,0,2);
      $dto  =substr($dto,6,4).'-'.substr($dto,3,2).'-'.substr($dto,0,2);
      $iperiode=substr($dto,6,4).substr($dto,3,2);
      // $this->db->select(" a.i_area, sum(a.v_netto) as v_nota_netto, sum(a.v_gross) as v_nota_gross, 
      //                     sum(a.v_discount) as v_nota_discounttotal, sum(a.v_spb) as v_spb, sum(a.v_spbdiscount) as v_spbdiscount, 
      //                     sum(a.v_kn) as v_kn
      //                     from(
      //                     select b.i_area, 0 AS v_netto, 0 AS v_gross, 0 as v_discount, b.v_spb, b.v_spb_discounttotal AS v_spbdiscount, 
      //                     0 AS v_kn
      //                       FROM tm_spb b
      //                       WHERE b.f_spb_cancel = false and b.d_spb>='$dfrom' and b.d_spb<='$dto'
      //                     UNION ALL 
      //                     SELECT a.i_area, a.v_nota_netto AS v_netto, a.v_nota_gross AS v_gross, a.v_nota_discounttotal as v_discount, 
      //                     0 AS v_spb, 0 AS v_spbdiscount, 0 AS v_kn
      //                       FROM tm_nota a, tm_spb b
      //                       WHERE NOT a.i_nota IS NULL AND a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area
      //                       and a.d_nota>='$dfrom' and a.d_nota<='$dto'
      //                     UNION ALL 
      //                     SELECT d.i_area, 0 AS v_netto, 0 AS v_gross, 0 as v_discount, 0 AS v_spb, 0 AS v_spbdiscount, d.v_netto AS v_kn
      //                       FROM tm_kn d
      //                       WHERE d.f_kn_cancel = false AND d.i_kn_type = '01' and d.d_kn>='$dfrom' and d.d_kn<='$dto'
      //                     ) as a
      //                     group by i_area order by i_area",false);

       $this->db->select(" a.i_area, a.e_area_name, sum(a.v_netto) as v_nota_netto, sum(a.v_gross) as v_nota_gross, 
                          sum(a.v_discount) as v_nota_discounttotal, sum(a.v_spb) as v_spb, sum(a.v_spbdiscount) as v_spbdiscount, 
                          sum(a.v_kn) as v_kn
                          from(
                          select b.i_area, c.e_area_name, 0 AS v_netto, 0 AS v_gross, 0 as v_discount, b.v_spb, b.v_spb_discounttotal AS v_spbdiscount, 
                          0 AS v_kn
                            FROM tm_spb b , tr_area c
                            WHERE b.f_spb_cancel = false and b.d_spb>='$dfrom' and b.d_spb<='$dto' and b.i_area=c.i_area
                          UNION ALL 
                          SELECT a.i_area, c.e_area_name, a.v_nota_netto AS v_netto, a.v_nota_gross AS v_gross, a.v_nota_discounttotal as v_discount, 
                          0 AS v_spb, 0 AS v_spbdiscount, 0 AS v_kn
                            FROM tm_nota a, tm_spb b, tr_area c
                            WHERE NOT a.i_nota IS NULL AND a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area
                            and a.d_nota>='$dfrom' and a.d_nota<='$dto' and a.i_area=c.i_area
                          UNION ALL 
                          SELECT d.i_area, c.e_area_name, 0 AS v_netto, 0 AS v_gross, 0 as v_discount, 0 AS v_spb, 0 AS v_spbdiscount, d.v_netto AS v_kn
                            FROM tm_kn d, tr_area c
                            WHERE d.f_kn_cancel = false AND d.i_kn_type = '01' and d.d_kn>='$dfrom' and d.d_kn<='$dto' and d.i_area=c.i_area
                          ) as a
                          group by i_area, e_area_name order by i_area",false);
/*
		  $this->db->select("	c.e_area_name, sum(v_target) as v_target, b.i_area, sum(b.v_nota_netto) as v_nota_netto, 
                          sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from(
                          select 0 as v_target, i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where d_docspb>='$dfrom' and d_docspb<='$dto'
                          
                          group by i_area
                          union all
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where d_doc>='$dfrom' and d_doc<='$dto'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_docspb>='$dfrom' and d_docspb<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_doc>='$dfrom' and d_doc<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_kn>='$dfrom' and d_kn<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and (d_doc>='$dfrom' and d_doc<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and (d_kn>='$dfrom' and d_kn<='$dto')
                          group by i_area
                          ) as a
                          group by i_area
                          ) as b, tr_area c
                          where b.i_area=c.i_area
                          group by b.i_area, c.e_area_name
                          order by b.i_area",false);
*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
