<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
		$this->db->query('DELETE FROM tm_op WHERE i_op=\''.$iop.'\'');
		$this->db->query('DELETE FROM tm_op_item WHERE i_op=\''.$iop.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" distinct(a.i_op),a.*, b.e_supplier_name from tm_op a, tr_supplier b, tm_spb c, tm_spmb_item d
                        where a.i_supplier=b.i_supplier
                        and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
                        or upper(a.i_op) like '%$cari%')
                        and d.i_op=a.i_op and d.i_spmb=c.i_spmb and not c.i_spmb isnull
					              order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iop)
    {
		$this->db->select(" * from tm_op
							inner join tr_supplier on (tm_op.i_supplier=tr_supplier.i_supplier)
							inner join tr_op_status on (tm_op.i_op_status=tr_op_status.i_op_status)
							inner join tr_area on (tm_op.i_area=tr_area.i_area)
							where tm_op.i_op = '$iop'
							order by tm_op.i_op desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($iop)
    {
      $reff='';
      $this->db->select(" i_reff from tm_op where tm_op.i_op = '$iop'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $reff=$tes->i_reff;
        }
		  }
      if(substr($reff,0,3)=='SPB'){
    		$this->db->select("a.*, b.e_remark from tm_op_item a, tm_spb_item b, tm_op c where a.i_op='$iop'
                           and a.i_op=c.i_op
                           and a.i_op=b.i_op and c.i_reff=b.i_spb and c.i_area=b.i_area 
                           and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product_grade=b.i_product_grade order by a.n_item_no",false);
      }else{
    		$this->db->select("a.*, b.e_remark from tm_op_item a, tm_spmb_item b, tm_op c where a.i_op='$iop'
                           and a.i_op=c.i_op
                           and a.i_op=b.i_op and c.i_reff=b.i_spmb and c.i_area=b.i_area 
                           and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                           and a.i_product_grade=b.i_product_grade order by a.n_item_no",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacadetailspb($iop)
    {
  		$this->db->select("a.*,e.i_op, e.d_op from tm_spb_item a, tm_spb b, tm_spmb c, tm_spmb_item d, tm_op e
                         where e.i_op='$iop'
                         and a.i_spb=b.i_spb and a.i_area=b.i_area and b.i_spmb=c.i_spmb
                         and c.i_spmb=d.i_spmb
                         and d.i_op=e.i_op 
                         and a.i_product=d.i_product and a.i_product_motif=d.i_product_motif
                         and a.i_product_grade=d.i_product_grade order by a.i_product, a.i_product_motif, a.i_product_grade, a.i_spb",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select(" distinct(a.i_op),a.*, b.e_supplier_name from tm_op a, tr_supplier b, tm_spb c, tm_spmb_item d
                        where a.i_supplier=b.i_supplier
                        and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
                        or upper(a.i_op) like '%$cari%')
                        and d.i_op=a.i_op and d.i_spmb=c.i_spmb and not c.i_spmb isnull
					              order by a.i_op desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
