<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idt,$iarea,$tgl) 
    {
		$this->db->query("update tm_dt set f_dt_cancel='t' WHERE i_dt='$idt' and i_area='$iarea'",False);
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                        a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
						            ORDER BY a.i_notapb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                        a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
						            ORDER BY a.i_notapb",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomer($num,$offset,$cari) {
    $area1	= $this->session->userdata('i_area');
    /*
		if($area1=='PB' || $area1='00'){
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
    }elseif($area1=='03'){
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer and a.i_area='03' order by a.i_customer", false)->limit($num,$offset);
    }elseif($area1=='04'){
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer and a.i_area='04' order by a.i_customer", false)->limit($num,$offset);
    }elseif($area1=='12'){
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer and a.i_area='12' order by b.i_customer", false)->limit($num,$offset);
    }
    */
	if($area1=='PB' || $area1=='00')
	{
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer
                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%') order by a.i_customer", FALSE)->limit($num,$offset);
    }
    else 
    {
		$this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer and a.i_area='$area1' 
                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%')
                    order by a.i_customer", FALSE)->limit($num,$offset);
     }

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function caricustomer($cari,$num,$offset){
		$area1	= $this->session->userdata('i_area');
		$this->db->select(" a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and a.i_area='$area1' and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($inotapb,$icustomer)
    {
		$this->db->select("a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_notapb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_notapb ='$inotapb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($inotapb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_notapb_item a, tr_product_motif b
						 where a.i_notapb = '$inotapb' and a.i_customer='$icustomer' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaproduct($num,$offset,$cari,$cust)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, b.v_product_retail as harga, c.e_product_name as nama
						                    from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d
						                    where a.i_product=c.i_product and a.i_product=b.i_product 
						                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
						                    and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
                                order by c.i_product, a.e_product_motifname, b.i_price_group
                                limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_notapb'    => $inotapb,
      'i_area'      => $iarea,
      'i_spg'       => $ispg,
      'i_customer'  => $icustomer,
      'd_notapb'    => $dnotapb,
      'n_notapb_discount' => $nnotapbdiscount,
      'v_notapb_discount' => $vnotapbdiscount,
      'v_notapb_gross'    => $vnotapbgross,
      'f_notapb_cancel'   => 'f',
      'd_notapb_entry'    => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_notapb');
    }
    function insertdetail($inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_notapb'        => $inotapb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_notapb'        => $dnotapb,
          'i_product'       => $iproduct,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity'      => $nquantity,
          'v_unit_price'    => $vunitprice,
          'd_notapb_entry'  => $dentry,
          'n_item_no'       => $i,
          'e_product_name'  => $eproductname,
          'i_price_groupco' => $ipricegroupco,
          'e_remark'        => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_notapb_item');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark)
    {
    	$this->db->set(
    		array(
			'd_spmb'	  => $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	  => $iarea,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($iproduct, $iproductgrade, $inotapb, $iarea, $icustomer, $iproductmotif, $vunitprice) 
    {
		  $this->db->query("DELETE FROM tm_notapb_item WHERE i_notapb='$inotapb' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_customer='$icustomer' and v_unit_price=$vunitprice");
    }
	
    public function deleteheader($xinotapb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_notapb WHERE i_notapb='$xinotapb' and i_area='$iarea' and i_customer='$icustomer'");
    }
    function bacaperiode($icustomer,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                        a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
                        and b.i_customer='$icustomer' and
                        a.d_notapb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_notapb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_notapb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($icustomer,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                        a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
                        and b.i_customer='$icustomer' and
                        a.d_notapb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_notapb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_notapb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
