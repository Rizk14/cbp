<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
    {
        parent::__construct();
    #$this->CI =& get_instance();
    }
    function baca($todate,$prevdate,$th,$prevth,$bl)
    {
      $prevthbl=$prevth.$bl;
      $thbl=$th.$bl;
      $this->db->select(" a.kdproduk, a.namakategory, sum(volly) as volly, sum(volcy) as volcy, sum(vollm) as vollm, sum(volcm) as volcm
                         from(
                         select a.kdproduk, a.namakategory, sum(a.vol) as volly, 0 as volcy, 0 as vollm, 0 as volcm 
                         from
                         (
                         select a.i_product_category as kdproduk, b.e_product_categoryname as namakategory, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_category b, tm_nota_item c, tm_nota d
                         where b.i_product_category=a.i_product_category and c.i_product=a.i_product
                         and to_char(d.d_nota, 'yyyy') = '$prevth' and d.f_nota_cancel='f' and d.d_nota<='$prevdate'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_category, b.e_product_categoryname
                         ) as a
                         group by a.kdproduk, a.namakategory
                         union all
                         select a.kdproduk, a.namakategory, 0 as volly, sum(a.vol) as volcy, 0 as vollm, 0 as volcm 
                         from
                         (
                         select a.i_product_category as kdproduk, b.e_product_categoryname as namakategory, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_category b, tm_nota_item c, tm_nota d
                         where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                         and to_char(d.d_nota, 'yyyy') = '$th' and d.f_nota_cancel='f' and d.d_nota<='$todate'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_category, b.e_product_categoryname
                         ) as a
                         group by a.kdproduk, a.namakategory
                         union all
                         select a.kdproduk, a.namakategory, 0 as volly, 0 as volcy, sum(a.vol) as vollm, 0 as volcm 
                         from
                         (
                         select a.i_product_category as kdproduk, b.e_product_categoryname as namakategory, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_category b, tm_nota_item c, tm_nota d
                         where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                         and to_char(d.d_nota, 'yyyymm') = '$prevthbl' and d.f_nota_cancel='f' and d.d_nota<='$prevdate'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_category, b.e_product_categoryname
                         ) as a
                         group by a.kdproduk, a.namakategory
                         union all
                         select a.kdproduk, a.namakategory, 0 as volly, 0 as volcy, 0 as vollm, sum(a.vol) as volcm 
                         from
                         (
                         select a.i_product_category as kdproduk, b.e_product_categoryname as namakategory, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_category b, tm_nota_item c, tm_nota d
                         where b.i_product_category=a.i_product_category and c.i_product=a.i_product 
                         and to_char(d.d_nota, 'yyyymm') = '$thbl' and d.f_nota_cancel='f' and d.d_nota<='$todate'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_category, b.e_product_categoryname
                         ) as a
                         group by a.kdproduk, a.namakategory
                         )as a
                         group by a.kdproduk, a.namakategory
                         order by a.kdproduk",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
