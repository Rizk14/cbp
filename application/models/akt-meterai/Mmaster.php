<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacaperiode($dfrom, $dto, $iarea, $num, $offset, $cari)
    {
		if($iarea == "NA"){
			$this->db->select(" 	a.i_area,
									c.e_area_name,
									a.i_customer,
									b.e_customer_name,
									a.i_nota,
									a.d_nota,
									a.v_nota_netto,
									a.v_sisa,
									a.v_materai,
									a.v_materai_sisa
								FROM
									tm_nota a
								INNER JOIN tr_customer b ON
									(a.i_customer = b.i_customer
										AND a.i_area = b.i_area)
								INNER JOIN tr_area c ON
									(a.i_area = c.i_area)
								WHERE
									a.f_nota_cancel = 'f'
									AND a.v_sisa = 0
									AND a.v_materai_sisa > 0
									AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
									AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
								ORDER BY
									a.i_nota ",false);
		}else{
			$this->db->select(" 	a.i_area,
									c.e_area_name,
									a.i_customer,
									b.e_customer_name,
									a.i_nota,
									a.d_nota,
									a.v_nota_netto,
									a.v_sisa,
									a.v_materai,
									a.v_materai_sisa
								FROM
									tm_nota a
								INNER JOIN tr_customer b ON
									(a.i_customer = b.i_customer
										AND a.i_area = b.i_area)
								INNER JOIN tr_area c ON
									(a.i_area = c.i_area)
								WHERE
									a.f_nota_cancel = 'f'
									AND a.v_sisa = 0
									AND a.v_materai_sisa > 0
									AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
									AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
									AND a.i_area = '$iarea'
								ORDER BY
									a.i_nota ",false);
		}
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function bacaarea($num,$offset,$iuser) {
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
		   return $query->result();
		}
	}
	function cariarea($cari,$num,$offset,$iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function runningnumber($iarea,$thbl){
		$th   	= substr($thbl,0,4);
		$asal	= $thbl;
		$thbl	= substr($thbl,2,2).substr($thbl,4,2);
		$this->db->select(" n_modul_no as max from tm_dgu_no
							where i_modul='AM'
							and substr(e_periode,1,4)='$th' for update", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$terakhir=$row->max;
			}
			$noal  =$terakhir+1;
			$this->db->query(" update tm_dgu_no
							set n_modul_no=$noal
							where i_modul='AM'
							and substr(e_periode,1,4)='$th' ", false);
			settype($noal,"string");
			$a=strlen($noal);
			while($a<5){
				$noal="0".$noal;
				$a=strlen($noal);
			}
			$noal  ="AM-".$thbl."-".$noal;

			return $noal;
		}else{
			$noal  ="00001";
			$noal  ="AM-".$thbl."-".$noal;
			$this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
							values ('AM','00','$asal',1)");

			return $noal;
		}
	}
	function insertheader($ialokasi, $iarea, $icustomer, $dalokasi, $vjumlah, $icoabank)
	{
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		
		$this->db->set(
			array(
				'i_alokasi'		=> $ialokasi,
				'i_area'		=> $iarea,
				'i_customer'	=> $icustomer,
				'd_alokasi'		=> $dalokasi,
				'v_jumlah'		=> $vjumlah,
				'd_entry'		=> $dentry,
				'i_coa_bank'	=> $icoabank
			)
		);
		$this->db->insert('tm_meterai');
	}
	function insertdetail($ialokasi, $iarea, $inota, $dnota, $vjumlah, $vmeteraisisa, $icoabank)
	{
		//* n_item_no dibuat default 1 karena 1 alokasi 1 nota
		$this->db->set(
			array(
				'i_alokasi'		=> $ialokasi,
				'i_area'		=> $iarea,
				'i_nota'		=> $inota,
				'd_nota'		=> $dnota,
				'v_jumlah'		=> $vjumlah,
				'v_sisa'		=> $vmeteraisisa,
				'n_item_no'		=> 1,
				'i_coa_bank'	=> $icoabank
			)
		);
		$this->db->insert('tm_meterai_item');
	}
    function updatenota($inota, $iarea, $vjumlah)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate	= $row->c;
		$this->db->query(" UPDATE tm_nota SET v_materai_sisa = v_materai_sisa-$vjumlah WHERE i_nota = '$inota' AND i_area = '$iarea' ",FALSE);
    }
	function bacaheader($iarea, $ialokasi){
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, c.e_customer_address, c.e_customer_city
							FROM tm_meterai a
							inner join tr_area b on (a.i_area=b.i_area)
							inner join tr_customer c on (a.i_customer=c.i_customer)
							WHERE a.i_alokasi = '$ialokasi' AND a.i_area = '$iarea' ",false);
		
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	function bacadetail($iarea, $ialokasi){
		$this->db->select(" a.*, b.v_materai_sisa, b.v_materai
							FROM tm_meterai_item a
							inner join tm_nota b on (a.i_nota=b.i_nota)
							WHERE a.i_alokasi = '$ialokasi' AND a.i_area = '$iarea'
							order by a.i_alokasi,a.i_area ",false);
		
		$query = $this->db->get();
		if($query->num_rows()>0){
			return $query->result();
		}
	}
	//* POSTING
	function namaacc($icoa)
	{
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
		foreach ($query->result() as $tmp) {
			$xxx = $tmp->e_coa_name;
		}
		return $xxx;
		}
	}
	function inserttransheader($ipelunasan, $iarea, $egirodescription, $fclose, $dbukti)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;
		$egirodescription = str_replace("'", "''", $egirodescription);
		$this->db->query("insert into tm_jurnal_transharian 
							(i_refference, i_area, d_entry, e_description, f_close,d_refference,d_mutasi)
								values
							('$ipelunasan','$iarea','$dentry','$egirodescription','$fclose','$dbukti','$dbukti')",FALSE);
	}
	function inserttransitemdebet($accdebet, $ipelunasan, $namadebet, $fdebet, $fposting, $iarea, $egirodescription, $vjumlah, $dbukti, $icoabank)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
							(i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_debet, d_refference, d_mutasi, d_entry,i_coa_bank, i_area)
								values
							('$accdebet','$ipelunasan','$namadebet','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry','$icoabank', '$iarea')",FALSE);
	}
	function inserttransitemkredit($acckredit, $ipelunasan, $namakredit, $fdebet, $fposting, $iarea, $egirodescription, $vjumlah, $dbukti, $icoabank)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;
		$this->db->query("insert into tm_jurnal_transharianitem
							(i_coa, i_refference, e_coa_description, f_debet, f_posting, v_mutasi_kredit, d_refference, d_mutasi, d_entry,i_coa_bank,i_area)
								values
							('$acckredit','$ipelunasan','$namakredit','$fdebet','$fposting','$vjumlah','$dbukti','$dbukti','$dentry','$icoabank','$iarea')",FALSE);
	}
	function insertgldebet($accdebet, $ipelunasan, $namadebet, $fdebet, $iarea, $vjumlah, $dbukti, $egirodescription, $icoabank)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;
		$egirodescription = str_replace("'", "''", $egirodescription);
		$this->db->query("insert into tm_general_ledger
							(i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_debet,i_area,d_refference,e_description,d_entry,i_coa_bank)
								values
							('$ipelunasan','$accdebet','$dbukti','$namadebet','$fdebet',$vjumlah,'$iarea','$dbukti','$egirodescription','$dentry','$icoabank')",FALSE);
	}
	function insertglkredit($acckredit, $ipelunasan, $namakredit, $fdebet, $iarea, $vjumlah, $dbukti, $egirodescription, $icoabank)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;
		$egirodescription = str_replace("'", "''", $egirodescription);
		$this->db->query("insert into tm_general_ledger
							(i_refference,i_coa,d_mutasi,e_coa_name,f_debet,v_mutasi_kredit,i_area,d_refference,e_description,d_entry,i_coa_bank)
								values
							('$ipelunasan','$acckredit','$dbukti','$namakredit','$fdebet','$vjumlah','$iarea','$dbukti','$egirodescription','$dentry','$icoabank')",FALSE);
	}
	function carisaldo($icoa, $iperiode)
	{
		$query   = $this->db->query("SELECT current_timestamp as c");
		$row     = $query->row();
		$dentry  = $row->c;

		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row;
		}else{
			$this->db->query(" INSERT INTO tm_coa_saldo (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry, i_entry) 
								VALUES('$iperiode','$icoa',0,0,0,0,'$dentry','SYSTEM') ",FALSE);
			
			$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
			if ($query->num_rows() > 0) {
				$row = $query->row();
				return $row;
			}
		}
	}
	function updatesaldodebet($accdebet, $iperiode, $vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet+$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
							where i_coa='$accdebet' and i_periode='$iperiode'",FALSE);
	}
	function updatesaldokredit($acckredit, $iperiode, $vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit+$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
							where i_coa='$acckredit' and i_periode='$iperiode'",FALSE);
	}
	//* END POSTING
}
?>
