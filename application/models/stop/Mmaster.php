<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($istop,$iarea)
    {
		$this->db->select(" distinct on (tm_stop.i_stop||substring(x.i_nota,1,7)) (tm_stop.i_stop||substring(x.i_nota,1,7)) as xx, tm_stop.i_stop, 
                        tm_stop.i_stop_kirim, tm_stop.i_stop_via, tm_stop.i_area, tm_stop.i_ekspedisi, tm_stop.d_stop, 
                        tm_stop.i_stop_old, tm_stop.i_kendaraan, tm_stop.e_sopir_name, tm_stop.v_stop, tm_stop.f_stop_batal, 
                        tm_stop.d_entry, to_char(tm_stop.d_entry,'yyyy-mm-dd') as tglentry, tm_stop.d_update, tm_stop.i_approve1,
							          tr_area.e_area_name, tr_stop_kirim.e_stop_kirim, tr_stop_via.e_stop_via, tr_ekspedisi.e_ekspedisi
							          from tm_stop 
                        left join tm_nota x on(tm_stop.i_stop=x.i_stop and tm_stop.i_area=x.i_area)
							          inner join tr_area on(tm_stop.i_area=tr_area.i_area)
							          inner join tr_stop_kirim on(tm_stop.i_stop_kirim=tr_stop_kirim.i_stop_kirim)
							          inner join tr_stop_via on(tm_stop.i_stop_via=tr_stop_via.i_stop_via)
							          left join tr_ekspedisi on(tm_stop.i_ekspedisi=tr_ekspedisi.i_ekspedisi)
							          where  tm_stop.i_stop ='$istop'",false);# and tm_stop.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($istop,$iarea)
    {
		$this->db->select(" a.* from tm_stop_item a where a.i_stop='$istop'",false);# and i_area='$iarea' order by i_stop", false);
		/*$this->db->select(" a.*, b.i_nota
                        from tm_stop_item a, tm_nota b where a.i_stop='$istop'
                        and a.i_stop=b.i_stop and a.i_sj=b.i_sj and a.i_area=b.i_area",false);*/# and i_area='$iarea' order by i_stop", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetailrunningjml($istop,$iarea)
    {
		return $this->db->query(" select sum(v_jumlah) as v_total from tm_stop_item where i_stop='$istop' and i_area='$iarea' ", false);

    }    
    function customertodetail($isj,$dsj,$iarea)
    {
		return $this->db->query(" select b.i_customer, b.e_customer_name 
                              from tm_nota a
                              inner join tr_customer b on b.i_customer=a.i_customer 
                              where a.i_sj='$isj' and a.d_sj='$dsj' and a.i_area='$iarea'");
    }		
    function insertheader($istop,$dstop,$iarea,$eremark)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		
	    	$this->db->set(
	    		array(
				    'i_stop'		=> $istop,
				    'e_remark'	  	=> $eremark,
				    'i_area'	    => $iarea,
				    'd_stop'		=> $dstop,
				    'f_stop_batal'	=> 'f',
				    'd_entry'	    => $dentry
	    		)
	    	);
    	$this->db->insert('tm_stop');
		
    }
    function insertdetail($istop,$i_customer,$iop,$dstop,$dop,$isupplier,$ireff,$i)
    {
			//$this->db->query("DELETE FROM tm_stop_item WHERE i_stop='$istop' and i_area='$iarea' and i_op='$iop'");
			$this->db->set(
				array(
				'i_stop'	=> $istop,
				'i_customer'	=> $i_customer,
  				'i_op'	   	=> $iop,
  				'd_stop'   	=> $dstop,
  				'd_op'  	=> $dop,
  				'i_supplier'	=> $isupplier,
  				'i_reff'	=> $ireff,
          		'n_item_no' => $i
				)
			);
			$this->db->insert('tm_stop_item');
    }

	function updateop($istop,$isj,$iareasj,$dstop)
    {
    	$this->db->set(
    		array(
			'i_stop'	=> $istop,	
			'd_stop' => $dstop
    		)
    	);
    	$this->db->where('i_sj',$isj);
    	$this->db->where('i_area',$iareasj);
    	$this->db->update('tm_nota');
    }

    function updateheader($ispmb, $dspmb, $iarea, $ispmbold)
    {
    	$this->db->set(
    		array(
			'd_spmb'	=> $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	=> $iarea
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($istop, $iarea, $isj) 
    {
		  $this->db->select("v_jumlah from tm_stop_item
        						     where i_stop = '$istop' and i_area='$iarea' and i_sj='$isj'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $this->db->query("update tm_stop set v_stop=v_stop WHERE i_stop='$istop' and i_area='$iarea'");
				  $this->db->query("update tm_nota set i_stop=null, d_stop=null WHERE i_sj='$isj' and i_area='$iarea'");
			  }
		  }
		  $this->db->query("DELETE FROM tm_stop_item WHERE i_stop='$istop' and i_area='$iarea' and i_sj='$isj'");
    }

    public function deletedetail2($istop, $iarea, $isj, $iareasj) 
    {
      $iareaxx = $this->session->userdata('i_area');
      if($iareaxx=='00') $daer='f';
      if($iareaxx!='00') $daer='t';
		  $this->db->select(" v_jumlah from tm_stop_item
						     where i_stop = '$istop' and i_area_referensi='$iarea' and i_sj='$isj'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
				  $this->db->query("update tm_stop set v_stop=v_stop-$row->v_jumlah WHERE i_stop='$istop' and i_area_referensi='$iarea'");
				  $this->db->query("update tm_nota set i_stop=null WHERE i_sj_type='04' and i_sj='$isj' and i_area_referensi='$iareasj' and f_sj_daerah='$daer'");
			  }
		  }
		  $this->db->query("DELETE FROM tm_stop_item WHERE i_stop='$istop' and i_area_referensi='$iarea' and i_sj='$isj'");
    }
    	
    public function deleteheader($istop, $iarea) 
    {
		$this->db->query("DELETE FROM tm_stop WHERE i_stop='$istop' and i_area='$iarea'");
    }

    function bacasemua()
    {
			$this->db->select(" * from tm_spmb order by i_spmb desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaproduct($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($iarea,$thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
#      $thbl ='1602';
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='ST'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nostop  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nostop
                            where i_modul='ST'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nostop,"string");
			  $a=strlen($nostop);
			  while($a<4){
			    $nostop="0".$nostop;
			    $a=strlen($nostop);
			  }
		  	$nostop  ="ST-".$thbl."-".$iarea.$nostop;
			  return $nostop;
		  }else{
			  $nostop  ="0001";
		  	$nostop  ="ST-".$thbl."-".$iarea.$nostop;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('ST','$iarea','$asal',1)");
			  return $nostop;
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacacustomer($cari,$iarea,$num,$offset,$per)
    {
      $this->db->select(" a.i_customer, g.e_customer_statusname,  * from tr_customer a
				left join tr_customer_pkp b on
				(a.i_customer=b.i_customer)
				left join tr_price_group c on
				(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
				left join tr_customer_area d on
				(a.i_customer=d.i_customer)
				left join tr_customer_salesman e on
				(a.i_customer=e.i_customer and e.i_product_group='01' and e.e_periode='$per')
				left join tr_customer_discount f on
				(a.i_customer=f.i_customer) 
				left join tr_customer_status g on
				(a.i_customer_status= g.i_customer_status)
				where a.i_area='$iarea' and f_customer_aktif ='t'
          and a.f_approve='t' and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
               order by a.i_customer",false)->limit($num,$offset);
#          and a.f_approve='t' and a.f_approve2='t' and a.f_approve3='t' and a.f_approve4='t'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
	function bacakirim($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
			$this->db->select("* from tr_stop_kirim order by i_stop_kirim",false)->limit($num,$offset);
    }else{
			$this->db->select("* from tr_stop_kirim where i_stop_kirim='1' order by i_stop_kirim",false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function carikirim($cari,$num,$offset)
    {
		$this->db->select("i_stop_kirim, e_stop_kirim from tr_stop_kirim where upper(e_stop_kirim) like '%$cari%' or upper(i_stop_kirim) like '%$cari%' order by i_stop_kirim ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacavia($num,$offset)
    {
		$this->db->select("* from tr_stop_via order by i_stop_via", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carivia($cari,$num,$offset)
    {
		$this->db->select("i_stop_via, e_stop_via from tr_stop_via where upper(e_stop_via) like '%$cari%' or upper(i_stop_via) like '%$cari%' order by i_stop_via ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaekspedisi($area,$num,$offset)
    {
		$this->db->select("* from tr_ekspedisi
                       where i_area='$area' order by i_ekspedisi", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariekspedisi($cari,$num,$offset)
    {
		$this->db->select("i_ekspedisi, e_ekspedisi from tr_ekspedisi where upper(e_ekspedisi) like '%$cari%' or upper(i_ekspedisi) like '%$cari%' order by i_ekspedisi ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    	
	function bacaop($dstop,$cari,$area,$num,$offset)
    {
    $this->db->select(" a.i_op, a.d_op, a.i_supplier, d.e_supplier_name, a.i_reff, a.d_reff, b.i_customer, e.e_customer_name from tm_op a
						left join tm_spb b on a.i_reff = b.i_spb  
						left join tm_spmb c on a.i_reff = c.i_spmb  
						inner join tr_supplier d on a.i_supplier = d.i_supplier 
						left join tr_customer e on b.i_customer = e.i_customer
						where a.i_area ='$area' and a.f_op_cancel='f' and (a.d_reff = to_date('$dstop','dd-mm-yyyy') or a.d_op = to_date('$dstop','dd-mm-yyyy'))
						and (upper(a.i_op) like '%$cari%' or upper(d.e_supplier_name) like '%$cari%' 
						or upper(a.i_reff) like '%$cari%') 
						order by i_customer, i_op", false)->limit($num,$offset);
	        $query = $this->db->get();
	        if ($query->num_rows() > 0){
		    return $query->result();
	       }	
    }
	function carisj($iarea,$cari,$num,$offset,$areasj)
    {
      		$area1	= $this->session->userdata('i_area');
		if($area1=='00'){
		    $this->db->select("	a.*, b.i_customer, b.e_customer_name from tm_nota a, tr_customer b 
					        where a.i_nota isnull and a.i_sj_type='04' and a.i_area_to='$iarea' 
                            			and a.i_stop isnull and a.f_sj_cancel='f'
			                     	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                            			or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					        and a.i_customer=b.i_customer and a.f_sj_daerah='f' order by a.i_sj ", FALSE)->limit($num,$offset);
#					                  and a.i_sj not in(select i_sj from tm_stop_item where i_area='$iarea')
      		}else{
		    $this->db->select("	a.*, b.i_customer, b.e_customer_name from tm_nota a, tr_customer b 
					                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_to='$iarea' 
                            				and a.i_stop isnull
			                     		and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                            				or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                and a.i_customer=b.i_customer and a.f_sj_daerah='t' order by a.i_sj ", FALSE)->limit($num,$offset);
#					                  and a.i_sj not in(select i_sj from tm_stop_item where i_area='$iarea')
      		}
		$query = $this->db->get();
		if ($query->num_rows() < 1 || $query->num_rows()==0){
		    $this->db->select("	a.*, b.i_customer, b.e_customer_name from tm_nota a, tr_customer b 
					        where a.i_nota isnull and a.i_sj_type='04' and a.i_area_referensi='$iarea' 
                            and a.i_stop isnull and a.f_sj_cancel='f'
			                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                            or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					        and a.i_customer=b.i_customer and a.f_sj_daerah='f' and a.f_entry_pusat='t' order by a.i_sj ", FALSE)->limit($num,$offset);			
			$query = $this->db->get();		        
			return $query->result();		        
		}else{
			return $query->result();
		}
    }
    function insertdetailekspedisi($istop,$iarea,$iekspedisi,$dstop,$eremark,$i)
    {
		$this->db->query("DELETE FROM tm_stop_ekspedisi WHERE i_stop='$istop' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
		$this->db->set(
			array(
						'i_stop'			  => $istop,
						'i_area'		  => $iarea,
	 					'i_ekspedisi'	=> $iekspedisi,
	 					'd_stop' 		  => $dstop,
						'e_remark'		=> $eremark,
            'n_item_no'   => $i
			)
		);
		$this->db->insert('tm_stop_ekspedisi');
   } 
   public function deletedetailekspedisi($istop, $iarea, $iekspedisi) 
   {
		$this->db->query("DELETE FROM tm_stop_ekspedisi WHERE i_stop='$istop' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
   }     
   function bacadetailx($istop,$iarea)
   {
		$this->db->select("a.*, b.e_ekspedisi from tm_stop_ekspedisi a, tr_ekspedisi b
										   where a.i_stop = '$istop' and a.i_area='$iarea' and a.i_ekspedisi = b.i_ekspedisi order by a.i_stop", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
   }  
}
?>
