<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
	function bacabank($num,$offset)
    {
		  $this->db->select("* from tr_bank order by i_bank", false)->limit($num,$offset);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	function caribank($cari,$num,$offset)
    {
	  $this->db->select("i_bank, e_bank_name, i_coa from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%') 
	                     order by i_bank ", FALSE)->limit($num,$offset);

		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($icoabank,$dfrom,$dto,$ibank)
    {
		$this->db->select("i_reff, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_debet, v_kredit, i_periode, f_debet, 
		                   f_posting, i_cek, e_area_name, i_bank, i_coa_bank, e_bank_name
		                   from(
		                    			select	x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, 
		                    			a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank, 
		                    			a.i_coa_bank, d.e_bank_name from tr_area e, tm_kbank a
		                    			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
		                    			and b.i_coa_bank='$icoabank')
		                    			inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
		                    			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
		                    			and x.i_coa_bank='$icoabank')
		                    			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
		                    			where a.i_area=e.i_area	and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') 
		                    			AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
		                    			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_kbank_cancel='f'
                              union all
                              select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, 
                              a.v_bank as v_debet, 0 as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank, 
                              a.i_coa_bank, d.e_bank_name from tr_area e, tm_kbank a
                              left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
                              and b.i_coa_bank<>'$icoabank' and b.i_coa='$icoabank')
                              inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
                              left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
                              and x.i_coa_bank<>'$icoabank')
                              left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                              where a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy') 
                              AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') and a.i_coa='$icoabank' and d.i_bank <>'$ibank' 
                              AND a.f_kbank_cancel='f'
                              union all
		                      		select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, 
		                      		0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank, 
		                      		a.i_coa_bank, d.e_bank_name
		                    			from tr_area e, tm_kbank a
		                      		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' 
		                      		and b.i_coa_bank='$icoabank')
		                      		inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
		                      		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv 
		                      		and x.i_coa_bank='$icoabank')
		                      		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
		                      		where a.i_area=e.i_area and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') 
		                      		AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
		                      		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_kbank_cancel='f'
		                      		union all
                              select x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, 
                              0 as v_debet, a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank, 
                              a.i_coa_bank, d.e_bank_name from tr_area e, tm_kbank a
                              left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' 
                              and b.i_coa_bank<>'$icoabank' and b.i_coa='$icoabank')
                              inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
                              left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv 
                              and x.i_coa_bank<>'$icoabank')
                              left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
                              where a.i_area=e.i_area and a.d_bank >= to_date('$dfrom', 'dd-mm-yyyy') 
                              AND a.d_bank <= to_date('$dto', 'dd-mm-yyyy') and a.i_coa='$icoabank' and d.i_bank <>'$ibank' 
                              AND a.f_kbank_cancel='f'
                              ) as a
                        where a.i_coa_bank='$icoabank'
                        ORDER BY d_bank, f_debet, i_kbank",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
/*
*/
	function bacasaldo($tanggal,$icoabank)
  {	    
		$tmp = explode("-", $tanggal);
		$thn	= $tmp[0];
		$bln	= $tmp[1];
		$tgl 	= $tmp[2];
		$dsaldo	= $thn."/".$bln."/".$tgl;
		$dtos	= $this->mmaster->dateAdd("d",1,$dsaldo);
		$tmp1 	= explode("-", $dtos,strlen($dtos));
		$th	= $tmp1[0];
		$bl	= $tmp1[1];
		$dt	= $tmp1[2];
		$dtos	= $th.$bl;
		$this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ",false);
#and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.1'
		$query = $this->db->get();
		$saldo=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$saldo=$row->v_saldo_awal;
			}
		}
#		echo 'tanggal '.$tanggal.'<br>'.$dtos.'';die;
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL							
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x ",false);
		$query = $this->db->get();
		$kredit=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$kredit=$row->v_bank;
			}
		}
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','yyyy-mm-dd')  and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','yyyy-mm-dd') and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x",false);
		$query = $this->db->get();
		$debet=0;
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$debet=$row->v_bank;
			}
		}
		$saldo=$saldo+$debet-$kredit;
		return $saldo;
  }
	function dateAdd($interval,$number,$dateTime) {
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr=getdate($dateTime);
		$yr=$dateTimeArr['year'];
		$mon=$dateTimeArr['mon'];
		$day=$dateTimeArr['mday'];
		$hr=$dateTimeArr['hours'];
		$min=$dateTimeArr['minutes'];
		$sec=$dateTimeArr['seconds'];
		switch($interval) {
		    case "s"://seconds
		        $sec += $number;
		        break;
		    case "n"://minutes
		        $min += $number;
		        break;
		    case "h"://hours
		        $hr += $number;
		        break;
		    case "d"://days
		        $day += $number;
		        break;
		    case "ww"://Week
		        $day += ($number * 7);
		        break;
		    case "m": //similar result "m" dateDiff Microsoft
		        $mon += $number;
		        break;
		    case "yyyy": //similar result "yyyy" dateDiff Microsoft
		        $yr += $number;
		        break;
		    default:
		        $day += $number;
		     }      
		    $dateTime = mktime($hr,$min,$sec,$mon,$day,$yr);
		    $dateTimeArr=getdate($dateTime);
		    $nosecmin = 0;
		    $min=$dateTimeArr['minutes'];
		    $sec=$dateTimeArr['seconds'];
		    if ($hr==0){$nosecmin += 1;}
		    if ($min==0){$nosecmin += 1;}
		    if ($sec==0){$nosecmin += 1;}
		    if ($nosecmin>2){     
				return(date("Y-m-d",$dateTime));
			} else {     
				return(date("Y-m-d G:i:s",$dateTime));
			}
	}
	function namabank($icoabank)
    {
		  $this->db->select("* from tr_bank where i_coa='$icoabank'", false);
  		$query = $this->db->get();
		  if ($query->num_rows() > 0){
		    foreach($query->result() as $xx){
		      return $xx->e_bank_name;
		    }
			}
    }
}
?>
