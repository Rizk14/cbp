<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomer)
    {
		$this->db->select("i_customer, e_customer_ownername, e_customer_owneraddress, e_customer_ownerphone, e_customer_setor from tr_customer_owner where i_customer = '$icustomer' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomer, $ecustomerownername, $ecustomerowneraddress,$ecustomerownerphone,$ecustomersetor)
    {
    	$this->db->set(
    		array(
    			'i_customer' 	  	  => $icustomer,
    			'e_customer_ownername' 	  => $ecustomerownername,
			'e_customer_owneraddress' => $ecustomerowneraddress,
			'e_customer_ownerphone'	  => $ecustomerownerphone,
			'e_customer_setor'	=> $ecustomersetor
    		)
    	);
    	
    	$this->db->insert('tr_customer_owner');
		#redirect('customerowner/cform/');
    }
    function update($icustomer, $ecustomerownername, $ecustomerowneraddress, $ecustomerownerphone, $ecustomersetor)
    {
    	$data = array(
				'i_customer'              => $icustomer,
				'e_customer_ownername'    => $ecustomerownername,
				'e_customer_owneraddress' => $ecustomerowneraddress,
				'e_customer_ownerphone'   => $ecustomerownerphone,
				'e_customer_setor'	      => $ecustomersetor
            );
		  $this->db->where('i_customer =', $icustomer);
		  $this->db->update('tr_customer_owner', $data); 

      $this->db->select("i_customer from tr_customer_tmp where i_customer='$icustomer'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        $data = array(
                 'i_customer' 		          => $icustomer,
			           'e_customer_owner'         => $ecustomerownername,
			           'e_customer_owneraddress'  => $ecustomerowneraddress,
			           'e_customer_ownerphone'    => $ecustomerownerphone
                );
        $this->db->where('i_customer', $icustomer);
        $this->db->update('tr_customer_tmp', $data);
      }else{
        $this->db->select("i_customer from tr_customer_tmpnonspb where i_customer='$icustomer'", false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          $data = array(
                 'i_customer' 		          => $icustomer,
			           'e_customer_owner'         => $ecustomerownername,
			           'e_customer_owneraddress'  => $ecustomerowneraddress,
			           'e_customer_ownerphone'    => $ecustomerownerphone
                  );
          $this->db->where('i_customer', $icustomer);
          $this->db->update('tr_customer_tmpnonspb', $data);
        }
      }

		  #redirect('customerowner/cform/');
    }
	
    public function delete($icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer_owner WHERE i_customer='$icustomer'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("i_customer, e_customer_ownername, e_customer_owneraddress, e_customer_ownerphone, e_customer_setor from tr_customer_owner where upper(i_customer) like '%$cari%' or upper(e_customer_ownername) like '%$cari%' order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_customer, e_customer_ownername, e_customer_owneraddress, e_customer_ownerphone, e_customer_setor from tr_customer_owner where upper(i_customer) ilike '%$cari%' or upper(e_customer_ownername) ilike '%$cari%' order by i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacapelanggan($num,$offset) {
		$this->db->select("* from tr_customer order by e_customer_name asc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
    function caripelanggan($cari,$num,$offset) {
		$this->db->select("* from tr_customer where (upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%') order by e_customer_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}	
}
?>
