<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	function bacaheader_newexc($dfrom, $dto)
	{
		$this->db->select("	a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm') as masa_pajak, 
							to_char(a.d_pajak,'yyyy') as tahun_pajak, to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, 
							sum(d.v_dpp) as dpp,
							sum(d.v_ppn) as ppn, 
							(100-((a.v_nota_netto/a.v_nota_gross)*100)) as diskon, c.e_customer_pkpnpwp, 
							case when b.f_customer_pkp = 'f' and (length(b.i_nik) = 16) then b.i_nik||'#NIK#NAMA#' else '' end
							|| case when b.f_customer_pkp = 'f' then b.e_customer_name /* else c.e_customer_pkpname */ end as e_customer_name,
							b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
							from tm_nota a, tr_customer b, tr_customer_pkp c, tm_nota_item d
							where a.f_nota_cancel='false' and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and 
							a.i_customer = c.i_customer and a.i_sj=d.i_sj and a.i_area=d.i_area
							group by a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm'), to_char(a.d_pajak,'yyyy'), 
							to_char(a.d_pajak,'dd/mm/yyyy'), v_nota_netto, v_nota_gross, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress,b.f_customer_pkp
							order by a.i_faktur_komersial", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetail_newexc($dfrom, $dto, $inota)
	{
		$this->db->select(" 	a.i_nota,
								b.i_product,
								b.e_product_name,
								b.n_deliver,
								b.v_unit_price,
								b.v_dpp,
								b.v_ppn,
								b.v_nota_discount,
								b.v_netto,
								b.n_item_no,
								n_tax / 100 + 1 excl_divider,
								n_tax / 100 n_tax_val
							FROM
								tm_nota a
							LEFT JOIN tr_tax_amount c ON (a.d_sj BETWEEN d_start AND d_finish),
								tm_nota_item b
							WHERE
								a.f_nota_cancel = 'false'
								AND (d_pajak >= '$dfrom' AND d_pajak <= '$dto')
								AND a.i_nota = b.i_nota
								AND (a.i_nota = '$inota')
							ORDER BY b.n_item_no  ", false);
		return  $this->db->get();
	}
	/* function bacaheader($dfrom, $dto)
    {
		$this->db->select("	a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm') as masa_pajak, 
							to_char(a.d_pajak,'yyyy') as tahun_pajak, to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, 
							sum(d.v_dpp) as dpp,
							sum(d.v_ppn) as ppn, 
							(100-((a.v_nota_netto/a.v_nota_gross)*100)) as diskon, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
							from tm_nota a, tr_customer b, tr_customer_pkp c, tm_nota_item d
							where a.f_nota_cancel='false' and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and 
							a.i_customer = c.i_customer and a.i_sj=d.i_sj and a.i_area=d.i_area
							group by a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm'), to_char(a.d_pajak,'yyyy'), 
							to_char(a.d_pajak,'dd/mm/yyyy'), v_nota_netto, v_nota_gross, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
							b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
							order by a.i_faktur_komersial", false);
  		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
	function bacadetail($dfrom, $dto, $inota)
	{
		$this->db->select(" 	a.i_nota,
								a.i_product,
								a.e_product_name,
								a.n_deliver,
								a.v_dpp,
								a.v_ppn,
								(a.v_unit_price / excl_divider) AS v_unit_price,
								(a.n_deliver * a.v_unit_price) / excl_divider AS sub
							FROM
								(
									SELECT
										a.i_nota,
										b.i_product,
										b.e_product_name,
										b.n_deliver,
										b.v_unit_price,
										b.v_dpp,
										b.v_ppn,
										b.n_item_no,
										n_tax / 100 + 1 excl_divider,
										n_tax / 100 n_tax_val
									FROM
										tm_nota a
									LEFT JOIN tr_tax_amount c ON (a.d_sj BETWEEN d_start AND d_finish),
										tm_nota_item b
									WHERE
										a.f_nota_cancel = 'false'
										AND (d_pajak >= '$dfrom' AND d_pajak <= '$dto')
										AND a.i_nota = b.i_nota
										AND (a.i_nota = '$inota')
								) AS a
							ORDER BY a.n_item_no  ", false);
		return  $this->db->get();
		//   if ($query->num_rows() > 0){
		// 	  return $query->result();
		//   }
	}
	function bacaheader_unity($dfrom, $dto, $nppn)
	{
		$this->db->select(" * from f_vw_sales_tax_header('$dfrom','$dto','$nppn')", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacadetail_unity($dfrom, $dto, $nppn)
	{
		$this->db->select(" * from f_vw_sales_tax_detail('$dfrom','$dto','$nppn')", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	#	function bacaheader($dfrom, $dto, $fpengganti)
	function bacaheader($dfrom, $dto)
	{
		/*
		  $this->db->select("to_char(a.d_pajak,'mm') as masa_pajak, to_char(a.d_pajak,'yyyy') as tahun_pajak, 
		                    to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, floor(v_nota_netto/1.1) as dpp, floor((v_nota_netto/1.1)*0.1) as ppn, 
		                    (100-((v_nota_netto/v_nota_gross)*100)) as diskon, * 
		                    from tm_nota a, tr_customer b, tr_customer_pkp c
                        where a.f_nota_cancel='false' 
                        and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and a.i_customer = c.i_customer
                        order by a.i_faktur_komersial", false);
*/

		$this->db->select("a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm') as masa_pajak, 
		                     to_char(a.d_pajak,'yyyy') as tahun_pajak, to_char(a.d_pajak,'dd/mm/yyyy') as tgl_pajak, 
		                     floor((sum(d.v_unit_price*d.n_deliver)/1.1)-(((sum(d.v_unit_price*d.n_deliver)/1.1)*(100-((a.v_nota_netto/a.v_nota_gross)*100)))/100)) as dpp,
		                     floor(((sum(d.v_unit_price*d.n_deliver)/1.1)-(((sum(d.v_unit_price*d.n_deliver)/1.1)*(100-((a.v_nota_netto/a.v_nota_gross)*100)))/100))*0.1) as ppn, 
		                     (100-((a.v_nota_netto/a.v_nota_gross)*100)) as diskon, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
		                     b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
                         from tm_nota a, tr_customer b, tr_customer_pkp c, tm_nota_item d
                         where a.f_nota_cancel='false' and (a.d_pajak >='$dfrom' and a.d_pajak <='$dto') and b.i_customer=a.i_customer and 
                         a.i_customer = c.i_customer and a.i_sj=d.i_sj and a.i_area=d.i_area
                         group by a.i_nota, a.i_faktur_komersial, to_char(a.d_pajak,'mm'), to_char(a.d_pajak,'yyyy'), 
                         to_char(a.d_pajak,'dd/mm/yyyy'), v_nota_netto, v_nota_gross, c.e_customer_pkpnpwp, b.e_customer_name, b.i_nik,
		                     b.e_customer_address, a.f_pajak_pengganti, a.i_seri_pajak, c.e_customer_pkpname, c.e_customer_pkpaddress
                         order by a.i_faktur_komersial", false);
		#                        and a.f_pajak_pengganti='$fpengganti' and b.i_customer=a.i_customer
		#a.i_seri_pajak is not null and 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetail_old($dfrom, $dto, $fpengganti)
	{
		$this->db->select("b.n_deliver, b.v_unit_price, b.n_deliver * b.v_unit_price as sub, a.n_nota_discount1, a.n_nota_discount2, a.n_nota_discount3, 
                        round((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)) as dis1, 
                        round ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) as v_discount_product1,
                        round(
                        ((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) --v_discount_product1
                        * (a.n_nota_discount2/100) --%diskon
                        ) as disc2,

                        round(
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))- -- v_discount_product1
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * (a.n_nota_discount2/100)) --disc2
                        ) as v_discount2,

                        round(
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100)) --v_discount_product2
                        )* (a.n_nota_discount3/100) --%diskon
                        )as disc3,

                        round(
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100))
                        )- --v_discount_product2
                        (
                        ((b.v_unit_price * b.n_deliver) - ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100)))-
                        (((b.n_deliver * b.v_unit_price)- ((b.v_unit_price * b.n_deliver) * (a.n_nota_discount1/100))) * 
                        (a.n_nota_discount2/100)) --v_discount_product2
                        )* (a.n_nota_discount3/100)
                        ) as v_discount_product3,

                        a.*, (b.n_deliver * b.v_unit_price )as v_subtotal, a.i_nota, a.i_seri_pajak, b.i_product, b.*
                        from tm_nota  a, tm_nota_item b
                        where a.f_nota_cancel='false' 
                        and (d_pajak >='2015-06-01' and d_pajak <='2015-06-30')
                        and a.f_pajak_pengganti='false' and a.i_nota = b.i_nota
                        and (a.i_faktur_komersial ='FK-1506-17144' or a.i_faktur_komersial ='FK-1506-17145')
                        order by b.n_item_no", false);
		#a.i_seri_pajak is not null and 
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function caribank($cari, $num, $offset)
	{
		$this->db->select("i_bank, e_bank_name, i_coa from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%') 
	                     order by i_bank ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($icoabank, $dfrom, $dto, $ibank)
	{
		$this->db->select("i_reff, f_kbank_cancel, i_kbank, i_area, d_bank, i_coa, e_description, v_debet, v_kredit, i_periode, f_debet, f_posting, 
		                    i_cek, e_area_name, i_bank, i_coa_bank, e_bank_name
		                    	from(
		                    			select	x.i_rvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank as v_debet, 
		                    			0 as v_kredit, a.i_periode, a.f_debet,
		                    			a.f_posting, a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name
		                    			from tr_area e, tm_kbank a
		                    			left join tm_rv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='02' and b.i_coa_bank='$icoabank')
		                    			inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv)
		                    			left join tm_rvb x on(c.i_rv_type=x.i_rv_type and c.i_area=x.i_area and c.i_rv=x.i_rv and x.i_coa_bank='$icoabank')
		                    			left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
		                    			where a.i_area=e.i_area	and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
		                    			and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_kbank_cancel='f'
		                    			
		                    			union all
		                      
		                      		select x.i_pvb as i_reff, a.f_kbank_cancel, a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, 0 as v_debet, 
		                      		a.v_bank as v_kredit, a.i_periode, a.f_debet, a.f_posting, a.i_cek, e.e_area_name, d.i_bank, a.i_coa_bank, d.e_bank_name
		                    			from tr_area e, tm_kbank a
		                      		left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02' and b.i_coa_bank='$icoabank')
		                      		inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
		                      		left join tm_pvb x on(c.i_pv_type=x.i_pv_type and c.i_area=x.i_area and c.i_pv=x.i_pv and x.i_coa_bank='$icoabank')
		                      		left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
		                      		where a.i_area=e.i_area and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
		                      		and a.i_coa_bank='$icoabank' and d.i_bank ='$ibank' AND a.f_kbank_cancel='f'
	                      			) as a
                        ORDER BY d_bank, f_debet, i_kbank", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacasaldo($tanggal, $icoabank)
	{
		$tmp = explode("-", $tanggal);
		$thn	= $tmp[0];
		$bln	= $tmp[1];
		$tgl 	= $tmp[2];
		$dsaldo	= $thn . "/" . $bln . "/" . $tgl;
		$dtos	= $this->mmaster->dateAdd("d", 1, $dsaldo);
		$tmp1 	= explode("-", $dtos, strlen($dtos));
		$th	= $tmp1[0];
		$bl	= $tmp1[1];
		$dt	= $tmp1[2];
		$dtos	= $th . $bl;
		$this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ", false);
		#and substr(i_coa,6,2)='$area' and substr(i_coa,1,5)='111.1'
		$query = $this->db->get();
		$saldo = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$saldo = $row->v_saldo_awal;
			}
		}
		#		echo 'tanggal '.$tanggal.'<br>'.$dtos.'';die;
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL							
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= '$tanggal' and b.f_debet='t' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x ", false);
		$query = $this->db->get();
		$kredit = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$kredit = $row->v_bank;
			}
		}
		$this->db->select(" sum(x.v_bank) as v_bank from 
		          (
		          select sum(b.v_bank) as v_bank from tm_rv x, tm_rv_item z, tm_kbank b
							where x.i_rv=z.i_rv and x.i_area=z.i_area and x.i_rv_type=z.i_rv_type and x.i_rv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy')  and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
              UNION ALL
		          select sum(b.v_bank) as v_bank from tm_pv x, tm_pv_item z, tm_kbank b
							where x.i_pv=z.i_pv and x.i_area=z.i_area and x.i_pv_type=z.i_pv_type and x.i_pv_type='02'
							and b.i_periode='$dtos' and b.d_bank <= to_date('$tanggal','dd-mm-yyyy') and b.f_debet='f' and x.i_coa='$icoabank'
							and b.f_kbank_cancel='f' and z.i_kk=b.i_kbank and z.i_area_kb=b.i_area and x.i_periode=b.i_periode
							) as x", false);
		$query = $this->db->get();
		$debet = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$debet = $row->v_bank;
			}
		}
		$saldo = $saldo + $debet - $kredit;
		return $saldo;
	}
	function dateAdd($interval, $number, $dateTime)
	{
		$dateTime = (strtotime($dateTime) != -1) ? strtotime($dateTime) : $dateTime;
		$dateTimeArr = getdate($dateTime);
		$yr = $dateTimeArr['year'];
		$mon = $dateTimeArr['mon'];
		$day = $dateTimeArr['mday'];
		$hr = $dateTimeArr['hours'];
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		switch ($interval) {
			case "s": //seconds
				$sec += $number;
				break;
			case "n": //minutes
				$min += $number;
				break;
			case "h": //hours
				$hr += $number;
				break;
			case "d": //days
				$day += $number;
				break;
			case "ww": //Week
				$day += ($number * 7);
				break;
			case "m": //similar result "m" dateDiff Microsoft
				$mon += $number;
				break;
			case "yyyy": //similar result "yyyy" dateDiff Microsoft
				$yr += $number;
				break;
			default:
				$day += $number;
		}
		$dateTime = mktime($hr, $min, $sec, $mon, $day, $yr);
		$dateTimeArr = getdate($dateTime);
		$nosecmin = 0;
		$min = $dateTimeArr['minutes'];
		$sec = $dateTimeArr['seconds'];
		if ($hr == 0) {
			$nosecmin += 1;
		}
		if ($min == 0) {
			$nosecmin += 1;
		}
		if ($sec == 0) {
			$nosecmin += 1;
		}
		if ($nosecmin > 2) {
			return (date("Y-m-d", $dateTime));
		} else {
			return (date("Y-m-d G:i:s", $dateTime));
		}
	}
}
