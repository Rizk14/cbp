<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($icustomer,$dfrom,$dto,$cari, $num,$offset)
    {
      $this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                          from tm_notapb a, tr_customer b, tr_area c, tr_spg d
                          where a.i_customer=b.i_customer
                          and a.i_area=c.i_area and b.i_area=c.i_area
                          and a.i_spg=d.i_spg and b.i_customer=d.i_customer
                          and a.i_customer='$icustomer'
                          and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_notapb <= to_date('$dto','dd-mm-yyyy'))
                          and (upper(a.i_notapb) like '%$cari%' or upper(a.i_spg) like '%$cari%'
                              or upper(d.e_spg_name) like '%$cari%')
                          order by a.d_notapb, a.i_notapb",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($icustomer,$dfrom,$dto,$cari,$num,$offset)
    {
		  $this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                          from tm_notapb a, tr_customer b, tr_area c, tr_spg d
                          where a.i_customer=b.i_customer
                          and a.i_area=c.i_area and b.i_area=c.i_area
                          and a.i_spg=d.i_spg and b.i_customer=d.i_customer
                          and a.i_customer='$icustomer'
                          and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                          and a.d_notapb <= to_date('$dto','dd-mm-yyyy'))
                          and (upper(a.i_notapb) like '%$cari%' or upper(a.i_spg) like '%$cari%'
                              or upper(d.e_spg_name) like '%$cari%')
                          order by a.d_notapb, a.i_notapb",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function baca($inotapb, $icustomer)
    {
		  $this->db->select(" a.*, b.e_area_name, c.e_customer_name, d.e_spg_name
                          from tm_notapb a, tr_area b, tr_customer c, tr_spg d
					                where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					                and a.i_notapb ='$inotapb' and a.i_customer='$icustomer'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
	  function bacadetail($inotapb, $icustomer)
    {
		  $this->db->select(" a.*, b.e_product_motifname from tm_notapb_item a, tr_product_motif b
						              where a.i_notapb = '$inotapb' and a.i_customer='$icustomer'
                          and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						              order by a.n_item_no", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function approve($inotapb,$icustomer,$ecek,$user)
    {
		  $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd') as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
    	$data = array(
					'e_cek'		=> $ecek,
					'd_cek'		=> $dentry,
					'i_cek'		=> $user
    				 );
    	$this->db->where('i_notapb', $inotapb);
    	$this->db->where('i_customer', $icustomer);
		  $this->db->update('tm_notapb', $data);
    }

	  function bacacustomer($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select(" a.*, b.e_customer_name, c.e_area_name
                            from tr_spg a, tr_customer b, tr_area c
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select(" a.*, b.e_customer_name, c.e_area_name
                            from tr_spg a, tr_customer b, tr_area c
                            where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                            and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
						                or a.i_area = '$area4' or a.i_area = '$area5')", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function caricustomer($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $this->db->select("a.*, b.e_customer_name, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                          and (upper(a.i_customer) like '%$cari%'
                          or upper(b.e_customer_name) like '%$cari%') ", FALSE)->limit($num,$offset);
		  }else{
			  $this->db->select("a.*, b.e_customer_name, c.e_area_name
                          from tr_spg a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area and
                          (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                          and (a.i_area = '$area1' or a.i_area = '$area2' 
    										  or a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5')", FALSE)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function updateheader($inotapb,$icustomer,$ecek,$user)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
      $row   		= $query->row();
      $dnotapbupdate	= $row->c;
      	$data = array(
	      'e_cek'		    => $ecek,
	      'd_cek'		    => $dnotapbupdate,
	      'i_cek'		    => $user
              );
      $this->db->where('i_notapb', $inotapb);
      $this->db->where('i_customer', $icustomer);
      $this->db->update('tm_notapb', $data); 
    }
}
?>
