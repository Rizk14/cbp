<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ispmb)
    {
	$this->db->select(" * from tm_spmb 
			   inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
			   where i_spmb ='$ispmb'", false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->row();
	}
    }
    function bacadetail($ispmb)
    {
	/* Disabled 14042011
	$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
			   where a.i_spmb = '$ispmb' and a.i_product_motif=b.i_product_motif and a.i_product=b.i_product
			   order by a.i_product", false);
	*/
	$this->db->select(" a.*, b.e_product_motifname from tm_spmb_item a, tr_product_motif b
			   where a.i_spmb = '$ispmb' and a.i_product_motif=b.i_product_motif and a.i_product=b.i_product
			   order by a.i_product asc ", false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function spmbsiapsj($ispmb)
    {
    	$data = array(
		'f_spmb_pemenuhan'=>'t'
	);
	$this->db->where('i_spmb', $ispmb);
	$this->db->update('tm_spmb', $data);
    }    
    function insertdetail($ispmb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$ndeliver,$iarea)
    {
    	$this->db->set(
    		array(
					'i_spmb'		=> $ispmb,
					'i_product'		=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'n_order'		=> $norder,
					'n_deliver'		=> $ndeliver,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname,
					'i_area'		=> $iarea
    		)
    	);
    	
    	$this->db->insert('tm_spmb_item');
    }
    function updateheader($ispmb,$fspmbop,$fspmbclose,$fspmbcancel)
    {
    	$data = array(
			'f_op' 			=> $fspmbop,
			'f_spmb_close'	=> $fspmbclose,
			'f_spmb_cancel'	=> $fspmbcancel
		            );
	$this->db->where('i_spmb', $ispmb);
	$this->db->update('tm_spmb', $data); 
    }

    public function deletedetail($iproduct, $iproductgrade, $ispmb, $iarea) 
    {
	$delete_spmb = $this->db->query("DELETE FROM tm_spmb_item WHERE i_spmb='$ispmb' and i_area='$iarea'
						and i_product='$iproduct' and i_product_grade='$iproductgrade'");
	return TRUE;
    }
	
    public function delete($ispmb) 
    {
	$delete_spmb = $this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
	$delete_spmb_item = $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
	return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
	$this->db->select(" distinct(b.i_spmb) as no ,a.*, c.e_area_name from tm_spmb_item b, tm_spmb a, tr_area c
						where not a.i_approve2 isnull
						and not a.i_store isnull
						and not a.i_store_location isnull
						and a.f_op = 'f' and a.f_spmb_pemenuhan='f'
						and a.f_spmb_close = 'f'
						and a.f_spmb_cancel = 'f'
						and upper(a.i_spmb) like '%$cari%' 
						and a.i_spmb=b.i_spmb and b.n_order>b.n_stock
						and a.i_area=c.i_area
						order by a.i_spmb desc",false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function bacaproduct($num,$offset,$kdharga)
    {
		$this->db->select("i_product, i_product_grade, e_product_name, v_product_retail
						   from tr_product_price 
						   where i_price_group = '$kdharga'
						   order by i_product, i_product_grade",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacastore($num,$offset,$area)
    {
	$this->db->select("a.*, b.* from tr_store a, tr_store_location b
					   where a.i_store in(select i_store from tr_area 
					   where i_area='$area' or i_area='00')
					   and a.i_store=b.i_store
					   order by a.i_store",false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function runningnumber(){
    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
		$row   	= $query->row();
		$thbl	= $row->c;
		$th		= substr($thbl,0,2);
		$this->db->select(" max(substr(i_spmb,10,6)) as max from tm_spmb 
				  			where substr(i_spmb,5,2)='$th' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
			  $terakhir=$row->max;
			}
			$nospmb  =$terakhir+1;
			settype($nospmb,"string");
			$a=strlen($nospmb);
			while($a<6){
			  $nospmb="0".$nospmb;
			  $a=strlen($nospmb);
			}
			$nospmb  ="spmb-".$thbl."-".$nospmb;
			return $nospmb;
		}else{
			$nospmb  ="000001";
			$nospmb  ="spmb-".$thbl."-".$nospmb;
			return $nospmb;
		}
    }
    function cari($cari,$num,$offset)
    {
	$this->db->select(" distinct(b.i_spmb) as no ,a.*, c.e_area_name from tm_spmb_item b, tm_spmb a, tr_area c
						where not a.i_approve1 isnull
						and not a.i_approve2 isnull
						and not a.i_store isnull
						and not a.i_store_location isnull
						and a.f_op = 'f' and a.f_spmb_pemenuhan='f'
						and a.f_spmb_close = 'f'
						and a.f_spmb_cancel = 'f'
						and upper(a.i_spmb) like '%$cari%' 
						and a.i_spmb=b.i_spmb and b.n_order>b.n_stock
						and a.i_area=c.i_area
						order by a.i_spmb desc",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function caristore($cari,$num,$offset)
    {
	$this->db->select("a.*, b.* from tr_store a, tr_store_location b
					   where a.i_store in(select i_store from tr_area 
					   where i_area='$area' or i_area='00')
					   and a.i_store=b.i_store and upper(a.i_store) like '%$cari%' 
					   or upper(a.e_store_name) like '%$cari%' order by a.i_store",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
    function cariproduct($cari,$num,$offset,$kdstore)
    {
	$this->db->select("  * from tr_product_price
			     where (upper(i_product) like '%$cari%' 
				or upper(e_product_name) like '%$cari%'
				or upper(i_product_grade) like '%$cari%')
				and i_product in(
							 select i_product from tm_ic where i_store = 'AA' or i_store='$kdstore')
			    order by i_product, i_product_grade",FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
	function bacaarea($num,$offset)
    {
	$this->db->select("i_area, e_area_name from tr_area order by i_area", false)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
	function cariarea($cari,$num,$offset)
    {
	$this->db->select("i_area, e_area_name from tr_area where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' order by i_area ", FALSE)->limit($num,$offset);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
		return $query->result();
	}
    }
}
?>
