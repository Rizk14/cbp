<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_op WHERE i_op=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_op_item WHERE i_op=\''.$iop.'\'');
			return TRUE;
    }
   function bacasemua($iuser,$cari, $num,$offset,$dfrom,$dto)
    {
      
      /*if($iarea!='PB'){
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  				where 
			  				a.i_customer=b.i_customer 
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sj) like '%$cari%') 
							and substring(a.i_sj,9,2)=c.i_area 
							and substring(a.i_sj,9,2)='$iarea'
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							order by i_sj desc",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and substring(a.i_sj,9,2)=c.i_area 
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sj) like '%$cari%') 
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
                            and substring(a.i_sj,9,2)='BK'
							and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							order by i_sj desc",false)->limit($num,$offset);
      }*/
      	$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  				where 
			  				a.i_customer=b.i_customer 
			  				and not a.i_sjk isnull
			  				and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sjk) like '%$cari%') 
							and a.i_area=c.i_area 
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and a.d_sj_receive >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj_receive <= to_date('$dto','dd-mm-yyyy')
							order by a.i_sjk",false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
				return $query->result();
			}
    }
    function baca($isj)
    {
		$this->db->select(" tm_nota.*, c.e_customer_name, c.e_customer_address, c.e_customer_city, 
							c.f_customer_plusppn as f_plus_ppn,
							c.e_customer_phone, tr_area.e_area_name, tm_spb.i_spb_po, tm_spb.f_spb_consigment  
							from tm_nota inner join tr_customer c on (tm_nota.i_customer=c.i_customer)
				          	inner join tr_area on (tm_nota.i_area=tr_area.i_area)
				          	left join tm_spb on (tm_spb.i_spb=tm_nota.i_spb and tm_spb.i_area=tm_nota.i_area)
							where tm_nota.i_sjk = '$isj'",false);
		
		/*$this->db->select(" tm_nota.*, c.e_customer_name, c.e_customer_address, c.e_customer_city, 
							c.f_customer_plusppn as f_plus_ppn,
							c.e_customer_phone, tr_area.e_area_name, tm_spb.i_spb_po, tm_spb.f_spb_consigment  
							from tm_nota inner join tr_customer c on (tm_nota.i_customer=c.i_customer)
				          	inner join tr_area on (substring(tm_nota.i_sj,9,2)=tr_area.i_area)
				          	left join tm_spb on (tm_spb.i_spb=tm_nota.i_spb and tm_spb.i_area=tm_nota.i_area)
							where tm_nota.i_sj = '$isj' and substring(tm_nota.i_sj,9,2)='$iarea'",false);*/
		
		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
				return $query->result();
			}
    }
    function bacadetail($isj)
    {
      	$cust='';
		$tes=$this->db->query(" select i_customer from tm_nota where i_sjk = '$isj'",false);

		/*$tes=$this->db->query(" select i_customer from tm_nota where i_sjk = '$isj' 
								and substring(i_sj,9,2)='$iarea'",false);*/
		
		if ($tes->num_rows() > 0)
			{
        		foreach($tes->result() as $xx)
        			{
  			  			$cust=$xx->i_customer;
        			}
		  	}
      
      	$group	= '';
		$que 	= $this->db->query("select i_customer_plugroup from tr_customer_plugroup 
									where i_customer='$cust'",false);
		if($que->num_rows()>0)
			{
        		foreach($que->result() as $hmm)
        			{
          				$group=$hmm->i_customer_plugroup;
        			}
      		}
      	
      	if($group=='')
      		{
		    	$this->db->select(" * from tm_nota_item inner join tr_product_motif on 
									(tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
									and tm_nota_item.i_product=tr_product_motif.i_product)
									where i_sj in(select i_sj from tm_nota where i_sjk='$isj')
									order by n_item_no",false);
		    
		    /*$this->db->select(" * from tm_nota_item inner join tr_product_motif on 
		    						(tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
									and tm_nota_item.i_product=tr_product_motif.i_product)
									where i_sj = '$isj' and substring(i_sj,9,2)='$iarea' 
									order by n_item_no",false);
		    */
				$query = $this->db->get();
				if ($query->num_rows() > 0)
					{
						return $query->result();
		    		}
      		}else{
		    	$this->db->select(" a.i_sj, a.i_nota, a.i_product as product, a.i_product_grade,
									a.i_product_motif, a.n_deliver, a.v_unit_price, a.e_product_name, a.i_area, 
									a.d_nota, a.n_item_no, a.i_customer_plu, c.i_product 
									from tm_nota_item a
									inner join tr_product_motif b on (a.i_product_motif=b.i_product_motif and 
									a.i_product=b.i_product)
									left join tr_customer_plu c on (c.i_customer_plugroup='$group' and 
									a.i_product=c.i_product) 
									where i_sj in(select i_sj from tm_nota where i_sjk='$isj')
									order by n_item_no",false);

		    	/*$this->db->select(" a.i_sj, a.i_nota, a.i_product as product, a.i_product_grade,
		    						a.i_product_motif, a.n_deliver, a.v_unit_price, a.e_product_name, a.i_area, 
		    						a.d_nota, a.n_item_no, a.i_customer_plu, c.i_product from tm_nota_item a
									inner join tr_product_motif b on (a.i_product_motif=b.i_product_motif and 
									a.i_product=b.i_product)
                            		left join tr_customer_plu c on (c.i_customer_plugroup='$group' and 
                            		a.i_product=c.i_product) 
							        where i_sj = '$isj' and substring(i_sj,9,2)='$iarea' 
							        order by n_item_no",false);*/

				$query = $this->db->get();
		    	if ($query->num_rows() > 0)
		    		{
			    		return $query->result();
		    		}
      			}
    }
    function cari($cari,$num,$offset,$area,$iuser,$dfrom,$dto)
    {
      /*if($iarea!='PB'){
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  				where 
			  				a.i_customer=b.i_customer 
			  				and substring(a.i_sj,9,2)=c.i_area 
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sj) like '%$cari%') 
							and substring(a.i_sj,9,2)='$iarea'
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							order by i_sj desc",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
							where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area 
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sj) like '%$cari%') 
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
                            and substring(a.i_sj,9,2)='BK'
							and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							order by i_sj desc",false)->limit($num,$offset);
      }*/
      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  				where 
			  				a.i_customer=b.i_customer 
			  				and not a.i_sjk isnull
			  				and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_sjk) like '%$cari%') 
							and a.i_area=c.i_area 
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and a.d_sj_receive >= to_date('$dfrom','dd-mm-yyyy') 
							and a.d_sj_receive <= to_date('$dto','dd-mm-yyyy')
							order by a.i_sjk",false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0)
			{
				return $query->result();
			}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function updatesj($isj)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dprint	= $row->c;
      	$this->db->set(array('d_sj_print'=> $dprint));
		$this->db->where('i_sjk', $isj);
		#$this->db->where('i_area', $iarea);
		$this->db->update('tm_nota'); 
    }
/*DICOMMENT TANGGAL 15-07-2017*/
/*function bacasemua($cari, $num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$dfrom,$dto)
    {
      $iuser   = $this->session->userdata('user_id');
      if($iarea1!='PB'){
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
						                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and substring(a.i_sj,9,2) in ( select i_area from tm_user_area where i_user='$iuser')
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
						                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and substring(a.i_sj,9,2) in ( select i_area from tm_user_area where i_user='$iuser') 
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }*/
    /*function bacasemua($cari, $num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$dfrom,$dto)
    {
      if($iarea1!='PB'){
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
						                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2) = '$iarea2' or substring(a.i_sj,9,2) = '$iarea3' 
						                or substring(a.i_sj,9,2) = '$iarea4' 
						                or substring(a.i_sj,9,2) = '$iarea5') 
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
						                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
						                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                or upper(a.i_sj) like '%$cari%')
						                and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2) = '$iarea2' or substring(a.i_sj,9,2) = '$iarea3' 
						                or substring(a.i_sj,9,2) = '$iarea4' or substring(a.i_sj,9,2) = '$iarea5' or substring(a.i_sj,9,2) = 'BK') 
											      and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
											      order by a.i_sj desc",false)->limit($num,$offset);
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($isj,$iarea)
    {
		$this->db->select(" tm_nota.*, c.e_customer_name, c.e_customer_address, c.e_customer_city, c.f_customer_plusppn as f_plus_ppn,
												c.e_customer_phone, tr_area.e_area_name, tm_spb.i_spb_po, tm_spb.f_spb_consigment  from tm_nota
							          inner join tr_customer c on (tm_nota.i_customer=c.i_customer)
							          inner join tr_area on (substring(tm_nota.i_sj,9,2)=tr_area.i_area)
							          left join tm_spb on (tm_spb.i_spb=tm_nota.i_spb and tm_spb.i_area=tm_nota.i_area)
							          where tm_nota.i_sj = '$isj' and substring(tm_nota.i_sj,9,2)='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($isj,$iarea)
    {
      $cust='';
		  $tes=$this->db->query("select i_customer from tm_nota where i_sj = '$isj' and substring(i_sj,9,2)='$iarea'",false);
		  if ($tes->num_rows() > 0){
        foreach($tes->result() as $xx){
  			  $cust=$xx->i_customer;
        }
		  }
      $group='';
		  $que 	= $this->db->query(" select i_customer_plugroup from tr_customer_plugroup where i_customer='$cust'",false);
		  if($que->num_rows()>0){
        foreach($que->result() as $hmm){
          $group=$hmm->i_customer_plugroup;
        }
      }
      if($group==''){
		    $this->db->select(" * from tm_nota_item
							              inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
														              and tm_nota_item.i_product=tr_product_motif.i_product)
							              where i_sj = '$isj' and substring(i_sj,9,2)='$iarea' order by n_item_no",false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
		    $this->db->select(" a.i_sj, a.i_nota, a.i_product as product, a.i_product_grade, a.i_product_motif, a.n_deliver, a.v_unit_price,
                            a.e_product_name, a.i_area, a.d_nota, a.n_item_no, a.i_customer_plu, c.i_product from tm_nota_item a
							              inner join tr_product_motif b on (a.i_product_motif=b.i_product_motif and a.i_product=b.i_product)
                            left join tr_customer_plu c on (c.i_customer_plugroup='$group' and a.i_product=c.i_product) 
							              where i_sj = '$isj' and substring(i_sj,9,2)='$iarea' order by n_item_no",false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }
    function cari($cari,$num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$dfrom,$dto)
    {
      if($iarea1!='PB'){
		    $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
							              where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
							              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							              or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2) = '$iarea2' 
												    or substring(a.i_sj,9,2) = '$iarea3' or substring(a.i_sj,9,2) = '$iarea4' or substring(a.i_sj,9,2) = '$iarea5')
												    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							              order by a.i_sj desc",FALSE)->limit($num,$offset);
      }else{
		    $this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
							              where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
							              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							              or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2) = '$iarea2' 
												    or substring(a.i_sj,9,2) = '$iarea3' or substring(a.i_sj,9,2) = '$iarea4' or substring(a.i_sj,9,2) = '$iarea5'
                            or substring(a.i_sj,9,2) = 'BK')
												    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')
							              order by a.i_sj desc",FALSE)->limit($num,$offset);
      }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function updatesj($isj, $iarea)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dprint	= $row->c;
      $this->db->set(
      		array(
			  'd_sj_print'			=> $dprint
      		)
      	);
		  $this->db->where('i_sj', $isj);
		  $this->db->where('i_area', $iarea);
		  $this->db->update('tm_nota'); 
    }*/
}
?>
