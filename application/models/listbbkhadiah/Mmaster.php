<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ibbk) 
    {
  		$this->db->query("update tm_bbk set f_bbk_cancel='t' WHERE i_bbk='$ibbk' and i_bbk_type='03'");
######
		  $ibbk=trim($ibbk);
		  $this->db->select(" b.e_product_name, a.d_bbk, b.n_quantity, b.i_product, b.i_product_grade, b.i_product_motif 
                          from tm_bbk a, tm_bbk_item b WHERE a.i_bbk=b.i_bbk and a.i_bbk='$ibbk'");
		  $query = $this->db->get();
		  foreach($query->result() as $row){
			  $jml    = $row->n_quantity;
			  $product= $row->i_product;
			  $grade  = $row->i_product_grade;
			  $motif  = $row->i_product_motif;
			  $eproductname = $row->e_product_name;
        $dbbk    = $row->d_bbk;
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
        $th=substr($dbbk,0,4);
			  $bl=substr($dbbk,5,2);
			  $emutasiperiode=$th.$bl;
			  $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$ibbk'
                                    order by d_transaction desc, i_trans desc",false);
        if ($queri->num_rows() > 0){
      	  $row   		= $queri->row();
          $que 	= $this->db->query("SELECT current_timestamp as c");
	        $ro 	= $que->row();
	        $now	 = $ro->c;
          $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$product','$grade','$motif','$istore','$istorelocation','$istorelocationbin', 
                                '$eproductname', '$ibbk', '$now', $jml, 0, $row->n_quantity_akhir+$jml, $row->n_quantity_akhir
                              )
                           ",false);
        }
        if( ($jml!='') && ($jml!=0) ){
          $this->db->query(" 
                            UPDATE tm_mutasi set n_mutasi_bbk=n_mutasi_bbk-$jml, n_saldo_akhir=n_saldo_akhir+$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            and e_mutasi_periode='$emutasiperiode'
                           ",false);
          $this->db->query(" 
                            UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$jml
                            where i_product='$product' and i_product_grade='$grade' and i_product_motif='$motif'
                            and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                           ",false);
        }

		  }
######
    }
    function bacasemua($cari, $num,$offset)
    {
      $this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
					                from tr_customer b, tm_ttbretur c, tm_bbm a
					                left join tm_kn d on (a.i_bbm=d.i_refference) 
					                where 
					                c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
					                a,i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
					                or upper(a.i_bbm) like '%$cari%') 
					                order by a.i_bbm desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
							          from tm_bbm a, tr_customer b, tm_ttbretur c
							          where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
							          and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
							          order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name
							          from tm_bbk a, tr_customer b
					              where a.i_supplier=b.i_customer and a.i_bbk_type='03'
					              and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbk) like '%$cari%')
					              and a.d_bbk >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bbk <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbk ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, c.i_customer, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area, d.i_kn, d.d_kn 
							          from tr_customer b, tm_ttbretur c, tm_bbm a
							          left join tm_kn d on (a.i_bbm=d.i_refference) 
							          where 
							          c.i_customer=b.i_customer and a.i_bbm=c.i_bbm and c.i_customer=b.i_customer and 
							          a.i_area=c.i_area and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
							          or upper(a.i_bbm) like '%$cari%') 
							          and a.i_area='$iarea' and
							          a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_bbm <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_bbm desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
