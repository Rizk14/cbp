<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset,$dfrom,$dto)
    {
    if($iarea1=='00'){
		$this->db->select(" a.* from tm_bbk_pajak a
	                      where upper(a.i_faktur_komersial) like '%$cari%'
                        and a.d_pajak >= to_date('$dfrom','dd-mm-yyyy') and a.d_pajak <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
					              order by a.i_faktur_komersial desc",false)->limit($num,$offset);
#and (a.n_print=0 or a.n_print isnull)
    }else{
		$this->db->select(" a.* from tm_nota a
	                      where upper(a.i_faktur_komersial) like '%$cari%'
	                      and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' 
                        or a.i_area='$iarea5')
                        and a.d_pajak >= to_date('$dfrom','dd-mm-yyyy') and a.d_pajak <= to_date('$dto','dd-mm-yyyy')
                        and a.i_area='$iarea' and not a.i_faktur_komersial isnull
              					order by a.i_faktur_komersial desc",false)->limit($num,$offset);
#and (a.n_print=0 or a.n_print isnull)
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ifaktur,$iarea)
    {
		$this->db->select(" distinct(a.i_faktur_komersial), a.* from tm_bbk_pajak a 
                        where a.i_faktur_komersial = '$ifaktur' and a.i_area='$iarea' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatebbk($ifaktur,$iarea)
    {
	    $this->db->query(" update tm_bbk_pajak set n_faktur_komersialprint=n_faktur_komersialprint+1
                				 where i_faktur_komersial = '$ifaktur' and i_area='$iarea'",false);
    }
    function bacadetail($ifaktur)
    {
		$this->db->select(" a.* from tm_bbk_pajak b, tm_bbk_item a
					              inner join tr_product_motif on (a.i_product_motif=tr_product_motif.i_product_motif 
                                                        and a.i_product=tr_product_motif.i_product)
					              where b.i_faktur_komersial = '$ifaktur' and a.i_bbk=b.i_bbk and a.i_bbk_type='03' order by a.i_bbk,a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' or a.i_area='$iarea5')
					order by a.i_spb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5)
    {
		if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
							   or i_area = '$iarea4' or i_area = '$iarea5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function close($iarea,$inota)
    {
		$this->db->query("	update tm_nota set n_faktur_komersialprint=n_faktur_komersialprint+1 
          							where i_nota = '$inota' and i_area = '$iarea' ",false);
    }
}
?>
