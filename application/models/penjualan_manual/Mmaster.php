<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
 function baca($dpm,$isalesman,$iarea)
    {
    $this->db->select(" a.*, b.e_customer_name from tm_penjualan_manual a, tr_customer b
               where a.i_customer=b.i_customer 
               and a.d_pm ='$dpm' and a.i_salesman='$isalesman' and a.i_area='$iarea'", false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      return $query->row();
    }
    }


    function bacacustomer($num,$offset,$iarea)
    {
      $this->db->select(" i_customer, e_customer_name from tr_customer 
                          where i_area='$iarea' order by i_customer",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function caricustomer($num,$offset,$iarea,$cari)
    {
      $this->db->select(" i_customer, e_customer_name 
                          from tr_customer 
                          where i_area='$iarea' and (upper(i_customer) like '%$cari%' 
                          or upper(e_customer_name) like '%$cari%')
                          order by i_customer",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function runningnumber($iarea){
      $query    = $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
      $row      = $query->row();
      $thbl     = $row->c;
      $th       = substr($thbl,0,2);
      $this->db->select("max(substr(i_pm, 9, 6)) as max from tm_penjualan_manual
                         where substr(i_pm, 4, 2)='$th' and i_area='$iarea' and substr(i_pm, 1, 2)='PM'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         foreach($query->result() as $row){
           $terakhir=$row->max;
         }
         $noby  =$terakhir+1;
         settype($noby,"string");
         $a=strlen($noby);
         while($a<6){
           $noby="0".$noby;
           $a=strlen($noby);
         }
         $noby  ="PM-".$thbl."-".$noby;
         return $noby;
      }else{
         $noby  ="000001";
         $noby  ="PM-".$thbl."-".$noby;
         return $noby;
      }
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacasalesman($num,$offset,$iarea)
    {
      $this->db->select(" distinct(i_salesman), e_salesman_name from tr_customer_salesman 
                          where i_area='$iarea' order by i_salesman",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
     function carisalesman($num,$offset,$iarea,$cari)
    {
      $this->db->select(" distinct(i_salesman), e_salesman_name 
                          from tr_customer_salesman 
                          where i_area='$iarea' and (upper(i_salesman) like '%$cari%' 
                          or upper(e_salesman_name) like '%$cari%')
                          order by i_salesman",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function simpanby($iby,$dby,$iarea,$icustomer,$ijenisby,$vby,$remark,$isalesman){
#    echo $dby.'<br>';
    $tmpx=explode('-',$dby);
    $th=$tmpx[2];
    $bl=$tmpx[1];
    $dd=$tmpx[0];
    $dby=$th.'-'.$bl.'-'.$dd;
#    echo $dby.'<br>';
#    die;
    $query  = $this->db->query("SELECT current_timestamp as c");
    $row    = $query->row();
    $dentry = $row->c;

    $query=$this->db->query("insert into tm_penjualan_manual (i_pm, i_area, d_pm,  i_customer, v_pm, d_entry, e_remark, i_salesman) 
                               values ('$iby', '$iarea', '$dby', '$icustomer', '$vby','$dentry', '$remark', '$isalesman')",false);
    }
}
?>
