<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacaperiode($dfrom,$dto,$iarea)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
        $sql =" a.i_spb, a.d_spb, a.i_customer, e.e_customer_name, a.n_spb_toplength, a.i_salesman, f.e_salesman_name,
                h.e_product_classname as product, c.i_product, c.e_product_name, c.v_unit_price, c.n_order, c.n_deliver,
                (c.n_order * c.v_unit_price) as spb_kotor, (d.n_deliver * c.v_unit_price) as nota_kotor,
                a.n_spb_discount1, a.n_spb_discount2, a.n_spb_discount3,  
                b.n_nota_discount1, b.n_nota_discount2, b.n_nota_discount3, 
                round((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100)) as v_spb_disc1, 
                round(((c.n_order * c.v_unit_price) - ((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)) as v_spb_disc2,
                round((((c.n_order * c.v_unit_price)-(c.n_order * c.v_unit_price)*(a.n_spb_discount1/100))-(((c.n_order * c.v_unit_price)-
                ((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)))* a.n_spb_discount3) as v_spb_disc3,

                round((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100)) as v_nota_disc1, 
                round(((c.n_deliver * c.v_unit_price) - ((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)) as v_nota_disc2,
                round((((c.n_deliver * c.v_unit_price)-(c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100))-(((c.n_deliver * c.v_unit_price)-
                ((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)))* a.n_spb_discount3) as v_nota_disc3,
                round(
                (c.n_deliver * c.v_unit_price)
                - ((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100))
                - (((c.n_deliver * c.v_unit_price) - ((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100))
                - ((((c.n_deliver * c.v_unit_price)-(c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100))-(((c.n_deliver * c.v_unit_price)-
                ((c.n_deliver * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)))* a.n_spb_discount3)
                 )as v_nota_netto,
                round(
                (c.n_order * c.v_unit_price)
                - ((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100))
                - (((c.n_order * c.v_unit_price) - ((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100))
                - ((((c.n_order * c.v_unit_price)-(c.n_order * c.v_unit_price)*(a.n_spb_discount1/100))-(((c.n_order * c.v_unit_price)-
                ((c.n_order * c.v_unit_price)*(a.n_spb_discount1/100)))*(a.n_spb_discount2/100)))* a.n_spb_discount3)
                 )as v_spb_netto,
                i.i_supplier, i.e_supplier_name, a.i_price_group, b.i_sj, b.d_sj, b.i_nota, b.d_nota, a.f_spb_stockdaerah, a.f_spb_cancel, a.i_approve1, a.i_approve2, 
                a.i_notapprove, a.i_store, a.f_spb_op, a.f_spb_siapnotagudang, a.f_spb_siapnotasales, a.f_spb_opclose, b.i_sj , b.i_dkb, b.i_nota
                from tm_spb a
                full outer join tm_nota b on(a.i_spb = b.i_spb and a.i_area = b.i_area)
                full outer join tm_spb_item c on(a.i_spb = c.i_spb and a.i_area = c.i_area)
                full outer join tm_nota_item d on(b.i_sj = d.i_sj and b.i_area = d.i_area and c.i_product = d.i_product)
                full outer join tr_customer e on(e.i_customer = a.i_customer)
                full outer join tr_salesman f on(f.i_salesman = a.i_salesman and a.i_area = f.i_area)
                full outer join tr_product g on(c.i_product = g.i_product)
                full outer join tr_product_class h on(g.i_product_class = h.i_product_class)
                full outer join tr_supplier i on(g.i_supplier = i.i_supplier)
                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
                AND a.f_spb_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_spb, c.n_item_no";
								else $sql.=" order by a.i_spb, c.n_item_no";
        
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
