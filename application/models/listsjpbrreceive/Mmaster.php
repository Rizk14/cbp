<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isjpbr, $iarea) 
    {
			$this->db->query("update tm_sjpbr set f_sjpbr_cancel='t' WHERE i_sjpbr='$isjpbr' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($this->session->userdata('level')=='0'){
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
								where a.i_area_to=b.i_area and a.i_sj_type='01'
								and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_sj) like '%$cari%')
								order by a.i_sj desc",false)->limit($num,$offset);
			}else{
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') order by a.i_sj desc",false)->limit($num,$offset);
	/*
			$this->db->select(" 	a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%') and (a.i_area_to='$area1' or a.i_area_to='$area2' or a.i_area_to='$area3' or a.i_area_to='$area4'
						or a.i_area_to='$area5')
						order by a.i_sj desc",false)->limit($num,$offset);
	*/
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
			$this->db->select(" a.*, b.e_area_name from tm_nota a, tr_area b
						where a.i_area_to=b.i_area and a.i_sj_type='01'
						and (upper(a.i_area_to) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						or upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' )
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_customer_name
                          from tm_sjpbr a, tr_customer b
													where (upper(a.i_sjpbr) like '%$cari%') and a.i_area='$iarea' and a.i_customer=b.i_customer and
                          not a.d_sjpbr_receive isnull and
													a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.i_sjpbr",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isjpbr,$iarea)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                        from tm_sjpbr a, tr_customer b, tr_area c, tr_spg d
						            where a.i_customer=b.i_customer and a.i_area=c.i_area and a.i_spg=d.i_spg
						            and a.i_sjpbr ='$isjpbr' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isjpbr, $iarea)
    {
		$this->db->select("a.i_sjpbr,a.d_sjpbr,a.i_area,a.i_product,a.i_product_grade,
                       a.i_product_motif,a.n_quantity_retur,a.n_quantity_receive,a.v_unit_price,
                       a.e_product_name,b.e_product_motifname,a.e_remark 
                       from tm_sjpbr_item a, tr_product_motif b
				               where a.i_sjpbr = '$isjpbr' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatesjheader($isjpbr,$iarea,$dsjreceive,$vsjnetto,$vsjrec)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjupdate= $row->c;
    	$this->db->set(
    		array(
				'v_sjpbr_receive' => $vsjrec,
				'd_sjpbr_receive' => $dsjreceive,
        'd_sjpbr_update'  => $dsjupdate

    		)
    	);
    	$this->db->where('i_sjpbr',$isjpbr);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_sjpbr');
    }
    public function deletesjdetail( $isjpbr, $iarea, $iproduct, $iproductgrade, $iproductmotif, $ndeliver) 
    {
      $cek=$this->db->query("select * from tm_sjpbr_item WHERE i_sjpbr='$isjpbr' 
                          and i_area='$iarea'
										      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										      and i_product_motif='$iproductmotif'");
      if($cek->num_rows()>0)
      {
		    $this->db->query("DELETE FROM tm_sjpbr_item WHERE i_sjpbr='$isjpbr'
                          and i_area='$iarea'
										      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										      and i_product_motif='$iproductmotif'");
      }
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj)
    {
      $queri 		= $this->db->query("SELECT i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'");
		  $row   		= $queri->row();
      $query=$this->db->query(" 
                                DELETE FROM tm_ic_trans 
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation'
                                and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                              ",false);
      if($row->i_trans!=''){
        return $row->i_trans;
      }else{
        return 1;
      }
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }

    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,$vunitprice,$isj,$dsj,$iarea,$eremark,$i)
    {
      $th=substr($dsj,0,4);
      $bl=substr($dsj,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_sjpbr'		          => $isj,
				'i_area'  	          => $iarea,
				'd_sjpbr'		          => $dsj,
				'i_product'       		=> $iproduct,
				'i_product_motif'   	=> $iproductmotif,
				'i_product_grade'   	=> $iproductgrade,
				'e_product_name'    	=> $eproductname,
				'n_quantity_retur'  	=> $nretur,
				'n_quantity_receive'	=> $nreceive,
				'v_unit_price'		    => $vunitprice,
        'e_remark'            => $eremark,
        'e_mutasi_periode'    => $pr,
        'n_item_no'           => $i
    		)
    	);
    	
    	$this->db->insert('tm_sjpbr_item');
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by d_transaction desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$icustomer,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                )
                              ",false);
    }
    function updatemutasi1($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$emutasiperiodesj,$iarea)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi
                                set n_mutasi_daritoko=n_mutasi_daritoko+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$iarea' and i_store_location='PB' and i_store_locationbin='00' and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if($emutasiperiodesj==$emutasiperiode){
        $query=$this->db->query(" 
                                  UPDATE tm_mutasi_consigment
                                  set n_mutasi_git=n_mutasi_git-$qsj
                                  where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                  and i_customer='$icustomer'
                                  and e_mutasi_periode='$emutasiperiodesj'
                                ",false);
      }
    }
    function cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $hasil='kosong';
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$hasil='ada';
			}
      return $hasil;
    }
    function insertmutasi1($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$emutasiperiodesj,$iarea)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                insert into tm_mutasi
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close, n_mutasi_git,n_mutasi_daritoko)
                                values
                                (
 '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,0,0,0,0,$qsj,0,'f',0,$qsj)
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      if( ($q_ak=='')||($q_ak==null) ) $q_ak=0;
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                insert into tm_ic
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore','$istorelocation','$istorelocationbin', '$eproductname', $qsj, 't'
                                )
                              ",false);
    }
    function bacaproduct($num,$offset,$cari)
    {
			$this->db->select("	a.i_product as kode, a.e_product_name as nama, b.v_product_retail as harga, 
                          c.i_product_motif as motif, c.e_product_motifname as namamotif
                          from tr_product a, tr_product_price b, tr_product_motif c
                          where a.i_product=b.i_product and b.i_price_group='00'
                          and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                          and a.i_product=c.i_product ORDER BY a.e_product_name",false)->limit($num,$offset);
                          #and a.i_product_status<>'4'
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updatemutasi04($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$emutasiperiodesj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_daritoko=n_mutasi_daritoko-$qsj, 
                                n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if($emutasiperiodesj==$emutasiperiode){
        $query=$this->db->query(" 
                                  UPDATE tm_mutasi_consigment set n_mutasi_git=n_mutasi_git+$qsj
                                  where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                  and i_customer='$icustomer' and e_mutasi_periode='$emutasiperiodesj'
                                ",false);
      }
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      if( ($qsj=='')||($qsj==null) ) $qsj=0;
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                  			    $vunitprice,$isjpbr,$iarea,$i,$dsjpbr,$nreceive)
    {
      $periode=substr($dsjpbr,0,4).substr($dsjpbr,5,2);
    	$this->db->set(
    		array(
				'i_sjpbr'			    => $isjpbr,
				'i_area'	        => $iarea,
				'i_product'			  => $iproduct,
				'i_product_motif'	=> $iproductmotif,
				'i_product_grade'	=> $iproductgrade,
				'n_quantity_retur'=> $ndeliver,
				'n_quantity_receive' => $nreceive,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
        'd_sjpbr'         => $dsjpbr,
        'e_mutasi_periode'=> $periode,
				'n_item_no'       => $i
    		)
    	);
    	$this->db->insert('tm_sjpbr_item');
    }
}
?>
