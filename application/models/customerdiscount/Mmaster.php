<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomer)
    {
			$this->db->select("i_customer, n_customer_discount1, n_customer_discount2, n_customer_discount3 from tr_customer_discount where i_customer = '$icustomer' ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->row();
		}
    }
    function insert($icustomer, $ncustomerdiscount1, $ncustomerdiscount2,$ncustomerdiscount3)
    {
		if($ncustomerdiscount1=='')
			$ncustomerdiscount1=0.00;
		if($ncustomerdiscount2=='')
			$ncustomerdiscount2=0.00;
		if($ncustomerdiscount3=='')
			$ncustomerdiscount3=0.00;
    	$this->db->set(
    		array(
    			'i_customer' 	  	 => $icustomer,
    			'n_customer_discount1' 	 => $ncustomerdiscount1,
				'n_customer_discount2'   => $ncustomerdiscount2,
				'n_customer_discount3'	 => $ncustomerdiscount3
    		)
    	);
    	
    	$this->db->insert('tr_customer_discount');
		#redirect('customerdiscount/cform/');
    }
    function update($icustomer, $ncustomerdiscount1, $ncustomerdiscount2, $ncustomerdiscount3)
    {
		if($ncustomerdiscount1=='')
			$ncustomerdiscount1=0.00;
		if($ncustomerdiscount2=='')
			$ncustomerdiscount2=0.00;
		if($ncustomerdiscount3=='')
			$ncustomerdiscount3=0.00;
    	$data = array(
               'i_customer' 		=> $icustomer,
               'n_customer_discount1' 	=> $ncustomerdiscount1,
	       'n_customer_discount2' 	=> $ncustomerdiscount2,
	       'n_customer_discount3' 	=> $ncustomerdiscount3,
		        'd_customer_update' => date("Y-m-d H:i:s"),
            );
		$this->db->where('i_customer =', $icustomer);
		$this->db->update('tr_customer_discount', $data); 
		#redirect('customerdiscount/cform/');
    }
	
    public function delete($icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer_discount WHERE i_customer='$icustomer'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("	a.i_customer, a.n_customer_discount1, a.n_customer_discount2, a.n_customer_discount3, b.e_customer_name
											 	from tr_customer_discount a, tr_customer b 
												where upper(a.i_customer) like '%$cari%' 
												and a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_customer, a.n_customer_discount1, a.n_customer_discount2, a.n_customer_discount3, b.e_customer_name
											 	from tr_customer_discount a, tr_customer b 
												where a.i_customer=b.i_customer and upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%' order by a.i_customer", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
