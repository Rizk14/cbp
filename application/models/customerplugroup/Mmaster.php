<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerplugroup,$icustomer)
    {
		$this->db->select("i_customer, e_customer_plugroupname, i_customer_plugroup from tr_customer_plugroup where i_customer = '$icustomer' and i_customer_plugroup = '$icustomerplugroup'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerplugroup, $ecustomerplugroupname, $icustomer)
    {
    	$this->db->set(
    		array(
    			'i_customer_plugroup' => $icustomerplugroup,
    			'e_customer_plugroupname' => $ecustomerplugroupname,
			'i_customer' => $icustomer
    		)
    	);
    	
    	$this->db->insert('tr_customer_plugroup');
		#redirect('customerplugroup/cform/');
    }
    function update($icustomerplugroup, $ecustomerplugroupname, $icustomer)
    {
    	$data = array(
               'i_customer_plugroup' => $icustomerplugroup,
               'e_customer_plugroupname' => $ecustomerplugroupname,
	       'i_customer' => $icustomer
            );
		$this->db->where('i_customer =', $icustomer);
		$this->db->where('i_customer_plugroup =', $icustomerplugroup);
		$this->db->update('tr_customer_plugroup', $data); 
		#redirect('customerplugroup/cform/');
    }
	
    public function delete($icustomerplugroup, $icustomer) 
    {
		$this->db->query("DELETE FROM tr_customer_plugroup WHERE i_customer='$icustomer' and i_customer_plugroup='$icustomerplugroup'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("i_customer, e_customer_plugroupname, i_customer_plugroup from tr_customer_plugroup where upper(i_customer) like '%$cari%' or upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer,i_customer_plugroup", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_customer, e_customer_plugroupname, _customer_plugroup from tr_customer_plugroup where upper(i_customer) like '%$cari%' or upper(e_customer_plugroupname) like '%$cari%' or upper(i_customer_plugroup) like '%$cari%' order by i_customer, i_customer_plugroup", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
