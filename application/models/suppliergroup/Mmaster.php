<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($isuppliergroup)
    {
		$this->db->select("i_supplier_group, e_supplier_groupname, e_supplier_groupnameprint1, e_supplier_groupnameprint2 from tr_supplier_group where i_supplier_group = '$isuppliergroup' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($isuppliergroup, $esuppliergroupname, $esuppliergroupnameprint1,$esuppliergroupnameprint2)
    {
    	$this->db->set(
    		array(
    			'i_supplier_group' 	  	=> $isuppliergroup,
    			'e_supplier_groupname' 	  	=> $esuppliergroupname,
			'e_supplier_groupnameprint1' 	=> $esuppliergroupnameprint1,
			'e_supplier_groupnameprint2'	=> $esuppliergroupnameprint2
    		)
    	);
    	
    	$this->db->insert('tr_supplier_group');
//		redirect('suppliergroup/cform/');
    }
    function update($isuppliergroup, $esuppliergroupname, $esuppliergroupnameprint1, $esuppliergroupnameprint2)
    {
    	$data = array(
               'i_supplier_group' => $isuppliergroup,
               'e_supplier_groupname' => $esuppliergroupname,
	       'e_supplier_groupnameprint1' => $esuppliergroupnameprint1,
	       'e_supplier_groupnameprint2' => $esuppliergroupnameprint2
            );
		$this->db->where('i_supplier_group =', $isuppliergroup);
		$this->db->update('tr_supplier_group', $data); 
//		redirect('suppliergroup/cform/');
    }
	
    public function delete($isuppliergroup) 
    {
		$this->db->query("DELETE FROM tr_supplier_group WHERE i_supplier_group='$isuppliergroup'", false);
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("i_supplier_group, e_supplier_groupname, e_supplier_groupnameprint1, e_supplier_groupnameprint2 from tr_supplier_group where upper(i_supplier_group) like '%$cari%' or upper(e_supplier_groupname) like '%$cari%' order by i_supplier_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_supplier_group, e_supplier_groupname, e_supplier_groupnameprint1, e_supplier_groupnameprint2 from tr_supplier_group where upper(i_supplier_group) like '%$cari%' or upper(e_supplier_groupname) like '%$cari%' order by i_supplier_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
