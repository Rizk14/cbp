<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb,$area) 
    {
		$this->db->query("UPDATE tm_spb set f_spb_cancel='t' WHERE i_spb='$ispb' and i_area='$area'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb desc ";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb desc ";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		}else{
			$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   order by i_area ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		  if ($cari == '') {
			  $this->db->select(" a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , d.e_area_name , a.n_print , a.i_approve1 , a.i_approve2
						from tm_spb a , tr_customer b , tr_product_group c, tr_area d
						where a.i_customer=b.i_customer
						and a.i_product_group=c.i_product_group
						and a.i_area=d.i_area
						and a.f_spb_stockdaerah='true'
						and a.f_spb_cancel='false'
						and a.i_area='$iarea'
						and  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
						                a.d_spb <= to_date('$dto','dd-mm-yyyy')) and a.n_print=0
						group by a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , a.i_area , a.n_print , d.e_area_name
						order by a.i_spb , a.i_area ",false)->limit($num,$offset);
		  }else{
			  $this->db->select("a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , d.e_area_name , a.n_print , a.i_approve1 , a.i_approve2
						from tm_spb a , tr_customer b , tr_product_group c, tr_area d
						where a.i_customer=b.i_customer
						and a.i_product_group=c.i_product_group
						and a.i_area=d.i_area
						and a.f_spb_stockdaerah='true'
						and a.f_spb_cancel='false'
						and a.i_area='$iarea'
						and  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
						                a.d_spb <= to_date('$dto','dd-mm-yyyy')) and a.n_print=0
						and (upper(b.e_customer_name) like '%$cari%' or upper(a.i_spb) like '%$cari%')
						group by a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , a.i_area , a.n_print , d.e_area_name
						order by a.i_spb , a.i_area",false)->limit($num,$offset);
		  }		
	
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ispb,$iarea)
    {
      $this->db->select(" a.e_remark1 AS emark1, a.*, e.e_price_groupname,
           d.e_area_name, b.e_customer_name, b.e_customer_address, c.e_salesman_name, b.f_customer_first
           from tm_spb a
               inner join tr_customer b on (a.i_customer=b.i_customer)
               inner join tr_salesman c on (a.i_salesman=c.i_salesman)
               inner join tr_customer_area d on (a.i_customer=d.i_customer)
               left join tr_price_group e on (a.i_price_group=e.i_price_group)
               where a.i_spb ='$ispb' and a.i_area='$iarea'", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function bacadetail($ispb,$iarea)
    {
      $this->db->select(" a.i_spb,a.i_product,a.i_product_grade,a.i_product_motif,a.n_order,a.n_deliver,a.n_stock,
                                    a.v_unit_price,a.e_product_name,a.i_op,a.i_area,a.e_remark,a.n_item_no, b.e_product_motifname,
                                    a.i_product_status
                        from tm_spb_item a, tr_product_motif b
                            where a.i_spb = '$ispb' and i_area='$iarea' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                            order by a.n_item_no", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacadetailnilaispb($ispb,$iarea)
    {
      return $this->db->query(" select (sum(a.n_deliver * a.v_unit_price)) AS nilaispb from tm_spb_item a
                 where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function bacadetailnilaiorderspb($ispb,$iarea)
    {
      return $this->db->query(" select (sum(a.n_order * a.v_unit_price)) AS nilaiorderspb from tm_spb_item a
                 where a.i_spb = '$ispb' and a.i_area='$iarea' ", false);
    }
    function updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman,
                   $ipricegroup, $dspbreceive, $dspbstorereceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                   $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                   $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
                   $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                   $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold,$eremark1)
    {
      $query      = $this->db->query("SELECT current_timestamp as c");
      $row        = $query->row();
      $dspbupdate = $row->c;
      $data = array( 'd_spb_storereceive'    => $dspbstorereceive,
         'd_spb_update'    => $dspbupdate
            );
       $this->db->where('i_spb', $ispb);
       $this->db->where('i_area', $iarea);
       $this->db->update('tm_spb', $data);
    }
}
?>
