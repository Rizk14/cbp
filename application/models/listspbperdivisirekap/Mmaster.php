<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($dfrom,$dto)
  {
		$this->db->select(" a.i_area, a.e_area_name, a.i_product_group, sum(a.jumlah) as jumlah, sum(a.netto) as netto, sum(a.n_spb) as n_spb from (
                        select a.i_area, b.e_area_name, d.i_product_group, 0 as jumlah, sum(a.v_spb-a.v_spb_discounttotal) as netto, 0 as n_spb
                        from tr_area b, tr_product_group d, tm_spb a
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' 
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_product_group=d.i_product_group
                        group by a.i_area, d.i_product_group, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, d.i_product_group, sum(c.v_unit_price*c.n_order) as jumlah, 0 as netto, sum(c.n_order) 
                        as n_spb
                        from tr_area b, tr_product_group d, tm_spb a, tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_product_group=d.i_product_group
                        group by a.i_area, d.i_product_group, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, 0 as jumlah, sum(a.v_spb-a.v_spb_discounttotal) as netto, 
                        0 as n_spb
                        from tr_area b, tm_spb a
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='t'
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' 
                        group by a.i_area, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, sum(c.v_unit_price*c.n_order) as jumlah, 0 as netto, 
                        sum(c.n_order) as n_spb from tr_area b, tm_spb a, tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='t'
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_spb=c.i_spb and a.i_area=c.i_area
                        group by a.i_area, b.e_area_name
                        ) as a
                        group by a.i_area, a.e_area_name, a.i_product_group
                        order by a.i_product_group, a.i_area, a.e_area_name",false);
/*
		$this->db->select(" a.i_area, a.e_area_name, a.i_product_group, sum(a.jumlah) as jumlah, sum(a.netto) as netto, sum(a.n_spb) as n_spb from (
                        select a.i_area, b.e_area_name, d.i_product_group, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.v_unit_price*c.n_order-a.v_spb_discounttotalafter) as netto, sum(c.n_order) as n_spb
                        from tr_area b, tr_product_group d, tm_spb a, tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' and a.i_spb=c.i_spb and a.i_area=c.i_area
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_product_group=d.i_product_group
                        group by a.i_area, d.i_product_group, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.v_unit_price*c.n_order-a.v_spb_discounttotalafter) as netto,
                        sum(c.n_order) as n_spb
                        from tr_area b, tm_spb a, tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='t'
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_spb=c.i_spb and a.i_area=c.i_area
                        group by a.i_area, b.e_area_name
                        ) as a
                        group by a.i_area, a.e_area_name, a.i_product_group
                        order by a.i_product_group, a.i_area, a.e_area_name",false);
*/
/*
		$this->db->select(" a.i_area, a.e_area_name, a.i_product_group, sum(a.jumlah) as jumlah from (
                        select a.i_area, b.e_area_name, d.i_product_group, sum(a.v_spb) as jumlah
                        from tr_area b, tr_product_group d, tm_spb a
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f'
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_product_group=d.i_product_group
                        group by a.i_area, d.i_product_group, b.e_area_name
                        union all
                        select a.i_area, b.e_area_name, 'PB' as i_product_group, sum(a.v_spb) as jumlah
                        from tr_area b, tm_spb a
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='t'
                        and a.d_spb>='$dfrom' and a.d_spb<='$dto' 
                        group by a.i_area, b.e_area_name
                        ) as a
                        group by a.i_area, a.e_area_name, a.i_product_group
                        order by a.i_product_group, a.i_area, a.e_area_name",false);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($dfrom,$dto)
  {
		$this->db->select(" i_area, e_area_name 
                        from tr_area where f_area_real='t'
                        order by i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($dfrom,$dto)
  {
		$this->db->select(" distinct a.i_product_group, b.e_product_groupname
                        from vspbperdivisi a left join tr_product_group b on(a.i_product_group=b.i_product_group)
                        order by b.e_product_groupname",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacabedding($dfrom,$dto)
  {

		$this->db->select(" a.i_area, sum(a.jumlah) as jumlah, sum(a.n_spb) as n_spb from(
                        select i_area, 0 as jumlah, 0 as n_spb from tr_area where f_area_real='t'
                        union all
                        SELECT b.i_area, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.n_order) as n_spb
                        from tr_area b 
                        left join tm_spb a on(a.i_area=b.i_area), tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' and a.i_product_group='01'
                        and a.i_spb=c.i_spb and a.i_area=c.i_area and a.d_spb>='$dfrom' and a.d_spb<='$dto'
                        group by b.i_area
                        ) as a
                        group by a.i_area
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacanonbedding($dfrom,$dto)
  {

		$this->db->select(" a.i_area, sum(a.jumlah) as jumlah, sum(a.n_spb) as n_spb from(
                        select i_area, 0 as jumlah, 0 as n_spb from tr_area where f_area_real='t'
                        union all
                        SELECT b.i_area, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.n_order) as n_spb
                        from tr_area b 
                        left join tm_spb a on(a.i_area=b.i_area), tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' and a.i_product_group='02'
                        and a.i_spb=c.i_spb and a.i_area=c.i_area and a.d_spb>='$dfrom' and a.d_spb<='$dto'
                        group by b.i_area
                        ) as a
                        group by a.i_area
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacahome($dfrom,$dto)
  {
		$this->db->select(" a.i_area, sum(a.jumlah) as jumlah, sum(a.n_spb) as n_spb from(
                        select i_area, 0 as jumlah, 0 as n_spb from tr_area where f_area_real='t'
                        union all
                        SELECT b.i_area, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.n_order) as n_spb
                        from tr_area b 
                        left join tm_spb a on(a.i_area=b.i_area), tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='f' and a.i_product_group='00'
                        and a.i_spb=c.i_spb and a.i_area=c.i_area and a.d_spb>='$dfrom' and a.d_spb<='$dto'
                        group by b.i_area
                        ) as a
                        group by a.i_area
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacakonsinyasi($dfrom,$dto)
  {
		$this->db->select(" a.i_area, sum(a.jumlah) as jumlah, sum(a.n_spb) as n_spb from(
                        select i_area, 0 as jumlah, 0 as n_spb from tr_area where f_area_real='t'
                        union all
                        SELECT b.i_area, sum(c.v_unit_price*c.n_order) as jumlah, sum(c.n_order) as n_spb
                        from tr_area b 
                        left join tm_spb a on(a.i_area=b.i_area), tm_spb_item c
                        where a.f_spb_cancel='f' and a.i_area=b.i_area and a.f_spb_consigment='t' 
                        and a.i_spb=c.i_spb and a.i_area=c.i_area and a.d_spb>='$dfrom' and a.d_spb<='$dto'
                        group by b.i_area
                        ) as a
                        group by a.i_area
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
