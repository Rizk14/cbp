<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispb,$area) 
    {
		$this->db->query("UPDATE tm_spb set f_spb_cancel='t' WHERE i_spb='$ispb' and i_area='$area'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and a.i_area=c.i_area
					order by a.i_spb desc ";
		}else{
			$sql="	a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
					or a.i_area='$area5') and a.i_area=c.i_area
					order by a.i_spb desc ";
		}
		$this->db->select($sql,FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
   function bacaarea($num,$offset,$iuser) {
      $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    
    function bacaperiode($iarea,$dfrom,$dto)
    {
			$this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                          a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                          a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                          a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                          b.e_customer_name, c.e_area_name, x.e_customer_name as xname
                          from tm_spb a 
													left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area and b.i_customer_status<>'4')
													left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area 
                          and x.i_customer like '%000' and b.i_customer_status<>'4')
													, tr_area c
		                      where 
                            not a.i_approve1 isnull and not a.i_approve2 isnull and a.i_store isnull and
			                      a.i_area=c.i_area and	a.f_spb_cancel='f' and a.f_spb_stockdaerah='t' and a.i_area='$iarea' and a.f_spb_rekap='f' 
                            and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy'))
		                      order by a.i_spb ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumberspmb($thbl){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPM'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospmb
                            where i_modul='SPM'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
		  	$nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPM','00','$asal',1)");
			  return $nospmb;
		  }
    }
    function insertheader($ispmb, $dspmb, $iarea, $eremark)
    {
    	$this->db->set(
    		array(
			'i_spmb'		=> $ispmb,
			'd_spmb'		=> $dspmb,
			'i_area'		=> $iarea,
      'i_approve2'=> 'SYSTEM',
      'd_approve2'=> $dspmb,
#      'i_store'   => 'AA',
#      'i_store_location'=> '01',
      'f_spmb_acc'=> 't',
#			'f_op'			=> 't',
			'n_print'		=> 0,
      'e_remark'  => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_spmb');
    }
    function insertdetail($ispb,$iarea,$ispmb)
    {
      $que=$this->db->query(" 	select a.*, b.v_product_retail from tm_spb_item a, tr_product_price b 
                                where i_spb='$ispb' and i_area='$iarea' and a.i_product=b.i_product and 
                                a.i_product_grade=b.i_product_grade and b.i_price_group='00'");
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $query=$this->db->query(" 	select count(*) as brs from tm_spmb_item where i_spmb='$ispmb'");
          if($query->num_rows()>0){
            $br=$query->row();
            $baris=$br->brs+1;
          }else{
            $baris=1;
          }
          $query=$this->db->query(" 	select a.*, b.v_product_retail from tm_spmb_item a, tr_product_price b
                                      where a.i_spmb='$ispmb' and a.i_product='$row->i_product' and a.i_product=b.i_product and 
                                      a.i_product_grade=b.i_product_grade and b.i_price_group='00' and
                                      a.i_product_grade='$row->i_product_grade' and a.i_product_motif='$row->i_product_motif'");
          if($query->num_rows()>0){
            foreach($query->result() as $xx){
              $rem=$xx->e_remark;
            }
            $this->db->query(" 	update tm_spmb_item set n_order=n_order+$row->n_order, n_acc=n_acc+$row->n_order, n_saldo=n_saldo+$row->n_order, e_remark='$rem - $row->e_remark'  where i_spmb='$ispmb' and i_product='$row->i_product' and i_product_grade='$row->i_product_grade' and i_product_motif='$row->i_product_motif'");
          }else{
/*
            $this->db->query(" 	insert into tm_spmb_item values('$ispmb','$row->i_product','$row->i_product_grade','$row->i_product_motif','$row->e_product_name',$row->n_order,0,$row->v_product_retail,'$row->e_remark',null,'$row->i_area',0,$baris,$row->n_order,$row->n_order)");
*/
            $this->db->set(
            array(
                  'i_spmb'          => $ispmb,
                  'i_product'       => $row->i_product,
                  'i_product_grade' => $row->i_product_grade,
                  'i_product_motif' => $row->i_product_motif,
                  'e_product_name'  => $row->e_product_name,
                  'n_order'         => $row->n_order,
                  'n_stock'         => 0,
                  'v_unit_price'    => $row->v_product_retail,
                  'e_remark'        => $row->e_remark,
                  'i_op'            => null,
                  'i_area'          => $row->i_area,
                  'n_deliver'       => 0,
                  'n_item_no'       => $baris,
                  'n_acc'           => $row->n_order,
                  'n_saldo'         => $row->n_order
                 )
            );
            $this->db->insert('tm_spmb_item');
          }
        }
      }
    }
    function updatespb($ispb,$iarea,$ispmb)
    {
      $this->db->query(" 	update tm_spb set f_spb_rekap='t', i_spmb='$ispmb' where i_spb='$ispb' and i_area='$iarea'");
      $query=$this->db->query("select e_remark from tm_spmb where i_spmb='$ispmb'");
      if($query->num_rows()>0){
        foreach($query->result() as $xx){
          $rem=$xx->e_remark;
        }
	if($rem!=''){
	  $this->db->query("update tm_spmb set e_remark='$rem - $ispb'  where i_spmb='$ispmb'");
	}else{
	  $this->db->query("update tm_spmb set e_remark='$ispb'  where i_spmb='$ispmb'");
	}
      }
    }
}
?>
