<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idt,$iarea,$tgl) 
    {
		$this->db->query("update tm_dt set f_dt_cancel='t' WHERE i_dt='$idt' and i_area='$iarea'",False);
//		$this->db->query('DELETE FROM tm_dt WHERE i_dt=\''.$idt.'\' and i_area=\''.$iarea.'\'');
//		$this->db->query('DELETE FROM tm_dt_item WHERE i_dt=\''.$idt.'\' and i_area=\''.$iarea.'\'');
//		$this->db->query("update tm_op set f_op_close='f' WHERE i_op='$iop' and i_supplier='G0000' ",False);
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dt) like '%$cari%')
							order by a.i_dt desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_dt a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dt) like '%$cari%')
							order by a.i_dt desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select(" i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						              order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	distinct(c.i_dt) as i_pelunasan, a.*, b.e_area_name from tr_area b, tm_dt a
                        left join tm_pelunasan c on(a.i_dt=c.i_dt and a.i_area=c.i_area and a.d_dt=c.d_dt)
							          where a.i_area=b.i_area
							          and (upper(a.i_dt) like '%$cari%')
							          and a.i_area='$iarea'
							          and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
							          AND a.d_dt <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_dt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	distinct(c.i_dt) as i_pelunasan,a.*, b.e_area_name from tr_area b, tm_dt a
                        left join tm_pelunasan c on(a.i_dt=c.i_dt and a.i_area=c.i_area and a.d_dt=c.d_dt)
							          where a.i_area=b.i_area 
							          and (upper(a.i_dt) like '%$cari%')
							          and a.i_area='$iarea'
							          and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') 
							          AND a.d_dt <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_dt ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
