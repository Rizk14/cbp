<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($drrkh,$isalesman,$iarea) 
    {
			$this->db->query("update tm_rrkh set f_rrkh_cancel='t' where d_rrkh='$drrkh' and i_salesman='$isalesman' and i_area='$iarea'");
    }
  
    function bacaperiode($dfrom,$dto,$cari)
    {
		$this->db->select("c.e_area_name, d.e_salesman_name, a.d_rrkh, b.i_customer, f.e_customer_name, 
								e.e_kunjungan_typename, b.f_kunjungan_realisasi, b.e_remark
							from tm_rrkh a, tm_rrkh_item b, tr_area c, tr_salesman d, tr_kunjungan_type e, tr_customer f
							where a.i_area = b.i_area and a.i_salesman = b.i_salesman and a.d_rrkh = b.d_rrkh
							and a.i_area = c.i_area 
							and a.i_salesman = d.i_salesman
							and b.i_kunjungan_type = e.i_kunjungan_type
							and b.i_customer = f.i_customer
							and (a.d_rrkh >= to_date('$dfrom','dd-mm-yyyy') and a.d_rrkh <= to_date('$dto','dd-mm-yyyy'))
							and a.f_rrkh_cancel = 'false'
							order by c.e_area_name, d.e_salesman_name ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
