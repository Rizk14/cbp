<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iarea, $ikum, $tahun)
    {
		$this->db->select("	a.d_kum, a.i_kum, a.e_bank_name, d.e_area_name, a.v_jumlah, a.v_sisa, a.i_area, a.i_salesman, e.e_salesman_name,
		                    a.i_customer, a.i_customer_groupar, c.e_customer_name, a.e_remark
		      from tm_kum a
					left join tr_customer_salesman e on(a.i_customer=e.i_customer and a.i_area=e.i_area and a.i_salesman=e.i_salesman)
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					where a.i_kum='$ikum' and a.i_area='$iarea' and a.n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($iarea,$ikum,$tahun)
    {
		$this->db->select(" i_kum from tm_kum where i_area='$iarea' and i_kum='$ikum' and n_kum_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($ikum,$dkum,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
				    $isalesman,$eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			=> $iarea,
				'i_kum'				=> $ikum,
				'i_customer'		=> $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'		=> $isalesman,
				'd_kum'				=> $dkum,
				'd_entry'			=> $dentry,
				'e_bank_name'		=> $ebankname,
				'e_remark'			=> $eremark,
				'n_kum_year'			=> $tahun,
				'v_jumlah'			=> $vjumlah,
				'v_sisa'			=> $vsisa
    		)
    	);
    	
    	$this->db->insert('tm_kum');
    }
    function update($ikum,$xkum,$dkum,$xtahun,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
				    $isalesman,$eremark,$vjumlah,$vsisa,$iareaasal)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    $this->db->set(
    		array(
				'i_area'						=> $iarea,
				'i_kum'							=> $ikum,
				'i_customer'				=> $icustomer,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman'				=> $isalesman,
				'd_kum'							=> $dkum,
				'd_update'					=> $dentry,
				'e_bank_name'				=> $ebankname,
				'e_remark'					=> $eremark,
				'n_kum_year'				=> $tahun,
				'v_jumlah'					=> $vjumlah,
				'v_sisa'						=> $vsisa,
				'f_kum_cancel'			=> 'f'
    		)
    	);
    	$this->db->where('i_kum',$xkum);
    	$this->db->where('i_area',$iareaasal);
    	$this->db->where('n_kum_year',$xtahun);
    	$this->db->update('tm_kum');
    }
	function bacaarea($num,$offset){
		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacacustomer($iarea,$num,$offset){
		$this->db->select(" a.*, 
					b.i_customer_groupar, 
					c.e_salesman_name, 
					c.i_salesman,
					d.e_customer_setor 

				from tr_customer a 
				left join tr_customer_groupar b on(a.i_customer=b.i_customer) 
				left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area) 
				left join tr_customer_owner d on(a.i_customer=d.i_customer)
				
				where a.i_area='$iarea'
				order by a.i_customer ",FALSE)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function caricustomer($cari,$iarea,$num,$offset){
		$this->db->select(" a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
							left join tr_customer_groupar b on(a.i_customer=b.i_customer)
							left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
							left join tr_customer_owner d on(a.i_customer=d.i_customer)

						   	where a.i_area='$iarea' 
							and (upper(a.i_customer) like '%$cari%' or 
							upper(a.e_customer_name) like '%$cari%' or 
							upper(d.e_customer_setor) like '%$cari%' ) 
							order by a.i_customer ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
}
?>
