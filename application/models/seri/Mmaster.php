<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iseri)
    {
		$this->db->select('i_product_seri, e_product_seriname')->from('tr_product_seri')->where('i_product_seri', $iseri);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iseri,$e_product_seriname)
    {
    	$this->db->set(
    		array(
    			'i_product_seri' => $iseri,
    			'e_product_seriname' => $e_product_seriname
    		)
    	);
    	
    	$this->db->insert('tr_product_seri');
		redirect('seri/cform/');
    }
    function update($iseri,$e_product_seriname)
    {
    	$data = array(
               'i_product_seri' => $iseri,
               'e_product_seriname' => $e_product_seriname
            );
		$this->db->where('i_product_seri', $iseri);
		$this->db->update('tr_product_seri', $data); 
		redirect('seri/cform/');
    }
	
    public function delete($iseri) 
    {
		$this->db->query('DELETE FROM tr_product_seri WHERE i_product_seri=\''.$iseri.'\'');
		return TRUE;
    }
    function bacasemua($num,$offset)
    {
		$this->db->select('i_product_seri, e_product_seriname from tr_product_seri order by i_product_seri', false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
        $this->db->select("i_product_seri, e_product_seriname from tr_product_seri where upper(i_product_seri) like '%$cari%' or upper(e_product_seriname) like '%$cari%' order by i_product_seri",FALSE)->limit($num,$offset);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
            return $query->result();
        }
    }
}
?>
