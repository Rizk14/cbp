<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
			$this->db->query("update tm_nota set f_nota_cancel='t', d_nota_update='$dentry', d_sj_update='$dentry' where i_nota='$inota' and i_area='$iarea'");
			$this->db->query("update tm_spb set f_spb_cancel='t', d_spb_update='$dentry' where i_spb='$ispb' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_nota_cancel='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
#   						and a.f_nota_koreksi='f'
#             and a.f_ttb_tolak='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
 							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
#             and a.f_nota_koreksi='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
#              and a.f_nota_koreksi='f'
#             and a.f_ttb_tolak='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacanota($inota,$ispb,$area)
    {
		$this->db->select(" tm_nota.i_nota, tm_nota.d_nota, tm_nota.i_customer, tm_nota.i_salesman, tm_nota.i_area,
							tm_spb.n_spb_toplength, tm_nota.e_remark, tm_nota.f_cicil, tm_nota.i_nota_old, tm_nota.v_nota_netto,
							tm_nota.v_nota_discount1, tm_nota.v_nota_discount2, tm_nota.v_nota_discount3, tm_nota.v_nota_discount4,
							tm_nota.n_nota_discount1, tm_nota.n_nota_discount2, tm_nota.n_nota_discount3, tm_nota.n_nota_discount4,
							tm_nota.v_nota_gross, tm_nota.v_nota_discounttotal, tm_nota.n_price, tm_nota.v_nota_ppn, tm_nota.f_cicil,
              tm_nota.i_dkb, tm_nota.d_dkb, tm_nota.i_bapb, tm_nota.d_bapb, tm_nota.v_sisa, tm_nota.e_alasan, 
							tm_nota.i_spb, tm_spb.i_spb_old, tm_nota.d_spb, tm_spb.v_spb, tm_spb.f_spb_consigment, tm_spb.i_spb_po,
							tm_spb.v_spb_discounttotal, tm_spb.f_spb_plusppn, tm_spb.f_spb_plusdiscount, tm_nota.i_sj, tm_nota.d_sj,
							tr_customer.e_customer_name, tm_nota.f_masalah, tm_nota.f_insentif,
							tr_customer_area.e_area_name, tm_spb.i_price_group, tr_price_group.e_price_groupname,
							tr_salesman.e_salesman_name, k.n_toleransi_pusat, k.n_toleransi_cabang
				   from tm_nota 
				   left join tm_spb on (tm_nota.i_spb=tm_spb.i_spb and tm_nota.i_area=tm_spb.i_area and tm_spb.i_spb = '$ispb')
           inner join tr_price_group on(tm_spb.i_price_group=tr_price_group.i_price_group)
				   left join tm_promo on (tm_nota.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
           left join tr_city k on (tr_customer.i_city=k.i_city and tr_customer.i_area=k.i_area)
				   where tm_nota.i_nota = '$inota' and tm_nota.i_area='$area'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetailnota($inota,$area)
    {
		$this->db->select("a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname, a.v_unit_price, a.n_deliver
                       from tm_nota_item a
					             inner join tr_product_motif b on (b.i_product_motif=a.i_product_motif
												             and b.i_product=a.i_product)
					             where a.i_nota = '$inota' and a.i_area = '$area'  
					             order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function updatenota($isj,$iarea,$eremark,$fmasalah,$finsentif,$isalesman,$ispb){
    if($eremark=='')$eremark=null;
    $query 	= $this->db->query("SELECT current_timestamp as c");
	  $row   	= $query->row();
	  $dentry	= $row->c;
		$data = array(
          'f_masalah'       => $fmasalah,
					'f_insentif'			=> $finsentif,
          'e_remark'        => $eremark,
          'i_salesman'      => $isalesman,
          'd_nota_update'   => $dentry
					 );
  	$this->db->where('i_sj', $isj);
  	$this->db->where('i_area', $iarea);
		$this->db->update('tm_nota', $data);

		$data = array(
          'i_salesman'     => $isalesman
					 );
  	$this->db->where('i_spb', $ispb);
  	$this->db->where('i_area', $iarea);
		$this->db->update('tm_spb', $data);
  }
  function bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
      $query = $this->db->select("distinct i_salesman, e_salesman_name from tr_customer_salesman
                                  where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
                                  and i_area='$iarea' order by i_salesman",false)->limit($num,$offset);
#and e_periode='$per'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
