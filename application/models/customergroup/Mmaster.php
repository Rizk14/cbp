<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomergroup)
    {
		$this->db->select('i_customer_group, e_customer_groupname')->from('tr_customer_group')->where('i_customer_group', $icustomergroup);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomergroup, $ecustomergroupname)
    {
    	$this->db->set(
    		array(
    			'i_customer_group' => $icustomergroup,
    			'e_customer_groupname' => $ecustomergroupname
    		)
    	);
    	
    	$this->db->insert('tr_customer_group');
		#redirect('customergroup/cform/');
    }
    function update($icustomergroup, $ecustomergroupname)
    {
    	$data = array(
               'i_customer_group' => $icustomergroup,
               'e_customer_groupname' => $ecustomergroupname
            );
		$this->db->where('i_customer_group', $icustomergroup);
		$this->db->update('tr_customer_group', $data); 
		#redirect('customergroup/cform/');
    }
	
    public function delete($icustomergroup) 
    {
		$this->db->query('DELETE FROM tr_customer_group WHERE i_customer_group=\''.$icustomergroup.'\'');
		return TRUE;
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group where upper(i_customer_group) like '%$cari%' or upper(e_customer_groupname) like '%$cari%' order by i_customer_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_customer_group, e_customer_groupname from tr_customer_group where upper(i_customer_group) ilike '%$cari%' or upper(e_customer_groupname) ilike '%$cari%' order by i_customer_group", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
