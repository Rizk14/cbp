<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_ap WHERE i_ap=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_ap_item WHERE i_ap=\''.$iop.'\'');
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" * from tm_do
								inner join tr_supplier on (tm_do.i_supplier=tr_supplier.i_supplier)
								inner join tr_area on (tm_do.i_area=tr_area.i_area)
								
								order by tm_do.i_do desc",false)->limit($num,$offset);

			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($ido,$area)
    {
			$this->db->select(" * from tm_do
								inner join tr_supplier on (tm_do.i_supplier=tr_supplier.i_supplier)
								inner join tr_area on (tm_do.i_area=tr_area.i_area)
								where tm_do.i_do = '$ido' and tm_do.i_area='$area'
								order by tm_do.i_do desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetail($ido,$area)
    {
			$this->db->select(" * from tm_do_item
								inner join tr_product_motif on (tm_do_item.i_product_motif=tr_product_motif.i_product_motif
								and tm_do_item.i_product=tr_product_motif.i_product)
								where tm_do_item.n_deliver>0 and tm_do_item.i_do = '$ido' order by tm_do_item.i_do desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
      function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.i_do, a.i_supplier, a.i_area, c.e_supplier_name, a.i_supplier from tm_do a
										              left join tr_supplier c on (a.i_supplier=c.i_supplier)
										              where 
										              upper(a.i_supplier) like '%$cari%'
										              or upper(a.i_do) like '%$cari%'",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
