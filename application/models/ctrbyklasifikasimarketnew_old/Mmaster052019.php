<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom,$dto,$group)
    {
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $dtoprev=$hr."-".$bl."-".$th;
        if ($group=="NA") {
            $this->db->select(" a.i_customer_class, a.e_customer_classname,sum(ob) as ob, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , 
                            sum(qnotaprev) as qnotaprev from (select * from f_sales_report_klasifikasi_new('$dfrom','$dto','$dfromprev','$dtoprev')
                            group by grup , i_customer_class, e_customer_classname, i_product_group,ob, oa, vnota, qnota, oaprev, qnotaprev, vnotaprev
                            order by grup) as a 
                            group by a.i_customer_class, a.e_customer_classname
                            order by a.e_customer_classname",false);            
        }else{
	        $this->db->select(" * from f_sales_report_klasifikasi_new('$dfrom','$dto','$dfromprev','$dtoprev') where i_product_group = '$group' order by e_customer_classname",false);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          return $query->result();
        }

    }
    function bacagroup($th,$prevth,$todate,$prevdate)
    {
        $this->db->select("a.group from (select * from f_sales_report_klasifikasi('$th','$prevth','$todate','$prevdate') 
                           order by a.grup) as a 
                           group by a.grup  
                           order by a.grup",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
     function bacaclass($dfrom,$dto)
    {
        $tmp=explode("-",$dfrom);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $thnow=$tmp[2];
        $dfromprev=$hr."-".$bl."-".$th;
      
        $tmp=explode("-",$dto);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2]-1;
        $dtoprev=$hr."-".$bl."-".$th;
        $this->db->select("a.e_customer_classname from (select * from f_sales_report_klasifikasi_new('$dfrom','$dto','$dfromprev','$dtoprev') 
                            order by grup) as a 
                            group by a.e_customer_classname
                            order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacanas($th,$prevth,$todate,$prevdate)
    {
        $this->db->select(" a.i_customer_class, a.e_customer_classname, sum(oa) as oa , sum(a.vnota)  as vnota, sum(qnota) as qnota ,sum(oaprev) as oaprev, sum(vnotaprev) as vnotaprev , 
                            sum(qnotaprev) as qnotaprev from (select * from f_sales_report_klasifikasi('$th','$prevth','$todate','$prevdate')
                            group by grup , i_customer_class, e_customer_classname,i_product_group,ob,oa,vnota,qnota,oaprev,vnotaprev,qnotaprev
                            order by grup) as a 
                            group by a.i_customer_class, a.e_customer_classname
                            order by a.e_customer_classname",false);
          $query = $this->db->get();
          if ($query->num_rows() > 0){
              return $query->result();
          }
    }
    function bacaproductgroup()
    {
      $this->db->select(" * from tr_product_group",false);
    
      $query = $this->db->get();
    
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
