<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacaakhir($iarea,$dfrom,$dto,$cari, $num,$offset)
    {
		  $this->db->select(" a.d_sj, a.i_sj from tm_nota a, tr_customer b, tr_area c, tm_spb d
                          where a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%')
                          and a.i_spb=d.i_spb and a.i_area=d.i_area and a.i_approve_pajak isnull
                          and not a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f'
                          and a.i_area='$iarea' and not a.i_seri_pajak isnull
                          and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy')
                          and a.d_sj <= to_date('$dto', 'dd-mm-yyyy'))
            							order by a.d_sj, a.i_sj ",false)->limit($num,$offset);
		  $query = $this->db->get();

		  if ($query->num_rows() > 0){
			  return $query->result();
		  } 
    }
    function bacasemua($iarea,$dfrom,$dto,$cari, $num,$offset)
    {		
		  $this->db->select(" a.i_nota, a.d_nota, a.d_sj, a.d_spb, a.i_sj, a.i_spb, d.i_spb_old, a.i_area,
                          d.f_spb_stockdaerah, b.e_customer_name, c.e_area_name, a.i_faktur_komersial, a.i_seri_pajak, a.d_pajak
                          from tm_nota a, tr_customer b, tr_area c, tm_spb d
                          where a.i_customer=b.i_customer and a.i_area=c.i_area and a.i_approve_pajak isnull
                          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                          or upper(a.i_faktur_komersial) like '%$cari%' or upper(a.i_nota) like '%$cari%')
                          and a.i_spb=d.i_spb and a.i_area=d.i_area
                          and not a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                          and not a.i_dkb isnull and not a.i_seri_pajak isnull and a.i_area='$iarea'
                          and (a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') 
                          and a.d_nota <= to_date('$dto', 'dd-mm-yyyy'))
            							order by a.d_nota, a.i_nota ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  } 
    }
    
	function cari($iarea,$dfrom,$dto,$cari,$num,$offset)
    {
		$this->db->select(" a.i_spb, a.i_salesman, a.i_customer, a.i_pricegroup, a.i_nota, a.i_store, a.i_store_location, a.d_spb, a.d_nota, a.d_spbentry, a.d_sj, a.d_spb_receive, a.f_spb_op, a.f_spb_pkp, a.f_spb_plusdiscount, a.f_spb_stockdaerah, a.f_spb_program, a.f_spb_consigment, a.f_spb_valid, a.f_spb_siapnotagudang, a.f_spb_cancel, a.n_spb_discount1, a.n_spb_discount2, a.n_spb_discount3, a.v_spb, a.v_spb_after, a.i_approve1, a.i_approve2, a.d_approve1, a.d_approve2, a.i_area, a.f_siapnotasales, a.i_spb_old, a.i_product_group, a.f_spb_opclose, a.f_spb_pemenuhan, a.i_cek, a.d_cek, a.e_remark1, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c, tm_nota d
				where a.i_customer=b.i_customer and a.i_area=c.i_area
        and a.i_spb=d.i_spb and a.i_area=d.i_area_to and d.i_sj_type='04'
				and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
				or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
				and a.i_nota isnull 
				and not a.i_sj isnull
				and not a.i_store isnull 
				and a.f_spb_cancel = 'f' 
            and not d.i_dkb isnull
				and ((
					not a.i_approve1 isnull 
					and not a.i_approve2 isnull 
					and a.f_spb_siapnotagudang = 't'
					and a.f_spb_siapnotasales = 't' 
					and a.f_spb_stockdaerah = 'f'
        				and d.f_sj_daerah = 'f')
					or
					(a.f_spb_stockdaerah = 't'
        				and d.f_sj_daerah = 't'))
				
				and a.i_area='$iarea' 
				and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
				a.d_spb <= to_date('$dto','dd-mm-yyyy'))

				order by a.i_spb",FALSE)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

	function bacanota($inota,$ispb,$area)
    {
		$this->db->select(" tm_nota.i_nota, tm_nota.d_nota, tm_nota.i_customer, tm_nota.i_salesman, tm_nota.i_area,
							tm_spb.n_spb_toplength, tm_nota.e_remark, tm_nota.f_cicil, tm_nota.i_nota_old, tm_nota.v_nota_netto,
							tm_nota.v_nota_discount1, tm_nota.v_nota_discount2, tm_nota.v_nota_discount3, tm_nota.v_nota_discount4,
							tm_nota.n_nota_discount1, tm_nota.n_nota_discount2, tm_nota.n_nota_discount3, tm_nota.n_nota_discount4,
							tm_nota.v_nota_gross, tm_nota.v_nota_discounttotal, tm_nota.n_price, tm_nota.v_nota_ppn, tm_nota.f_cicil,
              tm_nota.i_dkb, tm_nota.d_dkb, tm_nota.i_bapb, tm_nota.d_bapb, tm_nota.v_sisa, tm_nota.e_alasan, 
							tm_nota.i_spb, tm_spb.i_spb_old, tm_nota.d_spb, tm_spb.v_spb, tm_spb.f_spb_consigment, tm_spb.i_spb_po,
							tm_spb.v_spb_discounttotal, tm_spb.f_spb_plusppn, tm_spb.f_spb_plusdiscount, tm_nota.i_sj, tm_nota.d_sj,
							tr_customer.e_customer_name, tm_nota.f_masalah, tm_nota.f_insentif,
							tr_customer_area.e_area_name, tm_spb.i_price_group, tr_price_group.e_price_groupname,
							tr_salesman.e_salesman_name, k.n_toleransi_pusat, k.n_toleransi_cabang
				   from tm_nota 
				   left join tm_spb on (tm_nota.i_spb=tm_spb.i_spb and tm_nota.i_area=tm_spb.i_area and tm_spb.i_spb = '$ispb')
           inner join tr_price_group on(tm_spb.i_price_group=tr_price_group.i_price_group)
				   left join tm_promo on (tm_nota.i_spb_program=tm_promo.i_promo)
				   inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
				   inner join tr_salesman on (tm_nota.i_salesman=tr_salesman.i_salesman)
				   inner join tr_customer_area on (tm_nota.i_customer=tr_customer_area.i_customer)
           left join tr_city k on (tr_customer.i_city=k.i_city and tr_customer.i_area=k.i_area)
				   where tm_nota.i_nota = '$inota' and tm_nota.i_area='$area'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetailnota($inota,$area)
    {
		$this->db->select("a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname, a.v_unit_price, a.n_deliver
                       from tm_nota_item a
					             inner join tr_product_motif b on (b.i_product_motif=a.i_product_motif
												             and b.i_product=a.i_product)
					             where a.i_nota = '$inota' and a.i_area = '$area'  
					             order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
	function baca($isj,$iarea)
    {
		$this->db->select(" a.i_spb, a.d_nota, a.d_sj, a.d_spb, a.i_spb, a.i_area, a.i_customer,
                        a.v_nota_discounttotal, a.i_salesman, a.i_sj, a.i_nota, a.v_nota_discounttotal, 
                        a.v_nota_netto, 
                        j.i_spb_program, j.i_spb_old, j.i_spb_po, j.f_spb_consigment, j.n_spb_toplength, j.v_spb,
                        j.v_spb_discounttotal, j.i_price_group, j.n_spb_discount1,
                        j.n_spb_discount2, j.n_spb_discount3, j.n_spb_discount4, j.v_spb_discount1,
                        j.v_spb_discount2, j.v_spb_discount3, j.v_spb_discount4, j.f_spb_plusppn, 
                        j.f_spb_plusdiscount, j.f_spb_pkp, j.e_customer_pkpnpwp, 
                        e.e_promo_name, 
                        f.e_customer_name, f.f_customer_cicil,
                        g.e_salesman_name, 
                        h.e_area_name,
                        i.e_price_groupname,
                        k.n_toleransi_pusat, k.n_toleransi_cabang
                        from tm_nota a 
                        inner join tm_spb j on (a.i_spb=j.i_spb and a.i_area=j.i_area) 
                        left join tm_promo e on (j.i_spb_program=e.i_promo) 
                        inner join tr_customer f on (a.i_customer=f.i_customer)
                        inner join tr_salesman g on (a.i_salesman=g.i_salesman) 
                        inner join tr_customer_area h on (a.i_customer=h.i_customer) 
                        left join tr_price_group i on (j.i_price_group=i.i_price_group)
                        left join tr_city k on (f.i_city=k.i_city and f.i_area=k.i_area)
              			    where a.i_sj = '$isj' and a.i_area = '$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
	
	function bacadetail($isj,$iarea)
    {
		$this->db->select(" a.i_product, a.e_product_name, a.v_unit_price, c.n_deliver, c.n_order, 
                        a.i_product_motif, b.e_product_motifname
                        from tm_nota_item a, tr_product_motif b, tm_spb_item c, tm_spb d, tm_nota e
                        where b.i_product_motif=a.i_product_motif and b.i_product=a.i_product
                        and a.i_sj=e.i_sj and a.i_area=e.i_area
                        and e.i_spb=d.i_spb and e.i_area=d.i_area
                        and d.i_spb=c.i_spb and d.i_area=c.i_area
                        and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif 
                        and a.i_product_grade=c.i_product_grade and a.n_deliver>0
					              and a.i_sj = '$isj' and a.i_area='$iarea' and a.n_deliver>0
					              order by a.n_item_no", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	
	function updatespb($ispb,$iarea,$inota,$dnota,$vspbdiscounttotalafter,$vspbafter)
    {
      $data = array(
		                'i_nota'					        => $inota,
		                'd_nota'					        => $dnota,
		                'v_spb_discounttotalafter'=> $vspbdiscounttotalafter, 
		                'v_spb_after'				      => $vspbafter
		               );
      $this->db->where('i_spb', $ispb);
      $this->db->where('i_area', $iarea);
      $this->db->update('tm_spb', $data); 
    }
	
	function updatenota($isj,$iarea,$eremarkpajak,$dapprovepajak)
    {
#    	$query 	= $this->db->query("SELECT current_timestamp as c");
#		  $row   	= $query->row();
#		  $dentry	= $row->c;
      $user=$this->session->userdata('user_id');
    	$this->db->set(
    		array(
			'd_approve_pajak'     => $dapprovepajak,
			'e_approve_pajak'			=> $eremarkpajak,
			'i_approve_pajak' 		=> $user
    		)
    	);
      $this->db->where('i_sj',$isj);
      $this->db->where('i_area',$iarea);
    	$this->db->update('tm_nota');
    }
  function updatenotabaru($isj,$iarea,$inota,$dnota,$eremark,$inotaold,$djatuhtempo,$nnotatoplength,$nprice,$vspbdiscounttotalafter,$vspbafter){
    if($eremark='')$eremark=null;
    $query 	= $this->db->query("SELECT current_timestamp as c");
	  $row   	= $query->row();
	  $dentry	= $row->c;
		$data = array(
          'n_price'         => $nprice,
					'i_nota'					=> $inota,
					'd_nota'					=> $dnota,
					'i_nota_old'			=> $inotaold,
          'e_remark'        => $eremark,
          'd_jatuh_tempo'   => $djatuhtempo,
          'n_nota_toplength'=> $nnotatoplength,
          'v_nota_discount' => $vspbdiscounttotalafter,
          'v_nota_netto'    => $vspbafter,
          'v_sisa'          => $vspbafter,
          'd_nota_entry'    => $dentry
					 );
  	$this->db->where('i_sj', $isj);
  	$this->db->where('i_area', $iarea);
		$this->db->update('tm_nota', $data); 
		$data = array(
					'i_nota'					=> $inota,
					'd_nota'					=> $dnota
					 );
  	$this->db->where('i_sj', $isj);
  	$this->db->where('i_area', $iarea);
 		$this->db->update('tm_nota_item', $data); 
  }
	function runningnumber($iarea,$thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='FP'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nonota  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nonota
                            where i_modul='FP'
                            and substr(e_periode,1,4)='$th' 
                            and i_area='$iarea'", false);
			  settype($nonota,"string");
			  $a=strlen($nonota);
			  while($a<5){
			    $nonota="0".$nonota;
			    $a=strlen($nonota);
			  }
			  $nonota  ="FP-".$thbl."-".$iarea.$nonota;
			  return $nonota;
		  }else{
			  $nonota  ="00001";
			  $nonota  ="FP-".$thbl."-".$iarea.$nonota;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('FP','$iarea','$asal',1)");
			  return $nonota;
		  }
  }
	function insertheader($inota,$ispb,$iarea,$icustomer,$isalesman,$ispbprogram,$ispbpo,$dspb,$dnota,
				  $djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,$nnotatoplength,$nnotadiscount1,
				  $nnotadiscount2,$nnotadiscount3,$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,
				  $vnotadiscounttotal,$vnotanetto,$vsisa,$vspbdiscounttotal,$vspb,$fspbplusppn,
				  $fspbplusdiscount,$nprice,$vnotappn,$vnotagross,$vnotadiscount,$nnotadiscount4,
				  $vnotadiscount4,$fcicil,$inotaold,$isj,$dsj)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_nota'        			=> $inota,
			'i_spb'       				=> $ispb,
			'i_area'        			=> $iarea,
			'i_customer'      		=> $icustomer,
			'i_salesman'       		=> $isalesman,
 			'i_spb_program'   		=> $ispbprogram,
			'i_spb_po'		      	=> $ispbpo,
			'd_spb'       				=> $dspb,
			'd_nota'        			=> $dnota,
			'd_jatuh_tempo'   		=> $djatuhtempo,
			'e_remark'			      => $eremark,
			'f_masalah'     			=> $fmasalah,
 			'f_insentif'		      => $finsentif,
 			'f_lunas'       			=> $flunas,
 			'f_cicil'			        => $fcicil,
			'n_nota_toplength'	  => $nnotatoplength,
			'n_nota_discount1'	  => $nnotadiscount1,
			'n_nota_discount2'	  => $nnotadiscount2,
			'n_nota_discount3'	  => $nnotadiscount3,
			'n_nota_discount4'	  => $nnotadiscount4,
			'v_nota_discount1'	  => $vnotadiscount1,
			'v_nota_discount2'	  => $vnotadiscount2,
			'v_nota_discount3'	  => $vnotadiscount3,
			'v_nota_discount4'	  => $vnotadiscount4,
			'v_nota_discount'	    => $vnotadiscount,
			'v_nota_discounttotal'=> $vnotadiscounttotal,
			'v_nota_netto'    		=> $vnotanetto,
			'v_nota_gross'	    	=> $vnotagross,
			'v_nota_ppn'      		=> $vnotappn,
			'n_price'       			=> $nprice,
			'f_plus_ppn'      		=> $fspbplusppn,
			'f_plus_discount'   	=> $fspbplusdiscount,
			'v_sisa'        			=> $vsisa,
			'd_nota_entry'    		=> $dentry,
			'f_nota_cancel'   		=> 'f',
			'i_sj'	        			=> $isj,
			'd_sj'        				=> $dsj,
			'i_nota_old'      		=> $inotaold
    		)
    	);
    	$this->db->insert('tm_nota');

    	$this->db->set(
  		array(
      			'f_customer_first'		=> 'f'
        		)
    	);
      $this->db->where('i_customer',$icustomer);      
    	$this->db->update('tr_customer');

    }
    
	function insertdetail($inota,$iarea,$iproduct,$iproductgrade,$eproductname,$ndeliver,$vunitprice,$iproductmotif,$dnota)
    {
    	$this->db->set(
    		array(
					'i_nota'			=> $inota,
					'd_nota'			=> $dnota,
					'i_area'			=> $iarea,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_deliver'			=> $ndeliver,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname
    		)
    	);
    	
    	$this->db->insert('tm_nota_item');
    }
	function updateheader($inota,$iarea,$ispb,$icustomer,$isalesman,$ipricegroup,$ispbprogram,$ispbpo,$dspb,$dnota,
						  $djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,$nnotatoplength,$nnotadiscount1,
						  $nnotadiscount2,$nnotadiscount3,$nnotadiscount4,$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,
						  $vnotadiscount4,$vnotadiscounttotal,$vnota,$vsisa,$vnotagross,$fcicil)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
			'f_masalah'				=> $fmasalah,
  			'f_insentif'			=> $finsentif,
  			'f_lunas'				=> $flunas,
  			'f_cicil'				=> $fcicil,
			'd_nota'				=> $dnota,
			'e_remark'				=> $eremark,
			'n_nota_discount1'		=> $nnotadiscount1,
			'n_nota_discount2'		=> $nnotadiscount2,
			'n_nota_discount3'		=> $nnotadiscount3,
			'n_nota_discount4'		=> $nnotadiscount4,
			'v_nota_discount1'		=> $vnotadiscount1,
			'v_nota_discount2'		=> $vnotadiscount2,
			'v_nota_discount3'		=> $vnotadiscount3,
			'v_nota_discount4'		=> $vnotadiscount4,
			'v_nota_discounttotal'	=> $vnotadiscounttotal,
			'v_nota_discount'		=> $vnotadiscounttotal,
			'v_nota_netto'			=> $vnota,
			'v_sisa'				=> $vsisa,
			'v_nota_gross'			=> $vnotagross,
			'd_nota_update'			=> $dentry
    		)
    	);
		$this->db->where('i_nota',$inota);
		$this->db->where('i_area',$iarea);
    	$this->db->update('tm_nota');
    }
	function updatedetail($inota,$iarea,$iproduct,$iproductgrade,$eproductname,$ndeliver,$vunitprice,$iproductmotif)
    {
    	$this->db->set(
    		array(
					'i_nota'			=> $inota,
					'i_product'			=> $iproduct,
					'i_product_grade'	=> $iproductgrade,
					'i_product_motif'	=> $iproductmotif,
					'n_deliver'			=> $ndeliver,
					'v_unit_price'		=> $vunitprice,
					'e_product_name'	=> $eproductname
    		)
    	);
		$this->db->where('i_nota',$inota);
		$this->db->where('i_area',$iarea);
		$this->db->where('i_product',$iproduct);
		$this->db->where('i_product_grade',$iproductgrade);
		$this->db->where('i_product_motif',$iproductmotif);
    	$this->db->update('tm_nota_item');
    }
    function delete($inota,$ispb,$area) 
    {
		  $this->db->query("update tm_spb set i_nota = null,
						    d_nota = null,
						    v_spb_discounttotalafter = null,
						    v_spb_after = null
						    WHERE i_spb='$ispb' and i_area='$area'");
		  $this->db->query("delete from tm_nota where i_nota='$inota' and i_area='$area'");
		  $this->db->query("delete from tm_nota_item where i_nota='$inota' and i_area='$area'");
		  return TRUE;
    }
    function bacapricegroup($cari,$num,$offset)
    {
			$this->db->select("	* from tr_price_group
								where upper(i_price_group) like '%$cari%' or upper(e_price_groupname) like '%$cari%'
								order by i_price_group", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
	  function insertheaderkoreksi($inota,$ispb,$iarea,$icustomer,$isalesman,$ispbprogram,$ispbpo,$dspb,$dnota,
				    $djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,$nnotatoplength,$nnotadiscount1,
				    $nnotadiscount2,$nnotadiscount3,$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,
				    $vnotadiscounttotal,$vnotanetto,$vsisa,$vspbdiscounttotal,$vspb,$fspbplusppn,
				    $fspbplusdiscount,$nprice,$vnotappn,$vnotagross,$vnotadiscount,$nnotadiscount4,
				    $vnotadiscount4,$fcicil,$inotaold,$isj,$dsj,$idkb,$ibapb,$ddkb,$dbapb)
    {
      $query 	= $this->db->query("SELECT i_nota from tm_notakoreksi where i_nota='$inota'");
  	  if($query->num_rows()==0){
        $this->db->query("insert into tm_notakoreksi select * from tm_nota where i_nota='$inota'");
		    $this->db->query("update tm_nota set f_nota_koreksi='t' where i_nota='$inota'");
        $this->db->query("insert into tm_notakoreksi_item select * from tm_nota_item where i_nota='$inota'");
      }

      $rr=$this->db->query("select a.*, c.f_spb_consigment as kons from tm_nota_item a, tm_nota b, tm_spb c
                            where a.i_nota='$inota' and a.i_sj=b.i_sj and a.i_area=b.i_area and b.i_area=c.i_area and b.i_spb=c.i_spb");
  	  if($rr->num_rows()>0){
        foreach($rr->result() as $row){
          $dsj=$row->d_nota;
		      $th=substr($dsj,0,4);
		      $bl=substr($dsj,5,2);
		      $emutasiperiode=$th.$bl;
          $isj=$row->i_sj;
          $area=substr($isj,8,2);
          if($area=='00'){
            $istore='AA';
            $istorelocation='01';
          }else{
            $istore=$area;
            if($row->kons=='t'){
              $istorelocation='PB';
            }else{
              $istorelocation='00';
            }
          }
          $istorelocationbin='00';
		      $this->mmaster->deletetrans($row->i_product,$row->i_product_grade,$row->i_product_motif,$istore,$istorelocation,$istorelocationbin,$isj,$row->n_deliver,$row->e_product_name);
          if( ($row->n_deliver!='') && ($row->n_deliver!=0) ){
	          $this->mmaster->updatemutasi04($row->i_product,$row->i_product_grade,$row->i_product_motif,$istore,$istorelocation,$istorelocationbin,$row->n_deliver,$emutasiperiode);
	          $this->mmaster->updateic04($row->i_product,$row->i_product_grade,$row->i_product_motif,$istore,$istorelocation,$istorelocationbin,$row->n_deliver);
          }
        }
      }
  
      $this->db->query("delete from tm_nota_item where i_nota='$inota'");
  }
	function insertdetailkoreksi($isj,$inota,$iarea,$iproduct,$iproductgrade,$eproductname,$ndeliver,$vunitprice,$iproductmotif,$dnota,$i)
  {
  	$this->db->set(
  		array(
				'i_sj'			      => $isj,
				'i_nota'    			=> $inota,
				'd_nota'		    	=> $dnota,
				'i_area'	    		=> $iarea,
				'i_product'		  	=> $iproduct,
				'i_product_grade'	=> $iproductgrade,
				'i_product_motif'	=> $iproductmotif,
				'n_deliver'		  	=> $ndeliver,
				'v_unit_price'		=> $vunitprice,
				'e_product_name'	=> $eproductname,
        'n_item_no'       => $i
  		)
  	);
  	$this->db->insert('tm_nota_item');
  }
  function balikspb($ispb,$area)
  {
  	$this->db->set(
  		array(
				'f_spb_valid'			    => 'f',
				'f_spb_siapnotasales'	=> 'f'
  		)
  	);
  	$this->db->where('i_spb',$ispb);
  	$this->db->where('i_area',$area);
  	$this->db->update('tm_spb');
  }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  function bacasalesman($cari,$area1,$area2,$area3,$area4,$area5,$num,$offset)
  {
		if($area1=='00'){
			$query = $this->db->select("i_salesman, e_salesman_name from tr_salesman
									               	where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
																	order by i_salesman",false)->limit($num,$offset);
		}else{
			$this->db->select(" i_salesman, e_salesman_name from tr_salesman
										              where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
														      or i_area = '$area4' or i_area = '$area5')
										              and (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')  
										              order by i_salesman",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacacustomer($cari,$iarea,$num,$offset)
  {
		$this->db->select(" * from tr_customer a 
					left join tr_customer_pkp b on
					(a.i_customer=b.i_customer) 
					left join tr_price_group c on
					(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
					left join tr_customer_area d on
					(a.i_customer=d.i_customer) 
					left join tr_customer_salesman e on
					(a.i_customer=e.i_customer and e.i_product_group='01')
					left join tr_customer_discount f on
					(a.i_customer=f.i_customer) where a.i_area='$iarea'
          and a.f_approve='t' and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
					order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea,$vunitprice,$i)
  {
    $query=$this->db->query(" select i_product from tm_spb_item where i_spb='$ispb' and i_product='$iproduct' 
                              and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif' and i_area='$iarea'");
    if ($query->num_rows() > 0){
      $this->db->query("update tm_spb_item set n_deliver = $ndeliver
                        where i_spb='$ispb' and i_product='$iproduct' and i_product_grade='$iproductgrade'
                        and i_product_motif='$iproductmotif' and i_area='$iarea' ",false);
    }else{
      $query=$this->db->query(" select e_product_name, i_product_status from tr_product where i_product='$iproduct'");
      if ($query->num_rows() > 0){
        foreach($query->result as $row){
          $nama=$row->e_product_name;
          $status=$row->i_product_status;
        }
      }     
      $this->db->query("insert into tm_spb_item 
                        values
                        ('$ispb','$iproduct','$iproductgrade','$iproductmotif',$ndeliver,$ndeliver,$ndeliver,$vunitprice,
                         '$nama',null,'$iarea',null,$i,'$status')");
    }
  }
  function updatenilaispb($ispb,$iarea)
  {
    $query=$this->db->query(" select * from tm_spb_item where i_spb='$ispb' and i_area='$iarea'");
    if ($query->num_rows() > 0){
      $nilai=0;
      $vdis1=0;
      $vdis2=0;
      $vdis3=0;
      foreach($query->result() as $raw){
        $nilai=$nilai+($raw->n_order*$raw->v_unit_price);
      }
      $query=$this->db->query(" select * from tm_spb where i_spb='$ispb' and i_area='$iarea'");
      if ($query->num_rows() > 0){
        foreach($query->result() as $raw){
          $ndisc1=$raw->n_spb_discount1;
          $ndisc2=$raw->n_spb_discount2;
          $ndisc3=$raw->n_spb_discount3;
          $vdis1=$vdis1+(($nilai*$ndisc1)/100);
		      $vdis2=$vdis2+((($nilai-$vdis1)*$ndisc2)/100);
		      $vdis3=$vdis3+((($nilai-($vdis1+$vdis2))*$ndisc3)/100);
          $vtotdis=$vdis1+$vdis2+$vdis3;
        }
        $query=$this->db->query(" update tm_spb 
                                  set v_spb_discount1=$vdis1,
                                      v_spb_discount2=$vdis2,
                                      v_spb_discount3=$vdis3,
                                      v_spb_discounttotal=$vtotdis,
                                      v_spb=$nilai
                                  where i_spb='$ispb' and i_area='$iarea'");
      }
    }
  }
  function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
  {
    $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                              from tm_ic_trans
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              order by d_transaction desc",false);
    if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
  {
    $query=$this->db->query(" SELECT n_quantity_stock
                              from tm_ic
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            ",false);
    if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
  {
    $query 	= $this->db->query("SELECT current_timestamp as c");
    $row   	= $query->row();
    $now	  = $row->c;
    $query=$this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                '$eproductname', '$isj', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                              )
                            ",false);
  }
  function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
  {
    $ada=false;
    $query=$this->db->query(" SELECT i_product
                              from tm_mutasi
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                            ",false);
    if ($query->num_rows() > 0){
			$ada=true;
		}
    return $ada;
  }
  function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
  {
    $query=$this->db->query(" 
                              UPDATE tm_mutasi 
                              set n_mutasi_penjualan=n_mutasi_penjualan+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                            ",false);
  }
  function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
  {
    $query=$this->db->query(" 
                              insert into tm_mutasi 
                              (
                                i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                              values
                              (
                                '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,$qsj,0,0,$qaw-$qsj,0,'f')
                            ",false);
  }
  function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
  {
    $ada=false;
    $query=$this->db->query(" SELECT i_product
                              from tm_ic
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            ",false);
    if ($query->num_rows() > 0){
			$ada=true;
		}
    return $ada;
  }
  function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
  {
    $query=$this->db->query(" 
                              UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                              where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                            ",false);
  }
  function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
  {
    $query=$this->db->query(" 
                              insert into tm_ic 
                              values
                              (
                                '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                              )
                            ",false);
  }
    function bacaproduct($cari,$num,$offset,$kdharga)
    {
		if($offset=='')
			$offset=0;
      $query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
						        a.e_product_motifname as namamotif, 
						        c.e_product_name as nama,b.v_product_retail as harga
						        from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
					          where 
					          d.i_product_type=c.i_product_type 
					          and b.i_product=a.i_product and a.i_product_motif='00'
					          and a.i_product=c.i_product
					          and b.i_price_group='$kdharga'
					          and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') limit $num offset $offset",false);
#and d.i_product_group='01'
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$kdharga,$num,$offset)
    {
		if($offset=='')
			$offset=0;
			$str	= "  select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,b.v_product_retail as harga
							from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
							where b.i_product=a.i_product and a.i_product_motif='00'
							and d.i_product_type=c.i_product_type 
							and a.i_product=c.i_product
							and b.i_price_group='$kdharga' 
							and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
							limit $num offset $offset";
		$query=$this->db->query($str,false);
#and d.i_product_group='01'
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by d_transaction desc, i_trans desc",false);
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$isj', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
}
?>
