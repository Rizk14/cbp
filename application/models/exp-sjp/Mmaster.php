<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
    }
    function baca($dfrom,$dto,$num,$offset,$cari)
    {
		  $this->db->select("	a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                                b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                                from tm_sjp a, tm_sjp_item b, tr_area c
                                where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and a.f_sjp_cancel='f'
                                and a.d_sjp>=to_date('$dfrom','dd-mm-yyyy')
                                and a.d_sjp<= to_date('$dto','dd-mm-yyyy')
                                and not a.d_sjp_receive is null
                                order by a.i_sjp",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($dfrom,$dto,$cari)
    {
		  $this->db->select("a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                                b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                                from tm_sjp a, tm_sjp_item b, tr_area c
                                where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and a.f_sjp_cancel='f'
                                and a.d_sjp>=to_date('$dfrom','dd-mm-yyyy')
                                and a.d_sjp<= to_date('$dto','dd-mm-yyyy')
                                and not a.d_sjp_receive is null
                                order by a.i_sjp",false);
		  $query = $this->db->get();
      return $query;
    }
    function proses($dfrom,$dto,$iarea)
    {
		  $this->db->select("	a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                                b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                                from tm_sjp a, tm_sjp_item b, tr_area c
                                where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and a.f_sjp_cancel='f'
                                and a.d_sjp>=to_date('$dfrom','dd-mm-yyyy')
                                and a.d_sjp<= to_date('$dto','dd-mm-yyyy')
                                and not a.d_sjp_receive is null
                                order by a.i_sjp",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }


    /*DICOMMENT TANGGAL 14-08-2017
    function baca($iperiode,$num,$offset,$cari)
    {
      $this->db->select(" a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                          b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                          from tm_sjp a, tm_sjp_item b, tr_area c
                          where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area 
                          and to_char(a.d_sjp,'yyyymm')='$iperiode' and (b.n_quantity_receive is null
                          or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                          order by a.i_sjp ",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
    function bacaexcel($iperiode,$cari)
    {
      $this->db->select(" a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                          b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                          from tm_sjp a, tm_sjp_item b, tr_area c
                          where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area 
                          and to_char(a.d_sjp,'yyyymm')='$iperiode' and (b.n_quantity_receive is null
                          or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                          order by a.i_sjp ",false);
      $query = $this->db->get();
      return $query;
    }
    function proses($iperiode,$iarea)
    {
      $this->db->select(" a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                          b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                          from tm_sjp a, tm_sjp_item b, tr_area c
                          where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area 
                          and to_char(a.d_sjp,'yyyymm')='$iperiode' and (b.n_quantity_receive is null
                          or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                          order by a.i_sjp ",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }*/
}
?>
