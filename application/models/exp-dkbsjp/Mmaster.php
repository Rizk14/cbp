<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	function bacasemua($iarea, $dfrom, $dto, $num, $offset, $cari, $iuser)
	{
		if ($iarea == '00') {
			$this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
								where a.i_area=b.i_area
								and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_dkb) like '%$cari%') and substr(a.i_dkb,11,2)='$iarea' and 
								a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_dkb <= to_date('$dto','dd-mm-yyyy')
								order by a.i_dkb desc", false)->limit($num, $offset);
		} else {
			$this->db->select(" a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
								where a.i_area=b.i_area
								and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
								or upper(a.i_dkb) like '%$cari%') /*and substr(a.i_dkb,11,2)='$iarea'*/ and
								a.i_area='$iarea' and 
								a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_dkb <= to_date('$dto','dd-mm-yyyy')
								order by a.i_dkb desc", false)->limit($num, $offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cari($iarea, $dfrom, $dto, $num, $offset, $cari, $iuser)
	{
		if ($iarea == '00') {
			$this->db->select("  da.*, b.e_area_name from tm_dkb_sjp a, tr_area b
										where a.i_area=b.i_area
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dkb) like '%$cari%') and substr(a.i_dkb,11,2)='$iarea' and 
										a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dkb <= to_date('$dto','dd-mm-yyyy')
										order by a.i_dkb desc", false)->limit($num, $offset);
		} else {
			$this->db->select("   da.*, b.e_area_name from tm_dkb_sjp a, tr_area b
										where a.i_area=b.i_area
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dkb) like '%$cari%') and /*substr(a.i_dkb,11,2)='$iarea' and*/
										a.i_area in(select i_area from tm_user_area where i_user='$iuser') and 
										a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dkb <= to_date('$dto','dd-mm-yyyy')
										order by a.i_dkb desc", false)->limit($num, $offset);
		}

		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaarea($num, $offset, $iuser)
	{
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function cariarea($cari, $num, $offset, $iuser)
	{
		$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function bacaperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select("	a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							          where a.i_area=b.i_area 
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							          or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							          and substr(a.i_dkb,11,2)='$iarea' and 
							          a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							          a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							          order by a.i_dkb desc ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
	function cariperiode($iarea, $dfrom, $dto, $num, $offset, $cari)
	{
		$this->db->select("	a.*, b.e_area_name from tm_dkb_sjp a, tr_area b
							where a.i_area=b.i_area 
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dkb <= to_date('$dto','dd-mm-yyyy')
							order by a.i_dkb desc ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function baca($idkb, $iarea)
	{
		$this->db->select(" 	a.i_dkb,
								a.i_area,
								b.e_area_name,
								c.e_dkb_kirim,
								a.i_dkb_via,
								d.e_dkb_via,
								a.e_sopir_name,
								a.v_dkb,
								a.d_dkb,
								f.i_ekspedisi,
								f.e_ekspedisi
							FROM
								tm_dkb_sjp a
								INNER JOIN tr_area b ON (a.i_area = b.i_area)
								INNER JOIN tr_dkb_kirim c ON (a.i_dkb_kirim = c.i_dkb_kirim)
								INNER JOIN tr_dkb_via d ON (a.i_dkb_via = d.i_dkb_via)
								LEFT JOIN tm_dkb_ekspedisi e ON (a.i_dkb = e.i_dkb AND a.i_area = e.i_area)
								LEFT JOIN tr_ekspedisi f ON (e.i_ekspedisi = f.i_ekspedisi)
							WHERE
								a.i_dkb = '$idkb' AND a.i_area = '$iarea'
							ORDER BY
								a.i_dkb DESC ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}

	function bacadetail($idkb, $iarea)
	{
		$this->db->select(" 	a.*,
								b.i_area,
								c.e_area_name,
								b.i_spmb,
								d.d_spmb,
								b.d_sjp_receive, 
								d.d_approve2 AS d_approve,
								d.e_remark AS keterangan,
								c.e_area_name
							FROM
								tm_dkb_sjp_item a,
								tm_sjp b,
								tr_area c,
								tm_spmb d
							WHERE
								a.i_area = b.i_area
								AND a.i_sjp = b.i_sjp
								AND a.i_dkb = '$idkb'
								AND a.i_area = '$iarea'
								AND b.i_spmb = d.i_spmb
								AND b.i_area = d.i_area
								AND b.i_area = c.i_area
							ORDER BY
								a.i_sjp DESC", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		}
	}
}
