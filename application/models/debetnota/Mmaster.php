<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$iarea)
    {
		  $this->db->select(" * from tm_giro a
							  inner join tr_area on(a.i_area=tr_area.i_area)
							  inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							  where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
	}

	function data_alokasi($i_kbank, $iarea, $i_coa_bank){
		return $this->db->query("select a.i_customer, b.i_customer_groupar from tm_alokasi a, tr_customer_groupar b where  
		a.i_customer = b.i_customer
		and a.i_kbank = '$i_kbank' and a.i_area = '$iarea' and a.i_coa_bank = '$i_coa_bank' limit 1");
	}

    function insert($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
      if($dpajak!=''){
      	$this->db->set(
      		array(
				  'i_area'			=> $iarea,
				  'i_kn'				=> $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 				=> $dkn,
				  'd_refference'		=> $drefference,
				  'i_pajak' 			=> $ipajak,
				  'd_pajak' 			=> $dpajak,
				  'd_entry' 			=> $dentry,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			=> $vsisa

      		)
      	);
      }else{
      	$this->db->set(
      		array(
				  'i_area'			=> $iarea,
				  'i_kn'				=> $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 				=> $dkn,
				  'd_refference'		=> $drefference,
				  'd_entry' 			=> $dentry,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			=> $vsisa

      		)
      	);
      }
    	
    	$this->db->insert('tm_kn');
    }

    function updatebank($i_kbank,$i_coa_bank,$vsisa)
    {
    	$this->db->set(
    		array(
				'v_sisa'			=> 0,
    		)
    	);
    	$this->db->where("i_kbank",$i_kbank);
    	$this->db->where("i_coa_bank",$i_coa_bank);
    	$this->db->update('tm_kbank');

    }

    function update($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dupdate	= $row->c;
    	$this->db->set(
    		array(
				'i_area'			=> $iarea,
				'i_kn'				=> $ikn,
				'i_customer' 		=> $icustomer,
				'i_refference' 	=> $irefference,
				'i_customer_groupar'=> $icustomergroupar,
				'i_salesman' 		=> $isalesman,
				'i_kn_type' 		=> $ikntype,
				'd_kn' 				=> $dkn,
				'd_refference'		=> $drefference,
				'd_update' 			=> $dupdate,
				'e_remark' 			=> $eremark,
				'f_cetak' 			=> $fcetak,
				'f_masalah' 		=> $fmasalah,
				'f_insentif' 		=> $finsentif,
				'n_kn_year' 		=> $nknyear,
				'v_netto' 			=> $vnetto,
				'v_gross' 			=> $vgross,
				'v_discount' 		=> $vdiscount,
				'v_sisa'			=> $vsisa

    		)
    	);
    	$this->db->where("i_kn",$ikn);
    	$this->db->where("n_kn_year",$nknyear);
    	$this->db->where("i_area",$iarea);
    	$this->db->update('tm_kn');
    }
    public function delete($igiro,$iarea) 
    {
		  $this->db->query("DELETE FROM tm_giro WHERE i_giro='$igiro' and i_area='$iarea'");
		  return TRUE;
    }
    function bacasemua($num,$offset,$cari)
    {
			$sm=PiutangDagang;
			$year = date('Y');
		  $this->db->select(" a.i_kbank,a.i_giro, a.i_area, a.d_bank, a.v_bank, b.e_area_name, c.e_bank_name, a.v_sisa, a.i_coa_bank, a.e_description
		  from tm_kbank a, tr_area b, tr_bank c
		  where a.i_area=b.i_area and a.f_kbank_cancel='false' and a.f_debet=false and a.v_sisa>0 and a.v_sisa <= 1000
		  and a.i_coa_bank=c.i_coa and (upper(a.i_kbank) like '%$cari%') and a.i_coa='$sm'
		  and a.i_kbank||a.i_area||a.d_bank not in(
			select i_refference||i_area||d_refference from tm_kn where f_kn_cancel = 'f'
			and i_refference like 'BM%'
			and n_kn_year = '$year'
			)
		  order by a.i_kbank, a.d_bank",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacaarea($num,$offset)
    {
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function cariarea($cari,$num,$offset)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacacustomer($cari, $iarea,$num,$offset)
    {
		  $this->db->select("   * from tr_customer a 
							  left join tr_customer_groupar f on
							  (a.i_customer=f.i_customer)  
							  left join tr_customer_discount g on
							  (a.i_customer=g.i_customer) 
							  left join tr_customer_area d on
							  (a.i_customer=d.i_customer) 
							  left join tr_customer_salesman e on
							  (a.i_customer=e.i_customer) 
							  where a.i_area='$iarea' and
							  (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
							  order by a.i_customer",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		  $this->db->select("  * from tr_customer a 
							  left join tr_customer_groupar f on
							  (a.i_customer=f.i_customer)  
							  left join tr_customer_discount g on
							  (a.i_customer=g.i_customer) 
							  left join tr_customer_area d on
							  (a.i_customer=d.i_customer) 
							  left join tr_customer_salesman e on
							  (a.i_customer=e.i_customer)
							  where a.i_area='$iarea' and
							  (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							  order by a.i_customer",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function runningnumberkn($th,$iarea)
  	{
/*
		$pot=substr($th,2,2);
		$this->db->select(" trim(to_char(count(i_kn)+1,'000')) as no from tm_kn where n_kn_year=$th and i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
			  $kn="D".$iarea.$row->no.$pot;
			}
			return $kn;
		}else{
			$kn="D".$iarea."001".$pot;
			return $kn;
		}
*/
		$pot=substr($th,2,2);
		$this->db->select(" max(substring(i_kn,4,3)) as no from tm_kn where i_area='$iarea' and substring(i_kn,1,1)='D'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
        $nono=$row->no+1;			  
			}
      settype($nono,"string");
		  $a=strlen($nono);
		  while($a<3){
		    $nono="0".$nono;
		    $a=strlen($nono);
		  }
      $kn="D".$iarea.$nono.$pot;
			return $kn;
		}else{
			$kn="D".$iarea."001".$pot;
			return $kn;
		}
	}
	function bacakn($ikn,$nknyear,$iarea)
	{
 		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman),
              a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
							from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
							where a.i_customer=b.i_customer 
							  and b.i_customer=d.i_customer
							  and a.i_salesman=e.i_salesman
							  and a.i_area=c.i_area 
							  and a.i_kn_type='03'
							  and a.i_kn='$ikn'
							  and a.n_kn_year=$nknyear
							  and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
}
?>
