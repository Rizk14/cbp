<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    function bacaperiode($icustomer,$dfrom,$dto)
    {
			$this->db->select("	a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                          from tm_orderpb a, tr_customer b , tr_area c, tr_spg d
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and
                          a.i_area=c.i_area and a.f_orderpb_cancel='f'
                          and a.i_spg=d.i_spg and a.i_customer=d.i_customer
                          and a.i_customer='$icustomer' and a.f_orderpb_rekap='f' 
                          and (a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy')
                          AND a.d_orderpb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_orderpb",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacacustomer($num,$offset) {
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
		    $this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                        where a.i_customer=b.i_customer order by a.i_customer", false)->limit($num,$offset);
      }else{
		    $this->db->select("a.*, b.e_customer_name from tr_spg a, tr_customer b
                        where a.i_customer=b.i_customer and (b.i_area='$area1' or b.i_area='$area2' or
                    b.i_area='$area3' or b.i_area='$area4' or b.i_area='$area5') order by a.i_customer", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function caricustomer($cari,$num,$offset){
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
		  $this->db->select(" a.*, b.e_customer_name from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
                            or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%')
						                order by a.i_customer ", FALSE)->limit($num,$offset);
      }else{
		  $this->db->select(" a.*, b.e_customer_name from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
                            or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%') and (b.i_area='$area1' or b.i_area='$area2' or b.i_area='$area3' or b.i_area='$area4' or b.i_area='$area5') 
						                order by a.i_customer ", FALSE)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumberspmb($thbl){
      $th	= '20'.substr($thbl,0,2);
      $asal='20'.$thbl;
      $thbl=substr($thbl,0,2).substr($thbl,2,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SPM'
                          and substr(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nospmb  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nospmb
                            where i_modul='SPM'
                            and substr(e_periode,1,4)='$th' ", false);
			  settype($nospmb,"string");
			  $a=strlen($nospmb);
			  while($a<6){
			    $nospmb="0".$nospmb;
			    $a=strlen($nospmb);
			  }
		  	$nospmb  ="SPMB-".$thbl."-".$nospmb;
			  return $nospmb;
		  }else{
			  $nospmb  ="000001";
			  $nospmb  ="SPMB-".$thbl."-".$nospmb;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SPM','00','$asal',1)");
			  return $nospmb;
		  }
    }
    function insertheader($ispmb, $dspmb, $iarea, $eremark)
    {
    	$this->db->set(
    		array(
			'i_spmb'		        => $ispmb,
			'd_spmb'		        => $dspmb,
			'i_area'		        => $iarea,
      'i_approve2'        => 'SYSTEM',
      'd_approve2'        => $dspmb,
#      'i_store'   => 'AA',
#      'i_store_location'=> '01',
      'f_spmb_acc'        => 't',
			'f_spmb_consigment'	=> 't',
			'n_print'		        => 0,
      'e_remark'          => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_spmb');
    }
    function insertdetail($iorderpb,$icustomer,$iarea,$ispmb)
    {
      $que=$this->db->query(" 	select a.*, b.v_product_retail from tm_orderpb_item a, tr_product_price b 
                                where i_orderpb='$iorderpb' and i_area='$iarea' and a.i_customer='$icustomer'
                                and a.i_product=b.i_product and 
                                a.i_product_grade=b.i_product_grade and b.i_price_group='00'");
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $query=$this->db->query(" 	select count(*) as brs from tm_spmb_item where i_spmb='$ispmb'");
          if($query->num_rows()>0){
            $br=$query->row();
            $baris=$br->brs+1;
          }else{
            $baris=1;
          }
          $query=$this->db->query(" 	select a.*, b.v_product_retail from tm_spmb_item a, tr_product_price b
                                      where a.i_spmb='$ispmb' and a.i_product='$row->i_product' and a.i_product=b.i_product and 
                                      a.i_product_grade=b.i_product_grade and b.i_price_group='00' and
                                      a.i_product_grade='$row->i_product_grade' and a.i_product_motif='$row->i_product_motif'");
          if($query->num_rows()>0){
            foreach($query->result() as $xx){
              $rem=$xx->e_remark;
            }
            $this->db->query(" 	update tm_spmb_item set n_order=n_order+$row->n_quantity_order, n_acc=n_acc+$row->n_quantity_order, n_saldo=n_saldo+$row->n_quantity_order, e_remark='$rem - $row->e_remark'  where i_spmb='$ispmb' and i_product='$row->i_product' and i_product_grade='$row->i_product_grade' and i_product_motif='$row->i_product_motif'");
          }else{
            $this->db->query(" 	insert into tm_spmb_item values('$ispmb','$row->i_product','$row->i_product_grade','$row->i_product_motif','$row->e_product_name',$row->n_quantity_order,0,$row->v_product_retail,'$row->e_remark',null,'$row->i_area',0,$baris,$row->n_quantity_order,$row->n_quantity_order)");
          }
        }
      }
    }
    function updateorderpb($iorderpb,$icustomer,$iarea,$ispmb)
    {
      $this->db->query(" 	update tm_orderpb set f_orderpb_rekap='t', i_spmb='$ispmb' where i_orderpb='$iorderpb' and i_area='$iarea' 
                          and i_customer='$icustomer'");
      $query=$this->db->query("select e_remark from tm_spmb where i_spmb='$ispmb'");
      if($query->num_rows()>0){
        foreach($query->result() as $xx){
          $rem=$xx->e_remark;
        }
	      if($rem!=''){
	        $this->db->query("update tm_spmb set e_remark='$rem - $iorderpb'  where i_spmb='$ispmb'");
	      }else{
	        $this->db->query("update tm_spmb set e_remark='$iorderpb'  where i_spmb='$ispmb'");
	      }
      }

      $que=$this->db->query("select a.i_customer, b.e_customer_name from tm_orderpb a, tr_customer b
                             where a.i_customer=b.i_customer and a.i_area=b.i_area
                             and a.i_orderpb='$iorderpb' and a.i_area='$iarea' and a.i_customer='$icustomer'");
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $customer=$row->e_customer_name;
        }
        $this->db->query("update tm_spmb_item set e_remark='$customer' where i_spmb='$ispmb' and n_item_no=1 ");
      }
    }
}
?>
