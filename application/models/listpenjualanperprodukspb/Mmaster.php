<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($iperiode)
  {
		$this->db->select(" a.i_area, a.i_product, a.e_product_name, b.e_area_name, e.i_product_group, 
                        sum(a.v_unit_price*a.n_order) as nilai, sum(a.n_order) as jumlah
                        from tm_spb c, tm_spb_item a, tr_area b, tr_product d, tr_product_type e
                        where c.f_spb_cancel='f' and c.i_spb=a.i_spb and c.i_area=a.i_area and c.i_area=b.i_area
                        and to_char(c.d_spb,'yyyymm')='$iperiode' and a.i_product=d.i_product and d.i_product_type=e.i_product_type
                        group by a.i_area, e.i_product_group, a.i_product, a.e_product_name, b.e_area_name
                        order by a.i_area, e.i_product_group, a.i_product, a.e_product_name, b.e_area_name ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($iperiode)
  {
		$this->db->select(" distinct i_area, e_area_name from vpenjualanperprodukspb where i_periode='$iperiode' order by i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($iperiode)
  {
		$this->db->select(" distinct i_product, e_product_name, e_product_groupname from vpenjualanperprodukspb
                        where i_periode='$iperiode'
                        order by e_product_groupname, i_product",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
