<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
  public function __construct()
    {
        parent::__construct();
    #$this->CI =& get_instance();
    }
    function baca($dfrom,$dto)
    {
        if($dfrom!=''){
            $tmp = explode("-", $dfrom);
            $hr = $tmp[0];
            $bl = $tmp[1];
            $th = $tmp[2];
            $thprev = $tmp[2]-1;
            $dfrom = $th."-".$bl."-".$hr;
            $dfromprev = $thprev."-".$bl."-".$hr;
        }

        if($dto){
            $tem = explode("-", $dto);
            $hri = $tem[0];
            $bln = $tem[1];
            $thn = $tem[2];
            $thnprev = $tem[2]-1;
            $dto = $thn."-".$bln."-".$hri;
            $dtoprev = $thnprev."-".$bln."-".$hri;
        }
      $this->db->select(" a.iseri, a.seriname, sum(a.oa) as oa, sum(a.oaprev) as oaprev ,sum(slsqty) as slsqty, sum(slsqtyprev) as slsqtyprev, sum(netsls) as netsls, sum(netslsprev) as netslsprev
                         from(

                         --//Hitung SALES QTY
                         select a.iseri, a.seriname,0 as oa, 0 as oaprev , sum(a.vol) as slsqty, 0 as slsqtyprev, 0 as netsls, 0 as netslsprev 
                         from
                         (
                         select a.i_product_seri as iseri, b.e_product_seriname as seriname, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_seri b, tm_nota_item c, tm_nota d
                         where b.i_product_seri=a.i_product_seri and c.i_product=a.i_product
                         and (d.d_nota >='$dfrom' and d.d_nota <='$dto') and d.f_nota_cancel='f'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_seri, b.e_product_seriname
                         ) as a
                         group by a.iseri, a.seriname
                         
                         union all
                         
            select a.iseri, a.seriname,0 as oa, 0 as oaprev , 0 as slsqty, sum(a.vol) as slsqtyprev , 0 as netsls, 0 as netslsprev 
                         from
                         (
                         select a.i_product_seri as iseri, b.e_product_seriname as seriname, sum(c.n_deliver) as vol
                         from tr_product a, tr_product_seri b, tm_nota_item c, tm_nota d
                         where b.i_product_seri=a.i_product_seri and c.i_product=a.i_product
                         and (d.d_nota >='$dfromprev' and d.d_nota <='$dtoprev') and d.f_nota_cancel='f'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_seri, b.e_product_seriname
                         ) as a
                         group by a.iseri, a.seriname

                         --// End Hitung Sales Qty
                         
                         union all
                         --// Hitung Net Sales
                         select a.iseri, a.seriname,0 as oa, 0 as oaprev , 0 as slsqty, 0 as slsqtyprev, sum(a.vnota) as netsls, 0 as netslsprev 
                         from
                         (
                         select a.i_product_seri as iseri, b.e_product_seriname as seriname,
                         round(sum((c.n_deliver*c.v_unit_price-(((c.n_deliver*c.v_unit_price)/d.v_nota_gross)*d.v_nota_discounttotal)))) as vnota
                         from tr_product a, tr_product_seri b, tm_nota_item c, tm_nota d
                         where b.i_product_seri=a.i_product_seri and c.i_product=a.i_product 
                         and (d.d_nota >='$dfrom' and d.d_nota <='$dto') and d.f_nota_cancel='f'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_seri, b.e_product_seriname
                         ) as a
                         group by a.iseri, a.seriname
                         
                         union all
                         
                         select a.iseri, a.seriname,0 as oa, 0 as oaprev , 0 as slsqty, 0 as slsqtyprev, 0 as netsls, sum(a.vnota) as netslsprev 
                         from
                         (
                         select a.i_product_seri as iseri, b.e_product_seriname as seriname,
                         round(sum((c.n_deliver*c.v_unit_price-(((c.n_deliver*c.v_unit_price)/d.v_nota_gross)*d.v_nota_discounttotal)))) as vnota
                         from tr_product a, tr_product_seri b, tm_nota_item c, tm_nota d
                         where b.i_product_seri=a.i_product_seri and c.i_product=a.i_product 
                         and (d.d_nota >='$dfromprev' and d.d_nota <='$dtoprev') and d.f_nota_cancel='f'
                         and d.i_nota=c.i_nota and d.i_area=c.i_area and not d.i_nota isnull
                         group by a.i_product_seri, b.e_product_seriname
                         ) as a
                         group by a.iseri, a.seriname
                         --// End Hitung Net sls
                         union all

                         select a.i_product_seri as iseri,a.e_product_seriname as seriname,count(a.oa) as oa, 0 as oaprev ,0 as slsqty, 0 as slsqtyprev, 0 as netsls, 0 as netslsprev from (
                           select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, e.e_product_seriname, e.i_product_seri
                           from tm_nota a, tm_nota_item b, tr_product d, tr_product_seri e 
                           where e.i_product_seri=d.i_product_seri and b.i_product=d.i_product 
                           and (a.d_nota >='$dfrom' and a.d_nota <='$dto') and a.f_nota_cancel='f'
                           and a.i_nota=b.i_nota and a.i_area=b.i_area and not a.i_nota isnull
                           group by e.i_product_seri, e.e_product_seriname,a.i_customer, to_char(a.d_nota,'yyyymm')
                        ) as a
                        group by a.i_product_seri,a.e_product_seriname
                        union all
                        select a.i_product_seri as iseri,a.e_product_seriname as seriname,0 as oa, count(a.oa) as oaprev ,0 as slsqty, 0 as slsqtyprev, 0 as netsls, 0 as netslsprev from (
                           select distinct on (to_char(a.d_nota,'yyyymm') , a.i_customer)  a.i_customer as oa, e.e_product_seriname, e.i_product_seri
                           from tm_nota a, tm_nota_item b, tr_product d, tr_product_seri e 
                           where e.i_product_seri=d.i_product_seri and b.i_product=d.i_product 
                           and (a.d_nota >='$dfromprev' and a.d_nota <='$dtoprev') and a.f_nota_cancel='f'
                           and a.i_nota=b.i_nota and a.i_area=b.i_area and not a.i_nota isnull
                           group by e.i_product_seri, e.e_product_seriname,a.i_customer, to_char(a.d_nota,'yyyymm')
                        ) as a
                        group by a.i_product_seri,a.e_product_seriname
                                     
            )as a
            group by a.iseri, a.seriname
            order by a.iseri",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }
}
?>
