<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode,$d_opname)
    {
		$query = $this->db->query("select a.*, b.e_area_name from f_umur_piutang('$iperiode','$d_opname') a, tr_area b
                               where a.i_area = b.i_area order by a.i_area, i_umur_piutang, i_customer");
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
  	function bacaareapiutang($iperiode,$d_opname)
    {
			$this->db->select("distinct(area) from f_umur_piutang_area('$iperiode','$d_opname') order by area;", false);
  		$query = $this->db->get();
	  	if ($query->num_rows() > 0){
	  		return $query->result();
    	}
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_store_name as e_area_name from tm_spmb a, tr_store b
							          where a.i_area=b.i_store and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" * from tr_area order by i_area", false)->limit($num,$offset);
  		$query = $this->db->get();
	  	if ($query->num_rows() > 0){
	  		return $query->result();
    	}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
        							   order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
 	function bacano($iperiodeawal, $iperiodeakhir)
  {
    $this->db->select(" a.e_area_name, b.i_customer_groupbayar, c.e_customer_name , b.e_periode_awal, b.e_periode_akhir, b.i_kategori, b.e_kategori,
                        b.n_rata_telat, b.i_index, b.v_total_penjualan, b.v_max_penjualan, b.v_rata_penjualan, b.v_plafond as v_plafond_program, b.v_plafond_before, b.v_plafond_acc 
                        from tm_plafond b, tr_area a, tr_customer c
                        where a.i_area = b.i_area and b.e_periode_awal ='$iperiodeawal' and b.e_periode_akhir ='$iperiodeakhir'
                        and c.i_customer= b.i_customer_groupbayar",false);
    $tes=$this->db->get();
    return $tes;
  }
}
?>
