<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($iarea,$num,$offset,$cari)
    {
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy/mm/dd') as c");
      $row   	= $query->row();
      $tglsys	= $row->c;
      $tmp    = $this->fungsi->dateAdd("d",-7,$tglsys);
      $tmp 	  = explode("-", $tmp);
		  $det1	  = $tmp[2];
		  $mon1	  = $tmp[1];
		  $yir1 	= $tmp[0];
		  $dakhir	= $yir1."-".$mon1."-".$det1;

		  if ($cari == '') {
			  $this->db->select("	distinct a.*, c.e_customer_name
                            from tm_spb a
                            inner join tm_spb_item b on (a.i_spb=b.i_spb and a.i_area=b.i_area)
                            inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                            where not a.i_sj is null and not a.i_nota is null
                            and a.f_spb_cancel='f' and b.n_deliver < b.n_order
                            and a.i_area='$iarea' 
		                        order by a.i_spb, a.d_spb ",false)->limit($num,$offset);
  /*
  and
  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
  a.d_spb <= to_date('$dto','dd-mm-yyyy'))
  */
		  }else{
			  $this->db->select("	distinct a.*, c.e_customer_name
                            from tm_spb a
                            inner join tm_spb_item b on (a.i_spb=b.i_spb and a.i_area=b.i_area)
                            inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                            where not a.i_sj is null and not a.i_nota is null
                            and a.f_spb_cancel='f' and b.n_deliver < b.n_order
                            and a.i_area='$iarea' AND
					                  (upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%'
					                  or upper(a.i_spb) like '%$cari%')
					                  order by a.i_spb, a.d_spb ",false)->limit($num,$offset);
  /*
  and
  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
  a.d_spb <= to_date('$dto','dd-mm-yyyy'))
  */
		  }		
	
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
