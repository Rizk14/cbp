<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Logger extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	function write($user_id, $ip_address, $waktu, $message)
	{
		if ($user_id != '') {
			$data['user_id'] 	= $user_id;
			$data['ip_address'] = $ip_address;
			$data['waktu'] = $waktu;
			$data['activity'] 	= $message;
			$this->db->insert('dgu_log', $data);
		}
	}

	public function writenew($pesan)
	{
		$query    = $this->db->query("SELECT current_timestamp as c");
		$row      = $query->row();
		$waktu    = $row->c;
		$user_id  = $this->session->userdata('user_id');
		if ($user_id == "") $user_id = "SYSTEM";
		$ip_address = $_SERVER['REMOTE_ADDR'];

		$data = array(
			'user_id' 	 => $user_id,
			'ip_address' => $ip_address,
			'waktu' 	 => $waktu,
			'activity' 	 => $pesan
		);

		$this->db->insert('dgu_log', $data);
	}
}
