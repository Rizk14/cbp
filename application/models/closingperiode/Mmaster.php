<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		#$this->CI =& get_instance();
	}
	function cekopname($iperiode)
	{
		$komplit = false;
		$store = '';
		$this->db->select(" i_store from tr_store where f_aktif='t' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$store = $row->i_store;
				$this->db->select(" i_stockopname_akhir from tm_mutasi_header 
			                      where e_mutasi_periode='$iperiode' and i_store='$store' ", false);
				$que = $this->db->get();
				if ($que->num_rows() > 0) {
					foreach ($que->result() as $ro) {
						if ($ro->i_stockopname_akhir == null || $ro->i_stockopname_akhir == '') {
							$komplit = false;
							break 2;
						} else {
							$komplit = true;
						}
					}
					$que->free_result();
				} else {
					$komplit = false;
					break;
				}
			}
			$query->free_result();
		}
		if ($komplit) {
			$komplit = false;
			$store = '';
			$this->db->select(" i_customer from tr_customer_consigment where i_customer like 'PB%' 
	                        and i_customer in(select i_customer from tr_customer where f_customer_aktif=true) 
	                        order by i_customer ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $row) {
					$store = $row->i_customer;
					$this->db->select(" i_stockopname_akhir from tm_mutasi_headerconsigment
			                        where e_mutasi_periode='$iperiode' and i_customer='$store' ", false);
					$que = $this->db->get();
					if ($que->num_rows() > 0) {
						foreach ($que->result() as $ro) {
							if ($ro->i_stockopname_akhir == null || $ro->i_stockopname_akhir == '') {
								$komplit = false;
								break 2;
							} else {
								$komplit = true;
							}
						}
						$que->free_result();
					} else {
						$komplit = false;
						break;
					}
				}
				$query->free_result();
			}
		}
		$komplit = true;
		return array($komplit, $store);
	}

	function cekap($iperiode)
	{
		$komplit = false;
		$do = '';
		$this->db->select(" i_do from tm_do where to_char(d_do,'yyyymm')='$iperiode' and f_do_cancel='f' 
	                      and i_do not in(select b.i_do from tm_dtap a, tm_dtap_item b 
	                      where a.i_dtap=b.i_dtap and a.i_supplier=b.i_supplier and to_char(a.d_dtap,'yyyymm')='$iperiode'
                        and a.f_dtap_cancel='f')", false);
		$que = $this->db->get();
		if ($que->num_rows() > 0) {
			foreach ($que->result() as $row) {
				$do = $row->i_do;
				break;
			}
			$que->free_result();
			$komplit = false;
		} else {
			$komplit = true;
		}
		return array($komplit, $do);
	}

	function cekar($iperiode)
	{
		$komplit = false;
		$sj = '';
		$this->db->select(" i_sj from tm_nota where to_char(d_sj_receive,'yyyymm')='$iperiode' and f_nota_cancel='f' 
	                      and i_nota isnull", false);
		$que = $this->db->get();
		if ($que->num_rows() > 0) {
			$komplit = false;
			foreach ($que->result() as $row) {
				$sj = $row->i_sj;
				break;
			}
		} else {
			$komplit = true;
		}
		return array($komplit, $sj);
	}

	function pindah($iperiode)
	{
		$user 			= $this->session->userdata('user_id');
		$emutasiperiode = $iperiode;
		$bldpn 			= substr($emutasiperiode, 4, 2) + 1;

		/* TAMBAH NOL KALAU HARI KURANG DARI 2 KARAKTER (05 Apr 2023) */
		settype($bldpn, "string");
		$a = strlen($bldpn);
		while ($a < 2) {
			$bldpn = "0" . $bldpn;
			$a	= strlen($bldpn);
		}
		/* ********** */

		if ($bldpn == 13) {
			$perdpn = substr($emutasiperiode, 0, 4) + 1;
			$perdpn = $perdpn . '01';
		} else {
			$perdpn = substr($emutasiperiode, 0, 4);
			// $perdpn = $perdpn . (substr($emutasiperiode, 4, 2) + 1); /* COMMENT (05 Apr 2023) */
			$perdpn = $perdpn . $bldpn;
		}

		$xperiode = $perdpn;
		#####Saldo Awal Stock    
		$this->db->query(" delete from tm_mutasi_saldoawalmo where e_mutasi_periode='$xperiode'", FALSE);
		$this->db->query(" insert into tm_mutasi_saldoawalmo 
    SELECT   i_product, i_product_motif, i_product_grade, i_customer, '$xperiode', n_saldo_akhir-n_mutasi_git as n_saldo_awal 
    from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode')", false);
		$this->db->query(" delete from tm_mutasi_saldoawal where e_mutasi_periode='$xperiode'", FALSE);
		$this->db->query(" insert into tm_mutasi_saldoawal 
    SELECT   i_store, i_store_location, i_store_locationbin, '$xperiode', i_product, i_product_grade, i_product_motif, n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal 
    from f_mutasi_stock_daerah_all_saldoakhir('$iperiode')", false);
		$this->db->query(" insert into tm_mutasi_saldoawal 
    SELECT   i_store, i_store_location, i_store_locationbin, '$xperiode', i_product, i_product_grade, i_product_motif, n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal 
    from f_mutasi_stock_pusat_saldoakhir('$iperiode')", false);
		$this->db->query(" insert into tm_mutasi_saldoawal 
    SELECT   i_store, i_store_location, i_store_locationbin, '$xperiode', i_product, i_product_grade, i_product_motif, n_saldo_akhir as n_saldo_awal 
    from f_mutasi_stock_mo_pb_saldoakhir('$iperiode')", false);
		#####End Of Saldo Awal Stock

		#####Stock Reguler
		/*
    $store='';
    $this->db->select(" i_store from tr_store where f_aktif='t' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_stock where i_periode='$xperiode'",false);
		  foreach($query->result() as $row){
			  $store=$row->i_store;
			  $this->db->select(" i_stockopname_akhir from tm_mutasi_header 
			                      where e_mutasi_periode='$iperiode' and i_store='$store' ",false);
	      $que = $this->db->get();
	      if ($que->num_rows() > 0){
		      foreach($que->result() as $ro){
			      $iopname=$ro->i_stockopname_akhir;
			      $this->db->select(" a.*, b.v_product_mill from tm_stockopname_item a, tr_product b
			                          where a.i_stockopname='$iopname' and a.i_store='$store' and a.i_product=b.i_product ",false);
	          $qu = $this->db->get();
	          if ($qu->num_rows() > 0){
		          foreach($qu->result() as $r){
		            if($r->n_stockopname==null)$r->n_stockopname=0;
		            $query 	= $this->db->query("SELECT current_timestamp as c");
                $row   	= $query->row();
            	  $entry	= $row->c;
                $this->db->query(" insert into tm_saldo_stock (i_periode, i_store, i_product, i_product_motif, i_product_grade, 
                                   n_quantity_awal, n_quantity_in, 
                                   n_quantity_out, n_quantity_akhir, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                                   d_entry, i_entry, d_update, i_update) values ('$xperiode', '$r->i_store', '$r->i_product', 
                                   '$r->i_product_motif', '$r->i_product_grade', 
                                   $r->n_stockopname, 0, 0, $r->n_stockopname, $r->n_stockopname*$r->v_product_mill, 0, 0, 
                                   $r->n_stockopname*$r->v_product_mill, '$entry', '$user', null, null) ",false);
                $this->db->query(" update tm_saldo_stock set n_quantity_akhir=$r->n_stockopname, 
                                   v_saldo_akhir=$r->n_stockopname*$r->v_product_mill where i_periode='$iperiode' 
                                   and i_store='$store' and i_product='$r->i_product' and i_product_motif='$r->i_product_motif' 
                                   and i_product_grade='$r->i_product_grade'",false);
              }
              $qu->free_result();
		        }
		      }
		      $que->free_result();
	      }
      }
      $query->free_result();
	  }
*/
		#####End of Stock Reguler
		#####Stock Kons
		/*
    $cust='';
    $this->db->select(" i_customer from tr_customer_consigment where i_customer like 'PB%' ",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_stockcons where i_periode='$xperiode'",false);
	    foreach($query->result() as $row){
		    $cust=$row->i_customer;
		    $this->db->select(" i_stockopname_akhir from tm_mutasi_headerconsigment
		                        where e_mutasi_periode='$iperiode' and i_customer='$cust' ",false);
        $que = $this->db->get();
        if ($que->num_rows() > 0){
	        foreach($que->result() as $ro){
		        $iopname=$ro->i_stockopname_akhir;
			      $this->db->select(" a.*, b.v_product_mill from tm_sopb_item a, tr_product b
			                          where a.i_sopb='$iopname' and a.i_customer='$cust' and a.i_product=b.i_product ",false);
	          $qu = $this->db->get();
	          if ($qu->num_rows() > 0){
		          foreach($qu->result() as $r){
		            if($r->n_sopb==null)$r->n_sopb=0;
		            $query 	= $this->db->query("SELECT current_timestamp as c");
                $row   	= $query->row();
            	  $entry	= $row->c;
                $this->db->query(" insert into tm_saldo_stockcons (i_periode, i_customer, i_product, i_product_motif, i_product_grade, 
                                   n_quantity_awal, n_quantity_in, 
                                   n_quantity_out, n_quantity_akhir, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry, 
                                   i_entry, d_update, i_update) values ('$xperiode', '$r->i_customer', '$r->i_product', 
                                   '$r->i_product_motif', '$r->i_product_grade', $r->n_sopb, 
                                   0, 0, $r->n_sopb, $r->n_sopb*$r->v_product_mill, 0, 0, $r->n_sopb*$r->v_product_mill,
                                   '$entry', '$user', null, null) ",false);
                $this->db->query(" update tm_saldo_stockcons set n_quantity_akhir=$r->n_sopb, 
                                   v_saldo_akhir=$r->n_sopb*$r->v_product_mill 
                                   where i_periode='$iperiode' and i_customer='$r->i_customer' and i_product='$r->i_product'
                                   and i_product_motif='$r->i_product_motif' and i_product_grade='$r->i_product_grade'",false);
			        }
			        $qu->free_result();
			      }
	        }
	        $que->free_result();
        }
	    }
	    $query->free_result();
    }
*/
		#####End of Stock Kons
		#####Saldo AP
		/*
    $this->db->select(" i_supplier from tr_supplier order by i_supplier",false);
	  $que = $this->db->get();
	  if ($que->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_ap where i_periode='$xperiode'",false);
		  foreach($que->result() as $ro){
			  $this->db->select(" i_supplier, sum(x.amount) as amount from (
                            SELECT a.i_supplier, (a.v_sisa) as amount FROM tm_dtap a 
                            WHERE to_char(a.d_dtap,'yyyymm')<'$xperiode' and a.f_dtap_cancel='f' and a.v_sisa=a.v_netto
                            and a.i_dtap not in(select i_dtap from tm_pelunasanap_item) and a.i_supplier='$ro->i_supplier'
                            union all
                            SELECT a.i_supplier, c.v_jumlah+a.v_sisa as amount FROM tm_dtap a, tm_pelunasanap b, tm_pelunasanap_item c
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_dtap_cancel='f' and b.f_pelunasanap_cancel='f' 
                            and b.i_pelunasanap=c.i_pelunasanap and b.d_bukti=c.d_bukti and b.i_area=c.i_area and a.i_dtap=c.i_dtap 
                            and a.i_supplier=b.i_supplier and to_char(a.d_dtap,'yyyymm')<'$xperiode' and a.i_supplier='$ro->i_supplier'
                            ) as x group by x.i_supplier",false);
	      $query = $this->db->get();
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $row){
		        $query 	= $this->db->query("SELECT current_timestamp as c");
            $riw   	= $query->row();
        	  $entry	= $riw->c;
            $this->db->query(" insert into tm_saldo_ap (i_periode, i_supplier, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                               d_entry, i_entry, d_update, i_update) values ('$xperiode', '$ro->i_supplier', $row->amount, 0, 0, 
                               $row->amount, '$entry', '$user', null, null) ",false);
            $this->db->query(" update tm_saldo_ap set v_saldo_akhir=$row->amount where i_periode='$iperiode' and 
                               i_supplier='$ro->i_supplier'",false);
			    }
			    $query->free_result();
        }			  
		  }
		  $que->free_result();
	  }
*/
		#####end of Saldo AP
		#####Saldo AR
		/*
    $this->db->select(" i_customer from tr_customer order by i_customer",false);
	  $que = $this->db->get();
	  if ($que->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_ar where i_periode='$xperiode'",false);
		  foreach($que->result() as $ro){
			  $this->db->select(" y.i_customer, sum(y.sisa) as amount from (
                            SELECT a.i_customer, a.v_nota_netto as sisa FROM tm_nota a  WHERE to_char(a.d_nota,'yyyymm')<'$xperiode' 
                            and a.f_nota_cancel='f' and a.v_sisa=a.v_nota_netto and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, c.v_jumlah as sisa FROM tm_nota a, tm_pelunasan b, tm_pelunasan_item c
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_nota_cancel='f' and b.f_pelunasan_cancel='f' 
                            and b.f_giro_tolak='f' and b.f_giro_batal='f'
                            and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area and b.i_dt=c.i_dt and a.i_nota=c.i_nota 
                            and to_char(a.d_nota,'yyyymm')<'$xperiode' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, a.v_nota_netto as amount FROM tm_nota a, tm_pelunasan b, tm_pelunasan_item c
                            WHERE to_char(b.d_bukti,'yyyymm')<'$xperiode' and a.v_sisa>0 and a.v_sisa<>a.v_nota_netto 
                            and a.f_nota_cancel='f' and b.f_pelunasan_cancel='f' and b.f_giro_tolak='f' 
                            and b.f_giro_batal='f' and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area and b.i_dt=c.i_dt 
                            and a.i_nota=c.i_nota and to_char(a.d_nota,'yyyymm')<'$xperiode' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, -(a.v_netto) as sisa from tm_kn a
                            where a.f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')<'$xperiode' and a.v_netto=a.v_sisa 
                            and upper(substring(a.i_kn,1,1))='K' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, -((sum(b.v_jumlah)-sum(b.v_lebih))+a.v_sisa) as sisa
                            FROM tm_kn a, tm_pelunasan b
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_kn_cancel='f' and b.f_pelunasan_cancel='f' 
                            and b.f_giro_tolak='f' and b.f_giro_batal='f' and a.i_customer='$ro->i_customer'
                            and a.i_kn=b.i_giro and to_char(a.d_kn,'yyyymm')<'$xperiode' and b.i_jenis_bayar='04'  
                            and upper(substring(a.i_kn,1,1))='K' and a.i_area=b.i_area
                            group by a.i_customer, a.v_sisa
                            ) as y
                            group by y.i_customer",false);
	      $query = $this->db->get();
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $row){
		        if($row->amount==null)$row->amount=0;
		        $query 	= $this->db->query("SELECT current_timestamp as c");
            $riw   	= $query->row();
        	  $entry	= $riw->c;
            $this->db->query(" insert into tm_saldo_ar (i_periode, i_customer, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                               d_entry, i_entry, d_update, i_update) values ('$xperiode', '$ro->i_customer', $row->amount, 0, 0,
                               $row->amount, '$entry', '$user', null, null) ",false);
            $this->db->query(" update tm_saldo_ar set v_saldo_akhir=$row->amount where i_periode='$iperiode' and 
                               i_customer='$ro->i_customer'",false);
			    }
			    $query->free_result();
        }			  
		  }
		  $que->free_result(); 
	  }
*/
		#####end of Saldo AR
		#####################	  
		#	  closing kas kecil 
		/*
	  $this->db->select(" i_area from tr_area where f_area_real='t' ",false);
	  $y = $this->db->get();
	  if ($y->num_rows() > 0){
		  foreach($y->result() as $w){
		  	$iarea=$w->i_area;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and substr(i_coa,6,2)='$iarea' 
														and substr(i_coa,1,5)='111.2' ",false);
			  $query = $this->db->get();
				$sawal=0;
			  $saldo=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $sawal=$row->v_saldo_awal;
					  $saldo=$row->v_saldo_awal;
				  }
				  $query->free_result();
			  }
			  $kredit=0;
			  $this->db->select(" sum(v_kk) as v_kk from tm_kk
								  where i_periode='$iperiode' and i_area='$iarea' and f_debet='t' and f_kk_cancel='f'",false);						 
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $kredit=$row->v_kk;
				  }
				  $query->free_result();
			  }
			  $debet=0;
			  $this->db->select(" sum(v_kk) as v_kk from tm_kk
								  where i_periode='$iperiode' and i_area='$iarea' and f_debet='f' and f_kk_cancel='f'",false);							
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $debet=$row->v_kk;
				  }
				  $query->free_result();
			  }
			  $saldo=$saldo+$debet-$kredit;
			  $coa=KasKecil.$iarea;
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$update	= $row->c;
				$user=$this->session->userdata('user_id');
				if($debet=='' || $debet==null) $debet=0;
				if($kredit=='' || $kredit==null) $kredit=0;
			  $this->db->query("update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
			  					 				v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
								 					and i_coa='$coa'");
			  $emutasiperiode=$iperiode;
			  $bldpn=substr($emutasiperiode,4,2)+1;
			  if($bldpn==13)
			  {
			    $perdpn=substr($emutasiperiode,0,4)+1;
			    $perdpn=$perdpn.'01';
			  }else{
			    $perdpn=substr($emutasiperiode,0,4);
			    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
			  }
			  $xperiode=$perdpn;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and substr(i_coa,6,2)='$iarea' 
							    and substr(i_coa,1,5)='111.2' ",false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $update	= $row->c;
			    $coa=KasKecil.$iarea;
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
			                       d_update='$update', i_update='$user' where i_periode='$xperiode' and i_coa='$coa'");
			  }else{
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $entry	= $row->c;
			    $coa=KasKecil.$iarea;
			    $nama='';
			    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $tes){
				      $nama=$tes->e_coa_name;
				    }
				    $query->free_result();
			    }
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" insert into tm_coa_saldo 
								   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
								    i_entry, e_coa_name)
								   values
								   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
			  }
		  }
		  $y->free_result();
	  }
*/
		###### end of kas kecil
		#####################	  
		#	  closing kas besar
		/*
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and substr(i_coa,1,5)='111.1' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $this->db->select(" sum(v_kb) as v_kb from tm_kb
						  where i_periode='$iperiode' and f_debet='t' and f_kb_cancel='f'",false);						 
	  $query = $this->db->get();
	  $kredit=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $kredit=$row->v_kb;
		  }
		  $query->free_result();
	  }
	  $this->db->select(" sum(v_kb) as v_kb from tm_kb
						  where i_periode='$iperiode' and f_debet='f' and f_kb_cancel='f'",false);							
	  $query = $this->db->get();
	  $debet=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $debet=$row->v_kb;
		  }
		  $query->free_result();
	  }
	  $saldo=$saldo+$debet-$kredit;
	  $coa=KasBesar;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
		if($debet=='' || $debet==null) $debet=0;
		if($kredit=='' || $kredit==null) $kredit=0;
	  $this->db->query(" update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
						 v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
						 and i_coa='$coa'");
	$emutasiperiode=$iperiode;
	$bldpn=substr($emutasiperiode,4,2)+1;
	if($bldpn==13)
	{
	  $perdpn=substr($emutasiperiode,0,4)+1;
	  $perdpn=$perdpn.'01';
	}else{
	  $perdpn=substr($emutasiperiode,0,4);
	  $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	}
	$xperiode=$perdpn;
	$this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and substr(i_coa,1,5)='111.1' ",false);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  $query 	= $this->db->query("SELECT current_timestamp as c");
	  $row   	= $query->row();
	  $update	= $row->c;
	  $coa=KasBesar;
	  $user=$this->session->userdata('user_id');
	  $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						 where i_periode='$xperiode' and i_coa='$coa'");
	}else{
	  $query 	= $this->db->query("SELECT current_timestamp as c");
	  $row   	= $query->row();
	  $entry	= $row->c;
	  $coa=KasBesar;
	  $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $tes){
		    $nama=$tes->e_coa_name;
		  }
		  $query->free_result();
	  }
	  $user=$this->session->userdata('user_id');
	  $this->db->query(" insert into tm_coa_saldo 
						 (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						  i_entry, e_coa_name)
						 values
						 ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	}
*/
		###### end of kas Besar
		#####################	  
		#	  closing Bank
		/*
	  $this->db->select(" distinct(i_coa_bank) as i_coa from tm_kbank 
	  					  where f_kbank_cancel='f' and to_char(d_bank,'yyyymm')='$iperiode' ",false);
	  $y = $this->db->get();
	  if ($y->num_rows() > 0){
		  foreach($y->result() as $w){
		  	$coa=$w->i_coa;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  $sawal=0;
			  $saldo=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $sawal=$row->v_saldo_awal;
					  $saldo=$row->v_saldo_awal;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" sum(v_bank) as v_bank from tm_kbank
								  where i_periode='$iperiode' and i_coa_bank='$coa' and f_debet='t' and f_kbank_cancel='f'",false);						 
			  $query = $this->db->get();
			  $kredit=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $kredit=$row->v_bank;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" sum(v_bank) as v_bank from tm_kbank
								  where i_periode='$iperiode' and i_coa_bank='$coa' and f_debet='f' and f_kbank_cancel='f'",false);							
			  $query = $this->db->get();
			  $debet=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $debet=$row->v_bank;
				  }
				  $query->free_result();
			  }
			  $saldo=$saldo+$debet-$kredit;
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$update	= $row->c;
				$user=$this->session->userdata('user_id');
			  if($debet=='' || $debet==null) $debet=0;
			  if($kredit=='' || $kredit==null) $kredit=0;
		    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
        			  					 v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
          								 and i_coa='$coa'");
			  $emutasiperiode=$iperiode;
			  $bldpn=substr($emutasiperiode,4,2)+1;
			  if($bldpn==13)
			  {
			    $perdpn=substr($emutasiperiode,0,4)+1;
			    $perdpn=$perdpn.'01';
			  }else{
			    $perdpn=substr($emutasiperiode,0,4);
			    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
			  }
			  $xperiode=$perdpn;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $update	= $row->c;
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
								   where i_periode='$xperiode' and i_coa='$coa'");
			  }else{
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $entry	= $row->c;
			    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $tes){
				      $nama=$tes->e_coa_name;
				    }
				    $query->free_result();
			    }
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" insert into tm_coa_saldo 
								   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
								    i_entry, e_coa_name)
								   values
								   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
			  }
		  }
		  $y->free_result();
	  }
*/
		###### end of Bank
		#####################	  
		#	  closing Biaya Penjualan
		/*
	  $this->db->select(" distinct(a.i_coa) as i_coa from (
	                      select distinct(i_coa) as i_coa from tm_kk where f_kk_cancel='f' and to_char(d_kk,'yyyymm')='$iperiode'
	                      and i_coa like '%612.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kb where f_kb_cancel='f' and to_char(d_kb,'yyyymm')='$iperiode' 
	                      and i_coa like '%612.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kbank where f_kbank_cancel='f' and to_char(d_bank,'yyyymm')='$iperiode' 
	                      and i_coa like '%612.%'
	                      ) as a
	                      ",false);
	  $y = $this->db->get();
	  if ($y->num_rows() > 0){
		  foreach($y->result() as $w){
		  	$coa=$w->i_coa;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  $sawal=0;
			  $saldo=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $sawal=$row->v_saldo_awal;
					  $saldo=$row->v_saldo_awal;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as penjualan
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $kredit=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $kredit=$row->penjualan;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as penjualan
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='t'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $debet=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $debet=$row->penjualan;
				  }
				  $query->free_result();
			  }
			  $saldo=$saldo+$debet-$kredit;
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$update	= $row->c;
				$user=$this->session->userdata('user_id');
			  if($debet=='' || $debet==null) $debet=0;
			  if($kredit=='' || $kredit==null) $kredit=0;
		    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
        			  					 v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
          								 and i_coa='$coa'");
			  $emutasiperiode=$iperiode;
			  $bldpn=substr($emutasiperiode,4,2)+1;
			  if($bldpn==13)
			  {
			    $perdpn=substr($emutasiperiode,0,4)+1;
			    $perdpn=$perdpn.'01';
			  }else{
			    $perdpn=substr($emutasiperiode,0,4);
			    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
			  }
			  $xperiode=$perdpn;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $update	= $row->c;
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
								   where i_periode='$xperiode' and i_coa='$coa'");
			  }else{
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $entry	= $row->c;
			    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $tes){
				      $nama=$tes->e_coa_name;
				    }
				    $query->free_result();
			    }
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" insert into tm_coa_saldo 
								   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
								    i_entry, e_coa_name)
								   values
								   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
			  }
		  }
		  $y->free_result();
	  }
*/
		###### end of Biaya Penjualan
		#####################	  
		#	  closing Pendapatan Lain2
		/*
	  $this->db->select(" distinct(a.i_coa) as i_coa from (
	                      select distinct(i_coa) as i_coa from tm_kk where f_kk_cancel='f' and to_char(d_kk,'yyyymm')='$iperiode'
	                      and i_coa like '%710.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kb where f_kb_cancel='f' and to_char(d_kb,'yyyymm')='$iperiode' 
	                      and i_coa like '%710.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kbank where f_kbank_cancel='f' and to_char(d_bank,'yyyymm')='$iperiode' 
	                      and i_coa like '%710.%'
	                      ) as a
	                      ",false);
	  $y = $this->db->get();
	  if ($y->num_rows() > 0){
		  foreach($y->result() as $w){
		  	$coa=$w->i_coa;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  $sawal=0;
			  $saldo=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $sawal=$row->v_saldo_awal;
					  $saldo=$row->v_saldo_awal;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as pendapatan
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $kredit=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $kredit=$row->pendapatan;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as pendapatan
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='t'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $debet=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $debet=$row->pendapatan;
				  }
				  $query->free_result();
			  }
			  $saldo=$saldo+$debet-$kredit;
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$update	= $row->c;
				$user=$this->session->userdata('user_id');
			  if($debet=='' || $debet==null) $debet=0;
			  if($kredit=='' || $kredit==null) $kredit=0;
		    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
        			  					 v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
          								 and i_coa='$coa'");
			  $emutasiperiode=$iperiode;
			  $bldpn=substr($emutasiperiode,4,2)+1;
			  if($bldpn==13)
			  {
			    $perdpn=substr($emutasiperiode,0,4)+1;
			    $perdpn=$perdpn.'01';
			  }else{
			    $perdpn=substr($emutasiperiode,0,4);
			    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
			  }
			  $xperiode=$perdpn;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $update	= $row->c;
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
								   where i_periode='$xperiode' and i_coa='$coa'");
			  }else{
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $entry	= $row->c;
			    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $tes){
				      $nama=$tes->e_coa_name;
				    }
				    $query->free_result();
			    }
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" insert into tm_coa_saldo 
								   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
								    i_entry, e_coa_name)
								   values
								   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
			  }
		  }
		  $y->free_result();
	  }
*/
		###### end of Pendapatan Lain2
		#####################	  
		#	  closing Bunga Bank
		/*
	  $this->db->select(" distinct(a.i_coa) as i_coa from (
	                      select distinct(i_coa) as i_coa from tm_kk where f_kk_cancel='f' and to_char(d_kk,'yyyymm')='$iperiode'
	                      and i_coa like '%720.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kb where f_kb_cancel='f' and to_char(d_kb,'yyyymm')='$iperiode' 
	                      and i_coa like '%720.%'
	                      union all
	                      select distinct(i_coa) as i_coa from tm_kbank where f_kbank_cancel='f' and to_char(d_bank,'yyyymm')='$iperiode' 
	                      and i_coa like '%720.%'
	                      ) as a
	                      ",false);
	  $y = $this->db->get();
	  if ($y->num_rows() > 0){
		  foreach($y->result() as $w){
		  	$coa=$w->i_coa;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  $sawal=0;
			  $saldo=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $sawal=$row->v_saldo_awal;
					  $saldo=$row->v_saldo_awal;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as BungaBank
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='f'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='f' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $kredit=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $kredit=$row->bungabank;
				  }
				  $query->free_result();
			  }
			  $this->db->select(" cast(sum(data.bank+data.kk+data.kb) as integer) as BungaBank
                            from(
                            select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
                            where to_char(d_bank,'yyyymm')='$iperiode' and i_coa='$coa' and f_kbank_cancel='f' and f_debet='t'
                            group by i_coa
                            union all
                            select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
                            where to_char(d_kk,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kk_cancel='f'
                            group by i_coa 
                            union all
                            select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
                            where to_char(d_kb,'yyyymm')='$iperiode' and i_coa='$coa' and f_debet='t' and f_kb_cancel='f'
                            group by i_coa 
                            )as data",false);						 
			  $query = $this->db->get();
			  $debet=0;
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
					  $debet=$row->bungabank;
				  }
				  $query->free_result();
			  }
			  $saldo=$saldo+$debet-$kredit;
				$query 	= $this->db->query("SELECT current_timestamp as c");
				$row   	= $query->row();
				$update	= $row->c;
				$user=$this->session->userdata('user_id');
			  if($debet=='' || $debet==null) $debet=0;
			  if($kredit=='' || $kredit==null) $kredit=0;
		    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$sawal, v_mutasi_debet=$debet, v_mutasi_kredit=$kredit, 
        			  					 v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
          								 and i_coa='$coa'");
			  $emutasiperiode=$iperiode;
			  $bldpn=substr($emutasiperiode,4,2)+1;
			  if($bldpn==13)
			  {
			    $perdpn=substr($emutasiperiode,0,4)+1;
			    $perdpn=$perdpn.'01';
			  }else{
			    $perdpn=substr($emutasiperiode,0,4);
			    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
			  }
			  $xperiode=$perdpn;
			  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $update	= $row->c;
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
								   where i_periode='$xperiode' and i_coa='$coa'");
			  }else{
			    $query 	= $this->db->query("SELECT current_timestamp as c");
			    $row   	= $query->row();
			    $entry	= $row->c;
			    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $tes){
				      $nama=$tes->e_coa_name;
				    }
				    $query->free_result();
			    }
			    $user=$this->session->userdata('user_id');
			    $this->db->query(" insert into tm_coa_saldo 
								   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
								    i_entry, e_coa_name)
								   values
								   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
			  }
		  }
		  $y->free_result();
	  }
*/
		###### end of Bunga Bank
		#####################
		#	  closing persediaanAkhir
		/*    
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='113.000';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum((n_opname+n_beli)*v_harga) as persediaanAkhir from tm_hpp where e_periode='$iperiode'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->persediaanakhir;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of persediaanAkhir
		#####################
		#	  closing hasilpenjualan
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='411.100';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_nota_gross)as gross from tm_nota where to_char(d_nota,'yyyymm')='$iperiode' and 
		                            f_nota_cancel='f' and not i_nota isnull");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->gross;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of hasilpenjualan
		#####################
		#	  closing returpenjualan
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='412.100';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_gross) as retur from tm_kn where to_char(d_kn,'yyyymm')='$iperiode' and f_kn_cancel='f'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->retur;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of returpenjualan
		#####################
		#	  closing potonganpenjualan
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)

	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{

	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='412.200';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;

	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;

			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }

	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_nota_discounttotal) as disc from tm_nota where to_char(d_nota,'yyyymm')='$iperiode' and 
		                            f_nota_cancel='f' and not i_nota isnull");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->disc;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of potonganpenjualan
		#####################
		#	  closing persediaanawal
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {

	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);

	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
#  	$coa='113.000';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);

	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){

		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;

#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum((n_opname+n_beli)*v_harga) as persediaanAkhir from tm_hpp where e_periode='$yperiode'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){

  		  $saldo	= $row->persediaanakhir;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");

		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;

	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;

	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();

	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{

	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,

						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of persediaanawal
		#####################
		#	  closing pembelianbarangjadi
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='511.100';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_gross) as ju from tm_dtap where f_dtap_cancel = 'f' AND to_char(d_dtap,'yyyymm')='$iperiode'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->ju;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 

      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){

		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of pembelianbarangjadi
		#####################
		#	  closing returpembelianbarangjadi
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='512.100';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;

#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_bbkretur) as ju from tm_bbkretur where f_bbkretur_cancel = 'f' 
		                            AND to_char(d_bbkretur,'yyyymm')='$iperiode'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->ju;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;

	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of returpembelianbarangjadi
		#####################
		#	  closing potonganpembelianbarangjadi
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
  	$coa='512.200';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(v_discount) as ju from tm_dtap where f_dtap_cancel = 'f' 
		                            AND to_char(d_dtap,'yyyymm')='$iperiode' ");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->ju;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of potonganpembelianbarangjadi

		/*
###### HPP
      if($iperiode>'201512'){
        $x=$this->db->query(" SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb_saldoakhir
                              ('$iperiode') a where not a.e_product_name isnull",false);
      }else{
        $x=$this->db->query(" SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git, case when i_store_location='PB' then 0 else n_git_penjualan end as 
                              n_git_penjualan, i_store, i_store_location, i_product, i_product_grade, i_product_motif, e_product_name
                              from f_mutasi_stock_daerah_all('$iperiode') where i_store<>'PB' and not e_product_name isnull
                              union all
                              SELECT n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_stockopname, case when i_store_location='PB' then 
                              0 else n_mutasi_git end as n_mutasi_git,
                              case when i_store_location='PB' then 0 else n_git_penjualan end as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_pusat
                              ('$iperiode') a where not a.e_product_name isnull
                              union all
                              SELECT n_saldo_akhir as n_saldo_stockopname, 0 as n_mutasi_git, 0 as n_git_penjualan, i_store, 
                              i_store_location, i_product, i_product_grade, i_product_motif, e_product_name from f_mutasi_stock_mo_pb
                              ('$iperiode') a where not a.e_product_name isnull",false);
      }
      if ($x->num_rows() > 0){
        $this->db->query("delete from tm_stockopname_gabungan where e_periode='$iperiode'",false);
        $i=0;
        foreach($x->result() as $xx){
          $i++;
          $jml      = $xx->n_saldo_stockopname;#+$xx->n_mutasi_git+$xx->n_git_penjualan;
          $iproduct = $xx->i_product;
          $nama     = $xx->e_product_name;
          $grade    = $xx->i_product_grade;
          $motif    = $xx->i_product_motif;
          $qu=$this->db->query("select * from tm_stockopname_gabungan
                                where e_periode='$iperiode' and i_product='$iproduct'",false);
          if ($qu->num_rows() > 0){
            $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                              where e_periode='$iperiode' and i_product='$iproduct'",false);
          }else{
            $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
            $row   	= $query->row();
            $entry	= $row->c;
            $this->db->query("insert into tm_stockopname_gabungan values
                              ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
          }
        }
        if($iperiode>'201512'){
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }else{
          $query=$this->db->query(" SELECT a.i_customer, case when (a.n_saldo_akhir-a.n_mutasi_git) < 0 then 0
                                    else (a.n_saldo_akhir-a.n_mutasi_git) end as n_saldo_stockopname, a.i_product, 
                                    a.i_product_grade, a.i_product_motif, a.e_product_name from f_mutasi_stock_mo_cust_all('$iperiode') a
                                    where not a.e_product_name isnull
                                    order by a.i_customer",false);
        }
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $tmp){
            $i++;
            $customer = $tmp->i_customer;
            $iproduct = $tmp->i_product;
            $grade    = $tmp->i_product_grade;
            $motif    = $tmp->i_product_motif;
            $jml      = $tmp->n_saldo_stockopname;
            $nama     = $tmp->e_product_name;              
            $qu=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct'",false);
            if ($qu->num_rows() > 0){
              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct'",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
            }
          }
	      }
        $qie=$this->db->query("select b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name, sum(b.n_quantity) as n_quantity
                               from tm_notapb a, tm_notapb_item b
                               where a.i_notapb=b.i_notapb and a.i_area=b.i_area and a.i_customer=b.i_customer and
                               to_char(a.d_notapb,'yyyymm')='$iperiode' and a.f_notapb_cancel='f'                               
                               group by b.i_product, b.i_product_grade, b.i_product_motif, b.e_product_name",false);
        if ($qie->num_rows() > 0){
          foreach($qie->result() as $mt){
            $i++;
            $iproduct = $mt->i_product;
            $grade    = $mt->i_product_grade;
            $motif    = $mt->i_product_motif;
            $jml      = $mt->n_quantity;
            $nama     = $mt->e_product_name;
            $qz=$this->db->query("select * from tm_stockopname_gabungan
                                  where e_periode='$iperiode' and i_product='$iproduct'",false);
            if ($qz->num_rows() > 0){
              $this->db->query("update tm_stockopname_gabungan set n_stockopname=n_stockopname+$jml
                                where e_periode='$iperiode' and i_product='$iproduct'",false);
            }else{
              $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
              $row   	= $query->row();
          	  $entry	= $row->c;
              $this->db->query("insert into tm_stockopname_gabungan values
                                ('$iperiode','$iproduct','$grade','$motif','$nama',$jml,'$entry',$i)",false);
            }
          }
        }
        $this->db->query("delete from tm_hpp where e_periode='$iperiode'");
#############################################################################################################################################
        $qu=$this->db->query("select * from tm_stockopname_gabungan where e_periode='$iperiode' ",false);
        if ($qu->num_rows() > 0){
          foreach($qu->result() as $xx){
            $jml=$xx->n_stockopname;
            $xx->e_product_name=str_replace("'","''",$xx->e_product_name);
            $hrg=0;
            $jmlbeli=0;
            $qi=$this->db->query("select * from f_history_beli('$iperiode','$xx->i_product')",false);
            if ($qi->num_rows() > 0){
              $jmlx=$jml;
              foreach($qi->result() as $yy){
                $hrg=$yy->hargabeli;
                $hrgdisc=(($yy->hargabeli*75)/100);
                $jmlasal=$jml-$jmlbeli;
                $jmlbeli=$jmlbeli+$yy->beli;
                if($jml>=$jmlbeli){
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$yy->beli, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                            '$xx->i_product_grade', '$xx->e_product_name', 0, $yy->beli, $hrg, 
                                            'f',$xx->n_stockopname)",false);
                  }
                }else{
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$hrg
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                            '$xx->i_product_grade', '$xx->e_product_name', 0, $jmlasal, $hrg, 
                                            'f',$xx->n_stockopname)",false);
                  }
                  break;
                }
              }
            }else{
#              $qa=$this->db->query("select v_product_mill from tr_harga_beli
#                                    where i_product='$xx->i_product' and i_price_group='00' 
#                                    and i_product_grade='$xx->i_product_grade'",false);
              $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                    where i_product='$xx->i_product' and i_price_group='00'",false);
              if ($qa->num_rows() > 0){
                foreach($qa->result() as $txt){
                  $pangaos=$txt->v_product_mill;
                  $qa=$this->db->query("select * from tm_hpp
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  if ($qa->num_rows() > 0){
                      $this->db->query("update tm_hpp set n_opname=n_opname+$jmlasal, n_opname_total=$xx->n_stockopname
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                  }else{
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0",false);
                    if ($qa->num_rows() == 0){
                      $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                        '$xx->i_product_grade', '$xx->e_product_name', 0, $jml, 0, 
                                        'f',$xx->n_stockopname)",false);
                    }
                  }
                }
              }else{
                $qa=$this->db->query("select * from tm_hpp
                                      where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=0
                                      and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                if ($qa->num_rows() == 0){
                  $this->db->query("insert into tm_hpp values ('$iperiode', '$xx->i_product', '$xx->i_product_motif', 
                                    '$xx->i_product_grade', '$xx->e_product_name', 0, $jml, 0, 
                                    'f',$xx->n_stockopname)",false);
                }
              }
            }
#            $qa=$this->db->query("select v_product_mill from tr_harga_beli
#                                  where i_product='$xx->i_product' and i_price_group='00'
#                                  and i_product_grade='$xx->i_product_grade'",false);
            $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                  where i_product='$xx->i_product' and i_price_group='00'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
                $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                  where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                  and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
              }
            }

#####yang ga ketemu harganya ambil dari master
            $qa=$this->db->query("select v_product_mill from tr_product where i_product='$xx->i_product'",false);
            if ($qa->num_rows() > 0){
              foreach($qa->result() as $txt){
                $this->db->query("update tm_hpp set v_harga=$txt->v_product_mill
                                  where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                  and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
              }
            }
##############################################

            $qz=$this->db->query("select i_product_a from tr_product_ab where i_product_b='$xx->i_product'",false);
            if ($qz->num_rows() > 0){
              foreach($qz->result() as $tx){
#                $qa=$this->db->query("select v_product_mill from tr_harga_beli
#                                      where i_product='$tx->i_product_a' and i_price_group='00'
#                                      and i_product_grade='$xx->i_product_grade'",false);
                $qa=$this->db->query("select v_product_mill from tr_harga_beli
                                      where i_product='$tx->i_product_a' and i_price_group='00'",false);
                if ($qa->num_rows() > 0){
                  foreach($qa->result() as $txt){
                    $pangaos=(($txt->v_product_mill*75)/100);
                    $qa=$this->db->query("select * from tm_hpp
                                          where e_periode='$iperiode' and i_product='$xx->i_product' and v_harga=$pangaos
                                          and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                    if ($qa->num_rows()==0){
                      $this->db->query("update tm_hpp set v_harga=$pangaos
                                        where e_periode='$iperiode' and i_product='$xx->i_product' and (v_harga=0 or v_harga isnull)
                                        and i_product_grade='$xx->i_product_grade' and i_product_motif='$xx->i_product_motif'",false);
                    }
                  }
                }
              }
            }
          } 
        }

        $qi=$this->db->query("select a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, sum(b.n_jumlah) as n_jumlah,
                              b.i_product_motif 
                              from tm_dtap a, tm_dtap_item b
                              where a.i_dtap=b.i_dtap and a.i_area=b.i_area and a.i_supplier=b.i_supplier 
                              and a.f_dtap_cancel='f' and to_char(a.d_dtap,'yyyymm')='$iperiode'
                              group by a.d_dtap, b.i_product, b.e_product_name, b.v_pabrik, b.i_product_motif
                              order by a.d_dtap desc",false);
        if ($qi->num_rows() > 0){
          foreach($qi->result() as $yy){
            $yy->e_product_name=str_replace("'","''",$yy->e_product_name);
            $beli=$yy->n_jumlah;
            $qa=$this->db->query("select * from tm_hpp
                                  where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik
                                  and i_product_motif='$yy->i_product_motif'",false);
            if ($qa->num_rows() > 0){
              $this->db->query("update tm_hpp set n_beli=n_beli+$beli, f_beli='t', v_harga=$yy->v_pabrik
                                where e_periode='$iperiode' and i_product='$yy->i_product' and v_harga=$yy->v_pabrik
                                and i_product_motif='$yy->i_product_motif'",false);
            }else{
              $this->db->query("insert into tm_hpp values ('$iperiode', '$yy->i_product', '00', 'A', '$yy->e_product_name', $yy->n_jumlah, 
                                0, $yy->v_pabrik, 't', 0)",false);
            }
          }
        }
#############################################################################################################################################
        $xy=$this->db->query("select * from tm_hpp where e_periode='$iperiode' order by e_product_name, i_product");
        if ($xy->num_rows() > 0){
          return $xy->result();
        }
		  }
*/
		######


		###### closing 
		$this->db->select(" * from tr_coa where not i_coa like '" . KasKecil . "%'", false);
		$que = $this->db->get();
		if ($que->num_rows() > 0) {
			foreach ($que->result() as $ro) {
				$this->db->select("v_saldo_akhir as jum from tm_coa_saldo where i_periode='$iperiode' and i_coa='$ro->i_coa'", false);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						if ($row->jum == null) $row->jum = 0;
						$query 	= $this->db->query("SELECT current_timestamp as c");
						$saldo	= $row->jum;
						$riw   	= $query->row();
						$update	= $riw->c;
						$coa = $ro->i_coa;
						$query = $this->db->query("select e_coa_name from tr_coa where i_coa like '$coa'");
						if ($query->num_rows() > 0) {
							foreach ($query->result() as $tes) {
								$coaname = $tes->e_coa_name;
							}
							$query->free_result();
						}
						#select data periode yang sudah ada
						$this->db->select("* from tm_coa_saldo where i_periode='$xperiode' and i_coa='$ro->i_coa'", false);
						$query = $this->db->get();
						if ($query->num_rows() == 0) {
							$query 	= $this->db->query("SELECT current_timestamp as c");
							$row   	= $query->row();
							$entry	= $row->c;
							$coa = $ro->i_coa;
							$user = $this->session->userdata('user_id');
							#update saldo akhir=saldo+debet-kredit (yang di update saldo awal, saldo akhir)
							$this->db->query(" insert into tm_coa_saldo 
							         (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
							          i_entry, e_coa_name)
							         values
							         ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$coaname')"), FALSE;
						} else {
							$query 	= $this->db->query("SELECT current_timestamp as c");
							$row   	= $query->row();
							$update	= $row->c;
							$coa = $ro->i_coa;
							$user = $this->session->userdata('user_id');
							$this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
		                             d_update='$update', i_update='$user' where i_periode='$xperiode' and i_coa='$coa'", FALSE);
						}
						$query->free_result();
					}
				}
			}
			$que->free_result();

			$coasm = PiutangDagangSementara;
			$coahl = HutangLain;

			$query = $this->db->query("select i_kbank,i_periode,v_sisa,i_coa_bank from tm_kbank 
                                 where to_char(d_bank,'yyyymm')='$iperiode' and v_sisa>0 and i_coa='$coasm' and f_kbank_cancel='f'
                                 and i_kbank like 'BM%'");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $rec) {
					$ikbank = $rec->i_kbank;
					$vsisa = $rec->v_sisa;
					$icoabank = $rec->i_coa_bank;

					$query = $this->db->query("select e_coa_name from tr_coa where i_coa like '$coahl'");
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $tes) {
							$coaname = $tes->e_coa_name;
						}
						$query->free_result();
					}
					$this->db->select("sum(v_sisa) as jum from tm_kbank 
                               where to_char(d_bank,'yyyymm')<='$iperiode' and i_coa='$coasm' and f_kbank_cancel='f'
                               and i_kbank like 'BM%'", false);
					$query = $this->db->get();
					if ($query->num_rows() > 0) {
						$row   	= $query->row();
						$saldo	= $row->jum;
						$this->db->select("* from tm_coa_saldo where i_periode='$xperiode' and i_coa='$coahl'", false);
						$query = $this->db->get();
						if ($query->num_rows() == 0) {
							$query 	= $this->db->query("SELECT current_timestamp as c");
							$row   	= $query->row();
							$entry	= $row->c;
							$user = $this->session->userdata('user_id');
							$this->db->query(" insert into tm_coa_saldo 
						             (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						              i_entry, e_coa_name)
						             values
						             ('$xperiode','$coahl',$saldo,0,0,$saldo,'$entry','$user','$coaname')", FALSE);
							$this->db->query(" insert into tm_alokasihl_reff 
                        (i_kbank, i_periode, v_bank, i_coa_bank)
                         values
                         ('$ikbank','$iperiode',$vsisa,'$icoabank')");
							$this->db->query("update tm_kbank set v_sisa=0 where to_char(d_bank,'yyyymm')<='$iperiode' and i_coa='$coasm' 
						                      and f_kbank_cancel='f' and i_kbank like 'BM%'", FALSE);
						} else {
							$query 	= $this->db->query("SELECT current_timestamp as c");
							$row   	= $query->row();
							$update	= $row->c;
							$this->db->query("update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
                                  d_update='$update', i_update='$user' where i_periode='$xperiode' and i_coa='$coahl'", FALSE);
							$this->db->query("update tm_kbank set v_sisa=0 where to_char(d_bank,'yyyymm')<='$iperiode' and i_coa='$coasm' 
                                  and f_kbank_cancel='f' and i_kbank like 'BM%'");
							$this->db->query("delete from tm_alokasihl_reff where i_kbank='$ikbank' and i_periode='$iperiode' and i_coa_bank='$icoabank'", FALSE);
							$this->db->query(" insert into tm_alokasihl_reff 
                          (i_kbank, i_periode, v_bank, i_coa_bank)
                          values
                          ('$ikbank','$iperiode',$vsisa,'$icoabank')", FALSE);
						}
					}
				}
				$query->free_result();
			}
		}
		/*
    $this->db->select(" * from tr_coa ",false);
    $que = $this->db->get();
	  if ($que->num_rows() > 0){
		  foreach($que->result() as $ro){
		   $this->db->select("v_saldo_akhir as jum from tm_coa_saldo where i_periode='$iperiode' and i_coa='$ro->i_coa'",false);
	      $query = $this->db->get();
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $row){
		        if($row->jum==null)$row->jum=0;
		        $query 	= $this->db->query("SELECT current_timestamp as c");
		        $saldo	= $row->jum;
            $riw   	= $query->row();
        	  $update	= $riw->c;
        	  $coa=$ro->i_coa;
            $query=$this->db->query("select e_coa_name from tr_coa where i_coa like '$coa'");
	          if ($query->num_rows() > 0){
		          foreach($query->result() as $tes){
		            $coaname=$tes->e_coa_name;
		          }
	            $query->free_result();
            }
#select data periode yang sudah ada
            $this->db->select("* from tm_coa_saldo where i_periode='$xperiode' and i_coa='$ro->i_coa'",false);
			      $query = $this->db->get();
		        if ($query->num_rows()== 0){
		          $query 	= $this->db->query("SELECT current_timestamp as c");
		          $row   	= $query->row();
		          $update	= $row->c;
		          $coa=$ro->i_coa;
		          $user=$this->session->userdata('user_id');
    #update saldo akhir=saldo+debet-kredit (yang di update saldo awal, saldo akhir)
		          $this->db->query(" insert into tm_coa_saldo 
							         (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
							          i_entry, e_coa_name)
							         values
							         ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$coaname')");
		        }else{
		          $query 	= $this->db->query("SELECT current_timestamp as c");
		          $row   	= $query->row();
		          $entry	= $row->c;
		          $coa=$ro->i_coa;
		          $user=$this->session->userdata('user_id');
		          $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
		                             d_update='$update', i_update='$user' where i_periode='$xperiode' and i_coa='$coa'");
		          }
		          $query->free_result();
            }
          }			  
        }
        $que->free_result();
      }
*/
		#####end
		#####################
		#	  closing hadiah
		/*
    $emutasiperiode=$iperiode;
	  $blsbl=substr($emutasiperiode,4,2)-1;
	  if($blsbl==0)
	  {
	    $persbl=substr($emutasiperiode,0,4)-1;
	    $persbl=$persbl.'12';
	  }else{
	    $persbl=substr($emutasiperiode,0,4);
	    $persbl=$persbl.substr($emutasiperiode,4,2)-1;;
	  }
	  $yperiode=$persbl;
#  	$coa='512.200';
	  $this->db->select(" v_saldo_akhir as v_saldo_awal from tm_coa_saldo where i_periode='$yperiode' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  $sawal=0;
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $sawal=$row->v_saldo_awal;
			  $saldo=$row->v_saldo_awal;
		  }
		  $query->free_result();
	  }
	  $kredit=0;
	  $debet=0;
#	  $saldo=$saldo+$debet-$kredit;
		$query 	= $this->db->query("select sum(n_quantity*v_unit_price) as ju from tm_bbk where f_bbk_cancel = 'f' 
		                            AND to_char(d_bbkretur,'yyyymm')='$iperiode' AND i_bbk_type='03'");
		if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
  		  $saldo	= $row->ju;
  		}
  		$query->free_result();
		}else{
		  $saldo	= 0;
		}
	  if($saldo=='' || $saldo==null) $saldo=0;
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$user=$this->session->userdata('user_id');
	  if($debet=='' || $debet==null) $debet=0;
	  if($kredit=='' || $kredit==null) $kredit=0;
    $this->db->query(" update tm_coa_saldo set v_saldo_akhir=$saldo, d_update='$update', i_update='$user' where i_periode='$iperiode' 
      								 and i_coa='$coa'");
	  $emutasiperiode=$iperiode;
	  $bldpn=substr($emutasiperiode,4,2)+1;
	  if($bldpn==13)
	  {
	    $perdpn=substr($emutasiperiode,0,4)+1;
	    $perdpn=$perdpn.'01';
	  }else{
	    $perdpn=substr($emutasiperiode,0,4);
	    $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
	  }
	  $xperiode=$perdpn;
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and i_coa='$coa' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){

	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $update	= $row->c;
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
						   where i_periode='$xperiode' and i_coa='$coa'");
	  }else{
	    $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $entry	= $row->c;
	    $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $tes){
		      $nama=$tes->e_coa_name;
		    }
		    $query->free_result();
	    }
	    $user=$this->session->userdata('user_id');
	    $this->db->query(" insert into tm_coa_saldo 
						   (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
						    i_entry, e_coa_name)
						   values
						   ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
	  }
*/
		###### end of hadiah

		##############done
		/*
select cast(sum(data.bank+data.kk+data.kb) as integer) as biayapenjualan
from(
select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
where to_char(d_bank,'yyyymm')='201409'
and i_coa like '%612.%' and
f_kbank_cancel='f' and f_debet='t'
group by i_coa
union all
select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
where to_char(d_kk,'yyyymm')='201409'
and i_coa like '%612.%' and f_debet='t' and
f_kk_cancel='f'
group by i_coa 
union all
select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
where to_char(d_kb,'yyyymm')='201409'
and i_coa like '%612.%' and f_debet='t' and
f_kb_cancel='f'
group by i_coa 
)as data

select cast(sum(data.bank+data.kk+data.kb) as integer) as pendapatan lain2
from(
select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
where to_char(d_bank,'yyyymm')='201409'
and i_coa like '%710.%' and
f_kbank_cancel='f' and f_debet='t'
group by i_coa
union all
select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
where to_char(d_kk,'yyyymm')='201409'
and i_coa like '%710.%' and f_debet='t' and
f_kk_cancel='f'
group by i_coa 
union all
select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
where to_char(d_kb,'yyyymm')='201409'
and i_coa like '%710.%' and f_debet='t' and
f_kb_cancel='f'
group by i_coa 
)as data

select cast(sum(data.bank+data.kk+data.kb) as integer) as BungaBank
from(
select i_coa, sum(v_bank) as bank, 0 as kk, 0 as kb from tm_kbank
where to_char(d_bank,'yyyymm')='201409'
and i_coa like '%720.%' and
f_kbank_cancel='f' and f_debet='t'
group by i_coa
union all
select i_coa, 0 as bank,sum(v_kk) as kk, 0 as kb from tm_kk
where to_char(d_kk,'yyyymm')='201409'
and i_coa like '%720.%' and f_debet='t' and
f_kk_cancel='f'
group by i_coa 
union all
select i_coa, 0 as bank,0 as kk, sum(v_kb) as kb from tm_kb
where to_char(d_kb,'yyyymm')='201409'
and i_coa like '%720.%' and f_debet='t' and
f_kb_cancel='f'
group by i_coa 
)as data

select sum((n_opname+n_beli)*v_harga) as persediaanAkhir from tm_hpp where e_periode='201409'

// hasil penjualan 
select sum(v_nota_gross) from tm_nota where to_char(d_nota,'yyyymm')='201509' and f_nota_cancel='f'  and not i_nota isnull

//return penjualan 
select sum(v_gross) from tm_kn where to_char(d_kn,'yyyymm')='201509' and f_kn_cancel='f'

//potongan penjualan
select sum(v_nota_discounttotal) from tm_nota where to_char(d_nota,'yyyymm')='201509' and f_nota_cancel='f' and not i_nota isnull

//persediaan awal barang jadi
select sum((n_opname+n_beli)*v_harga) as persediaan from tm_hpp where e_periode='201502'

//Pembelian barang jadi
select sum(v_gross) as jum from tm_dtap where f_dtap_cancel = 'f' AND to_char(d_dtap,'yyyymm')='201509'  

//Retur Pembelian barang jadi
select sum(v_bbkretur) as ju from tm_bbkretur where f_bbkretur_cancel = 'f' AND to_char(d_bbkretur,'yyyymm')='$iperiode'
		                            
//Potongan Pembelian barang jadi
select sum(v_discount) as ju from tm_dtap where f_dtap_cancel = 'f' AND to_char(d_dtap,'yyyymm')='$iperiode'
		                            
//Hadiah
select sum(n_quantity*v_unit_price) as ju from tm_bbk where f_bbk_cancel = 'f' 
		                            AND to_char(d_bbkretur,'yyyymm')='$iperiode' AND i_bbk_type='03'
*/
		################end done
		$this->db->query(" update tm_periode set i_periode='$xperiode'", false);
	}
}
