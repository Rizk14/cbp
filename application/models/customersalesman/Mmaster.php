<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }

    function baca($per, $icustomer, $iarea, $isalesman, $iproductgroup)
    {
      $this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name, e.e_product_groupname
               from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d, tr_product_group e
               where a.i_customer = b.i_customer
                     and a.i_area = c.i_area
                     and a.i_salesman = d.i_salesman
                     and a.i_product_group=e.i_product_group
                     and a.i_customer = '$icustomer'
                     and a.i_area = '$iarea' and a.i_salesman = '$isalesman'
                     and a.i_product_group = '$iproductgroup' and a.e_periode='$per'
               order by a.i_customer, a.i_area, a.i_salesman", false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->row();
      }
    }
    function cek_data($icustomer,$iarea,$isalesman,$iproductgroup,$esalesmanname,$iperiode){
      $this->db->query("select *from tr_customer_salesman where i_customer = '$icustomer' and i_area = '$iarea' and i_product_group = '$iproductgroup' and e_salesman_name = '$esalesmanname' and e_periode = '$iperiode'");
    }
    function insert($icustomer, $iarea, $isalesman, $iproductgroup, $esalesmanname, $iperiode)
    {
      $this->db->query("insert into tr_customer_salesman (i_customer,   i_salesman,  i_area, i_product_group, e_salesman_name, e_periode)
                                       values ('$icustomer', '$isalesman','$iarea','$iproductgroup', '$esalesmanname', '$iperiode')");
      #redirect('customersalesman/cform/');
    }

    function update($icustomer, $iarea, $isalesman, $iproductgroup, $esalesmanname, $iperiode)
    {
      $this->db->query("update tr_customer_salesman set i_customer = '$icustomer', i_area = '$iarea', i_salesman = '$isalesman', e_salesman_name='$esalesmanname', e_periode='$iperiode' where i_customer = '$icustomer' and i_area='$iarea' and i_product_group='$iproductgroup'");
      #redirect('customersalesman/cform/');
    }

    public function delete($icustomer, $iarea, $isalesman, $iproductgroup)
    {
      $this->db->query("   DELETE FROM tr_customer_salesman WHERE i_area='$iarea' and i_product_group='$iproductgroup' and i_customer='$icustomer'");
    }

    function bacasemua($per,$cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1   = $this->session->userdata('i_area');
        $area2 = $this->session->userdata('i_area2');
        $area3 = $this->session->userdata('i_area3');
        $area4 = $this->session->userdata('i_area4');
        $area5 = $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00')
      {
        /*$this->db->select("  a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                             where a.i_customer = b.i_customer
                             and a.i_area = c.i_area
                             and a.i_salesman = d.i_salesman
                             and a.i_area=b.i_area and a.i_area=d.i_area
                             and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                             or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                             and a.e_periode='$per' order by a.i_customer, a.i_area, a.i_salesman", false)->limit($num,$offset);*/
        $this->db->select("  a.*, b.e_customer_name, c.e_area_name, a.e_salesman_name
                             from tr_customer_salesman a, tr_customer b, tr_area c
                             where a.i_customer = b.i_customer
                             and a.i_area = c.i_area
                             and a.i_area=b.i_area
                             and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                             or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                             and a.e_periode='$per' order by a.i_customer, a.i_area, a.i_salesman", false)->limit($num,$offset);
      }
      else
      {
          /*$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                              from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                              where a.i_customer = b.i_customer
                              and a.i_area = c.i_area
                              and a.i_salesman = d.i_salesman
                              and a.i_area=b.i_area and a.i_area=d.i_area
                              and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                              or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                              and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                              or a.i_area = '$area4' or a.i_area = '$area5')
                              and a.e_periode='$per' order by a.i_customer, a.i_area, a.i_salesman",false)->limit($num,$offset);*/
          $this->db->select(" a.*, b.e_customer_name, c.e_area_name, a.e_salesman_name
                              from tr_customer_salesman a, tr_customer b, tr_area c
                              where a.i_customer = b.i_customer
                              and a.i_area = c.i_area
                              and a.i_area=b.i_area
                              and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                              or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                              and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                              or a.i_area = '$area4' or a.i_area = '$area5')
                              and a.e_periode='$per' order by a.i_customer, a.i_area, a.i_salesman",false)->limit($num,$offset);
      }
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
    }

    function bacasalesman($num,$offset)
    {
      $this->db->select("i_salesman, e_salesman_name from tr_salesman order by i_salesman",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacacustomerarea($num,$offset)
    {
      $this->db->select("* from tr_customer_area
               inner join tr_customer on (tr_customer_area.i_customer = tr_customer.i_customer)
               inner join tr_area on (tr_customer_area.i_area = tr_area.i_area)
               order by tr_customer_area.i_area, tr_customer_area.i_customer",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function cari($per, $cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      $area1   = $this->session->userdata('i_area');
        $area2 = $this->session->userdata('i_area2');
        $area3 = $this->session->userdata('i_area3');
        $area4 = $this->session->userdata('i_area4');
        $area5 = $this->session->userdata('i_area5');
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00')
      {
          /*$this->db->select(" a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                               from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                               where a.i_customer = b.i_customer
                              and a.i_area = c.i_area
                              and a.i_salesman = d.i_salesman and a.i_area=d.i_area
                              and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                              or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%') 
                               order by a.i_customer, a.i_area, a.i_salesman", false)->limit($num,$offset);*/
          $this->db->select(" a.*, b.e_customer_name, c.e_area_name, a.e_salesman_name from tr_customer_salesman a, 
                              tr_customer b, tr_area c where a.i_customer = b.i_customer and a.i_area = c.i_area
                              and (upper(a.e_salesman_name) ilike '%$cari%' or upper(a.i_customer) ilike '%$cari%'
                              or upper(a.i_salesman) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%' or 
                              upper(c.e_area_name) ilike '%$cari%') and a.e_periode='$per' order by a.i_customer, a.i_area, 
                              a.i_salesman ", false)->limit($num,$offset);
      }
      else
      {
          /*$this->db->select("  a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                 from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                 where a.i_customer = b.i_customer
                                    and a.i_area = c.i_area
                                    and a.i_salesman = d.i_salesman and a.i_area=d.i_area
                                    and (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(a.i_salesman) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5') order by a.i_customer, a.i_area, a.i_salesman
                            ",false)->limit($num,$offset);*/
          $this->db->select(" a.*, b.e_customer_name, c.e_area_name, a.e_salesman_name from tr_customer_salesman a, tr_customer b,
                              tr_area c where a.i_customer = b.i_customer and a.i_area = c.i_area and 
                              (upper(a.e_salesman_name) ilike '%$cari%' or upper(a.i_customer) ilike '%$cari%' or 
                              upper(a.i_salesman) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%' or 
                              upper(c.e_area_name) ilike '%$cari%') and (a.i_area = '$area1' or a.i_area = '$area2' or 
                              a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5') and a.e_periode='$per' 
                              order by a.i_customer, a.i_area, a.i_salesman
                            ",false)->limit($num,$offset);
      }
        $query = $this->db->get();
        if ($query->num_rows() > 0){
           return $query->result();
        }
    }
    function carisalesman($cari,$num,$offset)
    {
      $this->db->select("i_salesman, e_salesman_name from tr_salesman where upper(e_salesman_name) ilike '%$cari%' or upper(i_salesman) ilike '%$cari%' order by i_salesman",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function caricustomerarea($cari,$num,$offset)
    {
      $this->db->select("  * from tr_customer_area
               inner join tr_customer on (tr_customer_area.i_customer = tr_customer.i_customer)
               inner join tr_area on (tr_customer_area.i_area = tr_area.i_area)
               where upper(tr_customer_area.i_customer) ilike '%$cari%' or upper(tr_customer_area.i_area) ilike '%$cari%'
               or upper(tr_customer.e_customer_name) ilike '%$cari%' or upper(tr_area.e_area_name) ilike '%$cari%'
                  order by tr_customer_area.i_area, tr_customer_area.i_customer", false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaproductgroup($cari,$num,$offset)
    {
      $this->db->select("i_product_group, e_product_groupname from tr_product_group where upper(e_product_groupname) like '%$cari%' or upper(i_product_group) like '%$cari%' order by i_product_group",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cariproductgroup($cari,$num,$offset)
    {
      $this->db->select("i_product_group, e_product_groupname from tr_product_group where upper(e_product_groupname) like '%$cari%' or upper(i_product_group) like '%$cari%' order by i_product_group",false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
}
?>
