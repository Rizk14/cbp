<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='t' and a.f_close='f'
							order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_kk a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_kk) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='t' and a.f_close='f'
							order by a.i_area, a.i_kk desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function baca($ikk,$iperiode,$iarea)
	{
		$this->db->select("	a.i_kendaraan from tm_kk a 
							inner join tr_area b on(a.i_area=b.i_area)
							where a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->i_kendaraan;
			}
		}
		if($xxx==''){
			$this->db->select("	a.*, b.e_area_name, '' as e_pengguna from tm_kk a, tr_area b where a.i_area=b.i_area
								and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}else{
			$this->db->select("	a.*, b.e_area_name, c.e_pengguna
								from tm_kk a, tr_area b, tr_kendaraan c
								where a.i_area=b.i_area and a.i_kendaraan=c.i_kendaraan
								and a.i_periode='$iperiode' and a.i_kk='$ikk' and a.i_area='$iarea'",false);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader(	$ikk,$iarea,$dkk )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$ikk' and i_area='$iarea' and d_refference='$dkk'");
	}
	function deletetransitemdebet($accdebet,$ikn,$dkn)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$accdebet' and i_refference='$ikn' and d_refference='$dkn'");
	}
	function deletetransitemkredit($acckredit,$ikn,$dkn)
    {
		$this->db->query("delete from tm_jurnal_transharianitem 
						  where i_coa='$acckredit' and i_refference='$ikn' and d_refference='$dkn'");
	}
	function deletegldebet($accdebet,$ikn,$iarea,$dkn)
    {
		$this->db->query("delete from tm_general_ledger 
						  where i_refference='$ikn' and i_coa='$accdebet' and i_area='$iarea' and d_refference='$dkn'");
	}
	function deleteglkredit($acckredit,$ikn,$iarea,$dkn)
    {
		$this->db->query("delete from tm_general_ledger
						  where i_refference='$ikn' and i_coa='$acckredit' and i_area='$iarea' and d_refference='$dkn'");
	}
	function updatekk($ikk,$iarea,$iperiode)
    {
		$this->db->query("update tm_kk set f_posting='f' where i_kk='$ikk' and i_area='$iarea' and i_periode='$iperiode'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
