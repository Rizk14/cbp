<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacaperiode($dfrom,$dto,$iarea)
    {
        $sql =" a.i_area, e.e_area_name , a.i_nota ,a.i_customer, f.e_customer_name , b.i_product , b.e_product_name ,h.i_price_group, 
                b.v_unit_price, b.n_deliver,round(b.v_unit_price * b.n_deliver) as nota_kotor,
                round(a.n_nota_discount1+a.n_nota_discount2+a.n_nota_discount3+a.n_nota_discount4) as disc, 
                round(round(b.v_unit_price * b.n_deliver)*(round(a.n_nota_discount1+a.n_nota_discount2+a.n_nota_discount3+a.n_nota_discount4)/100 )) as disctot,
                round((b.n_deliver * b.v_unit_price)- ((b.n_deliver * b.v_unit_price)*(a.n_nota_discount1/100))- (((b.n_deliver * b.v_unit_price) 
                - ((b.n_deliver * b.v_unit_price)*(a.n_nota_discount1/100)))*(a.n_nota_discount2/100))
                - ((((b.n_deliver * b.v_unit_price)-(b.n_deliver * b.v_unit_price)*(a.n_nota_discount1/100))-(((b.n_deliver * b.v_unit_price)-
                ((b.n_deliver * b.v_unit_price)*(a.n_nota_discount1/100)))*(a.n_nota_discount2/100)))* a.n_nota_discount3))as v_nota_netto,
                g.i_supplier, c.e_supplier_name, d.v_product_mill as harga_beli,d.i_price_group as id, a.d_nota,a.i_sj
                from tm_nota a , tm_nota_item b , tr_supplier c , tr_harga_beli d, tr_area e , tr_customer f , tr_product g, tm_spb h
                where a.i_sj=b.i_sj and a.i_area=b.i_area
                and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                and a.f_nota_cancel='false'
                and g.i_product=d.i_product
                and g.i_supplier=c.i_supplier
                and b.i_product=d.i_product
                and a.i_area=e.i_area
                and a.i_customer=f.i_customer
                and h.i_nota = a.i_nota";
        if($iarea!='NA') $sql.=" and a.i_area ='$iarea' order by i_area, i_nota , i_product , id desc ";
								else $sql.=" order by i_area, i_nota , i_product , id desc ";
        
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
