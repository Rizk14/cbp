<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iicconvertion)
    {
		  $this->db->select(" a.*, b.i_bbk, c.i_bbm  from tm_ic_convertion a, tm_bbk b, tm_bbm c
							            where i_ic_convertion = '$iicconvertion'
							            and a.i_ic_convertion=b.i_refference_document
                          and c.i_bbm_type='03'
							            and a.i_ic_convertion=c.i_refference_document
                          and b.i_bbk_type='04'", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($iicconvertion)
    {
		  $this->db->select("* from tm_ic_convertionitem
			                   where i_ic_convertion = '$iicconvertion'
			                   order by i_product", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacabbm($cari,$num,$offset,$iuser)
    {
		  $this->db->select(" a.*, b.e_area_name
                          from tm_bbm a, tr_area b
                          where a.i_area=b.i_area 
                          and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.f_bbm_cancel='f' and a.i_bbm_type='05'
                          and a.i_bbm not in (select i_refference from tm_ic_convertion where a.i_bbm=tm_ic_convertion.i_refference)
                          and a.f_bbm_convertion='f' and a.i_bbm like '%-1712-%'
                          order by a.d_bbm DESC, a.i_area ASC, ",false)->limit($num,$offset);
#and a.i_area=d.i_area 
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caribbm($cari,$num,$offset,$iuser)
    {
		  $this->db->select(" a.*, b.e_area_name
                          from tm_bbm a, tr_area b
                          where a.i_area=b.i_area and (a.i_bbm like '%$cari%')
                          and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
                          and a.f_bbm_cancel='f' and a.i_bbm_type='05'
                          and a.i_bbm not in (select i_refference from tm_ic_convertion where a.i_bbm=tm_ic_convertion.i_refference)
                          and a.f_bbm_convertion='f' and a.i_bbm like '%-1712-%'
                          order by a.d_bbm DESC, a.i_area ASC, ",FALSE)->limit($num,$offset);
  #and a.i_area=d.i_area 
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacabbmdetail($irefference)
    {
		  $query=$this->db->query(" select a.i_product, a.i_product_motif, a.e_product_motifname, c.e_product_name,
                                b.v_unit_price, b.n_quantity, b.i_product_grade
                                from tm_bbm_item b 
                                left join tm_bbm e on ( e.i_bbm=b.i_bbm and e.i_bbm_type = b.i_bbm_type )
                                left join tr_product_motif a on ( b.i_product_motif=a.i_product_motif and b.i_product=a.i_product )
                                left join tr_product c on ( a.i_product=c.i_product )
                                where b.i_bbm='$irefference'  and e.i_bbm_type='05' ",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function insertheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
						  $eproductname,$iproductmotif,$ficconvertion,$nicconvertion,$irefference,$drefference)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
    			'i_ic_convertion' 	=> $iicconvertion,
    			'd_ic_convertion' 	=> $dicconvertion,
				  'i_product'			    => $iproduct,
				  'i_product_motif'	  => $iproductmotif,
				  'i_product_grade'	  => 'A',
				  'e_product_name'	  => $eproductname,
				  'f_ic_convertion'	  => $ficconvertion,
				  'n_ic_convertion'	  => $nicconvertion,
          'i_refference'      => $irefference,
          'd_refference'      => $drefference,
  				'd_entry'           => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_ic_convertion');
    }
    function insertdetail($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
						  $eproductname,$iproductmotif,$nicconvertion)
    {
    	$this->db->set(
    		array(
    			'i_ic_convertion' 	=>$iicconvertion,
				  'i_product'			    =>$iproduct,
				  'i_product_motif'	  =>$iproductmotif,
				  'i_product_grade'	  =>'B',
				  'e_product_name'	  =>$eproductname,
				  'n_ic_convertion'	  =>$nicconvertion
    		)
    	);
    	$this->db->insert('tm_ic_convertionitem');
    }
	function insertbbkheader($iicconvertion,$dicconvertion,$ibbk,$dbbk,$ibbktype,$eremark,$iarea)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_bbk'					        => $ibbk,
				'i_bbk_type'			      => $ibbktype,
				'i_refference_document'	=> $iicconvertion,
				'd_refference_document'	=> $dicconvertion,
				'd_bbk'					        => $dbbk,
				'e_remark'				      => $eremark,
				'i_area'				        => $iarea,
				'd_entry'               => $dentry
    		)
    	);
    	$this->db->insert('tm_bbk');
    }
	function insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,$vunitprice,$iicconvertion,$ibbk,$eremark,$ibbktype)
    {
    	$this->db->set(
    		array(
				'i_bbk'					=> $ibbk,
				'i_bbk_type'					=> $ibbktype,
				'i_refference_document'	=> $iicconvertion,
				'i_product'				=> $iproduct,
				'i_product_motif'		=> $iproductmotif,
				'i_product_grade'		=> $iproductgrade,
				'e_product_name'		=> $eproductname,
				'n_quantity'			=> $nicconvertion,
				'v_unit_price'			=> $vunitprice,
				'e_remark'				=> $eremark
    		)
    	);
    	$this->db->insert('tm_bbk_item');
    }
	function updatestockbbk($iproduct,$iproductgrade,$iproductmotif,$nicconvertion,$istore,$istorelocation,$istorelocationbin)
    {
    	$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock-$nicconvertion
						  where i_product='$iproduct' and i_product_motif='$iproductmotif'
						  and i_product_grade='$iproductgrade' and i_store='$istore'
						  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
    }
	function insertbbmheader($iicconvertion,$dicconvertion,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_bbm'					        => $ibbm,
				'i_bbm_type'			      => $ibbmtype,
				'i_refference_document'	=> $iicconvertion,
				'd_refference_document'	=> $dicconvertion,
				'd_bbm'					        => $dbbm,
				'e_remark'				      => $eremark,
				'i_area'				        => $iarea,
				'f_bbm_convertion'      => 't',
				'd_entry'               => $dentry
    		)
    	);
    	$this->db->insert('tm_bbm');
    }
	function insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,$vunitprice,$iicconvertion,$ibbm,$eremark,$ibbmtype)
    {
    	$this->db->set(
    		array(
				'i_bbm'			        		=> $ibbm,
				'i_bbm_type'  					=> $ibbmtype,
				'i_refference_document'	=> $iicconvertion,
				'i_product'	      			=> $iproduct,
				'i_product_motif'   		=> $iproductmotif,
				'i_product_grade'   		=> $iproductgrade,
				'e_product_name'		    => $eproductname,
				'n_quantity'      			=> $nicconvertion,
				'v_unit_price'		    	=> $vunitprice,
				'e_remark'			      	=> $eremark
    		)
    	);
    	$this->db->insert('tm_bbm_item');
    }
	function updatestockbbm($iproduct,$iproductgrade,$iproductmotif,$nicconvertion,$istore,$istorelocation,$istorelocationbin)
    {
    	$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$nicconvertion
						  where i_product='$iproduct' and i_product_motif='$iproductmotif'
						  and i_product_grade='$iproductgrade' and i_store='$istore'
						  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
    }
    function insertinserttmictrans($iproduct, $iproductgrade, $eproductname, $nstockopname, $istockopname, $istockopnameitem)
    {
	}
	function updateheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
						  $eproductname,$iproductmotif,$ficconvertion,$nicconvertion)
    {
    	$this->db->set(
    		array(
    			'd_ic_convertion' 	=>$dicconvertion,
				'i_product'			=>$iproduct,
				'i_product_motif'	=>$iproductmotif,
				'i_product_grade'	=>$iproductgrade,
				'e_product_name'	=>$eproductname,
				'f_ic_convertion'	=>$ficconvertion,
				'n_ic_convertion'	=>$nicconvertion
    		)
    	);
    	$this->db->where('i_ic_convertion',$iicconvertion);
    	$this->db->update('tm_ic_convertion');
    }
    public function deletedetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
								 $istorelocation,$istorelocationbin) 
    {
		$this->db->query("DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion='$iicconvertion'
						  and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						  and i_product_grade='$iproductgrade'");
		if($ficconvertion=='t'){
			$this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$iicconvertion'
							  and i_product='$iproduct' and i_product_motif='$iproductmotif' 
							  and i_product_grade='$iproductgrade'");
			$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock-$nicconvertion
							  where i_product='$iproduct' and i_product_motif='$iproductmotif'
							  and i_product_grade='$iproductgrade' and i_store='$istore'
							  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
		}else{
			$this->db->query("DELETE FROM tm_bbk_item WHERE i_refference_document='$iicconvertion'
							  and i_product='$iproduct' and i_product_motif='$iproductmotif' 
							  and i_product_grade='$iproductgrade'");
			$this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$nicconvertion
							  where i_product='$iproduct' and i_product_motif='$iproductmotif'
							  and i_product_grade='$iproductgrade' and i_store='$istore'
							  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
		}
		return TRUE;
    }
    public function updatebbkdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
								 $istorelocation,$istorelocationbin,$nicconvertionx) 
    {
      $rr=null;
      $qq 		= $this->db->query("SELECT a.d_ic_convertion,b.n_ic_convertion 
                                  FROM tm_ic_convertion a, tm_ic_convertionitem b 
                                  WHERE a.i_ic_convertion=b.i_ic_convertion and a.i_ic_convertion='$iicconvertion'
						                      and b.i_product='$iproduct' and b.i_product_motif='$iproductmotif' 
						                      and b.i_product_grade='$iproductgrade'",false);
      if ($qq->num_rows() > 0){
    	  $rr   		= $qq->row();
      }else{
        $qq 		= $this->db->query("SELECT a.d_ic_convertion,b.n_ic_convertion 
                                    FROM tm_ic_convertion a, tm_ic_convertionitem b 
                                    WHERE a.i_ic_convertion=b.i_ic_convertion and a.i_ic_convertion='$iicconvertion'
						                        and a.i_product='$iproduct' and a.i_product_motif='$iproductmotif' 
						                        and a.i_product_grade='$iproductgrade'",false);
        if ($qq->num_rows() > 0){
      	  $rr   		= $qq->row();
        }
      }
		  $this->db->query("DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion='$iicconvertion'
						    and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						    and i_product_grade='$iproductgrade'");
		  $this->db->query("DELETE FROM tm_bbk_item WHERE i_refference_document='$iicconvertion'
						    and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						    and i_product_grade='$iproductgrade'");
##############
      $queri 		= $this->db->query("SELECT e_product_name, n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
#and i_refference_document='$iicconvertion'
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        if(trim($row->e_product_name)==''){
          $quer 		= $this->db->query("SELECT e_product_name FROM tr_product where i_product='$iproduct' ",false);
          if ($quer->num_rows() > 0){
        	  $rw   		= $quer->row();
            $row->e_product_name=$rw->e_product_name;
          }
        }
        if($nicconvertionx!='' || $nicconvertionx!=NULL){
          $que 	= $this->db->query("SELECT current_timestamp as c");
          $ro 	= $que->row();
          $now	 = $ro->c;
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$row->e_product_name', '$iicconvertion', '$now', $nicconvertionx, 0, $row->n_quantity_akhir+$nicconvertionx, $row->n_quantity_akhir
                                  )
                                ",false);

      //}
      $th=substr($rr->d_ic_convertion,0,4);
	    $bl=substr($rr->d_ic_convertion,5,2);
	    $emutasiperiode=$th.$bl;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbk=n_mutasi_bbk-$nicconvertionx, n_saldo_akhir=n_saldo_akhir+$nicconvertionx
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$nicconvertionx
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);

##############
        }
      }
  		return TRUE;
    }
	public function updatebbmdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
								 $istorelocation,$istorelocationbin,$nicconvertionx) 
    {
      $rr=null;

      $qq 		= $this->db->query("SELECT a.d_ic_convertion,b.n_ic_convertion 
                                  FROM tm_ic_convertion a, tm_ic_convertionitem b 
                                  WHERE a.i_ic_convertion=b.i_ic_convertion and a.i_ic_convertion='$iicconvertion'
						                      and b.i_product='$iproduct' and b.i_product_motif='$iproductmotif' 
						                      and b.i_product_grade='$iproductgrade'",false);
      if ($qq->num_rows() > 0){
    	  $rr   		= $qq->row();
      }else{
        $qq 		= $this->db->query("SELECT a.d_ic_convertion,b.n_ic_convertion 
                                    FROM tm_ic_convertion a, tm_ic_convertionitem b 
                                    WHERE a.i_ic_convertion=b.i_ic_convertion and a.i_ic_convertion='$iicconvertion'
						                        and a.i_product='$iproduct' and a.i_product_motif='$iproductmotif' 
						                        and a.i_product_grade='$iproductgrade'",false);
        if ($qq->num_rows() > 0){
      	  $rr   		= $qq->row();
        }
      }
		  $this->db->query("DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion='$iicconvertion'
						    and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						    and i_product_grade='$iproductgrade'");
		  $this->db->query("DELETE FROM tm_bbm_item WHERE i_refference_document='$iicconvertion'
						    and i_product='$iproduct' and i_product_motif='$iproductmotif' 
						    and i_product_grade='$iproductgrade'");
#		  $this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock-$nicconvertionx
#			  			  where i_product='$iproduct' and i_product_motif='$iproductmotif'
#			  			  and i_product_grade='$iproductgrade' and i_store='$istore'
#			  			  and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'",false);
##############
      $queri 		= $this->db->query("SELECT e_product_name, n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
# and i_refference_document='$iicconvertion'
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
        $ro 	= $que->row();
        $now	 = $ro->c;
        $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$row->e_product_name', '$iicconvertion', '$now', 0, $nicconvertionx, $row->n_quantity_akhir-$nicconvertionx, $row->n_quantity_akhir
                                )
                              ",false);
      }
      $th=substr($rr->d_ic_convertion,0,4);
	    $bl=substr($rr->d_ic_convertion,5,2);
	    $emutasiperiode=$th.$bl;
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$nicconvertionx, n_saldo_akhir=n_saldo_akhir-$nicconvertionx
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$nicconvertionx
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);

##############
  		return TRUE;
    }
    public function delete($iicconvertion) 
    {
		$this->db->query('DELETE FROM tm_ic_convertion WHERE i_ic_convertion=\''.$iicconvertion.'\'');
		$this->db->query('DELETE FROM tm_ic_convertionitem WHERE i_ic_convertion=\''.$iicconvertion.'\'');
		return TRUE;
    }
    function bacaproduct($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.v_product_retail from tm_ic a, tr_product b
							          where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							          and a.i_store='AA' and a.i_store_location='01' and (substring(a.i_product,2,6)=substring(b.i_product,2,6))
							          order by a.i_product, a.i_product_grade ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($thbl){
#	   	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
#		  $row   	= $query->row();
#		  $thbl	= $row->c;
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_ic_convertion,9,6)) as max from tm_ic_convertion
				    where substr(i_ic_convertion,4,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<6){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="KS-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="000001";
			  $noso  ="KS-".$thbl."-".$noso;
			  return $noso;
		  }
    }
    function runningnumberbbk($thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BBK'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobbk  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nobbk
                            where i_modul='BBK'
                            and i_area='00'
                            and substring(e_periode,1,4)='$th'", false);
			  settype($nobbk,"string");
			  $a=strlen($nobbk);
			  while($a<6){
			    $nobbk="0".$nobbk;
			    $a=strlen($nobbk);
			  }
			  	$nobbk  ="BBK-".$thbl."-".$nobbk;
			  return $nobbk;
		  }else{
			  $nobbk  ="000001";
		  	$nobbk  ="BBK-".$thbl."-".$nobbk;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BBK','00','$asal',1)");
			  return $nobbk;
		  }
/*
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_bbk,10,6)) as max from tm_bbk where substr(i_bbk,5,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<6){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="BBK-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="000001";
			  $noso  ="BBK-".$thbl."-".$noso;
			  return $noso;
		  }
*/
    }
    function runningnumberbbm($thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BBM'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobbm  =$terakhir+1;
        $this->db->query(" update tm_dgu_no 
                            set n_modul_no=$nobbm
                            where i_modul='BBM'
                            and i_area='00'
                            and substring(e_periode,1,4)='$th'", false);
			  settype($nobbm,"string");
			  $a=strlen($nobbm);
			  while($a<6){
			    $nobbm="0".$nobbm;
			    $a=strlen($nobbm);
			  }
			  	$nobbm  ="BBM-".$thbl."-".$nobbm;
			  return $nobbm;
		  }else{
			  $nobbm  ="000001";
		  	$nobbm  ="BBM-".$thbl."-".$nobbm;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BBM','00','$asal',1)");
			  return $nobbm;
		  }
/*
		  $th		= substr($thbl,0,2);
		  $this->db->select(" max(substr(i_bbm,10,6)) as max from tm_bbm where substr(i_bbm,5,2)='$th' ", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $noso  =$terakhir+1;
			  settype($noso,"string");
			  $a=strlen($noso);
			  while($a<6){
			    $noso="0".$noso;
			    $a=strlen($noso);
			  }
			  $noso  ="BBM-".$thbl."-".$noso;
			  return $noso;
		  }else{
			  $noso  ="000001";
			  $noso  ="BBM-".$thbl."-".$noso;
			  return $noso;
		  }
*/
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_ic_convertion 
							where upper(i_ic_convertion) like '%$cari%' order by i_ic_convertion",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		  $this->db->select(" a.*, b.v_product_retail from tm_ic a, tr_product b 
							  where a.i_store='AA' and a.i_store_location='01'
						    	and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							  and a.i_product=b.i_product ",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){

				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ido,$q_in,$q_out,$qdo,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ido', '$now', $qdo, 0, $q_ak+$qdo, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qdo, n_saldo_akhir=n_saldo_akhir+$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',0,0,0,$qdo,0,0,0,$qdo,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qdo)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname',$qdo, 't'
                                )
                              ",false);
    }
    function inserttransbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbm,$q_in,$q_out,$qbbm,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbm', '$now', $qbbm, 0, $q_ak+$qbbm, $q_ak
                                )
                              ",false);
    }
    function updatemutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qbbm, n_saldo_akhir=n_saldo_akhir+$qbbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbm,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,$qbbm,0,0,0,$qbbm,0,'f')
                              ",false);
    }
    function updateicbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbm,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qbbm
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbbm)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qbbm, 't'
                                )
                              ",false);
    }
    function inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$qbbk,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbk', '$now', 0, $qbbk, $q_ak-$qbbk, $q_ak
                                )
                              ",false);
    }
    function updatemutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qbbk, n_saldo_akhir=n_saldo_akhir-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
			                            n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,0,0,0,$qbbk,$qbbk,0,'f')
                              ",false);
    }
    function updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qbbk, n_saldo_akhir=n_saldo_akhir-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',$qbbk,0,0,0,0,0,$qbbk,0,0,'f')
                              ",false);
    }
    function updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbbk)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', 0, 't'
                                )
                              ",false);
    }
    function inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ido,$q_in,$q_out,$qdo,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ido', '$now', 0, $qdo, $q_ak-$qdo, $q_ak
                                )
                              ",false);
    }
    function updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qdo, n_saldo_akhir=n_saldo_akhir-$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qdo,0,0,0,0,0,$qdo,0,0,'f')
                              ",false);
    }
    function updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qdo,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qdo
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qdo)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname',0, 't'
                                )
                              ",false);
    }
}
?>
