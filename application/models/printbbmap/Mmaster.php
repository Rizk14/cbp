<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iop) 
    {
			$this->db->query('DELETE FROM tm_ap WHERE i_ap=\''.$iop.'\'');
			$this->db->query('DELETE FROM tm_ap_item WHERE i_ap=\''.$iop.'\'');
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_supplier_name, c.e_area_name from tm_ap a, tr_supplier b, tr_area c
								where a.i_supplier=b.i_supplier and a.i_area=c.i_area
								and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
								or upper(a.i_ap) like '%$cari%' or upper(a.i_area) like '%$cari%' 
								or upper(c.e_area_name) like '%$cari%') and (a.i_area='$area1' or a.i_area like '%$area2%'
								or a.i_area like '%$area3%' or a.i_area like '%$area4%' or a.i_area like '%$area5%') order by a.i_ap desc",false)->limit($num,$offset);

			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($iap,$area)
    {
			$this->db->select(" * from tm_ap
								inner join tr_supplier on (tm_ap.i_supplier=tr_supplier.i_supplier)
								inner join tr_area on (tm_ap.i_area=tr_area.i_area)
								where tm_ap.i_ap = '$iap' and tm_ap.i_area='$area'
								order by tm_ap.i_ap desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacadetail($iap,$area)
    {
			$this->db->select(" * from tm_ap_item
								inner join tr_product_motif on (tm_ap_item.i_product_motif=tr_product_motif.i_product_motif
															and tm_ap_item.i_product=tr_product_motif.i_product)
								where tm_ap_item.i_ap = '$iap' order by tm_ap_item.i_ap desc",false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_supplier_name, c.e_area_name from tm_ap a, tr_supplier b, tr_area c
							where a.i_supplier=b.i_supplier and a.i_area=c.i_area
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_ap) like '%$cari%' or upper(a.i_area) like '%$cari%' or upper(c.e_area_name) like '%$cari%')
							order by a.i_ap desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
