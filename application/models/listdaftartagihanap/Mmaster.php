<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($idt,$iarea,$isupplier) 
    {
	      $sql	= " select i_supplier_group, i_supplier FROM TR_SUPPLIER where i_supplier = '$isupplier'";
	      $rs		= pg_query($sql);
	      while($row=pg_fetch_assoc($rs)){
		      $igroup   = $row['i_supplier_group'];
          if($igroup=='G0000'){
            $sql	= " select * from tm_dtap_item where i_dtap='$idt'";
            $rs		= pg_query($sql);
            while($row=pg_fetch_assoc($rs)){
	            $iap    = $row['i_do'];
              $sql2	  = "update tm_ap set f_ap_cancel='t' where i_ap='$iap' and i_supplier ='$isupplier'";
          		pg_query($sql2);
              }
            }
    		$this->db->query("update tm_dtap set f_dtap_cancel='t' WHERE i_dtap='$idt' and i_area='$iarea' and i_supplier='$isupplier'");

        $sql	= " select * from tm_dtap_item where i_dtap='$idt'";
        $rs		= pg_query($sql);
        while($row=pg_fetch_assoc($rs)){
          $ido        = $row['i_do'];
          $isupplier  = $row['i_supplier'];
          $iop        = $row['i_op'];
          $sql2	  = "update tm_do set i_faktur='' where i_do='$ido' and i_op='$iop' and i_supplier ='$isupplier'";
      		pg_query($sql2);
          }

      }
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							order by a.i_dtap desc",false)->limit($num,$offset);
		$query = $this->db->get(); 
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_supplier_name from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							order by a.i_dtap desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
    	if($isupplier!='AS'){
		$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group from tm_dtap a, tr_area b, tr_supplier c
							          where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%')
							          and a.i_supplier='$isupplier' and
							          a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_dtap ",false)->limit($num,$offset);
		}else{

			$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group from tm_dtap a, tr_area b, tr_supplier c
							          where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%') and
							          a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							          ORDER BY a.i_supplier, a.i_dtap",false);

		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group from tm_dtap a, tr_area b, tr_supplier c
							where a.i_area=b.i_area and a.i_supplier=c.i_supplier
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_dtap) like '%$cari%')
							and a.i_supplier='$isupplier' and
							a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_dtap ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select(" * from tr_supplier order by i_supplier",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
							order by i_supplier",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
