<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerservice)
    {
		$this->db->select('i_customer_service, e_customer_servicename')->from('tr_customer_service')->where('i_customer_service', $icustomerservice);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
	}
    function insert($icustomerservice, $ecustomerservicename)
    {
    	$this->db->set(
    		array(
    			'i_customer_service' => $icustomerservice,
    			'e_customer_servicename' => $ecustomerservicename
    		)
    	);
    	
    	$this->db->insert('tr_customer_service');
    }
    function update($icustomerservice, $ecustomerservicename)
    {
    	$data = array(
               'i_customer_service' => $icustomerservice,
               'e_customer_servicename' => $ecustomerservicename
            );
		$this->db->where('i_customer_service', $icustomerservice);
		$this->db->update('tr_customer_service', $data); 
    }
	
    public function delete($icustomerservice) 
    {
		$this->db->query('DELETE FROM tr_customer_service WHERE i_customer_service=\''.$icustomerservice.'\'');
    }
    function bacasemua()
    {
		$this->db->select("i_customer_service, e_customer_servicename from tr_customer_service order by i_customer_service", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
