<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
		  $this->db->select("  data.i_supplier, data.e_supplier_name, sum(data.jmlbeli) as beli, sum(data.sisa_hutang) as hutang, sum(data.bayar) as bayar
                            from(
                            select a.i_supplier, a.e_supplier_name, 0 as jmlbeli, 0 as sisa_hutang, 0 as bayar from tr_supplier a
                            union all
                            select a.i_supplier, c.e_supplier_name, 0 as jmlbeli, sum(a.v_sisa) as sisa_hutang, 0 as bayar from tm_dtap a, tr_supplier c 
                            where a.i_supplier=c.i_supplier and
                            to_char(a.d_dtap,'yyyymm')='201603' and a.f_dtap_cancel=FALSE
                            group by a.i_supplier, c.e_supplier_name
                            union all 
                            select a.i_supplier, c.e_supplier_name, sum(a.v_netto) as jmlbeli, 0 as sisa_hutang, 0 as bayar from tm_dtap a, tr_supplier c
                            where a.i_supplier=c.i_supplier and
                            to_char(a.d_dtap,'yyyymm')='201603'
                            group by a.i_supplier, c.e_supplier_name
                            union all
                            select  b.i_supplier, c.e_supplier_name, 0 as jmlbeli, 0 as sisa_hutang, sum(b.v_jumlah) as bayar from tm_alokasi_bk a, 
                            tm_alokasi_bk_item b, tr_supplier c 
                            where a.i_alokasi=b.i_alokasi and
                            a.i_supplier=c.i_supplier and
                            to_char(b.d_nota,'yyyymm')='201603' and
                            f_alokasi_cancel=FALSE
                            group by b.i_supplier, c.e_supplier_name
                            ) as data
                            group by i_supplier, e_supplier_name
                            order by i_supplier",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
