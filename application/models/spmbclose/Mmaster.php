<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" 	* from tm_spmb 
			                    where f_spmb_pemenuhan='t' and f_spmb_close='f'
			                    and (upper(i_spmb) ilike '%$cari%')
					                order by i_spmb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb 
		                    where f_spmb_pemenuhan='t' and f_spmb_close='f'
		                    and (upper(i_spmb) ilike '%$cari%')
				                order by i_spmb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function updatespmb($ispmb)
    {
		  $data = array(
		             'f_spmb_close' => 't'
		          );
		  $this->db->where('i_spmb', $ispmb);
		  $this->db->update('tm_spmb', $data); 
    }
}
?>
