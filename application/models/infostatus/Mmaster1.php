<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("  'Gudang' as status, sum(nilai_pusat) as nilai_pusat, sum(nilai_cabang) as nilai_cabang, sum(total) as total from(
select 'Gudang Pusat' as status, case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat, 0 as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=false and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')  
 and f_spb_cancel=FALSE 
 and not i_approve1 isnull 
 and not i_approve2 isnull 
 and i_store isnull

Union ALL

select 'Gudang Cabang' as status, 0 as nilai_pusat,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=true and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and not i_approve1 isnull 
 and not i_approve2 isnull 
 and i_store isnull
 
union all 

select 'Total Gudang' as status, 0 as nilai_pusat, 0 as nilai_cabang,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total from tm_spb
where d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 notnull 
 and i_store isnull


)as a

Union ALL
select 'sales' as status, sum(nilai_pusat) as nilai_pusat, sum(nilai_cabang) as nilai_cabang, sum(total) as total from(
select 'Sales Pusat' as status,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat, 0 as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=false and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

Union ALL

select 'Sales Cabang' as status, 0 as nilai_pusat,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=true and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all

select 'Total Sales' as status, 0 as nilai_pusat, 0 as nilai_cabang,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total from tm_spb
where d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull

)as b

Union ALL
select 'Keuangan' as status, sum(nilai_pusat) as nilai_pusat, sum(nilai_cabang) as nilai_cabang, sum(total) as total from(
select 'Keuangan Pusat' as status,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat, 0 as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=false and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

Union ALL

select 'Keuangan Cabang' as status, 0 as nilai_pusat,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang, 0 as total from tm_spb
where f_spb_stockdaerah=true and d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all

select 'Total Keuangan' as status, 0 as nilai_pusat, 0 as nilai_cabang,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total from tm_spb
where d_spb >= to_date('01-09-2016','dd-mm-yyyy') and d_spb <= to_date('30-09-2016','dd-mm-yyyy')
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull
 ) as c",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
