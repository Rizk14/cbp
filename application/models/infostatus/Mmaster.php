<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("   'Gudang' as status, sum(nilai_pusat) as nilai_pusat,sum(qty) as qty, sum(nilai_cabang) as nilai_cabang,sum(qtycab) as qtycab, sum(total) as total, sum(qtyjum) as qtyjum from(

select 'Gudang Pusat' as status, case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat,0 as qty, 0 as nilai_cabang, 0 as qtycab, 0 as total, 0 as qtyjum from tm_spb
where f_spb_stockdaerah=false and  (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=FALSE 
 and not i_approve1 isnull 
 and not i_approve2 isnull 
 and i_store isnull

 union all 

select 'Gudang Pusat' as status, 0 as nilai_pusat,sum(b.n_order) as qty ,0 as nilai_cabang,0 as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where a.f_spb_stockdaerah=false 
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and not a.i_approve1 isnull 
 and not a.i_approve2 isnull 
 and a.i_store isnull

 union all 

 select 'Gudang Cabang' as status, 0 as nilai_pusat, 0 as qty ,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang, 0 as qtycab,0 as total, 0 as qtyjum from tm_spb
where f_spb_stockdaerah=true and (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and not i_approve1 isnull 
 and not i_approve2 isnull 
 and i_store isnull
 
union all 

 select 'Gudang Cabang' as status, 0 as nilai_pusat,0 as qty ,0 as nilai_cabang,sum(b.n_order) as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and not a.i_approve1 isnull 
 and not a.i_approve2 isnull 
 and a.i_store isnull

union all

select 'Total Gudang' as status, 0 as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab ,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total, 0 as qtyjum from tm_spb
where (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 notnull 
 and i_store isnull

union all

select 'Total Gudang' as status, 0 as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab , 0 as total , sum (b.n_order) as qtyjum from tm_spb a, tm_spb_item b
where (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.f_spb_cancel=false 
 and a.i_approve1 notnull 
 and a.i_approve2 notnull 
 and a.i_store isnull

 ) as a

union all

 select 'Sales' as status, sum(nilai_pusat) as nilai_pusat,sum(qty) as qty, sum(nilai_cabang) as nilai_cabang,sum(qtycab) as qtycab, sum(total) as total, sum(qtyjum) as qtyjum from(
select 'Sales Pusat' as status,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat, 0 as qty, 0 as nilai_cabang, 0 as qtycab, 0 as total, 0 as qtyjum from tm_spb
where f_spb_stockdaerah=false and (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all 

select 'Sales Pusat' as status, 0 as nilai_pusat,sum(b.n_order) as qty ,0 as nilai_cabang,0 as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where a.f_spb_stockdaerah=false 
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.i_approve1 isnull 
 and a.i_notapprove isnull


Union ALL

select 'Sales Cabang' as status, 0 as nilai_pusat, 0 as qty,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang, 0 as qtycab, 0 as total, 0 as qtyjum from tm_spb
where f_spb_stockdaerah=true and (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all 

select 'Sales Cabang' as status, 0 as nilai_pusat,0 as qty ,0 as nilai_cabang,sum(b.n_order) as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.i_approve1 isnull 
 and a.i_notapprove isnull


union all

select 'Total Sales' as status, 0 as nilai_pusat, 0 as qty, 0 as nilai_cabang, 0 as qtycab ,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total, 0 as qtyjum from tm_spb
where (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 isnull 
 and i_notapprove isnull

 union all

 select 'Total Sales' as status, 0 as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab , 0 as total , sum (b.n_order) as qtyjum from tm_spb a, tm_spb_item b
where (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.f_spb_cancel=false 
 and a.i_approve1 isnull 
 and a.i_notapprove isnull


)as b

union all

select 'Keuangan' as status, sum(nilai_pusat) as nilai_pusat,sum(qty) as qty, sum(nilai_cabang) as nilai_cabang,sum(qtycab) as qtycab, sum(total) as total, sum(qtyjum) as qtyjum from(
select 'Keuangan Pusat' as status,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab, 0 as total,0 as qtyjum from tm_spb
where f_spb_stockdaerah=false and (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all

select 'Keuangan Pusat' as status, 0 as nilai_pusat,case when sum(b.n_order) isnull then 0 else sum(b.n_order) end as qty ,0 as nilai_cabang,0 as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where a.f_spb_stockdaerah=false 
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.i_approve1 notnull 
 and a.i_approve2 isnull 
 and a.i_notapprove isnull


Union ALL

select 'Keuangan Cabang' as status, 0 as nilai_pusat,0 as qty,case when sum(v_spb) isnull then 0 else sum(v_spb) end as nilai_cabang,0 as qtycab, 0 as total,0 as qtyjum from tm_spb
where f_spb_stockdaerah=true and (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull
group by f_spb_stockdaerah

union all

select 'Keuangan Cabang' as status, 0 as nilai_pusat,0 as qty ,0 as nilai_cabang, case when sum(b.n_order) isnull then 0 else sum(b.n_order) end as qtycab, 0 as total, 0 as qtyjum from tm_spb a, tm_spb_item b
where f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.i_approve1 notnull 
 and a.i_approve2 isnull 
 and a.i_notapprove isnull

union all

select 'Total Keuangan' as status, 0 as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab,case when sum(v_spb) isnull then 0 else sum(v_spb) end as total,0 as qtyjum from tm_spb
where (d_spb >= to_date('$dfrom','dd-mm-yyyy') AND d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and f_spb_cancel=false 
 and i_approve1 notnull 
 and i_approve2 isnull 
 and i_notapprove isnull

 union all

select 'Total Keuangan' as status, 0 as nilai_pusat,0 as qty, 0 as nilai_cabang,0 as qtycab , 0 as total , case when sum (b.n_order) isnull then 0 else sum(b.n_order) end as qtyjum from tm_spb a, tm_spb_item b
where (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb 
 and a.f_spb_cancel=false  and i_approve1 notnull 
 and a.i_approve2 isnull 
 and a.i_notapprove isnull
 ) as c",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }		
    function baca($dfrom,$dto,$status)
    {
		if($status=='Gudang'){
			
			
		
		$this->db->select(" 'Gudang Pusat' as stat , b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=false
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and not a.i_approve1 isnull 
 and not a.i_approve2 isnull 
 and a.i_store isnull
group by   b.i_product, b.e_product_name


union all 

select 'Gudang Cabang' as stat ,b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and not a.i_approve1 isnull 
 and not a.i_approve2 isnull 
 and a.i_store isnull
group by   b.i_product, b.e_product_name
order by stat, i_product",false);
$query = $this->db->get();
	}
	if($status=='Sales'){
		
		$this->db->select(" 'Sales Pusat' as stat , b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=false
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and a.i_approve1 isnull 
 and a.i_notapprove isnull
group by   b.i_product, b.e_product_name


union all 

select 'Sales cabang' as stat ,b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and a.i_approve1 isnull 
 and a.i_notapprove isnull
group by   b.i_product, b.e_product_name
order by stat, i_product",false);
		$query = $this->db->get();
		}if
		($status=='Keuangan'){
			
			$this->db->select(" 'Keuangan Pusat' as stat , b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=false
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and a.i_approve1 notnull 
 and a.i_approve2 isnull 
 and a.i_notapprove isnull
group by   b.i_product, b.e_product_name


union all 

select 'Keuangan cabang' as stat ,b.i_product ,  b.e_product_name ,sum(b.n_order) as qty from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah=true
 and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
 and a.f_spb_cancel=FALSE 
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
and a.i_approve1 notnull 
 and a.i_approve2 isnull 
 and a.i_notapprove isnull
group by   b.i_product, b.e_product_name
order by stat, i_product",false);
			$query = $this->db->get();
			
			}
		
		
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
}
}
