<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
		  $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, sum(b.v_target_tagihan) as total, sum(b.v_realisasi_tagihan) as realisasi, 
                          0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, 
                          sum(b.v_target_tagihan) as totalnon, sum(b.v_realisasi_tagihan) as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function simpan($iperiode,$batas)
        
    {
      $x=0;
      
      $periode=$iperiode;
		  $a=substr($periode,0,4);
	    $b=substr($periode,4,2);
		  $periode=mbulan($b)." - ".$a;
		  settype($a,'integer');
		  settype($b,'integer');
		  if($b==12){
		    $a=$a+1;
		    settype($a,'string');		  
		    settype($b,'string');
		    $b='01';
		  }else{
        $b=$b+1;
		    settype($a,'string');		  
		    settype($b,'string');
		    if(strlen($b)==1)$b='0'.$b;
		  }
		  $bts=$a.'-'.$b.'-01';
      $this->db->query("delete from tm_collection where e_periode='$iperiode'");
      $this->db->query("delete from tm_collection_item where e_periode='$iperiode'");
/*
      $this->db->select("	* from f_target_collection('$iperiode')
                          union all
                          select * from f_pelunasan_piutang('$iperiode')",false);
*/
      $this->db->select("	* from f_target_collection_detailnota('$iperiode','$bts')
                          union all
                          select * from f_target_collection_detailnota('$iperiode','$bts') where substring(kel_umur,1,2)='01'",FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $row   	= $que->row();
	      $now	  = $row->c;
	      $id     = $this->session->userdata('user_id');
			  foreach($query->result() as $row){
			    $x++;
          $this->db->query("insert into tm_collection values('$iperiode', '$now', '$id')");
          if($x>0){
            break;
          }
        }
        
			  foreach($query->result() as $row){
          if($row->i_as==null)$row->i_as=null;
          if($row->e_namaas==null)$row->e_namaas=null;
          if($row->i_salesman==null)$row->i_salesman=null;
          if($row->e_salesman_name==null)$row->e_salesman_name=null;
          if($row->i_area==null)$row->i_area=null;
          if($row->e_area_name==null)$row->e_area_name=null;
          if($row->f_spb_stockdaerah==null)$row->f_spb_stockdaerah=null;
          if($row->f_spb_consigment==null)$row->f_spb_consigment=null;
          if($row->f_insentif==null)$row->f_insentif=null;
          if($row->i_nota==null)$row->i_nota=null;
          if($row->d_nota==null)$row->d_nota=null;
          if($row->n_top==null)$row->n_top=null;
          if($row->d_jatuh_tempo==null)$row->d_jatuh_tempo=null;
          if($row->n_lamatoleransi==null)$row->n_lamatoleransi=0;
          if($row->d_jatuh_tempo_plustoleransi==null)$row->d_jatuh_tempo_plustoleransi=null;
#          if($row->d_bayar==null)$row->d_bayar=null;
#          if($row->i_pelunasan==null)$row->i_pelunasan=null;
#          if($row->d_bukti_pelunasan==null)$row->d_bukti_pelunasan=null;
#          if($row->i_reff_pelunasan==null)$row->i_reff_pelunasan=null;
#          if($row->i_jenis_bayar==null)$row->i_jenis_bayar=null;
#          if($row->e_jenis_bayarname==null)$row->e_jenis_bayarname=null;
          $row->d_bayar=null;
          $row->i_pelunasan=null;
          $row->d_bukti_pelunasan=null;
          $row->i_reff_pelunasan=null;
          $row->i_jenis_bayar=null;
          $row->e_jenis_bayarname=null;

          if($row->n_lamabayar==null)$row->n_lamabayar=null;
          
          $row->e_customer_name=str_replace("'","''",$row->e_customer_name);
          if($row->i_customer==null)$row->i_customer=null;
          if($row->e_customer_name==null)$row->e_customer_name=null;
#          if($row->e_customer_classname==null)$row->e_customer_classname=null;
          $row->e_customer_classname=null;
          if($row->i_customer_groupar==null)$row->i_customer_groupar=null;
          if($row->i_customer_groupbayar==null)$row->i_customer_groupbayar=null;
          if($row->i_product_group==null)$row->i_product_group=null;
          
          if($row->v_target_tagihan==null)$row->v_target_tagihan=0;
          if($row->v_realisasi_tagihan==null)$row->v_realisasi_tagihan=0;


          $sql="insert into tm_collection_item values('$iperiode', '$row->i_as', '$row->e_namaas', '$row->i_salesman',
                '$row->e_salesman_name', '$row->i_area', '$row->e_area_name', '$row->i_customer', '$row->e_customer_name',
                '$row->e_customer_classname', '$row->i_customer_groupar', '$row->i_customer_groupbayar', '$row->i_product_group', ";
          if($row->f_spb_stockdaerah==null){
            $sql.="null, ";
          }else{
            $sql.="'$row->f_spb_stockdaerah', ";
          }
          if($row->f_spb_consigment==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->f_spb_consigment', ";
          }
          if($row->f_insentif==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->f_insentif', ";
          }
          $sql.="'$row->i_nota', '$row->d_nota', $row->n_top, '$row->d_jatuh_tempo', $row->n_lamatoleransi, ";
          if($row->d_jatuh_tempo_plustoleransi==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->d_jatuh_tempo_plustoleransi', ";
          }
          if($row->d_bayar==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->d_bayar', ";
          }
          $sql.="'$row->i_pelunasan', ";
          if($row->d_bukti_pelunasan==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->d_bukti_pelunasan', ";
          }
          $sql.="'$row->i_reff_pelunasan', '$row->i_jenis_bayar', '$row->e_jenis_bayarname', ";
          if($row->d_bukti_pelunasan==null) {
            $sql.="null, ";
          }else{
            $sql.="'$row->n_lamabayar', ";
          }
          $sql.="$row->v_target_tagihan, $row->v_realisasi_tagihan, '$now','$row->kelompok')";
          $this->db->query($sql);
        }
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.i_salesman, a.e_salesman_name, a.i_nota, a.d_nota, 
                          sum(a.total) as total, sum(a.realisasi) as realisasi, sum(a.totalnon) as totalnon, 
                          sum(a.realisasinon) as realisasinon from(
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota, 
                          sum(b.sisa+b.bayar) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota
                          union all
                          select a.i_area, a.e_area_name, e.i_customer, e.e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota, 
                          0 as total, 0 as realisasi, sum(b.sisa+b.bayar) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection b on(a.i_area=b.i_area)
                          left join tr_customer e on (b.i_customer=e.i_customer)
                          left join tr_salesman f on (b.i_salesman=f.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_customer, e_customer_name, f.i_salesman, f.e_salesman_name, i_nota, d_nota
                          ) as a 
                          group by a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.i_salesman, a.e_salesman_name, a.i_nota, 
                          a.d_nota
                          order by a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
