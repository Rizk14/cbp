<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($iadj,$icustomer) 
    {
  		$this->db->query("update tm_adjmo set f_adj_cancel='t' WHERE i_adj='$iadj' and i_customer='$icustomer'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name, c.e_area_name from tm_ttbretur a, tr_customer b, tr_area c
							where a.i_area=c.i_area and a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
							order by a.i_ttb desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($num,$offset)
    {
		  $this->db->select("i_customer, e_customer_name from tr_customer
                         where substring(i_customer,1,2)='PB' and f_customer_aktif='t'
                         order by i_customer", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function caricustomer($cari,$num,$offset)
    {
		  $this->db->select("i_customer, e_customer_name from tr_customer 
		                     where substring(i_customer,1,2)='PB' and f_customer_aktif='t' and
		                     (upper(e_customer_name) like '%$cari%' or upper(i_customer) like '%$cari%')
						             order by i_customer ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($icustomer,$dfrom,$dto,$num,$offset,$cari)
    {
	    $this->db->select("	a.*, c.e_customer_name from tr_customer c, tm_adjmo a
						    where a.i_customer=c.i_customer and (upper(a.i_adj) like '%$cari%')
						    and a.i_customer='$icustomer' and
						    a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
						    a.d_adj <= to_date('$dto','dd-mm-yyyy')
						    order by a.d_adj, a.i_customer, a.i_adj desc ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($iarea=='NA'){
		    $this->db->select("	a.*, c.e_area_name from tm_adj a, tr_area c
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }else{
		    $this->db->select("	a.*, c.e_area_name from tm_adj a, tr_area c
							    where a.i_area=c.i_area and (upper(a.i_adj) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_adj <= to_date('$dto','dd-mm-yyyy')
							    order by a.d_adj, a.i_area, a.i_adj desc ",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
