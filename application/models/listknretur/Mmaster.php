<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ikn,$nknyear,$iarea) 
    {
		$this->db->query("update tm_kn set f_kn_cancel='t' WHERE i_kn='$ikn' and n_kn_year=$nknyear and i_area='$iarea'");
    $this->db->select(" i_refference from tm_kn WHERE i_kn='$ikn' and n_kn_year=$nknyear and i_area='$iarea' ",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
	    foreach($query->result() as $kn){
        $irefference=$kn->i_refference;
      }
      $this->db->insert('tm_kn');
        $this->db->set(
	        array(
          'f_kn'				=> 'f'
	        )
        );
      }
      $this->db->where("i_bbm",$irefference);
      $this->db->where("i_bbm_type",'05');
      $this->db->update('tm_bbm');
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name, 
            b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name 
				    from tm_kn a
				    inner join tr_customer b on (a.i_customer=b.i_customer)
				    inner join tr_area c on (a.i_area=c.i_area)
				    inner join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    inner join tr_salesman e on (a.i_salesman=e.i_salesman)
				    where 
				    a.i_kn_type='01' 
				    and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
				        upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
				    order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name,
            b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name 
				    from tm_kn a
				    left join tr_customer b on (a.i_customer=b.i_customer)
				    left join tr_area c on (a.i_area=c.i_area)
				    left join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    left join tr_salesman e on (a.i_salesman=e.i_salesman)
				    where 
				    a.i_kn_type='01' 
				    and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
				        upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
				    order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name,
              b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
							e.e_salesman_name, f.i_ttb, f.d_ttb 
							from tm_kn a
							inner join tr_customer b on (a.i_customer=b.i_customer)
							inner join tr_area c on (a.i_area=c.i_area)
							inner join tr_customer_groupar d on (b.i_customer=d.i_customer)
							inner join tr_salesman e on (a.i_salesman=e.i_salesman)
                     inner join tm_ttbretur f on (a.i_area=f.i_area and a.i_refference=f.i_bbm)
							where 
							a.i_kn_type='01' 
							and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
								upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							order by a.i_kn desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name,
              b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
							e.e_salesman_name, f.i_ttb, f.d_ttb
							from tm_kn a
							inner join tr_customer b on (a.i_customer=b.i_customer)
							inner join tr_area c on (a.i_area=c.i_area)
							inner join tr_customer_groupar d on (b.i_customer=d.i_customer)
							inner join tr_salesman e on (a.i_salesman=e.i_salesman)
              inner join tm_ttbretur f on (a.i_area=f.i_area and a.i_refference=f.i_bbm)
							where 
							a.i_kn_type='01' 
							and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
								upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							order by a.i_kn desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
