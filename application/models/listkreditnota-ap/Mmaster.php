<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ikn,$nknyear,$iarea) 
    {
		$this->db->query("update tm_kn set f_kn_cancel='t' WHERE i_kn='$ikn' and n_kn_year=$nknyear and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name, 
            b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name 
				    from tm_kn a
				    inner join tr_customer b on (a.i_customer=b.i_customer)
				    inner join tr_area c on (a.i_area=c.i_area)
				    left join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    inner join tr_salesman e on (a.i_salesman=e.i_salesman)
				    where 
				    a.i_kn_type='03' 
				    and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
				     upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
				    order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name,
            b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name 
				    from tm_kn a
				    inner join tr_customer b on (a.i_customer=b.i_customer)
				    inner join tr_area c on (a.i_area=c.i_area)
				    left join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    inner join tr_salesman e on (a.i_salesman=e.i_salesman)
				    where 
				    a.i_kn_type='03' 
				    and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
				        upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
				    order by a.i_kn desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		  $this->db->select("* from tr_supplier  order by i_supplier", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function carisupplier($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_supplier, e_supplier_name from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	* from tm_kn_ap a
			left join tr_supplier b using(i_supplier)
				where a.i_supplier='$isupplier' and
										  a.d_kn_ap >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_kn_ap <= to_date('$dto','dd-mm-yyyy') ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman), a.*, b.e_customer_name,
              b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
							e.e_salesman_name 
							from tm_kn a
							inner join tr_customer b on (a.i_customer=b.i_customer)
							inner join tr_area c on (a.i_area=c.i_area)
							left join tr_customer_groupar d on (b.i_customer=d.i_customer)
							inner join tr_salesman e on (a.i_salesman=e.i_salesman)
							where 
							a.i_kn_type='03' 
							and (a.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
								upper(a.i_kn) like '%$cari%' or upper(a.i_refference) like '%$cari%') 
							and a.i_area='$iarea' and
							a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_kn <= to_date('$dto','dd-mm-yyyy')
							order by a.i_kn desc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
