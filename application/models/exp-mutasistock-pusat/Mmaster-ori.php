<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode,$istore,$num,$offset,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
#		  if ($query->num_rows() > 0){
#			  return $query->result();
#		  }
    }

    function bacastore($num,$offset)
    {
	    $this->db->select(" distinct c.i_store as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
												  from tr_store_location a, tr_store b, tr_area c
												  where a.i_store = b.i_store and b.i_store=c.i_store and a.i_store='AA'
                         order by c.i_store", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset)
    {
	    $this->db->select(" distinct c.i_store as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
												  from tr_store_location a, tr_store b, tr_area c
												  where a.i_store = b.i_store and b.i_store=c.i_store and a.i_store='AA'
                         and(  upper(a.i_store) like '%$cari%' 
                         or upper(a.e_store_name) like '%$cari%'
                         or upper(b.i_store_location) like '%$cari%'
                         or upper(b.e_store_locationname) like '%$cari%')
                         order by c.i_store", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detail($iperiode,$iarea,$iproduct)
    {
      if($iarea=='00')
      {
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and daerah='f' and product='$iproduct' and a.product=b.i_product",false);
      }else{
		    $this->db->select("	a.*,b.e_product_name from vmutasidetail a, tr_product b
							    where periode = '$iperiode' and area='$iarea' and product='$iproduct'  and a.product=b.i_product",false);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
