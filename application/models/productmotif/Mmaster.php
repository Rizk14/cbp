<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproductmotif,$iproduct)
    {
		$query = $this->db->query("select a.*, b.e_product_name from tr_product_motif a, tr_product b 
						   where a.i_product_motif='$iproductmotif' and a.i_product=b.i_product
							 and a.i_product='$iproduct'");
		//$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cari_brg($kodebrg,$motif){
		return $this->db->query(" SELECT * FROM tr_product_motif WHERE i_product='$kodebrg' AND i_product_motif='$motif' ");
	}    
    function insert($iproductmotif, $eproductmotifname,$iproduct, $eproductname)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->set(
			array(
				'i_product'				=> $iproduct,
				'i_product_motif' 		=> $iproductmotif,
				'e_product_motifname' 	=> $eproductmotifname,
				'd_product_motifentry'	=> $dentry
			)
		);
		
		$this->db->insert('tr_product_motif');
		redirect('productmotif/cform/');
    }
    function update($iproductmotif, $eproductmotifname, $iproduct, $iproductname)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_product_motif set i_product='$iproduct',	               	
													  i_product_motif='$iproductmotif',
		           									  e_product_motifname='$eproductmotifname',
											   		  d_product_motifupdate='$dupdate'
						  where i_product_motif='$iproductmotif' and i_product='$iproduct'");
		redirect('productmotif/cform/');
    }
	
    public function delete($iproductmotif,$iproduct) 
    {
		$this->db->query("DELETE FROM tr_product_motif WHERE i_product_motif='$iproductmotif'
												 and i_product='$iproduct'");
		return TRUE;
    }
    function bacasemua($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_product_name from tr_product_motif a, tr_product b where (upper(a.i_product_motif) like '%$cari%' or upper(a.i_product) like '%$cari%' 
							  or upper(a.e_product_motifname) like '%$cari%') and a.i_product=b.i_product
							  order by a.i_product,a.i_product_motif",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaproduct($num,$offset)
    {
		$this->db->select("i_product, e_product_name
						   from tr_product 
						   order by i_product",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariproductbase($cari,$num,$offset)
    {
		$this->db->select(" * from tr_product
					 where (upper(i_product) ilike '%$cari%' 
					or upper(e_product_name) ilike '%$cari%')
					order by i_product",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
