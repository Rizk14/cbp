<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
    }

    function baca($isjbr,$iarea)
    {
		 /* $this->db->select(" a.i_sjbr, a.d_sjbr, d.d_sjpbr, a.i_area, d.i_sjpbr, 
                          a.v_sjbr, b.e_area_name
                          from tm_sjbr a, tr_area b, tm_sjpbr d
                          where a.i_area=b.i_area and a.i_sjbr=d.i_sjbr
                          and a.i_sjbr ='$isjbr' ", false);*/
		  $this->db->select(" a.*, b.e_area_name, b.i_store from tm_sjbr a, tr_area b where a.i_area=b.i_area
						            and a.i_sjbr ='$isjbr' and a.i_area='$iarea' ", false);
	    $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($isjbr, $iarea)
    {
		  /*$this->db->select(" a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname,
                          a.v_unit_price as harga, a.v_unit_price, a.n_quantity_retur as qty,
                          a.n_quantity_receive, a.i_product_grade
                          from tm_sjbr_item a, tr_product_motif b, tm_sjbr c, tm_sjpbr d
                          where a.i_sjbr = '$isjbr' and a.i_product=b.i_product 
                          and a.i_sjbr=c.i_sjbr and a.i_area=c.i_area
                          and c.i_sjbr=d.i_sjbr and c.i_area=d.i_area
                          and a.i_product_motif=b.i_product_motif
                          order by a.n_item_no", false);*/
      $this->db->select(" a.i_sjbr,a.d_sjbr,a.i_area,
                       a.i_product,a.i_product_grade,a.i_product_motif,a.n_quantity_retur,
                       a.n_quantity_receive,a.v_unit_price,a.e_product_name,a.i_store,
                       a.i_store_location,a.i_store_locationbin,a.e_remark,
                       b.e_product_motifname from tm_sjbr_item a, tr_product_motif b
				               where a.i_sjbr = '$isjbr' and a.i_area='$iarea'
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    public function deletedetail($iproduct, $iproductgrade, $isjbr, $iproductmotif) 
    {
		  $this->db->query("DELETE FROM tm_sjbr_item WHERE i_sjbr='$isjbr'
										  and i_product='$iproduct' and i_product_grade='$iproductgrade' 
										  and i_product_motif='$iproductmotif'");
		  return TRUE;
    }
	
    function bacasemua()
    {
		  $this->db->select("* from tm_sjpbr order by i_sjpbr desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacasjpbr($area,$num,$offset)
    {
		  if($offset=='') $offset=0;
			$this->db->select("	a.*, b.e_customer_name, c.e_area_name, c.i_store
                          from tm_sjpbr a, tr_customer b, tr_area c
                          where a.i_customer=b.i_customer and a.i_area=c.i_area
                          and (a.i_sjbr isnull or trim(a.i_sjbr)='')
                          and not a.d_sjpbr_receive is null
                          and a.f_sjpbr_cancel = 'f' 
                          and a.i_area='$area'
                          order by a.i_sjpbr
                          limit $num offset $offset",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisjpbr($cari,$area,$num,$offset)
    {
		if($offset=='') $offset=0;
		$this->db->select("	a.*, b.e_customer_name, c.e_area_name, c.i_store
                        from tm_sjpbr a, tr_customer b, tr_area c
                        where a.i_customer=b.i_customer and a.i_area=c.i_area
                        and (a.i_sjbr isnull or trim(a.i_sjbr)='')
                        and not a.d_sjpbr_receive is null
                        and a.f_sjpbr_cancel = 'f' 
                        and a.i_area='$area'
                        and (upper(a.i_sjpbr) like '%$cari%')
                        order by a.i_sjpbr
                        limit $num offset $offset",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function product($sjpbr,$iarea)
    {
	    $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif, b.n_quantity_retur,
                                a.e_product_motifname as namamotif, b.n_quantity_receive as n_qty,
                                c.e_product_name as nama,b.v_unit_price as harga, b.i_product_grade as grade
                                from tr_product_motif a,tr_product c, tm_sjpbr_item b
                                where a.i_product=c.i_product 
                                and b.i_product_motif=a.i_product_motif
                                and c.i_product=b.i_product and b.n_quantity_receive>0
                                and b.i_sjpbr='$sjpbr' and i_area='$iarea' order by b.n_item_no",false);
	    if ($query->num_rows() > 0){
		    return $query->result();
	    }
    }

    function cari($cari,$num,$offset)

    {
		  $this->db->select(" * from tm_sjpbr where upper(i_sjpbr) like '%$cari%' 
					  order by i_sjpbr",FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariproduct($cari,$num,$offset)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif, b.n_quantity_retur,
                                a.e_product_motifname as namamotif, b.n_quantity_receive as n_qty,
                                c.e_product_name as nama,b.v_unit_price as harga, b.i_product_grade as grade
                                from tr_product_motif a,tr_product c, tm_sjpbr_item b
                                where a.i_product=c.i_product 
                                and b.i_product_motif=a.i_product_motif
                                and c.i_product=b.i_product and b.n_quantity_receive>0
						                   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
                                and b.i_sjpbr='$sjpbr' and i_area='$iarea' order by b.n_item_no
								                limit $num offset $offset",false);
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($area1,$area2,$area3,$area4,$area5,$num,$offset)
    {
		  if($area1=='00'){
			  $this->db->select("	* from tr_area 
								  order by i_area", false)->limit($num,$offset);
		  }else{
			  $this->db->select("	* from tr_area 
								  where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'
								  order by i_area", false)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariarea($area1,$area2,$area3,$area4,$area5,$cari,$num,$offset)
    {
		  if($area1=='00'){
			  $this->db->select("	i_area, e_area_name, i_store from tr_area 
								  where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
								  order by i_area ", FALSE)->limit($num,$offset);
		  }else{
			  $this->db->select("	i_area, e_area_name, i_store from tr_area 
								  where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
								  and (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
								  order by i_area ", FALSE)->limit($num,$offset);
		  }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function runningnumbersj($iarea,$thbl)
    {
		  $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='SRK'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){

			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nosj = $terakhir+1;
        $this->db->query("update tm_dgu_no 
                          set n_modul_no=$nosj
                          where i_modul='SRK'
                          and substr(e_periode,1,4)='$th' 
                          and i_area='$iarea'", false);
			  settype($nosj,"string");
			  $a=strlen($nosj);
			  while($a<4){
			    $nosj="0".$nosj;
			    $a=strlen($nosj);
			  }
        
			  $nosj  ="SRK-".$thbl."-".$iarea.$nosj;
			  return $nosj;
		  }else{
			  $nosj  ="0001";
			  $nosj  ="SRK-".$thbl."-".$iarea.$nosj;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('SRK','$iarea','$asal',1)");
			  return $nosj;
		  }
    }
    function insertsjheader($isjbr,$dsjbr,$iarea,$vsjbr)
    {
		  $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjentry	= $row->c;
		  $this->db->set(
		    array(	  
          'i_sjbr'     		=> $isjbr,
	        'd_sjbr'     		=> $dsjbr,
	        'i_area'        => $iarea,
	        'v_sjbr'    		=> $vsjbr,
	        'd_sjbr_entry'	=> $dsjentry,
	        'f_sjbr_cancel'	=> 'f'
		    )
    	);
    	$this->db->insert('tm_sjbr');
    }

    function insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
			                      $vunitprice,$isjbr,$dsjbr,$iarea,$istore,$istorelocation,$istorelocationbin,$i)
    {
      $th=substr($dsjbr,0,4);
      $bl=substr($dsjbr,5,2);
      $pr=$th.$bl;
    	$this->db->set(
    		array(
				'i_sjbr'			        => $isjbr,
				'd_sjbr'			        => $dsjbr,
				'i_area'		          => $iarea,
				'i_product'       		=> $iproduct,
				'i_product_motif'   	=> $iproductmotif,
				'i_product_grade'   	=> $iproductgrade,
				'e_product_name'    	=> $eproductname,
				'n_quantity_retur'  	=> $nretur,
				'n_quantity_receive'	=> $nreceive,
				'v_unit_price'		    => $vunitprice,
				'i_store'         		=> $istore,
				'i_store_location'	  => $istorelocation,
				'i_store_locationbin'	=> $istorelocationbin, 
        'e_remark'            => '',
        'e_mutasi_periode'    => $pr,
        'n_item_no'           => $i
    		)
    	);
    	
    	$this->db->insert('tm_sjbr_item');
    }  
  
    function updatesjpbr($isjpbr,$dsjpbr,$isjbr,$iarea)
    {
	    $this->db->query("update tm_sjpbr set i_sjbr = '$isjbr'
                        where i_sjpbr='$isjpbr' and i_area='$iarea'",false);
    }

    function updatesjheader($isj,$iarea,$dsj,$vsjnetto)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dsjupdate= $row->c;
    	$this->db->set(
    		array(
				'v_sjbr'	      => $vsjnetto,
				'd_sjbr'       => $dsj,
        'd_sjbr_update'=> $dsjupdate

    		)
    	);
    	$this->db->where('i_sjbr',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_sjbr');
    }

    function searchsjheader($isjbr,$iarea)
    {
		return $this->db->query(" SELECT * FROM tm_sjbr WHERE i_sjbr='$isjbr' AND i_area='$iarea' ");
	  }    
    function deletesjdetail($isj, $iarea, $iproduct, $iproductgrade, $iproductmotif) 
    {
	    $this->db->query("DELETE FROM tm_sjbr_item WHERE i_sjbr='$isj'
                        and i_area='$iarea'
									      and i_product='$iproduct' and i_product_grade='$iproductgrade' 
									      and i_product_motif='$iproductmotif'");
    }

    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isjbr,$q_in,$q_out,$qsj,$q_aw,$q_ak,$tra)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isjbr', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal,i_trans)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw, $tra
                                )
                              ",false);
*/    }
    function inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isjbr,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isjbr', '$now', 0, $qsj, $q_ak-$qsj, $q_ak
                                )
                              ",false);
/*
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (

                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', '$now', $q_in, $q_out+$qsj, $q_ak-$qsj, $q_aw
                                )
                              ",false);
*/
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode,$qaw)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close,n_git_penjualan)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','$istore','$istorelocation','$istorelocationbin','$emutasiperiode',$qaw,0,0,0,$qsj,0,$qsj,$qaw-$qsj,0,'f',0)
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj,$q_aw)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '00', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $q_aw-$qsj, 't'
                                )
                              ",false);
    }
/*
    function inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$qsj,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	= $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$isj', $now, $q_in+$qsj, $q_out, $q_ak+$qsj, $q_aw
                                )
                              ",false);
    }
*/
    function updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbm=n_mutasi_bbm+$qsj, n_saldo_akhir=n_saldo_akhir+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                                  n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,$qsj,0,0,0,$qsj,0,'f')
                              ",false);
    }
    function updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
/*
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=$q_ak+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
*/
    }
    function insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qsj)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '00', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', $qsj, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isjbr,$nretur,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
      if ($queri->num_rows() > 0){
#       return $query->result();
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($nretur!=0 || $nretur!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','00','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$isjbr', '$now', $nretur, 0, $row->n_quantity_akhir+$nretur, $row->n_quantity_akhir
                                  )
                                ",false);
        }
/*
        $query=$this->db->query(" 
                                      DELETE FROM tm_ic_trans 
                                      where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                      and i_store='$istore' and i_store_location='$istorelocation'
                                      and i_store_locationbin='$istorelocationbin' and i_refference_document='$isj'
                                ",false);
*/
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qsj, n_saldo_akhir=n_saldo_akhir+$qsj,
                                n_git_penjualan=n_git_penjualan-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='00'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbm=n_mutasi_bbm-$qsj, n_saldo_akhir=n_saldo_akhir-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qsj)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qsj
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function bacaproduct($store,$num,$offset,$cari)
    {
			$area1	= $this->session->userdata('i_area');
      if($area1=='PB') {
			  $this->db->select("	a.i_product as kode, a.e_product_name as nama, b.v_product_retail as harga, 
                          c.i_product_motif as motif, c.e_product_motifname as namamotif
                          from tr_product a, tr_product_price b, tr_product_motif c, tm_ic d
                          where a.i_product=b.i_product and b.i_price_group='00' 
                          and a.i_product=d.i_product and c.i_product_motif=d.i_product_motif 
                          and d.i_store='PB' and d.i_store_location='00'
                          and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                          and a.i_product=c.i_product
													ORDER BY a.e_product_name",false)->limit($num,$offset);
      }else{
			  $this->db->select("	a.i_product as kode, a.e_product_name as nama, b.v_product_retail as harga, 
                          c.i_product_motif as motif, c.e_product_motifname as namamotif
                          from tr_product a, tr_product_price b, tr_product_motif c, tm_ic d
                          where a.i_product=b.i_product and b.i_price_group='00' 
                          and a.i_product=d.i_product and c.i_product_motif=d.i_product_motif 
                          and d.i_store='$store' and d.i_store_location='PB'
                          and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                          and a.i_product=c.i_product
													ORDER BY a.e_product_name",false)->limit($num,$offset);
      }
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
