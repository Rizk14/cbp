<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icoa)
    {
		$this->db->select(" * from tr_coa where i_coa='$icoa'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($icoa)
    {
		$this->db->select(" i_coa from tr_coa where i_coa='$icoa'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($icoa,$ecoaname)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_coa'				=> $icoa,
				'e_coa_name' 		=> $ecoaname
    		)
    	);
    	
    	$this->db->insert('tr_coa');
    }
    function update($icoa,$ecoaname)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate= $row->c;
    	$this->db->set(
    		array(
				'e_coa_name' 		=> $ecoaname
    		)
    	);
    	$this->db->where("i_coa",$icoa);
    	$this->db->update('tr_coa');
    }
}
?>
