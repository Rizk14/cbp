<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
        parent::__construct();
		#$this->CI =& get_instance();
  }
  function bacaperiode($dfrom,$dto)
  {
/*
    $this->db->select(" 
                area, grup, sum(atu) as atu, sum(ua) as ua, sum(iga) as iga, sum(ampa) as ampa, sum(ima) as ima, sum(anam) as anam, 
                sum(uju) as uju, sum(apan) as apan, sum(alan) as alan from crosstab (
                'SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''00''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''01''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''02''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,''PB'' as i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND b.f_spb_consigment = true
                and a.i_sj=d.i_sj and a.i_area=d.i_area
                GROUP BY a.i_area, i_product_group, c.i_customer_class','select * from generate_series(1,9)')
                as
                (area text, grup text, atu bigint, ua bigint, iga bigint, ampa bigint, ima bigint, anam bigint,
                uju bigint, apan bigint, alan bigint)
                group by area, grup
                order by area, grup  ",false);
*/

/*                $this->db->select(" area, grup, sum(atu) as atu, sum(siji) as siji, sum(ua) as ua, sum(loro) as loro, sum(iga) as iga, 
                sum(telu) as telu, sum(ampa) as ampa, sum(papat) as papat, sum(ima) as ima, sum(limo) as limo, sum(anam) as anam, 
                sum(anem) as anem, sum(uju) as uju, sum(pitu) as pitu, sum(apan) as apan, sum(lapan) as lapan, sum(alan) as alan, 
                sum(sanga) as sanga from(
                select area, grup, atu, 0 as siji, ua, 0 as loro, iga, 0 as telu, ampa, 0 as papat, ima, 0 as limo, anam, 0 as anem, uju, 
                0 as pitu, apan, 0 as lapan, alan, 0 as sanga from(
                select area, grup, sum(atu) as atu, sum(ua) as ua, sum(iga) as iga, sum(ampa) as ampa, sum(ima) as ima, sum(anam) as anam, 
                sum(uju) as uju, sum(apan) as apan, sum(alan) as alan from crosstab (
                'SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''00''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''01''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''02''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,''PB'' as i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND b.f_spb_consigment = true
                and a.i_sj=d.i_sj and a.i_area=d.i_area
                GROUP BY a.i_area, i_product_group, c.i_customer_class','select * from generate_series(1,9)')
                as
                (area text, grup text, atu bigint, ua bigint, iga bigint, ampa bigint, ima bigint, anam bigint,
                uju bigint, apan bigint, alan bigint)
                group by area, grup
                ) as a
                union all
                
                
                select area, grup, 0 as atu, atu as siji, 0 as ua, ua as loro, 0 as iga, iga as telu, 0 as ampa, ampa as papat, 0 as ima, 
                ima as limo, 0 as anam, anam as anem, 0 as uju, uju as pitu, 0 as apan, apan as lapan, 0 as alan, alan as sanga from(
                select area, grup, sum(atu) as atu, sum(ua) as ua, sum(iga) as iga, sum(ampa) as ampa, sum(ima) as ima, sum(anam) as anam, 
                sum(uju) as uju, sum(apan) as apan, sum(alan) as alan from crosstab (
                'SELECT a.i_area,f.i_product_group,c.i_customer_class, count(c.i_customer) as jml
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''00''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, count(c.i_customer) as jml
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''01''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, count(c.i_customer) as jml
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''02''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,''PB'' as i_product_group,c.i_customer_class, count(c.i_customer) as jml
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND b.f_spb_consigment = true
                and a.i_sj=d.i_sj and a.i_area=d.i_area
                GROUP BY a.i_area, i_product_group, c.i_customer_class','select * from generatei_product_group_series(1,9)')
                as
                (area text, grup text, atu bigint, ua bigint, iga bigint, ampa bigint, ima bigint, anam bigint,
                uju bigint, apan bigint, alan bigint)
                group by area, grup
                ) as a
                )as a
                group by area, grup
                order by area, grup",false);
*/
                $this->db->select(" area, grup, sum(atu) as atu, sum(siji) as siji, sum(ua) as ua, sum(loro) as loro, sum(iga) as iga, 
                sum(telu) as telu, sum(ampa) as ampa, sum(papat) as papat, sum(ima) as ima, sum(limo) as limo, sum(anam) as anam, 
                sum(anem) as anem, sum(uju) as uju, sum(pitu) as pitu, sum(apan) as apan, sum(lapan) as lapan, sum(alan) as alan, 
                sum(sanga) as sanga from(
                select area, grup, atu, 0 as siji, ua, 0 as loro, iga, 0 as telu, ampa, 0 as papat, ima, 0 as limo, anam, 0 as anem, uju, 
                0 as pitu, apan, 0 as lapan, alan, 0 as sanga from(
                select area, grup, sum(atu) as atu, sum(ua) as ua, sum(iga) as iga, sum(ampa) as ampa, sum(ima) as ima, sum(anam) as anam, 
                sum(uju) as uju, sum(apan) as apan, sum(alan) as alan from crosstab (
                'SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''00''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''01''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,f.i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''02''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class
                UNION ALL 
                SELECT a.i_area,''PB'' as i_product_group,c.i_customer_class, sum(d.n_deliver*d.v_unit_price)  AS v_gross
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND b.f_spb_consigment = true
                and a.i_sj=d.i_sj and a.i_area=d.i_area
                GROUP BY a.i_area, i_product_group, c.i_customer_class','select * from generate_series(1,9)')
                as
                (area text, grup text, atu bigint, ua bigint, iga bigint, ampa bigint, ima bigint, anam bigint,
                uju bigint, apan bigint, alan bigint)
                group by area, grup
                ) as a
                union all
                
               
                select area, grup, 0 as atu, atu as siji, 0 as ua, ua as loro, 0 as iga, iga as telu, 0 as ampa, ampa as papat, 0 as ima, 
                ima as limo, 0 as anam, anam as anem, 0 as uju, uju as pitu, 0 as apan, apan as lapan, 0 as alan, alan as sanga from(
                select area, grup, sum(atu) as atu, sum(ua) as ua, sum(iga) as iga, sum(ampa) as ampa, sum(ima) as ima, sum(anam) as anam, 
                sum(uju) as uju, sum(apan) as apan, sum(alan) as alan from crosstab (
                'SELECT x.i_area,x.i_product_group,x.i_customer_class, count(x.i_customer) as jml from(
                SELECT distinct(c.i_customer) as i_customer, a.i_area,f.i_product_group,c.i_customer_class
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''00''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class, c.i_customer
                ) as x
                GROUP BY x.i_area, x.i_product_group, x.i_customer_class
                UNION ALL 
                SELECT x.i_area,x.i_product_group,x.i_customer_class, count(x.i_customer) as jml from(
                SELECT distinct(c.i_customer) as i_customer, a.i_area,f.i_product_group,c.i_customer_class
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND 
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product 
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''01''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class, c.i_customer
                ) as x
                GROUP BY x.i_area, x.i_product_group, x.i_customer_class
                UNION ALL
                SELECT x.i_area,x.i_product_group,x.i_customer_class, count(x.i_customer) as jml from(
                SELECT distinct(c.i_customer) as i_customer, a.i_area,f.i_product_group,c.i_customer_class
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d, tr_product e, tr_product_type f
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND
                b.f_spb_consigment = false and a.i_sj=d.i_sj and a.i_area=d.i_area and d.i_product=e.i_product
                AND e.i_product_type=f.i_product_type AND f.i_product_group = ''02''
                GROUP BY a.i_area, f.i_product_group, c.i_customer_class, c.i_customer
                ) as x
                GROUP BY x.i_area, x.i_product_group, x.i_customer_class
                UNION ALL
                SELECT x.i_area,''PB'' as i_product_group,x.i_customer_class, count(x.i_customer) as jml from(
                SELECT distinct(c.i_customer) as i_customer, a.i_area, ''PB'' as i_product_group,c.i_customer_class
                FROM tm_nota a, tm_spb b, tr_customer c, tm_nota_item d
                WHERE a.d_nota>=''$dfrom'' AND a.d_nota<=''$dto'' AND a.i_customer=c.i_customer AND NOT a.i_nota IS NULL AND 
                a.f_nota_cancel = false AND a.i_spb = b.i_spb AND a.i_area = b.i_area AND b.f_spb_consigment = true
                and a.i_sj=d.i_sj and a.i_area=d.i_area
                GROUP BY a.i_area, i_product_group, c.i_customer_class, c.i_customer
                ) as x
                GROUP BY x.i_area, x.i_product_group, x.i_customer_class','select * from generate_series(1,9)')
                as
                (area text, grup text, atu bigint, ua bigint, iga bigint, ampa bigint, ima bigint, anam bigint,
                uju bigint, apan bigint, alan bigint)
                group by area, grup
                ) as a
                )as a
                group by area, grup
                order by area, grup",false);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacakelasnya()
  {
		$this->db->select(" * from tr_customer_class order by i_customer_class",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareanya($dfrom,$dto)
  {
		$this->db->select(" distinct a.i_area, b.e_area_name
                        from vpenjualanperdivisikelas a, tr_area b
                        where a.i_area=b.i_area and a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        order by a.i_area",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaproductnya($dfrom,$dto)
  {
		$this->db->select(" a.* from (
                        SELECT distinct a.i_product_group, b.e_product_groupname
                        from vpenjualanperdivisikelas a 
                        inner join tr_product_group b on (a.i_product_group=b.i_product_group)
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        union all
                        SELECT distinct a.i_product_group, 'Modern Outlet' as e_product_groupname
                        from vpenjualanperdivisikelas a 
                        where a.d_doc>='$dfrom' and a.d_doc<='$dto'
                        and a.i_product_group not in (select i_product_group from tr_product_group)
                        ) as a
                        order by a.e_product_groupname",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
}
?>
