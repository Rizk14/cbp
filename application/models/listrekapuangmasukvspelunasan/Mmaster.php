<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
    // $tahun = substr($iperiode,2,4);
    // 	  $this->db->select("* from f_rekap_uangmasuk_banding_pelunasan_perarea('$iperiode')",false);
    //   $query = $this->db->get();
    $coa = '110-41000';
    $query = $this->db->query("select x.i_area, z.e_area_name, sum(x.v_bank) as v_bank, sum(x.alokasi) as alokasi, (sum(x.v_bank) - sum(x.alokasi)) as selisih from(
        select i_area, SUM(v_bank) as v_bank, 0 as alokasi from tm_kbank 
        where to_char(d_bank,'yyyymm')='$iperiode'
        and f_kbank_cancel = 'f'
        and i_kbank like 'BM%'
        and i_coa = '$coa'
        group by i_area
        union all
        select a.i_area, 0 as v_bank, sum(a.v_jumlah) as alokasi from tm_alokasi_item a, tm_alokasi b
        where a.i_alokasi = b.i_alokasi
        and a.i_kbank = b.i_kbank
        and a.i_area = b.i_area
        and b.f_alokasi_cancel = 'f'
        and to_char(b.d_alokasi,'yyyymm')='$iperiode'
        group by a.i_area
        ) as x
        inner join tr_area z on(x.i_area = z.i_area)
        group by x.i_area, z.e_area_name");
		
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

}
?>
