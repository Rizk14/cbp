<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
   public function __construct()
    {
        parent::__construct();
      #$this->CI =& get_instance();
    }
    function baca_plapon($i_customer){
      return $this->db->query("select * from tr_customer_groupar where i_customer = '$i_customer'");
    }
    function baca_nilai($i_customer){
      $thbln = date('ym');
      return $this->db->query("select (x.v_spb - x.diskon) as nilai_spb from(
select sum(v_spb) as v_spb, sum(v_spb_discounttotal) as diskon from tm_spb where i_customer = '$i_customer' and i_spb like '%SPB-$thbln-%' and f_spb_cancel = 'f' 
) as x");
    }
    function baca_area_pusat(){
      $user = $this->session->userdata('user_id');
        return $this->db->query("select * from tm_user_area where i_user = '$user' and i_area = '00'");
    }
    function bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$num,$offset,$dfrom,$dto)
    {
    if($area1=='00'){
      $this->db->select("  a.*, d.i_dkb, b.e_customer_name, b.i_customer_status , b.f_customer_pkp, c.e_customer_ownername
              from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area), tr_customer b, tr_customer_owner c
              where a.i_customer=b.i_customer and a.i_customer=c.i_customer and (a.n_print=0 or a.n_print isnull)
              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
              or upper(a.i_spb) like '%$cari%')
              and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
              and a.i_area='$iarea'
              order by a.i_spb desc",false)->limit($num,$offset);
    }else{
      $this->db->select("  a.*, d.i_dkb, b.e_customer_name, b.i_customer_status, b.f_customer_pkp, c.e_customer_ownername 
               from tm_spb a left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area), tr_customer b, tr_customer_owner c
               where a.i_customer=b.i_customer and a.i_customer=c.i_customer and (a.n_print=0 or a.n_print isnull)
               and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
               or upper(a.i_spb) like '%$cari%')
               and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
               and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
               and a.i_area='$iarea'
               order by a.i_spb desc",false)->limit($num,$offset);
    }
#and a.f_spb_stockdaerah='f'
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function baca($ispb,$area)
    {
      $this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city, b.i_area, b.f_customer_pkp, b.d_signin, c.*, d.*, e.*, f.*
                             , g.e_customer_ownername, h.e_customer_pkpname, i.e_area_name
                             from tm_spb a, tr_customer b, tr_salesman c, tr_customer_class d, tr_price_group e, tr_customer_groupar f
                             , tr_customer_owner g, tr_customer_pkp h, tr_area i
                             where a.i_spb = '$ispb' and a.i_area='$area'
                             and a.i_area = i.i_area 
                             and a.i_customer=b.i_customer and a.i_customer=f.i_customer
                             and a.i_salesman=c.i_salesman
                             and a.i_customer=g.i_customer
                             and a.i_customer=h.i_customer
                             and (e.n_line=b.i_price_group or e.i_price_group=b.i_price_group)
                             and b.i_customer_class=d.i_customer_class
                             order by a.i_spb desc",false);
#and (a.n_print=0 or a.n_print isnull)
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacadetail($ispb,$area)
    {
      $this->db->select(" a.i_spb,
                          a.i_product,
                          a.i_product_grade,
                          a.i_product_motif,
                          a.n_order,
                          a.n_deliver,
                          a.n_stock,
                          a.v_unit_price,
                          substr(a.e_product_name,1,46) as e_product_name,
                          a.i_op,
                          a.i_area,
                          a.e_remark,
                          a.n_item_no,
                          tr_product.i_product_status from tm_spb_item a
                         inner join tr_product on (a.i_product=tr_product.i_product)
                         inner join tr_product_motif on (a.i_product_motif=tr_product_motif.i_product_motif
                         and a.i_product=tr_product_motif.i_product)
                         where a.i_spb = '$ispb' and a.i_area='$area' order by a.n_item_no",false);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function cari($area1,$area2,$area3,$area4,$area5,$cari,$num,$offset)
    {
      $this->db->select("  a.*, b.e_customer_name from tm_spb a, tr_customer b
               where a.i_customer=b.i_customer
               and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
               or upper(a.i_spb) like '%$cari%')
               and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
               order by a.i_spb desc",FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function close($area,$ispb)
    {
      $this->db->query("   update tm_spb set n_print=n_print+1
                              where i_spb = '$ispb' and i_area = '$area' ",false);
    }
    
    function bacapiutang($ispb,$area)
    {
      $this->db->select(" i_customer from tm_spb where i_spb = '$ispb' and i_area='$area'",false);
      $quer = $this->db->get();
      $cust='';      
      $saldo=0;
      if ($quer->num_rows() > 0){
        foreach($quer->result() as $rowi){
          $cust=$rowi->i_customer;
        }
        $this->db->select(" sum(v_sisa) as sisa from tm_nota where i_customer = '$cust' and f_nota_cancel='f' and not i_nota isnull",false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $saldo=$row->sisa;
          }
        }
      }
      return $saldo;
    }

    function bacanotapiutang($ispb,$area)
    {
      $this->db->select(" i_customer from tm_spb where i_spb = '$ispb' and i_area='$area'",false);
      $quer = $this->db->get();
      $cust='';      
      $nota=0;
      if ($quer->num_rows() > 0){
        foreach($quer->result() as $rowi){
          $cust=$rowi->i_customer;
        }
        $this->db->select(" i_nota from tm_nota where i_customer = '$cust' and f_nota_cancel='f' and not i_nota isnull",false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          foreach($query->result() as $row){
            $nota=$row->i_nota;
          }
        }
      }
      return $nota;
    }
}
?>
