<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($icustomerclass)
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class where i_customer_class = '$icustomerclass'");
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($icustomerclass, $ecustomerclassname)
    {
    	$this->db->set(
    		array(
    			'i_customer_class' => $icustomerclass,
    			'e_customer_classname' => $ecustomerclassname
    		)
    	);
    	
    	$this->db->insert('tr_customer_class');
		#redirect('customerclass/cform/');
    }
    function update($icustomerclass, $ecustomerclassname)
    {
    	$data = array(
               'i_customer_class' => $icustomerclass,
               'e_customer_classname' => $ecustomerclassname
            );
		$this->db->where('i_customer_class', $icustomerclass);
		$this->db->update('tr_customer_class', $data); 
		#redirect('customerclass/cform/');
    }
	
    public function delete($icustomerclass) 
    {
		$this->db->query('DELETE FROM tr_customer_class WHERE i_customer_class=\''.$icustomerclass.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select("i_customer_class, e_customer_classname from tr_customer_class order by i_customer_class", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
