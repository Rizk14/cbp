<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ikuk, $tahun)
    {
		$this->db->select("	* from tm_kuk a
					left join tr_supplier c on(a.i_supplier=c.i_supplier)
					where a.i_kuk='$ikuk' and a.n_kuk_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function cek($ikuk,$tahun)
    {
		$this->db->select(" i_kuk from tm_kuk where i_kuk='$ikuk' and n_kuk_year='$tahun'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
    }
    function insert($ikuk,$dkuk,$tahun,$ebankname,$isupplier,
				    $eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_kuk'				=> $ikuk,
				'i_supplier'		=> $isupplier,
				'd_kuk'				=> $dkuk,
				'd_entry'			=> $dentry,
				'e_bank_name'		=> $ebankname,
				'e_remark'			=> $eremark,
				'n_kuk_year'		=> $tahun,
				'v_jumlah'			=> $vjumlah,
				'v_sisa'			=> $vsisa
    		)
    	);
    	
    	$this->db->insert('tm_kuk');
    }
    function update($ikuk,$dkuk,$tahun,$ebankname,$isupplier,
				    $eremark,$vjumlah,$vsisa)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
    	$this->db->set(
    		array(
				'i_kuk'				=> $ikuk,
				'i_supplier'	=> $isupplier,
				'd_kuk'				=> $dkuk,
				'd_entry'			=> $dentry,
				'e_bank_name'	=> $ebankname,
				'e_remark'		=> $eremark,
				'n_kuk_year'	=> $tahun,
				'v_jumlah'		=> $vjumlah,
				'v_sisa'			=> $vsisa,
				'f_kuk_cancel'=>'f'
    		)
    	);
    	$this->db->where('i_kuk',$ikuk);
    	$this->db->where('n_kuk_year',$tahun);
    	$this->db->update('tm_kuk');
    }
	function bacaarea($num,$offset){
		$this->db->select(" * from tr_area order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function cariarea($cari,$num,$offset){
		$this->db->select(" * from tr_area 
							where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function bacasupplier($num,$offset){
		$this->db->select(" * from tr_supplier order by i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
	function carisupplier($cari,$num,$offset){
		$this->db->select(" * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%' order by i_supplier ",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}	
	}
}
?>
