<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function insert($iperiode,$iarea,$isalesman,$icity, $vareatarget, $vsalesmantarget, $vcitytarget)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$query	= $this->db->query("select * from tm_target where i_periode='$iperiode' and i_area='$iarea'",false);
		if($query->num_rows()==0){
			$this->db->query("insert into tm_target (i_periode, i_area, v_target, d_entry) values ('$iperiode', '$iarea','$vareatarget','$dentry')");
		}else{
			$this->db->query("update tm_target set v_target=$vareatarget where i_periode='$iperiode' and i_area='$iarea'");
		}
		$query	= $this->db->query("select * from tm_target_itemkota where i_periode='$iperiode' and i_area='$iarea' 
									and i_salesman='$isalesman' and i_city='$icity'",false);
		if($query->num_rows()==0){
			$this->db->query("	insert into tm_target_itemkota (i_periode, i_area, i_city, i_salesman, v_target, d_entry, d_process) 
								values ('$iperiode', '$iarea','$icity','$isalesman','$vcitytarget','$dentry','$dentry')");
		}else{
			$this->db->query("	update tm_target_itemkota set v_target='$vcitytarget' where i_periode='$iperiode' and i_area='$iarea' 
								and i_city='$icity' and i_salesman='$isalesman'");
		}
		$query	= $this->db->query("select * from tm_target_itemsls where i_periode='$iperiode' and i_area='$iarea' 
									and i_salesman='$isalesman'",false);
		if($query->num_rows()==0){
			$this->db->query("insert into tm_target_itemsls (i_periode, i_area, i_salesman, v_target) values ('$iperiode', '$iarea','$isalesman','$vsalesmantarget')");
		}else{
			$this->db->query("	update tm_target_itemsls set v_target='$vsalesmantarget' where i_periode='$iperiode' and i_area='$iarea' 
								and i_salesman='$isalesman'");
		}
//		redirect('salestarget/cform/');
    }

    function update($iperiode,$iarea,$isalesman,$icity, $vareatarget, $vsalesmantarget, $vcitytarget)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
		$query	= $this->db->query("select * from tm_target where i_periode='$iperiode' and i_area='$iarea'",false);
		if($query->num_rows()==0){
			$this->db->query("insert into tm_target (i_periode, i_area, v_target, d_entry) values ('$iperiode', '$iarea','$vareatarget','$dentry')");
		}else{
			$this->db->query("update tm_target set v_target=$vareatarget where i_periode='$iperiode' and i_area='$iarea'");
		}
		$query	= $this->db->query("select * from tm_target_itemkota where i_periode='$iperiode' and i_area='$iarea' 
									and i_salesman='$isalesman' and i_city='$icity'",false);
		if($query->num_rows()==0){
			$this->db->query("	insert into tm_target_itemkota (i_periode, i_area, i_city, i_salesman, v_target, d_entry, d_process) 
								values ('$iperiode', '$iarea','$icity','$isalesman','$vcitytarget','$dentry','$dentry')");
		}else{
			$this->db->query("	update tm_target_itemkota set v_target='$vcitytarget' where i_periode='$iperiode' and i_area='$iarea' 
								and i_city='$icity' and i_salesman='$isalesman'");
		}
		$query	= $this->db->query("select * from tm_target_itemsls where i_periode='$iperiode' and i_area='$iarea' 
									and i_salesman='$isalesman'",false);
		if($query->num_rows()==0){
			$this->db->query("insert into tm_target_itemsls (i_periode, i_area, i_salesman, v_target) values ('$iperiode', '$iarea','$isalesman','$vsalesmantarget')");
		}else{
			$this->db->query("	update tm_target_itemsls set v_target='$vsalesmantarget' where i_periode='$iperiode' and i_area='$iarea' 
								and i_salesman='$isalesman'");
		}
//		redirect('salestarget/cform/');
    }

/*
    function update($istorelocation, $estorelocationname, $istore, $istorelocationbin)
    {
		$this->db->query("update tr_store_location set e_store_locationname = '$estorelocationname'
		where i_store_location = '$istorelocation' and i_store = '$istore' 
		  and i_store_locationbin='$istorelocationbin' ");
		redirect('salestarget/cform/');
    }
*/
	
    public function delete($istorelocation,$istore,$istorelocationbin) 
    {
		$this->db->query('DELETE FROM tr_store_location WHERE i_store_location=\''.$istorelocation.'\' and i_store=\''.$istore.'\' and i_store_locationbin=\''.$istorelocationbin.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.*, b.e_store_name 
				   from tr_store_location a, tr_store b 
				   where a.i_store=b.i_store 
					 and (upper(b.e_store_name) like '%$cari%' or upper(a.i_store_location) like '%$cari%' 
					  or upper(a.e_store_locationname) like '%$cari%') 
				   order by a.i_store_location ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($iperiode,$num,$offset)
    {
		$this->db->select("	* from tm_target
							right join tr_area on (tm_target.i_area=tr_area.i_area and tm_target.i_periode='$iperiode') 
							order by tr_area.i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasalesman($iperiode,$iarea,$num,$offset)
    {
		$this->db->select("	distinct on (tr_customer_salesman.i_salesman) *,tr_customer_salesman.i_salesman from tr_customer_salesman
                        left join tm_target_itemsls on (tm_target_itemsls.i_salesman=tr_customer_salesman.i_salesman 
                        and tm_target_itemsls.i_periode='$iperiode' and tm_target_itemsls.i_area='$iarea'
                        and tm_target_itemsls.i_periode=tr_customer_salesman.e_periode 
                        and tm_target_itemsls.i_area=tr_customer_salesman.i_area)
                        where tr_customer_salesman.i_area='$iarea' and tr_customer_salesman.e_periode='$iperiode'
                        order by tr_customer_salesman.i_salesman",false)->limit($num,$offset);
/*
		$this->db->select("	* from tm_target_itemsls
							right join tr_salesman on (tm_target_itemsls.i_salesman=tr_salesman.i_salesman and tm_target_itemsls.i_periode='$iperiode' 
                                         and tm_target_itemsls.i_area='$iarea')
							where tm_target_itemsls.i_area='$iarea' order by tr_salesman.i_salesman",false)->limit($num,$offset);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacity($isalesman,$iperiode,$iarea,$num,$offset)
    {
		$this->db->select("	* from tm_target_itemkota 
							right join tr_city on (tm_target_itemkota.i_city=tr_city.i_city 
							and tm_target_itemkota.i_periode='$iperiode' and tm_target_itemkota.i_salesman='$isalesman'
              and tm_target_itemkota.i_area='$iarea') 
							where tr_city.i_area='$iarea' order by tr_city.i_city",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_store_location, a.i_store_locationbin, a.e_store_locationname, a.i_store, b.e_store_name 
				   from tr_store_location a, tr_store b where (upper(a.e_store_locationname) like '%$cari%' 
					 or upper(a.i_store_location) like '%$cari%' or upper(b.e_store_name) like '%$cari%' 
					 or upper(a.i_store) like '%$cari%') and a.i_store=b.i_store 
				   order by a.i_store_location",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariarea($iperiode,$cari,$num,$offset)
    {
		$this->db->select("	* from tm_target
							right join tr_area on (tm_target.i_area=tr_area.i_area and tm_target.i_periode='$iperiode')
							where tr_area.i_area like '%$cari%' or tr_area.e_area_name like '%$cari%'
							order by tr_area.i_area",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisalesman($iperiode,$iarea,$cari,$num,$offset)
    {
		$this->db->select("	distinct on (tr_customer_salesman.i_salesman) *,tr_customer_salesman.i_salesman from tr_customer_salesman
                        left join tm_target_itemsls on (tm_target_itemsls.i_salesman=tr_customer_salesman.i_salesman 
                        and tm_target_itemsls.i_periode='$iperiode' and tm_target_itemsls.i_area='$iarea'
                        and tm_target_itemsls.i_periode=tr_customer_salesman.e_periode 
                        and tm_target_itemsls.i_area=tr_customer_salesman.i_area)
                        where tr_customer_salesman.i_area='$iarea' and tr_customer_salesman.e_periode='$iperiode'
                        and (tr_customer_salesman.i_salesman like '%$cari%' or tr_customer_salesman.e_salesman_name like '%$cari%')",
                        false)->limit($num,$offset);
/*
		$this->db->select("	* from tm_target_itemsls
							right join tr_salesman on (tm_target_itemsls.i_salesman=tr_salesman.i_salesman
							and tm_target_itemsls.i_periode='$iperiode' and tm_target_itemsls.i_area='$iarea')
							where tm_target_itemsls.i_area='$iarea' 
							and (tr_salesman.i_salesman like '%$cari%' or tr_salesman.e_salesman_name like '%$cari%')
							order by tr_salesman.i_salesman",false)->limit($num,$offset);
*/
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricity($isalesman,$iperiode,$iarea,$cari,$num,$offset)
    {
		$this->db->select("	* from tm_target_itemkota 
							right join tr_city on (tm_target_itemkota.i_city=tr_city.i_city 
							and tm_target_itemkota.i_periode='$iperiode' and tm_target_itemkota.i_salesman='$isalesman') 
							where tr_city.i_area='$iarea' 
							and (tr_city.e_city_name like '%$cari%' or tr_city.i_city like '%$cari%')",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
