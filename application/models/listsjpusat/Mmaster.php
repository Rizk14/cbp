<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($isj,$iarea) 
    {
			$this->db->query(" update tm_nota set f_nota_cancel='t' WHERE i_sj='$isj' and i_area='$iarea'");
    }
    function baca($isj,$iarea)
    {
		$this->db->select(" a.i_nota, a.d_sj, a.d_spb, a.i_sj, a.i_area, a.i_spb, a.i_sj_old, a.v_nota_netto, a.i_customer, c.e_customer_name,
                        b.e_area_name, d.v_spb, a.i_nota from tm_nota a, tr_area b, tr_customer c, tm_spb d
						   					where a.i_area=b.i_area and a.i_customer=c.i_customer
                        and a.i_spb=d.i_spb and a.i_area=d.i_area
						   					and a.i_sj ='$isj' ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($isj, $iarea)
    {
		$this->db->select(" a.i_product_motif, a.i_product, a.e_product_name, b.e_product_motifname, d.v_unit_price as harga,
                        a.v_unit_price, a.n_deliver, d.n_order, a.i_product_grade, d.n_order as n_qty, e.v_spb
                        from tm_nota_item a, tr_product_motif b, tm_nota c, tm_spb_item d, tm_spb e
                        where a.i_sj = '$isj' and a.i_product=b.i_product 
                        and a.i_sj=c.i_sj and a.i_area=c.i_area
                        and c.i_spb=e.i_spb and e.i_spb=d.i_spb and c.i_area=e.i_area
                        and c.i_spb=d.i_spb and c.i_area=d.i_area and a.i_product=d.i_product 
                        and a.i_product_grade=d.i_product_grade and a.i_product_motif=d.i_product_motif
                        and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no ", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			$this->db->select(" a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
						where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
						and (a.i_area_from='$area1' or a.i_area_from='$area2' or a.i_area_from='$area3' or a.i_area_from='$area4' or a.i_area_from='$area5')
						and (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%')
						order by a.i_sj desc",FALSE)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      //$area1	= $this->session->userdata('i_area');
			//if($area1!='00'){
			  $this->db->select("	a.i_sj , a.d_sj, a.i_customer, c.e_customer_name, a.i_area, a.i_spb, a.i_dkb, a.d_dkb,
                            a.f_nota_cancel, a.i_sj_old, a.i_bapb, a.d_bapb,a.i_spb, a.d_spb, e.v_spb, a.i_nota
                            from tr_area b, tr_customer c, tm_spb e, tm_nota a 
                            left join tm_bapb d on(a.i_bapb=d.i_bapb and a.i_area=d.i_area)
			                      where a.i_area=b.i_area and a.i_customer=c.i_customer and
				                    substr(a.i_sj,9,2) = '00' and a.i_spb=e.i_spb and a.i_area=e.i_area and
                            (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_dkb) like '%$cari%') and 
				                    a.i_area='$iarea' and 
				                    (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy'))
                            order by a.i_sj asc, a.d_sj desc ",false)->limit($num,$offset);
      //}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
			$this->db->select("	a.i_sj, a.d_sj, a.i_customer, c.e_customer_name, a.i_area, a.i_spb, a.i_dkb, a.d_dkb,
                          a.i_bapb, a.d_bapb, a.f_nota_cancel, a.i_sj_old, a.i_spb, a.d_spb, a.i_nota
                          from tm_nota a, tr_area b, tr_customer c
				                  where a.i_area=b.i_area and a.i_customer=c.i_customer and
                          (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_dkb) like '%$cari%')
					                and substr(a.i_sj,9,2) = '00' and
					                a.i_area='$iarea' and 
					                (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_sj asc, a.d_sj desc','dd-mm-yyyy')",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
}
?>
