<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    
    /*function bacaperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	x.i_op, x.d_op, x.i_supplier, x.i_area, x.e_supplier_name, x.e_area_name, x.i_op_old, d.i_do, d.d_do from
								(
								SELECT a.i_op, a.d_op, a.i_supplier, a.i_area, a.i_op_old, b.e_supplier_name, c.e_area_name
								FROM tm_op a, tr_supplier b, tr_area c
								WHERE 
								a.i_supplier = b.i_supplier AND
								a.i_area = c.i_area AND
								a.i_supplier='$isupplier' AND
								a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_op <= to_date('$dto','dd-mm-yyyy')
								) as x
							left join tm_do d on (x.i_op=d.i_op AND x.i_area=d.i_area)
							ORDER BY x.i_op ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }*/
    
    // 20-12-2013
    //function bacaperiode($userid, $dfrom,$dto,$num,$offset,$cari) {
		function bacaperiode($userid, $dfrom,$dto,$cari) {
			// -----------------------------------------------------------------------------------------------------
			//cekareapusat
			//if ($offset == '') $offset = 0;
			if ($cari == "all") $cari = '';
			$isadapusat = 0; $areanya = "";
			/*$this->db->select(" a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($cari)."%'  order by a.i_area ")->limit($num,$offset);
			$sqlnya = $this->db->get(); */
			$sqlnya = $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($cari)."%' 
										AND a.i_area <> 'XX' order by a.i_area ");
			if ($sqlnya->num_rows() > 0){
				$hasilnya=$sqlnya->result();
				foreach ($hasilnya as $rownya) {
					if ($rownya->i_area == '00')
						$isadapusat = '1';
					$areanya.= $rownya->i_area.";";
				}
			}
			
			// query utk ambil data di tm_spb berdasarkan areanya. Jika isadapusat = 1 maka ga pake filter area
			$outputdata = array();
			if ($isadapusat == '0') {
				$listarea = explode(";", $areanya);
				foreach ($listarea as $rowarea) {
					$rowarea = trim($rowarea);
					if ($rowarea != '') {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
						
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '$rowarea' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' AND a.i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $rowarea,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					}
				}
			}
			else {
				/*$this->db->select(" a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($cari)."%'  order by a.i_area ")->limit($num,$offset);
				$sqlnya = $this->db->get(); */
				$sqlnya = $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($cari)."%' 
										AND a.i_area <> 'XX' order by a.i_area ");
				
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
							
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$rownya->i_area."' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' AND a.i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $rownya->i_area,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					}
				}
			}
			return $outputdata;
			
			// -----------------------------------------------------------------------------------------------------
	}
    
/*    function cariperiode($isupplier,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	x.i_op, x.d_op, x.i_supplier, x.i_area, x.e_supplier_name, x.e_area_name, x.i_op_old, d.i_do, d.d_do from
								(
								SELECT a.i_op, a.d_op, a.i_supplier, a.i_area, a.i_op_old, b.e_supplier_name, c.e_area_name
								FROM tm_op a, tr_supplier b, tr_area c
								WHERE 
								a.i_supplier = b.i_supplier AND
								a.i_area = c.i_area AND
								a.i_supplier='$isupplier' AND
								a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_op <= to_date('$dto','dd-mm-yyyy') AND
								(upper(a.i_op) like '%$cari%' or upper(a.i_area) like '%$cari%')
								) as x
							left join tm_do d on (x.i_op=d.i_op AND x.i_area=d.i_area and (upper(d.i_do) like '%$cari%'))
							ORDER BY x.i_op ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacasupplier($num,$offset)
    {
		$this->db->select("	* FROM tr_supplier ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function carisupplier($cari,$num,$offset)
    {
		$this->db->select("	* FROM tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') 
							ORDER BY i_supplier ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    } */
}
?>
