<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$num,$offset,$dfrom,$dto)
    {
		  $this->db->select(" 	a.*, b.e_customer_name, c.e_customer_pkpnpwp from tm_kn a, tr_customer b
		        left join tr_customer_pkp c on (b.i_customer=c.i_customer)
					  where a.i_customer=b.i_customer 
					  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					  or upper(a.i_kn) like '%$cari%') 
            and a.d_kn >= to_date('$dfrom','dd-mm-yyyy') and a.d_kn <= to_date('$dto','dd-mm-yyyy')
            and a.i_area='$iarea'
					  order by a.i_kn desc",false)->limit($num,$offset);
#and (a.d_cetak isnull)
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($ikn,$area)
    {
		  $this->db->select(" a.*, b.e_customer_name as customer_name, c.e_customer_pkpname as e_customer_name, 
                          b.e_customer_address as customer_address, c.e_customer_pkpaddress as e_customer_address, 
                          b.f_customer_plusppn, b.e_customer_city, c.e_customer_pkpnpwp, d.e_area_name
					                from tr_area d, tr_customer b, tm_kn a
                          left join tr_customer_pkp c on (a.i_customer=c.i_customer)
					                where a.i_kn = '$ikn' and a.i_area='$area' 
					                and a.i_customer=b.i_customer
					                and a.i_area=d.i_area
					                order by a.i_kn desc",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($ikn,$area)
    {
		  $query=$this->db->query(" select i_refference from tm_kn where i_kn = '$ikn' and i_area='$area' ",false);
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $tes){
          $reff=$tes->i_refference;
        }
		  }
		  $this->db->select(" 	* from tm_bbm_item
					  inner join tr_product_motif on (tm_bbm_item.i_product_motif=tr_product_motif.i_product_motif
					  and tm_bbm_item.i_product=tr_product_motif.i_product)
					  where i_bbm = '$reff' order by n_item_no",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($area1,$area2,$area3,$area4,$area5,$cari,$num,$offset)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_kn a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_kn) like '%$cari%')
					and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
					order by a.i_kn desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function close($area,$ikn)
    {
		$this->db->query("	update tm_kn set n_print=n_print+1 
          							where i_kn = '$ikn' and i_area = '$area' ",false);
    }
}
?>
