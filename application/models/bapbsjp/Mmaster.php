<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibapb,$iarea)
    {
		$this->db->select(" tm_bapbsjp.i_bapb, tm_bapbsjp.i_dkb_kirim, tm_bapbsjp.i_area, tm_bapbsjp.d_bapb, tm_bapbsjp.i_bapb_old,
							          tm_bapbsjp.f_bapb_cancel, tm_bapbsjp.n_bal, tr_customer.e_customer_name, tm_bapbsjp.i_customer,
							          tr_area.e_area_name, tr_dkb_kirim.e_dkb_kirim, tm_bapbsjp.v_bapb, tm_bapbsjp.v_kirim
							          from tm_bapbsjp 
							          inner join tr_area on(tm_bapbsjp.i_area=tr_area.i_area)
							          left join tr_customer on(tm_bapbsjp.i_customer=tr_customer.i_customer)
							          inner join tr_dkb_kirim on(tm_bapbsjp.i_dkb_kirim=tr_dkb_kirim.i_dkb_kirim)
							          where tm_bapbsjp.i_bapb ='$ibapb' and tm_bapbsjp.i_area='$iarea'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ibapb,$iarea)
    {
		$this->db->select("* from tm_bapbsjp_item
										   where i_bapb = '$ibapb' and i_area='$iarea' order by i_bapb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetailx($ibapb,$iarea)
    {
		$this->db->select("a.*, b.e_ekspedisi from tm_bapbsjp_ekspedisi a, tr_ekspedisi b
										   where a.i_bapb = '$ibapb' and a.i_area='$iarea' and a.i_ekspedisi = b.i_ekspedisi order by a.i_bapb", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function insertheader($ibapb, $dbapb, $iarea, $idkbkirim, $nbal, $ibapbold, $vbapb, $vkirim)
    {
    	$this->db->set(
    		array(
			'i_bapb'			=> $ibapb,
			'd_bapb'			=> $dbapb,
			'i_dkb_kirim'	=> $idkbkirim,
			'i_area'			=> $iarea,
			'n_bal'				=> $nbal,
			'i_bapb_old'	=> $ibapbold,
			'v_bapb'      => $vbapb,
			'v_kirim'     => $vkirim
    		)
    	);
    	$this->db->insert('tm_bapbsjp');
    }
   function insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj)
   {
		$this->db->query("DELETE FROM tm_bapbsjp_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sj='$isj'");
		$this->db->set(
			array(
						'i_bapb'	=> $ibapb,
						'i_area'	=> $iarea,
	 					'i_sj'	 	=> $isj,
	 					'd_bapb' 	=> $dbapb,
	 					'd_sj'	 	=> $dsj,
	 					'e_remark'=> $eremark,
	      		'v_sj'    => $vsj
			)
		);
		$this->db->insert('tm_bapbsjp_item');
  }
	function updatesj($ibapb,$isj,$iarea,$dbapb)
    {
    	$this->db->set(
    		array(
			'i_bapb'	=> $ibapb,	
			'd_bapb' => $dbapb
    		)
    	);
    	$this->db->where('i_sjp',$isj);
    	$this->db->where('i_area',$iarea);
    	$this->db->update('tm_sjp');
    }
   function insertdetailekspedisi($ibapb,$iarea,$iekspedisi,$dbapb,$eremark)
   {
		$this->db->query("DELETE FROM tm_bapbsjp_ekspedisi WHERE i_bapb='$ibapb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
		$this->db->set(
			array(
						'i_bapb'			=> $ibapb,
						'i_area'			=> $iarea,
	 					'i_ekspedisi'	=> $iekspedisi,
	 					'd_bapb' 			=> $dbapb,
						'e_remark'		=> $eremark
			)
		);
		$this->db->insert('tm_bapbsjp_ekspedisi');
  }
    function updateheader($ispmb, $dspmb, $iarea, $ispmbold)
    {
    	$this->db->set(
    		array(
			'd_spmb'	=> $dspmb,
			'i_spmb_old'=> $ispmbold,
			'i_area'	=> $iarea
    		)
    	);
    	$this->db->where('i_spmb',$ispmb);
    	$this->db->update('tm_spmb');
    }

    public function deletedetail($ibapb, $iarea, $isj, $daer) 
    {
		$this->db->select(" * from tm_bapbsjp_item where i_bapb = '$ibapb' and i_area='$iarea' and i_sj='$isj'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$this->db->query("update tm_sjp set i_bapb=null and d_bapb=null WHERE i_sjp='$isj' and i_area='$iarea'");
			}
		}
		$this->db->query("DELETE FROM tm_bapbsjp_item WHERE i_bapb='$ibapb' and i_area='$iarea' and i_sj='$isj'");
    }
	
		public function deletedetailekspedisi($ibapb, $iarea, $iekspedisi) 
    {
			$this->db->query("DELETE FROM tm_bapbsjp_ekspedisi WHERE i_bapb='$ibapb' and i_area='$iarea' and i_ekspedisi='$iekspedisi'");
    }

    public function deleteheader($ibapb, $iarea) 
    {
		$this->db->query("DELETE FROM tm_bapbsjp WHERE i_bapb='$ibapb' and i_area='$iarea'");
    }

    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							from tr_product_motif a,tr_product c
							where a.i_product=c.i_product limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function runningnumber($iarea,$thbl){
#    	$query 	= $this->db->query("SELECT to_char(current_timestamp,'yymm') as c");
#			$row   	= $query->row();
#			$thbl		= $row->c;
			$th			= substr($thbl,0,2);
			$this->db->select(" max(substr(i_bapb,11,6)) as max from tm_bapbsjp where substr(i_bapb,6,2)='$th' and i_area='$iarea'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				foreach($query->result() as $row){
					$terakhir=$row->max;
				}
				$nobapb  =$terakhir+1;
				settype($nobapb,"string");
				$a=strlen($nobapb);
				while($a<6){
					$nobapb="0".$nobapb;
					$a=strlen($nobapb);
				}
				$nobapb  ="BAPB-".$thbl."-".$nobapb;
				return $nobapb;
			}else{
				$nobapb  ="000001";
				$nobapb  ="BAPB-".$thbl."-".$nobapb;
				return $nobapb;
			}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								limit $num offset $offset",false);
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$iuser)
    {
		$this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$iuser)
    {
		$this->db->select(" * from tr_area
                          where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
                          and i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", false)->limit($num, $offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacakirim($num,$offset)
    {
		$this->db->select("* from tr_dkb_kirim order by i_dkb_kirim", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carikirim($cari,$num,$offset)
    {
		$this->db->select("i_dkb_kirim, e_dkb_kirim from tr_dkb_kirim where upper(e_dkb_kirim) like '%$cari%' or upper(i_dkb_kirim) like '%$cari%' order by i_dkb_kirim ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacavia($num,$offset)
    {
		$this->db->select("* from tr_dkb_via order by i_dkb_via", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function carivia($cari,$num,$offset)
    {
		$this->db->select("i_dkb_via, e_dkb_via from tr_dkb_via where upper(e_dkb_via) like '%$cari%' or upper(i_dkb_via) like '%$cari%' order by i_dkb_via ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaekspedisi($num,$offset)
    {
		$this->db->select("* from tr_ekspedisi order by i_ekspedisi", false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariekspedisi($cari,$num,$offset)
    {
		$this->db->select("i_ekspedisi, e_ekspedisi from tr_ekspedisi where upper(e_ekspedisi) like '%$cari%' or upper(i_ekspedisi) like '%$cari%' order by i_ekspedisi ", FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacasj2($iarea,$num,$offset)
  {
    $area1	= $this->session->userdata('i_area');
    if($area1=='00'){
		  $this->db->select("	a.* from tm_sjp a  
				                  where a.i_area='$iarea' and a.i_bapb isnull
				                  order by a.i_sjp", false)->limit($num,$offset);
    }else{
		  $this->db->select("	a.* from tm_sjp a
				                  where a.i_area='$iarea' and a.i_bapb isnull
                          order by a.i_sjp", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
	function carisj($iarea,$cari,$num,$offset)
    {
    $area1	= $this->session->userdata('i_area');
    if($area1=='00'){
	    $this->db->select("	a.* from tm_sjp a
			    where a.i_area='$iarea' and a.i_bapb isnull
	       	and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
          order by a.i_sjp ", FALSE)->limit($num,$offset);
    }else{
	    $this->db->select("	a.* from tm_sjp a
			    where a.i_area='$iarea' and a.i_bapb isnull
	       	and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
			    order by a.i_sjp ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacacustomer($iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) where a.i_area='$iarea'
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caricustomer($cari,$iarea,$num,$offset)
    {
		$this->db->select(" * from tr_customer a 
							left join tr_customer_area d on
							(a.i_customer=d.i_customer) 
							where a.i_area='$iarea' and
							(upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
							order by a.i_customer",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
