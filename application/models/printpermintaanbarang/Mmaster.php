<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		return TRUE;
    }
    function bacasemua($iarea,$dfrom,$dto,$iuser,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where 
							a.i_area=b.i_area
							and (upper(a.i_spmb) like '%$cari%')
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and a.i_area='$iarea'
							and d_spmb>= to_date('$dfrom','dd-mm-yyyy')
							and d_spmb<= to_date('$dto','dd-mm-yyyy')
							order by a.i_spmb desc",false)->limit($num,$offset);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaproductgroup()
    {
		$this->db->select(" * from tr_product_group",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($iarea,$dfrom,$dto,$iuser,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where 
							a.i_area=b.i_area
							and (upper(a.i_spmb) like '%$cari%')
							and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
							and d_spmb>= to_date('$dfrom','dd-mm-yyyy')
							and d_spmb<= to_date('$dto','dd-mm-yyyy')
							order by a.i_spmb desc",FALSE)->limit($num,$offset);
							
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($dfrom,$dto,$iproductgroup)
    {
		$this->db->select(" a.i_area , c.e_area_name , a.i_spb , a.d_spb, e.e_customer_name, b.i_product , d.e_product_name , n_order , n_deliver 
							from tm_spb a , tm_spb_item b , tr_area c , tr_product d , tr_customer e
							where a.i_spb=b.i_spb 
							and a.i_area=b.i_area
							and a.f_spb_cancel='false'
							and a.f_spb_stockdaerah='false'
							and not b.n_deliver isnull
							and a.i_sj isnull 
							and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')
							and b.n_deliver < b.n_order
							and a.i_product_group = '$iproductgroup'
							and a.i_area=c.i_area
							and b.i_area=c.i_area
							and b.i_product=d.i_product
							and a.i_customer=e.i_customer
							order by a.i_area , a.i_spb , a.d_spb ,b.n_item_no ",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ispmb)
    {
		$this->db->select(" * from tm_spmb_item
							inner join tr_product_motif on (tm_spmb_item.i_product_motif=tr_product_motif.i_product_motif
							and tm_spmb_item.i_product=tr_product_motif.i_product)
							where i_spmb = '$ispmb' 
							
							order by n_item_no",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  
		  					order by i_area", false)->limit($num,$offset);
		  
		  $query = $this->db->get();
		  if ($query->num_rows() > 0)
		  	{
			  return $query->result();
		  	}
    }
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
/*DICOMMENT TANGGAL 14-07-2017
function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$fltr_area	= " ";
		} else {
			$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
		}
					
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area ".$fltr_area."
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%')
							order by a.i_spmb desc",false)->limit($num,$offset);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ispmb)
    {
		$this->db->select(" * from tm_spmb
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
							where tm_spmb.i_spmb = '$ispmb'
							order by tm_spmb.i_spmb desc",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacadetail($ispmb)
    {
		$this->db->select(" * from tm_spmb_item
							inner join tr_product_motif on (tm_spmb_item.i_product_motif=tr_product_motif.i_product_motif
														and tm_spmb_item.i_product=tr_product_motif.i_product)
							where i_spmb = '$ispmb' order by n_item_no",false);
		
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
			$fltr_area	= " ";
		} else {
			$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
		}
					
		$this->db->select(" a.*, b.e_area_name from tm_spmb a, tr_area b
							where a.i_area=b.i_area ".$fltr_area."
							and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							or upper(a.i_spmb) like '%$cari%')
							order by a.i_spmb desc",FALSE)->limit($num,$offset);
							
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
*/    
}
?>
