<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num, $offset)
    {
		  $this->db->select(" distinct(a.i_sjp), a.d_sjp, a.d_sjp_receive, c.i_area, c.e_area_name
                        from tm_sjp a, tm_sjp_item b, tr_area c
                        where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and b.i_area=c.i_area
                        and (a.i_sjp like '%$cari%' or b.i_product like '%$cari%' or b.e_product_name like '%$cari%')
                        and (b.n_quantity_receive is null
                        or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                        order by a.i_sjp",false)->limit($num,$offset);

		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function baca($isjp,$iarea)
    {
		$this->db->select(" distinct(c.i_store), a.*, b.e_area_name 
                        from tm_sjp a, tr_area b, tm_sjp_item c
						            where a.i_area=b.i_area and a.i_sjp=c.i_sjp 
                        and a.i_area=c.i_area
						            and a.i_sjp ='$isjp' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($isjp, $iarea)
    {
		$this->db->select("a.i_sjp,a.d_sjp,a.i_area,a.i_product,a.i_product_grade,a.i_product_motif,
                       a.n_quantity_receive,a.n_quantity_deliver,a.v_unit_price,a.e_product_name,
                       a.i_store,a.i_store_location,a.i_store_locationbin,a.e_remark,
                       b.e_product_motifname from tm_sjp_item a, tr_product_motif b
				               where a.i_sjp = '$isjp' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
