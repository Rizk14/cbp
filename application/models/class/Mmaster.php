<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iclass)
    {
		$query = $this->db->query("select i_product_class, e_product_classname, to_char(d_product_classregister,'dd-mm-yyyy') as d_product_classregister 
					 from tr_product_class where i_product_class='$iclass'");
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iclass, $eclassname, $dclassregister)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->query("insert into tr_product_class (i_product_class, e_product_classname, d_product_classregister, d_product_classentry) values ('$iclass', '$eclassname',to_date('$dclassregister','dd-mm-yyyy'), '$dentry')");
		redirect('class/cform/');
    }
    function update($iclass, $eclassname, $dclassregister)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
		$this->db->query("update tr_product_class set e_product_classname = '$eclassname', d_product_classregister = to_date('$dclassregister','dd-mm-yyyy'), d_product_classupdate = '$dupdate' where i_product_class = '$iclass'");
		redirect('class/cform/');
    }
	
    public function delete($iclass) 
    {
		$this->db->query('DELETE FROM tr_product_class WHERE i_product_class=\''.$iclass.'\'');
		return TRUE;
    }
    function bacasemua($num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname, to_char(d_product_classregister,'dd-mm-yyyy') as d_product_classregister from tr_product_class order by i_product_class",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select("i_product_class, e_product_classname, to_char(d_product_classregister,'dd-mm-yyyy') as d_product_classregister from tr_product_class where upper(e_product_classname) like '%$cari%' or upper(i_product_class) like '%$cari%' or to_char(d_product_classregister,'dd-mm-yyyy') like '%$cari%' order by i_product_class",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
