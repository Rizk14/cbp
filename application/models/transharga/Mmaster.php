<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
  function insertdetail($kodeprod, $grade, $kodeharga, $nama, $harga, $hjp, $margin)
  {
   	$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$update	= $row->c;
		$entry	= $row->c;
    $que=$this->db->query(" select i_product from tr_product_price 
                            where i_product='$kodeprod' and i_product_grade='$grade' and i_price_group='$kodeharga'");
    if($que->num_rows()>0){
    	$this->db->set(
    		array(
          'v_product_retail'      => $harga,
          'd_product_priceupdate' => $update
    		)
    	);
    	$this->db->where('i_product',$kodeprod);
    	$this->db->where('i_product_grade',$grade);
    	$this->db->where('i_price_group',$kodeharga);
    	$this->db->update('tr_product_price');
    }else{
    	$this->db->set(
    		array(
          'i_product'             => $kodeprod,
          'i_product_grade'       => $grade,
          'i_price_group'         => $kodeharga,
          'e_product_name'        => $nama,
          'v_product_retail'      => $harga,
          'v_product_mill'        => $hjp,
          'd_product_priceentry'  => $entry
    		)
    	);
    	$this->db->insert('tr_product_price');
    }
    if($kodeharga=='00'){
      $this->db->set(
        array(
          'v_product_retail'      => $harga,
          'd_product_update'      => $update
        )
      );
      $this->db->where('i_product',$kodeprod);
      $this->db->update('tr_product');
    }
  }
}
?>
