<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
//		$this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
//		$this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
		$this->db->query("update tm_spmb set f_spmb_cancel='t' WHERE i_spmb='$ispmb'");
		return TRUE;
    }
    function deleterekap($ispmb)
    {
      $query=$this->db->query("select i_spb from tm_spb where i_spmb='$ispmb'");
      if($query->num_rows()>0){
        foreach($query->result() as $row){
          $spb=$row->i_spb;
        }
        $this->db->update("update tm_spb set i_spmb=null, f_spb_rekap='f' where i_spb='$spb'");
      }

      $que=$this->db->query("select i_orderpb from tm_orderpb where i_spmb='$ispmb'");
      if($que->num_rows()>0){
        foreach($que->result() as $row){
          $orderpb=$row->i_orderpb;
        }
        $this->db->update("update tm_orderpb set i_spmb=null, f_orderpb_rekap='f' where i_orderpb='$orderpb'");
      }
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_store_name as e_area_name from tm_spmb a, tr_store b
							          where a.i_area=b.i_store and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%' or upper(a.i_spmb_old) like '%$cari%')
					order by a.i_spmb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function bacaarea($num,$offset,$iuser)
  {
	  $this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
                        and not a.i_store in ('AA','PB') and c.i_store_location='00'
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
# and i_store_location='00'
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
  function cariarea($cari,$num,$offset,$iuser)
  {
	  $this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                         from tr_area a, tr_store b, tr_store_location c
                         where  a.i_store=b.i_store and b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						 		and (i_area in( select i_area from tm_user_area where i_user='$iuser') )
						 		and not a.i_store in ('AA','PB') and c.i_store_location='00' 
						 		order by a.i_store ", FALSE)->limit($num,$offset);
# and i_store_location='00'
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	  return $query->result();
	}
  }
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($istore,$dfrom,$dto,$num,$offset,$cari)
    {
      if($istore=='00'){
        $xistore='AA';
      }else{
        $xistore=$istore;
      }

		  $this->db->select(" a.*, b.e_area_name from tm_spmb a,tr_area b
						              where  a.i_area=b.i_area 
						              and (upper(a.i_spmb) like '%$cari%')
						              and b.i_store='$xistore' and
						              a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
						              a.d_spmb <= to_date('$dto','dd-mm-yyyy')
						              ORDER BY i_spmb asc ",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
      if($istore=='00')$istore=='AA';
    		$this->db->select("	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_area b
						where a.i_area=b.i_area and a.f_spmb_cancel='f'
						and (upper(a.i_spmb) like '%$cari%')
						and a.i_area='$iarea' and
						a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
						a.d_spmb <= to_date('$dto','dd-mm-yyyy')
						ORDER BY e_area_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacastore($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            order by c.i_store", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													   c.i_area = '$area3' or c.i_area = '$area4' or
													   c.i_area = '$area5')
                            order by c.i_store", false)->limit($num,$offset);			
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(b.e_store_name) like '%$cari%'
                            or upper(a.i_store_location) like '%$cari%'
                            or upper(a.e_store_locationname) like '%$cari%')
                            order by c.i_store", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(b.e_store_name) like '%$cari%'
                            or upper(a.i_store_location) like '%$cari%'
                            or upper(a.e_store_locationname) like '%$cari%')
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													  c.i_area = '$area3' or c.i_area = '$area4' or
													  c.i_area = '$area5')
                            order by c.i_store", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
