<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
    parent::__construct();
	  #$this->CI =& get_instance();
  }
  function cekopname($iperiode)
  {
    $komplit=false;
    $store='';
    $this->db->select(" i_store from tr_store where f_aktif='t' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $store=$row->i_store;
			  $this->db->select(" i_stockopname_akhir from tm_mutasi_header 
			                      where e_mutasi_periode='$iperiode' and i_store='$store' ",false);
	      $que = $this->db->get();
	      if ($que->num_rows() > 0){
		      foreach($que->result() as $ro){
			      if($ro->i_stockopname_akhir==null || $ro->i_stockopname_akhir==''){
              $komplit=false;
              break 2;
			      }else{
			        $komplit=true;
			      }
		      }
	      }else{
	        $komplit=false;
	        break;
	      }
		  }
	  }
	  if($komplit){
	    $komplit=false;
	    $store='';
	    $this->db->select(" i_customer from tr_customer_consigment where i_customer like 'PB%' order by i_customer ",false);
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
		    foreach($query->result() as $row){
			    $store=$row->i_customer;
			    $this->db->select(" i_stockopname_akhir from tm_mutasi_headerconsigment
			                        where e_mutasi_periode='$iperiode' and i_customer='$store' ",false);
	        $que = $this->db->get();
	        if ($que->num_rows() > 0){
		        foreach($que->result() as $ro){
			        if($ro->i_stockopname_akhir==null || $ro->i_stockopname_akhir==''){
                $komplit=false;
#                echo 'toko : '.$store.'<br>';
                break 2;
			        }else{
			          $komplit=true;
			        }
		        }
	        }else{
#            echo 'toko : '.$store.'<br>';
	          $komplit=false;
	          break;
	        }
		    }
	    }
	  }
#	  $komplit=false;
#	  die;
    $komplit=true;
	  return array($komplit,$store);
  }
  
  function cekap($iperiode)
  {
    $komplit=false;
    $do='';
	  $this->db->select(" i_do from tm_do where to_char(d_do,'yyyymm')='$iperiode' and f_do_cancel='f' 
	                      and i_do not in(select b.i_do from tm_dtap a, tm_dtap_item b 
	                      where a.i_dtap=b.i_dtap and a.i_supplier=b.i_supplier and to_char(a.d_dtap,'yyyymm')='$iperiode'
                        and a.f_dtap_cancel='f')",false);
    $que = $this->db->get();
    if ($que->num_rows() > 0){
      foreach($que->result() as $row){
        $do=$row->i_do;
        break;
      }
      $komplit=false;
    }else{
      $komplit=true;
    }
	  return array($komplit,$do);
  }

  function cekar($iperiode)
  {
#    $komplit=false;
    $sj='';
#	  $this->db->select(" i_sj from tm_nota where to_char(d_sj_receive,'yyyymm')='$iperiode' and f_nota_cancel='f' 
#	                      and i_nota isnull",false);
#    $que = $this->db->get();
#    if ($que->num_rows() > 0){
#      $komplit=false;
#      foreach($que->result() as $row){
#        $sj=$row->i_sj;
#        break;
#      }
#    }else{
      $komplit=true;
#    }
	  return array($komplit,$sj);
  }

  function pindah($iperiode)
  {
    $user=$this->session->userdata('user_id');
    $emutasiperiode=$iperiode;
    $bldpn=substr($emutasiperiode,4,2)+1;
    if($bldpn==13)
    {
      $perdpn=substr($emutasiperiode,0,4)+1;
      $perdpn=$perdpn.'01';
    }else{
      $perdpn=substr($emutasiperiode,0,4);
      $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
    }
    $xperiode=$perdpn;
    
    $store='';
    $this->db->select(" i_store from tr_store where f_aktif='t' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_stock where i_periode='$xperiode'",false);
		  foreach($query->result() as $row){
			  $store=$row->i_store;
			  $this->db->select(" i_stockopname_akhir from tm_mutasi_header 
			                      where e_mutasi_periode='$iperiode' and i_store='$store' ",false);
	      $que = $this->db->get();
	      if ($que->num_rows() > 0){
		      foreach($que->result() as $ro){
			      $iopname=$ro->i_stockopname_akhir;
			      $this->db->select(" a.*, b.v_product_mill from tm_stockopname_item a, tr_product b
			                          where a.i_stockopname='$iopname' and a.i_store='$store' and a.i_product=b.i_product ",false);
	          $qu = $this->db->get();
	          if ($qu->num_rows() > 0){
		          foreach($qu->result() as $r){
		            if($r->n_stockopname==null)$r->n_stockopname=0;
		            $query 	= $this->db->query("SELECT current_timestamp as c");
                $row   	= $query->row();
            	  $entry	= $row->c;
                $this->db->query(" insert into tm_saldo_stock (i_periode, i_store, i_product, i_product_motif, i_product_grade, 
                                   n_quantity_awal, n_quantity_in, 
                                   n_quantity_out, n_quantity_akhir, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                                   d_entry, i_entry, d_update, i_update) values ('$xperiode', '$r->i_store', '$r->i_product', 
                                   '$r->i_product_motif', '$r->i_product_grade', 
                                   $r->n_stockopname, 0, 0, $r->n_stockopname, $r->n_stockopname*$r->v_product_mill, 0, 0, 
                                   $r->n_stockopname*$r->v_product_mill, '$entry', '$user', null, null) ",false);
                $this->db->query(" update tm_saldo_stock set n_quantity_akhir=$r->n_stockopname, 
                                   v_saldo_akhir=$r->n_stockopname*$r->v_product_mill where i_periode='$iperiode' 
                                   and i_store='$store' and i_product='$r->i_product' and i_product_motif='$r->i_product_motif' 
                                   and i_product_grade='$r->i_product_grade'",false);
              }
		        }
		      }
	      }
      }
	  }
	  
    $cust='';
    $this->db->select(" i_customer from tr_customer_consigment where i_customer like 'PB%' ",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_stockcons where i_periode='$xperiode'",false);
	    foreach($query->result() as $row){
		    $cust=$row->i_customer;
		    $this->db->select(" i_stockopname_akhir from tm_mutasi_headerconsigment
		                        where e_mutasi_periode='$iperiode' and i_customer='$cust' ",false);
        $que = $this->db->get();
        if ($que->num_rows() > 0){
	        foreach($que->result() as $ro){
		        $iopname=$ro->i_stockopname_akhir;
			      $this->db->select(" a.*, b.v_product_mill from tm_sopb_item a, tr_product b
			                          where a.i_sopb='$iopname' and a.i_customer='$cust' and a.i_product=b.i_product ",false);
	          $qu = $this->db->get();
	          if ($qu->num_rows() > 0){
		          foreach($qu->result() as $r){
		            if($r->n_sopb==null)$r->n_sopb=0;
		            $query 	= $this->db->query("SELECT current_timestamp as c");
                $row   	= $query->row();
            	  $entry	= $row->c;
                $this->db->query(" insert into tm_saldo_stockcons (i_periode, i_customer, i_product, i_product_motif, i_product_grade, 
                                   n_quantity_awal, n_quantity_in, 
                                   n_quantity_out, n_quantity_akhir, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry, 
                                   i_entry, d_update, i_update) values ('$xperiode', '$r->i_customer', '$r->i_product', 
                                   '$r->i_product_motif', '$r->i_product_grade', $r->n_sopb, 
                                   0, 0, $r->n_sopb, $r->n_sopb*$r->v_product_mill, 0, 0, $r->n_sopb*$r->v_product_mill,
                                   '$entry', '$user', null, null) ",false);
                $this->db->query(" update tm_saldo_stockcons set n_quantity_akhir=$r->n_sopb, 
                                   v_saldo_akhir=$r->n_sopb*$r->v_product_mill 
                                   where i_periode='$iperiode' and i_customer='$r->i_customer' and i_product='$r->i_product'
                                   and i_product_motif='$r->i_product_motif' and i_product_grade='$r->i_product_grade'",false);
			        }
			      }
	        }
        }
	    }
    }
    
    
    $this->db->select(" i_supplier from tr_supplier order by i_supplier",false);
	  $que = $this->db->get();
	  if ($que->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_ap where i_periode='$xperiode'",false);
		  foreach($que->result() as $ro){
			  $this->db->select(" i_supplier, sum(x.amount) as amount from (
                            SELECT a.i_supplier, (a.v_sisa) as amount FROM tm_dtap a 
                            WHERE to_char(a.d_dtap,'yyyymm')<'$xperiode' and a.f_dtap_cancel='f' and a.v_sisa=a.v_netto
                            and a.i_dtap not in(select i_dtap from tm_pelunasanap_item) and a.i_supplier='$ro->i_supplier'
                            union all
                            SELECT a.i_supplier, c.v_jumlah+a.v_sisa as amount FROM tm_dtap a, tm_pelunasanap b, tm_pelunasanap_item c
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_dtap_cancel='f' and b.f_pelunasanap_cancel='f' 
                            and b.i_pelunasanap=c.i_pelunasanap and b.d_bukti=c.d_bukti and b.i_area=c.i_area and a.i_dtap=c.i_dtap 
                            and a.i_supplier=b.i_supplier and to_char(a.d_dtap,'yyyymm')<'$xperiode' and a.i_supplier='$ro->i_supplier'
                            ) as x group by x.i_supplier",false);
	      $query = $this->db->get();
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $row){
		        $query 	= $this->db->query("SELECT current_timestamp as c");
            $riw   	= $query->row();
        	  $entry	= $riw->c;
            $this->db->query(" insert into tm_saldo_ap (i_periode, i_supplier, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                               d_entry, i_entry, d_update, i_update) values ('$xperiode', '$ro->i_supplier', $row->amount, 0, 0, 
                               $row->amount, '$entry', '$user', null, null) ",false);
                               
            $this->db->query(" update tm_saldo_ap set v_saldo_akhir=$row->amount where i_periode='$iperiode' and 
                               i_supplier='$ro->i_supplier'",false);
			    }
        }			  
		  }
	  }
    $this->db->select(" i_customer from tr_customer order by i_customer",false);
	  $que = $this->db->get();
	  if ($que->num_rows() > 0){
      $this->db->query(" delete from tm_saldo_ar where i_periode='$xperiode'",false);
		  foreach($que->result() as $ro){
			  $this->db->select(" y.i_customer, sum(y.sisa) as amount from (
                            SELECT a.i_customer, a.v_nota_netto as sisa FROM tm_nota a  WHERE to_char(a.d_nota,'yyyymm')<'$xperiode' 
                            and a.f_nota_cancel='f' and a.v_sisa=a.v_nota_netto and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, c.v_jumlah as sisa FROM tm_nota a, tm_pelunasan b, tm_pelunasan_item c
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_nota_cancel='f' and b.f_pelunasan_cancel='f' 
                            and b.f_giro_tolak='f' and b.f_giro_batal='f'
                            and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area and b.i_dt=c.i_dt and a.i_nota=c.i_nota 
                            and to_char(a.d_nota,'yyyymm')<'$xperiode' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, a.v_nota_netto as amount FROM tm_nota a, tm_pelunasan b, tm_pelunasan_item c
                            WHERE to_char(b.d_bukti,'yyyymm')<'$xperiode' and a.v_sisa>0 and a.v_sisa<>a.v_nota_netto 
                            and a.f_nota_cancel='f' and b.f_pelunasan_cancel='f' and b.f_giro_tolak='f' 
                            and b.f_giro_batal='f' and b.i_pelunasan=c.i_pelunasan and b.i_area=c.i_area and b.i_dt=c.i_dt 
                            and a.i_nota=c.i_nota and to_char(a.d_nota,'yyyymm')<'$xperiode' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, -(a.v_netto) as sisa from tm_kn a
                            where a.f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')<'$xperiode' and a.v_netto=a.v_sisa 
                            and upper(substring(a.i_kn,1,1))='K' and a.i_customer='$ro->i_customer'
                            union all
                            SELECT a.i_customer, -((sum(b.v_jumlah)-sum(b.v_lebih))+a.v_sisa) as sisa
                            FROM tm_kn a, tm_pelunasan b
                            WHERE to_char(b.d_bukti,'yyyymm')>='$xperiode' and a.f_kn_cancel='f' and b.f_pelunasan_cancel='f' 
                            and b.f_giro_tolak='f' and b.f_giro_batal='f' and a.i_customer='$ro->i_customer'
                            and a.i_kn=b.i_giro and to_char(a.d_kn,'yyyymm')<'$xperiode' and b.i_jenis_bayar='04'  
                            and upper(substring(a.i_kn,1,1))='K' and a.i_area=b.i_area
                            group by a.i_customer, a.v_sisa
                            ) as y
                            group by y.i_customer",false);
	      $query = $this->db->get();
	      if ($query->num_rows() > 0){
		      foreach($query->result() as $row){
		        if($row->amount==null)$row->amount=0;
		        $query 	= $this->db->query("SELECT current_timestamp as c");
            $riw   	= $query->row();
        	  $entry	= $riw->c;
            $this->db->query(" insert into tm_saldo_ar (i_periode, i_customer, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir,
                               d_entry, i_entry, d_update, i_update) values ('$xperiode', '$ro->i_customer', $row->amount, 0, 0,
                               $row->amount, '$entry', '$user', null, null) ",false);
            $this->db->query(" update tm_saldo_ar set v_saldo_akhir=$row->amount where i_periode='$iperiode' and 
                               i_customer='$ro->i_customer'",false);
			    }
        }			  
		  }
	  }
  $this->db->query(" update tm_periode set i_periode='$xperiode'",false);
  }
}
?>
