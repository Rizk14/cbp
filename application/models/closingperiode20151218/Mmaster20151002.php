<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
  {
    parent::__construct();
	  #$this->CI =& get_instance();
  }
  function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
	    $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
	    $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
					       or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
  {
    if($area1=='00'){
	    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
	    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
					       and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
					       or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function pindah($iperiode,$iarea)
  {
	  $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$iperiode' and substr(i_coa,6,2)='$iarea' 
                        and substr(i_coa,1,5)='111.2' ",false);
	  $query = $this->db->get();
	  $saldo=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $saldo=$row->v_saldo_awal;
		  }
	  }
	  $this->db->select(" sum(v_kk) as v_kk from tm_kk
						  where i_periode='$iperiode' and i_area='$iarea' and f_debet='t' and f_kk_cancel='f'",false);						 
	  $query = $this->db->get();
	  $kredit=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $kredit=$row->v_kk;
		  }
	  }

# 5457641

	  $this->db->select(" sum(v_kk) as v_kk from tm_kk
						  where i_periode='$iperiode' and i_area='$iarea' and f_debet='f' and f_kk_cancel='f'",false);							
	  $query = $this->db->get();
	  $debet=0;
	  if ($query->num_rows() > 0){
		  foreach($query->result() as $row){
			  $debet=$row->v_kk;
		  }
	  }
	  $saldo=$saldo+$debet-$kredit;

    $emutasiperiode=$iperiode;
    $bldpn=substr($emutasiperiode,4,2)+1;
    if($bldpn==13)
    {
      $perdpn=substr($emutasiperiode,0,4)+1;
      $perdpn=$perdpn.'01';
    }else{
      $perdpn=substr($emutasiperiode,0,4);
      $perdpn=$perdpn.substr($emutasiperiode,4,2)+1;;
    }
    $xperiode=$perdpn;
    $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$perdpn' and substr(i_coa,6,2)='$iarea' 
                      and substr(i_coa,1,5)='111.2' ",false);
    $query = $this->db->get();
    if ($query->num_rows() > 0){
      $query 	= $this->db->query("SELECT current_timestamp as c");
      $row   	= $query->row();
  	  $update	= $row->c;
      $coa='111.2'.$iarea;
      $user=$this->session->userdata('user_id');
      $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo, d_update='$update', i_update='$user'
                         where i_periode='$xperiode' and i_coa='$coa'");
    }else{
      $query 	= $this->db->query("SELECT current_timestamp as c");
      $row   	= $query->row();
  	  $entry	= $row->c;
      $coa='111.2'.$iarea;
      $query=$this->db->query(" select e_coa_name from tr_coa where i_coa='$coa'");
      if ($query->num_rows() > 0){
        foreach($query->result() as $tes){
          $nama=$tes->e_coa_name;
        }
      }
      $user=$this->session->userdata('user_id');
      $this->db->query(" insert into tm_coa_saldo 
                         (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
                          i_entry, e_coa_name)
                         values
                         ('$xperiode','$coa',$saldo,0,0,$saldo,'$entry','$user','$nama')");
    }
  }
}
?>
