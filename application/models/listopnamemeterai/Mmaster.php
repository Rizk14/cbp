<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dentry	= $row->c;
			$this->db->query("update tm_nota set f_nota_cancel='t', d_nota_update='$dentry', d_sj_update='$dentry' where i_nota='$inota' 
			                  and i_area='$iarea'");
			$this->db->query("update tm_spb set f_spb_cancel='t', d_spb_update='$dentry' where i_spb='$ispb' and i_area='$iarea'");
#####UnPosting
      $this->db->query("insert into th_jurnal_transharian select * from tm_jurnal_transharian 
                        where i_refference='$inota' and i_area='$iarea'");
      $this->db->query("insert into th_jurnal_transharianitem select * from tm_jurnal_transharianitem 
                        where i_refference='$inota' and i_area='$iarea'");
      $this->db->query("insert into th_general_ledger select * from tm_general_ledger
                        where i_refference='$inota' and i_area='$iarea'");

      $quer 	= $this->db->query("SELECT i_coa, v_mutasi_debet, v_mutasi_kredit, to_char(d_refference,'yyyymm') as periode 
                                  from tm_general_ledger
                                  where i_refference='$inota' and i_area='$iarea'");
  	  if($quer->num_rows()>0){
        foreach($quer->result() as $xx){
          $this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$xx->v_mutasi_debet, 
                            v_mutasi_kredit=v_mutasi_kredit-$xx->v_mutasi_kredit,
                            v_saldo_akhir=v_saldo_akhir-$xx->v_mutasi_debet+$xx->v_mutasi_kredit
                            where i_coa='$xx->i_coa' and i_periode='$xx->periode'");
        }
      }

      $this->db->query("delete from tm_jurnal_transharian where i_refference='$inota' and i_area='$iarea'");
      $this->db->query("delete from tm_jurnal_transharianitem where i_refference='$inota' and i_area='$iarea'");
      $this->db->query("delete from tm_general_ledger where i_refference='$inota' and i_area='$iarea'");
#####
#####
      $isj='';
      $this->db->select(" * from tm_nota where i_nota='$inota' and i_area='$iarea'");
			$qry = $this->db->get();
			if ($qry->num_rows() > 0){
        foreach($qry->result() as $qyr){  
          $dnota=$qyr->d_nota;
          $isj=$qyr->i_sj;
        }
      }
      $th=substr($dnota,0,4);
		  $bl=substr($dnota,5,2);
		  $emutasiperiode=$th.$bl;
      $query=$this->db->query(" select f_spb_consigment from tm_spb where i_spb='$ispb' and i_area='$iarea'",false);
      $consigment='f';
			if ($query->num_rows() > 0){
				foreach($query->result() as $qq){
					$consigment=$qq->f_spb_consigment;
				}
			}
      $iareasj= substr($isj,8,2);
      if($iareasj=='BK')$iareasj=$iarea;
      $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
      $st=$que->row();
      $istore=$st->i_store;
      if($istore=='AA'){
				$istorelocation		= '01';
			}else{
        if($consigment=='t')
          $istorelocation		= 'PB';
        else
  				$istorelocation		= '00';
			}
			$istorelocationbin	= '00';
      $this->db->select(" * from tm_nota_item where i_nota='$inota' and i_area='$iarea' order by n_item_no");
			$qery = $this->db->get();
			if ($qery->num_rows() > 0){
        foreach($qery->result() as $qyre){ 
          $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                        where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                                        and i_product_motif='$qyre->i_product_motif'
                                        and i_store='$istore' and i_store_location='$istorelocation'
                                        and i_store_locationbin='$istorelocationbin'
                                        order by i_trans desc",false);
          if ($queri->num_rows() > 0){
        	  $row   		= $queri->row();
            $que 	= $this->db->query("SELECT current_timestamp as c");
	          $ro 	= $que->row();
	          $now	 = $ro->c;
            $this->db->query(" 
                              INSERT INTO tm_ic_trans
                              (
                                i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                n_quantity_in, n_quantity_out,
                                n_quantity_akhir, n_quantity_awal)
                              VALUES 
                              (
                                '$qyre->i_product','$qyre->i_product_grade','$qyre->i_product_motif',
                                '$istore','$istorelocation','$istorelocationbin', 
                                '$qyre->e_product_name', '$isj', '$now', $qyre->n_deliver, 0, $row->n_quantity_akhir+$qyre->n_deliver, 
                                $row->n_quantity_akhir
                              )
                             ",false);

            $this->db->query(" 
                              UPDATE tm_mutasi set n_mutasi_penjualan=n_mutasi_penjualan-$qyre->n_deliver, 
                              n_saldo_akhir=n_saldo_akhir+$qyre->n_deliver
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif'
                              and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              and e_mutasi_periode='$emutasiperiode'
                             ",false);
            $this->db->query(" 
                              UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qyre->n_deliver
                              where i_product='$qyre->i_product' and i_product_grade='$qyre->i_product_grade' 
                              and i_product_motif='$qyre->i_product_motif' and i_store='$istore' and i_store_location='$istorelocation' 
                              and i_store_locationbin='$istorelocationbin'
                             ",false);

          }
        }
      }
#####
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaperiode($where,$dfrom,$dto,$num, $offset,$cari)
    {
		$query = $this->db->select(" 	a.i_nota, to_char(a.d_nota, 'dd-mm-yyyy') as d_nota, to_char(a.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_contact || 
										case when c.e_customer_contact is not null and c.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
										case when c.e_customer_phone is not null and c.e_customer_phone <> '' then '(' || c.e_customer_phone || ')' end as contact, d.e_salesman_name, 
										a.v_materai, a.v_materai_sisa, n_nota_toplength as top, a.v_nota_netto, a.v_sisa
										from tm_nota a 
										inner join tr_area b on (a.i_area = b.i_area)
										inner join tr_customer c on (a.i_area = c.i_area and a.i_customer = c.i_customer)
										inner join tr_salesman d on (a.i_salesman = d.i_salesman)
										where a.v_materai > 0 and d_nota between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
										and (i_nota ilike '%$cari%' or to_char(d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f'
										order by b.e_area_name, d_nota ",false)->limit($num,$offset);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiode_nolimit($where,$dfrom,$dto,$cari)
    {
		$query = $this->db->select(" 	a.i_nota, to_char(a.d_nota, 'dd-mm-yyyy') as d_nota, to_char(a.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_contact || 
										case when c.e_customer_contact is not null and c.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
										case when c.e_customer_phone is not null and c.e_customer_phone <> '' then '(' || c.e_customer_phone || ')' end as contact, d.e_salesman_name, 
										a.v_materai, a.v_materai_sisa, n_nota_toplength as top, a.v_nota_netto, a.v_sisa
										from tm_nota a 
										inner join tr_area b on (a.i_area = b.i_area)
										inner join tr_customer c on (a.i_area = c.i_area and a.i_customer = c.i_customer)
										inner join tr_salesman d on (a.i_salesman = d.i_salesman)
										where a.v_materai > 0 and d_nota between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
										and (i_nota ilike '%$cari%' or to_char(d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f'
										order by b.e_area_name, d_nota ",false);

		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function get_area($iarea) {
     	return $this->db->query("select e_area_name from tr_area where i_area='$iarea'")->row()->e_area_name;
    }

    function bacatotal($iarea,$dfrom,$dto,$num,$offset)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and not a.i_nota isnull
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ");
#   						and a.f_nota_koreksi='f'
#             and a.f_ttb_tolak='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
 							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
#             and a.f_nota_koreksi='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
#              and a.f_nota_koreksi='f'
#             and a.f_ttb_tolak='f'
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
