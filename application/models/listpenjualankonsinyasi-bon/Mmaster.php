<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($inotapb,$icustomer)
    {
		$this->db->select(" a.*, b.e_area_name, c.e_customer_name, d.e_spg_name from tm_notapb a, tr_area b, tr_customer c, tr_spg d
					where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_spg=d.i_spg
					and a.i_notapb ='$inotapb' and a.i_customer='$icustomer'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($inotapb,$icustomer)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_notapb_item a, tr_product_motif b
						 where a.i_notapb = '$inotapb' and a.i_customer='$icustomer' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    public function delete($inotapb,$icustomer) 
    {
		$this->db->query("update tm_notapb set f_notapb_cancel='t' WHERE i_notapb='$inotapb' and i_customer='$icustomer'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_store_name as e_area_name from tm_spmb a, tr_store b
							          where a.i_area=b.i_store and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%' or upper(a.i_spmb_old) like '%$cari%')
					order by a.i_spmb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name  as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          group by a.i_store, b.e_store_name
                          order by a.i_store", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                          or a.i_area = '$area4' or a.i_area = '$area5')
                          group by a.i_store, b.e_store_name order by a.i_store", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b 
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($ispg,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name,  
		                    a.f_notapb_cancel, a.f_spb_rekap, a.i_spb, a.i_cek
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
                        and b.i_spg='$ispg' and
                        a.d_notapb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_notapb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_notapb ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
						where a.i_area=b.i_area and a.f_spmb_cancel='f'
						and (upper(a.i_spmb) like '%$cari%')
						and a.i_area='$iarea' and
						a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
						a.d_spmb <= to_date('$dto','dd-mm-yyyy')
						ORDER BY e_area_name asc ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacastore($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													   c.i_area = '$area3' or c.i_area = '$area4' or
													   c.i_area = '$area5')", false)->limit($num,$offset);			
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function caristore($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(b.e_store_name) like '%$cari%'
                            or upper(a.i_store_location) like '%$cari%'
                            or upper(a.e_store_locationname) like '%$cari%')", false)->limit($num,$offset);			
      }else{
		    $this->db->select(" distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
													  from tr_store_location a, tr_store b, tr_area c
													  where a.i_store = b.i_store and b.i_store=c.i_store
                            and(  upper(a.i_store) like '%$cari%' 
                            or upper(b.e_store_name) like '%$cari%'
                            or upper(a.i_store_location) like '%$cari%'
                            or upper(a.e_store_locationname) like '%$cari%')
													  and (c.i_area = '$area1' or c.i_area = '$area2' or
													  c.i_area = '$area3' or c.i_area = '$area4' or
													  c.i_area = '$area5')", false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaproduct($num,$offset,$cari,$cust)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
						                    a.e_product_motifname as namamotif, b.v_product_retail as harga,
						                    c.e_product_name as nama
						                    from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d
						                    where a.i_product=c.i_product and a.i_product=b.i_product 
						                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
						                    and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
                                order by c.i_product, a.e_product_motifname, b.i_price_group
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    public function deletedetail($iproduct, $iproductgrade, $inotapb, $iarea, $icustomer, $iproductmotif, $vunitprice) 
    {
		  $this->db->query("DELETE FROM tm_notapb_item WHERE i_notapb='$inotapb' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_customer='$icustomer' and v_unit_price=$vunitprice");
    }
	
    public function deleteheader($xinotapb, $iarea, $icustomer) 
    {
		  $this->db->query("DELETE FROM tm_notapb WHERE i_notapb='$xinotapb' and i_area='$iarea' and i_customer='$icustomer'");
    }
    function insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
			'i_notapb'    => $inotapb,
      'i_area'      => $iarea,
      'i_spg'       => $ispg,
      'i_customer'  => $icustomer,
      'd_notapb'    => $dnotapb,
      'n_notapb_discount' => $nnotapbdiscount,
      'v_notapb_discount' => $vnotapbdiscount,
      'v_notapb_gross'    => $vnotapbgross,
      'f_notapb_cancel'   => 'f',
      'd_notapb_entry'    => $dentry
    		)
    	);
    	
    	$this->db->insert('tm_notapb');
    }
    function insertdetail($inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark)
    {
      $query 		= $this->db->query("SELECT current_timestamp as c");
		  $row   		= $query->row();
		  $dentry	= $row->c;
    	$this->db->set(
    		array(
					'i_notapb'        => $inotapb,
          'i_area'          => $iarea,
          'i_customer'      => $icustomer,
          'd_notapb'        => $dnotapb,
          'i_product'       => $iproduct,
          'i_product_motif' => $iproductmotif,
          'i_product_grade' => $iproductgrade,
          'n_quantity'      => $nquantity,
          'v_unit_price'    => $vunitprice,
          'd_notapb_entry'  => $dentry,
          'n_item_no'       => $i,
          'e_product_name'  => $eproductname,
          'i_price_groupco' => $ipricegroupco,
          'e_remark'        => $eremark
    		)
    	);
    	
    	$this->db->insert('tm_notapb_item');
    }
}
?>
