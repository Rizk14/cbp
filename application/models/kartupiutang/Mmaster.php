<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    #$this->CI =& get_instance();
  }
  public function delete($inota, $ispb, $iarea)
  {
    $this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
  }
  function bacasemua($cari, $num, $offset)
  {
    $area1 = $this->session->userdata('i_area');
    $area2 = $this->session->userdata('i_area2');
    $area3 = $this->session->userdata('i_area3');
    $area4 = $this->session->userdata('i_area4');
    $area5 = $this->session->userdata('i_area5');
    $allarea = $this->session->userdata('allarea');
    if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
      $this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc", false)->limit($num, $offset);
    } else {
      $this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc", false)->limit($num, $offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function cari($cari, $num, $offset)
  {
    $area1 = $this->session->userdata('i_area');
    $area2 = $this->session->userdata('i_area2');
    $area3 = $this->session->userdata('i_area3');
    $area4 = $this->session->userdata('i_area4');
    $area5 = $this->session->userdata('i_area5');
    $allarea = $this->session->userdata('allarea');
    if (($allarea == 't') || ($area1 == '00') || ($area2 == '00') || ($area3 == '00') || ($area4 == '00') || ($area5 == '00')) {
      $this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc", FALSE)->limit($num, $offset);
    } else {
      $this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc", false)->limit($num, $offset);
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacaarea($num, $offset, $iuser)
  {
    $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }

  function cariarea($cari, $num, $offset, $iuser)
  {
    $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacaperiode($iarea, $iperiode, $cari, $cust, $area)
  {
    $periode =  $iperiode;
    $tanggal_awal = '01-' . substr($periode, 4, 2) . '-' . substr($periode, 0, 4);
    $periodesebelum =  date('Ym', strtotime('-1 month', strtotime($tanggal_awal)));
    if ($cust == 'qqq') {
      if ($iarea == 'NA') {
        $this->db->select("   x.i_customer,
                              x.e_customer_name,
                              sum(x.saldo_awal) AS saldo_awal,
                              sum(x.penjualan) AS penjualan,
                              sum(x.alokasi_bm) AS alokasi_bm,
                              sum(x.alokasi_knr) AS alokasi_knr,
                              sum(x.alokasi_kn) AS alokasi_kn,
                              sum(x.alokasi_hll) AS alokasi_hll,
                              sum(x.pembulatan) AS pembulatan,
                              ((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) AS saldo_akhir
                            FROM
                                (
                                    SELECT
                                        z.i_customer,
                                        z.e_customer_name,
                                        z.i_area,
                                        sum(z.v_sisa) AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        (
                                        SELECT
                                            x.i_nota,
                                            x.i_area,
                                            x.e_area_shortname,
                                            x.e_area_name,
                                            x.i_customer,
                                            x.e_customer_name,
                                            x.e_customer_address,
                                            x.e_customer_phone,
                                            x.e_salesman_name,
                                            x.n_customer_toplength,
                                            x.i_sj,
                                            x.d_nota,
                                            x.d_jatuh_tempo,
                                            sum(x.v_sisa) AS v_sisa,
                                            x.v_nota_netto,
                                            x.d_jatuh_tempo_plustoleransi,
                                            x.n_toleransi,
                                            x.e_product_groupname,
                                            x.e_remark
                                        FROM
                                            (
                                    /* BACA NOTA SISA */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                b.e_area_shortname,
                                                b.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                d.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                a.d_nota,
                                                a.d_jatuh_tempo,
                                                a.v_sisa,
                                                a.v_nota_netto,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                CASE
                                                    WHEN h.f_spb_consigment = 'f' THEN i.e_product_groupname
                                                    ELSE 'Modern Outlet'
                                                END AS e_product_groupname,
                                                '' AS e_remark
                                            FROM
                                                tm_nota a
                                            LEFT JOIN tr_area b ON
                                                (a.i_area = b.i_area)
                                            LEFT JOIN tr_customer c ON
                                                (a.i_customer = c.i_customer
                                                    AND a.i_area = c.i_area
                                                    AND b.i_area = c.i_area)
                                            LEFT JOIN tr_salesman d ON
                                                (a.i_salesman = d.i_salesman)
                                            LEFT JOIN tr_city g ON
                                                (c.i_city = g.i_city
                                                    AND c.i_area = g.i_area)
                                            LEFT JOIN tm_spb h ON
                                                a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                            LEFT JOIN tr_product_group i ON
                                                h.i_product_group = i.i_product_group
                                            WHERE
                                                to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.f_nota_cancel = 'f'
                                                AND a.v_sisa>0
                                                AND NOT a.i_nota ISNULL
                                        UNION ALL
                                    /* BACA ALOKASI BANK MASUK */
                                            SELECT
                                                j.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                j.d_nota,
                                                a.d_jatuh_tempo,
                                                j.v_jumlah,
                                                j.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                j.e_remark
                                            FROM
                                                tm_alokasi_item j,
                                                tm_alokasi k,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND j.i_alokasi = k.i_alokasi
                                                AND j.i_kbank = k.i_kbank
                                                AND j.i_area = k.i_area
                                                AND k.f_alokasi_cancel = 'f'
                                                AND a.i_nota = j.i_nota
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN NON RETUR */
                                            SELECT
                                                l.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                l.d_nota,
                                                a.d_jatuh_tempo,
                                                l.v_jumlah,
                                                (l.v_sisa + l.v_jumlah) AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                l.e_remark
                                            FROM
                                                tm_alokasikn_item l,
                                                tm_alokasikn m,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_nota = l.i_nota
                                                AND l.i_alokasi = m.i_alokasi
                                                AND l.i_kn = m.i_kn
                                                AND l.i_area = m.i_area
                                                AND m.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN RETUR */
                                            SELECT
                                                n.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                n.d_nota,
                                                a.d_jatuh_tempo,
                                                n.v_jumlah,
                                                n.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                n.e_remark
                                            FROM
                                                tm_alokasiknr_item n,
                                                tm_alokasiknr o,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = n.i_nota
                                                AND n.i_alokasi = o.i_alokasi
                                                AND n.i_kn = o.i_kn
                                                AND n.i_area = o.i_area
                                                AND o.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI HUT LAIN */
                                            SELECT
                                                p.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_jumlah,
                                                p.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_remark
                                            FROM
                                                tm_alokasihl_item p,
                                                tm_alokasihl q,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = p.i_nota
                                                AND p.i_alokasi = q.i_alokasi
                                                AND p.i_area = q.i_area
                                                AND q.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA JURNAL */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_refference AS d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_mutasi_debet AS v_jumlah,
                                                p.v_mutasi_debet AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_description AS e_remark
                                            FROM
                                                tm_general_ledger p,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = substring(p.i_refference, 15, 15)
                                                AND p.f_debet = 't'
                                                AND p.i_coa = '610-2902'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /************************KONSINYASI************************/
                                            /* BACA ALOKASI BANK MASUK KONSINYASI */
                                            SELECT
                                                j.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                j.d_nota,
                                                a.d_jatuh_tempo,
                                                j.v_jumlah,
                                                j.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                j.e_remark
                                            FROM
                                                tm_alokasi_item j,
                                                tm_alokasi k,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND j.i_alokasi = k.i_alokasi
                                                AND j.i_kbank = k.i_kbank
                                                AND j.i_area = k.i_area
                                                AND k.f_alokasi_cancel = 'f'
                                                AND a.i_nota = j.i_nota
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN NON RETUR KONSINYASI */
                                            SELECT
                                                l.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                l.d_nota,
                                                a.d_jatuh_tempo,
                                                l.v_jumlah,
                                                (l.v_sisa + l.v_jumlah) AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                l.e_remark
                                            FROM
                                                tm_alokasikn_item l,
                                                tm_alokasikn m,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = l.i_nota
                                                AND l.i_alokasi = m.i_alokasi
                                                AND l.i_kn = m.i_kn
                                                AND l.i_area = m.i_area
                                                AND m.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN RETUR KONSINYASI */
                                            SELECT
                                                n.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                n.d_nota,
                                                a.d_jatuh_tempo,
                                                n.v_jumlah,
                                                n.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                n.e_remark
                                            FROM
                                                tm_alokasiknr_item n,
                                                tm_alokasiknr o,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = n.i_nota
                                                AND n.i_alokasi = o.i_alokasi
                                                AND n.i_kn = o.i_kn
                                                AND n.i_area = o.i_area
                                                AND o.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOAKSI HUTANG LAIN KONSINYASI */
                                            SELECT
                                                p.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_jumlah,
                                                p.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                p.e_remark
                                            FROM
                                                tm_alokasihl_item p,
                                                tm_alokasihl q,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = p.i_nota
                                                AND p.i_alokasi = q.i_alokasi
                                                AND p.i_area = q.i_area
                                                AND q.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA JURNAL KONSINYASI */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_refference AS d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_mutasi_debet AS v_jumlah,
                                                p.v_mutasi_debet AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_description AS e_remark
                                            FROM
                                                tm_general_ledger p,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = substring(p.i_refference, 15, 15)
                                                AND p.f_debet = 't'
                                                AND p.i_coa = '610-2902'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                    ) AS x
                                        GROUP BY
                                            x.i_nota,
                                            x.i_area,
                                            x.e_area_shortname,
                                            x.e_area_name,
                                            x.i_customer,
                                            x.e_customer_name,
                                            x.e_salesman_name,
                                            x.n_customer_toplength,
                                            x.i_sj,
                                            x.d_nota,
                                            x.d_jatuh_tempo,
                                            x.d_jatuh_tempo_plustoleransi,
                                            x.n_toleransi,
                                            x.e_product_groupname,
                                            x.e_remark,
                                            x.v_nota_netto,
                                            x.e_customer_address,
                                            x.e_customer_phone
                                        ORDER BY
                                            x.i_area ASC,
                                            x.i_nota ASC,
                                            x.i_customer ASC
                                    ) as z
                                    group by z.i_customer, z.e_customer_name, z.i_area
                                    UNION ALL
                                    /* Penjualan */
                                    SELECT
                                        a.i_customer,
                                        b.e_customer_name,
                                        a.i_area,
                                        0 AS saldo_awal,
                                        sum(a.v_nota_netto) AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_nota a,
                                        tr_customer b
                                    WHERE
                                        a.i_customer = b.i_customer
                                        AND to_char(a.d_nota, 'yyyymm')= '$iperiode'
                                        AND a.f_nota_cancel = 'f'
                                    GROUP BY
                                        a.i_customer,
                                        b.e_customer_name,
                                        a.i_area
                                    UNION ALL
                                    /* Alokasi Bank Masuk */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        a.v_jumlah AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasi_item a,
                                        tm_alokasi b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_kbank = b.i_kbank
                                        AND a.i_area = b.i_area
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI KN NON RETUR */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        a.v_jumlah AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasiknr_item a,
                                        tm_alokasiknr b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND a.i_kn = b.i_kn
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI KN RETUR */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        a.v_jumlah AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasikn_item a,
                                        tm_alokasikn b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND a.i_kn = b.i_kn
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI HUTANG LAIN */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        a.v_jumlah AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasihl_item a,
                                        tm_alokasihl b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* JURNAL */
                                    SELECT
                                        a.i_customer,
                                        c.e_customer_name,
                                        a.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        p.v_mutasi_debet AS pembulatan
                                    FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                    WHERE
                                        to_char(p.d_mutasi, 'yyyymm')= '$iperiode'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                ) as x
                                where (x.i_customer like '%$cari%' OR x.e_customer_name like '%$cari%')
                                group by x.i_customer, x.e_customer_name ", false);
      } else {
        $this->db->select("   x.i_customer,
                              x.e_customer_name,
                              sum(x.saldo_awal) AS saldo_awal,
                              sum(x.penjualan) AS penjualan,
                              sum(x.alokasi_bm) AS alokasi_bm,
                              sum(x.alokasi_knr) AS alokasi_knr,
                              sum(x.alokasi_kn) AS alokasi_kn,
                              sum(x.alokasi_hll) AS alokasi_hll,
                              sum(x.pembulatan) AS pembulatan,
                              ((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) AS saldo_akhir
                            FROM
                                (
                                    SELECT
                                        z.i_customer,
                                        z.e_customer_name,
                                        z.i_area,
                                        sum(z.v_sisa) AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        (
                                        SELECT
                                            x.i_nota,
                                            x.i_area,
                                            x.e_area_shortname,
                                            x.e_area_name,
                                            x.i_customer,
                                            x.e_customer_name,
                                            x.e_customer_address,
                                            x.e_customer_phone,
                                            x.e_salesman_name,
                                            x.n_customer_toplength,
                                            x.i_sj,
                                            x.d_nota,
                                            x.d_jatuh_tempo,
                                            sum(x.v_sisa) AS v_sisa,
                                            x.v_nota_netto,
                                            x.d_jatuh_tempo_plustoleransi,
                                            x.n_toleransi,
                                            x.e_product_groupname,
                                            x.e_remark
                                        FROM
                                            (
                                    /* BACA NOTA SISA */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                b.e_area_shortname,
                                                b.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                d.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                a.d_nota,
                                                a.d_jatuh_tempo,
                                                a.v_sisa,
                                                a.v_nota_netto,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                CASE
                                                    WHEN h.f_spb_consigment = 'f' THEN i.e_product_groupname
                                                    ELSE 'Modern Outlet'
                                                END AS e_product_groupname,
                                                '' AS e_remark
                                            FROM
                                                tm_nota a
                                            LEFT JOIN tr_area b ON
                                                (a.i_area = b.i_area)
                                            LEFT JOIN tr_customer c ON
                                                (a.i_customer = c.i_customer
                                                    AND a.i_area = c.i_area
                                                    AND b.i_area = c.i_area)
                                            LEFT JOIN tr_salesman d ON
                                                (a.i_salesman = d.i_salesman)
                                            LEFT JOIN tr_city g ON
                                                (c.i_city = g.i_city
                                                    AND c.i_area = g.i_area)
                                            LEFT JOIN tm_spb h ON
                                                a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                            LEFT JOIN tr_product_group i ON
                                                h.i_product_group = i.i_product_group
                                            WHERE
                                                to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.f_nota_cancel = 'f'
                                                AND a.v_sisa>0
                                                AND NOT a.i_nota ISNULL
                                        UNION ALL
                                    /* BACA ALOKASI BANK MASUK */
                                            SELECT
                                                j.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                j.d_nota,
                                                a.d_jatuh_tempo,
                                                j.v_jumlah,
                                                j.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                j.e_remark
                                            FROM
                                                tm_alokasi_item j,
                                                tm_alokasi k,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND j.i_alokasi = k.i_alokasi
                                                AND j.i_kbank = k.i_kbank
                                                AND j.i_area = k.i_area
                                                AND k.f_alokasi_cancel = 'f'
                                                AND a.i_nota = j.i_nota
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN NON RETUR */
                                            SELECT
                                                l.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                l.d_nota,
                                                a.d_jatuh_tempo,
                                                l.v_jumlah,
                                                (l.v_sisa + l.v_jumlah) AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                l.e_remark
                                            FROM
                                                tm_alokasikn_item l,
                                                tm_alokasikn m,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_nota = l.i_nota
                                                AND l.i_alokasi = m.i_alokasi
                                                AND l.i_kn = m.i_kn
                                                AND l.i_area = m.i_area
                                                AND m.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN RETUR */
                                            SELECT
                                                n.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                n.d_nota,
                                                a.d_jatuh_tempo,
                                                n.v_jumlah,
                                                n.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                n.e_remark
                                            FROM
                                                tm_alokasiknr_item n,
                                                tm_alokasiknr o,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = n.i_nota
                                                AND n.i_alokasi = o.i_alokasi
                                                AND n.i_kn = o.i_kn
                                                AND n.i_area = o.i_area
                                                AND o.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI HUT LAIN */
                                            SELECT
                                                p.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_jumlah,
                                                p.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_remark
                                            FROM
                                                tm_alokasihl_item p,
                                                tm_alokasihl q,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = p.i_nota
                                                AND p.i_alokasi = q.i_alokasi
                                                AND p.i_area = q.i_area
                                                AND q.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA JURNAL */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_refference AS d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_mutasi_debet AS v_jumlah,
                                                p.v_mutasi_debet AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_description AS e_remark
                                            FROM
                                                tm_general_ledger p,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = substring(p.i_refference, 15, 15)
                                                AND p.f_debet = 't'
                                                AND p.i_coa = '610-2902'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 'f'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /************************KONSINYASI************************/
                                            /* BACA ALOKASI BANK MASUK KONSINYASI */
                                            SELECT
                                                j.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                j.d_nota,
                                                a.d_jatuh_tempo,
                                                j.v_jumlah,
                                                j.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                j.e_remark
                                            FROM
                                                tm_alokasi_item j,
                                                tm_alokasi k,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND NOT a.i_nota ISNULL
                                                AND j.i_alokasi = k.i_alokasi
                                                AND j.i_kbank = k.i_kbank
                                                AND j.i_area = k.i_area
                                                AND k.f_alokasi_cancel = 'f'
                                                AND a.i_nota = j.i_nota
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN NON RETUR KONSINYASI */
                                            SELECT
                                                l.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                l.d_nota,
                                                a.d_jatuh_tempo,
                                                l.v_jumlah,
                                                (l.v_sisa + l.v_jumlah) AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                l.e_remark
                                            FROM
                                                tm_alokasikn_item l,
                                                tm_alokasikn m,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = l.i_nota
                                                AND l.i_alokasi = m.i_alokasi
                                                AND l.i_kn = m.i_kn
                                                AND l.i_area = m.i_area
                                                AND m.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOKASI KN RETUR KONSINYASI */
                                            SELECT
                                                n.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                n.d_nota,
                                                a.d_jatuh_tempo,
                                                n.v_jumlah,
                                                n.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                n.e_remark
                                            FROM
                                                tm_alokasiknr_item n,
                                                tm_alokasiknr o,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = n.i_nota
                                                AND n.i_alokasi = o.i_alokasi
                                                AND n.i_kn = o.i_kn
                                                AND n.i_area = o.i_area
                                                AND o.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA ALOAKSI HUTANG LAIN KONSINYASI */
                                            SELECT
                                                p.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_jumlah,
                                                p.v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                'Modern Outlet' AS e_product_groupname,
                                                p.e_remark
                                            FROM
                                                tm_alokasihl_item p,
                                                tm_alokasihl q,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = p.i_nota
                                                AND p.i_alokasi = q.i_alokasi
                                                AND p.i_area = q.i_area
                                                AND q.f_alokasi_cancel = 'f'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                        UNION ALL
                                    /* BACA JURNAL KONSINYASI */
                                            SELECT
                                                a.i_nota,
                                                a.i_area,
                                                area.e_area_shortname,
                                                area.e_area_name,
                                                a.i_customer,
                                                c.e_customer_name,
                                                c.e_customer_address,
                                                c.e_customer_phone,
                                                b.e_salesman_name,
                                                c.n_customer_toplength,
                                                a.i_sj,
                                                p.d_refference AS d_nota,
                                                a.d_jatuh_tempo,
                                                p.v_mutasi_debet AS v_jumlah,
                                                p.v_mutasi_debet AS v_sisa,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                                    ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                                END AS d_jatuh_tempo_plustoleransi,
                                                CASE
                                                    WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                                    ELSE g.n_toleransi_cabang
                                                END AS n_toleransi,
                                                i.e_product_groupname,
                                                p.e_description AS e_remark
                                            FROM
                                                tm_general_ledger p,
                                                tr_customer c,
                                                tr_city g,
                                                tr_salesman b,
                                                tm_spb h,
                                                tr_product_group i,
                                                tr_area area,
                                                tm_nota a
                                            WHERE
                                                to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                                AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                                AND NOT a.i_nota ISNULL
                                                AND a.i_salesman = b.i_salesman
                                                AND a.f_nota_cancel = 'f'
                                                AND a.i_nota = substring(p.i_refference, 15, 15)
                                                AND p.f_debet = 't'
                                                AND p.i_coa = '610-2902'
                                                AND c.i_city = g.i_city
                                                AND c.i_area = g.i_area
                                                AND a.i_salesman = b.i_salesman
                                                AND h.i_product_group = i.i_product_group
                                                AND a.i_spb = h.i_spb
                                                AND a.i_area = h.i_area
                                                AND h.f_spb_consigment = 't'
                                                AND area.i_area = a.i_area
                                                AND a.i_customer = c.i_customer
                                    ) AS x
                                        GROUP BY
                                            x.i_nota,
                                            x.i_area,
                                            x.e_area_shortname,
                                            x.e_area_name,
                                            x.i_customer,
                                            x.e_customer_name,
                                            x.e_salesman_name,
                                            x.n_customer_toplength,
                                            x.i_sj,
                                            x.d_nota,
                                            x.d_jatuh_tempo,
                                            x.d_jatuh_tempo_plustoleransi,
                                            x.n_toleransi,
                                            x.e_product_groupname,
                                            x.e_remark,
                                            x.v_nota_netto,
                                            x.e_customer_address,
                                            x.e_customer_phone
                                        ORDER BY
                                            x.i_area ASC,
                                            x.i_nota ASC,
                                            x.i_customer ASC
                                    ) as z
                                    group by z.i_customer, z.e_customer_name, z.i_area
                                    UNION ALL
                                    /* Penjualan */
                                    SELECT
                                        a.i_customer,
                                        b.e_customer_name,
                                        a.i_area,
                                        0 AS saldo_awal,
                                        sum(a.v_nota_netto) AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_nota a,
                                        tr_customer b
                                    WHERE
                                        a.i_customer = b.i_customer
                                        AND to_char(a.d_nota, 'yyyymm')= '$iperiode'
                                        AND a.f_nota_cancel = 'f'
                                    GROUP BY
                                        a.i_customer,
                                        b.e_customer_name,
                                        a.i_area
                                    UNION ALL
                                    /* Alokasi Bank Masuk */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        a.v_jumlah AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasi_item a,
                                        tm_alokasi b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_kbank = b.i_kbank
                                        AND a.i_area = b.i_area
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI KN NON RETUR */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        a.v_jumlah AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasiknr_item a,
                                        tm_alokasiknr b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND a.i_kn = b.i_kn
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI KN RETUR */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        a.v_jumlah AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasikn_item a,
                                        tm_alokasikn b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND a.i_kn = b.i_kn
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* ALOKASI HUTANG LAIN */
                                    SELECT
                                        d.i_customer,
                                        c.e_customer_name,
                                        d.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        a.v_jumlah AS alokasi_hll,
                                        0 AS pembulatan
                                    FROM
                                        tm_alokasihl_item a,
                                        tm_alokasihl b,
                                        tr_customer c,
                                        tm_nota d
                                    WHERE
                                        a.i_alokasi = b.i_alokasi
                                        AND a.i_area = b.i_area
                                        AND d.i_customer = c.i_customer
                                        AND b.f_alokasi_cancel = 'f'
                                        AND a.i_nota = d.i_nota
                                        AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                    UNION ALL
                                    /* JURNAL */
                                    SELECT
                                        a.i_customer,
                                        c.e_customer_name,
                                        a.i_area,
                                        0 AS saldo_awal,
                                        0 AS penjualan,
                                        0 AS alokasi_bm,
                                        0 AS alokasi_knr,
                                        0 AS alokasi_kn,
                                        0 AS alokasi_hll,
                                        p.v_mutasi_debet AS pembulatan
                                    FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                    WHERE
                                        to_char(p.d_mutasi, 'yyyymm')= '$iperiode'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                ) as x
                                WHERE
                                  x.i_area = '$iarea'
                                  AND (x.i_customer like '%$cari%' OR x.e_customer_name like '%$cari%')
                                group by 
                                  x.i_customer, 
                                  x.e_customer_name ", false);
      }
    } else {
      if ($iarea == 'NA') {
        $this->db->select("   a.*,
                              b.e_area_name
                            FROM
                              (
                              SELECT
                                x.i_area,
                                sum(x.saldo_awal) AS saldo_awal,
                                sum(x.penjualan) AS penjualan,
                                sum(x.alokasi_bm) AS alokasi_bm,
                                sum(x.alokasi_knr) AS alokasi_knr,
                                sum(x.alokasi_kn) AS alokasi_kn,
                                sum(x.alokasi_hll) AS alokasi_hll,
                                sum(x.pembulatan) AS pembulatan,
                                ((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) AS saldo_akhir
                              FROM
                                (
                                SELECT
                                  z.i_customer,
                                  z.e_customer_name,
                                  z.i_area,
                                  sum(z.v_sisa) AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  (
                                  SELECT
                                    x.i_nota,
                                    x.i_area,
                                    x.e_area_shortname,
                                    x.e_area_name,
                                    x.i_customer,
                                    x.e_customer_name,
                                    x.e_customer_address,
                                    x.e_customer_phone,
                                    x.e_salesman_name,
                                    x.n_customer_toplength,
                                    x.i_sj,
                                    x.d_nota,
                                    x.d_jatuh_tempo,
                                    sum(x.v_sisa) AS v_sisa,
                                    x.v_nota_netto,
                                    x.d_jatuh_tempo_plustoleransi,
                                    x.n_toleransi,
                                    x.e_product_groupname,
                                    x.e_remark
                                  FROM
                                    (
                                      /* BACA NOTA */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        b.e_area_shortname,
                                        b.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        d.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        a.d_nota,
                                        a.d_jatuh_tempo,
                                        a.v_sisa,
                                        a.v_nota_netto,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        CASE
                                          WHEN h.f_spb_consigment = 'f' THEN i.e_product_groupname
                                          ELSE 'Modern Outlet'
                                        END AS e_product_groupname,
                                        '' AS e_remark
                                      FROM
                                        tm_nota a
                                      LEFT JOIN tr_area b ON
                                        (a.i_area = b.i_area)
                                      LEFT JOIN tr_customer c ON
                                        (a.i_customer = c.i_customer
                                          AND a.i_area = c.i_area
                                          AND b.i_area = c.i_area)
                                      LEFT JOIN tr_salesman d ON
                                        (a.i_salesman = d.i_salesman)
                                      LEFT JOIN tr_city g ON
                                        (c.i_city = g.i_city
                                          AND c.i_area = g.i_area)
                                      LEFT JOIN tm_spb h ON
                                        a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                      LEFT JOIN tr_product_group i ON
                                        h.i_product_group = i.i_product_group
                                      WHERE
                                        to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.f_nota_cancel = 'f'
                                        AND a.v_sisa>0
                                        AND NOT a.i_nota ISNULL
                                      UNION ALL
                                      /* BACA ALOKASI */
                                      SELECT
                                        j.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        j.d_nota,
                                        a.d_jatuh_tempo,
                                        j.v_jumlah,
                                        j.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        j.e_remark
                                      FROM
                                        tm_alokasi_item j,
                                        tm_alokasi k,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND j.i_alokasi = k.i_alokasi
                                        AND j.i_kbank = k.i_kbank
                                        AND j.i_area = k.i_area
                                        AND k.f_alokasi_cancel = 'f'
                                        AND a.i_nota = j.i_nota
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI KN NON RETUR */	
                                      SELECT
                                        l.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        l.d_nota,
                                        a.d_jatuh_tempo,
                                        l.v_jumlah,
                                        (l.v_sisa + l.v_jumlah) AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        l.e_remark
                                      FROM
                                        tm_alokasikn_item l,
                                        tm_alokasikn m,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_nota = l.i_nota
                                        AND l.i_alokasi = m.i_alokasi
                                        AND l.i_kn = m.i_kn
                                        AND l.i_area = m.i_area
                                        AND m.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI KN RETUR */
                                      SELECT
                                        n.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        n.d_nota,
                                        a.d_jatuh_tempo,
                                        n.v_jumlah,
                                        n.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        n.e_remark
                                      FROM
                                        tm_alokasiknr_item n,
                                        tm_alokasiknr o,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = n.i_nota
                                        AND n.i_alokasi = o.i_alokasi
                                        AND n.i_kn = o.i_kn
                                        AND n.i_area = o.i_area
                                        AND o.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI HUT LAIN */
                                      SELECT
                                        p.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_jumlah,
                                        p.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_remark
                                      FROM
                                        tm_alokasihl_item p,
                                        tm_alokasihl q,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = p.i_nota
                                        AND p.i_alokasi = q.i_alokasi
                                        AND p.i_area = q.i_area
                                        AND q.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* JURNAL */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_refference AS d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_mutasi_debet AS v_jumlah,
                                        p.v_mutasi_debet AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_description AS e_remark
                                      FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /*************************KONSINYASI*************************/
                                      /* ALOKASI BANK MASUK KONSINYASI */
                                      SELECT
                                        j.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        j.d_nota,
                                        a.d_jatuh_tempo,
                                        j.v_jumlah,
                                        j.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        j.e_remark
                                      FROM
                                        tm_alokasi_item j,
                                        tm_alokasi k,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND j.i_alokasi = k.i_alokasi
                                        AND j.i_kbank = k.i_kbank
                                        AND j.i_area = k.i_area
                                        AND k.f_alokasi_cancel = 'f'
                                        AND a.i_nota = j.i_nota
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOKASI KN NON RETUR KONSINYASI */
                                      SELECT
                                        l.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        l.d_nota,
                                        a.d_jatuh_tempo,
                                        l.v_jumlah,
                                        (l.v_sisa + l.v_jumlah) AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        l.e_remark
                                      FROM
                                        tm_alokasikn_item l,
                                        tm_alokasikn m,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = l.i_nota
                                        AND l.i_alokasi = m.i_alokasi
                                        AND l.i_kn = m.i_kn
                                        AND l.i_area = m.i_area
                                        AND m.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOKASI KN RETUR KONSINYASI */
                                      SELECT
                                        n.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        n.d_nota,
                                        a.d_jatuh_tempo,
                                        n.v_jumlah,
                                        n.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        n.e_remark
                                      FROM
                                        tm_alokasiknr_item n,
                                        tm_alokasiknr o,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = n.i_nota
                                        AND n.i_alokasi = o.i_alokasi
                                        AND n.i_kn = o.i_kn
                                        AND n.i_area = o.i_area
                                        AND o.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOAKSI HUT LAIN KONSINYASI */
                                      SELECT
                                        p.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_jumlah,
                                        p.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        p.e_remark
                                      FROM
                                        tm_alokasihl_item p,
                                        tm_alokasihl q,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = p.i_nota
                                        AND p.i_alokasi = q.i_alokasi
                                        AND p.i_area = q.i_area
                                        AND q.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* JURNAL KONSINYASI */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_refference AS d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_mutasi_debet AS v_jumlah,
                                        p.v_mutasi_debet AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_description AS e_remark
                                      FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                  ) AS x
                                  GROUP BY
                                    x.i_nota,
                                    x.i_area,
                                    x.e_area_shortname,
                                    x.e_area_name,
                                    x.i_customer,
                                    x.e_customer_name,
                                    x.e_salesman_name,
                                    x.n_customer_toplength,
                                    x.i_sj,
                                    x.d_nota,
                                    x.d_jatuh_tempo,
                                    x.d_jatuh_tempo_plustoleransi,
                                    x.n_toleransi,
                                    x.e_product_groupname,
                                    x.e_remark,
                                    x.v_nota_netto,
                                    x.e_customer_address,
                                    x.e_customer_phone
                                  ORDER BY
                                    x.i_area ASC,
                                    x.i_nota ASC,
                                    x.i_customer ASC
                                ) as z
                                GROUP BY z.i_customer,
                                  z.e_customer_name,
                                  z.i_area
                                UNION ALL
                                /* Penjualan */
                                SELECT
                                  a.i_customer,
                                  b.e_customer_name,
                                  a.i_area,
                                  0 AS saldo_awal,
                                  sum(a.v_nota_netto) AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_nota a,
                                  tr_customer b
                                WHERE
                                  a.i_customer = b.i_customer
                                  AND to_char(a.d_nota, 'yyyymm')= '$iperiode'
                                  AND a.f_nota_cancel = 'f'
                                GROUP BY
                                  a.i_customer,
                                  b.e_customer_name,
                                  a.i_area
                                UNION ALL
                                /* Alokasi */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  a.v_jumlah AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasi_item a,
                                  tm_alokasi b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_kbank = b.i_kbank
                                  AND a.i_area = b.i_area
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* ALOKASI KN NON RETUR */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  a.v_jumlah AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasiknr_item a,
                                  tm_alokasiknr b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND a.i_kn = b.i_kn
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'	
                                UNION ALL
                                /* ALOKASI KN RETUR */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  a.v_jumlah AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasikn_item a,
                                  tm_alokasikn b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND a.i_kn = b.i_kn
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* ALOKASI HUT LAIN */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  a.v_jumlah AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasihl_item a,
                                  tm_alokasihl b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* JURNAL */
                                SELECT
                                  a.i_customer,
                                  c.e_customer_name,
                                  a.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  p.v_mutasi_debet AS pembulatan
                                FROM
                                  tm_general_ledger p,
                                  tr_customer c,
                                  tr_city g,
                                  tr_salesman b,
                                  tm_spb h,
                                  tr_product_group i,
                                  tr_area area,
                                  tm_nota a
                                WHERE
                                  to_char(p.d_mutasi, 'yyyymm')= '$iperiode'
                                  AND NOT a.i_nota ISNULL
                                  AND a.i_salesman = b.i_salesman
                                  AND a.f_nota_cancel = 'f'
                                  AND a.i_nota = substring(p.i_refference, 15, 15)
                                  AND p.f_debet = 't'
                                  AND p.i_coa = '610-2902'
                                  AND c.i_city = g.i_city
                                  AND c.i_area = g.i_area
                                  AND a.i_salesman = b.i_salesman
                                  AND h.i_product_group = i.i_product_group
                                  AND a.i_spb = h.i_spb
                                  AND a.i_area = h.i_area
                                  AND area.i_area = a.i_area
                                  AND a.i_customer = c.i_customer
                                ) as x
                                group by x.i_area
                              ) a 
                              INNER JOIN tr_area b ON (a.i_area = b.i_area)
                              WHERE (b.i_area like '%$cari%' OR b.e_area_name like '%$cari%')
                              ORDER BY
                                i_area ", false);
      } else {
        $this->db->select("   a.*,
                              b.e_area_name
                            FROM
                              (
                              SELECT
                                x.i_area,
                                sum(x.saldo_awal) AS saldo_awal,
                                sum(x.penjualan) AS penjualan,
                                sum(x.alokasi_bm) AS alokasi_bm,
                                sum(x.alokasi_knr) AS alokasi_knr,
                                sum(x.alokasi_kn) AS alokasi_kn,
                                sum(x.alokasi_hll) AS alokasi_hll,
                                sum(x.pembulatan) AS pembulatan,
                                ((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) AS saldo_akhir
                              FROM
                                (
                                SELECT
                                  z.i_customer,
                                  z.e_customer_name,
                                  z.i_area,
                                  sum(z.v_sisa) AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  (
                                  SELECT
                                    x.i_nota,
                                    x.i_area,
                                    x.e_area_shortname,
                                    x.e_area_name,
                                    x.i_customer,
                                    x.e_customer_name,
                                    x.e_customer_address,
                                    x.e_customer_phone,
                                    x.e_salesman_name,
                                    x.n_customer_toplength,
                                    x.i_sj,
                                    x.d_nota,
                                    x.d_jatuh_tempo,
                                    sum(x.v_sisa) AS v_sisa,
                                    x.v_nota_netto,
                                    x.d_jatuh_tempo_plustoleransi,
                                    x.n_toleransi,
                                    x.e_product_groupname,
                                    x.e_remark
                                  FROM
                                    (
                                      /* BACA NOTA */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        b.e_area_shortname,
                                        b.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        d.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        a.d_nota,
                                        a.d_jatuh_tempo,
                                        a.v_sisa,
                                        a.v_nota_netto,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        CASE
                                          WHEN h.f_spb_consigment = 'f' THEN i.e_product_groupname
                                          ELSE 'Modern Outlet'
                                        END AS e_product_groupname,
                                        '' AS e_remark
                                      FROM
                                        tm_nota a
                                      LEFT JOIN tr_area b ON
                                        (a.i_area = b.i_area)
                                      LEFT JOIN tr_customer c ON
                                        (a.i_customer = c.i_customer
                                          AND a.i_area = c.i_area
                                          AND b.i_area = c.i_area)
                                      LEFT JOIN tr_salesman d ON
                                        (a.i_salesman = d.i_salesman)
                                      LEFT JOIN tr_city g ON
                                        (c.i_city = g.i_city
                                          AND c.i_area = g.i_area)
                                      LEFT JOIN tm_spb h ON
                                        a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                      LEFT JOIN tr_product_group i ON
                                        h.i_product_group = i.i_product_group
                                      WHERE
                                        to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.f_nota_cancel = 'f'
                                        AND a.v_sisa>0
                                        AND NOT a.i_nota ISNULL
                                      UNION ALL
                                      /* BACA ALOKASI */
                                      SELECT
                                        j.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        j.d_nota,
                                        a.d_jatuh_tempo,
                                        j.v_jumlah,
                                        j.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        j.e_remark
                                      FROM
                                        tm_alokasi_item j,
                                        tm_alokasi k,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND j.i_alokasi = k.i_alokasi
                                        AND j.i_kbank = k.i_kbank
                                        AND j.i_area = k.i_area
                                        AND k.f_alokasi_cancel = 'f'
                                        AND a.i_nota = j.i_nota
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI KN NON RETUR */	
                                      SELECT
                                        l.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        l.d_nota,
                                        a.d_jatuh_tempo,
                                        l.v_jumlah,
                                        (l.v_sisa + l.v_jumlah) AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        l.e_remark
                                      FROM
                                        tm_alokasikn_item l,
                                        tm_alokasikn m,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_nota = l.i_nota
                                        AND l.i_alokasi = m.i_alokasi
                                        AND l.i_kn = m.i_kn
                                        AND l.i_area = m.i_area
                                        AND m.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI KN RETUR */
                                      SELECT
                                        n.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        n.d_nota,
                                        a.d_jatuh_tempo,
                                        n.v_jumlah,
                                        n.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        n.e_remark
                                      FROM
                                        tm_alokasiknr_item n,
                                        tm_alokasiknr o,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = n.i_nota
                                        AND n.i_alokasi = o.i_alokasi
                                        AND n.i_kn = o.i_kn
                                        AND n.i_area = o.i_area
                                        AND o.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* BACA ALOKASI HUT LAIN */
                                      SELECT
                                        p.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_jumlah,
                                        p.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_remark
                                      FROM
                                        tm_alokasihl_item p,
                                        tm_alokasihl q,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = p.i_nota
                                        AND p.i_alokasi = q.i_alokasi
                                        AND p.i_area = q.i_area
                                        AND q.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* JURNAL */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_refference AS d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_mutasi_debet AS v_jumlah,
                                        p.v_mutasi_debet AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_description AS e_remark
                                      FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 'f'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /*************************KONSINYASI*************************/
                                      /* ALOKASI BANK MASUK KONSINYASI */
                                      SELECT
                                        j.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        j.d_nota,
                                        a.d_jatuh_tempo,
                                        j.v_jumlah,
                                        j.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        j.e_remark
                                      FROM
                                        tm_alokasi_item j,
                                        tm_alokasi k,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND NOT a.i_nota ISNULL
                                        AND j.i_alokasi = k.i_alokasi
                                        AND j.i_kbank = k.i_kbank
                                        AND j.i_area = k.i_area
                                        AND k.f_alokasi_cancel = 'f'
                                        AND a.i_nota = j.i_nota
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOKASI KN NON RETUR KONSINYASI */
                                      SELECT
                                        l.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        l.d_nota,
                                        a.d_jatuh_tempo,
                                        l.v_jumlah,
                                        (l.v_sisa + l.v_jumlah) AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        l.e_remark
                                      FROM
                                        tm_alokasikn_item l,
                                        tm_alokasikn m,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = l.i_nota
                                        AND l.i_alokasi = m.i_alokasi
                                        AND l.i_kn = m.i_kn
                                        AND l.i_area = m.i_area
                                        AND m.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOKASI KN RETUR KONSINYASI */
                                      SELECT
                                        n.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        n.d_nota,
                                        a.d_jatuh_tempo,
                                        n.v_jumlah,
                                        n.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        n.e_remark
                                      FROM
                                        tm_alokasiknr_item n,
                                        tm_alokasiknr o,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = n.i_nota
                                        AND n.i_alokasi = o.i_alokasi
                                        AND n.i_kn = o.i_kn
                                        AND n.i_area = o.i_area
                                        AND o.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* ALOAKSI HUT LAIN KONSINYASI */
                                      SELECT
                                        p.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_jumlah,
                                        p.v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        'Modern Outlet' AS e_product_groupname,
                                        p.e_remark
                                      FROM
                                        tm_alokasihl_item p,
                                        tm_alokasihl q,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = p.i_nota
                                        AND p.i_alokasi = q.i_alokasi
                                        AND p.i_area = q.i_area
                                        AND q.f_alokasi_cancel = 'f'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                      UNION ALL
                                      /* JURNAL KONSINYASI */
                                      SELECT
                                        a.i_nota,
                                        a.i_area,
                                        area.e_area_shortname,
                                        area.e_area_name,
                                        a.i_customer,
                                        c.e_customer_name,
                                        c.e_customer_address,
                                        c.e_customer_phone,
                                        b.e_salesman_name,
                                        c.n_customer_toplength,
                                        a.i_sj,
                                        p.d_refference AS d_nota,
                                        a.d_jatuh_tempo,
                                        p.v_mutasi_debet AS v_jumlah,
                                        p.v_mutasi_debet AS v_sisa,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_pusat
                                          ELSE a.d_jatuh_tempo + INTERVAL '1' DAY * g.n_toleransi_cabang
                                        END AS d_jatuh_tempo_plustoleransi,
                                        CASE
                                          WHEN substring(a.i_sj, 9, 2)= '00' THEN g.n_toleransi_pusat
                                          ELSE g.n_toleransi_cabang
                                        END AS n_toleransi,
                                        i.e_product_groupname,
                                        p.e_description AS e_remark
                                      FROM
                                        tm_general_ledger p,
                                        tr_customer c,
                                        tr_city g,
                                        tr_salesman b,
                                        tm_spb h,
                                        tr_product_group i,
                                        tr_area area,
                                        tm_nota a
                                      WHERE
                                        to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum'
                                        AND to_char(a.d_nota, 'yyyymm')<= '$periodesebelum'
                                        AND NOT a.i_nota ISNULL
                                        AND a.i_salesman = b.i_salesman
                                        AND a.f_nota_cancel = 'f'
                                        AND a.i_nota = substring(p.i_refference, 15, 15)
                                        AND p.f_debet = 't'
                                        AND p.i_coa = '610-2902'
                                        AND c.i_city = g.i_city
                                        AND c.i_area = g.i_area
                                        AND a.i_salesman = b.i_salesman
                                        AND h.i_product_group = i.i_product_group
                                        AND a.i_spb = h.i_spb
                                        AND a.i_area = h.i_area
                                        AND h.f_spb_consigment = 't'
                                        AND area.i_area = a.i_area
                                        AND a.i_customer = c.i_customer
                                  ) AS x
                                  GROUP BY
                                    x.i_nota,
                                    x.i_area,
                                    x.e_area_shortname,
                                    x.e_area_name,
                                    x.i_customer,
                                    x.e_customer_name,
                                    x.e_salesman_name,
                                    x.n_customer_toplength,
                                    x.i_sj,
                                    x.d_nota,
                                    x.d_jatuh_tempo,
                                    x.d_jatuh_tempo_plustoleransi,
                                    x.n_toleransi,
                                    x.e_product_groupname,
                                    x.e_remark,
                                    x.v_nota_netto,
                                    x.e_customer_address,
                                    x.e_customer_phone
                                  ORDER BY
                                    x.i_area ASC,
                                    x.i_nota ASC,
                                    x.i_customer ASC
                                ) as z
                                GROUP BY z.i_customer,
                                  z.e_customer_name,
                                  z.i_area
                                UNION ALL
                                /* Penjualan */
                                SELECT
                                  a.i_customer,
                                  b.e_customer_name,
                                  a.i_area,
                                  0 AS saldo_awal,
                                  sum(a.v_nota_netto) AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_nota a,
                                  tr_customer b
                                WHERE
                                  a.i_customer = b.i_customer
                                  AND to_char(a.d_nota, 'yyyymm')= '$iperiode'
                                  AND a.f_nota_cancel = 'f'
                                GROUP BY
                                  a.i_customer,
                                  b.e_customer_name,
                                  a.i_area
                                UNION ALL
                                /* Alokasi */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  a.v_jumlah AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasi_item a,
                                  tm_alokasi b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_kbank = b.i_kbank
                                  AND a.i_area = b.i_area
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* ALOKASI KN NON RETUR */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  a.v_jumlah AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasiknr_item a,
                                  tm_alokasiknr b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND a.i_kn = b.i_kn
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'	
                                UNION ALL
                                /* ALOKASI KN RETUR */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  a.v_jumlah AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasikn_item a,
                                  tm_alokasikn b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND a.i_kn = b.i_kn
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* ALOKASI HUT LAIN */
                                SELECT
                                  d.i_customer,
                                  c.e_customer_name,
                                  d.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  a.v_jumlah AS alokasi_hll,
                                  0 AS pembulatan
                                FROM
                                  tm_alokasihl_item a,
                                  tm_alokasihl b,
                                  tr_customer c,
                                  tm_nota d
                                WHERE
                                  a.i_alokasi = b.i_alokasi
                                  AND a.i_area = b.i_area
                                  AND d.i_customer = c.i_customer
                                  AND b.f_alokasi_cancel = 'f'
                                  AND a.i_nota = d.i_nota
                                  AND to_char(b.d_alokasi, 'yyyymm')= '$iperiode'
                                UNION ALL
                                /* JURNAL */
                                SELECT
                                  a.i_customer,
                                  c.e_customer_name,
                                  a.i_area,
                                  0 AS saldo_awal,
                                  0 AS penjualan,
                                  0 AS alokasi_bm,
                                  0 AS alokasi_knr,
                                  0 AS alokasi_kn,
                                  0 AS alokasi_hll,
                                  p.v_mutasi_debet AS pembulatan
                                FROM
                                  tm_general_ledger p,
                                  tr_customer c,
                                  tr_city g,
                                  tr_salesman b,
                                  tm_spb h,
                                  tr_product_group i,
                                  tr_area area,
                                  tm_nota a
                                WHERE
                                  to_char(p.d_mutasi, 'yyyymm')= '$iperiode'
                                  AND NOT a.i_nota ISNULL
                                  AND a.i_salesman = b.i_salesman
                                  AND a.f_nota_cancel = 'f'
                                  AND a.i_nota = substring(p.i_refference, 15, 15)
                                  AND p.f_debet = 't'
                                  AND p.i_coa = '610-2902'
                                  AND c.i_city = g.i_city
                                  AND c.i_area = g.i_area
                                  AND a.i_salesman = b.i_salesman
                                  AND h.i_product_group = i.i_product_group
                                  AND a.i_spb = h.i_spb
                                  AND a.i_area = h.i_area
                                  AND area.i_area = a.i_area
                                  AND a.i_customer = c.i_customer
                                ) as x
                                WHERE
                                  x.i_area = '$iarea'
                                group by x.i_area
                              ) a 
                              INNER JOIN tr_area b ON (a.i_area = b.i_area)
                              WHERE (b.i_area like '%$cari%' OR b.e_area_name like '%$cari%')
                              ORDER BY
                                i_area ", false);
      }
    }
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
  function bacadetail($icustomer, $iperiode, $num, $offset)
  {
    $this->db->select(" x.* from(
		/* Penjualan */
		select 'Penjualan' as status, a.i_customer, b.e_customer_name, a.i_area, a.i_nota, a.d_nota, a.v_nota_netto as debet, 0 as kredit from
		tm_nota a, tr_customer b
		where
		a.i_customer = b.i_customer
		and to_char(a.d_nota,'yyyymm')='$iperiode'
		and a.f_nota_cancel = 'f'
		and a.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_kbank = b.i_kbank
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet,  a.v_jumlah  from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Pembulatan' as status, a.i_customer, c.e_customer_name, a.i_area, p.i_refference, p.d_mutasi, 0 as debet, p.v_mutasi_debet as v_jumlah from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
		where to_char(p.d_mutasi,'yyyymm')='$iperiode' and not a.i_nota isnull
		and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
		and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
		and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
		and a.i_spb=h.i_spb and a.i_area=h.i_area  and area.i_area=a.i_area  and a.i_customer=c.i_customer
		and a.i_customer = '$icustomer'
		) as x order by x.d_nota asc", false)->limit($num, $offset);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      return $query->result();
    }
  }
}
