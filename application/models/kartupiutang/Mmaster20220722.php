<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3' 
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaarea($num,$offset,$iuser)
    {
		  $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')  order by i_area", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    
    function cariarea($cari,$num,$offset,$iuser)
    {
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by i_area ", FALSE)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
/*
    function bacaperiode($iarea,$dto,$num,$offset,$cari,$nt,$jt)
    {
      if($nt!=''){
	      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
						      where a.i_customer=b.i_customer 
						      and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
                  and not a.i_nota isnull
						      and (upper(a.i_nota) like '%$cari%' 
						        or upper(a.i_spb) like '%$cari%' 
						        or upper(a.i_customer) like '%$cari%' 
						        or upper(b.e_customer_name) like '%$cari%')
						      and a.i_area='$iarea' 
                  and a.d_nota <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 
                  and a.f_nota_cancel='f'
						      ORDER BY a.d_nota ",false)->limit($num,$offset);
      }else{
	      $this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
						      where a.i_customer=b.i_customer 
						      and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
                  and not a.i_nota isnull
						      and (upper(a.i_nota) like '%$cari%' 
						        or upper(a.i_spb) like '%$cari%' 
						        or upper(a.i_customer) like '%$cari%' 
						        or upper(b.e_customer_name) like '%$cari%')
						      and a.i_area='$iarea' 
                  and a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and a.v_sisa>0 
                  and a.f_nota_cancel='f'
						      ORDER BY a.d_jatuh_tempo",false)->limit($num,$offset);
      }
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
*/
    function bacaperiodeperpages($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cariperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    {
		$this->db->select("	a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
              and a.f_nota_koreksi='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')
							ORDER BY a.i_nota ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($iarea,$dto)
    {
		$this->db->select(" a.*, b.e_customer_name,b.e_customer_address,b.e_customer_city
					              from tm_nota a, tr_customer b
					              where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
					              and a.i_customer=b.i_customer
					              order by a.i_nota ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iarea,$iperiode,$num,$offset,$cari,$cust,$area)
    {
		$periode=  $iperiode;
		$tanggal_awal = '01-'.substr($periode, 4, 2).'-'.substr($periode, 0, 4);
		$periodesebelum =  date('Ym', strtotime('-1 month', strtotime($tanggal_awal)));
		if($cust == 'qqq'){
		if($iarea == 'NA'){
			$this->db->select(" x.i_customer, x.e_customer_name, sum(x.saldo_awal) as saldo_awal, sum(x.penjualan) as penjualan, sum(x.alokasi_bm) as alokasi_bm, sum(x.alokasi_knr) as alokasi_knr, sum(x.alokasi_kn) as alokasi_kn, sum(x.alokasi_hll) as alokasi_hll, sum(x.pembulatan) as pembulatan, 
			((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) as saldo_akhir from(
			 select z.i_customer, z.e_customer_name, z.i_area, sum(z.v_sisa) as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from(
			 SELECT x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_address, x.e_customer_phone, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, sum(x.v_sisa) as v_sisa, x.v_nota_netto, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark from (
			 select a.i_nota, a.i_area, b.e_area_shortname, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, d.e_salesman_name, c.n_customer_toplength, a.i_sj, a.d_nota, a.d_jatuh_tempo, a.v_sisa, a.v_nota_netto, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, case when h.f_spb_consigment='f' then i.e_product_groupname else 'Modern Outlet' end as e_product_groupname, '' as e_remark
			 from tm_nota a
			 left join tr_area b on (a.i_area = b.i_area) 
			 left join tr_customer c on (a.i_customer = c.i_customer and a.i_area = c.i_area and b.i_area = c.i_area)
			 left join tr_salesman d on (a.i_salesman=d.i_salesman)
			 left join tr_city g on (c.i_city = g.i_city and c.i_area = g.i_area)
			 left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
			 left join tr_product_group i on h.i_product_group=i.i_product_group
			 where to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.f_nota_cancel='f' and a.v_sisa>0 and not a.i_nota isnull
			 
			 union all
			 /*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
			 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum'
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 /*--------------------------------------Konsinyasi-----------------------------------------------------------------------------------------------------*/
			 union all
			 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 
			 ) as x
			 group by x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark, x.v_nota_netto, x.e_customer_address, x.e_customer_phone order by x.i_area asc, x.i_nota asc, x.i_customer asc
			 ) as z
			 group by z.i_customer, z.e_customer_name, z.i_area
			 union all
			 /* Penjualan */
			 select a.i_customer, b.e_customer_name, a.i_area, 0 as saldo_awal, sum(a.v_nota_netto) as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from
			 tm_nota a, tr_customer b
			 where
			 a.i_customer = b.i_customer
			 and to_char(a.d_nota, 'yyyymm')='$iperiode'
			 and a.f_nota_cancel = 'f'
			 group by a.i_customer, b.e_customer_name, a.i_area
			 union all
			 /* Alokasi */
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, a.v_jumlah as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_kbank = b.i_kbank
			 and a.i_area = b.i_area
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, a.v_jumlah as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and a.i_kn = b.i_kn
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, a.v_jumlah as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and a.i_kn = b.i_kn
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, a.v_jumlah as alokasi_hll, 0 as pembulatan from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select a.i_customer, c.e_customer_name, a.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, p.v_mutasi_debet as pembulatan from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')='$iperiode' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and area.i_area=a.i_area and a.i_customer=c.i_customer
			 ) as x
			 group by x.i_customer, x.e_customer_name
			  ",false);
		}else{
			$this->db->select(" x.i_customer, x.e_customer_name, sum(x.saldo_awal) as saldo_awal, sum(x.penjualan) as penjualan, sum(x.alokasi_bm) as alokasi_bm, sum(x.alokasi_knr) as alokasi_knr, sum(x.alokasi_kn) as alokasi_kn, sum(x.alokasi_hll) as alokasi_hll, sum(x.pembulatan) as pembulatan, 
			((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) as saldo_akhir from(
			 select z.i_customer, z.e_customer_name, z.i_area, sum(z.v_sisa) as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from(
			 SELECT x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_address, x.e_customer_phone, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, sum(x.v_sisa) as v_sisa, x.v_nota_netto, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark from (
			 select a.i_nota, a.i_area, b.e_area_shortname, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, d.e_salesman_name, c.n_customer_toplength, a.i_sj, a.d_nota, a.d_jatuh_tempo, a.v_sisa, a.v_nota_netto, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, case when h.f_spb_consigment='f' then i.e_product_groupname else 'Modern Outlet' end as e_product_groupname, '' as e_remark
			 from tm_nota a
			 left join tr_area b on (a.i_area = b.i_area) 
			 left join tr_customer c on (a.i_customer = c.i_customer and a.i_area = c.i_area and b.i_area = c.i_area)
			 left join tr_salesman d on (a.i_salesman=d.i_salesman)
			 left join tr_city g on (c.i_city = g.i_city and c.i_area = g.i_area)
			 left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
			 left join tr_product_group i on h.i_product_group=i.i_product_group
			 where to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.f_nota_cancel='f' and a.v_sisa>0 and not a.i_nota isnull
			 
			 union all
			 /*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
			 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum'
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 /*--------------------------------------Konsinyasi-----------------------------------------------------------------------------------------------------*/
			 union all
			 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
			 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 
			 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
			 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, 'Modern Outlet' as e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 union all
			 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
			 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
			 else g.n_toleransi_cabang
			 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
			 
			 ) as x
			 group by x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark, x.v_nota_netto, x.e_customer_address, x.e_customer_phone order by x.i_area asc, x.i_nota asc, x.i_customer asc
			 ) as z
			 group by z.i_customer, z.e_customer_name, z.i_area
			 union all
			 /* Penjualan */
			 select a.i_customer, b.e_customer_name, a.i_area, 0 as saldo_awal, sum(a.v_nota_netto) as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from
			 tm_nota a, tr_customer b
			 where
			 a.i_customer = b.i_customer
			 and to_char(a.d_nota, 'yyyymm')='$iperiode'
			 and a.f_nota_cancel = 'f'
			 group by a.i_customer, b.e_customer_name, a.i_area
			 union all
			 /* Alokasi */
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, a.v_jumlah as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_kbank = b.i_kbank
			 and a.i_area = b.i_area
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, a.v_jumlah as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and a.i_kn = b.i_kn
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, a.v_jumlah as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and a.i_kn = b.i_kn
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, a.v_jumlah as alokasi_hll, 0 as pembulatan from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
			 where a.i_alokasi = b.i_alokasi
			 and a.i_area = b.i_area
			 and d.i_customer = c.i_customer
			 and b.f_alokasi_cancel = 'f'
			 and a.i_nota = d.i_nota
			 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
			 union all
			 select a.i_customer, c.e_customer_name, a.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, p.v_mutasi_debet as pembulatan from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
			 where to_char(p.d_mutasi, 'yyyymm')='$iperiode' and not a.i_nota isnull
			 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
			 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
			 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
			 and a.i_spb=h.i_spb and a.i_area=h.i_area and area.i_area=a.i_area and a.i_customer=c.i_customer
			 ) as x
			 where x.i_area = '$iarea'
			 group by x.i_customer, x.e_customer_name
				",false);
				}
			}else{
				if($iarea == 'NA'){
					$this->db->
					select(" a.*, b.e_area_name from (select x.i_area, sum(x.saldo_awal) as saldo_awal, sum(x.penjualan) as penjualan, sum(x.alokasi_bm) as alokasi_bm, sum(x.alokasi_knr) as alokasi_knr, sum(x.alokasi_kn) as alokasi_kn, sum(x.alokasi_hll) as alokasi_hll, sum(x.pembulatan) as pembulatan, 
					((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) as saldo_akhir from(
					 select z.i_customer, z.e_customer_name, z.i_area, sum(z.v_sisa) as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from(
					 SELECT x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_address, x.e_customer_phone, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, sum(x.v_sisa) as v_sisa, x.v_nota_netto, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark from (
					 select a.i_nota, a.i_area, b.e_area_shortname, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, d.e_salesman_name, c.n_customer_toplength, a.i_sj, a.d_nota, a.d_jatuh_tempo, a.v_sisa, a.v_nota_netto, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, case when h.f_spb_consigment='f' then i.e_product_groupname else 'Modern Outlet' end as e_product_groupname, '' as e_remark
					 from tm_nota a
					 left join tr_area b on (a.i_area = b.i_area) 
					 left join tr_customer c on (a.i_customer = c.i_customer and a.i_area = c.i_area and b.i_area = c.i_area)
					 left join tr_salesman d on (a.i_salesman=d.i_salesman)
					 left join tr_city g on (c.i_city = g.i_city and c.i_area = g.i_area)
					 left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
					 left join tr_product_group i on h.i_product_group=i.i_product_group
					 where to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.f_nota_cancel='f' and a.v_sisa>0 and not a.i_nota isnull
					 
					 union all
					 /*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
					 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum'
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 /*--------------------------------------Konsinyasi-----------------------------------------------------------------------------------------------------*/
					 union all
					 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 
					 ) as x
					 group by x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark, x.v_nota_netto, x.e_customer_address, x.e_customer_phone order by x.i_area asc, x.i_nota asc, x.i_customer asc
					 ) as z
					 group by z.i_customer, z.e_customer_name, z.i_area
					 union all
					 /* Penjualan */
					 select a.i_customer, b.e_customer_name, a.i_area, 0 as saldo_awal, sum(a.v_nota_netto) as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from
					 tm_nota a, tr_customer b
					 where
					 a.i_customer = b.i_customer
					 and to_char(a.d_nota, 'yyyymm')='$iperiode'
					 and a.f_nota_cancel = 'f'
					 group by a.i_customer, b.e_customer_name, a.i_area
					 union all
					 /* Alokasi */
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, a.v_jumlah as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_kbank = b.i_kbank
					 and a.i_area = b.i_area
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, a.v_jumlah as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and a.i_kn = b.i_kn
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, a.v_jumlah as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and a.i_kn = b.i_kn
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, a.v_jumlah as alokasi_hll, 0 as pembulatan from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select a.i_customer, c.e_customer_name, a.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, p.v_mutasi_debet as pembulatan from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')='$iperiode' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and area.i_area=a.i_area and a.i_customer=c.i_customer
					 ) as x
					 group by x.i_area) a 
					 inner join tr_area b on (a.i_area = b.i_area)
					 order by i_area
					  ",false);
				}else{
					$this->db->select(" a.*, b.e_area_name from (select x.i_area, sum(x.saldo_awal) as saldo_awal, sum(x.penjualan) as penjualan, sum(x.alokasi_bm) as alokasi_bm, sum(x.alokasi_knr) as alokasi_knr, sum(x.alokasi_kn) as alokasi_kn, sum(x.alokasi_hll) as alokasi_hll, sum(x.pembulatan) as pembulatan, 
					((sum(x.saldo_awal) + sum(x.penjualan)) - (sum(x.alokasi_bm) + sum(x.alokasi_knr) + sum(x.alokasi_kn) + sum(x.alokasi_hll) + sum(x.pembulatan) )) as saldo_akhir from(
					 select z.i_customer, z.e_customer_name, z.i_area, sum(z.v_sisa) as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from(
					 SELECT x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_address, x.e_customer_phone, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, sum(x.v_sisa) as v_sisa, x.v_nota_netto, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark from (
					 select a.i_nota, a.i_area, b.e_area_shortname, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, d.e_salesman_name, c.n_customer_toplength, a.i_sj, a.d_nota, a.d_jatuh_tempo, a.v_sisa, a.v_nota_netto, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, case when h.f_spb_consigment='f' then i.e_product_groupname else 'Modern Outlet' end as e_product_groupname, '' as e_remark
					 from tm_nota a
					 left join tr_area b on (a.i_area = b.i_area) 
					 left join tr_customer c on (a.i_customer = c.i_customer and a.i_area = c.i_area and b.i_area = c.i_area)
					 left join tr_salesman d on (a.i_salesman=d.i_salesman)
					 left join tr_city g on (c.i_city = g.i_city and c.i_area = g.i_area)
					 left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
					 left join tr_product_group i on h.i_product_group=i.i_product_group
					 where to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.f_nota_cancel='f' and a.v_sisa>0 and not a.i_nota isnull
					 
					 union all
					 /*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
					 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum'
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='f' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 /*--------------------------------------Konsinyasi-----------------------------------------------------------------------------------------------------*/
					 union all
					 select j.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, j.d_nota, a.d_jatuh_tempo, j.v_jumlah, j.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, j.e_remark from tm_alokasi_item j, tm_alokasi k, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(k.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and a.i_salesman=b.i_salesman and a.f_nota_cancel='f' and not a.i_nota isnull
					 and j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area and k.f_alokasi_cancel='f' and a.i_nota=j.i_nota
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select l.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, l.d_nota, a.d_jatuh_tempo, l.v_jumlah, (l.v_sisa+l.v_jumlah) as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, l.e_remark from tm_alokasikn_item l, tm_alokasikn m, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(m.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=l.i_nota and l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area and m.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select n.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, n.d_nota, a.d_jatuh_tempo, n.v_jumlah, n.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, n.e_remark from tm_alokasiknr_item n, tm_alokasiknr o, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(o.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=n.i_nota and n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area and o.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 
					 select p.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_nota, a.d_jatuh_tempo, p.v_jumlah, p.v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
					 end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, 'Modern Outlet' as e_product_groupname, p.e_remark from tm_alokasihl_item p, tm_alokasihl q, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(q.d_alokasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=p.i_nota and p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 union all
					 select a.i_nota, a.i_area, area.e_area_shortname, area.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_phone, b.e_salesman_name, c.n_customer_toplength, a.i_sj, p.d_refference as d_nota, a.d_jatuh_tempo, p.v_mutasi_debet as v_jumlah, p.v_mutasi_debet as v_sisa, case when substring(a.i_sj, 9, 2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
					 else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi, case when substring(a.i_sj, 9, 2)='00' then g.n_toleransi_pusat
					 else g.n_toleransi_cabang
					 end as n_toleransi, i.e_product_groupname, p.e_description as e_remark from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')>'$periodesebelum' and to_char(a.d_nota, 'yyyymm')<='$periodesebelum' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and h.f_spb_consigment='t' and area.i_area=a.i_area and a.i_customer=c.i_customer
					 
					 ) as x
					 group by x.i_nota, x.i_area, x.e_area_shortname, x.e_area_name, x.i_customer, x.e_customer_name, x.e_salesman_name, x.n_customer_toplength, x.i_sj, x.d_nota, x.d_jatuh_tempo, x.d_jatuh_tempo_plustoleransi, x.n_toleransi, x.e_product_groupname, x.e_remark, x.v_nota_netto, x.e_customer_address, x.e_customer_phone order by x.i_area asc, x.i_nota asc, x.i_customer asc
					 ) as z
					 group by z.i_customer, z.e_customer_name, z.i_area
					 union all
					 /* Penjualan */
					 select a.i_customer, b.e_customer_name, a.i_area, 0 as saldo_awal, sum(a.v_nota_netto) as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from
					 tm_nota a, tr_customer b
					 where
					 a.i_customer = b.i_customer
					 and to_char(a.d_nota, 'yyyymm')='$iperiode'
					 and a.f_nota_cancel = 'f'
					 group by a.i_customer, b.e_customer_name, a.i_area
					 union all
					 /* Alokasi */
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, a.v_jumlah as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_kbank = b.i_kbank
					 and a.i_area = b.i_area
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, a.v_jumlah as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and a.i_kn = b.i_kn
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, a.v_jumlah as alokasi_kn, 0 as alokasi_hll, 0 as pembulatan from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and a.i_kn = b.i_kn
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select d.i_customer, c.e_customer_name, d.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, a.v_jumlah as alokasi_hll, 0 as pembulatan from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
					 where a.i_alokasi = b.i_alokasi
					 and a.i_area = b.i_area
					 and d.i_customer = c.i_customer
					 and b.f_alokasi_cancel = 'f'
					 and a.i_nota = d.i_nota
					 and to_char(b.d_alokasi, 'yyyymm')='$iperiode'
					 union all
					 select a.i_customer, c.e_customer_name, a.i_area, 0 as saldo_awal, 0 as penjualan, 0 as alokasi_bm, 0 as alokasi_knr, 0 as alokasi_kn, 0 as alokasi_hll, p.v_mutasi_debet as pembulatan from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
					 where to_char(p.d_mutasi, 'yyyymm')='$iperiode' and not a.i_nota isnull
					 and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
					 and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
					 and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
					 and a.i_spb=h.i_spb and a.i_area=h.i_area and area.i_area=a.i_area and a.i_customer=c.i_customer
					 ) as x
					 where x.i_area = '$iarea'
					 group by x.i_area) a 
					 inner join tr_area b on (a.i_area = b.i_area)
					 order by i_area",false);

			}
		}	
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($icustomer,$iperiode,$num,$offset)
    {
      $this->db->select(" x.* from(
		/* Penjualan */
		select 'Penjualan' as status, a.i_customer, b.e_customer_name, a.i_area, a.i_nota, a.d_nota, a.v_nota_netto as debet, 0 as kredit from
		tm_nota a, tr_customer b
		where
		a.i_customer = b.i_customer
		and to_char(a.d_nota,'yyyymm')='$iperiode'
		and a.f_nota_cancel = 'f'
		and a.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_kbank = b.i_kbank
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet,  a.v_jumlah  from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, 0 as debet, a.v_jumlah from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Pembulatan' as status, a.i_customer, c.e_customer_name, a.i_area, p.i_refference, p.d_mutasi, 0 as debet, p.v_mutasi_debet as v_jumlah from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
		where to_char(p.d_mutasi,'yyyymm')='$iperiode' and not a.i_nota isnull
		and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
		and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
		and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
		and a.i_spb=h.i_spb and a.i_area=h.i_area  and area.i_area=a.i_area  and a.i_customer=c.i_customer
		and a.i_customer = '$icustomer'
		) as x order by x.d_nota asc",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
