<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($iperiode,$cari)
    {
		  $this->db->select(" a.i_area, c.e_area_name, a.i_salesman, b.e_salesman_name, a.n_jumlah_kunjungan, a.n_jumlah_order,
                          ((a.n_jumlah_order/a.n_jumlah_kunjungan)*100) as hasil,
                          trunc(a.n_jumlah_kunjungan/a.n_jumlah_hari) as rata,
                          (a.n_jumlah_order/a.n_jumlah_hari) as efektif
                          from tm_kunjungan a, tr_salesman b, tr_area c
                          where a.i_salesman=b.i_salesman
                          and a.i_area=c.i_area and a.i_periode='$iperiode'
                          and (upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
                          order by a.i_area, a.i_salesman",false);//->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cari($iperiode,$cari)
    {
		  $this->db->select(" a.i_area, c.e_area_name, a.i_salesman, b.e_salesman_name, a.n_jumlah_kunjungan, a.n_jumlah_order,
                          ((a.n_jumlah_order/a.n_jumlah_kunjungan)*100) as hasil,
                          trunc(a.n_jumlah_kunjungan/a.n_jumlah_hari) as rata,
                          (a.n_jumlah_order/a.n_jumlah_hari) as efektif
                          from tm_kunjungan a, tr_salesman b, tr_area c
                          where a.i_salesman=b.i_salesman
                          and a.i_area=c.i_area and a.i_periode='$iperiode'
                          and (upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
                          order by a.i_area, a.i_salesman",FALSE);//->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetailkunjungan($iperiode,$isalesman)
    {
		  $this->db->select(" a.d_rrkh, a.i_customer, b.e_customer_name, c.e_kunjungan_typename, e.e_customer_classname,
                          a.f_kunjungan_realisasi, a.i_area, a.i_salesman, d.e_salesman_name, a.e_remark
                          from tm_rrkh_item a, tr_customer b, tr_kunjungan_type c, tr_salesman d, tr_customer_class e
                          where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_kunjungan_type=c.i_kunjungan_type
                          and a.i_salesman=d.i_salesman and b.i_customer_class=e.i_customer_class
                          and to_char(a.d_rrkh,'yyyymm')='$iperiode' and a.i_salesman='$isalesman'",FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
