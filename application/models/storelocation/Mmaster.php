<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($istorelocation,$istore,$istorelocationbin)
    {
		$this->db->select("a.*, b.e_store_name from tr_store_location a, tr_store b where i_store_location = '$istorelocation' and b.i_store = '$istore' and i_store_locationbin = '$istorelocationbin' and  a.i_store=b.i_store", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }

    function insert($istorelocation, $estorelocationname, $istore, $istorelocationbin)
    {
		$this->db->query("insert into tr_store_location (i_store_location, e_store_locationname, i_store, i_store_locationbin) values ('$istorelocation', '$estorelocationname','$istore','$istorelocationbin')");
		redirect('storelocation/cform/');
    }

    function update($istorelocation, $estorelocationname, $istore, $istorelocationbin)
    {
		$this->db->query("update tr_store_location set e_store_locationname = '$estorelocationname'
		where i_store_location = '$istorelocation' and i_store = '$istore' 
		  and i_store_locationbin='$istorelocationbin' ");
		redirect('storelocation/cform/');
    }
	
    public function delete($istorelocation,$istore,$istorelocationbin) 
    {
		$this->db->query('DELETE FROM tr_store_location WHERE i_store_location=\''.$istorelocation.'\' and i_store=\''.$istore.'\' and i_store_locationbin=\''.$istorelocationbin.'\'');
		return TRUE;
    }
    
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select("a.*, b.e_store_name 
				   from tr_store_location a, tr_store b 
				   where a.i_store=b.i_store 
					 and (upper(b.e_store_name) like '%$cari%' or upper(a.i_store_location) like '%$cari%' 
					  or upper(a.e_store_locationname) like '%$cari%') 
				   order by a.i_store_location ",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    
    function bacastore($num,$offset)
    {
		$this->db->select("i_store, e_store_name from tr_store order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }

    function cari($cari,$num,$offset)
    {
		$this->db->select("a.i_store_location, a.i_store_locationbin, a.e_store_locationname, a.i_store, b.e_store_name 
				   from tr_store_location a, tr_store b where (upper(a.e_store_locationname) like '%$cari%' 
					 or upper(a.i_store_location) like '%$cari%' or upper(b.e_store_name) like '%$cari%') and a.i_store=b.i_store 
				   order by a.i_store_location",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caristore($cari,$num,$offset)
    {
		$this->db->select("i_store, e_store_name from tr_store where upper(e_store_name) like '%$cari%' or upper(i_store) like '%$cari%' order by i_store",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
