<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
   function bacaarea($num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$allarea,$iuser)
    {
      if($allarea=='t'){
         $this->db->select("* from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }
      else
      {
         $this->db->select("* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false)->limit($num,$offset);
      }

      $query = $this->db->get();

      if ($query->num_rows() > 0){
         return $query->result();
      }
    }

    function bacaperiodeOMI($dfrom,$dto,$iarea)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
        $th=$tmp[2];				
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
			}
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
								else $sql.=" order by a.i_nota";
        
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaperiodeAIM($dfrom,$dto,$iarea)
    {
      $aim = $this->load->database('dbaim',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $aim->select($sql,false);
      $query = $aim->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeBINABA($dfrom,$dto,$iarea)
    {
      $binaba = $this->load->database('dbbinaba',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $binaba->select($sql,false);
      $query = $binaba->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeBCL($dfrom,$dto,$iarea)
    {
      $bcl = $this->load->database('dbbcl',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $bcl->select($sql,false);
      $query = $bcl->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeKPU($dfrom,$dto,$iarea)
    {
      $kpu = $this->load->database('dbkpu',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $kpu->select($sql,false);
      $query = $kpu->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodePKA($dfrom,$dto,$iarea)
    {
      $pka = $this->load->database('dbpka',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $pka->select($sql,false);
      $query = $pka->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeCKN($dfrom,$dto,$iarea)
    {
      $ckn = $this->load->database('dbckn',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $ckn->select($sql,false);
      $query = $ckn->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeCVKAB($dfrom,$dto,$iarea)
    {
      $cvkab = $this->load->database('dbcvkab',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $cvkab->select($sql,false);
      $query = $cvkab->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function bacaperiodeBTN($dfrom,$dto,$iarea)
    {
      $btn = $this->load->database('dbbtn',TRUE);
      if($dfrom!=''){
        $tmp=explode("-",$dfrom);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
      if($dto!=''){
        $tmp=explode("-",$dto);
        $th=$tmp[2];        
        $bl=$tmp[1];
        $dt=$tmp[0];
        $tgl=$th.'-'.$bl.'-'.$dt;
      }
        $sql =" a.i_nota, f.e_area_name, to_char(d_nota,'dd-mm-yyyy') as d_nota, to_char(d_jatuh_tempo,'dd-mm-yyyy') as d_jatuh_tempo, 
                a.i_sj, to_char(d_sj,'dd-mm-yyyy') as d_sj, a.i_customer, b.e_customer_name, b.n_customer_toplength, c.e_city_name, a.i_salesman, d.e_salesman_name, 
                a.v_nota_gross, v_nota_discounttotal, v_nota_netto from tm_nota a
                left join tr_customer b on(a.i_customer = b.i_customer and a.i_area = b.i_area)
                left join tr_city c on(c.i_city = b.i_city  and c.i_area = b.i_area)
                left join tr_salesman d on(a.i_salesman = d.i_salesman  and a.i_area = d.i_area)
                left join tr_area f on(a.i_area = f.i_area)
                where a.d_nota >= to_date('$dfrom', 'dd-mm-yyyy') and a.d_nota <= to_date('$dto', 'dd-mm-yyyy')
                AND a.f_nota_cancel='f'
                ";
        if($iarea!='NA') $sql.="and a.i_area ='$iarea' order by a.i_nota";
                else $sql.=" order by a.i_nota";
        
      $btn->select($sql,false);
      $query = $btn->get();
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

    function interval($dfrom,$dto)
    {
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
		  $this->db->select("(DATE_PART('year', '$dto'::date) - DATE_PART('year', '$dfrom'::date)) * 12 +
                         (DATE_PART('month', '$dto'::date) - DATE_PART('month', '$dfrom'::date)) as inter ",false);
		  $query = $this->db->get();
		  if($query->num_rows() > 0){
			  $tmp=$query->row();
        return $tmp->inter+1;
		  }
    }
}
?>
