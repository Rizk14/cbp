<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacaperiode($iperiode)
    {
        $sql=" i_nota, d_nota, d_jatuh_tempo,i_customer, e_customer_name, i_faktur_komersial, i_seri_pajak, d_pajak, round (v_nota_netto/1.1) as v_dpp,
	              v_nota_gross, v_nota_discount, v_nota_netto, (v_nota_netto-round (v_nota_netto/1.1)) as v_nota_ppn, a.i_area, b.e_area_name 
	              from tm_nota a left join tr_area b using (i_area) left join tr_customer c using (i_customer)
	              where a.f_nota_cancel='f' and to_char(d_nota,'yyyymm')='$iperiode' order by a.i_nota";
		  $this->db->select($sql,false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
