<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($inota,$ispb,$iarea) 
    {
			$this->db->query("update tm_nota set f_nota_cancel='t' where i_nota='$inota' and i_area='$iarea'");
    }
    function bacasemua($cari, $num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						or upper(a.i_spb) like '%$cari%' 
						or upper(a.i_customer) like '%$cari%' 
						or upper(b.e_customer_name) like '%$cari%')
						order by a.i_nota desc",false)->limit($num,$offset);
		}else{
			$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
						where a.i_customer=b.i_customer 
						and a.f_ttb_tolak='f'
						and not a.i_nota isnull
						and (upper(a.i_nota) like '%$cari%' 
						  or upper(a.i_spb) like '%$cari%' 
						  or upper(a.i_customer) like '%$cari%' 
						  or upper(b.e_customer_name) like '%$cari%')
						and (a.i_area='$area1' 
						or a.i_area='$area2' 
						or a.i_area='$area3' 
						or a.i_area='$area4' 
						or a.i_area='$area5')
						order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$area1= $this->session->userdata('i_area');
		$area2= $this->session->userdata('i_area2');
		$area3= $this->session->userdata('i_area3');
		$area4= $this->session->userdata('i_area4');
		$area5= $this->session->userdata('i_area5');
		$allarea= $this->session->userdata('allarea');
		if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
		{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					or upper(a.i_spb) like '%$cari%' 
					or upper(a.i_customer) like '%$cari%' 
					or upper(b.e_customer_name) like '%$cari%')
					order by a.i_nota desc",FALSE)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_customer_name from tm_nota a, tr_customer b
					where a.i_customer=b.i_customer 
					and a.f_ttb_tolak='f'
					and not a.i_nota isnull
					and (upper(a.i_nota) like '%$cari%' 
					  or upper(a.i_spb) like '%$cari%' 
					  or upper(a.i_customer) like '%$cari%' 
					  or upper(b.e_customer_name) like '%$cari%')
					and (a.i_area='$area1' 
					or a.i_area='$area2' 
					or a.i_area='$area3'
					or a.i_area='$area4' 
					or a.i_area='$area5')
					order by a.i_nota desc",false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
    }else{
		  $this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
    if($area1=='00'){
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area ", FALSE)->limit($num,$offset);
    }else{
		  $this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
						     and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
						     or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
    }
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaperiode($iperiode)
    {
      $xy=$this->db->query("select * from tm_hpp where e_periode='$iperiode' order by e_product_name, i_product");
      if ($xy->num_rows() > 0){
        return $xy->result();
      }
    }
    function bacadetail($icustomer,$iperiode,$num,$offset)
    {
      $this->db->select(" * from(
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_nota as tglbukti, a.i_nota as bukti, '' as jenis, '1. penjualan' as keterangan, a.v_nota_netto as debet, 
                          0 as kredit, 0 as dn, 0 as kn
                          from tm_nota a, tr_customer_groupar b, tr_customer c
                          where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='$iperiode'
                          and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'kredit nota' as keterangan, 0 as debet, 0 as kredit, 0 as dn, 
                          v_netto as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_bukti as tglbukti, a.i_pelunasan as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 
                          0 as debet, a.v_jumlah as kredit, 0 as dn, 0 as kn
                          from tm_pelunasan a, tr_customer_groupar b, tr_customer c, tr_jenis_bayar d
                          where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and to_char(d_bukti,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar 
                          and b.i_customer_groupar='$icustomer'
                          union all
                          select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                          a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'debet nota' as keterangan, 0 as debet, 0 as kredit, v_netto as dn, 
                          0 as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                          where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                          and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                          ) as a order by area, groupar, tglbukti, keterangan, bukti",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
