<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($cari, $num,$offset)
    {
		$this->db->select(" a.*, b.e_area_name from tm_general_jurnal a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_jurnal) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='t'
							order by a.i_jurnal desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cari($cari,$num,$offset)
    {
		$this->db->select(" sa.*, b.e_area_name from tm_general_jurnal a, tr_area b
							where (upper(a.i_area) like '%$cari%' or upper(a.i_jurnal) like '%$cari%')
							and a.i_area=b.i_area and a.f_posting='t'
							order by a.i_jurnal desc",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function baca($ijurnal)
    {
		$this->db->select(" * from tm_general_jurnal 
				   inner join tr_area on (tm_general_jurnal.i_area=tr_area.i_area)
				   where tm_general_jurnal.i_jurnal='$ijurnal'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ijurnal)
    {
		$this->db->select("	* from tm_general_jurnalitem 
							inner join tr_coa on(tm_general_jurnalitem.i_coa=tr_coa.i_coa)				
						   	where tm_general_jurnalitem.i_jurnal='$ijurnal'
						   	order by tm_general_jurnalitem.f_debet desc", false);//and i_supplier='$isupplier' 
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function namaacc($icoa)
    {
		$this->db->select(" e_coa_name from tr_coa where i_coa='$icoa' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $tmp)			
			{
				$xxx=$tmp->e_coa_name;
			}
			return $xxx;
		}
    }
	function carisaldo($icoa,$iperiode)
	{
		$query = $this->db->query("select * from tm_coa_saldo where i_coa='$icoa' and i_periode='$iperiode'");
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			return $row;
		}	
	}
	function deletetransheader(	$ijurnal,$iarea,$djurnal )
    {
		$this->db->query("delete from tm_jurnal_transharian where i_refference='$ijurnal' and i_area='$iarea' and d_refference='$djurnal'");
	}
	function deletetransitemdebet($acc,$ijurnal,$djurnal)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$acc' and i_refference='$ijurnal' and d_refference='$djurnal'");
	}
	function deletetransitemkredit($acc,$ijurnal,$djurnal)
    {
		$this->db->query("delete from tm_jurnal_transharianitem where i_coa='$acc' and i_refference='$ijurnal' and d_refference='$djurnal'");
	}
	function deletegldebet($acc,$ijurnal,$djurnal)
    {
		$this->db->query("delete from tm_general_ledger where i_refference='$ijurnal' and i_coa='$acc' and d_refference='$djurnal'");
	}
	function deleteglkredit($acc,$ijurnal,$djurnal)
    {
		$this->db->query("delete from tm_general_ledger where i_refference='$ijurnal' and i_coa='$acc' and d_refference='$djurnal'");
	}
	function updatejurnal($ijurnal,$iarea)
    {
		$this->db->query("update tm_general_jurnal set f_posting='f' where i_jurnal='$ijurnal' and i_area='$iarea'");
	}
	function updatesaldodebet($accdebet,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_debet=v_mutasi_debet-$vjumlah, v_saldo_akhir=v_saldo_akhir-$vjumlah
						  where i_coa='$accdebet' and i_periode='$iperiode'");
	}
	function updatesaldokredit($acckredit,$iperiode,$vjumlah)
	{
		$this->db->query("update tm_coa_saldo set v_mutasi_kredit=v_mutasi_kredit-$vjumlah, v_saldo_akhir=v_saldo_akhir+$vjumlah
						  where i_coa='$acckredit' and i_periode='$iperiode'");
	}
}
?>
