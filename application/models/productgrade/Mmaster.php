<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($iproductgrade)
    {
		$this->db->select('i_product_grade, e_product_gradename')->from('tr_product_grade')->where('i_product_grade', $iproductgrade);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iproductgrade, $eproductgradename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dentry= $row->c;
		$this->db->set(
			array(
				'i_product_grade' => $iproductgrade,
				'e_product_gradename' => $eproductgradename,
			'd_product_gradeentry' => $dentry
			)
		);
		
		$this->db->insert('tr_product_grade');
		redirect('productgrade/cform/');
    }
    function update($iproductgrade, $eproductgradename)
    {
		$query = $this->db->query("SELECT current_timestamp as c");
		$row   = $query->row();
		$dupdate= $row->c;
			$data = array(
		           'i_product_grade' => $iproductgrade,
		           'e_product_gradename' => $eproductgradename,
			   'd_product_gradeupdate' => $dupdate

		        );
		$this->db->where('i_product_grade', $iproductgrade);
		$this->db->update('tr_product_grade', $data); 
		redirect('productgrade/cform/');
    }
	
    public function delete($iproductgrade) 
    {
		$this->db->query('DELETE FROM tr_product_grade WHERE i_product_grade=\''.$iproductgrade.'\'');
		return TRUE;
    }
    function bacasemua()
    {
		$this->db->select('i_product_grade, e_product_gradename from tr_product_grade order by i_product_grade',false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
