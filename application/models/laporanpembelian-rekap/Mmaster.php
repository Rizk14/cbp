<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($dfrom, $dto)
    {
      $dfrom=substr($dfrom,6,4).'-'.substr($dfrom,3,2).'-'.substr($dfrom,0,2);
      $dto  =substr($dto,6,4).'-'.substr($dto,3,2).'-'.substr($dto,0,2);
      $iperiode=substr($dto,6,4).substr($dto,3,2);
      $this->db->select(" data.i_supplier as supplier, sum(data.totalop) as op, sum(data.v_netto) as netto, sum(data.v_discount) as diskon, 
                          sum(data.v_gross) kotor
						              from(
						              select a.i_supplier, 0 as v_netto, 0 AS v_gross, 0 as totalop, 0 v_discount  
						              from tr_supplier a
						              union all
						              select  a.i_supplier, c.v_netto, c.v_gross, sum(b.v_product_mill*b.n_order) as totalop, c.v_discount 
						              FROM tm_op_item b
						              left join tm_op a using(i_op)
						              left join tm_dtap_item c on b.i_op=c.i_op
						              WHERE a.f_op_cancel = false and a.d_op>='$dfrom' and a.d_op<='$dto'
						              group by a.i_supplier,v_netto, b.v_product_mill,b.n_order, v_discount, v_gross
						              ) as data
						              group by i_supplier
						              order by i_supplier",false);
/*
		  $this->db->select("	c.e_area_name, sum(v_target) as v_target, b.i_area, sum(b.v_nota_netto) as v_nota_netto, 
                          sum(b.v_nota_gross) as v_nota_gross, 
                          sum(b.v_nota_grossinsentif) as v_nota_grossinsentif, sum(b.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(b.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(b.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(b.v_nota_reguler) as v_nota_reguler, sum(b.v_nota_baby) as v_nota_baby, 
                          sum(b.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(b.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(b.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(b.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(b.v_spb_gross) as v_spb_gross, 
                          sum(b.v_spb_netto) as v_spb_netto, sum(b.v_retur_insentif) as v_retur_insentif, 
                          sum(b.v_retur_noninsentif) as v_retur_noninsentif
                          from(
                          select 0 as v_target, i_area, sum(a.v_nota_netto) as v_nota_netto, sum(a.v_nota_gross) as v_nota_gross, 
                          sum(a.v_nota_grossinsentif) as v_nota_grossinsentif, sum(a.v_nota_nettoinsentif) as v_nota_nettoinsentif, 
                          sum(a.v_nota_grossnoninsentif) as v_nota_grossnoninsentif, 
                          sum(a.v_nota_nettononinsentif) as v_nota_nettononinsentif,
                          sum(a.v_nota_reguler) as v_nota_reguler, sum(a.v_nota_baby) as v_nota_baby, 
                          sum(a.v_nota_babyinsentif) as v_nota_babyinsentif,
                          sum(a.v_nota_babynoninsentif) as v_nota_babynoninsentif, sum(a.v_nota_regulerinsentif) as v_nota_regulerinsentif, 
                          sum(a.v_nota_regulernoninsentif) as v_nota_regulernoninsentif,sum(a.v_spb_gross) as v_spb_gross, 
                          sum(a.v_spb_netto) as v_spb_netto, sum(a.v_retur_insentif) as v_retur_insentif, 
                          sum(a.v_retur_noninsentif) as v_retur_noninsentif
                          from (
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, sum(v_spb) as v_spb_gross, sum(v_spb)-sum(v_spbdiscount) as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where d_docspb>='$dfrom' and d_docspb<='$dto'
                          
                          group by i_area
                          union all
                          select i_area, sum(v_netto) as v_nota_netto, sum(v_gross) as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where d_doc>='$dfrom' and d_doc<='$dto'
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_docspb>='$dfrom' and d_docspb<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, sum(v_gross) as v_nota_grossinsentif, 
                          sum(v_netto) as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_doc>='$dfrom' and d_doc<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 
                          0 as v_nota_nettoinsentif, 0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 
                          0 as v_nota_reguler, 0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 
                          0 as v_nota_regulerinsentif, 0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          sum(v_kn) as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='t' and (d_kn>='$dfrom' and d_kn<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          sum(v_gross) as v_nota_grossnoninsentif, sum(v_netto) as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, 0 as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and (d_doc>='$dfrom' and d_doc<='$dto')
                          group by i_area
                          union all
                          select i_area, 0 as v_nota_netto, 0 as v_nota_gross, 0 as v_nota_grossinsentif, 0 as v_nota_nettoinsentif, 
                          0 as v_nota_grossnoninsentif, 0 as v_nota_nettononinsentif, 0 as v_nota_reguler,
                          0 as v_nota_baby, 0 as v_nota_babyinsentif, 0 as v_nota_babynoninsentif, 0 as v_nota_regulerinsentif, 
                          0 as v_nota_regulernoninsentif, 0 as v_spb_gross, 0 as v_spb_netto,
                          0 as v_retur_insentif, sum(v_kn) as v_retur_noninsentif
                          from vpenjualan where f_insentif='f' and (d_kn>='$dfrom' and d_kn<='$dto')
                          group by i_area
                          ) as a
                          group by i_area
                          ) as b, tr_area c
                          where b.i_area=c.i_area
                          group by b.i_area, c.e_area_name
                          order by b.i_area",false);
*/
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select(" * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' order by i_area",false)->limit($num,$offset);
			}else{
				$this->db->select(" * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area",false)->limit($num,$offset);
			}
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
