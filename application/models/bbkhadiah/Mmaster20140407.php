<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($ibbk)
    {
		$this->db->select(" a.*, b.e_customer_name from tm_bbk a, tr_customer b
					              where a.i_supplier=b.i_customer
					              and i_bbk ='$ibbk' and a.i_bbk_type='03'", false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function bacadetail($ispmb)
    {
			$this->db->select(" a.*, b.e_product_motifname from tm_bbk_item a, tr_product_motif b
						 where a.i_bbk = '$ispmb' and i_bbk_type='03' and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						 order by a.n_item_no ", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function updateheader($ibbk, $dbbk, $icustomer, $eremark)
    {
    	$this->db->set(
    		array(
			'd_bbk'	    => $dbbk,
			'i_supplier'=> $icustomer,
      'e_remark'  => $eremark
    		)
    	);
    	$this->db->where('i_bbk',$ibbk);
    	$this->db->where('i_bbk_type','03');
    	$this->db->update('tm_bbk');
    }
    function insertheader($ibbk, $ibbktype, $dbbk, $icustomer, $ibbkold, $eremark)
    {
    	$this->db->set(
    		array(
			    'i_bbk'		              => $ibbk,
          'i_bbk_type'            => $ibbktype,
			    'd_bbk'		              => $dbbk,
			    'i_area'    	          => '00',#substr($icustomer,0,2),
			    'i_supplier' 	          => $icustomer,
          'e_remark'              => $eremark,
          'i_refference_document' => 'HADIAH'
    		)
    	);
    	$this->db->insert('tm_bbk');
    }
    function insertdetail($ibbk,$ibbktype,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl)
    {
    	$this->db->set(
    		array(
					'i_bbk'	   	            => $ibbk,
          'i_bbk_type'            => $ibbktype,
					'i_product'	 	          => $iproduct,
					'i_product_grade'	      => $iproductgrade,
					'i_product_motif'	      => $iproductmotif,
					'n_quantity'		        => $nquantity,
					'v_unit_price'		      => $vunitprice,
					'e_product_name'	      => $eproductname,
					'e_remark'		          => $eremark,
          'i_refference_document' => 'HADIAH',
          'e_mutasi_periode'      => $thbl,
          'n_item_no'             => $i
    		)
    	);
    	
    	$this->db->insert('tm_bbk_item');
    }
    public function deletedetail($iproduct, $iproductgrade, $ibbk, $iproductmotif, $nquantityx, $istore, $istorelocation, $istorelocationbin)
    {
		  $this->db->query("DELETE FROM tm_bbk_item WHERE i_bbk='$ibbk' and i_product='$iproduct' and i_product_grade='$iproductgrade' 
						and i_product_motif='$iproductmotif' and i_bbk_type='03'");
/*
      $this->db->query("update tm_ic set n_quantity_stock=n_quantity_stock+$nquantityx
						    where i_product='$iproduct' and i_product_motif='$iproductmotif'
						    and i_product_grade='$iproductgrade' and i_store='$istore'
						    and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'");
*/
    }
	
    public function delete($ispmb) 
    {
#		  $this->db->query('DELETE FROM tm_spmb WHERE i_spmb=\''.$ispmb.'\'');
#		  $this->db->query('DELETE FROM tm_spmb_item WHERE i_spmb=\''.$ispmb.'\'');
    }
    function bacasemua()
    {
		$this->db->select("* from tm_spmb order by i_spmb desc",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function bacaproduct($num,$offset,$cari)
    {
		  if($offset=='')
			  $offset=0;
		  $query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif, 
						                    c.e_product_name as nama, c.v_product_mill as harga
						                    from tr_product_motif a,tr_product c
						                    where a.i_product=c.i_product and (upper(a.i_product) like '%$cari%' or 
						                    upper(c.e_product_name) like '%$cari%')
                                order by c.i_product, a.e_product_motifname
                                limit $num offset $offset",false);
  #c.v_product_mill as harga
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function runningnumber($thbl){
      $th	= substr($thbl,0,4);
      $asal=$thbl;
      $thbl=substr($thbl,2,2).substr($thbl,4,2);
		  $this->db->select(" n_modul_no as max from tm_dgu_no 
                          where i_modul='BBH'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th' for update", false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  foreach($query->result() as $row){
			    $terakhir=$row->max;
			  }
			  $nobbk  =$terakhir+1;
        $this->db->query("update tm_dgu_no 
                          set n_modul_no=$nobbk
                          where i_modul='BBH'
                          and i_area='00'
                          and substring(e_periode,1,4)='$th'", false);
			  settype($nobbk,"string");
			  $a=strlen($nobbk);
			  while($a<4){
			    $nobbk="0".$nobbk;
			    $a=strlen($nobbk);
			  }
			  	$nobbk  ="BBK-".$thbl."-KH".$nobbk;
			  return $nobbk;
		  }else{
			  $nobbk  ="KH0001";
		  	$nobbk  ="BBK-".$thbl."-".$nobbk;
        $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no) 
                           values ('BBH','00','$asal',1)");
			  return $nobbk;
		  }
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" * from tm_spmb where upper(i_spmb) like '%$cari%' 
					order by i_spmb",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cariproduct($cari,$num,$offset)
    {
		if($offset=='')
			$offset=0;
		$query=$this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
								a.e_product_motifname as namamotif, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
							   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')
								order by a.e_product_motifname asc limit $num offset $offset",false);
# c.v_product_mill as harga
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacacustomer($num,$offset,$cari)
    {
			$this->db->select(" * from tr_customer where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                          order by i_customer", false)->limit($num,$offset);			
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_awal, n_quantity_akhir, n_quantity_in, n_quantity_out 
                                from tm_ic_trans
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                order by i_trans desc",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $query=$this->db->query(" SELECT n_quantity_stock
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$qbbk,$q_aw,$q_ak)
    {
      $query 	= $this->db->query("SELECT current_timestamp as c");
	    $row   	= $query->row();
	    $now	  = $row->c;
      $query=$this->db->query(" 
                                INSERT INTO tm_ic_trans
                                (
                                  i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                  i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                  n_quantity_in, n_quantity_out,
                                  n_quantity_akhir, n_quantity_awal)
                                VALUES 
                                (
                                  '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                  '$eproductname', '$ibbk', '$now', 0, $qbbk, $q_ak-$qbbk, $q_ak
                                )
                              ",false);
    }
    function cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_mutasi
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi 
                                set n_mutasi_bbk=n_mutasi_bbk+$qbbk, n_saldo_akhir=n_saldo_akhir-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                insert into tm_mutasi 
                                (
                                  i_product,i_product_motif,i_product_grade,i_store,i_store_location,i_store_locationbin,
                                  e_mutasi_periode,n_saldo_awal,n_mutasi_pembelian,n_mutasi_returoutlet,n_mutasi_bbm,n_mutasi_penjualan,
                    			        n_mutasi_returpabrik,n_mutasi_bbk,n_saldo_akhir,n_saldo_stockopname,f_mutasi_close)
                                values
                                (
                                  '$iproduct','$iproductmotif','$iproductgrade','AA','01','00','$emutasiperiode',0,0,0,0,0,0,$qbbk,$qbbk,0,'f')
                              ",false);
    }
    function cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)
    {
      $ada=false;
      $query=$this->db->query(" SELECT i_product
                                from tm_ic
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
      if ($query->num_rows() > 0){
				$ada=true;
			}
      return $ada;
    }
    function updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$q_ak)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock-$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
    function inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$qbbk)
    {
      $query=$this->db->query(" 
                                insert into tm_ic 
                                values
                                (
                                  '$iproduct', '$iproductmotif', '$iproductgrade', '$istore', '$istorelocation', '$istorelocationbin', '$eproductname', 0, 't'
                                )
                              ",false);
    }
    function deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibbk,$ntmp,$eproductname)
    {
      $queri 		= $this->db->query("SELECT n_quantity_akhir, i_trans FROM tm_ic_trans 
                                    where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                    and i_store='$istore' and i_store_location='$istorelocation'
                                    and i_store_locationbin='$istorelocationbin'
                                    order by i_trans desc",false);
#and i_refference_document='$ibbk'
      if ($queri->num_rows() > 0){
    	  $row   		= $queri->row();
        $que 	= $this->db->query("SELECT current_timestamp as c");
	      $ro 	= $que->row();
	      $now	 = $ro->c;
        if($ntmp!=0 || $ntmp!=''){
          $query=$this->db->query(" 
                                  INSERT INTO tm_ic_trans
                                  (
                                    i_product, i_product_grade, i_product_motif, i_store, i_store_location, 
                                    i_store_locationbin, e_product_name, i_refference_document, d_transaction, 
                                    n_quantity_in, n_quantity_out,
                                    n_quantity_akhir, n_quantity_awal)
                                  VALUES 
                                  (
                                    '$iproduct','$iproductgrade','$iproductmotif','$istore','$istorelocation','$istorelocationbin', 
                                    '$eproductname', '$ibbk', '$now', $ntmp, 0, $row->n_quantity_akhir+$ntmp, $row->n_quantity_akhir
                                  )
                                ",false);
        }
      }
      if(isset($row->i_trans)){
        if($row->i_trans!=''){
          return $row->i_trans;
        }else{
          return 1;
        }
      }else{
        return 1;
      }
    }
    function updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk,$emutasiperiode)
    {
      $query=$this->db->query(" 
                                UPDATE tm_mutasi set n_mutasi_bbk=n_mutasi_bbk-$qbbk, n_saldo_akhir=n_saldo_akhir+$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                                and e_mutasi_periode='$emutasiperiode'
                              ",false);
    }
    function updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$qbbk)
    {
      $query=$this->db->query(" 
                                UPDATE tm_ic set n_quantity_stock=n_quantity_stock+$qbbk
                                where i_product='$iproduct' and i_product_grade='$iproductgrade' and i_product_motif='$iproductmotif'
                                and i_store='$istore' and i_store_location='$istorelocation' and i_store_locationbin='$istorelocationbin'
                              ",false);
    }
}
?>
