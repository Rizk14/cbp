<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function baca($igiro,$iarea)
    {
		$this->db->select(" * from tm_giro a
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							where a.i_giro='$igiro' and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->row();
		}
    }
    function insert($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dentry	= $row->c;
      if($dpajak!=''){
      	$this->db->set(
      		array(
				  'i_area'			  => $iarea,
				  'i_kn'				  => $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 				  => $dkn,
				  'd_refference'	=> $drefference,
				  'i_pajak' 			=> $ipajak,
				  'd_pajak' 			=> $dpajak,
				  'd_entry' 			=> $dentry,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			  => $vsisa
      		)
      	);
      }else{
      	$this->db->set(
      		array(
				  'i_area'		  	=> $iarea,
				  'i_kn'		  		=> $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 				  => $dkn,
				  'd_refference'	=> $drefference,
				  'd_entry' 			=> $dentry,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'			  => $vsisa

      		)
      	);
      }
    	$this->db->insert('tm_kn');
    }
    function update($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
					$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak)
    {
		$query 	= $this->db->query("SELECT current_timestamp as c");
		$row   	= $query->row();
		$dupdate	= $row->c;
      if($dpajak!=''){
      	$this->db->set(
      		array(
				  'i_area'				=> $iarea,
				  'i_kn'					=> $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 					=> $dkn,
   				'i_pajak' 			=> $ipajak,
				  'd_pajak' 			=> $dpajak,
				  'd_refference'	=> $drefference,
				  'd_update' 			=> $dupdate,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'				=> $vsisa,
				  'f_kn_cancel'		=> 'f'

      		)
      	);
      }else{
    	  $this->db->set(
      		array(
				  'i_area'				=> $iarea,
				  'i_kn'					=> $ikn,
				  'i_customer' 		=> $icustomer,
				  'i_refference' 	=> $irefference,
				  'i_customer_groupar'=> $icustomergroupar,
				  'i_salesman' 		=> $isalesman,
				  'i_kn_type' 		=> $ikntype,
				  'd_kn' 					=> $dkn,
				  'd_refference'	=> $drefference,
				  'd_update' 			=> $dupdate,
				  'e_remark' 			=> $eremark,
				  'f_cetak' 			=> $fcetak,
				  'f_masalah' 		=> $fmasalah,
				  'f_insentif' 		=> $finsentif,
				  'n_kn_year' 		=> $nknyear,
				  'v_netto' 			=> $vnetto,
				  'v_gross' 			=> $vgross,
				  'v_discount' 		=> $vdiscount,
				  'v_sisa'				=> $vsisa,
				  'f_kn_cancel'		=> 'f'

      		)
      	);
      }
    	$this->db->where("i_kn",$ikn);
    	$this->db->where("n_kn_year",$nknyear);
    	$this->db->where("i_area",$iarea);
    	$this->db->update('tm_kn');
    }
    public function delete($igiro,$iarea) 
    {
			$this->db->query("DELETE FROM tm_giro WHERE i_giro='$igiro' and i_area='$iarea'");
    }
    function bacasemua()
    {
		$this->db->select(' * from tm_giro a 
							inner join tr_area on(a.i_area=tr_area.i_area)
							inner join tr_customer on(a.i_customer=tr_customer.i_customer)
							order by a.i_area,a.i_giro', false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
   function bacaarea($num,$offset,$iuser) {
        $sql = "* from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area";
      $this->db->select($sql, false)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
    }
   function cariarea($cari,$num,$offset,$iuser)
      {
      $sql = "* from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' and 
              i_area in ( select i_area from tm_user_area where i_user='$iuser') )";
      $this->db->select($sql, FALSE)->limit($num,$offset);
      $query = $this->db->get();
      if ($query->num_rows() > 0){
         return $query->result();
      }
   }
    function bacabbm($iarea,$num,$offset)
    {
		$this->db->select(" a.i_bbm, a.i_area, a.i_salesman, d.e_salesman_name, c.e_area_name, b.i_customer, e.e_customer_name, e.e_customer_address,
					              b.n_ttb_discount1, b.n_ttb_discount2, b.n_ttb_discount3, b.v_ttb_discount1, b.v_ttb_discount2, b.i_ttb, b.d_ttb,
                        b.v_ttb_discount3, b.v_ttb_gross, b.v_ttb_discounttotal, b.v_ttb_netto, b.v_ttb_sisa, a.d_bbm, a.i_refference_document,
                        a.d_refference_document, f.i_customer_groupar
					from tm_bbm a
					inner join tr_salesman d on (a.i_salesman=d.i_salesman)
					inner join tm_ttbretur b on (a.i_bbm=b.i_bbm and a.i_area=b.i_area)
					inner join tr_area c on (a.i_area=c.i_area)
					left join tr_customer e on (b.i_customer=e.i_customer)
					left join tr_customer_groupar f on (e.i_customer=f.i_customer)
					where a.i_bbm_type='05' and a.i_area='$iarea' and a.i_bbm like 'BBM%'
          and not a.i_bbm in(select i_refference from tm_kn where i_area='$iarea' and i_refference like 'BBM%' and f_kn_cancel='f')
					order by a.i_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function caribbm($cari,$iarea,$num,$offset)
    {
		$this->db->select(" a.i_bbm, a.i_area, a.i_salesman, d.e_salesman_name, c.e_area_name, b.i_customer, e.e_customer_name, e.e_customer_address,
					              b.n_ttb_discount1, b.n_ttb_discount2, b.n_ttb_discount3, b.v_ttb_discount1, b.v_ttb_discount2, b.i_ttb, b.d_ttb,
                        b.v_ttb_discount3, b.v_ttb_gross, b.v_ttb_discounttotal, b.v_ttb_netto, b.v_ttb_sisa, a.d_bbm, a.i_refference_document,
                        a.d_refference_document, f.i_customer_groupar
					from tm_bbm a
					inner join tr_salesman d on (a.i_salesman=d.i_salesman)
					inner join tm_ttbretur b on (a.i_bbm=b.i_bbm and a.i_area=b.i_area)
					inner join tr_area c on (a.i_area=c.i_area)
					left join tr_customer e on (b.i_customer=e.i_customer)
					left join tr_customer_groupar f on (e.i_customer=f.i_customer)
					where a.i_bbm_type='05' and a.i_area='$iarea' and a.i_bbm like 'BBM%'
          and not a.i_bbm in(select i_refference from tm_kn where i_area='$iarea' and i_refference like 'BBM%' and f_kn_cancel='f')
					and (a.i_bbm like '%$cari%' or b.i_ttb like '%$cari%')
					order by a.i_bbm",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function runningnumberkn($th,$iarea)
	{
		$pot=substr($th,2,2);
#		$this->db->select(" trim(to_char(count(i_kn)+1,'000')) as no from tm_kn where n_kn_year=$th and i_area='$iarea'",false);
		$this->db->select(" max(substring(i_kn,4,3)) as no from tm_kn where n_kn_year=$th and i_area='$iarea' 
                        and substring(i_kn,1,2)<>'KP' and substring(i_kn,1,1)<>'D'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $row)
			{
        $nono=$row->no+1;			  
			}
      settype($nono,"string");
		  $a=strlen($nono);
		  while($a<3){
		    $nono="0".$nono;
		    $a=strlen($nono);
		  }
      $kn="K".$iarea.$nono.$pot;
			return $kn;
		}else{
			$kn="K".$iarea."001".$pot;
			return $kn;
		}
	}
	function bacakn($ikn,$nknyear,$iarea)
	{
 		$this->db->select(" distinct (a.i_kn||a.i_area||a.i_customer||a.i_salesman),
            a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, 
				    e.e_salesman_name, g.n_ttb_discount1, g.n_ttb_discount2, g.n_ttb_discount3,
            g.v_ttb_discount1, g.v_ttb_discount2, g.v_ttb_discount3, g.v_ttb_gross, g.v_ttb_discounttotal,g.v_ttb_netto
					from tm_kn a
				    inner join tr_customer b on (a.i_customer=b.i_customer)
				    inner join tr_area c on (a.i_area=c.i_area)
				    left join tr_customer_groupar d on (b.i_customer=d.i_customer)
				    inner join tr_salesman e on (a.i_salesman=e.i_salesman)
            inner join tm_bbm f on (a.i_refference=f.i_bbm)
            inner join tm_ttbretur g on (f.i_refference_document=g.i_ttb and f.d_refference_document=g.d_ttb and f.i_area=g.i_area)
			    where
					  a.i_kn_type='01'
					  and a.i_kn='$ikn'
					  and a.n_kn_year=$nknyear
					  and a.i_area='$iarea'",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
	}
  function bacabbmdetail($ibbm)
    {
		$this->db->select(" a.i_bbm, a.i_refference_document, a.i_product, a.i_product_motif, a.i_product_grade, a.n_quantity, a.v_unit_price,
                        a.e_remark, a.e_product_name, a.d_refference_document, a.e_mutasi_periode, b.e_product_motifname
					              from tm_bbm_item a, tr_product_motif b where a.i_bbm_type='05' and a.i_bbm='$ibbm' 
                        and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                        order by a.n_item_no",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
  function jmldetail($ibbm)
    {
    $jml=0;
		$this->db->select(" count(a.i_bbm) as jml
					              from tm_bbm_item a, tr_product_motif b where a.i_bbm_type='05' and a.i_bbm='$ibbm' 
                        and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			foreach($query->result() as $xx){
        $jml=$xx->jml;
      }
		}
    return $jml;
    }
  function bacapajak($iproduct,$icustomer,$cari,$num,$offset)
  {
	  $this->db->select(" a.i_nota, a.d_nota, a.i_seri_pajak, a.d_pajak
											  from tm_nota a, tr_customer b, tm_nota_item c
											  where a.i_customer=b.i_customer 
											  and a.i_nota=c.i_nota and a.i_area=c.i_area
											  and a.f_ttb_tolak='f' and a.f_nota_koreksi='f'
											  and a.f_nota_cancel='f'
											  and not a.i_nota isnull and a.i_customer='$icustomer' 
											  and upper(c.i_product) like '%$iproduct%'
											  ORDER BY a.i_nota ",false)->limit($num,$offset);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
}
?>
