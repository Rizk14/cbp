<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }

    function bacasj($cari,$dfrom,$dto,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select("	i_sj, i_area from tm_nota where upper(i_sj) like '%$cari%'
                          and (substring(i_sj,9,2)='$area1' or substring(i_sj,9,2)='$area2' or 
                          substring(i_sj,9,2)='$area3' or substring(i_sj,9,2)='$area4' or substring(i_sj,9,2)='$area5') 
                          and d_sj >= to_date('$dfrom','dd-mm-yyyy') and d_sj <= to_date('$dto','dd-mm-yyyy')
                          order by i_sj", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacasj2($cari,$dfrom,$dto,$num,$offset,$area)
    {
		  $this->db->select("	i_sj, i_area from tm_nota where upper(i_sj) like '%$cari%'
                          and (substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area' or 
                          substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area')
                          and d_sj >= to_date('$dfrom','dd-mm-yyyy') and d_sj <= to_date('$dto','dd-mm-yyyy') order by i_sj", false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		  $this->db->select("	i_sj, substring(i_sj,9,2) as i_area from tm_nota where upper(i_sj) like '%$cari%'
                          and (substring(i_sj,9,2)='$area1' or substring(i_sj,9,2)='$area2' or 
                          substring(i_sj,9,2)='$area3' or substring(i_sj,9,2)='$area4' or substring(i_sj,9,2)='$area5') 
                          order by i_sj",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function carisj2($cari,$num,$offset,$area)
    {
		  $this->db->select("	i_sj, substring(i_sj,9,2) as i_area from tm_nota where upper(i_sj) like '%$cari%'
                          and substring(i_sj,9,2)='$area' order by i_sj",false)->limit($num,$offset);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacamaster($sjfrom,$sjto)
    {
		  $this->db->select("	tm_nota.*, tr_customer.e_customer_name, tr_customer.e_customer_address, tr_customer.e_customer_city,
                          tr_customer.e_customer_phone, tr_area.e_area_name, tm_spb.i_spb_po, tr_customer.f_customer_plusppn as f_plus_ppn, 
                          tm_spb.f_spb_consigment, tr_area.e_area_phone from tm_nota
							            inner join tr_customer on (tm_nota.i_customer=tr_customer.i_customer)
							            inner join tr_area on (substring(tm_nota.i_sj,9,2)=tr_area.i_area)
							            left join tm_spb on (tm_spb.i_spb=tm_nota.i_spb and tm_spb.i_area=tm_nota.i_area)
							            where tm_nota.i_sj >= '$sjfrom' and tm_nota.i_sj <= '$sjto' order by tm_nota.i_sj",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacadetail($sj,$area)
    {
      $cust='';
		  $tes=$this->db->query("select i_customer from tm_nota where i_sj = '$sj' and substring(i_sj,9,2)='$area'",false);
		  if ($tes->num_rows() > 0){
        foreach($tes->result() as $xx){
  			  $cust=$xx->i_customer;
        }
		  }
      $group='';
		  $que 	= $this->db->query(" select i_customer_plugroup from tr_customer_plugroup where i_customer='$cust'",false);
		  if($que->num_rows()>0){
        foreach($que->result() as $hmm){
          $group=$hmm->i_customer_plugroup;
        }
      }
      if($group==''){
		    $this->db->select("	* from tm_nota_item 
							              inner join tr_product_motif on (tm_nota_item.i_product_motif=tr_product_motif.i_product_motif
							              and tm_nota_item.i_product=tr_product_motif.i_product)
							              where tm_nota_item.i_sj = '$sj'
                            order by tm_nota_item.n_item_no",false);
  #and substring(tm_nota_item.i_sj,9,2)='$area'
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }else{
		    $this->db->select(" a.i_sj, a.i_nota, a.i_product as product, a.i_product_grade, a.i_product_motif, a.n_deliver, a.v_unit_price,
                            a.e_product_name, a.i_area, a.d_nota, a.n_item_no, c.i_customer_plu, c.i_product from tm_nota_item a
							              inner join tr_product_motif b on (a.i_product_motif=b.i_product_motif and a.i_product=b.i_product)
                            left join tr_customer_plu c on (c.i_customer_plugroup='$group' and a.i_product=c.i_product) 
							              where i_sj = '$sj' order by n_item_no",false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    return $query->result();
		    }
      }
    }

    function updatesj($isj, $area)
    {
		  $query 	= $this->db->query("SELECT current_timestamp as c");
		  $row   	= $query->row();
		  $dprint	= $row->c;
      $this->db->set(
      		array(
			  'd_sj_print'			=> $dprint
      		)
      	);
		  $this->db->where('i_sj', $isj);
		  $this->db->where('i_area', $area);
		  $this->db->update('tm_nota'); 
    }
}
?>
