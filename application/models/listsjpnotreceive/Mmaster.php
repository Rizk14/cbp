<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
		function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("* from tr_area order by i_area", false)->limit($num,$offset);
			}else{
				$this->db->select("* from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5' order by i_area", false)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
		function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 order by i_area ", FALSE)->limit($num,$offset);
			}else{
				$this->db->select("i_area, e_area_name from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
									 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									 or i_area = '$area4' or i_area = '$area5') order by i_area ", FALSE)->limit($num,$offset);
			}
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function bacaperiode($num,$offset,$cari)
    {
			$this->db->select("	a.*, b.e_area_name from tm_sjp a, tr_area b
													where a.i_area=b.i_area and a.f_sjp_cancel='f'
													and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
													and a.d_sjp_receive is null
													ORDER BY a.i_bapb desc,a.i_area,a.i_sjp",false)->limit($num,$offset);
			$query = $this->db->get();
			if ($query->num_rows() > 0){
				return $query->result();
			}
    }
    function baca($isjp,$iarea)
    {
		$this->db->select(" distinct(c.i_store), a.*, b.e_area_name 
                        from tm_sjp a, tr_area b, tm_sjp_item c
						            where a.i_area=b.i_area and a.i_sjp=c.i_sjp 
                        and a.i_area=c.i_area
						            and a.i_sjp ='$isjp' and a.i_area='$iarea' ", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->row();
		  }
    }
    function bacadetail($isjp, $iarea)
    {
		$this->db->select("a.i_sjp,a.d_sjp,a.i_area,a.i_product,a.i_product_grade,a.i_product_motif,
                       a.n_quantity_receive,a.n_quantity_deliver,a.v_unit_price,a.e_product_name,
                       a.i_store,a.i_store_location,a.i_store_locationbin,a.e_remark,
                       b.e_product_motifname from tm_sjp_item a, tr_product_motif b
				               where a.i_sjp = '$isjp' and a.i_area='$iarea' 
                       and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                       order by a.n_item_no", false);
		$query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
}
?>
