<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function bacasemua($tahun,$tanggalreminder,$num,$offset)
    {
		  $this->db->select("* from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where a.d_kontrak_ke2_akhir >= to_date('$tanggalreminder','yyyy-mm-dd') AND a.d_kontrak_ke2_akhir <= to_date('$tahun','yyyy-mm-dd')
			   and a.i_karyawan_status='2'",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
  		}
    }

    function baca()
    {
		  $this->db->select("* from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where a.i_karyawan_status='1' ",false);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
  		}
    }
    function cari($m2,$mm, $num, $offset)
    {
		  $this->db->select(" * from tr_kendaraan 
							            left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
							            left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
							            left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
							            left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
							            left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 							  			left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
							            where upper(tr_kendaraan.i_kendaraan) like '%$cari%' 
							            and tr_kendaraan_pengguna.i_periode='$iperiode' and to_char(tr_kendaraan.d_pajak,'yyyymm')='$m2' and tr_kendaraan_pengguna.i_periode='$mm'
							            order by tr_kendaraan_pengguna.i_periode desc, tr_kendaraan.i_kendaraan",false)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
}
?>
