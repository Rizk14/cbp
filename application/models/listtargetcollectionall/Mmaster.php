<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
      parent::__construct();
		  #$this->CI =& get_instance();
    }
    function baca($iperiode)
    {
    	$iuser=$this->session->userdata('user_id');
      $this->db->select("	i_area, e_area_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon,
                          sum(realisasinon) as realisasinon, sum(blmbayar) as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat,
                          sum(realisasitdktelat) as realisasitdktelat, sum(realisasitelat) as realisasitelat from(
                          select a.i_area, a.e_area_name, sum(b.v_target_tagihan) as total, sum(b.v_realisasi_tagihan) as realisasi, 
                          0 as totalnon, 0 as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat,
                          0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' 
                          and b.f_insentif='t'
                          group by a.i_area, a.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, sum(b.v_target_tagihan) as totalnon, 
                          sum(b.v_realisasi_tagihan) as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat,
                          0 as realisasitdktelat, 0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' 
                          and b.f_insentif='f'
                          group by a.i_area, a.e_area_name
                          union all
                          select x.i_area, x.e_area_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          sum(x.v_target_tagihan) as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_nota, sum(b.v_target_tagihan) as v_target_tagihan, 
                          sum(b.v_realisasi_tagihan) as v_realisasi_tagihan
                          from 
                          tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' 
                          and b.f_insentif='t' and substring(b.e_kelompok,1,2)='02'
                          group by a.i_area, a.e_area_name, b.i_nota
                          ) as x
                          where x.v_realisasi_tagihan=0
                          group by x.i_area, x.e_area_name
                          union all
                          select a.i_area, a.e_area_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 0  as blmbayar, 
                          sum(tdktelat) as tdktelat, sum(telat) as telat, sum(realisasitdktelat) as realisasitdktelat, 
                          sum(realisasitelat) as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, 0  as blmbayar, sum(b.v_target_tagihan) as tdktelat,
                          0 as telat, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          sum(b.v_realisasi_tagihan) as realisasitdktelat, 0 as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_nota, avg(b.n_lamabayar) as n_lama
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' 
                          and b.f_insentif='t' and substring(b.e_kelompok,1,2)='00'
                          group by a.i_area, a.e_area_name, b.i_nota
                          ) as c, tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and a.i_area=c.i_area 
                          and b.i_nota=c.i_nota
                          and a.i_area in (select i_area from tm_user_area where i_user='$iuser')
                          group by a.i_area, a.e_area_name, c.n_lama
                          
                          union all
                          
                          select a.i_area, a.e_area_name, 0  as blmbayar, 0 as tdktelat,
                          sum(b.v_target_tagihan) as telat, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          0 as realisasitdktelat, sum(b.v_realisasi_tagihan) as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_nota, avg(b.n_lamabayar) as n_lama
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' 
                          and b.f_insentif='t' and substring(b.e_kelompok,1,2)='01'
                          group by a.i_area, a.e_area_name, b.i_nota
                          ) as c, tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and a.i_area=c.i_area 
                          and b.i_nota=c.i_nota
                          group by a.i_area, a.e_area_name, c.n_lama
                          )as a
                          group by a.i_area, a.e_area_name
                          ) as a
                          group by a.i_area, a.e_area_name
                          order by a.i_area",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detail($iarea,$iperiode)
    {
      $this->db->select("	a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, a.e_salesman_name, 
                          a.i_nota, a.d_nota, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon, sum(blmbayar) as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat,
                          sum(realisasitdktelat) as realisasitdktelat, sum(realisasitelat) as realisasitelat from(
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, sum(b.v_target_tagihan) as total, sum(b.v_realisasi_tagihan) as realisasi, 
                          0 as totalnon, 0 as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat,
                          0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota
                          union all
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, 0 as total, 0 as realisasi, sum(b.v_target_tagihan) as totalnon, 
                          sum(b.v_realisasi_tagihan) as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat,
                          0 as realisasitdktelat, 0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota
                          union all
                          select x.i_area, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_classname, x.i_salesman, 
                          x.e_salesman_name, x.i_nota, x.d_nota, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          sum(x.v_target_tagihan) as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, sum(b.v_target_tagihan) as v_target_tagihan, 
                          sum(b.v_realisasi_tagihan) as v_realisasi_tagihan
                          from 
                          tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='02' 
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota
                          ) as x
                          where x.v_realisasi_tagihan=0
                          group by x.i_area, x.e_area_name, x.i_customer, x.e_customer_name, x.e_customer_classname, x.i_salesman, 
                          x.e_salesman_name, x.i_nota, x.d_nota
                          union all
                          select a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, 
                          a.e_salesman_name, a.i_nota, a.d_nota, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          0  as blmbayar, 
                          sum(tdktelat) as tdktelat, sum(telat) as telat, sum(realisasitdktelat) as realisasitdktelat, 
                          sum(realisasitelat) as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, 0  as blmbayar,
                          sum(b.v_target_tagihan) as tdktelat,
                          0 as telat,
                          0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          sum(b.v_realisasi_tagihan) as realisasitdktelat,
                          0 as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, avg(b.n_lamabayar) as n_lama
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='00' 
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota
                          ) as c, tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and a.i_area=c.i_area 
                          and b.i_nota=c.i_nota
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, c.n_lama
                          
                          union all
                          
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, 0  as blmbayar,
                          0 as tdktelat, sum(b.v_target_tagihan) as telat, 
                          0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          0 as realisasitdktelat, sum(b.v_realisasi_tagihan) as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, avg(b.n_lamabayar) as n_lama
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='01' 
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota
                          
                          ) as c, tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t' and a.i_area=c.i_area 
                          and b.i_nota=c.i_nota
                          group by a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_salesman, 
                          b.e_salesman_name, b.i_nota, b.d_nota, c.n_lama
                          )as a
                          group by a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, 
                          a.e_salesman_name, a.i_nota, a.d_nota
                          ) as a
                          group by a.i_area, a.e_area_name, a.i_customer, a.e_customer_name, a.e_customer_classname, a.i_salesman, 
                          a.e_salesman_name, a.i_nota, a.d_nota
                          order by a.i_area, a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detailsales($iarea,$iperiode)
    {
#                          where x.v_realisasi_tagihan=0
      $this->db->select("	a.i_salesman, a.e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon, sum(blmbayar) as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat,
                          sum(realisasitdktelat) as realisasitdktelat, sum(realisasitelat) as realisasitelat from(
                          select b.i_salesman, b.e_salesman_name, sum(b.v_target_tagihan) as total, sum(b.v_realisasi_tagihan) as realisasi, 
                          0 as totalnon, 0 as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat,
                          0 as realisasitelat
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.i_salesman, b.e_salesman_name
                          union all
                          select b.i_salesman, b.e_salesman_name, 0 as total, 0 as realisasi, sum(b.v_target_tagihan) as totalnon, 
                          sum(b.v_realisasi_tagihan) as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat,
                          0 as realisasitdktelat, 0 as realisasitelat
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.i_salesman, b.e_salesman_name
                          union all

                          select x.i_salesman, x.e_salesman_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          sum(x.v_target_tagihan) as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, 
                          sum(b.v_target_tagihan)-sum(b.v_realisasi_tagihan) as v_target_tagihan, 
                          0 as v_realisasi_tagihan
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='02'
                          group by b.i_salesman, b.e_salesman_name
                          ) as x
                          group by x.i_salesman, x.e_salesman_name

                          union all

                          select a.i_salesman, a.e_salesman_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          0  as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat, sum(realisasitdktelat) as realisasitdktelat, 
                          sum(realisasitelat) as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, 0  as blmbayar,
                          sum(b.v_target_tagihan) as tdktelat, 0 as telat, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          sum(b.v_realisasi_tagihan) as realisasitdktelat, 0 as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, b.i_nota
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='00'
                          group by b.i_salesman, b.e_salesman_name, b.i_nota
                          ) as c, tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and b.i_nota=c.i_nota
                          group by b.i_salesman, b.e_salesman_name

                          union all
                                                                              
                          select b.i_salesman, b.e_salesman_name, 0  as blmbayar,
                          0 as tdktelat, sum(b.v_target_tagihan) as telat, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon,
                          0 as realisasitdktelat, sum(b.v_realisasi_tagihan) as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, b.i_nota
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and substring(b.e_kelompok,1,2)='01'
                          group by b.i_salesman, b.e_salesman_name, b.i_nota
                          ) as c, tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and b.i_nota=c.i_nota
                          group by b.i_salesman, b.e_salesman_name

                          )as a
                          group by a.i_salesman, a.e_salesman_name
                          ) as a
                          group by a.i_salesman, a.e_salesman_name
                          order by a.e_salesman_name, a.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detaildivisi($iarea,$iperiode)
    {
/*
      $this->db->select("	a.i_area, a.e_area_name, a.i_nota, a.i_product_group, a.e_product_groupname, a.d_jatuh_tempo,   
                          to_char(a.d_jatuh_tempo_plustoleransi,'yyyy-mm-dd') as d_jatuh_tempo_plustoleransi,
                          sum(a.total) as total, sum(a.realisasi) as realisasi, sum(a.totalnon) as totalnon, 
                          sum(a.realisasinon) as realisasinon from
                          (
                          select a.i_area, a.e_area_name, b.i_nota, b.i_product_group, c.e_product_groupname, b.d_jatuh_tempo,
                          b.d_jatuh_tempo_plustoleransi, 
                          b.v_target_tagihan as total, b.v_realisasi_tagihan as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          left join tr_product_group c on (b.i_product_group=c.i_product_group)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          union all
                          select a.i_area, a.e_area_name, b.i_nota, b.i_product_group, c.e_product_groupname, b.d_jatuh_tempo, 
                          b.d_jatuh_tempo_plustoleransi, 0 as total, 
                          0 as realisasi, b.v_target_tagihan as totalnon, b.v_realisasi_tagihan as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          left join tr_product_group c on (b.i_product_group=c.i_product_group)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          ) as a 
                          group by a.i_area, a.e_area_name, a.i_nota, a.i_product_group, a.e_product_groupname, a.d_jatuh_tempo,
                          a.d_jatuh_tempo_plustoleransi
                          order by a.i_area, a.i_product_group, a.i_nota",false);
*/
      $this->db->select("	a.i_area, a.e_area_name, a.i_nota, a.i_product_group, a.e_product_groupname, a.d_jatuh_tempo,   
                          to_char(a.d_jatuh_tempo_plustoleransi,'yyyy-mm-dd') as d_jatuh_tempo_plustoleransi,
                          sum(a.total) as total, sum(a.realisasi) as realisasi, sum(a.totalnon) as totalnon, 
                          sum(a.realisasinon) as realisasinon, sum(blmbayar) as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat,
                          sum(realisasitdktelat) as realisasitdktelat, sum(realisasitelat) as realisasitelat from
                          (
                          select a.i_area, a.e_area_name, b.i_nota, b.i_product_group, c.e_product_groupname, b.d_jatuh_tempo,
                          b.d_jatuh_tempo_plustoleransi, 
                          b.v_target_tagihan as total, b.v_realisasi_tagihan as realisasi, 0 as totalnon, 0 as realisasinon, 0 as blmbayar, 
                          0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          left join tr_product_group c on (b.i_product_group=c.i_product_group)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'
                          union all
                          select a.i_area, a.e_area_name, b.i_nota, b.i_product_group, c.e_product_groupname, b.d_jatuh_tempo, 
                          b.d_jatuh_tempo_plustoleransi, 0 as total, 
                          0 as realisasi, b.v_target_tagihan as totalnon, b.v_realisasi_tagihan as realisasinon, 0 as blmbayar, 
                          0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          left join tr_product_group c on (b.i_product_group=c.i_product_group)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='f'
                          
                          union all
                          select x.i_area, x.e_area_name, x.i_nota, x.i_product_group, x.e_product_groupname, x.d_jatuh_tempo, 
                          x.d_jatuh_tempo_plustoleransi, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          x.v_target_tagihan as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from (
                          select a.i_area, a.e_area_name, b.i_nota, b.i_product_group, c.e_product_groupname, b.d_jatuh_tempo, 
                          b.d_jatuh_tempo_plustoleransi, 0 as total, 
                          0 as realisasi, 0 as totalnon, 0 as realisasinon, b.v_target_tagihan, 
                          0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from 
                          tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          left join tr_product_group c on (b.i_product_group=c.i_product_group)
                          where a.f_area_real='t' and b.e_periode='$iperiode' and b.f_insentif='t'



                          ) as x
                          where x.realisasi=0

                          
                          ) as a 
                          group by a.i_area, a.e_area_name, a.i_nota, a.i_product_group, a.e_product_groupname, a.d_jatuh_tempo,
                          a.d_jatuh_tempo_plustoleransi
                          order by a.i_area, a.i_product_group, a.i_nota",false);
                          
                          
#                          group by a.i_area, a.e_area_name, b.i_nota                          
#                          group by x.i_area, x.e_area_name
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function detailcetak($iarea,$iperiode)
    {
      $this->db->select("	e_periode, i_area, e_area_name, i_customer, e_customer_name, e_customer_classname, i_nota, d_nota, 
                          sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, sum(realisasinon) as realisasinon 
                          from(
                          select b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota, sum(b.sisa) as total, sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota, sum(b.bayar) as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota, 0 as total, 0 as realisasi, sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota, 0 as total, 0 as realisasi, sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_item b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          where a.f_area_real='t' and b.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by b.e_periode, a.i_area, a.e_area_name, b.i_customer, b.e_customer_name, b.e_customer_classname, b.i_nota, 
                          b.d_nota
                          ) as a 
                          group by e_periode, i_area, e_area_name, i_customer, e_customer_name, e_customer_classname, i_nota, d_nota
                          order by a.i_area, a.i_nota",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function cetaksales($iarea,$iperiode)
    {
      $this->db->select("	e_periode,i_area, e_area_name, i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                          sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.sisa) as total, 
                          sum(b.bayar) as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, sum(b.bayar) as total, 
                          0 as realisasi, 0 as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='t'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.sisa) as totalnon, sum(b.bayar) as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          union all
                          select b.e_periode, a.i_area, a.e_area_name, e.i_salesman, e.e_salesman_name, 0 as total, 0 as realisasi, 
                          sum(b.bayar) as totalnon, 0 as realisasinon
                          from tr_area a
                          left join tm_collection_credit b on(a.i_area=b.i_area and b.v_nota_netto=0 and b.sisa=0)
                          left join tr_salesman e on (b.i_salesman=e.i_salesman)
                          where a.f_area_real='t' and a.i_area='$iarea' and b.e_periode='$iperiode' and b.f_insentif='f'
                          group by a.i_area, a.e_area_name, e.i_salesman, e_salesman_name, b.e_periode
                          ) as a 
                          group by i_area, e_area_name, i_salesman, e_salesman_name, e_periode
                          order by a.i_area, a.i_salesman",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaexcel($iperiode,$istore,$cari)
    {
		  $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$istore' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
      return $query;
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct (b.i_store), b.e_store_name, c.i_store_location, c.e_store_locationname
                        from tr_area a, tr_store b, tr_store_location c
                        where a.i_store=b.i_store and b.i_store=c.i_store
                        and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                        or a.i_area = '$area4' or a.i_area = '$area5')
                        order by b.i_store, c.i_store_location", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname 
                 from tr_area a, tr_store b , tr_store_location c
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store and b.i_store=c.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name, c.i_store_location, c.e_store_locationname
                 from tr_area a, tr_store b, tr_store_location c
                 where b.i_store=c.i_store and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
  }
  function bacaareas($iperiode)
  {
    $this->db->select(" distinct i_area from tm_collection_credit where e_periode='$iperiode' ",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
  function detailall($iperiode)
  {
    $this->db->select("	a.i_area, a.i_salesman, e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, 
                        sum(totalnon) as totalnon, sum(realisasinon) as realisasinon from(
                        select b.i_area, b.i_salesman, b.e_salesman_name, sum(b.v_target_tagihan) as total, 
                        sum(b.v_realisasi_tagihan) as realisasi, 0 as totalnon, 0 as realisasinon
                        from tm_collection_item b
                        where b.e_periode='$iperiode' and b.f_insentif='t'
                        group by b.i_area, b.i_salesman, b.e_salesman_name
                        union all
                        select b.i_area, b.i_salesman, b.e_salesman_name, 0 as total, 0 as realisasi, 
                        sum(b.v_target_tagihan) as totalnon, sum(b.v_realisasi_tagihan) as realisasinon
                        from tm_collection_item b
                        where b.e_periode='$iperiode' and b.f_insentif='f'
                        group by b.i_area, b.i_salesman, b.e_salesman_name
                        ) as a 
                        group by a.i_area, a.i_salesman, a.e_salesman_name
                        order by a.i_area, a.i_salesman",false);
	  $query = $this->db->get();
	  if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
}
?>
