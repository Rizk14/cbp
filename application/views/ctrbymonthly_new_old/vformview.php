<style type="text/css">
  #container{float:left;width:100px;}
  }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/FusionCharts.js"></script>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbymonthly_new/cform/view','update'=>'#main','type'=>'post'));?>
<?php 
if($dfrom!=''){
$tmp=explode("-",$dfrom); 
$th=$tmp[2];
$tahun=$th;
} ?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $tahun; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
         <tr>
            <td style="width:100px;">Group</td>
            <td style="width:5px;">:</td>
            <?php $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
                if ($query->num_rows() > 0){
                  foreach($query->result() as $tmp){
                     $group=$tmp->e_product_groupname;
                 }
               }

               if($group=="NA"){
                $group="NASIONAL";
               }
               
                ?>
            <td colspan=2> <?php echo $group; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totvnota=0;
            $totqty=0;
            foreach($isi as $ro){
              $totvnota=$totvnota+$ro->vnota;
              $totqty=$totqty+$ro->qty;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan="2">Bulan</th>
         <th rowspan="2">OB</th>
         <th colspan="3">OA</th>
         <th colspan="3">Sales Qty(Unit)</th>
         <th colspan="3">Net Sales(Rp.)</th>
         <th rowspan="2">%Ctr Net Sales(Rp.)</th>
         </tr>
         <tr>
            <?php $prevth= $tahun-1; ?>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth OA</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Qty</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Rp</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $grwoa=0;
        $grwqty=0;
        $grwrp=0;
        $totnota=0;
        $totrp=0;
        $ctrrp=0;
        $totoaprev=0;
        $totoa=0;
        $totqtyprev=0;
        $totvnotaprev=0;
        $totctrrp=0;
        $totgrwoa=0;
        $totgrwqty=0;
        $totgrwrp=0;
        $totpersenvnota=0;
        $totob=0;

        foreach($isi as $row){
            $totnota+=$row->vnota;
        }
        $totrp=$totnota;


        foreach($isi as $row){
          
          if($totvnota==0){
            $persenvnota=0;
          }else{
            $persenvnota=($row->vnota/$totvnota)*100;
          }
          $totpersenvnota=$totpersenvnota+$persenvnota;

          if ($row->oaprev == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->oaprev)/$row->oaprev)*100;
          }

          if ($row->qtyprev == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->qty-$row->qtyprev)/$row->qtyprev)*100;
          }

          if ($row->vnotaprev == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->vnota-$row->vnotaprev)/$row->vnotaprev)*100;
          }

          $ctrrp= ($row->vnota/$totrp)*100;
          $totoaprev=$totoaprev+$row->oaprev;
          $totoa=$totoa+$row->oa;
          $totqtyprev=$totqtyprev+$row->qtyprev;
          $totvnotaprev=$totvnotaprev+$row->vnotaprev;
          $totctrrp=$totctrrp+$ctrrp;
          $totob=$totob+$row->ob;
          $period=mbulan($row->i_periode);


          echo "<tr>
              <td style='font-size:12px;'>$period</td>
              <td style='font-size:12px;' align=right>".number_format($row->ob)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwoa,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->qtyprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qty)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwqty,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwrp,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
         }

         if ($totoaprev == 0) {
              $totgrwoa = 0;
          } else { //jika pembagi tidak 0
              $totgrwoa = (($totoa-$totoaprev)/$totoaprev)*100;
          }

          if ($totqtyprev == 0) {
              $totgrwqty = 0;
          } else { //jika pembagi tidak 0
              $totgrwqty = (($totqty-$totqtyprev)/$totqtyprev)*100;
          }

          if ($totvnotaprev == 0) {
              $totgrwrp = 0;
          } else { //jika pembagi tidak 0
              $totgrwrp = (($totvnota-$totvnotaprev)/$totvnotaprev)*100;
          }

        echo "<tr>
        <td style='font-size:12px;' ><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totob)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwoa,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqtyprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqty)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwqty,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwrp,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>
        <tr>
          <td colspan='2'></td>
          <td colspan='3'><a href=\"#\" onclick=\"trendchartoa('".$dfrom."','".$dto."','".$group."');\">Chart Trend OA Nasional</td>
          <td colspan='3'><a href=\"#\" onclick=\"trendchartqty('".$dfrom."','".$dto."','".$group."');\">Chart Trend Qty Nasional</td>
          <td colspan='3'><a href=\"#\" onclick=\"trendchartvnota('".$dfrom."','".$dto."','".$group."');\">Chart Net Sales Nasional</td>
          <td></td>
        </tr>
        <tr>
          <td colspan='2'></td>
          <td colspan='3'><a href=\"#\" onclick=\"growthchartoa('".$dfrom."','".$dto."','".$group."');\">Chart %Growth OA Nasional</td>
          <td colspan='3'><a href=\"#\" onclick=\"growthchartqty('".$dfrom."','".$dto."','".$group."');\">Chart %Growth Qty Nasional</td>
          <td colspan='3'><a href=\"#\" onclick=\"growthchartvnota('".$dfrom."','".$dto."','".$group."');\">Chart %Growth Net Sales Nasional</td>
          <td></td>
        </tr>";
      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
function trendchartoa(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartoa/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function trendchartqty(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartqty/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function trendchartvnota(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartvnota/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function growthchartoa(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartgrowthoa/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function growthchartqty(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartgrowthqty/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
function growthchartvnota(dfrom,dto,group) {
  lebar =1000;
  tinggi=600;
  eval('window.open("<?php echo site_url(); ?>"+"/ctrbymonthly_new/cform/chartgrowthvnota/"+dfrom+"/"+dto+"/"+group,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1, top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
}
</script>
