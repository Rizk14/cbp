<header>
  <title>SPB vs SJ Nasional</title>
</header>

<style>
  .container {
    position: relative;
    overflow: scroll;
    height: 500px;
  }
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tablefixheader.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>
<?php
// include ("php/fungsi.php");

/******************************************* */
function isSunday($date)
{
  return (date('N', strtotime($date)) == 7); // Hari Minggu memiliki kode 7
}

function isHoliday($date, $result)
{
  return in_array($date, $result);
}

function calculateBusinessDays($startDate, $endDate, $result)
{
  $currentDate = $startDate;
  $businessDays = 0;

  while ($currentDate < $endDate) {

    if (!isSunday($currentDate) && !isHoliday($currentDate, $result)) {
      $businessDays++;
    }

    $currentDate = date('Y-m-d', strtotime($currentDate .
      ' +1 day'));
  }

  return $businessDays;
}
/* ************************************** */
?>
<!-- <h2><?php echo $page_title; ?></h2> -->

<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'spbvssjnasional/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
      <div id="listform">
        <div class="effect">

          <input name="cmdreset" id="cmdreset" value="Export View" type="button">

          <table class="listtablex" id="sitabel">
            <thead class="sticky-head">
              <tr>
                <th>Kode Pelanggan</th>
                <th>Pelanggan</th>
                <th>Status</th>
                <th>Kode Area</th>
                <th>Area</th>
                <th>Kota</th>
                <th>No SPB</th>
                <th>Tgl SPB</th>
                <th>Tgl App SPB</th>
                <th>SPB->APP(Hari)</th>
                <th>No SJ</th>
                <th>Tgl SJ</th>
                <th>SPB->SJ(Hari)</th>
                <th>No Nota</th>
                <th>Tgl Nota</th>
                <th>SJ->Nota(Hari)</th>
                <th>SPB->Nota(Hari)</th>
                <th>Tgl Terima Toko</th>
                <th>Nota->Terima Toko(Hari)</th>
                <th>SPB->Terima Toko(Hari)</th>
                <th>Nilai Penjualan</th>
              </tr>
            </thead>

            <tbody>
              <?php
              if ($isi) {
                foreach ($isi as $row) {
                  // Untuk Tanggal Approve
                  if ($row->d_approve1 > $row->d_approve2) {
                    $tglapprovespb = $row->d_approve1;
                  } elseif ($row->d_approve2 > $row->d_approve1) {
                    $tglapprovespb = $row->d_approve2;
                  } elseif ($row->d_approve2 = $row->d_approve1) {
                    $tglapprovespb = $row->d_approve2;
                  } else {
                    $tglapprovespb = $row->d_spb;
                  }

                  $selisihspbapp      = calculateBusinessDays($row->d_spb, $tglapprovespb, $result);
                  $selisihsj          = calculateBusinessDays($row->d_spb, $row->d_sj, $result);
                  $selisihsjnot       = calculateBusinessDays($row->d_sj, $row->d_nota, $result);
                  $selisihspbnot      = calculateBusinessDays($row->d_spb, $row->d_nota, $result);
                  $selisihnotaterima  = calculateBusinessDays($row->d_nota, $row->d_sj_receive, $result);
                  $selisihspbterima   = calculateBusinessDays($row->d_spb, $row->d_sj_receive, $result);

                  if ($row->d_spb != '') {
                    $tmp = explode("-", $row->d_spb);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $d_spb = $hr . "-" . $bl . "-" . $th;
                  }

                  if ($row->d_sj != '') {
                    $tmp = explode("-", $row->d_sj);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $d_sj = $hr . "-" . $bl . "-" . $th;
                  }

                  if ($row->d_nota != '') {
                    $tmp = explode("-", $row->d_nota);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $d_nota = $hr . "-" . $bl . "-" . $th;
                  }

                  if ($tglapprovespb != '') {
                    $tmp = explode("-", $tglapprovespb);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $tglapprovespb = $hr . "-" . $bl . "-" . $th;
                  }

                  if ($row->d_sj_receive != '') {
                    $tmp = explode("-", $row->d_sj_receive);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $row->d_sj_receive = $hr . "-" . $bl . "-" . $th;
                  }

                  echo "  <tr>
                              <td style='font-size:12px;' align=center>" . $row->i_customer . "</td>
                              <td style='font-size:12px;'>" . $row->e_customer_name . "</td>
                              <td style='font-size:12px;'>" . $row->e_customer_statusname . "</td>
                              <td style='font-size:12px;' align=center>" . $row->i_area . "</td>
                              <td style='font-size:12px;'>" . $row->e_area_name . "</td>
                              <td style='font-size:12px;'>" . $row->e_city_name . "</td>
                              <td style='font-size:12px;'>" . $row->i_spb . "</td>
                              <td style='font-size:12px;'>" . $d_spb . "</td>
                              <td style='font-size:12px;'>" . $tglapprovespb . "</td>
                              <td style='font-size:12px;' align=center>" . $selisihspbapp . "</td>
                              <td style='font-size:12px;'>" . $row->i_sj . "</td>
                              <td style='font-size:12px;'>" . $d_sj . "</td>
                              <td style='font-size:12px;' align=center>" . $selisihsj . "</td>
                              <td style='font-size:12px;'>" . $row->i_nota . "</td>
                              <td style='font-size:12px;'>" . $d_nota . "</td>
                              <td style='font-size:12px;' align=center>" . $selisihsjnot . "</td>
                              <td style='font-size:12px;' align=center>" . $selisihspbnot . "</td> 
                              <td style='font-size:12px;' align=center>" . $row->d_sj_receive . "</td>
                              <td style='font-size:12px;' align=center>" . $selisihnotaterima . "</td> 
                              <td style='font-size:12px;' align=center>" . $selisihspbterima . "</td> 
                              <td style='font-size:12px;' align=right>" . number_format($row->v_nota_netto) . "</td>
                            </tr> ";
                }
              }
              ?>
            </tbody>
          </table>
          <!-- <#?php echo "<center>".$this->pagination->create_links()."</center>";?> -->
          <input name="cmdreset" id="cmdreset" value="Export View" type="button">
        </div>
      </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
  // $("#cmdreset").click(function() {
  //   var Contents = $('#sitabel').html();
  //   //    alert(''+Contents);
  //   window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
  //   //    alert('exporting records...');
  // });

  $("#cmdreset").click(function() {
    //getting values of current time for generating the file name
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    //creating a temporary HTML link element (they support setting file names)
    var a = document.createElement('a');
    //getting data from our div that contains the HTML table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = $('#sitabel').html();
    // var table_html = table_div.outerHTML.replace(/ /g, '%20');
    a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

    //setting the file name
    // var nofaktur = document.getElementById('no_faktur').value;
    a.download = 'export_view_spbvssj_' + postfix + '.xls';
    //triggering the function
    a.click();
    //just in case, prevent default behaviour

    // var Contents = $('#sitabel').html();
    // window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
  });
</script>