<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>Detail $page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'rekaprrkh/cform/detailkunjungan','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
          <th>Area</th>
     	    <th>Salesman</th>
	        <th>Hari</th>
	        <th>Tgl Dokumen</th>
     	    <th>Customer</th>
          <th>Jenis</th>
	        <th>Rencana</th>
          <th>Realisasi</th>
          <th>Order</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->d_rrkh!=''){
          $tmp=explode('-',$row->d_rrkh);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_rrkh=$tgl.'-'.$bln.'-'.$thn;
        }
        $ts = strtotime($row->d_rrkh);
        $hari=date('w', $ts);
        switch($hari){       
            case 0 : {
                $hari='Minggu';
            }break;
            case 1 : {
                $hari='Senin';
            }break;
            case 2 : {
                $hari='Selasa';
            }break;
            case 3 : {
                $hari='Rabu';
            }break;
            case 4 : {
                $hari='Kamis';
            }break;
            case 5 : {
                $hari="Jum'at";
            }break;
            case 6 : {
                $hari='Sabtu';
            }break;
            default: {
                $hari='Tidak Terdeteksi';
            }break;
        }
        if($row->f_kunjungan_realisasi=='t'){
          $real='YA';
        }else{
          $real='TIDAK';
        }
        //$order=$row->e_remark;
        //preg_match_all("/[\^D]/", $order, $matches);
			  echo "<tr> 
          <td>$row->i_area</td>
				  <td>($row->i_salesman) $row->e_salesman_name</td>
				  <td>$hari</td>
				  <td>$row->d_rrkh</td>
				  <td>($row->i_customer) $row->e_customer_name</td>
          <td>$row->e_customer_classname</td>
				  <td>$row->e_kunjungan_typename</td>
          <td>$real</td>";
          //foreach($matches as $key=>$value)
          //{
          // echo "<td>$value</td>";
          //}
           echo "<td>$row->e_remark</td>";
			echo "</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <br>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
