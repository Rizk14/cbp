<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'pelunasan/cform/kn';
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No KN</th>
	    	<th>Tgl KN</th>
	    	<th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_kn);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_kn=$dd."-".$mm."-".$yy;
			  $row->v_netto=number_format($row->v_netto);
			  $row->v_sisa=number_format($row->v_sisa);
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_kn','$row->d_kn','$row->v_sisa')\">$row->i_kn</a></td>
			  <td><a href=\"javascript:setValue('$row->i_kn','$row->d_kn','$row->v_sisa')\">$row->d_kn</a></td>
			  <td><a href=\"javascript:setValue('$row->i_kn','$row->d_kn','$row->v_sisa')\">$row->v_netto</a></td>
			  <td><a href=\"javascript:setValue('$row->i_kn','$row->d_kn','$row->v_sisa')\">$row->v_sisa</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,d)
  {
    document.getElementById("igiro").value=a;
	document.getElementById("dgiro").value=b;
    document.getElementById("vjumlah").value=d;
    document.getElementById("vlebih").value=d;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
