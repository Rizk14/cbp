<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		$tujuan = 'pelunasan-ap/cform/idtap/'.$baris.'/'.$isupplier;
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"><input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier;?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea;?>"></td>
	      </tr>
	    </thead>
      	    <th align="center">Nota</th>
	    	<th align="center">Tanggal</th>
			<th align="center">Jumlah</th>
			<th align="center">Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_dtap);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_dtap=$dd."-".$mm."-".$yy;
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">$row->i_dtap</a></td>
			  <td><a href=\"javascript:setValue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">$row->d_dtap</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">".number_format($row->v_netto)."</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">".number_format($row->v_sisa)."</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e)
  {
    ada=false;
    for(z=1;z<=e;z++){
		if(
			(a==document.getElementById("inota"+z).value) && 
			(z!==e)
		  ){
			alert ("Nota : "+a+" sudah ada !!!!!");
			ada=true;
			break;
		}else{
			ada=false;	   
		}
    }	
    if(!ada)
    {
		  document.getElementById("inota"+e).value=a;
		  document.getElementById("dnota"+e).value=b;
		  document.getElementById("vnota"+e).value=formatcemua(d);
		  tmp=parseFloat(formatulang(document.getElementById("vjumlah").value));
		  jml=document.getElementById("jml").value;

		  if(tmp>0)
      {
		    sisa=0;
		    jumall=tmp;
		    for(x=1;x<=jml;x++)
        {
			    jum	  = parseFloat(formatulang(document.getElementById("vnota"+x).value));
			    jumall= jumall-jum;
		      if(jumall>0)
		      {
		        document.getElementById("vlebih").value=formatcemua(jumall);
		        document.getElementById("vjumlah"+e).value=formatcemua(d);
		        document.getElementById("vsisa"+e).value=0;
		      }else{
		        document.getElementById("vlebih").value="0";
            y=jumall+jum;
            if(y>0){
      			  document.getElementById("vjumlah"+e).value=formatcemua(y);
            }else{
      			  document.getElementById("vjumlah"+e).value=0;
            }
    			  sisa=jum-parseFloat(formatulang(document.getElementById("vjumlah"+e).value));
		        document.getElementById("vsisa"+e).value=formatcemua(sisa);
		      }
		    }
		  }
  		jsDlgHide("#konten *", "#fade", "#light");
    }
  }
  function bbatal(){
	baris	= document.getElementById("baris").value;
	si_inner= document.getElementById("detailisi").innerHTML;
	var temp= new Array();
	temp	= si_inner.split('<tbody disabled="disabled">');
	if( (document.getElementById("inota"+baris).value=='')){
		si_inner='';
		for(x=1;x<baris;x++){
			si_inner=si_inner+'<tbody>'+temp[x];
		}
		j=0;
		var barbar			= Array();
		var inota			= Array();
		var dnota			= Array();
		var vjumlah			= Array();
		var vsisa			= Array();

		for(i=1;i<baris;i++){
			j++;
			barbar[j]			= document.getElementById("baris"+i).value;
			inota[j]			= document.getElementById("inota"+i).value;
			dnota[j]			= document.getElementById("dnota"+i).value;
			vjumlah[j]			= document.getElementById("vjumlah"+i).value;
			vsisa[j]			= document.getElementById("vsisa"+i).value;
		}
		document.getElementById("detailisi").innerHTML=si_inner;
		j=0;
		for(i=1;i<baris;i++){
			j++;
			document.getElementById("baris"+i).value			= barbar[j];
			document.getElementById("inota"+i).value			= inota[j];
			document.getElementById("dnota"+i).value			= dnota[j];
			document.getElementById("vjumlah"+i).value			= vjumlah[j];
			document.getElementById("vsisa"+i).value			= vsisa[j];
		}
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;		
		jsDlgHide("#konten *", "#fade", "#light");
	}
  }
</script>
