<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'pelunasan/cform/giro';
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No Pelunasan</th>
	    	<th>Tgl Pelunasan</th>
	    	<th>Jumlah</th>
	    	<th>Lebih</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_bukti);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_bukti=$dd."-".$mm."-".$yy;
			  $this->db->select(" i_pelunasan from tm_pelunasan_lebih 
								  where substr(i_pelunasan,1,10)='$row->i_pelunasan' 
								  and i_area='$row->i_area' ", false);
			  $query = $this->db->get();
			  if ($query->num_rows() > 0){
				foreach($query->result() as $rs){
				  $row->i_pelunasan=$rs->i_pelunasan;
				}
			  }
			  $row->v_jumlah=number_format($row->v_jumlah);
			  $row->v_lebih=number_format($row->v_lebih);
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_pelunasan','$row->i_area','$row->v_lebih','$row->v_jumlah')\">$row->i_pelunasan</a></td>
			  <td><a href=\"javascript:setValue('$row->i_pelunasan','$row->i_area','$row->v_lebih','$row->v_jumlah')\">$row->d_bukti</a></td>
			  <td><a href=\"javascript:setValue('$row->i_pelunasan','$row->i_area','$row->v_lebih','$row->v_jumlah')\">$row->v_jumlah</a></td>
			  <td><a href=\"javascript:setValue('$row->i_pelunasan','$row->i_area','$row->v_lebih','$row->v_jumlah')\">$row->v_lebih</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d)
  {
    document.getElementById("igiro").value=a;
    document.getElementById("vjumlah").value=c;
    document.getElementById("vlebih").value=c;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
