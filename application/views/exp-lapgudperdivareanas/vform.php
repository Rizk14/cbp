<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'exp-lapgudperdivareanas/cform/export', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="spbperareaform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable">
              <tr>
                <td width="19%">Pilihan</td>
                <td width="1%">:</td>
                <td width="80%">
                  <?php
                  $n = 0;
                  echo "<input type='hidden' name='jml' id='jml' value='0' readonly>";
                  echo "<input type='hidden' name='judul' id='judul' value='' readonly>";
                  foreach ($productgrp->result() as $riw) {
                    echo $riw->e_product_groupname_short . "&nbsp;
                        <input type='checkbox' name='grp$n' id='grp$n' value='xxx' onclick=\"get_jenis('$n','$riw->i_product_group','$riw->e_product_groupname');\">
                        <input type='hidden' name='grp" . $n . "x' id='grp" . $n . "x' value='xxx'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    $n++;
                  }
                  ?>
                  <!-- <input type='hidden' name='chkbx' id='chkbx' value='xxx'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Non Bedding&nbsp;<input type='checkbox' name='chknb' id='chknb' value='xxx' onclick='puyeng2();'>
                  <input type='hidden' name='chknbx' id='chknbx' value='xxx'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  Reguler&nbsp;<input type='checkbox' name='chkr' id='chkr' value='xxx' onclick='puyeng3();'>
                  <input type='hidden' name='chkrx' id='chkrx' value='xxx'>
                  Lain-lain&nbsp;<input type='checkbox' name='chkl' id='chkl' value='xxx' onclick='puyeng4();'>
                  <input type='hidden' name='chklx' id='chklx' value='xxx'> -->
                </td>
              </tr>
              <tr>
                <td width="19%">Periode</td>
                <td width="1%">:</td>
                <td width="80%">
                  <input type="hidden" id="iperiode" name="iperiode" value="<?= date('Ym') ?>">
                  <select name="bulan" id="bulan" onchange="buatperiode()">
                    <option value='01' <?php echo date('m') == '01' ? 'selected' : '' ?>>Januari</option>
                    <option value='02' <?php echo date('m') == '02' ? 'selected' : '' ?>>Februari</option>
                    <option value='03' <?php echo date('m') == '03' ? 'selected' : '' ?>>Maret</option>
                    <option value='04' <?php echo date('m') == '04' ? 'selected' : '' ?>>April</option>
                    <option value='05' <?php echo date('m') == '05' ? 'selected' : '' ?>>Mei</option>
                    <option value='06' <?php echo date('m') == '06' ? 'selected' : '' ?>>Juni</option>
                    <option value='07' <?php echo date('m') == '07' ? 'selected' : '' ?>>Juli</option>
                    <option value='08' <?php echo date('m') == '08' ? 'selected' : '' ?>>Agustus</option>
                    <option value='09' <?php echo date('m') == '09' ? 'selected' : '' ?>>September</option>
                    <option value='10' <?php echo date('m') == '10' ? 'selected' : '' ?>>Oktober</option>
                    <option value='11' <?php echo date('m') == '11' ? 'selected' : '' ?>>November</option>
                    <option value='12' <?php echo date('m') == '12' ? 'selected' : '' ?>>Desember</option>
                  </select>

                  <select name="tahun" id="tahun" onMouseUp="buatperiode()">
                    <option></option>
                    <?php
                    $tahun1 = date('Y') - 3;
                    $tahun2 = date('Y');
                    for ($i = $tahun1; $i <= $tahun2; $i++) {
                      $selected = date('Y') == $i ? "selected" : "";

                      echo "<option value='$i' $selected>$i</option>";
                    }
                    ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="19%">&nbsp;</td>
                <td width="1%">&nbsp;</td>
                <td width="80%">
                  <input name="login" id="login" value="Transfer" type="submit">
                  <input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-lapgudperdivareanas/cform/','#main')">
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <?= form_close() ?>
      <div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function get_jenis(x, b, c) {
    $.ajax({
      type: "post",
      // data: {
      //   'icity': a,
      //   'isalesman': document.getElementById('isalesman').value,
      //   'iperiode': document.getElementById('iperiode').value,
      //   'iarea': document.getElementById('iarea').value,
      // },
      url: '<?= site_url('exp-lapgudperdivareanas/cform/get_group'); ?>',
      dataType: "json",
      success: function(data) {
        if (data['detail'].length > 0) {

          document.getElementById('jml').value = data['detail'].length; /* BUAT LOOPING NANTI DI CONTROLLERS WAKTU SIMPAN */
          document.getElementById('judul').value = c; /* BUAT JUDUL NANTI DI CONTROLLERS WAKTU SIMPAN */

          if (document.getElementById('grp' + x).checked) {

            document.getElementById('grp' + x).value = b;
            document.getElementById('grp' + x + "x").value = 'qqq';

            for (let a = 0; a < data['detail'].length; a++) {
              if (x != a) {
                document.getElementById('grp' + a).checked = false;
                document.getElementById('grp' + a).value = "xxx";
                document.getElementById('grp' + a + "x").value = "xxx";
              }
            }
          } else {
            for (let a = 0; a < data['detail'].length; a++) {
              document.getElementById('grp' + a).value = "xxx";
              document.getElementById('grp' + a + "x").value = "xxx";
              document.getElementById('judul').value = '';
            }
          }
        }
      },
      error: function() {
        swal('Data kosong :)');
      }
    });
  }

  function puyeng1() {
    b = document.getElementById("chkb");
    if (b.checked) {
      document.getElementById("chknb").checked = false;
      document.getElementById("chknbx").value = 'xxx';

      document.getElementById("chkr").checked = false;
      document.getElementById("chkrx").value = 'xxx';

      document.getElementById("chkbx").value = 'qqq';
    } else {
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkrx").value = 'xxx';
    }
  }

  function puyeng2() {
    nb = document.getElementById("chknb");
    if (nb.checked) {
      document.getElementById("chkb").checked = false;
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chkr").checked = false;
      document.getElementById("chkrx").value = 'xxx';
      document.getElementById("chknbx").value = 'qqq';
    } else {
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkrx").value = 'xxx';
    }
  }

  function puyeng3() {
    r = document.getElementById("chkr");
    if (r.checked) {
      document.getElementById("chkb").checked = false;
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknb").checked = false;
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkrx").value = 'qqq';
    } else {
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkrx").value = 'xxx';
    }
  }

  function puyeng4() {
    r = document.getElementById("chkl");
    if (r.checked) {
      document.getElementById("chkb").checked = false;
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknb").checked = false;
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkr").checked = false;
      document.getElementById("chkrx").value = 'xxx';
      document.getElementById("chklx").value = 'qqq';
    } else {
      document.getElementById("chkbx").value = 'xxx';
      document.getElementById("chknbx").value = 'xxx';
      document.getElementById("chkrx").value = 'xxx';
      document.getElementById("chklx").value = 'xxx';
    }
  }

  function buatperiode() {
    periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
    document.getElementById("iperiode").value = periode;
  }
</script>