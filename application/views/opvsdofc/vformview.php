<?php echo "<h2>$page_title</h2>"; ?>
<?php require_once ('php/fungsi.php'); ?>
<table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'opvsdofc/cform/cari','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="12" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			  <th>Supplier</th>
			  <th>OP</th>
        <th>Tgl OP</th>
			  <th>P. OP</th>
        <th>J. OP</th>
			  <th>Area</th>
        <th>Harga</th>
			  <th>DO</th>
        <th>Tgl DO</th>
        <th>P. DO</th>
			  <th>J. DO</th>
        <th>Sls Hr</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $row->v_product_mill	= number_format($row->v_product_mill);
        if($row->d_op){
			    $tmp=explode('-',$row->d_op);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_op=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_do!=''){
          if($row->d_do){
			      $tmp=explode('-',$row->d_do);
			      $tgl=$tmp[2];
			      $bln=$tmp[1];
			      $thn=$tmp[0];
			      $row->d_do=$tgl.'-'.$bln.'-'.$thn;
          }
        }else{
          $row->d_do='';
        }
        $selisih=datediff("d",$row->d_op,$row->d_do);
			  echo "<tr> 
				  <td>$row->e_supplier_name</td>
				  <td>$row->i_op</td>
				  <td>$row->d_op</td>
          <td>$row->i_product</td>
				  <td align=right>$row->n_order</td>
				  <td>$row->e_area_name</td>
          <td align=right>$row->v_product_mill</td>
				  <td>$row->i_do</td>
				  <td>$row->d_do</td>
          <td>$row->proddo</td>
				  <td align=right>$row->n_deliver</td>
          <td align=right>$selisih Hr</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
	</div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(a,b,c){
	document.getElementById("dfrom").value=a;
	document.getElementById("dto").value=b;
	document.getElementById("iarea").value=c;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/opvsdofc/cform/viewdetail";
	formna.submit();
  }
</script>
