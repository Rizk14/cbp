<div id='tmp'>
	<h2><?php echo $page_title; ?> on hand</h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'liststock/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div id="listform">
					<div class="effect">
						<div class="accordion2">
							<table class="listtable">
								<thead>
									<tr>
										<td colspan="9" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" placeholder="Kd/Nm Product / Kategori">
											<input readonly type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
											<input readonly type="hidden" id="istore" name="istore" value="<?php echo $istore; ?>">
											<input readonly type="hidden" id="istorelocation" name="istorelocation" value="<?php echo $istorelocation; ?>">
											<input readonly type="hidden" name="is_cari" value="1">
											<input type="submit" id="bcari" name="bcari" value="Cari">
										</td>
									</tr>
								</thead>

								<th>No</th>
								<th>Kategori Penjualan</th>
								<th width="9%">Area</th>
								<th>Kode Gudang</th>
								<th>Kode Product</th>
								<th>Grade</th>
								<th>Motif</th>
								<th>Nama Product</th>
								<th>Jumlah Stock</th>

								<tbody>
									<?php
									if ($isi) {
										$i = 0;
										foreach ($isi as $row) {
											$i++;
											echo "<tr> 
													<td align='center'>$i</td>
													<td align='center'>$row->e_sales_categoryname</td>
													<td align='center'>$row->e_area_name</td>
													<td align='center'>$row->i_store</td>
													<td align='center'>$row->i_product</td>
													<td align='center'>$row->i_product_grade</td>
													<td align='center'>$row->e_product_motifname</td>
													<td>$row->e_product_name</td>				  
													<td align='center'>$row->n_quantity_stock</td>
												</tr>";
										}
									}
									?>
								</tbody>
							</table>
							<!-- <#?php echo "<center>".$this->pagination->create_links()."</center>";?> -->
							<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('liststock/cform/','#main')">
						</div>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
</div>