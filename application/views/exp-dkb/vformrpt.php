<title>Exp Dkb</title>
<link rel="stylesheet" type="text/css" href="<?php  echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php  echo  $page_title; ?></h2>
<?php 
  include ("php/fungsi.php");
?>
<table class="maintable">
  <tr>
  <td align="left">
  <?php  echo  $this->pquery->form_remote_tag(array('url'=>'exp-dkb/cform/view','update'=>'#main','type'=>'post'));?>
  <div class="effect">
    <div class="accordion2">
      <table class="listtable" id="sitabel">
<?php 
foreach($isi as $row)
{
?>
  

<?php 
		$idkb	= $row->i_dkb;
		$iarea	= $row->i_area;
		$query 	= $this->db->query(" select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'",false);
		$jml 	= $query->num_rows();
		$i=0;	
		$kes=0;
		$kre=0;
		$vkes=0;
		$vkre=0;
?>
  <tr>
    <td width="31" rowspan="2"><div align="center">No</div></td>
    <td colspan="3"><div align="center">DEBITUR</div></td>
    <td colspan="4"><div align="center">SURAT JALAN </div></td>
    <td width="117" rowspan="2"><div align="center">KET</div></td>
    <td width="117" rowspan="2"><div align="center">SPB - SJ</div></td>
    <td width="117" rowspan="2"><div align="center">SJ - DKB</div></td>
  </tr>
  <tr>
    <td width="63"><div align="center">KODE</div></td>
    <td width="158"><div align="center">NAMA</div></td>
    <td width="121"><div align="center">KOTA</div></td>
    <td width="101"><div align="center">NOMOR</div></td>
    <td width="89"><div align="center">TANGGAL SJ</div></td>
    <td width="89"><div align="center">TANGGAL SPB</div></td>
    <td width="110"><div align="center">NILAI</div></td>
  </tr>
<?php 
		foreach($detail as $rowi){

			$i++;
			if($rowi->n_spb_toplength==0){
				$kes++;
				$vkes=$vkes+$rowi->v_jumlah;
			}else{ 
				$kre++;
				$vkre=$vkre+$rowi->v_jumlah;
			}
			if(strlen($i)==2)$aw=0;
			if(strlen($i)==1)$aw=1;
			$aw=str_repeat(" ",$aw);
			$cust	= strval($rowi->i_customer);
			$name	= $rowi->e_customer_name;
			if(strlen($name)>27){
				$name=substr($name,0,27);
			}
			$name	= $name.str_repeat(" ",27-strlen($name));
			$city	= $rowi->e_customer_city;
			if(strlen($city)>27){
				$city=substr($city,0,27);
			}
			$city	= $city.str_repeat(" ",27-strlen($city));
			$sls	= $rowi->i_salesman;
			$isj	= $rowi->i_sj;
      $tmp=explode("-",$rowi->d_sj);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dsj  = $hr."-".$bl."-".$th;
      $tmp=explode("-",$rowi->d_spb);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dspb  = $hr."-".$bl."-".$th;
			$vsj	= $rowi->v_jumlah;
			$ket	= $rowi->e_remark;
			$ket	= $ket.str_repeat(" ",24-strlen($ket));
			$sj  		= strtotime($dsj);
			$dkb 		= strtotime($row->d_dkb);
			$tspb 		= strtotime($rowi->d_terimaspb);

			if($dkb!=''){
			#$spb2 	= $dkeuangan-$dsales;
			$sjdkb 	= $dkb-$sj;
			$dsjdkb	= floor($sjdkb / (60 * 60 * 24))." hari";
			}else{
			$dsjdkb	= '-';
			}

			/*if($sj!=''){
			#$spb2 	= $dkeuangan-$dsales;
			$dspbsj 	= $sj-$tspb;
			$spbsj	= floor($dspbsj / (60 * 60 * 24))." hari";
			}else{
			$spbsj	= '-';
			}*/

		$dsales  	= strtotime($rowi->d_approvesales);
		$dkeuangan  = strtotime($rowi->d_approve);
		if($dsales!=''){
			$spbx2 	= $dkeuangan-$dsales;
		}
			//HITUNG JML HARI SPB - SJ
		if($sj!=''){
			#$spb2 	= $dkeuangan-$dsales;
			#$keusj 	= $sj-$dkeuangan;
			#$dspbsj	= floor($keusj / (60 * 60 * 24))." hari";
			if($spbx2<0){
			$salesj	= $sj-$dsales;
			$dspbsj	= floor($salesj / (60 * 60 * 24))." hari";
		}else{
			$salesj	= $sj-$dkeuangan;
			$dspbsj	= floor($salesj / (60 * 60 * 24))." hari";
		}
		}
		else{
			$dspbsj	= '-';
		}
?>
  <tr>
    <td width="31"><?php echo $i; ?></td>
    <td width="63"><?php echo $cust; ?></td>
    <td width="159"><?php echo $name; ?></td>
    <td width="122"><?php echo $city; ?></td>
    <td width="100"><?php echo $isj; ?></td>
    <td width="87"><?php echo $dsj; ?></td>
    <td width="87"><?php echo $dspb; ?></td>
    <td width="109" align="right"><?php echo $vsj; ?></td>
    <td width="118"><?php echo $ket; ?></td>
    <td width="118"><?php echo $dspbsj; ?></td>
    <td width="118"><?php echo $dsjdkb; ?></td>
  </tr>
<?php 
    }
		$vsj	= $row->v_dkb;
		$tmp=explode("-",$row->d_dkb);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$ddkb=$hr." ".mbulan($bl)." ".$th;	
		$vkes=number_format($vkes);
		$vkre=number_format($vkre);
		$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
?>
  <tr>
    <td width="570" align="right" colspan="7">Jumlah Rp. </td>
    <td width="172" align="right"><?php echo $vsj; ?></td>
    <td colspan="3"></td>
  </tr>
<br>
 
  </table>
<?php 
}
?>
<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
</BODY>
</html>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>

