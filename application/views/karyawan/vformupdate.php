    <table class="maintable">
      <tr>
    	<td align="left">
	      <?php echo $this->pquery->form_remote_tag(array('url'=>'karyawan/cform/update','update'=>'#main','type'=>'post'));?>
	      <div id="masterkaryawanform">
	      <table class="mastertable">
	      	<tr>
	      	<?php 
			if($isi->d_join_date!=''){
				$tmp=explode('-',$isi->d_join_date);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$isi->d_join_date=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		  <td width="19%">Tanggal Bergabung</td>
		  <marquee direction="right">*Kode Nik Otomatis Di Random Sistem(Tanggal Gabung Wajib di Isi)</marquee>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_join_date" id="d_join_date" value="<?php echo $isi->d_join_date;?>" readonly>
		    <input type="hidden" name="i_nik" id="i_nik" value="<?php echo $isi->i_nik;?>" readonly>
		  </td>
		  </td>
		  <td width="19%">Nama Karyawan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nama_karyawan_lengkap" id="e_nama_karyawan_lengkap" value="<?php echo $isi->e_nama_karyawan_lengkap;?>" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Department</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    	  <input type="hidden" name="i_department" id="i_department" value="<?php echo $isi->i_department;?>" 
					   onclick='showModal("karyawan/cform/department/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_department_name" id="e_department_name" value="<?php echo $isi->e_department_name;?>" 
					   onclick='showModal("karyawan/cform/department/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Nama Panggilan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nama_karyawan_panggilan" id="e_nama_karyawan_panggilan" value="<?php echo $isi->e_nama_karyawan_panggilan;?>" maxlength='15'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Area</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="i_area" id="i_area" value="<?php echo $isi->i_area;?>" 
					   onclick='showModal("karyawan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_area_name" id="e_area_name" value="<?php echo $isi->e_area_name;?>" 
					   onclick='showModal("karyawan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Jenis Kelamin</td>
		  <td width="1%">:</td>
		  <td width="30%">
		  	<select name="e_jenis_kelamin" id="e_jenis_kelamin">
		  		<?php if ($e_jenis_kelamin=='P'){?>
		  		<option selected value="P">Perempuan</option>
		  		<?php }else{?>
		  		<option selected value="L">Laki-Laki</option>
		  		<?php }?>
		  		<option value="L">Laki-Laki</option>
		  		<option value="P">Perempuan</option>
		  	</select>
		  	</td>
	      	<tr>
		  <td width="19%">Status Karyawan</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="i_karyawan_status" id="i_karyawan_status" value="<?php echo $isi->i_karyawan_status;?>" 
					   onclick='showModal("karyawan/cform/karyawan_status/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_karyawan_status" id="e_karyawan_status" value="<?php echo $isi->e_karyawan_status;?>" 
					   onclick='showModal("karyawan/cform/karyawan_status/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Tempat Lahir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		  		  <input type="text" name="e_tempat_lahir" id="e_tempat_lahir" value="<?php echo $isi->e_tempat_lahir;?>" maxlength='15'></td>
	      	</tr>
	      	<tr>
	      	<?php 
				if($isi->d_kontrak_ke1_awal!=''){
					$tmp=explode('-',$isi->d_kontrak_ke1_awal);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke1_awal=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontrak 1 Awal</td>
		  <td width="1%">:</td>
		  <td width="30%">
		  	<input type="text" name="d_kontrak_ke1_awal" id="d_kontrak_ke1_awal" value="<?php echo $isi->d_kontrak_ke1_awal;?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke1_awal',0,18,1)"></td>
		  <td width="19%">Tanggal Lahir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		  	<?php 
				if($isi->d_lahir!=''){
					$tmp=explode('-',$isi->d_lahir);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_lahir=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  	<input type="text" name="d_lahir" id="d_lahir" value="<?php echo $isi->d_lahir;?>" readonly onclick="showCalendar('',this,this,'','d_lahir',0,18,1)"></td>
	      	</tr>
		<tr>
			<?php 
				if($isi->d_kontrak_ke1_akhir!=''){
					$tmp=explode('-',$isi->d_kontrak_ke1_akhir);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke1_akhir=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontrak 1 Akhir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_kontrak_ke1_akhir" id="d_kontrak_ke1_akhir" value="<?php echo $isi->d_kontrak_ke1_akhir?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke1_akhir',0,18,1)"></td>
		  <td width="19%">Agama</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_agama" id="e_agama" value="<?php echo $isi->e_agama;?>" maxlength='10'></td>
	      	</tr>
		<tr>
			<?php 
				if($isi->d_kontrak_ke2_awal!=''){
					$tmp=explode('-',$isi->d_kontrak_ke2_awal);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke2_awal=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontak 2 Awal</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_kontrak_ke2_awal" id="d_kontrak_ke2_awal" value="<?php echo $isi->d_kontrak_ke2_awal;?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke2_awal',0,18,1)"></td>
		  <td width="19%">No KTP</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_no_ktp" id="e_no_ktp" value="<?php echo $isi->e_no_ktp;?>" maxlength='20'></td>
	      	</tr>
	    <tr>
	    	<?php 
				if($isi->d_kontrak_ke2_akhir!=''){
					$tmp=explode('-',$isi->d_kontrak_ke2_akhir);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke2_akhir=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontak 2 Akhir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_kontrak_ke2_akhir" id="d_kontrak_ke2_akhir" value="<?php echo $isi->d_kontrak_ke2_akhir;?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke2_akhir',0,18,1)"></td>
		  <td width="19%">No KK</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_no_kk" id="e_no_kk" value="<?php echo $isi->e_no_kk?>" maxlength='20'></td>
	      	</tr>
	    <tr>
	    	<?php 
				if($isi->d_kontrak_ke3_awal!=''){
					$tmp=explode('-',$isi->d_kontrak_ke3_awal);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke3_awal=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontak 3 Awal</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_kontrak_ke3_awal" id="d_kontrak_ke3_awal" value="<?php echo $isi->d_kontrak_ke3_awal;?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke3_awal',0,18,1)"></td>
		  <td width="19%">Alamat KTP</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <textarea type="text" name="e_alamat_ktp" id="e_alamat_ktp"><?php echo $isi->e_alamat_ktp;?></textarea></td>
	      	</tr>
	    <tr>
	    	<?php 
				if($isi->d_kontrak_ke3_akhir!=''){
					$tmp=explode('-',$isi->d_kontrak_ke3_akhir);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kontrak_ke3_akhir=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
		  <td width="19%">Kontak 3 Akhir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="d_kontrak_ke3_akhir" id="d_kontrak_ke3_akhir" value="<?php echo $isi->d_kontrak_ke3_akhir;?>" readonly onclick="showCalendar('',this,this,'','d_kontrak_ke3_akhir',0,18,1)"></td>
		  <td width="19%">Alamat Sekarang</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <textarea type="text" name="e_alamat_sekarang" id="e_alamat_sekarang"><?php echo $isi->e_alamat_sekarang;?></textarea></td>
	      	</tr>
	    <tr>
		  <td width="19%">Daerah</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_karyawan_daerah" id="e_karyawan_daerah" value="<?php echo $isi->e_karyawan_daerah?>" maxlength='20'></td>
		  <td width="19%">Pendidikan Terakhir</td>
		  <td width="1%">:</td>
		  <td width="30%">
		   	<select name="e_pendidikan_terakhir" id="e_pendidikan_terakhir">
		  		<option selected value="<?php echo $isi->e_pendidikan_terakhir;?>"><?php echo $isi->e_pendidikan_terakhir;?></option>
		  		<option value="SD">SD</option>
		  		<option value="SMP">SMP</option>
		  		<option value="SMA">SMA</option>
		  		<option value="D1">D1</option>
		  		<option value="D3">D3</option>
		  		<option value="Sarjana">Sarjana</option>
		  		<option value="Magister">Magister</option>
		  	</select>
		  </td>
	      	</tr>
		<tr>
		  <td width="19%">BPJS Ketenagakerjaan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nomor_bpjs_ketenagakerjaan" id="e_nomor_bpjs_ketenagakerjaan" value="<?php echo $isi->e_nomor_bpjs_ketenagakerjaan;?>" maxlength='20'></td>
		  <td width="19%">Jurusan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_jurusan" id="e_jurusan" value="<?php echo $isi->e_jurusan;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">BPJS Kesehatan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nomor_bpjs_kesehatan" id="e_nomor_bpjs_kesehatan" value="<?php echo $isi->e_nomor_bpjs_kesehatan;?>" maxlength='20'></td>
		  <td width="19%">Asal Sekolah/Universitas</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_asal_universitas_sekolah" id="e_asal_universitas_sekolah" value="<?php echo $isi->e_asal_universitas_sekolah;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">NPWP</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nomor_npwp" id="e_nomor_npwp" value="<?php echo $isi->e_nomor_npwp;?>" maxlength='20'></td>
		  <td width="19%">Status Pernikahan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_status_pernikahan" id="e_status_pernikahan" value="<?php echo $isi->e_status_pernikahan;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">Sim A</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_sim_a" id="e_sim_a" value="<?php echo $isi->e_sim_a;?>" maxlength='20'></td>
		  <td width="19%">Nama Pasangan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nama_pasangan" id="e_nama_pasangan" value="<?php echo $isi->e_nama_pasangan;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">SIM B</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_sim_b" id="e_sim_b" value="<?php echo $isi->e_sim_b;?>" maxlength='20'></td>
		  <td width="19%">Alamat Pasangan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_alamat_pasangan" id="e_alamat_pasangan" value="<?php echo $isi->e_alamat_pasangan;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">SIM C</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_sim_c" id="e_sim_c" value="<?php echo $isi->e_sim_c;?>" maxlength='20'></td>
		  <td width="19%">No Telepon</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nomor_telepon" id="e_nomor_telepon" value="<?php echo $isi->e_nomor_telepon;?>"></td>
	      	</tr>
	    <tr>
		  <td width="19%">Jabatan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_karyawan_jabatan" id="e_karyawan_jabatan" value="<?php echo $isi->e_karyawan_jabatan;?>" maxlength='20'></td>
		</tr>
		<tr>
		  <td width="19%">Penempatan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_karyawan_penempatan" id="e_karyawan_penempatan" value="<?php echo $isi->e_karyawan_penempatan;?>" maxlength='20'></td>
		</tr>
	      	<tr>
		  <td width="19%">&nbsp;</td>
		  <td width="1%">&nbsp;</td>
		  <td colspan=4 width="80%">
      <?php 
        $departement =$this->session->userdata('departement');
        if($departement =='0' or $departement =='6'){?>
		    <input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
		    <?php }?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('karyawan/cform/','#main');">
		  </td>
	       </tr>
	      </table>
	      </div>
	<?=form_close()?>
        </td>
      </tr> 
    </table>
<script language="javascript" type="text/javascript">
  function view_productgroup(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productgroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_productstatus(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productstatus","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_producttype(a){
    lebar =450;
    tinggi=400;
//    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/producttype/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
    showModal("productbase/cform/producttype/"+a+"/","#light");
  	jsDlgShow("#konten *", "#fade", "#light");

  }
  function view_productcategory(a){
//    lebar =450;
//    tinggi=400;
//    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productcategory/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
    showModal("productbase/cform/productcategory/"+a+"/","#light");
  	jsDlgShow("#konten *", "#fade", "#light");
  }
  function view_productclass(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productclass","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function cek(){
    kode=document.getElementById("iproduct").value;
    nama=document.getElementById("eproductname").value;
    if(kode=='' || nama==''){
	alert("Minimal kode Produk dan nama Produk diisi terlebih dahulu !!!");
    }
  }
</script>
