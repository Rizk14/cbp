<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('product/cform', array('id' => 'masterproductform'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="16%">Kode Barang</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'iproduct',
			              'id'          => 'iproduct',
			              'value'       => '',
				      'readonly'    => 'true',
			              'maxlength'   => '9');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="16%">Nama Barang</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'eproductname',
			              'id'          => 'eproductname',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="16%">Nama Dasar Barang</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'eproductbasename',
			              'id'          => 'eproductbasename',
			              'value'       => '',
			              'readonly'    => 'true',
				      'onclick'	    => 'view_productbase();');
				echo form_input($data);
				?>
		<input type="hidden" id="iproductbase" name="iproductbase" value=""></td>
	      </tr>
	      <tr>
		<td width="16%">Nama Motif Barang</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'eproductmotifname',
			              'id'          => 'eproductmotifname',
			              'value'       => '',
			              'readonly'    => 'true',
				      'onclick'	    => 'view_productmotif();');
				echo form_input($data);
				?>
		<input type="hidden" id="iproductmotif" name="iproductmotif" value=""></td>
	      </tr>
	      <tr>
		<td width="16%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="83%">
		  <input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="reset">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('product/cform/cari', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Barang</th>
	    <th>Nama Barang</th>
	    <th>Motif</th>
	    <th>Tgl Register</th>
	    <th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td>$row->i_product</td>
				  <td>$row->e_product_name</td>
				  <td>$row->e_product_motifname</td>
				  <td>$row->d_product_entry</td>
				  <td class=\"action\"><a href=\"javascript:yyy('".$row->i_product."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;&nbsp;<a href=\"javascript:xxx('".$row->i_product."','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>
				</tr>";
			}
		echo "<input type=\"hidden\" id=\"iproductdelete\" name=\"iproductdelete\" value=\"\">
		      <input type=\"hidden\" id=\"iproductedit\" name=\"iproductedit\" value=\"\">";
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b){
    if (confirm(b)==1){
	  document.getElementById("iproductdelete").value=a;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/product/cform/delete";
	  formna.submit();
    }
  }
  function yyy(b){
	document.getElementById("iproductedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/product/cform/edit";
	formna.submit();
  }
  function view_productbase(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/product/cform/productbase","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_productmotif(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/product/cform/productmotif","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
