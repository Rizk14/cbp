<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('product/cform/cariproductmotif', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="2" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Dasar Barang</th>
	    <th>Keterangan</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_product_motif','$row->e_product_motifname')\">$row->i_product_motif</a></td>
				  <td><a href=\"javascript:setValue('$row->i_product_motif','$row->e_product_motifname')\">$row->e_product_motifname</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    if((opener.document.getElementById("iproduct").value != '') && (opener.document.getElementById("iproduct").value.length==7)){
	opener.document.getElementById("iproduct").value=opener.document.getElementById("iproduct").value+a;
    }else if((opener.document.getElementById("iproduct").value != '') && (opener.document.getElementById("iproduct").value.length==9)){
    	x=opener.document.getElementById("iproduct").value.substring(0,7);
	opener.document.getElementById("iproduct").value=x+a;
    }else{
	opener.document.getElementById("iproduct").value=a;
    };
    opener.document.getElementById("iproductmotif").value=a;
    opener.document.getElementById("eproductmotifname").value=b;
    this.close();
  }
</script>
