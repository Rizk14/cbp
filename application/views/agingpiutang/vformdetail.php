<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body id="bodylist">
  <div id="main">
    <div id="tmpx">
      <table class="maintable" id="sitabel">
        <tr>
          <td colspan="1">
            <?php echo "<center><h2>$icustomergroupar - $ecustomername <br> Jatuhtempo $djt</h2></center>"; ?>
          </td>
        </tr>
        <tr>
          <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url' => 'agingpiutang/cform/detail', 'update' => '#light', 'type' => 'post')); ?>
            <div id="listform">
              <div class="effect">
                <div class="accordion2">
                  <table class="listtable">
                    <th>No</th>
                    <th>Kode Pelanggan</th>
                    <th>Nota</th>
                    <th>Tgl Nota</th>
                    <th>Tgl JT</th>
                    <th>No SJ</th>
                    <th>Jumlah</th>
                    <th>Sisa</th>
                    <th>Lama</th>
                    <th>TOP</th>
                    <th>Jenis</th>
                    <tbody>
                      <?php
                      if ($isi) {
                        $no = 0;
                        #####
                        $tmp   = explode("-", $djt);
                        $det  = $tmp[0];
                        $mon  = $tmp[1];
                        $yir   = $tmp[2];
                        $djt  = $yir . "-" . $mon . "-" . $det;

                        $pecah1 = explode("-", $djt);
                        $date1 = $pecah1[2];
                        $month1 = $pecah1[1];
                        $year1 = $pecah1[0];
                        $jd1 = GregorianToJD($month1, $date1, $year1);
                        #####
                        foreach ($isi as $row) {
                          $tgl2 = $row->d_jatuh_tempo;
                          $pecah2 = explode("-", $tgl2);
                          $date2 = $pecah2[2];
                          $month2 = $pecah2[1];
                          $year2 =  $pecah2[0];
                          $jd2 = GregorianToJD($month2, $date2, $year2);
                          $lama = $jd1 - $jd2;
                          $tmp   = explode("-", $row->d_nota);
                          $det  = $tmp[2];
                          $mon  = $tmp[1];
                          $yir   = $tmp[0];
                          $row->d_nota  = $det . "-" . $mon . "-" . $yir;
                          $tmp   = explode("-", $row->d_jatuh_tempo);
                          $det  = $tmp[2];
                          $mon  = $tmp[1];
                          $yir   = $tmp[0];
                          $row->d_jatuh_tempo  = $det . "-" . $mon . "-" . $yir;
                          if ($row->i_product_group == '00') {
                            $jenis = 'Home';
                          } elseif ($row->i_product_group == '01') {
                            $jenis = 'Bd';
                          } else {
                            $jenis = 'NB';
                          }
                          $no++;
                          echo "<tr> 
                                  <td style='text-align:center'>$no</td>
                                  <td style='text-align:center'>$row->i_customer</td>
                                  <td style='text-align:center'>$row->i_nota</td>
                                  <td style='text-align:center'>$row->d_nota</td>
                                  <td style='text-align:center'>$row->d_jatuh_tempo</td>
                                  <td style='text-align:center'>$row->i_sj</td>
                                  <td style='text-align:right'>" . number_format($row->v_nota_netto) . "</td>
                                  <td style='text-align:right'>" . number_format($row->v_sisa) . "</td>
                                  <td style='text-align:center'>$lama</td>
                                  <td style='text-align:center'>$row->n_nota_toplength</td>
                                  <td style='text-align:center'>$jenis</td>
                                </tr>";
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                  <?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
                  <br>

                </div>
              </div>
            </div>
            <?= form_close() ?>
            <center>
              <a href="<?php echo site_url('agingpiutang/cform/exportdetail/' . $icustomergroupar . '/' . $djt . '/' . $ecustomername); ?>" target='blank'><button>Export</button></a>
              <input type="button" id="tutup" name="tutup" value="Tutup" onclick="bbatal()">
            </center>
          </td>
        </tr>
      </table>
    </div>
  </div>
  </div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
  function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
  }

  $("#expdet").click(function() {
    //getting values of current time for generating the file name
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
    //creating a temporary HTML link element (they support setting file names)
    var a = document.createElement('a');
    //getting data from our div that contains the HTML table
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = $('#sitabel').html();
    // var table_html = table_div.outerHTML.replace(/ /g, '%20');
    a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

    //setting the file name
    // var nofaktur = document.getElementById('no_faktur').value;
    a.download = 'export_detail_keterlambatancust_' + postfix + '.xls';
    //triggering the function
    a.click();
    //just in case, prevent default behaviour

    // var Contents = $('#sitabel').html();
    // window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
  });
</script>