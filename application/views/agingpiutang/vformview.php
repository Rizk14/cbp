<div id='tmp'>
  <h2><?php echo $page_title . ' Jatuh Tempo ' . $djt . ' (Tanpa Toleransi)'; ?></h2>
  <table class="maintable">
    <tr>
      <td align="left">
        <?php echo $this->pquery->form_remote_tag(array('url' => 'agingpiutang/cform/view', 'update' => '#main', 'type' => 'post')); ?>
        <div class="effect">
          <div class="accordion2">
            <table class="listtable">
              <thead>
                <tr>
                  <td colspan="12" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="djt" name="djt" value="<?php echo $djt; ?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
                </tr>
              </thead>
              <th>Kode Pelanggan</th>
              <th>Nama Pelanggan</th>
              <th>TOP</th>
              <th>Saldo Piutang</th>
              <th>Blm JT</th>
              <th>1-7</th>
              <th>8-30</th>
              <th>31-60</th>
              <th>61-90</th>
              <th>>90</th>
              <th>Jenis Pelanggan</th>
              <th class="action">Action</th>
              <tbody>
                <?php
                if ($isi) {
                  if ($djt) {
                    $tmp   = explode("-", $djt);
                    $det  = $tmp[0];
                    $mon  = $tmp[1];
                    $yir   = $tmp[2];
                    $djtx  = $yir . "/" . $mon . "/" . $det;
                  } else {
                    $djtx = '';
                  }
                  foreach ($isi as $row) {
                    $query = $this->db->query(" select a.n_customer_toplength, b.e_customer_classname 
                                  from tr_customer a, tr_customer_class b
                                  where a.i_customer='$row->i_customer' and a.i_customer_class=b.i_customer_class");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $top = $tmp->n_customer_toplength;
                        $clas = $tmp->e_customer_classname;
                      }
                    }
                    if ($top < 0) $top = $top * -1;
                    $toptujuh = -7;
                    $toplapan = -30;
                    $toptigasatu = -60;
                    $topenamsatu = -90;
                    $dudet  = $this->fungsi->dateAdd("d", 0, $djtx);
                    $djatuhtempo = $dudet;
                    $sisa = number_format($row->v_sisa);
                    $vsisaaman = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo >= '$djatuhtempo' and v_sisa>0 and f_nota_cancel='f'
                                  and f_ttb_tolak='f' and not i_nota isnull group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisaaman = $tmp->v_sisa;
                      }
                    }
                    $vsisaaman = number_format($vsisaaman);
                    $dudettujuh  = $this->fungsi->dateAdd("d", $toptujuh, $djtx);
                    $vsisatujuh = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$djatuhtempo' and d_jatuh_tempo >= '$dudettujuh' 
                                  and v_sisa>0 and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisatujuh = $tmp->v_sisa;
                      }
                    }
                    $vsisatujuh = number_format($vsisatujuh);
                    $dudetlapan  = $this->fungsi->dateAdd("d", $toplapan, $djtx);
                    $vsisalapan = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudettujuh' and d_jatuh_tempo >= '$dudetlapan' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisalapan = $tmp->v_sisa;
                      }
                    }
                    $vsisalapan = number_format($vsisalapan);
                    $dudettigasatu  = $this->fungsi->dateAdd("d", $toptigasatu, $djtx);
                    $vsisatiga = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetlapan' and d_jatuh_tempo >= '$dudettigasatu' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisatiga = $tmp->v_sisa;
                      }
                    }
                    $vsisatiga = number_format($vsisatiga);
                    $dudetenamsatu  = $this->fungsi->dateAdd("d", $topenamsatu, $djtx);
                    $vsisaenam = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudettigasatu' and d_jatuh_tempo >= '$dudetenamsatu' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisaenam = $tmp->v_sisa;
                      }
                    }
                    $vsisaenam = number_format($vsisaenam);
                    $dudetlalan  = $this->fungsi->dateAdd("d", $topenamsatu, $djtx);
                    $vsisalalan = 0;
                    $query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetenamsatu' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $vsisalalan = $tmp->v_sisa;
                      }
                    }
                    $vsisalalan = number_format($vsisalalan);
                    $query = $this->db->query(" select i_customer_groupar
                                  from tr_customer_groupar
                                  where i_customer='$row->i_customer'");
                    if ($query->num_rows() > 0) {
                      foreach ($query->result() as $tmp) {
                        $groupar = $tmp->i_customer_groupar;
                      }
                    }
                    $nama = $row->e_customer_name;
                    $nama = str_replace("'", "", $nama);
                    $nama  = str_replace("(", "tandakurungbuka", $nama);
                    $nama  = str_replace("&", "tandadan", $nama);
                    $nama  = str_replace(")", "tandakurungtutup", $nama);
                    $nama = str_replace('.', 'tandatitik', $nama);
                    $nama = str_replace(',', 'tandakoma', $nama);
                    $nama = str_replace('/', 'tandagaring', $nama);
                    $nama = str_replace(';', 'tandatikom', $nama);
                    echo "  <td>$row->i_customer</td>
                            <td>$row->e_customer_name</td>
                            <td align=right>$top</td>
                            <td align=right>$sisa</td>
                            <td align=right>$vsisaaman</td>
                            <td align=right>$vsisatujuh</td>
                            <td align=right>$vsisalapan</td>
                            <td align=right>$vsisatiga</td>
                            <td align=right>$vsisaenam</td>
                            <td align=right>$vsisalalan</td>
                            <td align=right>$clas</td>
                            <td class=\"action\">";
                    echo "<a href=\"javascript:yyy('" . $groupar . "','" . $nama . "','" . $djt . "');\"><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                    echo "</td></tr>";
                  }
                }
                ?>
              </tbody>
            </table>
            <?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
          </div>
        </div>
        <?= form_close() ?>
      </td>
    </tr>
  </table>

  <a href="<?php echo site_url('agingpiutang/cform/exportheader/' . $djt . '/' . $iarea); ?>" target='blank'><button>Export</button></a>
  <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('agingpiutang/cform/','#main')">
</div>
<script language="javascript" type="text/javascript">
  function yyy(b, d, c) {
    showModal("agingpiutang/cform/detail/" + b + "/" + c + "/" + d + "/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
</script>