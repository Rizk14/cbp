<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'spbrealisasi/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input id="ispb" name="ispb" value="<?php echo $ispb; ?>" readonly>
			<input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
		    <input id="ispb" name="ispb" type="hidden" value="<?php echo $isi->i_spb; ?>"></td>
		<td>Kelompok Harga</td>
		<td><input readonly id="epricegroupname" name="epricegroupname" 
			 	   value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Promo</td>
		<td width="38%"><input readonly id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name; ?>" 
			 	   readonly >
		    <input id="ispbprogram" name="ispbprogram" type="hidden" value="<?php echo $isi->i_spb_program; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input id="vspb" name="vspb" readonly value="<?php echo number_format($isi->v_spb); ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>" 
			 	   readonly >
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_spb_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>" readonly >
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2"
				   value="<?php echo $isi->n_spb_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_spb_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3"
				   value="<?php echo $isi->n_spb_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_spb_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>Gudang</td>
		<td><input id="estorename" name="estorename" value="" readonly 
			onclick='showModal("spbrealisasi/cform/store/<?php echo $jmlitem; ?>/<?php echo $ispb; ?>/<?php echo $isi->f_spb_stockdaerah; ?>/<?php echo $isi->i_area; ?>/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
			<input id="istore" name="istore" type="hidden" value=""></td>
		<td>Discount 4</td>
		<td><input readonly id="ncustomerdiscount4" name="ncustomerdiscount4"
				   value="<?php echo $isi->n_spb_discount4; ?>">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" 
				   value="<?php echo number_format($isi->v_spb_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td>Lokasi Gudang</td>
		<td><input id="estorelocationname" name="estorelocationname" value="" readonly>
			<input id="istorelocation" name="istorelocation" type="hidden" value=""></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_spb_toplength; ?>">&nbsp;Konsiyasi <input id="fspbconsigment"
				   name="fspbconsigment" type="checkbox" readonly 
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>   Stock Daerah
				   <input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" 
				   <?php if($isi->f_spb_stockdaerah=='t') echo 'checked'; ?> readonly></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" readonly
				   value="<?php echo number_format($tmp); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname"
				   value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden"
				   value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly></td>
	      </tr>
		<tr>
		<td>Stok Daerah</td>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly>
		    <input readonly <?php if($isi->f_spb_stockdaerah=='f') echo "readonly"; ?> id="dsj" name="dsj" value=""
			   onclick="showCalendar('',this,this,'','dsj',0,20,1)"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input  readonly id="vspbafter" name="vspbafter" value="<?php echo $isi->v_spb_after;?>"></td>
		</tr>
		<tr>
		  <td>PKP</td>
		  <td><input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
				   value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		  <td>Keterangan</td>
		  <td>
		    <input name="eapprove1" id="eapprove1" value="" type="text"></td>
		</tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value),'<?php echo $isi->f_spb_stockdaerah; ?>');">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spbrealisasi/cform/","#main")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:248px;" align="center">Nama Barang</th>
					<th style="width:94px;" align="center">Motif</th>
					<th style="width:85px;"  align="center">Harga</th>
					<th style="width:36px;"  align="center">Jml Pesan</th>
					<th style="width:100px;"  align="center">Total</th>
					<th style="width:36px;"  align="center">Jml Dlv</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\">";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$nilai=number_format($row->v_unit_price,2);
					$jujum=number_format($row->n_order,0);
					$ntot =number_format($row->v_unit_price*$row->n_order,2);
				  	echo '<tbody>
							<tr>
								<td style="width:24px;">
									<input style="width:24px;" readonly type="text" id="baris'.$i.'" 
									name="baris'.$i.'" value="'.$i.'">
									<input type="hidden" id="motif'.$i.'" name="motif'.$i.'" value="'.$row->i_product_motif.'">
									<input type="hidden" id="grade'.$i.'" name="grade'.$i.'" value="'.$row->i_product_grade.'">
                  <input type="hidden" id="iproductstatus'.$i.'" name="iproductstatus'.$i.'" value="'.$row->i_product_status.'">
								</td>
								<td style="width:64px;">
									<input style="width:64px;" readonly type="text" id="iproduct'.$i.'"
									name="iproduct'.$i.'" value="'.$row->i_product.'">
								</td>
								<td style="width:250px;">
									<input style="width:250px;" readonly type="text" id="eproductname'.$i.'"
									name="eproductname'.$i.'" value="'.$row->e_product_name.'">
								</td>
								<td style="width:94px;">
									<input readonly style="width:94px;"  type="text" id="emotifname'.$i.'"
									name="emotifname'.$i.'" value="'.$row->e_product_motifname.'">
								</td>
								<td style="width:84px;">
									<input readonly style="text-align:right; width:84px;" type="text"
									id="vproductretail'.$i.'" name="vproductretail'.$i.'" value="'.$nilai.'">
								</td>
								<td style="width:48px;">
									<input style="text-align:right; width:48px;" type="text" id="norder'.$i.'"
									name="norder'.$i.'" readonly value="'.$jujum.'">
								</td>
								<td style="width:100px;">
									<input readonly style="text-align:right; width:100px;" type="text"
									id="vtotal'.$i.'" name="vtotal'.$i.'" value="'.$ntot.'">
                  <input type="hidden" id="eremark'.$i.'" name="eremark'.$i.'" value="'.$row->e_remark.'">
								</td>
								<td style="width:36px;"><input style="text-align:right; width:36px;" type="text" 
									id="ndeliver'.$i.'" name="ndeliver'.$i.'" value="" onkeyup=\'hitungnilai(this.value,'.$jmlitem.')\'
                  onblur=\'hitungnilai(this.value,'.$jmlitem.')\' onpaste=\'hitungnilai(this.value,'.$jmlitem.')\' 
                  autocomplete=\'off\'><input type="hidden" id="nstock'.$i.'" name="nstock'.$i.'" value="">
								</td>
							</tr>
						</tbody>';
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
					  <input type=\"hidden\" id=\"iareadelete\" 			name=\"iareadelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(b,c){
	document.getElementById("ispbedit").value=b;
	document.getElementById("iareaedit").value=c;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spb/cform/edit";
	formna.submit();
  }
  function view_store(a,b,c,d){
    lebar =450;
    tinggi=400;
//	alert(a);
    eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/store/"+a+"/"+b+"/"+c+"/"+d,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a,b){
	cek='false';
	if(document.getElementById("istore").value!=''){
		if(a==0){
			alert('Isi data item minimal 1 !!!');
		}else{
			for(i=1;i<=a;i++){
				if((document.getElementById("ndeliver"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
			document.getElementById("login").disabled=true;
		}else{
			document.getElementById("login").disabled=false;
		}
	}else{
		alert('Data header masih ada yang salah !!!');
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function hitungnilai(isi,jml){
    jml=document.getElementById("jml").value;
    if (isNaN(parseFloat(isi))){
		  alert("Input harus numerik");
	  }else{
      salah=false;
      gud=document.getElementById("istore").value;
      if(gud!='AA'){
        for(i=1;i<=jml;i++){
          stock  =formatulang(document.getElementById("nstock"+i).value);
          deliver=formatulang(document.getElementById("ndeliver"+i).value);
          order=formatulang(document.getElementById("norder"+i).value);
          if(parseFloat(stock)<0)stock=0;
          if(parseFloat(deliver)>parseFloat(stock)){
            alert('Jumlah Kirim melebihi jumlah stock');
            document.getElementById("ndeliver"+i).value=0;
            salah=true;
            break;
          }else if(parseFloat(deliver)>parseFloat(order)){
            alert('Jumlah Kirim melebihi jumlah pesan');
            document.getElementById("ndeliver"+i).value=0;
            salah=true;
            break;
          }
        }
      }else{
        for(i=1;i<=jml;i++){
          deliver=formatulang(document.getElementById("ndeliver"+i).value);
          order=formatulang(document.getElementById("norder"+i).value);
          if(parseFloat(deliver)>parseFloat(order)){
            alert('Jumlah Kirim melebihi jumlah pesan');
            document.getElementById("ndeliver"+i).value=0;
            salah=true;
            break;
          }
        }
      }

      if(!salah){
		    dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		    dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		    dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		    vdis1=0;
		    vdis2=0;
		    vdis3=0;
		    vdis1x=0;
		    vdis2x=0;
		    vdis3x=0;

		    vtot =0;
		    vtotx=0;
		    for(i=1;i<=jml;i++){
			    vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			    vhrgx=formatulang(document.getElementById("vproductretail"+i).value);
			    nqty=formatulang(document.getElementById("norder"+i).value);
          nqtyx=formatulang(document.getElementById("ndeliver"+i).value);
			    vhrg=parseFloat(vhrg)*parseFloat(nqty);
			    vhrgx=parseFloat(vhrgx)*parseFloat(nqtyx);
			    vtot=vtot+vhrg;
			    vtotx=vtotx+vhrgx;
			    document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		    }
		    vdis1=vdis1+((vtot*dtmp1)/100);
		    vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		    vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		    vdis1x=vdis1x+((vtotx*dtmp1)/100);
		    vdis2x=vdis2x+(((vtotx-vdis1x)*dtmp2)/100);
		    vdis3x=vdis3x+(((vtotx-(vdis1x+vdis2x))*dtmp3)/100);

		    document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
		    document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
		    document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
		    vdis1=parseFloat(vdis1);
		    vdis2=parseFloat(vdis2);
		    vdis3=parseFloat(vdis3);
		    vtotdis=vdis1+vdis2+vdis3;
		    vdis1x=parseFloat(vdis1x);
		    vdis2x=parseFloat(vdis2x);
		    vdis3x=parseFloat(vdis3x);
		    vtotdisx=vdis1x+vdis2x+vdis3x;
		    document.getElementById("vspbdiscounttotal").value=formatcemua(Math.round(vtotdis));
		    document.getElementById("vspb").value=formatcemua(vtot);
		    vtotbersih=parseFloat(formatulang(formatcemua(vtot)))-parseFloat(formatulang(formatcemua(Math.round(vtotdis))));
		    document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
        document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vtotdisx));
		    vtotbersihx=parseFloat(formatulang(formatcemua(vtotx)))-parseFloat(formatulang(formatcemua(Math.round(vtotdisx))));
		    document.getElementById("vspbafter").value=formatcemua(vtotbersihx);
      }
	  }
  }
</script>
