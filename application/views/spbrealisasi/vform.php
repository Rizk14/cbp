<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'spbrealisasi/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
			<div class="effect">
				<div class="accordion2">
					<table class="listtable">
						<thead>
							<tr>
								<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
							</tr>
						</thead>
						<th>No SPB</th>
						<th>Tgl SPB</th>
						<th>Customer</th>
						<th>Divisi</th>
						<th>Area</th>
						<th>Pemenuhan Daerah</th>
						<th>Status Cetak</th>
						<th class="action">Action</th>
						<tbody>
							<?php
							if ($isi) {
								foreach ($isi as $row) {
									$tmp = explode('-', $row->d_spb);
									$tgl = $tmp[2];
									$bln = $tmp[1];
									$thn = $tmp[0];
									$row->d_spb = $tgl . '-' . $bln . '-' . $thn;
									echo "<tr> 
											<td>$row->i_spb</td>
											<td>$row->d_spb</td>
											<td>$row->e_customer_name</td>
											<td>" . strtoupper($row->e_product_groupname) . "</td>
											<td>$row->e_area_name</td>";
									if ($row->f_spb_stockdaerah == 't') {
										echo "<td>ya</td>";
									} else {
										echo "<td>tidak</td>";
									}
									echo "  <td>$row->n_print_gudang</td>
											<td class=\"action\">
					  							<a href=\"#\" onclick='show(\"spbrealisasi/cform/edit/$row->i_spb/$row->i_area/\",\"#main\")'>
													<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
												</a>

												<a href=\"javascript:print('" . $row->i_spb . "','" . $row->i_area . "');\">
													<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/print.gif\" border=\"0\" alt=\"edit\">
												</a>
												</td>
											</tr>";
								}
								echo "	<input type=\"hidden\" id=\"ispbdelete\" name=\"ispbdelete\" value=\"\">
										<input type=\"hidden\" id=\"ispbedit\" name=\"ispbedit\" value=\"\">
										<input type=\"hidden\" id=\"iareadelete\" name=\"iareadelete\" value=\"\">
										<input type=\"hidden\" id=\"iareaedit\" name=\"iareaedit\" value=\"\">";
							} ?>
						</tbody>
					</table>
					<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function xxx(a, g) {
		if (confirm(g) == 1) {
			document.getElementById("ispbdelete").value = a;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/spbrealisasi/cform/delete";
			formna.submit();
		}
	}

	function yyy(b, c) {
		document.getElementById("ispbedit").value = b;
		document.getElementById("iareaedit").value = c;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/spbrealisasi/cform/edit";
		formna.submit();
	}

	function print(a, b) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/cetak/" + a + "/" + b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_store() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/store/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function tambah_item(a) {
		so_inner = document.getElementById("detailheader").innerHTML;
		si_inner = document.getElementById("detailisi").innerHTML;
		if (so_inner == '') {
			so_inner = '<table class="listtable">';
			so_inner += '<th width="4%"  align="center">No</th>';
			so_inner += '<th width="12%" align="center">Kd Barang</th>';
			so_inner += '<th width="50%" align="center">Nama Barang</th>';
			so_inner += '<th width="13%"  align="center">Harga</th>';
			so_inner += '<th width="7%"  align="center">Jml Pesan</th>';
			so_inner += '<th width="7%"  align="center">Jml Dlv</th>';
			so_inner += '<th width="7%"  align="center" class="action">Action</th></table><table class="listtable" width="100%">';
			document.getElementById("detailheader").innerHTML = so_inner;
		} else {
			so_inner = '';
		}
		if (si_inner == '') {
			document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
			juml = document.getElementById("jml").value;
			si_inner = '<tbody><tr><td width="4%"><input size="1" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
			si_inner += '<td width="12%"><input size="8" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
			si_inner += '<td width="50%"><input size="43" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
			si_inner += '<td width="13%"><input style="text-align:right;"; size="9" type="text" id="vproductretail' + a + '" name="vproductretail' + a + '" value=""></td>';
			si_inner += '<td width="7%"><input size="3" style="text-align:right;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(this.value,' + juml + ')"></td>';
			si_inner += '<td width="7%"><input disabled="true" style="text-align:right"; size="3" type="text" id="ndeliver' + a + '" name="ndeliver' + a + '" value="">';
			si_inner += '</td><td width="7%">&nbsp;</td></tr></tbody>';
		} else {
			document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
			juml = document.getElementById("jml").value;
			si_inner = si_inner + '<tbody><tr><td width="4%"><input size="1" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
			si_inner += '<td width="12%"><input size="8" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
			si_inner += '<td width="50%"><input size="43" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
			si_inner += '<td width="13%"><input size="9" style="text-align:right;" type="text" id="vproductretail' + a + '" name="vproductretail' + a + '" value=""></td>';
			si_inner += '<td width="7%"><input size="3" style="text-align:right;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(this.value,' + juml + ')"></td>';
			si_inner += '<td width="7%"><input disabled="true" size="3" style="text-align:right"; type="text" id="ndeliver' + a + '" name="ndeliver' + a + '" value=""></td>';
			si_inner += '<td width="7%">&nbsp;</td></tr></tbody>';
		}
		j = 0;
		var baris = Array()
		var iproduct = Array();
		var eproductname = Array();
		var vproductretail = Array();
		var norder = Array();
		var ndeliver = Array();
		for (i = 1; i < a; i++) {
			j++;
			baris[j] = document.getElementById("baris" + i).value;
			iproduct[j] = document.getElementById("iproduct" + i).value;
			eproductname[j] = document.getElementById("eproductname" + i).value;
			vproductretail[j] = document.getElementById("vproductretail" + i).value;
			norder[j] = document.getElementById("norder" + i).value;
			ndeliver[j] = document.getElementById("ndeliver" + i).value;
		}
		document.getElementById("detailisi").innerHTML = si_inner;
		j = 0;
		for (i = 1; i < a; i++) {
			j++;
			document.getElementById("baris" + i).value = baris[j];
			document.getElementById("iproduct" + i).value = iproduct[j];
			document.getElementById("eproductname" + i).value = eproductname[j];
			document.getElementById("vproductretail" + i).value = vproductretail[j];
			document.getElementById("norder" + i).value = norder[j];
			document.getElementById("ndeliver" + i).value = ndeliver[j];
		}
		lebar = 450;
		tinggi = 400;
		//kdstore=document.getElementById("istore").value;
		kdharga = document.getElementById("ipricegroup").value;
		//eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/product/"+a+"/"+kdstore+"/"+kdharga,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
		eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/product/"+a+"/"+kdharga,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function dipales(a) {
		cek = 'false';
		if ((document.getElementById("dspb").value != '') &&
			(document.getElementById("icustomer").value != '') &&
			(document.getElementById("iarea").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("iproduct" + i).value == '') ||
						(document.getElementById("eproductname" + i).value == '') ||
						(document.getElementById("norder" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						cek = 'false';
					} else {
						cek = 'true';
					}
				}
			}
			if (cek == 'true') {
				document.getElementById("login").hidden = true;
				formna = document.getElementById("spbform");
				sendRequest();
				formna.submit();
			} else {
				document.getElementById("login").hidden = false;
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}
	}

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").hidden = false;
	}

	function view_pelanggan(a) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/customer/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_area() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/spbrealisasi/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function hitungnilai(isi, jml) {
		if (isNaN(parseFloat(isi))) {
			alert("Input harus numerik");
		} else {
			dtmp1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
			dtmp2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
			dtmp3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
			vdis1 = 0;
			vdis2 = 0;
			vdis3 = 0;
			vtot = 0;
			for (i = 1; i <= jml; i++) {
				vhrg = formatulang(document.getElementById("vproductretail" + i).value);
				nqty = formatulang(document.getElementById("norder" + i).value);
				vhrg = parseFloat(vhrg) * parseFloat(nqty);
				vtot = vtot + vhrg;
				vdis1 = vdis1 + ((vhrg * dtmp1) / 100);
				vdis2 = vdis2 + (((vhrg - vdis1) * dtmp2) / 100);
				vdis3 = vdis3 + (((vhrg - (vdis1 + vdis2)) * dtmp3) / 100);
			}
			document.getElementById("vcustomerdiscount1").value = formatcemua(vdis1);
			document.getElementById("vcustomerdiscount2").value = formatcemua(vdis2);
			document.getElementById("vcustomerdiscount3").value = formatcemua(vdis3);
			vtotdis = vdis1 + vdis2 + vdis3;
			document.getElementById("vspbdiscounttotal").value = formatcemua(vtotdis);
			document.getElementById("vspb").value = formatcemua(vtot);
			vtotbersih = vtot - vtotdis;
			document.getElementById("vspbbersih").value = formatcemua(vtotbersih);
		}
	}

	function refreshview() {
		show('spbrealisasi/cform/index/', '#main');
	}
</script>