<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-kn/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div class="expkkform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="9%">Tgl Awal</td>
								<td width="1%">:</td>
								<td width="90%">
									<input readonly id="datefrom" name="datefrom" value="<?= date('01-m-Y') ?>" onclick="showCalendar('',this,this,'','datefrom',0,20,1)">
								</td>
							</tr>
							<tr>
								<td width="9%">Tgl Akhir</td>
								<td width="1%">:</td>
								<td width="90%">
									<input readonly id="dateto" name="dateto" value="<?= date('d-m-Y') ?>" onclick="showCalendar('',this,this,'','dateto',0,20,1)">
								</td>
							</tr>
							<tr>
								<td width="9%">Area</td>
								<td width="1%">:</td>
								<td width="90%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("exp-kn/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							</tr>
							<tr>
								<td width="9%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="90%">
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-kn/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function exportexcel() {
		var iarea = document.getElementById('iarea').value;
		var eareaname = document.getElementById('eareaname').value;
		var datefrom = document.getElementById('datefrom').value;
		var dateto = document.getElementById('dateto').value;

		if (iarea == '') {
			alert('Pilih Area Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url('exp-kn/cform/export/'); ?>" + iarea + "/" + datefrom + "/" + dateto;
			// console.log(abc);
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>