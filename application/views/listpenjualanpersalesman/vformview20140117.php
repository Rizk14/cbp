<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	require ("php/fungsi.php");
#	echo 'periode:'.$iperiode;
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanpersalesman/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>Area</th>
		<th>Salesman</th>
		<th>Target</th>
		<th>SPB</th>
		<th>%SPB</th>
		<th>Nota</th>
		<th>%Nota</th>
		<th>SPB-Nota</th>
		<th>%</th>
    <th>Retur</th>
    <th>% Retur</th>
	    <tbody>
<?php 
      $targetsub=0;
      $spbsub=0;
      $notasub=0;
      $spbnotsub=0;
      $retursub=0;
      $area='';

      $targettot=0;
      $spbtot=0;
      $notatot=0;
      $spbnottot=0;
      $returtot=0;

			foreach($isi as $row){
        if($row->v_target>0){
          $perspb=($row->v_spb/$row->v_target)*100;
          $pernot=($row->v_nota/$row->v_target)*100;
        }else{
          $perspb=100;
          $pernot=100;
        }
        $spbnot=$row->v_spb-$row->v_nota;
        if($spbnot>0){
          if($row->v_spb>0){
            $persno=($spbnot/$row->v_spb)*100;
          }else{
            $persno=0;
          }
        }else{
          $persno=0;
        }
        if($row->v_retur==null || $row->v_retur=='')$row->v_retur=0;
        if($row->v_nota!=0){
          $persenretur=number_format(($row->v_retur/$row->v_nota)*100,2);
        }else{
          $persenretur='0.00';
        }
        if( ($area!='') && ($area!=$row->i_area) ){
		      echo "<tr><td colspan=2></td>
				        <td align=right><b>".number_format($targetsub)."</td><td align=right><b>".number_format($spbsub)."</td>
                <td align=right><b>".number_format($perspbsub,2)."%</td><td align=right><b>".number_format($notasub)."</td>
                <td align=right><b>".number_format($pernotsub,2)."%</td><td align=right><b>".number_format($spbnotsub)."</td>
                <td align=right><b>".number_format($persnotsub,2)."%</td><td align=right><b>".number_format($retursub)."</td>
      			    <td align=right><b>".number_format($persenretursub,2)." %</td>
				      </tr>";
          $targettot=$targettot+$targetsub;
          $spbtot=$spbtot+$spbsub;
          $notatot=$notatot+$notasub;
          $spbnottot=$spbnottot+$spbnotsub;
          $returtot=$returtot+$retursub;

          $targetsub=0;
          $spbsub=0;
          $notasub=0;
          $spbnotsub=0;
          $retursub=0;
##########
          $targetsub=$targetsub+$row->v_target;
          $spbsub=$spbsub+$row->v_spb;
          $notasub=$notasub+$row->v_nota;
          if($targetsub>0){
            $perspbsub=($spbsub/$targetsub)*100;
            $pernotsub=($notasub/$targetsub)*100;
          }else{
            $perspbsub=100;
            $pernotsub=100;
          }
          $spbnotsub=$spbnotsub+$spbnot;
          $spbnsub=$spbsub-$notasub;
          if($spbsub>0){
            $persnotsub=($spbnotsub/$spbsub)*100;
          }else{
            $persnotsub=0;
          }
          $retursub=$retursub+$row->v_retur;
          if($notasub>0){
            $persenretursub=number_format(($retursub/$notasub)*100,2);
          }else{
            $persenretursub=0;
          }
			    echo "<tr><td>$row->i_area - $row->e_area_name</td><td>$row->i_salesman - $row->e_salesman_name</td>
				    <td align=right>".number_format($row->v_target)."</td><td align=right>".number_format($row->v_spb)."</td>
            <td align=right>".number_format($perspb,2)."%</td><td align=right>".number_format($row->v_nota)."</td>
            <td align=right>".number_format($pernot,2)."%</td><td align=right>".number_format($spbnot)."</td>
            <td align=right>".number_format($persno,2)."%</td><td align=right>".number_format($row->v_retur)."</td>
   			    <td align=right>".number_format($persenretur,2)." %</td>
				  </tr>";
##########
        }else{
          $targetsub=$targetsub+$row->v_target;
          $spbsub=$spbsub+$row->v_spb;
          $notasub=$notasub+$row->v_nota;
          if($targetsub>0){
            $perspbsub=($spbsub/$targetsub)*100;
            $pernotsub=($notasub/$targetsub)*100;
          }else{
            $perspbsub=100;
            $pernotsub=100;
          }
          $spbnotsub=$spbnotsub+$spbnot;
          $spbnsub=$spbsub-$notasub;
          if($spbsub>0){
            $persnotsub=($spbnotsub/$spbsub)*100;
          }else{
            $persnotsub=0;
          }
          $retursub=$retursub+$row->v_retur;
          if($row->v_retur==null || $row->v_retur=='')$row->v_retur=0;
          if($row->v_nota!=0){
            $persenretursub=number_format(($retursub/$notasub)*100,2);
#            $persenretursub=number_format(($row->v_retur/$row->v_nota)*100,2);
          }else{
            $persenretursub='0.00';
          }
    
			    echo "<tr><td>$row->i_area - $row->e_area_name</td><td>$row->i_salesman - $row->e_salesman_name</td>
				    <td align=right>".number_format($row->v_target)."</td><td align=right>".number_format($row->v_spb)."</td>
            <td align=right>".number_format($perspb,2)."%</td><td align=right>".number_format($row->v_nota)."</td>
            <td align=right>".number_format($pernot,2)."%</td><td align=right>".number_format($spbnot)."</td>
            <td align=right>".number_format($persno,2)."%</td><td align=right>".number_format($row->v_retur)."</td>
   			    <td align=right>".number_format($persenretursub,2)." %</td>
				  </tr>";
        }
        $area=$row->i_area;
			}
      echo "<tr><td colspan=2></td>
			        <td align=right><b>".number_format($targetsub)."</td><td align=right><b>".number_format($spbsub)."</td>
              <td align=right><b>".number_format($perspbsub,2)."%</td><td align=right><b>".number_format($notasub)."</td>
              <td align=right><b>".number_format($pernotsub,2)."%</td><td align=right><b>".number_format($spbnotsub)."</td>
              <td align=right><b>".number_format($persnotsub,2)."%</td><td align=right><b>".number_format($row->v_retur)."</td>
     			    <td align=right><b>".$persenretur." %</td>
			      </tr>";
      $targettot=$targettot+$targetsub;
      $spbtot=$spbtot+$spbsub;
      $notatot=$notatot+$notasub;
      $spbnottot=$spbnottot+$spbnotsub;
      $perspbtot=($spbtot/$targettot)*100;
      $pernottot=($notatot/$targettot)*100;
      $sistot=$spbtot-$notatot;
      $pernottots=($sistot/$spbtot)*100;
      $returtot=$returtot+$retursub;
      $perreturtot=number_format(($returtot/$notatot)*100,2);
      echo "<tr><td>NA</td><td>Total Nasional</td>
			        <td align=right><b>".number_format($targettot)."</td><td align=right><b>".number_format($spbtot)."</td>
              <td align=right><b>".number_format($perspbtot,2)."%</td><td align=right><b>".number_format($notatot)."</td>
              <td align=right><b>".number_format($pernottot,2)."%</td><td align=right><b>".number_format($spbnottot)."</td>
              <td align=right><b>".number_format($pernottots,2)."%</td><td align=right><b>".number_format($returtot)."</td>
     			    <td align=right><b>".number_format($perreturtot,2)." %</td>
			      </tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listpenjualanpersalesman/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
