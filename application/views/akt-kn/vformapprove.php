<h2><?php echo $page_title;?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-kn/cform/posting','update'=>'#pesan','type'=>'post'));?>
	<div id="masterknreturform">
	<div class="effect">
	  <div class="accordion2">
		<?php 
		foreach($isi as $row){ 
			$tmp=explode('-',$row->d_kn);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_kn=$tgl.'-'.$bln.'-'.$thn;
			$tmp=explode('-',$row->d_refference);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_refference=$tgl.'-'.$bln.'-'.$thn;
		?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Kredit Nota</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ikn" id="ikn" value="<?php echo $row->i_kn;?>">
						<input readonly name="dkn" id="dkn" value="<?php echo $row->d_kn ?>"></td>
		<td width="12%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="icustomer" id="icustomer" value="<?php echo $row->i_customer; ?>">
						<input type="hidden" name="icustomergroupar" id="icustomergroupar" value="<?php echo $row->i_customer_groupar; ?>">
						<input readonly name="ecustomername" id="ecustomername" value="<?php echo $row->e_customer_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>">
						<input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>"></td>
		<td width="12%">Alamat</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ecustomeraddress" id="ecustomeraddress" value="<?php echo $row->e_customer_address; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">No Refferensi</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="irefference" id="irefference" value="<?php echo $row->i_refference; ?>">
						<input readonly name="drefference" id="drefference" value="<?php echo $row->d_refference; ?>"></td>
		<td width="12%">Salesman</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="isalesman" id="isalesman" value="<?php echo $row->i_salesman; ?>">
						<input readonly name="esalesmanname" id="esalesmanname" value="<?php echo $row->e_salesman_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Kotor</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vgross" id="vgross" value="<?php echo number_format($row->v_gross); ?>"></td>
		<td width="12%">Insentif</td>
		<td width="1%">:</td>
		<td width="37%"><input disabled="true" type="checkbox" name="finsentif" id="finsentif" 
						<?php if($row->f_insentif=='t') 
							echo "checked value=\"on\" ";
						   else
							echo "value=\"\" ";
						?>>
						&nbsp;Masalah&nbsp;
						<input disabled="true" type="checkbox" name="fmasalah" id="fmasalah" 
						<?php if($row->f_masalah=='t') 
							echo "checked value=\"on\" ";
						   else
							echo "value=\"\" ";
						?>></td>
	      </tr>
	      <tr>
		<td width="12%">Potongan</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vdiscount" id="vdiscount" value="<?php echo number_format($row->v_discount); ?>"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value="<?php echo number_format($row->v_sisa); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Bersih</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vnetto" id="vnetto" value="<?php echo number_format($row->v_netto); ?>"></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="eremark" id="eremark" value="<?php echo $row->e_remark; ?>"></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Posting" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('akt-kn/cform/','#main')">
		</td>
	      </tr>
		</table>
		<?php }?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		document.getElementById("login").disabled=true;
	}
</script>
