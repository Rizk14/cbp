<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-spbdetail/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div class="effect">
				<div class="accordion2">
					<table class="mastertable">
						<tr>
							<td width="19%">Date From</td>
							<td width="1%">:</td>
							<td width="80%">
								<input type="hidden" id="areafrom" name="areafrom" value="">
								<?php
								$data = array(
									'name'        	=> 'dfrom',
									'id'          	=> 'dfrom',
									'value'       	=> date('01-m-Y'),
									'readonly'   	=> 'true',
									'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)"
								);
								echo form_input($data); ?>
							</td>
						</tr>
						<tr>
							<td width="19%">Date To</td>
							<td width="1%">:</td>
							<td width="80%">
								<?php
								$data = array(
									'name'        	=> 'dto',
									'id'          	=> 'dto',
									'value'       	=> date('d-m-Y'),
									'readonly'		=> 'true',
									'onclick'	   	=> "showCalendar('',this,this,'','dto',0,20,1)"
								);
								echo form_input($data); ?></td>
						</tr>

						<tr>
							<td width="9%">Area</td>
							<td width="1%">:</td>
							<td width="90%">
								<input type="hidden" id="iarea" name="iarea" value="">
								<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("exp-spbdetail/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							</td>
						</tr>
						<tr>
							<td width="19%">&nbsp;</td>
							<td width="1%">&nbsp;</td>
							<td width="80%">
								<!-- <input name="login" id="login" value="Transfer to Excell" type="submit"> -->
								<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
								<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-spbdetail/cform/','#main')">
							</td>
						</tr>
					</table>
				</div>
			</div>
			<?= form_close() ?>
			<div class='pesan'></div>
		</td>
	</tr>
</table>

<script languge=javascript type=text/javascript>
	function exportexcel() {
		var iarea = document.getElementById('iarea').value;
		var datefrom = document.getElementById('dfrom').value;
		var dateto = document.getElementById('dto').value;

		if (iarea == '') {
			alert('Pilih Area Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url('exp-spbdetail/cform/export/'); ?>" + iarea + "/" + datefrom + "/" + dateto;
			console.log(abc);
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>