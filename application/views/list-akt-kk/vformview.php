<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'list-akt-kk/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div class="effect">
				<div class="accordion2">
					<table class="listtable">
						<thead>
							<tr>
								<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
							</tr>
						</thead>
						<th>Area</th>
						<th>No KK</th>
						<th>Tgl KK</th>
						<th>No Vch</th>
						<th>CoA</th>
						<th>Keterangan</th>
						<th>Nilai</th>
						<th class="action">Action</th>
						<tbody>
							<?php 
							if ($isi) {
								$brs = 0;

								/* Closing / unclosingnya ambil dari tm_closing_kas_bank Per tanggal 16 Jul 2022 */
								// $periode = $this->db->query("select i_periode from tm_periode")->row()->i_periode;

								foreach ($isi as $row) {
									$tmp = explode('-', $row->d_kk);
									$tgl = $tmp[2];
									$bln = $tmp[1];
									$thn = $tmp[0];
									$row->d_kk = $tgl . '-' . $bln . '-' . $thn;
									#$row->d_kkk=$thn.'-'.$bln.'-'.$tgl;
									$dkk = $thn . $bln . $tgl;
									$thnbln = $thn . $bln;

									/* CLOSING / UNCLOSING tm_closing_kas_bank (16 JUL 2022) */
									$periodekk 		= date('Ym', strtotime(date($d_open_kk)));
									$periodekkin 	= date('Ym', strtotime(date($d_open_kkin)));
									if (($periodekk <= $thn . $bln) || ($periodekkin <= $thn . $bln)) {
										$bisaedit = true;
									} else {
										$bisaedit = false;
									}
									/* ************************************************** */

									if ($row->vc == '') $row->vc = $row->vcd;
									echo "<tr> 
											<td>$row->i_area - " . substr($row->e_area_name, 0, 3) . "</td>
											<td>$row->i_kk</td>
											<td>$row->d_kk</td>
											<td>$row->vc</td>
											<td>$row->i_coa</td>
											<td>$row->e_description</td>
											<td align=right>" . number_format($row->v_kk) . "</td>
											<td class=\"action\">";
									if ($row->f_posting != 't') {
										if ($row->f_debet == 't') {
											if ($dkk >= $d_open_kk) {
												echo "<a href=\"#\" onclick='show(\"akt-kk/cform/edit/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$periodekk/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												if ($row->i_cek == '' && $bisaedit == true) echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-kk/cform/delete/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>";
											}
										} else {
											if ($dkk >= $d_open_kkin) {
												echo "<a href=\"#\" onclick='show(\"akt-pkk/cform/edit/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$periodekkin/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												if ($row->i_cek == '' && $bisaedit == true) echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-kk/cform/delete/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>";
											}
										}
									} else {
										if ($row->f_debet == 't') {
											if ($dkk >= $d_open_kk) {
												echo "<a href=\"#\" onclick='show(\"akt-kk/cform/edit/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$periodekk/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											}
										} else {
											if ($dkk >= $d_open_kkin) {
												echo "<a href=\"#\" onclick='show(\"akt-pkk/cform/edit/$row->i_kk/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$periodekkin/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											}
										}
									}
									echo "
			    </td>
			  </tr>";
								}
							}
							?>
						</tbody>
					</table>
					<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>