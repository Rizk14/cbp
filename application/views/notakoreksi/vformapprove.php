<?php 
include ("php/fungsi.php");
?>
<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('notakoreksi/cform/update', array('id' => 'notakoreksiform', 'name' => 'notakoreksiform', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
		<tr>
			<td>Nota</td>
			<td><?php 	if($isi->i_nota!='') {
					echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
										   readonly>"; 
				}else{
					echo "<input type=\"hidden\" id=\"inota\" name=\"inota\" value=\"\"
										   readonly>"; 
				}	
				if($isi->d_nota!=''){				
					$tmp=explode("-",$isi->d_nota);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dnota=$hr."-".$bl."-".$th;
					echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" > ";
				
				}else{
			    ?>
				<input readonly id="dnota" name="dnota" value="" onclick="showCalendar('',this,this,'','dnota',0,20,1)">
				<?php }?>
			</td>			
			<td>Nilai Kotor</td>
			<td><input id="vnotagross" name="vnotagross" readonly value=""></td>
		</tr>
		<tr>
			<td>SPB</td>
				<?php 
					$tmp=explode("-",$isi->d_spb);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dspb=$hr."-".$bl."-".$th;
				?>
			<td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
				<input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
	      
			<td>Discount 1</td>
			<td><input id ="ncustomerdiscount1"name="ncustomerdiscount1" onkeyup="hithit();"
					   value="<?php echo $isi->n_nota_discount1; ?>">
				<input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
					   value="<?php echo number_format($isi->v_nota_discount1); ?>"></td>
		</tr>
		<tr>
			<td>Area</td>
			<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>" 
				 	   readonly >
				<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
			<td>Discount 2</td>
			<td><input id="ncustomerdiscount2" name="ncustomerdiscount2" onkeyup="hithit();"
					   value="<?php echo $isi->n_nota_discount2; ?>">
				<input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
					   value="<?php echo number_format($isi->v_nota_discount2); ?>"></td>
		</tr>
		<tr>
			<td>Pelanggan</td>
			<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan(document.getElementById('iarea').value)" 
					   value="<?php echo $isi->e_customer_name; ?>" onclick= >
				<input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 3</td>
		<td><input id="ncustomerdiscount3" name="ncustomerdiscount3" onkeyup="hithit();"
				   value="<?php echo $isi->n_nota_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_nota_discount3); ?>"></td>
		</tr>
		<tr>
			<td>PO</td>
			<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
			<td>Discount Total</td>
			<td><input readonly id="vnotadiscounttotal" name="vnotadiscounttotal" value=""></td>
		</tr>
		<tr>
			<td>Masalah</td>
			<td><input id="fmasalah" name="fmasalah" type="checkbox" readonly value="">&nbsp;Insentif&nbsp;
				<input id="finsentif" name="finsentif" type="checkbox" readonly value="on" checked></td>
			<td>Nilai Bersih</td>
			<td><input readonly id="vnotanetto" name="vnotanetto" value=""></td>
		</tr>
		<tr>
			<td>TOP</td>

		<?php 
			$tmp = explode("-", $isi->d_spb);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dspb	= $yir."/".$mon."/".$det;
			$dudet	=dateAdd("d",$isi->n_nota_toplength,$dspb);
			$dudet 	= explode("-", $dudet);
			$det1	= $dudet[2];
			$mon1	= $dudet[1];
			$yir1 	= $dudet[0];
			$dudet	= $det1."-".$mon1."-".$yir1;
		?>
			<td><input maxlength="3" id="nnotatoplength" name="nnotatoplength" readonly
				   value="<?php echo $isi->n_nota_toplength; ?>">
			&nbsp;&nbsp;Jatuh tempo&nbsp;
			<input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $dudet; ?>"></td>
				<td>Salesman</td>
				<td><input readonly id="esalesmanname" name="esalesmanname"
						   value="<?php echo $isi->e_salesman_name; ?>">
					<input id="isalesman" name="isalesman" type="hidden"
						   value="<?php echo $isi->i_salesman; ?>"></td>
		</tr>
		<tr>
			<td>PKP</td>
			<td><input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
					   value="<?php echo $isi->e_customer_pkpnpwp;?>">
				<input type="hidden" id="fnotaplusppn" name="fnotaplusppn" value="<?php echo $isi->f_plus_ppn;?>">
				<input type="hidden" id="fnotaplusdiscount" name="fnotaplusdiscount" value="<?php echo $isi->f_plus_discount;?>">
				<input type="hidden" id="nprice" name="nprice" value="<?php echo $isi->n_price;?>">
				<input type="hidden" id="vnotappn" name="vnotappn" value="<?php echo $isi->v_nota_ppn;?>"></td>
			<td>Keterangan</td>
			<td><input id="eremark" name="eremark" value=""></td>
		</tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("notakoreksi/cform/","#tmp")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:68px;" align="center">Kd Barang</th>
					<th style="width:347px;" align="center">Nama Barang</th>
					<th style="width:150px;" align="center">Motif</th>
					<th style="width:100px;"  align="center">Harga</th>
					<th style="width:60px;"  align="center">Jml Dlv</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\"";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$harga	=number_format($row->v_unit_price,2);
					$ndeliv	=number_format($row->del,0);
				  	echo "<tbody>
							<tr>
		    				<td width=\"23px\">
								<input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td width=\"66px\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td width=\"328px\"><input style=\"width:328px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td width=\"142px\"><input style=\"width:142px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td width=\"96px\"><input style=\"text-align:right; width:96px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>
							<td width=\"58px\"><input style=\"text-align:right; width:58px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\"></td>
							</tr>
						  </tbody>";
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	var jml 	= parseFloat(document.getElementById("jml").value);
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	for(i=1;i<=jml;i++){
	  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
		hrg			= hrg+hrgtmp;
	}
	vdis1=vdis1+((hrg*ndis1)/100);
	vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
	vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
	vdistot	= vdis1+vdis2+vdis3;
	vhrgreal= hrg-vdistot;
	document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
	document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
	document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
	document.getElementById("vnotagross").value=formatcemua(hrg);
	document.getElementById("vnotadiscounttotal").value=formatcemua(vdistot);
	document.getElementById("vnotanetto").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("fnotaplusppn").value;
	var fdis = document.getElementById("fnotaplusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vnotanetto").value));
	var kotor  = parseFloat(formatulang(document.getElementById("vnotagross").value));

	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("nprice").value=1;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("nprice").value=bersih/kotor;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("nprice").value=1/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("nprice").value=(bersih/kotor)/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}
</script>
<script language="javascript" type="text/javascript">
  function hitungnilai(){
	var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');
	var nilaidis=document.getElementById("vspbdiscounttotalafter").value.replace(/\,/g,'');
	if(!isNaN(nilaidis)){
		if((nilaispb-nilaidis)<0){
			alert("Nilai discount tidak valid !!!!!");
			document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
		}else{
			document.getElementById("vspbafter").value=formatcemua(nilaispb-nilaidis);
		}
	}else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
	}
  }
  function hitungdiscount(){
	var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');
	var nilaitot=document.getElementById("vspbafter").value.replace(/\,/g,'');
	if(!isNaN(nilaitot)){
		if((nilaispb-nilaitot)<0){
			alert("Nilai total tidak valid !!!!!");
			document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
		}else{
			document.getElementById("vspbdiscounttotalafter").value=formatcemua(nilaispb-nilaitot);
		}
	}else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
	}
  }
/*
  window.onload=function(){
	var jml 	= parseFloat(document.getElementById("jml").value);
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	for(i=1;i<=jml;i++){
	  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
		hrg			= hrg+hrgtmp;
	}
	vdis1=vdis1+((hrg*ndis1)/100);
	vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
	vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
	vdistot	= vdis1+vdis2+vdis3;
	vhrgreal= hrg-vdistot;
	document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
	document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
	document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
	document.getElementById("vnotagross").value=formatcemua(hrg);
	document.getElementById("vnotadiscounttotal").value=formatcemua(vdistot);
	document.getElementById("vnotanetto").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("fnotaplusppn").value;
	var fdis = document.getElementById("fnotaplusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vnotanetto").value));
	var kotor  = parseFloat(formatulang(document.getElementById("vnotagross").value));

	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("nprice").value=1;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("nprice").value=bersih/kotor;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("nprice").value=1/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("nprice").value=(bersih/kotor)/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}
  }
*/
  function dipales(){
  	if(document.getElementById("dnota").value!=''){
      document.getElementById("login").disabled=true;
	  }else{
		  alert("Tanggal nota tidak boleh kosong");
	  }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function view_pelanggan(a){

    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/notakoreksi/cform/customer/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function hithit()
  {
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	hrg=parseFloat(formatulang(document.getElementById("vnotagross").value));
	vdis1=vdis1+((hrg*ndis1)/100);
	vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
	vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
	vdistot	= vdis1+vdis2+vdis3;
	vhrgreal= hrg-vdistot;
	document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
	document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
	document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
	document.getElementById("vnotadiscounttotal").value=formatcemua(vdistot);
	document.getElementById("vnotanetto").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("fnotaplusppn").value;
	var fdis = document.getElementById("fnotaplusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vnotanetto").value));
	var kotor  = parseFloat(formatulang(document.getElementById("vnotagross").value));

	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("nprice").value=1;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("nprice").value=bersih/kotor;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("nprice").value=1/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("nprice").value=(bersih/kotor)/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}
  }
</script>
