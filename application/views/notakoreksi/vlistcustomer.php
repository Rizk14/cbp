<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
</head>
<body id="bodylist">
<div id="main">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		$tujuan = 'notakoreksi/cform/caricustomer/'.$iarea;
	   	echo form_open($tujuan, array('id' => 'listform'));
	?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Customer</th>
	    <th>Nama</th>
	    <th>Area</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_customer','$row->e_customer_name','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$row->e_customer_name','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount')\">$row->e_customer_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$row->e_customer_name','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount')\">$row->e_area_name</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h,i,j,k,l,m,n)
  {
	if( (parseFloat(i)==0) && (parseFloat(j)==0) && (parseFloat(i)==0) ){
		opener.document.getElementById("vnotadiscounttotal").disabled=false;
	}else{
		opener.document.getElementById("vnotadiscounttotal").value='0';
		opener.document.getElementById("vnotadiscounttotal").disabled=true;
	}
    opener.document.getElementById("icustomer").value=a;
    opener.document.getElementById("ecustomername").value=b;
//    opener.document.getElementById("ipricegroup").value=c;
//    opener.document.getElementById("epricegroupname").value=d;
    if(e!=''){
		opener.document.getElementById("ecustomerpkpnpwp").value=e;
//		opener.document.getElementById("fspbpkp").checked=true;
    }else{
		opener.document.getElementById("ecustomerpkpnpwp").value='';
//		opener.document.getElementById("fspbpkp").checked=false;
    }
	opener.document.getElementById("iarea").value=f;
    opener.document.getElementById("isalesman").value=g;
    opener.document.getElementById("esalesmanname").value=h;

    opener.document.getElementById("ncustomerdiscount1").value=i;
    opener.document.getElementById("ncustomerdiscount2").value=j;
    opener.document.getElementById("ncustomerdiscount3").value=k;
    opener.document.getElementById("nnotatoplength").value=l;
	opener.document.getElementById("fnotaplusppn").value=m;
 	opener.document.getElementById("fnotaplusdiscount").value=n;	

	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(opener.document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(opener.document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(opener.document.getElementById("ncustomerdiscount3").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	hrg=parseFloat(formatulang(opener.document.getElementById("vnotagross").value));
	vdis1=vdis1+((hrg*ndis1)/100);
	vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
	vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
	vdistot	= vdis1+vdis2+vdis3;
	vhrgreal= hrg-vdistot;
	opener.document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
	opener.document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
	opener.document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
	opener.document.getElementById("vnotadiscounttotal").value=formatcemua(vdistot);
	opener.document.getElementById("vnotanetto").value=formatcemua(vhrgreal);
    this.close();
  }
</script>
