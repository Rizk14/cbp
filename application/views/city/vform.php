<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Kota</a></li>
			<li><a href="#" class="tab" onclick="sitab('content_2')">Kota</a></li>
		</ul>
		<div id="content_1" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'city/cform/cari/', 'update' => '#main', 'type' => 'post')); ?>
						<div class="effect">
							<div class="accordion2">
								<table class="listtable">
									<thead>
										<tr>
											<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
										</tr>
									</thead>
									<th>Kode Kota</th>
									<th>Nama Kota</th>
									<th>Kode Area</th>
									<th>Nama Area</th>
									<th class="action">Action</th>
									<tbody>
										<?php
										if ($isi) {
											foreach ($isi as $row) {
												echo "<tr>
				  <td>$row->i_city</td>
				  <td>$row->e_city_name</td>
				  <td>$row->i_area</td>
				  <td>$row->e_area_name</td>";
												echo "<td class=\"action\"><a href=\"#\" onclick='show(\"city/cform/edit/$row->i_city/$row->i_area\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>"; #"&nbsp;&nbsp;";
												#			  echo "<a href=\"#\" onclick='hapus(\"city/cform/delete/$row->i_city/$row->i_area/$row->e_city_name/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
												echo "</td></tr>";
											}
										}
										?>
									</tbody>
								</table>
								<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>

		<div id="content_2" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'city/cform/simpan', 'update' => '#main', 'type' => 'post')); ?>
						<div id="mastercityform">
							<div class="effect">
								<div class="accordion2">
									<table class="mastertable">
										<tr>
											<td width="19%">Kode Kota</td>
											<td width="1%">:</td>
											<td width="30%"><?php
															$data = array(
																'name'        => 'icity',
																'id'          => 'icity',
																'value'       => '',
																'maxlength'   => '7',
																'readonly'   => true
															);
															echo form_input($data); ?> *SYSTEM</td>
											<td width="19%">Nama</td>
											<td width="1%">:</td>
											<td width="30%"><input type="text" name="ecityname" id="ecityname" maxlength="50" value="" onkeyup="gede(this)">
												<?php
												/*$data = array(
			              'name'        => 'ecityname',
			              'id'          => 'ecityname',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?></td>
										</tr>
										<tr>
											<td width="19%">Area</td>
											<td width="1%">:</td>
											<td width="30%"><input readonly name="eareaname" id="eareaname" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input type="hidden" name="iarea" id="iarea" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
											<td width="19%">Jenis Kota</td>
											<td width="1%">:</td>
											<td width="30%"><input readonly name="ecitytypename" id="ecitytypename" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input type="hidden" name="icitytype" id="icitytype" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
										</tr>
										<tr>
											<td width="19%">Keterangan</td>
											<td width="1%">:</td>
											<td width="30%"><input readonly name="ecitytypeperareaname" id="ecitytypeperareaname" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input type="hidden" name="icitytypeperarea" id="icitytypeperarea" value="" onclick='showModal("city/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
											<td width="19%">Group Kota</td>
											<td width="1%">:</td>
											<td width="30%"><input readonly name="ecitygroupname" id="ecitygroupname" value="" onclick='bacacitygroup(document.getElementById("iarea").value);'>
												<input type="hidden" name="icitygroup" id="icitygroup" value="" onclick='bacacitygroup(document.getElementById("iarea").value);'>
											</td>
										</tr>
										<tr>
											<td width="19%">Status Kota</td>
											<td width="1%">:</td>
											<td colspan="4" width="80%"><input readonly name="ecitystatusname" id="ecitystatusname" value="" onclick='showModal("city/cform/citystatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input type="hidden" name="icitystatus" id="icitystatus" value="" onclick='showModal("city/cform/citystatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
										</tr>
										<tr>
											<td width="19%">Toleransi SJ Pusat</td>
											<td width="1%">:</td>
											<td width="30%"><input name="ntoleransipusat" id="ntoleransipusat" value="" maxlength="2"></td>
											<td width="19%">Toleransi SJ Cabang</td>
											<td width="1%">:</td>
											<td width="30%"><input name="ntoleransicabang" id="ntoleransicabang" value="" maxlength="2"></td>
										</tr>
										<tr>
											<td colspan="6" width="100%">
												<center><input name="login" id="login" value="Simpan" type="submit">
													<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('city/cform/','#main');">
												</center>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	function bacacitygroup(a) {
		showModal("city/cform/citygroup/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function get_number(a) {
		$.ajax({
			type: "post",
			data: {
				'iarea': a,
			},
			url: '<?= site_url('city/cform/getnumber'); ?>',
			dataType: "json",
			success: function(data) {
				document.getElementById('icity').value = data['number'];
			},
			error: function() {
				// swal('Data kosong :)');
				alert(XMLHttpRequest.responseText);
			}
		});
	}
</script>