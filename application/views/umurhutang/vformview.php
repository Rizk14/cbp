<?php echo "<h2>$page_title</h2>"; ?>
<?php require_once ('php/fungsi.php'); ?>
<table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'umurhutang/cform/cari','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <!-- <tr>
		<td colspan="12" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr> -->
	    </thead>
			  	<th align="center">SUPPLIER</th>
			  	<th align="center">TANGGAL NOTA</th>
        		<th align="center">NO NOTA</th>
			  	<th align="center">DPP</th>
        		<th align="center">PPN</th>
			  	<th align="center">TOTAL</th>
        		<th align="center">TGL JATUH TEMPO</th>
			  	<th align="center">TANGGAL BAYAR</th>
        		<th align="center">BANK/KAS</th>
        		<th align="center">NO BUKTI</th>
			  	<th align="center">HARI</th>
        <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
		        if($row->d_dtap){
					    $tmp=explode('-',$row->d_dtap);
					    $tgl=$tmp[2];
					    $bln=$tmp[1];
					    $thn=$tmp[0];
					    $row->d_op=$tgl.'-'.$bln.'-'.$thn;
		        }
		        if($row->d_due_date!=''){
		          if($row->d_due_date){
					      $tmp=explode('-',$row->d_due_date);
					      $tgl=$tmp[2];
					      $bln=$tmp[1];
					      $thn=$tmp[0];
					      $row->d_do=$tgl.'-'.$bln.'-'.$thn;
		          }
		        }if($row->tanggalbayar!=''){
		          if($row->tanggalbayar){
					      $tmp=explode('-',$row->tanggalbayar);
					      $tgl=$tmp[2];
					      $bln=$tmp[1];
					      $thn=$tmp[0];
					      $row->d_do=$tgl.'-'.$bln.'-'.$thn;
		          }
		        }else{
		          $row->d_dtap='';
		          $row->d_due_date='';
		          $row->tanggalbayar='';
		        }
		        echo "<tr> 
						  	<td>$row->e_supplier_name</td>
						  	<td>$row->d_dtap</td>
						  	<td align='center'>$row->i_dtap</td>
		          			<td>Rp. ".number_format($row->dpp)."</td>
						  	<td>Rp. ".number_format($row->v_ppn)."</td>
		          			<td>Rp. ".number_format($row->v_netto)."</td>
						  	<td>$row->d_due_date</td>
						  	<td>$row->tanggalbayar</td>
		          			<td>$row->bankname</td>
		          			<td>$row->bukti</td>
						  	<td align='center'>$row->umurhutang</td>
		          	</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
	</div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(a,b,c){
	document.getElementById("dfrom").value=a;
	document.getElementById("dto").value=b;
	document.getElementById("iarea").value=c;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/umurhutang/cform/viewdetail";
	formna.submit();
  }
</script>
