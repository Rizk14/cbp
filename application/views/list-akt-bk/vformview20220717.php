<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'list-akt-bk/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
      <div class="effect">
        <div class="accordion2">
          <table class="listtable">
            <thead>
              <tr>
                <td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" placeholder="Kd Bank / Voucher / Nominal"><input
                    type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto"
                    name="dto" value="<?php echo $dto; ?>"><input type="hidden" id="icoabank" name="icoabank"
                    value="<?php echo $icoabank; ?>">&nbsp;<input type="hidden" id="ibank" name="ibank"
                    value="<?php echo $ibank; ?>"><input type="submit" id="bcari" name="bcari" value="Cari"></td>
              </tr>
            </thead>
            <th>Area</th>
            <th>No bank</th>
            <th>Tgl bank</th>
            <th>Tgl input</th>
            <th>No Vch</th>
            <th>CoA</th>
            <th>Keterangan</th>
            <th>Nilai</th>
            <th class="action">Action</th>
            
            <tbody>
            <?php 
              $debet    = 0;
              $kredit   = 0;
              $jml      = 0;
              $tmpkredit=0;
              $tmpdebet =0;
              $saldo = 0;
              $tmptotal = 0;
            
              if($isi) {
                $periode = $this->db->query("select i_periode from tm_periode")->row()->i_periode;
    
                /* AMBIL SALDO AWAL */
                $tmp  = explode("-", $dfrom);
                $th   = $tmp[2];
                $bl   = $tmp[1];
                $dt   = $tmp[0];
                $dtos = $th.$bl;

                $this->db->select(" v_saldo_awal from tm_coa_saldo where i_periode='$dtos' and i_coa='$icoabank' ", false);
                $query = $this->db->get();
                
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                        $saldo = $row->v_saldo_awal;
                    }
                }
                /* ***************************************** */

                foreach ($isi as $row) {
                    $tmp = explode('-', $row->d_bank);
                    $tgl = $tmp[2];
                    $bln = $tmp[1];
                    $thn = $tmp[0];
                    $row->d_bank = $tgl . '-' . $bln . '-' . $thn;
                    #$row->d_bankk = $thn . '-' . $bln . '-' . $tgl;
                    $dbank = $thn.$bln.$tgl;

                    $row->d_entry = date("d-m-Y", strtotime($row->d_entry));
                    $thnbln = $thn . $bln;

                    echo "<tr>
                            <td>$row->i_area - $row->e_area_name</td>";
                    
                            if($row->f_kbank_cancel == 't') {
                              echo "<td><h1>$row->i_kbank</h1></td>";
                            }else {
                              echo "<td>$row->i_kbank</td>";
                            }
                    
                    echo "
                          <td>$row->d_bank</td>
                          <td>$row->d_entry</td>
                          <td>$row->vc</td>
                          <td>$row->i_coa</td>
                          <td>$row->e_description</td>
                          <td align=right>" . number_format($row->v_bank) . "</td>
                          <td class=\"action\">";
                    
                    if(($this->session->userdata('departement')=="1")){
                      if ($dbank>=$d_open_kbank) {
                        echo "<a href=\"#\" onclick='show(\"akt-bk/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                      }
                    }
                    else if ($row->f_posting != 't') {
                      if ($row->f_debet == 't' && $row->v_bank == $row->v_sisa ) {
                        if ($dbank>=$d_open_kbank) {
                            echo "<a href=\"#\" onclick='show(\"akt-bk/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                            if ($row->i_cek == '' && $row->f_kbank_cancel == 'f' && $row->v_bank == $row->v_sisa && $periode <= $thnbln ) {
                            echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-bk/cform/delete/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>";
                          }
                        }
                      }else{
                        if ($dbank>=$d_open_kbankin) {
                          echo "<a href=\"#\" onclick='show(\"akt-pbk/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                          if ($row->i_cek == '' && $row->f_kbank_cancel == 'f' && $row->v_bank == $row->v_sisa && $periode <= $thnbln) {
                            echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-bk/cform/delete/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>";
                          }
                        }
                      }
                    }else{
                      if ($row->f_debet == 't' && $row->v_bank == $row->v_sisa) {
                        if ($dbank>=$d_open_kbank) {
                          echo "<a href=\"#\" onclick='show(\"akt-bk/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                        }
                      }else{
                        if ($dbank>=$d_open_kbankin) {
                          echo "<a href=\"#\" onclick='show(\"akt-pbk/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$icoabank/$ibank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
                        }
                      }
                    }

                    echo "</td></tr>";
                    
                    if(($row->f_debet == 'f' && $row->f_kbank_cancel == 'f')){
                      $debet   = $debet+floatval($row->v_bank);
                    }
                      
                    if(($row->f_debet == 't' && $row->f_kbank_cancel == 'f')){
                      $kredit = $kredit+floatval($row->v_bank);
                    }
                  }//END Foreach
                  $tmpkredit  = $tmpkredit+$kredit;
                  $tmpdebet   = $tmpdebet+$debet;
                  $tmptotal   = $saldo+$tmpdebet-$tmpkredit;
              }
            ?>
            </tbody>
          </table>

          <table class="listtable">
            <thead>
              <tr>
                <th>Saldo Awal</th>
                <th>Debet</th>
                <th>Kredit</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><?php echo number_format($saldo); ?></td>
                <td><?php echo number_format($tmpdebet); ?></td>
                <td><?php echo number_format($tmpkredit); ?></td>
                <td><?php echo number_format($tmptotal); ?></td>
              </tr>
            </tbody>
          </table>

          <?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
        </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>