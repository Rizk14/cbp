<?php 
	echo "<h2>$page_title</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'rtunai/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="transferuangform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Nomor</td>
		<td width="1%">:</td>
		<td colspan=4 width="37%"><input readonly id="irtunai" name="irtunai" value="<?php echo $isi->i_rtunai; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Tgl Setor</td>
		<td width="1%">:</td>
		<?php 
		  $tmp=explode('-',$isi->d_rtunai);
		  $yy=$tmp[0];
		  $mm=$tmp[1];
		  $dd=$tmp[2];
		  $isi->d_rtunai=$dd.'-'.$mm.'-'.$yy;
		?>
		<td width="37%"><input readonly id="drtunai" name="drtunai" value="<?php echo $isi->d_rtunai; ?>" onclick="showCalendar('',this,this,'','drtunai',0,20,1)"></td>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name; ?>"
						onclick='showModal("rtunai/cform/area/","#light"); jsDlgShow("#konten *", "#fade", "#light");'>
						<input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area; ?>">
						<input type="hidden" name="xiarea" id="xiarea" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
		  <tr>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value="<?php echo $isi->e_remark; ?>"></td>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="vjumlah" id="vjumlah" value="<?php echo number_format($isi->v_jumlah); ?>"></td>
		  </tr>
	      <tr>
		<td width="12%">Bank</td>
		<td width="1%">:</td>
		<td colspan=4 width="37%"><input readonly name="ebankname" id="ebankname" value="<?php echo $isi->e_bank_name; ?>"
						onclick='showModal("rtunai/cform/bank/","#light"); jsDlgShow("#konten *", "#fade", "#light");'>
						<input type="hidden" name="ibank" id="ibank" value="<?php echo $isi->i_bank; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan="4">
		  <?php 
		    if($detail){
      ?>
          <input type="hidden" name="jml" id="jml" value="<?php echo count($detail); ?>">
      <?php 		      
		    }
		  ?>
		  <?php if($isi->i_cek=='') { ?>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
		  <?php } ?>
		   <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick='show("listrtunai/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'>
		</td>
	      </tr>
		</table>
		<div id="detailheader" align="center">
      <table id="itemtem" class="listtable" style="width:930px;">
        <tr>
          		<th style="width:25px;"  align="center">No</th>
			    <th style="width:78px;" align="center">Area</th>
			    <th style="width:108px;" align="center">No Tunai</th>
			    <th style="width:75px;" align="center">Tgl Tunai</th>
			    <th style="width:185px;" align="center">Pelanggan</th>
			    <th style="width:120px;" align="center">No Nota</th>
			    <th style="width:78px;" align="center">Jml</th>
			    <th style="width:200px;" align="center">Keterangan</th>
			    <th style="width:32px;"  align="center" class="action">Act</th>
			  </tr>
			</table>
		</div>
		<div id="detailisi" align="center">
				<?php 
					if($detail){
						$i=0;
						foreach($detail as $row)
						{
							echo '<table class="listtable" style="width:930px;">';
						  $i++;
						  echo "
					  	  <tbody>
					  	    <tr>
					  	     <td style=\"width:20px;\">
					  	     	<input style=\"width:20px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\">
					  	     </td>

					  	     <td style=\"width:78px;\">
					  	     	<input style=\"width:78px;\" readonly type=\"text\" id=\"eareaname$i\" name=\"eareaname$i\" value=\"$row->e_area_name\">
					  	     	<input type=\"hidden\" id=\"iarea$i\" name=\"iarea$i\" value=\"$row->i_area_tunai\">
					  	     </td>

					  	     <td style=\"width:108px;\">
					  	     	<input style=\"width:108px;\" readonly type=\"text\" id=\"itunai$i\" name=\"itunai$i\" value=\"$row->i_tunai\">
					  	     </td>
					  	      
					  	     <td style=\"width:75px;\">
					  	     	<input style=\"width:75px;\" readonly type=\"text\" id=\"dtunai$i\" name=\"dtunai$i\" value=\"$row->d_tunai\">
					  	     </td>
					  	     
					  	     <td style=\"width:185px;\">
					  	     	<input style=\"width:185px;\" readonly type=\"text\" id=\"ecustomername$i\" name=\"ecustomername$i\" value=\"$row->e_customer_name\">
					  	     	<input type=\"hidden\" id=\"icustomer$i\" name=\"icustomer$i\" value=\"$row->i_customer\">
					  	     </td>
					  	     
					  	     <td style=\"width:120px;\">
					  	     	<input style=\"width:120px;\" readonly type=\"text\" value=\"$row->i_nota\">
					  	     </td>
					  	     
					  	     <td style=\"width:78px;\"><input type=\"hidden\" id=\"vjumlahasal$i\" name=\"vjumlahasal$i\" value=\"$row->v_jumlah\">
					  	     	<input type=\"hidden\" id=\"vjumlahasallagi$i\" name=\"vjumlahasallagi$i\" value=\"$row->v_jumlah\">
					  	     	<input style=\"text-align:right; width:78px;\"  type=\"text\" id=\"vjumlah$i\" name=\"vjumlah$i\" value=\"".number_format($row->v_jumlah)."\" onkeydown=\"reformat(this);\" onKeyUp=\"hetang('".$i."');\" onpaste=\"return false;\">
					  	     </td>

					  	     <td style=\"width:200px;\">
					  	      	<input style=\"width:200px;\" readonly type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\">
					  	     </td>
					  	     
					  	      <td style=\"width:30px;\">
					  	      	<a href=\"#\" onclick='hapus(\"rtunai/cform/deletedetail/$row->i_rtunai/$row->i_area/$row->i_tunai/$row->i_area_tunai/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\">
					  	      	</a>
					  	      </td>
					  	    </tr>
					  	  </tbody>
					  	</table>";
            }
					}		
				?>
		</div>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("drtunai").value=='')||(document.getElementById("iarea").value=='')||(document.getElementById("vjumlah").value=='')||(document.getElementById("vjumlah").value=='0')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
			document.getElementById("cmdtambahitem").disabled=true;
		}
	}
  function tambah_item(a){
    if(a<=30){
	    so_inner=document.getElementById("detailheader").innerHTML;
	    si_inner=document.getElementById("detailisi").innerHTML;
	    if(so_inner==''){
			  so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
			  so_inner+= '<tr><th style="width:20px;"  align="center">No</th>';
			  so_inner+= '<th style="width:78px;" align="center">Area</th>';
			  so_inner+= '<th style="width:108px;" align="center">No Tunai</th>';
			  so_inner+= '<th style="width:75px;" align="center">Tgl Tunai</th>';
			  so_inner+= '<th style="width:185px;" align="center">Pelanggan</th>';
			  so_inner+= '<th style="width:120px;" align="center">No Nota</th>';
			  so_inner+= '<th style="width:78px;" align="center">Jml</th>';
			  so_inner+= '<th style="width:200px;" align="center">Keterangan</th>';
			  so_inner+= '<th style="width:32px;"  align="center" class="action">Act</th></tr></table>';
			  document.getElementById("detailheader").innerHTML=so_inner;
	    }else{
			  so_inner=''; 
	    }
	    if(si_inner==''){
			  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			  juml=document.getElementById("jml").value;	
			  si_inner='<table class="listtable" style="width:930px;"><tbody><tr><td style="width:20px;"><input style="width:20px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
			  si_inner+='<td style="width:78px;"><input style="width:78px;" readonly type="text" id="eareaname'+a+'" name="eareaname'+a+'" value=""><input type="hidden" id="iarea'+a+'" name="iarea'+a+'" value=""></td>';
			  si_inner+='<td style="width:108px;"><input style="width:108px;" readonly type="text" id="itunai'+a+'" name="itunai'+a+'" value=""></td>';
			  si_inner+='<td style="width:75px;"><input style="width:75px;" readonly type="text" id="dtunai'+a+'" name="dtunai'+a+'" value=""></td>';
			  si_inner+='<td style="width:185px;"><input style="width:185px;" readonly type="text" id="ecustomername'+a+'" name="ecustomername'+a+'" value=""><input type="hidden" id="icustomer'+a+'" name="icustomer'+a+'" value=""></td>';
			  si_inner+='<td style="width:120px;"><input style="width:120px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""></td>';
			  si_inner+='<td style="width:78px;"><input type="hidden" id="vjumlahasal'+a+'" name="vjumlahasal'+a+'" value=""><input type="hidden" id="vjumlahasallagi'+a+'" name="vjumlahasallagi'+a+'" value=""><input style="text-align:right; width:78px;"  type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetang('+a+');" onpaste="return false;"></td>';
			  si_inner+='<td style="width:200px;"><input style="width:200px;" readonly type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
			  si_inner+='<td style="width:30px;">&nbsp;</td></tr></tbody></table>';
	    }else{
			  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			  juml=document.getElementById("jml").value;
			  si_inner+='<table class="listtable" style="width:930px;"><tbody><tr><td style="width:20px;"><input style="width:20px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
			  si_inner+='<td style="width:78px;"><input style="width:78px;" readonly type="text" id="eareaname'+a+'" name="eareaname'+a+'" value=""><input type="hidden" id="iarea'+a+'" name="iarea'+a+'" value=""></td>';
			  si_inner+='<td style="width:108px;"><input style="width:108px;" readonly type="text" id="itunai'+a+'" name="itunai'+a+'" value=""></td>';
			  si_inner+='<td style="width:75px;"><input style="width:75px;" readonly type="text" id="dtunai'+a+'" name="dtunai'+a+'" value=""></td>';
			  si_inner+='<td style="width:185px;"><input style="width:185px;" readonly type="text" id="ecustomername'+a+'" name="ecustomername'+a+'" value=""><input type="hidden" id="icustomer'+a+'" name="icustomer'+a+'" value=""></td>';
			  si_inner+='<td style="width:120px;"><input style="width:120px;" readonly type="text" id="inota'+a+'" name="inota'+a+' " value=""></td>';
			  si_inner+='<td style="width:78px;"><input type="hidden" id="vjumlahasal'+a+'" name="vjumlahasal'+a+'" value=""><input type="hidden" id="vjumlahasallagi'+a+'" name="vjumlahasallagi'+a+'" value=""><input style="text-align:right; width:78px;"  type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetang('+a+');" onpaste="return false;"></td>';
			  si_inner+='<td style="width:200px;"><input style="width:200px;" readonly type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
			  si_inner+='<td style="width:30px;">&nbsp;</td></tr></tbody></table>';
	    }
	    j=0;
	    var baris	        = Array()
	    var iarea	        = Array();
	    var eareaname	    = Array();
	    var itunai	      = Array();
	    var dtunai	      = Array();
	    var icustomer	    = Array();
	    var ecustomername	= Array();
		  var eremark	      = Array();
		  var vjumlah	      = Array();
		  var vjumlahasal     = Array();
 		  var vjumlahasallagi = Array();
 		  var inota 			= Array();
	    for(i=1;i<a;i++){
			  j++;
			  baris[j]		      = document.getElementById("baris"+i).value;
			  iarea[j]			    = document.getElementById("iarea"+i).value;
			  eareaname[j]			= document.getElementById("eareaname"+i).value;
			  itunai[j]			    = document.getElementById("itunai"+i).value;
			  dtunai[j]			    = document.getElementById("dtunai"+i).value;
			  icustomer[j]	    = document.getElementById("icustomer"+i).value;
			  ecustomername[j]	= document.getElementById("ecustomername"+i).value;
			  vjumlah[j]		    = document.getElementById("vjumlah"+i).value;
			  vjumlahasal[j]      = document.getElementById("vjumlahasal"+i).value;
			  vjumlahasallagi[j]  = document.getElementById("vjumlahasallagi"+i).value;
			  eremark[j]	      = document.getElementById("eremark"+i).value;
			  inota[j] 			= document.getElementById("inota"+i).value;
	    }
	    
	    document.getElementById("detailisi").innerHTML=si_inner;
	    j=0;
	    for(i=1;i<a;i++){
			  j++;
			  document.getElementById("baris"+i).value=baris[j];8204900
			  document.getElementById("iarea"+i).value=iarea[j];
			  document.getElementById("eareaname"+i).value=eareaname[j];
			  document.getElementById("itunai"+i).value=itunai[j];
			  document.getElementById("dtunai"+i).value=dtunai[j];
			  document.getElementById("icustomer"+i).value=icustomer[j];
			  document.getElementById("ecustomername"+i).value=ecustomername[j];
			  document.getElementById("vjumlah"+i).value=vjumlah[j];
			  document.getElementById("vjumlahasal"+i).value=vjumlahasal[j];
			  document.getElementById("vjumlahasallagi"+i).value=vjumlahasallagi[j];
  			document.getElementById("eremark"+i).value=eremark[j];
  			document.getElementById("inota"+i).value=eremark[j];
	    }
	    area=document.getElementById("iarea").value;
	    drtunai=document.getElementById("drtunai").value;
	    showModal("rtunai/cform/tunai/"+a+"/"+area+"/"+drtunai,"#light");
	    jsDlgShow("#konten *", "#fade", "#light");
    }else{
	alert("Maksimal 30 item");
    }
  }
    function hetang(x)
  {	
    num=document.getElementById("vjumlah"+x).value.replace(/\,/g,'');
    numasal=document.getElementById("vjumlahasal"+x).value.replace(/\,/g,'');
    numasli=document.getElementById("vjumlahasallagi"+x).value.replace(/\,/g,'');
    if(!isNaN(num)){
      xx=document.getElementById("vjumlah").value.replace(/\,/g,'');
      if(parseFloat(num)>parseFloat(numasli)){
        alert('Jumlah tidak boleh lebih dari Rp. '+numasli+' !!!');
        document.getElementById("vjumlah"+x).value=document.getElementById("vjumlahasal"+x).value;
        yy=(parseFloat(xx)+parseFloat(numasal));
        document.getElementById("vjumlah").value=formatcemua(parseFloat(yy)-parseFloat(num));
        xx=document.getElementById("vjumlah").value.replace(/\,/g,'');
      }
      if(parseFloat(num)<0){
        alert('Jumlah tidak boleh kurang dari 0 !!!');
        document.getElementById("vjumlah"+x).value=document.getElementById("vjumlahasal"+x).value;
        yy=(parseFloat(xx)+parseFloat(numasal));
        document.getElementById("vjumlah").value=formatcemua(parseFloat(yy)-parseFloat(num));
        xx=document.getElementById("vjumlah").value.replace(/\,/g,'');
      }
      if(document.getElementById("vjumlah"+x).value==''){
        yy=(parseFloat(xx)-parseFloat(numasal));
        document.getElementById("vjumlah").value=formatcemua(parseFloat(yy));
        document.getElementById("vjumlahasal"+x).value='0';
      }else{
        yy=(parseFloat(xx)-parseFloat(numasal));
        document.getElementById("vjumlah").value=formatcemua(parseFloat(yy)+parseFloat(num));
        document.getElementById("vjumlahasal"+x).value=document.getElementById("vjumlah"+x).value;
        if(parseFloat(num)<=parseFloat(numasli)){
          document.getElementById("vjumlah"+x).value=formatcemua(parseFloat(num));
        }
      }
    }else{ 
		  alert('input harus numerik !!!');
      document.getElementById("vjumlah"+x).value=0;
	  }
  }
</script>
