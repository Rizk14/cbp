<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<!--
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
-->
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		$tujuan = 'spmbnewmanualmo/cform/productupdate';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"><input type="hidden" id="peraw" name="peraw" value="<?php echo $peraw;?>"><input type="hidden" id="perak" name="perak" value="<?php echo $perak;?>"><input type="hidden" id="area" name="area" value="<?php echo $area;?>"></td>
	      </tr>
	    </thead>
   	    <th>Kode Product</th>
	    	<th>Nama</th>
  			<th>Motif</th>
        <th>Jml Rata2</th>
  			<th>Nilai Rata2</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $fpaw='FP-'.$peraw;
        $fpak='FP-'.$perak;
        $query = $this->db->query(" select trunc(sum(n_deliver*v_unit_price)/3) as vrata, trunc(sum(n_deliver)/3) as nrata, i_product 
                                    from tm_nota_item
                                    where i_nota>'$fpaw' and i_nota<'$fpak' and i_product='$row->kode' 
                                    and i_product_motif='$row->motif' and i_area='$area'
                                    group by i_product
                                    order by i_product");
        if($query->num_rows()>0){
          foreach($query->result() as $raw){
            $vrata=number_format($raw->vrata);
            $nrata=number_format($raw->nrata);
          }
        }else{
          $vrata=0;          
          $nrata=0;
        }
			  echo "<tr>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga,2)."',$baris,'$row->motif','$row->namamotif','$nrata','$vrata')\">$row->kode</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga,2)."',$baris,'$row->motif','$row->namamotif','$nrata','$vrata')\">$row->nama</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga,2)."',$baris,'$row->motif','$row->namamotif','$nrata','$vrata')\">$row->namamotif</a></td>
				  <td align='right'><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga,2)."',$baris,'$row->motif','$row->namamotif','$nrata','$vrata')\">$nrata</a></td>
				  <td align='right'><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga,2)."',$baris,'$row->motif','$row->namamotif','$nrata','$vrata')\">$vrata</a></td>

				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h)
  {
    ada=false;
    for(i=1;i<=d;i++){
	if(
		(a==document.getElementById("iproduct"+i).value) && 
		(i!==d) && 
		(e==document.getElementById("motif"+i).value)
	  ){
		alert ("kode : "+a+" sudah ada !!!!!");
		ada=true;
		break;
	}else{
		ada=false;	   
	}
    }
    if(!ada){
		document.getElementById("iproduct"+d).value=a;
		document.getElementById("eproductname"+d).value=b;
		document.getElementById("vproductmill"+d).value=c;
		document.getElementById("motif"+d).value=e;
		document.getElementById("emotifname"+d).value=f;
		document.getElementById("jmlrata"+d).value=g;
		document.getElementById("nilairata"+d).value=h;
		jsDlgHide("#konten *", "#fade", "#light");
    }
	
  }
  function bbatal(){
	baris	= document.getElementById("jml").value;
	si_inner= document.getElementById("detailisi").innerHTML;
	var temp= new Array();
	temp	= si_inner.split('<table disabled="disabled" class="listtable" style="width: 930px;" align="center"><tbody disabled="disabled">');
	if( (document.getElementById("iproduct"+baris).value=='')){
		si_inner='';
		for(x=1;x<baris;x++){
			si_inner=si_inner+'<table class="listtable" style="width:930px;" align="center"><tbody>'+temp[x];
		}
		j=0;
		var barbar			= Array();
		var iproduct		= Array();
		var eproductname= Array();
		var norder			= Array();
		var nacc  			= Array();
		var jmlrata  		= Array();
		var nilairata		= Array();
		var vproductmill= Array();
		var motif			  = Array();
		var motifname		= Array();
		var vtotal			= Array();
		for(i=1;i<baris;i++){
			j++;
			barbar[j]			  = document.getElementById("baris"+i).value;
			iproduct[j]			= document.getElementById("iproduct"+i).value;
			eproductname[j]	= document.getElementById("eproductname"+i).value;
			norder[j]			  = document.getElementById("norder"+i).value;
			nacc[j]			    = document.getElementById("nacc"+i).value;
			jmlrata[j]	    = document.getElementById("jmlrata"+i).value;		
			nilairata[j]    = document.getElementById("nilairata"+i).value;				
			vproductmill[j]	= document.getElementById("vproductmill"+i).value;		
			motif[j]			  = document.getElementById("motif"+i).value;
			motifname[j]		= document.getElementById("emotifname"+i).value;
			vtotal[j]			  = document.getElementById("vtotal"+i).value;
		}
		document.getElementById("detailisi").innerHTML=si_inner;
		j=0;
		for(i=1;i<baris;i++){
			j++;
			document.getElementById("baris"+i).value        = barbar[j];
			document.getElementById("iproduct"+i).value     = iproduct[j];
			document.getElementById("eproductname"+i).value = eproductname[j];
			document.getElementById("norder"+i).value       = norder[j];
			document.getElementById("nacc"+i).value         = nacc[j];
			document.getElementById("jmlrata"+i).value      = jmlrata[j];
			document.getElementById("nilairata"+i).value    = nilairata[j];
			document.getElementById("vproductmill"+i).value = vproductmill[j];		
			document.getElementById("motif"+i).value        = motif[j];
			document.getElementById("emotifname"+i).value   = motifname[j];
			document.getElementById("vtotal"+i).value       = vtotal[j];
		}
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
		jsDlgHide("#konten *", "#fade", "#light");
	}
  }
</script>
