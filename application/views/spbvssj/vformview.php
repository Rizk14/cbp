<?php echo "<h2>$page_title</h2>"; ?><br>
<h3>Periode <?php echo $dfrom." s/d ".$dto ?> </h3>
<i>* Data bersifat terpisah, nilai SJ dan nilai nota bukan dari realisasi SPB</i>
<table class="maintable">
  <tr>
    <td align="left">
    <?php //echo $this->pquery->form_remote_tag(array('url'=>'spbvssj/cform/view','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <!--  <thead>
	    <tr>
		<td colspan="8" align="center">Cari nama area : <input type="text" id="cari" name="cari" value="<?php //echo $cari; ?>" >
		<input type="hidden" id="dfrom" name="dfrom" value="<?php //echo $dfrom; ?>" >
		<input type="hidden" id="dto" name="dto" value="<?php //echo $dto; ?>" >
		&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr> 
	      </thead>
	      -->
	      <tr>
	   	    <th>Area</th>
			<th>Nilai SPB</th>
			<th>Nilai SJ</th>
			<th>Nilai Nota</th>
			<th>Opsi</th>
		</tr>
	    <tbody>
	      <?php 
	      $totalspb = 0; $totalsj = 0; $totalnota = 0;
		if (is_array($isi)) {
			for($j=0;$j<count($isi);$j++){
		?>
				<tr>
					<td><?php echo $isi[$j]['i_area']." - ".$isi[$j]['namaarea'] ?></td>
					<td align="right"><?php $totalspb+=$isi[$j]['nilaispb']; echo number_format($isi[$j]['nilaispb'],'2','.',','); ?></td>
					<td align="right"><?php $totalsj+=$isi[$j]['nilaisj']; echo number_format($isi[$j]['nilaisj'],'2','.',','); ?></td>
					<td align="right"><?php $totalnota+=$isi[$j]['nilainota']; echo number_format($isi[$j]['nilainota'],'2','.',',');  ?></td>
					<td class="action">
					<a href="#" onclick='show("spbvssj/cform/detailpersales/<?php echo $isi[$j]['i_area']."/".$dfrom."/".$dto."/".$is_groupbrg."/".$cari ?>/","#main")'>Detail Per Sales</a></td>
				</tr>
		<?php 
			}
		?>
			<tr>
				<td align="center"><b>TOTAL SELURUH AREA/NASIONAL</b></td>
				<td align="right"><b><?php echo number_format($totalspb,'2','.',',') ?></b></td>
				<td align="right"><b><?php echo number_format($totalsj,'2','.',',') ?></b></td>
				<td align="right"><b><?php echo number_format($totalnota,'2','.',',') ?></b></td>
				<td align="center">
					<!--<input type="button" name="export_excel" value="Export ke Excel">-->
					<?php 
					$attributes = array('name' => 'f_export', 'id' => 'f_export');
					echo form_open('spbvssj/cform/export_excel', $attributes); ?>
					<!--<form action="index.php/spbvssj/cform/export_excel" method="post"> -->
						<input type="hidden" name="dfrom" value="<?php echo $dfrom ?>" >
						<input type="hidden" name="dto" value="<?php echo $dto ?>" >
						<input type="hidden" name="is_groupbrg" value="<?php echo $is_groupbrg ?>" >
						<input type="submit" name="export_excel" id="export_excel" value="Export ke Excel">
					<?php echo form_close();  ?>
				</td>
			</tr>
			<?php 
		}
	      ?>
	    </tbody>
	  </table>
	  <?php //echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
	</div>
      <?php //=form_close() ?>
    </td>
  </tr>
</table>
