<style>
#cmdreset {
  border: 1px solid #777;
  background: #F2F3F4;
  color: #231f20;
  font: bold 11px 'Trebuchet MS';
  padding: 3px;
  margin-top:2px;
  margin-bottom:2px;
  cursor: pointer;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
}
</style>

<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
 	<tr>
   <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listttbretur/cform/cari','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	<div class="accordion2">
   <table class="listtable">
	<thead>
		<tr>
		<td colspan="9" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	   </tr>
	</thead>
	<th>Area</th>
	<th>No TTB</th>
	<th>Tgl TTB</th>
	<th>Pelanggan</th>
   <th>Tgl Sls</th>
   <th>Alasan</th>
	<th>No BBM</th>
	<th>Tgl BBM</th>
	<th class="action">Action</th>
	<tbody>
	<?php 
		if($isi)
		{
			foreach($isi as $row)
			{
				if(!empty($row->d_ttb) || $row->d_ttb!='')
				{
					$tmp=explode('-',$row->d_ttb);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$dttb=$tgl.'-'.$bln.'-'.$thn;
				}
				else
				{
					$dttb='';	
				}
        		$row->i_ttb=trim($row->i_ttb);
			  	echo "<tr> 
				  	<td>$row->e_area_name</td>";
				if($row->f_ttb_cancel=='t')
				{
		  			echo "<td><h1>$row->i_ttb</h1></td>";
				}
				else
				{
		  			echo "<td>$row->i_ttb</td>";
				}
		  		echo "
			  		<td>$dttb</td>
			  		<td>($row->i_customer) $row->e_customer_name</td>	";
   			if(!empty($row->d_receive1) || $row->d_receive1!='')
   			{					
    				$tmp=explode('-',$row->d_receive1);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$dreceive1=$tgl.'-'.$bln.'-'.$thn;
  				}
  				else
  				{
					$dreceive1='';	
				}
  				echo "
    				<td> $dreceive1</td> ";
  				echo "
    				<td> $row->e_alasan_returname</td> ";
  				if(!empty($row->i_bbm) || $row->i_bbm!=='') 
  				{
    				$ibbm=$row->i_bbm;
  				}
  				else
  				{
      			$ibbm='';
  				}
    			if($row->f_bbm_cancel=='t'){
			    echo "<td><h1>$row->i_bbm</h1></td>";
				  }else{
			    echo "<td>$row->i_bbm</td>";
				  }
  				if(!empty($row->d_bbm) || $row->d_bbm!='')
  				{					
    				$tmp=explode('-',$row->d_bbm);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$dbbm=$tgl.'-'.$bln.'-'.$thn;
  				}
  				else
  				{
					$dbbm='';	
				}
  				echo "
    				<td> $dbbm</td> ";
  				echo "
		  			<td class=\"action\">";
    			if($iarea=='NA')
    			{
	      		echo "<a href=\"#\" onclick='show(\"ttbretur/cform/editna/$row->i_ttb/$row->i_area/$row->n_ttb_year/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
    			}
    			else
    			{
	      		echo "<a href=\"#\" onclick='show(\"ttbretur/cform/edit/$row->i_ttb/$row->i_area/$row->n_ttb_year/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
    			}
		  		if(empty($row->d_bbm) or $row->f_bbm_cancel=='t')
		  		{
		  			echo "<a href=\"#\" onclick='hapus(\"listttbretur/cform/delete/$row->i_ttb/$row->i_area/$row->n_ttb_year/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
		  		}
	  			echo "</td></tr>";
			}
		}
      ?>
	</tbody>
	</table>
	<center>
		<input name="cmdreset" id="cmdreset" style="cursor:pointer; outline-offset:3px" value=" Kembali " type="button" onclick='show("listttbretur/cform/","#main")'>
	</center>
	<?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
   </div>
   </div>
	<?=form_close()?>
   </td>
  	</tr>
</table>
</div>
