<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'akt-kbin-multi/cform/start', 'update' => '#main', 'type' => 'post')); ?>
			<div id="aktkkform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="12%">Area</td>
								<td width="1%">:</td>
								<td width="87%"><input type="hidden" name="periode" id="periode" value="<?php if ($periode) echo $periode; ?>"><input type="hidden" name="iarea" id="iarea" value="<?php echo $area1; ?>"><input readonly name="eareaname" id="eareaname" value="<?php echo $nama; ?>"></td>
							</tr>
							<tr>
								<td width="12%">Periode</td>
								<td width="1%">:</td>
								<?php $bulan = date('m'); ?>
								<td width="87%"><select name="iperiodebl" id="iperiodebl" onclick="cektanggal();">
										<option value='01' <?php if ($bulan == '01') echo "Selected" ?>>01</option>
										<option value='02' <?php if ($bulan == '02') echo "Selected" ?>>02</option>
										<option value='03' <?php if ($bulan == '03') echo "Selected" ?>>03</option>
										<option value='04' <?php if ($bulan == '04') echo "Selected" ?>>04</option>
										<option value='05' <?php if ($bulan == '05') echo "Selected" ?>>05</option>
										<option value='06' <?php if ($bulan == '06') echo "Selected" ?>>06</option>
										<option value='07' <?php if ($bulan == '07') echo "Selected" ?>>07</option>
										<option value='08' <?php if ($bulan == '08') echo "Selected" ?>>08</option>
										<option value='09' <?php if ($bulan == '09') echo "Selected" ?>>09</option>
										<option value='10' <?php if ($bulan == '10') echo "Selected" ?>>10</option>
										<option value='11' <?php if ($bulan == '11') echo "Selected" ?>>11</option>
										<option value='12' <?php if ($bulan == '12') echo "Selected" ?>>12</option>
									</select>&nbsp;<input name="iperiodeth" id="iperiodeth" value="<?= date('Y'); ?>" onkeyup="cektanggal();" maxlength="4"></td>
							</tr>
							<tr>
								<td width="12%">Tanggal</td>
								<td width="1%">:</td>
								<td width="87%"><input readonly name="dkb" id="dkb" value="<?= date('d-m-Y'); ?>" onclick="showCalendar('',this,this,'','dkb',0,20,1)"></td>
							</tr>
							<td width="12%">&nbsp;</td>
							<td width="1%">&nbsp;</td>
							<td width="87%">
								<input name="login" id="login" value="Proses" type="submit">
								<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('akt-kbin-multi/cform/','#main');">
							</td>
	</tr>
</table>
</div>
</div>
</div>
<?= form_close() ?>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
	function cektanggal() {
		/*----------  Tanggal System  ----------*/
		var today = "<?= date('Ymd'); ?>";
		var monthday = "<?= date('Ym'); ?>";
		var bulan1 = "<?= date('Ym', strtotime('-1 month', strtotime(date('Ym')))); ?>";
		var backmonth = bulan1 + '01';
		var bulantahun = "<?= date('m-Y', strtotime('-1 month', strtotime('01-' . date('m-Y')))); ?>";
		var batas = "<?= $dclosing; ?>";
		var batasawal = "<?= $dopen; ?>";
		var myday = "<?= date('Ym', strtotime($dopen)); ?>";

		/*----------  Tanggal Yang Dipilih  ----------*/
		var dkb = $('#dkb').val();
		var tgl = dkb.split("-")[2] + dkb.split("-")[1] + dkb.split("-")[0];
		var bulan = $('#iperiodebl').val();
		var tahun = $('#iperiodeth').val();

		/*----------  Jika Tanggal Yang Dipilih Lebih Besar Dari Hari Ini  ----------*/
		if ((parseInt(tgl) > parseInt(today)) || (parseInt(tahun + bulan) > parseInt(monthday))) {
			/* alert('Maaf tanggal/periode tidak boleh lebih besar dari Hari Ini!');
			$('#dkb').val('<?= date('d-m-Y'); ?>');
			$('#iperiodebl').val('<?= date('m'); ?>');
			$('#iperiodeth').val('<?= date('Y'); ?>'); */
			return false;
		} else {
			/*----------  Jika Hari Ini Lebih Besar Atau Sama Dengan Tanggal 04 Bulan Berjalan  ----------*/
			if (parseInt(today) >= parseInt(batas)) {
				/*----------  Jika Tanggal Yang Dipilih Lebih Kecil Dari Tanggal 04 Bulan Berjalan  ----------*/
				if (parseInt(tgl) < parseInt(batasawal)) {
					alert('Maaf tanggal tidak boleh lebih kecil dari tanggal ' + '<?= date('d-m-Y', strtotime($dopen)); ?>' + '!');
					$('#dkb').val('<?= date('d-m-Y'); ?>');
					return false;
				}

				/*----------  Jika Periode Yang Dipilih Lebih Kecil Dari Periode Sekarang  ----------*/
				if (parseInt(tahun + bulan) < parseInt(myday)) {
					alert('Maaf, Periode tidak boleh lebih kecil dari periode = ' + myday + ' !');
					$('#iperiodebl').val('<?= date('m'); ?>');
					$('#iperiodeth').val('<?= date('Y'); ?>');
					return false;
				}

				/*----------  Jika Hari Ini Lebih Kecil Dari Tanggal 04 Bulan Berjalan Dan Lebih Besar Dari Tanggal 01 Bulan Sebelumnya  ----------*/
			} else if ((parseInt(today) < parseInt(batas)) && (parseInt(backmonth) < parseInt(batas))) {
				/*----------  Jika Tanggal Yang Dipilih Lebih Kecil Dari Tanggal 01 Bulan Sebelumnya  ----------*/
				if (parseInt(tgl) < parseInt(backmonth)) {
					alert('Maaf tanggal tidak boleh lebih kecil dari tanggal 01-' + bulantahun + '!');
					$('#dkb').val('<?= date('d-m-Y'); ?>');
					return false;
				}

				/*----------  Jika Periode Yang Dipilih Lebih Kecil Dari Periode 1 Bulan Sebelumnya  ----------*/
				if (parseInt(tahun + bulan) < parseInt(bulan1)) {
					alert('Maaf, Periode tidak boleh lebih kecil dari periode sekarang = ' + bulan1 + ' !');
					$('#iperiodebl').val('<?= date('m'); ?>');
					$('#iperiodeth').val('<?= date('Y'); ?>');
					return false;
				}
			}
		}
	}
	// function cektanggal() {

	// 	var today = "<?= date('Ymd'); ?>";
	// 	var bulantahun = '<?= date('m-Y'); ?>';
	// 	var batasawal = "<?= date('Ym') . '01'; ?>";
	// 	var batas = "<?= date('Ym') . '04'; ?>";
	// 	var dkb   = $('#dkb').val();
	// 	var tgl   = dkb.split("-")[2]+dkb.split("-")[1]+dkb.split("-")[0];
	// 	var myday = "<?= date('Ym'); ?>";
	// 	var bulan = $('#iperiodebl').val();
	// 	var tahun = $('#iperiodeth').val();

	// 		if (parseInt(tahun+bulan) < parseInt(myday)) {
	// 			alert('Maaf, Periode tidak boleh lebih kecil dari periode sekarang = '+myday+' !');
	// 			$('#iperiodebl').val('<?= date('m'); ?>');
	// 			$('#iperiodeth').val('<?= date('Y'); ?>');
	// 		}

	// 		if(today<batasawal){
	// 			if (parseInt(today)>=parseInt(batas)) {
	// 				if (parseInt(tgl) < parseInt(batas)) {
	// 					alert('Maaf tanggal tidak boleh lebih kecil dari tanggal 04-'+bulantahun+'!');
	// 					$('#dkb').val('<?= date('d-m-Y'); ?>');
	// 				}
	// 			}

	// 			if (parseInt(dkb.split("-")[2]+dkb.split("-")[1])!=parseInt(tahun+bulan)) {
	// 				alert("Periode harus sama dengan tanggal voucher !!!");
	// 				$('#iperiodebl').val('<?= date('m'); ?>');
	// 				$('#iperiodeth').val('<?= date('Y'); ?>');
	// 				$('#dkb').val('<?= date('d-m-Y'); ?>');
	// 			}
	// 		}else{
	// 			coaperiode	= document.getElementById('periode').value; // dari tm_periode
	// 			dkb			= document.getElementById('dkb').value;
	// 			periode		= document.getElementById('iperiodeth').value+document.getElementById('iperiodebl').value;

	// 			if(periode!='' && coaperiode!='' && periode.length==6)
	// 			{
	// 			if( periode<coaperiode ){
	// 				alert("Periode minimal = "+coaperiode+" !!!");
	// 				document.getElementById('iperiodeth').value='';
	// 				document.getElementById("iperiodebl").selectedIndex = "-1";
	// 				document.getElementById("dkb").value='';
	// 			}else{
	// 				if(periode!='' && dkb!='')
	// 				{
	// 				dtmp=dkb.split('-');
	// 				per=dtmp[2]+dtmp[1];
	// 				if( periode!=per ){
	// 					alert("Periode harus sama dengan tanggal voucher !!!");
	// 					document.getElementById("dkb").value='';
	// 				}
	// 				}
	// 			}
	// 			}
	// 		}
	// }


	function cektanggalold() {
		coaperiode = document.getElementById('periode').value;
		dkb = document.getElementById('dkb').value;
		periode = document.getElementById('iperiodeth').value + document.getElementById('iperiodebl').value;
		if (periode != '' && coaperiode != '' && periode.length == 6) {
			if (periode < coaperiode) {
				alert("Periode minimal = " + coaperiode + " !!!");
				document.getElementById('iperiodeth').value = '';
				document.getElementById("iperiodebl").selectedIndex = "-1";
				document.getElementById("dkb").value = '';
			} else {
				if (periode != '' && dkb != '') {
					dtmp = dkb.split('-');
					per = dtmp[2] + dtmp[1];
					if (periode != per) {
						alert("Periode harus sama dengan tanggal voucher !!!");
						document.getElementById("dkb").value = '';
					}
				}
			}
		}
	}

	function afterSetDateValue(ref_field, target_field, date) {
		cektanggal();
	}
</script>