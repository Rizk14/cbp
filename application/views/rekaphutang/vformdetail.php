<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<?php 
  $query=$this->db->query(" select e_supplier_name from tr_supplier where i_supplier='$isupplier'");
  if ($query->num_rows() > 0){
    foreach($query->result() as $tmp){
      $nama=$tmp->e_supplier_name;
    }
  }else{
    $nama='';
  }
  echo "<center><h2>$page_title ($isupplier-$nama) Periode $iperiode</h2></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'kartuhutang/cform/detail','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
      <th>Tanggal</th>
	    <th>No Bukti</th>
	    <th>Keterangan</th>
	    <th>Pembelian</th>
	    <th>Pelunasan</th>
	    <th>Saldo Akhir</th>
	    <tbody>
	      <?php 
		if($isi){
      if($offset==''){
        $offset=0;
        $saldoakhir=$saldo;
      }else{
        $num=$offset;
        $offset=0;
        $this->db->select(" sum(debet) as debet, sum(kredit) as kredit from(
                            select * from(
                            select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_dtap as tglbukti, a.i_dtap as bukti, 
                            '' as jenis, '1. pembelian' as keterangan, a.v_netto as debet, 0 as kredit
                            from tm_dtap a, tr_supplier c
                            where f_dtap_cancel='f' and a.i_supplier=c.i_supplier and to_char(a.d_dtap, 'yyyymm')='$iperiode'
                            and a.i_supplier='$isupplier'
                            union all
                            select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_bukti as tglbukti, a.i_pelunasanap as bukti, 
                            a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 0 as debet, a.v_jumlah as kredit
                            from tm_pelunasanap a, tr_supplier c, tr_jenis_bayar d
                            where a.f_pelunasanap_cancel='f' and to_char(d_bukti, 'yyyymm')='$iperiode'
                            and a.i_supplier=c.i_supplier and a.i_jenis_bayar=d.i_jenis_bayar 
                            and a.i_supplier='$isupplier'
                            ) as a order by supplier, tglbukti, keterangan, bukti limit $num offset $offset 
                            ) as b");
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $raw){
            $debet =$raw->debet;
            $kredit=$raw->kredit;
          }
		    }
        $saldoakhir=$saldo+$debet-$kredit;
      }
      echo "<tr> 
				    <td></td>
				    <td></td>
				    <td>Saldo</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>".number_format($saldoakhir)."</td>
	  			  </tr>";
			foreach($isi as $row){
        $tmp 	= explode("-", $row->tglbukti);
        $det	= $tmp[2];
        $mon	= $tmp[1];
        $yir 	= $tmp[0];
        $row->tglbukti	= $det."-".$mon."-".$yir;
        if($row->keterangan!='2. LEBIH BAYAR' && $row->keterangan!='2. NOTA KREDIT'){
          $saldoakhir=$saldoakhir+$row->debet-$row->kredit;
        }
			  echo "<tr> 
				  <td>$row->tglbukti</td>
				  <td>$row->bukti</td>
				  <td>$row->keterangan</td>
				  <td align=right>".number_format($row->debet)."</td>
				  <td align=right>".number_format($row->kredit)."</td>
				  <td align=right>".number_format($saldoakhir)."</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
