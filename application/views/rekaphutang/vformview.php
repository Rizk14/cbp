<div id='tmp'>
<h2><?php echo $page_title.' Periode : '.$iperiode; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'rekaphutang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">Supplier</th>
			<th rowspan="2">Sld Awal</th>
			<th colspan="3">Pembelian</th>
			<th rowspan="2">Pelunasan</th>
			<th rowspan="2">Sld Akhir</th>
		</tr>
		<tr>
			<th>DPP</th>
			<th>PPN</th>
			<th>Subtotal</th>
		</tr>
	    <tbody>
	      <?php 
		if($isi){
      if($hal=='')$hal=0;
      $no=$hal;
      $akhir=0;
      $totsa=0;
      $totno=0;
      $totpl=0;
      $total=0;
      
      $totdpp=0; $totppn=0;
      $totsaldoawal = 0; $totpembelian = 0; $totpelunasan = 0; $totsaldoakhir = 0;
		  foreach($isi as $row){
        $no++;
        
        $query=$this->db->query(" select e_supplier_name from tr_supplier where i_supplier='$row->i_supplier'");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $nama=$tmp->e_supplier_name;
          }
		    }

        $akhir=$row->saldo+$row->nota-$row->pl;

        $totsa=$totsa+$row->saldo;
        $totno=$totno+$row->nota;
        $totpl=$totpl+$row->pl;
        $total=$total+$akhir;
        
        $totsaldoawal+= $row->saldo;
        $totpembelian+= $row->nota;
        $totpelunasan+= $row->pl;
        $totsaldoakhir+= $akhir;
        
        $totdpp+= $row->dpp;
        $totppn+= $row->ppn;
        
			  echo "<tr>
				  <td>".$no."</td>
				  <td>".$row->i_supplier." - ".$nama."</td>
				  <td align=right>".number_format($row->saldo)."</td>
				  <td align=right>".number_format($row->dpp)."</td>
				  <td align=right>".number_format($row->ppn)."</td>
				  <td align=right>".number_format($row->nota)."</td>
				  <td align=right>".number_format($row->pl)."</td>
				  <td align=right>".number_format($akhir)."</td>
				</tr>";
			}

		}
	      ?>
	   <tr>
			<td align="right" colspan="2">Total</td>
			<td align="right"><?php echo number_format($totsaldoawal) ?></td>
			<td align="right"><?php echo number_format($totdpp) ?></td>
			<td align="right"><?php echo number_format($totppn) ?></td>
			<td align="right"><?php echo number_format($totpembelian) ?></td>
			<td align="right"><?php echo number_format($totpelunasan) ?></td>
			<td align="right"><?php echo number_format($totsaldoakhir) ?></td>
	   </tr>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
