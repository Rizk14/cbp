<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'siapnotasales/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Tgl SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input readonly id="ispb" name="ispb" type="text" value="<?php echo $isi->i_spb; ?>">
			<input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>"
			   onclick="showCalendar('',this,this,'','dspb',0,20,1)"></td>
		<td>Kelompok Harga</td>
		<td><input readonly id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
<!-- onclick="pricegroup();" -->
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input  readonly id="vspb" name="vspb" value="<?php echo number_format($isi->v_spb); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>"
			onclick='showModal("spb/cform/customer/"+document.getElementById("iarea").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_spb_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10"></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2"
				   value="<?php echo $isi->n_spb_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_spb_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox"
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>
		    SPB Lama&nbsp;&nbsp;&nbsp;<input id="ispbold" name="ispbold" type="text" value="<?php echo $isi->i_spb_old; ?>"></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3"
				   value="<?php echo $isi->n_spb_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_spb_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_spb_toplength; ?>">   Stock Daerah<input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" <?php if($isi->f_spb_stockdaerah=='t') {
				   echo 'checked  value="on"';}else{echo 'value=""';} ?>" onclick="pilihstockdaerah(this.value)"></td></td>
		<td>Discount Total</td>
		<td><input <?php if( ($isi->n_spb_discount1!='0.00') || ($isi->n_spb_discount2!='0.00') || 
						  ($isi->n_spb_discount2!='0.00') ){
						echo "readonly ";
					  }
				   ?>id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
        <input style="text-align:right;" readonly id="persen" name="persen" value="">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" 
				   value="<?php echo number_format($tmp); ?>"></td>
	      </tr>
	      <tr>
    <td width="12%">Promo</td>
		<td width="38%"><input readonly id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name; ?>">
		    <input id="ispbprogram" name="ispbprogram" type="hidden" value="<?php echo $isi->i_spb_program; ?>"></td>
<!--		<td>Stok Daerah</td>
		<td>--><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly type="hidden">
		    <input readonly readonly id="dsj" name="dsj" type="hidden""><!--</td>-->
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly value="<?php echo number_format($isi->v_spb_discounttotalafter); ?>"></td>
	      </tr>
	      <tr>
		<td>PKP</td>
		<td><input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
				   value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input id="vspbafter" name="vspbafter" readonly
				   value="<?php echo number_format($isi->v_spb_after);?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
			<input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("siapnotasales/cform/","#main")'>
      <!--<input name="cmdrefreshharga" id="cmdrefreshharga" value="Refresh" type="button" onclick='refreshharga()'>--></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 920px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:94px;" align="center">Motif</th>
					<th style="width:90px;" align="center">Harga</th>
					<th style="width:46px;" align="center">Jml Psn</th>
					<th style="width:46px;" align="center">Jml Krm</th>
					<th style="width:94px;" align="center">Total</th>
					<th style="width:180px;" align="center">Keterangan</th>
					<th style="width:32px;" align="center" class="action">Act</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{	
					echo '<table class="listtable" style="width:920px;" align="center">';
				  	$i++;
					$pangaos=number_format($row->v_unit_price,2);
					$total=$row->v_unit_price*$row->n_deliver;
					$total=number_format($total,2);
					/*$orde =number_format($row->n_order);
					$deli =number_format($row->n_deliver); */
          $hrgnew=number_format($row->hrgnew,2);
				  	echo "<tbody>
							<tr>
		    				<td style=\"width:21px;\"><input style=\"font-size:12px; width:21px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
													  <input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\">
                <input type=\"hidden\" id=\"iproductstatus$i\" name=\"iproductstatus$i\" value=\"$row->i_product_status\"></td>
							<td style=\"width:58px;\"><input style=\"font-size:12px; width:58px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:258px;\"><input style=\"font-size:12px; width:258px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:82px;\"><input readonly style=\"font-size:12px; width:82px;\" type=\"text\" 
								id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:81px;\"><input readonly style=\"font-size:12px; text-align:right; 
								width:81px;\"  type=\"text\" id=\"vproductretail$i\"
								name=\"vproductretail$i\" value=\"$pangaos\"><input type=\"hidden\" id=\"hrgnew$i\"
								name=\"hrgnew$i\" value=\"$hrgnew\"></td>
							<td style=\"width:42px;\"><input readonly style=\"font-size:12px; text-align:right; width:42px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_order\"></td>
							<td style=\"width:41px;\"><input style=\"font-size:12px; text-align:right; width:41px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_deliver\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem'); pembandingnilai(".$i.");\">
                <input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$row->n_deliver\">
               </td>
							<td style=\"width:83px;\"><input style=\"font-size:12px; text-align:right; width:83px;\" readonly
								type=\"text\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
							<td style=\"width:164px;\"><input style=\"font-size:12px; width:164px;\" readonly
								type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\"></td>
							<td style=\"width:48px;\" align=\"center\">
								<a href=\"javascript:xxx('$row->i_spb','$row->i_product','$row->i_product_grade',
														 '$row->v_unit_price','$row->n_order','$jmlitem',
								   						 '".$this->lang->line('delete_confirm')."',
														 '$row->i_product_motif','$row->i_area'
														);\">";
							if($isi->i_store == null){
								echo"	<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" 
									 border=\"0\" alt=\"delete\"></a>";
							}
							echo	"</td>
							</tr>
						  </tbody></table>";
				}
				?>
			</div>
	  </div>
	</div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,e,f,g,h,i){
    if (confirm(g)==1){
	  document.getElementById("ispbdelete").value=a;
	  document.getElementById("iproductdelete").value=b;
	  document.getElementById("iproductgradedelete").value=c;
	  document.getElementById("iproductmotifdelete").value=h;
	  document.getElementById("iareadelete").value=i;
// 	  --hitung ulang--
		dtmp1	=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2	=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3	=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		vtotdis =parseFloat(formatulang(document.getElementById("vspbdiscounttotal").value));
		vdis1	=0;
		vdis2	=0;
		vdis3	=0;
		vtot	=parseFloat(formatulang(document.getElementById("vspb").value));
		dismin	=parseFloat(d*e);
		vtot	=vtot-dismin;
		vdis1	=vdis1+((vtot*dtmp1)/100);
		vdis2	=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3	=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		vtotdis=vdis1+vdis2+vdis3;
		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);

		document.getElementById("vdis1").value=formatcemua(vdis1);
		document.getElementById("vdis2").value=formatcemua(vdis2);
		document.getElementById("vdis3").value=formatcemua(vdis3);
		document.getElementById("vtotdis").value=formatcemua(vtotdis);
		document.getElementById("vtot").value=formatcemua(vtot);
		document.getElementById("vtotbersih").value=formatcemua(vtotbersih);
		show("spb/cform/deletedetail/"+a+"/"+i+"/"+b+"/"+c+"/"+h+"/"+vdis1+"/"+vdis2+"/"+vdis3+"/"+vtotdis+"/"+vtot+"/","#main");
    }
  }
  function pembandingnilai(a) 
  {
	 	var n_order	= document.getElementById('norder'+a).value;
	  var n_deliver	= document.getElementById('ndeliver'+a).value;
	  var deliverasal	= document.getElementById('ndeliverhidden'+a).value;
	  
	  if(parseInt(n_deliver) > parseInt(n_order)) {
		  alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Order ( '+n_order+' item )');
		  document.getElementById('ndeliver'+a).value	= deliverasal;
		  document.getElementById('ndeliver'+a).focus();
		  return false;
	  }
  }  
  function yyy(b){
	document.getElementById("ispbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spb/cform/edit";
	formna.submit();
  }
  function view_store(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/store/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
		so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		so_inner+= '<th style="width:100px;" align="center">Motif</th>';
		so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
		so_inner+= '<th style="width:46px;"  align="center">Jml Pesan</th>';
		so_inner+= '<th style="width:94px;"  align="center">Total</th>';
		so_inner+= '<th style="width:180px;" align="center">Keterangan</th>';
		so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr></table>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
	so_inner='';
    }
    if(si_inner==''){
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" style="width:930px;" align="center"><tbody><tr><td style="width:21px;"><input style="width:21px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:281px;"><input style="width:281px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:94px;"><input readonly style="width:94px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:88px;"><input disabled="true" style="text-align:right; width:88px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:171px;"><input disabled="true" style="text-align:right; width:171px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';

    }else{
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" style="width:930px;" align="center"><tbody><tr><td style="width:21px;"><input style="width:21px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';

		si_inner+='<td style="width:281px;"><input style="width:281px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:94px;"><input readonly style="width:94px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:88px;"><input disabled="true" style="text-align:right; width:88px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:171px;"><input disabled="true" style="text-align:right; width:171px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris			=Array()
    var iproduct		=Array();
    var eproductname		=Array();
    var vproductretail		=Array();
    var norder			=Array();
    var motif			=Array();
    var motifname		=Array();
    var vtotal			=Array();
    for(i=1;i<a;i++){
		j++;
		baris[j]		=document.getElementById("baris"+i).value;
		iproduct[j]		=document.getElementById("iproduct"+i).value;
		eproductname[j]		=document.getElementById("eproductname"+i).value;
		vproductretail[j]	=document.getElementById("vproductretail"+i).value;
		norder[j]		=document.getElementById("norder"+i).value;
		motif[j]		=document.getElementById("motif"+i).value;
		motifname[j]		=document.getElementById("emotifname"+i).value;
		vtotal[j]		=document.getElementById("vtotal"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vproductretail"+i).value=vproductretail[j];
		document.getElementById("norder"+i).value=norder[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("emotifname"+i).value=motifname[j];
		document.getElementById("vtotal"+i).value=vtotal[j];	
    }
    lebar =500;
    tinggi=400;
	showModal("spb/cform/productupdate/"+a+"/"+document.getElementById("ipricegroup").value+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("icustomer").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
		(document.getElementById("ipricegroup").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
	  	  	document.getElementById("login").disabled=true;
    		}else{
		    	document.getElementById("login").disabled=false;
		}
    	}else{
      		alert('Data header masih ada yang salah !!!');
    	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function view_pelanggan(a){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/customer/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
/*
  function hitungnilai(isi,jml){
    if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		vdis1=0;
		vdis2=0;
		vdis3=0;
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			nqty=formatulang(document.getElementById("ndeliver"+i).value);
			vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		}
		vdis1=vdis1+((vtot*dtmp1)/100);
		vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		vdis1=parseFloat(vdis1);
		vdis2=parseFloat(vdis2);
		vdis3=parseFloat(vdis3);
		vtotdis=vdis1+vdis2+vdis3;
		vtotdis=Math.round(vtotdis);
		vtotbersih=parseFloat(vtot)-parseFloat(vtotdis);
		document.getElementById("vspbdiscounttotalafter").value=formatcemua(vtotdis);
		document.getElementById("vspbafter").value=formatcemua(vtotbersih);
	}
  }
*/
  function hitungnilai(isi,jml){
    jml=document.getElementById("jml").value;
    if (isNaN(parseFloat(isi))){
		  alert("Input harus numerik");
	}else{
		  dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		  dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		  dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		  vdis1=0;
		  vdis2=0;
		  vdis3=0;
		  vdis1x=0;
		  vdis2x=0;
		  vdis3x=0;

		  vtot =0;
		  vtotx=0;
		  for(i=1;i<=jml;i++){
			  vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			  vhrgx=formatulang(document.getElementById("vproductretail"+i).value);
			  nqty=formatulang(document.getElementById("norder"+i).value);
        nqtyx=formatulang(document.getElementById("ndeliver"+i).value);
			  vhrg=parseFloat(vhrg)*parseFloat(nqty);
			  vhrgx=parseFloat(vhrgx)*parseFloat(nqtyx);
			  vtot=vtot+vhrg;
			  vtotx=vtotx+vhrgx;
			  document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		  }
		  vdis1=vdis1+((vtot*dtmp1)/100);
		  vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		  vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		  vdis1x=vdis1x+((vtotx*dtmp1)/100);
		  vdis2x=vdis2x+(((vtotx-vdis1x)*dtmp2)/100);
		  vdis3x=vdis3x+(((vtotx-(vdis1x+vdis2x))*dtmp3)/100);

		  document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
		  document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
		  document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
		  vdis1=parseFloat(vdis1);
		  vdis2=parseFloat(vdis2);
		  vdis3=parseFloat(vdis3);
		  vtotdis=vdis1+vdis2+vdis3;
		  vdis1x=parseFloat(vdis1x);
		  vdis2x=parseFloat(vdis2x);
		  vdis3x=parseFloat(vdis3x);
		  vtotdisx=vdis1x+vdis2x+vdis3x;
//alert(vtotdis);
//alert(vtotdisx);
		  document.getElementById("vspbdiscounttotal").value=formatcemua(Math.round(vtotdis));
		  document.getElementById("vspb").value=formatcemua(vtot);
		  vtotbersih=parseFloat(formatulang(formatcemua(vtot)))-parseFloat(formatulang(formatcemua(Math.round(vtotdis))));
		  document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
      document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vtotdisx));
		  vtotbersihx=parseFloat(formatulang(formatcemua(vtotx)))-parseFloat(formatulang(formatcemua(Math.round(vtotdisx))));
		  document.getElementById("vspbafter").value=formatcemua(vtotbersihx);

      persen=formatcemua((vtotx/vtot)*100)+" %";
		  document.getElementById("persen").value=persen;
	  }
  }
  function pilihstockdaerah(a){
	if(a==''){
		document.getElementById("fspbstockdaerah").value='on';
	}else{
		document.getElementById("fspbstockdaerah").value='';
	}
  }
  $(document).ready(function () {
  	hitungnilai(0,document.getElementById("jml").value);
  });
  function pricegroup()
  {
    spb=document.getElementById("ispb").value;
    area=document.getElementById("iarea").value;
    showModal("siapnotasales/cform/pricegroup/"+spb+"/"+area,"#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  function refreshharga(){
    jml=document.getElementById("jml").value;
    for(i=1;i<=jml;i++){
      document.getElementById("vproductretail"+i).value=document.getElementById("hrgnew"+i).value;
    }
    hitungnilai(0,jml);
  }
</script>
