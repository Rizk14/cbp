<table class="maintable">
  <tr>
    <td align="left">
    <?php echo form_open('spbpromo/cform/update', array('id' => 'spbpromoformupdate', 'name' => 'spbpromoformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Tgl SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input readonly id="ispb" name="ispb" type="text" value="<?php echo $isi->i_spb; ?>">
			<input readonly id="dspb" name="dspb" onclick="showCalendar('',this,this,'','dspb',0,20,1)" value="<?php echo $dspb; ?>"></td>
		<td>Kelompok Harga</td>
		<td><input readonly id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
	      </tr>
	      <tr>
		<td>Kode Promo</td>
		<td><input readonly id="epromoname" name="epromoname" 
			onclick='showModal("spbpromo/cform/promo/"+document.getElementById("dspb").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");' 				value="<?php echo $isi->e_promo_name; ?>">			
		    <input type="hidden" id="ipromo" name="ipromo" value="<?php echo $isi->i_spb_program; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input  readonly id="vspb" name="vspb" value="<?php echo number_format($isi->v_spb); ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" 
			onclick='showModal("spbpromo/cform/area/"+document.getElementById("ipromo").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");'
			value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1"
				   value="<?php echo number_format($isi->v_spb_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" 
			onclick='showModal("spbpromo/cform/customer/"+document.getElementById("ipromo").value+"/"+document.getElementById("iarea").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");' value="<?php echo $isi->e_customer_name; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2" value="<?php echo $isi->n_spb_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="<?php echo number_format($isi->v_spb_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10"></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3" value="<?php echo $isi->n_spb_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="<?php echo number_format($isi->v_spb_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox" <?php if($isi->f_spb_consigment=='t') echo "checked";?>>
		    SPB Lama&nbsp;&nbsp;&nbsp;<input id="ispbold" name="ispbold" type="text" value="<?php echo $isi->i_spb_old; ?>"></td>
		<td>Discount 4</td>
		<td><input readonly id="ncustomerdiscount4" name="ncustomerdiscount4" value="<?php echo $isi->n_spb_discount4; ?>">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" value="<?php echo number_format($isi->v_spb_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly value="<?php echo $isi->n_spb_toplength; ?>">
				   &nbsp;Stock Daerah&nbsp;<input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" 
				   <?php if($isi->f_spb_stockdaerah=='t') {
				   echo 'checked  value="on"';}else{echo 'value=""';} ?>
				   onclick="pilihstockdaerah(this.value)"></td>
		<td>Discount Total</td>
		<td><input <?php if( ($isi->n_spb_discount1!='0.00') || ($isi->n_spb_discount2!='0.00') || 
						  ($isi->n_spb_discount2!='0.00') || ($isi->n_spb_discount2!='0.00') ){
						echo "readonly ";
					  }
				   ?>id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input  readonly id="vspbbersih" name="vspbbersih" readonly 
					value="<?php echo number_format($tmp); ?>"></td>
	      </tr>
	      <tr>
		<td>Stok Daerah</td>
		<?php 
			if($isi->d_sj!=''){			
				$tmp=explode("-",$isi->d_sj);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$dsj=$hr."-".$bl."-".$th;
			}else{
				$dsj='';
			}
		?>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly value="<?php echo $isi->i_sj; ?>">
		    <input readonly readonly id="dsj" name="dsj" value="<?php echo $dsj; ?>"
			   onclick="showCalendar('',this,this,'','dsj',0,20,1)"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly 
				   value="<?php echo number_format($isi->v_spb_discounttotalafter); ?>"></td>
	      </tr>
	      <tr>
		<td>PKP</td>
		<td><input id="fspbplusppn" name="fspbplusppn" type="hidden"
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
				   value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input  readonly id="vspbafter" name="vspbafter" readonly 
					value="<?php echo number_format($isi->v_spb_after);?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input <?php if(($isi->i_store != null) || ($departement != '3')) echo "disabled=true"; ?> name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listspb/cform/","#main");'>
		    <input <?php if(($isi->i_store != null) || ($departement != '3')) echo "disabled=true"; ?> name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 930px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:90px;" align="center">Harga</th>
					<th style="width:46px;" align="center">Jml Psn</th>
					<th style="width:94px;" align="center">Total</th>
					<th style="width:180px;" align="center">Keterangan</th>
					<th style="width:32px;" align="center" class="action">Act</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
					echo '<table class="listtable" align="center" style="width:930px;">';
				  	$i++;
					$pangaos=number_format($row->v_unit_price,2);
					$total=$row->v_unit_price*$row->n_order;
					$total=number_format($total,2);
				  	echo "<tbody>
							<tr>
		    				<td style=\"width:21px;\"><input style=\"font-size:12px; width:21px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
													  <input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td style=\"width:60px;\"><input style=\"font-size:12px; width:60px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:281px;\"><input style=\"font-size:12px; width:281px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:94px;\"><input readonly style=\"font-size:12px; width:94px;\" type=\"text\" 
								id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:85px;\"><input readonly style=\"font-size:12px; text-align:right; 
								width:85px;\"  type=\"text\" id=\"vproductretail$i\"
								name=\"vproductretail$i\" value=\"$pangaos\"></td>
							<td style=\"width:43px;\"><input style=\"font-size:12px; text-align:right; width:43px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_order\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem')\"></td>
							<td style=\"width:88px;\"><input style=\"font-size:12px; text-align:right; width:88px;\" readonly
								type=\"text\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
							<td style=\"width:172px;\"><input style=\"font-size:12px; text-align:right; width:172px;\"
								type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\"></td>
							<td style=\"width:48px;\" align=\"center\">
								<a href=\"javascript:xxx('$row->i_spb','$row->i_product','$row->i_product_grade',
														 '$row->v_unit_price','$row->n_order','$jmlitem',
								   						 '".$this->lang->line('delete_confirm')."',
														 '$row->i_product_motif','$row->i_area','$isi->i_spb_program'
														);\">";
					
							if($isi->i_store == null){
								echo"	<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" 
									 border=\"0\" alt=\"delete\"></a>";
							}
							echo	"</td>
							</tr>
						  </tbody></table>";
				}
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" <?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	  </div>
	</div>
	<?=form_close()?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>'spbpromo/cform','update'=>'#main','id'=>'id1','type'=>'post'));?>
	<?php 
	echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
		  <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" 	value=\"\">
		  <input type=\"hidden\" id=\"iareadelete\" name=\"iareadelete\" 	value=\"\">
		  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
		  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
		  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
		  <input type=\"hidden\" id=\"vdis4\" name=\"vdis4\" value=\"\">
		  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
		  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
		  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		 ";
	?>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,e,f,g,h,i,j){
    if (confirm(g)==1){
	  document.getElementById("ispbdelete").value=a;
	  document.getElementById("iproductdelete").value=b;
	  document.getElementById("iproductgradedelete").value=c;
	  document.getElementById("iproductmotifdelete").value=h;
	  document.getElementById("iareadelete").value=i;
// 	  --hitung ulang--
		dtmp1	=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2	=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3	=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		dtmp4	=parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
		vtotdis =parseFloat(formatulang(document.getElementById("vspbdiscounttotal").value));
		vdis1	=0;
		vdis2	=0;
		vdis3	=0;
		vdis4	=0;
		vtot	=parseFloat(formatulang(document.getElementById("vspb").value));
		dismin	=parseFloat(d*e);
		vtot	=vtot-dismin;
		vdis1	=vdis1+((vtot*dtmp1)/100);
		vdis2	=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3	=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		vdis4	=vdis4+(((vtot-(vdis1+vdis2+vdis3))*dtmp4)/100);
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		document.getElementById("vcustomerdiscount4").value=formatcemua(vdis4);
		vtotdis=vdis1+vdis2+vdis3+vdis4;
		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);

		document.getElementById("vdis1").value=formatcemua(vdis1);
		document.getElementById("vdis2").value=formatcemua(vdis2);
		document.getElementById("vdis3").value=formatcemua(vdis3);
		document.getElementById("vdis4").value=formatcemua(vdis4);
		document.getElementById("vtotdis").value=formatcemua(vtotdis);
		document.getElementById("vtot").value=formatcemua(vtot);
		document.getElementById("vtotbersih").value=formatcemua(vtotbersih);
	  	show("spbpromo/cform/deletedetail/"+a+"/"+i+"/"+b+"/"+c+"/"+h+"/"+vdis1+"/"+vdis2+"/"+vdis3+"/"+vdis4+"/"+vtotdis+"/"+vtot+"/"+j+"/","#main");
    }
  }
  function yyy(b){
	document.getElementById("ispbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spbpromo/cform/edit";
	formna.submit();
  }
  function view_store(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spbpromo/cform/store/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a){
    if(a<22){
	  so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
	so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
	so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
	so_inner+= '<th style="width:63px;" align="center">Kode</th>';
	so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
	so_inner+= '<th style="width:100px;" align="center">Motif</th>';
	so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
	so_inner+= '<th style="width:46px;"  align="center">Jml Psn</th>';
	so_inner+= '<th style="width:94px;"  align="center">Total</th>';
	so_inner+= '<th style="width:180px;" align="center">Keterangan</th>';
	so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr></table>';
	document.getElementById("detailheader").innerHTML=so_inner;
    }else{
	so_inner='';
    }
    if(si_inner==''){
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" style="width:930px;" align="center"><tbody><tr><td style="width:21px;"><input style="width:21px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:281px;"><input style="width:281px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:94px;"><input readonly style="width:94px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:88px;"><input disabled="true" style="text-align:right; width:88px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:172px;"><input disabled="true" style="text-align:right; width:172px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';

    }else{
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" style="width:930px;" align="center"><tbody><tr><td style="width:21px;"><input style="width:21px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:281px;"><input style="width:281px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:94px;"><input readonly style="width:94px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:88px;"><input disabled="true" style="text-align:right; width:88px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:172px;"><input disabled="true" style="text-align:right; width:172px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris		=Array()
    var iproduct	=Array();
    var eproductname	=Array();
    var vproductretail	=Array();
    var norder		=Array();
    var motif		=Array();
    var motifname	=Array();
    var vtotal		=Array();
    var eremark		=Array();
    for(i=1;i<a;i++){
		j++;
		baris[j]		= document.getElementById("baris"+i).value;
		iproduct[j]		= document.getElementById("iproduct"+i).value;
		eproductname[j]		= document.getElementById("eproductname"+i).value;
		vproductretail[j]	= document.getElementById("vproductretail"+i).value;
		norder[j]		= document.getElementById("norder"+i).value;
		motif[j]		= document.getElementById("motif"+i).value;
		motifname[j]		= document.getElementById("emotifname"+i).value;
		vtotal[j]		= document.getElementById("vtotal"+i).value;
		eremark[j]		= document.getElementById("eremark"+i).value;		
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vproductretail"+i).value=vproductretail[j];
		document.getElementById("norder"+i).value=norder[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("emotifname"+i).value=motifname[j];
		document.getElementById("vtotal"+i).value=vtotal[j];
		document.getElementById("eremark"+i).value=eremark[j];		
    }
	kdharga=document.getElementById("ipricegroup").value;
	promo	= document.getElementById("ipromo").value;

	showModal("spbpromo/cform/productupdate/"+a+"/"+kdharga+"/"+promo,"#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }else{
      alert('Maksimum 21 item');
    }
	}
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("icustomer").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
  		(document.getElementById("ipricegroup").value!='')&&
  		(document.getElementById("esalesmanname").value!='')&&
		  (document.getElementById("isalesman").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
  	  		document.getElementById("cmdtambahitem").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function view_promo(a){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spbpromo/cform/promo/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_pelanggan(a,b){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spbpromo/cform/customer/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_area(a){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spbpromo/cform/area/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function hitungnilai(isi,jml){
	jml=document.getElementById("jml").value;
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		dtmp4=parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
		vdis1=0;
		vdis2=0;
		vdis3=0;
		vdis4=0;
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			nqty=formatulang(document.getElementById("norder"+i).value);
			hrg =parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+hrg;
			document.getElementById("vtotal"+i).value=formatcemua(hrg);
		}
		vdis1=vdis1+((vtot*dtmp1)/100);
		vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		vdis4=vdis4+(((vtot-(vdis1+vdis2+vdis3))*dtmp4)/100);
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		document.getElementById("vcustomerdiscount4").value=formatcemua(vdis4);
		vtotdis=vdis1+vdis2+vdis3+vdis4;
		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }

  function diskonrupiah(isi){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot   =parseFloat(formatulang(document.getElementById("vspb").value));
		vtotdis=parseFloat(formatulang(isi));
		vtotbersih=vtot-vtotdis;
//		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
  function pilihstockdaerah(a){
	if(a==''){
		document.getElementById("fspbstockdaerah").value='on';
	}else{
		document.getElementById("fspbstockdaerah").value='';
	}
  }
</script>
