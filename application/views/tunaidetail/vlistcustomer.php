<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'tunaidetail/cform/customer/'.$iarea;
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>"><input type="hidden" id="tgl" name="tgl" value="<?php echo $tgl; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Customer</th>
	    <th>Nama</th>
	     <th>Salesman</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $nama	= str_replace("'","\'",$row->e_customer_name);
			  echo "<tr>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_customer_groupar','$row->i_salesman','$row->e_salesman_name')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_customer_groupar','$row->i_salesman','$row->e_salesman_name')\">$row->e_customer_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_customer_groupar','$row->i_salesman','$row->e_salesman_name')\">$row->e_salesman_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_customer_groupar','$row->i_salesman','$row->e_salesman_name')\">$row->e_customer_setor</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e)
  {
    document.getElementById("icustomer").value=a;
    document.getElementById("ecustomername").value=b;
    document.getElementById("icustomergroupar").value=c;
    document.getElementById("isalesman").value=d;
    document.getElementById("esalesmanname").value=e;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
