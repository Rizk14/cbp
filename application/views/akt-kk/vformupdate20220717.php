<?php 
	echo "<h2>".$page_title."</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-kk/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="aktkkform">
	<div class="effect">
	  <div class="accordion2">
		 <?php foreach($isi as $row){ ?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Nomor</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="ikk" id="ikk" value="<?php echo $row->i_kk; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="ipvtype" id="ipvtype" value="00"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>"><input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><select readonly name="iperiodebl" id="iperiodebl">
							<?php $bl=substr($row->i_periode,4,2);?>
							<option <?php if($bl=='01') echo 'selected'; ?>>01</option>
							<option <?php if($bl=='02') echo 'selected'; ?>>02</option>
							<option <?php if($bl=='03') echo 'selected'; ?>>03</option>
							<option <?php if($bl=='04') echo 'selected'; ?>>04</option>
							<option <?php if($bl=='05') echo 'selected'; ?>>05</option>
							<option <?php if($bl=='06') echo 'selected'; ?>>06</option>
							<option <?php if($bl=='07') echo 'selected'; ?>>07</option>
							<option <?php if($bl=='08') echo 'selected'; ?>>08</option>
							<option <?php if($bl=='09') echo 'selected'; ?>>09</option>
							<option <?php if($bl=='10') echo 'selected'; ?>>10</option>
							<option <?php if($bl=='11') echo 'selected'; ?>>11</option>
							<option <?php if($bl=='12') echo 'selected'; ?>>12</option>
						</select>&nbsp;<input readonly name="iperiodeth" id="iperiodeth" value="<?php echo substr($row->i_periode,0,4);?>" maxlength="4"></td>
	      </tr>
		<tr>
		<?php 
			$tmp=explode('-',$row->d_kk);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_kk=$tgl.'-'.$bln.'-'.$thn;
			$row->d_kkk=$thn.'-'.$bln.'-'.$tgl;
		?>
		<td width="12%">Tanggal</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dkk" id="dkk" value="<?php echo $row->d_kk; ?>" onclick="showCalendar('',this,this,'','dkk',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Rekening</td>
		<td width="1%">:</td>
		<td width="87%">
      <input type="hidden" name="icoa" id="icoa" value="<?php echo $row->i_coa; ?>" onclick='showModal("akt-kk/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
      <input readonly name="ecoaname" id="ecoaname" value="<?php echo $row->e_coa_name; ?>"  onclick='showModal("akt-kk/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
		<?php 
			$tmp=explode('-',$row->d_bukti);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
		?>
	      <?php 
	      if($lepel==00 || $lepel=='00') {
	      ?> 		
		<tr>
		<td width="12%">No Bukti</td>
		<td width="1%">:</td>
		<td width="87%"><input type="text" name="ibukti" id="ibukti" value="<?php echo $row->i_bukti_pengeluaran; ?>" onclick="cektanggal();"></td>
	      </tr>
	      <?php } else { ?>
		<input type="hidden" name="ibukti" id="ibukti" value="">
	      <?php } ?>
		<tr>
		<td width="12%">Tanggal Bukti</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dbukti" id="dbukti" value="<?php echo $row->d_bukti; ?>" onclick="showCalendar('',this,this,'','dbukti',0,20,1)">
						Toko &nbsp;: <input name="enamatoko" id="enamatoko" value="<?php echo $row->e_nama_toko; ?>"></td>
	      </tr>
	      <tr>
		<!--<td width="12%">Plat Nomor</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="ikendaraan" id="ikendaraan" value="<?php echo $row->i_kendaraan; ?>" onclick="kendaraan();"></td>
	      --></tr>
	      <tr>
	<!--<td width="12%">Pengguna</td>
		<td width="1%">:</td>
		<td width="87%"><input name="epengguna" id="epengguna" value="<?php echo $row->e_pengguna; ?>"></td>
	     -->   </tr>
	      <tr>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="edescription" id="edescription" value="<?php echo $row->e_description; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="87%"><input name="vkk" id="vkk" value="<?php echo number_format($row->v_kk); ?>" onkeyup="reformat(this);" <?php if($this->session->userdata('user_id')=="akt-cbp"){ ;?> readonly <?php } ;?>></td>
	      </tr>
	      <tr>
		<!--<td width="12%">Tempat</td>
		<td width="1%">:</td>
		<td width="87%"><input name="etempat" id="etempat" value="<?php echo $row->e_tempat; ?>"></td>
	     --> </tr>
	      <tr>
	<!--	<td width="12%">Jam Masuk</td>
		<td width="1%">:</td>
		<td width="87%"><input name="ejamin" id="ejamin" value="<?php echo $row->e_jam_in; ?>"></td>
	    -->  </tr>
	      <tr>
	<!--	<td width="12%">Jam Keluar</td>
		<td width="1%">:</td>
		<td width="87%"><input name="ejamout" id="ejamout" value="<?php echo $row->e_jam_out; ?>"></td>
	    -->  </tr>
	      <tr>
		<!--<td width="12%">Kilo Meter</td>
		<td width="1%">:</td>
		<td width="87%"><input name="nkm" id="nkm" value="<?php echo number_format($row->n_km); ?>" onkeyup="reformat(this);"></td>
	      --></tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		<?php 
		  if($row->i_cek=='' && $bisaedit){
			//   if($now <= $row->d_kkk ){
		?>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		<?php 
			//   }
		  }
		?>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('list-akt-kk/cform/view/<?php echo $dfrom.'/'.$dto.'/'.$iarea; ?>','#main')">
		</td>
	      </tr>
		</table>
		<?php }?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("iarea").value=='') ||
			(document.getElementById("dkk").value=='') ||
			(document.getElementById("icoa").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function kendaraan()
	{
		area=document.getElementById("iarea").value;
		periode=document.getElementById("iperiodeth").value+document.getElementById("iperiodebl").value;
		showModal("akt-kk/cform/kendaraan/"+area+"/"+periode+"/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function cektanggal()
  {
		dbukti=document.getElementById('dbukti'+i).value;
    dkk=document.getElementById('dkk').value;
	  dtmp=dbukti.split('-');
	  thnbk=dtmp[2];
		blnbk=dtmp[1];
		hrbk =dtmp[0];
	  dtmp=dkk.split('-');
	  thnkk=dtmp[2];
		blnkk=dtmp[1];
		hrkk =dtmp[0];
	  if( thnbk>thnkk ){
			alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
	  }else if (thnbk==thnkk){
      if( blnbk>blnkk ){
				alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
			}else if( blnbk==blnkk ){
        if( hrbk>hrkk ){
					alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
				}
      }
    }
	}
</script>
