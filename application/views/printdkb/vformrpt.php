<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$hal	= 1;
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab		= str_repeat(" ",9);
		$ipp  = new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
//		CetakHeader($row,$hal,$nor,$abn,$ab,$host,$uri);
####################################################
		$cetak.=CHR(18);		
		$cetak.=$nor.NmPerusahaan."\n\n";		
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."DAFTAR KIRIMAN BARANG".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n";		
		$cetak.=$nor."No. ".$row->i_dkb."/".$row->i_area."-".$row->e_area_name."\n";		
		$cetak.=CHR(15);		
		$cetak.=$ab."Kirim ke     : ".$row->e_dkb_kirim."\n";		
		$cetak.=$ab."Via Angkutan : ".$row->e_dkb_via;
		if($row->i_dkb_via=='1'){
		 $cetak.=$ab."Via Angkutan : ".$row->e_dkb_via.str_repeat(" ",44)."Nama Ekspedisi    : ".$row->i_ekspedisi."-".$row->e_ekspedisi."\n";
   	}else{
     $cetak.=$ab."Via Angkutan : ".$row->e_dkb_via.str_repeat(" ",44)."Nama Supir/Helper : ".$row->e_sopir_name."\n";
		}
		$cetak.=$ab.CHR(218).str_repeat(CHR(196),2).CHR(194).str_repeat(CHR(196),61).CHR(194).str_repeat(CHR(196),5).CHR(194).str_repeat(CHR(196),47).CHR(194).str_repeat(CHR(196),12).CHR(191)."\n";
		$cetak.=$ab.CHR(179)."  ".CHR(179)."                        D E B I T U R                        ".CHR(179)."KODE ".CHR(179)."              SURAT JALAN                      ".CHR(179)."            ".CHR(179)."\n";
		$cetak.=$ab.CHR(179)."NO".CHR(195).str_repeat(CHR(196),5).CHR(194).str_repeat(CHR(196),27).CHR(194).str_repeat(CHR(196),27).CHR(180)."SALES".CHR(195).str_repeat(CHR(196),8).CHR(194).str_repeat(CHR(196),11).CHR(194).str_repeat(CHR(196),11).CHR(194).str_repeat(CHR(196),14).CHR(180)." KETERANGAN ".CHR(179)."\n";		
		$cetak.=$ab.CHR(179)."  ".CHR(179)."KODE ".CHR(179)."        N A M A            ".CHR(179)."          K O T A          ".CHR(179)." MAN ".CHR(179)." NOMOR  ".CHR(179)."TANGGAL SJ ".CHR(179)."TANGGAL SPB".CHR(179)." NILAI        ".CHR(179)."            ".CHR(179)."\n";		
		$cetak.=$ab.CHR(198).str_repeat(CHR(205),2).CHR(216).str_repeat(CHR(205),5).CHR(216).str_repeat(CHR(205),27).CHR(216).str_repeat(CHR(205),27).CHR(216).str_repeat(CHR(205),5).CHR(216).str_repeat(CHR(205),8).CHR(216).str_repeat(CHR(205),11).CHR(216).str_repeat(CHR(205),11).CHR(216).str_repeat(CHR(205),14).CHR(216).str_repeat(CHR(205),12).CHR(181)."\n";		
#####################################################
		$idkb	= $row->i_dkb;
		$iarea	= $row->i_area;
		$query 	= $this->db->query(" select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'",false);
		$jml 	= $query->num_rows();
		$i=0;	
		$kes=0;
		$kre=0;
		$vkes=0;
		$vkre=0;
		foreach($detail as $rowi){
			$i++;
			if($rowi->n_spb_toplength==0){
				$kes++;
				$vkes=$vkes+$rowi->v_jumlah;
			}else{ 
				$kre++;
				$vkre=$vkre+$rowi->v_jumlah;
			}
			if(strlen($i)==2)$aw=0;
			if(strlen($i)==1)$aw=1;
			$aw=str_repeat(" ",$aw);
			$cust	= $rowi->i_customer;
			$name	= $rowi->e_customer_name;
			if(strlen($name)>27){
				$name=substr($name,0,27);
			}
			$name	= $name.str_repeat(" ",27-strlen($name));
			$city	= $rowi->e_customer_city;
			if(strlen($city)>27){
				$city=substr($city,0,27);
			}
			$city	= $city.str_repeat(" ",27-strlen($city));
			$sls	= $rowi->i_salesman;
			$isj	= substr($rowi->i_sj,8,6);
      $tmp=explode("-",$rowi->d_sj);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dsj  = $hr."-".$bl."-".$th;
#			$dsj	= $rowi->d_sj;
      $tmp=explode("-",$rowi->d_spb);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dspb  = $hr."-".$bl."-".$th;
			$vsj	= number_format($rowi->v_jumlah);
			$vsj	= str_repeat(" ",14-strlen($vsj)).$vsj;
			$ket	= substr($rowi->e_remark,0,12);
			$ket	= $ket.str_repeat(" ",12-strlen($ket));
			$cetak.=$ab.CHR(179).$aw.$i.CHR(179).$cust.CHR(179).$name.CHR(179).$city.CHR(179)." ".$sls."  ".CHR(179).$isj."  ".CHR(179).$dsj." ".CHR(179).$dspb." ".CHR(179).$vsj.CHR(179).$ket.CHR(179)."\n";			
		}
//		CetakFooter($row,$nor,$abn,$ab,$j,$host,$uri);		
################################################################
		$cetak.=CHR(15);		
		$cetak.=$ab.CHR(195).str_repeat(CHR(196),2).CHR(193).str_repeat(CHR(196),5).CHR(193).str_repeat(CHR(196),27).CHR(193).str_repeat(CHR(196),27).CHR(193).str_repeat(CHR(196),5).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),11).CHR(193).str_repeat(CHR(196),11).CHR(197).str_repeat(CHR(196),14).CHR(197).str_repeat(CHR(196),12).CHR(217)."\n";		
		$vsj	= number_format($row->v_dkb);
		$vsj	= str_repeat(" ",14-strlen($vsj)).$vsj;
		$tmp=explode("-",$row->d_dkb);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$ddkb=$hr." ".mbulan($bl)." ".$th;		
		$cetak.=$ab.CHR(179).str_repeat(" ",93)."Jumlah Rp.".CHR(179).$vsj.CHR(179)."\n";		
		$cetak.=$ab.CHR(192).str_repeat(CHR(196),103).CHR(193).str_repeat(CHR(196),14).CHR(217)."\n\n\n";		
		$cetak.=$ab.str_repeat(" ",106)."Bandung, ".$ddkb."\n";		
		$cetak.=$ab.CHR(218).str_repeat(CHR(196),43).CHR(194).str_repeat(CHR(196),43).CHR(194).str_repeat(CHR(196),43).CHR(191)."\n";		
		$cetak.=$ab.CHR(179).str_repeat(" ",17)."Berangkat".str_repeat(" ",17).CHR(179).str_repeat(" ",17)."Kembali".str_repeat(" ",19).CHR(179).str_repeat(" ",14)."Diterima oleh,".str_repeat(" ",15).CHR(179)."\n";		
		$cetak.=$ab.CHR(195).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),21).CHR(197).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),21).CHR(197).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),21).CHR(180)."\n";		
		$cetak.=$ab.CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179)."\n";		
		$cetak.=$ab.CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179).str_repeat(" ",21).CHR(179)."\n";		
		$cetak.=$ab.CHR(179).str_repeat(" ",4)."(Kep.Gudang)".str_repeat(" ",5).CHR(179).str_repeat(" ",4)."(Ekspeditur)".str_repeat(" ",5).CHR(179).str_repeat(" ",4)."(Kep.Gudang)".str_repeat(" ",5).CHR(179).str_repeat(" ",4)."(Ekspeditur)".str_repeat(" ",5).CHR(179).str_repeat(" ",7)."(Kasir)".str_repeat(" ",7).CHR(179).str_repeat(" ",4)."(Adm Piutang)".str_repeat(" ",4).CHR(179)."\n";		
		$cetak.=$ab.CHR(192).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),21).CHR(217)."\n";		
		$vkes=number_format($vkes);
		$vkre=number_format($vkre);
		$cetak.=$ab.str_repeat(" ",67)."Catatan : Cash ".$kes." Nota     Kredit ".$kre." Nota \n";		
    if(strlen($vkes)==1)
      $spasi=str_repeat(" ", 11);
    elseif(strlen($vkes)==2)
      $spasi=str_repeat(" ", 10);
    elseif(strlen($vkes)==3)
      $spasi=str_repeat(" ", 9);
    elseif(strlen($vkes)==4)
      $spasi=str_repeat(" ", 8);
    elseif(strlen($vkes)==5)
      $spasi=str_repeat(" ", 7);
    elseif(strlen($vkes)==6)
      $spasi=str_repeat(" ", 6);
    elseif(strlen($vkes)==7)
      $spasi=str_repeat(" ", 5);
    elseif(strlen($vkes)==8)
      $spasi=str_repeat(" ", 4);
    elseif(strlen($vkes)==9)
      $spasi=str_repeat(" ", 3);
    elseif(strlen($vkes)==10)
      $spasi=str_repeat(" ", 2);
    elseif(strlen($vkes)>=11)
      $spasi=str_repeat(" ", 1);
		$cetak.=$ab." Putih  : Pusat".str_repeat(" ",62)."Rp. ".$vkes.$spasi.$vkre."\n";		
		$cetak.=$ab." Merah  : Adm. Piutang\n";		
		$cetak.=$ab." Kuning : Gudang\n";		
		$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
		$cetak.=$ab." TANGGAL CETAK : ".$tgl."\n";		
		$ipp->setFormFeed();
		$cetak.=CHR(18);		
    break;
	}
 # echo $cetak;
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";
?>
