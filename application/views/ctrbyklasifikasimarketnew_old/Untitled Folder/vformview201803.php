<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyklasifikasimarketnew/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <?php 
          if ($groupx) {
            foreach ($groupx as $gro) {
             $gr=$gro->group;
             $isi  = $this->mmaster->baca($th,$prevth,$gr); 
          if($isi){
            $totoaprev=0;
            $totqtyprev=0;
            $totqty=0;
            $totoa=0;
            $totvnotaprev=0;
            $totvnota=0;
            foreach($isi as $ro){
              $totoaprev=$totoaprev+$ro->oaprev;
              $totoa=$totoa+$ro->oa;
              $totvnotaprev=$totvnotaprev+$ro->vnotaprev;
              $totvnota=$totvnota+$ro->vnota;
              $totqty=$totqty+$ro->qnota;
              $totqtyprev=$totqtyprev+$ro->qnotaprev;
            }
          }

        ?>
        <table class="mastertable">
         <tr>
            <td style="width:100px;" align ="center"><?php echo $gr;?></td>
            <td style="width:5px;"></td>
            <td colspan=2></td>
            <td style="width:5px;"></td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <table class="listtable">
         <tr>
         <th rowspan=3>No</th>
         <th rowspan=3>Klasifikasi Market</th>
         <th colspan=3>OA</th>
         <th colspan=3>Qty Sales</th>
         <th colspan=3>Net Sales</th>
         <th rowspan=2>% CTR</th>
         </tr>
         <!--<tr>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustlm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totoaprev); ?></th>
         <th></th>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustcm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totoa); ?></th>
         <th></th>
         <th colspan=2>MTD</th>
         <th colspan=2>YTD</th>
         </tr>-->
         <tr>
         <th><?php echo $prevth; ?></th>
         <th><?php echo $th; ?></th>
         <th>% Growth</th>
         <th><?php echo $prevth; ?></th>
         <th><?php echo $th; ?></th>
         <th>% Growth</th>
         <th><?php echo $prevth; ?></th>
         <th><?php echo $th; ?></th>
         <th>% Growth</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totnota=0;
        $totpersenoaprev=0;
        $totpersenoa=0;
        $totctrrp=0;


        foreach($isi as $rowa){
            $totnota+=$rowa->vnota;
        }
        $totrp=$totnota;

        foreach($isi as $row){
          $i++;
          $growthnota=0;
          $growthoa=0;
          $growthqty=0;


          $qtyly = $row->qnota-$row->qnotaprev;
          $spbsely=$row->vnota-$row->vnotaprev;
          $custsely=$row->oa-$row->oaprev;
          ####################################################
          $growthytdval=$totvnota/$totvnotaprev*100-100;
          $growthytdoa=$totoa/$totoaprev*100-100;
          $growthytdqty=$totqty/$totqtyprev*100-100;
          $ctrrp= $row->vnota/$totrp;
          $totctrrp= $totctrrp+$ctrrp;

          if($row->vnotaprev==0){
            $growthnota=0;
          }else{
            $growthnota=($spbsely/$row->vnotaprev)*100;
          }
          if($row->oaprev==0){
            $growthoa=0;
          }else{
            $growthoa=($custsely/$row->oaprev)*100;
          }
          if($row->qnotaprev==0){
            $growthqty=0;
          }else{
            $growthqty=($qtyly/$row->qnotaprev)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->e_customer_classname</td>

              <td style='font-size:12px;' align=right>".number_format($row->oaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthoa,2)." %</td>

              <td style='font-size:12px;' align=right>".number_format($row->qnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthqty)." %</td>

              <td style='font-size:12px;' align=right>".number_format($row->vnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthnota,2)." %</td>

              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
         }
        $growthytdval=$totvnota/$totvnotaprev*100-100;
        $growthytdoa=$totoa/$totoaprev*100-100;
        $growthytdqty=$totqty/$totqtyprev*100-100;

        echo "<tr>
        <td style='font-size:12px;' colspan=2><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdoa,2)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totqtyprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqty)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdqty)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totvnotaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdval,2)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>";
      }
      }
      }
         ?>
       </tbody>
     </table>
     <?php 
           $i=0;
      $n=0;
      $d='';
      $result = $this->mmaster->bacagroup($th,$prevth);
      foreach($result as $gro){
        $gr=$gro->group;
     $tipe=$this->uri->segment(5);
      if($tipe==''){
      $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = $gr;
      $graph_numberPrefix = '' ;
      $graph_title        = $gr;
      $graph_width        = 954;
      $graph_height       = 500;
            // data set
      $dataset[0] = 'GROWTHOA' ;
      $dataset[1] = 'GROWTHQTY';
      $dataset[2] = 'GROWTHRP';

      //data 1

      $result = $this->mmaster->bacaclass($th,$prevth);
      foreach($result as $row){
        $category[$i] = $row->e_customer_classname;
        $i++;
      }

      $result = $this->mmaster->baca($th,$prevth,$gr);
      $grwoa=0;
      foreach($result as $row){
        if ($row->oaprev == 0) {
              $grwoa = 0;
            } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->oaprev)/$row->oaprev);
          }
        
        $arrData['GROWTHOA'][$i] = number_format($grwoa,2);
        $i++;
      }

      $result = $this->mmaster->baca($th,$prevth,$gr);
      $grwqty=0;
      foreach($result as $row){
            if ($row->qnotaprev == 0) {
                $grwqty = 0;
            } else { //jika pembagi tidak 0
                $grwqty = (($row->qnota-$row->qnotaprev)/$row->qnotaprev);
            }
        
        $arrData['GROWTHQTY'][$i] = number_format($grwqty,2);
        $i++;
      }

      $result = $this->mmaster->baca($th,$prevth,$gr);
      $grwrp=0;
      foreach($result as $row){
            if ($row->vnotaprev == 0) {
                $grwrp = 0;
            } else { //jika pembagi tidak 0
                $grwrp = (($row->vnota-$row->vnotaprev)/$row->vnotaprev);
            }

        $arrData['GROWTHRP'][$i] = number_format($grwrp,2);
        $i++;
      }

      $strXML= "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='5' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30' id='".$n."'>" ;

      #      $strXML = "caption=Factory Output report;subCaption=By Quantity;pieSliceDepth=30; showBorder=1;formatNumberScale=0;numberSuffix=Units;animation=1";
      # $FC->setChartParams($strXML);

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
      $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
      $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
      foreach ($arrData[$set] as $d) {
        $strXML .= "<set value='".$d."'/>" ;
      }
      $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";
      $d="div";
      $graph.$n = renderChart($graph_swfFile, $graph_title, $strXML , $d , $graph_width, $graph_height);
      $modul=$gr;
      directory_map('./flash/');
      $file='';
      echo $graph.$n;
      $graph_caption      = '';
      $graph_numberPrefix = '' ;
      $graph_title        = '';
      $graph_width        = '';
      $graph_height       = '';
      $graph_swfFile ='';
      $strXML='';
      $n++;
      }

   //   for ($i=0; $i<=$n; $i++) {
       
    //  }
     ?>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
