<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyklasifikasimarket/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $th; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totcustlm=0;
            $totcustly=0;
            $totcustcm=0;
            $totcustcy=0;
            $totnotaly=0;
            $totnotacm=0;
            $totnotalm=0;
            $totnotacy=0;
            foreach($isi as $ro){
              $totcustlm=$totcustlm+$ro->custlm;
              $totcustly=$totcustly+$ro->custly;
              $totcustcm=$totcustcm+$ro->custcm;
              $totcustcy=$totcustcy+$ro->custcy;
              $totnotaly=$totnotaly+$ro->notaly;
              $totnotacm=$totnotacm+$ro->notacm;
              $totnotalm=$totnotalm+$ro->notalm;
              $totnotacy=$totnotacy+$ro->notacy;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan=3>No</th>
         <th rowspan=3>Klasifikasi Market</th>
         <th colspan=6><?php echo $prevth; ?></th>
         <th colspan=6><?php echo $th; ?></th>
         <th colspan=4>% Growth</th>
         </tr>
         <tr>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustlm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totcustly); ?></th>
         <th></th>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustcm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totcustcy); ?></th>
         <th></th>
         <th colspan=2>MTD</th>
         <th colspan=2>YTD</th>
         </tr>
         <tr>
         <th>Value</th>
         <th>OA</th>
         <th>%</th>
         <th>Value</th>
         <th>OA</th>
         <th>%</th>
         <th>Value</th>
         <th>OA</th>
         <th>%</th>
         <th>Value</th>
         <th>OA</th>
         <th>%</th>
         <th>Value</th>
         <th>OA</th>
         <th>Value</th>
         <th>OA</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totpersencustlm=0;
        $totpersencustly=0;
        $totpersencustcm=0;
        $totpersencustcy=0;
        foreach($isi as $row){
          $i++;
          
          $persenspbselm=0;
          $persencustselm=0;
          $persenspbsely=0;
          $persencustsely=0;

          $persencustlm=($row->custlm/$totcustlm)*100;
          $persencustly=($row->custly/$totcustly)*100;
          $persencustcm=($row->custcm/$totcustcm)*100;
          $persencustcy=($row->custcy/$totcustcy)*100;
          $totpersencustlm=$totpersencustlm+$persencustlm;
          $totpersencustly=$totpersencustly+$persencustly;
          $totpersencustcm=$totpersencustcm+$persencustcm;
          $totpersencustcy=$totpersencustcy+$persencustcy;
          $spbselm=$row->notacm-$row->notalm;
          $custselm=$row->custcm-$row->custlm;
          $spbsely=$row->notacy-$row->notaly;
          $custsely=$row->custcy-$row->custly;
          ####################################################
          $growthmtdvalue=$totnotacm/$totnotalm*100-100;
          $growthmtdvol=$totnotacm/$totnotalm*100-100;
          $growthmtdoa=$totcustcm/$totcustcy*100-100;
          $growthytdval=$totnotacy/$totnotaly*100-100;
          $growthytdoa=$totcustcy/$totcustly*100-100;

          if($row->notalm==0){
            $persenspbselm=0;
          }else{
            $persenspbselm=($spbselm/$row->notalm)*100;
          }
          if($row->custlm==0){
            $persencustselm=0;
          }else{
            $persencustselm=($custselm/$row->custlm)*100;
          }
          if($row->notaly==0){
            $persenspbsely=0;
          }else{
            $persenspbsely=($spbsely/$row->notaly)*100;
          }
          if($row->custly==0){
            $persencustsely=0;
          }else{
            $persencustsely=($custsely/$row->custly)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->e_customer_classname</td>

              <td style='font-size:12px;' align=right>".number_format($row->notalm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->custlm)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustlm,2)." %</td>

              <td style='font-size:12px;' align=right>".number_format($row->notaly)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->custly)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustly,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->notacm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->custcm)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustcm,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->notacy)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->custcy)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustcy,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persenspbselm,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustselm,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenspbsely,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustsely,2)."</td>
              </tr>";
         }
        $growthmtdvalue=$totnotacm/$totnotalm*100-100;
        $growthmtdvol=$totnotacm/$totnotalm*100-100;
        $growthmtdoa=$totcustcm/$totcustlm*100-100;
        $growthytdval=$totnotacy/$totnotaly*100-100;
        $growthytdoa=$totcustcy/$totcustly*100-100;
        echo "<tr>
        <td style='font-size:12px;' colspan=2><b>Total</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totnotalm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totcustlm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersencustlm,2)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totnotaly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totcustly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersencustly,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotacm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totcustcm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersencustcm,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotacy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totcustcy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersencustcy,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdvalue,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdoa,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdval,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdoa,2)."</b></td>
        </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
