<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab	= str_repeat(" ",9);
		$ipp 	= new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$tmp=explode("-",$row->d_bbkretur);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$bbk=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$tmp=explode("-",$row->d_kn);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dkn=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$tmp=explode("-",$row->d_refference);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$drefference=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$cetak.=CHR(18)."\n";
		$cetak.=$nor.NmPerusahaan."        Tgl. : ".$dkn."\n\n";		
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."K R E D I T    N O T A".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0).str_repeat(" ",13)."Kepada Yth.\n";
		$cetak.=$nor."No.: ".$row->i_kn.str_repeat(" ",22).$row->i_supplier."-".$row->e_supplier_name."\n";
    $pjg=9+strlen($row->e_salesman_name);
    $spcsls=35;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcsls=$spcsls-1;
		}
    $pjg=6+strlen($dkn);
    $spcref=54;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcref=$spcref-1;
		}
    $pjg=strlen(number_format($row->v_bbkretur));
    $spcnet=13;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcnet=$spcnet-1;
		}
		$cetak.=$nor.CHR(15).$row->e_supplier_address." ".$row->e_supplier_city.CHR(18)."\n";
    $cetak.=$nor.CHR(218).str_repeat(CHR(196),54).CHR(194).str_repeat(CHR(196),16).CHR(191)."\n";
    $cetak.=$nor.CHR(179)." Telah kami Kredit Rekening Saudara atas ".str_repeat(" ",13).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179)." Sesuai dengan BBK Nomor ".$row->i_bbkretur.str_repeat(" ",14).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179)." Tgl. ".$dkn.str_repeat(" ",$spcref).CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_bbkretur).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(192).str_repeat(CHR(196),54).CHR(197).str_repeat(CHR(196),16).CHR(180)."\n";
    $cetak.=$nor.str_repeat(" ",55).CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_bbkretur).CHR(179)."\n";
    $cetak.=$nor.str_repeat(" ",55).CHR(192).str_repeat(CHR(196),16).CHR(217)."\n";
		$bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($row->v_bbkretur));	
		$cetak.=$nor.CHR(15)."Terbilang : ".$kata." RUPIAH\n\n".CHR(18);				
    $cetak.=$nor."    Mengetahui                                           Hormat kami,   \n\n\n\n\n";
    $cetak.=$nor."( ".TahuKN1." )                                        ( ".BikinKN1." ) \n".CHR(15);
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $cetak.=$ab."TANGGAL CETAK : ".$tgl."\n\n\n\n\n\n".CHR(18);			
  
#####
    $ipp->setFormFeed();
    $ipp->setdata($cetak);
    $ipp->printJob();
	}
#  echo $cetak;
	echo "<script>this.close();</script>";
?>
