<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Untitled Document</title>
</head>

<body>
  <style type="text/css" media="all">
    /*
@page land {size: landscape;}
*/
    * {
      size: landscape;
    }

    @page {
      size: Letter;
    }

    .huruf {
      FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
    }

    .miring {
      font-style: italic;

    }

    .ceKotak {
      - background-color: #f0f0f0;
      border-bottom: #80c0e0 1px solid;
      border-top: #80c0e0 1px solid;
      border-left: #80c0e0 1px solid;
      border-right: #80c0e0 1px solid;
    }

    .garisy {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisy td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      padding: 1px;
    }

    .garisx {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: none;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisx td {
      background-color: #FFFFFF;
      border-style: none;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .judul {
      font-size: 20px;
      FONT-WEIGHT: normal;
    }

    .nmper {
      font-size: 18px;
      FONT-WEIGHT: normal;
    }

    .isi {
      font-size: 14px;
      font-weight: normal;
      padding: 1px;
    }

    .eusinya {
      font-size: 10px;
      font-weight: normal;
    }
  </style>
  <style type="text/css" media="print">
    .noDisplay {
      display: none;
    }

    .pagebreak {
      page-break-before: always;
    }
  </style>
  <?php
  foreach ($isi as $row) {
  ?>
    <table width="985" border="0">
      <tr>
        <td colspan="3" class="nmper"><?php echo NmPerusahaan; ?></td>
      </tr>
      <tr>
        <td colspan="3" class="judul">DAFTAR KIRIMAN BARANG </td>
      </tr>
      <tr>
        <td colspan="3">No. <?php echo "$row->i_dkb/$row->i_area-$row->e_area_name"; ?></td>
      </tr>
      <tr>
        <td colspan="3">Kirim ke : <?php echo $row->e_dkb_kirim; ?></td>
      </tr>
      <?php if ($row->i_dkb_via == '1') {
        echo "<tr><td width=\"339\">Via Angkutan : $row->e_dkb_via</td>";
        echo "<td width=\"333\" align=\"right\">Nama Ekspedisi : $row->i_ekspedisi-$row->e_ekspedisi</td></tr>";
      } else {
        echo "<tr><td width=\"339\">Via Angkutan : $row->e_dkb_via " . " / " . $row->i_kendaraan . "</td>";
        echo "<td width=\"333\" align=\"right\">Nama Supir/Helper : $row->e_sopir_name</td></tr>";
      } ?>
    </table>
    <?php
    $idkb  = $row->i_dkb;
    $iarea  = $row->i_area;
    $query   = $this->db->query(" select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'", false);
    $jml   = $query->num_rows();
    $i = 0;
    $kes = 0;
    $kre = 0;
    $vkes = 0;
    $vkre = 0;
    ?>
    <table width="931" border="0" class="garisy ceKotak">
      <tr>
        <td width="31" rowspan="2">
          <div align="center">No</div>
        </td>
        <td colspan="3">
          <div align="center">DEBITUR</div>
        </td>
        <!-- <td width="83" rowspan="2">
          <div align="center">KODE SALES MAN </div>
        </td> -->
        <td colspan="4">
          <div align="center">SURAT JALAN </div>
        </td>
        <!-- <td width="117" rowspan="2"><div align="center">KET</div></td> -->
        <td colspan="3">
          <div align="center">Kemasan </div>
        </td>
      </tr>
      <tr>
        <td width="63">
          <div align="center">KODE</div>
        </td>
        <td width="158">
          <div align="center">NAMA</div>
        </td>
        <td width="121">
          <div align="center">ALAMAT</div>
        </td>
        <td width="101">
          <div align="center">NOMOR</div>
        </td>
        <td width="89">
          <div align="center">TANGGAL SJ</div>
        </td>
        <td width="89">
          <div align="center">TANGGAL SPB</div>
        </td>
        <td width="110">
          <div align="center">NILAI</div>
        </td>
        <td width="89">
          <div align="center">KOLI</div>
        </td>
        <td width="89">
          <div align="center">IKAT</div>
        </td>
        <td width="110">
          <div align="center">KARDUS</div>
        </td>
      </tr>
      <!-- </table> -->
      <?php
      if ($detail) {


        foreach ($detail as $rowi) {
          $i++;
          if ($rowi->n_spb_toplength == 0) {
            $kes++;
            $vkes = $vkes + $rowi->v_jumlah;
          } else {
            $kre++;
            $vkre = $vkre + $rowi->v_jumlah;
          }
          if (strlen($i) == 2) $aw = 0;
          if (strlen($i) == 1) $aw = 1;
          $aw = str_repeat(" ", $aw);
          $cust  = $rowi->i_customer;
          $name  = $rowi->e_customer_name;
          if (strlen($name) > 27) {
            $name = substr($name, 0, 27);
          }
          $name  = $name . str_repeat(" ", 27 - strlen($name));
          // $city  = $rowi->e_customer_city;
          // if (strlen($city) > 27) {
          //   $city = substr($city, 0, 27);
          // }
          // $city  = $city . str_repeat(" ", 27 - strlen($city));
          $sls  = $rowi->i_salesman;
          $isj  = substr($rowi->i_sj, 8, 6);
          $tmp = explode("-", $rowi->d_sj);
          $th = $tmp[0];
          $bl = $tmp[1];
          $hr = $tmp[2];
          $dsj  = $hr . "-" . $bl . "-" . $th;
          $tmp = explode("-", $rowi->d_spb);
          $th = $tmp[0];
          $bl = $tmp[1];
          $hr = $tmp[2];
          $dspb  = $hr . "-" . $bl . "-" . $th;
          $vsj  = number_format($rowi->v_jumlah);
          $vsj  = str_repeat(" ", 14 - strlen($vsj)) . $vsj;
          $ket  = $rowi->e_remark;
          if (strlen($ket) > 0) {
            $ket  = $ket . str_repeat(" ", 24 - strlen($ket));
          } else {
            $ket  = $ket . str_repeat(" ", 24 - 1);
          }
      ?>
          <!-- <table width="1201"  border="0" class="garisy ceKotak huruf ici"> -->
          <tr>
            <td width="35"><?php echo $i; ?></td>
            <td width="68"><?php echo $cust; ?></td>
            <td width="170"><?php echo $name; ?></td>
            <td width="132"><?php echo $rowi->e_customer_address; ?></td>
            <!-- <td width="89" align="center"><#?php echo $sls; ?></td> -->
            <td width="110"><?php echo $isj; ?></td>
            <td width="96"><?php echo $dsj; ?></td>
            <td width="96"><?php echo $dspb; ?></td>
            <td width="120" align="right"><?php echo $vsj; ?></td>
            <td style="text-align:center" width="95"><?= $rowi->n_koli == 0 ? "" : $rowi->n_koli ?></td>
            <td style="text-align:center" width="95"><?= $rowi->n_ikat == 0 ? "" : $rowi->n_ikat ?></td>
            <td style="text-align:center" width="119"><?= $rowi->n_kardus == 0 ? "" : $rowi->n_kardus ?></td>
            <!-- <td width="95"><? #php echo $ket; 
                            ?></td>
            <td width="95"><? #php echo $ket; 
                            ?></td>
            <td width="119"><? #php echo $ket; 
                            ?></td> -->

            <!-- <td width="18"><? #php echo $ket; 
                                ?></td> -->


          </tr>

      <?php
        }
      }
      $vsj  = number_format($row->v_dkb);
      $vsj  = str_repeat(" ", 14 - strlen($vsj)) . $vsj;
      $tmp = explode("-", $row->d_dkb);
      $th = $tmp[0];
      $bl = $tmp[1];
      $hr = $tmp[2];
      $ddkb = $hr . " " . mbulan($bl) . " " . $th;
      $vkes = number_format($vkes);
      $vkre = number_format($vkre);
      $tgl = date("d") . " " . mbulan(date("m")) . " " . date("Y") . "  Jam : " . date("H:i:s");
      ?>
      <!-- </table> -->
      <!-- <table width="861" border="0" class="garisy ceKotak"> -->
      <tr>
        <td colspan="7" width="570" align="right">Jumlah Rp. </td>
        <td align="right" width="172"><?php echo $vsj; ?></td>
        <td colspan="3" width="172"></td>
      </tr>
    </table>
    <br>
    <table width="986" border="0">
      <tr>
        <td>
          <div align="right">Bandung, <?php echo $ddkb; ?></div>
        </td>
      </tr>
    </table>
    <table width="987" border="0" class=" garisy td ceKotak">
      <tr>
        <td colspan="2">
          <div align="center">Berangkat</div>
        </td>
        <td colspan="2">
          <div align="center">Kembali</div>
        </td>
        <td colspan="2">
          <div align="center">Diterima Oleh, </div>
        </td>
      </tr>
      <tr height="80">
        <td width="157" align="center"><br><br>(Staff Gudang)</td>
        <td width="162" align="center"><br><br>(Driver/Pihak Perantara Driver)</td>
        <td width="164" align="center"><br><br>(Staff Gudang)</td>
        <td width="159" align="center"><br><br>(Driver/Pihak Perantara Driver)</td>
        <td width="149" align="center"><br><br>(Kasir)</td>
        <td width="156" align="center"><br><br>(Adm Piutang)</td>
      </tr>
    </table>
    <table width="824" border="0">
      <tr>
        <!-- <td width="651"><div align="right">Catatan : Cash <? #php echo $kes;
                                                                ?> Nota</div></td> -->
        <td width="651">
          <div align="right"></div>
        </td>
        <td width="25">&nbsp;</td>
        <td width="151">Kredit <?php echo $kre; ?> Nota</td>
      </tr>
      <tr>
        <!-- <td width="651"><div align="right">Rp. <? #php echo $vkes; 
                                                    ?></div></td> -->
        <td width="651">
          <div align="right"></div>
        </td>
        <td width="25">&nbsp;</td>
        <td width="151"><?php echo $vkre; ?></td>
      </tr>
    </table>
    <table width="620" border="0">
      <!--   <tr>
    <td width="184">Putih</td>
    <td width="20">:</td>
    <td width="402">Pusat</td>
  </tr>
  <tr>
    <td>Merah</td>
    <td>:</td>
    <td>Adm.Piutang</td>
  </tr>
  <tr>
    <td>Kuning</td>
    <td>:</td>
    <td>Gudang</td>
  </tr> -->
      <tr>
        <td>TANGGAL CETAK </td>
        <td>:</td>
        <td><?php echo $tgl; ?></td>
      </tr>
    </table>
  <?php
  }
  ?>
  <div class="noDisplay">
    <center><b><a href="#" onClick="window.print()">Print</a></b></center>
  </div>
</BODY>

</html>