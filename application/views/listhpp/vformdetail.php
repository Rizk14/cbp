<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<?php 
  $query=$this->db->query(" select e_customer_name from tr_customer where i_customer='$icustomer'");
  if ($query->num_rows() > 0){
    foreach($query->result() as $tmp){
      $nama=$tmp->e_customer_name;
    }
  }
  echo "<center><h2>$page_title ($icustomer-$nama) Periode $iperiode</h2></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listhpp/cform/detail','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
      <th>Tanggal</th>
	    <th>No Bukti</th>
	    <th>Keterangan</th>
	    <th>Penjualan</th>
	    <th>DN</th>
	    <th>Pelunasan</th>
	    <th>KN</th>
	    <th>Saldo Akhir</th>
	    <tbody>
	      <?php 
		if($isi){
      $saldoakhir=$saldo;
      echo "<tr> 
				    <td></td>
				    <td></td>
				    <td>Saldo</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>".number_format($saldoakhir)."</td>
	  			  </tr>";
			foreach($isi as $row){
        $tmp 	= explode("-", $row->tglbukti);
        $det	= $tmp[2];
        $mon	= $tmp[1];
        $yir 	= $tmp[0];
        $row->tglbukti	= $det."-".$mon."-".$yir;
        if($row->keterangan!='2. LEBIH BAYAR' && $row->keterangan!='2. NOTA KREDIT'){
          $saldoakhir=$saldoakhir+$row->debet+$row->dn-($row->kredit+$row->kn);
        }
			  echo "<tr> 
				  <td>$row->tglbukti</td>
				  <td>$row->bukti</td>
				  <td>$row->keterangan</td>
				  <td align=right>".number_format($row->debet)."</td>
				  <td align=right>".number_format($row->dn)."</td>
				  <td align=right>".number_format($row->kredit)."</td>
				  <td align=right>".number_format($row->kn)."</td>
				  <td align=right>".number_format($saldoakhir)."</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
