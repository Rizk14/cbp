<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
  if($isi){
	  foreach($isi as $row){
		  $nor	= str_repeat(" ",5);
		  $abn	= str_repeat(" ",12);
		  $ab	= str_repeat(" ",9);
		  $ipp 	= new PrintIPP();
		  $ipp->setHost($host);
		  $ipp->setPrinterURI($uri);
		  $ipp->setRawText();
		  $ipp->unsetFormFeed();
		  $cetak.=CHR(18)."\n";
		  $cetak.=$nor.NmPerusahaan."                  \n";		
		  $cetak.=$nor."                  BUKTI BARANG KELUAR           No.  ".$row->i_bbk."\n";
		  $tmp=explode("-",$row->d_bbk);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dbbk=$hr." ".mbulan($bl)." ".$th;
		  $cetak.=$nor."                      ( B B K )                 Tgl. ".$dbbk."\n";		
		  $cetak.=$nor."Perwakilan : GD-PST                             Kepada Yth.\n";
		  $aw		= 34;
		  $pjg	= strlen($row->e_remark."/".$row->i_area);
		  for($xx=1;$xx<=$pjg;$xx++){
			  $aw=$aw-1;
		  }
      $cetak.=$nor."Referensi  : ".$row->e_remark."/".$row->i_area.str_repeat(" ",20).$row->e_customer_name."\n";
      $cetak.=$nor.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),45).CHR(194).str_repeat(CHR(196),9).CHR(194).str_repeat(CHR(196),12).CHR(191)."\n";
      $cetak.=$nor.CHR(179)."NO. ".CHR(179)." KODE                                        ".CHR(179)." JUMLAH  ".CHR(179)." KETERANGAN ".CHR(179)."\n";
      $cetak.=$nor.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G        ".CHR(179)."         ".CHR(179)."            ".CHR(179)."\n";
      $cetak.=$nor.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),45).CHR(216).str_repeat(CHR(205),9).CHR(216).str_repeat(CHR(205),12).CHR(181)."\n";
		  $i=0;	
		  foreach($detail as $rowi){
			  $i++;
			  $hrg=number_format($rowi->v_unit_price);
			  $prod	= $rowi->i_product;
        if(strlen($rowi->e_product_name)>37)$rowi->e_product_name=substr($rowi->e_product_name,0,37);
			  $name	= $rowi->e_product_name.str_repeat(" ",37-strlen($rowi->e_product_name));
			  $motif	= $rowi->e_remark;
			  $qty	= number_format($rowi->n_quantity);
			  $aw		= 3;
			  $pjg	= strlen($i);
			  for($xx=1;$xx<=$pjg;$xx++){
				  $aw=$aw-1;
			  }
			  $pjg	= strlen($qty);
			  $spcqty	= 8;			
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcqty	= $spcqty-1;
			  }
        $pjg=strlen($rowi->e_remark);
        if($pjg>12){
          $pjg=12;
          $rowi->e_remark=substr($rowi->e_remark,0,12);
        }
			  $spcrmk	= 12;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcrmk	= $spcrmk-1;
			  }
			  $cetak.=$nor.CHR(179).str_repeat(" ",$aw).$i." ".CHR(179).$prod." ".$name.CHR(179).str_repeat(" ",$spcqty).$qty." ".CHR(179).$rowi->e_remark.str_repeat(" ",$spcrmk).CHR(179)."\n";
			  if(($i%44)==0){
				  $cetak.=$nor.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),45).CHR(193).str_repeat(CHR(196),9).CHR(193).str_repeat(CHR(196),12).CHR(217)."\n";
          $cetak.=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
        }
		  }
      $cetak.=$nor.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),45).CHR(193).str_repeat(CHR(196),9).CHR(193).str_repeat(CHR(196),12).CHR(217)."\n";
      $cetak.=$nor." Penerima        Pembawa          Yang menyetujui           Kepala Gudang\n\n\n\n";
      $cetak.=$nor."(          )    (           )     (             )        (              )\n".CHR(15);
      $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
      $cetak.=$nor."TANGGAL CETAK : ".$tgl;
      $ipp->setFormFeed();
		  $cetak.=CHR(18)."\n";
      $ipp->setdata($cetak);
      $ipp->printJob();
#      echo $cetak;
	  }
  }
  function CetakHeader($row,$hal,$nor,$abn,$ab,$ipp){
		$ipp->unsetFormFeed();
	  $cetek.=CHR(18)."\n";
	  $cetek.=$nor.NmPerusahaan."                  \n";		
	  $cetek.=$nor."                  BUKTI BARANG KELUAR           No.  ".$row->i_bbk."\n";
	  $tmp=explode("-",$row->d_bbk);
	  $th=$tmp[0];
	  $bl=$tmp[1];
	  $hr=$tmp[2];
	  $dbbk=$hr." ".mbulan($bl)." ".$th;
	  $cetek.=$nor."                      ( B B K )                 Tgl. ".$dbbk."\n";		
	  $cetek.=$nor."Perwakilan : ".$row->i_area."-PST                             Kepada Yth.\n";
	  $aw		= 34;
	  $pjg	= strlen($row->e_remark."/".$row->i_area);
	  for($xx=1;$xx<=$pjg;$xx++){
		  $aw=$aw-1;
	  }
    $cetek.=$nor."Referensi  : ".$row->e_remark."/".$row->i_area.str_repeat(" ",4).$row->e_customer_name."\n";
    $cetek.=$nor.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),45).CHR(194).str_repeat(CHR(196),9).CHR(194).str_repeat(CHR(196),12).CHR(191)."\n";
    $cetek.=$nor.CHR(179)."NO. ".CHR(179)." KODE                                        ".CHR(179)." JUMLAH  ".CHR(179)." KETERANGAN ".CHR(179)."\n";
    $cetek.=$nor.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G        ".CHR(179)."         ".CHR(179)."            ".CHR(179)."\n";
    $cetek.=$nor.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),45).CHR(216).str_repeat(CHR(205),9).CHR(216).str_repeat(CHR(205),12).CHR(181)."\n";
    return $cetek;
	}
	echo "<script>this.close();</script>";
?>
