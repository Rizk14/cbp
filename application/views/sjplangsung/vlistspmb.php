<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'sjplangsung/cform/carispmb','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="iarea" name="iarea" value="<?php echo $area; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Tgl SPmB</th>
		    <th>No SPmB</th>
		    <th>Reff</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
				$tmp=explode('-',$row->d_spmb);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_spmb=$tgl.'-'.$bln.'-'.$thn;
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_spmb','$row->d_spmb')\">$row->d_spmb</a></td>
				  <td><a href=\"javascript:setValue('$row->i_spmb','$row->d_spmb')\">$row->i_spmb</a></td>
				  <td><a href=\"javascript:setValue('$row->i_spmb','$row->d_spmb')\">$row->i_spmb_old</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    document.getElementById("ispmb").value=a;
    document.getElementById("dspmb").value=b;
    c=document.getElementById("iarea").value;
    d=document.getElementById("eareaname").value;
    e=document.getElementById("dsj").value;
    f=document.getElementById("istore").value;
    g=document.getElementById("isjold").value;
    jsDlgHide("#konten *", "#fade", "#light");
    show("sjplangsung/cform/hitung/"+a+"/"+b+"/"+c+"/"+d+"/"+e+"/"+f+"/"+g+"/","#main");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
