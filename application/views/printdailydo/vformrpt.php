<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: portrait;
}
@page { size: Letter; 
        margin: 5mm;  /* this affects the margin in the printer settings */
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}

</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<table width="985" border="0">
  <tr>
    <td colspan="3" class="nmper"><?php echo NmPerusahaan; ?></td>
  </tr>
  <tr>
    <td colspan="3" class="judul" >DAILY DO - <?php echo $daily ?> </td>
  </tr>
  
</table>
<table width="620" border="0" class="garisy ceKotak">
  <tr>
    <td width="10"><div align="center">No</div></td>
    <td width="50"><div align="center">NO DO</div></td>
    <td width="50"><div align="center">TANGGAL DO</div></td>
    <td width="150"><div align="center">SUPPLIER</div></td>
    <td width="150"><div align="center">NO OP</div></td>
    <td width="150"><div align="center">NO REFF</div></td>
  </tr>
<?php 
$i=0;
		foreach($isi as $row){
			$i++;
			
			$ido	= $row->i_do;
			$name	= $row->e_supplier_name;
			// if(strlen($name)>40){
			// 	$name=substr($name,0,40);
			// }
			// $name	= $name.str_repeat(" ",27-strlen($name));
      $ireff = $row->i_reff;
      $iop	= $row->i_op;
      $tmp=explode("-",$row->d_do);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $ddo  = $hr."-".$bl."-".$th;
      $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
 
			#$ket	= $ket.str_repeat(" ",24-strlen($ket));
?>  
<tr class="garisy ceKotak huruf ici" align="center">
    <td><?php echo $i; ?></td>
    <td><?php echo $ido; ?></td>
    <td><?php echo $ddo; ?></td>
    <td><?php echo $name; ?></td>
    <td><?php echo $iop; ?></td>
    <td><?php echo $ireff; ?></td>
  </tr>

<?php 
    }
    ?>
    
</table>
<br>
<!--<table width="300" border="0" align="right">
  <tr>
    <td><div align="center">Cilacap, <?php //echo $ddo; ?></div></td>
  </tr>
   <tr>
    <td><div align="center">Diterima Oleh, </div></td>
  </tr>
    <td width="156" align="center"><br><br>(...........)</td>
  </tr>
</table>
<br>
<br>
<br>
<br>
-->
<br>
<table width="620" border="0">
  <tr>
    <td>TANGGAL CETAK </td>
    <td>:</td>
    <td><?php echo $tgl; ?></td>
  </tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
