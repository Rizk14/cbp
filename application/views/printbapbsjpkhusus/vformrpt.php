<?php 
 	include ("php/fungsi.php");
?>
<html>
<head>
<title>Print BAPB</title>
</head>
<body>
<style type="text/css" media="all">
{
size: landscape;
}
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
}
.ceKotak{
	background-color:#f0f0f0;
	border-bottom:#000000 1px solid;
	border-top:#000000 1px solid;
	border-left:#000000 1px solid;
	border-right:#000000 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garistd {
	border-bottom:0.01px solid;
	border-left:0.01px solid;
	font-size: 10px;
}
.garistd td { 
	border-bottom:0.01px;
	border-left:0.01px;
	border-right:0.01px solid;
	border-top:0.01px solid;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.ici {
  font-size: 12px;
  font-weight:normal;
}
.garisbawah { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
		$tmp=explode("-",$row->d_bapb);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_bapb=$hr." ".substr(mbulan($bl),0,3)." ".$th;
?>
<table width="695">
	<tr>
		<td width="457" class="huruf isi"><?php echo NmPerusahaan; ?></td>
		<td width="226">&nbsp;</td>
	</tr>
	<tr>
		<td class="huruf isi">BERITA ACARA PENERIMAAN BARANG</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="huruf isi">No. <?php echo $row->i_bapb; ?>
		<td class="huruf ici">Tanggal : <?php echo $row->d_bapb; ?>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="huruf ici">Kepada YTH</td>
	</tr>
	<?php 
    $cust=false;
    if( (trim($row->i_customer)=='') || ($row->i_customer==null) ){
	  $qarea = $this->db->query(" select * from tr_area where i_area='$row->i_area' ");
	  if ($qarea->num_rows() > 0)
	  {
		  $rowarea	= $qarea->row();
		  $areaname	= $rowarea->e_area_name;
	  }?>
	  <tr>
		  <td class="huruf ici"><?php echo "KP/PS - ".$row->i_area." - ".$areaname; ?></td>
	  </tr><?php 
      }else{
      $cust=true;?>
	  <tr>
		  <td class="huruf ici"><?php echo $row->i_customer."-".$row->e_customer_name; ?></td>
	  </tr>
	  <tr>
		  <td class="huruf ici"><?php echo $row->e_customer_address."-".$row->e_customer_city; ?></td>
	  </tr>
	<?php }	?>

</table>
<p class="huruf ici">
  Kami telah menerima kiriman barang sebagai berikut :<br>
  1. Melalui :<br>
  <?php 
    $dataeks = $this->db->query(" select a.*, b.e_ekspedisi from tm_bapbsjp_ekspedisi a, tr_ekspedisi b
                                  where a.i_bapb='$row->i_bapb' and a.i_area='$row->i_area' and a.i_ekspedisi=b.i_ekspedisi
                                  order by a.ctid desc");
		if ($dataeks->num_rows() > 0)
    {
			$eks=$dataeks->row();
      $namaekspedisi=$eks->e_ekspedisi;
    }
?>
  &nbsp;&nbsp;&nbsp;&nbsp;Jasa Angkutan : <?php echo $namaekspedisi; ?><br>
  <?php 
	if($row->n_berat<=0){
      $berat="-";
    }else{
      $berat=number_format($row->n_berat,2);
    }
?>
  2. Dengan berat keseluruhan : <?php echo $berat; ?> Kg dan terdiri dari : <?php echo $row->n_bal; ?> BALL,dengan rincian Nota/Surat Jalan sbb:<br>
  <?php 
    $datasj = $this->db->query(" select * from tm_bapbsjp_item where i_bapb='$row->i_bapb' and i_area='$row->i_area'",false);
		if ($datasj->num_rows() > 0)
    {
      $sj1="";
      $sj2="";
      $sj3="";
      $sj4="";
      $sj5="";
      $jsj=0;
	  foreach($datasj->result() as $item)
      {
        $jsj++;
        if($sj1==''){
          $sj1=$sj1.$item->i_sj;
        }elseif($jsj<5){
          $sj1=$sj1.','.$item->i_sj;
        }elseif($sj2==''){
          $sj2=$sj2.$item->i_sj;
        }elseif($jsj<9){
          $sj2=$sj2.','.$item->i_sj;
        }elseif($sj3==''){
          $sj3=$sj3.$item->i_sj;
        }elseif($jsj<13){
          $sj3=$sj3.','.$item->i_sj;
        }elseif($sj4==''){
          $sj4=$sj4.$item->i_sj;
        }elseif($jsj<17){
          $sj4=$sj4.','.$item->i_sj;
        }elseif($sj5==''){
          $sj5=$sj5.$item->i_sj;
        }elseif($jsj<21){
          $sj5=$sj5.','.$item->i_sj;
        }
      }
    }
	echo "&nbsp;&nbsp;&nbsp;&nbsp;".$sj1."\n";
	 if($sj2!=''){
		echo "&nbsp;&nbsp;&nbsp;&nbsp;".$sj2."\n";
    }
	if($sj3!=''){
		echo "&nbsp;&nbsp;&nbsp;&nbsp;".$sj3."\n";
    }
	if($sj4!=''){
		echo "&nbsp;&nbsp;&nbsp;&nbsp;".$sj4."\n";
    }
	if($sj5!=''){
		echo "&nbsp;&nbsp;&nbsp;&nbsp;".$sj5."\n";
    }
?>
  <br>
  3. Keadaan Pembungkusnya (* <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(a) Masih sangat baik dan terjahit rapi <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(b) Terjahit, tetapi tidak rapi <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(c) Sedikit terbuka (Ada jahitan yang terlepas) <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(d) Terbuka jahitannya & pembungkusnya robek <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(e) Lain - lainnya (beri penjelasan) <br>
  .............................................................................................................................................................................<br>
  .............................................................................................................................................................................<br>
  .............................................................................................................................................................................<br>
  <br>
  &nbsp;&nbsp;&nbsp;&nbsp;(* Beri tanda silang pada huruf sesuai kondisi sebenarnya<br>
  <br>
4. Barang yang tercantum dibawah ini terdapat ketidaksesuaian<br>
 &nbsp;&nbsp;&nbsp;&nbsp;antara Nota/Surat Jalan dengan jumlah yang diterima</p>
<table class="garistd" cellspacing="0" border="1">
	<tr>
		<td width="22" align="center">No</td>
		<td width="397" align="center">NAMA BARANG</td>
		<td width="209" align="center">JUMLAH di Nota/SJ</td>
		<td width="220" align="center">JUMLAH YG DITERIMA</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
	<tr>
		<td width="22">&nbsp;</td>
		<td width="397">&nbsp;</td>
		<td width="209">&nbsp;</td>
		<td width="220">&nbsp;</td>
	</tr>
</table><br>
<table class="huruf ici">
  <tr>
    <td>KLAIM DILAYANI APABILA BAPB INI DIISI PIHAK PENERIMA DAN DIKIRIM KE <?php echo NmPerusahaan; ?></td>
  </tr>
</table>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
.......................................<br>
<table>
 <table width="736">
 	<tr>
	  <td width="231" rowspan=6><?php echo "<img height=150px; style=\"cursor:hand;\" src=\"". base_url()."img/bapb.png\" border=\"0\" alt=\"edit\">"; ?></td>
		<td width="159">&nbsp;</td>
		<td width="176" align="left" class="huruf ici">Tanda tangan</td>
		<td width="150">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" class="huruf ici">Admin Gudang,</td>
		<td align="center" class="huruf ici">pembawa,</td>
		<td align="center" class="huruf ici">penerima</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>

	</tr>
	<tr>
		<td align="center">(.......................)</td>
		<td align="center">(.......................)</td>
		<td align="center">(.......................)</td>
	</tr>
</table>
<?php } ?>
<br>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
