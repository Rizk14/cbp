<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'productbase/cform/update', 'update' => '#main', 'type' => 'post')); ?>
			<div id="masterproductbaseform">
				<table class="mastertable">
					<tr>
						<td width="19%">Kode Barang</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" readonly name="iproduct" id="iproduct" value="<?php echo $kode; ?>" maxlength='7'>
						</td>
						<td width="19%">Nama</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="eproductname" id="eproductname" value="<?php echo $isi->e_product_name; ?>" maxlength='100' onkeyup="gede(this)">
							<input readonly type="hidden" name="eproductnamex" id="eproductnamex" value="<?php echo $isi->e_product_name; ?>" maxlength='100' onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="19%">Kode Produk (Pemasok)</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="iproductsupplier" id="iproductsupplier" value="<?php echo $isi->i_product_supplier; ?>" maxlength='7' onkeyup="gede(this)">
							<input readonly type="hidden" name="iproductsupplierx" id="iproductsupplierx" value="<?php echo $isi->i_product_supplier; ?>" maxlength='7' onkeyup="gede(this)">
						</td>
						<td width="19%">Nama Produk (Pemasok)</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="eproductsuppliername" id="eproductsuppliername" value="<?php echo $isi->e_product_suppliername; ?>" maxlength='100' onkeyup="gede(this)">
							<input readonly type="hidden" name="eproductsuppliernamex" id="eproductsuppliernamex" value="<?php echo $isi->e_product_suppliername; ?>" maxlength='100' onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="19%">Supplier</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="isupplier" id="isupplier" value="<?php echo $isi->i_supplier; ?>" onclick='showModal("productbase/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="esuppliername" id="esuppliername" value="<?php echo $isi->e_supplier_name; ?>" onclick='showModal("productbase/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="esuppliernamex" id="esuppliernamex" value="<?php echo $isi->e_supplier_name; ?>" onclick='showModal("productbase/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
						<td width="19%">Status Produk</td>
						<td width="1%">:</td>
						<td width="30%"><input type="hidden" name="iproductstatus" id="iproductstatus" value="<?php echo $isi->i_product_status; ?>" onclick='showModal("productbase/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="eproductstatusname" id="eproductstatusname" value="<?php echo $isi->e_product_statusname; ?>" onclick='showModal("productbase/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="eproductstatusnamex" id="eproductstatusnamex" value="<?php echo $isi->e_product_statusname; ?>" onclick='showModal("productbase/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
					</tr>
					<tr>
						<td width="19%">Group Produk</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="iproductgroup" id="iproductgroup" value="<?php echo $isi->i_product_group; ?>" onclick='showModal("productbase/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="eproductgroupname" id="eproductgroupname" value="<?php echo $isi->e_product_groupname; ?>" onclick='showModal("productbase/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="eproductgroupnamex" id="eproductgroupnamex" value="<?php echo $isi->e_product_groupname; ?>" onclick='showModal("productbase/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
						<td width="19%">Jenis</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="iproducttype" id="iproducttype" value="<?php echo $isi->i_product_type; ?>" onclick="view_producttype(document.getElementById('iproductgroup').value);">
							<input readonly name="eproducttypename" id="eproducttypename" value="<?php echo $isi->e_product_typename; ?>" onclick="view_producttype(document.getElementById('iproductgroup').value);">
							<input readonly hidden name="eproducttypenamex" id="eproducttypenamex" value="<?php echo $isi->e_product_typename; ?>" onclick="view_producttype(document.getElementById('iproductgroup').value);">
						</td>
					</tr>
					<tr>
						<!--		  <td width="19%">Kelas</td> -->
						<td width="19%">Kategori</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="iproductclass" id="iproductclass" value="<?php echo $isi->i_product_class; ?>" onclick='showModal("productbase/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="eproductclassname" id="eproductclassname" value="<?php echo $isi->e_product_classname; ?>" onclick='showModal("productbase/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="eproductclassnamex" id="eproductclassnamex" value="<?php echo $isi->e_product_classname; ?>" onclick='showModal("productbase/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
						<!--		  <td width="19%">Kategori</td> -->
						<td width="19%">Sub Kategori</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="iproductcategory" id="iproductcategory" value="<?php echo $isi->i_product_category; ?>" onclick="view_productcategory(document.getElementById('iproductclass').value);">
							<input readonly name="eproductcategoryname" id="eproductcategoryname" value="<?php echo $isi->e_product_categoryname; ?>" onclick="view_productcategory(document.getElementById('iproductclass').value);">
							<input readonly hidden name="eproductcategorynamex" id="eproductcategorynamex" value="<?php echo $isi->e_product_categoryname; ?>" onclick="view_productcategory(document.getElementById('iproductclass').value);">
						</td>
					</tr>
					<tr>
						<td width="19%">Harga Eceran</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="vproductretail" id="vproductretail" value="<?php echo number_format($isi->v_product_retail, 2); ?>" maxlength='10' onkeyup="reformat(this)">
							<input readonly hidden type="hidden" name="vproductretailx" id="vproductretailx" value="<?php echo number_format($isi->v_product_retail, 2); ?>" maxlength='10' onkeyup="reformat(this)">
						</td>
						<td width="19%">Harga Pabrik</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="vproductmill" id="vproductmill" value="<?php echo number_format($isi->v_product_mill, 2); ?>" maxlength='10' onkeyup="reformat(this)">
							<input readonly type="hidden" name="vproductmillx" id="vproductmillx" value="<?php echo number_format($isi->v_product_mill, 2); ?>" maxlength='10' onkeyup="reformat(this)">
						</td>
					</tr>
					<tr>
						<td width="19%">Tanggal Stop Produksi</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="dproductstopproduction" id="dproductstopproduction" value="<?php echo $isi->d_product_stopproduction; ?>" readonly onclick="showCalendar('',this,this,'','dproductstopproduction',0,18,1)">
							<input readonly type="hidden" name="dproductstopproductionx" id="dproductstopproductionx" value="<?php echo $isi->d_product_stopproduction; ?>" readonly onclick="showCalendar('',this,this,'','dproductstopproduction',0,18,1)">
						</td>
						<td width="19%">Tanggal Pendaftaran</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" name="dproductregister" id="dproductregister" value="<?php echo $isi->d_product_register; ?>" readonly>
							<input type="hidden" name="dproductregisterx" id="dproductregisterx" value="<?php echo $isi->d_product_register; ?>" readonly>
						</td>
					</tr>
					<tr>
						<td width="19%">Price List</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="checkbox" name="fproductpricelist" id="fproductpricelist" value="on" <?php if ($isi->f_product_pricelist == 't') echo "checked"; ?>>
							<input hidden hidden type="checkbox" name="fproductpricelistx" id="fproductpricelistx" value="on" <?php if ($isi->f_product_pricelist == 't') echo "checked"; ?>>
						</td>
						<td width="19%">Seri</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="iproductseri" id="iproductseri" value="<?php echo $isi->i_product_seri; ?>" onclick='showModal("productbase/cform/productseri/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="eproductseriname" id="eproductseriname" value="<?php echo $isi->e_product_seriname; ?>" onclick='showModal("productbase/cform/productseri/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="eproductserinamex" id="eproductserinamex" value="<?php echo $isi->e_product_seriname; ?>" onclick='showModal("productbase/cform/productseri/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
					</tr>
					<tr>
						<td width="19%">Kategori Penjualan</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="hidden" name="isalescategory" id="isalescategory" value="<?php echo $isi->i_sales_category; ?>" onclick='showModal("productbase/cform/salescategory/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly name="esalescategoryname" id="esalescategoryname" value="<?php echo $isi->e_sales_categoryname; ?>" onclick='showModal("productbase/cform/salescategory/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input readonly hidden name="esalescategorynamex" id="esalescategorynamex" value="<?php echo $isi->e_sales_categoryname; ?>" onclick='showModal("productbase/cform/salescategory/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						</td>
					</tr>
					<tr>
						<td width="19%">Berat (Kg)</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" onclick="if($(this).val()=='0'){ $(this).val('') }" onfocus="if($(this).val()=='0'){ $(this).val('') }" onmouseleave="if($(this).val()==''){ $(this).val(0) }" onkeyup="hanyaAngka(this);" name="n_berat" id="n_berat" value="<?php echo $isi->n_berat; ?>">
						</td>
						<td width="19%">Volume (M<sup>3</sup>)</td>
						<td width="1%">:</td>
						<td width="30%">
							<input type="text" onclick="if($(this).val()=='0'){ $(this).val('') }" onfocus="if($(this).val()=='0'){ $(this).val('') }" onmouseleave="if($(this).val()==''){ $(this).val(0) }" onkeyup="hanyaAngka(this);" name="n_volume" id="n_volume" value="<?php echo $isi->n_volume; ?>">
						</td>
					</tr>
					<tr>
						<td width="19%">&nbsp;</td>
						<td width="1%">&nbsp;</td>
						<td colspan=4 width="80%">
							<?php
							$departement = $this->session->userdata('departement');
							$level = $this->session->userdata('level');
							if ($departement == '0' or $departement == '6' or $departement == '3' or ($departement == '7' and $level == '5')) { ?>
								<input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
							<?php } ?>
							<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('productbase/cform/','#main');">
						</td>
					</tr>
				</table>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function view_productgroup() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productgroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_supplier() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_productstatus() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productstatus","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_producttype(a) {
		lebar = 450;
		tinggi = 400;
		//    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/producttype/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
		showModal("productbase/cform/producttype/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");

	}

	function view_productcategory(a) {
		//    lebar =450;
		//    tinggi=400;
		//    eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productcategory/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
		showModal("productbase/cform/productcategory/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_productclass() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productclass","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function cek() {
		kode = document.getElementById("iproduct").value;
		nama = document.getElementById("eproductname").value;
		if (kode == '' || nama == '') {
			alert("Minimal kode Produk dan nama Produk diisi terlebih dahulu !!!");
		}
	}
</script>