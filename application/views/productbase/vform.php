<SCRIPT LANGUAGE="javascript">
	function ckbrg(nomor) {
		$.ajax({

			type: "POST",
			url: "<?php echo site_url('productbase/cform/cari_barang'); ?>",
			data: "nbrg=" + nomor,
			success: function(data) {
				$("#confnomor").html(data);
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};
</SCRIPT>

<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Barang</a></li>
			<li><a href="#" class="tab" onclick="sitab('content_2')">Barang</a></li>
		</ul>
		<div id="content_1" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'productbase/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="6" align="center">
										Cari data :
										<input type="text" id="cari" name="cari" value="">&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
									</td>
								</tr>
							</thead>
							<th>Kode Produk</th>
							<th>Nama</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										echo "<tr> 
												<td>$row->i_product</td>
												<td>$row->e_product_name</td>";
										echo "<td class=\"action\"><a href=\"#\" onclick='show(\"productbase/cform/edit/$row->i_product\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
										#&nbsp;&nbsp;";
										#			  echo "<a href=\"#\" onclick='hapus(\"productbase/cform/delete/$row->i_product\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
										echo "</td></tr>";
									}
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
		<div id="content_2" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'productbase/cform/simpan', 'update' => '#main', 'type' => 'post')); ?>
						<div id="masterproductbaseform">
							<table class="mastertable">
								<tr>
									<td width="19%">Kode Produk</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="iproduct" id="iproduct" value="" maxlength='7' onkeyup="gede(this); ckbrg(document.getElementById('iproduct').value);">
										<div id="confnomor"></div>
									</td>
									<td width="19%">Nama</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="eproductname" id="eproductname" value="" maxlength='100' onkeyup="gede(this)">
									</td>
								</tr>
								<tr>
									<td width="19%">Kode Produk (Pemasok)</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="iproductsupplier" id="iproductsupplier" value="" maxlength='7' onkeyup="gede(this)">
									</td>
									<td width="19%">Nama Produk (Pemasok)</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="eproductsuppliername" id="eproductsuppliername" value="" maxlength='100' onkeyup="gede(this)">
									</td>
								</tr>
								<tr>
									<td width="19%">Supplier</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="isupplier" id="isupplier" value="" onclick='showModal("productbase/cform/supplier/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="esuppliername" id="esuppliername" value="" onclick='showModal("productbase/cform/supplier/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
									<td width="19%">Status Produk</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="iproductstatus" id="iproductstatus" value="" onclick='showModal("productbase/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="eproductstatusname" id="eproductstatusname" value="" onclick='showModal("productbase/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
								</tr>
								<tr>
									<td width="19%">Group Produk</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="iproductgroup" id="iproductgroup" value="" onclick='showModal("productbase/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="eproductgroupname" id="eproductgroupname" value="" onclick='showModal("productbase/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
									<td width="19%">Jenis</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="iproducttype" id="iproducttype" value="" onclick="view_producttype(document.getElementById('iproductgroup').value);">
										<input readonly name="eproducttypename" id="eproducttypename" value="" onclick="view_producttype(document.getElementById('iproductgroup').value);">
									</td>
								</tr>
								<tr>
									<!--		  <td width="19%">Kelas</td>-->
									<td width="19%">Kategori</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="iproductclass" id="iproductclass" value="" onclick='showModal("productbase/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="eproductclassname" id="eproductclassname" value="" onclick='showModal("productbase/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
									<!--		  <td width="19%">Kategori</td>-->
									<td width="19%">Sub Kategori</td>
									<td width="1%">:</td>
									<td width="30%"><input type="hidden" name="iproductcategory" id="iproductcategory" value="" onclick="view_productcategory(document.getElementById('iproductclass').value);">
										<input readonly name="eproductcategoryname" id="eproductcategoryname" value="" onclick="view_productcategory(document.getElementById('iproductclass').value);">
									</td>
								</tr>
								<tr>
									<td width="19%">Harga Eceran</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="vproductretail" id="vproductretail" value="" maxlength='10' onkeyup="reformat(this)">
									</td>
									<td width="19%">Harga Pabrik</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="vproductmill" id="vproductmill" value="" maxlength='10' onkeyup="reformat(this)">
									</td>
								</tr>
								<tr>
									<td width="19%">Tanggal Stop Produksi</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="dproductstopproduction" id="dproductstopproduction" value="" readonly onclick="showCalendar('',this,this,'','dproductstopproduction',0,18,1)">
									</td>
									<td width="19%">Tanggal Pendaftaran</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="dproductregister" id="dproductregister" value="<?php echo date("d-m-Y"); ?>" readonly>
									</td>
								</tr>
								<tr>
									<td width="19%">Price List</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="checkbox" name="fproductpricelist" id="fproductpricelist" value="on">
									</td>
									<td width="19%">Margin</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" name="nproductmargin" id="nproductmargin" value="">
									</td>
								</tr>
								<tr>
									<td width="19%">Kategori Penjualan</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="hidden" name="isalescategory" id="isalescategory" value="" onclick='showModal("productbase/cform/salescategory/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="esalescategoryname" id="esalescategoryname" value="" onclick='showModal("productbase/cform/salescategory/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
									<td width="19%">Seri</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="hidden" name="iproductseri" id="iproductseri" value="" onclick='showModal("productbase/cform/productseri/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input readonly name="eproductseriname" id="eproductseriname" value="" onclick='showModal("productbase/cform/productseri/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									</td>
								</tr>
								<tr>
									<td width="19%">Berat (Kg)</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" onclick="if($(this).val()=='0'){ $(this).val('') }" onfocus="if($(this).val()=='0'){ $(this).val('') }" onmouseleave="if($(this).val()==''){ $(this).val(0) }" onkeyup="hanyaAngka(this);" name="n_berat" id="n_berat" value="0">
									</td>
									<td width="19%">Volume (M<sup>3</sup>)</td>
									<td width="1%">:</td>
									<td width="30%">
										<input type="text" onclick="if($(this).val()=='0'){ $(this).val('') }" onfocus="if($(this).val()=='0'){ $(this).val('') }" onmouseleave="if($(this).val()==''){ $(this).val(0) }" onkeyup="hanyaAngka(this);" name="n_volume" id="n_volume" value="0">
									</td>
								</tr>
								<tr>
									<td>
										<center>
											<input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
											<input name="cmdreset" id="cmdreset" value="Clear" type="button" onclick="show('productbase/cform/','#main');">
										</center>
									</td>
								</tr>
							</table>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script language="javascript" type="text/javascript">
	function xxx(a, b) {
		if (confirm(b) == 1) {
			document.getElementById("iproductdelete").value = a;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/productbase/cform/delete";
			formna.submit();
		}
	}

	function yyy(a) {
		document.getElementById("iproductedit").value = a;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/productbase/cform/edit";
		formna.submit();
	}

	function view_productgroup() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productgroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_supplier() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_productstatus() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productstatus","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_producttype(a) {
		showModal("productbase/cform/producttype/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_productcategory(a) {
		showModal("productbase/cform/productcategory/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_productclass() {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/productbase/cform/productclass","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function cek() {
		kode = document.getElementById("iproduct").value;
		nama = document.getElementById("eproductname").value;
		if (kode == '' || nama == '') {
			alert("Minimal kode Pelanggan dan nama Pelanggan diisi terlebih dahulu !!!");
		}
	}
</script>