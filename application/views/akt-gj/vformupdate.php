<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-gj/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="gjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Jurnal</td>
		<td><input readonly id="ijurnal" name="ijurnal" value="<?php echo $isi->i_jurnal; ?>"></td>
		  </tr>
			<?php 
				$tmp=explode('-',$isi->d_jurnal);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$isi->d_jurnal=$tgl.'-'.$bln.'-'.$thn;
			?>
	      <tr>
		<td width="10%">Tanggal</td>
		<td><input readonly id="djurnal" name="djurnal" value="<?php echo $isi->d_jurnal; ?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick='showModal("akt-gj/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");' value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Keterangan</td>
		<td><input id="edescription" name="edescription" value="<?php echo $isi->e_description;?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Debet</td>
		<td><input style="text-align:right;" readonly id="vdebet" name="vdebet" value="<?php echo number_format($isi->v_debet);?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Kredit</td>
		<td><input style="text-align:right;" readonly id="vkredit" name="vkredit" value="<?php echo number_format($isi->v_kredit);?>"></td>
	      </tr>
		<tr>
		  <td width="10%">&nbsp;</td>
		  <td>
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="reset" onclick="cekbalance();">
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:100px;" align="center">CoA</th>
					<th style="width:350px;" align="center">Keterangan</th>
					<th style="width:130px;" align="center">Debet</th>
					<th style="width:130px;" align="center">Kredit</th>
					<th style="width:50px;" align="center">Acction</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$debet	=number_format($row->v_mutasi_debet);
					$kredit	=number_format($row->v_mutasi_kredit);
				  	echo "<table class=\"listtable\" align=\"center\" style=\"width: 750px;\"><tbody>
							<tr>
		    				<td style=\"width:93px;\">
								<input style=\"width:93px;\" type=\"text\" 
								id=\"icoa$i\" name=\"icoa$i\" value=\"$row->i_coa\">
								<input type=\"hidden\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
							<td style=\"width:326px;\"><input style=\"width:326px;\" readonly type=\"text\" 
								id=\"ecoaname$i\" name=\"ecoaname$i\" value=\"$row->e_coa_name\"></td>
							<td style=\"width:120px;\"><input style=\"width:120px;text-align:right;\" type=\"text\" 
								id=\"vdebet$i\" name=\"vdebet$i\" value=\"$debet\" onkeyup=\"hitung();reformat(this);\"></td>
							<td style=\"width:120px;\"><input style=\"width:120px;text-align:right;\" type=\"text\" 
								id=\"vkredit$i\" name=\"vkredit$i\" value=\"$kredit\" onkeyup=\"hitung();reformat(this);\"></td>
							<td class=\"action\" style=\"width:60px;\"><a href=\"#\" onclick='hapus(\"akt-gj/cform/deletedetail/$row->i_jurnal/$row->i_coa/$row->v_mutasi_debet/$row->v_mutasi_kredit/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>
							</tr>
						  </tbody></table>";
				}
				?>			
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem;?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function view_area(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/akt-gj/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table class="listtable" align="center" style="width: 750px;">';
		so_inner+= '<th style="width:100px;" align="center">CoA</th>';
		so_inner+= '<th style="width:400px;" align="center">Keterangan</th>';
		so_inner+= '<th style="width:130px;" align="center">Debet</th>';
		so_inner+= '<th style="width:130px;" align="center">Kredit</th>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" align="center" style="width: 750px;"><tbody><tr><td style="width:93px;"><input type="hidden" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input style="width:93px;" readonly type="text" id="icoa'+a+'" name="icoa'+a+'" value=""></td>';
		si_inner+='<td style="width:326px;"><input style="width:326px;" readonly type="text" id="ecoaname'+a+'" name="ecoaname'+a+'" value=""></td>';
		si_inner+='<td style="width:120px;"><input style="width:120px;text-align:right;" id="vdebet'+a+'" name="vdebet'+a+'" value="0" onkeyup="hitung();reformat(this);"></td>';
		si_inner+='<td style="width:120px;"><input style="width:120px;text-align:right;" id="vkredit'+a+'" name="vkredit'+a+'" value="0" onkeyup="hitung();reformat(this);"></td><td class="action" style="width:60px;">&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" align="center" style="width: 750px;"><tbody><tr><td style="width:93px;"><input type="hidden" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input style="width:93px;" readonly type="text" id="icoa'+a+'" name="icoa'+a+'" value=""></td>';
		si_inner+='<td style="width:326px;"><input style="width:326px;" readonly type="text" id="ecoaname'+a+'" name="ecoaname'+a+'" value=""></td>';
		si_inner+='<td style="width:120px;"><input style="width:120px;text-align:right" id="vdebet'+a+'" name="vdebet'+a+'" value="0" onkeyup="hitung();reformat(this);"></td>';
		si_inner+='<td style="width:120px;"><input style="width:120px;text-align:right" id="vkredit'+a+'" name="vkredit'+a+'" value="0" onkeyup="hitung();reformat(this);"></td><td class="action" style="width:60px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris			=Array();
    var icoa			=Array();
    var ecoaname		=Array();
    var vdebet			=Array();
	var vkredit			=Array();
    for(i=1;i<a;i++){
		j++;
		baris[j]		= document.getElementById("baris"+i).value;
		icoa[j]			= document.getElementById("icoa"+i).value;
		ecoaname[j]		= document.getElementById("ecoaname"+i).value;
		vdebet[j]		= document.getElementById("vdebet"+i).value;
		vkredit[j]		= document.getElementById("vkredit"+i).value;
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("icoa"+i).value=icoa[j];
		document.getElementById("ecoaname"+i).value=ecoaname[j];
		document.getElementById("vdebet"+i).value=vdebet[j];
		document.getElementById("vkredit"+i).value=vkredit[j];
    }
	showModal("akt-gj/cform/coa/"+a+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function dipales(a){
  	 if((document.getElementById("ijurnal").value!='') &&
  	 	(document.getElementById("djurnal").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
		(document.getElementById("vdebet").value!='0') &&
		(document.getElementById("vkredit").value!='0')
		) {

  	 	if(a==0){
  	 		alert('Isi data item minimal 2 !!!');
		}else if(document.getElementById("vdebet").value!=document.getElementById("vkredit").value){
			alert('Nilai debet harus sama dengan nilai kredit !!!');
  	 	}else{
			if(parsefloat(formatulang(document.getElementById("vdebet").value))==parsefloat(formatulang(document.getElementById("vkredit").value))){
	  	  		document.getElementById("login").disabled=true;
			}else{
				alert("Debet kredit belum sama !!!!!!");
			}
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function hitung(){
	brs=document.getElementById("jml").value;
	vdebet =0;
	vkredit=0;
	for(i=1;i<=brs;i++){
		debet=formatulang(document.getElementById("vdebet"+i).value);
		kredit=formatulang(document.getElementById("vkredit"+i).value);
		vdebet = vdebet+parseFloat(debet);
		vkredit= vkredit+parseFloat(kredit);
	}
	document.getElementById("vdebet").value=formatcemua(vdebet);
	document.getElementById("vkredit").value=formatcemua(vkredit);
  }
  function cekbalance(){
	debet  = document.getElementById("vdebet").value;
	kredit = document.getElementById("vkredit").value;
	if(debet==kredit){
		show('list-akt-gj/cform/','#main');
	}else{
		alert("Debet Kredit belum sama !!!");
	}
  }
</script>
