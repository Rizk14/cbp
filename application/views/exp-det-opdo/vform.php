<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'exp-det-opdo/cform/export', 'update' => '#pesan', 'type' => 'post')); ?>
			<div class="effect">
				<div class="accordion2">
					<table class="mastertable">
						<tr>
							<td width="19%">Date From</td>
							<td width="1%">:</td>
							<td width="80%">
								<?php 
								$data = array(
									'name'        => 'dfrom',
									'id'          => 'dfrom',
									'value'       => date('01-m-Y'),
									'readonly'    => 'true',
									'onclick'	  => "showCalendar('',this,this,'','dfrom',0,20,1)"
								);
								echo form_input($data); ?></td>
						</tr>
						<tr>
							<td width="19%">Date To</td>
							<td width="1%">:</td>
							<td width="80%">
								<?php 
								$data = array(
									'name'        => 'dto',
									'id'          => 'dto',
									'value'       => date('d-m-Y'),
									'readonly'    => 'true',
									'onclick'	  => "showCalendar('',this,this,'','dto',0,20,1)"
								);
								echo form_input($data); ?></td>
						</tr>

						<tr>
							<td width="19%">Supplier</td>
							<td width="1%">&nbsp;</td>
							<td width="80%"><input type="text" id="esuppliername" name="esuppliername" value="" readonly onclick='showModal("exp-det-opdo/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input type="hidden" id="isupplier" name="isupplier" value=""></td>
						</tr>

						<tr>
							<td width="19%">&nbsp;</td>
							<td width="1%">&nbsp;</td>
							<td width="80%">
								<input name="login" id="login" value="Export" type="submit">
								<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-det-opdo/cform/','#main')">
							</td>
						</tr>
					</table>
				</div>
			</div>
			<?= form_close(); ?>
		</td>
	</tr>
</table>
<div id="pesan"></div>