<div id="tmp">
<h2><?php echo 'Export Op vs Do Supplier Detail'; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'exp-det-opdo/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
 	      <tr>
		<td colspan="13" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr> 
	    </thead>
	    	<th>No</th>
	     	<th>OP</th>
			  <th>Kode Barang</th>
			   <th>SPB</th>
			  <th>Nama</th>
			  <th>Area</th>
			  <th>TGLOP</th>
				<th>ORDER</th>
				<th>RP ORDER</th>
				<th>DELIVER</th>
				<th>RP DELIVER</th>
				<th>PENDING</th>
			  <th>Supplier</th>

	    <tbody>
			<?php 
			$no = 0;
			foreach ($isi->result() as $row) {
				$no++;
				echo '<tr>
				<td>'.$no.'</td>
				<td>'.$row->i_op.'</td>
				<td>'.$row->i_product.'</td>
				<td>'.$row->i_reff.'</td>
				<td>'.$row->e_op_remark.'</td>
				<td>'.$row->i_area.'</td>
				<td>'.$row->d_op.'</td>
				<td>'.$row->n_order.'</td>
				<td>'.$row->rpop.'</td>
				<td>'.$row->n_delivery.'</td>
				<td>'.$row->rpdo.'</td>
				<td>'.$row->pending.'</td>
			 <td>'.$row->e_supplier_name.'</td>
			 </tr>';
			}
		?>
	    </tbody>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

</script>
