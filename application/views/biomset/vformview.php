<table class="maintable">
  <tr>
    <td align="left">
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable">
            <a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();">
              <input type="button" value="Export" class="float-right" />
            </a>
            <div id="detailheader" align="center">
              <table id="itemtem" class="listtable" style="width: 100%;">
                <thead>
                  <tr align="center">
                    <th style="width: 65px">Periode</th>
                    <th style="width: 352px">Area</th>
                    <th style="width: 98px">Biaya Kas Kecil</th>
                    <th style="width: 97px">Biaya Kas Besar</th>
                    <th style="width: 98px">Jumlah Biaya</th>
                    <th>Omset</th>
                    <th style="width: 104px">Persentase</th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                  $totalkk    = 0;
                  $totalkb    = 0;
                  $totalbiaya = 0;
                  $totalnota  = 0;
                  if ($isi != 'zero') :
                    foreach ($isi as $row) :
                  ?>
                      <tr>
                        <td><?= $row->i_periode; ?></td>
                        <td><?= $row->i_area . ' - ' . $row->e_area_name; ?></td>
                        <td align="right"><?= number_format($row->v_biaya_kk); ?></td>
                        <td align="right"><?= number_format($row->v_biaya_kb); ?></td>
                        <td align="right"><?= number_format($row->v_biaya_total); ?></td>
                        <td align="right"><?= number_format($row->v_omset); ?></td>
                        <td align="right"><?= $row->persen; ?> %</td>
                      </tr>
                  <?php 
                      $totalkk    += $row->v_biaya_kk;
                      $totalkb    += $row->v_biaya_kb;
                      $totalbiaya += $row->v_biaya_total;
                      $totalnota  += $row->v_omset;
                    endforeach;
                  endif;
                  $totalpersen =  $totalbiaya / (($totalnota == 0) ? 1 : $totalnota) * 100;
                  ?>
                </tbody>

                <tfoot id="total">
                  <tr>
                    <th align="center" colspan="2" style="width: 45%">Total</th>
                    <th align="right" style="width: 11%"><?= number_format($totalkk); ?></th>
                    <th align="right"><?= number_format($totalkb); ?></th>
                    <th align="right"><?= number_format($totalbiaya); ?></th>
                    <th align="right"><?= number_format($totalnota); ?></th>
                    <th align="right" style="width: 11%"><?= number_format($totalpersen, 3); ?> %</th>
                  </tr>
                </tfoot>
              </table>
            </div>

            <!-- <div id="detailisi" align="center">
              <table class="listtable" style="width: 100%;">

              </table>
            </div> -->

            <table class="listtable">

            </table>
          </table> <!-- end mastertable -->
          <div id="pesan"></div>
        </div> <!-- end accordion2 -->
      </div> <!-- end effect -->
      <?= form_close() ?>
    </td> <!-- end td -->
  </tr> <!-- end tr -->
</table> <!-- end table -->

<div>
  <div id="div-chart-area" style="position: relative; width:100% ; height: 300px;"><canvas id="chart-area"></canvas></div>
</div>

<script language="javascript" type="text/javascript">
  $(function() {
    initOverallChart();
  })

  function exportexcel() {
    var iperiode1 = '<?= $iperiode1; ?>';
    var iperiode2 = '<?= $iperiode2; ?>';

    var abc = "<?php echo site_url('biomset/cform/export/'); ?>/" + iperiode1 + "/" + iperiode2;
    $("#href").attr("href", abc);
    return true;
  }

  // function chartx(iperiode) {
  //   lebar = 1366;
  //   tinggi = 768;
  //   /*
  //       eval('window.open("<?php echo site_url(); ?>"+"/salesperformance_new/cform/chartx/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  //   */
  //   /*
  //       eval('window.open("<?php echo site_url(); ?>"+"/salesperformance_new/cform/fcf/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  //   */
  //   show("salesperformance_new/cform/fcf/" + iperiode, "#main");
  // }

  function initOverallChart() {
    var dslabel = ['Kas Kecil', 'Kas Besar', 'Omset (Nota)'];
    var dscolor = ['#E86464CC', '#1400CCCC', '#5DC067CC'];

    var tfoot = document.querySelector('#total').querySelectorAll('th');

    var dataSet = [];
    var vkk = [];
    var vkb = [];
    var vomset = [];

    tfoot.forEach(function(el, x) {
      if (x == 1) {
        vkk.push(formatulang(el.innerText));
      }
      if (x == 2) {
        vkb.push(formatulang(el.innerText));
      }
      if (x == 4) {
        vomset.push(formatulang(el.innerText));
      }
    })

    if (vkk[0] == 0 && vkb[0] == 0 && vomset[0] == 0) {
      $('#div-chart-area').parent().hide();
    } else {
      $('#div-chart-area').parent().show();
      var dataReal = [vkk, vkb, vomset];

      var dataData = {
        label: '',
        backgroundColor: [],
        data: []
      }

      for (let n = 0; n < dslabel.length; n++) {
        dataData.backgroundColor.push(dscolor[n]);
        dataData.data.push(dataReal[n]);
      }

      dataSet.push(dataData);

      var config = {
        type: 'doughnut',
        data: {
          labels: dslabel,
          datasets: dataSet,
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            legend: {
              position: 'top'
            }
          }
        }

      }

      var BarChart = new Chart(document.querySelector('#chart-area'), config);
    }
  }
</script>