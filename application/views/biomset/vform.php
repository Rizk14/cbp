<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'biomset/cform/view', 'update' => '#pesan', 'type' => 'post')); ?>
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable">
            <tr>
              <td width="19%">Periode</td>
              <td width="1%">:</td>
              <td width="89%">
                <!-- <input type="text" id="iperiode1" name="iperiode1" class="monthpicker" readonly="readonly" value="<?= (isset($periode1)) ? $periode1 : ''; ?>" onclick="showCalendar('',this,this,'','dfrom',0,20,1)"> -->
                <input type="hidden" id="iperiode1" name="iperiode1" value="">
                <select name="bulan1" id="bulan1" onmouseup="buatperiode1()">
                  <option></option>
                  <option value='01'>Januari</option>
                  <option value='02'>Pebruari</option>
                  <option value='03'>Maret</option>
                  <option value='04'>April</option>
                  <option value='05'>Mei</option>
                  <option value='06'>Juni</option>
                  <option value='07'>Juli</option>
                  <option value='08'>Agustus</option>
                  <option value='09'>September</option>
                  <option value='10'>Oktober</option>
                  <option value='11'>November</option>
                  <option value='12'>Desember</option>
                </select>
                <select name="tahun1" id="tahun1" onMouseUp="buatperiode1()">
                  <option></option>
                  <?php 
                  $tahun1 = date('Y') - 3;
                  $tahun2 = date('Y');
                  for ($i = $tahun1; $i <= $tahun2; $i++) {
                    echo "<option value='$i'>$i</option>";
                  }
                  ?>
                </select>
                S / d
                <!-- <input type="text" id="iperiode2" name="iperiode2" class="monthpicker" readonly="readonly" placeholder="Opsional" onclick="showCalendar('',this,this,'','dto',0,20,1)"> -->
                <input type="hidden" id="iperiode2" name="iperiode2" value="">
                <select name="bulan2" id="bulan2" onmouseup="buatperiode2()">
                  <option></option>
                  <option value='01'>Januari</option>
                  <option value='02'>Pebruari</option>
                  <option value='03'>Maret</option>
                  <option value='04'>April</option>
                  <option value='05'>Mei</option>
                  <option value='06'>Juni</option>
                  <option value='07'>Juli</option>
                  <option value='08'>Agustus</option>
                  <option value='09'>September</option>
                  <option value='10'>Oktober</option>
                  <option value='11'>November</option>
                  <option value='12'>Desember</option>
                </select>
                <select name="tahun2" id="tahun2" onMouseUp="buatperiode2()">
                  <option></option>
                  <?php 
                  $tahun1 = date('Y') - 3;
                  $tahun2 = date('Y');
                  for ($i = $tahun1; $i <= $tahun2; $i++) {
                    echo "<option value='$i'>$i</option>";
                  }
                  ?>
                </select>
                <span class="px-3"></span>
                <input type="submit" class="px-3 btn btn-default" id="login" value="View" onclick="return dipales()">
                <span class="px-2"></span>
                <input name="cmdreset" id="cmdreset" value="Reset" type="button" onclick="show('biomset/cform/','#main')">
              </td>
            </tr>
          </table>
        </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>

<div id="pesan"></div>

<script languge=javascript type=text/javascript>
  xcDateFormat = "yyyy-mm";

  function dipales() {
    var periode1 = $('#iperiode1').val() /* .replace('-', '') */ ;
    var periode2 = $('#iperiode2').val() /* .replace('-', '') */ ;

    if (periode2 != '' && (parseInt(periode1) > parseInt(periode2))) {
      alert('Periode Awal Lebih Besar!!');
      return false;
    }
  }

  function buatperiode1() {
    periode = document.getElementById("tahun1").value + document.getElementById("bulan1").value;
    document.getElementById("iperiode1").value = periode;
  }

  function buatperiode2() {
    periode = document.getElementById("tahun2").value + document.getElementById("bulan2").value;
    document.getElementById("iperiode2").value = periode;
  }
</script>