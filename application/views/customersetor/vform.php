<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Penyetor (Pelanggan)</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Penyetor (Pelanggan)</a></li>  
	  </ul>  
	  <div id="content_1" class="content">
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customersetor/cform/cari','update'=>'#main','type'=>'post'));?>
	<div class="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Kode Pelanggan</th>
			<th>Kode Area</th>
			<th>Penyetor</th>
			<th>No. Rekening</th>	
			<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td>$row->i_customer</td>
				  <td>$row->i_area</td>
				  <td>$row->e_customer_setorname</td>
				  <td>$row->e_customer_setorrekening</td>
				  <td class=\"action\">
		<a href=\"#\" onclick='show(\"customersetor/cform/edit/$row->i_setor/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>
		&nbsp;&nbsp;&nbsp;
		<a href=\"#\" onclick='hapus(\"customersetor/cform/delete/$row->i_setor/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>
					  </td>
					</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
  <div id="content_2" class="content">
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customersetor/cform/simpan','update'=>'#tmpx','type'=>'post'));?>
	<div id="mastercustomerownerform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="16%">Area</td>
		<td width="1%">:</td>
		<td width="83%">
		<input id="iarea" name="iarea" maxlength="5" onclick='showModal("customersetor/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		</td>
	      </tr>	    
	      <tr>
		<td width="16%">Kode Pelanggan</td>
		<td width="1%">:</td>
		<td width="83%">
		<input id="icustomer" name="icustomer" maxlength="5" onclick='showModal("customersetor/cform/pelanggan/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		</td>
	      </tr>
	      <tr>
		<td width="16%">Nama Penyetor</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'ecustomersetor',
			              'id'          => 'ecustomersetor',
			              'value'       => '',
			              'maxlength'   => '30');
				echo form_input($data);?></td>
	      </tr>	 
	      <tr>
		<td width="16%">No. Rekening</td>
		<td width="1%">:</td>
		<td width="83%"><?php 
				$data = array(
			              'name'        => 'ecustomerrekening',
			              'id'          => 'ecustomerrekening',
			              'value'       => '',
			              'maxlength'   => '30');
				echo form_input($data);?></td>
	      </tr>	           
	      <tr>
		<td width="16%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="83%">
		  <input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('customersetor/cform/','#tmpx');">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
  </div>
</div>
</div>
