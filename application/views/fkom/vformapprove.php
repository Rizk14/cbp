<?php 
include ("php/fungsi.php");
?>
<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('fkom/cform/simpan', array('id' => 'notasformupdate', 'name' => 'notasformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">

	      <tr>
		<td>No Faktur</td>
		<td><input id="ifakturkomersial" name="ifakturkomersial" value="" maxlength=6></td>
		<td></td>
		<td></td>
	      </tr>
	      <tr>
		<td>Nota</td>
		<td><?php 	if($isi->i_nota!='') {
					echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
										   readonly>"; 
				}else{
					echo "<input type=\"hidden\" id=\"inota\" name=\"inota\" value=\"\"
										   readonly>"; 
				}	
				if($isi->d_nota!=''){				
					$tmp=explode("-",$isi->d_nota);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dnota=$hr."-".$bl."-".$th;
					echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\">
						 ";
				
				}else{
			?>
			<input readonly id="dnota" name="dnota" value="">
			<?php }?>
			</td>			
		<td>Nilai Kotor</td>
		<?php 
			//$enin=number_format($isi->v_nota_gross);
			$enin=number_format($isi->v_spb);
		?>	
		<td><input id="vspb" name="vspb" readonly value="<?php echo $enin; ?>"></td>
	      </tr>
	      <tr>
		<td>SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
			<input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
		<td>Discount 1</td>
		<td><input id ="ncustomerdiscount1" name="ncustomerdiscount1" readonly
				   value="<?php echo $isi->n_nota_discount1; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_nota_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="arena" name="arena" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
		    <input readonly id="spbold" name="spbold" value="<?php echo $isi->i_spb_old; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 2</td>
		<td><input id="ncustomerdiscount2" name="ncustomerdiscount2" readonly
				   value="<?php echo $isi->n_nota_discount2; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_nota_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 3</td>
		<td><input id="ncustomerdiscount3" name="ncustomerdiscount3" readonly
				   value="<?php echo $isi->n_nota_discount3; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_nota_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly >
		<input readonly id="esalesmanname" name="esalesmanname" value="<?php if($isi->e_salesman_name=='KONSINYASI') echo 'PUSAT BABY'; else echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount 4</td>
		<td><input id="ncustomerdiscount4" name="ncustomerdiscount4" readonly
				   value="<?php echo $isi->n_nota_discount4; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" 
				   value="<?php echo number_format($isi->v_nota_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox" readonly 
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>&nbsp;Masalah&nbsp;
			<input id="fmasalah" name="fmasalah" type="checkbox" value="" <?php if($isi->f_masalah=='t') echo 'value="on" checked'; else echo 'value=""';?> onclick="cekmasalah();">&nbsp;Insentif&nbsp;
			<input id="finsentif" name="finsentif" type="checkbox" <?php if($isi->f_insentif=='t') echo 'value="on" checked'; else echo 'value=""';?> onclick="cekinsentif();">&nbsp;Cicil&nbsp;
			<input id="fcicil" name="fcicil" type="checkbox" <?php if($isi->f_cicil=='t') echo 'value="on" checked'; else echo 'value=""';?> onclick="cekcicil();"></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Insentif</td>
		<td><input id="finsentif" name="finsentif" type="checkbox" value="on" checked>
			Nota Lama&nbsp;<input readonly id="inotaold" name="inotaold" value="<?php echo $isi->i_nota_old; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			/*$tmp=$isi->v_nota-$isi->v_nota_discounttotal;*/
			$tmp=$isi->v_nota_netto;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" readonly
				   value="<?php echo number_format($isi->v_spb-$isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<?php 
			$tmp = explode("-", $isi->d_nota);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dspb	= $yir."/".$mon."/".$det;
			$dudet	=dateAdd("d",$isi->n_nota_toplength,$dspb);
			$dudet 	= explode("-", $dudet);
			$det1	= $dudet[2];
			$mon1	= $dudet[1];
			$yir1 	= $dudet[0];
			$dudet	= $det1."-".$mon1."-".$yir1;
		?>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_nota_toplength; ?>">
			&nbsp;&nbsp;Jatuh tempo&nbsp;
			<input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $dudet; ?>"></td>
		<td>Nilai Kotor (realisasi)</td>
		<td><input id="vnotagross" name="vnotagross" readonly
			 value="<?php echo number_format($isi->v_nota_gross); ?>"></td>
	      </tr>
	      <tr>
		<td>Surat Jalan</td>
		<?php 
			if($isi->d_sj!=''){
				$tmp=explode("-",$isi->d_sj);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$dsj=$hr."-".$bl."-".$th;
			}else{
				$dsj='';
			}
		?>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly value="<?php if($isi->i_sj) echo $isi->i_sj; ?>">
		    <input readonly readonly id="dsj" name="dsj" value="<?php echo $dsj; ?>"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly
			 value="<?php echo number_format($isi->v_nota_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Keterangan</td>
		<td><input readonly id="eremark" name="eremark" value="<?php echo $isi->e_remark; ?>"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input id="vspbafter" name="vspbafter" readonly
			 value="<?php echo number_format($isi->v_nota_netto); ?>">
			 <input type="hidden" id="fspbplusppn" name="fspbplusppn" value="<?php echo $isi->f_spb_plusppn;?>">
			 <input type="hidden" id="fspbplusdiscount" name="fspbplusdiscount" value="<?php echo $isi->f_spb_plusdiscount;?>">
			 <input type="hidden" id="nprice" name="nprice" value="<?php echo $isi->n_price;?>">
			 <input type="hidden" id="vnotappn" name="vnotappn" value="<?php echo number_format($isi->v_nota_ppn); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("fkom/cform/view/<?php echo $dfrom.'/'.$dto.'/';?>","#main")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:100px;"  align="center">Harga</th>
<!--					<th style="width:60px;"  align="center">Jml Pesan</th> -->
					<th style="width:60px;"  align="center">Jml Dlv</th>
<!--				</table>	-->
			</div>
			<div id="detailisi" align="center">
				<?php 				
/*				echo "<table class=\"listtable\" style=\"width:750px;\">";	*/
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$harga	=number_format($row->v_unit_price);
//					$norder	=number_format($row->n_order,0);
					$ndeliv	=number_format($row->n_deliver);
				  	echo "<tbody>
							<tr>
		    				<td>
								<input style=\"width:25px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td><input style=\"width:63px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td><input style=\"width:364px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td><input style=\"width:100px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td><input style=\"text-align:right; width:100px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>";
/*							<td width=\"58px\"><input style=\"text-align:right; width:58px;\"  type=\"text\" 
								id=\"norder$i\" readonly
								name=\"norder$i\" value=\"$norder\"></td>	*/
					echo "	<td><input style=\"text-align:right; width:60px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\"></td>
							</tr>
						  </tbody>";
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function hitungnilai(){
	var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');
	var nilaidis=document.getElementById("vspbdiscounttotalafter").value.replace(/\,/g,'');
	if(!isNaN(nilaidis)){
		if((nilaispb-nilaidis)<0){
			alert("Nilai discount tidak valid !!!!!");
			document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
		}else{
			document.getElementById("vspbafter").value=formatcemua(nilaispb-nilaidis);
		}
	}else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
	}
  }
  function hitungdiscount(){
	var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');
	var nilaitot=document.getElementById("vspbafter").value.replace(/\,/g,'');
	if(!isNaN(nilaitot)){
		if((nilaispb-nilaitot)<0){
			alert("Nilai total tidak valid !!!!!");
			document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
		}else{
			document.getElementById("vspbdiscounttotalafter").value=formatcemua(nilaispb-nilaitot);
		}
	}else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
	}
  }
  function dipales(){
  	if( (document.getElementById("dnota").value!='')&&(document.getElementById("ifakturkomersial").value!='') ){
   		document.getElementById("login").disabled=true;
	}else{
		document.getElementById("login").disabled=false;
		alert("Masih ada header yang salah");
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function editnilai(){
	var jml 	= parseFloat(document.getElementById("jml").value);
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var ndis4	= parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	var vdis4	= 0;
	for(i=1;i<=jml;i++){
	  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
		hrg			= hrg+hrgtmp;
	}
	hrg=Math.round(hrg);
	vdis1=Math.round(vdis1+(hrg*ndis1)/100);
	vdis2=Math.round(vdis2+(((hrg-vdis1)*ndis2)/100));
	vdis3=Math.round(vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100));
	vdis4=Math.round(vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100));
	vdistot	= Math.round(vdis1+vdis2+vdis3+vdis4);
	vhrgreal= Math.round(hrg-vdistot);
	document.getElementById("vspbdiscounttotalafter").value=formatcemua(vdistot);
	document.getElementById("vspbafter").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("fspbplusppn").value;
	var fdis = document.getElementById("fspbplusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vspbafter").value));
	var kotor  = parseFloat(formatulang(document.getElementById("vspb").value));
	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("nprice").value=1;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("nprice").value=bersih/kotor;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("nprice").value=1/1.1;
		kotorminppn=Math.round(hrg/1.1);
		vhrgreal= Math.round(kotorminppn);
		pepen = Math.round(vhrgreal*0.1);
		berrsih=Math.round(kotorminppn+pepen)
		document.getElementById("vnotagross").value=formatcemua(kotorminppn);
		document.getElementById("vspbdiscounttotalafter").value=0;//formatcemua(vdistot);
		document.getElementById("vspbafter").value=formatcemua(berrsih);
		document.getElementById("vnotappn").value=formatcemua(pepen);
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("nprice").value=1/1.1;
		kotorminppn=Math.round(hrg/1.1);
		vdis1	= 0;
		vdis2	= 0;
		vdis3	= 0;
		vdis4	= 0;
		vdis1=Math.round(vdis1+(kotorminppn*ndis1)/100);
		vdis2=Math.round(vdis2+(((kotorminppn-vdis1)*ndis2)/100));
		vdis3=Math.round(vdis3+(((kotorminppn-(vdis1+vdis2))*ndis3)/100));
		vdis4=Math.round(vdis4+(((kotorminppn-(vdis1+vdis2+vdis3))*ndis4)/100));
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		document.getElementById("vcustomerdiscount4").value=formatcemua(vdis4);
		vdistot	= Math.round(vdis1+vdis2+vdis3+vdis4);
		vhrgreal= Math.round(kotorminppn-vdistot);
		pepen = Math.round(vhrgreal*0.1);

		berrsih=Math.round(kotorminppn-vdistot+pepen)
		document.getElementById("vnotagross").value=formatcemua(kotorminppn);
		document.getElementById("vspbdiscounttotalafter").value=formatcemua(vdistot);
		document.getElementById("vspbafter").value=formatcemua(berrsih);
		document.getElementById("vnotappn").value=formatcemua(pepen);
	}
  }
  function cekmasalah()
  {
    masalah=document.getElementById("fmasalah").value;
    if(masalah=='on')
    {
      document.getElementById("fmasalah").checked=false;
      document.getElementById("fmasalah").value='';
    }else{
      document.getElementById("fmasalah").checked=true;
      document.getElementById("fmasalah").value='on';
    }
  }
  function cekinsentif()
  {
    masalah=document.getElementById("finsentif").value;
    if(masalah=='on')
    {
      document.getElementById("finsentif").checked=false;
      document.getElementById("finsentif").value='';
    }else{
      document.getElementById("finsentif").checked=true;
      document.getElementById("finsentif").value='on';
    }
  }
  function cekcicil()
  {
    masalah=document.getElementById("fcicil").value;
    if(masalah=='on')
    {
      document.getElementById("fcicil").checked=false;
      document.getElementById("fcicil").value='';
    }else{
      document.getElementById("fcicil").checked=true;
      document.getElementById("fcicil").value='on';
    }
  }
</script>
