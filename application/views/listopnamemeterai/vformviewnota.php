<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url'=>'listopnamemeterai/cform/view','update'=>'#main','type'=>'post'));?>
				<div class="effect">
					<div class="accordion2">
						<br>
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="" align="center"><input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" placeholder="Cari Data" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" > <input type="hidden" id="chkntx" name="chkntx" value="<?php echo $chkntx; ?>" > <input type="hidden" id="chkalx" name="chkalx" value="<?php echo $chkalx; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
								</tr>
							</thead>
							
						</table>
						
						<table class="listtable" id="sitabel">
							
							<th>No Nota</th>
							<th>Tgl Nota</th>
							<th>Tgl Jatuh Tempo</th>
							<th>Area</th>
							<th>Customer</th>
							<th>TOP</th>
							<th>Salesman</th>
							<th>Nilai</th>
							<th>Sisa</th>
							<tbody>
								<?php 
								$jumnilai = 0;
								$jumsisa = 0;
								if($isi){
									foreach($isi as $row){
										$jumnilai = $jumnilai + $row->v_materai;
										$jumsisa = $jumsisa + $row->v_materai_sisa;
										echo "<tr>";
										echo "<td>$row->i_nota</td>";
										echo "<td>$row->d_nota</td>
										<td>$row->d_jatuh_tempo</td>
										<td>$row->e_area_name</td>";
										echo "<td>($row->i_customer) $row->e_customer_name</td>
										     <td>$row->top</td>
										     <td>$row->e_salesman_name</td>";
										echo "<td align=right>".number_format($row->v_materai)."</td>
										<td align=right>".number_format($row->v_materai_sisa)."</td>
									    </tr>";
								}
							}
							echo "
							<tr>
								<td colspan='7'>Total Per Halaman</td>
								<td align='right'>".number_format($jumnilai)."</td>
								<td align='right'>".number_format($jumsisa)."</td>
							</tr>";
							?>
						</tbody>
					</table>
					<?php echo "<center>".$this->pagination->create_links()."</center>"; ?>
				</div>
			</div>
			<br>
			<!-- <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button"> -->
			<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><input type="button" value="Export"/></a>
			<input name="kembali" id="kembali" value="Kembali" type="button" onclick="show('listopnamemeterai/cform/','#main')">
			<?=form_close()?>
		</td>
	</tr>
</table>

<table class="listtable">
	<thead>
		<tr >
			<th>Total Meterai</th>
			<th>Sisa Meterai</th>
			<th>Meterai Dibayar</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo number_format($tmp_meterai); ?></td>
			<td><?php echo number_format($tmp_sisameterai); ?></td>
			<td><?php echo number_format($tmp_meterai - $tmp_sisameterai); ?></td>
		</tr>
	</tbody>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

    function exportexcel(){
      var datefrom = document.getElementById('dfrom').value;
      var dateto = document.getElementById('dto').value;
      var iarea = document.getElementById('iarea').value;
      var chkntx = document.getElementById('chkntx').value;
      var chkalx = document.getElementById('chkalx').value;
      // // alert(ibrand);

        var abc = "<?php echo site_url('listopnamemeterai/cform/exportexcel/'); ?>/"+datefrom+"/"+dateto+"/"+iarea+"/"+chkntx+"/"+chkalx;
        $("#href").attr("href",abc);
        return true;
      
    }

</script>
