<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'kategoripenjualan/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="mastergroupform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="17%">ID Kategori Penjualan</td>
								<td width="1%">:</td>
								<td width="87%">
									<input readonly type="text" name="isalescategory" id="isalescategory" value="<?php echo $isi->i_sales_category; ?>" maxlength='5'>
								</td>
							</tr>
							<tr>
								<td width="17%">Nama Kategori Penjualan</td>
								<td width="1%">:</td>
								<td width="87%">
									<input type="text" name="esalescategoryname" id="esalescategoryname" value="<?php echo $isi->e_sales_categoryname; ?>" maxlength="50" onkeyup="gede(this)">
									<input type="hidden" name="esalescategorynamex" id="esalescategorynamex" value="<?php echo $isi->e_sales_categoryname; ?>" maxlength="50" onkeyup="gede(this)">
								</td>
							</tr>
							<tr>
								<td width="12%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="87%">
									<input name="submit" id="submit" value="Simpan" type="submit">
									<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('kategoripenjualan/cform/','#main');">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<div id="pesan"></div>

<script language="javascript" type="text/javascript">
	function dipales() {
		if (document.getElementById("esalescategoryname").value == "") {
			alert("Data Tidak Boleh Kosong!!!");
			return false;
		} else {
			document.getElementById("submit").hidden = true;
			return true;
		}
	}
</script>