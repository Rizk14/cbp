<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'kategoripenjualan/cform/simpan', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="mastergroupform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="17%">Nama Kategori Penjualan</td>
								<td width="1%">:</td>
								<td width="87%">
									<input type="text" name="esalescategoryname" id="esalescategoryname" maxlength="50" value="" onkeyup="gede(this)">
								</td>
							</tr>
							<tr>
								<td width="12%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="87%">
									<input name="submit" id="submit" value="Simpan" type="submit" onclick="return dipales()">
									<input name="cmdreset" id="cmdreset" value="Clear" type="button" onclick="show('kategoripenjualan/cform/','#main');">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'kategoripenjualan/cform/index', 'update' => '#main', 'type' => 'post')); ?>
			<div class="effect">
				<div class="accordion2">
					<table class="listtable">
						<thead>
							<tr>
								<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
							</tr>
						</thead>

						<th>ID Kategori Penjualan</th>
						<th>Nama Kategori Penjualan</th>
						<th class="action">Action</th>

						<tbody>
							<?php
							if ($isi) {
								foreach ($isi as $row) {
									echo "<tr> 
											<td align='center'>$row->i_sales_category</td>
											<td align='center'>$row->e_sales_categoryname</td>";
									echo "	<td class=\"action\"><a href=\"#\" onclick='show(\"kategoripenjualan/cform/edit/$row->i_sales_category\",\"#main\")'>
												<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
									echo "	</td></tr>";
								}
							}
							?>
						</tbody>
					</table>
					<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<div id="pesan"></div>

<script language="javascript" type="text/javascript">
	function dipales() {
		if (document.getElementById("esalescategoryname").value == "") {
			alert("Data Tidak Boleh Kosong!!!");
			return false;
		} else {
			document.getElementById("submit").hidden = true;
			return true;
		}
	}
</script>