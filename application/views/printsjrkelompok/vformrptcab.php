<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $ymp='';
  $xmp='';
  $zmp='';
  $cet='';
  $isi=$master;
	foreach($isi as $row){
    $cetak='';
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab		= str_repeat(" ",9);
		$hal	= 1;
		$ipp    = new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$tmp=explode("-",$row->d_sj);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_sj=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$xmp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
		$i	= 0;
		$j	= 0;
		$hrg    = 0;
		$sj			= $row->i_sj;
		$iarea	= $row->i_area;
		$query 	= $this->db->query(" select * from tm_nota_item where i_sj='$sj' and substring(i_sj,9,2)='$iarea'",false);
		$jml 	= $query->num_rows();
    $detail	= $this->mmaster->bacadetail($row->i_sj,$row->i_area);
		foreach($detail as $rowi){
			$i++;
			$j++;
			$hrg	= $hrg+($rowi->n_deliver*$rowi->v_unit_price);
			$pro	= $rowi->i_product;
			if(strlen($rowi->e_product_name )>65){
				$nam	= substr($rowi->e_product_name,0,65);
			}else{
				$nam	= $rowi->e_product_name.str_repeat(" ",65-strlen($rowi->e_product_name ));
			}			
			$del	= number_format($rowi->n_deliver);
			$pjg	= strlen($del);
			$spcdel	= 4;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcdel	= $spcdel-1;
			}
			$pric	= number_format($rowi->v_unit_price);
			$pjg	= strlen($pric);
			$spcpric= 15;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcpric= $spcpric-1;
			}
			$tot	= number_format($rowi->n_deliver*$rowi->v_unit_price);
			$pjg	= strlen($tot);
			$spctot = 20;
			for($xx=1;$xx<=$pjg;$xx++){
				$spctot	= $spctot-1;
			}
			$aw	= 3;
			$pjg	= strlen($i);
			for($xx=1;$xx<=$pjg;$xx++){
				$aw=$aw-1;
			}+
			$aw=str_repeat(" ",$aw);
			$cetak.=($ab.$aw." ".$i.str_repeat(" ",6).$pro." ".$nam." ".str_repeat(" ",$spcdel).$del.str_repeat(" ",$spcpric).$pric.str_repeat(" ",$spctot)." ".$tot."\n");
			if($jml>42){
				if(($i%51)==0){
					$cetak.=(CHR(18).$nor.str_repeat('-',73)."\n");
					$cetak.=($nor.str_repeat(" ",40)."bersambung ......."."\n");
					$hal=$hal+1;
					$ymp=CetakHeader($row,$hal,$nor,$abn,$ab);
					$j	= 0;
				}elseif(
					(($i<51)&&($i==$jml)) 
					){
					$cetak.=(CHR(18).$nor.str_repeat('-',73)."\n");
					$cetak.=($nor.str_repeat(" ",40)."bersambung ......."."\n");
					$hal=$hal+1;
					$ymp=CetakHeader($row,$hal,$nor,$abn,$ab);
					$j	= 0;
				}
			}	
		}
    $zmp=CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp);
    $cet.=$xmp.$cetak.$ymp.$zmp;
	}
#  echo $cet;
  $ipp->setdata($cet);
  $ipp->printJob();
  echo "<script>this.close();</script>";
	function CetakHeader($row,$hal,$nor,$abn,$ab,$ipp){
    $cetak='';
		$cetak.=(CHR(18));
		$cetak.=($nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).NmPerusahaan."           ".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."KEPADA Yth.\n");
		$cetak.=($nor.AlmtPerusahaan."                   ".$row->e_customer_name."\n");
		if(strlen($row->e_customer_address)<35){
			$cetak.=($nor."Telp.: ".TlpPerusahaan."                   ".$row->e_customer_address."\n");
		}else{
			$cetak.=($nor."Telp.: ".TlpPerusahaan."                   ".CHR(15).$row->e_customer_address.CHR(18)."\n");
		}
		$cetak.=($nor."Fax  : ".FaxPerusahaan."                   ".$row->e_customer_city."\n");
		$cetak.=($nor."NPWP : ".NPWPPerusahaan."           Telp. ".$row->e_customer_phone."\n\n");
		$cetak.=($nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."No. Surat Jalan: ".$row->i_sj.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n");
		$cetak.=($nor."No. PO         : ".$row->i_spb_po."\n");
		$cetak.=($nor."No. SPB        : ".$row->i_spb."\n\n");
		$cetak.=($nor."Harap diterima barang-barang berikut ini:\n");
		$cetak.=($nor.CHR(218).str_repeat(CHR(196),72).CHR(191)."\n");
		$cetak.=($nor.CHR(179)."NO.  KD-BARANG       NAMA BARANG                UNIT   HARGA     JUMLAH ".CHR(179)."\n");
		$cetak.=($nor.CHR(212).str_repeat(CHR(205),72).CHR(190).CHR(15)."\n");
    return $cetak;
	}
	function CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp){
    $cetak='';
		$cetak.=(CHR(18));
		$cetak.=($nor.str_repeat('-',73)."\n");
		$gro	= number_format($row->v_nota_gross);
		$pjg	= strlen($gro);
		$spcgro = 13;
		for($xx=1;$xx<=$pjg;$xx++){
			$spcgro= $spcgro-1;
		}
		$dis	= number_format($row->v_nota_discounttotal);
		$pjg	= strlen($dis);
		$spcdis = 13;
		for($xx=1;$xx<=$pjg;$xx++){
			$spcdis= $spcdis-1;
		}
		$tot	= number_format($row->v_nota_gross-$row->v_nota_discounttotal);
		$pjg	= strlen($tot);
		$spctot = 13;
		for($xx=1;$xx<=$pjg;$xx++){
			$spctot= $spctot-1;
		}
		$cetak.=($nor.str_repeat(" ",45)."TOTAL        : ".str_repeat(" ",$spcgro).number_format($row->v_nota_gross)."\n");
		$cetak.=($nor.str_repeat(" ",45)."POTONGAN     : ".str_repeat(" ",$spcdis).number_format($row->v_nota_discounttotal)."\n");
		$cetak.=($nor.str_repeat(' ',58)."--------------- -\n");
		$cetak.=($nor.str_repeat(" ",45)."NILAI        : ".str_repeat(" ",$spctot).number_format($row->v_nota_gross-$row->v_nota_discounttotal)."\n\n");
		$cetak.=($nor.str_repeat(' ',53)."Bandung, ".$row->d_sj."\n\n");
		$cetak.=($nor."  Penerima                      Mengetahui                    Pengirim    "."\n\n\n\n\n");
		$cetak.=($nor."(          )                 (            )                (            )\n\n");
		$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
		$cetak.=($nor.str_repeat(" ",25).CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(218).str_repeat(CHR(196),21).CHR(191)."\n");
		$cetak.=($nor.str_repeat(" ",25).CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(179)."S U R A T   J A L A N".CHR(179)."\n");
		$cetak.=($nor.str_repeat(" ",25).CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(179)."                     ".CHR(179)."\n");
		$cetak.=($nor.str_repeat(" ",25).CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(179)." BUKAN UNTUK TAGIHAN ".CHR(179)."\n");
		if($j>0){
			switch($j){
			case 1:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 2:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 3:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 4:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 5:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 6:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 7:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 8:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 9:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 10:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 11:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 12:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 13:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 14:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 15:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 16:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 17:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 18:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 19:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 20:
				$tm="\n\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 21:
				$tm="\n\n\n\n\n\n\n\n\n\n\n";
				break;
			case 22:
				$tm="\n\n\n\n\n\n\n\n\n\n";
				break;
			case 23:
				$tm="\n\n\n\n\n\n\n\n\n";
				break;
			case 24:
				$tm="\n\n\n\n\n\n\n\n";
				break;
			case 25:
				$tm="\n\n\n\n\n\n\n";
				break;
			case 26:
				$tm="\n\n\n\n\n\n";
				break;
			case 27:
				$tm="\n\n\n\n\n";
				break;
			case 28:
				$tm="\n\n\n\n";
				break;
			case 29:
				$tm="\n\n\n";
				break;
			case 30:
				$tm="\n\n";
				break;
			case 31:
				$tm="\n";
				break;
			case 32:
				$tm="";
				break;
			case 33:
				$tm="";
				break;
			case 34:
				$tm="";
				break;
			case 35:
				$tm="";
				break;
			case 36:
				$tm="";
				break;
			case 37:
				$tm="";
				break;
			}
		}			
		$cetak.=($nor.str_repeat(" ",25).CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(192).str_repeat(CHR(196),21).CHR(217).CHR(15).$tm.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0).CHR(18)."\n");
    return $cetak;
	}
?>
