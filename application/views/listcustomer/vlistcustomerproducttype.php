<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Dialogue Garmindo Utama : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listcustomer/cform/caricustomerproducttype','update'=>'#tmp','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="2" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Tipe Produk Pelanggan</th>
	    <th>Keterangan</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_customer_producttype','$row->e_customer_producttypename')\">$row->i_customer_producttype</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer_producttype','$row->e_customer_producttypename')\">$row->e_customer_producttypename</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    document.getElementById("icustomerproducttype").value=a;
    document.getElementById("ecustomerproducttypename").value=b;
    if(a=1){
      document.getElementById("icustomerspecialproduct").value="";
      document.getElementById("ecustomerspecialproductname").value="";
    }
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal()
  {
	jsDlgHide("#konten *","#fade","#light");
  }
</script>
