<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $i=0;
  $j=0;
  $cetak='';
  if($isi){
  	foreach($isi as $row){
	  $iic_convertion = $row->i_ic_convertion;
    if($row->d_ic_convertion){
      $tmp=explode('-',$row->d_ic_convertion);
      $tgl=$tmp[2];
      $bln=$tmp[1];
      $thn=$tmp[0];
      $row->d_ic_convertion=$tgl.'-'.$bln.'-'.$thn; 
    }
    if($row->d_refference!='' or $row->d_refference!=null){
      $tmp=explode('-',$row->d_refference);
      $tgl=$tmp[2];
      $bln=$tmp[1];
      $thn=$tmp[0];
      $row->d_refference=$tgl.'-'.$bln.'-'.$thn;
    }else{
      $row->d_refference='';
    }
		  $nor	= str_repeat(" ",5);
		  $abn	= str_repeat(" ",12);
		  $ab		= str_repeat(" ",9);
		  $ac		= str_repeat(" ",27);
		  $ad		= str_repeat(" ",28);
		  $ae		= str_repeat(" ",55);
		  $hal	= 1;
		  $ipp  = new PrintIPP();
		  $ipp->setHost($host);
		  $ipp->setPrinterURI($uri);
		  $ipp->setRawText();
		  $ipp->unsetFormFeed();
		  $cetak=CHR(18);
		  $cetak.=$nor.CHR(27).CHR(71).NmPerusahaan.CHR(27).CHR(72)."\n\n";
		  $cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14)."KONVERSI STOK".CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n";
		  $cetak.=$nor."No Konversi   : ".$row->i_ic_convertion.CHR(18)."\n";
		  $cetak.=$nor."Tanggal       : ".$row->d_ic_convertion.CHR(18)."\n";
		  $cetak.=$nor."Referensi     : ".$row->i_refference.'/'.$row->d_refference.CHR(18)."\n";
		  $cetak.=CHR(15).$ab.str_repeat(" ",119)."Hal: ".$hal."\n";		
		  $cetak.=CHR(15).$ab.CHR(18).$ac."*** Produk Asal ***"."\n";
		  $cetak.=CHR(15).$ab.CHR(218).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),67).CHR(194).str_repeat(CHR(196),15).CHR(194).str_repeat(CHR(196),14).CHR(191)."\n";		
		  $cetak.=$ab.CHR(179)."  NO. ".CHR(179)."     KODE BARANG     ".CHR(179).$ad."NAMA BARANG".$ad.CHR(179)."     GRADE     ".CHR(179)."    JUMLAH    ".CHR(179)."\n";
		  $cetak.=$ab.CHR(198).str_repeat(CHR(205),6).CHR(216).str_repeat(CHR(205),21).CHR(216).str_repeat(CHR(205),67).CHR(216).str_repeat(CHR(205),15).CHR(216).str_repeat(CHR(205),14).CHR(181)."\n";
		  
      if($row->f_ic_convertion=='t'){
      $j=1;
      $kodeasal = $row->i_product;
      $asal = $row->e_product_name;
			if(strlen($row->e_product_name )>45){
				$namaasal	= substr($row->e_product_name,0,45);
			}else{
				$namaasal	= $row->e_product_name.str_repeat(" ",45-strlen($row->e_product_name ));
			}
			$pjg	= strlen($namaasal);
			$spcnamaasal	= 67;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcnamaasal	= $spcnamaasal-1;
			}
      $gradeasal = $row->i_product_grade;
      $jmlasal = number_format($row->n_ic_convertion);
			$pjg	= strlen($jmlasal);
			$spcjmlasal	= 14;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcjmlasal	= $spcjmlasal-1;
			}
		  $cetak.=$ab.CHR(179)."1".str_repeat(" ",5).CHR(179).$kodeasal.str_repeat(" ",14).CHR(179).$namaasal.str_repeat(" ",$spcnamaasal).CHR(179).$gradeasal.str_repeat(" ",14).CHR(179).str_repeat(" ",$spcjmlasal).$jmlasal.CHR(179)."\n";
		  $cetak.=$ab.CHR(192).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),67).CHR(193).str_repeat(CHR(196),15).CHR(193).str_repeat(CHR(196),14).CHR(217).CHR(18)."\n\n";
		  $cetak.=CHR(15).$ab.CHR(18).$ac."*** Produk Jadi ***"."\n";
		  $cetak.=CHR(15).$ab.CHR(218).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),67).CHR(194).str_repeat(CHR(196),15).CHR(194).str_repeat(CHR(196),14).CHR(191)."\n";		
		  $cetak.=$ab.CHR(179)."  NO. ".CHR(179)."     KODE BARANG     ".CHR(179).$ad."NAMA BARANG".$ad.CHR(179)."     GRADE     ".CHR(179)."    JUMLAH    ".CHR(179)."\n";
		  $cetak.=$ab.CHR(198).str_repeat(CHR(205),6).CHR(216).str_repeat(CHR(205),21).CHR(216).str_repeat(CHR(205),67).CHR(216).str_repeat(CHR(205),15).CHR(216).str_repeat(CHR(205),14).CHR(181)."\n";
		  
		  $detail	= $this->mmaster->bacadetail($row->i_ic_convertion);
      foreach($detail as $rowi){
      	$i++;
			  $aw		= 6;
			  $pjg	= strlen($i);
			  for($xx=1;$xx<=$pjg;$xx++){
				  $aw=$aw-1;
			  }
			  $aw=str_repeat(" ",$aw);
        $kodejadi = $rowi->i_product;
        $jadi = $rowi->e_product_name;
			  if(strlen($rowi->e_product_name )>45){
				  $namajadi	= substr($rowi->e_product_name,0,45);
			  }else{
				  $namajadi	= $rowi->e_product_name.str_repeat(" ",45-strlen($rowi->e_product_name ));
			  }
			  $pjg	= strlen($namajadi);
			  $spcnamajadi	= 67;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcnamajadi	= $spcnamajadi-1;
			  }
        $gradejadi = $rowi->i_product_grade;
        $jmljadi = number_format($rowi->n_ic_convertion);
			  $pjg	= strlen($jmljadi);
			  $spcjmljadi	= 14;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcjmljadi	= $spcjmljadi-1;
  			  }
			  }
		  
		  $cetak.=$ab.CHR(179)."1".str_repeat(" ",5).CHR(179).$kodejadi.str_repeat(" ",14).CHR(179).$namajadi.str_repeat(" ",$spcnamajadi).CHR(179).$gradejadi.str_repeat(" ",14).CHR(179).str_repeat(" ",$spcjmljadi).$jmljadi.CHR(179)."\n";
		  $cetak.=$ab.CHR(192).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),67).CHR(193).str_repeat(CHR(196),15).CHR(193).str_repeat(CHR(196),14).CHR(217).CHR(18)."\n\n";
      }
      
      elseif($row->f_ic_convertion=='f'){
		  $detail	= $this->mmaster->bacadetail($row->i_ic_convertion);
      foreach($detail as $rowi){
      	$i++;
      	$j++;
			  $aw		= 6;
			  $pjg	= strlen($i);
			  for($xx=1;$xx<=$pjg;$xx++){
				  $aw=$aw-1;
			  }
			  $aw=str_repeat(" ",$aw);
        $kodejadi = $rowi->i_product;
        $jadi = $rowi->e_product_name;
			  if(strlen($rowi->e_product_name )>45){
				  $namajadi	= substr($rowi->e_product_name,0,45);
			  }else{
				  $namajadi	= $rowi->e_product_name.str_repeat(" ",45-strlen($rowi->e_product_name ));
			  }
			  $pjg	= strlen($namajadi);
			  $spcnamajadi	= 67;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcnamajadi	= $spcnamajadi-1;
			  }
        $gradejadi = $rowi->i_product_grade;
        $jmljadi = number_format($rowi->n_ic_convertion);
			  $pjg	= strlen($jmljadi);
			  $spcjmljadi	= 14;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcjmljadi	= $spcjmljadi-1;
			  }
		    $cetak.=CHR(15).$ab.CHR(179).$i.$aw.CHR(179).$kodejadi.str_repeat(" ",14).CHR(179).$namajadi.str_repeat(" ",$spcnamajadi).CHR(179).$gradejadi.str_repeat(" ",14).CHR(179).str_repeat(" ",$spcjmljadi).$jmljadi.CHR(179)."\n";

			  if($jmldatajadi>6){
				  if(($i%18)==0){
      		  $cetak.=$ab.CHR(192).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),67).CHR(193).str_repeat(CHR(196),15).CHR(193).str_repeat(CHR(196),14).CHR(217).CHR(18)."\n";
					  $cetak.=$ab.str_repeat(" ",84)."bersambung ......."."\n\n";
					  $hal=$hal+1;
					  $cetak.=CetakHeader($row,$hal,$nor,$abn,$ab,$host,$uri,$ipp,$cetak);
					  $j	= 0;
				  }elseif(
					  (($i<18)&&($i==$jmldatajadi)) 
					  ){
      		  $cetak.=$ab.CHR(192).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),67).CHR(193).str_repeat(CHR(196),15).CHR(193).str_repeat(CHR(196),14).CHR(217).CHR(18)."\n";				
					  $zz="\n\n\n";
#					  for($yy=14;$yy!=$i;$yy--){
					  for($yy=18;$yy!=$i;$yy--){
						  $zz=$zz."\n";
					  }
					  $cetak.=$ab.str_repeat(" ",84)."bersambung .......".$zz;							
					  $hal=$hal+1;
					  $cetak.=CetakHeader($row,$hal,$nor,$abn,$ab,$host,$uri,$ipp,$cetak);
					  $j	= 0;
				  }
			  }
      }
      $kodeasal = $row->i_product;
      $asal = $row->e_product_name;
			if(strlen($row->e_product_name )>45){
				$namaasal	= substr($row->e_product_name,0,45);
			}else{
				$namaasal	= $row->e_product_name.str_repeat(" ",45-strlen($row->e_product_name ));
			}
			$pjg	= strlen($namaasal);
			$spcnamaasal	= 67;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcnamaasal	= $spcnamaasal-1;
			}
      $gradeasal = $row->i_product_grade;
      $jmlasal = number_format($row->n_ic_convertion);
			$pjg	= strlen($jmlasal);
			$spcjmlasal	= 14;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcjmlasal	= $spcjmlasal-1;
			}
		  $cetak.=CHR(15).$ab.CHR(18).$ac."*** Produk Jadi ***"."\n";
		  $cetak.=CHR(15).$ab.CHR(218).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),67).CHR(194).str_repeat(CHR(196),15).CHR(194).str_repeat(CHR(196),14).CHR(191)."\n";		
		  $cetak.=$ab.CHR(179)."  NO. ".CHR(179)."     KODE BARANG     ".CHR(179).$ad."NAMA BARANG".$ad.CHR(179)."     GRADE     ".CHR(179)."    JUMLAH    ".CHR(179)."\n";
		  $cetak.=$ab.CHR(198).str_repeat(CHR(205),6).CHR(216).str_repeat(CHR(205),21).CHR(216).str_repeat(CHR(205),67).CHR(216).str_repeat(CHR(205),15).CHR(216).str_repeat(CHR(205),14).CHR(181)."\n";
		  $cetak.=$ab.CHR(179)."1".str_repeat(" ",5).CHR(179).$kodeasal.str_repeat(" ",14).CHR(179).$namaasal.str_repeat(" ",$spcnamaasal).CHR(179).$gradeasal.str_repeat(" ",14).CHR(179).str_repeat(" ",$spcjmlasal).$jmlasal.CHR(179)."\n";
		  $cetak.=$ab.CHR(192).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),21).CHR(193).str_repeat(CHR(196),67).CHR(193).str_repeat(CHR(196),15).CHR(193).str_repeat(CHR(196),14).CHR(217).CHR(18)."\n\n";
		  }
      
    if($j>0){
			switch($j){
			case 1:
				$spc="\n\n\n\n\n\n\n\n\n";
				break;
			case 2:
				$spc="\n\n\n\n\n\n\n\n";
				break;
			case 3:
				$spc="\n\n\n\n\n\n\n";
				break;
			case 4:
				$spc="\n\n\n\n\n\n";
				break;
			case 5:
				$spc="\n\n\n\n\n";
				break;
		  case 6:
		    $spc="\n\n\n\n";
		    break;
			}		
		}
  
	    $cetak.=CHR(15).$ab.$ae.str_repeat(" ",4)."Tanda  Terima".str_repeat(" ",4).str_repeat(" ",5).str_repeat(" ",5)."Mengetahui".str_repeat(" ",5).str_repeat(" ",5).str_repeat(" ",4)."Hormat  Kami".str_repeat(" ",4)."\n\n\n\n";
	    $cetak.=CHR(15).$ab.$ae."(".str_repeat("_",18).")".str_repeat(" ",5)."(".str_repeat("_",18).")".str_repeat(" ",5)."(".str_repeat("_",18).")".$spc;
	    }
  $ipp->setdata($cetak);
  $ipp->printJob();
  echo "<script>this.close();</script>";
  }

function CetakHeader($row,$hal,$nor,$abn,$ab,$host,$uri,$ipp){
		$ipp->unsetFormFeed();
    $cetek=CHR(18);
	  $cetek.=$nor.CHR(27).CHR(71).NmPerusahaan.CHR(27).CHR(72)."\n\n";
	  $cetek.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14)."KONVERSI STOK".CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n";
	  $cetek.=$nor."No Konversi   : ".$row->i_ic_convertion.CHR(18)."\n";
	  $cetek.=$nor."Tanggal       : ".$row->d_ic_convertion.CHR(18)."\n";
	  $cetek.=$nor."Referensi     : ".$row->i_refference.'/'.$row->d_refference.CHR(18)."\n";
	  $cetek.=CHR(15).$ab.str_repeat(" ",119)."Hal: ".$hal."\n";		
	  $cetek.=CHR(15).$ab.CHR(18).$ac."*** Produk Asal ***"."\n";
	  $cetek.=CHR(15).$ab.CHR(218).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),21).CHR(194).str_repeat(CHR(196),67).CHR(194).str_repeat(CHR(196),15).CHR(194).str_repeat(CHR(196),14).CHR(191)."\n";		
	  $cetek.=$ab.CHR(179)."  NO. ".CHR(179)."     KODE BARANG     ".CHR(179).$ad."NAMA BARANG".$ad.CHR(179)."     GRADE     ".CHR(179)."    JUMLAH    ".CHR(179)."\n";

	  $cetek.=$ab.CHR(198).str_repeat(CHR(205),6).CHR(216).str_repeat(CHR(205),21).CHR(216).str_repeat(CHR(205),67).CHR(216).str_repeat(CHR(205),15).CHR(216).str_repeat(CHR(205),14).CHR(181)."\n";
    return $cetek;
	}
