<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	$nor	= str_repeat(" ",5);
	$abn	= str_repeat(" ",12);
	$ab		= str_repeat(" ",9);
	$hal	= 1;
	$ipp  = new PrintIPP();
	$ipp->setHost($host);
	$ipp->setPrinterURI($uri);
	$ipp->setRawText();
	$ipp->unsetFormFeed();
	foreach($isi as $row){
		$xmp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
		$ymp='';
		$i	= 0;
		$j	= 0;
		$hrg    = 0;
		$ibbm	= $row->i_bbm;
		$query 	= $this->db->query(" select * from tm_bbm_item where i_bbm='$ibbm' and i_bbm_type='05' order by n_item_no",false);
		$jml 	= $query->num_rows();
		foreach($detail as $rowi){
			$i++;
			$j++;
			$pro	= $rowi->i_product;
      if(strlen($rowi->e_product_name )>34){
        $nam	= substr($rowi->e_product_name,0,34);
      }else{
        $nam	= $rowi->e_product_name.str_repeat(" ",34-strlen($rowi->e_product_name ));
      }			
      $ket="                    ";
      $del	= number_format($rowi->n_quantity);
      $pjg	= strlen($del);
      $spcdel	= 6;
      for($xx=1;$xx<=$pjg;$xx++){
        $spcdel	= $spcdel-1;
      }
      $aw	= 3;
      $pjg	= strlen($i);
      for($xx=1;$xx<=$pjg;$xx++){
        $aw=$aw-1;
      }
      $aw=str_repeat(" ",$aw);
      $cetak.=$nor.CHR(179).$aw.$i." ".CHR(179).$pro."  ".$nam.CHR(179).str_repeat(" ",$spcdel).$del." ".CHR(179).$ket.CHR(179)."\n";
			if($jml>11){
				if(($i%15)==0){
          #$cetak.=CHR(18).$nor.str_repeat('-',73)."\n";
          $cetak.=$ab.CHR(179).str_repeat(CHR(196),5).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),11).CHR(193).str_repeat(CHR(196),46).CHR(193).str_repeat(CHR(196),44).CHR(217)."\n";
					$cetak.=$nor.str_repeat(" ",52)."bersambung ......."."\n\n\n";				  		
				  $hal=$hal+1;
				  $ymp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
				  $j	= 0;
				} elseif(
					(($i<15)&&($i==$jml)) 
					){
					$cetak.=$nor.CHR(179).str_repeat(CHR(196),5).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),11).CHR(193).str_repeat(CHR(196),46).CHR(193).str_repeat(CHR(196),44).CHR(217)."\n";					
					$zz="\n\n\n\n\n\n\n";
					for($yy=17;$yy!=$i;$yy--){
						$zz=$zz."\n";
					}
					$cetak.=$nor.str_repeat(" ",52)."bersambung .......".$zz;							
					$hal=$hal+1;
					$ymp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
					$j	= 0;
				}
      }
      if($j>0){
			  switch($j){
			   case 1:
				  $tm="\n\n\n\n\n\n\n\n\n\n\n\n\n";
				  break;
			  case 2:
				  $tm="\n\n\n\n\n\n\n\n\n\n\n\n";
				  break;
			  case 3:
				  $tm="\n\n\n\n\n\n\n\n\n\n\n";
				  break;
			  case 4:
				  $tm="\n\n\n\n\n\n\n\n\n\n";
				  break;
			  case 5:
				  $tm="\n\n\n\n\n\n\n\n\n";
				  break;
			  case 6:
				  $tm="\n\n\n\n\n\n\n\n";
				  break;
			  case 7:
				  $tm="\n\n\n\n\n\n\n";
				  break;
			  case 8:
				  $tm="\n\n\n\n\n\n";
				  break;
			  case 9:
				  $tm="\n\n\n\n\n";
				  break;
			  case 10:
				  $tm="\n\n\n\n";
				  break;

			  }	
		  }
    }
		$cetak=$xmp.$cetak.$ymp;
		$zmp=CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp);
		$cetak=$cetak.$zmp.$tm;
	}
#	$ipp->setBinary();
#  echo $cetak;
#  $ipp->setFormFeed();
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";
	function CetakHeader($row,$hal,$nor,$abn,$ab,$ipp){
		$ipp->unsetFormFeed();
		$cetak=CHR(18);		
		$tmp=explode("-",$row->d_bbm);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dbbm=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$cetak.=$nor.NmPerusahaan."\n";		
		$cetak.=$nor."BUKTI BARANG MASUK                                 No. : ".$row->i_bbm."\n";		
		$cetak.=$nor."( B B M ) - RETUR                              Tgl.: ".$dbbm."\n";		
		$cetak.=$nor."Telah diterima dari : ".$row->i_customer." - ".$row->e_customer_name."\n\n";	
		$cetak.=$nor."Referensi  : ".$row->e_remark."\n";
		$cetak.=$nor."Keterangan : ".$row->i_refference_document."\n";
		$cetak.=$nor.str_repeat(" ",65)."Hal: ".$hal."\n";		
    $cetak.=$nor.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),43).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),20).CHR(191)."\n";
		$cetak.=$nor.CHR(179)."NO. ".CHR(179)." KODE                                      ".CHR(179)."JUMLAH ".CHR(179)." KETERANGAN         ".CHR(179)."\n";
		$cetak.=$nor.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G      ".CHR(179)."       ".CHR(179)."                    ".CHR(179)."\n";
		$cetak.=$nor.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),43).CHR(216).str_repeat(CHR(205),7).CHR(216).str_repeat(CHR(205),20).CHR(181)."\n";
    return $cetak;
	}
	function CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp){
		$ipp->unsetFormFeed();
#		$cetak =CHR(18);
		$cetak=$nor.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),43).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),20).CHR(217)."\n";
    $tmp=explode("-",$row->d_bbm);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dbbm=$hr." ".substr(mbulan($bl),0,3)." ".$th;
    $cetak.=$nor.str_repeat(' ',52)."Bandung, ".$dbbm."\n";
    $cetak.=$nor." Penerima        Mengetahui                              Pengirim    \n\n\n\n";
    $cetak.=$nor."(          )    (           )                        (              )\n".CHR(15);
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $cetak.=$ab."TANGGAL CETAK : ".$tgl.CHR(18);
	  $tm="\n\n\n\n\n\n\n\n\n\n\n";
		return $cetak;		
	}
?>
