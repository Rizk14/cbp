<?php 
	$totjum = 0;
	$totsisa = 0;
	if($total){
		foreach ($total as $row) {
			$totjum = $totjum + $row->v_jumlah;
			$totsisa = $totsisa + $row->v_sisa;
		}
	}
?>
<h2><?php echo $page_title; ?></h2>
<?php echo "<center><h3>Periode $dfrom s/d $dto</h3></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'list-akt-meterai/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
		<div class="accordion2">
			<table class="listtable">
				<thead>
					<tr>
						<td colspan="10" align="center">Cari data : 
							<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" placeholder = "No Alokasi / Kd Pelanggan">
							<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" >
							<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" >
							<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;
							<input type="submit" id="bcari" name="bcari" value="Cari">
						</td>
					</tr>
	    		</thead>

				<th>NO ALOKASI</th>
				<th>TGL ALOKASI</th>
				<th>AREA</th>
				<th>KD PELANGGAN</th>
				<th>NAMA PELANGGAN</th>
				<th>NILAI</th>
				<th>SISA</th>
				<th>REFERENSI ALOKASI</th>
				<th class="action">ACTION</th>
	    		
				<tbody>
	      		<?php 
					if($isi){
						$brs=0;
						$tmpjumlah = 0;
						$tmpsisa = 0;
						$periode = $this->db->query("select i_periode from tm_periode")->row()->i_periode;

						foreach($isi as $row){
							if($row->f_alokasi_cancel == 'f'){
								$tmpjumlah = $tmpjumlah + $row->v_jumlah;
								$tmpsisa = $tmpsisa + $row->v_sisa;
							}
							
							$tmp	= explode('-',$row->d_alokasi);
							$tgl	= $tmp[2];
							$bln	= $tmp[1];
							$thn	= $tmp[0];
							$row->d_alokasi = $tgl.'-'.$bln.'-'.$thn;
							$thnbln = $thn.$bln;

							echo "<tr>";
							if($row->f_alokasi_cancel=='t'){
								echo "<td><h1>$row->i_alokasi</h1></td>";
							}else{
								echo "<td>$row->i_alokasi</td>";
							}
							echo "	<td>$row->d_alokasi</td>
									<td>$row->e_area_name</td>
									<td>".strval($row->i_customer)."</td>
									<td>$row->e_customer_name</td>
									<td align=right>".number_format($row->v_jumlah)."</td>
									<td align=right>".number_format($row->v_sisa)."</td>
									<td>$row->i_alokasi_reff</td>
									<td class=\"action\">";
									if($row->f_alokasi_cancel=='f'){
										if(substr($row->i_alokasi,0,2)=="MT"){
											echo "<a href=\"#\" onclick='show(\"akt-bankin-mt/cform/edit/$row->i_alokasi/$row->i_kbank/$row->i_area/$row->i_coa_bank/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
										}else if(substr($row->i_alokasi,0,2)=="AM"){
											echo "<a href=\"#\" onclick='show(\"akt-meterai/cform/edit/$row->i_alokasi/$row->i_area/$dfrom/$dto/$iarea\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
										}
										if($periode <= $thnbln) echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-meterai/cform/delete/$row->i_alokasi/$row->i_area/$dfrom/$dto/$iarea/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
									}
							echo "</td></tr>";
						}

						echo "	<tr>
									<td colspan='5'><b>Total Per Halaman</b></td>
									<td align=right>".number_format($tmpjumlah)."</td>
									<td align=right>".number_format($tmpsisa)."</td>
									<td colspan='2'></td>
								</tr>";
						echo "	<tr>
									<td colspan='5'><b>Total Semua</b></td>
									<td align=right>".number_format($totjum)."</td>
									<td align=right>".number_format($totsisa)."</td>
									<td colspan='2'></td>
								</tr>";
					}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
	  <marquee direction="left" scrolldelay="120" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 6, 0);">*Jika Nomor Alokasi Membesar Maka Nomor Alokasi Tersebut Telah Di Batalkan.</marquee>
      </div>
	  <input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('list-akt-meterai/cform/','#tmpx')">
      <?=form_close()?>
    </td>
  </tr>
</table>
