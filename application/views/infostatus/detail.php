<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<?php echo "<h2>$page_title</h2>"; ?>
<h3 class="kepala">Informasi Status</h3>
<h3 class="kepala2">Periode <?php echo $dfrom." s/d ".$dto ?> </h3>
<table class="maintable">
  <tr>
    <td align="left">
      <!--	<?php echo form_open('infostatusgudang/cform/cari', array('id' => 'listform'));?> -->
      <?php echo $this->pquery->form_remote_tag(array('url'=>'infostatusgudang/cform/cari','update'=>'#main','type'=>'post'));?>
      <div id="listform">
      <div class="effect">
      <div class="accordion2">
      <table class="listtable">
        <thead>
        </thead>
        <th>Status</th>
        <th>Kode Produk</th>
        <th>Nama Produk</th>
        <th>Qty</th>
        <?php 
          if($isi){
            $total=0;
            $grandtotal=0;
            $judul='';
            $ada=false;
            foreach($isi as $row){
              $stat=$row->stat;
              $kode=$row->i_product;
              $nama=$row->e_product_name;
              $qty=$row->qty;
              if( ($judul!='') && ($judul!=$stat) ){
                $ada=true;
                echo "<tr><th colspan=3><b>Total ".$judul."</th><th align=right><b>".number_format($total)."</th></tr>";
                $grandtotal=$grandtotal+$total;
                $total=0;
                ##########
                $total=$total+$qty;
                echo "<tr><td>$stat</td><td>$kode</td><td>$nama</td><td align=right>".number_format($qty)."</td></tr>";
              }else{
                $total=$total+$qty;
                echo "<tr><td>$stat</td><td>$kode</td><td>$nama</td><td align=right>".number_format($qty)."</td></tr>";
              }
              $judul=$stat;
            }
            
            echo "<tr><th colspan=3><b>Total ".$judul."</th><th align=right><b>".number_format($total)."</th></tr>";
            $grandtotal=$grandtotal+$total;
            if((!$ada)&&(($judul=='Gudang Cabang')||($judul=='Sales cabang')||($judul=='Keuangan Cabang'))){
              echo "<th colspan=3 align=left>Total Pusat</th>";
              echo "<th align=right>0</th>";
            }else{
              if((!$ada)&&(($judul=='Gudang Pusat')||($judul=='Sales Pusat')||($judul=='Keuangan Pusat'))){
                echo "<th colspan=3 align=left>Total Cabang</th>";
                echo "<th align=right>0</th>";
              }
            }
            echo "<tr><th colspan=3><b>Grand Total</th><th align=right><b>".number_format($grandtotal)."</th></tr>";
          }else{
            echo "<tr><th colspan=3><b>Total Pusat</th><th align=right><b>0</th></tr>";
            echo "<tr><th colspan=3><b>Total Cabang</th><th align=right><b>0</th></tr>";
            echo "<tr><th colspan=3><b>Grand Total</th><th align=right><b>0</th></tr>";
          }	
        ?>
      </table>
      <?php echo "<center>".$this->pagination->create_links()."</center>";?>
      </div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
function yyy(a,b,c){
document.getElementById("dfrom").value=a;
document.getElementById("dto").value=b;
document.getElementById("iarea").value=c;
formna=document.getElementById("listform");
formna.action="<?php echo site_url(); ?>"+"/infostatusgudang/cform/viewdetail";
formna.submit();
}
</script>
