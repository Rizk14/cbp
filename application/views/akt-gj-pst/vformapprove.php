<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-gj-pst/cform/posting','update'=>'#pesan','type'=>'post'));?>
	<div id="gjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Jurnal</td>
		<td><input readonly id="ijurnal" name="ijurnal" value="<?php echo $isi->i_jurnal; ?>"></td>
		  </tr>
			<?php 
				$tmp=explode('-',$isi->d_jurnal);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$isi->d_jurnal=$tgl.'-'.$bln.'-'.$thn;
			?>
	      <tr>
		<td width="10%">Tanggal</td>
		<td><input readonly id="djurnal" name="djurnal" value="<?php echo $isi->d_jurnal; ?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Keterangan</td>
		<td><input readonly id="edescription" name="edescription" value="<?php echo $isi->e_description;?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Debet</td>
		<td><input style="text-align:right;" readonly id="vdebet" name="vdebet" value="<?php echo number_format($isi->v_debet);?>"></td>
	      </tr>
		  <tr>
		<td width="10%">Kredit</td>
		<td><input style="text-align:right;" readonly id="vkredit" name="vkredit" value="<?php echo number_format($isi->v_kredit);?>"></td>
	      </tr>
		<tr>
		  <td width="10%">&nbsp;</td>
		  <td>
		    <input name="login" id="login" value="Posting" type="submit" 
			   onclick="dipales();">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="reset" onclick="show('akt-gj-pst/cform/','#main');"></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:100px;" align="center">CoA</th>
					<th style="width:350px;" align="center">Keterangan</th>
					<th style="width:130px;" align="center">Debet</th>
					<th style="width:130px;" align="center">Kredit</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$debet	=number_format($row->v_mutasi_debet);
					$kredit	=number_format($row->v_mutasi_kredit);
				  	echo "<table class=\"listtable\" align=\"center\" style=\"width: 750px;\"><tbody>
							<tr>
		    				<td style=\"width:100px;\">
								<input style=\"width:101px;\" type=\"text\" 
								id=\"icoa$i\" name=\"icoa$i\" value=\"$row->i_coa\">
								<input type=\"hidden\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
							<td style=\"width:350px;\"><input style=\"width:359px;\" readonly type=\"text\" 
								id=\"ecoaname$i\" name=\"ecoaname$i\" value=\"$row->e_coa_name\"></td>
							<td style=\"width:130px;\"><input style=\"width:132px;text-align:right;\" type=\"text\" 
								id=\"vdebet$i\" name=\"vdebet$i\" value=\"$debet\" readonly></td>
							<td style=\"width:130px;\"><input style=\"width:132px;text-align:right;\" type=\"text\" 
								id=\"vkredit$i\" name=\"vkredit$i\" value=\"$kredit\" readonly></td>
							</tr>
						  </tbody></table>";
				}
				?>			
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem;?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
	document.getElementById("login").disabled=true;
  }
</script>
