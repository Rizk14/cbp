<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<table class="maintable" id="sitabel">
  <tr>
    <td colspan="1">
<?php echo "<center><h2>$page_title ($icustomergroupar-$ecustomername) per Jatuhtempo $djt</h2></center>"; ?>
    </td>
  </tr>
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listagingpiutangnasional/cform/detail','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
        <center>
              <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
        </center>
    	  <table class="listtable">
      <th>No</th>
      <th>kodelang</th>
	    <th>Nota</th>
	    <th>Tgl Nota</th>
	    <th>Tgl JT</th>
	    <th>Jumlah</th>
	    <th>Sisa</th>
	    <th>Lama</th>
	    <th>TOP</th>
	    <tbody>
	      <?php 
		if($isi){
      $no=0;
#####
      $tmp 	= explode("-", $djt);
      $det	= $tmp[0];
      $mon	= $tmp[1];
      $yir 	= $tmp[2];
      $djt  = $yir."-".$mon."-".$det;
      $pecah1 = explode("-", $djt);
      $date1 = $pecah1[2];
      $month1 = $pecah1[1];
      $year1 = $pecah1[0];
      $jd1 = GregorianToJD($month1, $date1, $year1);
#####
			foreach($isi as $row){
        $tgl2 = $row->d_jatuh_tempo;
        $pecah2 = explode("-", $tgl2);
        $date2 = $pecah2[2];
        $month2 = $pecah2[1];
        $year2 =  $pecah2[0];
        $jd2 = GregorianToJD($month2, $date2, $year2);
        $lama = $jd1 - $jd2;
        $tmp 	= explode("-", $row->d_nota);
        $det	= $tmp[2];
        $mon	= $tmp[1];
        $yir 	= $tmp[0];
        $row->d_nota  = $det."-".$mon."-".$yir;
        $tmp 	= explode("-", $row->d_jatuh_tempo);
        $det	= $tmp[2];
        $mon	= $tmp[1];
        $yir 	= $tmp[0];
        $row->d_jatuh_tempo  = $det."-".$mon."-".$yir;
        $no++;
			  echo "<tr> 
				  <td>$no</td>
				  <td>$row->i_customer</td>
				  <td>$row->i_nota</td>
				  <td>$row->d_nota</td>
				  <td>$row->d_jatuh_tempo</td>
				  <td>".number_format($row->v_nota_netto)."</td>
				  <td>".number_format($row->v_sisa)."</td>
				  <td>$lama</td>
				  <td>$row->n_nota_toplength</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center>
      <input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()">
    </center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function bbatal(){
  	jsDlgHide("#konten *", "#fade", "#light");
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
