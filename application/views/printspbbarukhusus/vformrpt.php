<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: potrait;}
*/
*{
size: potrait;
}

@page { size: Letter; margin: 0.10in 0.50in 0.50in 0.50in;}


.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 40px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 18px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 18px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($lang as $row)
{
?>
  <table width="100%" border="0" class="nmper">
    <tr>
      <td colspan="8" class="huruf judul" align="center" >DATA PELANGGAN</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Kode Pelanggan</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->i_customer;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper" >Contact Person</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_contact; ?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Nama Pelanggan</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_name;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">non PKP</td>
      <td class="huruf nmper"></td>
      <td class="huruf nmper"></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Alamat</td>
      <td>:</td>
      <td ><?php echo $row->e_customer_address; ?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Kodepos</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_postal1;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">Alamat Pemilik </td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_owneraddress;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">No.Telepon </td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_phone;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">&nbsp;</td>
      <td class="huruf nmper">&nbsp;</td>
      <td class="huruf nmper">&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Fax</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_fax1;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">NPWP</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_pkpnpwp;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="10%">E-mail</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_mail;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">No.Telepon</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_ownerphone;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Kode Daerah</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->i_area."-".$row->e_area_name;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper">Tipe Pelanggan</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_classname;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="15%">Nomor SPB</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->i_spb;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
	  <td class="huruf nmper" width="15%">Tanggal</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php 
        		$tmp=explode("-",$row->d_spb);
		        $th=$tmp[0];
		        $bl=$tmp[1];
		        $hr=$tmp[2];
		        $dspb=$hr." ".mbulan($bl)." ".$th;
            echo $dspb; ?></td>
      <td width="3%" class="huruf nmper"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
<br>
<table width="1147">
  <tr>
    <td colspan=7 class=garisbawah>&nbsp;</td>
  </tr>
  <tr>
    <td width="50" class="huruf nmper">No Urut</td>
    <td width="125" class="huruf nmper">Kode Barang</td>
    <td width="420" class="huruf nmper">Nama Barang</td>
    <td width="117" class="huruf nmper">Banyak yg dipesan</td>
    <td width="103" class="huruf nmper">Harga Satuan</td>
    <td width="104" class="huruf nmper">Banyak yg dipenuhi</td>
    <td width="126" class="huruf nmper">Motif</td>
  </tr>
    <tr>
    <td colspan=7 class=garisbawah>&nbsp;</td>
  </tr>
</table>
<?php 
		$i=0;	
				foreach($detail as $rowi){
			$i++;
			$hrg=number_format($rowi->v_unit_price);
			$prod	= $rowi->i_product;
      if($rowi->i_product_status=='4') $rowi->e_product_name='* '.$rowi->e_product_name;
			$name	= $rowi->e_product_name.str_repeat(" ",/*46-*/strlen($rowi->e_product_name ));
			$motif	= $rowi->e_remark;
			$orde	= number_format($rowi->n_order);
			$deli	= number_format($rowi->n_deliver);
			$aw		= 9;
			$pjg	= strlen($i);
			for($xx=1;$xx<=$pjg;$xx++){
				$aw=$aw-1;
			}
			$pjg	= strlen($orde);
			$spcord	= 4;			
			for($xx=1;$xx<=$pjg;$xx++){
				$spcord	= $spcord-1;
			}
			$pjg	= strlen($hrg);
			$spcprc	= 13;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcprc	= $spcprc-1;
			}
?>
<table width="1147">
  <tr>
    <td width="50" class="huruf nmper" align="center"><?php echo $i; ?></td>
    <td width="125" class="huruf nmper"><?php echo $prod; ?></td>
    <td width="420" class="huruf nmper"><?php echo $name; ?></td>
    <td width="117" class="huruf nmper" align="center"><?php echo $orde; ?></td>
    <td width="103" class="huruf nmper"><?php echo $hrg; ?></td>
    <td width="104" class="huruf nmper">__________</td>
    <td width="126" class="huruf nmper"><?php echo $motif; ?></td>
  </tr>
</table>
<?php 
    }
		  $kotor=$row->v_spb;
		  $kotor=number_format($kotor);
		  $pjg=strlen($kotor);
		  $spckot=14;
		  for($xx=1;$xx<=$pjg;$xx++){
			  $spckot=$spckot-1;
		  }
		  $spckot=str_repeat(" ",$spckot);

      $nNDisc      = $row->n_spb_discount1 + $row->n_spb_discount2*(100-$row->n_spb_discount1)/100;
      $nNDisc0     = $row->n_spb_discount3 + $row->n_spb_discount4*(100-$row->n_spb_discount3)/100;
      $dis      = $nNDisc + (100-$nNDisc)*$nNDisc0/100;
		  $dis	= number_format($dis,2);
		  $pjg	= strlen($dis);
		  $spcdis	= 6;
		  for($xx=1;$xx<=$pjg;$xx++){
			  $spcdis	= $spcdis-1;
		  }
		  $spcdis=str_repeat(" ",$spcdis);
		  $vdis	= number_format($row->v_spb_discounttotal);
		  $pjg	= strlen($vdis);
		  $spcvdis	= 14;
		  for($xx=1;$xx<=$pjg;$xx++){
			  $spcvdis = $spcvdis-1;
		  }
		  $spcvdis=str_repeat(" ",$spcvdis);

		  $nb	= number_format($row->v_spb-$row->v_spb_discounttotal);
		  $pjg=strlen($nb);
		  $spcnb=14;
		  for($xx=1;$xx<=$pjg;$xx++){
			  $spcnb=$spcnb-1;
		  }
		  $spcnb=str_repeat(" ",$spcnb);
?>
<table width="1147">
  <tr>
    <td colspan=7 class=garisbawah>&nbsp;</td>
  </tr>
  <tr>
    <td width="60">&nbsp;</td>
    <td width="125">&nbsp;</td>
    <td width="365">&nbsp;</td>
    <td width="140">&nbsp;</td>
    <td width="230" class="huruf nmper">NILAI KOTOR</td>
    <td width="16" class="huruf nmper">:</td>
    <td width="119" align=right class="huruf nmper"><?php echo $spckot.$kotor; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="huruf nmper">POTONGAN (<?php echo "$dis"."$spcdis" ?> %)</td>
    <td class="huruf nmper">:</td>
    <td align=right class="huruf nmper"><?php echo $spcvdis.$vdis ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align=right class="huruf nmper">------------------</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="huruf nmper">NILAI BERSIH</td>

    <td class="huruf nmper">:</td>
    <td align=right class="huruf nmper"><?php echo $spcnb.$nb; ?></td>
  </tr>
</table>

<table width="902" >

  <tr>
    <td width="300" class="huruf nmper"  align="center">Menyetujui</td>
  </tr>
  <tr>
    <td width="300" class="huruf nmper">&nbsp;</td>
    <td width="9"  class="huruf nmper">&nbsp;</td>
    <td width="194" class="huruf nmper">&nbsp;</td>
	  <td width="185">&nbsp;</td>
	  <td width="190">&nbsp;</td>
  </tr>
  <tr>
    <td width="300" class="huruf nmper">&nbsp;</td>
  </tr>
  <tr>
    <td width="300" class="huruf nmper"  align="center">Area Supervisor/SDH       ADH</td>
    </tr>

</table>
<br>
<table width="100%" border="0" class="nmper">
    <tr>
      <td class="huruf nmper" width="185"> TOP </td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->n_spb_toplength;?></td>
      <td width="185">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper" width="185">Kode Sales</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->i_salesman; ?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">Discount</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->n_customer_discount;?></td>
      <td width="185">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper" width="185">Nama Salesman</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_salesman_name; ?></td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">REFERENSI</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"></td>
      <td width="185">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper" width="185"><?php echo $row->e_customer_refference; ?></td>
      <td class="huruf nmper"></td>
      <td class="huruf nmper"></td>
    </tr>
  </table>
<br>
<table width="100%" border="0" class="nmper">
     <tr>
      <td class="huruf nmper" width="185">lama usaha</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_age; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">status toko</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_shop_status; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">luas toko</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->n_shop_broad; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">Cara Bayar</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_paymentmethod; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper" width="185">Tipe</td>
      <td class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->e_customer_classname; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	  <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <br>
<table class="garisy" width="903">
  <tr>
    <td rowspan=2 align=center class="huruf isi">Tgl & Jam Terima SPB</td>
    <td colspan=3 align=center class="huruf isi">GUDANG</td>
    <td colspan=3 align=center class="huruf isi">Serah Terima Gudang</td>
    <td rowspan=2 width="72" align=center class="huruf isi">MD</td>
    <td colspan=3 align=center class="huruf isi">Tgl&Jam Terima Nota</td>
    <td colspan=3 align=center class="huruf isi">CEK PLAFON</td>
  </tr>
  <tr>
    <td width="59" align=center class="huruf isi">CEK I</td>
    <td width="61" align=center class="huruf isi">CEK II</td>
    <td width="62" align=center class="huruf isi">CEK III</td>
    <td width="61" align=center class="huruf isi">I</td>
    <td width="64" align=center class="huruf isi">II</td>
    <td width="64" align=center class="huruf isi">III</td>
    <td width="53" align=center class="huruf isi">I</td>
    <td width="53" align=center class="huruf isi">II</td>
    <td width="54" align=center class="huruf isi">III</td>
    <td width="48" align=center class="huruf isi">AR</td>
    <td width="54" align=center class="huruf isi">FADH</td>
    <td width="43" align=center class="huruf isi">SDH</td>
  </tr>
  <tr>
    <td align=center width=95 height=80>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
    <td align=center>&nbsp;</td>
  </tr>
</table>
<?php //echo '<br>'.$row->e_remark1; ?>
<?php 
}
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
