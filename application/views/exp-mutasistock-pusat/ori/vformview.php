<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'exp-mutasistock-pusat/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
		if($isi){
			foreach($isi as $row){
				$periode=$row->e_mutasi_periode;
			}
		}else{
			$periode=$iperiode;
		}
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <input name="pperiode" id="pperiode" value="<?php echo $row->e_mutasi_periode; ?>" type="hidden">
    <input name="iarea" id="iarea" value="<?php echo $iarea; ?>" type="hidden">
    <input name="cmdexport" id="cmdexport" value="Export to Excel" type="submit">
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>LAPORAN MUTASI STOCK - $row->i_store</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
		?>
    	  <table class="listtable" border=none>
<?php if($row->i_store=='AA'){?>
	   	    <th>No</th>
	   	    <th>Kode</th>
	   	    <th>Nama</th>
			    <th>Saldo Awal</th>
			    <th>Pembelian</th>
			    <th>Dari Cabang</th>
			    <th>Retur Penjualan</th>
			    <th>Penjualan</th>
			    <th>Ke Cabang</th>
			    <th>Sld Akhir</th>
			    <th>Sld Opname</th>
			    <th>Selisih</th>
<?php }else{?>
	   	    <th>No</th>
	   	    <th>Kode</th>
	   	    <th>Nama</th>
			    <th>Saldo Awal</th>
			    <th>Dari Pusat</th>
			    <th>Penjualan</th>
			    <th>Ke Pusat</th>
			    <th>Sld Akhir</th>
			    <th>Sld Opname</th>
			    <th>Selisih</th>
<?php }?>
        	<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
			foreach($isi as $row){
        $selisih=$row->n_saldo_stockopname-$row->n_saldo_akhir;
        if($row->i_store=='AA'){
		      echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_product</td>
            <td>$row->e_product_name</td>
				    <td align=right>$row->n_saldo_awal</td>
				    <td align=right>$row->n_mutasi_pembelian</td>
				    <td align=right>$row->n_mutasi_bbm</td>
				    <td align=right>$row->n_mutasi_returoutlet</td>
				    <td align=right>$row->n_mutasi_penjualan</td>
				    <td align=right>$row->n_mutasi_bbk</td>
				    <td align=right>$row->n_saldo_akhir</td>
				    <td align=right>$row->n_saldo_stockopname</td>
            <td align=right>$selisih</td>";
        }else{
		      echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_product</td>
            <td>$row->e_product_name</td>
				    <td align=right>$row->n_saldo_awal</td>
				    <td align=right>$row->n_mutasi_bbm</td>
				    <td align=right>$row->n_mutasi_penjualan</td>
				    <td align=right>$row->n_mutasi_bbk</td>
				    <td align=right>$row->n_saldo_akhir</td>
				    <td align=right>$row->n_saldo_stockopname</td>
            <td align=right>$selisih</td>";
        }
        $i++;
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='show(\"exp-mutasistock-pusat/cform/detail/$iperiode/$iarea/$row->i_product/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
#					echo "<a href=\"#\" onclick='showModal(\"exp-mutasistock-pusat/cform/detail/$iperiode/$iarea/$row->i_product/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td></tr>";	
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php #echo "<center>".$this->pagination->create_links()."</center>";?>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/exp-mutasistock-pusat/cform/viewdetail";
	  formna.submit();
  }
</script>
