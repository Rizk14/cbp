<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listsjpbedaterima/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="sjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Tgl SJ</td>
		<?php if($isi->d_sjp){
			  if($isi->d_sjp!=''){
				  $tmp=explode("-",$isi->d_sjp);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_sjp=$hr."-".$bl."-".$th;
			  }
		   }
       if($isi->d_sjp_receive){
			  if($isi->d_sjp_receive!=''){
				  $tmp=explode("-",$isi->d_sjp_receive);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_sjp_receive=$hr."-".$bl."-".$th;
			  }
		   }
		?>
		<td><input readonly id="dsj" name="dsj" value="<?php if($isi->d_sjp) echo $isi->d_sjp; ?>">
		    <input readonly id="isj" name="isj" value="<?php if($isi->i_sjp) echo $isi->i_sjp; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>">
		    <input id="istore" name="istore" type="hidden" value="<?php if($isi->i_store) echo $isi->i_store; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SJ Lama</td>
		<td><input readonly id="isjold" name="isjold" type="text" value="<?php echo $isi->i_sjp_old; ?>"></td>
	      </tr>
        <tr>
		<td width="10%">Tgl Terima</td>
		<td><input readonly id="dreceive" name="dreceive" type="text" value="<?php echo $isi->d_sjp_receive; ?>"
         onclick="showCalendar('',this,this,'','dreceive',0,20,1)"></td>
	      </tr>
        <tr>
		<td width="10%">Nilai Kirim</td>
		<td><input readonly style="text-align:right;" readonly id="vsj" name="vsj" value="<?php echo number_format($isi->v_sjp); ?>">
        <input type="hidden" name="jml" id="jml" value="<?php if($jmlitem) echo $jmlitem; ?>"></td>
	      </tr>
        <tr>
		<td width="10%">Nilai Terima</td>
		<td><input readonly style="text-align:right;" readonly id="vsjrec" name="vsjrec" value="<?php echo number_format($isi->v_sjp_receive); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick='show("listsjpbedaterima/cform/view/<?php echo $dfrom."/".$dto."/"; ?>","#main")'>
        <input name="cmdres" id="cmdres" value="Export to excell" type="button">
		  </td>
 	    </tr>
	    </table>
			<div id="detailheader" align="center">
				<?php 
					if($detail){
					?>
						<table class="listtable" align="center" style="width: 900px;" id="sitabel"">
							<th align="center">No</th>
							<th align="center">SJP</th>
							<th align="center">Area</th>
							<th align="center">Kode</th>
							<th align="center">Nama Barang</th>
							<th align="center">Ket</th>
							<th align="center">Jml Krm</th>
							<th align="center">Jml Trm</th>
							<tbody>
					<?php 
						$i=0;
						foreach($detail as $row)
            {
              $vtotal=$row->v_unit_price*$row->n_quantity_receive;
						  	$i++;
						  	echo "
								<tr>
      						<td style=\"width:23px;\">$i</td>
      						<td style=\"width:23px;\">$row->i_sjp</td>
      						<td style=\"width:23px;\">$row->i_area</td>
								  <td style=\"width:66px;\">$row->i_product</td>
								  <td style=\"width:314px;\">$row->e_product_name></td>
								  <td style=\"width:103px;\">$row->e_remark</td>
								  <td style=\"width:74px;\">$row->n_quantity_deliver</td>
								  <td style=\"width:74px;\">$row->n_quantity_receive</td>
							  </tr>";
            }
            echo "</tbody></table>";
          }
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php if($jmlitem) echo $jmlitem; ?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("iarea").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function diskonrupiah(isi){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot   =parseFloat(formatulang(document.getElementById("vspb").value));
		vtotdis=parseFloat(formatulang(isi));
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
  function tambih_item()
  {
    so_inner=='';
    so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
    so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
    so_inner+= '<th style="width:63px;" align="center">Kode</th>';
    so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
    so_inner+= '<th style="width:100px;" align="center">Motif</th>';
    so_inner+= '<th style="width:46px;"  align="center">Jml</th>';
    so_inner+= '<th style="width:100px;"  align="center">Keterangan</th>';
    so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
    document.getElementById("detailheader").innerHTML=so_inner;
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:69px;"><input style="width:69px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';
		si_inner+='<td style="width:105px;"><input style="width:105px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:54px;">&nbsp;</td></tr></tbody>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:69px;"><input style="width:69px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+'); pembandingnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';
		si_inner+='<td style="width:105px;"><input style="width:105px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:54px;">&nbsp;</td></tr></tbody>';
    }
    b=document.getElementById("ispb").value
    showModal("sj/cform/product/"+a+"/"+b+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  function pilihan(a,b){
	  if(a==''){
		  document.getElementById("chk"+b).value='on';
	  }else{
		  document.getElementById("chk"+b).value='';
		  document.getElementById("ndeliver"+b).value='0';
	  }
	  hitungnilai();
  }
  function view_spb(a)
  {
	showModal("sj/cform/spb/"+a+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }

  function hitungnilai(){
    jml=document.getElementById("jml").value;
    if (jml<=0){
    }else{

      salah=false;
      gud=document.getElementById("istore").value;
      if(gud!='AA'){
        for(i=1;i<=jml;i++){
          stock  =formatulang(document.getElementById("ndeliverhidden"+i).value);
          deliver=formatulang(document.getElementById("ndeliver"+i).value);
          if(parseFloat(stock)<0)stock=0;
          if(parseFloat(deliver)>parseFloat(stock)){
            alert('Jumlah Kirim melebihi jumlah stock');
            document.getElementById("ndeliver"+i).value=0;
            salah=true;
            break;
          }
        }
      }

      if(!salah){
        gros=0;
        for(i=1;i<=jml;i++){
          if(document.getElementById("chk"+i).value=='on'){
	          hrg=formatulang(document.getElementById("vproductmill"+i).value);
	          qty=formatulang(document.getElementById("ndeliver"+i).value);
	          vhrg=parseFloat(hrg)*parseFloat(qty);
	          document.getElementById("vtotal"+i).value=vhrg;
            gros=gros+parseFloat(formatulang(document.getElementById("vtotal"+i).value));
	          document.getElementById("vtotal"+i).value=formatcemua(vhrg);
          }
        }
        document.getElementById("vsjgross").value=formatcemua(gros);  
        nsjdisc1=parseFloat(formatulang(document.getElementById("nsjdiscount1").value));
        nsjdisc2=parseFloat(formatulang(document.getElementById("nsjdiscount2").value));
        nsjdisc3=parseFloat(formatulang(document.getElementById("nsjdiscount3").value));
        if(nsjdisc1==0)
          vsjdisc1=parseFloat(formatulang(document.getElementById("vsjdiscount1").value));
        else
          vsjdisc1=0;
        vsjdisc2=0;
        vsjdisc3=0;
        vtot =0;
        if(gros>0){
          if(nsjdisc1>0) vsjdisc1=vsjdisc1+((gros*nsjdisc1)/100);
	        vsjdisc2=vsjdisc2+(((gros-vsjdisc1)*nsjdisc2)/100);
	        vsjdisc3=vsjdisc3+(((gros-(vsjdisc1+vsjdisc2))*nsjdisc3)/100);
	        document.getElementById("vsjdiscount1").value=formatcemua(vsjdisc1);
	        document.getElementById("vsjdiscount2").value=formatcemua(vsjdisc2);
	        document.getElementById("vsjdiscount3").value=formatcemua(vsjdisc3);
	        vdis1=parseFloat(vsjdisc1);
	        vdis2=parseFloat(vsjdisc2);
	        vdis3=parseFloat(vsjdisc3);
	        vtotdis=vdis1+vdis2+vdis3;
//        	vtotdis=Math.round(vtotdis);
          if(document.getElementById("fspbconsigment").value=='f'){
	          document.getElementById("vsjdiscounttotal").value=formatcemua(Math.round(vtotdis));
	          vtotbersih=parseFloat(gros)-parseFloat(vtotdis);
	          document.getElementById("vsjnetto").value=formatcemua(Math.round(vtotbersih));
          }else{
	          vtotbersih=parseFloat(gros)-parseFloat(formatulang(document.getElementById("vsjdiscounttotal").value));
	          document.getElementById("vsjnetto").value=formatcemua(Math.round(vtotbersih));
          }
        }else{
          document.getElementById("vsjdiscount1").value=formatcemua(vsjdisc1);
	        document.getElementById("vsjdiscount2").value=formatcemua(vsjdisc2);
	        document.getElementById("vsjdiscount3").value=formatcemua(vsjdisc3);
          document.getElementById("vsjnetto").value=0;
          if(document.getElementById("fspbconsigment").value=='f'){
          	document.getElementById("vsjdiscounttotal").value=0;
          }
        }
      }
    }
  }
  function pembandingnilai(a) 
  {
	  var n_qty	= document.getElementById('norder'+a).value;
	  var n_deliver	= document.getElementById('ndeliver'+a).value;
	  var deliverasal	= document.getElementById('ndeliverhidden'+a).value;
	  if(parseInt(n_deliver) > parseInt(n_qty)) {
		  alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Order ( '+n_qty+' item )');
		  document.getElementById('ndeliver'+a).value	= deliverasal;
		  document.getElementById('ndeliver'+a).focus();
		  return false;
	  }else if(parseInt(n_deliver) > parseInt(deliverasal)) {
      i_store = document.getElementById('istore').value;
	    if(i_store!='AA') {
		    alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Stock ( '+deliverasal+' item )');
		    document.getElementById('ndeliver'+a).value	= deliverasal;
		    document.getElementById('ndeliver'+a).focus();
		    return false;
      }
    }
  }
  $( "#cmdres" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>

