<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listknretur/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="12" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>No KN</th>
			<th>Tgl KN</th>
			<th>No Reff</th>
			<th>Tgl Reff</th>
      <th>No TTB</th>
      <th>Tgl TTB</th>
			<th>Area</th>
			<th>Customer</th>
			<th>Salesman</th>
			<th>Nilai Bersih</th>
			<th>Sisa</th>
      <?php if($this->session->userdata('status')=='1')
      {
      ?>
			<th class="action">Action</th>
      <?php }?>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->n_kn_year=='')$row->n_kn_year=null;#'20'.substr($row->i_kn,6,2);
        if($row->d_kn!=''){
				  $tmp=explode('-',$row->d_kn);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_kn=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_refference!=''){
				  $tmp=explode('-',$row->d_refference);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_refference=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_ttb!=''){
				  $tmp=explode('-',$row->d_ttb);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_ttb=$tgl.'-'.$bln.'-'.$thn;
        }
        $row->i_kn=trim($row->i_kn);
        $row->i_refference=trim($row->i_refference);
			  echo "<tr>";
				if($row->f_kn_cancel=='t'){
			  echo "<td><h1>$row->i_kn</h1></td>";
				}else{
			  echo "<td>$row->i_kn</td>";
				}
			  echo " 
				  <td>$row->d_kn</td>
				  <td>$row->i_refference</td>
				  <td>$row->d_refference</td>
				  <td>$row->i_ttb</td>
				  <td>$row->d_ttb</td>
				  <td>$row->i_area</td>
				  <td>($row->i_customer) - $row->e_customer_name</td>
				  <td>$row->i_salesman</td>
				  <td align='right'>".number_format($row->v_netto)."</td>
				  <td align='right'>".number_format($row->v_sisa)."</td>";
				  if($this->session->userdata('status')=='1'){
				    echo "<td class=\"action\">";
    			  echo "<a href=\"#\" onclick='show(\"knretur/cform/edit/$row->i_kn/$row->n_kn_year/$row->i_area/$dfrom/$dto/$row->i_refference/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;";
		    		if($row->f_kn_cancel=='f' and $row->v_sisa!=0 and $row->v_netto == $row->v_sisa){
		    	  echo "<a href=\"#\" onclick='hapus(\"listknretur/cform/delete/$row->i_kn/$row->n_kn_year/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
		    		}
		    	  echo "</td>";
		    	}
		    	echo "</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
