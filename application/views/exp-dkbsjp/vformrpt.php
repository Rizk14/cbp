<title><?= $page_title ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>
<div id='tmp'>
	<?php
	include("php/fungsi.php");
	?>
	<h2><?php echo  $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo  $this->pquery->form_remote_tag(array('url' => $folder . '/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<table class="listtable" id="sitabel">
							<?php
							foreach ($isi as $row) {
								$idkb	= $row->i_dkb;
								$iarea	= $row->i_area;

								$i 		= 0;
								$kes 	= 0;
								$kre 	= 0;
								$vkes 	= 0;
								$vkre 	= 0;
							?>
								<tr>
									<td colspan="12" style="text-align: left; font-size: 16px"><b><?php echo $row->i_dkb . " (" . $row->e_area_name . ")" ?></b></td>
								</tr>
								<tr>
									<td width="31" rowspan="2">
										<div align="center">No</div>
									</td>
									<td colspan="2">
										<div align="center">SURAT JALAN PINJAMAN</div>
									</td>
									<td colspan="2">
										<div align="center">SPmB</div>
									</td>
									<td rowspan="2">
										<div align="center">NILAI</div>
									</td>
									<td width="117" rowspan="2">
										<div align="center">KETERANGAN</div>
									</td>
									<td colspan="3">
										<div align="center">KEMASAN</div>
									</td>
									<td width="117" rowspan="2">
										<div align="center">SPmB - SJP</div>
									</td>
									<td width="117" rowspan="2">
										<div align="center">SJP - DKB</div>
									</td>
								</tr>

								<tr>
									<td width="101">
										<div align="center">NOMOR SJP</div>
									</td>
									<td width="89">
										<div align="center">TANGGAL SJP</div>
									</td>
									<td width="101">
										<div align="center">NOMOR SPmB</div>
									</td>
									<td width="89">
										<div align="center">TANGGAL SPmB</div>
									</td>
									<td width="89">
										<div align="center">KOLI</div>
									</td>
									<td width="89">
										<div align="center">IKAT</div>
									</td>
									<td width="89">
										<div align="center">KARDUS</div>
									</td>
								</tr>

								<?php
								foreach ($detail as $rowi) {
									$i++;
									if (strlen($i) == 2) $aw = 0;
									if (strlen($i) == 1) $aw = 1;
									$aw = str_repeat(" ", $aw);
									$cust	= strval($rowi->i_area);
									$name	= $rowi->e_area_name;
									if (strlen($name) > 27) {
										$name = substr($name, 0, 27);
									}
									$name	= $name . str_repeat(" ", 27 - strlen($name));
									$isjp	= $rowi->i_sjp;

									$tmp 	= explode("-", $rowi->d_sjp);
									$th 	= $tmp[0];
									$bl 	= $tmp[1];
									$hr 	= $tmp[2];
									$dsjp  	= $hr . "-" . $bl . "-" . $th;

									$tmp 	= explode("-", $rowi->d_spmb);
									$th 	= $tmp[0];
									$bl 	= $tmp[1];
									$hr 	= $tmp[2];
									$dspmb 	= $hr . "-" . $bl . "-" . $th;

									$vsj	= $rowi->v_jumlah;

									$ket	= $rowi->keterangan;
									// $ket	= $ket . str_repeat(" ", 24 - strlen($ket));

									$sjp  	= strtotime($dsjp);
									$dkb 	= strtotime($row->d_dkb);

									if ($dkb != '') {
										$sjdkb 	= $dkb - $sjp;
										$dsjdkb	= floor($sjdkb / (60 * 60 * 24)) . " hari";
									} else {
										$dsjdkb	= '-';
									}

									$dapprove  = strtotime($rowi->d_approve);
									$spbx2 	= $dapprove - strtotime($rowi->d_spmb);

									//HITUNG JML HARI SPmB - SJP
									$dsjpx = strtotime($rowi->d_sjp) - strtotime($rowi->d_spmb);

									$dspmbsj = floor($dsjpx / (60 * 60 * 24)) . " hari";
								?>
									<tr>
										<td style="text-align:center" width="31"><?php echo $i; ?></td>
										<td style="text-align:center" width="100"><?php echo $isjp; ?></td>
										<td style="text-align:center" width="87"><?php echo $dsjp; ?></td>
										<td style="text-align:center" width="100"><?php echo $rowi->i_spmb; ?></td>
										<td style="text-align:center" width="87"><?php echo $dspmb; ?></td>
										<td width="109" align="right"><?php echo number_format($vsj); ?></td>
										<td width="118"><?php echo $ket; ?></td>
										<td style="text-align:right" width="118"><?php echo $rowi->n_koli; ?></td>
										<td style="text-align:right" width="118"><?php echo $rowi->n_ikat; ?></td>
										<td style="text-align:right" width="118"><?php echo $rowi->n_kardus; ?></td>
										<td style="text-align:center" width="118"><?php echo $dspmbsj; ?></td>
										<td style="text-align:center" width="118"><?php echo $dsjdkb; ?></td>
									</tr>
								<?php
								}

								$vsj	= $row->v_dkb;

								$tmp 	= explode("-", $row->d_dkb);
								$th 	= $tmp[0];
								$bl 	= $tmp[1];
								$hr 	= $tmp[2];
								$ddkb 	= $hr . " " . mbulan($bl) . " " . $th;

								$tgl 	= date("d") . " " . mbulan(date("m")) . " " . date("Y") . "  Jam : " . date("H:i:s");
								?>
								<tr>
									<td width="570" align="right" colspan="5">Jumlah Rp. </td>
									<td width="172" align="right"><?php echo number_format($vsj); ?></td>
									<td colspan="6"></td>
								</tr>
								<br>

						</table>
					<?php
							}
					?>
					<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
					</BODY>

					</html>
					<script language="javascript" type="text/javascript">
						// $("#cmdreset").click(function() {
						// 	var Contents = $('#sitabel').html();
						// 	/*window.open('data:application/vnd-ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );*/
						// 	window.open('data:application/vnd.ms-Excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');

						// });

						$("#cmdreset").click(function() {
							//getting values of current time for generating the file name
							var dt = new Date();
							var day = dt.getDate();
							var month = dt.getMonth() + 1;
							var year = dt.getFullYear();
							var hour = dt.getHours();
							var mins = dt.getMinutes();
							var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;

							//creating a temporary HTML link element (they support setting file names)
							var a = document.createElement('a');

							//getting data from our div that contains the HTML table
							var data_type = 'data:application/vnd.ms-excel';
							var table_div = $('#sitabel').html();

							// var table_html = table_div.outerHTML.replace(/ /g, '%20');
							a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

							//setting the file name
							// var nofaktur = document.getElementById('no_faktur').value;
							a.download = 'exp_dkbsjp_<?= $iarea ?>' + "_" + postfix + '.xls';

							//triggering the function
							a.click();
							//just in case, prevent default behaviour
						});
					</script>