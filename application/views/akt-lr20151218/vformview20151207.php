<?php 
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>Per $tanggal $namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" width="750px">
	   	    <th width="750px" colspan=5>&nbsp;</th>
		<?php 
		echo "<tr valign=top><td colspan=2>";

		echo "<table width=\"100%\">
			  <tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Hasil Penjualan :</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";
		if($pendapatan){
		  $penjualan=0;
		  $retpot=0;
		  foreach($pendapatan as $row){
		    if($row->v_saldo_akhir<0)$row->v_saldo_akhir=$row->v_saldo_akhir*-1;
		    if($row->i_coa=='411.100'){
          echo "<tr valign=top> 
			        <td width=\"450px\"colspan=2>".$row->e_coa_name."</td>
			        <td width=\"100px\" align=right>&nbsp;</td>";
		      echo "<td width=\"100px\" align=right>".number_format($row->v_saldo_akhir)."</td>";
          echo "<td width=\"100px\" align=right>&nbsp;</td>";
      		echo "</tr>";
      		$penjualan=$penjualan+$row->v_saldo_akhir;
      	}else{
          echo "<tr valign=top> 
			        <td width=\"400px\"colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;".$row->e_coa_name."</td>
			        <td width=\"175px\" align=right>".number_format($row->v_saldo_akhir)."</td>";
		      echo "<td width=\"175px\" align=right>&nbsp;</td>";
		      echo "<td width=\"175px\" align=right>&nbsp;</td>";
      		echo "</tr>";
      		$retpot=$retpot+$row->v_saldo_akhir;
      	}
      }
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Retur dan Potongan Penjualan</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>( ".number_format($retpot)." )</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Hasil Penjualan Bersih</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>".number_format($penjualan-$retpot)."</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td colspan=5>&nbsp;</td>";
  		echo "</tr>";
    }
#style=\"font-weight: bold;\"
		echo "
			  <tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Harga Pokok Penjualan :</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
        <td width=\"400px\"colspan=2>Persediaan Awal Barang Jadi</td>
        <td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>0</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td colspan=5>&nbsp;</td>";
		echo "</tr>";
		if($pembelian){
		  $beli=0;
		  $retpot=0;
		  foreach($pembelian as $row){
		    if($row->v_saldo_akhir<0)$row->v_saldo_akhir=$row->v_saldo_akhir*-1;
		    if($row->i_coa=='511.100'){
          echo "<tr valign=top> 
			        <td width=\"450px\"colspan=2>".$row->e_coa_name."</td>
			        <td width=\"100px\" align=right>&nbsp;</td>";
		      echo "<td width=\"100px\" align=right>".number_format($row->v_saldo_akhir)."</td>";
          echo "<td width=\"100px\" align=right>&nbsp;</td>";
      		echo "</tr>";
      		$beli=$beli+$row->v_saldo_akhir;
      	}else{
          echo "<tr valign=top> 
			        <td width=\"400px\"colspan=2>&nbsp;&nbsp;&nbsp;&nbsp;".$row->e_coa_name."</td>
			        <td width=\"175px\" align=right>".number_format($row->v_saldo_akhir)."</td>";
		      echo "<td width=\"175px\" align=right>&nbsp;</td>";
		      echo "<td width=\"175px\" align=right>&nbsp;</td>";
      		echo "</tr>";
      		$retpot=$retpot+$row->v_saldo_akhir;
      	}
      }
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Retur dan Potongan Pembelian</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>( ".number_format($retpot)." )</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Pembelian Barang Jadi Bersih</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>".number_format($beli-$retpot)."</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td colspan=5>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Barang Yang Tersedia Untuk Dijual</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>".number_format($beli-$retpot)."</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td colspan=5>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Persediaan Akhir Barang Jadi</td>
	          <td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>(0)</td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2>Harga Pokok Barang Yang Dijual</td>
	          <td width=\"175px\" align=right></td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>".number_format(0)."</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Laba Kotor</td>
	          <td width=\"175px\" align=right></td>";
      echo "<td width=\"175px\" align=right>&nbsp;</td>";
      echo "<td width=\"175px\" align=right>".number_format(0)."</td>";
  		echo "</tr>";
      echo "<tr valign=top> 
	          <td colspan=5>&nbsp;</td>";
  		echo "</tr>";
    }
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Biaya Operasional</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";		
		if($bebanope){
			echo "<td width=\"100px\" align=right>".number_format($bebanope)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "</tr>";
    echo "<tr valign=top> 
          <td colspan=5>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Laba Opersaional</td>
          <td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>( ".number_format(0)." )</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td colspan=5>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Pendapatan/Biaya Lainnya :</td>
          <td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "</tr>";
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Pendapatan Lain-lain</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		if($pendapatanlain){
		  foreach($pendapatanlain as $row){
  			echo "<td width=\"100px\" align=right>".number_format($row->v_saldo_akhir)."</td>";
  		}
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";		
		echo "</tr>";
		echo "
			  <tr valign=top> 
			  <td width=\"400px\"colspan=2>Biaya Bunga Bank & Biaya Lainnya</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		if($bebanlai){
			echo "<td width=\"175px\" align=right>".number_format($bebanlai)."</td>";
		}else{
			echo "<td width=\"175px\" align=right>0</td>";
		}
		echo "<td width=\"175px\" align=right>&nbsp;</td></tr>";
		$totalbeban=$bebanadm+$bebanope+$bebanlai;
		echo "<tr valign=top> 
			  <td width=\"400px\"colspan=2>&nbsp;</td>";
		echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "<td width=\"175px\" align=right style=\"font-weight: bold;\">(".number_format($totalbeban).")</td></tr>";
		echo "<tr valign=top> 
			  <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Pendapatan Bersih Sebelum Pajak</td>";
		echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "<td width=\"175px\" align=right style=\"font-weight: bold;\">0</td></tr></table>";
		#".number_format($pendapatan-$totalbeban)."
		echo "</td></tr>";
		?>
		</table>
      </tbody>
      </div>
	</div>
    </td>
  </tr>
</table>
