<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-pembelian-akunting/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<?php
							$bulan = date('m');
							$tahun = date('Y');
							$periode = date('Ym');
							?>
							<tr>
								<td width="19%">Periode</td>
								<td width="1%">:</td>
								<td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="<?= $periode ?>">
									<select name="bulan" id="bulan" value="<?= $bulan ?>" onmouseup="buatperiode()">
										<option value='01' <?php echo $bulan == '01' ? "selected" : ''; ?>>Januari</option>
										<option value='02' <?php echo $bulan == '02' ? "selected" : ''; ?>>Februari</option>
										<option value='03' <?php echo $bulan == '03' ? "selected" : ''; ?>>Maret</option>
										<option value='04' <?php echo $bulan == '04' ? "selected" : ''; ?>>April</option>
										<option value='05' <?php echo $bulan == '05' ? "selected" : ''; ?>>Mei</option>
										<option value='06' <?php echo $bulan == '06' ? "selected" : ''; ?>>Juni</option>
										<option value='07' <?php echo $bulan == '07' ? "selected" : ''; ?>>Juli</option>
										<option value='08' <?php echo $bulan == '08' ? "selected" : ''; ?>>Agustus</option>
										<option value='09' <?php echo $bulan == '09' ? "selected" : ''; ?>>September</option>
										<option value='10' <?php echo $bulan == '10' ? "selected" : ''; ?>>Oktober</option>
										<option value='11' <?php echo $bulan == '11' ? "selected" : ''; ?>>November</option>
										<option value='12' <?php echo $bulan == '12' ? "selected" : ''; ?>>Desember</option>
									</select>
									<select name="tahun" id="tahun" value="<?= $tahun ?>" onMouseUp="buatperiode()">
										<?php
										$tahun1 = date('Y') - 3;
										$tahun2 = date('Y');
										for ($i = $tahun1; $i <= $tahun2; $i++) {
											echo "<option value='$i'";
											echo $tahun == $i ? "selected" : '';
											echo ">$i</option>";
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-pembelian-akunting/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script languge=javascript type=text/javascript>
	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}

	function view_peri() {
		lebar = 1366;
		tinggi = 768;
		dfrom = document.getElementById("dfrom").value;
		dto = document.getElementById("dto").value;
		iarea = document.getElementById("iarea").value;
		eval('window.open("<?php echo site_url(); ?>"+"/exp-pembelian-akunting/cform/view/"+iarea+"/"+dfrom+"/"+dto,"","width="+lebar+"px,height="+tinggi+"px,resizable=yes,scrollbars=yes,location=yes,menubar=yes,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function view_excel() {
		lebar = 1366;
		tinggi = 768;
		dfrom = document.getElementById("dfrom").value;
		dto = document.getElementById("dto").value;
		iarea = document.getElementById("iarea").value;
		eval('window.open("<?php echo site_url(); ?>"+"/exp-pembelian-akunting/cform/export/"+iarea+"/"+dfrom+"/"+dto,"","width="+lebar+"px,height="+tinggi+"px,resizable=yes,scrollbars=yes,location=yes,menubar=yes,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function exportexcel() {
		var bulan = document.getElementById("bulan").value;
		var tahun = document.getElementById("tahun").value;
		var periode = tahun + bulan;

		if (bulan == '') {
			alert('Pilih Tanggal Terlebih Dahulu!!!');
			return false;
		} else if (tahun == '') {
			alert('Pilih Bulan Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url('exp-pembelian-akunting/cform/export/'); ?>/" + periode + "/" + bulan + "/" + tahun;
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>