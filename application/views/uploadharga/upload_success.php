<?php 
	$this->load->view('header');
	echo "<h2>Upload</h2>";
?>
<table class="maintable">
	<tr>
		<td align="left">
			<div class="effect">
	  			<div class="accordion2">
	    			<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      				<tr>
							<td>
								<h2>Proses upload berhasil !!!</h2>
							</td>
						</tr>
						<?php foreach($upload_data as $item => $value):?>
            <?php if( ($item!='file_path')&&($item!='full_path')&&($item!='raw_name')&&($item!='is_image')&&($item!='image_width')&&($item!='image_height')&&($item!='image_type')&&($item!='image_size_str')&&($item!='file_ext')&&($item!='orig_name') )
               {            
            ?>
						<tr>
							<td style="font-size:20px;">
								<?php echo $item;?>: <?php echo $value;?>
							</td>
						</tr>
            <?php }?>
						<?php endforeach; ?>
						<tr>
							<td style="font-size:20px;">
<!--								<?php echo anchor('spmbupload/upload', 'Upload File Lainnya!'); ?></p>-->
								<?php echo anchor('main', 'Kembali !!!'); ?></p>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</td>
	</tr>
</table
<?php 
	$this->load->view('footer');
?>
