<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listtargetcollection/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Target Collection per Area</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
?>
    	  <table class="listtable" border=none>
    	    <tr>
	     	    <th rowspan=2>No</th>
	     	    <th rowspan=2>Area</th>
	     	    <th rowspan=2>Target</th>
			      <th rowspan=2>Realisasi</th>
			      <th rowspan=2>Persen</th>
			      <th rowspan=2>Blm Bayar</th>
			      <th colspan=2>Tdk Telat</th>
			      <th colspan=2>Telat</th>
          	<th rowspan=2 class="action">/Nota</th>
          	<th rowspan=2 class="action">/Sales</th>
          	<th rowspan=2 class="action">/Divisi</th>
          </tr>
    	    <tr>
			      <th>Target</th>
			      <th>Realisasi</th>
			      <th>Target</th>
			      <th>Realisasi</th>
          </tr>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
      $ttarget=0;
      $trealis=0;
#      $ttarget2=0;
#      $trealis2=0;
#      $tlama=0;
      $tblm=0;
      $tsdh=0;
      $ttlt=0;
      $trealsdh=0;
      $trealtlt=0;
			foreach($isi as $row){
			  settype($row->lama,"integer");
#			  $tlama=$tlama+$row->lama;
        if($row->realisasi==null || $row->realisasi=='')$row->realisasi=0;
#        if($row->total==0)$row->total=$row->realisasi;
        if($row->total!=0){
          $persen=number_format(($row->realisasi/$row->total)*100,2);
        }else{
          $persen='0';
        }
/*
        if($row->realisasinon==null || $row->realisasinon=='')$row->realisasinon=0;
        if($row->totalnon!=0){
          $persennon=number_format(($row->realisasinon/$row->totalnon)*100,2);
        }else{
          $persennon='0';
        }
*/
        $tblm=$tblm+$row->blmbayar;
        $tsdh=$tsdh+$row->tdktelat;
        $ttlt=$ttlt+$row->telat;
        $trealsdh=$trealsdh+$row->realisasitdktelat;
        $trealtlt=$trealtlt+$row->realisasitelat;
        $ttarget=$ttarget+$row->total;
        $trealis=$trealis+$row->realisasi;
#        $ttarget2=$ttarget2+$row->totalnon;
#        $trealis2=$trealis2+$row->realisasinon;
        if($ttarget!=0) $persenall=number_format(($trealis/$ttarget)*100,2);
#        if($ttarget2!=0) $persenallnon=number_format(($trealis2/$ttarget2)*100,2);
	      echo "<tr>
          <td align=right><a href=\"#\" onclick='chartx(\"$iperiode\");'>$i</a></td>
          <td>$row->i_area-$row->e_area_name</td>
          <td align=right>Rp. ".number_format($row->total)."</td>
          <td align=right>RP. ".number_format($row->realisasi)."</td>
			    <td align=right>".number_format($persen,2)." %</td>";
			    
#			    <td align=right>Rp. ".number_format($row->totalnon)."</td>
#			    <td align=right>RP. ".number_format($row->realisasinon)."</td>
#			    <td align=right>".number_format($persennon,2)." %</td>
        echo "
          <td align=right>Rp. ".number_format($row->blmbayar)."</td>
          <td align=right>Rp. ".number_format($row->tdktelat)."</td>
          <td align=right>Rp. ".number_format($row->realisasitdktelat)."</td>
          <td align=right>Rp. ".number_format($row->telat)."</td>
          <td align=right>Rp. ".number_format($row->realisasitelat)."</td>";
        $i++;
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='view_detail(\"$iperiode\",\"$row->i_area\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td>";	
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='view_sales(\"$iperiode\",\"$row->i_area\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td>";
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='view_divisi(\"$iperiode\",\"$row->i_area\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
#				echo "</td>";
#			  echo "<td class=\"action\">";
#				echo "<a href=\"#\" onclick='show(\"listtargetcollection/cform/all/$iperiode/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td></tr>";	
			}
#			$tlama=$tlama/($i-1);
      echo "<tr>
          <td colspan=2>Total</td>
          <td align=right>Rp. ".number_format($ttarget)."</td>
          <td align=right>RP. ".number_format($trealis)."</td>
			    <td align=right>".number_format($persenall,2)." %</td>";
/*
			    <td align=right>Rp. ".number_format($ttarget2)."</td>
			    <td align=right>RP. ".number_format($trealis2)."</td>
			    <td align=right>".number_format($persenallnon,2)." %</td>
*/
      echo "
			    <td align=right>Rp. ".number_format($tblm)."</td>
			    <td align=right>RP. ".number_format($tsdh)."</td>
          <td align=right>RP. ".number_format($trealsdh)."</td>
			    <td align=right>Rp. ".number_format($ttlt)."</td>
			    <td align=right>Rp. ".number_format($trealtlt)."</td>
			    <td colspan=3></td></tr>";		
		}
	      ?>
	    </tbody>
	  </table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listtargetcollection/cform/viewdetail";
	  formna.submit();
  }
  function view_detail(a,b){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/listtargetcollection/cform/detail/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_sales(a,b){
    lebar =1366;
    tinggi=768;
    periode=document.getElementById("iperiode").value;
    eval('window.open("<?php echo site_url(); ?>"+"/listtargetcollection/cform/sales/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_divisi(a,b){
    lebar =1366;
    tinggi=768;
    periode=document.getElementById("iperiode").value;
    eval('window.open("<?php echo site_url(); ?>"+"/listtargetcollection/cform/divisi/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function chartx(iperiode){
    lebar =1366;
    tinggi=768;
/*
    eval('window.open("<?php echo site_url(); ?>"+"/listtpperarea/cform/chartx/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
*/
/*
    eval('window.open("<?php echo site_url(); ?>"+"/listtpperarea/cform/fcf/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
*/
    show("listtargetcollection/cform/fcf/"+iperiode,"#main");
  }
</script>
