<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'knretur/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="masterknreturform">
				<div class="effect">
					<div class="accordion2">
						<?php
						foreach ($isi as $row) {
							$tmp = explode('-', $row->d_kn);
							$tgl = $tmp[2];
							$bln = $tmp[1];
							$thn = $tmp[0];
							$row->d_kn = $tgl . '-' . $bln . '-' . $thn;
							if ($row->d_pajak) {
								$tmp = explode('-', $row->d_pajak);
								$tgl = $tmp[2];
								$bln = $tmp[1];
								$thn = $tmp[0];
								$row->d_pajak = $tgl . '-' . $bln . '-' . $thn;
							}
						?>
							<table class="mastertable">
								<tr>
									<td width="12%">Kredit Nota</td>
									<td width="1%">:</td>
									<td width="37%"><input readonly name="ikn" id="ikn" value="<?php echo $row->i_kn; ?>">
										<input readonly name="dkn" id="dkn" value="<?php echo $row->d_kn ?>" <?php if ($row->v_netto == $row->v_sisa) echo "onclick=\"showCalendar('',this,this,'','dkn',0,20,1)\""; ?>>
									</td>
									<td width="12%">Pelanggan</td>
									<td width="1%">:</td>
									<td width="37%"><input type="hidden" name="icustomer" id="icustomer" value="<?php echo $row->i_customer; ?>">
										<input type="hidden" name="icustomergroupar" id="icustomergroupar" value="<?php echo $row->i_customer_groupar; ?>">
										<input readonly name="ecustomername" id="ecustomername" value="<?php echo $row->e_customer_name; ?>">
									</td>
								</tr>
								<tr>
									<td width="12%">Area</td>
									<td width="1%">:</td>
									<td width="37%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>">
										<input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>" <?php if ($row->v_netto == $row->v_sisa) echo "onclick='showModal(\"knretur/cform/area/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'"; ?>>
									</td>
									<td width="12%">Alamat</td>
									<td width="1%">:</td>
									<td width="37%"><input readonly name="ecustomeraddress" id="ecustomeraddress" value="<?php echo $row->e_customer_address; ?>"></td>
								</tr>
								<tr>
									<td width="12%">No Refferensi</td>
									<td width="1%">:</td>
									<?php
									if (!empty($row->d_refference) || $row->d_refference != '') {
										$tmp = explode("-", $row->d_refference);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = substr($tmp[2], 0, 2);
										$drefference = $hr . "-" . $bl . "-" . $th;
									} else {
										$drefference = '';
									}
									?>
									<td width="37%"><input readonly name="irefference" id="irefference" value="<?php echo $row->i_refference; ?>" <?php if ($row->v_netto == $row->v_sisa) echo "onclick=\"view_bbm()\""; ?>>
										<input readonly name="drefference" id="drefference" value="<?php echo $drefference; ?>"> <!-- onclick="showCalendar('',this,this,'','drefference',0,20,1)" -->
									</td>
									<td width="12%">Salesman</td>
									<td width="1%">:</td>
									<td width="37%"><input type="hidden" name="isalesman" id="isalesman" value="<?php echo $row->i_salesman; ?>">
										<input readonly name="esalesmanname" id="esalesmanname" value="<?php echo $row->e_salesman_name; ?>">
									</td>
								</tr>
								<tr>
									<td width="12%">Kotor</td>
									<td width="1%">:</td>
									<td width="37%"><input style="text-align:right;" readonly name="vgross" id="vgross" value="<?php echo number_format($row->v_gross); ?>"></td>
									<td width="12%">Insentif</td>
									<td width="1%">:</td>
									<td width="37%"><input type="checkbox" name="finsentif" id="finsentif" <?php if ($row->f_insentif == 't')
																												echo "checked value=\"on\" ";
																											else
																												echo "value=\"\" ";
																											?> onclick="insentif(this.value)">
										&nbsp;Masalah&nbsp;
										<input type="checkbox" name="fmasalah" id="fmasalah" <?php if ($row->f_masalah == 't')
																									echo "checked value=\"on\" ";
																								else
																									echo "value=\"\" ";
																								?> onclick="masalah(this.value)">
									</td>
								</tr>
								<tr>
									<td width="12%">Potongan</td>
									<td width="1%">:</td>
									<td width="37%"><input style="text-align:right;" readonly name="vdiscount" id="vdiscount" value="<?php echo number_format($row->v_discount); ?>">
										<input type="hidden" name="nttbdiscount1" id="nttbdiscount1" value="<?php if (isset($row->n_ttb_discount1)) echo $row->n_ttb_discount1; ?>">
										<input type="hidden" name="nttbdiscount2" id="nttbdiscount2" value="<?php if (isset($row->n_ttb_discount2)) echo $row->n_ttb_discount2; ?>">
										<input type="hidden" name="nttbdiscount3" id="nttbdiscount3" value="<?php if (isset($row->n_ttb_discount3)) echo $row->n_ttb_discount3; ?>">
										<input type="hidden" name="vttbdiscount1" id="vttbdiscount1" value="<?php if (isset($row->v_ttb_discount1)) echo $row->v_ttb_discount1; ?>">
										<input type="hidden" name="vttbdiscount2" id="vttbdiscount2" value="<?php if (isset($row->v_ttb_discount2)) echo $row->v_ttb_discount2; ?>">
										<input type="hidden" name="vttbdiscount3" id="vttbdiscount3" value="<?php if (isset($row->v_ttb_discount3)) echo $row->v_ttb_discount3; ?>">
										<input type="hidden" name="vttbdiscounttotal" id="vttbdiscounttotal" value="<?php if (isset($row->v_ttb_discounttotal)) echo $row->v_ttb_discounttotal; ?>">
										<input type="hidden" name="vttbnetto" id="vttbnetto" value="<?php if (isset($row->v_ttb_netto)) echo $row->v_ttb_netto; ?>">
										<input type="hidden" name="vttbgross" id="vttbgross" value="<?php if (isset($row->v_ttb_gross)) echo $row->v_ttb_gross; ?>">
									</td>
									<td width="12%">Sisa</td>
									<td width="1%">:</td>
									<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value="<?php echo number_format($row->v_sisa); ?>"></td>
								</tr>
								<tr>
									<td width="12%">Bersih</td>
									<td width="1%">:</td>
									<td width="37%"><input style="text-align:right;" readonly name="vnetto" id="vnetto" value="<?php echo number_format($row->v_netto); ?>"></td>
									<td width="12%">Keterangan</td>
									<td width="1%">:</td>
									<td width="37%"><input name="eremark" id="eremark" value="<?php echo $row->e_remark; ?>"></td>
								</tr>
								<tr>
									<td width="12%">No / Tgl Pajak</td>
									<td width="1%">:</td>
									<td width="37%"><input readonly name="ipajak" id="ipajak" value="<?php echo $row->i_pajak; ?>" onclick='view_pajak()'>
										<input type="hidden" name="jml" id="jml" value="<?php if ($jml) echo $jml;
																						else echo '0'; ?>">
										<input readonly name="dpajak" id="dpajak" value="<?php echo $row->d_pajak; ?>">
									</td>
									<td width="12%">&nbsp;</td>
									<td width="1%">&nbsp;</td>
									<td width="37%">&nbsp;</td>
								</tr>
								<td width="12%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="87%" colspan=4>
									<?php if ($row->v_netto == $row->v_sisa) { ?>
										<input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
									<?php } ?>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listknretur/cform/view/<?php echo $dfrom . "/" . $dto . "/" . $iarea . "/"; ?>','#main')">
								</td>
	</tr>
</table>
<?php } ?>
<div id="detailheader" align="center">
	<table class="listtable" style="width:900px;">
		<tr>
			<th style="width:25px;" align="center">No</th>
			<th style="width:100px;" align="center">Kode</th>
			<th style="width:300px;" align="center">Nama Barang</th>
			<th style="width:100px;" align="center">Motif</th>
			<th style="width:100px;" align="center">Jumlah</th>
			<th style="width:120px;" align="center">Harga</th>
			<th style="width:150px;" align="center">Keterangan</th>
		</tr>
	</table>
</div>
<div id="detailisi" align="center">
	<?php
	$i = 0;
	if ($detail) {
		foreach ($detail as $row) {
			$i++;
			echo "<table class=listtable style=width:900px;>";
			echo "<tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td><td style=\"width:95px;\"><input style=\"width:95px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td><td style=\"width:288px;\"><input style=\"width:288px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\"><input type=\"hidden\" id=\"iproductmotif$i\" name=\"iproductmotif$i\" value=\"$row->i_product_motif\"><input type=\"hidden\" id=\"iproductgrade$i\" name=\"iproductgrade$i\" value=\"$row->i_product_grade\"></td><td style=\"width:94px;\"><input readonly style=\"width:94px;\"  type=\"text\" id=\"emotifname$i\" name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td><td style=\"width:96px;\"><input readonly style=\"text-align:right; width:96px;\" type=\"text\" id=\"nquantity$i\" name=\"nquantity$i\" value=\"$row->n_quantity\" onkeyup=\"cekval();\"></td><td style=\"width:116px;\"><input style=\"text-align:right; width:116px;\" type=\"text\" id=\"vunitprice$i\" name=\"vunitprice$i\" value=\"$row->v_unit_price\" onkeyup=\"cekval();\"></td><td style=\"width:146px;\"><input style=\"width:146px;\" type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\"></td></tr></tbody></table>";
		}
	}
	?>
</div>
</div>
<div id="pesan"></div>
</div>
</div>
<?= form_close() ?>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales() {
		if (
			(document.getElementById("dkn").value == '') ||
			(document.getElementById("iarea").value == '') ||
			(document.getElementById("irefference").value == '') ||
			(document.getElementById("eremark").value == '') ||
			(document.getElementById("finsentif").checked == false)
		) {
			alert("Data Header belum lengkap !!!");
		} else {
			document.getElementById("login").hidden = true;
		}
	}

	function view_bbm() {
		area = document.getElementById("iarea").value;
		if (area != '') {
			showModal("knretur/cform/bbm/" + area + "/", "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	}

	function view_pajak() {
		cust = document.getElementById("icustomer").value;
		prod = document.getElementById("iproduct1").value;
		if (cust != '' && prod != '') {
			showModal("knretur/cform/pajak/" + cust + "/" + prod + "/", "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}

	}

	function insentif(a) {
		if (a == '') {
			document.getElementById("finsentif").value = 'on';
		} else {
			document.getElementById("finsentif").value = '';
		}
	}

	function masalah(a) {
		if (a == '') {
			document.getElementById("fmasalah").value = 'on';
		} else {
			document.getElementById("fmasalah").value = '';
		}
	}

	function tesss() {
		document.getElementById("ikn").value = "";
		document.getElementById("dkn").value = "";
		document.getElementById("icustomer").value = "";
		document.getElementById("icustomergroupar").value = "";
		document.getElementById("ecustomername").value = "";
		document.getElementById("iarea").value = "";
		document.getElementById("eareaname").value = "";
		document.getElementById("ecustomeraddress").value = "";
		document.getElementById("irefference").value = "";
		document.getElementById("drefference").value = "";
		document.getElementById("isalesman").value = "";
		document.getElementById("esalesmanname").value = "";
		document.getElementById("vgross").value = "";
		document.getElementById("finsentif").value = "";
		document.getElementById("finsentif").checked = false;
		document.getElementById("vdiscount").value = "";
		document.getElementById("fmasalah").value = "";
		document.getElementById("fmasalah").checked = false
		document.getElementById("vnetto").value = "";
		document.getElementById("vsisa").value = "";
		document.getElementById("eremark").value = "";
		document.getElementById("login").disabled = false;
		document.getElementById("pesan").innerHTML = '';
	}

	function cekval() {
		var jml = parseFloat(document.getElementById("jml").value);
		//	  var num = input.replace(/\,/g,'');
		//    alert(jml);
		//	  if(!isNaN(num)){
		for (j = 1; j <= jml; j++) {
			if (document.getElementById("nquantity" + j).value == '') document.getElementById("nquantity" + j).value = '0';
			var jml = parseFloat(document.getElementById("jml").value);
			var totdis = 0;
			var totnil = 0;
			var hrg = 0;
			var ndis1 = parseFloat(formatulang(document.getElementById("nttbdiscount1").value));
			var ndis2 = parseFloat(formatulang(document.getElementById("nttbdiscount2").value));
			var ndis3 = parseFloat(formatulang(document.getElementById("nttbdiscount3").value));
			var vdis1 = 0;
			var vdis2 = 0;
			var vdis3 = 0;
			for (i = 1; i <= jml; i++) {
				vprod = parseFloat(formatulang(document.getElementById("vunitprice" + i).value));
				nquan = parseFloat(formatulang(document.getElementById("nquantity" + i).value));
				var hrgtmp = vprod * nquan;
				hrg = hrg + hrgtmp;
			}
			vdis1 = vdis1 + ((hrg * ndis1) / 100);
			vdis2 = vdis2 + (((hrg - vdis1) * ndis2) / 100);
			vdis3 = vdis3 + (((hrg - (vdis1 + vdis2)) * ndis3) / 100);
			vdistot = Math.round(vdis1 + vdis2 + vdis3);
			vhrgreal = hrg - vdistot;
			document.getElementById("vttbdiscount1").value = formatcemua(vdis1);
			document.getElementById("vttbdiscount2").value = formatcemua(vdis2);
			document.getElementById("vttbdiscount3").value = formatcemua(vdis3);
			document.getElementById("vttbdiscounttotal").value = formatcemua(vdistot);
			document.getElementById("vttbnetto").value = formatcemua(vhrgreal);
			document.getElementById("vttbgross").value = formatcemua(hrg);
			document.getElementById("vdiscount").value = formatcemua(vdistot);
			document.getElementById("vnetto").value = formatcemua(vhrgreal);
			document.getElementById("vsisa").value = formatcemua(vhrgreal);
			document.getElementById("vgross").value = formatcemua(hrg);

		}
		//    }else{ 
		//		  alert('input harus numerik !!!');
		//      input = input.substring(0,input.length-1);
		//	  }
	}
	$(document).ready(function() {
		cekval();
	});
</script>