<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php $tujuan = 'knretur/cform/pajak/'.$cust.'/'.$prod; ?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Nota</th>
      	    <th>Tgl Nota</th>
      	    <th>Pajak</th>
      	    <th>Tgl Pajak</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->d_nota){
			    $tmp=explode("-",$row->d_nota);
			    $th=$tmp[0];
			    $bl=$tmp[1];
			    $hr=$tmp[2];
			    $row->d_nota=$hr."-".$bl."-".$th;
        }
        if($row->d_pajak){
			    $tmp=explode("-",$row->d_pajak);
			    $th=$tmp[0];
			    $bl=$tmp[1];
			    $hr=$tmp[2];
			    $row->d_pajak=$hr."-".$bl."-".$th;
        }
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_seri_pajak','$row->d_pajak')\">$row->i_nota</a></td>
				  <td><a href=\"javascript:setValue('$row->i_seri_pajak','$row->d_pajak')\">$row->d_nota</a></td>
				  <td><a href=\"javascript:setValue('$row->i_seri_pajak','$row->d_pajak')\">$row->i_seri_pajak</a></td>
				  <td><a href=\"javascript:setValue('$row->i_seri_pajak','$row->d_pajak')\">$row->d_pajak</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    jsDlgHide("#konten *", "#fade", "#light");
    document.getElementById("ipajak").value=a;
    document.getElementById("dpajak").value=b;
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
