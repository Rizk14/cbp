<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body id="bodylist">
	<div id="main">
		<div id="tmp">
			<?php echo "<center><h2>$page_title</h2></center>"; ?>
			<table class="maintable">
				<tr>
					<td align="left">

						<?php $tujuan = 'knretur/cform/caribbm/' . $iarea . '/' . $dkn; ?>
						<?php echo $this->pquery->form_remote_tag(array('url' => $tujuan, 'update' => '#light', 'type' => 'post')); ?>
						<div id="listform">
							<div class="effect">
								<div class="accordion2">
									<table class="listtable">
										<thead>
											<tr>
												<td colspan="10" align="center">Cari data :
													<input type="text" id="cari" name="cari" value="">&nbsp;
													<input type="submit" id="bcari" name="bcari" value="Cari">
												</td>
											</tr>
										</thead>
										<th>BBM</th>
										<th>Tgl BBM</th>
										<th>TTB</th>
										<th>Tgl TTB</th>
										<th>Pelanggan</th>
										<th>Area</th>
										<tbody>
											<?php
											if ($isi) {
												foreach ($isi as $row) {
													if ($row->d_bbm) {
														$tmp = explode("-", $row->d_bbm);
														$th = $tmp[0];
														$bl = $tmp[1];
														$hr = $tmp[2];
														$row->d_bbm = $hr . "-" . $bl . "-" . $th;
													}
													if ($row->d_ttb) {
														$tmp = explode("-", $row->d_ttb);
														$th = $tmp[0];
														$bl = $tmp[1];
														$hr = $tmp[2];
														$row->d_ttb = $hr . "-" . $bl . "-" . $th;
													}
													$nama = $row->e_customer_name;
													$nama = str_replace("'", "", $nama);
													$nama	= str_replace("(", "tandakurungbuka", $nama);
													$nama	= str_replace(";", "tandatitikdua", $nama);
													$nama	= str_replace("&", "tandadan", $nama);
													$nama	= str_replace(")", "tandakurungtutup", $nama);
													$nama = str_replace('.', 'tandatitik', $nama);
													$nama = str_replace(',', 'tandakoma', $nama);
													$nama = str_replace('/', 'tandagaring', $nama);
													$nama = str_replace('%20', ' ', $nama);
													$row->e_salesman_name = str_replace('.', 'tandatitik', $row->e_salesman_name);
													$row->e_salesman_name = str_replace("'", 'tandatikatas', $row->e_salesman_name);
													$row->e_customer_address = str_replace('.', 'tandatitik', $row->e_customer_address);
													$row->e_customer_address = str_replace(';', 'tandatikma', $row->e_customer_address);
													$row->e_customer_address = str_replace(':', 'tandatikwa', $row->e_customer_address);
													$row->e_customer_address = str_replace(',', 'tandakoma', $row->e_customer_address);
													$row->e_customer_address = str_replace('/', 'tandagaring', $row->e_customer_address);
													$row->e_customer_address	= str_replace("(", "tandakurungbuka", $row->e_customer_address);
													$row->e_customer_address	= str_replace(")", "tandakurungtutup", $row->e_customer_address);
													$row->e_customer_address = str_replace('&', 'tandadan', $row->e_customer_address);
													$row->e_customer_address = str_replace('%20', ' ', $row->e_customer_address);
													if ($row->e_customer_address == '') $row->e_customer_address = 'kosong';
													echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "','" . number_format($row->v_ttb_discounttotal) . "','" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3,$row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3, '$row->i_customer_groupar')\">$row->i_bbm</a></td>
<td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "','" . number_format($row->v_ttb_discounttotal) . "','" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3,$row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3, '$row->i_customer_groupar')\">$row->d_bbm</a></td>
<td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "','" . number_format($row->v_ttb_discounttotal) . "','" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3,$row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3, '$row->i_customer_groupar')\">$row->i_ttb</a></td>
<td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "','" . number_format($row->v_ttb_discounttotal) . "','" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3,$row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3, '$row->i_customer_groupar')\">$row->d_ttb</a></td>
<td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "','" . number_format($row->v_ttb_discounttotal) . "','" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3, $row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3,'$row->i_customer_groupar')\">$row->i_customer</a></td>
<td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$nama','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','" . number_format($row->v_ttb_gross) . "'," . number_format($row->v_ttb_discounttotal) . ",'" . number_format($row->v_ttb_netto) . "','" . number_format($row->v_ttb_sisa) . "','$row->d_bbm',$row->n_ttb_discount1,$row->n_ttb_discount2,$row->n_ttb_discount3, $row->v_ttb_discount1,$row->v_ttb_discount2,$row->v_ttb_discount3,'$row->i_customer_groupar')\">$row->e_area_name</a></td>
				</tr>";
												}
											}
											?>
										</tbody>
									</table>
									<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
									<br>
									<center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
								</div>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
	function setValue(a, b, c, d, e, f, g, h, i, j, k, l, m, o, p, q, r, s, t, n) {
		jsDlgHide("#konten *", "#fade", "#light");
		u = document.getElementById("dkn").value;
		tujuan = "knretur/cform/adabbm/" + a + "/" + b + "/" + c + "/" + d + "/" + e + "/" + f + "/" + g + "/" + h + "/" + formatulang(i) + "/" + formatulang(j) + "/" + formatulang(k) + "/" + formatulang(l) + "/" + m + "/" + o + "/" + p + "/" + q + "/" + r + "/" + s + "/" + t + "/" + u + "/" + n + "/";
		//    alert(tujuan);
		show(tujuan, "#main");
	}

	function bbatal() {
		jsDlgHide("#konten *", "#fade", "#light");
	}
</script>