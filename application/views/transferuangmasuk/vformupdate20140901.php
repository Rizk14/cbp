<?php 
	echo "<h2>$page_title</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'transferuangmasuk/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="transferuangform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
			<?php 
				if($isi->d_kum!=''){
					$tmp=explode('-',$isi->d_kum);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kum=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
	      <tr>
		<td width="12%">Bukti transfer</td>
		<td width="1%">:</td>
		<td width="37%"><input name="ikum" id="ikum" value="<?php echo $isi->i_kum; ?>" maxlength="15">
                    <input type="hidden" name="xkum" id="xkum" value="<?php echo $isi->i_kum; ?>">
                    <input type="hidden" name="xdkum" id="xdkum" value="<?php echo $isi->d_kum; ?>">
                    <input readonly id="dkum" name="dkum" value="<?php echo $isi->d_kum; ?>" 
                    <?php if($isi->v_jumlah==$isi->v_sisa) echo "onclick=\"showCalendar('',this,this,'','dkum',0,20,1)\"";?>></td>
		<td width="12%">Nama Bank</td>
		<td width="1%">:</td>
		<td width="37%"><select readonly name="ebankname" id="ebankname">
							<option <?php if($isi->e_bank_name=='BCA Bandung') echo 'selected'; ?>>BCA Bandung</option>
							<option <?php if($isi->e_bank_name=='BCA Jakarta') echo 'selected'; ?>>BCA Jakarta</option>
							<option <?php if($isi->e_bank_name=='Permata Bandung') echo 'selected'; ?>>Permata Bandung</option>
							<option <?php if($isi->e_bank_name=='BRI Bandung') echo 'selected'; ?>>BRI Bandung</option>
							<option>--</option>
						</select>
<!--<input name="ebankname" id="ebankname" value="<?php echo $isi->e_bank_name; ?>" maxlength="50">-->
    </td>
	      </tr>
		  <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name; ?>"
						<?php if($isi->v_jumlah==$isi->v_sisa) echo "onclick='showModal(\"transferuangmasuk/cform/area/\",\"#light\"); jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'";?>>
						<input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area; ?>">
            <input type="hidden" name="iareaasal" id="iareaasal" value="<?php echo $isi->i_area; ?>"></td>
		<td width="12%">Salesman</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="esalesmanname" id="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
						<input type="hidden" name="isalesman" id="isalesman" value="<?php echo $isi->i_salesman; ?>"></td>
		  </tr>
		  <tr>
		<td width="12%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ecustomername" id="ecustomername" value="<?php echo $isi->e_customer_name; ?>" 
            <?php if($isi->v_jumlah==$isi->v_sisa) echo "onclick=\"ncust()\"";?>>
						<input type="hidden" name="icustomer" id="icustomer" value="<?php echo $isi->i_customer; ?>">
						<input type="hidden" name="icustomergroupar" id="icustomergroupar" value="<?php echo $isi->i_customer_groupar; ?>"></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value="<?php echo $isi->e_remark; ?>"></td>
		  </tr>
		  <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="37%"><input name="vjumlah" id="vjumlah" value="<?php echo number_format($isi->v_jumlah); ?>" onkeyup="reformat(this);hetang(this);"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="vsisa" id="vsisa" value="<?php echo number_format($isi->v_sisa); ?>"></td>
		  </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan="4">
		  <input <?php if($pst!='00') echo 'disabled'; ?> name="login" id="login" value="Simpan" type="submit" onclick="dipales()" <?php if($isi->v_sisa!=$isi->v_jumlah || $isi->v_sisa==0) echo 'disabled'; ?>>
		   <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick='show("listtransferuangmasuk/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'>
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("ikum").value=='')||
			(document.getElementById("dkum").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
		}
	}
	function ncust()
	{
		area=document.getElementById("iarea").value;
		showModal("transferuangmasuk/cform/customer/"+area+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function hetang()
	{
		document.getElementById("vsisa").value=document.getElementById("vjumlah").value;
	}
</script>
