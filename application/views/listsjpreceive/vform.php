<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'listsjpreceive/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="areafrom" name="areafrom" value="">
									<?php
									$data = array(
										'name'      => 'dfrom',
										'id'        => 'dfrom',
										'value'     => date('01-m-Y'),
										'readonly'	=> 'true',
										'onclick'	=> "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'      => 'dto',
										'id'        => 'dto',
										'value'     => date('d-m-Y'),
										'readonly'	=> 'true',
										'onclick'	=> "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Area</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("listsjpreceive/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<input name="login" id="login" value="View" type="submit" onclick="return dipales()">
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('listsjpreceive/cform/','#tmpx')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function dipales(a) {
		if (document.getElementById("iarea").value == '') {
			alert("Pilih Area Dulu!!!");
			return false;
		} else {
			return true;
		}
	}
</script>