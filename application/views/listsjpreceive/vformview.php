<style>
	.container {
		position: relative;
		overflow: scroll;
		height: 500px;
	}
</style>

<div id='tmp'>
	<h2><?php echo $page_title . " (Total Data : " . number_format($total_rows, 0, ",", ".") . ")"; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'listsjpreceive/cform/cari', 'update' => '#tmpx', 'type' => 'post')); ?>
				<div id="listform">
					<div class="effect">
						<div class="accordion2">
							<div class="container">
								<table class="listtablex" id="sitabel">
									<thead class="sticky-head">
										<tr>
											<td colspan="9">
												<!-- Status Terima :
												<select id="freceive" name="freceive">
													<option value="all">Semua</option>
													<option value="0">Belum</option>
													<option value="1">Sudah</option>
												</select> -->

												Status Approve Keuangan :
												<select id="fapprovefa" name="fapprovefa">
													<option <?= $fapprovefa == "all" ? "selected" : "" ?> value="all">Semua</option>
													<option <?= $fapprovefa == "0" ? "selected" : "" ?> value="0">Belum</option>
													<option <?= $fapprovefa == "1" ? "selected" : "" ?> value="1">Sudah</option>
												</select>

												Konsinyasi :
												<select id="fconsigment" name="fconsigment">
													<option <?= $fconsigment == "all" ? "selected" : "" ?> value="all">Semua</option>
													<option <?= $fconsigment == "0" ? "selected" : "" ?> value="0">Tidak</option>
													<option <?= $fconsigment == "1" ? "selected" : "" ?> value="1">Ya</option>
												</select>

												<!-- 
												Area :
												<select id="fconsigment" name="fconsigment">
												<#?php
												if ($iarea1 == "00") {
													echo "<option value='NA'>NASIONAL</option>";
												}

												foreach ($list_area->result() as $ri) {
													echo "<option value='$ri->i_area'>" . $ri->e_area_name . "</option>";
												}
												?>
												</select>

												<#?php
												for ($n = 0; $n < 20; $n++) {
													echo "&nbsp;";
												}
												?>

												Cari data :
												<input type="text" id="cari" name="cari" value="<#?php echo $cari; ?>">
												<input type="hidden" id="dfrom" name="dfrom" value="<#?php echo $dfrom; ?>" readonly>
												<input type="hidden" id="dto" name="dto" value="<#?php echo $dto; ?>" readonly>
												<input type="hidden" id="iarea" name="iarea" value="<#?php echo $iarea; ?>" readonly>&nbsp;
												<input type="submit" id="bcari" name="bcari" value="Cari">

												<#?php
												for ($n = 0; $n < 32; $n++) {
													echo "&nbsp;";
												}
												?>
												 -->

												<?php
												for ($n = 0; $n < 20; $n++) {
													echo "&nbsp;";
												}
												?>

												Cari data :
												<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">
												<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
												<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
												<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;
												<input type="submit" id="bcari" name="bcari" value="Cari">

												<?php
												for ($n = 0; $n < 119; $n++) {
													echo "&nbsp;";
												}
												?>

												<input name="cmdexp" id="cmdexp" value="Download" type="button">
											</td>
										</tr>

										<th>No</th>
										<th>No SJP</th>
										<th>Tgl SJP</th>
										<!-- <th>SJP Lama</th> -->
										<th>Area</th>
										<th>Terima</th>
										<th>Tgl Terima</th>
										<th>Tgl Approve Keuangan</th>
										<th>Konsinyasi</th>
										<th class="action">Action</th>
									</thead>

									<tbody>
										<?php
										if ($isi) {
											$no = 1;
											foreach ($isi as $row) {
												$tmp = explode('-', $row->d_sjp);
												$tgl = $tmp[2];
												$bln = $tmp[1];
												$thn = $tmp[0];
												$row->d_sjp = $tgl . '-' . $bln . '-' . $thn;

												if ($row->d_sjp_receive != '') {
													$tm	= explode('-', $row->d_sjp_receive);
													$tgl	= $tm[2];
													$bln	= $tm[1];
													$thn	= $tm[0];
													$row->d_sjp_receive = $tgl . '-' . $bln . '-' . $thn;
												}

												if ($row->d_sjp_receive == '') {
													$terima = 'Belum';
												} else {
													$terima = 'Sudah';
												}

												echo "<tr> 
														<td style='text-align:center'>$no</td>";

												if ($row->f_sjp_cancel == 't') {
													echo "<td style='text-align:center'><h2>$row->i_sjp</h2></td>";
												} else {
													echo "<td style='text-align:center'>$row->i_sjp</td>";
												}

												if ($row->f_spmb_consigment == 't') {
													$kons = 'ya';
												} else {
													$kons = 'tdk';
												}

												$dapprove = $row->d_approve != "" ? date('d-m-Y', strtotime($row->d_approve)) : "";

												// <td>$row->i_sjp_old</td>
												echo "  <td style='text-align:center'>$row->d_sjp</td>
														<td>$row->e_area_name</td>
														<td style='text-align:center'>$terima</td>
														<td style='text-align:center'>$row->d_sjp_receive</td>
														<td style='text-align:center'>$dapprove</td>
														<td style='text-align:center'>$kons</td>
														<td id='action' class=\"action\">";
												echo "<a href=\"#\" onclick='show(\"listsjpreceive/cform/edit/$row->i_sjp/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												$iarea	= $this->session->userdata('i_area');
												/*					    if($row->f_sjp_cancel == 'f' && $iarea=='00'){
					echo "&nbsp;&nbsp;
<a href=\"#\" onclick='hapus(\"listsjpreceive/cform/delete/$row->i_sjp/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
				   	    }*/
												echo "</td>
											</tr>";
												$no++;
											}
										}
										?>
									</tbody>
								</table>
							</div>
							<!-- <#?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?> -->
						</div>
					</div>
				</div>
				<?= form_close() ?>
				<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('listsjpreceive/cform/','#tmpx')">
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		$('#freceive').change(function() {
			$('#bcari').click();
		});

		$('#fapprovefa').change(function() {
			$('#bcari').click();
		});

		$('#fconsigment').change(function() {
			$('#bcari').click();
		});

		console.log($('#sitabel').html());
	});

	$("#cmdexp").click(function() {

		document.getElementById("action").style.display = "none";

		//getting values of current time for generating the file name
		var dt = new Date();
		var day = dt.getDate();
		var month = dt.getMonth() + 1;
		var year = dt.getFullYear();
		var hour = dt.getHours();
		var mins = dt.getMinutes();
		var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
		//creating a temporary HTML link element (they support setting file names)
		var a = document.createElement('a');
		//getting data from our div that contains the HTML table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = $('#sitabel').html();
		// var table_html = table_div.outerHTML.replace(/ /g, '%20');
		a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

		//setting the file name
		// var nofaktur = document.getElementById('no_faktur').value;
		a.download = 'export_daftar_sjp_receive_' + postfix + '.xls';
		//triggering the function
		a.click();
		//just in case, prevent default behaviour

		// var Contents = $('#sitabel').html();
		// window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
	});
</script>