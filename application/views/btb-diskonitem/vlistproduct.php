<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url'=>'btb-diskonitem/cform/cariproduct/'.$baris.'/'.$op,'id'=>'listform','update'=>'#tmp','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="listtable">
       <thead>
         <tr>
      <td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"></td>
         </tr>
       </thead>
             <th>Kode Product</th>
         <th>Nama</th>
         <th>Motif</th>
       <tbody>
         <?php 
      if($isi){
         foreach($isi as $row){
           echo "<tr>
              <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_order')\">$row->kode</a></td>
              <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_order')\">$row->nama</a></td>
              <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_order')\">$row->namamotif</a></td>
            </tr>";
         }
      }
         ?>
       </tbody>
     </table>
     <?php echo "<center>".$this->pagination->create_links()."</center>";?>
     <br>
     <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g)
  {
    ada=false;
    for(i=1;i<=d;i++){
      if(
         (a==document.getElementById("iproduct"+i).value) &&
         (i!==d) &&
         (e==document.getElementById("motif"+i).value)
        ){
         alert ("kode : "+a+" sudah ada !!!!!");
         ada=true;
         break;
      }else{
         ada=false;
      }
    }
    if(!ada){
      document.getElementById("iproduct"+d).value=a;
      document.getElementById("eproductname"+d).value=b;
      document.getElementById("vproductmill"+d).value=c;
      document.getElementById("motif"+d).value=e;
      document.getElementById("emotifname"+d).value=f;
      document.getElementById("ndeliver"+d).value=g;
      if (isNaN(parseFloat(g))){
         alert("Input harus numerik");
      }else{
         vtot =0;
         for(i=1;i<=d;i++){
            vhrg=formatulang(document.getElementById("vproductmill"+i).value);
            nqty=formatulang(document.getElementById("ndeliver"+i).value);
            vhrg=parseFloat(vhrg)*parseFloat(nqty);
            vtot=vtot+vhrg;
            document.getElementById("vtotal"+i).value=formatcemua(vhrg);
         }
         document.getElementById("vdogross").value=formatcemua(vtot);
      }
      jsDlgHide("#konten *", "#fade", "#light");
    }

  }
  function bbatal(){
   baris = document.getElementById("baris").value;
   si_inner= document.getElementById("detailisi").innerHTML;
// alert(si_inner);
   var temp= new Array();
   temp  = si_inner.split('<table disabled="disabled">');
   if( (document.getElementById("iproduct"+baris).value=='')){
      si_inner='';
      for(x=1;x<baris;x++){
         si_inner=si_inner+'<table>'+temp[x];
      }
      j=0;
      var barbar     = Array();
      var iproduct      = Array();
      var eproductname  = Array();
      var ndeliver      = Array();
      var vproductmill  = Array();
      var motif      = Array();
      var motifname     = Array();
      var vtotal     = Array();
      for(i=1;i<baris;i++){
         j++;
         barbar[j]      = document.getElementById("baris"+i).value;
         iproduct[j]    = document.getElementById("iproduct"+i).value;
         eproductname[j]      = document.getElementById("eproductname"+i).value;
         ndeliver[j]    = document.getElementById("ndeliver"+i).value;
         vproductmill[j]      = document.getElementById("vproductmill"+i).value;
         motif[j]    = document.getElementById("motif"+i).value;
         motifname[j]      = document.getElementById("emotifname"+i).value;
         vtotal[j]      = document.getElementById("vtotal"+i).value;
      }
      document.getElementById("detailisi").innerHTML=si_inner;
      j=0;
      for(i=1;i<baris;i++){
         j++;
         document.getElementById("baris"+i).value=barbar[j];
         document.getElementById("iproduct"+i).value=iproduct[j];
         document.getElementById("eproductname"+i).value=eproductname[j];
         document.getElementById("ndeliver"+i).value=ndeliver[j];
         document.getElementById("vproductmill"+i).value=vproductmill[j];
         document.getElementById("motif"+i).value=motif[j];
         document.getElementById("emotifname"+i).value=motifname[j];
         document.getElementById("vtotal"+i).value=vtotal[j];
      }
      document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
      jsDlgHide("#konten *", "#fade", "#light");
   }
  }
</script>
