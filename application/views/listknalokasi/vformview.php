<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'listknalokasi/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="10" align="center">Cari data :
										<input type="text" id="cari" name="cari" value=""><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
										&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
									</td>
								</tr>
							</thead>
							<th>Area</th>
							<th>No Alokasi</th>
							<th>No KN</th>
							<th>Tgl alokasi</th>
							<th>Jenis Biaya</th>
							<th>Customer</th>
							<th>Jumlah</th>
							<th>Jumlah Alokasi</th>
							<th>Lebih</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								$totjumlah = 0;
								$totbayar = 0;
								$totlebih = 0;
								if ($isi) {
									foreach ($isi as $row) {
										$d_dcair_cek = substr($row->d_alokasi, 0, 4) . substr($row->d_alokasi, 5, 2);
										$tmp = explode('-', $row->d_alokasi);
										$tgl = $tmp[2];
										$bln = $tmp[1];
										$thn = $tmp[0];
										$row->d_alokasi = $tgl . '-' . $bln . '-' . $thn;
										$tmp = explode('-', $row->d_kn);
										$tgl = $tmp[2];
										$bln = $tmp[1];
										$thn = $tmp[0];
										$row->d_kn = $tgl . '-' . $bln . '-' . $thn;
										echo "<tr> 
				  							<td>$row->e_area_name</td>";
										if ($row->f_alokasi_cancel == 't') {
											echo "<td><h1>$row->i_alokasi</h1></td>";
										} else {
											echo "<td>$row->i_alokasi</td>";
										}
										echo "<td>$row->i_kn</td>";
										echo "<td>$row->d_alokasi</td>";
										echo "<td>$row->e_jenis_bayarname</td>";
										echo "<td>($row->i_customer) - $row->e_customer_name</td>
												<td align=right>" . number_format($row->v_jumlah) . "</td>
												<td align=right>" . number_format($row->bayar) . "</td>
												<td align=right>" . number_format($row->v_lebih) . "</td>";
										echo "<td class=\"action\">";
										echo "<a href=\"#\" onclick='show(\"knalokasi/cform/edit/$row->i_alokasi/$row->i_area/$row->i_kn/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
										#				if( (($row->f_pelunasan_cancel!='t') && ($area1=='00')) || 
										#            (($row->f_pelunasan_cancel!='t') && ($area1!='00') && ($row->i_cek=='') && ($row->i_cek_ikhp==''))  ){
										if ((($row->f_alokasi_cancel != 't') && ($row->i_cek == '') && ($row->i_cek == ''))) {
											$query3	= $this->db->query(" select i_periode from tm_periode ");
											if ($query3->num_rows() > 0) {
												$hasilrow = $query3->row();
												$i_periode	= $hasilrow->i_periode;
												if ($i_periode <= $d_dcair_cek)
													$bisahapus = 't';
												else
													$bisahapus = 'f';
											}

											if ($bisahapus == 't') {
												echo "&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"listknalokasi/cform/delete/$row->i_alokasi/$row->i_area/$row->i_kn/$row->i_jenis_bayar/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>";
											}
											echo "</td><tr>";
										}
										echo "<input type=\"hidden\" id=\"igroupdelete\" name=\"igroupdelete\" value=\"\"><input type=\"hidden\" id=\"igroupedit\" name=\"igroupedit\" value=\"\">";
										if ($row->f_alokasi_cancel == 't') {
											$totjumlah = $totjumlah + 0;
											$totbayar = $totbayar + 0;
											$totlebih = $totlebih + 0;
										} else {
											$totjumlah = $totjumlah + $row->v_jumlah;
											$totbayar = $totbayar + $row->bayar;
											$totlebih = $totlebih + $row->v_lebih;
										}
									}
								}
								echo "	<tr>
											<td colspan='6'>Total Per Halaman</td>
											<td align='right'>" . number_format($totjumlah) . "</td>
											<td align='right'>" . number_format($totbayar) . "</td>
											<td align='right'>" . number_format($totlebih) . "</td>
											<td></td>
										</tr>";
								$totaljumlah = 0;
								$totalbayar = 0;
								$totallebih = 0;
								if ($total) {
									foreach ($total as $row) {
										if ($row->f_alokasi_cancel == 't') {
											$totaljumlah = $totaljumlah + 0;
											$totalbayar = $totalbayar + 0;
											$totallebih = $totallebih + 0;
										} else {
											$totaljumlah = $totaljumlah + $row->v_jumlah;
											$totalbayar = $totalbayar + $row->bayar;
											$totallebih = $totallebih + $row->v_lebih;
										}
									}
									echo "<tr>
	<td colspan='6'>Total Semua</td>
	<td align='right'>" . number_format($totaljumlah) . "</td>
	<td align='right'>" . number_format($totalbayar) . "</td>
	<td align='right'>" . number_format($totallebih) . "</td>
	<td></td>
	</tr>";
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
					</div>
					<marquee direction="left" scrolldelay="120" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 6, 0);">*Jika Nomor Alokasi Membesar Maka Nomor Alokasi Tersebut Telah Di Batalkan.</marquee>
				</div>
				<?= form_close() ?>
				<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('listknalokasi/cform/','#tmpx')">
			</td>
		</tr>
	</table>
</div>
<script language="javascript" type="text/javascript">
	function xxx(x, a, g) {
		if (confirm(g) == 1) {
			document.getElementById("ispbdelete").value = a;
			document.getElementById("inotadelete").value = x;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/listnota/cform/delete";
			formna.submit();
		}
	}

	function yyy(x, b) {
		document.getElementById("ispbedit").value = b;
		document.getElementById("inotaedit").value = x;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/nota/cform/edit";
		formna.submit();
	}
</script>