<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'cek-akt-kk/cform/carikendaraan','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="area" name="area" value="<?php echo $area; ?>"><input type="hidden" id="periode" name="periode" value="<?php echo $periode; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No. Pol</th>
		    <th>Jenis Kend</th>
		    <th>BBM</th>
		    <th>Pengguna</th>			
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_kendaraan','$row->e_pengguna')\">$row->i_kendaraan</a></td>
				  <td><a href=\"javascript:setValue('$row->i_kendaraan','$row->e_pengguna')\">$row->e_kendaraan_jenis</a></td>
				  <td><a href=\"javascript:setValue('$row->i_kendaraan','$row->e_pengguna')\">$row->e_kendaraan_bbm</a></td>
				  <td><a href=\"javascript:setValue('$row->i_kendaraan','$row->e_pengguna')\">$row->e_pengguna</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    document.getElementById("ikendaraan").value=a;
    document.getElementById("epengguna").value=b;
	jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
