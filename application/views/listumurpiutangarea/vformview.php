<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listumurpiutangarea/cform/export','update'=>'#main','type'=>'post'));
		$a=substr($iperiode,0,4);
	  $b=substr($iperiode,4,2);
		$periode=mbulan($b)." - ".$a;
	echo "<center><h2>LAPORAN UMUR PIUTANG PER AREA PERIODE $periode </h2></center>";?>
  <input name="cmdexport" id="cmdexport" value="Export To Excel" type="submit">
  	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
          <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
          <input type="hidden" id="dopname" name="dopname" value="<?php echo $d_opname; ?>">
	      </tr>
	    </thead>
	    <tr>
	    <th rowspan="2" align="center">No</th>
 	    <th rowspan="2" align="center">Area</th>
			<th align="center" colspan="8">Kelompok</th>
			<th rowspan="2" align="center">Total</th>
			</tr>
      <tr>
			  <th align="center" width=120px>Blm Jatuh Tempo</th>
			  <th align="center">0-15</th>
			  <th align="center">16-30</th>
			  <th align="center">31-45</th>
			  <th align="center">46-60</th>
			  <th align="center">61-75</th>
			  <th align="center">76-90</th>
			  <th align="center">>90</th>
      </tr>
<!--			<th class="action">Action</th>-->
	    <tbody>
    <?php 
		if($isi){
		$i=0;
		$j=0;
		$area='';
		$total=0;
		$grandtotal=0;
			foreach($isi as $row){
        if($area==''){
          $i++;
    	    echo "<tr>";
			    echo "<td>$i</td>
				        <td>$row->area</td>";
     	  }elseif($area!='' && $area!=$row->area){
     	    $i++;
  		    echo "<td align='right'>".number_format($total)."</td>";
     	    $total=0;
     	    echo "</tr><tr>";
			    echo "<td>$i</td>
				        <td>$row->area</td>";
     	  }
        $j++;
		    echo "<td align='right'>".number_format($row->jumlah)."</td>";
        $area=$row->area;
        $total = $total+$row->jumlah;
      }
	    echo "<td align='right'>".number_format($total)."</td>";
      echo "</tr>";
	    echo "<tr rowspan=2>";
	    echo "<th colspan=2>TOTAL AREA</th>";
      $sql = "select * from f_umur_piutang_nasional('$iperiode','$d_opname')";
      $rs		= pg_query($sql);
    	while($raw=pg_fetch_assoc($rs)){
    		$jumlah    = $raw['jumlah'];
        $grandtotal = $grandtotal + $jumlah;
        echo "<th align=right>".number_format($jumlah)."</th>";
      }
        echo "
        <th align=right>".number_format($grandtotal)."</th>";
      echo "</tr>";
	  }
    ?>
	    </tbody>
	  </table>
  	</div>
      </div>
      </div>	  
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
