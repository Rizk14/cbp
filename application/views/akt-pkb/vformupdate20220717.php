<?php 
	echo "<h2>".$page_title."</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-pkb/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="aktkbform">
	<div class="effect">
	  <div class="accordion2">
		 <?php foreach($isi as $row){ ?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Nomor</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="ipvtype" id="ipvtype" value="01"><input readonly name="ikb" id="ikb" value="<?php echo $row->i_kb; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><select readonly name="iperiodebl" id="iperiodebl">
							<?php $bl=substr($row->i_periode,4,2);?>
							<option <?php if($bl=='01') echo 'selected'; ?>>01</option>
							<option <?php if($bl=='02') echo 'selected'; ?>>02</option>
							<option <?php if($bl=='03') echo 'selected'; ?>>03</option>
							<option <?php if($bl=='04') echo 'selected'; ?>>04</option>
							<option <?php if($bl=='05') echo 'selected'; ?>>05</option>
							<option <?php if($bl=='06') echo 'selected'; ?>>06</option>
							<option <?php if($bl=='07') echo 'selected'; ?>>07</option>
							<option <?php if($bl=='08') echo 'selected'; ?>>08</option>
							<option <?php if($bl=='09') echo 'selected'; ?>>09</option>
							<option <?php if($bl=='10') echo 'selected'; ?>>10</option>
							<option <?php if($bl=='11') echo 'selected'; ?>>11</option>
							<option <?php if($bl=='12') echo 'selected'; ?>>12</option>
						</select>&nbsp;<input readonly name="iperiodeth" id="iperiodeth" value="<?php echo substr($row->i_periode,0,4);?>" maxlength="4"></td>
	      </tr>
		<tr>
		<?php 
			$tmp=explode('-',$row->d_kb);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_kb=$tgl.'-'.$bln.'-'.$thn;
		?>
		<td width="12%">Tanggal</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dkb" id="dkb" value="<?php echo $row->d_kb; ?>" onclick="showCalendar('',this,this,'','dkb',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">CoA</td>
		<td width="1%">:</td>
		<td width="87%">
      <input type="text" name="icoa" id="icoa" value="<?php echo $row->i_coa; ?>" onclick='showModal("akt-pkb/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Ket CoA</td>
		<td width="1%">:</td>
		<td width="87%">
      <input readonly name="ecoaname" id="ecoaname" value="<?php echo $row->e_coa_name; ?>"  onclick='showModal("akt-pkb/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
		<?php 
		  if($row->d_bukti!=''){
			  $tmp=explode('-',$row->d_bukti);
			  $tgl=$tmp[2];
			  $bln=$tmp[1];
			  $thn=$tmp[0];
			  $row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<tr>
		<td width="12%">Tanggal Bukti</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dbukti" id="dbukti" value="<?php echo $row->d_bukti; ?>" onclick="showCalendar('',this,this,'','dbukti',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>">
			<input type="hidden" name="iareaold" id="iareaold" value="<?php echo $row->i_area; ?>">
			<input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>"></td>
<!--  onclick='showModal("akt-pkb/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'-->
	      </tr>
	      <tr>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="edescription" id="edescription" value="<?php echo $row->e_description; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="87%"><input name="vkb" id="vkb" value="<?php echo number_format($row->v_kb); ?>" onkeyup="reformat(this);"></td>
	      </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		<?php if($row->i_cek==''){
			if($row->f_kb_cancel!='t'){?>
				<input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
			<?php } 
		}?>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('list-akt-kb/cform/view/<?php echo $dfrom.'/'.$dto.'/'.$iarea; ?>','#main')">
		</td>
	      </tr>
		</table>
		<?php }?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("iarea").value=='') ||
			(document.getElementById("dkb").value=='') ||
			(document.getElementById("icoa").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function kendaraan()
	{
		area=document.getElementById("iarea").value;
		periode=document.getElementById("iperiodeth").value+document.getElementById("iperiodebl").value;
		showModal("akt-pkb/cform/kendaraan/"+area+"/"+periode+"/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function cektanggal()
  {
		dbukti=document.getElementById('dbukti'+i).value;
    dkb=document.getElementById('dkb').value;
	  dtmp=dbukti.split('-');
	  thnbk=dtmp[2];
		blnbk=dtmp[1];
		hrbk =dtmp[0];
	  dtmp=dkb.split('-');
	  thnkb=dtmp[2];
		blnkb=dtmp[1];
		hrkb =dtmp[0];
	  if( thnbk>thnkb ){
			alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
	  }else if (thnbk==thnkb){
      if( blnbk>blnkb ){
				alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
			}else if( blnbk==blnkb ){
        if( hrbk>hrkb ){
					alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
					document.getElementById('dbukti').value='';
				}
      }
    }
	}
</script>
