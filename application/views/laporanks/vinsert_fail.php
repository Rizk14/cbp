<?php 
$this->load->view('header');
?>
<h2>
<?php 
echo $page_title;
?></h2>
<p class="error">
<?php 
echo $this->lang->line('laporanks_wrong_input');
?></p>
<?php 
$this->load->view('laporanks/vform');
$this->load->view('footer');
?>
