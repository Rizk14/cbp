<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'list-akt-kb/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Area</th>
	   	    <th>No kb</th>
			<th>Tgl kb</th>
			<th>No Vch</th>
			<th>CoA</th>
			<th>Keterangan</th>
			<th>Nilai</th>
			<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			$i_periode = $this->db->query("select i_periode from tm_periode")->row()->i_periode;
			foreach($isi as $row){
				$tmp=explode('-',$row->d_kb);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_kb=$tgl.'-'.$bln.'-'.$thn;
				#$row->d_kbb=$thn.'-'.$bln.'-'.$tgl;
				$dkb = $thn.$bln.$tgl;
				if($i_periode <= $thn.$bln){
					$bisaedit = true;
				}else{
					$bisaedit = false;
				}
			  echo "<tr> 
				  <td>$row->i_area - ".substr($row->e_area_name,0,3)."</td>";
				  if($row->f_kb_cancel == 't') {
					echo "<td><h1>$row->i_kb</h1></td>";
				  }else {
					echo "<td>$row->i_kb</td>";
				  }
				echo"
				  <td>$row->d_kb</td>
				  <td>$row->vc</td>
				  <td>$row->i_coa</td>
				  <td>$row->e_description</td>
				  <td align=right>".number_format($row->v_kb)."</td>
				  <td class=\"action\">";
			  if($row->f_posting!='t'){
				  if($row->f_debet=='t'){
					if ($dkb>=$d_open_kb) {
						echo "<a href=\"#\" onclick='show(\"akt-kb/cform/edit/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					  	if($row->i_cek=='' && $bisaedit == true && $row->f_kb_cancel == 'f') echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-kb/cform/delete/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					}
				  }else{
					if ($dkb>=$d_open_kbin) {
						echo "<a href=\"#\" onclick='show(\"akt-pkb/cform/edit/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						if($row->i_cek=='' && $bisaedit == true && $row->f_kb_cancel == 'f') echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-kb/cform/delete/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					}
				  }
			  }else{
				  if($row->f_debet=='t'){
					if ($dkb>=$d_open_kb) {
						echo "<a href=\"#\" onclick='show(\"akt-kb/cform/edit/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				  }else{
					if ($dkb>=$d_open_kbin) {
						echo "<a href=\"#\" onclick='show(\"akt-pkb/cform/edit/$row->i_kb/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				  }
			  }
			  echo "
				  </td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  <?php echo "<center>"/*.$this->paginationxx->create_links()."</center>"; */?> 
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

</script>
