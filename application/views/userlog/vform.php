<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url'=>'userlog/cform/view','update'=>'#main','type'=>'post'));?>
	<div id="spbperareaform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Date From</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dfrom',
			              'id'          => 'dfrom',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Date To</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dto',
			              'id'          => 'dto',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="View" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('userlog/cform/','#tmpx')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
  function TambahHari(theDate, numDays) {
	  numDays = parseInt(numDays);
	  if(theDate.indexOf("/") !== -1) var seperator = '/';
	  else if(theDate.indexOf("-") !== -1) var seperator = '-';
	  else return false;
	  var parts = theDate.split(seperator);
	  var month = parts[1];
	  if(parts[2].length==4) {
		  var day = parts[0];
		  var year = parts[2];
	  } else if(parts[0].length==4) {
		  var day = parts[2];
		  var year = parts[0];
	  }
	  var date = new Date(year, month-1, day);
	  var newdate = new Date(date);
	  newdate.setDate(newdate.getDate() + numDays);
	  var dd = newdate.getDate();
	  var mm = newdate.getMonth() + 1;
	  var y = newdate.getFullYear();
	  return dd + '-'+ mm + '-'+ y;
  }

  function afterSetDateValue(ref_field, target_field, date) {
    alert(date);
    var date2 = TambahHari(date, 2);
    alert(date2);
  }
</script>
