<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>

<body id="bodylist">
	<div id="main">
		<div id="tmpx">
			<?php 
			$query = $this->db->query(" select e_customer_name from tr_customer where i_customer='$icustomer'");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$nama = $tmp->e_customer_name;
				}
			}
			?>
			<table class="maintable" id="sitabel">
				<tr>
					<td align="left">
						<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'kartupiutang/cform/detail', 'update' => '#light', 'type' => 'post')); ?> -->
						<div id="listform">
							<div class="effect">
								<div class="accordion2">
									<?php echo "<center><h2>$page_title ($icustomer-$nama) Periode $eperiode</h2></center>"; ?>
									<table class="listtable">
										<th>Tanggal</th>
										<th>No Bukti</th>
										<th>Keterangan</th>
										<th>Penjualan</th>
										<th>Alokasi</th>
										<th>Saldo Akhir</th>
										<tbody>
											<?php 
											$saldoakhir = $saldo;
											echo "<tr> 
													<td></td>
													<td></td>
													<td>Saldo</td>
													<td align=right>0</td>
													<td align=right>0</td>
													<td align=right>" . number_format($saldoakhir) . "</td>
												</tr>";
											if ($isi) {
												foreach ($isi as $row) {
													$tmp 	= explode("-", $row->d_nota);
													$det	= $tmp[2];
													$mon	= $tmp[1];
													$yir 	= $tmp[0];
													$row->d_nota	= $det . "-" . $mon . "-" . $yir;
													if ($row->status == 'Penjualan') {
														$saldoakhir = $saldoakhir + $row->debet;
													} else {
														$saldoakhir = $saldoakhir - $row->kredit;
													}
													echo "<tr> 
															<td>$row->d_nota</td>
															<td>$row->i_nota</td>
															<td>$row->status</td>
															<td align=right>" . number_format($row->debet) . "</td>
															<td align=right>" . number_format($row->kredit) . "</td>
															<td align=right>" . number_format($saldoakhir) . "</td>
														</tr>";
												}
											}
											?>
										</tbody>
									</table>
									<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
									<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
									<br>
									<center><input type="button" id="batal" name="batal" value="Keluar" onclick="windowClose()"></center>
								</div>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
	function windowClose() {
		window.open('', '_parent', '');
		window.close();
	}

	$("#cmdreset").click(function() {
		//getting values of current time for generating the file name
		var dt = new Date();
		var day = dt.getDate();
		var month = dt.getMonth() + 1;
		var year = dt.getFullYear();
		var hour = dt.getHours();
		var mins = dt.getMinutes();
		var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
		//creating a temporary HTML link element (they support setting file names)
		var a = document.createElement('a');
		//getting data from our div that contains the HTML table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = $('#sitabel').html();
		// var table_html = table_div.outerHTML.replace(/ /g, '%20');
		a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

		//setting the file name
		// var nofaktur = document.getElementById('no_faktur').value;
		a.download = 'export_detail_kartupiutang_' + postfix + '.xls';
		//triggering the function
		a.click();
		//just in case, prevent default behaviour

		// var Contents = $('#sitabel').html();
		// window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
	});
</script>