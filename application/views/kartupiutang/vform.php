<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'kartupiutang/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Pilihan</td>
								<td width="1%">:</td>
								<td width="80%">Customer&nbsp;
									<input type='checkbox' name='chknt' id='chknt' value='xxx' onclick='muter1();'>
									<input type='hidden' name='chkntx' id='chkntx' value='xxx'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									Area&nbsp;
									<input type='checkbox' name='chkjt' id='chkjt' value='xxx' onclick='muter2();'>
									<input type='hidden' name='chkjtx' id='chkjtx' value='xxx'>
								</td>
							</tr>
							<tr>
								<td width="19%">Periode</td>
								<td width="1%">:</td>
								<td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="">
									<select name="bulan" id="bulan" onmouseup="buatperiode()">
										<option></option>
										<option value='01'>Januari</option>
										<option value='02'>Februari</option>
										<option value='03'>Maret</option>
										<option value='04'>April</option>
										<option value='05'>Mei</option>
										<option value='06'>Juni</option>
										<option value='07'>Juli</option>
										<option value='08'>Agustus</option>
										<option value='09'>September</option>
										<option value='10'>Oktober</option>
										<option value='11'>November</option>
										<option value='12'>Desember</option>
									</select>
									<select name="tahun" id="tahun" onMouseUp="buatperiode()">
										<option></option>
										<?php 
										$tahun1 = date('Y') - 3;
										$tahun2 = date('Y');
										for ($i = $tahun1; $i <= $tahun2; $i++) {
											echo "<option value='$i'>$i</option>";
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="19%">Area</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("kartupiutang/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<input name="login" id="login" value="View" type="submit" onclick="return dipales()">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('kartupiutang/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales() {
		iperiode = document.getElementById("iperiode").value;
		iarea = document.getElementById("iarea").value;
		chkntx = document.getElementById("chkntx").value;
		chkjtx = document.getElementById("chkjtx").value;

		if (chkntx == "xxx" && chkjtx == "xxx") {
			alert("Pilih Salah Satu Per Customer / Per Area!!!");
			return false;
		} else if (iperiode == '') {
			alert("Periode Harus dipilih!!!");
			return false;
		} else if (iarea == "") {
			alert("Pilih Area!!!");
			return false;
		} else {
			return true;
		}
	}

	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}

	function muter1() {
		nt = document.getElementById("chknt");
		if (nt.checked) {
			document.getElementById("chkjt").checked = false;
			document.getElementById("chkjtx").value = 'xxx';
			document.getElementById("chkntx").value = 'qqq';
		} else {
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'xxx';
		}
	}

	function muter2() {
		jt = document.getElementById("chkjt");
		if (jt.checked) {
			document.getElementById("chknt").checked = false;
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'qqq';
		} else {
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'xxx';
		}
	}
</script>