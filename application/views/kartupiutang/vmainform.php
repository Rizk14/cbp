<?php 
if (($iarea == '') || ($iperiode == '')) {
	$this->load->view('kartupiutang/vform');
} else {
	if ($chkntx == 'qqq') {
		$data['cari']		= $cari;
		$data['isi']		= $isi;
		$data['iarea']		= $iarea;
		$data['eareaname']	= $eareaname;
		$data['iperiode']	= $iperiode;
		$data['chkntx']		= $chkntx;
		$data['chkjtx']		= $chkjtx;
		$this->load->view('kartupiutang/vformview', $data);
	} else {
		$data['cari']		= $cari;
		$data['isi']		= $isi;
		$data['iarea']		= $iarea;
		$data['iperiode']	= $iperiode;
		$this->load->view('kartupiutang/vformviewarea', $data);
	}
}
