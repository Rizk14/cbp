  <div id='tmpx'>
  	<h2><?php if ($chkntx == "qqq") {
				$by = " Per Customer ";
			} else {
				$by = " Per Area ";
			}
			echo $page_title . $by . ' Periode : ' . $eperiode; ?></h2>
  	<table class="maintable">
  		<tr>
  			<td align="left">
  				<?php echo $this->pquery->form_remote_tag(array('url' => 'kartupiutang/cform/view', 'update' => '#main', 'type' => 'post')); ?>
  				<div class="effect">
  					<div class="accordion2">
  						<table class="listtable" id="sitabel">
  							<thead>
  								<tr>
  									<td colspan="12">
  										Periode :
  										<input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
  										<select name="bulan" id="bulan" onchange="buatperiode()">
  											<option value='01' <?php if ($bln == '01') {
																		echo "selected";
																	} ?>>Januari</option>
  											<option value='02' <?php if ($bln == '02') {
																		echo "selected";
																	} ?>>Februari</option>
  											<option value='03' <?php if ($bln == '03') {
																		echo "selected";
																	} ?>>Maret</option>
  											<option value='04' <?php if ($bln == '04') {
																		echo "selected";
																	} ?>>April</option>
  											<option value='05' <?php if ($bln == '05') {
																		echo "selected";
																	} ?>>Mei</option>
  											<option value='06' <?php if ($bln == '06') {
																		echo "selected";
																	} ?>>Juni</option>
  											<option value='07' <?php if ($bln == '07') {
																		echo "selected";
																	} ?>>Juli</option>
  											<option value='08' <?php if ($bln == '08') {
																		echo "selected";
																	} ?>>Agustus</option>
  											<option value='09' <?php if ($bln == '09') {
																		echo "selected";
																	} ?>>September</option>
  											<option value='10' <?php if ($bln == '10') {
																		echo "selected";
																	} ?>>Oktober</option>
  											<option value='11' <?php if ($bln == '11') {
																		echo "selected";
																	} ?>>November</option>
  											<option value='12' <?php if ($bln == '12') {
																		echo "selected";
																	} ?>>Desember</option>
  										</select>
  										<select name="tahun" id="tahun" onMouseUp="buatperiode()">
  											<?php 
												$tahun1 = date('Y') - 3;
												$tahun2 = date('Y');
												for ($i = $tahun1; $i <= $tahun2; $i++) {
													echo "<option value='$i'";
													if ($thn == $i) {
														echo "selected";
													}
													echo " >$i</option>";
												}
												?>
  										</select>
  										Area :
  										<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
  										<input type="text" id="eareaname" name="eareaname" value="<?php echo $eareaname; ?>" onclick='showModal("kartupiutang/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  										&nbsp;&nbsp;
  										Cari Data :
  										<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" placeholder="Kd Area / Nm Area">
  										<input type="hidden" id="chkntx" name="chkntx" value="<?php echo $chkntx; ?>">
  										<input type="hidden" id="chkjtx" name="chkjtx" value="<?php echo $chkjtx; ?>">
  										<input type="submit" id="bcari" name="bcari" value="Cari">
  									</td>
  								</tr>
  							</thead>
  							<th>No</th>
  							<th>Kode Area</th>
  							<th>Nama Area</th>
  							<th>Saldo Awal</th>
  							<th>Penjualan</th>
  							<th>Alokasi BM</th>
  							<th>Alokasi KN Retur</th>
  							<th>Alokasi KN Non Retur</th>
  							<th>Alokasi Hutang Lain-Lain</th>
  							<th>Pembulatan</th>
  							<th>Saldo Akhir</th>
  							<tbody>
  								<?php 
									if ($isi) {
										$no = 0;
										$totakhir = 0;
										$totsa = 0;
										$totpenjualan = 0;
										$totalokasi_bm = 0;
										$totalokasi_knr = 0;
										$totalokasi_kn = 0;
										$totalokasi_hll = 0;
										$totalokasi_pembulatan = 0;
										foreach ($isi as $row) {
											if ($row->saldo_awal > 0 || $row->penjualan > 0) {
												$no++;
												$totsa = $totsa + $row->saldo_awal;
												$totpenjualan = $totpenjualan + $row->penjualan;
												$totalokasi_bm = $totalokasi_bm + $row->alokasi_bm;
												$totalokasi_knr = $totalokasi_knr + $row->alokasi_knr;
												$totalokasi_kn = $totalokasi_kn + $row->alokasi_kn;
												$totalokasi_hll = $totalokasi_hll + $row->alokasi_hll;
												$totalokasi_pembulatan = $totalokasi_pembulatan + $row->pembulatan;
												$totakhir = $totakhir + $row->saldo_akhir;

												echo "<tr>
														<td align=center>$no</td>
														<td align=center>'" . $row->i_area . "</td>
														<td>$row->e_area_name</td>
														<td align=right>" . number_format($row->saldo_awal) . "</td>
														<td align=right>" . number_format($row->penjualan) . "</td>
														<td align=right>" . number_format($row->alokasi_bm) . "</td>
														<td align=right>" . number_format($row->alokasi_knr) . "</td>
														<td align=right>" . number_format($row->alokasi_kn) . "</td>
														<td align=right>" . number_format($row->alokasi_hll) . "</td>
														<td align=right>" . number_format($row->pembulatan) . "</td>
														<td align=right>" . number_format($row->saldo_akhir) . "</td>
													</tr>";
											}
										}
										echo "<tr>
													<td align=right colspan=3>Total</td>
													<td align=right>" . number_format($totsa) . "</td>
													<td align=right>" . number_format($totpenjualan) . "</td>
													<td align=right>" . number_format($totalokasi_bm) . "</td>
													<td align=right>" . number_format($totalokasi_knr) . "</td>
													<td align=right>" . number_format($totalokasi_kn) . "</td>
													<td align=right>" . number_format($totalokasi_hll) . "</td>
													<td align=right>" . number_format($totalokasi_pembulatan) . "</td>
													<td align=right>" . number_format($totakhir) . "</td>
												</tr>";
									}
									?>
  							</tbody>
  						</table>
  						<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  						<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('kartupiutang/cform/','#main')">
  						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
  					</div>
  				</div>
  				<?= form_close() ?>
  			</td>
  		</tr>
  	</table>
  </div>
  <script language="javascript" type="text/javascript">
  	$(document).ready(function() {
  		$('#bulan').change(function() {
  			$('#bcari').click();
  		});
  	});

  	function buatperiode() {
  		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
  		document.getElementById("iperiode").value = periode;
  	}

  	/* function yyy(b, d, c) {
  		showModal("kartupiutang/cform/detail/" + b + "/" + c + "/" + d + "/", "#light");
  		jsDlgShow("#konten *", "#fade", "#light");
  	} */
  	$("#cmdreset").click(function() {
  		//getting values of current time for generating the file name
  		var dt = new Date();
  		var day = dt.getDate();
  		var month = dt.getMonth() + 1;
  		var year = dt.getFullYear();
  		var hour = dt.getHours();
  		var mins = dt.getMinutes();
  		var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
  		//creating a temporary HTML link element (they support setting file names)
  		var a = document.createElement('a');
  		//getting data from our div that contains the HTML table
  		var data_type = 'data:application/vnd.ms-excel';
  		var table_div = $('#sitabel').html();
  		// var table_html = table_div.outerHTML.replace(/ /g, '%20');
  		a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

  		//setting the file name
  		// var nofaktur = document.getElementById('no_faktur').value;
  		a.download = 'export_kartupiutang_area_' + postfix + '.xls';
  		//triggering the function
  		a.click();
  		//just in case, prevent default behaviour

  		// var Contents = $('#sitabel').html();
  		// window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
  	});
  </script>