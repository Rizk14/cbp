<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customersalesman/cform/update','update'=>'#main','type'=>'post'));?>
	<div id="mastercustomersalesmanformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Area</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name; ?>" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area; ?>" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="ecustomername" id="ecustomername" value="<?php echo $isi->e_customer_name; ?>" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="icustomer" id="icustomer" value="<?php echo $isi->i_customer; ?>" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Salesman</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="esalesmanname" id="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>" 
				 onclick='showModal("customersalesman/cform/salesman/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="isalesman" id="isalesman" value="<?php echo $isi->i_salesman; ?>" 
				 onclick='showModal("customersalesman/cform/salesman/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Jenis</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="eproductgroupname" id="eproductgroupname" value="<?php echo $isi->e_product_groupname; ?>" 
				 onclick='showModal("customersalesman/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="iproductgroup" id="iproductgroup" value="<?php echo $isi->i_product_group; ?>" 
				 onclick='showModal("customersalesman/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
        </tr>
	      <tr>
		      <td width="19%">Periode</td>
		      <td width="1%">:</td>
		      <td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="">
                  <?php 
                     $thn=substr($per,0,4);
                     $bln=substr($per,4,2);
                  ?>
						      <select name="bulan" id="bulan" onmouseup="buatperiode()">
							      <option></option>
							      <option <?php if($bln=='01') echo 'selected'; ?>>Januari</option>
							      <option <?php if($bln=='02') echo 'selected'; ?>>Pebruari</option>
							      <option <?php if($bln=='03') echo 'selected'; ?>>Maret</option>
							      <option <?php if($bln=='04') echo 'selected'; ?>>April</option>
							      <option <?php if($bln=='05') echo 'selected'; ?>>Mei</option>
							      <option <?php if($bln=='06') echo 'selected'; ?>>Juni</option>
							      <option <?php if($bln=='07') echo 'selected'; ?>>Juli</option>
							      <option <?php if($bln=='08') echo 'selected'; ?>>Agustus</option>
							      <option <?php if($bln=='09') echo 'selected'; ?>>September</option>
							      <option <?php if($bln=='10') echo 'selected'; ?>>Oktober</option>
							      <option <?php if($bln=='11') echo 'selected'; ?>>November</option>
							      <option <?php if($bln=='12') echo 'selected'; ?>>Desember</option>
						      </select>
						      <select name="tahun" id="tahun" onMouseUp="buatperiode()">
							      <option></option>
							      <option <?php if($thn=='2009') echo 'selected'; ?>>2009</option>
							      <option <?php if($thn=='2010') echo 'selected'; ?>>2010</option>
							      <option <?php if($thn=='2011') echo 'selected'; ?>>2011</option>
							      <option <?php if($thn=='2012') echo 'selected'; ?>>2012</option>
							      <option <?php if($thn=='2013') echo 'selected'; ?>>2013</option>
							      <option <?php if($thn=='2014') echo 'selected'; ?>>2014</option>
							      <option <?php if($thn=='2015') echo 'selected'; ?>>2015</option>
						      </select>
			      </td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('customersalesman/cform/','#main');">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script languge=javascript type=text/javascript>
  function buatperiode(){
	  periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
	  document.getElementById("iperiode").value=periode;
  }
</script>
