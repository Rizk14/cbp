<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Salesman per Pelanggan</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Salesman per Pelanggan</a></li>  
	  </ul>  
    <div id="content_1" class="content">
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customersalesman/cform','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Area</th>
	    <th>Pelanggan</th>
	    <th>Salesman</th>
	    <th>Jenis</th>
	    <th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td>$row->e_area_name</td>
				  <td>$row->i_customer - $row->e_customer_name</td>
				  <td>$row->i_salesman - $row->e_salesman_name</td>";
          if($row->i_product_group=='00')
  				  echo "<td>Reguler</td>";
          elseif($row->i_product_group=='01')
            echo "<td>BABY BEDDING</td>";
          else
            echo "<td>BABY NON BEDDING</td>";
			  echo "<td class=\"action\"><a href=\"#\" onclick='show(\"customersalesman/cform/edit/$row->i_area/$row->i_customer/$row->i_salesman/$row->i_product_group/$row->e_periode\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;";
			  echo "<a href=\"#\" onclick='hapus(\"customersalesman/cform/delete/$row->i_area/$row->i_customer/$row->i_salesman/$row->i_product_group\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
  </table>
  </div>
  <div id="content_2" class="content">
  <table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customersalesman/cform/simpan','update'=>'#main','type'=>'post'));?>
	<div id="mastercustomersalesmanform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Area</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="eareaname" id="eareaname" value="" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="iarea" id="iarea" value="" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="ecustomername" id="ecustomername" value="" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="icustomer" id="icustomer" value="" 
				 onclick='showModal("customersalesman/cform/customerarea/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Salesman</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="esalesmanname" id="esalesmanname" value="" 
				 onclick='showModal("customersalesman/cform/salesman/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="isalesman" id="isalesman" value="" 
				 onclick='showModal("customersalesman/cform/salesman/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="19%">Jenis</td>
		<td width="1%">:</td>
		<td width="80%"><input readonly name="eproductgroupname" id="eproductgroupname" value="" 
				 onclick='showModal("customersalesman/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="iproductgroup" id="iproductgroup" value="" 
				 onclick='showModal("customersalesman/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		      <td width="19%">Periode</td>
		      <td width="1%">:</td>
		      <td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="">
						      <select name="bulan" id="bulan" onmouseup="buatperiode()">
							      <option></option>
							      <option value='01'>Januari</option>
							      <option value='02'>Pebruari</option>
							      <option value='03'>Maret</option>
							      <option value='04'>April</option>
							      <option value='05'>Mei</option>
							      <option value='06'>Juni</option>
							      <option value='07'>Juli</option>
							      <option value='08'>Agustus</option>
							      <option value='09'>September</option>
							      <option value='10'>Oktober</option>
							      <option value='11'>November</option>
							      <option value='12'>Desember</option>
						      </select>
						      <select name="tahun" id="tahun" onMouseUp="buatperiode()">
							      <option></option>
							      <option value='2009'>2009</option>
							      <option value='2010'>2010</option>
							      <option value='2011'>2011</option>
							      <option value='2012'>2012</option>
							      <option value='2013'>2013</option>
							      <option value='2014'>2014</option>
							      <option value='2015'>2015</option>
						      </select>
			      </td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('customersalesman/cform/','#main');">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
  </table>
	<div id="pesan"></div>
</div>
</div>
<script languge=javascript type=text/javascript>
  function buatperiode(){
	  periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
	  document.getElementById("iperiode").value=periode;
  }
</script>
