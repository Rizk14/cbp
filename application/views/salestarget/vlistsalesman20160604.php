<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('salestarget/cform/carisalesman', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="2" align="center">Cari data : 	<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
													<input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
													<input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Sales</th>
	    	<th>Nama Sales</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  if($row->v_target==''){
				$row->v_target='0';
			  }
			  if($row->n_target==''){
				$row->n_target='0';
			  }
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_salesman','$row->e_salesman_name','$row->v_target','$row->n_target')\">$row->i_salesman</a></td>
				  <td><a href=\"javascript:setValue('$row->i_salesman','$row->e_salesman_name','$row->v_target','$row->n_target')\">$row->e_salesman_name</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d)
  {
    opener.document.getElementById("isalesman").value=a;
    opener.document.getElementById("esalesmanname").value=b;
    opener.document.getElementById("vsalesmantarget").value=formatcemua(c);
    opener.document.getElementById("htargetsalesman").value=c;
    opener.document.getElementById("nsalesmantarget").value=formatcemua(d);
    opener.document.getElementById("hntargetsalesman").value=d;
    this.close();
  }
</script>
