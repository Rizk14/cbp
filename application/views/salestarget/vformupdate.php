<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('salestarget/cform/update', array('id' => 'salestargetform'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="9%">Periode</td>
		<td width="1%">:</td>
		<td width="90%"><input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
						<?php 
							$th=substr($iperiode,0,4);
							$bl=substr($iperiode,4,2);
						?>
						<select disabled=true name="bulan" id="bulan" onmouseup="buatperiode()">
							<option></option>
							<option value='01' <?php if($bl=='01') echo 'selected'; ?>>Januari</option>
							<option value='02' <?php if($bl=='02') echo 'selected'; ?>>Pebruari</option>
							<option value='03' <?php if($bl=='03') echo 'selected'; ?>>Maret</option>
							<option value='04' <?php if($bl=='04') echo 'selected'; ?>>April</option>
							<option value='05' <?php if($bl=='05') echo 'selected'; ?>>Mei</option>
							<option value='06' <?php if($bl=='06') echo 'selected'; ?>>Juni</option>
							<option value='07' <?php if($bl=='07') echo 'selected'; ?>>Juli</option>
							<option value='08' <?php if($bl=='08') echo 'selected'; ?>>Agustus</option>
							<option value='09' <?php if($bl=='09') echo 'selected'; ?>>September</option>
							<option value='10' <?php if($bl=='10') echo 'selected'; ?>>Oktober</option>
							<option value='11' <?php if($bl=='11') echo 'selected'; ?>>November</option>
							<option value='12' <?php if($bl=='12') echo 'selected'; ?>>Desember</option>
						</select>
<!--
						<select disabled=true name="tahun" id="tahun" onMouseUp="buatperiode()">
							<option></option>
							<option value='2009' <?php #if($th=='2009') echo 'selected'; ?>>2009</option>
							<option value='2010' <?php #if($th=='2010') echo 'selected'; ?>>2010</option>
							<option value='2011' <?php #if($th=='2011') echo 'selected'; ?>>2011</option>
							<option value='2012' <?php #if($th=='2012') echo 'selected'; ?>>2012</option>
							<option value='2013' <?php #if($th=='2013') echo 'selected'; ?>>2013</option>
							<option value='2014' <?php #if($th=='2014') echo 'selected'; ?>>2014</option>
							<option value='2015' <?php #if($th=='2015') echo 'selected'; ?>>2015</option>
						</select>
-->
            <select name="tahun" id="tahun" onMouseUp="buatperiode()">
							<option></option>
                     <?php 
                        $tahun1 = date('Y')-3;
                        $tahun2 = date('Y');
                        for($i=$tahun1;$i<=$tahun2;$i++)
                        {
                           echo "<option value='$i' ";
                           if($th=='$i') echo 'selected'; 
                           echo ">$i</option>";
                        }
                     ?>
						</select>		</td>
	      </tr>
	      <tr>
		<td width="9%">Area</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'iarea',
			              'id'          => 'iarea',
			              'readonly'    => 'true',
			              'value'       => $iarea);
				echo form_input($data);
				$data = array(
			              'name'        => 'eareaname',
			              'id'          => 'eareaname',
			              'readonly'    => 'true',
			              'value'       => $eareaname);
				echo form_input($data);
				$data = array(
			              'name'        => 'vareatarget',
			              'id'          => 'vareatarget',
			              'value'       => number_format($targetarea),
						  'readonly'	=> 'true');
				echo "&nbsp;Target&nbsp;".form_input($data);?>
<!--				<input type="hidden" id="htargetarea" name="htargetarea" value="<?php #echo $targetarea; ?>">-->
			</td>
	      </tr>
	      <tr>
		<td width="9%">Salesman</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'isalesman',
			              'id'          => 'isalesman',
			              'readonly'    => 'true',
			              'value'       => $isalesman);
				echo form_input($data);
				$data = array(
			              'name'        => 'esalesmanname',
			              'id'          => 'esalesmanname',
			              'readonly'    => 'true',
						  'value'       => $esalesmanname);
				echo form_input($data);
				$data = array(
			              'name'        => 'vsalesmantarget',
			              'id'          => 'vsalesmantarget',
			              'value'       => number_format($targetsales),
						  'readonly'	=> 'true');
				echo "&nbsp;Target&nbsp;".form_input($data);?>
<!--				<input type="hidden" id="htargetsalesman" name="htargetsalesman" value="<?php #echo $targetsales; ?>">-->
			</td>
	      </tr>
	      <tr>
		<td width="9%">Kota</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'icity',
			              'id'          => 'icity',
			              'value'       => $icity,
			              'readonly'    => 'true');
				echo form_input($data);
				$data = array(
			              'name'        => 'ecityname',
			              'id'          => 'ecityname',
			              'value'       => $ecityname,
			              'readonly'    => 'true');
				echo form_input($data);
				$data = array(
			              'name'        => 'vcitytarget',
			              'id'          => 'vcitytarget',
			              'value'       => number_format($targetkota),
						  'onkeyup'		=> 'reformat(this); hitung();');
				echo "&nbsp;Target&nbsp;".form_input($data);?>
<!--				<input type="hidden" id="htargetkota" name="htargetkota" value="<?php #echo $targetkota; ?>">-->
			</td>
	      </tr>
	      <tr>
		<td width="9%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="90%">
		  <input name="login" id="login" value="Simpan" type="submit" onclick='dipales();'>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("salestarget/cform/","#main")'>
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("bulan").value=='')||
			(document.getElementById("tahun").value=='')||
			(document.getElementById("iarea").value=='')||
			(document.getElementById("isalesman").value=='')||
			(document.getElementById("icity").value=='')||
			(document.getElementById("vcitytarget").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function view_area(){
		lebar =450;
		tinggi=400;
		periode=document.getElementById("iperiode").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/area/"+periode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function view_salesman(){
		lebar =450;
		tinggi=400;
		periode=document.getElementById("iperiode").value;
		area=document.getElementById("iarea").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/salesman/"+area+"/"+periode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function view_city(){
		lebar 	= 450;
		tinggi	= 400;
		periode	= document.getElementById("iperiode").value;
		area	= document.getElementById("iarea").value;
		salesman= document.getElementById("isalesman").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/city/"+area+"/"+periode+"/"+salesman,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function buatperiode(){
		periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
		document.getElementById("iperiode").value=periode;
	}
	function hitung(){
		vtarea			= parseFloat(formatulang(document.getElementById("vareatarget").value));
		vtsalesman		= parseFloat(formatulang(document.getElementById("vsalesmantarget").value));
		vtcity			= parseFloat(formatulang(document.getElementById("vcitytarget").value));
		vasalarea		= parseFloat(formatulang(document.getElementById("htargetarea").value));
		vasalsalesman	= parseFloat(formatulang(document.getElementById("htargetsalesman").value));
		vasalcity		= parseFloat(formatulang(document.getElementById("htargetkota").value));
		vtsalesman		= (vasalsalesman+vtcity)-vasalcity;
		vtarea			= (vasalarea+vtcity)-vasalcity;
		document.getElementById("vareatarget").value	= formatcemua(vtarea);
		document.getElementById("vsalesmantarget").value= formatcemua(vtsalesman);
	}
	function kembali(){
		periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
		iarea=document.getElementById("iarea").value;
		self.location="<?php echo site_url(); ?>"+"/listtppercity/cform/view/"+periode+"/"+iarea;
	}
</script>
