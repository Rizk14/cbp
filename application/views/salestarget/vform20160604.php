<table class="maintable">
  <tr>
    <td align="left">
    <!--<?php echo form_open('salestarget/cform', array('id' => 'salestargetform'));?>-->
	 <?php echo $this->pquery->form_remote_tag(array('url'=>'salestarget/cform','update'=>'#pesan','type'=>'post'));?>
	<div id="salestargetform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="9%">Periode</td>
		<td width="1%">:</td>
		<td width="90%"><input type="hidden" id="iperiode" name="iperiode" value="">
						<select name="bulan" id="bulan" onmouseup="buatperiode()">
							<option></option>
							<option value='01'>Januari</option>
							<option value='02'>Pebruari</option>
							<option value='03'>Maret</option>
							<option value='04'>April</option>
							<option value='05'>Mei</option>
							<option value='06'>Juni</option>
							<option value='07'>Juli</option>
							<option value='08'>Agustus</option>
							<option value='09'>September</option>
							<option value='10'>Oktober</option>
							<option value='11'>November</option>
							<option value='12'>Desember</option>
						</select>
						<select name="tahun" id="tahun" onMouseUp="buatperiode()">
							<option></option>
                     <?php 
                        $tahun1 = date('Y')-3;
                        $tahun2 = date('Y');
                        for($i=$tahun1;$i<=$tahun2;$i++)
                        {
                           echo "<option value='$i'>$i</option>";
                        }
                     ?>
						</select>

		</td>
	      </tr>
	      <tr>
		<td width="9%">Area</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'iarea',
			              'id'          => 'iarea',
			              'readonly'    => 'true',
			              'value'       => '',
					      'onclick'	    => 'view_area();');
				echo form_input($data);
				$data = array(
			              'name'        => 'eareaname',
			              'id'          => 'eareaname',
			              'readonly'    => 'true',
			              'value'       => '',
					      'onclick'	    => 'view_area();');
				echo form_input($data);
				$data = array(
			              'name'        => 'vareatarget',
			              'id'          => 'vareatarget',
			              'value'       => '',
						  'readonly'	=> 'true');
				echo "&nbsp;Target Rp.&nbsp;".form_input($data);
				$data = array(
			              'name'        => 'nareatarget',
			              'id'          => 'nareatarget',
			              'value'       => '',
						  'readonly'	=> 'true');
				echo "&nbsp;Target Qty&nbsp;".form_input($data);?>
				<input type="hidden" id="htargetarea" name="htargetarea" value="">
				<input type="hidden" id="hntargetarea" name="hntargetarea" value="">
			</td>
	      </tr>
	      <tr>
		<td width="9%">Salesman</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'isalesman',
			              'id'          => 'isalesman',
			              'readonly'    => 'true',
			              'value'       => '',
					      'onclick'	    => 'view_salesman();');
				echo form_input($data);
				$data = array(
			              'name'        => 'esalesmanname',
			              'id'          => 'esalesmanname',
			              'readonly'    => 'true',
						  'value'       => '',
					      'onclick'	    => 'view_salesman();');
				echo form_input($data);
				$data = array(
			              'name'        => 'vsalesmantarget',
			              'id'          => 'vsalesmantarget',
			              'value'       => '',
						  'readonly'	=> 'true');
				echo "&nbsp;Target Rp.&nbsp;".form_input($data);
				$data = array(
			              'name'        => 'nsalesmantarget',
			              'id'          => 'nsalesmantarget',
			              'value'       => '',
						  'readonly'	=> 'true');
				echo "&nbsp;Target Qty&nbsp;".form_input($data);?>
				<input type="hidden" id="htargetsalesman" name="htargetsalesman" value="">
				<input type="hidden" id="hntargetsalesman" name="hntargetsalesman" value="">
			</td>
	      </tr>
	      <tr>
		<td width="9%">Kota</td>
		<td width="1%">:</td>
		<td width="90%"><?php 
				$data = array(
			              'name'        => 'icity',
			              'id'          => 'icity',
			              'value'       => '',
			              'readonly'    => 'true',
					      'onclick'	    => 'view_city();');
				echo form_input($data);
				$data = array(
			              'name'        => 'ecityname',
			              'id'          => 'ecityname',
			              'value'       => '',
			              'readonly'    => 'true',
					      'onclick'	    => 'view_city();');
				echo form_input($data);
				$data = array(
			              'name'        => 'vcitytarget',
			              'id'          => 'vcitytarget',
			              'value'       => '',
						  'onkeyup'		=> 'reformat(this); hitung();');
				echo "&nbsp;Target Rp.&nbsp;".form_input($data);
				$data = array(
			              'name'        => 'ncitytarget',
			              'id'          => 'ncitytarget',
			              'value'       => '',
						  'onkeyup'		=> 'reformat(this); hitung();');
				echo "&nbsp;Target Qty&nbsp;".form_input($data);?>
				<input type="hidden" id="htargetkota" name="htargetkota" value="">
				<input type="hidden" id="hntargetkota" name="hntargetkota" value="">
			</td>
	      </tr>
	      <tr>
		<td width="9%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="90%">
		  <input name="login" id="login" value="Simpan" type="submit" onclick='dipales();'>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("salestarget/cform/","#main")'>
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("bulan").value=='')||
			(document.getElementById("tahun").value=='')||
			(document.getElementById("iarea").value=='')||
			(document.getElementById("isalesman").value=='')||
			(document.getElementById("icity").value=='')||
			(document.getElementById("vcitytarget").value=='')||
			(document.getElementById("ncitytarget").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
		}
	}
  	function view_area(){
		lebar =450;
		tinggi=400;
		periode=document.getElementById("iperiode").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/area/"+periode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function view_salesman(){
		lebar =450;
		tinggi=400;
		periode=document.getElementById("iperiode").value;
		area=document.getElementById("iarea").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/salesman/"+area+"/"+periode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function view_city(){
		lebar 	= 450;
		tinggi	= 400;
		periode	= document.getElementById("iperiode").value;
		area	= document.getElementById("iarea").value;
		salesman= document.getElementById("isalesman").value;
		eval('window.open("<?php echo site_url(); ?>"+"/salestarget/cform/city/"+area+"/"+periode+"/"+salesman,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
	function buatperiode(){
		periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
		document.getElementById("iperiode").value=periode;
	}
	function hitung(){
		vtarea			  = parseFloat(formatulang(document.getElementById("vareatarget").value));
		vtsalesman	  = parseFloat(formatulang(document.getElementById("vsalesmantarget").value));
		vtcity		  	= parseFloat(formatulang(document.getElementById("vcitytarget").value));
		vasalarea		  = parseFloat(formatulang(document.getElementById("htargetarea").value));
		vasalsalesman	= parseFloat(formatulang(document.getElementById("htargetsalesman").value));
		vasalcity		  = parseFloat(formatulang(document.getElementById("htargetkota").value));
		vtsalesman		= (vasalsalesman+vtcity)-vasalcity;
		vtarea			  = (vasalarea+vtcity)-vasalcity;
		document.getElementById("vareatarget").value	= formatcemua(vtarea);
		document.getElementById("vsalesmantarget").value= formatcemua(vtsalesman);

		ntarea			  = parseFloat(formatulang(document.getElementById("nareatarget").value));
		ntsalesman	  = parseFloat(formatulang(document.getElementById("nsalesmantarget").value));
		ntcity		  	= parseFloat(formatulang(document.getElementById("ncitytarget").value));
		nasalarea		  = parseFloat(formatulang(document.getElementById("hntargetarea").value));
		nasalsalesman	= parseFloat(formatulang(document.getElementById("hntargetsalesman").value));
		nasalcity		  = parseFloat(formatulang(document.getElementById("hntargetkota").value));
		ntsalesman		= (nasalsalesman+ntcity)-nasalcity;
		ntarea			  = (nasalarea+ntcity)-nasalcity;
		document.getElementById("nareatarget").value	= formatcemua(ntarea);
		document.getElementById("nsalesmantarget").value= formatcemua(ntsalesman);
	}
</script>
