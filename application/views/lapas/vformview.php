<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'lapas/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($penjualan){
?>
	  <th>Area</th>
		<th>Salesman</th>
    <th>Target Omset</th>
    <th>Real Omset</th>
    <th>% Omset</th>
    <th>Target Tagihan</th>
    <th>Real Tagihan</th>
    <th>% Tagih</th>
    <?php 
	    echo '<tbody>';

      $subtargetomset=0;
      $subrealomset=0;
      $subpersenomset=0;
      $subtargettagih=0;
      $subrealtagih=0;
      $subpersentagih=0;

      $ada=false;
      foreach($penjualan as $raw)
      {
        $ada=true;
        if($raw->v_spb=='' || $raw->v_spb==null)$raw->v_spb=0;
        if($raw->v_target=='' || $raw->v_target==null)$raw->v_target=0;
        if($raw->v_spb!=0 && $raw->v_target!=0){
          $persenreal=($raw->v_spb/$raw->v_target)*100;
        }else{
          $persenreal=0;
        }
        $subtargetomset=$subtargetomset+$raw->v_target;
        $subrealomset=$subrealomset+$raw->v_spb;
        $subpersenomset=number_format(($subrealomset/$subtargetomset)*100,2);
        echo "<tr><td>$raw->i_area - $raw->e_area_name</td>
              <td>$raw->i_salesman - $raw->e_salesman_name</td>";
        echo "<td align=right>".number_format($raw->v_target)."</td>
              <td align=right>".number_format($raw->v_spb)."</td>
              <td align=right>".number_format($persenreal)." %</td>";

        $ada=false;
        foreach($cash as $riw)
        {
          if( ($riw->i_area==$raw->i_area) && ($riw->i_salesman==$raw->i_salesman)){
            $ada=true;
            if($riw->realisasi==null || $riw->realisasi==''){
              $realcash=0;
            }else{
              $realcash=$riw->realisasi;
            }
            if($riw->total==null || $riw->total==''){
              $totcash=0;
            }else{
              $totcash=$riw->total;
            }
            if($riw->total!=0){
              $persencash=number_format(($riw->realisasi/$riw->total)*100,2);
            }else{
              $persencash='0';
            }
            /*echo "<td align=right>".number_format($totcash)."</td>
                  <td align=right>".number_format($realcash)."</td>
                  <td align=right>".number_format($persencash)."</td>";*/
          }
          if($ada)break;
        }

        $ada=false;
        foreach($credit as $rew)
        {
          if( ($rew->i_area==$raw->i_area) && ($rew->i_salesman==$raw->i_salesman)){
            $ada=true;
            if($rew->realisasi==null || $rew->realisasi==''){
              $realcr=0;
            }else{
              $realcr=$rew->realisasi;
            }
            if($rew->total==null || $rew->total==''){
              $totcr=0;
            }else{
              $totcr=$rew->total;
            }
            if($rew->total!=0){
              $persencr=number_format(($rew->realisasi/$rew->total)*100,2);
            }else{
              $persencr='0';
            }
            /*echo "<td align=right>".number_format($totcr)."</td>
                  <td align=right>".number_format($realcr)."</td>
                  <td align=right>".number_format($persencr)."</td>";*/
          }
          if($ada)break;

        }

        $targettagih=$totcash+$totcr;
        $realtagih=$realcash+$realcr;
        $persentagih=number_format(($realtagih/$targettagih)*100,2);
        $subtargettagih=$subtargettagih+$targettagih;
        $subrealtagih=$subrealtagih+$realtagih;
        $subpersentagih=number_format(($subrealtagih/$subtargettagih)*100,2);
        echo " <td align=right>".number_format($targettagih)."</td>
               <td align=right>".number_format($realtagih)."</td>
               <td align=right>".number_format($persentagih)." %</td></tr>";
#       if($ada)break;
      }
#      if($ada)break;

      echo "<tr><td colspan=2 align=right><b>Total Nasional</b></td>
                <td align=right><b>".number_format($subtargetomset)."</b></td>
                <td align=right><b>".number_format($subrealomset)."</b></td>
                <td align=right><b>".number_format($subpersenomset)." %</b></td>
                <td align=right><b>".number_format($subtargettagih)."</b></td>
                <td align=right><b>".number_format($subrealtagih)."</b></td>
                <td align=right><b>".number_format($subpersentagih)." %</b></td>";
    }
?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
