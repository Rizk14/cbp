<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: portrait;
}
@page { size: Letter; 
        margin: 5mm;  /* this affects the margin in the printer settings */
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}

</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<table width="985" border="0">
  <tr>
    <td colspan="3" class="nmper"><?php echo NmPerusahaan; ?></td>
  </tr>
  <tr>
    <td colspan="3" class="judul" >SERAH TERIMA ORDER PEMBELIAN </td>
  </tr>
</table>
<?php 
$n=0;
foreach($isi as $row)
{
?>
<!--<table width="985" border="0">
  <tr>
    <td>No. <?php echo "$row->i_stop"; ?></td>
    <td>Area : <?php echo $row->i_area."-".$row->e_area_name; ?></td>
    <td>Keterangan : <?php echo $row->e_remark; ?></td>
  </tr>
</table>-->
<?php 
    $tmp=explode("-",$row->d_stop);
    $th=$tmp[0];
    $bl=$tmp[1];
    $hr=$tmp[2];
    $dstop  = $hr." ".mbulan($bl)." ".$th;  
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $kdsupp = $row->i_supplier;
		$istop	= $row->i_stop;
		$iarea	= $row->i_area;
		$query 	= $this->db->query(" select * from tm_stop_item where i_stop='$istop' and i_supplier='$kdsupp'",false);
		$jml 	= $query->num_rows();
    $detail = $this->mmaster->bacadetail($istop,$kdsupp);
		$i=0;	
?>
<table width="620" border="0" class="garisy ceKotak">
  <?php if($n==0){?>
  <tr>
    <td width="10"><div align="center">No</div></td>
    <td width="50"><div align="center">NO OP</div></td>
    <td width="50"><div align="center">TANGGAL OP</div></td>
    <td width="150"><div align="center">Pelanggan</div></td>
    <td width="150"><div align="center">SUPPLIER</div></td>
    <!--<td width="50"><div align="center">NO ST</div></td>
    <td width="150"><div align="center">Keterangan</div></td>-->
  </tr>
  <?php 
}
  ?>
<?php 
		foreach($detail as $rowi){
			$n=$n+$i;
      $n++;
			$stop  = $rowi->i_stop;
			$supp	= $rowi->i_supplier;
			$name	= $rowi->e_supplier_name;
			if(strlen($name)>40){
				$name=substr($name,0,40);
			}
			$name	= $name.str_repeat(" ",27-strlen($name));
      $cust = $rowi->e_customer_name;
      if(strlen($cust)>40){
        $cust=substr($cust,0,40);
      }
      $cust = $cust.str_repeat(" ",40-strlen($cust));
			$iop	= $rowi->i_op;
      $tmp=explode("-",$rowi->d_op);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dop  = $hr."-".$bl."-".$th;
      $ket = $rowi->e_remark;
			$ket	= $ket.str_repeat(" ",24-strlen($ket));
?>  
<tr class="garisy ceKotak huruf ici" align="center">
    <td width="24"><?php echo $n; ?></td>
    <td width="50"><?php echo $iop; ?></td>
    <td width="50"><?php echo $dop; ?></td>
    <td width="150"><?php echo $cust; ?></td>
    <td width="150"><?php echo $name; ?></td>
   <!-- <td width="50"><?php echo $stop; ?></td>
    <td width="150"><?php echo $ket; ?></td>-->
  </tr>

<?php 
    }
    ?>
    
</table>
<?php 
}
?>
<br>
<table width="300" border="0" align="right">
  <tr>
    <td><div align="center">Cilacap, <?php echo $dstop; ?></div></td>
  </tr>
   <tr>
    <td><div align="center">Diterima Oleh, </div></td>
  </tr>
    <td width="156" align="center"><br><br>(...........)</td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<br>
<table width="620" border="0">
  <tr>
    <td>TANGGAL CETAK </td>
    <td>:</td>
    <td><?php echo $tgl; ?></td>
  </tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
