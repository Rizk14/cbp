<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listmutasipusat/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
		if($isi){
			foreach($isi as $row){
				$periode=$row->e_mutasi_periode;
			}
		}else{
			$periode=$iperiode;
		}
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <input name="pperiode" id="pperiode" value="<?php echo $row->e_mutasi_periode; ?>" type="hidden">
    <input name="iarea" id="iarea" value="<?php echo $iarea; ?>" type="hidden">
<!--    <input name="cmdexport" id="cmdexport" value="Export to Excel" type="submit">-->
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>LAPORAN MUTASI STOCK - $row->i_store ($istorelocation)</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
    $selisih=0;
    $saldoakhir=0;
    $stockopname=0;
    $rpselisih=0;
    $rpsaldoakhir=0;
    $rpstockopname=0;
		if($isi){
			foreach($isi as $row){
        $selisih=$selisih+(($row->n_saldo_stockopname+$row->n_saldo_git+$row->n_git_penjualan)-$row->n_saldo_akhir);
        $saldoakhir=$saldoakhir+$row->n_saldo_akhir;
        $stockopname=$stockopname+($row->n_saldo_stockopname+$row->n_saldo_git+$row->n_git_penjualan);
        $rpselisih=$rpselisih+((($row->n_saldo_stockopname+$row->n_saldo_git+$row->n_git_penjualan)-$row->n_saldo_akhir)*$row->v_product_retail);
        $rpsaldoakhir=$rpsaldoakhir+($row->n_saldo_akhir*$row->v_product_retail);
        $rpstockopname=$rpstockopname+(($row->n_saldo_stockopname+$row->n_saldo_git+$row->n_git_penjualan)*$row->v_product_retail);
      }
    }
		?>
<h3>Saldo Akhir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpsaldoakhir); ?><br>
    	Saldo Stock Opname <?php echo 'Rp. '.number_format($rpstockopname); ?><br>
		Selisih &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpselisih); ?><h3>
    <center>
      <input name="export1" id="export1" value="Export to Excel" type="button"> &nbsp;

      <input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasipusat/cform/index","#main");' ></center>
        <table class="listtable" border=none id="sitabel">
	   	    <th>No</th>
	   	    <th>Kode</th>
	   	    <th>Nama</th>
			    <th>Saldo Awal</th>
			    <th>Pembelian</th>
			    <th>Dari Cabang</th>
			    <th>Retur Penjualan</th>
			    <th>Retur Pabrik</th>
			    <th>Penjualan</th>
			    <th>Ke Cabang</th>
			    <th>Sld Akhir</th>
			    <th>Sld Opname</th>
			    <th>Selisih</th>
          <th>GiT</th>
          <th>GiT Penj</th>
        	<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
      $selisih=0;
      $rpsaldoakhir=0;
      $rpstockopname=0;
      $group='';
      $totsawal=0;
      $totbeli=0;
      $totdcbg=0;
      $totretj=0;
      $totretp=0;
      $totjual=0;
      $totkcbg=0;
      $totsakhir=0;
      $totsopn=0;
      $totselisih=0;
      $totgit=0;
      $totgitj=0;
			foreach($isi as $row){
        $selisih=($row->n_saldo_stockopname+$row->n_saldo_git+$row->n_git_penjualan)-$row->n_saldo_akhir;
        $rpsaldoakhir=$row->n_saldo_akhir*$row->v_product_retail;
        $rpstockopname=$row->n_saldo_stockopname*$row->v_product_retail;

        $saldoawal=$row->n_saldo_awal+$row->n_mutasi_gitasal+$row->n_git_penjualanasal;

          if($group=='')
          {
    	      echo "<tr><td colspan=16 align=center style=\"font-size:16px;\">".strtoupper($row->e_product_groupname)."</td></tr>";
            $i=1;
            $gtotsawal=0;
            $gtotbeli=0;
            $gtotdcbg=0;
            $gtotretj=0;
            $gtotretp=0;
            $gtotjual=0;
            $gtotkcbg=0;
            $gtotsakhir=0;
            $gtotsopn=0;
            $gtotselisih=0;
            $gtotgit=0;
            $gtotgitj=0;
          }else{
            if($group!=$row->e_product_groupname){
		          echo "<tr>
                <td colspan=3 align=center>Total ".strtoupper($group)."</td>
				        <td align=right>$gtotsawal</td>
				        <td align=right>$gtotbeli</td>
				        <td align=right>$gtotdcbg</td>
				        <td align=right>$gtotretj</td>
				        <td align=right>$gtotretp</td>
				        <td align=right>$gtotjual</td>
				        <td align=right>$gtotkcbg</td>
				        <td align=right>$gtotsakhir</td>
				        <td align=right>$gtotsopn</td>
                <td align=right>$gtotselisih</td>
				        <td align=right>$gtotgit</td>
                <td align=right>$gtotgitj</td><td></td></tr>";

      	      echo "<tr><td colspan=16 align=center style=\"font-size:16px;\">".strtoupper($row->e_product_groupname)."</td></tr>";
              $i=1;
              $gtotsawal=0;
              $gtotbeli=0;
              $gtotdcbg=0;
              $gtotretj=0;
              $gtotretp=0;
              $gtotjual=0;
              $gtotkcbg=0;
              $gtotsakhir=0;
              $gtotsopn=0;
              $gtotselisih=0;
              $gtotgit=0;
              $gtotgitj=0;
            }
          }
          $group=$row->e_product_groupname;
		      echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_product</td>
            <td>$row->e_product_name($row->i_product_grade)</td>
				    <td align=right>$saldoawal</td>
				    <td align=right>$row->n_mutasi_pembelian</td>
				    <td align=right>$row->n_mutasi_bbm</td>
				    <td align=right>$row->n_mutasi_returoutlet</td>
				    <td align=right>$row->n_mutasi_returpabrik</td>
				    <td align=right>$row->n_mutasi_penjualan</td>
				    <td align=right>$row->n_mutasi_bbk</td>
				    <td align=right>$row->n_saldo_akhir</td>
				    <td align=right>$row->n_saldo_stockopname</td>
            <td align=right>$selisih</td>
				    <td align=right>$row->n_saldo_git</td>
            <td align=right>$row->n_git_penjualan</td>";
        		$i++;
			  	echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='show(\"listmutasipusat/cform/detail/$iperiode/$iarea/$row->i_product/$saldoawal/$istorelocation/$row->i_product_grade/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td></tr>";	

        $gtotsawal=$gtotsawal+$saldoawal;
        $gtotbeli=$gtotbeli+$row->n_mutasi_pembelian;
        $gtotdcbg=$gtotdcbg+$row->n_mutasi_bbm;
        $gtotretj=$gtotretj+$row->n_mutasi_returoutlet;
        $gtotretp=$gtotretp+$row->n_mutasi_returpabrik;
        $gtotjual=$gtotjual+$row->n_mutasi_penjualan;
        $gtotkcbg=$gtotkcbg+$row->n_mutasi_bbk;
        $gtotsakhir=$gtotsakhir+$row->n_saldo_akhir;
        $gtotsopn=$gtotsopn+$row->n_saldo_stockopname;
        $gtotselisih=$gtotselisih+$selisih;
        $gtotgit=$gtotgit+$row->n_saldo_git;
        $gtotgitj=$gtotgitj+$row->n_git_penjualan;
        $totsawal=$totsawal+$saldoawal;
        $totbeli=$totbeli+$row->n_mutasi_pembelian;
        $totdcbg=$totdcbg+$row->n_mutasi_bbm;
        $totretj=$totretj+$row->n_mutasi_returoutlet;
        $totretp=$totretp+$row->n_mutasi_returpabrik;
        $totjual=$totjual+$row->n_mutasi_penjualan;
        $totkcbg=$totkcbg+$row->n_mutasi_bbk;
        $totsakhir=$totsakhir+$row->n_saldo_akhir;
        $totsopn=$totsopn+$row->n_saldo_stockopname;
        $totselisih=$totselisih+$selisih;
        $totgit=$totgit+$row->n_saldo_git;
        $totgitj=$totgitj+$row->n_git_penjualan;

			}
        echo "<tr>
          <td colspan=3 align=center>Total ".strtoupper($group)."</td>
	        <td align=right>$gtotsawal</td>
	        <td align=right>$gtotbeli</td>
	        <td align=right>$gtotdcbg</td>
	        <td align=right>$gtotretj</td>
	        <td align=right>$gtotretp</td>
	        <td align=right>$gtotjual</td>
	        <td align=right>$gtotkcbg</td>
	        <td align=right>$gtotsakhir</td>
	        <td align=right>$gtotsopn</td>
          <td align=right>$gtotselisih</td>
	        <td align=right>$gtotgit</td>
          <td align=right>$gtotgitj</td><td></td></tr>";
        echo "<tr>
          <td colspan=3 align=center>Total</td>
	        <td align=right>$totsawal</td>
	        <td align=right>$totbeli</td>
	        <td align=right>$totdcbg</td>
	        <td align=right>$totretj</td>
	        <td align=right>$totretp</td>
	        <td align=right>$totjual</td>
	        <td align=right>$totkcbg</td>
	        <td align=right>$totsakhir</td>
	        <td align=right>$totsopn</td>
          <td align=right>$totselisih</td>
	        <td align=right>$totgit</td>
          <td align=right>$totgitj</td><td></td></tr>";

		}
	      ?>
	    </tbody>
	  </table>
    <center>
      <input name="export" id="export" value="Export to Excel" type="button"> &nbsp;

      <input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasipusat/cform/index","#main");' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listmutasipusat/cform/viewdetail";
	  formna.submit();
  }

  $( "#export" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

  $( "#export1" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
