<?php echo $this->pquery->form_remote_tag(array('url'=>'customerpareto/cform/update','update'=>'#pesan','type'=>'post'));?>
<div id="customernewform"> 

				<table class="maintable">
				<tr>
		  		<td align="left">
			    	<table class="mastertable">
						<tr>
              <td width="16%">Kode Pelanggan</td>
              <td width="1%">:</td>
              <td width="33%">
                <input readonly type="text" name="icustomer" id="icustomer" value="<?php echo $isi->i_customer; ?>" maxlength='5'></td>
							<td width="16%">Sales</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="esalesmanname" id="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>" readonly>
								<input type="hidden" name="isalesman" id="isalesman" value="<?php echo $isi->i_salesman; ?>"></td>
						</tr>
				<?php 
					if($isi->d_survey!='') {
						$tmp=explode("-",$isi->d_survey);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$dsurvey=$hr."-".$bl."-".$th;
					} else {
						$dsurvey='';
					}	
				?>
						<tr>
							<td width="16%">Tanggal Survey</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="dsurvey" id="dsurvey" value="<?php echo $dsurvey; ?>" readonly value=""></td>
							<td width="16%">Periode Kunjungan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="nvisitperiod" id="nvisitperiod" value="<?php echo $isi->n_visit_period; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Kriteria Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="checkbox" name="chkcriterianew" id="chkcriterianew"
								<?php if($isi->f_customer_new=='t') {
										  echo 'checked  value="on"';}else{echo 'value=""';} ?> onclick='chkcriteriaa()'>
								Plg Baru / New &nbsp;&nbsp;&nbsp;&nbsp;
								<input type="checkbox" name="chkcriteriaupdate" id="chkcriteriaupdate" 								
								<?php if($isi->f_customer_new=='f') {
						   				echo 'checked  value="on"';}else{echo 'value=""';} ?>
								onclick='chkcriteriab()'>
								Plg Lama / UpDate</td>
  						<td width="16%">Area</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name; ?>" readonly>
								<input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area; ?>"></td>
						</tr>
            <tr>
              <td class="batas" width="16%"></td>
              <td class="batas" width="1%"></td>
              <td class="batas" width="33%">								
								<input type="checkbox" name="fcustomeraktif" id="fcustomeraktif" value="on" <?php if($isi->f_customer_aktif=='t') echo 'checked'; ?>> Aktif <input type="checkbox" name="fpareto" id="fpareto" <?php if($isi->f_pareto=='t') echo 'checked'; ?>>
								Pareto</td>
              <td class="batas" width="16%">Kota</td>
		          <td class="batas" width="1%">:</td>
		          <td class="batas" width="33%">
		            <input type="text" name="ecityname" id="ecityname" value="<?php if($isi->e_city_name) echo $isi->e_city_name; ?>" readonly> #
		            <input type="hidden" name="icity" id="icity" value="<?php echo $isi->i_city;?>"></td>
            </tr>
						<tr>
							<td colspan=6>DATA TOKO / PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Nm Toko / Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomername" id="ecustomername" value="<?php echo $isi->e_customer_name; ?>" readonly></td>
							<td width="16%">Alamat Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomeraddress" id="ecustomeraddress" value="<?php echo $isi->e_customer_address; ?>" maxlength='100'></td>
						</tr>
						<tr>
							<td width="16%">Penanda Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomersign" id="ecustomersign" value="<?php echo $isi->e_customer_sign; ?>"></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ert1" id="ert1" maxlength='2' value="<?php echo $isi->e_rt1; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="erw1" id="erw1" maxlength='2' value="<?php echo $isi->e_rw1; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="epostal1" id="epostal1" maxlength='5' value="<?php echo $isi->e_postal1; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Telepon</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerphone" id="ecustomerphone" value="<?php echo $isi->e_customer_phone; ?>" maxlength='20'></td>
							<td width="16%">Fax</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="efax1" id="efax1" value="<?php echo $isi->e_fax1; ?>" maxlength='20'></td>
						</tr>
						<tr>
							<td width="16%">Yang dihubungi</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomercontact" id="ecustomercontact" maxlength='30' value="<?php echo $isi->e_customer_contact; ?>"></td>
							<td width="16%">Jabatan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomercontactgrade" id="ecustomercontactgrade" maxlength='30' value="<?php echo $isi->e_customer_contactgrade; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Mulai Usaha</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomermonth" id="ecustomermonth" value="<?php echo $isi->e_customer_month; ?>" maxlength=2>
								/
								<input readonly type="text" name="ecustomeryear" id="ecustomeryear" value="<?php echo $isi->e_customer_year; ?>" maxlength=4>(bl / tahun)</td>
							<td width="16%">Tahun</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerage" id="ecustomerage" readonly value="<?php echo $isi->e_customer_age; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Status Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eshopstatus" id="eshopstatus" value="<?php echo $isi->e_shop_status; ?>" readonly>
								<input type="hidden" name="ishopstatus" id="ishopstatus" value="<?php echo $isi->i_shop_status; ?>"'></td>
							<td width="16%">Luas Fisik Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="nshopbroad" id="nshopbroad" maxlength='7' value="<?php echo $isi->n_shop_broad; ?>"> M2</td>
						</tr>
						<tr>
							<td width="16%">Kelurahan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerkelurahan1" id="ecustomerkelurahan1" maxlength='30' value="<?php echo $isi->e_customer_kelurahan1; ?>"></td>
							<td width="16%">Kecamatan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerkecamatan1" id="ecustomerkecamatan1" maxlength='30' value="<?php echo $isi->e_customer_kecamatan1; ?>"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerkota1" id="ecustomerkota1" maxlength='30' value="<?php echo $isi->e_customer_kota1; ?>"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerprovinsi1" id="ecustomerprovinsi1" maxlength='30' value="<?php echo $isi->e_customer_provinsi1; ?>"></td>
						</tr>
						<tr>
							<td colspan=6>DATA 	PEMILIK / PENGURUS TOKO / PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Nama Pemilik</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerowner" id="ecustomerowner" value="<?php echo $isi->e_customer_owner; ?>"></td>
							<td width="16%">TTL / Umur</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerownerttl" id="ecustomerownerttl" value="<?php echo $isi->e_customer_ownerttl; ?>"> / <input readonly type="text" name="ecustomerownerage" id="ecustomerownerage" value="<?php echo $isi->e_customer_ownerage; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Status</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="emarriage" id="emarriage" value="<?php echo $isi->e_marriage; ?>" readonly>
								<input type="hidden" name="imarriage" id="imarriage" value="<?php echo $isi->i_marriage; ?>"></td>
							<td width="16%">Jenis Kelamin</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ejeniskelamin" id="ejeniskelamin" value="<?php echo $isi->e_jeniskelamin; ?>" readonly>
								<input type="hidden" name="ijeniskelamin" id="ijeniskelamin" value="<?php echo $isi->i_jeniskelamin; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Alamat Rumah</td>
							<td width="1%">:</td>
							<td width="33%"><input type="checkbox" name="chkidemtoko1" id="chkidemtoko1" value="" onclick='chkdemtoko1();'> Sama dengan alamat toko</td>
							<td width="16%">Agama</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ereligion" id="ereligion" value="<?php echo $isi->e_religion; ?>" readonly>
								<input type="hidden" name="ireligion" id="ireligion" value="<?php echo $isi->i_religion; ?>"></td>
						</tr>
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%"><input type="text" name="ecustomerowneraddress" id="ecustomerowneraddress" value="<?php echo $isi->e_customer_owneraddress; ?>" maxlength='100' readonly ></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ert2" id="ert2" maxlength='2' value="<?php echo $isi->e_rt2; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="erw2" id="erw2" maxlength='2' value="<?php echo $isi->e_rw2; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="epostal2" id="epostal2" maxlength='5' value="<?php echo $isi->e_postal2; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Telepon / HP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerownerphone" id="ecustomerownerphone" value="<?php echo $isi->e_customer_ownerphone; ?>"> / <input readonly type="text" name="ecustomerownerhp" id="ecustomerownerhp" value="<?php echo $isi->e_customer_ownerhp; ?>"></td>
							<td width="16%">Fax / Email</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerownerfax" id="ecustomerownerfax" value="<?php echo $isi->e_customer_ownerfax; ?>"> / <input readonly type="text" name="ecustomermail" id="ecustomermail" value="<?php echo $isi->e_customer_mail; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Nama Suami / Istri</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerownerpartner" id="ecustomerownerpartner" value="<?php echo $isi->e_customer_ownerpartner; ?>"></td>
							<td width="16%">TTL / Umur</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerownerpartnerttl" id="ecustomerownerpartnerttl" value="<?php echo $isi->e_customer_ownerpartnerttl; ?>"> / <input readonly type="text" name="ecustomerownerpartnerage" id="ecustomerownerpartnerage" value="<?php echo $isi->e_customer_ownerpartnerage; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Kelurahan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerkelurahan2" id="ecustomerkelurahan2" maxlength='30' value="<?php echo $isi->e_customer_kelurahan2; ?>"></td>
							<td width="16%">Kecamatan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomerkecamatan2" id="ecustomerkecamatan2" maxlength='30' value="<?php echo $isi->e_customer_kecamatan2; ?>"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerkota2" id="ecustomerkota2" maxlength='30' value="<?php echo $isi->e_customer_kota2; ?>"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerprovinsi2" id="ecustomerprovinsi2" maxlength='30' value="<?php echo $isi->e_customer_provinsi2; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Alamat kirim</td>
							<td width="1%">:</td>
							<td colspan=4 width="33%"><input type="checkbox" name="chkidemtoko2" id="chkidemtoko2" value="" onclick='chkdemtoko2()'> Sama dengan alamat toko
																				<input type="checkbox" name="chkidemtoko3" id="chkidemtoko3" value="" onclick='chkdemtoko3()'> Sama dengan alamat rumah</td>
						</tr>
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%">
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%"><input readonly type="text" name="ecustomersendaddress" id="ecustomersendaddress" value="<?php echo $isi->e_customer_sendaddress; ?>" maxlength='100'></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ert3" id="ert3" maxlength='2' value="<?php echo $isi->e_rt3; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="erw3" id="erw3" maxlength='2' value="<?php echo $isi->e_rw3; ?>">&nbsp;/&nbsp;
								<input readonly type="text" name="epostal3" id="epostal3" maxlength='5' value="<?php echo $isi->e_postal3; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Telepon</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomersendphone" id="ecustomersendphone" value="<?php echo $isi->e_customer_sendphone; ?>"></td>
							<td width="16%">Lokasi Bisa dilalui o/</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="etraversed" id="etraversed" value="<?php echo $isi->e_traversed; ?>" readonly>
								<input type="hidden" name="itraversed" id="itraversed" value="<?php echo $isi->i_traversed; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Ada Biaya Retribusi Parkir</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="checkbox" name="fparkir" id="fparkir" <?php if($isi->f_parkir=='t') {
										  echo 'checked  value="on"';}else{echo 'value=""';} ?> onclick="cekparkir();"></td>
							<td width="16%">Ada Biaya Kuli</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="checkbox" name="fkuli" id="fkuli" <?php if($isi->f_kuli=='t') {
										  echo 'checked  value="on"';}else{echo 'value=""';} ?> onclick="cekkuli();"></td>
						</tr>
						<tr>
							<td width="16%">Ekspedisi Toko 1</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="eekspedisi1" id="eekspedisi1" value="<?php echo $isi->e_ekspedisi1; ?>"></td>
							<td width="16%">Ekspedisi Toko 2</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="eekspedisi2" id="eekspedisi2" value="<?php echo $isi->e_ekspedisi2; ?>"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerkota3" id="ecustomerkota3" maxlength='30' value="<?php echo $isi->e_customer_kota3; ?>"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input readonly type="text" name="ecustomerprovinsi3" id="ecustomerprovinsi3" maxlength='30' value="<?php echo $isi->e_customer_provinsi3; ?>"></td>
						</tr>
						<tr>
							<td width="16%">No NPWP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomernpwp" id="ecustomernpwp" value="<?php if ($isi->e_customer_pkpnpwp==''){ echo $isi->pkpnpwp; }else{ echo $isi->e_customer_pkpnpwp;} ?>" maxlength=20></td>
							<td width="16%">Nama NPWP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ecustomernpwpname" id="ecustomernpwpname" value="<?php if ($isi->e_customer_npwpname==''){ echo $isi->namapkp; } else{ echo $isi->e_customer_npwpname;} ?>"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Alamat NPWP</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" colspan=4 width="83%">
								<input readonly type="text" name="ecustomernpwpaddress" id="ecustomernpwpaddress" value="<?php if ($isi->e_customer_npwpaddress==''){ echo $isi->almtpkp; } else{ echo $isi->e_customer_npwpaddress;} ?>"></td>
						</tr>
						<tr>
							<td colspan=6>KUALIFIKASI PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Type Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerclassname" id="ecustomerclassname" value="<?php echo $isi->e_customer_classname; ?>" readonly>
								<input type="hidden" name="icustomerclass" id="icustomerclass" value="<?php echo $isi->i_customer_class; ?>"></td>
							<td width="16%">Pola Pembayaran</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="epaymentmethod" id="epaymentmethod" value="<?php echo $isi->e_paymentmethod; ?>" readonly>
								<input type="hidden" name="ipaymentmethod" id="ipaymentmethod" value="<?php echo $isi->i_paymentmethod; ?>"></td>
						</tr>
						<tr>
							<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I. Nama Bank :&nbsp;&nbsp;<input readonly type="text" name="ecustomerbank1" id="ecustomerbank1" value="<?php echo $isi->e_customer_bank1; ?>">
																				 &nbsp;&nbsp;No. A/C   : <input readonly type="text" name="ecustomerbankaccount1" id="ecustomerbankaccount1" value="<?php echo $isi->e_customer_bankaccount1; ?>">
																				 &nbsp;Atas Nama : <input readonly type="text" name="ecustomerbankname1" id="ecustomerbankname1" value="<?php echo $isi->e_customer_bankname1; ?>"></td>
						</tr>
						<tr>
							<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;II. Nama Bank :&nbsp;&nbsp;<input readonly type="text" name="ecustomerbank2" id="ecustomerbank2" value="<?php echo $isi->e_customer_bank2; ?>">
																				 &nbsp;&nbsp;No. A/C   : <input readonly type="text" name="ecustomerbankaccount2" id="ecustomerbankaccount2" value="<?php echo $isi->e_customer_bankaccount2; ?>">
																				 &nbsp;Atas Nama : <input readonly type="text" name="ecustomerbankname2" id="ecustomerbankname2" value="<?php echo $isi->e_customer_bankname2; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Nama Kompetitor</td>
							<td width="1%">:</td>
							<td colspan=4 width="83%">
								1.<input readonly type="text" maxlength=20 name="ekompetitor1" id="ekompetitor1" value="<?php echo $isi->e_kompetitor1; ?>">
								2.<input readonly type="text" maxlength=20 name="ekompetitor2" id="ekompetitor2" value="<?php echo $isi->e_kompetitor2; ?>">
								3.<input readonly type="text" maxlength=20 name="ekompetitor3" id="ekompetitor3" value="<?php echo $isi->e_kompetitor3; ?>"></td>
						</tr>
						<tr>
							<td width="16%">TOP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ncustomertoplength" id="ncustomertoplength" value="<?php echo $isi->n_customer_toplength; ?>" maxlength='3'> Hari</td>
							<td width="16%">Discount</td>
							<td width="1%">:</td>
							<td width="33%">
								<input readonly type="text" name="ncustomerdiscount" id="ncustomerdiscount" value="<?php echo $isi->n_customer_discount1; ?>" maxlength='3'> %
								<input readonly type="text" name="ncustomerdiscount2" id="ncustomerdiscount2" value="<?php echo $isi->n_customer_discount2; ?>" maxlength='3'> %</td>
						</tr>
						<tr>
							<td width="16%">Tukar Nota (Kontra Bon)</td>
							<td width="1%">:</td>
							<td width="33%">
								 <input type="checkbox" name="fkontrabon" id="fkontrabon" <?php if($isi->f_kontrabon=='t') {
										  echo 'checked  value="on"';}else{echo 'value=""';} ?> onclick="cekkontrabon();"></td>
							<td width="16%">Waktu u/ menghubungi</td>
							<td width="1%">:</td>
							<td width="33%"><input type="text" name="ecall" id="ecall" value="<?php echo $isi->e_call; ?>" readonly>
								<input type="hidden" name="icall" id="icall" value="<?php echo $isi->i_call; ?>">
								</td>
						</tr>
						<tr>
							<td class="batas" width="16%">Jadwal Kontra Bon</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">Hari
								<input readonly type="text" name="ekontrabonhari" id="ekontrabonhari" value="<?php echo $isi->e_kontrabon_hari; ?>">&nbsp;Jam
								<input readonly type="text" name="ekontrabonjam1" id="ekontrabonjam1" value="<?php echo $isi->e_kontrabon_jam1; ?>">s/d
								<input readonly type="text" name="ekontrabonjam2" id="ekontrabonjam2" value="<?php echo $isi->e_kontrabon_jam2; ?>"></td>
							<td class="batas" width="16%">Jadwal Tagih</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">Hari
								<input readonly type="text" name="etagihhari" id="etagihhari" value="<?php echo $isi->e_tagih_hari; ?>">&nbsp;Jam
								<input readonly type="text" name="etagihjam1" id="etagihjam1" value="<?php echo $isi->e_tagih_jam1; ?>">s/d
								<input readonly type="text" name="etagihjam2" id="etagihjam2" value="<?php echo $isi->e_tagih_jam2; ?>"></td>
						</tr>
						<tr>
							<td colspan=6>LAIN - LAIN</td>
						</tr>
						<tr>
							<td width="16%">Group Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomergroupname" id="ecustomergroupname" value="<?php echo $isi->e_customer_groupname; ?>" readonly>
								<input type="hidden" name="icustomergroup" id="icustomergroup" value="<?php echo $isi->i_customer_group; ?>"></td>
							<td width="16%">PLU Group</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerplugroupname" id="ecustomerplugroupname" value="<?php echo $isi->e_customer_plugroupname; ?>" readonly>
								<input type="hidden" name="icustomerplugroup" id="icustomerplugroup" value="<?php echo $isi->i_customer_plugroup; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Tipe Produk</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerproducttypename" id="ecustomerproducttypename" value="<?php echo $isi->e_customer_producttypename; ?>" readonly>
								<input type="hidden" name="icustomerproducttype" id="icustomerproducttype" value="<?php echo $isi->i_customer_producttype; ?>"></td>
							<td width="16%">Produk Khusus</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerspecialproductname" id="ecustomerspecialproductname" value="<?php echo $isi->e_customer_specialproductname; ?>" readonly>
								<input type="hidden" name="icustomerspecialproduct" id="icustomerspecialproduct" value="<?php echo $isi->i_customer_specialproduct; ?>"></td>
						</tr>
						<tr>
							<td width="16%">Status Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerstatusname" id="ecustomerstatusname" value="<?php echo $isi->e_customer_statusname; ?>" readonly>
								<input type="hidden" name="icustomerstatus" id="icustomerstatus" value="<?php echo $isi->i_customer_status; ?>"></td>
							<td width="16%">Tingkat Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomergradename" id="ecustomergradename" value="<?php echo $isi->e_customer_gradename; ?>" readonly>
								<input type="hidden" name="icustomergrade" id="icustomergrade" value="<?php echo $isi->i_customer_grade; ?>"></td>
            </tr>
						<tr>
							<td width="16%">Jenis Pelayanan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerservicename" id="ecustomerservicename" value="<?php echo $isi->e_customer_servicename; ?>" readonly>
								<input type="hidden" name="icustomerservice" id="icustomerservice" value="<?php echo $isi->i_customer_service; ?>"></td>
							<td width="16%">Cara Penjualan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomersalestypename" id="ecustomersalestypename" value="<?php echo $isi->e_customer_salestypename; ?>" readonly>
								<input type="hidden" name="icustomersalestype" id="icustomersalestype" value="<?php echo $isi->i_customer_salestype; ?>"></td>
           </tr>
           <tr>
		         <td width="16%">Group Harga</td>
		         <td width="1%">:</td>
		         <td width="33%">
		            <input type="text" name="epricegroupname" id="epricegroupname" value="<?php echo $isi->e_price_groupname;?>" readonly>
		            <input type="hidden" name="ipricegroup" id="ipricegroup" value="<?php echo $isi->i_price_group;?>"><input type="hidden" name="nline" id="nline" value="<?php echo $isi->n_line;?>"</td>
           <tr>
              <td width="16%">PLus PPN</td>
              <td width="1%">:</td>
              <td width="33%">								
								<input type="checkbox" name="fcustomerplusppn" id="fcustomerplusppn" value="on" <?php if($isi->f_customer_plusppn=='t') echo 'checked'; ?>></td>
              <td width="16%">PKP</td>
              <td width="1%">:</td>
              <td width="33%">								
								<input type="checkbox" name="fcustomerpkp" id="fcustomerpkp" value="on" <?php if($isi->f_customer_pkp=='t') echo 'checked'; ?>></td>
						</tr>
                        <tr>
                            <td width="100%" align="center" colspan="6">
                                <input name="login" id="login" value="Simpan" type="submit" onclick="dipales();">
                                <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("customerpareto/cform/","#main")'>
                            </td>
						</tr>                        
		      	</table>
		  		</td>
				</tr> 
	      </table>
				<div id="pesan"></div>
			
</div>
<?=form_close()?>
<script language="javascript" type="text/javascript">
  function dipales(){
	  document.getElementById("login").hidden=true;
  }
  function cekpareto(){
    if(document.getElementById("fpareto").value=='on'){
      document.getElementById("fpareto").value='';
    }else{
      document.getElementById("fpareto").value='on';
    }
  }
</script>
