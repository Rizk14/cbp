<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'exp-do/cform/export', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="spbperareaform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable">
              <tr>
                <td width="19%">Date From</td>
                <td width="1%">:</td>
                <td width="80%">
                  <input type="hidden" id="areafrom" name="areafrom" value="">
                  <?php
                  $data = array(
                    'name' => 'dfrom',
                    'id' => 'dfrom',
                    'value' => date('01-m-Y'),
                    'readonly' => 'true',
                    'onclick' => "showCalendar('',this,this,'','dfrom',0,20,1)"
                  );
                  echo form_input($data); ?>
                </td>
              </tr>
              <tr>
                <td width="19%">Date To</td>
                <td width="1%">:</td>
                <td width="80%">
                  <?php
                  $data = array(
                    'name' => 'dto',
                    'id' => 'dto',
                    'value' => date('d-m-Y'),
                    'readonly' => 'true',
                    'onclick' => "showCalendar('',this,this,'','dto',0,20,1)"
                  );
                  echo form_input($data); ?>
                </td>
              </tr>
              <tr>
                <td width="19%">Supplier</td>
                <td width="1%">:</td>
                <td width="80%">
                  <input type="hidden" id="isupplier" name="isupplier" value="">
                  <input type="text" id="esuppliername" name="esuppliername" size="40" value="" onclick='showModal("exp-do/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
                </td>
              <tr>
                <td width="19%">&nbsp;</td>
                <td width="1%">&nbsp;</td>
                <td width="80%">
                  <input name="view" id="view" value="View" type="submit">
                  <input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-do/cform/','#main')">
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      <?= form_close() ?>
      <div id="pesan"></div>
    </td>
  </tr>
</table>