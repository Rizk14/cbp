<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
  <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
  <script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
</head>

<body id="bodylist">
  <div id="main">
    <div id="tmp">
      <?php echo "<center><h2>$page_title</h2></center>"; ?>
      <table class="maintable">
        <tr>
          <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url'=>'exp-op/cform/supplier','id'=>'listform','update'=>'#light','type'=>'post'));?>
            <div class="effect">
              <div class="accordion2">
                <table class="listtable">
                  <input type="hidden" id="ispb" name="ispb" value="">
                  <input type="hidden" id="isupplier" name="isupplier" value="">
                  <input type="hidden" id="esuppliername" name="esuppliername" value="">
                  <input type="hidden" id="nsuppliertoplength" name="nsuppliertoplength" value="">
                  <input type="hidden" id="isuppliergroup" name="isuppliergroup" value="">
                  <input type="hidden" id="iarea" name="iarea" value="">
                  <thead>
                    <tr>
                      <td colspan="10" align="center">Cari data : <input type="hidden" id="spb" name="spb"
                          value="<?php echo $spb; ?>"><input type="hidden" id="area" name="area"
                          value="<?php echo $area; ?>"><input type="text" id="cari" name="cari" value="">&nbsp;<input
                          type="submit" id="bcari" name="bcari" value="Cari"></td>
                    </tr>
                  </thead>
                  <th>Kode Supplier</th>
                  <th>Nama Supplier</th>
                  <tbody>
                    <?php 
		if($isi){
			echo "<tr> 
			<td><a href=\"javascript:setValue('AS','All Supplier')\">AS</a></td>
			<td><a href=\"javascript:setValue('AS','All Supplier')\">All Supplier</a></td>
		</tr>";
			foreach($isi as $row){
				$nama=str_replace(",","",$row->e_supplier_name);
				$nama=str_replace("'","",$nama);
				$nama=str_replace("&","",$nama);
				$nama=str_replace("&","",$nama);
				$nama=str_replace("(","",$nama);
				$nama=str_replace(")","",$nama);
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_supplier','$nama')\">$row->i_supplier</a></td>
				  <td><a href=\"javascript:setValue('$row->i_supplier','$nama')\">$nama</a></td>
				</tr>";
			}
		}
	      ?>
                  </tbody>
                </table>
                <?php echo "<center>".$this->pagination->create_links()."</center>";?>
              </div>
            </div>
            <?=form_close()?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
function setValue(a, b) {
  document.getElementById("isupplier").value = a;
  document.getElementById("esuppliername").value = b;
  jsDlgHide("#konten *", "#fade", "#light");
}
</script>