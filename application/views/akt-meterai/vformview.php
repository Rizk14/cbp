<h2><?php echo $page_title ;?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-meterai/cform/simpan','update'=>'#pesan','type'=>'post'));?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<?php if($isi){ ?>
						<table class="listtable" id="sitabel">
							<thead>
								<tr>
									<th>NO</th>
									<th>NO NOTA</th>
									<th>TGL NOTA</th>
									<th>KODE PELANGGAN</th>
									<th>NAMA PELANGGAN</th>
									<th class="action">
										Action <br>
										<?php echo "<input type='checkbox' name='chkall' id='chkall' value='0' onclick='pilihsemua(this.value)'>" ;?>
									</th>
								</tr>
							</thead>
							
							<tbody>
								<?php 
									$i = 1;
									foreach($isi as $row){
										$tmp 	  = explode("-",$row->d_nota);
										$th	 	  = $tmp[0];
										$bl	 	  = $tmp[1];
										$hr	 	  = $tmp[2];
										$thbl	  = $th.$bl;
										$dnota	  = $hr."-".$bl."-".$th;
										$nama	  = str_replace('%20', ' ',$row->e_area_name);
									
										echo "<tr>
												<td class='text-center'>$i</td>
												<td class='text-center'>$row->i_nota</td>
												<td class='text-center'>$dnota</td>
												<td class='text-center'>$row->i_customer</td>
												<td>$row->e_customer_name</td>
												<input type = 'hidden' name = 'thbl".$i."' id = 'thbl".$i."' value = '$thbl'>
												<input type = 'hidden' name = 'periode".$i."' id = 'periode".$i."' value = '$periode'>
												<td class=\"action\">";
													echo "<input type='hidden' name='inota".$i."' id='inota".$i."' value='$row->i_nota'>";
													echo "<input type='hidden' name='dnota".$i."' id='dnota".$i."' value='$row->d_nota'>";
													echo "<input type='hidden' name='iarea".$i."' id='iarea".$i."' value='$row->i_area'>";
													echo "<input type='hidden' name='icustomer".$i."' id='icustomer".$i."' value='$row->i_customer'>";
													echo "<input type='hidden' name='vmeterai".$i."' id='vmeterai".$i."' value='$row->v_materai'>";
													echo "<input type='hidden' name='vmeteraisisa".$i."' id='vmeteraisisa".$i."' value='$row->v_materai_sisa'>";
													echo "<input type='checkbox' name='chk".$i."' id='chk".$i."' value=0 onclick='pilihan(this.value,".$i.")'>";
										echo "</td></tr>";

										$i++;
									}
									echo " <tr>
											<td colspan='10' align='center'><input type='submit' id='proses' name='proses' value='Lanjut Proses' onclick = 'return dipales();'></td>
											</tr>
											<input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">
											<input type=\"hidden\" id=\"cekon\" name=\"cekon\" value='0'>";
								?>
							</tbody>
						</table>
						<?php 
							}else{
								echo "<center><h1>Belum ada data !</h1></center>";
							}
						?>
					</div>
				</div>
			</div>
			<input id = "cmdback" name = "cmdback" value = "Kembali" type="button" onclick="show('akt-meterai/cform/','#main')">
			<?=form_close()?>
		</td>
	</tr>
</table>
<div id="pesan"></div>

<script language="javascript" type="text/javascript">
function pilihan(a,b){
	var jml 	= document.getElementById("jml").value;	
	var cekon 	= document.getElementById("cekon").value;	
	var counter	= 1;

	if(a==0){
		document.getElementById("chk"+b).value=1;
		for(var i=1;i<jml;i++){
			if(document.getElementById("chk"+i).value==1){
				document.getElementById("cekon").value = counter++;
			}
		}
	}else{
		document.getElementById("chk"+b).value=0;
		for(var i=1;i<jml;i++){
			if(document.getElementById("chk"+i).value==0){
				document.getElementById("cekon").value = cekon-1;
			}
		}
	}
}
function pilihsemua(a){
	var jml 	= document.getElementById("jml").value;	
	var cekon 	= document.getElementById("cekon").value;	
	var counter	= 1;
	if(a==0){
		for(var i=1;i<jml;i++){
			document.getElementById("chk"+i).value=1;
			document.getElementById("chkall").value=1;
			document.getElementById("chk"+i).checked = true;
			
			if(document.getElementById("chk"+i).value==1){
				document.getElementById("cekon").value = counter++;
			}
		}
	}else{
		for(var i=1;i<jml;i++){
				document.getElementById("chk"+i).value=0;
				document.getElementById("chk"+i).checked = false;
			}
				document.getElementById("cekon").value = 0;
				document.getElementById("chkall").value=0;
		}
}
function dipales() {
    cek = 'false';
    $s = 0;
    if ((document.getElementById("cekon").value == 0)) {
        alert('Cek data item minimal 1 !!!');
		$s = 1;
		return false;
	}else{
		var kon = window.confirm("Simpan Data ?")

		if (kon) {
			cek = 'true';
			document.getElementById("proses").hidden = true;
			return true;
		}else{
			return false;
		}
	}
}
</script>