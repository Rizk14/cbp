<link rel="stylesheet" type="text/css" href="<?php  echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php  echo  $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
?>
<h3>&nbsp;&nbsp;&nbsp;<?php  echo  'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php  echo  $this->pquery->form_remote_tag(array('url'=>'listbonmasukrekap/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
		$i=0;
?>
    <tr>
	  	<th>No</th>
	  	<th>No Bon Masuk</th>
	  	<th>Tanggal Bon Masuk</th>
	  	<th>Kode Product</th>
	  	<th>Nama Product</th>
	  	<th>Qty</th>
	  	<th>Keterangan</th>
    </tr>
    <tr>
      <tbody>
<?php 
		foreach($isi as $row){
      if($row->d_bm){
          $tmp=explode('-',$row->d_bm);
          $tgl=$tmp[2];
          $bln=$tmp[1];
          $thn=$tmp[0];
          $row->d_bm=$tgl.'-'.$bln.'-'.$thn;
           }
			$i++;
  	    echo "<tr>
                <td>$i</td>
                <td>$row->i_bm</td>
                <td>$row->d_bm</td>
                <td>$row->i_product</td>
                <td>$row->e_product_name</td>
                <td align='right'>$row->n_quantity</td>
                <td align='right'>$row->e_remark</td>
              </tr>";
		}/*<td>".IntervalDays($date1,$date2)."</td>*/
	}
?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
</div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
