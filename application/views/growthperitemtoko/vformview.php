<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<!--<div id='tmp'>-->
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'growthperitemtoko/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo mbulan($bl).' '.$th; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totqnotaly=0;
            $totqnotacy=0;
            $totqnotalm=0;
            $totqnotacm=0;
            $totvnotaly=0;
            $totvnotalm=0;
            $totvnotacy=0;
            $totvnotacm=0;
            foreach($isi as $ro){
              $totqnotaly=$totqnotaly+$ro->qnotaly;
              $totqnotalm=$totqnotalm+$ro->qnotalm;
              $totqnotacm=$totqnotacm+$ro->qnotacm;
              $totqnotacy=$totqnotacy+$ro->qnotacy;
              $totvnotaly=$totvnotaly+$ro->vnotaly;
              $totvnotalm=$totvnotalm+$ro->vnotalm;
              $totvnotacm=$totvnotacm+$ro->vnotacm;
              $totvnotacy=$totvnotacy+$ro->vnotacy;
            }
          }
/*
          if($total){
            $headqnotaly=0;
            $headqnotalm=0;
            $headqnotacy=0;
            $headqnotacm=0;
            foreach($total as $r){
              $headqnotaly=$headqnotaly+$r->qnotaly;
              $headqnotalm=$headqnotalm+$r->qnotalm;
              $headqnotacy=$headqnotacy+$r->qnotacy;
              $headqnotacm=$headqnotacm+$r->qnotacm;
            }
          }
*/
        ?>
        <table class="listtable">
         <tr>
         <th rowspan=3 align=center>Toko</th>
         <th rowspan=3 align=center>Kategori</th>
         <th rowspan=3 align=center>Kd Prod</th>         
         <th rowspan=3 align=center>Nama Produk</th>
         <th colspan=4 align=center><?php echo $prevth; ?></th>
         <th colspan=4 align=center><?php echo $th; ?></th>
         <th colspan=4 align=center>% Growth</th>
         </tr>
         <tr>
         <th colspan=2 align=center><?php echo mbulan($bl); ?></th>
         <th colspan=2>Total</th>
         <th colspan=2 align=center><?php echo mbulan($bl); ?></th>
         <th colspan=2>Total</th>
         <th colspan=2 align=center>MTD</th>
         <th colspan=2 align=center>YTD</th>
         </tr>
         <tr>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totpersenqnotalm=0;
        $totpersenvnotalm=0;
        $totpersenqnotaly=0;
        $totpersenvnotaly=0;
        foreach($isi as $row){
          $i++;
          $persenvnotaselm=0;
          $persenqnotaselm=0;
          $persenvnotasely=0;
          $persenqnotasely=0;

          if($totqnotaly==0){
            $persenqnotaly=0;
          }else{
            $persenqnotaly=($row->qnotaly/$totqnotaly)*100;
          }
          if($totqnotalm==0){
            $persenqnotalm=0;
          }else{
            $persenqnotalm=($row->qnotalm/$totqnotalm)*100;
          }
          if($totqnotacm==0){
            $persenqnotacm=0;
          }else{
            $persenqnotacm=($row->qnotacm/$totqnotacm)*100;
          }
          if($totqnotacy==0){
            $persenqnotacy=0;
          }else{
            $persenqnotacy=($row->qnotacy/$totqnotacy)*100;
          }
          $vnotaselm=$row->vnotacm-$row->vnotalm;
          $qnotaselm=$row->qnotacm-$row->qnotalm;
          $vnotasely=$row->vnotacy-$row->vnotaly;
          $qnotasely=$row->qnotacy-$row->qnotaly;

          if($row->vnotalm==0){
            $persenvnotaselm=0;
          }else{
            $persenvnotaselm=($vnotaselm/$row->vnotalm)*100;
          }
          if($row->qnotalm==0){
            $persenqnotaselm=0;
          }else{
            $persenqnotaselm=($qnotaselm/$row->qnotalm)*100;
          }
          if($row->vnotaly==0){
            $persenvnotasely=0;
          }else{
            $persenvnotasely=($vnotasely/$row->vnotaly)*100;
          }
          if($row->qnotaly==0){
            $persenqnotasely=0;
          }else{
            $persenqnotasely=($qnotasely/$row->qnotaly)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$row->e_customer_name</td>
              <td style='font-size:12px;'>$row->e_product_categoryname</td>
              <td style='font-size:12px;'>$row->i_product</td>
              <td style='font-size:12px;'>$row->e_product_name</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotalm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotalm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotaly)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotaly)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotacm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotacm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotacy)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotacy)."</td>
              
              <td style='font-size:12px;' align=right>".number_format($persenqnotaselm,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persenvnotaselm,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persenqnotasely,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persenvnotasely,2)." %</td>
              
              </tr>";
        }
        
        $totpersenvnotalm=$totvnotacm-$totvnotalm;
        $totpersenqnotalm=$totqnotacm-$totqnotalm;
        $totpersenvnotaly=$totvnotacy-$totvnotaly;
        $totpersenqnotaly=$totqnotacy-$totqnotaly;
        if($totvnotalm==0){
          $totpersenvnotalm=0;
        }else{
          $totpersenvnotalm=($totpersenvnotalm/$totvnotalm)*100;
        }
        if($totqnotalm==0){
          $totpersenqnotalm=0;
        }else{
          $totpersenqnotalm=($totpersenqnotalm/$totqnotalm)*100;
        }
        if($totvnotaly==0){
          $totpersenvnotaly=0;
        }else{
          $totpersenvnotaly=($totpersenvnotaly/$totvnotaly)*100;
        }
        if($totqnotaly==0){
          $totpersenqnotaly=0;
        }else{
          $totpersenqnotaly=($totpersenqnotaly/$totqnotaly)*100;
        }
        echo "<tr>
        <td style='font-size:12px;' colspan=4><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnotalm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotalm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnotaly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotaly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnotacm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotacm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnotacy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotacy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenqnotalm,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvnotalm,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenqnotaly,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvnotaly,2)." %</b></td>
        </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<!--</div>-->
