<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'sjpreceive/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
				<div id="listform">
					<div class="effect">
						<div class="accordion2">
							<table class="listtable">
								<thead>
									<tr>
										<td colspan="10" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="">&nbsp;
											<input type="submit" id="bcari" name="bcari" value="Cari">
										</td>
									</tr>
								</thead>
								<th>No SJP</th>
								<th>Tgl SjP</th>
								<th>BAPB</th>
								<th>DKB</th>
								<th>Tgl DKB</th>
								<th>Area</th>
								<th>SPMB</th>
								<th>Konsinyasi</th>
								<th>SPMB Retur</th>
								<th class="action">Action</th>
								<tbody>
									<?php
									if ($isi) {
										foreach ($isi as $row) {

											$ddkb = $row->d_dkb != '' ? date('d-m-Y', strtotime($row->d_dkb)) : '';

											$tmp = explode('-', $row->d_sjp);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_sjp = $tgl . '-' . $bln . '-' . $thn;

											if ($row->f_spmb_consigment == 't') {
												$kons = 'ya';
											} else {
												$kons = 'tidak';
											}

											if ($row->f_retur == 't') {
												$retur = 'ya';
											} else {
												$retur = 'tidak';
											}

											echo "<tr> ";
											if ($row->f_sjp_cancel == 't') {
												echo "<td><h2>$row->i_sjp</h2></td>";
											} else {
												echo "<td>$row->i_sjp</td>";
											}

											echo "  <td>$row->d_sjp</td>
													<td>$row->i_bapb</td>
													<td>$row->i_dkb</td>
													<td>" . $ddkb . "</td>
													<td>$row->e_area_name</td>
													<td>$row->i_spmb</td>
													<td>$kons</td>
													<td>$retur</td>
													<td class=\"action\">";
											/*
					echo "<a href=\"#\" onclick='show(\"sjpreceive/cform/edit/$row->i_sjp/$row->i_area/$dfrom/$dto/$row->i_spmb/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
*/
											if (($row->i_dkb != '' || $row->i_dkb != null) && ($row->d_dkb > '2023-08-31')) {
												if (($row->i_approve1 != '' || $row->i_approve1 != null)) {
													echo "	<a href=\"#\" onclick='show(\"sjpreceive/cform/edit/$row->i_sjp/$row->i_area/$row->i_spmb/$row->f_retur/\",\"#main\")'>
																<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
															</a>";
												} else {
													echo "	<a href=\"#\" onclick='cekappdkb()'>
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
														</a>";
												}
											} else {
												echo "<a href=\"#\" onclick='show(\"sjpreceive/cform/edit/$row->i_sjp/$row->i_area/$row->i_spmb/f/\",\"#main\")'>
														<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
													</a>";
											}
											echo "</td></tr>";
										}
									}
									?>
								</tbody>
							</table>
							<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
						</div>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
	function cekdkb() {
		alert("SJP Belum dibuatkan DKB-SJP !!");
		return false;
	}

	function cekappdkb() {
		alert("DKB-SJP Belum di Approve !!");
		return false;
	}
</script>