<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'sjpreceive/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="spbform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
              <tr>
                <td width="10%">Tgl SJ</td>
                <?php

                if ($isi->d_sjp) {
                  // if ($isi->d_sjp != '') {
                  $tmp = explode("-", $isi->d_sjp);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_sjp = $hr . "-" . $bl . "-" . $th;
                  // }
                }

                if ($isi->d_spmb) {
                  // if ($isi->d_spmb != '') {
                  $tmp = explode("-", $isi->d_spmb);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_spmb = $hr . "-" . $bl . "-" . $th;
                  // }
                }

                if ($isi->d_sjp_receive) {
                  // if ($isi->d_sjp_receive != '') {
                  $tmp = explode("-", $isi->d_sjp_receive);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_sjp_receive = $hr . "-" . $bl . "-" . $th;
                  // }
                }

                $i_segel_pulangkirim = null;

                if ($isi->i_bapb) {
                  if ($isi->i_bapb != '' || $isi->i_bapb != null) {
                    $bapb = $isi->i_bapb;
                    $areaa = $isi->i_area;
                    $data_bapb = $this->db->query("select * from tm_bapbsjp where i_bapb = '$bapb' and i_area = '$areaa'");

                    if ($data_bapb->num_rows() > 0) {
                      $data_bapb = $data_bapb->row();

                      if ($data_bapb->i_segel_pulangkirim != '' || $data_bapb->i_segel_pulangkirim != null) {
                        $i_segel_pulangkirim = $data_bapb->i_segel_pulangkirim;
                      }
                    }
                  }
                }
                ?>

                <td>
                  <input readonly id="dsj" name="dsj" value="<?php if ($isi->d_sjp) echo $isi->d_sjp; ?>">
                  <input readonly id="isj" name="isj" value="<?php if ($isi->i_sjp) echo $isi->i_sjp; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">Area</td>
                <td>
                  <input readonly id="eareaname" name="eareaname" value="<?php if ($isi->e_area_name) echo $isi->e_area_name; ?>">
                  <input id="iarea" name="iarea" type="hidden" value="<?php if ($isi->i_area) echo $isi->i_area; ?>">
                  <input id="istore" name="istore" type="hidden" value="<?php if ($isi->i_store) echo $isi->i_store; ?>">
                  <input id="i_bapb" name="i_bapb" type="hidden" value="<?php if ($isi->i_bapb) echo $isi->i_bapb; ?>">
                  <input id="i_segel_pulangkirim" name="i_segel_pulangkirim" type="hidden" value="<?php if ($i_segel_pulangkirim) echo $i_segel_pulangkirim; ?>">
                  <input id="istorelocation" name="istorelocation" type="hidden" value="<?php if ($isi->i_store_location) echo $isi->i_store_location; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">SPmB</td>
                <td>
                  <input readonly id="ispmb" name="ispmb" value="<?php if ($isi->i_spmb) echo $isi->i_spmb; ?>">
                  <input readonly id="dspmb" name="dspmb" type="text" value="<?php if ($isi->d_spmb) echo $isi->d_spmb; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">SJ Lama</td>
                <td>
                  <input readonly id="isjold" name="isjold" type="text" value="<?php echo $isi->i_sjp_old; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">Tgl Terima</td>
                <td>
                  <input readonly id="dreceive" name="dreceive" type="text" value="<?php echo $isi->d_sjp_receive; ?>" onclick="showCalendar('',this,this,'','dsjp',0,20,1)">
                </td>
                <input hidden id="dreceivex" name="dreceivex" type="text" value="<?php echo date('d-m-Y'); ?>">
                <input hidden id="ntoleransi" name="ntoleransi" type="text" value="<?php echo $isi->n_tolerance; ?>">
                <input hidden id="dreceivemin" name="dreceivemin" type="text" value="">
                <input hidden id="dapprovefa" name="dapprovefa" type="text" value="<?= $isi->approvefa ?>" readonly>
              </tr>

              <tr>
                <td width="10%">Nilai Kirim</td>
                <td>
                  <input readonly style="text-align:right;" readonly id="vsj" name="vsj" value="<?php if ($isi->v_sjp >= 0) {
                                                                                                  echo number_format((float)$isi->v_sjp);
                                                                                                }; ?>">
                  <input type="hidden" name="jml" id="jml" value="<?php if ($jmlitem) echo $jmlitem; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">Nilai Terima</td>
                <td>
                  <input readonly style="text-align:right;" readonly id="vsjrec" name="vsjrec" value="<?php if ($isi->v_sjp > 0) {
                                                                                                        echo number_format($isi->v_sjp);
                                                                                                      }; ?>">
                </td>
              </tr>

              <tr <?php if ($i_segel_pulangkirim == null) { ?>style="display: none;" <?php } ?>>
                <td width="10%">No Segel Pulang</td>
                <td><input id="i_segel_pulangkirimx" name="i_segel_pulangkirimx" value=""></td>
              </tr>

              <tr>
                <td width="100%" align="center" colspan="4">
                  <input name="login" id="login" value="Simpan" type="submit" onclick="return dipales(parseFloat(document.getElementById('jml').value));">
                  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("sjpreceive/cform/index/","#main")'>
                  <!--        <input <?php #if($pst=='00') echo 'disabled'; 
                                      ?> name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">-->
                  <?php
                  if (($jmlitem != 0) || ($jmlitem != '')) { ?>
                    <input type="button" id="cmdpilihsemua" name="cmdpilihsemua" value="Pilih Semua" onclick="pilihsemua();">
                    <input type="button" id="cmdtidakpilihsemua" name="cmdtidakpilihsemua" value="Tidak Semua" onclick="tidakpilihsemua();">
                  <?php }
                  ?>
                </td>
              </tr>
            </table>
            <div id="detailheader" align="center">
              <?php
              if ($detail) {
              ?>
                <table class="listtable" align="center" style="width: 750px;">
                  <th style="width:25px;" align="center">No</th>
                  <th style="width:63px;" align="center">Kode</th>
                  <th style="width:300px;" align="center">Nama Barang</th>
                  <th style="width:100px;" align="center">Ket</th>
                  <th style="width:73px;" align="center">Jml Krm</th>
                  <th style="width:73px;" align="center">Jml Trm</th>
                  <th style="width:32px;" align="center" class="action">Action</th>
                </table>
              <?php
              }
              ?>
            </div>
            <div id="detailisi" align="center">
              <?php
              if ($detail) {
                $i = 0;
                foreach ($detail as $row) {
                  #              if($row->n_quantity_receive=='' || $row->n_quantity_receive==0) $row->n_quantity_receive=$row->n_quantity_deliver;
                  if ($row->n_quantity_receive == '' || $row->n_quantity_receive == 0) $jmlterima = $row->n_quantity_deliver;
                  if ($row->n_quantity_receive == '') $row->n_quantity_receive == 0;
                  $vtotal = $row->v_unit_price * $row->n_quantity_deliver;
                  echo '<table class="listtable" align="center" style="width:750px;">';
                  $i++;
                  echo "<tbody>
								<tr>
		    						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"><input type=\"hidden\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
								<td style=\"width:103px;\"><input style=\"width:103px;\" type=\"text\" id=\"eremark$i\"
								name=\"eremark$i\" value=\"$row->e_remark\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$row->v_unit_price\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_quantity_deliver\">
                <input type=\"hidden\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_quantity_order\"></td>
								<td style=\"width:74px;\"><input style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"nreceive$i\" name=\"nreceive$i\" value=\"$jmlterima\"
								onkeyup=\"hitungnilai(" . $i . ")\" autocomplete=\"off\"><input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_quantity_receive\">
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$vtotal\"></td>
								<td style=\"width:60px;\" align=\"center\"><input type='checkbox' name='chk" . $i . "' id='chk" . $i . "' 	
																			value='on' checked onclick='pilihan(this.value," . $i . ")'</td>
							</tr>
						  </tbody></table>";
                }
              }
              ?>
            </div>
            <div id="pesan"></div>
          </div>
        </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function tambah_item(a) {
    so_inner = document.getElementById("detailheader").innerHTML;
    si_inner = document.getElementById("detailisi").innerHTML;
    if (so_inner == '') {
      so_inner = '<table class="listtable" align="center" style="width: 750px;">';
      so_inner += '<th style="width:25px;" align="center">No</th>';
      so_inner += '<th style="width:63px;" align="center">Kode</th>';
      so_inner += '<th style="width:300px;" align="center">Nama Barang</th>';
      so_inner += '<th style="width:100px;" align="center">Ket</th>';
      so_inner += '<th style="width:73px;" align="center">Jml Krm</th>';
      so_inner += '<th style="width:73px;" align="center">Jml Trm</th>';
      so_inner += '<th style="width:32px;" align="center" class="action">Action</th></table>';
      document.getElementById("detailheader").innerHTML = so_inner;
    } else {
      so_inner = '';
    }
    if (si_inner == '') {
      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
      juml = document.getElementById("jml").value;
      si_inner =
        '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' +
        a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a +
        '" value=""></td>';
      si_inner += '<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct' + a +
        '" name="iproduct' + a + '" value=""></td>';
      si_inner += '<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname' + a +
        '" name="eproductname' + a + '" value=""><input type="hidden" id="emotifname' + a + '" name="emotifname' + a +
        '" value=""></td>';
      si_inner += '<td style="width:103px;"><input style="width:103px;" type="text" id="eremark' + a + '" name="eremark' +
        a + '" value=""><input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
      si_inner += '<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="ndeliver' + a +
        '" name="ndeliver' + a + '" value=""><input type="hidden" id="norder' + a + '" name="norder' + a +
        '" value=""></td>';
      si_inner += '<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="nreceive' + a +
        '" name="nreceive' + a + '" value="" onkeyup="hitungnilai(' + a +
        ')" autocomplete="off"><input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value=""></td>';
      si_inner += '<td style="width:60px;" align="center"><input type="checkbox" name="chk' + a + '" id="chk' + a +
        '" value="on" checked onclick="pilihan(this.value,' + a + ')"</td></tr></tbody></table>';
    } else {
      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
      juml = document.getElementById("jml").value;
      si_inner +=
        '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' +
        a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a +
        '" value=""></td>';
      si_inner += '<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct' + a +
        '" name="iproduct' + a + '" value=""></td>';
      si_inner += '<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname' + a +
        '" name="eproductname' + a + '" value=""><input type="hidden" id="emotifname' + a + '" name="emotifname' + a +
        '" value=""></td>';
      si_inner += '<td style="width:103px;"><input style="width:103px;" type="text" id="eremark' + a + '" name="eremark' +
        a + '" value=""><input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
      si_inner += '<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="ndeliver' + a +
        '" name="ndeliver' + a + '" value=""><input type="hidden" id="norder' + a + '" name="norder' + a +
        '" value=""></td>';
      si_inner += '<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="nreceive' + a +
        '" name="nreceive' + a + '" value="" onkeyup="hitungnilai(' + a +
        ')" autocomplete="off"><input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value=""></td>';
      si_inner += '<td style="width:60px;" align="center"><input type="checkbox" name="chk' + a + '" id="chk' + a +
        '" value="on" checked onclick="pilihan(this.value,' + a + ')"</td></tr></tbody></table>';
    }
    j = 0;
    var baris = Array()
    var motif = Array();
    var iproduct = Array();
    var eproductname = Array();
    var emotifname = Array();
    var eremark = Array();
    var vproductmill = Array();
    var norder = Array();
    var ndeliver = Array();
    var nreceive = Array();
    var vtotal = Array();
    var chk = Array();
    for (i = 1; i < a; i++) {
      j++;
      baris[j] = document.getElementById("baris" + i).value;
      motif[j] = document.getElementById("motif" + i).value;
      iproduct[j] = document.getElementById("iproduct" + i).value;
      eproductname[j] = document.getElementById("eproductname" + i).value;
      emotifname[j] = document.getElementById("emotifname" + i).value;
      eremark[j] = document.getElementById("eremark" + i).value;
      vproductmill[j] = document.getElementById("vproductmill" + i).value;
      ndeliver[j] = document.getElementById("ndeliver" + i).value;
      norder[j] = document.getElementById("norder" + i).value;
      nreceive[j] = document.getElementById("nreceive" + i).value;
      vtotal[j] = document.getElementById("vtotal" + i).value;
      chk[j] = document.getElementById("chk" + i).value;
    }
    document.getElementById("detailisi").innerHTML = si_inner;
    j = 0;
    for (i = 1; i < a; i++) {
      j++;
      document.getElementById("baris" + i).value = baris[j];
      document.getElementById("motif" + i).value = motif[j];
      document.getElementById("iproduct" + i).value = iproduct[j];
      document.getElementById("eproductname" + i).value = eproductname[j];
      document.getElementById("emotifname" + i).value = emotifname[j];
      document.getElementById("eremark" + i).value = eremark[j];
      document.getElementById("vproductmill" + i).value = vproductmill[j];
      document.getElementById("norder" + i).value = norder[j];
      document.getElementById("ndeliver" + i).value = ndeliver[j];
      document.getElementById("nreceive" + i).value = nreceive[j];
      document.getElementById("vtotal" + i).value = vtotal[j];
      document.getElementById("chk" + i).value = chk[j];
    }
    showModal("sjpreceive/cform/product/" + a + "/x01/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }

  function dipales(a) {
    cek = 'false';
    if ((document.getElementById("iarea").value != '') &&
      (document.getElementById("ispmb").value != '') &&
      (document.getElementById("dreceive").value != '')) {
      if (a == 0) {
        alert('Isi data item minimal 1 !!!');
        return false;
      } else {
        for (i = 1; i <= a; i++) {
          if ((document.getElementById("iproduct" + i).value == '') ||
            (document.getElementById("eproductname" + i).value == '') ||
            (document.getElementById("ndeliver" + i).value == '')) {
            alert('Data item masih ada yang salah !!!');
            exit();
            cek = 'false';
            return false;
          } else {
            cek = 'true';
          }
        }
      }


      let i_segel_pulangkirim = document.getElementById("i_segel_pulangkirim").value;
      let i_segel_pulangkirimx = document.getElementById("i_segel_pulangkirimx").value;

      if ((i_segel_pulangkirim != '')) {
        if (i_segel_pulangkirim != i_segel_pulangkirimx) {
          alert('No Segel Tidak Sesuai !');
          return false;
        }
      }


      if (cek == 'true') {
        document.getElementById("login").hidden = true;
        document.getElementById("cmdtambahitem").hidden = true;
        document.getElementById("cmdpilihsemua").hidden = true;
        document.getElementById("cmdtidakpilihsemua").hidden = true;
      } else {
        document.getElementById("login").hidden = false;
        return false;
      }
    } else {
      alert('Data header masih ada yang salah !!!');
      return false;
    }
  }

  function hitungnilai(brs) {
    var tot = 0;
    ord = document.getElementById("nreceive" + brs).value;
    if (isNaN(parseFloat(ord))) {
      alert("Input harus numerik");
    } else {
      hrg = formatulang(document.getElementById("vproductmill" + brs).value);
      qty = formatulang(ord);
      vhrg = parseFloat(hrg) * parseFloat(qty);
      document.getElementById("vtotal" + brs).value = formatcemua(vhrg);

      jml = parseFloat(document.getElementById("jml").value);
      for (i = 1; i <= jml; i++) {
        if (document.getElementById("chk" + i).value == 'on') {
          tot += parseFloat(formatulang(document.getElementById("vtotal" + i).value));
        }
      }
      document.getElementById("vsjrec").value = formatcemua(tot);
    }
  }

  function pilihan(a, b) {
    if (a == '') {
      document.getElementById("chk" + b).value = 'on';
    } else {
      document.getElementById("chk" + b).value = '';
    }
    hitungnilai(b);
  }

  function view_spmb(a) {
    showModal("sjpreceive/cform/spmb/" + a + "/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }

  function pilihsemua() {
    var jml = parseFloat(document.getElementById("jml").value);
    for (i = 1; i <= jml; i++) {
      document.getElementById("chk" + i).checked = true;
      document.getElementById("chk" + i).value = 'on';
    }
    for (i = 1; i <= jml; i++) {
      hitungnilai(i);
    }
  }

  function tidakpilihsemua() {
    var jml = parseFloat(document.getElementById("jml").value);
    for (i = 1; i <= jml; i++) {
      document.getElementById("chk" + i).checked = false;
      document.getElementById("chk" + i).value = '';


    }
    for (i = 1; i <= jml; i++) {
      hitungnilai(i);
    }
  }

  function afterSetDateValue(ref_field, target_field, date) {
    dtmp = date.split('-');
    thn = dtmp[2];
    bln = dtmp[1];
    hr = dtmp[0];
    var rec = new Date(thn + "-" + bln + "-" + hr);

    // Mengambil kode hari (0 = Minggu, 1 = Senin, dst.)
    var kodeHari = rec.getDay();

    // Memeriksa apakah tanggal adalah hari Minggu
    if (kodeHari === 0) {
      alert("Tidak boleh receive dihari Minggu!!");
      document.getElementById('dreceive').value = '';
      return false;
    } else {
      cekminimalterima();
    }
  }

  function cekminimalterima() {
    isjp = document.getElementById('isj').value;
    dsjp = document.getElementById('dsj').value;
    iarea = document.getElementById('iarea').value;
    dreceive = document.getElementById('dreceive').value;
    fretur = '<?= $isi->f_retur ?>';

    $.ajax({
      type: "POST",
      url: "<?php echo site_url('sjpreceive/cform/hitungminimalterima'); ?>",
      data: {
        'isjp': isjp,
        'dsjp': dsjp,
        'iarea': iarea,
        'fretur': fretur,
        'dreceive': dreceive,
      },
      success: function(data) {
        cektanggal(data);
      },

      error: function(XMLHttpRequest) {
        alert(XMLHttpRequest.responseText);
      }
    })
  }

  function cektanggal(a) {
    dsj = document.getElementById('dsj').value;
    dreceive = document.getElementById('dreceive').value;
    dreceivex = document.getElementById('dreceivex').value;
    dapprovefa = document.getElementById('dapprovefa').value;
    document.getElementById('dreceivemin').value = a;

    /* VALIDASI TANGGAL TERIMA < TANGGAL HARI INI */
    dtmp = dsj.split('-');
    thnsj = dtmp[2];
    blnsj = dtmp[1];
    hrsj = dtmp[0];

    if (dreceive != '') {
      dtmp = dreceive.split('-');
      thnrec = dtmp[2];
      blnrec = dtmp[1];
      hrrec = dtmp[0];

      if (thnsj > thnrec) {
        alert('Tanggal terima tidak boleh lebih kecil dari tanggal kirim !!!');
        document.getElementById('dreceive').value = '';
      } else if (thnsj == thnrec) {
        if (blnsj > blnrec) {
          alert('Tanggal terima tidak boleh lebih kecil dari tanggal kirim !!!');
          document.getElementById('dreceive').value = '';
        } else if (blnsj == blnrec) {
          if (hrsj > hrrec) {
            alert('Tanggal terima tidak boleh lebih kecil dari tanggal kirim !!!');
            document.getElementById('dreceive').value = '';
          }
        }
      }
    }

    /* VALIDASI TANGGAL TERIMA > TANGGAL HARI INI */
    dtmp = dreceive.split('-');
    thndkb = dtmp[2];
    blndkb = dtmp[1];
    hrdkb = dtmp[0];
    dreceive = thndkb + '-' + blndkb + '-' + hrdkb;

    /* COMMENT 14 AGS JADI BISA APPROVE SELAMA TANGGAL TERIMA TIDAK LEBIH BESAR DARI (TANGGAL SJP + TOLERANSI APPROVE)  */
    // dtmp = dreceivex.split('-');
    // thndkbx = dtmp[2];
    // blndkbx = dtmp[1];
    // hrdkbx = dtmp[0];

    // console.log(thnmin + '-' + blnmin + '-' + hrmin)

    // if (thndkb > thndkbx) {
    //   alert('Tanggal Receive tidak boleh lebih besar dari hari ini !!!');
    //   document.getElementById('dreceive').value = '';
    // } else if (thndkb == thndkbx) {
    //   if (blndkb > blndkbx) {
    //     alert('Tanggal Receive tidak boleh lebih besar dari hari ini !!!');
    //     document.getElementById('dreceive').value = '';
    //   } else if (blndkb == blndkbx) {
    //     if (hrdkb > hrdkbx) {
    //       alert('Tanggal Receive tidak boleh lebih besar dari hari ini !!!');
    //       document.getElementById('dreceive').value = '';
    //     }
    //   }
    // }

    /* VALIDASI BOLEH APPROVE (TANGGAL SJP + TOLERANSI APPROVE) */
    dtmp2 = a.split('-');
    hrmin = dtmp2[2];
    blnmin = dtmp2[1];
    thnmin = dtmp2[0];
    dlimit = thnmin + '-' + blnmin + '-' + hrmin;
    dlimitx = hrmin + '-' + blnmin + '-' + thnmin;

    dtmpfa = dapprovefa.split('-');
    hrfa = dtmpfa[2];
    blnfa = dtmpfa[1];
    thnfa = dtmpfa[0];
    dfa = hrfa + '-' + blnfa + '-' + thnfa;

    // console.log(hrmin + '-' + blnmin + '-' + thnmin);
    // console.log(dreceive + ' x ' + dlimit);

    if (dreceive < dlimit) {
      if ((dapprovefa == null || dapprovefa == '')) {
        alert('Tanggal Minimal Receive = ' + dlimitx + '!');
        document.getElementById('dreceive').value = '';
      } else {
        alert('Tanggal Minimal Receive = ' + dfa + '!!');
        document.getElementById('dreceive').value = '';
      }
    }
  }
</script>