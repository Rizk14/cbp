<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'area/cform/update', 'update' => '#main', 'type' => 'post')); ?>
			<div id="masterareaformupdate">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="12%">Kode Area</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'iarea',
													'id'          => 'iarea',
													'value'       => $isi->i_area,
													'readonly'	=> 'true',
													'maxlength'   => '2'
												);
												echo form_input($data); ?></td>
								<td width="12%">Nama Area</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'eareaname',
													'id'          => 'eareaname',
													'value'       => $isi->e_area_name,
													'maxlength'   => '50'
												);
												echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="12%">Nama Jenis Area</td>
								<td width="1%">:</td>
								<td width="37%"><input readonly name="eareatypename" id="eareatypename" value="<?php echo $isi->e_area_typename; ?>" onclick='showModal("area/cform/areatype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									<input readonly type="hidden" name="iareatype" id="iareatype" value="<?php echo $isi->i_area_type; ?>" onclick='showModal("area/cform/areatype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
								<td width="12%">Gudang</td>
								<td width="1%">:</td>
								<td width="37%"><input readonly name="estorename" id="estorename" value="<?php echo $isi->e_store_name; ?>" onclick='showModal("area/cform/store/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									<input readonly type="hidden" name="istore" id="istore" value="<?php echo $isi->i_store; ?>" onclick='showModal("area/cform/store/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							</tr>
							<tr>
								<td width="12%">Alamat</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'eareaaddress',
													'id'          => 'eareaaddress',
													'value'       => $isi->e_area_address,
													'maxlength'   => '200'
												);
												echo form_input($data); ?></td>
								<td width="12%">Kota</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'eareacity',
													'id'          => 'eareacity',
													'value'       => $isi->e_area_city
												);
												echo form_input($data);
												?>
								</td>
							</tr>
							<tr>
								<td width="12%">Inisial</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'eareashortname',
													'id'          => 'eareashortname',
													'value'       => $isi->e_area_shortname,
													'maxlength'   => '50'
												);
												echo form_input($data); ?></td>
								<td width="12%">Telepon</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'eareaphone',
													'id'          => 'eareaphone',
													'value'       => $isi->e_area_phone
												);
												echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="12%">Toleransi</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'nareatoleransi',
													'id'          => 'nareatoleransi',
													'value'       => $isi->n_area_toleransi,
													'maxlength'   => '50'
												);
												echo form_input($data); ?></td>
								<td width="12%">Keterangan</td>
								<td width="1%">:</td>
								<td width="37%"><?php 
												$data = array(
													'name'        => 'earearemark',
													'id'          => 'earearemark',
													'value'       => $isi->e_area_remark
												);
												echo form_input($data);
												?>
								</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;</td>
								<td colspan="4">
									<input name="login" id="login" value="Simpan" type="submit">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('area/cform/','#main');">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>