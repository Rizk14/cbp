<div id="tabbed_box_1" class="tabbed_box">
	<div class="tabbed_area">
		<ul class="tabs">
			<li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Area</a></li>
			<li><a href="#" class="tab" onclick="sitab('content_2')">Area</a></li>
		</ul>
		<div id="content_1" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'area/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
						<div class="effect">
							<div class="accordion2">
								<table class="listtable">
									<thead>
										<tr>
											<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
										</tr>
									</thead>
									<th>Kode Area</th>
									<th>Nama Area</th>
									<th>Nama Area Type</th>
									<th>Tanggal Entry</th>
									<th class="action">Action</th>
									<tbody>
										<?php 
										if ($isi) {
											foreach ($isi as $row) {
												echo "<tr> 
				  <td>$row->i_area</td>
				  <td>$row->e_area_name</td>
				  <td>$row->e_area_typename</td>
				  <td>$row->d_area_entry</td>";
												echo "<td class=\"action\"><a href=\"#\" onclick='show(\"area/cform/edit/$row->i_area\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>"; #"&nbsp;&nbsp;";
												#			  echo "<a href=\"#\" onclick='hapus(\"area/cform/delete/$row->i_area\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
												echo "</td></tr>";
											}
										}
										?>
									</tbody>
								</table>
								<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
		<div id="content_2" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url' => 'area/cform/simpan', 'update' => '#main', 'type' => 'post')); ?>
						<div id="masterareaform">
							<div class="effect">
								<div class="accordion2">
									<table class="mastertable">
										<tr>
											<td width="12%">Kode Area</td>
											<td width="1%">:</td>
											<td width="37%"><?php 
															$data = array(
																'name'        => 'iarea',
																'id'          => 'iarea',
																'value'       => '',
																'maxlength'   => '2'
															);
															echo form_input($data); ?></td>
											<td width="12%">Nama Area</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="eareaname" id="eareaname" maxlength="50" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'eareaname',
			              'id'          => 'eareaname',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?>
											</td>
										</tr>
										<tr>
											<td width="12%">Nama Jenis Area</td>
											<td width="1%">:</td>
											<td width="37%"><input readonly name="eareatypename" id="eareatypename" value="" onclick='showModal("area/cform/areatype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input readonly type="hidden" name="iareatype" id="iareatype" value="" onclick='showModal("area/cform/areatype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
											<td width="12%">Gudang</td>
											<td width="1%">:</td>
											<td width="37%"><input readonly name="estorename" id="estorename" value="" onclick='showModal("area/cform/store/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
												<input readonly type="hidden" name="istore" id="istore" value="" onclick='showModal("area/cform/store/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											</td>
										</tr>
										<tr>
											<td width="12%">Alamat</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="eareaaddress" id="eareaaddress" maxlength="200" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'eareaaddress',
			              'id'          => 'eareaaddress',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?>
											</td>
											<td width="12%">Kota</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="eareacity" id="eareacity" maxlength="50" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'eareacity',
			              'id'          => 'eareacity',
			              'value'       => '');
				echo form_input($data);
				*/ ?>
											</td>
										</tr>
										<tr>
											<td width="12%">Inisial</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="eareashortname" id="eareashortname" maxlength="50" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'eareashortname',
			              'id'          => 'eareashortname',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?>
											</td>
											<td width="12%">Telepon</td>
											<td width="1%">:</td>
											<td width="37%"><?php 
															$data = array(
																'name'        => 'eareaphone',
																'id'          => 'eareaphone',
																'value'       => ''
															);
															echo form_input($data); ?></td>
										</tr>
										<tr>
											<td width="12%">Toleransi</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="nareatoleransi" id="nareatoleransi" maxlength="50" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'nareatoleransi',
			              'id'          => 'nareatoleransi',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?>
											</td>
											<td width="12%">Keterangan</td>
											<td width="1%">:</td>
											<td width="37%">
												<input type="text" name="earearemark" id="earearemark" maxlength="50" value="" onkeyup="gede(this)">
												<?php /* 
				$data = array(
			              'name'        => 'earearemark',
			              'id'          => 'earearemark',
			              'value'       => '');
				echo form_input($data);
				*/ ?>
											</td>
										</tr>
										<tr>
											<td colspan="2">&nbsp;</td>
											<td colspan="4">
												<input name="login" id="login" value="Simpan" type="submit">
												<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('area/cform/','#main');">
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>