<table class="maintable">
  <tr>
    <td align="left">
<!--	<?php echo form_open('laporanpenjualancabang/cform/cari', array('id' => 'listform'));?>-->
  <?php echo $this->pquery->form_remote_tag(array('url'=>'laporanpenjualancabang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
      $periode=$iperiode;
		  $a=substr($periode,0,4);
    	$b=substr($periode,4,2);
		  $periode=mbulan($b)." - ".$a;
		?>
		  <table class="mastertable">
			<tr><td style="width:100px;">Periode</td><td style="width:5px;">:</td><td colspan=2><?php echo $periode; ?></td><td style="width:80px"></td><td></td></tr>
		  </table>
    	  <table class="listtable">
			<th>Area</th>
			<th>Target</th>
			<th>SPB(GROSS)</th>
			<th>%</th>
      <th>SPB(NETTO)</th>
      <th>%</th>
      <th>Nota(GROSS)</th>
			<th>%</th>
      <th>Nota(NETTO)</th>
      <th>%</th>
      <th>Retur</th>
			<th>%</th>
			<tbody>
	      <?php 
		if($isi){
			$ret=0;
      $target=0;
      $spb=0;
      $spbnetto=0;
      $ntgross=0;
      $ntnetto=0;
      $ntreguler=0;
      $ntbaby=0;
			foreach($isi as $row){
/*
			  if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_grossinsentif/$row->v_target)*100,2);
        }else{
          $persen='0.00';
        }
        if($row->v_spb_gross==null || $row->v_spb_gross=='')$row->v_spb_gross=0;
        if($row->v_target!=0){
          $persenspb=number_format(($row->v_spb_gross/$row->v_target)*100,2);
        }else{
          $persenspb='0.00';
        }
        if($row->v_retur_insentif==null || $row->v_retur_insentif=='')$row->v_retur_insentif=0;
        if($row->v_target!=0){
          $persenret=number_format(($row->v_retur_insentif/$row->v_target)*100,2);
        }else{
          $persenret='0.00';
        }
*/
			  if($row->v_nota_gross==null || $row->v_nota_gross=='')$row->v_nota_gross=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_gross/$row->v_target)*100,2);
          $persennetto=number_format(($row->v_nota_netto/$row->v_target)*100,2);
        }else{
          $persen='0.00';
          $persennetto='0.00';
        }
        if($row->v_spb_gross==null || $row->v_spb_gross=='')$row->v_spb_gross=0;
        if($row->v_target!=0){
          $persenspb=number_format(($row->v_spb_gross/$row->v_target)*100,2);
          $persenspbnetto=number_format(($row->v_spb_netto/$row->v_target)*100,2);
        }else{
          $persenspb='0.00';
          $persenspbnetto='0.00';
        }
        if($row->v_retur_insentif==null || $row->v_retur_insentif=='')$row->v_retur_insentif=0;
        if($row->v_target!=0){
          $persenret=number_format(($row->v_retur_insentif/$row->v_target)*100,2);
        }else{
          $persenret='0.00';
        }
			  echo "<tr> 
				  <td style='font-size:12px;'>$row->e_area_name</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_target)."</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_spb_gross)."</td>
				  <td style='font-size:12px;' align=right>".$persenspb." %</td>
          <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_spb_netto)."</td>
          <td style='font-size:12px;' align=right>".$persenspbnetto." %</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_nota_gross)."</td>
				  <td style='font-size:12px;' align=right>".$persen." %</td>
          <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_nota_netto)."</td>
          <td style='font-size:12px;' align=right>".$persennetto." %</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_retur_insentif)."</td>
				  <td style='font-size:12px;' align=right>".$persenret." %</td>
</tr>";
        $target=$target+$row->v_target;
        $ntgross=$ntgross+$row->v_nota_gross;
        $ntnetto=$ntnetto+$row->v_nota_netto;
        $spb=$spb+$row->v_spb_gross;
        $spbnetto=$spbnetto+$row->v_spb_netto;
        $ret=$ret+$row->v_retur_insentif;
			}
		  echo "<tr> 
		  <td style='font-size:12px;'><b>Total</b></td>
		  <td style='font-size:12px;' align=right><b>Rp. ".number_format($target)."</b></td>
		  <td style='font-size:12px;' align=right><b>Rp. ".number_format($spb)."</b></td>
		  <td style='font-size:12px;' align=right>&nbsp;</td>
      <td style='font-size:12px;' align=right><b>Rp. ".number_format($spbnetto)."</b></td>
      <td style='font-size:12px;' align=right>&nbsp;</td>
		  <td style='font-size:12px;' align=right><b>Rp. ".number_format($ntgross)."</b></td>
		  <td style='font-size:12px;' align=right>&nbsp;</td>
      <td style='font-size:12px;' align=right><b>Rp. ".number_format($ntnetto)."</b></td>
      <td style='font-size:12px;' align=right>&nbsp;</td>
		  <td style='font-size:12px;' align=right><b>Rp. ".number_format($ret)."</b></td>
		  <td style='font-size:12px;' align=right>&nbsp;</td></tr>";

		}
		echo "<input type=\"hidden\" id=\"iperiode\" name=\"iperiode\" value=\"$iperiode\">
			  <input type=\"hidden\" id=\"iarea\" name=\"iarea\" value=\"\">
		     ";
	      ?>
	    </tbody>
<tr>
		<td colspan="12" align="center"><input name="cmdsales" id="cmdsales" value="Per Sales" type="button" onclick="persales('<?php echo $iperiode; ?>','<?php echo $row->i_area;?>')"><input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('laporanpenjualancabang/cform/','#main')"></td>
	      </tr>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(a,c){
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/laporanpenjualancabang/cform/viewdetail";
	  formna.submit();
  }
  function persales(iperiode,area){
    lebar =1366;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/laporanpenjualancabang/cform/persales/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
