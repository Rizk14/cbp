<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listreturvsomsetnasional/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
	  <th rowspan=2>AREA</th>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($blasal,'integer');
	  }
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th colspan=3>Jan</th>';
        break;
      case '2' :
        echo '<th colspan=3>Feb</th>';
        break;
      case '3' :
        echo '<th colspan=3>Mar</th>';
        break;
      case '4' :
        echo '<th colspan=3>Apr</th>';
        break;
      case '5' :
        echo '<th colspan=3>Mei</th>';
        break;
      case '6' :
        echo '<th colspan=3>Jun</th>';
        break;
      case '7' :
        echo '<th colspan=3>Jul</th>';
        break;
      case '8' :
        echo '<th colspan=3>Agu</th>';
        break;
      case '9' :
        echo '<th colspan=3>Sep</th>';
        break;
      case '10' :
        echo '<th colspan=3>Okt</th>';
        break;
      case '11' :
        echo '<th colspan=3>Nov</th>';
        break;
      case '12' :
        echo '<th colspan=3>Des</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    $bl=$blasal;
    echo '</tr><tr>';
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '2' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '3' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '4' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '5' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '6' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '7' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '8' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '9' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '10' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '11' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      case '12' :
        echo '<th>Omset</th><th>Retur</th><th>%</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
?>
    </tr>
    <tbody>
<?php 
    $bl=$blasal;
    $area='';
    $grandomset01=0;
    $grandomset02=0;
    $grandomset03=0;
    $grandomset04=0;
    $grandomset05=0;
    $grandomset06=0;
    $grandomset07=0;
    $grandomset08=0;
    $grandomset09=0;
    $grandomset10=0;
    $grandomset11=0;
    $grandomset12=0;
    $grandretur01=0;
    $grandretur02=0;
    $grandretur03=0;
    $grandretur04=0;
    $grandretur05=0;
    $grandretur06=0;
    $grandretur07=0;
    $grandretur08=0;
    $grandretur09=0;
    $grandretur10=0;
    $grandretur11=0;
    $grandretur12=0;
		foreach($isi as $row){
      if($row->omset>0){
        $persen=($row->retur*100)/$row->omset;
      }else{
        $persen=0;
      }
      if($area==''){
        if($row->bln==$bl){
          echo '<tr><td>'.$row->kode.'</td>';
          echo '<td align=right>'.number_format($row->omset).'</td>';
          echo '<td align=right>'.number_format($row->retur).'</td>';
          echo '<td align=right>'.number_format($persen,2).'%</td>';
          switch ($row->bln){
          case '1' :
            $grandomset01=$grandomset01+$row->omset;
            $grandretur01=$grandretur01+$row->retur;
            break;
          case '2' :
            $grandomset02=$grandomset02+$row->omset;
            $grandretur02=$grandretur02+$row->retur;
            break;
          case '3' :
            $grandomset03=$grandomset03+$row->omset;
            $grandretur03=$grandretur03+$row->retur;
            break;
          case '4' :
            $grandomset04=$grandomset04+$row->omset;
            $grandretur04=$grandretur04+$row->retur;
            break;
          case '5' :
            $grandomset05=$grandomset05+$row->omset;
            $grandretur05=$grandretur05+$row->retur;
            break;
          case '6' :
            $grandomset06=$grandomset06+$row->omset;
            $grandretur06=$grandretur06+$row->retur;
            break;
          case '7' :
            $grandomset07=$grandomset07+$row->omset;
            $grandretur07=$grandretur07+$row->retur;
            break;
          case '8' :
            $grandomset08=$grandomset08+$row->omset;
            $grandretur08=$grandretur08+$row->retur;
            break;
          case '9' :
            $grandomset09=$grandomset09+$row->omset;
            $grandretur09=$grandretur09+$row->retur;
            break;
          case '10' :
            $grandomset10=$grandomset10+$row->omset;
            $grandretur10=$grandretur10+$row->retur;
            break;
          case '11' :
            $grandomset11=$grandomset11+$row->omset;
            $grandretur11=$grandretur11+$row->retur;
            break;
          case '12' :
            $grandomset12=$grandomset12+$row->omset;
            $grandretur12=$grandretur12+$row->retur;
            break;
          }
          $blakhir=$bl;
        }else{
          $bl=$blasal;
          for($i=1;$i<=$interval;$i++){
            if($row->bln==$bl){
              echo '<tr><td>'.$row->kode.'</td>';
              echo '<td align=right>'.number_format($row->omset).'</td>';
              echo '<td align=right>'.number_format($row->retur).'</td>';
              echo '<td align=right>'.number_format($persen,2).'%</td>';
              switch ($row->bln){
              case '1' :
                $grandomset01=$grandomset01+$row->omset;
                $grandretur01=$grandretur01+$row->retur;
                break;
              case '2' :
                $grandomset02=$grandomset02+$row->omset;
                $grandretur02=$grandretur02+$row->retur;
                break;
              case '3' :
                $grandomset03=$grandomset03+$row->omset;
                $grandretur03=$grandretur03+$row->retur;
                break;
              case '4' :
                $grandomset04=$grandomset04+$row->omset;
                $grandretur04=$grandretur04+$row->retur;
                break;
              case '5' :
                $grandomset05=$grandomset05+$row->omset;
                $grandretur05=$grandretur05+$row->retur;
                break;
              case '6' :
                $grandomset06=$grandomset06+$row->omset;
                $grandretur06=$grandretur06+$row->retur;
                break;
              case '7' :
                $grandomset07=$grandomset07+$row->omset;
                $grandretur07=$grandretur07+$row->retur;
                break;
              case '8' :
                $grandomset08=$grandomset08+$row->omset;
                $grandretur08=$grandretur08+$row->retur;
                break;
              case '9' :
                $grandomset09=$grandomset09+$row->omset;
                $grandretur09=$grandretur09+$row->retur;
                break;
              case '10' :
                $grandomset10=$grandomset10+$row->omset;
                $grandretur10=$grandretur10+$row->retur;
                break;
              case '11' :
                $grandomset11=$grandomset11+$row->omset;
                $grandretur11=$grandretur11+$row->retur;
                break;
              case '12' :
                $grandomset12=$grandomset12+$row->omset;
                $grandretur12=$grandretur12+$row->retur;
                break;
              }
              $blakhir=$bl;
              break;
            }else{
              echo '<tr><td>'.$row->kode.'</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0%</td>';
            }
            $bl++;
            if($bl==13)$bl=1;
          }
        }
      }elseif($area==$row->kode){
        if($row->bln==$bl){
          echo '<td align=right>'.number_format($row->omset).'</td>';
          echo '<td align=right>'.number_format($row->retur).'</td>';
          echo '<td align=right>'.number_format($persen,2).'%</td>';
          switch ($row->bln){
          case '1' :
            $grandomset01=$grandomset01+$row->omset;
            $grandretur01=$grandretur01+$row->retur;
            break;
          case '2' :
            $grandomset02=$grandomset02+$row->omset;
            $grandretur02=$grandretur02+$row->retur;
            break;
          case '3' :
            $grandomset03=$grandomset03+$row->omset;
            $grandretur03=$grandretur03+$row->retur;
            break;
          case '4' :
            $grandomset04=$grandomset04+$row->omset;
            $grandretur04=$grandretur04+$row->retur;
            break;
          case '5' :
            $grandomset05=$grandomset05+$row->omset;
            $grandretur05=$grandretur05+$row->retur;
            break;
          case '6' :
            $grandomset06=$grandomset06+$row->omset;
            $grandretur06=$grandretur06+$row->retur;
            break;
          case '7' :
            $grandomset07=$grandomset07+$row->omset;
            $grandretur07=$grandretur07+$row->retur;
            break;
          case '8' :
            $grandomset08=$grandomset08+$row->omset;
            $grandretur08=$grandretur08+$row->retur;
            break;
          case '9' :
            $grandomset09=$grandomset09+$row->omset;
            $grandretur09=$grandretur09+$row->retur;
            break;
          case '10' :
            $grandomset10=$grandomset10+$row->omset;
            $grandretur10=$grandretur10+$row->retur;
            break;
          case '11' :
            $grandomset11=$grandomset11+$row->omset;
            $grandretur11=$grandretur11+$row->retur;
            break;
          case '12' :
            $grandomset12=$grandomset12+$row->omset;
            $grandretur12=$grandretur12+$row->retur;
            break;
          }
          $blakhir=$bl;
        }else{
          for($i=1;$i<=$interval;$i++){
            if($row->bln==$bl){
              echo '<td align=right>'.number_format($row->omset).'</td>';
              echo '<td align=right>'.number_format($row->retur).'</td>';
              echo '<td align=right>'.number_format($persen,2).'%</td>';
              switch ($row->bln){
              case '1' :
                $grandomset01=$grandomset01+$row->omset;
                $grandretur01=$grandretur01+$row->retur;
                break;
              case '2' :
                $grandomset02=$grandomset02+$row->omset;
                $grandretur02=$grandretur02+$row->retur;
                break;
              case '3' :
                $grandomset03=$grandomset03+$row->omset;
                $grandretur03=$grandretur03+$row->retur;
                break;
              case '4' :
                $grandomset04=$grandomset04+$row->omset;
                $grandretur04=$grandretur04+$row->retur;
                break;
              case '5' :
                $grandomset05=$grandomset05+$row->omset;
                $grandretur05=$grandretur05+$row->retur;
                break;
              case '6' :
                $grandomset06=$grandomset06+$row->omset;
                $grandretur06=$grandretur06+$row->retur;
                break;
              case '7' :
                $grandomset07=$grandomset07+$row->omset;
                $grandretur07=$grandretur07+$row->retur;
                break;
              case '8' :
                $grandomset08=$grandomset08+$row->omset;
                $grandretur08=$grandretur08+$row->retur;
                break;
              case '9' :
                $grandomset09=$grandomset09+$row->omset;
                $grandretur09=$grandretur09+$row->retur;
                break;
              case '10' :
                $grandomset10=$grandomset10+$row->omset;
                $grandretur10=$grandretur10+$row->retur;
                break;
              case '11' :
                $grandomset11=$grandomset11+$row->omset;
                $grandretur11=$grandretur11+$row->retur;
                break;
              case '12' :
                $grandomset12=$grandomset12+$row->omset;
                $grandretur12=$grandretur12+$row->retur;
                break;
              }
              $blakhir=$bl;
              break;
            }else{
              echo '<td align=right>0</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0%</td>';
            }
            $bl++;
            if($bl==13)$bl=1;
          }
        }
      }else{
        $bl=$blasal;
        if($row->bln==$bl){
          $akhir=($blasal+$interval)-1;
#          echo 'akhir='.$akhir.'<br>';
          if($blakhir!=$akhir){
            while ($blakhir<$akhir){
              echo '<td align=right>0</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0%</td>';
              $blakhir++;
            }
          }
          echo '</tr><tr><td>'.$row->kode.'</td>';
          echo '<td align=right>'.number_format($row->omset).'</td>';
          echo '<td align=right>'.number_format($row->retur).'</td>';
          echo '<td align=right>'.number_format($persen,2).'%</td>';
          switch ($row->bln){
          case '1' :
            $grandomset01=$grandomset01+$row->omset;
            $grandretur01=$grandretur01+$row->retur;
            break;
          case '2' :
            $grandomset02=$grandomset02+$row->omset;
            $grandretur02=$grandretur02+$row->retur;
            break;
          case '3' :
            $grandomset03=$grandomset03+$row->omset;
            $grandretur03=$grandretur03+$row->retur;
            break;
          case '4' :
            $grandomset04=$grandomset04+$row->omset;
            $grandretur04=$grandretur04+$row->retur;
            break;
          case '5' :
            $grandomset05=$grandomset05+$row->omset;
            $grandretur05=$grandretur05+$row->retur;
            break;
          case '6' :
            $grandomset06=$grandomset06+$row->omset;
            $grandretur06=$grandretur06+$row->retur;
            break;
          case '7' :
            $grandomset07=$grandomset07+$row->omset;
            $grandretur07=$grandretur07+$row->retur;
            break;
          case '8' :
            $grandomset08=$grandomset08+$row->omset;
            $grandretur08=$grandretur08+$row->retur;
            break;
          case '9' :
            $grandomset09=$grandomset09+$row->omset;
            $grandretur09=$grandretur09+$row->retur;
            break;
          case '10' :
            $grandomset10=$grandomset10+$row->omset;
            $grandretur10=$grandretur10+$row->retur;
            break;
          case '11' :
            $grandomset11=$grandomset11+$row->omset;
            $grandretur11=$grandretur11+$row->retur;
            break;
          case '12' :
            $grandomset12=$grandomset12+$row->omset;
            $grandretur12=$grandretur12+$row->retur;
            break;
          }
          $blakhir=$bl;
        }else{
          for($i=1;$i<=$interval;$i++){
            if($row->bln==$bl){
              echo '<td align=right>'.number_format($row->omset).'</td>';
              echo '<td align=right>'.number_format($row->retur).'</td>';
              echo '<td align=right>'.number_format($persen,2).'%</td>';
              switch ($row->bln){
              case '1' :
                $grandomset01=$grandomset01+$row->omset;
                $grandretur01=$grandretur01+$row->retur;
                break;
              case '2' :
                $grandomset02=$grandomset02+$row->omset;
                $grandretur02=$grandretur02+$row->retur;
                break;
              case '3' :
                $grandomset03=$grandomset03+$row->omset;
                $grandretur03=$grandretur03+$row->retur;
                break;
              case '4' :
                $grandomset04=$grandomset04+$row->omset;
                $grandretur04=$grandretur04+$row->retur;
                break;
              case '5' :
                $grandomset05=$grandomset05+$row->omset;
                $grandretur05=$grandretur05+$row->retur;
                break;
              case '6' :
                $grandomset06=$grandomset06+$row->omset;
                $grandretur06=$grandretur06+$row->retur;
                break;
              case '7' :
                $grandomset07=$grandomset07+$row->omset;
                $grandretur07=$grandretur07+$row->retur;
                break;
              case '8' :
                $grandomset08=$grandomset08+$row->omset;
                $grandretur08=$grandretur08+$row->retur;
                break;
              case '9' :
                $grandomset09=$grandomset09+$row->omset;
                $grandretur09=$grandretur09+$row->retur;
                break;
              case '10' :
                $grandomset10=$grandomset10+$row->omset;
                $grandretur10=$grandretur10+$row->retur;
                break;
              case '11' :
                $grandomset11=$grandomset11+$row->omset;
                $grandretur11=$grandretur11+$row->retur;
                break;
              case '12' :
                $grandomset12=$grandomset12+$row->omset;
                $grandretur12=$grandretur12+$row->retur;
                break;
              }
              $blakhir=$bl;
              break;
            }elseif($bl==$blasal){
              $akhir=($blasal+$interval)-1;
              if($blakhir!=$akhir){
                while ($blakhir<=$akhir){
                  echo '<td align=right>0</td>';
                  echo '<td align=right>0</td>';
                  echo '<td align=right>0%</td>';
                  $blakhir++;
                }
              }
              echo '</tr><tr><td>'.$row->kode.'</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0%</td>';
            }else{
              echo '<td align=right>0</td>';
              echo '<td align=right>0</td>';
              echo '<td align=right>0%</td>';
            }
            $bl++;
            if($bl==13)$bl=1;
          }
        }
      }
      $area=$row->kode;
      $bl++;
      if($bl>($interval+$blasal))$bl=1;
    }
    echo '</tr>';
    echo '<tr><td><b>Total</td>';
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch($bl){
      case '1':
        if($grandomset01>0){
          $persen=($grandretur01*100)/$grandomset01;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset01).'</td>';
        echo '<td align=right><b>'.number_format($grandretur01).'</td>';
        echo '<td align=right<b>>'.number_format($persen,2).'%</td>';
        break;
      case '2':
        if($grandomset02>0){
          $persen=($grandretur02*100)/$grandomset02;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset02).'</td>';
        echo '<td align=right><b>'.number_format($grandretur02).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '3':
        if($grandomset03>0){
          $persen=($grandretur03*100)/$grandomset03;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset03).'</td>';
        echo '<td align=right><b>'.number_format($grandretur03).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '4':
        if($grandomset04>0){
          $persen=($grandretur04*100)/$grandomset04;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset04).'</td>';
        echo '<td align=right><b>'.number_format($grandretur04).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '5':  
        if($grandomset05>0){
          $persen=($grandretur05*100)/$grandomset05;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset05).'</td>';
        echo '<td align=right><b>'.number_format($grandretur05).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '6':
        if($grandomset06>0){
          $persen=($grandretur06*100)/$grandomset06;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset06).'</td>';
        echo '<td align=right><b>'.number_format($grandretur06).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '7':
        if($grandomset07>0){
          $persen=($grandretur07*100)/$grandomset07;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset07).'</td>';
        echo '<td align=right><b>'.number_format($grandretur07).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '8':
        if($grandomset08>0){
          $persen=($grandretur08*100)/$grandomset08;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset08).'</td>';
        echo '<td align=right><b>'.number_format($grandretur08).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '9':
        if($grandomset09>0){
          $persen=($grandretur09*100)/$grandomset09;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset09).'</td>';
        echo '<td align=right><b>'.number_format($grandretur09).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '10':
        if($grandomset10>0){
          $persen=($grandretur10*100)/$grandomset10;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset10).'</td>';
        echo '<td align=right><b>'.number_format($grandretur10).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '11':
        if($grandomset11>0){
          $persen=($grandretur11*100)/$grandomset11;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset11).'</td>';
        echo '<td align=right><b>'.number_format($grandretur11).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      case '12':  
        if($grandomset12>0){
          $persen=($grandretur12*100)/$grandomset12;
        }else{
          $persen=0;
        }
        echo '<td align=right><b>'.number_format($grandomset12).'</td>';
        echo '<td align=right><b>'.number_format($grandretur12).'</td>';
        echo '<td align=right><b>'.number_format($persen,2).'%</td>';
        break;
      }
      $bl++;
    }
  }
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listreturvsomsetnasional/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
