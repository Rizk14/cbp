<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listsjpnotreceive/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Tgl SJ</td>
		<?php if($isi->d_sjp){
			  if($isi->d_sjp!=''){
				  $tmp=explode("-",$isi->d_sjp);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_sjp=$hr."-".$bl."-".$th;
			  }
		   }
       if($isi->d_sjp_receive){
			  if($isi->d_sjp_receive!=''){
				  $tmp=explode("-",$isi->d_sjp_receive);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_sjp_receive=$hr."-".$bl."-".$th;
			  }
		   }
		?>
		<td><input readonly id="dsj" name="dsj" value="<?php if($isi->d_sjp) echo $isi->d_sjp; ?>">
		    <input readonly id="isj" name="isj" value="<?php if($isi->i_sjp) echo $isi->i_sjp; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SJ Lama</td>
		<td><input readonly id="isjold" name="isjold" type="text" value="<?php echo $isi->i_sjp_old; ?>"></td>
	      </tr>
        <tr>
		<td width="10%">Tgl Terima</td>
		<td><input readonly id="dreceive" name="dreceive" type="text" value="<?php echo $isi->d_sjp_receive; ?>"
         onclick="showCalendar('',this,this,'','dreceive',0,20,1)"></td>
	      </tr>
        <tr>
		<td width="10%">Nilai Kirim</td>
		<td><input readonly style="text-align:right;" readonly id="vsj" name="vsj" value="<?php echo number_format($isi->v_sjp); ?>">
        <input type="hidden" name="jml" id="jml" value="<?php if($jmlitem) echo $jmlitem; ?>"></td>
	      </tr>
        <tr>
		<td width="10%">Nilai Terima</td>
		<td><input readonly style="text-align:right;" readonly id="vsjrec" name="vsjrec" value="<?php echo number_format($isi->v_sjp_receive); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input <?php echo 'disabled'; ?> name="login" id="login" value="Simpan" type="submit">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("sjpgit/cform/index/","#main")'>
		  </td>
 	    </tr>
	    </table>
			<div id="detailheader" align="center">
				<?php 
					if($detail){
					?>
						<table class="listtable" align="center" style="width: 750px;">
							<th style="width:25px;" align="center">No</th>
							<th style="width:63px;" align="center">Kode</th>
							<th style="width:300px;" align="center">Nama Barang</th>
							<th style="width:100px;" align="center">Ket</th>
							<th style="width:73px;" align="center">Jml Krm</th>
							<th style="width:73px;" align="center">Jml Trm</th>
							<th style="width:10px;" align="center">Selisih</th>
						</table>
					<?php 
					}		
				?>
			</div>
			<div id="detailisi" align="center">
				<?php 
					if($detail){
						$i=0;
						foreach($detail as $row)
						{
              $vtotal=$row->v_unit_price*$row->n_quantity_receive;
              $selisih=$row->n_quantity_receive-$row->n_quantity_deliver;
							echo '<table class="listtable" align="center" style="width:750px;">';
						  	$i++;
						  	echo "<tbody>
								<tr>
		    						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"><input type=\"hidden\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
								<td style=\"width:103px;\"><input style=\"width:103px;\" type=\"text\" id=\"eremark$i\"
								name=\"eremark$i\" value=\"$row->e_remark\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$row->v_unit_price\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_quantity_deliver\"></td>
								<td style=\"width:74px;\"><input style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"nreceive$i\" name=\"nreceive$i\" value=\"$row->n_quantity_receive\"
								onkeyup=\"hitungnilai(".$i.")\"><input type=\"hidden\" id=\"nasal$i\" name=\"nasal$i\" value=\"$row->n_quantity_receive\">
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$vtotal\"></td>
								<td style=\"width:60px;\"><input readonly style=\"text-align:right; width:60px;\" 
								type=\"text\" id=\"selisih$i\" name=\"selisih$i\" value=\"$selisih\"></td>
							</tr>
						  </tbody></table>";
						}
					}		
				?>
			</div>
			<div id="pesan"></div>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function hitungnilai(brs){
    var tot=0;
	  ord=document.getElementById("nreceive"+brs).value;
	  if (isNaN(parseFloat(ord))){
		  alert("Input harus numerik");
	  }else{
		  hrg=formatulang(document.getElementById("vproductmill"+brs).value);
		  qty=formatulang(ord);
		  vhrg=parseFloat(hrg)*parseFloat(qty);
		  document.getElementById("vtotal"+brs).value=formatcemua(vhrg);

      jml=parseFloat(document.getElementById("jml").value);
      for(i=1;i<=jml;i++){
        if(document.getElementById("chk"+i).value=='on'){
          tot+=parseFloat(formatulang(document.getElementById("vtotal"+i).value));
        }
      }
      document.getElementById("vsjrec").value=formatcemua(tot);
	  }
  }
</script>
