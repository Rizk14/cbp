<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'sj/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="sjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">No SJ</td>
		<?php if($isi->d_sj){
			if($isi->d_sj!=''){
				$tmp=explode("-",$isi->d_sj);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				$isi->d_sj=$hr."-".$bl."-".$th;
			}
		   }
		   if($isi->d_spb){
			if($isi->d_spb!=''){
				$tmp=explode("-",$isi->d_spb);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				$isi->d_spb=$hr."-".$bl."-".$th;
			}
		   }
		?>
		<td><input id="isj" name="isj" type="text" value="<?php if($isi->i_sj) echo $isi->i_sj; ?>">
        <input readonly id="dsj" name="dsj" value="<?php if($isi->d_sj) echo $isi->d_sj; ?>" <?php if($isi->i_nota=='') echo "onclick=\"showCalendar('',this,this,'','dsj',0,20,1)\"";?>>
        </td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>" 
			<?php if($isi->i_nota=='') echo "onclick='showModal(\"sj/cform/area/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'";?>>
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>">
		    <input id="istore" name="istore" type="hidden" value="<?php if($istore) echo $istore; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SPB</td>
		<td><input readonly id="ispb" name="ispb" value="<?php if($isi->i_spb) echo $isi->i_spb; ?>" 
        <?php if($isi->i_nota=='') echo "onclick='view_spb(document.getElementById(\"iarea\").value)'";?>>
		    <input readonly id="dspb" name="dspb" type="text" value="<?php if($isi->d_spb) echo $isi->d_spb; ?>">
			  <input id="vsjgross" name="vsjgross" type="hidden" value="<?php if($vsjgross) echo $vsjgross; ?>">
		    <input id="nsjdiscount1" name="nsjdiscount1" type="hidden" value="<?php if($nsjdiscount1) echo $nsjdiscount1; ?>">
		    <input id="nsjdiscount2" name="nsjdiscount2" type="hidden" value="<?php if($nsjdiscount2) echo $nsjdiscount2; ?>">
		    <input id="nsjdiscount3" name="nsjdiscount3" type="hidden" value="<?php if($nsjdiscount3) echo $nsjdiscount3; ?>">
		    <input id="vsjdiscount1" name="vsjdiscount1" type="hidden" value="<?php if($vsjdiscount1) {echo $vsjdiscount1;}else{echo '0';} ?>">
		    <input id="vsjdiscount2" name="vsjdiscount2" type="hidden" value="<?php if($vsjdiscount2) {echo $vsjdiscount2;}else{echo '0';} ?>">
		    <input id="vsjdiscount3" name="vsjdiscount3" type="hidden" value="<?php if($vsjdiscount3) {echo $vsjdiscount3;}else{echo '0';} ?>">
		    <input id="vsjdiscounttotal" name="vsjdiscounttotal" type="hidden" value="<?php if($vsjdiscounttotal) {echo $vsjdiscounttotal;}else{echo '0';} ?>">
        <input id="fspbconsigment" name="fspbconsigment" type="hidden" value="<?php if($fspbconsigment) echo $fspbconsigment; ?>">
        <input id="fplusppn" name="fplusppn" type="hidden" value="<?php if($fplusppn) echo $fplusppn; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php if($icustomer) echo $icustomer; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php if($isalesman) echo $isalesman; ?>">
        <input id="ntop" name="ntop" type="hidden" value="<?php if($ntop) echo $ntop; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SJ Lama</td>
		<td><input id="isjold" name="isjold" type="text" value="<?php echo $isi->i_sj_old; ?>">
        <input style="text-align:right" id="vsjnetto" name="vsjnetto" type="text" value="<?php if($isi->v_nota_netto) echo number_format($isi->v_nota_netto); ?>"></td>
	      </tr>
			<tr>
		<td width="10%">Nama Toko</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php if($ecustomername) echo $ecustomername; ?>"></td>
	    </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input <?php if($isi->i_nota!=null || $isi->i_nota!='') echo 'disabled'; ?> name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listsj/cform/view/<?php echo $dfrom."/".$dto."/".$iareasj."/"; ?>","#main")'>
		  </td>
 	    </tr>
	    </table>
			<div id="detailheader" align="center">
				<?php 
					if($detail){
					?>
						<table class="listtable" align="center" style="width: 750px;">
							<th style="width:25px;" align="center">No</th>
							<th style="width:63px;" align="center">Kode</th>
							<th style="width:300px;" align="center">Nama Barang</th>
							<th style="width:100px;" align="center">Motif</th>
							<th style="width:73px;" align="center">Jml Ord</th>
							<th style="width:73px;" align="center">Jml Krm</th>
							<th style="width:32px;" align="center" class="action">Action</th>
						</table>
					<?php 
					}		
				?>
			</div>
			<div id="detailisi" align="center">
				<?php 
					if($detail){
						$i=0;
						foreach($detail as $row)
						{
						  $query=$this->db->query(" select f_spb_stockdaerah from tm_spb
																				where i_spb='$ispb' and i_area='$iarea'",false);
							if ($query->num_rows() > 0){
								foreach($query->result() as $qq){
									$stockdaerah=$qq->f_spb_stockdaerah;
								}
							}
							if($stockdaerah=='f'){
								$query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															where i_product='$row->i_product'
															and i_product_motif='00'
															and i_product_grade='$row->i_product_grade'
															and i_store='AA' and i_store_location='01' and i_store_locationbin='00'",false);
							}else{
								$query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															where i_product='$row->i_product'
															and i_product_motif='00'
															and i_product_grade='$row->i_product_grade'
															and i_store='$istore' and i_store_location='00' and i_store_locationbin='00'",false);
							}
              
							if ($query->num_rows() > 0){
								foreach($query->result() as $tt){
									$stock=$tt->qty;
								}
							}else{
								$stock=0;
							}
							if($stock>$row->n_qty)$stock=$row->n_qty;
							if($stock<0)$stock=0;
              $vtot=$row->harga*$stock;
#              if($stock>$row->n_deliver) $stock=$row->n_deliver;
							$stock=number_format($stock);

							echo '<table class="listtable" align="center" style="width:750px;">';
						  	$i++;
						  	echo "<tbody>
								<tr>
		    						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:103px;\"><input readonly style=\"width:103px;\" type=\"text\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$row->v_unit_price\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_qty\"></td>
								<td style=\"width:74px;\"><input style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_deliver\"
								onblur=\"hitungnilai(); pembandingnilai(".$i.");\" onkeyup=\"hitungnilai(); pembandingnilai(".$i.");\"
                onpaste=\"hitungnilai(); pembandingnilai(".$i.");\" autocomplete=\"off\">
                <input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_deliver\">
                <input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$stock\">
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>
								<td style=\"width:60px;\" align=\"center\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='on' checked onclick='pilihan(this.value,".$i.")'</td>
							</tr></tbody></table>";
						}
            $cquery=$this->db->query("  select a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b, tm_spb c
                                        where a.i_spb = '$isi->i_spb' and a.i_area='$isi->i_area' 
                                        and a.i_product not in (select i_product from tm_nota_item 
                                        where i_sj='$isi->i_sj' and i_area='$isi->i_area')
                                        and a.i_spb=c.i_spb and a.i_area=c.i_area
                                        and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                                        order by a.i_product", false);
            if ($cquery->num_rows() > 0){
              foreach($cquery->result() as $tmp){
							  echo '<table class="listtable" align="center" style="width:750px;">';
						  	$i++;
                $jmlitem++;
						    $query=$this->db->query(" select f_spb_stockdaerah from tm_spb
																				  where i_spb='$ispb' and i_area='$iarea'",false);
							  if ($query->num_rows() > 0){
								  foreach($query->result() as $qq){
									  $stockdaerah=$qq->f_spb_stockdaerah;
								  }
							  }
							  if($stockdaerah=='f'){
								  $query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															  where i_product='$tmp->i_product'
															  and i_product_motif='00'
															  and i_product_grade='$tmp->i_product_grade'
															  and i_store='AA' and i_store_location='01' and i_store_locationbin='00'",false);
							  }else{
								  $query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															  where i_product='$tmp->i_product'
															  and i_product_motif='00'
															  and i_product_grade='$tmp->i_product_grade'
															  and i_store='$istore' and i_store_location='00' and i_store_locationbin='00'",false);
							  }
							  if ($query->num_rows() > 0){
								  foreach($query->result() as $tt){
									  $stock=$tt->qty+$row->n_deliver;
								  }
							  }else{
								  $stock=0;
							  }
#							  if($stock>$row->n_qty)$stock=$row->n_qty;
							  if($stock<0)$stock=0;
                $vtot=$row->harga*$stock;
#                if($stock>$row->n_deliver) $stock=$row->n_deliver;
							  $stock=number_format($stock);

						  	echo "<tbody>
						          <tr>
            						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
							          id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
							          id=\"motif$i\" name=\"motif$i\" value=\"$tmp->i_product_motif\"></td>
							          <td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
							          id=\"iproduct$i\" name=\"iproduct$i\" value=\"$tmp->i_product\"></td>
							          <td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
							          id=\"eproductname$i\"
							          name=\"eproductname$i\" value=\"$tmp->e_product_name\"></td>
							          <td style=\"width:103px;\"><input readonly style=\"width:103px;\" type=\"text\" id=\"emotifname$i\"
							          name=\"emotifname$i\" value=\"$tmp->e_product_motifname\">
							          <input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$tmp->v_unit_price\"></td>
							          <td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
							          type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$tmp->n_order\"></td>
							          <td style=\"width:74px;\"><input style=\"text-align:right; width:74px;\" 
							          type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$tmp->n_deliver\"
							          onblur=\"hitungnilai(); pembandingnilai(".$i.");\" onkeyup=\"hitungnilai(); pembandingnilai(".$i.");\"
                        onpaste=\"hitungnilai(); pembandingnilai(".$i.");\" autocomplete=\"off\">
                        <input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$tmp->n_deliver\">
                        <input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$stock\">
							          <input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>
							          <td style=\"width:60px;\" align=\"center\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'</td>
						          </tr>
                      </tbody></table>";
              }
            }
					}		
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php if($jmlitem) echo $jmlitem; ?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("iarea").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function diskonrupiah(isi){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot   =parseFloat(formatulang(document.getElementById("vspb").value));
		vtotdis=parseFloat(formatulang(isi));
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
  function tambih_item()
  {
    so_inner=='';
    so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
    so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
    so_inner+= '<th style="width:63px;" align="center">Kode</th>';
    so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
    so_inner+= '<th style="width:100px;" align="center">Motif</th>';
    so_inner+= '<th style="width:46px;"  align="center">Jml</th>';
    so_inner+= '<th style="width:100px;"  align="center">Keterangan</th>';
    so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
    document.getElementById("detailheader").innerHTML=so_inner;
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:69px;"><input style="width:69px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';


		si_inner+='<td style="width:105px;"><input style="width:105px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:54px;">&nbsp;</td></tr></tbody>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:69px;"><input style="width:69px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+'); pembandingnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';
		si_inner+='<td style="width:105px;"><input style="width:105px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:54px;">&nbsp;</td></tr></tbody>';
    }
    b=document.getElementById("ispb").value
    showModal("sj/cform/product/"+a+"/"+b+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  function pilihan(a,b){
	  if(a==''){
		  document.getElementById("chk"+b).value='on';
	  }else{
		  document.getElementById("chk"+b).value='';
		  document.getElementById("ndeliver"+b).value='0';
	  }
	  hitungnilai();
  }
  function view_spb(a)
  {
	showModal("sj/cform/spb/"+a+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function hitungnilai(){
    jml=document.getElementById("jml").value;
    if (jml<=0){
    }else{
      salah=false;
      gud=document.getElementById("istore").value;
      kons=document.getElementById("fspbconsigment").value;
      if(gud!='AA' && gud!='PB' && kons!='t'){
        for(i=1;i<=jml;i++){
          stock  =formatulang(document.getElementById("ndeliverhidden"+i).value);
          deliver=formatulang(document.getElementById("ndeliver"+i).value);
          if(parseFloat(stock)<0)stock=0;
          if(parseFloat(deliver)>parseFloat(stock)){
            alert('Jumlah Kirim melebihi jumlah stock');
            document.getElementById("ndeliver"+i).value=0;
            salah=true;
            break;
          }
        }
      }
      if(!salah){
        gros=0;
        grosgr=0;
        for(i=1;i<=jml;i++){
          if(document.getElementById("chk"+i).value=='on'){
            if(document.getElementById("fplusppn").value=='f'){
  	          hrg=parseFloat(formatulang(document.getElementById("vproductmill"+i).value))/1.1;
            }else{
  	          hrg=formatulang(document.getElementById("vproductmill"+i).value);
            }
 	          hrggr=formatulang(document.getElementById("vproductmill"+i).value);
	          qty=formatulang(document.getElementById("ndeliver"+i).value);
	          vhrg=parseFloat(hrg)*parseFloat(qty);
	          vhrggr=parseFloat(hrggr)*parseFloat(qty);
            gros=gros+vhrg;
            grosgr=grosgr+vhrggr;
	          document.getElementById("vtotal"+i).value=formatcemua(vhrg);
          }
        }
//        document.getElementById("vsjgross").value=formatcemua(gros);
//        alert(grosgr);
        document.getElementById("vsjgross").value=formatcemua(grosgr);
        nsjdisc1=parseFloat(formatulang(document.getElementById("nsjdiscount1").value));
        nsjdisc2=parseFloat(formatulang(document.getElementById("nsjdiscount2").value));
        nsjdisc3=parseFloat(formatulang(document.getElementById("nsjdiscount3").value));
        if(nsjdisc1==0)
          vsjdisc1=parseFloat(formatulang(document.getElementById("vsjdiscount1").value));
        else
          vsjdisc1=0;
        vsjdisc2=0;
        vsjdisc3=0;
        vtot =0;
        if(gros>0){
//          if(nsjdisc1>0) vsjdisc1=vsjdisc1+((gros*nsjdisc1)/100);
//	        vsjdisc2=vsjdisc2+(((gros-vsjdisc1)*nsjdisc2)/100);
//	        vsjdisc3=vsjdisc3+(((gros-(vsjdisc1+vsjdisc2))*nsjdisc3)/100);
          if(nsjdisc1>0) vsjdisc1=vsjdisc1+((grosgr*nsjdisc1)/100);
	        vsjdisc2=vsjdisc2+(((grosgr-vsjdisc1)*nsjdisc2)/100);
	        vsjdisc3=vsjdisc3+(((grosgr-(vsjdisc1+vsjdisc2))*nsjdisc3)/100);
	        document.getElementById("vsjdiscount1").value=formatcemua(vsjdisc1);
	        document.getElementById("vsjdiscount2").value=formatcemua(vsjdisc2);
	        document.getElementById("vsjdiscount3").value=formatcemua(vsjdisc3);
	        vdis1=parseFloat(vsjdisc1);
	        vdis2=parseFloat(vsjdisc2);
	        vdis3=parseFloat(vsjdisc3);
//          vtotdis=vdis1+vdis2+vdis3;

          nTDisc1  = nsjdisc1  + nsjdisc2  * (100-nsjdisc1)/100;
          nTDisc2  = nsjdisc3  * (100-nsjdisc3)/100;
          nTDisc   = nTDisc1 + nTDisc2 * (100-nTDisc1)/100;
//          vtotdis = nTDisc * gros / 100;
          vtotdis = nTDisc * grosgr / 100;

          if(document.getElementById("fplusppn").value=='t'){
          	vtotdis=Math.round(vtotdis);
          }
          if(document.getElementById("fspbconsigment").value=='f'){
	          document.getElementById("vsjdiscounttotal").value=formatcemua(Math.round(vtotdis));
//            if(document.getElementById("fplusppn").value=='f'){
//              vppn=(Math.round(gros)-Math.round(vtotdis))*0.1;
//              vtotbersih=Math.round(gros)-Math.round(vtotdis)+Math.round(vppn);
//            }else{	
//              vtotbersih=parseFloat(gros)-parseFloat(vtotdis);
//            }
            vtotbersih=parseFloat(grosgr)-parseFloat(vtotdis);
	          document.getElementById("vsjnetto").value=formatcemua(Math.round(vtotbersih));
          }else if(document.getElementById("fplusppn").value=='t'){
	          vtotbersih=parseFloat(gros)-parseFloat(formatulang(document.getElementById("vsjdiscounttotal").value));
	          document.getElementById("vsjnetto").value=formatcemua(Math.round(vtotbersih));
          }else{
            vtotbersih=parseFloat(grosgr)-parseFloat(vtotdis);
	          document.getElementById("vsjnetto").value=formatcemua(Math.round(vtotbersih));
          }
        }else{
          document.getElementById("vsjdiscount1").value=formatcemua(vsjdisc1);
	        document.getElementById("vsjdiscount2").value=formatcemua(vsjdisc2);
	        document.getElementById("vsjdiscount3").value=formatcemua(vsjdisc3);
          document.getElementById("vsjnetto").value=0;
          if(document.getElementById("fspbconsigment").value=='f'){
          	document.getElementById("vsjdiscounttotal").value=0;
          }
        }
      }
    }
  }
  function pembandingnilai(a) 
  {
	  var n_qty	= document.getElementById('norder'+a).value;
	  var n_deliver	= document.getElementById('ndeliver'+a).value;
	  var deliverasal	= document.getElementById('ndeliverhidden'+a).value;
	  if(parseInt(n_deliver) > parseInt(n_qty)) {
		  alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Order ( '+n_qty+' item )');
		  document.getElementById('ndeliver'+a).value	= deliverasal;
		  document.getElementById('ndeliver'+a).focus();
		  return false;
	  }else if(parseInt(n_deliver) > parseInt(deliverasal)) {
      i_store = document.getElementById('istore').value;
      kons=document.getElementById("fspbconsigment").value;
	    if(i_store!='AA' && istore!='PB' && kons!='t') {
		    alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Stock ( '+deliverasal+' item )');
		    document.getElementById('ndeliver'+a).value	= deliverasal;
		    document.getElementById('ndeliver'+a).focus();
		    return false;

      }
    }
  }
  $(document).ready(function () {
//    hitungnilai();
  });
/*
  function afterSetDateValue(ref_field, target_field, date) {
    if (date!="") {
      var startDate=document.getElementById('dsj').value;
      var endDate=document.getElementById('dakhir').value;
      if(endDate!=''){
        tes=startDate.split('-');
        startDate=tes[2]+'-'+tes[1]+'-'+tes[0];
        if(startDate<endDate){
          alert("Tanggal SJ Terakhir = "+endDate+" !!!");
          tes=endDate.split('-');
          document.getElementById('dsj').value=tes[2]+'-'+tes[1]+'-'+tes[0];
        }
      }
    }
  }
*/
  function afterSetDateValue(ref_field, target_field, date) {
    if (date!="") {
      var startDate=document.getElementById('dsj').value;
      var asalDate =document.getElementById('dsjx').value;
/*
      var endDate  =document.getElementById('dakhir').value;
      if(endDate!=''){
        if(startDate<endDate){
          tes=endDate.split('-');
          dsj=tes[2]+'-'+tes[1]+'-'+tes[0];
          alert("Tanggal SJ Terakhir = "+dsj+" !!!");
          document.getElementById('dsj').value=dsj;
        }
      }
*/
      tes=asalDate.split('-');
      asalDate=tes[2]+'-'+tes[1]+'-'+tes[0];
      currentx=tes[0]+'-'+tes[1]+'-'+tes[2];
      tesi=startDate.split('-');
      startDate=tesi[2]+'-'+tesi[1]+'-'+tesi[0];
      if(startDate<asalDate){
          alert("Tanggal SJ tidak boleh lebih kecil dari ( "+currentx+" )");
          document.getElementById('dsj').value=currentx;
      }
    }
  }
</script>
