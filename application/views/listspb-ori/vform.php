<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
	$tujuan = 'listspb/cform/view2';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="12" align="center">
			Cari data : 
			<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" >
			&nbsp; <input type="hidden" name="dfrom" value= "<?php echo $dfrom; ?>" >
			<input type="hidden" name="dto" value= "<?php echo $dto; ?>" >
			<input type="hidden" name="iarea" value= "<?php echo $iarea; ?>" >
			<input type="hidden" name="is_cari" value= "1" >
			<input type="submit" id="bcari" name="bcari" value="Cari">
		</td>
	      </tr>
	    </thead>
	   	<th>No SPB</th>
			<th>Tgl SPB</th>
			<th>Sls</th>
			<th>Lang</th>
			<th>Area</th>
			<th>Spb Lm</th>
			<th>Kotor</th>
			<th>Disc</th>
			<th>Bersih</th>
			<th>Status</th>
			<th>Daerah</th>
			<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->f_spb_stockdaerah=='t')
        {
          $daerah='Ya';
        }else{
          $daerah='Tidak';
        }
        if($row->d_spb){
			    $tmp=explode('-',$row->d_spb);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_spb=$tgl.'-'.$bln.'-'.$thn;
        }
			  if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			  	$status='Batal';
			  }elseif(
				 	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					 ){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					  ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					  ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ar)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null)
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					 ){
			  	$status='OP Close';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap SJ (sales)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
#			  	$status='Siap SJ (gudang)';
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
        }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) 
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }
			  $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
			  $row->v_spb	= number_format($row->v_spb);
 			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);
			  echo "<tr> 
				  <td valign=top>$row->i_spb</td>
				  <td valign=top>$row->d_spb</td>
				  <td valign=top>$row->i_salesman</td>";
				if(substr($row->i_customer,2,3)!='000'){
					echo "
				  <td valign=top>($row->i_customer) $row->e_customer_name</td>";
				}else{
					echo "
				  <td valign=top>$row->xname</td>";
				}
			  echo "
				  <td valign=top>$row->i_area</td>
				  <td valign=top>".strtoupper($row->i_spb_old)."</td>
				  <td valign=top align=right>$row->v_spb</td>
				  <td valign=top align=right>$row->v_spb_discounttotal</td>
				  <td valign=top align=right>$bersih</td>
				  <td valign=top>$status</td>
				  <td valign=top>$daerah</td>
				  <td valign=top class=\"action\">";
			  if($row->i_spb_program!=null){
					if($row->i_product_group=='00'){
						echo "	
							<a href=\"#\" onclick='show(\"spbpromoreguler/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}else{
						echo "	
							<a href=\"#\" onclick='show(\"spbpromobaby/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				}else{
					if($row->xname!=''){
						echo "
								<a href=\"#\" onclick='show(\"customernew/cform/edit/$row->i_spb/$row->i_area/$row->i_price_group/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}else{
						if($row->i_product_group=='00'){
						echo "
								<a href=\"#\" onclick='show(\"spbreguler/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}else{
						echo "	
								<a href=\"#\" onclick='show(\"spbbaby/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}
					}
				}
			  if( ($row->i_store == null) && ($row->i_approve1==null) && ($row->i_approve2==null) )
			  {
					if($row->f_spb_cancel == 'f')
						{		echo "&nbsp;&nbsp;
						<a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
						}
				  }
				  echo "</td></tr>";				  
				}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
