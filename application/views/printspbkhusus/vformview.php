<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<!--<?php echo form_open('printspb/cform/cari', array('id' => 'listform'));?>-->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printspbkhusus/cform/view','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >
        &nbsp; <input type="hidden" name="dfrom" value= "<?php echo $dfrom; ?>" >
			<input type="hidden" name="dto" value= "<?php echo $dto; ?>" >
			<input type="hidden" name="iarea" value= "<?php echo $iarea; ?>" ><input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
	   	<th>No SPB</th>
		<th>Tanggal</th>
		<th>Customer</th>
		<th>Area</th>
		<th>Status</th>
		<th>Approve</th>
    <th>Print</th>
		<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			  	$status='Batal';
			  }elseif(
				 	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					 ){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					  ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					  ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ar)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null)
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					 ){
			  	$status='OP Close';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap SJ (sales)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
#			  	$status='Siap SJ (gudang)';
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
        }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) 
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }

/*
			  if(
				 ($row->i_approve1 == null) && ($row->i_approve2 == null) && 
			  	 ($row->i_store == null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				 ($row->f_spb_siapnotagudang == 'f') && ($row->i_notapprove == null)
				){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store == null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store == null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store == null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ku)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null) && ($row->i_nota == null) && 
					  ($row->f_spb_siapnotagudang == 'f')
					 ){
			  	$status='Gudang';
				}elseif(
					  ($row->i_approve1 == null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store == null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 't') && 
					  ($row->f_spb_siapnotagudang == 'f')
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_siapnotagudang == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap Nota (gudang)';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 't') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't')
					 ){
			  	$status='Siap Nota (sales)';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_approve2 == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 't') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't')
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) && 
					  ($row->f_spb_siapnotagudang == 't') &&
					  ($row->f_spb_siapnotasales == 't')
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }
*/
        if($row->d_spb){
          $tmp=explode('-',$row->d_spb);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_spb=$tgl.'-'.$bln.'-'.$thn;
        }
			  echo "<tr> 
				  <td>$row->i_spb</td>
				  <td>$row->d_spb</td>
				  <td>$row->e_customer_name</td>
				  <td>$row->i_area</td>";
			  echo "
				  <td>$status</td>";
				if($row->i_approve1 != null){
				  echo "<td>Ya</td>";
			  }else{
				  echo "<td>Tidak</td>";
			  }
       if($row->n_print==0) {
          echo "<td align='right'>Belum</td>"; }
        else {
          echo "<td align='right'>Sudah</td>"; }
				echo "<td class=\"action\">";
				//if($this->session->userdata('i_area')=='00'){
				  //if($row->f_spb_stockdaerah=='t'){
				    //if($row->i_approve1!=null){
					    echo "<a href=\"javascript:yyy('".$row->i_spb."','".$row->i_area."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/print.gif\" border=\"0\" alt=\"cetak\"></a>&nbsp;&nbsp;&nbsp;</td>";
			//       }if($row->i_customer_status=='4'){
			// 		    echo "<a href=\"javascript:yyy('".$row->i_spb."','".$row->i_area."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/print.gif\" border=\"0\" alt=\"cetak\"></a>&nbsp;&nbsp;&nbsp;</td>";
			//       }
			// 	  }else{
			// 		  echo "<a href=\"javascript:yyy('".$row->i_spb."','".$row->i_area."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/print.gif\" border=\"0\" alt=\"cetak\"></a>&nbsp;&nbsp;&nbsp;</td>";
			//     }
			// 	}else{
			// 		echo "<a href=\"javascript:yyy('".$row->i_spb."','".$row->i_area."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/print.gif\" border=\"0\" alt=\"cetak\"></a>&nbsp;&nbsp;&nbsp;</td>";
			//   }
			}
		echo "<input type=\"hidden\" id=\"ispbprint\" name=\"ispbprint\" value=\"\">
		     ";
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
	</div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(b,c){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/printspbkhusus/cform/cetak/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
