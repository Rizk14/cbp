<?php
echo "<h2>$page_title</h2>";
?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'supplier/cform/update', 'update' => '#main', 'type' => 'post')); ?>
			<div id="mastersupplierform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="16%">Kode Supplier</td>
								<td width="1%">:</td>
								<td width="33%">
									<input readonly type="text" name="isupplier" id="isupplier" value="<?php echo $isi->i_supplier; ?>" maxlength='5'>
								</td>
								<td width="16%">Nama</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esuppliername" id="esuppliername" value="<?php echo $isi->e_supplier_name; ?>" maxlength='30' onkeyup="gede(this)">
								</td>
							</tr>
							<tr>
								<td width="16%">Group Supplier</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="hidden" name="isuppliergroup" id="isuppliergroup" value="<?php echo $isi->i_supplier_group; ?>" onclick='showModal("supplier/cform/suppliergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
									<input readonly name="esuppliergroupname" id="esuppliergroupname" value="<?php echo $isi->e_supplier_groupname; ?>" onclick='showModal("supplier/cform/suppliergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
								<td width="16%">Alamat</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplieraddress" id="esupplieraddress" value="<?php echo $isi->e_supplier_address; ?>" onkeyup="gede(this)">
								</td>
							</tr>
							<tr>
								<td width="16%">Kota</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esuppliercity" id="esuppliercity" value="<?php echo $isi->e_supplier_city; ?>" maxlength='20' onkeyup="gede(this)">
								</td>
								<td width="16%">Kode Pos</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierpostalcode" id="esupplierpostalcode" value="<?php echo $isi->e_supplier_postalcode; ?>" maxlength='5'>
								</td>
							</tr>
							<tr>
								<td width="16%">Telepon</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierphone" id="esupplierphone" value="<?php echo $isi->e_supplier_phone; ?>" maxlength='12'>
								</td>
								<td width="16%">Fax</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierfax" id="esupplierfax" value="<?php echo $isi->e_supplier_fax; ?>" maxlength='12'>
								</td>
							</tr>
							<tr>
								<td width="16%">Nama Pemilik</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierownername" id="esupplierownername" value="<?php echo $isi->e_supplier_ownername; ?>" maxlength='30' onkeyup="gede(this)">
								</td>
								<td width="16%">Alamat Pemilik</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierowneraddress" id="esupplierowneraddress" value="<?php echo $isi->e_supplier_owneraddress; ?>" maxlength='100' onkeyup="gede(this)">
								</td>
							</tr>
							<tr>
								<td width="16%">NPWP</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esuppliernpwp" id="esuppliernpwp" value="<?php echo $isi->e_supplier_npwp; ?>" maxlength='20'>
								</td>
								<td width="16%">Telepon 2</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplierphone2" id="esupplierphone2" value="<?php echo $isi->e_supplier_phone2; ?>" maxlength='12'>
								</td>
							</tr>
							<tr>
								<td width="16%">Kontak</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esuppliercontact" id="esuppliercontact" value="<?php echo $isi->e_supplier_contact; ?>" maxlength='30' onkeyup="gede(this)">
								</td>
								<td width="16%">Email</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="esupplieremail" id="esupplieremail" value="<?php echo $isi->e_supplier_email; ?>" maxlength='30'>
								</td>
							</tr>
							<tr>
								<td width="16%">Diskon 1</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="nsupplierdiscount" id="nsupplierdiscount" value="<?php echo $isi->n_supplier_discount; ?>" maxlength='6'>
								</td>
								<td width="16%">Diskon 2</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="nsupplierdiscount2" id="nsupplierdiscount2" value="<?php echo $isi->n_supplier_discount2; ?>" maxlength='6'>
								</td>
							</tr>
							<tr>
								<td width="16%">TOP</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="nsuppliertoplength" id="nsuppliertoplength" value="<?php echo $isi->n_supplier_toplength; ?>" maxlength='3'>
								</td>

								<td width="16%">Sistem Pembayaran</td>
								<td width="1%">:</td>
								<td width="33%">
									<select name="paymenttype" id="paymenttype" value="">
										<option value="" <?php echo $isi->payment_type == "" ? "selected" : "" ?>></option>
										<option value="Tunai" <?php echo $isi->payment_type == "Tunai" ? "selected" : "" ?>>Tunai</option>
										<option value="Transfer" <?php echo $isi->payment_type == "Transfer" ? "selected" : "" ?>>Transfer</option>
										<option value="Cek/Giro" <?php echo $isi->payment_type == "Cek/Giro" ? "selected" : "" ?>>Cek/Giro</option>
									</select>
								</td>
							</tr>

							<tr>
								<td width="16%">Bank</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="bankname" id="bankname" value="<?= $isi->bank_name; ?>">
								</td>

								<td width="16%">Nomor Rekening</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="text" name="accountnumber" id="accountnumber" value="<?= $isi->account_number; ?>" maxlength="16">
								</td>
							</tr>
							<tr>
								<td width="16%">Nama Rekening</td>
								<td width="1%">:</td>
								<td colspan="4" width="83%">
									<input type="text" name="accountname" id="accountname" value="<?= $isi->account_name; ?>">
								</td>
							</tr>
							<tr>
								<td width="16%">PKP</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="checkbox" name="fsupplierpkp" id="fsupplierpkp" value="on" <?php if ($isi->f_supplier_pkp == 't') echo 'checked'; ?>>
								</td>
								<td width="16%">PPN</td>
								<td width="1%">:</td>
								<td width="33%">
									<input type="checkbox" name="fsupplierppn" id="fsupplierppn" value="on" <?php if ($isi->f_supplier_ppn == 't') echo 'checked'; ?>>
								</td>
							</tr>
							<tr>
								<td width="16%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td colspan=4 width="83%">
									<?php if (
										($this->session->userdata('level') == "4" && $this->session->userdata('departement') == "8") /* FIS SPV */
										|| ($this->session->userdata('level') == "5" && $this->session->userdata('departement') == "8") /* FIS MAN */
										|| ($this->session->userdata('level') == "0" && $this->session->userdata('departement') == "0") /* MIS */
										|| ($this->session->userdata('level') == "5" && $this->session->userdata('departement') == "4")/* USER */
									) {
										echo "<input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"return valid()\">";
									} ?>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("supplier/cform/","#tmpx")'>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<div id="pesan"></div>

<script>
	function valid() {
		var kode = $('#isupplier').val();
		var nama = $('#esuppliername').val();
		var alamat = $('#esupplieraddress').val();
		var email = $('#esupplieremail').val();
		var kon = window.confirm('Apakah yakin?');

		if (kon) {
			if (kode == '') {
				alert("Kode harus diisi..!");
				$('#isupplier').focus();
				return false;
			}

			if (nama == '') {
				alert("Nama supplier harus diisi..!");
				$('#esuppliername').focus();
				return false;
			}
			if (alamat == '') {
				alert("Alamat harus diisi..!");
				$('#esupplieraddress').focus();
				return false;
			}

			var atps = email.indexOf("@");
			var dots = email.lastIndexOf(".");

			if (email == '') {
				return true;
			} else if (atps < 1 || dots < atps + 2 || dots + 2 >= email.length) {
				alert("Alamat email tidak valid.");
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
</script>