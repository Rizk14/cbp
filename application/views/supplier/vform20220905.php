<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Supplier</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Supplier</a></li>  
	  </ul>  
    <div id="content_1" class="content">
		<table class="maintable">
			<tr>
			  <td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'supplier/cform/cari','update'=>'#tmpx','type'=>'post'));?>
					<table class="listtable">
				  <thead>
				  <tr>
				<td colspan="6" align="center">
				  Cari data : 
					<input type="text" id="cari" name="cari" value="" >&nbsp;
					<input type="submit" id="bcari" name="bcari" value="Cari">
				</td>
				  </tr>
				  </thead>
			  	      <th>Kode Supplier</th>
				  <th>Nama</th>
				  <th>Alamat</th>
				  <th>Telepon</th>
				  <th class="action">Action</th>
				  <tbody>
				  <?php 
				if($isi){
				  foreach($isi as $row){
					echo "<tr> 
					  <td>$row->i_supplier</td>
					  <td>".ucfirst(strtolower($row->e_supplier_name))."</td>
					  <td>".ucfirst(strtolower($row->e_supplier_address))."</td>
					  <td>$row->e_supplier_phone</td>
					  <td class=\"action\">
		<a href=\"#\" onclick='show(\"supplier/cform/edit/$row->i_supplier/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
#		&nbsp;&nbsp;";
#		<a href=\"#\" onclick='hapus(\"supplier/cform/delete/$row->i_supplier/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>
           echo "</td></tr>";
					}
				}
				  ?>
				  </tbody>
				</table>
				<?php #echo "<center>".$this->paginationxx->create_links()."</center>";?>
				<?=form_close()?>
			  </td>
			</tr>
		</table>
    </div>
	<div id="content_2" class="content">
    <table class="maintable">
      <tr>
    	<td align="left">
	      <?php echo $this->pquery->form_remote_tag(array('url'=>'supplier/cform/simpan','update'=>'#main','type'=>'post'));?>
	<div id="mastersupplierform">
	<div class="effect">
	  <div class="accordion2">
	      <table class="mastertable">
	      	<tr>
		  <td width="16%">Kode Supplier</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="isupplier" id="isupplier" value="" maxlength='5' onkeyup="gede(this)"></td>
		  <td width="16%">Nama</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esuppliername" id="esuppliername" value="" maxlength='30' onkeyup="gede(this)"></td>
	      	</tr>
	      	<tr>
		  <td width="16%">Group Supplier</td>
		  <td width="1%">:</td>
		  <td width="33%"><input type="hidden" name="isuppliergroup" id="isuppliergroup" value="" 
							   onclick='showModal("supplier/cform/suppliergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
						  <input readonly name="esuppliergroupname" id="esuppliergroupname" value="" 
							   onclick='showModal("supplier/cform/suppliergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="16%">Alamat</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplieraddress" id="esupplieraddress" value="" maxlength='30' onkeyup="gede(this)"></td>
	      	</tr>
	      	<tr>
		  <td width="16%">Kota</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esuppliercity" id="esuppliercity" value="" maxlength='20' onkeyup="gede(this)"></td>
		  <td width="16%">Kode Pos</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierpostalcode" id="esupplierpostalcode" value="" maxlength='5'></td>
	      	</tr>
	      	<tr>
		  <td width="16%">Telepon</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierphone" id="esupplierphone" value="" maxlength='12'></td>
		  <td width="16%">Fax</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierfax" id="esupplierfax" value="" maxlength='12'></td>
	      	</tr>
		<tr>
		  <td width="16%">Nama Pemilik</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierownername" id="esupplierownername" value="" maxlength='30' onkeyup="gede(this)"></td>
		  <td width="16%">Alamat Pemilik</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierowneraddress" id="esupplierowneraddress" value="" maxlength='30' onkeyup="gede(this)"></td>
	      	</tr>
		<tr>
		  <td width="16%">NPWP</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esuppliernpwp" id="esuppliernpwp" value="" maxlength='20'></td>
		  <td width="16%">Telepon 2</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplierphone2" id="esupplierphone2" value="" maxlength='12'></td>
	      	</tr>
		<tr>
		  <td width="16%">Kontak</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esuppliercontact" id="esuppliercontact" value="" maxlength='30' onkeyup="gede(this)"></td>
		  <td width="16%">Email</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="esupplieremail" id="esupplieremail" value="" maxlength='30'></td>
	      	</tr>
		<tr>
		  <td width="16%">Diskon 1</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="nsupplierdiscount" id="nsupplierdiscount" value="" maxlength='6'></td>
		  <td width="16%">Diskon 2</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="text" name="nsupplierdiscount2" id="nsupplierdiscount2" value="" maxlength='6'></td>
	      	</tr>
		<tr>
		  <td width="16%">TOP</td>
		  <td width="1%">:</td>
		  <td colspan="4" width="83%">
		    <input type="text" name="nsuppliertoplength" id="nsuppliertoplength" value="" maxlength='3'></td>
		</tr>
		<tr>
		  <td width="16%">PKP</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="checkbox" name="fsupplierpkp" id="fsupplierpkp" value="on"></td>
		  <td width="16%">PPN</td>
		  <td width="1%">:</td>
		  <td width="33%">
		    <input type="checkbox" name="fsupplierppn" id="fsupplierppn" value="on"</td>
	      	</tr>
	      	<tr>
		  <td width="16%">&nbsp;</td>
		  <td width="1%">&nbsp;</td>
		  <td colspan=4 width="83%">
		    <input name="login" id="login" value="Simpan" type="submit">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("supplier/cform/","#tmpx")'>
		  </td>
	       </tr>
	      </table>
		</div>
	</div>
	</div>
	<?=form_close()?>
        </td>
      </tr> 
    </table>
    </div>
  </div>
</div>
