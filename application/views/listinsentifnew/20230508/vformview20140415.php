<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'hitunginsentif/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
		settype($a,'integer');
		settype($b,'integer');
		if($b==12){
		  $a=$a+1;
		  settype($a,'string');		  
		  settype($b,'string');
		  $b='01';
		}else{
      $b=$b+1;
		  settype($a,'string');		  
		  settype($b,'string');
		  if(strlen($b)==1)$b='0'.$b;
		}
		$bts=$a.'-'.$b.'-01';
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Insentif Salesman</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
?>
    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Salesman</th>
			    <th>Tot Ins</th>
	   	    <th>Tgt Omset</th>
	   	    <th>Omset</th>
	   	    <th>Retur</th>
	   	    <th>Omset Net</th>
	   	    <th>% Tgt Omset</th>
	   	    <th>Ins Oms</th>
	   	    <th>Tgt Tgh</th>
	   	    <th>Real Tgh</th>
	   	    <th>Real Ins</th>
	   	    <th>>90</th>
 	   	    <th>%>90</th>
	   	    <th>% Tgt Col</th>
	   	    <th>Ins Col</th>
	    <tbody>
	      <?php 
		if($omset){
		  $sales='';
		  $namasales='';
			$i=1;
			foreach($omset as $row){
			  if($row->i_salesman!='00'){
          if($sales!='' && $row->i_salesman!=$sales && $row->v_omset_target!=0 && $sales!='  '){
	          echo "<tr>
              <td align=right>$i</td>
              <td>$row->i_salesman - $row->e_salesman_name</td>
			        <td align=right>Rp. ".number_format($row->v_insentif)."</td>
              <td align=right>Rp. ".number_format($row->v_omset_target)."</td>
              <td align=right>RP. ".number_format($row->v_omset_gross)."</td>
			        <td align=right>Rp. ".number_format($row->v_retur)."</td>
			        <td align=right>Rp. ".number_format($row->v_omset_netto)."</td>
              <td align=right>".number_format($row->n_omset)." %</td>
			        <td align=right>Rp. ".number_format($row->v_omset)."</td>
              <td align=right>RP. ".number_format($row->v_tagihan_target)."</td>
			        <td align=right>".number_format($row->v_tagihan_realisasi)." %</td>
			        <td align=right>Rp. ".number_format($row->v_tagihan_realisasiinsentif)."</td>
              <td align=right>RP. ".number_format($row->v_tagihan_realisasi90)."</td>
              <td align=right>".number_format($row->n_tagihan_realisasi90)." %</td>
			        <td align=right>".number_format($row->n_collection)." %</td>
			        <td align=right>Rp. ".number_format($row->v_collection)."</td></tr>";
/*            $quer 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
	          $raw   	= $quer->row();
	          $dproses= $raw->c;*/
            $i++;
            $sales='';
            $namasales='';
          }
/*          $this->db->select("	* from f_target_collection_rekapkodesalesman('$iperiode','$bts') where i_salesman='$row->i_salesman' ",false);
# and v_sisa90>0
          $q = $this->db->get();
          if ($q->num_rows() > 0){
            foreach($q->result() as $col){
              $this->db->select("	
                          a.i_salesman, a.e_salesman_name, sum(total) as total, sum(realisasi) as realisasi, sum(totalnon) as totalnon, 
                          sum(realisasinon) as realisasinon, sum(blmbayar) as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat,
                          sum(realisasitdktelat) as realisasitdktelat, sum(realisasitelat) as realisasitelat from(
                          select b.i_salesman, b.e_salesman_name, sum(b.v_target_tagihan) as total, sum(b.v_realisasi_tagihan) as realisasi, 
                          0 as totalnon, 0 as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat,
                          0 as realisasitelat
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and i_salesman='$row->i_salesman'
                          group by b.i_salesman, b.e_salesman_name
                          union all
                          select b.i_salesman, b.e_salesman_name, 0 as total, 0 as realisasi, sum(b.v_target_tagihan) as totalnon, 
                          sum(b.v_realisasi_tagihan) as realisasinon, 0 as blmbayar, 0 as tdktelat, 0 as telat,
                          0 as realisasitdktelat, 0 as realisasitelat
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='f' and i_salesman='$row->i_salesman'
                          group by b.i_salesman, b.e_salesman_name
                          union all
                          select x.i_salesman, x.e_salesman_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          sum(x.v_target_tagihan) as blmbayar, 0 as tdktelat, 0 as telat, 0 as realisasitdktelat, 0 as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, 
                          sum(b.v_target_tagihan)-sum(b.v_realisasi_tagihan) as v_target_tagihan, 
                          0 as v_realisasi_tagihan
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and i_salesman='$row->i_salesman'
                          group by b.i_salesman, b.e_salesman_name
                          ) as x
                          group by x.i_salesman, x.e_salesman_name
                          union all

                          select a.i_salesman, a.e_salesman_name, 0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 
                          0  as blmbayar, sum(tdktelat) as tdktelat, sum(telat) as telat, sum(realisasitdktelat) as realisasitdktelat, 
                          sum(realisasitelat) as realisasitelat
                          from (

                          select b.i_salesman, b.e_salesman_name, 0  as blmbayar, sum(b.v_target_tagihan) as tdktelat, 0 as telat,
                          0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, sum(b.v_realisasi_tagihan) as realisasitdktelat,
                          0 as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, b.i_nota
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and i_salesman='$row->i_salesman' 
                          and substring(b.e_kelompok,1,2)='00'
                          group by b.i_salesman, b.e_salesman_name, b.i_nota
                          ) as c, tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and b.i_nota=c.i_nota and b.i_salesman='$row->i_salesman'
                          group by b.i_salesman, b.e_salesman_name
                          union all
                          select b.i_salesman, b.e_salesman_name, 0  as blmbayar, 0 as tdktelat, sum(b.v_target_tagihan) as telat,
                          0 as total, 0 as realisasi, 0 as totalnon, 0 as realisasinon, 0 as realisasitdktelat,
                          sum(b.v_realisasi_tagihan) as realisasitelat
                          from (
                          select b.i_salesman, b.e_salesman_name, b.i_nota
                          from tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and i_salesman='$row->i_salesman' 
                          and substring(b.e_kelompok,1,2)='01'
                          group by b.i_salesman, b.e_salesman_name, b.i_nota
                          ) as c, tm_collection_item b
                          where b.e_periode='$iperiode' and b.f_insentif='t' and b.i_nota=c.i_nota and b.i_salesman='$row->i_salesman'
                          group by b.i_salesman, b.e_salesman_name
                          

                          )as a
                          group by a.i_salesman, a.e_salesman_name
                          ) as a
                          group by a.i_salesman, a.e_salesman_name
                          order by a.e_salesman_name, a.i_salesman ",false);
              $r = $this->db->get();
              if ($r->num_rows() > 0){
                foreach($r->result() as $cols){
                  if($cols->realisasi==null || $cols->realisasi=='')$cols->realisasi=0;
                  if($cols->total!=0){
                    $vtagihantarget=$cols->total;
                    if($cols->realisasi<0){
                      $cols->realisasi=0;
                    }
                    $ncollection=number_format(($cols->realisasi/$cols->total)*100);
                    $vtagihanrealisasiinsentif=$cols->realisasi;
                    $vcollection=$cols->realisasi;
                  }else{
                    $vtagihantarget=0;
                    $ncollection='0';
                    $vtagihanrealisasiinsentif=0;
                    $vcollection=0;
      #              $vtagihanrealisasiinsentif=$vtagihanrealisasiinsentif+0;
      #              $vcollection=$vcollection+0;
                  }

                  $vtagihanrealisasi=$cols->realisasi;
                  $vtagihanrealisasi90=$col->v_sisa90;#$col->v_nilai_realisasi_insentif;#v_target90;
    #              $persen=($col->v_nilai_realisasi_insentif/$col->v_target_tagihan)*100;
                  if($cols->total==0){
                    $persen=0;
                  }else{
                    $persen=($vtagihanrealisasi90/$cols->total)*100;
                  }
                  $ntagihanrealisasi90=$persen;#$col->persen;
      #            $vtagihanrealisasi=$vtagihanrealisasi+$col->v_realisasi_tagihan;
      #            $vtagihanrealisasi90=$vtagihanrealisasi90+$col->v_target90;
      #            $ntagihanrealisasi90=$ntagihanreal1isasi90+$col->persen;
                
                }
              }
            }
          }else{
            $vtagihanrealisasiinsentif=0;
            $vcollection=0;
  #          $vtagihanrealisasiinsentif=$vtagihanrealisasiinsentif+0;
  #          $vcollection=$vcollection+0;
          }*/
          
  /*        
          if($ncollection>=55 && $ntagihanrealisasi90<=10){
            if($nomset>70 and $row->i_salesman!='00'){
              if($nomset>80 and $nomset<=90){
                  $vomset=200000;
              }elseif($nomset>90 and $nomset<=95){
                  $vomset=300000;
              }elseif($nomset>95 and $nomset<=100){
                  $vomset=400000;
              }elseif($nomset>100 and $nomset<=105){
                  $vomset=500000;
              }elseif($nomset>105 and $nomset<=110){
                  $vomset=650000;
              }elseif($nomset>110){
                  $vomset=800000;
              }else{
                $vomset=0;
              }
            }
          }else{
            $vomset=0;
          }
          if($nomset>=70 && $ntagihanrealisasi90<=10){
            if($ncollection>60 && $ncollection<=70){
              $vcollection=200000;
            }elseif($ncollection>70 && $ncollection<=75){
              $vcollection=300000;
            }elseif($ncollection>75 && $ncollection<=80){
              $vcollection=400000;
            }elseif($ncollection>80 && $ncollection<=85){
              $vcollection=500000;
            }elseif($ncollection>85 && $ncollection<=90){
              $vcollection=600000;
            }elseif($ncollection>90 && $ncollection<=95){
              $vcollection=700000;
            }elseif($ncollection>95){
              $vcollection=800000;
            }else{
              $vcollection=0;
            }
          }else{
            $vcollection=0;
          }*/
#          $vomsetgross=$vomsetgross+$row->v_nota_grossinsentif;
#          $vomsettarget=$vomsettarget+$row->v_target;
#          $vretur=$vretur+$row->v_retur_insentif;
#          $vomsetnetto=$vomsetgross-$vretur;
#          if($ntagihanrealisasi90<=10){
#            $vinsentif=$vomset+$vcollection;
#          }else{
#            $vinsentif=0;
#          }
 /*         $area=$row->i_area;
          $sales=$row->i_salesman;
          $namasales=$row->e_salesman_name;

          if($sales==$row->i_as){
            $sales='';
            $namasales='';
            $vinsentif=0;
            $vomsettarget=0;
            $vomsetgross=0;
            $vretur=0;
            $vomsetnetto=0;
            $vomset=0;
            $nomset=0;
            $vtagihantarget=0;
            $vtagihanrealisasi=0;
            $vtagihanrealisasiinsentif=0;
            $vtagihanrealisasi90=0;
            $ntagihanrealisasi90=0;
            $ncollection=0;
            $vcollection=0;
          }
          $quer 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
          $raw   	= $quer->row();
          $dproses= $raw->c;
        }
  		}*/
      if($row->v_omset_target!=0 && $sales!='  '){
        echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_salesman - $row->e_salesman_name</td>
			      <td align=right>Rp. ".number_format($row->v_insentif)."</td>
            <td align=right>Rp. ".number_format($row->v_omset_target)."</td>
            <td align=right>RP. ".number_format($row->v_omset_gross)."</td>
			      <td align=right>Rp. ".number_format($row->v_retur)."</td>
			      <td align=right>Rp. ".number_format($row->v_omset_netto)."</td>
            <td align=right>".number_format($row->n_omset)." %</td>
			      <td align=right>Rp. ".number_format($row->v_omset)."</td>
            <td align=right>RP. ".number_format($row->v_tagihan_target)."</td>
			      <td align=right>".number_format($row->v_tagihan_realisasi)." %</td>
			      <td align=right>Rp. ".number_format($row->v_tagihan_realisasiinsentif)."</td>
            <td align=right>RP. ".number_format($row->v_tagihan_realisasi90)."</td>
            <td align=right>".number_format($row->n_tagihan_realisasi90)." %</td>
			      <td align=right>".number_format($row->n_collection)." %</td>
			      <td align=right>Rp. ".number_format($row->v_collection)."</td></tr>";
			      $i++;
      }
#      $quer 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy-mm-dd hh:mi:ss') as c");
#      $raw   	= $quer->row();
#      $dproses= $raw->c;
#      $this->db->query("insert into tm_insentif values('$iperiode','$area','$sales', $vinsentif, $vomsettarget,
#                        $vomsetgross,$vretur,$vomsetnetto,$nomset,$vomset,$vtagihantarget,$vtagihanrealisasi,
#                        $vtagihanrealisasiinsentif,$vtagihanrealisasi90,$ncollection,$vcollection,'$dproses')");
		}
	}
}
	      ?>
	    </tbody>
	  </table>
    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listinsentifnew/cform/index","#main");' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listinsentifnew/cform/viewdetail";
	  formna.submit();
  }
</script>
