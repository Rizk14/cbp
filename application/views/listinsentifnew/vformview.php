<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>
<div id='tmp'>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'hitunginsentifnew/cform/export', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<?php
						$periode = $iperiode;
						$a = substr($periode, 0, 4);
						$b = substr($periode, 4, 2);
						$periode = mbulan($b) . " - " . $a;
						settype($a, 'integer');
						settype($b, 'integer');
						if ($b == 12) {
							$a = $a + 1;
							settype($a, 'string');
							settype($b, 'string');
							$b = '01';
						} else {
							$b = $b + 1;
							settype($a, 'string');
							settype($b, 'string');
							if (strlen($b) == 1) $b = '0' . $b;
						}
						$bts = $a . '-' . $b . '-01';
						?>
						<input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
						<?php
						echo "<center><h2>" . NmPerusahaan . "</h2></center>";
						echo "<center><h3>Insentif Salesman</h3></center>";
						echo "<center><h3>Periode $periode</h3></center>";
						?>
						<table class="listtable" border=none>
							<th>No</th>
							<th>Salesman</th>
							<th>Total Insentif</th>
							<th>% Tgt Omset</th>
							<th>% NB + FS</th>
							<th>Ins Oms</th>
							<th>Ins NB + FS</th>
							<th>Effective Call</th>
							<th>Ins</th>
							<th>% Coll</th>
							<th>Ins</th>
							<th>% OB vs OA</th>
							<th>Ins</th>
							<tbody>
								<?php
								if ($omset) {
									#		  $sales='';
									#		  $namasales='';
									$i = 1;
									foreach ($omset as $row) {
										if ($row->i_salesman != '00') {
											#          if($sales!='' && $row->i_salesman!=$sales && $row->v_omset_target!=0 && $sales!='  '){
											echo "<tr>
              <td align=right>$i</td>
              <td>$row->i_salesman - $row->e_salesman_name</td>
			        <td align=right>Rp. " . number_format($row->v_insentif) . "</td>
              <td align=right>" . number_format($row->n_omset) . " %</td>
              <td align=right>" . number_format($row->n_omset_reguler) . " %</td>
			        <td align=right>Rp. " . number_format($row->v_omset) . "</td>
			        <td align=right>Rp. " . number_format($row->v_omset_reguler) . "</td>
              <td align=right>" . number_format($row->n_effective_call) . " %</td>
			        <td align=right>Rp. " . number_format($row->v_effective_call) . "</td>
              <td align=right>" . number_format($row->n_collection) . " %</td>
			        <td align=right>" . number_format($row->v_collection) . "</td>
			        <td align=right>" . number_format($row->n_oboa) . " %</td>
              <td align=right>RP. " . number_format($row->v_oboa) . "</td></tr>";
											$i++;
											#            $sales='';
											#            $namasales='';
											#          }
											#          $sales=$row->i_salesman;
										}
									}
								}
								?>
							</tbody>
						</table>
						<center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listinsentifnew/cform/index","#main");'></center>
					</div>
					<script language="javascript" type="text/javascript">
						function yyy(a, c) {
							document.getElementById("iperiode").value = a;
							document.getElementById("iarea").value = c;
							formna = document.getElementById("listform");
							formna.action = "<?php echo site_url(); ?>" + "/listinsentifnew/cform/viewdetail";
							formna.submit();
						}
					</script>