<div id='tmp'>
<h2><?php echo $page_title.' Periode : '.$iperiode; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'kartuhutanggeneral/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
		  <th>No</th>
      <th>Supplier</th>
      <th>Sld Awal</th>
      <th>Debet</th>
      <th>Kredit</th>
      <th>Sld Akhir</th>
      <tbody>
        <?php 
    $i=0;
  if($isi){
    foreach($isi as $row){
        $i++;
        echo "<tr> 
          <td>$i</td>
          <td>($row->i_supplier) $row->e_supplier_name</td>
          <td align=right>".number_format($row->v_saldo_awal)."</td>
          <td align=right>".number_format($row->v_debet)."</td>
          <td align=right>".number_format($row->v_kredit)."</td>
          <td align=right>".number_format($row->v_saldo_akhir)."</td>
        </tr>";
    }
  }
        ?>
    <tr>
      <td colspan='2' align="center">TOTAL</td>
      <td align="right"><?php foreach($jumsaldoawal as $raw){echo number_format($raw->jumsaldoawal); }?></td>
      <td align="right"><?php foreach($jumdebet as $raw){echo number_format($raw->jumdebet); }?></td>
      <td align="right"><?php foreach($jumkredit as $raw){echo number_format($raw->jumkredit); }?></td>
      <td align="right"><?php foreach($jumsaldoakhir as $raw){echo number_format($raw->jumsaldoakhir); }?></td>
    </tr>

    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(b,d,c){
    showModal("kartuhutanggeneral/cform/detail/"+b+"/"+c+"/"+d+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
