<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
	$tujuan = 'rekapspb/cform/rekap';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#pesan','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	   	<th>No SPB</th>
			<th>Tgl SPB</th>
			<th>Sls</th>
			<th>Lang</th>
			<th>Area</th>
			<th>SPB (Rp)</th>
			<th>Status</th>
			<th class="action">Act</th>
	    <tbody>
	      <?php 
        echo "<input type='hidden' id='dfrom' name='dfrom' value='$dfrom'>";
        echo "<input type='hidden' id='dto' name='dto' value='$dto'>";
		if($isi){
      $i=0;
			foreach($isi as $row){
        $i++;
        $que=$this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spb_item
                                where i_area = '$row->i_area' and i_spb='$row->i_spb'");
		    if ($que->num_rows() > 0){
			    foreach($que->result() as $riw){
            $order=$riw->order;
          }
        }else{
          $order=0;
        }
        if($row->d_spb){
			    $tmp=explode('-',$row->d_spb);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_spb=$tgl.'-'.$bln.'-'.$thn;
        }
			  if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			  	$status='Batal';
			  }elseif(
				 	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					 ){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					  ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					  ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ar)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null)
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					 ){
			  	$status='OP Close';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap SJ (sales)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
#			  	$status='Siap SJ (gudang)';
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
        }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) 
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }
			  $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
			  //$row->v_spb	= number_format($row->v_spb);
 			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);
#			  $nota	= number_format($row->v_nota_netto);
			  echo "<tr> 
				  <td valign=top style=\"font-size: 13px;\"><input type='hidden' id='ispb".$i."' name='ispb".$i."' value='$row->i_spb'>$row->i_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_salesman</td>";
				if(substr($row->i_customer,2,3)!='000'){
					echo "
				  <td valign=top style=\"font-size: 13px;\">($row->i_customer) $row->e_customer_name</td>";
				}else{
					echo "
				  <td valign=top style=\"font-size: 13px;\">$row->xname</td>";
				}
			  echo "
				  <td valign=top style=\"font-size: 13px;\"><input type='hidden' id='eareaname".$i."' name='eareaname".$i."' value='$row->e_area_name'><input type='hidden' id='iarea".$i."' name='iarea".$i."' value='$row->i_area'>$row->i_area</td>
				  <td valign=top align=right style=\"font-size: 13px;\">$bersih</td>
				  <td valign=top style=\"font-size: 13px;\">$status</td>
				  <td valign=top class=\"action\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'>";
				echo "</td></tr>";				  
			}
      echo "<tr>
							<td colspan='10' align='center'><input type='submit' id='transfer' name='transfer' value='Rekap'></td>
					  </tr>
					  <input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">";
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
      <table class="listtable">
				<tr>
					<td align="center">
						<input type="button" id="pilihsemua" name="pilihsemua" value="Pilih Semua" 
							   onclick="pilihsemua()">
						<input type="button" id="tidakpilihsemua" name="tidakpilihsemua" value="Tidak Semua"
							   onclick="tidakpilihsemua()">
						<input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="batal()">
					</td>
				</tr>
			</table>
      <div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function pilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=true;
			document.getElementById("chk"+i).value='on';
		}
	}
	function tidakpilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=false;
			document.getElementById("chk"+i).value='';
		}
	}
	function batal(){
		show('rekapspb/cform/','#main');
	}
	function pilihan(a,b){
		if(a==''){
			document.getElementById("chk"+b).value='on';
		}else{
			document.getElementById("chk"+b).value='';
		}
	}
</script>
