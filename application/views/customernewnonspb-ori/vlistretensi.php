<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'customernewnonspb/cform/retensi','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="2" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode retensi</th>
	    <th>Nama</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_retensi','$row->e_retensi')\">$row->i_retensi</a></td>
				  <td><a href=\"javascript:setValue('$row->i_retensi','$row->e_retensi')\">$row->e_retensi</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    document.getElementById("iretensi").value=a;
    if(a=='00') document.getElementById("nvisitperiod").value='0';
    if(a=='01') document.getElementById("nvisitperiod").value='30';
    if(a=='02') document.getElementById("nvisitperiod").value='7';
    if(a=='03') document.getElementById("nvisitperiod").value='3';
    if(a=='04') document.getElementById("nvisitperiod").value='60';
    if(a=='05') document.getElementById("nvisitperiod").value='14';
    if(a=='06') document.getElementById("nvisitperiod").value='21';
    if(a=='07') document.getElementById("nvisitperiod").value='120';
    if(a=='08') document.getElementById("nvisitperiod").value='90';
    if(a=='XX') document.getElementById("nvisitperiod").value='0';
    document.getElementById("eretensi").value=b;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal()
  {
	  jsDlgHide("#konten *","#fade","#light");
  }
</script>
