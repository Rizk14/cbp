<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'opnnotacustomer/cform/opname','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	<table class="listtable">
 	    <th>KodeLang</th>
 	    <th>KodeSales</th>
	    <th>No Nota</th>
	    <th>Tgl Nota</th>
 	    <th>Jatuh Tempo</th>
	    <th>Nilai Bersih</th>
	    <th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->d_nota!=''){
          $tmp=explode('-',$row->d_nota);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_jatuh_tempo!=''){
          $tmp=explode('-',$row->d_jatuh_tempo);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_jatuh_tempo=$tgl.'-'.$bln.'-'.$thn;
        }
			  echo "<tr> 
				  <td>$row->i_customer</td>
				  <td>$row->i_salesman</td>
				  <td>$row->i_nota</td>
				  <td>$row->d_nota</td>
				  <td>$row->d_jatuh_tempo</td>
				  <td align=right>".number_format($row->v_nota_netto)."</td>
				  <td align=right>".number_format($row->v_sisa)."</td>
				</tr>";

			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
