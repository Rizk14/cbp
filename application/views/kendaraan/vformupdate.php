<?php 
	echo "<h2>".$page_title."</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'kendaraan/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="kendaraanform">
	<div class="effect">
	  <div class="accordion2">
	  <?php foreach ($isi as $row) { ?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Plat Nomor</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="ikendaraan" id="ikendaraan" value="<?php echo $row->i_kendaraan;?>" onkeyup="gede(this)"></td>
	      </tr>
	      <tr>
		<td width="15%">Pemilik Pada STNK</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_pemilik_kendaraan" id="e_pemilik_kendaraan" value="<?php echo $row->e_pemilik_kendaraan;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Posisi Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_posisi_kendaraan" id="e_posisi_kendaraan" value="<?php echo $row->e_posisi_kendaraan;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Pengguna</td>
		<td width="1%">:</td>
		<td width="87%"><input name="epengguna" id="epengguna" value="<?php echo $row->e_pengguna;?>" onkeyup="gede(this)"></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_serah_terima_kendaraan!=''){
				$tmp=explode('-',$row->d_serah_terima_kendaraan);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_serah_terima_kendaraan=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="18%">Serah Terima Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_serah_terima_kendaraan" id="d_serah_terima_kendaraan" value="<?php echo $row->d_serah_terima_kendaraan;?>" readonly onclick="showCalendar('',this,this,'','d_serah_terima_kendaraan',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="15%">Merek Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_merek_kendaraan" id="e_merek_kendaraan" value="<?php echo $row->e_merek_kendaraan;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Jenis Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_jenis_kendaraan" id="e_jenis_kendaraan" value="<?php echo $row->e_jenis_kendaraan;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Warna Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_warna_kendaraan" id="e_warna_kendaraan" value="<?php echo $row->e_warna_kendaraan;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Nomor Rangka</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_nomor_rangka" id="e_nomor_rangka" value="<?php echo $row->e_nomor_rangka;?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Nomor Mesin</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_nomor_mesin" id="e_nomor_mesin" value="<?php echo $row->e_nomor_mesin;?>"></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_pajak_1_tahun!=''){
				$tmp=explode('-',$row->d_pajak_1_tahun);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_pajak_1_tahun=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Pajak 1 Tahun</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_pajak_1_tahun" id="d_pajak_1_tahun" value="<?php echo $row->d_pajak_1_tahun;?>" readonly onclick="showCalendar('',this,this,'','d_pajak_1_tahun',0,20,1)"></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_pajak_5_tahun!=''){
				$tmp=explode('-',$row->d_pajak_5_tahun);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_pajak_5_tahun=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Pajak 5 Tahun</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_pajak_5_tahun" id="d_pajak_5_tahun" value="<?php echo $row->d_pajak_5_tahun; ?>" readonly onclick="showCalendar('',this,this,'','d_pajak_5_tahun',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Asuransi Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="ikendaraanasuransi" id="ikendaraanasuransi" value="<?php echo $row->i_kendaraan_asuransi;?>" onclick='showModal("kendaraan/cform/asuransikendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanasuransi" id="ekendaraanasuransi" value="<?php echo $row->e_kendaraan_asuransi?>" onclick='showModal("kendaraan/cform/asuransikendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_bayar_asuransi!=''){
				$tmp=explode('-',$row->d_bayar_asuransi);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_bayar_asuransi=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Tanggal Bayar Asuransi</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_bayar_asuransi" id="d_bayar_asuransi" value="<?php echo $row->d_bayar_asuransi;?>" readonly onclick="showCalendar('',this,this,'','d_bayar_asuransi',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="15%">Nomor Polis Asuransi</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_nomor_polisasuransi" id="e_nomor_polisasuransi" value="<?php echo $row->e_nomor_polisasuransi; ?>"></td>
	      </tr>
	      <tr>
		<td width="15%">TLO</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_tlo" id="e_tlo" value="<?php echo $row->e_tlo;?>"></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_periode_awalasuransi!=''){
				$tmp=explode('-',$row->d_periode_awalasuransi);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_periode_awalasuransi=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Periode Awal Asuransi</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_periode_awalasuransi" id="d_periode_awalasuransi" value="<?php echo $row->d_periode_awalasuransi;?>" readonly onclick="showCalendar('',this,this,'','d_periode_awalasuransi',0,20,1)"></td>
	      </tr>
	      <tr>
	      	<?php 
			if($row->d_periode_akhirasuransi!=''){
				$tmp=explode('-',$row->d_periode_akhirasuransi);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_periode_akhirasuransi=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Periode Akhir Asuransi</td>
		<td width="1%">:</td>
		<td width="87%"><input name="d_periode_akhirasuransi" id="d_periode_akhirasuransi" value="<?php echo $row->d_periode_akhirasuransi;?>" readonly onclick="showCalendar('',this,this,'','d_periode_akhirasuransi',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Tahun</td>
		<td width="1%">:</td>
		<td width="87%"><input name="e_tahun" id="e_tahun" value="<?php echo $row->e_tahun;?>" maxlength="4"></td>
	      </tr>
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><select name="iperiodebl" id="iperiodebl">
							<?php $bl=substr($row->i_periode,4,2);?>
							<option <?php if($bl=='01') echo 'selected'; ?>>01</option>
							<option <?php if($bl=='02') echo 'selected'; ?>>02</option>
							<option <?php if($bl=='03') echo 'selected'; ?>>03</option>
							<option <?php if($bl=='04') echo 'selected'; ?>>04</option>
							<option <?php if($bl=='05') echo 'selected'; ?>>05</option>
							<option <?php if($bl=='06') echo 'selected'; ?>>06</option>
							<option <?php if($bl=='07') echo 'selected'; ?>>07</option>
							<option <?php if($bl=='08') echo 'selected'; ?>>08</option>
							<option <?php if($bl=='09') echo 'selected'; ?>>09</option>
							<option <?php if($bl=='10') echo 'selected'; ?>>10</option>
							<option <?php if($bl=='11') echo 'selected'; ?>>11</option>
							<option <?php if($bl=='12') echo 'selected'; ?>>12</option>
						</select>&nbsp;<input readonly name="iperiodeth" id="iperiodeth" value="<?php echo substr($row->i_periode,0,4);?>" maxlength="4"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area;?>" onclick='showModal("kendaraan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name;?>" onclick='showModal("kendaraan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Jenis Kendaraan</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="ikendaraanjenis" id="ikendaraanjenis" value="<?php echo $row->i_kendaraan_jenis;?>" onclick='showModal("kendaraan/cform/jeniskendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanjenis" id="ekendaraanjenis" value="<?php echo $row->e_kendaraan_jenis;?>" onclick='showModal("kendaraan/cform/jeniskendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Jenis BBM</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="ikendaraanbbm" id="ikendaraanbbm" value="<?php echo $row->i_kendaraan_bbm;?>" onclick='showModal("kendaraan/cform/bbmkendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanbbm" id="ekendaraanbbm" value="<?php echo $row->e_kendaraan_bbm;?>" onclick='showModal("kendaraan/cform/bbmkendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<?php 
			if($row->d_pajak!=''){
				$tmp=explode('-',$row->d_pajak);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_pajak=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
		<td width="12%">Tanggal Pajak</td>
		<td width="1%">:</td>
		<td width="87%"><input name="dpajak" id="dpajak" value="<?php echo $row->d_pajak;?>" readonly onclick="showCalendar('',this,this,'','dfrom',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Keterangan </td>
		<td width="1%">:</td>
		<td width="87%"><textarea name="e_desc" id="e_desc" value=""><?php echo $row->e_desc;?></textarea></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listkendaraan/cform/view/<?php echo $iperiode."/"; ?>','#main')">
		</td>
	      </tr>
		</table>
		<?php  } ?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("icoa").value=='')||
			(document.getElementById("vsaldoawal").value=='')||
			(document.getElementById("vsaldoakhir").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function tesss(){
		document.getElementById("icoa").value="";
		document.getElementById("iperiodebl").value="";
		document.getElementById("iperiodeth").value="";
		document.getElementById("ecoaname").value="";
		document.getElementById("vsaldoawal").value="0";
		document.getElementById("vmutasidebet").value="0";
		document.getElementById("vmutasikredit").value="0";
		document.getElementById("vsaldoakhir").value="0";
		document.getElementById("login").hidden=false;
		document.getElementById("pesan").innerHTML='';
	}
	function sinkronaw(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		sak = saw+vmd-vmk;
		document.getElementById("vsaldoakhir").value = formatcemua(sak);
	}
	function sinkronak(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		saw = sak+vmk-vmd;
		document.getElementById("vsaldoawal").value = formatcemua(saw);
	}
</script>
