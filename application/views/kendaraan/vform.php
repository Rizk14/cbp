<div id="tabbed_box_1" class="tabbed_box">  
	<div class="tabbed_area">  
		<ul class="tabs">  
			<li><a href="#" class="tab active" onclick="sitab('content_1')">Input Kendaraan</a></li>  
			<li><a href="#" class="tab" onclick="sitab('content_2')">Pemeliharaan Kendaraan</a></li>
		</ul>
		<div id="content_1" class="content">
			<table class="maintable">
				<tr>
					<td align="left">
						<?php echo $this->pquery->form_remote_tag(array('url'=>'kendaraan/cform/simpan','update'=>'#pesan','type'=>'post'));?>
						<div id="kendaraanform">
							<div class="effect">
								<div class="accordion2">
									<table class="mastertable">
										<tr>
											<td width="12%">Nomor Polisi</td>
											<td width="1%">:</td>
											<td width="30%">
												<input name="ikendaraan" id="ikendaraan" value="" maxlength="9" onkeyup="gede(this)">
												<div id="confnomor"></div>
											</td>
											<td width="15%">Nomor Rangka</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_nomor_rangka" id="e_nomor_rangka" value=""></td>  

										</tr>
										<tr>
											<td width="15%">Posisi Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_posisi_kendaraan" id="e_posisi_kendaraan" value="" onkeyup="gede(this)"></td>

											<td width="15%">Jenis Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_jenis_kendaraan" id="e_jenis_kendaraan" value=""></td>
										</tr>
										<tr>
											<td width="18%">Serah Terima Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_serah_terima_kendaraan" id="d_serah_terima_kendaraan" value="" readonly onclick="showCalendar('',this,this,'','d_serah_terima_kendaraan',0,20,1)"></td>

											<td width="15%">Merek Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_merek_kendaraan" id="e_merek_kendaraan" value="" onkeyup="gede(this)"></td>
										</tr>
										<tr>
											<td width="15%">Pengguna</td>
											<td width="1%">:</td>
											<td width="30%"><input name="epengguna" id="epengguna" value="" onkeyup="gede(this)"></td>

											<td width="15%">Warna Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_warna_kendaraan" id="e_warna_kendaraan" value=""></td>
										</tr>
										<tr>
											<td width="19%">Pemilik Pada STNK</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_pemilik_kendaraan" id="e_pemilik_kendaraan" value="" onkeyup="gede(this)"></td>

											<td width="15%">Nomor Mesin</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_nomor_mesin" id="e_nomor_mesin" value=""></td>
										</tr>
										<tr>
											<td width="15%">Pajak 1 Tahun</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_pajak_1_tahun" id="d_pajak_1_tahun" value="" readonly onclick="showCalendar('',this,this,'','d_pajak_1_tahun',0,20,1)"></td>

											<td width="15%">Pajak 5 Tahun</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_pajak_5_tahun" id="d_pajak_5_tahun" value="" readonly onclick="showCalendar('',this,this,'','d_pajak_5_tahun',0,20,1)"></td>
										</tr>
										<tr>
											<td width="15%">Asuransi Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input type="hidden" name="ikendaraanasuransi" id="ikendaraanasuransi" value="" onclick='showModal("kendaraan/cform/asuransikendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanasuransi" id="ekendaraanasuransi" value="" onclick='showModal("kendaraan/cform/asuransikendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>

											<td width="15%">Tanggal Bayar Asuransi</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_bayar_asuransi" id="d_bayar_asuransi" value="" readonly onclick="showCalendar('',this,this,'','d_bayar_asuransi',0,20,1)"></td>
										</tr>
										<tr>
											<td width="15%">Nomor Polis Asuransi</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_nomor_polisasuransi" id="e_nomor_polisasuransi" value=""></td>

											<td width="15%">TLO</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_tlo" id="e_tlo" value=""></td>
										</tr>
										<tr>
											<td width="15%">Periode Awal Asuransi</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_periode_awalasuransi" id="d_periode_awalasuransi" value="" readonly onclick="showCalendar('',this,this,'','d_periode_awalasuransi',0,20,1)"></td>

											<td width="15%">Periode Akhir Asuransi</td>
											<td width="1%">:</td>
											<td width="30%"><input name="d_periode_akhirasuransi" id="d_periode_akhirasuransi" value="" readonly onclick="showCalendar('',this,this,'','d_periode_akhirasuransi',0,20,1)"></td>
										</tr>
										<tr>
											<td width="12%">Tahun Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_tahun" id="e_tahun" value="" maxlength="4"></td>

											<td width="12%">Periode</td>
											<td width="1%">:</td>
											<td width="30%"><select name="iperiodebl" id="iperiodebl">
												<option>01</option>
												<option>02</option>
												<option>03</option>
												<option>04</option>
												<option>05</option>
												<option>06</option>
												<option>07</option>
												<option>08</option>
												<option>09</option>
												<option>10</option>
												<option>11</option>
												<option>12</option>
											</select>&nbsp;<input name="iperiodeth" id="iperiodeth" value="" onkeyup="cektahun(this)" maxlength="4"></td>
										</tr>
										<tr>
											<td width="12%">Area</td>
											<td width="1%">:</td>
											<td width="30%"><input type="hidden" name="iarea" id="iarea" value="" onclick='showModal("kendaraan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="eareaname" id="eareaname" value="" onclick='showModal("kendaraan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>

											<td width="12%">Jenis Kendaraan</td>
											<td width="1%">:</td>
											<td width="30%"><input type="hidden" name="ikendaraanjenis" id="ikendaraanjenis" value="" onclick='showModal("kendaraan/cform/jeniskendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanjenis" id="ekendaraanjenis" value="" onclick='showModal("kendaraan/cform/jeniskendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
										</tr>
										<tr>
											<td width="12%">Jenis BBM</td>
											<td width="1%">:</td>
											<td width="30%"><input type="hidden" name="ikendaraanbbm" id="ikendaraanbbm" value="" onclick='showModal("kendaraan/cform/bbmkendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ekendaraanbbm" id="ekendaraanbbm" value="" onclick='showModal("kendaraan/cform/bbmkendaraan/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>

											<td width="12%">Tanggal Pajak</td>
											<td width="1%">:</td>
											<td width="30%"><input name="dpajak" id="dpajak" script="width:200px" value="" readonly onclick="showCalendar('',this,this,'','dpajak',0,20,1)"></td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Mesin</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_mesin" id="e_km_gantioli_mesin" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Mesin Selanjutnya</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_mesin" id="e_km_gantioli_mesin" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Gardan</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_gardan" id="e_km_gantioli_gardan" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Gardan Selanjutnya</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_gardan" id="e_km_gantioli_gardan" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Transmisi</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_transmisi" id="e_km_gantioli_transmisi" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KM Ganti Oli Transmisi Selanjutnya</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_km_gantioli_transmisi" id="e_km_gantioli_transmisi" value="">/KM</td>
										</tr>
										<tr>
											<td width="25%">KIR</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_kir" id="e_kir" value=""></td>
										</tr>
										<tr>
											<td width="25%">IBM</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_ibm" id="e_km_gantioli_mesin" value=""></td>
										</tr>
										<tr>
											<td width="25%">SIPA</td>
											<td width="1%">:</td>
											<td width="30%"><input name="e_sipa" id="e_km_gantioli_mesin" value=""></td>
										</tr>
										<tr>
											<td width="25%">Keterangan</td>
											<td width="1%">:</td>
											<td width="30%"><textarea name="e_desc" id="e_desc" value=""></textarea></td>
										</tr>

										<td width="12%">&nbsp;</td>
										<td width="1%">&nbsp;</td>
										<td width="30%">
											<input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
											<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('kendaraan/cform/','#main');">
										</td>
									</tr>
								</table>
							</div>
							<div id="pesan"></div>
						</div>
					</div>
					<?=form_close()?>
				</td>
			</tr>
		</table>
	</div>
	<div id="content_2" class="content">
		<table class="maintable">
			<tr>
				<td align="left">
					<?php echo $this->pquery->form_remote_tag(array('url'=>'kendaraan/cform/simpanservice','update'=>'#main','type'=>'post'));?>
					<div id="masterproductbaseform">
						<table class="mastertable">
							<tr>
								<td width="19%">Plat Nomor</td>
								<td width="1%">:</td>
								<td width="30%">
									<input type="hidden" name="platnomor" id="platnomor" value=""></input>
									<input readonly id="xplatnomor" value="" onclick='showModal("kendaraan/cform/platnomor/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
								<td width="19%">Jenis Kendaraan</td>
								<td width="1%">:</td>
								<td width="30%">
									<input readonly type="text" name="xjeniskendaraan" id="xjeniskendaraan" value=""></td>
								</tr>
								<tr>
									<td width="19%">Tanggal</td>
									<td width="1%">:</td>
									<td width="30%">
									<?php 
												$data = array(
													'name'        => 'xtgl',
													'id'          => 'xtgl',
													'value'       => '',
													'readonly'   	=> 'true',
													'onclick'	    => "showCalendar('',this,this,'','xtgl',0,20,1)");
													echo form_input($data);?></td>
										<td width="19%">Jenis BBM</td>
										<td width="1%">:</td>
										<td width="30%">
											<input readonly type="text" name="xjenisbbm" id="xjenisbbm" value="" ></td>
										</tr>

										<tr>
											<td width="19%">&nbsp;</td>
											<td width="1%">&nbsp;</td>
											<td width="30%">
												<input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
												<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('kendaraan/cform/','#main');">
												<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
												<input type="hidden" name="jml" id="jml" value="1">
											</td>
									<td width="19%">Area</td>
									<td width="1%">:</td>
									<td width="30%">
										<input readonly type="text" name="xarea" id="xarea" value=""></td>
										</tr>
									</table>
								</div>

								<div id="detailheader" align="center">
									<table id="itemtem" class="listtable">
										<tr>
											<th style="width:90px;" align="center">KM Awal</th>
											<th style="width:90px;" align="center">KM Akhir</th>
											<th style="width:90px;" align="center">Selisih</th>
											<th style="width:50px;" align="center">Qty</th>
											<th style="width:50px;" align="center">Satuan</th>
											<th style="width:100px;" align="center">Uraian Perbaikan</th>
											<th style="width:100px;" align="center">Keterangan</th>
											<th style="width:100px;" align="center">Nama Bengkel</th>
											<th style="width:100px;" align="center">Biaya</th>
										</tr></table>
									</div>
									<div id="detailisi" align="center">
										<table id="itemtem" class="listtable">
											<tbody>
												<tr>
													<td style="width:90px;"><input style="width:90px;" type="text" id="xkmawal1" name="xkmawal1" value="" onkeyup="selisih(1);" onkeypress="return hanyaAngka(event)"></td>
													<td style="width:90px;"><input style="width:90px;" type="text" id="xkmakhir1" name="xkmakhir1" value="" onkeyup="selisih(1);" onkeypress="return hanyaAngka(event)"></td>
													<td style="width:90px;"><input style="width:90px;" type="text" id="xselisih1" name="xselisih1" value="" readonly="" onkeypress="return hanyaAngka(event)"></td>
													<td style="width:50px;"><input style="width:50px;" type="text" id="xqty1" name="xqty1" value="" onkeypress="return hanyaAngka(event)"></td>
													<td style="width:50px;"><input type="hidden"id="xsatuan1" name="xsatuan1" value=""></input><input style="width:50px;" type="text" id="xesatuan1" value="" onclick='showModal("kendaraan/cform/satuan/1","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
													<td style="width:100px;"><input style="width:100px;" type="text" id="xuraian1" name="xuraian1" value=""></td>
													<td style="width:100px;"><input style="width:100px;" type="text" id="xketerangan1" name="xketerangan1" value=""></td>
													<td style="width:100px;"><input style="width:100px;" type="text" id="xnama_bengkel1" name="xnama_bengkel1" value=""></td>
													<td style="width:100px;"><input style="width:100px;" type="text" id="xbiaya1" name="xbiaya1" value="" onkeypress="return hanyaAngka(event)"></td>
												</tr>
											</tbody>
										</table>
									</div>
									<?=form_close()?>
								</td>
							</tr> 
						</table>
						<div id="pesan"></div>
					</div>
				</div>
			</div>
			<script language="javascript" type="text/javascript">
				function dipales(){
					if(
						(document.getElementById("iperiodeth").value=='') ||
						(document.getElementById("iperiodebl").value=='') ||
						(document.getElementById("iarea").value=='')||
						(document.getElementById("ikendaraanjenis").value=='')||
						(document.getElementById("ikendaraanasuransi").value=='')||
						(document.getElementById("ikendaraanbbm").value=='')
						)
					{
						alert("Data Header belum lengkap !!!");
					}else{			
						document.getElementById("login").hidden=true;
					}
				}
				function tesss(){
					document.getElementById("icoa").value="";
					document.getElementById("iperiodebl").value="";
					document.getElementById("iperiodeth").value="";
					document.getElementById("ecoaname").value="";
					document.getElementById("vsaldoawal").value="0";
					document.getElementById("vmutasidebet").value="0";
					document.getElementById("vmutasikredit").value="0";
					document.getElementById("vsaldoakhir").value="0";
					document.getElementById("login").hidden=TRUE;
					document.getElementById("pesan").innerHTML='';
				}
				function sinkronaw(){
					saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
					vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
					vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
					sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
					sak = saw+vmd-vmk;
					document.getElementById("vsaldoakhir").value = formatcemua(sak);
				}
				function sinkronak(){
					saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
					vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
					vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
					sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
					saw = sak+vmk-vmd;
					document.getElementById("vsaldoawal").value = formatcemua(saw);
				}
				function tambah_item(a){
					if(a<11){
						si_inner=document.getElementById("detailisi").innerHTML;
						if(si_inner==''){
							document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
							juml=document.getElementById("jml").value;	
							si_inner='<table id="itemtem" class="listtable"><tbody><tr><td style="width:90px;"><input style="width:90px;" type="text" id="xkmawal'+a+'" name="xkmawal'+a+'" value="" onkeyup="selisih('+a+');" onkeypress="return hanyaAngka(event)"></td><td style="width:90px;"><input style="width:90px;" type="text" id="xkmakhir'+a+'" name="xkmakhir'+a+'" value="" onkeyup="selisih('+a+');" onkeypress="return hanyaAngka(event)"></td><td style="width:90px;"><input style="width:90px;" type="text" id="xselisih'+a+'" name="xselisih'+a+'" value="" onkeypress="return hanyaAngka(event)"></td><td style="width:50px;"><input style="width:50px;" type="text" id="xqty'+a+'" name="xqty'+a+'" value="" onkeypress="return hanyaAngka(event)"></td><td style="width:50px;"><input type="hidden"id="xsatuan'+a+'" name="xsatuan'+a+'" value=""><input style="width:50px;" type="text" id="xesatuan'+a+'" value="" onclick=\'showModal("kendaraan/cform/satuan/'+a+'","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td><td style="width:100px;"><input style="width:100px;" type="text" id="xuraian'+a+'" name="xuraian'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xketerangan'+a+'" name="xketerangan'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xnama_bengkel'+a+'" name="xnama_bengkel'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xbiaya'+a+'" name="xbiaya'+a+'" value="" onkeypress="return hanyaAngka(event)"></td></tr></tbody></table>';
						}else{
							document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
							juml=document.getElementById("jml").value;
							si_inner=si_inner+'<table id="itemtem" class="listtable"><tbody><tr><td style="width:90px;"><input style="width:90px;" type="text" id="xkmawal'+a+'" name="xkmawal'+a+'" value="" onkeyup="selisih('+a+');" onkeypress="return hanyaAngka(event)"></td><td style="width:90px;"><input style="width:90px;" type="text" id="xkmakhir'+a+'" name="xkmakhir'+a+'" value="" onkeyup="selisih('+a+');" onkeypress="return hanyaAngka(event)"></td><td style="width:90px;"><input style="width:90px;" type="text" id="xselisih'+a+'" name="xselisih'+a+'" value="" onkeypress="return hanyaAngka(event)"></td><td style="width:50px;"><input style="width:50px;" type="text" id="xqty'+a+'" name="xqty'+a+'" value="" onkeypress="return hanyaAngka(event)"></td><td style="width:50px;"><input type="hidden"id="xsatuan'+a+'" name="xsatuan'+a+'" value=""><input style="width:50px;" type="text" id="xesatuan'+a+'" value="" onclick=\'showModal("kendaraan/cform/satuan/'+a+'","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td><td style="width:100px;"><input style="width:100px;" type="text" id="xuraian'+a+'" name="xuraian'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xketerangan'+a+'" name="xketerangan'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xnama_bengkel'+a+'" name="xnama_bengkel'+a+'" value=""></td><td style="width:100px;"><input style="width:100px;" type="text" id="xbiaya'+a+'" name="xbiaya'+a+'" value="" onkeypress="return hanyaAngka(event)"></td></tr></tbody></table>';
						}

						j=0;
						var xkmawal		= Array();
						var xkmakhir		= Array();
						var xselisih		= Array();
						var xqty	= Array();
						var xsatuan= Array();
						var xuraian			= Array();
						var xketerangan			= Array();
						var xnama_bengkel			= Array();
						var xbiaya			= Array();
						for(i=1;i<a;i++){
							j++;
							xkmawal[j]			= document.getElementById("xkmawal"+i).value;
							xkmakhir[j]			= document.getElementById("xkmakhir"+i).value;
							xselisih[j]			= document.getElementById("xselisih"+i).value;
							xqty[j]			= document.getElementById("xqty"+i).value;
							xsatuan[j]			= document.getElementById("xsatuan"+i).value;
							xuraian[j]			= document.getElementById("xuraian"+i).value;
							xketerangan[j]			= document.getElementById("xketerangan"+i).value;
							xnama_bengkel[j]			= document.getElementById("xnama_bengkel"+i).value;
							xbiaya[j]			= document.getElementById("xbiaya"+i).value;
						}
						document.getElementById("detailisi").innerHTML=si_inner;
						j=0;
						for(i=1;i<a;i++){
							j++;
							document.getElementById("xkmawal"+i).value     = xkmawal[j];
							document.getElementById("xkmakhir"+i).value     = xkmakhir[j];
							document.getElementById("xselisih"+i).value     = xselisih[j];
							document.getElementById("xqty"+i).value     = xqty[j];
							document.getElementById("xsatuan"+i).value     = xsatuan[j];
							document.getElementById("xuraian"+i).value     = xuraian[j];
							document.getElementById("xketerangan"+i).value     = xketerangan[j];
							document.getElementById("xnama_bengkel"+i).value     = xnama_bengkel[j];
							document.getElementById("xbiaya"+i).value     = xbiaya[j];
						}
					}else{
						alert('Maksimum 10 item');
					}
				}
				function selisih(a){
					var km_awal = document.getElementById('xkmawal'+a).value;
					var kmakhir = document.getElementById('xkmakhir'+a).value;
					
					var hasil = kmakhir - km_awal;
					document.getElementById('xselisih'+a).value = hasil;
				}
				function hanyaAngka(evt) {
					var charCode = (evt.which) ? evt.which : event.keyCode
					if (charCode > 31 && (charCode < 48 || charCode > 57))

						return false;
					return true;
				}
			</script>
