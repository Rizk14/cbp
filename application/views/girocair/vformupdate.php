<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'girocair/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="mastergiroform">
	<div class="effect">
	  <div class="accordion2">
	  <?php 
			if($isi->d_giro!=''){
				$tmp=explode("-",$isi->d_giro);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_giro=$hr."-".$bl."-".$th;
			}
			if($isi->d_rv!=''){
				$tmp=explode("-",$isi->d_rv);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_rv=$hr."-".$bl."-".$th;
			}
			if($isi->d_giro_duedate!=''){
				$tmp=explode("-",$isi->d_giro_duedate);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_giro_duedate=$hr."-".$bl."-".$th;
			}
			if($isi->d_giro_cair!=''){
				$tmp=explode("-",$isi->d_giro_cair);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_giro_cair=$hr."-".$bl."-".$th;
			}
			if($isi->d_giro_tolak!=''){
				$tmp=explode("-",$isi->d_giro_tolak);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_giro_tolak=$hr."-".$bl."-".$th;
			}
	  ?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Giro</td>
		<td width="1%">:</td>
		<td width="37%"><input name="igiro" id="igiro" value="<?php echo $isi->i_giro ?>" maxlength="10">
						<input readonly name="dgiro" id="dgiro" value="<?php echo $isi->d_giro ?>" 
						 onclick="showCalendar('',this,this,'','dgiro',0,20,1)"></td>
		<td width="12%">Tanggal JT</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="dgiroduedate" id="dgiroduedate" value="<?php echo $isi->d_giro_duedate ?>" 
						 onclick="showCalendar('',this,this,'','dgiroduedate',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Receive Voucher</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="irv" id="irv" value="<?php echo $isi->i_rv ?>" maxlength="6">
						<input readonly name="drv" id="drv" value="<?php echo $isi->d_rv ?>" 
						 onclick="showCalendar('',this,this,'','drv',0,20,1)"></td>
		<td width="12%">Tanggal Cair</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="dgirocair" id="dgirocair" value="<?php echo $isi->d_giro_cair ?>"
						 onclick="showCalendar('',this,this,'','dgirocair',0,20,1)">
						 <input type="hidden" name="ibank" id="ibank" value="<?php echo $isi->i_bank ?>">
						 <input type="hidden" name="icoa" id="icoa" value="<?php echo $isi->i_coa ?>">
						 <input name="ebankname" id="ebankname" value="<?php echo $isi->e_bank_name ?>" 
						 onclick='showModal("girocair/cform/bank/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area ?>">
						<input readonly name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name ?>" 
							   onclick='showModal("giro/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		<td width="12%">Bank</td>
		<td width="1%">:</td>
		<td width="37%"><input name="egirobank" id="egirobank" value="<?php echo $isi->e_giro_bank ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="icustomer" id="icustomer" value="<?php echo $isi->i_customer ?>">
						<input readonly name="ecustomername" id="ecustomername" value="<?php echo $isi->e_customer_name ?>"
						 onclick='showModal("giro/cform/customer/"+document.getElementById("iarea").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="egirodescription" id="egirodescription" value="<?php echo $isi->e_giro_description ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="37%"><input <?php if($isi->f_giro_use=='t') echo "readonly"; ?> style="text-align:right;" name="vjumlah" id="vjumlah" value="<?php echo number_format($isi->v_jumlah) ?>" onkeyup="reformat(this);sama(this.value);"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value="<?php echo number_format($isi->v_sisa) ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Tolak</td>
		<td width="1%">:</td>
		<td width="37%"><?php if($isi->f_giro_tolak=='t'){?>
						<input name="fgirotolak" id="fgirotolak" value="on" type="checkbox" checked >
						<?php }else{?>
						<input name="fgirotolak" id="fgirotolak" value="" type="checkbox" >
						<?php }?>
						<input readonly name="dgirotolak" id="dgirotolak" value="<?php echo $isi->d_giro_tolak ?>"
						 onclick="showCalendar('',this,this,'','dgirotolak',0,20,1)">
						<input name="fgirouse" id="fgirouse" value="<?php echo $isi->f_giro_use; ?>" type="hidden"></td>
		<td width="12%">Batal</td>
		<td width="1%">:</td>
		<?php if($isi->f_giro_batal=='t'){?>
		<td width="37%"><input name="fgirobatal" id="fgirobatal" value="on" type="checkbox" checked ></td>
		<?php }else{?>
		<td width="37%"><input name="fgirobatal" id="fgirobatal" value="" type="checkbox" ></td>
		<?php }?>
	      </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <?php if($iarea!='NA'){?>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('girocair/cform/view/<?php echo $dfrom."/".$dto."/NA/"; ?>','#main')">
		<?php }else{ ?>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('girocair/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>','#main')">
		<?php } ?>
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>

			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:760px;">
					<th style="width:30px;" align="center">No</th>
					<th style="width:120px;" align="center">Nota</th>
					<th style="width:80px;" align="center">Tgl Nota</th>
					<th style="width:130px;" align="center">Nilai</th>
					<th style="width:130px;" align="center">Bayar</th>
					<th style="width:130px;" align="center">Sisa</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 
					$i=0;
					if(count($detail)>0 && $detail!=''){
					foreach($detail as $row){ 
					$i++;
					  if($row->d_nota!=''){
						  $tmp=explode('-',$row->d_nota);
						  $tgl=$tmp[2];
						  $bln=$tmp[1];
						  $thn=$tmp[0];
						  $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
					  }
				?><table id="itemtem" class="listtable" style="width:760px;">
				<tbody>
				<tr>
				<td style="width:32px;"><input style="width:32px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>"></td>
				<td style="width:132px;"><input style="width:132px;" readonly type="text" id="inota<?php echo $i; ?>" name="inota<?php echo $i; ?>" value="<?php echo $row->i_nota; ?>"></td>
				<td style="width:87px;"><input style="width:87px;" readonly type="text" id="dnota<?php echo $i; ?>" name="dnota<?php echo $i; ?>" value="<?php echo $row->d_nota; ?>"></td>
				<td style="width:144px;"><input readonly style="width:144px;text-align:right;" type="text" id="vnota<?php echo $i; ?>" name="vnota<?php echo $i; ?>" value="<?php echo number_format($row->v_nota); ?>"></td>
				<td style="width:144px;"><input style="width:144px;text-align:right;" type="text" id="vjumlah<?php echo $i; ?>" name="vjumlah<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>" onkeyup="reformat(this);hetangs(<?php echo $i; ?>);"></td>
				<td style="width:144px;"><input type="hidden" id="vsesa<?php echo $i; ?>" name="vsesa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa); ?>"><input readonly style="width:144px;text-align:right;" type="text" id="vsisa<?php echo $i; ?>" name="vsisa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa); ?>">
				<input type="hidden" id="vlebih<?php echo $i; ?>" name="vlebih<?php echo $i; ?>" value=""><input type="hidden" id="vasal<?php echo $i; ?>" name="vasal<?php echo $i; ?>" value="<?php echo $row->v_jumlah+$row->v_sisa; ?>"></td>
				</tr></tbody></table>
				<?php }}?>
			</div>
				  
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script type="text-javascript">
  function sama(a)
  {
	if(document.getElementById("fgirouse").value!='t'){
		document.getElementById("vsisa").value=a;
	}
  }
  function dipales(){
  	cek='false';
  	if(
  	  (document.getElementById("igiro").value!='') &&
 	    (document.getElementById("dgiroduedate").value!='') &&
 	    (document.getElementById("ibank").value!='')
 	    ) {
		cek='true';	
	} 
	if(cek=='true'){
  		document.getElementById("login").disabled=true;
    	}
  }
</script>
