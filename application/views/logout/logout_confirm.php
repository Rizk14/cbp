<?php 
$this->load->view('header');
?>
<center><h2><?php echo $page_title;?></h2><br />
<p><a class="tekslogout"><?php echo $this->lang->line('login_logout_confirm');?></a></p>
<ul id="logout_list">
	<a href="<?php echo site_url().'/logout/logout_routine'; ?>" class="lbAction" rel="deactivate"><?php echo $this->lang->line('login_logout');?></a>
	<a href="<?php echo site_url()?>/main" class="lbAction" rel="deactivate"><?php echo $this->lang->line('actions_cancel');?></a>
</ul>
<?php 
$this->load->view('footer');
?>
