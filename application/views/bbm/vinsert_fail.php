<?php 
$this->load->view('header');
?>
<h2>
<?php 
echo $page_title;
?></h2>
<p class="error">
<?php 
echo $this->lang->line('bbm_wrong_input');
?></p>
<?php 
$this->load->view('bbm/vform');
$this->load->view('footer');
?>
