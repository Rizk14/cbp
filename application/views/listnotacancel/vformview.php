<style>
	.container {
		position: relative;
		overflow: scroll;
		height: 500px;
	}
</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tablefixheader.css" />

<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'listnotacancel/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<div class="container">
							<table class="listtable" id="sitabel">
								<thead class="sticky-head">
									<tr>
										<td colspan="10" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="<?= $cari ?>">
											<input readonly type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
											<input readonly type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
											<input readonly type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;
											<input type="submit" id="bcari" name="bcari" value="Cari">&nbsp;
											<input name="cmdexp" id="cmdexp" style="cursor: pointer;" value="Export to Excel" type="button">
										</td>
									</tr>

									<th>No</th>
									<th>No Nota</th>
									<th>Tgl Nota</th>
									<th>Customer</th>
									<th>Nilai Asal</th>
									<th>Nilai Revisi</th>
									<th>Tgl Batal</th>
									<th>Tgl Revisi</th>
									<th>Alasan</th>
								</thead>

								<tbody>
									<?php
									if ($isi) {
										$i = 0;
										foreach ($isi as $row) {
											$i++;

											$discount 		= number_format($row->v_nota_discounttotal);
											$nilai 			= number_format($row->v_nota_netto);
											$nilairev 		= number_format($row->v_notarev);
											$sisa 			= number_format($row->v_sisa);

											$tmp 			= explode('-', $row->d_nota);
											$tgl 			= $tmp[2];
											$bln 			= $tmp[1];
											$thn 			= $tmp[0];
											$row->d_nota 	= $tgl . '-' . $bln . '-' . $thn;

											$tmp 			= explode('-', $row->d_spb);
											$tgl 			= $tmp[2];
											$bln 			= $tmp[1];
											$thn 			= $tmp[0];
											$row->d_spb 	= $tgl . '-' . $bln . '-' . $thn;

											if ($row->f_nota_cancel == 't') {
												$tglbatal 	= $row->d_nota_update;
												$tglkoreksi 	= '';
											} else {
												$tglbatal = '';
												$tglkoreksi 	= $row->d_koreksi;
											}

											echo "<tr>
												<td>$i</td>
												<td>$row->i_nota</td>
												<td>$row->d_nota</td>
												<td>($row->i_customer) $row->e_customer_name</td>";

											if ($row->n_koreksi > 0) {
												echo " 	<td align=right>$nilairev</td>
													<td align=right>$nilai</td>";
											} else {
												echo " 	<td align=right>$nilai</td>
													<td align=right>0</td>";
											}

											echo "	<td>$tglbatal</td>
													<td>$tglkoreksi</td>
													<td>$row->e_alasan</td>
											</tr>";
										}
										echo "	<input type=\"hidden\" id=\"ispbdelete\" name=\"ispbdelete\" value=\"\">
											<input type=\"hidden\" id=\"ispbedit\" name=\"ispbedit\" value=\"\">
											<input type=\"hidden\" id=\"inotadelete\" name=\"inotadelete\" value=\"\">
											<input type=\"hidden\" id=\"inotaedit\" name=\"inotaedit\" value=\"\">";
									}
									?>
								</tbody>
							</table>
							<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
						</div>
					</div>
				</div>
				<?= form_close() ?>
				<!-- <input name="cmdexp" id="cmdexp" style="cursor: pointer;" value="Export to Excel" type="button"> -->
				<center><input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('listnotacancel/cform/','#main')"></center>
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
	function xxx(x, a, g) {
		if (confirm(g) == 1) {
			document.getElementById("ispbdelete").value = a;
			document.getElementById("inotadelete").value = x;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/listnotacancel/cform/delete";
			formna.submit();
		}
	}

	function yyy(x, b) {
		document.getElementById("ispbedit").value = b;
		document.getElementById("inotaedit").value = x;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/nota/cform/edit";
		formna.submit();
	}

	$("#cmdexp").click(function() {
		const dfrom = '<?= $dfrom; ?>';
		const dto = '<?= $dto; ?>';

		//getting values of current time for generating the file name
		var dt = new Date();
		var day = dt.getDate();
		var month = dt.getMonth() + 1;
		var year = dt.getFullYear();
		var hour = dt.getHours();
		var mins = dt.getMinutes();
		var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
		//creating a temporary HTML link element (they support setting file names)
		var a = document.createElement('a');
		//getting data from our div that contains the HTML table
		var data_type = 'data:application/vnd.ms-excel';
		var table_div = $('#sitabel').html();
		// var table_html = table_div.outerHTML.replace(/ /g, '%20');
		a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

		//setting the file name
		// var nofaktur = document.getElementById('no_faktur').value;
		a.download = 'list_nota_batal_' + dfrom + "_" + dto + "_" + postfix + '.xls';
		//triggering the function
		a.click();
		//just in case, prevent default behaviour

		// var Contents = $('#sitabel').html();
		// window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
	});
</script>