<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'exp-akt-bkinalloc/cform/view','update'=>'#main','type'=>'post'));?>
			<div class="effect">
				<div class="accordion2">
					<br>
					<div align="left">
						<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">&nbsp;&nbsp;&nbsp;
						<input name="kembali" id="kembali" value="Kembali" type="button" onclick='show("exp-akt-bkinalloc/cform/index","#main");' >&nbsp;&nbsp;&nbsp;
					</div><br>
					<table class="listtable" id="sitabel">
						<th>No</th>
						<th>No BM</th>
						<th>Area</th>
						<th>Tgl BM</th>
						<th>Nilai BM</th>
						<th>Alokasi</th>
						<th>Tgl Alokasi</th>
						<th>Nota</th>
						<th>Kode Customer</th>
						<th>Nama Customer</th>
						<th>Kd Sls</th>
						<th>Tgl Nota</th>
						<th>Jumlah Alokasi</th>
						<th>Jumlah Nota</th>
						<th>Sisa</th>
						<th>Tgl Pelunasan</th>
						<th>Alasan</th>
						<th>No Bukti</th>
						<th>Bank</th>
						<th>Hari</th>
						<th>Hari ke Tempo</th>
						<tbody>
							<?php 
							$no = 1;
							foreach ($isi as $row) {
								echo "<tr>
								<td>".$no."</td>
								<td>".$row->i_kbank."</td>
								<td>".$row->i_area."</td>
								<td>".$row->d_bank."</td>
								<td>".$row->jum_bm."</td>
								<td>".$row->i_alokasi."</td>
								<td>".$row->d_alokasi."</td>
								<td>".$row->i_nota."</td>
								<td>".$row->i_customer."</td>
								<td>".$row->e_customer_name."</td>
								<td>".$row->i_salesman."</td>
								<td>".$row->d_nota."</td>
								<td align='right'>".number_format($row->jum_alokasi)."</td>
								<td align='right'>".number_format($row->jum_nota)."</td>
								<td align='right'>".number_format($row->sisa)."</td>
								<td align='right'>".substr($row->d_entry, 0, 10)."</td>
								<td>".$row->e_remark."</td>
								<td>".$row->i_giro."</td>
								<td>".$row->e_bank_name."</td>
								<td>".$row->hari_bayar."</td>
								<td>".$row->hari2."</td>
								</tr>";
								$no++;
							}
							?>
						</tbody>
					</table>
					<br>
					<div align="left">
						<input name="cmdreset2" id="cmdreset2" value="Export to Excel" type="button">&nbsp;&nbsp;&nbsp;
						<input name="kembali" id="kembali" value="Kembali" type="button" onclick='show("exp-akt-bkinalloc/cform/index","#main");' >&nbsp;&nbsp;&nbsp;
					</div>
				</div>
			</div>
			<?=form_close()?>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	$( "#cmdreset" ).click(function() {  
		var Contents = $('#sitabel').html();    
		window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
	});
</script>

