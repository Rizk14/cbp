<?php 
include ("php/fungsi.php");
?>
<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('akt-nota-un/cform/unposting', array('id' => 'spbformupdate', 'name' => 'spbformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php foreach($isi as $isi){?>
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Nota</td>
		<td><?php 	if($isi->i_nota!='') {
					echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
										   readonly>"; 
				}	
				if($isi->d_nota!=''){				
					$tmp=explode("-",$isi->d_nota);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dnota=$hr."-".$bl."-".$th;
					echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\">";
				}else{
			?>
			<input readonly id="dnota" name="dnota" value="">
			<?php }?>
			</td>			
		<td>Keterangan</td>
		<td><input readonly id="eremark" name="eremark" value="<?php echo $isi->e_remark; ?>"></td>
	      </tr>
	      <tr>
		<td>SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
			<input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
		<td>Nilai Kotor</td>
		<?php 
			$enin=number_format($isi->v_nota_gross);
		?>	
		<td><input id="vspb" name="vspb" readonly value="<?php echo $enin; ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1" value="<?php echo $isi->n_nota_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_nota_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2" value="<?php echo $isi->n_nota_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="<?php echo number_format($isi->v_nota_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3" value="<?php echo $isi->n_nota_discount3; ?>"> 
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="<?php echo number_format($isi->v_nota_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox" disabled=true
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>&nbsp;Masalah&nbsp;
			<input id="fmasalah" name="fmasalah" type="checkbox" value="" disabled=true>&nbsp;Insentif&nbsp;
			<input id="finsentif" name="finsentif" type="checkbox" value="on" checked disabled=true>&nbsp;Cicil&nbsp;
			<input disabled=true id="fcicil" name="fcicil" type="checkbox" value="on" <?php if($isi->f_customer_cicil=='t') echo "checked";?>></td>
		<td>Discount 4</td>
		<td><input readonly id="ncustomerdiscount4" name="ncustomerdiscount4" value="<?php echo $isi->n_nota_discount4; ?>">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" value="<?php echo number_format($isi->v_nota_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td>Insentif</td>
		<td><input id="finsentif" name="finsentif" type="checkbox" value="on" checked disabled=true></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal" value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>

	      <tr>
		<td>TOP</td>
		<?php 
			$tmp = explode("-", $isi->d_spb);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dspb	= $yir."/".$mon."/".$det;
			$dudet	=dateAdd("d",$isi->n_nota_toplength,$dspb);
			$dudet 	= explode("-", $dudet);
			$det1	= $dudet[2];
			$mon1	= $dudet[1];
			$yir1 	= $dudet[0];
			$dudet	= $det1."-".$mon1."-".$yir1;
		?>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly value="<?php echo $isi->n_nota_toplength; ?>">
			&nbsp;&nbsp;Jatuh tempo&nbsp;
			<input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $dudet; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_nota_netto;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" readonly value="<?php echo number_format($isi->v_spb-$isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly 
				   value="<?php echo number_format($isi->v_nota_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>Stok Daerah</td>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly>
			<input readonly readonly id="dsj" name="dsj"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input id="vspbafter" name="vspbafter" readonly value="<?php echo number_format($isi->v_nota_netto); ?>">
			 <input type="hidden" id="fspbplusppn" name="fspbplusppn" value="<?php echo $isi->f_spb_plusppn;?>">
			 <input type="hidden" id="fspbplusdiscount" name="fspbplusdiscount" value="<?php echo $isi->f_spb_plusdiscount;?>">
			 <input type="hidden" id="nprice" name="nprice" value="<?php echo $isi->n_price;?>">
			 <input type="hidden" id="vnotappn" name="vnotappn" value="<?php echo $isi->v_nota_ppn;?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="UnPosting" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("akt-nota-un/cform/","#tmp")'></td>
		</tr>
	    </table>
		<?php }?>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:100px;"  align="center">Harga</th>
					<th style="width:60px;"  align="center">Jml Dlv</th>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$harga	=number_format($row->v_unit_price);
					$ndeliv	=number_format($row->n_deliver);
				  	echo "<tbody>
							<tr>
		    				<td>
								<input style=\"width:25px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td><input style=\"width:63px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td><input style=\"width:364px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td><input style=\"width:100px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td><input style=\"text-align:right; width:100px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>";
					echo "	<td><input style=\"text-align:right; width:60px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\"></td>
							</tr>
						  </tbody>";
				}
				?>
			</div>
			</table>
	  </div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
  	if(document.getElementById("dnota").value!=''){
   		document.getElementById("login").disabled=true;
	}else{
		document.getElementById("login").disabled=false;
		alert("Tanggal nota tidak boleh kosong");
	}
  }
  window.onload=function(){
	var jml 	= parseFloat(document.getElementById("jml").value);
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var ndis4	= parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	var vdis4	= 0;
	for(i=1;i<=jml;i++){
	  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
		hrg			= hrg+hrgtmp;
	}
	hrg=Math.round(hrg);
	vdis1=Math.round(vdis1+((hrg*ndis1)/100));
	vdis2=Math.round(vdis2+(((hrg-vdis1)*ndis2)/100));
	vdis3=Math.round(vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100));
	vdis4=Math.round(vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100));
	vdistot	= Math.round(vdis1+vdis2+vdis3+vdis4);
	vhrgreal= Math.round(hrg-vdistot);
	document.getElementById("vspbdiscounttotalafter").value=formatcemua(vdistot);
	document.getElementById("vspbafter").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("f_spb_plusppn").value;
	var fdis = document.getElementById("f_spb_plusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vspbafter").value));
	var kotor  = parseFloat(formatulang(document.getElementById("vspb").value));
	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("n_price").value=1;
		document.getElementById("v_nota_ppn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("n_price").value=bersih/kotor;
		document.getElementById("v_nota_ppn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("n_price").value=1/1.1;
		document.getElementById("v_nota_ppn").value=bersih*0.1;
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("n_price").value=(bersih/kotor)/1.1;
		document.getElementById("v_nota_ppn").value=bersih*0.1;
	}
  }
</script>
