<?php 
$this->load->view('header');
?>
<h2>
<?php 
echo $page_title;
?></h2>
<p class="error">
<?php 
echo $this->lang->line('mastercountry_wrong_input');
?></p>
<?php 
$this->load->view('country/vform');
$this->load->view('footer');
?>
