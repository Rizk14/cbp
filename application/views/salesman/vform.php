<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>

<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Salesman</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Salesman</a></li>  
	  </ul>  
  <div id="content_1" class="content">
  <table class="maintable" >
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'salesman/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>'salesman/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	       <thead>
	            <tr>
				
		            <td colspan="8" align="center">
					<input style="cursor: pointer; position:left" name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
					<!-- <input style="cursor: pointer; position:left" name="login" id="login" value="Export to Excel" type="submit"> -->
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					  &nbsp;&nbsp;&nbsp;
						Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input style="cursor: pointer;" type="submit" id="bcari" name="bcari" value="Cari">		
				    </td>
	            </tr>
	       </thead>
 	    <th>Kode Salesman</th>
	    <th>Nama Salesman</th>
	    <th>Area</th>
		<th>Status</th>
	    <th>Alamat</th>
	    <th>Kota</th>
	    <th>Tgl.Daftar</th>	
	    <th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			if($row->d_salesman_entry!='') {
				$tmpentry=explode('-',$row->d_salesman_entry);
				$tglentry=$tmpentry[2];
				$blnentry=$tmpentry[1];
				$thnentry=$tmpentry[0];
				$dsalesmanentry=$tglentry.'-'.$blnentry.'-'.$thnentry;
			} else {
				$dsalesmanentry='';
			}
			if($row->f_salesman_aktif == 't'){
				$aktif = "AKTIF";
			}else{
				$aktif = "NON-AKTIF";
			}
			  echo "<tr> 
				  <td>$row->i_salesman</td>
				  <td>$row->e_salesman_name</td>
				  <td>$row->e_area_name</td>
				  <td>$aktif</td>
				  <td>$row->e_salesman_address</td>
				  <td>$row->e_salesman_city</td>
				  <td align=center>$dsalesmanentry</td>";
			  echo "<td class=\"action\"><a href=\"#\" onclick='show(\"salesman/cform/edit/$row->i_salesman/$row->i_area\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";#"&nbsp;&nbsp;";
#			  echo "<a href=\"#\" onclick='hapus(\"salesman/cform/delete/$row->i_salesman/$row->i_area\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <table class="listtable" style="width:100%;" id="sitabel" hidden="true">
	    <thead>
	      	<!-- <tr>
				<td colspan="7" align="center" >Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      	</tr> -->
		  	<tr>
		  		<th style="width:3%;">Kode</th>
	    		<th style="width:15%">Nama</th>
	    		<th style="width:15%">Area</th>
				<th style="width:15%">Status</th>
	    		<th style="width:25%">Alamat</th>
	    		<th style="width:10%">Kota</th>
	    		<th style="width:5%;">Tgl.Daftar</th>	
			</tr>
	    </thead>
 	    
	    <tbody class="font">
	      <?php 
		if($isi){
			foreach($isi as $row){
			if($row->d_salesman_entry!='') {
				$tmpentry=explode('-',$row->d_salesman_entry);
				$tglentry=$tmpentry[2];
				$blnentry=$tmpentry[1];
				$thnentry=$tmpentry[0];
				$dsalesmanentry=$tglentry.'-'.$blnentry.'-'.$thnentry;
			} else {
				$dsalesmanentry='';
			}
			if($row->f_salesman_aktif == 't'){
				$aktif = "AKTIF";
			}else{
				$aktif = "NON-AKTIF";
			}
			  echo "<tr> 
				  <td style='font-size:10px;'>'$row->i_salesman</td>
				  <td style='font-size:10px;'>$row->e_salesman_name</td>
				  <td style='font-size:10px;'>$row->e_area_name</td>
				  <td style='font-size:10px;'>$aktif</td>
				  <td style='font-size:10px;'>$row->e_salesman_address</td>
				  <td style='font-size:10px;'>$row->e_salesman_city</td>
				  <td style='font-size:10px;' align=center>$dsalesmanentry</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
  </table>
  </div>
  <div id="content_2" class="content">
  <table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'salesman/cform/simpan','update'=>'#main','type'=>'post'));?>
    <div id="mastersalesmanform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Kode Salesman</td>
		<td width="1%">:</td>
		<td width="37%"><?php 
				$data = array(
			              'name'        => 'isalesman',
			              'id'          => 'isalesman',
			              'value'       => '',
			              'maxlength'   => '2');
				echo form_input($data);?></td>
		<td width="12%">Nama salesman</td>
		<td width="1%">:</td>
		<td width="37%"><input type="text" name="esalesmanname" id="esalesmanname" maxlength="50" value="" onkeyup="gede(this)">
        <?php 
				/*$data = array(
			              'name'        => 'esalesmanname',
			              'id'          => 'esalesmanname',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?></td>
	      </tr>
	      <tr>
		<td width="12%">Nama Area</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="eareaname" id="eareaname" value="" 
				 onclick='showModal("salesman/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" name="iarea" id="iarea" value="" 
				 onclick='showModal("salesman/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		<td width="12%">Alamat</td>
		<td width="1%">:</td>
		<td width="37%"><input type="text" name="esalesmanaddress" id="esalesmanaddress" maxlength="50" value="" onkeyup="gede(this)">
        <?php 
				/*$data = array(
			              'name'        => 'esalesmanaddress',
			              'id'          => 'esalesmanaddress',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?></td>
	      </tr>
	      <tr>
		<td width="12%">Kota</td>
		<td width="1%">:</td>
		<td width="37%"><input type="text" name="esalesmancity" id="esalesmancity" maxlength="50" value="" onkeyup="gede(this)">
        <?php 
				/*$data = array(
			              'name'        => 'esalesmancity',
			              'id'          => 'esalesmancity',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);*/ ?></td>
	        <td width="12%">Kode Pos</td>
		<td width="1%">:</td>
		<td width="37%"><?php 
				$data = array(
			              'name'        => 'esalesmanpostal',
			              'id'          => 'esalesmanpostal',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
  		<td width="12%">Telepon</td>
		<td width="1%">:</td>
		<td colspan="4" width="87%"><?php 
				$data = array(
			              'name'        => 'esalesmanphone',
			              'id'          => 'esalesmanphone',
			              'value'       => '',
			              'maxlength'   => '50');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td align="center" colspan="6">
		  <center><input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('salesman/cform/','#main');"></center>
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
  </table>
  </div>
</div>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>

