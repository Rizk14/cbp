<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/FusionCharts.js"></script>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyklasifikasimarketnew/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <?php 
          if($isi){
            $totob=0;
            $totoaprev=0;
            $totqtyprev=0;
            $totqty=0;
            $totoa=0;
            $totvnotaprev=0;
            $totvnota=0;

            foreach($isi as $ro){
              $totob=$totob+$ro->ob;
              $totoaprev=$totoaprev+$ro->oaprev;
              $totoa=$totoa+$ro->oa;
              $totvnotaprev=$totvnotaprev+$ro->vnotaprev;
              $totvnota=$totvnota+$ro->vnota;
              $totqty=$totqty+$ro->qnota;
              $totqtyprev=$totqtyprev+$ro->qnotaprev;
              if($group!="NA"){
              //$group = $ro->group;
              $group = $this->db->query("select e_product_groupname from tr_product_group where i_product_group = '$ro->i_product_group'")->row()->e_product_groupname;
              // $group = $ro->i_product_group;
              }else{
                $group='NA';
              }
            }
          }

        ?>
        <table class="mastertable">
         <tr>
            <td style="width:100px;" align ="right"><?php if($group =='NA'){echo 'NASIONAL';}else{echo $group;} ?></td>
            <td style="width:5px;"></td>
            <td style="width:500px; text-align:left;"><?php echo 'Periode:'.$dfrom.' s/d '.$dto; ?>
            <td colspan=2></td>
            <td style="width:5px;"></td>
            </td>
            <td>
            </td>
         </tr>
        </table>
        <table class="listtable">
         <tr>
          <td>
            <?php 
              if($ob){
                foreach ($ob as $riw) {
                  #echo "<h1>Total OB : ".number_format($riw->ob)."</h1>";
                }
              }
            ?>
          </td>
         </tr>
       </table>
        <table class="listtable">
         <tr>
         <th rowspan=3>No</th>
         <th rowspan=3>Klasifikasi Outlet</th>
         <th rowspan=2>OB</th>
         <th colspan=3>OA</th>
         <th colspan=3>Qty Sales</th>
         <th colspan=3>Net Sales</th>
         <th rowspan=2>% CTR</th>
         </tr>
         <!--<tr>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustlm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totoaprev); ?></th>
         <th></th>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totcustcm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totoa); ?></th>
         <th></th>
         <th colspan=2>MTD</th>
         <th colspan=2>YTD</th>
         </tr>-->
<?php 
        $tmp=explode("-",$dfrom);
        $thlalu=$tmp[2]-1;
        $th=$tmp[2];
?>
         <tr>
         <th><?php echo $thlalu; ?></th>
         <th><?php echo $th; ?></th>
         <th>%</th>
         <th><?php echo $thlalu; ?></th>
         <th><?php echo $th; ?></th>
         <th>%</th>
         <th><?php echo $thlalu; ?></th>
         <th><?php echo $th; ?></th>
         <th>%</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totnota=0;
        $totpersenoaprev=0;
        $totpersenoa=0;
        $totctrrp=0;


        foreach($isi as $rowa){
            $totnota+=$rowa->vnota;
        }
        $totrp=$totnota;

        foreach($isi as $row){
          $i++;
          $growthnota=0;
          $growthoa=0;
          $growthqty=0;
          $qtyly = $row->qnota-$row->qnotaprev;
          $spbsely=$row->vnota-$row->vnotaprev;
          $custsely=$row->oa-$row->oaprev;
          ####################################################
          $growthytdval=$totvnota/$totvnotaprev*100-100;
          $growthytdoa=$totoa/$totoaprev*100-100;
          $growthytdqty=$totqty/$totqtyprev*100-100;
          $ctrrp= ($row->vnota/$totrp)*100;
          $totctrrp= $totctrrp+$ctrrp;

          if($row->vnotaprev==0){
            $growthnota=0;
          }else{
            $growthnota=($spbsely/$row->vnotaprev)*100;
          }
          if($row->oaprev==0){
            $growthoa=0;
          }else{
            $growthoa=($custsely/$row->oaprev)*100;
          }
          if($row->qnotaprev==0){
            $growthqty=0;
          }else{
            $growthqty=($qtyly/$row->qnotaprev)*100;
          }
           //
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->e_customer_classname</td>
              <td style='font-size:12px;' align=right>".number_format($row->ob)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthoa,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthqty)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($growthnota,2)." %</td>

              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
         }
        $growthytdval=$totvnota/$totvnotaprev*100-100;
        $growthytdoa=$totoa/$totoaprev*100-100;
        $growthytdqty=$totqty/$totqtyprev*100-100;
        //
        echo "<tr>
        <td style='font-size:12px;' colspan=2><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totob)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdoa,2)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totqtyprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqty)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdqty)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totvnotaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdval,2)." %</b></td>

        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>";
      }
         ?>
       </tbody>
     </table>
     <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('ctrbyklasifikasimarketnew/cform/','#main')">
     <?php     
      echo $graph;

   //   for ($i=0; $i<=$n; $i++) {
       
    //  }
     ?>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
