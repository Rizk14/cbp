<?php 
   include ("php/fungsi.php");
   require_once("printipp/PrintIPP.php");
   $isi=$master;
   foreach($isi as $row){
    $cetak='';
      $nor  = str_repeat(" ",5);
      $abn  = str_repeat(" ",12);
      $ab   = str_repeat(" ",9);
      $ipp  = new PrintIPP();
      $ipp->setHost($host);
      $ipp->setPrinterURI($uri);
      $ipp->setRawText();
      $ipp->unsetFormFeed();
      $cetak.=CHR(18);
      $alm  = strlen(trim($row->e_customer_address));
      $cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).NmPerusahaan.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."                  Kepada Yth.\n";
      $cetak.=$nor.AlmtPerusahaan.",".KotaPerusahaan."                 ".rtrim($row->e_customer_name)."\n";
      if($alm<35){
         $cetak.=$nor."Telp.: ".TlpPerusahaan."                         ".trim($row->e_customer_address)."\n";
      }else{
         $cetak.=$nor."Telp.: ".TlpPerusahaan."                         ".CHR(15).trim($row->e_customer_address).CHR(18)."\n";
      }
      $cetak.=$nor."Fax  : ".FaxPerusahaan."                         ".rtrim($row->e_customer_city)."\n";
      if($row->f_customer_pkp=='t'){
         $cetak.=$nor."NPWP : ".NPWPPerusahaan."                 NPWP : ".$row->e_customer_pkpnpwp."\n";
      }else{
         $cetak.=$nor."NPWP : ".NPWPPerusahaan."                 "."\n";
      }
      $cetak.=$nor.CHR(27).CHR(71)."BCA CABANG CIMAHI - BANDUNG\n";
      $cetak.=$nor."NO.AC: 139.300.1236".CHR(27).CHR(72)."\n\n";
      $cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14)."             NOTA PENJUALAN".CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n\n";

      $pjgpo  = strlen(trim($row->i_spb_po));
      $pjgpo  = 18-$pjgpo;

      $cetak.=$nor."NO PO:".trim($row->i_spb_po).str_repeat(" ",$pjgpo)."No.FAK. / No.SJ     : ".trim(substr($row->i_nota,8,7))."/".substr($row->i_sj,8,6)."\n";
      $cetak.=$nor.str_repeat(" ",24)."KODE SALES/KODELANG : ".$row->i_salesman."/".$row->i_customer."\n";
      #$xxx=datediff('d',$row->d_nota,$row->d_jatuh_tempo,false);
      $xxx=$row->n_customer_toplength_print;
      if(($xxx)>0){
         $cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN     : ".$xxx." hari SETELAH BARANG DITERIMA\n";
      }else{
         $cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN     : "."TUNAI\n";
      }
#      $cetak.="\n";
      $cetak.=$nor.CHR(218).str_repeat(CHR(196),77).CHR(191)."\n";
      $cetak.=$nor.CHR(179)."No.  KD-BARANG       NAMA BARANG                     UNIT   HARGA     JUMLAH ".CHR(179)."\n";
      $cetak.=$nor.CHR(212).str_repeat(CHR(205),77).CHR(190).CHR(15)."\n";
      $j=0;
      $i=0;
      $query   = $this->db->query(" select * from tm_nota_item where i_nota='$row->i_nota'",false);
      $jml  = $query->num_rows();
      $detail  = $this->mmaster->bacadetail($row->i_nota);
      //$this->mmaster->updatenota($row->i_nota);
      $total=0;
      foreach($detail as $rowi){
         if($rowi->n_deliver>0){
            $i++;
            $j++;
        $group='';
        $plu='';
         $qu   = $this->db->query(" select i_customer_plugroup from tr_customer_plugroup
                                 where i_customer='$row->i_customer'");
        if($qu->num_rows()>0){
          foreach($qu->result() as $ts){
            $group=$ts->i_customer_plugroup;
          }
          $qx  = $this->db->query("select i_customer_plu from tr_customer_plu
                                  where i_customer_plugroup='$group' and i_product='$rowi->i_product'");
          if($qx->num_rows()>0){
            foreach($qx->result() as $tx){
              $plu=$tx->i_customer_plu;
            }
          }
        }else{
          $plu='';
        }
            $prod = $rowi->i_product;
#        if($plu!=''){
#          $prod=$plu;
#          if(strlen($prod)>10){
#               $prod   = substr($prod,0,10);
#            }else{
#               $prod   = $prod.str_repeat(" ",10-strlen($prod));
#            }
#        }
        if($plu!=''){
             if(strlen($plu.' - '.$rowi->e_product_name )>66){
                $name   = substr($plu.' - '.$rowi->e_product_name,0,66);
             }else{
                $name   = $plu.' - '.$rowi->e_product_name.str_repeat(" ",66-strlen($plu.' - '.$rowi->e_product_name ));
             }
        }else{
             if(strlen($rowi->e_product_name )>66){
                $name   = substr($rowi->e_product_name,0,66);
             }else{
                $name   = $rowi->e_product_name.str_repeat(" ",66-strlen($rowi->e_product_name ));
             }
        }
#           $name = $rowi->e_product_name." ".str_repeat(".",66-strlen($rowi->e_product_name ));
            $deli = number_format($rowi->n_deliver);
            $pjg  = strlen($deli);
            $spcdel  = 7;

            for($xx=1;$xx<=$pjg;$xx++){
               $spcdel  = $spcdel-1;
            }
            if($row->f_plus_ppn=='t'){
               $pric = number_format($rowi->v_unit_price);
            }else{
               $pric = number_format($rowi->v_unit_price/1.1);
            }
            $pjg  = strlen($pric);
            $spcpric= 18;
            for($xx=1;$xx<=$pjg;$xx++){
               $spcpric= $spcpric-1;
            }
            if($row->f_plus_ppn=='t'){
               $tot  = $rowi->n_deliver*$rowi->v_unit_price;
          $totx   = number_format($rowi->n_deliver*$rowi->v_unit_price);
            }else{
               $tot  = $rowi->n_deliver*($rowi->v_unit_price/1.1);
          $totx   = number_format($rowi->n_deliver*($rowi->v_unit_price/1.1));
            }
            $pjg  = strlen($totx);
            $spctot = 19;
            for($xx=1;$xx<=$pjg;$xx++){
               $spctot  = $spctot-1;
            }
            $aw=13;
            $pjg  = strlen($i);
            for($xx=1;$xx<=$pjg;$xx++){
               $aw=$aw-1;
            }
            $aw=str_repeat(" ",$aw);
#        $total   = $total+str_replace(',','',$tot);
        $total = $total+$tot;
            $cetak.=CHR(15).$aw.$i.str_repeat(" ",5).$prod.str_repeat(" ",5).$name.str_repeat(" ",$spcdel).$deli.str_repeat(" ",$spcpric).$pric.str_repeat(" ",$spctot).number_format($tot)."\n";
         }
      }
      $cetak.=CHR(18).$nor.str_repeat("-",78)."\n";
#    if($row->f_spb_consigment=='f' || $row->f_plus_ppn!='t'){
      $row->v_nota_gross=$total;
#    }
      if(strlen(number_format($row->v_nota_gross))==1){
         $cetak.=str_repeat(" ",51)."TOTAL        :                ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==2){
         $cetak.=str_repeat(" ",51)."TOTAL        :               ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==3){
         $cetak.=str_repeat(" ",51)."TOTAL        :              ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==4){
         $cetak.=str_repeat(" ",51)."TOTAL        :             ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==5){
         $cetak.=str_repeat(" ",51)."TOTAL        :            ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==6){
         $cetak.=str_repeat(" ",51)."TOTAL        :           ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==7){
         $cetak.=str_repeat(" ",51)."TOTAL        :          ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==8){
         $cetak.=str_repeat(" ",51)."TOTAL        :         ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==9){
         $cetak.=str_repeat(" ",51)."TOTAL        :        ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==10){
         $cetak.=str_repeat(" ",51)."TOTAL        :       ".number_format($row->v_nota_gross)."\n";
      }elseif(strlen(number_format($row->v_nota_gross))==11){
         $cetak.=str_repeat(" ",51)."TOTAL        :      ".number_format($row->v_nota_gross)."\n";
      }
      $vdisc1=0;
      $vdisc2=0;
      $vdisc3=0;
      $vdisc4=0;
      if( ($row->n_nota_discount1+$row->n_nota_discount3+$row->n_nota_discount3+$row->n_nota_discount4==0) && $row->v_nota_discounttotal <> 0 )
      {
        $vdisc1=$row->v_nota_discounttotal;
#        $vdisc2=0;
#        $vdisc3=0;
#        $vdisc4=0;
        $vdistot   = round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
        if( ($row->f_plus_ppn=='f') ){#&& ($row->n_nota_discount1==0) ){
          $vdistot=$vdistot/1.1;
        }
      }
      else
      {
        $vdisc1=($total*$row->n_nota_discount1)/100;
        $vdisc2=((($total-$vdisc1)*$row->n_nota_discount2)/100);
        $vdisc3=((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100);
        $vdisc4=((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100);
        $vdistot   = round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
      }
#      $vdistot   = round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
      if( ($row->f_plus_ppn=='f') ){#&& ($row->n_nota_discount1==0) ){
#        $vdistot=$vdistot/1.1;
      }
#    if($row->f_spb_consigment=='f'){
      $row->v_nota_discounttotal=$vdistot;
#    }else{
#      $row->v_nota_discounttotal=$row->v_nota_discounttotal/1.1;
#    }
      if(strlen(number_format($row->v_nota_discounttotal))==1){
         $cetak.=str_repeat(" ",51)."POTONGAN     :                ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==2){
         $cetak.=str_repeat(" ",51)."POTONGAN     :               ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==3){
         $cetak.=str_repeat(" ",51)."POTONGAN     :              ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==4){
         $cetak.=str_repeat(" ",51)."POTONGAN     :             ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==5){
         $cetak.=str_repeat(" ",51)."POTONGAN     :            ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==6){
         $cetak.=str_repeat(" ",51)."POTONGAN     :           ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==7){
         $cetak.=str_repeat(" ",51)."POTONGAN     :          ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==8){
         $cetak.=str_repeat(" ",51)."POTONGAN     :         ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==9){
         $cetak.=str_repeat(" ",51)."POTONGAN     :        ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==10){
         $cetak.=str_repeat(" ",51)."POTONGAN     :       ".number_format($row->v_nota_discounttotal)."\n";
      }elseif(strlen(number_format($row->v_nota_discounttotal))==11){
         $cetak.=str_repeat(" ",51)."POTONGAN     :      ".number_format($row->v_nota_discounttotal)."\n";
      }
      if($row->f_plus_ppn=='f'){
      $vppn=(round($total)-round($vdistot))*0.1;
      $row->v_nota_ppn=$vppn;
         if(strlen(number_format($row->v_nota_ppn))==1){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :                ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==2){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :               ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==3){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :              ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==4){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :             ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==5){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :            ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==6){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :           ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==7){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :          ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==8){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :         ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==9){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :        ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==10){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :       ".number_format($row->v_nota_ppn)."\n";
         }elseif(strlen(number_format($row->v_nota_ppn))==11){
            $cetak.=str_repeat(" ",51)."PPN (10%)    :      ".number_format($row->v_nota_ppn)."\n";
         }
      }
      $cetak.=str_repeat(" ",66).str_repeat("-",16).CHR(" ").CHR("-")."\n";
    if($row->f_plus_ppn=='f'){
      $row->v_nota_netto=round(round($row->v_nota_gross)-round($row->v_nota_discounttotal)+$vppn);
#      $row->v_nota_netto=round($row->v_nota_gross-$row->v_nota_discount+$vppn);
      if(strlen(number_format($row->v_nota_netto))==1){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :                ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==2){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :               ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==3){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :              ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==4){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :             ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==5){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :            ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==6){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :           ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==7){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :          ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==8){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :         ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==9){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :        ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==10){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :       ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==11){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :      ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }
    }else{
      $row->v_nota_netto=$row->v_nota_gross-$row->v_nota_discounttotal;
        if(strlen(number_format($row->v_nota_netto))==1){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :                ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==2){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :               ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==3){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :              ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==4){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :             ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==5){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :            ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==6){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :           ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==7){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :          ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==8){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :         ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==9){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :        ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==10){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :       ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }elseif(strlen(number_format($row->v_nota_netto))==11){
           $cetak.=str_repeat(" ",51)."NILAI FAKTUR :      ".number_format($row->v_nota_netto).CHR(15)."\n\n";
        }
    }
      $bilangan = new Terbilang;
      $kata=ucwords($bilangan->eja($row->v_nota_netto));
      $cetak.=$ab."(".$kata." Rupiah)".CHR(18)."\n";
      $tmp=explode("-",$row->d_nota);
      $th=$tmp[0];
      $bl=$tmp[1];
      $hr=$tmp[2];
      $dnota=$hr." ".mbulan($bl)." ".$th;
      $cetak.=str_repeat(" ",50)."Bandung, ".$dnota."\n";
      $cetak.=str_repeat(" ",3)."        Penerima                                     S E & O\n\n\n\n\n";
      $cetak.=str_repeat(" ",3)."    (              )                            ( ".TtdNota." )\n".CHR(15);
      $cetak.=$ab."Catatan :\n";
      $cetak.=$ab."1. Barang-barang yang sudah dibeli tidak dapat ditukar/dikembalikan, kecuali ada perjanjian terlebih dahulu\n";
      if($row->f_plus_ppn=='t'){
         $cetak.=$ab."2. Faktur asli merupakan bukti pembayaran yang sah. (Harga sudah termasuk PPN)\n";
      }else{
         $cetak.=$ab."2. Faktur asli merupakan bukti pembayaran yang sah.\n";
      }
      $cetak.=$ab."3. Pembayaran dengan cek/giro berharga baru dianggap sah setelah diuangkan/cair.\n";
    $cetak.=$ab."4. Pembayaran dapat ditransfer atas nama ".CHR(27).CHR(71).CHR(18).NmPerusahaan." ".CHR(27).CHR(72).CHR(15)."ke Rekening :".CHR(18)."\n";
    $cetak.=$ab.CHR(27).CHR(71).BCACimahi.CHR(27).CHR(72)."\n";
    $cetak.=$ab.CHR(27).CHR(71).BRIBandung.CHR(27).CHR(72)."\n";
    //$cetak.=$ab.CHR(27).CHR(71).PermataBandung.CHR(27).CHR(72)."\n".CHR(18);
    $ipp->setFormFeed();
    $ipp->setdata($cetak);
    $ipp->printJob();
   }
#  echo $cetak;
  echo "<script>this.close();</script>";
?>
