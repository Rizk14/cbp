<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'spmbnew/cform', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="spbform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
              <tr>
                <td width="10%">Tgl SPMB</td>
                <td><input readonly id="dspmb" name="dspmb" value="<?php echo $tgl; ?>" onclick="showCalendar('',this,this,'','dspmb',0,20,1)">
                  <input id="d_now" name="d_now" type="hidden" value="<?php echo date('Y-m-d'); ?>">
                  <input id="ispmbold" name="ispmbold" type="text">
                  <input id="ispmb" name="ispmb" type="hidden">
                </td>
              </tr>
              <tr>
                <td width="10%">Gudang</td>
                <td><input readonly id="eareaname" name="eareaname" onclick='showModal("spmbnew/cform/store/","#light");jsDlgShow("#konten *", "#fade", "#light");' value="<?php echo $estorename; ?>">
                  <input id="iarea" name="iarea" type="hidden" value="<?php echo $istore; ?>">
                </td>
              </tr>
              <tr>
                <td width="10%">Keterangan</td>
                <td><textarea id="eremark" name="eremark"><?php echo $eremark; ?></textarea>
                  <!-- <input id="eremark" name="eremark" value="<?php //echo $eremark; 
                                                                  ?>"> --></td>
              </tr>
              <tr>
                <td width="100%" align="center" colspan="4">
                  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
                  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spmbnew/cform/","#main")'>
                  <!--<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">-->
                </td>
              </tr>
            </table>
            <div id="detailheader" align="center">
              <?php if ($detail) { ?>
                <table id="itemtem" class="listtable" style="width:930px;">
                  <tr>
                    <th style="width:25px;" align="center">No</th>
                    <th style="width:63px;" align="center">Kode</th>
                    <th style="width:300px;" align="center">Nama Barang</th>
                    <th style="width:100px;" align="center">Motif</th>
                    <th style="width:80px;" align="center">Jml Rata2</th>
                    <th style="width:100px;" align="center">Nilai Rata2</th>
                    <th style="width:46px;" align="center">Jml Psn</th>
                    <th style="width:46px;" align="center">Jml Acc</th>
                    <th style="width:100px;" align="center">Keterangan</th>
                  </tr>
                <?php } ?>
            </div>
            <div id="detailisi" align="center">
              <?php if ($detail) {
                $tmp = explode("-", $tgl);
                $thak = substr($tmp[2], 2, 2);
                $blak = $tmp[1];
                $blaw = $tmp[1];
                $thaw = $thak; #substr($tmp[2],2,2);
                for ($z = 1; $z <= 3; $z++) {
                  settype($blaw, 'integer');
                  $blaw = $blaw - 1;
                  if ($blaw == 0) {
                    $blaw = 12;
                    $thaw = $thaw - 1;
                  }
                }
                settype($blaw, 'string');
                if (strlen($blaw) == 1) {
                  $blaw = '0' . $blaw;
                }
                $peraw = $thaw . $blaw;
                $perak = $thak . $blak;
                $i = 0;
                foreach ($detail as $isi) {
                  $i++;
                  $fpaw = 'FP-' . $peraw;
                  $fpak = 'FP-' . $perak;
                  $query = $this->db->query(" select trunc(sum(n_deliver*v_unit_price)/3) as vrata, trunc(sum(n_deliver)/3) as nrata, 
                                            i_product 
                                            from tm_nota_item
                                            where i_nota>'$fpaw' and i_nota<'$fpak' and i_product='$isi->i_product' 
                                            and i_product_motif='$isi->i_product_motif' 
                                            and i_area in (select i_area from tr_area where i_area_parent='$iarea')
                                            group by i_product
                                            order by i_product");
                  #                                            and i_area ='$iarea'
                  if ($query->num_rows() > 0) {
                    foreach ($query->result() as $raw) {
                      $vrata = number_format($raw->vrata);
                      $nrata = number_format($raw->nrata);
                    }
                  } else {
                    $vrata = 0;
                    $nrata = 0;
                  }

              ?>
                  <tbody>
                    <tr>
                      <td style="width:23px;">
                        <input style="width:23px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>">
                        <input type="hidden" id="motif<?php echo $i; ?>" name="motif<?php echo $i; ?>" value="<?php echo $isi->i_product_motif; ?>">
                      </td>
                      <td style="width:63px;">
                        <input style="width:63px;" readonly type="text" id="iproduct<?php echo $i; ?>" name="iproduct<?php echo $i; ?>" value="<?php echo $isi->i_product; ?>">
                      </td>
                      <td style="width:290px;">
                        <input style="width:321px;" readonly type="text" id="eproductname<?php echo $i; ?>" name="eproductname<?php echo $i; ?>" value="<?php echo $isi->e_product_name; ?>">
                      </td>
                      <td style="width:98px;">
                        <input readonly style="width:98px;" type="text" id="emotifname<?php echo $i; ?>" name="emotifname<?php echo $i; ?>" value="<?php echo $isi->e_product_motifname; ?>">
                        <input type="hidden" id="vproductmill<?php echo $i; ?>" name="vproductmill<?php echo $i; ?>" value="<?php echo $isi->v_product_mill; ?>">
                      </td>
                      <td style="width:78px;">
                        <input readonly style="text-align:right; width:78px;" type="text" id="jmlrata<?php echo $i; ?>" name="jmlrata<?php echo $i; ?>" value="<?php echo $nrata; ?>">
                      </td>
                      <td style="width:98px;">
                        <input readonly style="text-align:right; width:98px;" type="text" id="nilairata<?php echo $i; ?>" name="nilairata<?php echo $i; ?>" value="<?php echo $vrata; ?>">
                      </td>
                      <td style="width:46px;">
                        <input style="text-align:right; width:46px;" type="text" id="norder<?php echo $i; ?>" name="norder<?php echo $i; ?>" value="0" onkeyup="hitungnilai(<?php echo $i; ?>);">
                      <td style="width:45px;">
                        <input style="text-align:right; width:45px;" readonly type="text" id="nacc<?php echo $i; ?>" name="nacc<?php echo $i; ?>" value="0">
                        <input type="hidden" id="vtotal<?php echo $i; ?>" name="vtotal<?php echo $i; ?>" value="">
                      </td>
                      <td style="width:100px;">
                        <input style="width:100px;" type="text" id="eremark<?php echo $i; ?>" name="eremark<?php echo $i; ?>" value="">
                      </td>
                    </tr>
                  </tbody>
                <?php    } ?>
</table>
<?php    } ?>
</div>
<div id="pesan"></div>
<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
</div>
</div>
</div>
<?= form_close() ?>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a, g) {
    if (confirm(g) == 1) {
      document.getElementById("ispmbdelete").value = a;
      formna = document.getElementById("listform");
      formna.action = "<?php echo site_url(); ?>" + "/spmbnew/cform/delete";
      formna.submit();
    }
  }

  function yyy(b) {
    document.getElementById("ispmbedit").value = b;
    formna = document.getElementById("listform");
    formna.action = "<?php echo site_url(); ?>" + "/spmbnew/cform/edit";
    formna.submit();
  }

  function tambah_item(a) {
    if (a <= 30) {
      so_inner = document.getElementById("detailheader").innerHTML;
      si_inner = document.getElementById("detailisi").innerHTML;
      if (so_inner == '') {
        so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
        so_inner += '<tr><th style="width:25px;"  align="center">No</th>';
        so_inner += '<th style="width:63px;" align="center">Kode</th>';
        so_inner += '<th style="width:300px;" align="center">Nama Barang</th>';
        so_inner += '<th style="width:100px;" align="center">Motif</th>';
        so_inner += '<th style="width:80px;" align="center">Jml Rata2</th>';
        so_inner += '<th style="width:100px;" align="center">Nilai Rata2</th>';
        so_inner += '<th style="width:46px;"  align="center">Jml Psn</th>';
        so_inner += '<th style="width:46px;"  align="center">Jml Acc</th>';
        so_inner += '<th style="width:100px;"  align="center">Keterangan</th>';
        so_inner += '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
        document.getElementById("detailheader").innerHTML = so_inner;
      } else {
        so_inner = '';
      }
      if (si_inner == '') {
        document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
        juml = document.getElementById("jml").value;
        si_inner = '<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
        si_inner += '<td style="width:63px;"><input style="width:63px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
        si_inner += '<td style="width:290px;"><input style="width:290px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
        si_inner += '<td style="width:98px;"><input readonly style="width:98px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value="">';
        si_inner += '<input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
        si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:78px;" type="text" id="jmlrata' + a + '" name="jmlrata' + a + '" value=""></td>';
        si_inner += '<td style="width:98px;"><input readonly style="text-align:right; width:98px;" type="text" id="nilairata' + a + '" name="nilairata' + a + '" value=""></td>';
        si_inner += '<td style="width:46px;"><input style="text-align:right; width:46px;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(' + a + ');">';
        si_inner += '<td style="width:45px;"><input style="text-align:right; width:45px;" readonly type="text" id="nacc' + a + '" name="nacc' + a + '" value="0">';
        si_inner += '<input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value="">';
        si_inner += '</td>';
        si_inner += '<td style="width:100px;"><input style="width:100px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
        si_inner += '<td style="width:50px;">&nbsp;</td></tr></tbody>';
      } else {
        document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
        juml = document.getElementById("jml").value;
        si_inner = si_inner + '<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
        si_inner += '<td style="width:63px;"><input style="width:63px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
        si_inner += '<td style="width:290px;"><input style="width:290px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
        si_inner += '<td style="width:98px;"><input readonly style="width:98px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value="">';
        si_inner += '<input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
        si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:78px;" type="text" id="jmlrata' + a + '" name="jmlrata' + a + '" value=""></td>';
        si_inner += '<td style="width:98px;"><input readonly style="text-align:right; width:98px;" type="text" id="nilairata' + a + '" name="nilairata' + a + '" value=""></td>';
        si_inner += '<td style="width:46px;"><input style="text-align:right; width:46px;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(' + a + ');">';
        si_inner += '<td style="width:45px;"><input style="text-align:right; width:45px;" readonly type="text" id="nacc' + a + '" name="nacc' + a + '" value="0">';
        si_inner += '<input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value="">';
        si_inner += '</td>';
        si_inner += '<td style="width:100px;"><input style="width:100px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
        si_inner += '<td style="width:50px;">&nbsp;</td></tr></tbody>';
      }
      j = 0;
      var baris = Array()
      var iproduct = Array();
      var eproductname = Array();
      var vproductmill = Array();
      var norder = Array();
      var nacc = Array();
      var jmlrata = Array();
      var nilairata = Array();
      var motif = Array();
      var motifname = Array();
      var vtotal = Array();
      for (i = 1; i < a; i++) {
        j++;
        baris[j] = document.getElementById("baris" + i).value;
        iproduct[j] = document.getElementById("iproduct" + i).value;
        eproductname[j] = document.getElementById("eproductname" + i).value;
        vproductmill[j] = document.getElementById("vproductmill" + i).value;
        norder[j] = document.getElementById("norder" + i).value;
        nacc[j] = document.getElementById("nacc" + i).value;
        jmlrata[j] = document.getElementById("jmlrata" + i).value;
        nilairata[j] = document.getElementById("nilairata" + i).value;
        motif[j] = document.getElementById("motif" + i).value;
        motifname[j] = document.getElementById("emotifname" + i).value;
        vtotal[j] = document.getElementById("vtotal" + i).value;
      }
      document.getElementById("detailisi").innerHTML = si_inner;
      j = 0;
      for (i = 1; i < a; i++) {
        j++;
        document.getElementById("baris" + i).value = baris[j];
        document.getElementById("iproduct" + i).value = iproduct[j];
        document.getElementById("eproductname" + i).value = eproductname[j];
        document.getElementById("vproductmill" + i).value = vproductmill[j];
        document.getElementById("norder" + i).value = norder[j];
        document.getElementById("nacc" + i).value = nacc[j];
        document.getElementById("jmlrata" + i).value = jmlrata[j];
        document.getElementById("nilairata" + i).value = nilairata[j];
        document.getElementById("motif" + i).value = motif[j];
        document.getElementById("emotifname" + i).value = motifname[j];
        document.getElementById("vtotal" + i).value = vtotal[j];
      }
      tgl = document.getElementById("dspmb").value;
      area = document.getElementById("iarea").value;
      tmp = tgl.split("-");
      thak = tmp[2].substring(2, 4);
      blak = tmp[1];
      blaw = parseFloat(tmp[1]);
      thaw = parseFloat(tmp[2]);
      for (z = 1; z <= 3; z++) {
        blaw = blaw - 1;
        if (blaw == 0) {
          blaw = 12;
          thaw = thaw - 1;
        }
      }
      thaw = thaw.toString();
      thaw = thaw.substring(2, 4);
      blaw = blaw.toString();
      if (blaw.length == 1) {
        blaw = '0' + blaw;
      }
      peraw = thaw + blaw;
      perak = thak + blak;
      showModal("spmbnew/cform/product/" + a + "/" + peraw + "/" + perak + "/" + area + "/", "#light");
      jsDlgShow("#konten *", "#fade", "#light");
    } else {
      alert("Maksimal 30 item");
    }
  }

  function dipales(a) {
    cek = 'false';
    if ((document.getElementById("dspmb").value != '') &&
      (document.getElementById("iarea").value != '')) {
      if (a == 0) {
        alert('Isi data item minimal 1 !!!');
      } else {
        for (i = 1; i <= a; i++) {
          if ((document.getElementById("iproduct" + i).value == '') ||
            (document.getElementById("eproductname" + i).value == '') ||
            (document.getElementById("norder" + i).value == '')) {
            alert('Data item masih ada yang salah !!!');
            exit();
            cek = 'false';
          } else {
            cek = 'true';
          }
        }
      }
      if (cek == 'true') {
        document.getElementById("login").hidden = true;
        document.getElementById("cmdtambahitem").hidden = true;
      } else {
        document.getElementById("login").hidden = false;
      }
    } else {
      alert('Data header masih ada yang salah !!!');
    }
  }

  function clearitem() {
    document.getElementById("detailisi").innerHTML = '';
    document.getElementById("pesan").innerHTML = '';
    document.getElementById("jml").value = '0';
    document.getElementById("login").hidden = false;
  }

  function view_area() {
    lebar = 450;
    tinggi = 400;
    eval('window.open("<?php echo site_url(); ?>"+"/spmbnew/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
  }

  function hitungnilai(brs) {
    ord = document.getElementById("norder" + brs).value;
    if (isNaN(parseFloat(ord))) {
      alert("Input harus numerik");
    } else {
      hrg = formatulang(document.getElementById("vproductmill" + brs).value);
      qty = formatulang(ord);
      vhrg = parseFloat(hrg) * parseFloat(qty);
      document.getElementById("vtotal" + brs).value = formatcemua(vhrg);
    }
  }

  function diskonrupiah(isi) {
    if (isNaN(parseFloat(isi))) {
      alert("Input harus numerik");
    } else {
      vtot = parseFloat(formatulang(document.getElementById("vspmb").value));
      vtotdis = parseFloat(formatulang(isi));
      vtotbersih = vtot - vtotdis;
      document.getElementById("vspmbbersih").value = formatcemua(vtotbersih);
    }
  }

  function afterSetDateValue(ref_field, target_field, date) {
    dspmb = document.getElementById('dspmb').value;
    d_now = document.getElementById('d_now').value;
    dtmpspmb = dspmb.split('-');
    perspmb = dtmpspmb[2] + dtmpspmb[1] + dtmpspmb[0];
    dtmp = d_now.split('-');
    perdnow = dtmp[2] + dtmp[1] + dtmp[0];
    if ((perspmb != '') && (perdnow != '')) {
      if (compareDates(document.getElementById('dspmb').value, document.getElementById('d_now').value) == -1) {
        alert("Tanggal SPMB tidak boleh lebih kecil dari hari ini !!!");
        document.getElementById("dspmb").value = d_now;
      }
    } else if (dspmb < d_now) {
      alert("Tanggal SPMB tidak boleh lebih kecil dari hari ini !!!");
      document.getElementById("dspmb").value = d_now;
    }
    var startDate = document.getElementById('dspmb').value;
    tes = startDate.split('-');
    startDate = tes[2] + '-' + tes[1] + '-' + tes[0];
    var today = new Date();
    var daten = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
    var curr_date = today.getDate();
    var curr_month = (today.getMonth() + 1);
    var curr_year = today.getFullYear();

    if (curr_month < 10) curr_month = '0' + curr_month;
    if (curr_date < 10) curr_date = '0' + curr_date;
    current = curr_year + "-" + curr_month + "-" + curr_date;
    currentx = curr_date + "-" + curr_month + "-" + curr_year;
    if (startDate > current) {
      alert("Tanggal tidak boleh lebih besar dari hari ini ( " + currentx + " )");
      document.getElementById('dspmb').value = currentx;
    }
  }
</script>