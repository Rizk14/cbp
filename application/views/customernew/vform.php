<?php echo $this->pquery->form_remote_tag(array('url'=>'customernew/cform/simpan','update'=>'#pesan','type'=>'post'));?>
<div id="customernewform">

	<div id="tabbed_box_1" class="tabbed_box">  
  	<div class="tabbed_area">  
	    <ul class="tabs">  
    	  <li><a href="#" class="tab active" onclick="sitab('content_1')">Detail Pelanggan</a></li>  
			  <li><a href="#" class="tab" onclick="sitab('content_2')">SPB</a></li>  
	    </ul>  
	    <div id="content_1" class="content">

				<table class="maintable">
				<tr>
		  		<td align="left">
			    	<table class="mastertable">
						<tr>
							<b><font color="red" size="3">Kolom Dengan Tanda # Wajib diisi</font></b>
						</tr>
						<tr>
							<td width="16%">Area</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eareaname" id="eareaname" value="" readonly onclick='showModal("customernew/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="iarea" id="iarea" value="">#</td>
							<td width="16%">Sales</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="esalesmanname" id="esalesmanname" readonly onclick='view_salesman(document.getElementById("iarea").value);'>
								<input type="hidden" name="isalesman" id="isalesman" value="">#</td>
						</tr>
						<tr>
							<td width="16%">Tanggal Survey</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="dsurvey" id="dsurvey" value="" readonly onclick="showCalendar('',this,this,'','dsurvey',0,20,1)" onblur="cektgl()" value="">#</td>
							<td width="16%">Periode Kunjungan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="nvisitperiod" id="nvisitperiod" value="" readonly onclick='view_retensi();'>
                <input type="hidden" name="iretensi" id="iretensi" value="">
                <input type="text" name="eretensi" id="eretensi" value="" readonly>#</td>
						</tr>
						<tr>
							<td width="16%">Kota</td>
							<td width="1%">:</td>
							<td width="83%" colspan="3">
		            <input type="text" name="ecityname" id="ecityname" value="" readonly onclick='view_city(document.getElementById("iarea").value);'> #
		            <input type="hidden" name="icity" id="icity" value=""></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kriteria Pelanggan</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" colspan=4 width="83%">
								<input type="checkbox" name="chkcriterianew" id="chkcriterianew" value="" onclick='chkcriteriaa()'>
								Pelanggan Baru / New &nbsp;&nbsp;#&nbsp;&nbsp;
								<input type="checkbox" name="chkcriteriaupdate" id="chkcriteriaupdate" value="" onclick='chkcriteriab()'>
								Pelanggan Lama / UpDate</td>
						</tr>
						<tr>
							<td colspan=6>DATA TOKO / PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Nm Toko / Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomername" id="ecustomername" value="" onkeyup="gede(this)";></td>
							<td width="16%">Alamat Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomeraddress" id="ecustomeraddress" value="" maxlength='100' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td width="16%">Penanda Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomersign" id="ecustomersign" value="" onkeyup="gede(this)"></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ert1" id="ert1" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="erw1" id="erw1" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="epostal1" id="epostal1" maxlength='5'></td>
						</tr>
						<tr>
							<td width="16%">Telepon</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerphone" id="ecustomerphone" value="" maxlength='20'></td>
							<td width="16%">Fax</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="efax1" id="efax1" value="" maxlength='20'></td>
						</tr>
						<tr>
							<td width="16%">Yang dihubungi</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomercontact" id="ecustomercontact" maxlength='30' onkeyup="gede(this)" value=""></td>
							<td width="16%">Jabatan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomercontactgrade" id="ecustomercontactgrade" maxlength='30' onkeyup="gede(this)" value=""></td>
						</tr>
						<tr>
							<td width="16%">Mulai Usaha</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomermonth" id="ecustomermonth" value="" onkeyup='lamaberdiri()' maxlength=2>
								/
								<input type="text" name="ecustomeryear" id="ecustomeryear" value="" onkeyup='lamaberdiri()' maxlength=4>(bl / tahun)</td>
							<td width="16%">Tahun</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerage" id="ecustomerage" readonly></td>
						</tr>
						<tr>
							<td width="16%">Status Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eshopstatus" id="eshopstatus" value="" readonly onclick='showModal("customernew/cform/shopstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="ishopstatus" id="ishopstatus" value=""'></td>
							<td width="16%">Luas Fisik Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="nshopbroad" id="nshopbroad" maxlength='7'> M2*total luas toko</td>
						</tr>
						<tr>
							<td width="16%">Kelurahan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerkelurahan1" id="ecustomerkelurahan1" maxlength='30' onkeyup="gede(this)"></td>
							<td width="16%">Kecamatan</td>
							<td width="1%">:</td>
							<td width="33%">
<!--								<input type="text" name="ecustomerkecamatan1" id="ecustomerkecamatan1" maxlength='30' readonly onclick='view_kecamatan1(document.getElementById("iarea").value,document.getElementById("icity").value);'></td>-->
								<input type="text" name="ecustomerkecamatan1" id="ecustomerkecamatan1" maxlength='30' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
<!--                <input type="hidden" name="icity" id="icity" value="">
								<input type="text" name="ecustomerkota1" id="ecustomerkota1" maxlength='30' readonly onclick='view_kota1(document.getElementById("iarea").value);'></td>-->
								<input type="text" name="ecustomerkota1" id="ecustomerkota1" maxlength='30' onkeyup="gede(this)"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input type="text" name="ecustomerprovinsi1" id="ecustomerprovinsi1" maxlength='30' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td colspan=6>DATA 	PEMILIK / PENGURUS TOKO / PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Nama Pemilik</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerowner" id="ecustomerowner" value="" onkeyup="gede(this)"></td>
							<td width="16%">TTL / Umur</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerownerttl" id="ecustomerownerttl" value=""> / <input type="text" name="ecustomerownerage" id="ecustomerownerage" value=""></td>
						</tr>
						<tr>
							<td width="16%">NIK</td>
							<td width="1%">:</td>
							<td width="33%"><input type="text" name="inik" id="inik" value=""></td>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%"></td>
						</tr>
						<tr>
							<td width="16%">Status</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="emarriage" id="emarriage" value="" readonly onclick='showModal("customernew/cform/marriage/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="imarriage" id="imarriage" value=""></td>
							<td width="16%">Jenis Kelamin</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ejeniskelamin" id="ejeniskelamin" value="" readonly onclick='showModal("customernew/cform/jeniskelamin/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="ijeniskelamin" id="ijeniskelamin" value=""></td>
						</tr>
						<tr>
							<td width="16%">Alamat Rumah</td>
							<td width="1%">:</td>
							<td width="33%"><input type="checkbox" name="chkidemtoko1" id="chkidemtoko1" value="" onclick='chkdemtoko1();'> Sama dengan alamat toko</td>
							<td width="16%">Agama</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ereligion" id="ereligion" value="" readonly onclick='showModal("customernew/cform/religion/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="ireligion" id="ireligion" value=""></td>
						</tr>
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%"><input type="text" name="ecustomerowneraddress" id="ecustomerowneraddress" value="" maxlength='100' onkeyup="gede(this)"></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ert2" id="ert2" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="erw2" id="erw2" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="epostal2" id="epostal2" maxlength='5'></td>
						</tr>
						<tr>
							<td width="16%">Telepon / HP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerownerphone" id="ecustomerownerphone" value=""> / <input type="text" name="ecustomerownerhp" id="ecustomerownerhp" value=""></td>
							<td width="16%">Fax / Email</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerownerfax" id="ecustomerownerfax" value=""> / <input type="text" name="ecustomermail" id="ecustomermail" value=""></td>
						</tr>
						<tr>
							<td width="16%">Nama Suami / Istri</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerownerpartner" id="ecustomerownerpartner" value="" onkeyup="gede(this)"></td>
							<td width="16%">TTL / Umur</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerownerpartnerttl" id="ecustomerownerpartnerttl" value=""> / <input type="text" name="ecustomerownerpartnerage" id="ecustomerownerpartnerage" value=""></td>
						</tr>
						<tr>
							<td width="16%">Kelurahan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerkelurahan2" id="ecustomerkelurahan2" maxlength='30' onkeyup="gede(this)"></td>
							<td width="16%">Kecamatan</td>
							<td width="1%">:</td>
							<td width="33%">
<!--								<input type="text" name="ecustomerkecamatan2" id="ecustomerkecamatan2" maxlength='30' readonly onclick='view_kecamatan2(document.getElementById("iarea").value,document.getElementById("icity2").value);'></td>-->
								<input type="text" name="ecustomerkecamatan2" id="ecustomerkecamatan2" maxlength='30' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
<!--                <input type="hidden" name="icity2" id="icity2" value="">
								<input type="text" name="ecustomerkota2" id="ecustomerkota2" maxlength='30' readonly onclick='view_kota2(document.getElementById("iarea").value);'></td>-->
								<input type="text" name="ecustomerkota2" id="ecustomerkota2" maxlength='30' onkeyup="gede(this)"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input type="text" name="ecustomerprovinsi2" id="ecustomerprovinsi2" maxlength='30' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td width="16%">Alamat kirim</td>
							<td width="1%">:</td>
							<td colspan=4 width="33%"><input type="checkbox" name="chkidemtoko2" id="chkidemtoko2" value="" onclick='chkdemtoko2()'> Sama dengan alamat toko
																				<input type="checkbox" name="chkidemtoko3" id="chkidemtoko3" value="" onclick='chkdemtoko3()'> Sama dengan alamat rumah</td>
						</tr>
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%">
						<tr>
							<td width="16%"></td>
							<td width="1%"></td>
							<td width="33%"><input type="text" name="ecustomersendaddress" id="ecustomersendaddress" value="" maxlength='100' onkeyup="gede(this)"></td>
							<td width="16%">RT / RW / Kode Pos</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ert3" id="ert3" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="erw3" id="erw3" maxlength='2'>&nbsp;/&nbsp;
								<input type="text" name="epostal3" id="epostal3" maxlength='5'></td>
						</tr>
						<tr>
							<td width="16%">Telepon</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomersendphone" id="ecustomersendphone" value=""></td>
							<td width="16%">Lokasi Bisa dilalui o/</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="etraversed" id="etraversed" value="" readonly onclick='showModal("customernew/cform/traversed/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="itraversed" id="itraversed" value=""></td>
						</tr>
						<tr>
							<td width="16%">Ada Biaya Retribusi Parkir</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="checkbox" name="fparkir" id="fparkir" value="" onclick="cekparkir();"></td>
							<td width="16%">Ada Biaya Kuli</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="checkbox" name="fkuli" id="fkuli" value="" onclick="cekkuli();"></td>
						</tr>
						<tr>
							<td width="16%">Ekspedisi Toko 1</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eekspedisi1" id="eekspedisi1" value="" onkeyup="gede(this)"></td>
							<td width="16%">Ekspedisi Toko 2</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="eekspedisi2" id="eekspedisi2" value="" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Kabupaten / Kodya</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
<!--								<input type="text" name="ecustomerkota3" id="ecustomerkota3" maxlength='30' readonly onclick='view_kota3(document.getElementById("iarea").value);'></td>-->
								<input type="text" name="ecustomerkota3" id="ecustomerkota3" maxlength='30' onkeyup="gede(this)"></td>
							<td class="batas" width="16%">Provinsi</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">
								<input type="text" name="ecustomerprovinsi3" id="ecustomerprovinsi3" maxlength='30' onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td width="16%">No NPWP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomernpwp" id="ecustomernpwp" value="" maxlength=20> #</td>
							<td width="16%">Nama NPWP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomernpwpname" id="ecustomernpwpname" value="" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td class="batas" width="16%">Alamat NPWP</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" colspan=4 width="83%">
								<input type="text" name="ecustomernpwpaddress" id="ecustomernpwpaddress" value="" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td colspan=6>KUALIFIKASI PELANGGAN</td>
						</tr>
						<tr>
							<td width="16%">Type Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerclassname" id="ecustomerclassname" value="" readonly onclick='showModal("customernew/cform/customerclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomerclass" id="icustomerclass" value="">#</td>
							<td width="16%">Pola Pembayaran</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="epaymentmethod" id="epaymentmethod" value="" readonly onclick='showModal("customernew/cform/paymentmethod/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="ipaymentmethod" id="ipaymentmethod" value="">#</td>
						</tr>
						<tr>
							<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I. Nama Bank :&nbsp;&nbsp;<input type="text" name="ecustomerbank1" id="ecustomerbank1" value="" onkeyup="gede(this)";>
																				 &nbsp;&nbsp;No. A/C   : <input type="text" name="ecustomerbankaccount1" id="ecustomerbankaccount1" value="">
																				 &nbsp;Atas Nama : <input type="text" name="ecustomerbankname1" id="ecustomerbankname1" value="" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;II. Nama Bank :&nbsp;&nbsp;<input type="text" name="ecustomerbank2" id="ecustomerbank2" value="" onkeyup="gede(this)";>
																				 &nbsp;&nbsp;No. A/C   : <input type="text" name="ecustomerbankaccount2" id="ecustomerbankaccount2" value="" onkeyup="gede(this)">
																				 &nbsp;Atas Nama : <input type="text" name="ecustomerbankname2" id="ecustomerbankname2" value="" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td width="16%">Nama Kompetitor</td>
							<td width="1%">:</td>
							<td colspan=4 width="83%">
								1.<input type="text" maxlength=20 name="ekompetitor1" id="ekompetitor1" value="" onkeyup="gede(this)" >
								2.<input type="text" maxlength=20 name="ekompetitor2" id="ekompetitor2" value="" onkeyup="gede(this)" >
								3.<input type="text" maxlength=20 name="ekompetitor3" id="ekompetitor3" value="" onkeyup="gede(this)" ></td>
						</tr>
						<tr>
							<td width="16%">TOP</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ncustomertoplength" id="ncustomertoplength" value="" maxlength='3'> Hari&nbsp;#</td>
							<td width="16%">Discount</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ncustomerdiscount" id="ncustomerdiscount" value="" maxlength='3' onkeyup='copydisc()'> %&nbsp;#</td>
						</tr>
						<tr>
							<td width="16%">Kontra Bon</td>
							<td width="1%">:</td>
							<td width="33%">
								 <input type="checkbox" name="chkkontrabon" id="chkkontrabon" value="" onclick="cekkontrabon();"></td>
							<td width="16%">Waktu u/ menghubungi</td>
							<td width="1%">:</td>
							<td width="33%"><input type="text" name="ecall" id="ecall" value="" readonly onclick='showModal("customernew/cform/call/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icall" id="icall" value="">
								</td>
						</tr>
						<tr>
							<td class="batas" width="16%">Jadwal Kontra Bon</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">Hari
								<input type="text" name="ekontrabonhari" id="ekontrabonhari" value="" onkeyup="gede(this)";>&nbsp;Jam
								<input type="text" name="ekontrabonjam1" id="ekontrabonjam1" value="">s/d
								<input type="text" name="ekontrabonjam2" id="ekontrabonjam2" value=""></td>
							<td class="batas" width="16%">Jadwal Tagih</td>
							<td class="batas" width="1%">:</td>
							<td class="batas" width="33%">Hari
								<input type="text" name="etagihhari" id="etagihhari" value="" onkeyup="gede(this)";>&nbsp;Jam
								<input type="text" name="etagihjam1" id="etagihjam1" value="">s/d
								<input type="text" name="etagihjam2" id="etagihjam2" value=""></td>
						</tr>
						<tr>
							<td colspan=6>LAIN - LAIN</td>
						</tr>
						<tr>
							<td width="16%">Group Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomergroupname" id="ecustomergroupname" value="" readonly onclick='showModal("customernew/cform/customergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomergroup" id="icustomergroup" value="">#</td>
							<td width="16%">PLU Group</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerplugroupname" id="ecustomerplugroupname" value="" readonly onclick='showModal("customernew/cform/plugroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomerplugroup" id="icustomerplugroup" value=""></td>
						</tr>
						<tr>
							<td width="16%">Tipe Produk</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerproducttypename" id="ecustomerproducttypename" value="" readonly onclick='showModal("customernew/cform/customerproducttype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomerproducttype" id="icustomerproducttype" value="">#</td>
							<td width="16%">Produk Khusus</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerspecialproductname" id="ecustomerspecialproductname" value="" readonly onclick="view_customerspecialproduct(document.getElementById('icustomerproducttype').value)">
								<input type="hidden" name="icustomerspecialproduct" id="icustomerspecialproduct" value="">#</td>
						</tr>
						<tr>
							<td width="16%">Status Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerstatusname" id="ecustomerstatusname" value="" readonly onclick='showModal("customernew/cform/customerstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomerstatus" id="icustomerstatus" value="">#</td>
							<td width="16%">Tingkat Pelanggan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomergradename" id="ecustomergradename" value="" readonly onclick='showModal("customernew/cform/customergrade/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomergrade" id="icustomergrade" value="">#</td>
						</tr>
						<tr>
							<td width="16%">Jenis Pelayanan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomerservicename" id="ecustomerservicename" value="" readonly onclick='showModal("customernew/cform/customerservice/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomerservice" id="icustomerservice" value="">#</td>
							<td width="16%">Cara Penjualan</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="ecustomersalestypename" id="ecustomersalestypename" value="" readonly onclick='showModal("customernew/cform/customersalestype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								<input type="hidden" name="icustomersalestype" id="icustomersalestype" value="">#</td>
						</tr>
						<tr>
							<td width="16%">Keterangan</td>
							<td width="1%">:</td>
							<td width="83%" colspan=4>
								<input type="text" name="ecustomerrefference" id="ecustomerrefference" value="" onkeyup="gede(this)"></td>
						</tr>
		      	</table>
		  		</td>
				</tr> 
	      </table>

	    </div>
	    <div id="content_2" class="content">
				<table class="maintable">
	  		<tr>
	    		<td align="left">
						<div class="effect">
	  					<div class="accordion2">
	    					<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      				<tr>
									<td>Tgl SPB</td>
									<td><input readonly id="dspb" name="dspb" 
											 onclick="showCalendar('',this,this,'','dspb',0,20,1)" onblur="cektgl()" value="<?php echo $tgl; ?>">
											<input id="ispb" name="ispb" type="hidden">
											<input id="iperiode" name="iperiode" type="hidden" value="<?php echo $iperiode; ?>">
			<input id="dspbsys" name="dspbsys" type="hidden" value="<?php echo $dspbsys; ?>"></td>
									<td>Discount 1</td>
									<td><input readonly id ="ncustomerdiscount1" name="ncustomerdiscount1" value="0">
											<input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" value="0"></td>
	      				</tr>
	      				<tr>
									<td>PO</td>
									<td><input id="ispbpo" name="ispbpo" maxlength="10"></td>
									<td>Discount 2</td>
									<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2" value="0">
											<input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="0"></td>
	  				    </tr>
	  				    <tr>
									<td>Stock Daerah</td>
									<td><input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" value="" onclick="pilihstockdaerah(this.value)">
											SPB Lama&nbsp;&nbsp;<input id="ispbold" name="ispbold" type="text" value=""></td>
									<td>Discount 3</td>
									<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3" value="0">
											<input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="0"></td>
	  				    </tr>
	  				    <tr>
									<td>PKP</td>
									<td><input id="fspbplusppn" name="fspbplusppn" type="hidden">
										<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden">
										<input id="fspbpkp" name="fspbpkp" type="hidden">
										<input id="fcustomerfirst" name="fcustomerfirst" type="hidden">
										<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly maxlength=20></td>
									<td>Discount Total</td>
									<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal" value="0" onkeyup="diskonrupiah(this.value)"></td>
	  				    </tr>
	  				    <tr>
									<td>Keterangan</td>
									<td><input  id="eremarkx" name="eremarkx" maxlength="100"></td>
									<td>Nilai Bersih</td>
									<td><input  readonly id="vspbbersih" name="vspbbersih" readonly value="0"></td>
	  				    </tr>
	  				    <tr>
									<td>Kelompok Harga</td>
									<td><input readonly id="epricegroupname" name="epricegroupname" onclick='showModal("customernew/cform/pricegroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
											<input id="ipricegroup" name="ipricegroup" type="hidden"><input id="nline" name="nline" type="hidden">#</td>
									<td>Discount Total (realisasi)</td>
									<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly value="0"></td>
	     				 </tr>
			    			<tr>
									<td>Nilai Kotor</td>
									<td><input  readonly id="vspb" name="vspb"></td>
									<td>Nilai SPB (realisasi)</td>
									<td><input  readonly id="vspbafter" name="vspbafter" readonly value="0"></td>
			    			</tr>
		  					<tr>
									<td width="100%" align="center" colspan="4">
										<div id='ketppn'></div>
									</td>
		  					</tr>
								<tr>
									<td width="100%" align="center" colspan="4">
										<input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
										<input name="cmdreset" id="cmdreset" value="Bersihkan" type="button" onclick='show("customernew/cform/","#main")'>
										<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
										 onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'></td>
								</tr>
	    					</table>
								<div id="detailheader" align="center"></div>
								<div id="detailisi" align="center"></div>
								<input type="hidden" name="jml" id="jml" value="0">
	  					</div>
						</div>
	  	  	</td>
	  		</tr>
				</table>
				<div id="pesan"></div>
			</div>
		</div>
	</div>
</div>
<?=form_close()?>
<script language="javascript" type="text/javascript">
	function view_retensi(a){
		showModal("customernew/cform/retensi/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_salesman(a){
		showModal("customernew/cform/salesman/"+a+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_city(a){
		showModal("customernew/cform/city/"+a+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
  function view_kota1(a){
		showModal("customernew/cform/kota1/"+a+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_kota2(a){
		showModal("customernew/cform/kota2/"+a+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_kota3(a){
		showModal("customernew/cform/kota3/"+a+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_kecamatan1(a,b){
		showModal("customernew/cform/kecamatan1/"+a+"/"+b+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_kecamatan2(a,b){
		showModal("customernew/cform/kecamatan2/"+a+"/"+b+"/zxqf/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_customerspecialproduct(a){
		showModal("customernew/cform/customerspecialproduct/"+a,"#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function cek(){
		area=document.getElementById("iarea").value;
		nama=document.getElementById("ecustomername").value;
		if(kode=='' || nama==''){
			alert("Minimal kode area dan nama Pelanggan diisi terlebih dahulu !!!");
		}else{
		}
	}
	function chkcriteriaa(){
		if(document.getElementById('chkcriterianew').checked){
			document.getElementById('chkcriteriaupdate').checked=false;
			document.getElementById('chkcriterianew').value='on';
			document.getElementById('chkcriteriaupdate').value='';
		}else{
			document.getElementById('chkcriteriaupdate').checked=true;
			document.getElementById('chkcriterianew').value='';
			document.getElementById('chkcriteriaupdate').value='on';
		}
	}
	function chkcriteriab(){
		if(document.getElementById('chkcriteriaupdate').checked){
			document.getElementById('chkcriterianew').checked=false;
			document.getElementById('chkcriteriaupdate').value='on';
			document.getElementById('chkcriterianew').value='';
		}else{
			document.getElementById('chkcriterianew').checked=true;
			document.getElementById('chkcriteriaupdate').value='';
			document.getElementById('chkcriterianew').value='on';
		}
	}
	function lamaberdiri(){
		var curDate=new Date();
		startDate=document.getElementById("ecustomeryear").value+'-'+document.getElementById("ecustomermonth").value+'-01';
		if(startDate.length==10){
			thn=curDate.getFullYear().toString();
			bl=curDate.getMonth()+1;
			bln='0'+bl.toString();
			endDate=thn+'-'+bln+'-01';
			curDate.DateDiff({interval:"yyyy",date1:startDate,date2:endDate});
	//		alert(curDate.difference);
			document.getElementById("ecustomerage").value=curDate.difference;
		}
	}
	function chkdemtoko1(){
		if(document.getElementById('chkidemtoko1').checked==true){
			document.getElementById('chkidemtoko1').value='on';
			document.getElementById('ecustomerowneraddress').value=document.getElementById('ecustomeraddress').value;
			document.getElementById('ert2').value=document.getElementById('ert1').value;
			document.getElementById('erw2').value=document.getElementById('erw1').value;
			document.getElementById('epostal2').value=document.getElementById('epostal1').value;
			document.getElementById('ecustomerkelurahan2').value=document.getElementById('ecustomerkelurahan1').value;
			document.getElementById('ecustomerkecamatan2').value=document.getElementById('ecustomerkecamatan1').value;
			document.getElementById('ecustomerkota2').value=document.getElementById('ecustomerkota1').value;
			document.getElementById('ecustomerprovinsi2').value=document.getElementById('ecustomerprovinsi1').value;
			document.getElementById('ecustomerownerphone').value=document.getElementById('ecustomerphone').value;
			document.getElementById('ecustomerownerfax').value=document.getElementById('efax1').value;
		}else{
			document.getElementById('chkidemtoko1').value='';
			document.getElementById('ecustomerowneraddress').value='';
			document.getElementById('ert2').value='';
			document.getElementById('erw2').value='';
			document.getElementById('epostal2').value='';
			document.getElementById('ecustomerkelurahan2').value='';
			document.getElementById('ecustomerkecamatan2').value='';
			document.getElementById('ecustomerkota2').value='';
			document.getElementById('ecustomerprovinsi2').value='';
			document.getElementById('ecustomerownerphone').value='';
			document.getElementById('ecustomerownerfax').value='';
		}

	}

	function chkdemtoko2(){
		if(document.getElementById('chkidemtoko2').checked==true){
			document.getElementById('chkidemtoko3').checked==false
			document.getElementById('chkidemtoko3').value='';
			document.getElementById('chkidemtoko2').value='on';
			document.getElementById('ecustomersendaddress').value=document.getElementById('ecustomeraddress').value;
			document.getElementById('ert3').value=document.getElementById('ert1').value;
			document.getElementById('erw3').value=document.getElementById('erw1').value;
			document.getElementById('epostal3').value=document.getElementById('epostal1').value;
			document.getElementById('ecustomerkota3').value=document.getElementById('ecustomerkota1').value;
			document.getElementById('ecustomerprovinsi3').value=document.getElementById('ecustomerprovinsi1').value;
			document.getElementById('ecustomersendphone').value=document.getElementById('ecustomerphone').value;
		}else{
			document.getElementById('chkidemtoko2').value='';
			document.getElementById('ecustomersendaddress').value='';
			document.getElementById('ert3').value='';
			document.getElementById('erw3').value='';
			document.getElementById('epostal3').value='';
			document.getElementById('ecustomerkota3').value='';
			document.getElementById('ecustomerprovinsi3').value='';
			document.getElementById('ecustomersendphone').value='';
		}
	}
	function chkdemtoko3(){
		if(document.getElementById('chkidemtoko3').checked==true){
			document.getElementById('chkidemtoko2').checked==false
			document.getElementById('chkidemtoko2').value='';
			document.getElementById('chkidemtoko3').value='on';
			document.getElementById('ecustomersendaddress').value=document.getElementById('ecustomerowneraddress').value;
			document.getElementById('ert3').value=document.getElementById('ert2').value;
			document.getElementById('erw3').value=document.getElementById('erw2').value;
			document.getElementById('epostal3').value=document.getElementById('epostal2').value;
			document.getElementById('ecustomerkota3').value=document.getElementById('ecustomerkota2').value;
			document.getElementById('ecustomerprovinsi3').value=document.getElementById('ecustomerprovinsi2').value;
			document.getElementById('ecustomersendphone').value=document.getElementById('ecustomerownerphone').value;
		}else{
			document.getElementById('chkidemtoko3').value='';
			document.getElementById('ecustomersendaddress').value='';
			document.getElementById('ert3').value='';
			document.getElementById('erw3').value='';
			document.getElementById('epostal3').value='';
			document.getElementById('ecustomerkota3').value='';
			document.getElementById('ecustomerprovinsi3').value='';
			document.getElementById('ecustomersendphone').value='';
		}
	}
	function copydisc(){
		document.getElementById('ncustomerdiscount1').value=document.getElementById('ncustomerdiscount').value;
	}
	function tambah_item(a){
    if(a<22){
      so_inner=document.getElementById("detailheader").innerHTML;
      si_inner=document.getElementById("detailisi").innerHTML;
      if(so_inner==''){
		    so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
		    so_inner+= '<thead><tr><th style="width:25px;"  align="center">No</th>';
		    so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		    so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		    so_inner+= '<th style="width:100px;" align="center">Motif</th>';
		    so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
		    so_inner+= '<th style="width:46px;"  align="center">Jml Psn</th>';
		    so_inner+= '<th style="width:94px;"  align="center">Total</th>';
		    so_inner+= '<th style="width:180px;"  align="center">Keterangan</th>';
		    so_inner+= '<th style="width:32px;"  align="center" class="Action">Act</th></tr></thead></table>';
		    document.getElementById("detailheader").innerHTML=so_inner;
      }else{
		    so_inner=''; 
      }
      if(si_inner==''){
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;	
		    si_inner='<table><tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="iproductstatus'+a+'" name="iproductstatus'+a+'" value=""></td>';
		    si_inner+='<td style="width:61px;"><input style="width:61px; font-size:12px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:283px;"><input style="width:283px; font-size:12px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:96px;"><input readonly style="width:96px; font-size:12px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		    si_inner+='<td style="width:87px;"><input readonly style="text-align:right; width:87px; font-size:12px;"  type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		    si_inner+='<td style="width:45px;"><input style="text-align:right; width:45px; font-size:12px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')" autocomplete="off"></td>';
		    si_inner+='<td style="width:90px;"><input readonly" style="text-align:right; width:90px; font-size:12px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		    si_inner+='<td style="width:173px;"><input style="width:173px; font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value="" maxlength="50"></td>';
		    //si_inner+='<td style=\"width:51px;\" align=\"center\"><a href=\"#\" onclick='show(\"dkb/cform/deletedetail/$row->i_dkb/$row->i_area/$row->i_sj/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>";
			si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
      }else{
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;
		    si_inner=si_inner+'<table><tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="iproductstatus'+a+'" name="iproductstatus'+a+'" value=""></td>';
		    si_inner+='<td style="width:61px;"><input style="width:61px; font-size:12px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:283px;"><input style="width:283px; font-size:12px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:96px;"><input readonly style="width:96px; font-size:12px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		    si_inner+='<td style="width:87px;"><input readonly style="text-align:right; width:87px; font-size:12px;"  type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		    si_inner+='<td style="width:45px;"><input style="text-align:right; width:45px; font-size:12px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')" autocomplete="off"></td>';
		    si_inner+='<td style="width:90px;"><input readonly" style="text-align:right; width:90px; font-size:12px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		    si_inner+='<td style="width:173px;"><input style="width:173px; font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value="" maxlength="50"></td>';
		    //si_inner+='<td style=\"width:51px;\" align=\"center\"><a href=\"#\" onclick='show("/cform/deletedetail/$row->i_dkb/$row->i_area/$row->i_sj/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>";
			si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
      }
      j=0;
      var baris			    = Array()
      var iproduct		  = Array();
      var eproductname	= Array();
      var vproductretail= Array();
      var norder			  = Array();
      var motif			    = Array();
      var motifname		  = Array();
      var vtotal			  = Array();
      var eremark			  = Array();
      var status			  = Array();
      for(i=1;i<a;i++){
	      j++;
	      baris[j]			    = document.getElementById("baris"+i).value;
	      iproduct[j]			  = document.getElementById("iproduct"+i).value;
	      status[j] 			  = document.getElementById("iproductstatus"+i).value;
	      eproductname[j]		= document.getElementById("eproductname"+i).value;
	      vproductretail[j]	= document.getElementById("vproductretail"+i).value;
	      norder[j]			    = document.getElementById("norder"+i).value;
	      motif[j]			    = document.getElementById("motif"+i).value;
	      motifname[j]			= document.getElementById("emotifname"+i).value;
	      vtotal[j]			    = document.getElementById("vtotal"+i).value;
	      eremark[j]			  = document.getElementById("eremark"+i).value;		
      }
      document.getElementById("detailisi").innerHTML=si_inner;
      j=0;
      for(i=1;i<a;i++){
		    j++;
		    document.getElementById("baris"+i).value=baris[j];
		    document.getElementById("iproduct"+i).value=iproduct[j];
        document.getElementById("iproductstatus"+i).value=status[j];
		    document.getElementById("eproductname"+i).value=eproductname[j];
		    document.getElementById("vproductretail"+i).value=vproductretail[j];
		    document.getElementById("norder"+i).value=norder[j];
		    document.getElementById("motif"+i).value=motif[j];
		    document.getElementById("emotifname"+i).value=motifname[j];
		    document.getElementById("vtotal"+i).value=vtotal[j];
		    document.getElementById("eremark"+i).value=eremark[j];		
      }
	    showModal("spbbaby/cform/product/"+a+"/"+document.getElementById("ipricegroup").value+"/x01/","#light");
	    jsDlgShow("#konten *", "#fade", "#light");
    }else{
      alert('Maksimum 21 item');
    }
  }

  function hitungnilai(isi,jml){
		jml=document.getElementById("jml").value;
		if (isNaN(parseFloat(isi))){
			alert("Input harus numerik");
		}else{
			dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
			dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
			dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
			vdis1=0;
			vdis2=0;
			vdis3=0;
			vtot =0;
			for(i=1;i<=jml;i++){
				vhrg=formatulang(document.getElementById("vproductretail"+i).value);
				nqty=formatulang(document.getElementById("norder"+i).value);
				vhrg=parseFloat(vhrg)*parseFloat(nqty);
				vtot=vtot+vhrg;
				document.getElementById("vtotal"+i).value=formatcemua(vhrg);
			}
			vdis1=vdis1+((vtot*dtmp1)/100);
			vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
			vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
			document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
			document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
			document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
			vdis1=parseFloat(vdis1);
			vdis2=parseFloat(vdis2);
			vdis3=parseFloat(vdis3);
			vtotdis=vdis1+vdis2+vdis3;
			vtotdis=Math.round(vtotdis);
			document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
			document.getElementById("vspb").value=formatcemua(vtot);
			vtotbersih=parseFloat(vtot)-parseFloat(vtotdis);
			document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
		}
  }
  function pilihstockdaerah(a){
		if(a=='')
		{
			document.getElementById("fspbstockdaerah").value='on';
		}else{
			document.getElementById("fspbstockdaerah").value='';
		}
  }
  function dipales(a){
//dspb
		cek='false';
		if(document.getElementById("dspb").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").disabled=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	 }//else{
		//    	document.getElementById("login").hidden=false;
		// 	}
    	}else{
   			alert('Tanggal Survey di isi dulu yah!!!');
    	}	
//dsurvey
		if(document.getElementById("dsurvey").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Tanggal Survey di isi dulu yah!!!');
    	}
//isalesman
		if(document.getElementById("isalesman").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Salesnya di isi dulu yah!!!');
    	}
//ncustomertoplength
		if(document.getElementById("ncustomertoplength").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('TOP di isi dulu yah!!!');
    	}
//iarea
		if(document.getElementById("iarea").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Areanya di isi dulu yah!!!');
    	}
//ncustomerdiscount
		if(document.getElementById("ncustomerdiscount").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Diskonnya di isi dulu yah!!!');
    	}
//nvisitperiod
		if(document.getElementById("nvisitperiod").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Periode Kunjungan di isi dulu yah!!!');
    	}
//icustomergroup
		if(document.getElementById("icustomergroup").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Group Pelanggan di isi dulu yah!!!');
    	}
//icustomerproducttype
		if(document.getElementById("icustomerproducttype").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Tipe Produk di isi dulu yah!!!');
    	}
//icustomerstatus
		if(document.getElementById("icustomerstatus").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Status Pelanggan di isi dulu yah!!!');
    	}
//icustomergrade
		if(document.getElementById("icustomergrade").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Tingkat Pelanggan di isi dulu yah!!!');
    	}
//icustomerservice
		if(document.getElementById("icustomerservice").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Jenis Pelayanan di isi dulu yah!!!');
    	}
//icustomersalestype
		if(document.getElementById("icustomersalestype").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Cara Penjualan di isi dulu yah!!!');
    	}
//ipaymentmethod
		if(document.getElementById("ipaymentmethod").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Pola Pembayaran di isi dulu yah!!!');
    	}
//ecustomerowner
		if(document.getElementById("ecustomerowner").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Nama Pemilik Toko di isi dulu yah!!!');
    	}
//ecustomeraddress
		if(document.getElementById("ecustomerowner").value!=''){
			if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
					} 
			}
		}
			if(cek=='true'){
 	  			//document.getElementById("login").hidden=true;
				document.getElementById("cmdtambahitem").hidden=true;
    	}//else{
		   	//document.getElementById("login").hidden=false;
			//}
    	}else{
   			alert('Alamat Pengirimannya di isi dulu yah!!!');
    	}
	}
//   	 	(document.getElementById("ecustomername").value!='') &&
//   	 	(document.getElementById("dsurvey").value!='') &&
//   	 	(document.getElementById("isalesman").value!='') &&
//   	 	(document.getElementById("ncustomertoplength").value!='') &&
//   	 	(document.getElementById("ncustomerdiscount").value!='') &&
//    	 	(document.getElementById("nvisitperiod").value!='') &&
//   	 	(document.getElementById("iarea").value!='') &&
//   	 	(document.getElementById("icustomergroup").value!='') &&
//   	 	(document.getElementById("icustomerproducttype").value!='') &&
//   	 	(document.getElementById("icustomerstatus").value!='') &&
//   	 	(document.getElementById("icustomergrade").value!='') &&
//   	 	(document.getElementById("icustomerservice").value!='') &&
//   	 	(document.getElementById("icustomersalestype").value!='') &&
// 			(document.getElementById("ipricegroup").value!='') &&
//   	 	(document.getElementById("ipaymentmethod").value!='')
//   	 	 {
// //  	 	(document.getElementById("icustomerspecialproduct").value!='') &&
//   	 	if(a==0){
//   	 		alert('Isi data item minimal 1 !!!');
//   	 	}else{
//    			for(i=1;i<=a;i++){
// 					if((document.getElementById("iproduct"+i).value=='') ||
// 						(document.getElementById("eproductname"+i).value=='') ||
// 						(document.getElementById("norder"+i).value=='')){
// 						alert('Data item masih ada yang salah !!!');
// 						exit();
// 						cek='false';
// 					}else{
// 						cek='true';	
// 					} 
// 				}
// 			}
// 			if(cek=='true'){
//  	  		document.getElementById("login").hidden=true;
// 				document.getElementById("cmdtambahitem").hidden=true;
//     	}else{
// 		   	document.getElementById("login").hidden=false;
// 			}
//     }else{
//    		alert('Data header masih ada yang salah !!!');
//     }
// }
  function cekparkir(){
    if(document.getElementById("fparkir").value=='on'){
      document.getElementById("fparkir").value='';
    }else{
      document.getElementById("fparkir").value='on';
    }
  }
  function cekkuli(){
    if(document.getElementById("fkuli").value=='on'){
      document.getElementById("fkuli").value='';
    }else{
      document.getElementById("fkuli").value='on';
    }
  }
  function cekkontrabon(){
    if(document.getElementById("fkontrabon").value=='on'){
      document.getElementById("fkontrabon").value='';
    }else{
      document.getElementById("fkontrabon").value='on';
    }
  }
  function afterSetDateValue(ref_field, target_field, date) {
      
      var startDate=document.getElementById('dspb').value;
      tes=startDate.split('-');
      startDate=tes[2]+'-'+tes[1]+'-'+tes[0];
      start=tes[2]+'-'+tes[1];
      
      var tglakhirthn='27-12-2017';
      tes3=tglakhirthn.split('-');
      tglakhirthn=tes3[2]+'-'+tes3[1]+'-'+tes3[0];

      var dspbSys=document.getElementById('dspbsys').value;
      tes2=dspbSys.split('-');
      dspbSys=tes2[0]+'-'+tes2[1]+'-'+tes2[2];

      var iperiode=document.getElementById('iperiode').value;
      //alert(dspbSys);
      //alert(iperiode);
      var d=new Date();
      var curr_date = d.getDate() - 1;
      var curr_month = d.getMonth() + 1;
      var curr_year = d.getFullYear();

      if ( curr_month < 10 ) curr_month = '0' + curr_month;
      if ( curr_date < 10 )curr_date='0'+curr_date;
      current=curr_year + "-" + curr_month + "-" + curr_date;
      //alert(startDate);
      //alert(current);
      //alert(tglakhirthn);
      curr=curr_year + "-" + curr_month;
      //currenttmp1=curr_year + "-" + curr_month + "-" + 29;
  	  //curr1=curr_year + "-" + curr_month;
      currentx=curr_date + "-" + curr_month + "-" + curr_year;
       /*if(current>=startDate){
      	
      	alert("Tanggal SPB tidak boleh kurang / sama dengan Tanggal Skrng "+current+"(1)");
  		document.getElementById('dspb').value='';

      }else if(startDate>=tglakhirthn){

      	alert("TUTUP SPB AKHIR TAHUN TERAKHIR SPB TANGGAL "+current+"(2)");
        document.getElementById('dspb').value='';

   	  }*/
  }

  function cektgl(){
		setTimeout(function(){
			var dspb = $('#dspb').val();
			var dsurvey = $('#dsurvey').val();
			/* var today = moment().format("DD-MM-YYYY"); */
			var today = '<?=date('d-m-Y')?>';

			if(date_diff_indays(today, dspb) > 0){
				/* alert('Tanggal SPB Harus Sama Dengan Tanggal Hari Ini'); */
				alert("Tanggal tidak boleh lebih besar dari hari ini ( "+today+" )");
				$('#dspb').val(today);
			}else if (date_diff_indays(today, dspb) < 0){
				alert("Tanggal tidak boleh lebih kecil dari hari ini ( "+today+" )");
				$('#dspb').val(today);
			}

			if(date_diff_indays(today, dsurvey) > 0){
				alert("Tanggal tidak boleh lebih besar dari hari ini ( "+today+" )");
				$('#dsurvey').val(today);
			}

		}, 100);
	}
</script>
