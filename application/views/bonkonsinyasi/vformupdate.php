<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'penjualankonsinyasi/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="notapbform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Tgl</td>
    <?php 
			$tmp=explode("-",$isi->d_notapb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$isi->d_notapb=$hr."-".$bl."-".$th;
		?>
		<td><input readonly id="dnotapb" name="dnotapb" value="<?php echo $isi->d_notapb; ?>">
<!-- onclick="showCalendar('',this,this,'','dnotapb',0,20,1)" -->
		    <input id="inotapb" name="inotapb" value="<?php if($inotapb) echo substr($inotapb,8,7); ?>" maxlength=7>
		    <input type="hidden" id="xinotapb" name="xinotapb" value="<?php if($inotapb) echo $inotapb; ?>" maxlength=7></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SPG</td>
		<td><input readonly id="espgname" name="espgname" value="<?php if($isi->e_spg_name) echo $isi->e_spg_name; ?>">
		    <input id="ispg" name="ispg" type="hidden" value="<?php if($isi->i_spg) echo $isi->i_spg; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php if($isi->e_customer_name) echo $isi->e_customer_name; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php if($icustomer) echo $icustomer; ?>"></td>
	      </tr>
        <tr>
		<td width="10%">Potongan</td>
		<td><input style="text-align:right;" id="nnotapbdiscount" name="nnotapbdiscount" value="<?php echo number_format($isi->n_notapb_discount); ?>" onkeyup="hitungnilai();">%
			<input style="text-align:right;" id="vnotapbdiscount" name="vnotapbdiscount" value="<?php echo number_format($isi->v_notapb_discount); ?>" onkeyup="diskonrupiah();"></td>
		</tr>
	      </tr>
        <tr>
		<td width="10%">Total</td>
		<td><input type="hidden" id="vnotapbgross" name="vnotapbgross" value="<?php echo number_format($isi->v_notapb_gross); ?>">
        <input readonly id="vnotapbnetto" name="vnotapbnetto" value="<?php echo number_format($isi->v_notapb_gross-$isi->v_notapb_discount); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
        <?php if ($isi->f_spb_rekap=='f' && ($isi->i_spb=='' || $isi->i_spb==null)){ ?>
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
        <?php }?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listpenjualankonsinyasi/cform/view/<?php echo $dfrom; ?>/<?php echo $dto; ?>/","#main")'>
        <?php if ($isi->f_spb_rekap=='f' && ($isi->i_spb=='' || $isi->i_spb==null)){ ?>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
        <?php }?>
      </td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
        <table id="itemtem" class="listtable" style="width:950px;">
			  <tr><th style="width:25px;"  align="center">No</th>
			  <th style="width:80px;" align="center">Kode</th>
			  <th style="width:400px;" align="center">Nama Barang</th>
			  <th style="width:100px;" align="center">Jumlah</th>
			  <th style="width:120px;" align="center">Harga</th>
			  <th style="width:120px;" align="center">Total</th>
			  <th style="width:72px;"  align="center">Ket</th></tr></table>
      </div>
			<div id="detailisi" align="center">
      <?php 
				$i=0;
				foreach($detail as $row)
				{
			  	$i++;
          $totall=$row->n_quantity*$row->v_unit_price;
          echo "<table><tbody><tr><td style=\"width:25px;\"><input style=\"width:25px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"><input type=\"hidden\" id=\"ipricegroupco$i\" name=\"ipricegroupco$i\" value=\"$row->i_price_groupco\"></td><td style=\"width:80px;\"><input style=\"width:80px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td><td style=\"width:404px;\"><input style=\"width:404px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\"></td><td style=\"width:100px;\"><input style=\"text-align:right; width:100px;\" type=\"text\" id=\"nquantity$i\" name=\"nquantity$i\" value=\"$row->n_quantity\" onkeyup=\"hitungnilai($i);\"></td><td style=\"width:120px;\"><input readonly style=\"text-align:right; width:120px;\" type=\"text\" id=\"vunitprice$i\" name=\"vunitprice$i\" value=\"$row->v_unit_price\"></td><td style=\"width:120px;\"><input style=\"text-align:right; width:120px;\" type=\"text\" id=\"total$i\" name=\"total$i\" value=\"$totall\"></td><td style=\"width:74px;\"><input style=\"text-align:right; width:74px;\" type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\"></td><td style=\"width:54px;\">&nbsp;</td></tr></tbody></table>";
        }
      ?>
      </div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,g){
    if (confirm(g)==1){
	  document.getElementById("ispmbdelete").value=a;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/penjualankonsinyasi/cform/delete";
	  formna.submit();
    }
  }
  function yyy(b){
	document.getElementById("ispmbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/penjualankonsinyasi/cform/edit";
	formna.submit();
  }
  function tambah_item(a){
    if(a<=30){
	      so_inner=document.getElementById("detailheader").innerHTML;
	      si_inner=document.getElementById("detailisi").innerHTML;
	      if(so_inner==''){
			  so_inner = '<table id="itemtem" class="listtable" style="width:950px;">';
			  so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
			  so_inner+= '<th style="width:80px;" align="center">Kode</th>';
			  so_inner+= '<th style="width:400px;" align="center">Nama Barang</th>';
			  so_inner+= '<th style="width:100px;" align="center">Jumlah</th>';
			  so_inner+= '<th style="width:120px;" align="center">Harga</th>';
			  so_inner+= '<th style="width:120px;" align="center">Total</th>';
			  so_inner+= '<th style="width:72px;"  align="center">Ket</th></tr>';
			  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
  			so_inner='';
    }
    if(si_inner==''){
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;	
		    si_inner='<tbody><tr><td style="width:25px;"><input style="width:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="ipricegroupco'+a+'" name="ipricegroupco'+a+'" value=""></td>';
		    si_inner+='<td style="width:80px;"><input style="width:80px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:404px;"><input style="width:404px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:100px;"><input style="text-align:right; width:100px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" value="" onkeyup="hitungnilai('+a+');"></td>';
		    si_inner+='<td style="width:120px;"><input readonly style="text-align:right; width:120px;" type="text" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		    si_inner+='<td style="width:120px;"><input style="text-align:right; width:120px;" type="text" id="total'+a+'" name="total'+a+'" value=""></td>';
		    si_inner+='<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody>';
    }else{
			  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			  juml=document.getElementById("jml").value;
			  si_inner=si_inner+'<tbody><tr><td style="width:25px;"><input style="width:25px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="ipricegroupco'+a+'" name="ipricegroupco'+a+'" value=""></td>';
		    si_inner+='<td style="width:80px;"><input style="width:80px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:404px;"><input style="width:404px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:100px;"><input style="text-align:right; width:100px;" type="text" id="nquantity'+a+'" name="nquantity'+a+'" value="" onkeyup="hitungnilai('+a+');"></td>';
		    si_inner+='<td style="width:120px;"><input readonly style="text-align:right; width:120px;" type="text" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		    si_inner+='<td style="width:120px;"><input style="text-align:right; width:120px;" type="text" id="total'+a+'" name="total'+a+'" value=""></td>';
		    si_inner+='<td style="width:74px;"><input style="text-align:right; width:74px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody>';
    }
	    j=0;
	    var baris			    = Array();
      var ipricegroupco = Array();
  		var motif			    = Array();
	    var iproduct  		= Array();
	    var eproductname  = Array();
	    var nquantity 		= Array();
  		var vunitprice    = Array();
	    var total 		  	= Array();
      var eremark       = Array();
	    for(i=1;i<a;i++){
	      j++;
	      baris[j]			  = document.getElementById("baris"+i).value;
	      motif[j]			  = document.getElementById("motif"+i).value;
	      ipricegroupco[j]= document.getElementById("ipricegroupco"+i).value;
	      iproduct[j]			= document.getElementById("iproduct"+i).value;
	      eproductname[j]	= document.getElementById("eproductname"+i).value;
	      nquantity[j]	  = document.getElementById("nquantity"+i).value;
	      vunitprice[j]	  = document.getElementById("vunitprice"+i).value;
	      total[j]  	    = document.getElementById("total"+i).value;
        eremark[j]      = document.getElementById("eremark"+i).value;
	    }
	    document.getElementById("detailisi").innerHTML=si_inner;
	    j=0;
	    for(i=1;i<a;i++){
			  j++;
			  document.getElementById("baris"+i).value        = baris[j];
			  document.getElementById("motif"+i).value        = motif[j];
			  document.getElementById("ipricegroupco"+i).value     = ipricegroupco[j];
			  document.getElementById("iproduct"+i).value     = iproduct[j];
			  document.getElementById("eproductname"+i).value = eproductname[j];
			  document.getElementById("nquantity"+i).value    = nquantity[j];
			  document.getElementById("vunitprice"+i).value   = vunitprice[j];
			  document.getElementById("total"+i).value        = total[j];
        document.getElementById("eremark"+i).value      = eremark[j];
	    }
		  showModal("penjualankonsinyasi/cform/product/"+a+"/","#light");
		  jsDlgShow("#konten *", "#fade", "#light");
    }else{
	alert("Maksimal 30 item");
    }
  }
  function dipales(a){
    cek='false';
	  if((document.getElementById("dnotapb").value!='') &&
	 	   (document.getElementById("inotapb").value!='')) {
 	 	  if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
	 	  }else{
   			for(i=1;i<=a;i++){
		      if((document.getElementById("iproduct"+i).value=='') ||
			      (document.getElementById("eproductname"+i).value=='') ||
			      (document.getElementById("nquantity"+i).value=='')){
  			      alert('Data item masih ada yang salah !!!');
	  		      exit();
	  		      cek='false';
		      }else{
	  		      cek='true';	
		      } 
	      }
      }
	    if(cek=='true'){
    		document.getElementById("login").disabled=true;
		    document.getElementById("cmdtambahitem").disabled=true;
     	}else{
	       	document.getElementById("login").disabled=false;
	    }
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function view_area(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/penjualankonsinyasi/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function hitungnilai(){
    jml=document.getElementById("jml").value;
    bener=true;
		for(i=1;i<=jml;i++){
      qty=document.getElementById("nquantity"+i).value;
      if (isNaN(parseFloat(qty))){
        alert("Input harus numerik");
        bener=false;
        break;
      }
    }
    if(bener){
      tot=0;
      subtot=0;
 			for(i=1;i<=jml;i++){
        qty=parseFloat(formatulang(document.getElementById("nquantity"+i).value));
        hrg=parseFloat(formatulang(document.getElementById("vunitprice"+i).value));
        subtot=qty*hrg;
   		  document.getElementById("total"+i).value=formatcemua(subtot);
        tot=tot+subtot;
      }
	    document.getElementById("vnotapbgross").value=formatcemua(tot);
      dis=parseFloat(formatulang(document.getElementById("nnotapbdiscount").value));
      vdis=(tot*dis)/100;
 		  document.getElementById("vnotapbdiscount").value=formatcemua(vdis);
 		  document.getElementById("vnotapbnetto").value=formatcemua(tot-vdis);
    }
  }
  function diskonrupiah(){
    jml=document.getElementById("jml").value;
    bener=true;
		for(i=1;i<=jml;i++){
      qty=document.getElementById("nquantity"+i).value;
      if (isNaN(parseFloat(qty))){
        alert("Input harus numerik");
        bener=false;
        break;
      }
    }
    if(bener){
      tot=0;
      subtot=0;
 			for(i=1;i<=jml;i++){
        qty=parseFloat(formatulang(document.getElementById("nquantity"+i).value));
        hrg=parseFloat(formatulang(document.getElementById("vunitprice"+i).value));
        subtot=qty*hrg;
   		  document.getElementById("total"+i).value=formatcemua(subtot);
        tot=tot+subtot;
      }
	    document.getElementById("vnotapbgross").value=formatcemua(tot);
      vdis=parseFloat(formatulang(document.getElementById("vnotapbdiscount").value));
      dis=roundNumber((vdis*100)/tot,2);
 		  document.getElementById("nnotapbdiscount").value=dis;
 		  document.getElementById("vnotapbnetto").value=formatcemua(tot-vdis);
    }
  }
</script>
