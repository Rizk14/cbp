<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak="";
	foreach($isi as $row){
		$ab		= str_repeat(" ",9);
		$ipp 	= new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$cetak.=CHR(18);
    $tmp=explode("-",$dto);
    $th=$tmp[0];
    $bl=$tmp[1];
    $hr=$tmp[2];
    $tanggal=$hr." ".mbulan($bl)." ".$th;
    $tanggalteks=$hr." ".mbulan($bl)." ".$th;
		$cetak.=" DAFTAR NOTA YANG BELUM LUNAS \n";
    if($nt=='qqq'){
      $cetak.=" PER ".$tanggalteks."\n";
    }else{
      $cetak.=" PER Jatuh Tempo ".$tanggalteks."\n";
    }
    $cetak.=" Area : ".$row->e_area_name."\n";
    $cetak.=CHR(15)." =====================================================================================================================================\n";
    $cetak.=" | No.  |     No. Nota      |  Tgl. Nota   |   Tgl. JT    |     Jumlah     |  Sisa Tagihan  |  Lama  |          Keterangan            |\n"; 
    $cetak.=" =====================================================================================================================================\n";
		$i=0;	
	  break;
  }
    $jumhal=0;
    $jumsis=0;
    $totjum=0;
    $totsis=0;
	  foreach($isi as $rowi){
			$i++;
			$nota	= $rowi->i_nota;
		  $tmp=explode("-",$rowi->d_nota);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dnota=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		  $tmp=explode("-",$rowi->d_jatuh_tempo);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
	    $djt=$hr." ".substr(mbulan($bl),0,3)." ".$th;
			$nilai= number_format($rowi->v_nota_netto);
			$sisa	= number_format($rowi->v_sisa);
			$aw		= 4;
			$pjg	= strlen($i);
			for($xx=1;$xx<=$pjg;$xx++){
				$aw=$aw-1;
			}
      $anil=14;
      $pjg	= strlen($nilai);
			for($xx=1;$xx<=$pjg;$xx++){
				$anil=$anil-1;
			}
      $asis=14;
      $pjg	= strlen($sisa);
			for($xx=1;$xx<=$pjg;$xx++){
				$asis=$asis-1;
			}
      $lama=datediff("d",$rowi->d_jatuh_tempo,date("Y-m-d"),false);
      $alam=5;
      $pjg	= strlen($lama);
			for($xx=1;$xx<=$pjg;$xx++){
				$alam=$alam-1;
			}
      $jumhal=$jumhal+$rowi->v_nota_netto;
      $jumsis=$jumsis+$rowi->v_sisa;
      $totjum=$totjum+$rowi->v_nota_netto;
      $totsis=$totsis+$rowi->v_sisa;
			$cetak.=" |".str_repeat(" ",$aw).$i."  |  ".$nota."  |  ".$dnota."  |  ".$djt."  | ".str_repeat(" ",$anil).$nilai." | ".str_repeat(" ",$asis).$sisa." | ".str_repeat(" ",$alam).$lama."  |                                | \n";			
		}
    $jumhal=number_format($jumhal);
    $jumsis=number_format($jumsis);
    $totjum=number_format($totjum);
    $totsis=number_format($totsis);
    $ajum=14;
    $pjg	= strlen($jumhal);
		for($xx=1;$xx<=$pjg;$xx++){
			$ajum=$ajum-1;
		}
    $asis=14;
    $pjg	= strlen($jumsis);
		for($xx=1;$xx<=$pjg;$xx++){
			$asis=$asis-1;
		}
    $ajumtot=14;
    $pjg	= strlen($totjum);
		for($xx=1;$xx<=$pjg;$xx++){
			$ajumtot=$ajumtot-1;
		}
    $asistot=14;
    $pjg	= strlen($totsis);
		for($xx=1;$xx<=$pjg;$xx++){
			$asistot=$asistot-1;
		}
    $cetak.=" |------------------------------------------------------------------------------------------------------------------------------------|\n";
    $cetak.=" |     Jumlah Sub Total Halaman ini                       | ".str_repeat(" ",$ajum).$jumhal." | ".str_repeat(" ",$asis).$jumsis." |                                         |\n";
    $cetak.=" |------------------------------------------------------------------------------------------------------------------------------------|\n";
    $cetak.=" |     J u m l a h   T o t a l                            | ".str_repeat(" ",$ajumtot).$totjum." | ".str_repeat(" ",$asistot).$totsis." |                                         | \n";
    $cetak.=" |------------------------------------------------------------------------------------------------------------------------------------|\n";
    $ipp->setFormFeed();
		$cetak.=CHR(18);	
    echo $cetak;
#	  $ipp->setdata($cetak);
#	  $ipp->printJob();
#		echo "<script>this.close();</script>";
?>
