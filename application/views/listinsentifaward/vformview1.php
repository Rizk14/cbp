<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listinsentif/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    echo "<center><h2>PT. DIALOGUE GARMINDO UTAMA</h2></center>";
		echo "<center><h3>Insentif Salesman</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
?>
    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Salesman</th>
			    <th>Total Insentif</th>
	   	    <th>% Tgt Omset</th>
			    <th>% Reguler</th>
	   	    <th>Ins Oms</th>
	   	    <th>Ins Reg</th>
			    <th>Effectv Call</th>
	   	    <th>Ins</th>
			    <th>% Coll</th>
	   	    <th>Ins</th>
			    <th>Retur</th>
	   	    <th>Ins</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
			foreach($isi as $row){
        echo "<tr>
          <td align=right>$i</td>
          <td>$row->i_salesman - $row->e_salesman_name</td>
		      <td align=right>Rp. ".number_format($row->v_insentif)."</td>
          <td align=right>".number_format($row->n_omset)." %</td>
		      <td align=right>".number_format($row->n_omset_reguler)." %</td>
          <td align=right>RP. ".number_format($row->v_omset)."</td>
		      <td align=right>Rp. ".number_format($row->v_omset_reguler)."</td>
		      <td align=right>".number_format($row->n_effective_call)." %</td>
		      <td align=right>Rp. ".number_format($row->v_effective_call)."</td>
		      <td align=right>".number_format($row->n_collection)." %</td>
		      <td align=right>Rp. ".number_format($row->n_collection)."</td>
		      <td align=right>".$row->n_retur." %</td>
		      <td align=right>Rp. ".number_format($row->v_retur)."</td></tr>";
        $i++;
      }
		}
	      ?>
	    </tbody>
	  </table>
    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listinsentif/cform/index","#main");' ></center>
</div>
