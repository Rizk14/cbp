<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url'=>'listtargetcollectionrealtime/cform/export','update'=>'#main','type'=>'post'));?>
				<div class="effect">
					<div class="accordion2">
						<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
    if($b!='') $periode=mbulan($b)." - ".$a;
	?>
						<input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
						<?php 
  	echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Daftar Insentif Sales Award</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
	?>
						<table class="listtable" border=none>
							<thead>
								<tr>
									<th rowspan="2">No</th>
									<th rowspan="2">Periode</th>
									<th rowspan="2">Area</th>
									<th rowspan="2">Salesman</th>
									<th colspan="3">Collection(Ytd)</th>
									<th colspan="3">Selling Out(Ytd)</th>
									<th colspan="3">OA</th>
									<th colspan="3">Sales Qty(Unit)</th>
									<th colspan="3">Net Sales(Rp)</th>
								</tr>
								<tr>
									<th>Target</th>
									<th>Realisasi</th>
									<th>%</th>
									<th>Target</th>
									<th>Realisasi</th>
									<th>%</th>
									<th>2018</th>
									<th>2019</th>
									<th>Growth</th>
									<th>2018</th>
									<th>2019</th>
									<th>Growth</th>
									<th>2018</th>
									<th>2019</th>
									<th>Growth</th>
								</tr>
							</thead>
							<tbody>
								<?php if($isi){
									$no = 1;
									foreach ($isi as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->i_periode; ?></td>
									<td><?= $row->e_area_name; ?></td>
									<td><?= $row->e_salesman_name;?> </td>
									<td><?= $row->v_targetcoll; ?></td>
									<td><?= $row->v_realcoll; ?></td>
									<td><?= $row->n_realcoll; ?></td>
									<td><?= $row->v_targetsls; ?></td>
									<td><?= $row->v_realsls; ?></td>
									<td><?= $row->n_realsls; ?></td>
									<td><?= $row->n_lastoa; ?></td>
									<td><?= $row->n_nowoa; ?></td>
									<td><?= $row->n_growthoa; ?></td>
									<td><?= $row->n_lastslsqty; ?></td>
									<td><?= $row->n_nowslsqty; ?></td>
									<td><?= $row->n_growthslsqty; ?></td>
									<td><?= $row->v_lastnetsls; ?></td>
									<td><?= $row->v_nownetsls; ?></td>
									<td><?= $row->n_growthnetsls; ?></td>
								</tr>
								<?php }
								};
								?>
							</tbody>
						</table>
					</div>
