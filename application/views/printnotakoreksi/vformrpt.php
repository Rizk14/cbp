<?php 
	require_once ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab	= str_repeat(" ",9);
		$ipp 	= new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
   	$cetak.=CHR(18);    	
		$alm	= strlen(trim($row->e_customer_address));
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).NmPerusahaan.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."                  Kepada Yth.\n";		
		$cetak.=$nor.AlmtPerusahaan.",".KotaPerusahaan."                 ".rtrim($row->e_customer_name)."\n";		
		if($alm<35){
			$cetak.=$nor."Telp.: ".TlpPerusahaan."                         ".trim($row->e_customer_address)."\n";
		}else{
			$cetak.=$nor."Telp.: ".TlpPerusahaan."                         ".CHR(15).trim($row->e_customer_address).CHR(18)."\n";
		}		
		$cetak.=$nor."Fax  : ".FaxPerusahaan."                         ".rtrim($row->e_customer_city)."\n";		
		if($row->f_customer_pkp=='t'){
			$cetak.=$nor."NPWP : ".NPWPPerusahaan."                 ".$row->e_customer_pkpnpwp."\n";
		}else{
			$cetak.=$nor."NPWP : ".NPWPPerusahaan."                 "."\n";
		}		
		$cetak.=$nor.CHR(27).CHR(71)."BCA CABANG CIMAHI - BANDUNG\n";		
		$cetak.=$nor."NO.AC: 139.300.1236".CHR(27).CHR(72)."\n\n";		
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14)."             NOTA PENJUALAN".CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n\n";		
		$cetak.=$nor."NO PO:".$row->i_spb_po.str_repeat(" ",18)."No.FAK. / No.SPB    : ".trim($row->i_nota)."/".$row->i_spb."\n";		
		$cetak.=$nor.str_repeat(" ",24)."KODE SALES/KODELANG : ".$row->i_salesman."/".$row->i_customer."\n";		
		$xxx=datediff('d',$row->d_nota,$row->d_jatuh_tempo,false);
		if(($xxx)>0){
			$cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN     : ".$xxx." hari SETELAH BARANG DITERIMA\n";
		}else{
			$cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN     : "."TUNAI\n";
		}		
		$cetak.="\n";		
		$cetak.=$nor.CHR(218).str_repeat(CHR(196),77).CHR(191)."\n";		
		$cetak.=$nor.CHR(179)."No.  KD-BARANG       NAMA BARANG                     UNIT   HARGA     JUMLAH ".CHR(179)."\n";		
		$cetak.=$nor.CHR(212).str_repeat(CHR(205),77).CHR(190).CHR(15)."\n";		
    $total=0;
		foreach($detail as $rowi){
			if($rowi->n_deliver>0){
				$i++;
				$j++;
				$prod	= $rowi->i_product;
				$name	= $rowi->e_product_name." ".str_repeat(".",66-strlen($rowi->e_product_name ));
				$deli	= number_format($rowi->n_deliver);
				$pjg	= strlen($deli);
				$spcdel	= 7;
				for($xx=1;$xx<=$pjg;$xx++){
					$spcdel	= $spcdel-1;
				}
				if($row->f_plus_ppn=='t'){
					$pric	= number_format($rowi->v_unit_price);
				}else{
					$pric	= number_format($rowi->v_unit_price/1.1);
				}
				$pjg	= strlen($pric);
				$spcpric= 18;
				for($xx=1;$xx<=$pjg;$xx++){
					$spcpric= $spcpric-1;
				}
				if($row->f_plus_ppn=='t'){
					$tot	= number_format($rowi->n_deliver*$rowi->v_unit_price);
				}else{
					$tot	= number_format($rowi->n_deliver*($rowi->v_unit_price/1.1));
				}
				$pjg	= strlen($tot);
				$spctot = 19;
				for($xx=1;$xx<=$pjg;$xx++){
					$spctot	= $spctot-1;
				}
				$aw=13;
				$pjg	= strlen($i);
				for($xx=1;$xx<=$pjg;$xx++){
					$aw=$aw-1;
				}
				$aw=str_repeat(" ",$aw);
        $total	= $total+str_replace(',','',$tot);
				$cetak.=CHR(15).$aw.$i.str_repeat(" ",5).$prod.str_repeat(" ",5).$name.str_repeat(" ",$spcdel).$deli.str_repeat(" ",$spcpric).$pric.str_repeat(" ",$spctot).$tot."\n";				
			}
			if($jml>27){
				if(($i%43)==0){
					$ipp->setFormFeed();
					$cetak.=CHR(18).$nor.str_repeat("-",78)."\n";					
					CetakHeader($row,$nor,$abn,$ab,$ipp,$cetak);
					$j	= 0;
				}elseif(
					($i==$jml)
					){
					$ipp->setFormFeed();
					$cetak.=CHR(18).$nor.str_repeat("-",78)."\n";					
					CetakHeader($row,$nor,$abn,$ab,$ipp,$cetak);
					$j	= 0;
				}
			}
		}
		$cetak.=CHR(18).$nor.str_repeat("-",78)."\n";		
    $row->v_nota_gross=$total;
		if(strlen(number_format($row->v_nota_gross))==1){
			$cetak.=str_repeat(" ",51)."TOTAL        :                ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==6){
			$cetak.=str_repeat(" ",51)."TOTAL        :           ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==7){
			$cetak.=str_repeat(" ",51)."TOTAL        :          ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==8){
			$cetak.=str_repeat(" ",51)."TOTAL        :         ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==9){
			$cetak.=str_repeat(" ",51)."TOTAL        :        ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==10){
			$cetak.=str_repeat(" ",51)."TOTAL        :       ".number_format($row->v_nota_gross)."\n";			
		}elseif(strlen(number_format($row->v_nota_gross))==11){
			$cetak.=str_repeat(" ",51)."TOTAL        :      ".number_format($row->v_nota_gross)."\n";			
		}
    $vdisc1=0;
    $vdisc2=0;
    $vdisc3=0;
    $vdisc4=0;
    $vdisc1=round($vdisc1+($total*$row->n_nota_discount1)/100);
	  $vdisc2=round($vdisc2+((($total-$vdisc1)*$row->n_nota_discount2)/100));
	  $vdisc3=round($vdisc3+((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100));
  	$vdisc4=round($vdisc4+((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100));
    $vdistot	= round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
    $row->v_nota_discount=$vdistot;
		if(strlen(number_format($row->v_nota_discount))==1){
			$cetak.=str_repeat(" ",51)."POTONGAN     :                ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==5){
			$cetak.=str_repeat(" ",51)."POTONGAN     :            ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==6){
			$cetak.=str_repeat(" ",51)."POTONGAN     :           ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==7){
			$cetak.=str_repeat(" ",51)."POTONGAN     :          ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==8){
			$cetak.=str_repeat(" ",51)."POTONGAN     :         ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==9){
			$cetak.=str_repeat(" ",51)."POTONGAN     :        ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==10){
			$cetak.=str_repeat(" ",51)."POTONGAN     :       ".number_format($row->v_nota_discount)."\n";			
		}elseif(strlen(number_format($row->v_nota_discount))==11){
			$cetak.=str_repeat(" ",51)."POTONGAN     :      ".number_format($row->v_nota_discount)."\n";			
		}
		if($row->f_plus_ppn=='f'){
      $vppn=($total-$vdistot)*0.1;
      $row->v_nota_ppn=$vppn;
			if(strlen(number_format($row->v_nota_ppn))==1){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :                ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==5){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :            ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==6){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :           ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==7){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :          ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==8){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :         ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==9){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :        ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==10){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :       ".number_format($row->v_nota_ppn)."\n";				
			}elseif(strlen(number_format($row->v_nota_ppn))==11){
				$cetak.=str_repeat(" ",51)."PPN (10%)    :      ".number_format($row->v_nota_ppn)."\n";				
			}
		}
		$cetak.=str_repeat(" ",66).str_repeat("-",16).CHR(" ").CHR("-")."\n";		
    if($row->f_plus_ppn=='f'){
      $row->v_nota_netto=$total-$vdistot+$vppn;
		  if(strlen(number_format($row->v_nota_netto))==1){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :                ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==6){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :           ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==7){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :          ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==8){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :         ".number_format($row->v_nota_netto).CHR(15)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==9){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :        ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==10){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :       ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==11){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :      ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }
    }else{
		  if(strlen(number_format($row->v_nota_netto))==1){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :                ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==6){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :           ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==7){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :          ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==8){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :         ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==9){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :        ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==10){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :       ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }elseif(strlen(number_format($row->v_nota_netto))==11){
			  $cetak.=str_repeat(" ",51)."NILAI FAKTUR :      ".number_format($row->v_nota_netto).CHR(15)."\n\n";			  
		  }
    }
		$bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($row->v_nota_netto));
		$cetak.=$ab."(".$kata." Rupiah)".CHR(18)."\n";		
		$tmp=explode("-",$row->d_nota);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dnota=$hr." ".mbulan($bl)." ".$th;
		$cetak.=str_repeat(" ",50)."Bandung, ".$dnota."\n";		
		$cetak.=str_repeat(" ",3)."        Penerima                                   S E & O\n";
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(218).str_repeat(CHR(196),21).CHR(191).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(179)."     P E N T I N G   ".CHR(179).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(179)."  CLAIM KEKURANGAN / ".CHR(179).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(179)."   TOLAKAN BARANG    ".CHR(179).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(179)."  MAX 7 HARI SETELAH ".CHR(179).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(179)."   BARANG DI TERIMA  ".CHR(179).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=CHR(27).CHR(120).CHR(1).str_repeat(" ",25).CHR(192).str_repeat(CHR(196),21).CHR(217).CHR(27).CHR(120).CHR(0)."\n";		
		$cetak.=str_repeat(" ",3)."    (              )                            ( "./*$ttd1*/"Ani Noviani"." )\n".CHR(15);		
		$cetak.=$ab."Catatan :\n";		
		$cetak.=$ab."1. Barang-barang yang sudah dibeli tidak dapat ditukar/dikembalikan, kecuali ada perjanjian terlebih dahulu\n";		
		if($row->f_plus_ppn=='t'){
			$cetak.=$ab."2. Faktur asli merupakan bukti pembayaran yang sah. (Harga sudah termasuk PPN)\n";
		}else{
			$cetak.=$ab."2. Faktur asli merupakan bukti pembayaran yang sah.\n";
		}
		$ipp->setFormFeed();
		$cetak.=$ab."3. Pembayaran dengan cek/giro berharga baru dianggap sah setelah diuangkan/cair.".CHR(18)."\n";		
#		$ipp->setBinary();
	}
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";
	function CetakHeader($row,$nor,$abn,$ab,$ipp,$cetak){
#		$ipp = new PrintIPP();
#		$ipp->setHost("localhost");
#		$ipp->setPrinterURI("/printers/Epson-lq-1170-24-pin");
#		$ipp->setHost($host);
#		$ipp->setPrinterURI($uri);
#		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$cetak.=CHR(18);		
		$alm	= strlen(trim($row->e_customer_address));
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).NmPerusahaan.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."                  Kepada Yth.\n";		
		$cetak.=$nor.AlmtPerusahaan."                 ".rtrim($row->e_customer_name)."\n";		
		if($alm<35){
			$cetak.=$nor.TlpPerusahaan."                         ".trim($row->e_customer_address)."\n";
		}else{
			$cetak.=$nor.TlpPerusahaan."                         ".CHR(15).trim($row->e_customer_address).CHR(18)."\n";
		}
		$cetak.=$nor.FaxPerusahaan."                         ".rtrim($row->e_customer_city)."\n";		
		if($row->f_customer_pkp=='t'){
			$cetak.=$nor.NPWPPerusahaan."                 ".$row->e_customer_pkpnpwp."\n";
		}else{
			$cetak.=$nor.NPWPPerusahaan."                 "."\n";
		}
		$cetak.=$nor.CHR(27).CHR(71)."BCA CABANG CIMAHI - BANDUNG\n";		
		$cetak.=$nor."NO.AC: 139.300.1236".CHR(27).CHR(72)."\n";		
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).CHR(14)."             NOTA PENJUALAN".CHR(20).CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n";		
		$cetak.=$nor."NO PO:".$row->i_spb_po.str_repeat(" ",18)."No.FAK. / No.SPB : ".trim($row->i_nota)."/".$row->i_spb."\n";		
		$cetak.=$nor.str_repeat(" ",24)."KODE SALES/KODELANG : ".$row->i_salesman."/".$row->i_area.$row->i_customer."\n";		
		if(($row->d_jatuh_tempo-$row->d_nota)>0){
			$cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN : ".$row->d_jatuh_tempo-$row->d_nota." hari SETELAH BARANG DITERIMA\n";
		}else{
			$cetak.=$nor.str_repeat(" ",24)."MASA PEMBAYARAN : "."TUNAI\n";
		}		
		$cetak.=$nor.CHR(218).str_repeat(CHR(196),77).CHR(191)."\n";		
		$cetak.=$nor.CHR(179)."No.  KD-BARANG       NAMA BARANG                     UNIT   HARGA     JUMLAH ".CHR(179)."\n";		
		$cetak.=$nor.CHR(212).str_repeat(CHR(205),77).CHR(190).CHR(15)."\n";		
	}
?>
