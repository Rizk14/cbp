<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'bbmap/cform/update','id'=>'bbmapformupdate','update'=>'#pesan','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>No OP</td>
		<td><input readonly id="iop" name="iop" onclick="view_op()" value="<?php echo $isi->i_op; ?>"></td>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td>No AP</td>
		<td><input readonly id="iap" name="iap" type="text" maxlength="14" value="<?php echo $isi->i_ap; ?>"></td>
		<td>Tanggal AP</td>
		<?php 
			$tmp=explode("-",$isi->d_ap);
			$th =$tmp[0];
			$bl =$tmp[1];
			$hr =$tmp[2];
			$dap=$hr."-".$bl."-".$th;
			
			// 21-08-2014, cek apakah udh ada nota/AP utk BBM-AP ini. jika udh ada, maka tombol simpan ga akan muncul
			$sqlxx = " SELECT distinct a.i_dtap, a.d_dtap FROM tm_dtap a 
							INNER JOIN tm_dtap_item b ON a.i_dtap = b.i_dtap
							WHERE b.i_do='$isi->i_ap' AND b.i_supplier = '$isi->i_supplier' ";
			$queryxx	= $this->db->query($sqlxx);
			
			if ($queryxx->num_rows() > 0){
				$is_ada_nota = "y";
			}
			else {
				$is_ada_nota = "t";
			}
		?>
		<input hidden id="bap" name="bap" value="<?php echo $bl; ?>">
		<td><input readonly id="dap" name="dap" onclick="showCalendar('',this,this,'','dap',0,20,1)" value="<?php echo $dap; ?>">
        <input hidden id="tglap" name="tglap" value="<?php echo $dap; ?>">
			<input id="iapold" name="iapold" type="text" maxlength="10" value="<?php echo $isi->i_ap_old; ?>">
		</td>
	      </tr>
	      <tr>
		<td>Pemasok</td>
		<td><input readonly id="esuppliername" name="esuppliername" onclick="view_supplier()" value="<?php echo $isi->e_supplier_name; ?>">
		    <input id="isupplier" name="isupplier" type="hidden" value="<?php echo $isi->i_supplier; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input style="text-align:right;" readonly id="vapgross" name="vapgross" value="<?php echo number_format($isi->v_ap_gross); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
        <?php if($isi->f_ap_cancel=='f' && $is_ada_nota == "t"){ ?>
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
        <?php }?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listbbmap/cform/view/<?php echo $dfrom."/".$dto."/AS/"; ?>","#main");'>
        <?php if($isi->f_ap_cancel=='f' && $is_ada_nota == "t"){ ?>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"><?php }?></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:70px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:90px;" align="center">Harga</th>
					<th style="width:46px;" align="center">Jml Terima</th>
					<th style="width:94px;" align="center">Total</th>
					<th style="width:32px;" align="center" class="action">Action</th>
				</table></div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
					echo '<table class="listtable" align="center" style="width:750px;">';
				  	$i++;
					$pangaos=number_format($row->v_product_mill);
					$toti=$row->v_product_mill*$row->n_receive;
					$total=number_format($toti);
				  	echo "<tbody>
							<tr>
		    				<td style=\"width:22px;\"><input style=\"width:22px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"> <input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td style=\"width:63px;\"><input style=\"width:63px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:264px;\"><input style=\"width:264px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:89px;\"><input readonly style=\"width:89px;\" type=\"text\" 
								id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:81px;\"><input readonly style=\"text-align:right; 
								width:81px;\"  type=\"text\" id=\"vproductmill$i\"
								name=\"vproductmill$i\" value=\"$pangaos\"></td>
							<td style=\"width:49px;\"><input style=\"text-align:right; width:49px;\" 
								type=\"text\" id=\"nreceive$i\" name=\"nreceive$i\" value=\"$row->n_receive\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem')\">
                <input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_receive\"></td>
							<td style=\"width:85px;\"><input style=\"text-align:right; width:85px;\" readonly
								type=\"text\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
							<td style=\"width:60px;\" align=\"center\">";
            if($isi->f_ap_cancel=='f'){
              echo "
								<a href=\"#\" onclick='hapus(\"bbmap/cform/deletedetail/$row->i_ap/$row->i_supplier/$row->i_product/$row->i_product_grade/$row->i_product_motif/$toti/$isi->i_op/$isi->i_area/$th/$dap/\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
            }
							echo	"</td>
							</tr>
						  </tbody></table>";
				}
				?>
			</div>
			<?php 
				echo "<input type=\"hidden\" id=\"iapdelete\" name=\"iapdelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"isupplierdelete\" name=\"isupplierdelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
		      		  <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"iproductdelete\" name=\"iproductdelete\" value=\"\">
		     		 ";
			?>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $i; ?>">
	  </div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/bbmap/cform/supplier/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_op(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/bbmap/cform/op/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
		so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		so_inner+= '<th style="width:100px;" align="center">Motif</th>';
		so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
		so_inner+= '<th style="width:46px;"  align="center">Jml kirim</th>';
		so_inner+= '<th style="width:94px;"  align="center">Total</th>';
		so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr></table>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" style="width: 750px;" align="center"><tbody><tr><td style="width:22px;"><input style="width:22px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:63px;"><input style="width:63px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:269px;"><input style="width:269px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:91px;"><input readonly style="width:91px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:81px;"><input readonly style="text-align:right; width:81px;"  type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="nreceive'+a+'" name="nreceive'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td><td style="width:40px;">&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" style="width: 750px;" align="center"><tbody><tr><td style="width:22px;"><input style="width:22px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:63px;"><input style="width:63px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:269px;"><input style="width:269px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:91px;"><input readonly style="width:91px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:81px;"><input readonly style="text-align:right; width:81px;" type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:43px;"><input style="text-align:right; width:43px;" type="text" id="nreceive'+a+'" name="nreceive'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:40px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris			=Array()
    var iproduct		=Array();
    var eproductname	=Array();
	var vproductmill	=Array();
    var nreceive		=Array();
	var motif			=Array();
	var motifname		=Array();
    var vtotal			=Array();
    for(i=1;i<a;i++){
	j++;
	baris[j]			=document.getElementById("baris"+i).value;
	iproduct[j]			=document.getElementById("iproduct"+i).value;
	eproductname[j]		=document.getElementById("eproductname"+i).value;
	vproductmill[j]		=document.getElementById("vproductmill"+i).value;
	nreceive[j]			=document.getElementById("nreceive"+i).value;
	motif[j]			=document.getElementById("motif"+i).value;
	motifname[j]		=document.getElementById("emotifname"+i).value;
	vtotal[j]			=document.getElementById("vtotal"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vproductmill"+i).value=vproductmill[j];
		document.getElementById("nreceive"+i).value=nreceive[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("emotifname"+i).value=motifname[j];
		document.getElementById("vtotal"+i).value=vtotal[j];
    }
	op=document.getElementById("iop").value;
    lebar =500;
    tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/bbmap/cform/productupdate/"+a+"/"+op,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dap").value!='') &&
  	 	(document.getElementById("isupplier").value!='') &&
  	 	(document.getElementById("iap").value!='') &&
		(document.getElementById("iop").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("nreceive"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
  	  		document.getElementById("cmdtambahitem").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function hitungnilai(isi,jml){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductmill"+i).value);
			nqty=formatulang(document.getElementById("nreceive"+i).value);
			vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		}
		document.getElementById("vapgross").value=formatcemua(vtot);
	}
  }
  function xxx(a,b,c,d,e,f,g,h){
    if (confirm(h)==1){
		document.getElementById("iapdelete").value=a;
		document.getElementById("isupplierdelete").value=b;
		document.getElementById("iproductdelete").value=c;
		document.getElementById("iproductgradedelete").value=d;
		document.getElementById("iproductmotifdelete").value=e;
		vtot	=parseFloat(formatulang(document.getElementById("vapgross").value));
		vmin	=parseFloat(formatulang(f))*parseFloat(formatulang(g));
		vtot	=vtot-vmin;
		document.getElementById("vapgross").value=formatcemua(vtot);
		formna=document.getElementById("bbmapformupdate");
		formna.action		="<?php echo site_url(); ?>"+"/bbmap/cform/deletedetail/"+vtot;
		formna.submit();
    }
  }
  function mbatal(a){
	self.location=a;
  }
  function afterSetDateValue(ref_field, target_field, date) {
    dspb=document.getElementById('dap').value;
    bspb=document.getElementById('bap').value;
    dtmp=dspb.split('-');
    per=dtmp[2]+dtmp[1]+dtmp[0];
    bln = dtmp[1];
    if( (bspb!='') && (dspb!='') ){
      if(bspb != bln)
      {
        alert("Tanggal AP tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("dap").value=document.getElementById("tglap").value;
      }
    }
  }
</script>
