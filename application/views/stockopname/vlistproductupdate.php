<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('stockopname/cform/cariproductupdate/'.$baris.'/'.$istore.'/'.$istorelocation, array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"></td>
	      </tr>
	    </thead>
      	    <th>Kode Product</th>
	    	<th>Nama</th>
	    	<th>Motif</th>
	    	<th>Grade</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_product','$row->e_product_name','$row->i_product_grade','$row->i_product_motif','$row->e_product_motifname','$baris')\">$row->i_product</a></td>
				  <td><a href=\"javascript:setValue('$row->i_product','$row->e_product_name','$row->i_product_grade','$row->i_product_motif','$row->e_product_motifname','$baris')\">$row->e_product_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_product','$row->e_product_name','$row->i_product_grade','$row->i_product_motif','$row->e_product_motifname','$baris')\">$row->e_product_motifname</a></td>
				  <td><a href=\"javascript:setValue('$row->i_product','$row->e_product_name','$row->i_product_grade','$row->i_product_motif','$row->e_product_motifname','$baris')\">$row->i_product_grade</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,x,y,d)
  {
    ada=false;
    for(i=1;i<=d;i++){
	if((a==opener.document.getElementById("iproduct"+i).value)
	   && (c == opener.document.getElementById("iproductgrade"+i).value)
	   && (i!=d)){
		alert ("kode : "+a+" sudah ada !!!!!");
		ada=true;
		break;
	}else{
		ada=false;	   
	}
    }
    if(!ada){
	opener.document.getElementById("iproduct"+d).value=a;
	opener.document.getElementById("eproductname"+d).value=b;
	opener.document.getElementById("iproductgrade"+d).value=c;
	opener.document.getElementById("iproductmotif"+d).value=x;
	opener.document.getElementById("eproductmotifname"+d).value=y;
	this.close();
    }
	
  }

//  window.onunload=function(){
  function bbatal(){
	baris	= document.getElementById("baris").value;
	si_inner= opener.document.getElementById("detailisi").innerHTML;
	var temp= new Array();
	temp	= si_inner.split('<table class="listtable" style="width: 750px;"><tbody>');
	if(opener.document.getElementById("iproduct"+baris).value==''){
		si_inner='';
		for(x=1;x<baris;x++){
			si_inner=si_inner+'<table class="listtable" style="width: 750px;"><tbody>'+temp[x];
		}
		j=0;
		var iproduct			=Array();
		var iproductgrade		=Array();
		var iproductmotif		=Array();
		var eproductname		=Array();
		var eproductmotifname	=Array();
		var nstockopname		=Array();
		for(i=1;i<baris;i++){
			j++;
			iproduct[j]	=opener.document.getElementById("iproduct"+i).value;
			iproductgrade[j]=opener.document.getElementById("iproductgrade"+i).value;
			iproductmotif[j]=opener.document.getElementById("iproductmotif"+i).value;
			eproductname[j]	=opener.document.getElementById("eproductname"+i).value;
			eproductmotifname[j]	=opener.document.getElementById("eproductmotifname"+i).value;
			nstockopname[j]	=opener.document.getElementById("nstockopname"+i).value;	
		}
		opener.document.getElementById("detailisi").innerHTML=si_inner;
		j=0;
		for(i=1;i<baris;i++){
			j++;
			opener.document.getElementById("iproduct"+i).value=iproduct[j];
			opener.document.getElementById("iproductgrade"+i).value=iproductgrade[j];
			opener.document.getElementById("iproductmotif"+i).value=iproductmotif[j];
			opener.document.getElementById("eproductname"+i).value=eproductname[j];
			opener.document.getElementById("eproductmotifname"+i).value=eproductmotifname[j];
			opener.document.getElementById("nstockopname"+i).value=nstockopname[j];
		}
		opener.document.getElementById("jml").value=parseFloat(opener.document.getElementById("jml").value)-1;

	}
	this.close();
  }
</script>
