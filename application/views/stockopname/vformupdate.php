<table class="maintable">
	<tr>
    	<td align="left">
			<?php echo form_open('stockopname/cform/update', array('id' => 'masterstockopnameformupdate', 'name' => 'masterstockopnameformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
			<div class="effect">
	  			<div class="accordion2">
	    			<table class="mastertable">
	      				<tr>
							<td width="19%">No Stok Opname</td>
							<td width="1%">:</td>
							<td width="30%"><?php 
									$data = array(
											  'name'        => 'istockopname',
											  'id'          => 'istockopname',
											  'value'       => $isi->i_stockopname,
										  	  'readonly'    => 'true',
											  'maxlength'   => '10');
									echo form_input($data);?></td>
							<td width="19%">Gudang</td>
							<td width="1%">:</td>
							<td width="30%"><?php 
									$data = array(
											  'name'        => 'estorename',
											  'id'          => 'estorename',
											  'value'       => $isi->e_store_name,
										  	  'readonly'	    => 'true',
											  'onclick'     => "view_store()");
									echo form_input($data);?>
									<input type="hidden" id="istore" name="istore" value="<?php echo $isi->i_store; ?>">
									<input type="hidden" id="iarea" name="iarea" value="<?php echo $isi->i_area; ?>"></td>
	      				</tr>
	      				<tr>
							<td width="19%">Tgl Akhir Periode</td>
							<td width="1%">:</td>
							<td width="30%"><?php 
									$data = array(
											  'name'        => 'dstockopname',
											  'id'          => 'dstockopname',
											  'value'       => $isi->d_stockopname,
										  	  'readonly'    => 'true',
										  	  'onclick'     => "showCalendar('',this,this,'','dstockopname',0,20,1)"   );
											  
									echo form_input($data);?></td>
							<td width="19%">Lokasi Gudang</td>
							<td width="1%">:</td>
							<td width="30%"><?php 
									$data = array(
											  'name'        => 'estorelocationname',
											  'id'          => 'estorelocationname',
											  'value'       => $isi->e_store_locationname,
										  	  'readonly'    => 'true',
										  	  'onclick'     => "view_store()");
									echo form_input($data);?>
									<input type="hidden" id="istorelocation" name="istorelocation" value="<?php echo $isi->i_store_location?>"></td>
	      				</tr>
	      				<tr>
							<td width="19%">&nbsp;</td>
							<td width="1%">&nbsp;</td>
							<td colspan="4" width="80%">
<?php 
              $bisaedit=true;
              $sql=" e_mutasi_periode from tm_mutasi_header where i_stockopname_awal='$isi->i_stockopname' and i_store='$isi->i_store'";
              $this->db->select($sql, false);
              $query = $this->db->get();
              if ($query->num_rows() > 0){
                foreach($query->result() as $row){
                  $sql=" i_product from tm_mutasi
                         where e_mutasi_periode='$row->e_mutasi_periode' and i_store='$isi->i_store' and 
                         i_store_location='$isi->i_store_location' and (n_mutasi_pembelian>0 or n_mutasi_returoutlet>0 or
                         n_mutasi_bbm>0 or n_mutasi_penjualan>0 or n_mutasi_returpabrik>0 or n_mutasi_bbk>0)";
                  $this->db->select($sql, false);
                  if ($query->num_rows()>0){
                    $bisaedit=false;
                  }
                }
              }
              if($bisaedit){?>
							  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value)+1);">
              <?php }?>
							  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("liststockopname/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'>
              <?php if($bisaedit){?>
							  <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1,document.getElementById('istore').value,document.getElementById('istorelocation').value );">
              <?php }?>
							</td>
	      				</tr>
	    			</table>
					<div id="detailheader" align="center">
						<table class="listtable" style="width:750px;">
						  <th width="40px" align="center">No</th>
						  <th width="87px" align="center">Kd Barang</th>
						  <th width="50px"align="center">Grade</th>
						  <th width="300px" align="center">Nama Barang</th>
						  <th width="100px" align="center">Motif</th>
						  <th width="50px" align="center">Jumlah</th>
<!--					 	  <th width="50px" align="center" class="action">Action</th>-->
						</table>
					</div>
					<div id="detailisi" align="center">
					<?php 
						$i=0;
						$x=0;
						foreach($detail as $row){
						  $i++;
						  echo "<table class=\"listtable\" style=\"width:750px;\">";
						  echo "<tbody><tr>
						  <td><input style=\"width:40px;text-align:right;\" readonly type=\"text\" id=\"no$i\" name=\"no$i\" value=\"$i\"></td>
							<td style=\"width:99px;\">
								<input type=\"hidden\" id=\"iproductmotif$i\" name=\"iproductmotif$i\" value=\"$row->i_product_motif\">
							  	<input style=\"width:99px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\">
							</td>
							<td style=\"width:58px;\">
							  	<input style=\"width:56px;\" readonly type=\"text\" id=\"iproductgrade$i\" name=\"iproductgrade$i\" value=\"$row->i_product_grade\">			</td>
							<td style=\"width:342px;\">
								<input style=\"width:342px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\">			</td>
							<td style=\"width:112px;\">
								<input style=\"width:112px;\" readonly type=\"text\" id=\"eproductmotifname$i\" name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:65px;\">
								<input style=\"width:62px; text-align:right;\" type=\"text\" id=\"nstockopname$i\" name=\"nstockopname$i\"i value=\"$row->n_stockopname\"></td>";
						  $x=$x+$row->n_stockopname;
#							<td style=\"width:160px;\" align=\"center\" >";
#							if($bisaedit){ 
#                echo "<a href=\"#\" onclick='hapus(\"stockopname/cform/deletedetail/$row->i_stockopname/$row->i_product_motif/$row->i_product/$row->i_product_grade/$row->i_store/$row->i_store_location/$row->i_store_locationbin/$iarea/$dfrom/$dto/\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
#              }
#              echo "</td>
						  echo "</tr></tbody></table>";
						}
						echo "<input type=\"hidden\" id=\"istockopnamedelete\" 		name=\"istockopnamedelete\" 	value=\"\">
							  <input type=\"hidden\" id=\"iproductmotifdelete\" 	name=\"iproductmotifdelete\" 	value=\"\">
							  <input type=\"hidden\" id=\"iproductdelete\" 			name=\"iproductdelete\" 		value=\"\">
							  <input type=\"hidden\" id=\"iproductgradedelete\" 	name=\"iproductgradedelete\" 	value=\"\">
							  <input type=\"hidden\" id=\"istoredelete\" 			name=\"istoredelete\" 			value=\"\">
							  <input type=\"hidden\" id=\"istorelocationdelete\" 	name=\"istorelocationdelete\" 	value=\"\">
							  <input type=\"hidden\" id=\"istorelocationbindelete\" name=\"istorelocationbindelete\"value=\"\">
							 ";
					?>
					</div>
					<div id="pesan"></div>
					<input type="hidden" name="jml" id="jml" 
					<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	  			</div>
			</div>
		<?=form_close()?>
    	</td>
  	</tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,e,f,g,h){
    if (confirm(h)==1){
	  document.getElementById("istockopnamedelete").value	= a;
	  document.getElementById("iproductmotifdelete").value	= b;
	  document.getElementById("iproductdelete").value		= c;
	  document.getElementById("iproductgradedelete").value	= d;
	  document.getElementById("istoredelete").value			= e;
	  document.getElementById("istorelocationdelete").value	= f;
	  document.getElementById("istorelocationbindelete").value = g;
	  formna=document.getElementById("masterstockopnameformupdate");
	  formna.action="<?php echo site_url(); ?>"+"/stockopname/cform/deletedetail";
	  formna.submit();
    }
  }
  function view_store(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/stockopname/cform/store/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a,b,c){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		  so_inner='<table id="itemtem" class="listtable" style="width:750px;"><th width="87px" align="center">Kd Barang</th><th width="50px" align="center">Grade</th><th width="300px" align="center">Nama Barang</th><th width="100px" align="center">Motif</th><th width="50px" align="center">Jumlah</th><th width="50px" align="center" class="action">Action</th>';
		  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
  		so_inner='';
    }
    if(si_inner==''){
		  si_inner='<table class="listtable" style="width:750px;"><tbody><tr><td style="width:97px;"><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input style="width:96px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td style="width:56px;"><input style="width:54px;" readonly type="text" id="iproductgrade'+a+'" name="iproductgrade'+a+'" value=""></td><td style="width:338px;"><input style="width:336px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td style="width:112px;"><input style="width:110px;" readonly type="text" id="eproductmotifname'+a+'" name="eproductmotifname'+a+'" value=""></td><td style="width:65px;"><input style="text-align:right; width:62px;" type="text" id="nstockopname'+a+'" name="nstockopname'+a+'" value="" onkeyup="reformat(this);"></td><td style="width:160px;">&nbsp;</td></tr></tbody></table>';
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
    }else{
  		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		  si_inner=si_inner+'<table class="listtable" style="width:750px;"><tbody><tr><td style="width:97px;"><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input style="width:96px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td style="width:56px;"><input style="width:54px;" readonly type="text" id="iproductgrade'+a+'" name="iproductgrade'+a+'" value=""></td><td style="width:338px;"><input style="width:336px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td style="width:112px;"><input style="width:110px;" readonly type="text" id="eproductmotifname'+a+'" name="eproductmotifname'+a+'" value=""></td><td style="width:65px;"><input style="text-align:right; width:62px;" type="text" id="nstockopname'+a+'" name="nstockopname'+a+'" value="" onkeyup="reformat(this);"></td><td style="width:160px;">&nbsp;</td></tr></tbody></table>';
	  }
    j=0;
    var iproduct			= Array();
    var iproductmotif		= Array();
    var iproductgrade		= Array();
    var eproductname		= Array();
    var eproductmotifname	= Array();
    var nstockopname		= Array();
//    alert(a);
    for(i=1;i<a;i++){
		  j++;
		  iproduct[j]			= document.getElementById("iproduct"+i).value;
		  iproductmotif[j]	= document.getElementById("iproductmotif"+i).value;
		  iproductgrade[j]	= document.getElementById("iproductgrade"+i).value;
		  eproductname[j]		= document.getElementById("eproductname"+i).value;
		  eproductmotifname[j]= document.getElementById("eproductmotifname"+i).value;
		  nstockopname[j]		= document.getElementById("nstockopname"+i).value;	
    }
//    alert("tes");
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		  j++;
		  document.getElementById("iproduct"+i).value			= iproduct[j];
		  document.getElementById("iproductmotif"+i).value	= iproductmotif[j];
		  document.getElementById("iproductgrade"+i).value	= iproductgrade[j];
		  document.getElementById("eproductname"+i).value		= eproductname[j];
		  document.getElementById("eproductmotifname"+i).value= eproductmotifname[j];
		  document.getElementById("nstockopname"+i).value		= nstockopname[j];
    }
//    alert("tes");
    lebar =600;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/stockopname/cform/productupdate/"+a+"/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dstockopname").value!='') &&
  	 	(document.getElementById("istore").value!='') &&
  	 	(document.getElementById("istorelocation").value!='')) {
  	 	if(a==1){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("iproductgrade"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("nstockopname"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					cek='false';
					break;
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
			document.getElementById("cmdtambahitem").disabled=true;
    	}else{
		    document.getElementById("login").disabled=false;
		}
    	}else{
      		alert('Data header masih ada yang salah !!!');
    	}
  }
</script>
