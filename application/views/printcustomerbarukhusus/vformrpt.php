<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Cetak Pelanggan Baru</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: potrait;}
*/
{
  size: potrait;
}

@page { size: Letter; 
		/*margin: 0.10in 0.50in 0.50in 0.50in;*/
		margin: 0.03in 0.37in 0.07in 0.26in;
	}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 26px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.nmper2 {
  font-size: 15px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 15px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 18px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.pagebreak {
    page-break-after: always;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<?php 
foreach($lang as $row)
{
  if($row->f_spb_pkp=='t'){
      $pkp="PKP";
    }else{
      $pkp="non PKP";
    }
?>
  <table width="100%" border="0" class="huruf nmper">
    <tr>
      <td colspan="8" class="huruf judul" align="center">DATA PELANGGAN</td>
    </tr>
    <tr>
      <td colspan="8" class="huruf judul garisbawah" align="center" ><?php echo NmPerusahaan ;?></td>
    </tr>
    <tr>
      <td colspan="8" class="garisbawah"></td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Area</td>
      <td>:</td>
      <td class="huruf isi"><?php echo substr($row->i_customer,0,2)."-".$row->e_area_name." Kota : ".$row->e_customer_kota1;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf nmper"></td>
      <td class="huruf nmper"></td>
      <td class="huruf nmper2"><?php echo $pkp;?></td>
    </tr>
    <tr>
      <td class="huruf isi garisbawah" width="10%">Kode Pelanggan</td>
      <td colspan="7" class="huruf isi garisbawah">:</td>
    </tr>

<!--DATA TOKO-->
    <tr>
      <td class="huruf nmper" width="20%">DATA TOKO</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Nama Toko</td>
      <td>:</td>
      <td class="huruf isi" colspan="6"><?php echo $row->e_customer_name;?></td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Alamat Toko</td>
      <td>:</td>
      <td class="huruf isi" colspan="6"><?php echo $row->e_customer_address;?></td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Kota</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_kota1;?></td>
      <td class="huruf isi">Kode Pos</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_postal1;?></td>
      <td width="3%">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf isi" width="10%">No.Telepon </td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_phone;?></td>
      <td class="huruf isi" width="8%">Fax</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_fax1;?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf isi" width="10%">Yang Dihubungi</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_contact;?></td>
    </tr>

    <tr>
      <td class="huruf isi garisbawah" width="10%">Alamat E-mail</td>
      <td class="garisbawah">:</td>
      <td colspan="6" class="huruf isi garisbawah"><?php echo $row->e_customer_mail;?></td>
    </tr>
<!---END DATA TOKO-->

<!--DATA PEMILIK-->
    <tr>
      <td class="huruf nmper" width="20%">DATA PEMILIK</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Nama Pemilik</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_owner;?></td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Alamat Pemilik</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_owneraddress;?></td>
    </tr>

    <tr>
      <td class="huruf isi garisbawah" width="10%">Telepon</td>
      <td class="huruf isi garisbawah">:</td>
      <td colspan="6" class="huruf isi garisbawah"><?php echo $row->e_customer_ownerphone." / ".$row->e_customer_ownerhp;?></td>
    </tr>
<!--END DATA PEMILIK-->

<!--DATA PKP-->
    <tr>
      <td class="huruf nmper" width="20%">DATA PKP</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Nama PKP</td>
      <td>:</td>
      <td class="huruf isi"><?php echo $row->e_customer_npwpname;?></td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Alamat PKP</td>
      <td>:</td>
      <td colspan="4" class="huruf isi"><?php echo substr($row->e_customer_npwpaddress,0,68);?></td>
    </tr>

    <tr>
      <td class="huruf isi garisbawah" width="10%">NPWP</td>
      <td class="huruf isi garisbawah">:</td>
      <td colspan="6" class="huruf isi garisbawah"><?php echo $row->e_customer_pkpnpwp;?></td>
    </tr>
<!--END DATA PKP-->

    <tr>
      <td class="huruf isi" width="10%">Tipe Pelanggan</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $row->e_customer_classname;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Pola Bayar</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $row->e_paymentmethod;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">T.O.P</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $row->n_spb_toplength;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Jadwal Kontra Bon</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
<?php 
  $disc='';
    if($row->n_spb_discount1!='' && $row->n_spb_discount1!='0'){
      $disc=$disc.$row->n_spb_discount1;
    }
    if($row->n_spb_discount2!='' && $row->n_spb_discount2!='0'){
      $disc=$disc."+".$row->n_spb_discount2;
    }
    if($row->n_spb_discount3!='' && $row->n_spb_discount3!='0'){
      $disc=$disc."+".$row->n_spb_discount3;
    }
    if($row->n_spb_discount4!='' && $row->n_spb_discount4!='0'){
      $disc=$disc."+".$row->n_spb_discount4;
    }
;?>
    <tr>
      <td class="huruf isi" width="10%">Discount</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $disc." % / ".$row->i_price_group;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td class="huruf isi" width="10%">Kode Salesman</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $row->i_salesman." - ".$row->e_salesman_name;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

<?php 
  $tmp=explode("-",$row->d_customer_entry);
    $th=$tmp[0];
    $bl=$tmp[1];
    $hr=$tmp[2];
    $daftar=$hr." ".mbulan($bl)." ".$th;
;?>
    <tr>
      <td class="huruf isi" width="10%">Tanggal Terdaftar</td>
      <td class="huruf isi">:</td>
      <td class="huruf isi"><?php echo $daftar;?></td>
      <td width="3%" class="huruf isi"></td>
      <td width="11">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>

    <tr>
      <td rowspan="6" align="right"><?php  echo "<img height=150px; style=\"cursor:hand;\" src=\"". base_url()."img/cap.png\" border=\"0\" alt=\"edit\">";?></td>
      <td></td>
      <td class="huruf isi  ">..................... .Tgl......................... <br>
      Admin Sales Supervisior &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Admin Sales
      </td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
      <td></td>
      <td></td>
    </tr>

    <tr>
      <td></td>
      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;........................... &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ...........................</td>
    </tr>
<br>
<?php 
 
  for ($i=0;$i<15;$i++) {
    echo "<tr><td> </td></tr>";
  }  
?>
<tr>
  <td align="right">__________________</td>
  <td></td>
  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;__________________</td>
  <td></td>
  <td></td>
  <td></td>
</tr>      

<tr>
  <td class="huruf isi" align="right">Sales Coordinator</td>
  <td></td>
  <td class="huruf isi">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;S.D.H&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F.A.D.H</td>
  <td></td>
  <td></td>
  <td></td>
</tr>
<?php 
  $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");

  echo "<tr>
          <td class='huruf isi' colspan='3' class='nmper'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tanggal Cetak : ".$tgl."</td>
        </tr>"
;?>
</table>
<br class="pagebreak">
<?php //echo '<br>'.$row->e_remark1; ?>
<?php 
}
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
