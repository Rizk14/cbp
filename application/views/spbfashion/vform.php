<table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'spbfashion/cform','update'=>'#pesan','type'=>'post'));?>
	<div id="spbform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Tgl SPB</td>
		<td><input readonly id="dspb" name="dspb" 
			   onclick="showCalendar('',this,this,'','dspb',0,20,1)" value="<?php echo $tgl; ?>">
		    <input id="ispb" name="ispb" type="hidden">
			<input id="iperiode" name="iperiode" type="hidden" value="<?php echo $iperiode; ?>">
			<input id="dspbsys" name="dspbsys" type="hidden" value="<?php echo $dspbsys; ?>"></td>
		<td>Kelompok Harga</td>
		<td><input readonly id="epricegroupname" name="epricegroupname">
		    <input id="ipricegroup" name="ipricegroup" type="hidden"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" 
		     onclick='kosong();showModal("spbfashion/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="iarea" name="iarea" type="hidden"><input id="istore" name="istore" type="hidden"><input id="fstock" name="fstock" type="hidden"></td>
		<td>Nilai Kotor</td>
		<td><input  readonly id="vspb" name="vspb"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="icustomer" name="icustomer" type="hidden"><input id="ecustomername" name="ecustomername" readonly onclick='kosong(); showModal("spbfashion/cform/customer/"+document.getElementById("iarea").value+"/"+document.getElementById("dspb").value+"/x01/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    </td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" value="0"></td>
	      </tr>

	      <tr>
		<td>Alamat</td>
		<td><input readonly id="ecumstomeraddress" name="ecumstomeraddress" maxlength="100"></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="0"></td>
	      </tr>

	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" maxlength="30"></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="0"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox" value="">
		    SPB Lama&nbsp;&nbsp;<input id="ispbold" name="ispbold" type="text" value=""></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal" value="0" onkeyup="diskonrupiah(this.value)"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly>   Stock Daerah
			<input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" value="" onclick="pilihstockdaerah(this.value)"></td>
		<td>Nilai Bersih</td>
		<td><input  readonly id="vspbbersih" name="vspbbersih" readonly value="0"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname" onclick='showModal("spbfashion/cform/salesman/"+document.getElementById("iarea").value+"/"+document.getElementById("dspb").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="isalesman" name="isalesman" type="hidden"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly value="0"></td>
	      </tr>
	      <tr>
		<td>Stok Daerah</td>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly>
		    <input readonly readonly id="dsj" name="dsj" 
			   onclick="showCalendar('',this,this,'','dsj',0,20,1)"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input  readonly id="vspbafter" name="vspbafter" readonly value="0"></td>
	      </tr>
	      <tr>
		<td>PKP</td>
		<td><input id="fspbplusppn" name="fspbplusppn" type="hidden">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden">
			<input id="fspbpkp" name="fspbpkp" type="hidden">
			<input id="fcustomerfirst" name="fcustomerfirst" type="hidden">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly></td>
		<td>Keterangan</td>
		<td><input  id="eremarkx" name="eremarkx" maxlength="100"></td>
	      </tr>
    <tr>
		  <td width="100%" align="center" colspan="4">
        <div id='ketppn'></div>
      </td>
    </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spbfashion/cform/","#main")'>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center"></div>
			<div id="detailisi" align="center"></div>
<!--			<div id="pesan"></div>-->
			<input type="hidden" name="jml" id="jml" value="0">
	  </div>
	</div>
	</div>
	<?=form_close()?>
	<?php echo form_open('/spbfashion/cform/area/', array('id' => 'areaform', 'name' => 'areaform', 'onsubmit' => 'sendRequestarea(); return false'));?>
	<?=form_close()?>
    </td>
  </tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">
function kosong(){
	document.getElementById("icustomer").value='';
	document.getElementById("ecustomername").value='';
	document.getElementById("ncustomerdiscount3").value=0;
	document.getElementById("vcustomerdiscount3").value=0;
	document.getElementById("ncustomerdiscount2").value=0;
	document.getElementById("vcustomerdiscount2").value=0;
	document.getElementById("ncustomerdiscount1").value=0;
	document.getElementById("vcustomerdiscount1").value=0;
	document.getElementById("ecumstomeraddress").value='';
	document.getElementById("epricegroupname").value='';
	document.getElementById("ecustomerpkpnpwp").value='';
	document.getElementById("fcustomerfirst").value='';
	document.getElementById("fspbpkp").value='';
	document.getElementById("fspbplusdiscount").value='';
	document.getElementById("esalesmanname").value='';
	document.getElementById("nspbtoplength").value='';
	document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
}
  function tambah_item(a){
    if(a<22){
      so_inner=document.getElementById("detailheader").innerHTML;
      si_inner=document.getElementById("detailisi").innerHTML;
      if(so_inner==''){
		    so_inner = '<table id="itemtem" class="listtable" style="width:930px;">';
		    so_inner+= '<thead><tr><th style="width:25px;"  align="center">No</th>';
		    so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		    so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		    so_inner+= '<th style="width:100px;" align="center">Motif</th>';
		    so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
		    so_inner+= '<th style="width:46px;"  align="center">Jml Psn</th>';
		    so_inner+= '<th style="width:94px;"  align="center">Total</th>';
		    so_inner+= '<th style="width:180px;"  align="center">Keterangan</th>';
		    so_inner+= '<th style="width:32px;"  align="center" class="Action">Act</th></tr></thead></table>';
		    document.getElementById("detailheader").innerHTML=so_inner;
      }else{
		    so_inner=''; 
      }
      if(si_inner==''){
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;	
		    si_inner='<table><tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="iproductstatus'+a+'" name="iproductstatus'+a+'" value=""></td>';
		    si_inner+='<td style="width:61px;"><input style="width:61px; font-size:12px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:283px;"><input style="width:283px; font-size:12px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:96px;"><input readonly style="width:96px; font-size:12px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		    si_inner+='<td style="width:87px;"><input readonly style="text-align:right; width:87px; font-size:12px;"  type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		    si_inner+='<td style="width:45px;"><input style="text-align:right; width:45px; font-size:12px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')" autocomplete="off"><input style="text-align:right; width:45px; font-size:12px;" type="hidden" id="nquantitystock'+a+'" name="nquantitystock'+a+'" value=""></td>';
		    si_inner+='<td style="width:90px;"><input readonly" style="text-align:right; width:90px; font-size:12px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		    si_inner+='<td style="width:173px;"><input style="width:173px; font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value="" maxlength="50"></td>';
		    si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
      }else{
		    document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		    juml=document.getElementById("jml").value;
		    si_inner=si_inner+'<table><tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""><input type="hidden" id="iproductstatus'+a+'" name="iproductstatus'+a+'" value=""></td>';
		    si_inner+='<td style="width:61px;"><input style="width:61px; font-size:12px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		    si_inner+='<td style="width:283px;"><input style="width:283px; font-size:12px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		    si_inner+='<td style="width:96px;"><input readonly style="width:96px; font-size:12px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		    si_inner+='<td style="width:87px;"><input readonly style="text-align:right; width:87px; font-size:12px;"  type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td>';
		    si_inner+='<td style="width:45px;"><input style="text-align:right; width:45px; font-size:12px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')" autocomplete="off"><input style="text-align:right; width:45px; font-size:12px;" type="hidden" id="nquantitystock'+a+'" name="nquantitystock'+a+'" value=""></td>';
		    si_inner+='<td style="width:90px;"><input readonly" style="text-align:right; width:90px; font-size:12px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		    si_inner+='<td style="width:173px;"><input style="width:173px; font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value="" maxlength="50"></td>';
		    si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
      }
      j=0;
      var baris			    = Array()
      var iproduct		  = Array();
      var eproductname	= Array();
      var vproductretail= Array();
      var norder			  = Array();
      var nquantitystock	= Array();
      var motif			    = Array();
      var motifname		  = Array();
      var vtotal			  = Array();
      var eremark			  = Array();
      var status			  = Array();
      for(i=1;i<a;i++){
	      j++;
	      baris[j]			    = document.getElementById("baris"+i).value;
	      iproduct[j]			  = document.getElementById("iproduct"+i).value;
	      status[j] 			  = document.getElementById("iproductstatus"+i).value;
	      eproductname[j]		= document.getElementById("eproductname"+i).value;
	      vproductretail[j]	= document.getElementById("vproductretail"+i).value;
	      norder[j]			    = document.getElementById("norder"+i).value;
	      nquantitystock[j]			    = document.getElementById("nquantitystock"+i).value;
	      motif[j]			    = document.getElementById("motif"+i).value;
	      motifname[j]			= document.getElementById("emotifname"+i).value;
	      vtotal[j]			    = document.getElementById("vtotal"+i).value;
	      eremark[j]			  = document.getElementById("eremark"+i).value;		
      }
      document.getElementById("detailisi").innerHTML=si_inner;
      j=0;
      for(i=1;i<a;i++){
		    j++;

		    document.getElementById("baris"+i).value=baris[j];
		    document.getElementById("iproduct"+i).value=iproduct[j];
        	document.getElementById("iproductstatus"+i).value=status[j];
		    document.getElementById("eproductname"+i).value=eproductname[j];
		    document.getElementById("vproductretail"+i).value=vproductretail[j];
		    document.getElementById("norder"+i).value=norder[j];
		    document.getElementById("nquantitystock"+i).value=nquantitystock[j];
		    document.getElementById("motif"+i).value=motif[j];
		    document.getElementById("emotifname"+i).value=motifname[j];
		    document.getElementById("vtotal"+i).value=vtotal[j];
		    document.getElementById("eremark"+i).value=eremark[j];		
      }
	    showModal("spbfashion/cform/product/"+a+"/"+document.getElementById("ipricegroup").value+"/"+document.getElementById("iarea").value+"/"+document.getElementById("istore").value+"/"+document.getElementById("fstock").value+"/x01/","#light");
	    jsDlgShow("#konten *", "#fade", "#light");
    }else{
      alert('Maksimum 21 item');
    }
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("icustomer").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
		  (document.getElementById("ipricegroup").value!='')&&
		  (document.getElementById("esalesmanname").value!='')&&
		  (document.getElementById("isalesman").value!=''))
		   {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
			document.getElementById("cmdtambahitem").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){

    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;

    show("listspb/cform/","#tmpx");
  }
  function hitungnilai(isi,jml){
	jml=document.getElementById("jml").value;
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		vdis1=0;
		vdis2=0;
		vdis3=0;
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductretail"+i).value);
    	if (isNaN(parseFloat(document.getElementById("norder"+i).value))){
        nqty=0;
      }else{
  		if((document.getElementById("fstock").value=='f')){
  			      		nqty=formatulang(document.getElementById("norder"+i).value);
      		vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
      	}else{
      		if(parseFloat(document.getElementById("nquantitystock"+i).value)<parseFloat(document.getElementById("norder"+i).value)){
      		
      		alert("Lebih Dari Stock!!!");
      		nqty=0;
      		vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("norder"+i).value=formatcemua(vhrg);
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
      		
      		}else{

  			nqty=formatulang(document.getElementById("norder"+i).value);
      		vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
      		
      		}

      	}
      }
//			nqty=formatulang(document.getElementById("norder"+i).value);
		}
		vdis1=vdis1+((vtot*dtmp1)/100);
		vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
		document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
		document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
		vdis1=parseFloat(vdis1);
		vdis2=parseFloat(vdis2);
		vdis3=parseFloat(vdis3);
		vtotdis=vdis1+vdis2+vdis3;
//		vtotdis=Math.round(vtotdis);
		document.getElementById("vspbdiscounttotal").value=formatcemua(Math.round(vtotdis));
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=parseFloat(formatulang(formatcemua(vtot)))-parseFloat(formatulang(formatcemua(Math.round(vtotdis))));
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
  function diskonrupiah(isi){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot   =parseFloat(formatulang(document.getElementById("vspb").value));
		vtotdis=parseFloat(formatulang(isi));
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
  function pilihstockdaerah(a){
		if(a=='')
		{
			document.getElementById("fspbstockdaerah").value='on';
		}else{
			document.getElementById("fspbstockdaerah").value='';
		}
  }
   function afterSetDateValue(ref_field, target_field, date) {
      
      var startDate=document.getElementById('dspb').value;
      tes=startDate.split('-');
      startDate=tes[2]+'-'+tes[1]+'-'+tes[0];
      start=tes[2]+'-'+tes[1];
      
      var tglakhirthn='27-12-2017';
      tes3=tglakhirthn.split('-');
      tglakhirthn=tes3[2]+'-'+tes3[1]+'-'+tes3[0];

      var dspbSys=document.getElementById('dspbsys').value;
      tes2=dspbSys.split('-');
      dspbSys=tes2[0]+'-'+tes2[1]+'-'+tes2[2];

      var iperiode=document.getElementById('iperiode').value;
      //alert(dspbSys);
      //alert(iperiode);
      var d=new Date();
      var curr_date = d.getDate() - 1;
      var curr_month = d.getMonth() + 1;
      var curr_year = d.getFullYear();

      if ( curr_month < 10 ) curr_month = '0' + curr_month;
      if ( curr_date < 10 )curr_date='0'+curr_date;
      current=curr_year + "-" + curr_month + "-" + curr_date;
      //alert(startDate);
      //alert(current);
      //alert(tglakhirthn);
      curr=curr_year + "-" + curr_month;
      //currenttmp1=curr_year + "-" + curr_month + "-" + 29;
  	  //curr1=curr_year + "-" + curr_month;
      currentx=curr_date + "-" + curr_month + "-" + curr_year;
       if(current>=startDate){
      	
      	alert("Tanggal SPB tidak boleh kurang / sama dengan Tanggal Sekarang");
  		document.getElementById('dspb').value='';

      }//else if(startDate>=tglakhirthn){

      	//alert("TUTUP SPB AKHIR TAHUN TERAKHIR SPB TANGGAL "+current+"(2)");
        //document.getElementById('dspb').value='';

   	  //}
  }
  $(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("detailisi",function(e){
        return false;
    });
});
</script>
