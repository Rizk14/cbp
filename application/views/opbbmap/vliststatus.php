<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('opbbmap/cform/caristatus', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
      	    <th>Kode Status</th>
	    	<th>Keterangan</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
				echo "<tr> 
						  <td><a href=\"javascript:setValue('$row->i_op_status','$row->e_op_statusname')\">$row->i_op_status</a></td>
						  <td><a href=\"javascript:setValue('$row->i_op_status','$row->e_op_statusname')\">$row->e_op_statusname</a></td>
				   	  </tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
	document.getElementById("iopstatus").value=a;
	document.getElementById("eopstatusname").value=b;
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
