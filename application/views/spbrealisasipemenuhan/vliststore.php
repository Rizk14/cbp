<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<!--
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
-->
</head>
<body id="bodylist">
<div id="main">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo form_open('spbrealisasipemenuhan/cform/caristore', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
			<td colspan="3" align="center">Cari data : 
						<input type="hidden" id="jml" name="jml" 
							   value="<?php echo $this->uri->segment(4); ?>">
						<input type="hidden" id="ispb" name="ispb" 
							   value="<?php echo $this->uri->segment(5); ?>">
						<input type="hidden" id="fspbstockdaerah" name="fspbstockdaerah" 
							   value="<?php echo $this->uri->segment(6); ?>">
						<input type="text" id="cari" name="cari" value="">
						&nbsp;
						<input type="submit" id="bcari" name="bcari" value="Cari">
			</td>
	      </tr>
	    </thead>
      	    <th>Kode Gudang</th>
	    	<th>Nama Gudang</th>
			<th>Lokasi</th>
	    <tbody>
	      <?php 
		$jml=$this->uri->segment(4);
		$spb=$this->uri->segment(5);
		$area=$this->uri->segment(7);
		if($isi){
			foreach($isi as $row){
				$i=0;
			 	$this->db->select(" i_product, i_product_motif from tm_spb_item where i_spb='$spb' and i_area='$area' order by n_item_no", false);
				$query1 = $this->db->get();
				foreach($query1->result() as $riw){
				 	$this->db->select(" i_product, n_quantity_stock from tm_ic 
										          where i_store='$row->i_store'
										          and i_store_location='$row->i_store_location'
										          and i_product='$riw->i_product'
										          and i_product_motif='00'
                              and i_product_grade='A'", false);
					$query = $this->db->get();
					if($query->num_rows>0){
						foreach($query->result() as $raw){
							$i++;
							$stloc[$i]=$raw->n_quantity_stock;
						}
					}else{
						$i++;
						$stloc[$i]='0';						
					}
					$tmp='';
					for($j=1;$j<=$i;$j++){
						if($j==1)
							$tmp=$tmp.$stloc[$j];
						else
							$tmp=$tmp.'|'.$stloc[$j];
					}
#					echo 'tes = '.$tmp.'<br>';
				}
				echo "<tr> 
						  <td><a href=\"javascript:setValue('$row->i_store','$row->e_store_name','$row->i_store_location','$row->e_store_locationname','$tmp')\">$row->i_store</a></td>
						  <td><a href=\"javascript:setValue('$row->i_store','$row->e_store_name','$row->i_store_location','$row->e_store_locationname','$tmp')\">$row->e_store_name</a></td>
						  <td><a href=\"javascript:setValue('$row->i_store','$row->e_store_name','$row->i_store_location','$row->e_store_locationname','$tmp')\">$row->e_store_locationname</a></td>
					   	  </tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e)
  {
  	tes=e.split('|');
	  document.getElementById("istore").value=a;
	  document.getElementById("estorename").value=b;
	  document.getElementById("istorelocation").value=c;
	  document.getElementById("estorelocationname").value=d;
	  var s = a.replace(/\,/g,'');
	  for(i=0;i<tes.length;i++){
		  j =i+1;
		  s = document.getElementById('norder'+j).value;
		  s = s.replace(/\,/g,'');
		  t = tes[i];
		  t = t.replace(/\,/g,'');
      if(parseFloat(t)<0) t=0;
		  if(parseFloat(t)>=parseFloat(s)){
			  document.getElementById('ndeliver'+j).value=document.getElementById('norder'+j).value;
		  }else{
			  document.getElementById('ndeliver'+j).value=t;//tes[i];
		  }
		  document.getElementById('nstock'+j).value=parseFloat(tes[i]);
	  }
	  hitungnilai(0,document.getElementById("jml").value);
	  jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
