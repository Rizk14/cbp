<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('spb/cform/update', array('id' => 'spbformupdate', 'name' => 'spbformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Tgl SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>"
			   onclick="showCalendar('',this,this,'','dspb',0,20,1)"></td>
		    <input id="ispb" name="ispb" type="hidden" value="<?php echo $isi->i_spb; ?>"></td>
		<td>Kelompok Harga</td>
		<td><input disabled=true id="epricegroupname" name="epricegroupname" 
			 	   value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input  disabled=true id="vspb" name="vspb" disabled=true value="<?php echo $isi->v_spb; ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan()" 
				   value="<?php echo $isi->e_customer_name; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 1</td>
		<td><input disabled=true id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>">
		    <input disabled=true id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_spb_discount1,2); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10"></td>
		<td>Discount 2</td>
		<td><input disabled=true id="ncustomerdiscount2" name="ncustomerdiscount2"
				   value="<?php echo $isi->n_spb_discount2; ?>">
		    <input disabled=true id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_spb_discount2,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Konsiyasi</td>
		<td><input id="fspbconsigment" name="fspbconsigment" type="checkbox"
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>></td>
		<td>Discount 3</td>
		<td><input disabled=true id="ncustomerdiscount3" name="ncustomerdiscount3"
				   value="<?php echo $isi->n_spb_discount3; ?>">
		    <input disabled=true id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_spb_discount3,2); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" disabled=true
				   value="<?php echo $isi->n_spb_toplength; ?>"></td>
		<td>Discount Total</td>
		<td><input disabled=true id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input disabled=true id="esalesmanname" name="esalesmanname"
				   value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden"
				   value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input disabled=true id="vspbbersih" name="vspbbersih" disabled=true
				   value="<?php echo number_format($tmp,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Stok Daerah</td>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" disabled=true>
		    <input readonly disabled=true id="dsj" name="dsj" 
			   onclick="showCalendar('',this,this,'','dsj',0,20,1)"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" disabled=true></td>
	      </tr>
	      <tr>
		<td>PKP</td>
		<td><input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" disabled=true
				   value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input  disabled=true id="vspbafter" name="vspbafter" disabled=true
				   value="<?php echo $isi->v_spb_after;?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="javascript:history.back();">
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"></td>
		</tr>
	    </table>
			<div id="detailheader">
				<table class="listtable">
					<th width="5%" align="center">No</th>
					<th width="12%"align="center">Kd Barang</th>
					<th width="50%" align="center">Nama Barang</th>
					<th width="13%" align="center">Harga</th>
					<th width="7%" align="center">Jml Psn</th>
					<th width="7%" align="center">Jml Dlv</th>
					<th width="7%" align="center" class="action">Action</th>
				</table>
			</div>
			<div id="detailisi">
				<?php 				
				echo "<table class=\"listtable\" width=\"100%\">";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
				  	echo "<tbody>
							<tr>
		    				<td width=\"5%\"><input size=\"1\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
                <input type=\"hidden\" id=\"iproductstatus$i\" name=\"iproductstatus$i\" value=\"$row->i_product_status\">
                </td>
							<td width=\"12%\"><input size=\"8\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td width=\"49%\"><input size=\"41\" readonly type=\"text\" id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td width=\"13%\"><input style=\"text-align:right\"; size=\"9\" type=\"text\" 
								id=\"vproductretail$i\"
								name=\"vproductretail$i\" value=\"$row->v_unit_price\"></td>
							<td width=\"7%\"><input style=\"text-align:right\"; size=\"3\" type=\"text\" 
								id=\"norder$i\" 
								name=\"norder$i\" value=\"$row->n_order\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem')\"><input type="hidden" id="eremark'.$i.'" name="eremark'.$i.'" value="'.$row->e_remark.'"></td>
							<td width=\"7%\"><input style=\"text-align:right\"; readonly size=\"3\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"\"></td>
							<td width=\"7%\" align=\"center\">
								<a href=\"javascript:xxx('$row->i_spb','$row->i_product','$row->i_product_grade',
														 '$row->v_unit_price','$row->n_order','$jmlitem',
								   						 '".$this->lang->line('delete_confirm')."'
														);\">
								<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" 
									 border=\"0\" alt=\"delete\"></a></td>
							</tr>
						  </tbody>";
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,e,f,g){
    if (confirm(g)==1){
	  document.getElementById("ispbdelete").value=a;
	  document.getElementById("iproductdelete").value=b;
	  document.getElementById("iproductgradedelete").value=c;
// 	  --hitung ulang--
		dtmp1	=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2	=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3	=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		vtotdis =parseFloat(formatulang(document.getElementById("vspbdiscounttotal").value));
		vdis1	=0;
		vdis2	=0;
		vdis3	=0;
		vtot	=parseFloat(formatulang(document.getElementById("vspb").value));
		dismin	=parseFloat(d*e);
		vtot	=vtot-dismin;
		vdis1	=vdis1+((vtot*dtmp1)/100);
		vdis2	=vdis2+(((vtot-vdis1)*dtmp2)/100);
		vdis3	=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		vtotdis=vdis1+vdis2+vdis3;
		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);

		document.getElementById("vdis1").value=formatcemua(vdis1);
		document.getElementById("vdis2").value=formatcemua(vdis2);
		document.getElementById("vdis3").value=formatcemua(vdis3);
		document.getElementById("vtotdis").value=formatcemua(vtotdis);
		document.getElementById("vtot").value=formatcemua(vtot);
		document.getElementById("vtotbersih").value=formatcemua(vtotbersih);
		
// end of --hitung ulang--
	  formna=document.getElementById("spbformupdate");
	  formna.action		="<?php echo site_url(); ?>"+"/spb/cform/deletedetail";
	  formna.submit();
    }
  }
  function yyy(b){
	document.getElementById("ispbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spb/cform/edit";
	formna.submit();
  }
  function view_store(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/store/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
	so_inner='<table class="listtable"><th width="4%" align="center">No</th><th width="12%"align="center">Kd Barang</th><th width="50%" align="center">Nama Barang</th><th width="8%" align="center">Harga</th><th width="8%" align="center">Jumlah Pesan</th><th width="8%" align="center">Jumlah Dipenuhi</th><th width="10%" align="center" class="action">Action</th></table>';
	document.getElementById("detailheader").innerHTML=so_inner;
    }else{
	so_inner='';
    }
    if(si_inner==''){
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;	
	si_inner='<table class="listtable" width="100%"><tbody><tr><td width="4%"><input size="1" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td><td width="12%"><input size="8" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td width="49%"><input size="41" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td width="13%"><input size="9" style="text-align:right"; type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""></td><td width="7%"><input size="3" style="text-align:right"; type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td><td width="7%"><input disabled="true" style="text-align:right"; size="3" type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value=""></td><td width="7%">&nbsp;</td></tr></tbody></table>';	
    }else{
	document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
	juml=document.getElementById("jml").value;
	si_inner=si_inner+'<table class="listtable" width="100%"><tbody><tr><td width="4%"><input size="1" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td><td width="12%"><input size="8" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td width="49%"><input size="41" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td width="13%"><input size="9" style="text-align:right"; type="text" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""><td width="7%"><input size="3" style="text-align:right"; type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+')"></td><td width="7%"><input disabled="true" style="text-align:right"; size="3" type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value=""></td><td width="7%">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris			=Array()
    var iproduct		=Array();
    var eproductname	=Array();
	var vproductretail	=Array();
    var norder			=Array();
    var ndeliver		=Array();
    for(i=1;i<a;i++){
	j++;
	baris[j]			=document.getElementById("baris"+i).value;
	iproduct[j]			=document.getElementById("iproduct"+i).value;
	eproductname[j]		=document.getElementById("eproductname"+i).value;
	vproductretail[j]	=document.getElementById("vproductretail"+i).value;
	norder[j]			=document.getElementById("norder"+i).value;
	ndeliver[j]			=document.getElementById("ndeliver"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
	j++;
	document.getElementById("baris"+i).value=baris[j];
	document.getElementById("iproduct"+i).value=iproduct[j];
	document.getElementById("eproductname"+i).value=eproductname[j];
	document.getElementById("vproductretail"+i).value=vproductretail[j];
	document.getElementById("norder"+i).value=norder[j];
	document.getElementById("ndeliver"+i).value=ndeliver[j];	
    }
    lebar =450;
    tinggi=400;
//	kdstore=document.getElementById("istore").value;
	kdharga=document.getElementById("ipricegroup").value;
    //eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/productupdate/"+a+"/"+kdstore+"/"+kdharga,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/productupdate/"+a+"/"+kdharga,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspb").value!='') &&
  	 	(document.getElementById("icustomer").value!='') &&
  	 	(document.getElementById("istore").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    		}else{
		    	document.getElementById("login").disabled=false;
		}
    	}else{
      		alert('Data header masih ada yang salah !!!');
    	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function view_pelanggan(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spb/cform/customer/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function hitungnilai(isi,jml){
	  jml=document.getElementById("jml").value;
	  if (isNaN(parseFloat(isi))){
		  alert("Input harus numerik");
	  }else{
		  dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		  dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		  dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		  vdis1=0;
		  vdis2=0;
		  vdis3=0;
		  vtot =0;
		  for(i=1;i<=jml;i++){
			  vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			  nqty=formatulang(document.getElementById("norder"+i).value);
			  vhrg=parseFloat(vhrg)*parseFloat(nqty);
			  vtot=vtot+vhrg;
			  document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		  }
		  vdis1=vdis1+((vtot*dtmp1)/100);
		  vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
		  vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
		  document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
		  document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
		  document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
		  vdis1=parseFloat(vdis1);
		  vdis2=parseFloat(vdis2);
		  vdis3=parseFloat(vdis3);
		  vtotdis=vdis1+vdis2+vdis3;
  //		vtotdis=Math.round(vtotdis);
		  document.getElementById("vspbdiscounttotal").value=formatcemua(Math.round(vtotdis));
		  document.getElementById("vspb").value=formatcemua(vtot);
		  vtotbersih=parseFloat(formatulang(formatcemua(vtot)))-parseFloat(formatulang(formatcemua(Math.round(vtotdis))));
		  document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	  }
  }
/*
  function hitungnilai(isi,jml){
    if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		vdis1=0;
		vdis2=0;
		vdis3=0;
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductretail"+i).value);
			nqty=formatulang(document.getElementById("norder"+i).value);
			vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			vdis1=vdis1+((vhrg*dtmp1)/100);
			vdis2=vdis2+(((vhrg-vdis1)*dtmp2)/100);
			vdis3=vdis3+(((vhrg-(vdis1+vdis2))*dtmp3)/100);
		}
		document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
		document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
		document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
		vtotdis=vdis1+vdis2+vdis3;
		document.getElementById("vspbdiscounttotal").value=formatcemua(vtotdis);
		document.getElementById("vspb").value=formatcemua(vtot);
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
	}
  }
*/
</script>
