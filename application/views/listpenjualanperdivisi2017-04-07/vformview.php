<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h3><?php 
    if($dfrom){
	    $tmp=explode('-',$dfrom);
	    $tgl=$tmp[2];
	    $bln=$tmp[1];
	    $thn=$tmp[0];
	    $dfrom=$tgl.'-'.$bln.'-'.$thn;
    }
    if($dto){
	    $tmp=explode('-',$dto);
	    $tgl=$tmp[2];
	    $bln=$tmp[1];
	    $thn=$tmp[0];
	    $dto=$tgl.'-'.$bln.'-'.$thn;
    }
echo 'Dari Tanggal : '.$dfrom.' Sampai Tanggal : '.$dto; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperdivisi/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>AREA</th>
		<th>NAMA AREA</th>
    <th>Jumlah Rp</th>
    <th>Qty Nota</th>
    <th>Qty SPB</th>
    <?php 
	    echo '<tbody>';
      $group='';
      $grandt=0;
      $grandtn=0;
      $grandts=0;
      foreach($prodnya as $row)
      { 
        if($group==''){# && $row->i_product_group!='PB'){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
#        }elseif($row->i_product_group=='PB'){
#          echo "<tr><td colspan=5><h2>Konsinyasi</h2></td></tr>";
        }
        if($group!='' && $group!=$row->e_product_groupname){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
        }
        $total=0;
        $totaln=0;
        $totals=0;
        foreach($areanya as $raw)
        { 
          echo "<tr><td>$raw->i_area</td><td>$raw->e_area_name</td>";
          $ada=false;
          foreach($isi as $riw)
          {
#            if( ($riw->i_area==$raw->i_area) && ($riw->e_product_groupname==$row->e_product_groupname)){
            if( ($riw->i_area==$raw->i_area) && ($riw->i_product_group==$row->i_product_group)){
              $ada=true;
              echo "<td align=right>".number_format($riw->jumlah)."</td>";
              echo "<td align=right>".number_format($riw->n_nota)."</td>";
              echo "<td align=right>".number_format($riw->n_spb)."</td>";
              $total=$total+$riw->jumlah;
              $totaln=$totaln+$riw->n_nota;
              $totals=$totals+$riw->n_spb;
            }
            if($ada)break;
          }
          if(!$ada){
            echo "<td align=right>0</td>";
            echo "<td align=right>0</td>";
            echo "<td align=right>0</td>";
          }
        }
        echo "</tr>";
        echo "<tr><td colspan=2 align=right><b>Total</td>
                  <td align=right><b>".number_format($total)."</td>
                  <td align=right><b>".number_format($totaln)."</td>
                  <td align=right><b>".number_format($totals)."</td></tr>";
        $group=$row->e_product_groupname;
        $grandt=$grandt+$total;
        $grandtn=$grandtn+$totaln;
        $grandts=$grandts+$totals;
      }
        echo "<tr><td colspan=2 align=right><b>Grand Total</td>
                  <td align=right><b>".number_format($grandt)."</td>
                  <td align=right><b>".number_format($grandtn)."</td>
                  <td align=right><b>".number_format($grandts)."</td></tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
