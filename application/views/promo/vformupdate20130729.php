<table class="maintable">
  <tr>
    <td align="left">
	  <?php echo $this->pquery->form_remote_tag(array('url'=>'promo/cform/update','id'=>'promoformupdate','update'=>'#pesan','type'=>'post'));?>
	    <div class="effect">
	      <div class="accordion2">
	        <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      		<tr>
							<td width="15%">Promo</td>
							<td><input id="ipromo" name="ipromo" value="<?php echo $isi->i_promo;?>">
								<?php 
									if($isi->d_promo!=''){
										$tmp=explode("-",$isi->d_promo);
										$th=$tmp[0];
										$bl=$tmp[1];
										$hr=$tmp[2];
										$isi->d_promo=$hr."-".$bl."-".$th;
									}
								?>
								<input readonly id="dpromo" name="dpromo" value="<?php echo $isi->d_promo;?>" onclick="showCalendar('',this,this,'','dpromo',0,20,1)">
		    			</td>
	      		</tr>
						<tr>
							<td width="15%">Keterangan</td>
							<td><input id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name;?>" onkeyup="gede(this)"></td>
						</tr>
						<tr>
							<td width="15%">Jenis Promo</td>
							<td><select readonly id="epromotypename" name="epromotypename">
								<option></option>
								<?php 
									$i=1;
									foreach($jenis as $jns){
										if($jns->i_promo_type==$isi->i_promo_type){
											echo "<option id=type$i name=$jns->i_promo_type onclick=\"cektype($jns->i_promo_type)\" selected>$jns->e_promo_typename</option>";
										}else{
											echo "<option id=type$i name=$jns->i_promo_type onclick=\"cektype($jns->i_promo_type)\">$jns->e_promo_typename</option>";
										}
									}
								?>
								</select>
								<input id="ipromotype" name="ipromotype" type="hidden" value="<?php echo $isi->i_promo_type; ?>">
							</td>
						</tr>
		  			<tr>
							<td width="15%">Periode</td>
								<?php 
									if($isi->d_promo_start!=''){
										$tmp=explode("-",$isi->d_promo_start);
										$th=$tmp[0];
										$bl=$tmp[1];
										$hr=$tmp[2];
										$isi->d_promo_start=$hr."-".$bl."-".$th;
									}
									if($isi->d_promo_finish!=''){
										$tmp=explode("-",$isi->d_promo_finish);
										$th=$tmp[0];
										$bl=$tmp[1];
										$hr=$tmp[2];
										$isi->d_promo_finish=$hr."-".$bl."-".$th;
									}
								?>
							</td>
							<td><input readonly id="dpromostart" name="dpromostart" value="<?php echo $isi->d_promo_start;?>"
			 	  				onclick="showCalendar('',this,this,'','dpromostart',0,20,1)">&nbsp;s/d&nbsp;
									<input readonly id="dpromofinish" name="dpromofinish" value="<?php echo $isi->d_promo_finish;?>"
									onclick="showCalendar('',this,this,'','dpromofinish',0,20,1)">
							</td>
	      		</tr>
            <tr>
							<td width="15%">Kode Harga</td>
							<td><input id="ipricegroup" name="ipricegroup" value="<?php echo $isi->i_price_group;?>"
                   onclick='view_kodeharga()'></td>
						</tr>
	      		<tr>
							<td width="15%">Semua Produk</td>
							<td><input id="fallproduct" name="fallproduct" type="checkbox" onclick="cekproduct()" 
								<?php 
									if($isi->f_all_product=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if( ($isi->f_all_baby=='t') || ($isi->f_all_reguler=='t') || ($isi->f_all_nb=='t') ){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
						</tr>
						<tr>
							<td width="15%">Baby Bedding Saja</td>
							<td><input id="fallbaby" name="fallbaby" type="checkbox" onclick="cekbaby()"
								<?php 
									if($isi->f_all_baby=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if( ($isi->f_all_product=='t') || ($isi->f_all_reguler=='t') || ($isi->f_all_nb=='t') ){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
	      		</tr>
	      		<tr>
							<td width="15%">Dialogue Home Saja</td>
							<td><input id="fallreguler" name="fallreguler" type="checkbox" onclick="cekreguler()"
								<?php 
									if($isi->f_all_reguler=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if( ($isi->f_all_product=='t') || ($isi->f_all_baby=='t') || ($isi->f_all_nb=='t') ){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
					  </tr>
						<tr>
							<td width="15%">Baby Non Bedding Saja</td>
							<td><input id="fallnb" name="fallnb" type="checkbox" onclick="ceknb()"
								<?php 
									if($isi->f_all_nb=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if( ($isi->f_all_product=='t') || ($isi->f_all_baby=='t') || ($isi->f_all_reguler=='t') ){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
	      		</tr>
					  <tr>
							<td width="15%">Semua Pelanggan</td>
							<td><input id="fallcustomer" name="fallcustomer" type="checkbox" onclick="cekcustomer()"
								<?php 
									if($isi->f_all_customer=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if($isi->f_customer_group=='t'){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
	      		</tr>
	      		<tr>
							<td width="15%">Group Pelanggan</td>
							<td><input id="fcustomergroup" name="fcustomergroup" type="checkbox" onclick="cekcustomergroup()"
								<?php 
									if($isi->f_customer_group=='t'){
										echo ' checked';
										echo ' value=\'on\'';
									}else{
										if($isi->f_all_customer=='t'){
											echo ' disabled=true';
										}
										echo ' value=\'\'';
									}
								?>>
							</td>
	      		</tr>
	      		<tr>
							<td width="15%">Area</td>
							<td><input id="fallarea" name="fallarea" type="checkbox" onclick="cekarea()"
								<?php 
									if($isi->f_all_area=='t'){
										echo " checked";
										echo " value='on'";
									}else{
										echo " value=''";
									}
								?>>
							</td>
	      		</tr>
					</table>
					<?php 
					if(
						($isi->i_promo_type=='1') || ($isi->i_promo_type=='3')
						)
						{
					?>
					<div id="prodis"><table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=15% class=batas>&nbsp;</td><td class=batas>&nbsp;</td></tr><tr><td width=15%>Discount I</td><td><input id=npromodiscount1 name=npromodiscount1 value="<?php echo $isi->n_promo_discount1; ?>"></td></tr><tr><td width=15% class=batas>Discount II</td><td class=batas><input id=npromodiscount2 name=npromodiscount2 value="<?php echo $isi->n_promo_discount2; ?>"></td></tr></table></div>
					<?php 
					}else if (($isi->i_promo_type=='5') || ($isi->i_promo_type=='6')){
					?>
					<div id="prodis"><table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=15% class=batas>&nbsp;</td><td class=batas>&nbsp;</td></tr><tr><td width=15%>Discount I</td><td><input id=npromodiscount1 name=npromodiscount1 value="<?php echo $isi->n_promo_discount1; ?>"></td></tr></table></div>
					<?php 
					}else{			
						echo "<div id=\"prodis\"></div>";
					}
					?>
					<div id="batten">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td width="100%" align="center">
									<input <?php if($areauser!='00') echo " disabled "; ?> name="login" id="login" value="Simpan" type="submit" 
									onclick="dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),
																	 parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listpromo/cform/","#main")'>
								<?php 
									if( ($isi->f_all_product=='f') && ($isi->f_all_baby=='f') && ($isi->f_all_reguler=='f') && ($isi->f_all_nb=='f')){
								?>		    
									<input <?php if($areauser!='00') echo " disabled "; ?> name="cmdtambahproduct" id="cmdtambahproduct" value="Tambah Produk" type="button" onclick="tambah_product(parseFloat(document.getElementById('jmlp').value)+1);">
								<?php 
									}
									if(($isi->f_all_customer=='f') && ($isi->f_customer_group=='f')){
								?>
									<input <?php if($areauser!='00') echo " disabled "; ?> name="cmdtambahcustomer" id="cmdtambahcustomer" value="Tambah Pelanggan" type="button" 
									onclick="tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);">
								<?php 
									}
									if($isi->f_customer_group=='t'){
								?>
								<input <?php if($areauser!='00') echo " disabled "; ?> name="cmdtambahcustomergroup" id="cmdtambahcustomergroup" value="Tambah  Group Pelanggan" type="button" 
								onclick="tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);">
								<?php 
									}
								?>
								<?php 
									if($isi->f_all_area=='f'){
								?>		    
									<input <?php if($areauser!='00') echo " disabled "; ?> name="cmdtambaharea" id="cmdtambaharea" value="Tambah Area" type="button" onclick="tambah_area(parseFloat(document.getElementById('jmla').value)+1);">
								<?php 
									}
								?>
								</td>
							</tr>
						</table>
					</div>
					<div id="detailheaderproduct" align="center">
						<?php 			
						if($jmlitemp>0){
							if(($isi->i_promo_type=='2')||($isi->i_promo_type=='4')||($isi->i_promo_type=='5')||($isi->i_promo_type=='6')){
								echo "<table id=\"itemp\" class=\"listtable\" style=\"width:750px;\"><tr><th style=\"width:25px;\"  align=\"center\">No</th><th style=\"width:70px;\" align=\"center\">Kode</th><th style=\"width:350px;\" align=\"center\">Nama Barang</th><th style=\"width:100px;\" align=\"center\">Motif</th><th style=\"width:100px;\" align=\"center\">Harga</th><th style=\"width:55px;\"  align=\"center\">Min Pesan</th><th style=\"width:50px;\"  align=\"center\">Action</th></tr></table>";
							}else{
								echo "<table id=\"itemp\" class=\"listtable\" style=\"width:750px;\"><tr><th style=\"width:25px;\"  align=\"center\">No</th><th style=\"width:70px;\" align=\"center\">Kode</th><th style=\"width:350px;\" align=\"center\">Nama Barang</th><th style=\"width:200px;\" align=\"center\">Motif</th><th style=\"width:55px;\"  align=\"center\">Min Pesan</th><th style=\"width:50px;\"  align=\"center\">Action</th></tr></table>";
							}
						}
						?>
					</div>
					<div id="detailproduct" align="center">
						<?php 
						if($jmlitemp>0){
							$i=0;
							foreach($detailp as $rowp){
								$i++;
								if(($isi->i_promo_type=='2')||($isi->i_promo_type=='4')||($isi->i_promo_type=='5')||($isi->i_promo_type=='6')){
									echo "<table class=\"listtable\" style=\"width:750px;\"><tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"barisp$i\" name=\"barisp$i\" value=\"$i\"><input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$rowp->i_product_motif\"></td><td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$rowp->i_product\"></td><td style=\"width:328px;\"><input style=\"width:328px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$rowp->e_product_name\"></td><td style=\"width:94px;\"><input readonly style=\"width:94px;\"  type=\"text\" id=\"emotifname$i\" name=\"emotifname$i\" value=\"$rowp->e_product_motifname\"></td><td style=\"width:95px;\"><input style=\"width:95px; text-align:right;\"  type=\"text\"  id=\"vunitprice$i\" name=\"vunitprice$i\" value=\"".number_format($rowp->v_unit_price)."\"></td><td style=\"width:53px;\"><input style=\"text-align:right; width:53px;\" type=\"text\" id=\"nquantitymin$i\" name=\"nquantitymin$i\" value=\"$rowp->n_quantity_min\"></td><td align=center><a href=\"javascript:xxxp('$rowp->i_promo','$rowp->i_product','$rowp->i_product_grade','$rowp->i_product_motif','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td></tr></tbody>";
								}else{
									echo "<table class=\"listtable\" style=\"width:750px;\"><tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"barisp$i\" name=\"barisp$i\" value=\"$i\"><input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$rowp->i_product_motif\"></td><td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$rowp->i_product\"></td><td style=\"width:333px;\"><input style=\"width:333px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$rowp->e_product_name\"></td><td style=\"width:189px;\"><input readonly style=\"width:189px;\"  type=\"text\" id=\"emotifname$i\" name=\"emotifname$i\" value=\"$rowp->e_product_motifname\"><input type=\"hidden\" id=\"vunitprice$i\" name=\"vunitprice$i\" value=\"".number_format($rowp->v_unit_price)."\"></td><td style=\"width:53px;\"><input style=\"text-align:right; width:53px;\" type=\"text\" id=\"nquantitymin$i\" name=\"nquantitymin$i\" value=\"$rowp->n_quantity_min\"></td><td align=center><a href=\"javascript:xxxp('$rowp->i_promo','$rowp->i_product','$rowp->i_product_grade','$rowp->i_product_motif','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td></tr></tbody>";
								}
							}
		 					echo "</table>";
						}
						?>
					</div>
					<div id="detailheadercustomer" align="center">
						<?php 
						if($jmlitemc>0){
							echo "<table id=\"itemc\" class=\"listtable\" style=\"width:750px;\"><tr><th style=\"width:25px;\"  align=\"center\">No</th><th style=\"width:70px;\" align=\"center\">Kode</th><th style=\"width:255px;\" align=\"center\">Nama Pelanggan</th><th style=\"width:345px;\" align=\"center\">Alamat</th><th style=\"width:50px;\"  align=\"center\">Action</th></tr></table>";
						}
						?>
					</div>
					<div id="detailcustomer" align="center">
						<?php 
						if($jmlitemc>0){
							$i=0;
							foreach($detailc as $rowc){
								$i++;
								echo "<table class=\"listtable\" style=\"width:750px;\"><tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"barisc$i\" name=\"barisc$i\" value=\"$i\"><td style=\"width:67px;\"><input style=\"width:67px;\" readonly type=\"text\" id=\"icustomer$i\" name=\"icustomer$i\" value=\"$rowc->i_customer\"></td><td style=\"width:247px;\"><input style=\"width:247px;\" readonly type=\"text\" id=\"ecustomername$i\" name=\"ecustomername$i\" value=\"$rowc->e_customer_name\"></td><td style=\"width:333px;\"><input readonly style=\"width:333px;\"  type=\"text\" id=\"ecustomeraddress$i\" name=\"ecustomeraddress$i\" value=\"$rowc->e_customer_address\"></td><td align=center><a href=\"javascript:xxxc('$rowc->i_promo','$rowc->i_customer','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td></tr></tbody>";
							}
		 					echo "</table>";
						}
						?>
					</div>
					<div id="detailheadercustomergroup" align="center">
						<?php 
						for($i=1;$i<=$jmlitemg;$i++){
							echo "<table id=\"itemg\" class=\"listtable\" style=\"width:750px;\"><tr><th style=\"width:25px;\"  align=\"center\">No</th><th style=\"width:70px;\" align=\"center\">Kode Group</th><th style=\"width:625px;\" align=\"center\">Nama Group</th><th style=\"width:50px;\"  align=\"center\">Action</th></tr></table>";
						}
						?>
					</div>
					<div id="detailcustomergroup" align="center">
						<?php 
						if($jmlitemg>0){
							$i=0;
							foreach($detailg as $rowg){
								$i++;
								echo "<table class=\"listtable\" style=\"width:750px;\"><tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"barisg$i\" name=\"barisg$i\" value=\"$i\"><td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" id=\"icustomergroup$i\" name=\"icustomergroup$i\" value=\"$rowg->i_customer_group\"></td><td style=\"width:587px;\"><input style=\"width:587px;\" readonly type=\"text\" id=\"ecustomergroupname$i\" name=\"ecustomergroupname$i\" value=\"$rowg->e_customer_groupname\"></td><td align=center><a href=\"javascript:xxxg('$rowg->i_promo','$rowg->i_customer_group','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td></tr></tbody>";
						
							}
		 					echo "</table>";
						}
						?>
					</div>
<!---->
					<div id="detailheaderarea" align="center">
						<?php 
						if($jmlitema>0){
							echo "<table id=\"itema\" class=\"listtable\" style=\"width:750px;\"><tr><th style=\"width:25px;\"  align=\"center\">No</th><th style=\"width:55px;\" align=\"center\">Kode</th><th style=\"width:600px;\" align=\"center\">Area</th><th style=\"width:50px;\"  align=\"center\">Action</th></tr></table>";
						}
						?>
					</div>
					<div id="detailarea" align="center">
						<?php 
						if($jmlitema>0){
							$i=0;
							foreach($detaila as $rowa){
								$i++;
								echo "<table class=\"listtable\" style=\"width:750px;\"><tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"barisa$i\" name=\"barisa$i\" value=\"$i\"><td style=\"width:53px;\"><input style=\"width:53px;\" readonly type=\"text\" id=\"iarea$i\" name=\"iarea$i\" value=\"$rowa->i_area\"></td><td style=\"width:600px;\"><input style=\"width:600px;\" readonly type=\"text\" id=\"eareaname$i\" name=\"eareaname$i\" value=\"$rowa->e_area_name\"></td><td align=center><a href=\"javascript:xxxa('$rowa->i_promo','$rowa->i_area','".$this->lang->line('delete_confirm')."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td></tr></tbody>";
							}
		 					echo "</table>";
						}
						?>
					</div>
<!---->
					<div id="pesan"></div>
					<input type="hidden" name="jmlp" id="jmlp" value="<?php echo $jmlitemp; ?>">
					<input type="hidden" name="jmlc" id="jmlc" value="<?php echo $jmlitemc; ?>">
					<input type="hidden" name="jmlg" id="jmlg" value="<?php echo $jmlitemg; ?>">
					<input type="hidden" name="jmla" id="jmla" value="<?php echo $jmlitema; ?>">
					<input type="hidden" name="ipromodelete" 					id="ipromodelete" 				value="">
					<input type="hidden" name="icustomerdelete" 			id="icustomerdelete" 			value="">
					<input type="hidden" name="iareadelete"			 			id="iareadelete"		 			value="">
					<input type="hidden" name="icustomergroupdelete"	id="icustomergroupdelete" value="">
					<input type="hidden" name="iproductdelete" 				id="iproductdelete" 			value="">
					<input type="hidden" name="iproductgradedelete" 	id="iproductgradedelete" 	value="">
					<input type="hidden" name="iproductmotifdelete" 	id="iproductmotifdelete" 	value="">
<!--	  		</div>-->
			</div>
		</div>
		<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxxp(a,b,c,d,g){
    if (confirm(g)==1){
			document.getElementById("ipromodelete").value=a;
			document.getElementById("iproductdelete").value=b;
			document.getElementById("iproductgradedelete").value=c;
			document.getElementById("iproductmotifdelete").value=d;
//			formna=document.getElementById("promoformupdate");
//			formna.action="<?php echo site_url(); ?>"+"/promo/cform/deletep";
//			formna.onsubmit='$.ajax({url: "promo/cform/deletep", data: $(this.elements).serialize(), success: function(response){$("#pesan").html(response);}, type: "post", dataType: "html"}); return false;';
//			formna.submit();
			show("promo/cform/deletep/"+a+"/"+b+"/"+c+"/"+d+"/","#main");
    }
  }
  function xxxc(a,b,g){
    if (confirm(g)==1){
			document.getElementById("ipromodelete").value=a;
			document.getElementById("icustomerdelete").value=b;
			show("promo/cform/deletec/"+a+"/"+b+"/","#main");
    }
  }
  function xxxa(a,b,g){
    if (confirm(g)==1){
			document.getElementById("ipromodelete").value=a;
			document.getElementById("iareadelete").value=b;
			show("promo/cform/deletea/"+a+"/"+b+"/","#main");
    }
  }
  function xxxg(a,b,g){
    if (confirm(g)==1){
	  document.getElementById("ipromodelete").value=a;
	  document.getElementById("icustomergroupdelete").value=b;
//	  formna=document.getElementById("promoformupdate");
//	  formna.action="<?php echo site_url(); ?>"+"/promo/cform/deleteg";
//	  formna.submit();
			show("promo/cform/deleteg/"+a+"/"+b+"/","#main");
    }
  }
  function tambah_product(a){
    so_inner=document.getElementById("detailheaderproduct").innerHTML;
    si_inner=document.getElementById("detailproduct").innerHTML;
	jenis	=document.getElementById("epromotypename").value;
	tipe	=document.getElementById("ipromotype").value;
    if(so_inner==''){
		so_inner = '<table id="itemp" class="listtable" style="width:750px;">';
		so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:70px;" align="center">Kode</th>';
		so_inner+= '<th style="width:400px;" align="center">Nama Barang</th>';
//		if((tipe=='2')||(tipe=='4')){
		if((tipe=='2')||(tipe=='4')||(tipe=='5')||(tipe=='6')){
			so_inner+= '<th style="width:100px;" align="center">Motif</th>';
			so_inner+= '<th style="width:100px;" align="center">Harga</th>';
		}else{
			so_inner+= '<th style="width:200px;" align="center">Motif</th>';
		}
		so_inner+= '<th style="width:55px;"  align="center">Min Pesan</th></tr>';
		document.getElementById("detailheaderproduct").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		tipe=document.getElementById("jmlp").value;
		document.getElementById("jmlp").value=parseFloat(document.getElementById("jmlp").value)+1;
		juml=document.getElementById("jmlp").value;	
		si_inner='<table class="listtable" style="width: 750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisp'+a+'" name="barisp'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
//		if((tipe=='2')||(tipe=='4')){
		if((tipe=='2')||(tipe=='4')||(tipe=='5')||(tipe=='6')){
			si_inner+='<td style="width:328px;"><input style="width:328px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
			si_inner+='<td style="width:94px;"><input readonly style="width:94px;" type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
			si_inner+='<td style="width:95px;"><input style="width:95px; text-align:right;" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		}else{
			si_inner+='<td style="width:333px;"><input style="width:333px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
			si_inner+='<td style="width:189px;"><input readonly style="width:189px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
			si_inner+='<input type="hidden" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		}
		si_inner+='<td style="width:53px;"><input style="text-align:right; width:53px;" type="text" id="nquantitymin'+a+'" name="nquantitymin'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jmlp").value=parseFloat(document.getElementById("jmlp").value)+1;
		juml=document.getElementById("jmlp").value;
		si_inner=si_inner+'<table class="listtable" style="width: 750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisp'+a+'" name="barisp'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		if((tipe=='2')||(tipe=='4')||(tipe=='5')||(tipe=='6')){
//		if((tipe=='2')||(tipe=='4')){
			si_inner+='<td style="width:328px;"><input style="width:328px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
			si_inner+='<td style="width:94px;"><input readonly style="width:94px;" type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
			si_inner+='<td style="width:95px;"><input style="width:95px; text-align:right;" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		}else{
			si_inner+='<td style="width:333px;"><input style="width:333px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
			si_inner+='<td style="width:189px;"><input readonly style="width:189px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
			si_inner+='<input type="hidden" id="vunitprice'+a+'" name="vunitprice'+a+'" value=""></td>';
		}
		si_inner+='<td style="width:53px;"><input style="text-align:right; width:53px;" type="text" id="nquantitymin'+a+'" name="nquantitymin'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var barisp			=Array()
    var iproduct		=Array();
    var eproductname		=Array();
    var vunitprice		=Array();
    var nquantitymin		=Array();
    var motif		=Array();
    var motifname		=Array();
    for(i=1;i<a;i++){
		j++;
		barisp[j]			=document.getElementById("barisp"+i).value;
		iproduct[j]			=document.getElementById("iproduct"+i).value;
		eproductname[j]		=document.getElementById("eproductname"+i).value;
		vunitprice[j]		=document.getElementById("vunitprice"+i).value;
		nquantitymin[j]		=document.getElementById("nquantitymin"+i).value;
		motif[j]			=document.getElementById("motif"+i).value;
		motifname[j]		=document.getElementById("emotifname"+i).value;
    }
    document.getElementById("detailproduct").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("barisp"+i).value=barisp[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vunitprice"+i).value=vunitprice[j];
		document.getElementById("nquantitymin"+i).value=nquantitymin[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("emotifname"+i).value=motifname[j];
    }
    lebar =500;
    tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/promo/cform/productupdate/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a,b,c,d){
  	 cek='false';
  	 if((document.getElementById("dpromo").value!='') &&
  	 	(document.getElementById("epromoname").value!='')) {
  	 	if((a==0)&&(b==0)&&(c==0)&&(d==0)){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
			if(a>0){
	   			for(i=1;i<=a;i++){
					if((document.getElementById("iproduct"+i).value=='') ||
						(document.getElementById("eproductname"+i).value=='') ||
						(document.getElementById("nquantitymin"+i).value=='')){
						alert('Data item masih ada yang salah !!!');
						exit();
						cek='false';
					}else{
						cek='true';	
					} 
				}
			}
			if(b>0){
	   			for(i=1;i<=b;i++){
					if((document.getElementById("icustomer"+i).value=='') ||
						(document.getElementById("ecustomername"+i).value=='')){
						alert('Data item masih ada yang salah !!!');
						exit();
						cek='false';
					}else{
						cek='true';	
					} 
				}
			}
			if(c>0){
	   			for(i=1;i<=c;i++){
					if((document.getElementById("icustomergroup"+i).value=='') ||
						(document.getElementById("ecustomergroupname"+i).value=='')){
						alert('Data item masih ada yang salah !!!');
						exit();
						cek='false';
					}else{
						cek='true';	
					} 
				}
			}
			if(cek=='false'){
				cek='true';
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
	show('listpromo/cform/','#tmpx')
  }
  function cekproduct(){
	ap=document.getElementById("fallproduct").value;
	ac=document.getElementById("fallcustomer").value;
	ag=document.getElementById("fcustomergroup").value;
	if(ap=='on'){
		document.getElementById("fallproduct").value='';
		document.getElementById("fallbaby").disabled=false;
		document.getElementById("fallreguler").disabled=false;
		document.getElementById("fallnb").disabled=false;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}else{
		document.getElementById("fallproduct").value='on';
		document.getElementById("detailheaderproduct").innerHTML='';
		document.getElementById("detailproduct").innerHTML='';
		document.getElementById("jmlp").value='0';
		document.getElementById("fallbaby").disabled=true;
		document.getElementById("fallreguler").disabled=true;
		document.getElementById("fallnb").disabled=true;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\"></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}
  }
  function cekcustomer(){
	ac=document.getElementById("fallcustomer").value;
	ap=document.getElementById("fallproduct").value;
	ab=document.getElementById("fallbaby").value;
	ar=document.getElementById("fallreguler").value;
	an=document.getElementById("fallnb").value;
	if(ac=='on'){
		document.getElementById("fallcustomer").value='';
		document.getElementById("fcustomergroup").disabled=false;
		if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}else{
		document.getElementById("fallcustomer").value='on';
		document.getElementById("detailheadercustomer").innerHTML='';
		document.getElementById("detailcustomer").innerHTML='';
		document.getElementById("jmlc").value='0';
		document.getElementById("fcustomergroup").disabled=true;
		if( (ap=='on')||(ab=='on')||(ar=='on')||(ar=='on') ){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\"></td></tr></table>";
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
		}
	}
  }
  function cekcustomergroup(){
	ap=document.getElementById("fallproduct").value;
	ag=document.getElementById("fcustomergroup").value;
	ab=document.getElementById("fallbaby").value;
	an=document.getElementById("fallnb").value;
	ar=document.getElementById("fallreguler").value;
	if(ag=='on'){
		document.getElementById("fcustomergroup").value='';
		document.getElementById("fallcustomer").disabled=false;
		document.getElementById("detailheadercustomergroup").innerHTML='';
		document.getElementById("detailcustomergroup").innerHTML='';
		document.getElementById("jmlg").value='0';
		if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}else{
		document.getElementById("fcustomergroup").value='on';
		document.getElementById("fallcustomer").disabled=true;
		document.getElementById("detailheadercustomer").innerHTML='';
		document.getElementById("detailcustomer").innerHTML='';
		document.getElementById("jmlc").value='0';
		if( (ap=='on')||(ab=='on')||(ar=='on')||(ar=='on') ){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah  Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick=\"clearitem();\">&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";
		}
	}
  }
  function tambah_customer(a){
    so_inner=document.getElementById("detailheadercustomer").innerHTML;
    si_inner=document.getElementById("detailcustomer").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemc" class="listtable" style="width:750px;">';
		so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:70px;" align="center">Kode</th>';
		so_inner+= '<th style="width:255px;" align="center">Nama Pelanggan</th>';
		so_inner+= '<th style="width:345px;" align="center">Alamat</th>';
		so_inner+= '<th style="width:50px;" align="center">Action</th></tr></table>';
		document.getElementById("detailheadercustomer").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jmlc").value=parseFloat(document.getElementById("jmlc").value)+1;
		juml=document.getElementById("jmlc").value;	
		si_inner='<table class="listtable" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisc'+a+'" name="barisc'+a+'" value="'+a+'">';
		si_inner+='<td style="width:67px;"><input style="width:67px;" readonly type="text" id="icustomer'+a+'" name="icustomer'+a+'" value=""></td>';
		si_inner+='<td style="width:247px;"><input style="width:247px;" readonly type="text" id="ecustomername'+a+'" name="ecustomername'+a+'" value=""></td>';
		si_inner+='<td style="width:333px;"><input readonly style="width:333px;"  type="text" id="ecustomeraddress'+a+'" name="ecustomeraddress'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jmlc").value=parseFloat(document.getElementById("jmlc").value)+1;
		juml=document.getElementById("jmlc").value;
		si_inner+='<table class="listtable" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisc'+a+'" name="barisc'+a+'" value="'+a+'">';
		si_inner+='<td style="width:67px;"><input style="width:67px;" readonly type="text" id="icustomer'+a+'" name="icustomer'+a+'" value=""></td>';
		si_inner+='<td style="width:247px;"><input style="width:247px;" readonly type="text" id="ecustomername'+a+'" name="ecustomername'+a+'" value=""></td>';
		si_inner+='<td style="width:333px;"><input readonly style="width:333px;"  type="text" id="ecustomeraddress'+a+'" name="ecustomeraddress'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var barisc		=Array()
    var icustomer	=Array();
    var ecustomername	=Array();
    var ecustomeraddress=Array();
    for(i=1;i<a;i++){
		j++;
		barisc[j]		=document.getElementById("barisc"+i).value;
		icustomer[j]		=document.getElementById("icustomer"+i).value;
		ecustomername[j]	=document.getElementById("ecustomername"+i).value;
		ecustomeraddress[j]	=document.getElementById("ecustomeraddress"+i).value;
    }
    document.getElementById("detailcustomer").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("barisc"+i).value=barisc[j];
		document.getElementById("icustomer"+i).value=icustomer[j];
		document.getElementById("ecustomername"+i).value=ecustomername[j];
		document.getElementById("ecustomeraddress"+i).value=ecustomeraddress[j];
    }
    lebar =500;
    tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/promo/cform/customerupdate/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_customergroup(a){
	if(parseFloat(a)<=1){
		so_inner=document.getElementById("detailheadercustomergroup").innerHTML;
		si_inner=document.getElementById("detailcustomergroup").innerHTML;
		if(so_inner==''){
			so_inner = '<table id="itemc" class="listtable" style="width:750px;">';
			so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner+= '<th style="width:70px;" align="center">Kode Group</th>';
			so_inner+= '<th style="width:625px;" align="center">Nama Group</th>';
			so_inner+= '<th style="width:50px;" align="center">Action</th></tr></table>';
			document.getElementById("detailheadercustomergroup").innerHTML=so_inner;
		}else{
			so_inner=''; 
		}
		if(si_inner==''){
			document.getElementById("jmlg").value=parseFloat(document.getElementById("jmlg").value)+1;
			juml=document.getElementById("jmlg").value;	
			si_inner='<table class="listtable" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisg'+a+'" name="barisg'+a+'" value="'+a+'">';
			si_inner+='<td style="width:66px;"><input style="width:66px;" readonly type="text" id="icustomergroup'+a+'" name="icustomergroup'+a+'" value=""></td>';
			si_inner+='<td style="width:587px;"><input style="width:587px;" readonly type="text" id="ecustomergroupname'+a+'" name="ecustomergroupname'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
		}else{
			document.getElementById("jmlg").value=parseFloat(document.getElementById("jmlg").value)+1;
			juml=document.getElementById("jmlg").value;
			si_inner+='<table class="listtable" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisg'+a+'" name="barisg'+a+'" value="'+a+'">';
			si_inner+='<td style="width:66px;"><input style="width:66px;" readonly type="text" id="icustomergroup'+a+'" name="icustomergroup'+a+'" value=""></td>';
			si_inner+='<td style="width:587px;"><input style="width:587px;" readonly type="text" id="ecustomergroupname'+a+'" name="ecustomergroupname'+a+'" value=""></td><td>&nbsp;</td></tr></tbody></table>';
		}
		j=0;
		var barisg				= Array()
		var icustomergroup		= Array();
		var ecustomergroupname	= Array();
		for(i=1;i<a;i++){
			j++;
			barisg[j]				=document.getElementById("barisg"+i).value;
			icustomergroup[j]		=document.getElementById("icustomergroup"+i).value;
			ecustomergroupname[j]	=document.getElementById("ecustomergroupname"+i).value;
		}
		document.getElementById("detailcustomergroup").innerHTML=si_inner;
		j=0;
		for(i=1;i<a;i++){
			j++;
			document.getElementById("barisg"+i).value=barisg[j];
			document.getElementById("icustomergroup"+i).value=icustomergroup[j];
			document.getElementById("ecustomergroupname"+i).value=ecustomergroupname[j];
		}
		lebar =500;
		tinggi=400;
		eval('window.open("<?php echo site_url(); ?>"+"/promo/cform/customergroupupdate/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}else{
		alert("Maksimal 1 Group Pelanggan !!!!!");
	}
  }
  function cektype(a){
	prodis=document.getElementById("prodis");
	if((a=='1')||(a=='3')){
		prodis.innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=15% class=batas>&nbsp;</td><td class=batas>&nbsp;</td></tr><tr><td width=15%>Discount I</td><td><input id=npromodiscount1 name=npromodiscount1 value=0></td></tr><tr><td width=15% class=batas>Discount II</td><td class=batas><input id=npromodiscount2 name=npromodiscount2 value=0></td></tr></table>";
	}else if((a=='5')||(a=='6')){
		prodis.innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=15% class=batas>&nbsp;</td><td class=batas>&nbsp;</td></tr><tr><td width=15%>Discount</td><td><input id=npromodiscount1 name=npromodiscount1 value=0></td></tr></table	>";
	}else{
		prodis.innerHTML="";
	}
	document.getElementById("ipromotype").value=a;
  }
  function cekbaby(){
	ab=document.getElementById("fallbaby").value;
	ac=document.getElementById("fallcustomer").value;
	ag=document.getElementById("fcustomergroup").value;
	an=document.getElementById("fallnb").value;
	if(ab=='on'){
		document.getElementById("fallbaby").value='';
		document.getElementById("fallproduct").disabled=false;
		document.getElementById("fallreguler").disabled=false;
		document.getElementById("fallnb").disabled=false;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Baby\" type=\"button\" onclick=\"tambah_baby(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Baby\" type=\"button\" onclick=\"tambah_baby(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Baby\" type=\"button\" onclick=\"tambah_baby(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}else{
		document.getElementById("fallbaby").value='on';
		document.getElementById("detailheaderproduct").innerHTML='';
		document.getElementById("detailproduct").innerHTML='';
		document.getElementById("jmlp").value='0';
		document.getElementById("fallproduct").disabled=true;
		document.getElementById("fallreguler").disabled=true;
		document.getElementById("fallnb").disabled=true;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}
  }
  function cekreguler(){
	ar=document.getElementById("fallreguler").value;
	ac=document.getElementById("fallcustomer").value;
	ag=document.getElementById("fcustomergroup").value;
	if(ar=='on'){
		document.getElementById("fallreguler").value='';
		document.getElementById("fallproduct").disabled=false;
		document.getElementById("fallbaby").disabled=false;
		document.getElementById("fallnb").disabled=false;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Reguler\" type=\"button\" onclick=\"tambah_reguler(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Reguler\" type=\"button\" onclick=\"tambah_reguler(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Reguler\" type=\"button\" onclick=\"tambah_reguler(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}else{
		document.getElementById("fallreguler").value='on';
		document.getElementById("detailheaderproduct").innerHTML='';
		document.getElementById("detailproduct").innerHTML='';
		document.getElementById("jmlp").value='0';
		document.getElementById("fallproduct").disabled=true;
		document.getElementById("fallbaby").disabled=true;
		document.getElementById("fallnb").disabled=true;
		if(ac=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'></td></tr></table>";
		}else if(ag=='on'){
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		}else{
			document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		}
	}
  }
/*****************/
function tambah_area(a){
    so_inner=document.getElementById("detailheaderarea").innerHTML;
    si_inner=document.getElementById("detailarea").innerHTML;
    if(so_inner.replace(/^\s\s*$/, '').replace(/\s\s*$/, '')==''){
			so_inner = '<table id="itemc" class="listtable" style="width:750px;">';
			so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner+= '<th style="width:55px;" align="center">Kode</th>';
			so_inner+= '<th style="width:600px;" align="center">Area</th>';
			so_inner+= '<th style="width:50px;" align="center"></th></tr>';
			document.getElementById("detailheaderarea").innerHTML=so_inner;
    }else{
			so_inner=''; 
    }
    if(si_inner==''){
			document.getElementById("jmla").value=parseFloat(document.getElementById("jmla").value)+1;
			juml=document.getElementById("jmla").value;	
			si_inner='<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="barisa'+a+'" name="barisa'+a+'" value="'+a+'">';
			si_inner+='<td style="width:55px;"><input style="width:55px;" readonly type="text" id="iarea'+a+'" name="iarea'+a+'" value=""></td>';
			si_inner+='<td style="width:602px;"><input style="width:602px;" readonly type="text" id="eareaname'+a+'" name="eareaname'+a+'" value=""></td><td style="width:50px;">&nbsp;</td></tr></tbody>';
    }else{
			document.getElementById("jmla").value=parseFloat(document.getElementById("jmla").value)+1;
			juml=document.getElementById("jmla").value;
			si_inner+='<tbody><tr><td style="width:24px;"><input style="width:24px;" readonly type="text" id="barisa'+a+'" name="barisa'+a+'" value="'+a+'">';
			si_inner+='<td style="width:55px;"><input style="width:55px;" readonly type="text" id="iarea'+a+'" name="iarea'+a+'" value=""></td>';
			si_inner+='<td style="width:602px;"><input style="width:602px;" readonly type="text" id="eareaname'+a+'" name="eareaname'+a+'" value=""></td><td style="width:50px;">&nbsp;</td></tr></tbody>';
    }

    j=0;
    var barisa		=Array()
    var iarea			=Array();
    var eareaname	=Array();
    for(i=1;i<a;i++){
			j++;
			barisa[j]		= document.getElementById("barisa"+i).value;
			iarea[j]		= document.getElementById("iarea"+i).value;
			eareaname[j]= document.getElementById("eareaname"+i).value;
    }
    document.getElementById("detailarea").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
			j++;
			document.getElementById("barisa"+i).value=barisa[j];
			document.getElementById("iarea"+i).value=iarea[j];
			document.getElementById("eareaname"+i).value=eareaname[j];
    }
    lebar =500;
    tinggi=400;
		showModal("promo/cform/area/"+a+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
//		eval('window.open("<?php echo site_url(); ?>"+"/promo/cform/area/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
/*****************/
/************/
  function cekarea(){
		ac=document.getElementById("fallcustomer").value;
		ap=document.getElementById("fallproduct").value;
		ab=document.getElementById("fallbaby").value;
		ar=document.getElementById("fallreguler").value;
		an=document.getElementById("fallnb").value;
		aa=document.getElementById("fallarea").value;
		if(aa=='on'){
			document.getElementById("fallarea").value='';
			if(ac=='on'){
				if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambaharea\" id=\"cmdtambaharea\" value=\"Tambah Area\" type=\"button\" onclick=\"tambah_area(parseFloat(document.getElementById('jmla').value)+1);\"></td></tr></table>";
				}else{
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambaharea\" id=\"cmdtambaharea\" value=\"Tambah Area\" type=\"button\" onclick=\"tambah_area(parseFloat(document.getElementById('jmla').value)+1);\"></td></tr></table>";
				}
			}else{
				if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\">&nbsp;<input name=\"cmdtambaharea\" id=\"cmdtambaharea\" value=\"Tambah Area\" type=\"button\" onclick=\"tambah_area(parseFloat(document.getElementById('jmla').value)+1);\"></td></tr></table>";
				}else{
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\">&nbsp;<input name=\"cmdtambaharea\" id=\"cmdtambaharea\" value=\"Tambah Area\" type=\"button\" onclick=\"tambah_area(parseFloat(document.getElementById('jmla').value)+1);\"></td></tr></table>";
				}
			}
		}else{
			document.getElementById("fallarea").value='on';
			document.getElementById("detailheaderarea").innerHTML='';
			document.getElementById("detailarea").innerHTML='';
			document.getElementById("jmla").value='0';
			if(ac=='on'){
				if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'></td></tr></table>";
				}else{
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
				}
			}else{
				if( (ap=='on')||(ab=='on')||(ar=='on')||(an=='on') ){
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
				}else{
					document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"listpromo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk\" type=\"button\" onclick=\"tambah_product(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
				}
			}
		}
  }
  function ceknb(){
	  ab=document.getElementById("fallbaby").value;
	  ac=document.getElementById("fallcustomer").value;
	  ag=document.getElementById("fcustomergroup").value;
	  an=document.getElementById("fallnb").value;
	  if(an=='on'){
		  document.getElementById("fallbaby").disabled=false;
		  document.getElementById("fallproduct").disabled=false;
		  document.getElementById("fallreguler").disabled=false;
		  document.getElementById("fallnb").value='';
		  if(ac=='on'){
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Non Bedding\" type=\"button\" onclick=\"tambah_nb(parseFloat(document.getElementById('jmlp').value)+1);\"></td></tr></table>";
		  }else if(ag=='on'){
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Non Bedding\" type=\"button\" onclick=\"tambah_nb(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		  }else{
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahproduct\" id=\"cmdtambahproduct\" value=\"Tambah Produk Baby\" type=\"button\" onclick=\"tambah_nb(parseFloat(document.getElementById('jmlp').value)+1);\">&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		  }
	  }else{
		  document.getElementById("fallnb").value='on';
		  document.getElementById("detailheaderproduct").innerHTML='';
		  document.getElementById("detailproduct").innerHTML='';
		  document.getElementById("jmlp").value='0';
		  document.getElementById("fallproduct").disabled=true;
		  document.getElementById("fallreguler").disabled=true;
		  document.getElementById("fallbaby").disabled=true;
		  if(ac=='on'){
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'></td></tr></table>";
		  }else if(ag=='on'){
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomergroup\" id=\"cmdtambahcustomergroup\" value=\"Tambah Group Pelanggan\" type=\"button\" onclick=\"tambah_customergroup(parseFloat(document.getElementById('jmlg').value)+1);\"></td></tr></table>";		
		  }else{
			  document.getElementById("batten").innerHTML="<table class=mastertable width=100% cellspacing=0 cellpadding=1><tr><td width=\"100%\" align=center colspan=4><input name=\"login\" id=\"login\" value=\"Simpan\" type=\"submit\" onclick=\"dipales(parseFloat(document.getElementById('jmlp').value),parseFloat(document.getElementById('jmlc').value),parseFloat(document.getElementById('jmlg').value),parseFloat(document.getElementById('jmla').value));\">&nbsp;<input name=\"cmdreset\" id=\"cmdreset\" value=\"Batal\" type=\"button\" onclick='show(\"promo/cform/\",\"#main\")'>&nbsp;<input name=\"cmdtambahcustomer\" id=\"cmdtambahcustomer\" value=\"Tambah Pelanggan\" type=\"button\" onclick=\"tambah_customer(parseFloat(document.getElementById('jmlc').value)+1);\"></td></tr></table>";
		  }
	  }
  }
/************/
  function view_kodeharga(){
    kode=document.getElementById("ipromotype").value;
    showModal("promo/cform/pricegroup/"+kode+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
</script>
