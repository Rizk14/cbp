<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
@media print {
input.noPrint { display: none; }
}
@page
        {
            size: auto;   /* auto is the initial value 
            margin: 0mm;   this affects the margin in the printer settings 
        */
*{
size: landscape;
}

@page { size: Letter; 
        margin: 0mm;  /* this affects the margin in the printer settings */
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.wrap {
	margin: 0 auto;
	text-align: left;
}

.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
 border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.catatan {
  font-size: 14px;
  FONT-WEIGHT: normal; 
}
.nmper {
	margin-top: 0;
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 12px;
  font-weight:normal;
}
.eusinya {
  font-size: 8px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.garisatas { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
?>
  <table width="100%" class="nmper" border="0">
    <tr>
      <td colspan="3" width="50%" class="huruf judul" ><?php echo NmPerusahaan; ?></td>
      <td >Kepada Yth.</td>
    </tr>
    <tr>
      <td colspan="3" width="50%"><?php echo AlmtPerusahaan; ?></td>
      <td><?php echo rtrim($row->e_customer_pkpname);?></td>
    </tr>
    <tr>
      <td colspan="3" width="50%"><?php echo KotaPerusahaan; ?></td>
      <td><?php echo trim($row->e_customer_pkpaddress);?></td>
    </tr>
    <tr>
      <td colspan="3" width="50%">Telp.&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo TlpPerusahaan; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" width="50%">NPWP.&nbsp;:&nbsp;<?php echo NPWPPerusahaan; ?></td>
      <td>NPWP.&nbsp;:&nbsp;<?php echo $row->e_customer_pkpnpwp; ?></td>
    </tr>
    <tr>
    <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td align="center" colspan="4"><?php echo"NO DOK/FAKTUR  : ".substr($row->i_nota,8,8)."/".substr($row->i_faktur_komersial,8,6);?></td>
    </tr>
      <td align="center" colspan="4"><?php echo "KODE SL/LANGG. : 01/".$row->i_salesman."-".$row->i_area."/".substr($row->i_customer,2,3);?></td>
    </tr>
    <tr align="center">
      <td colspan="4">
        <table width="98%" class="nmper" border="0">
          <tr>
            <td class="garisatas garisbawah">
              NO.
            </td>
            <td class="garisatas garisbawah">
              KD-BARANG
            </td>
            <td align="center" width="800px" class="garisatas garisbawah">
              NAMA BARANG
            </td>
            <td align="center" width="100px" class="garisatas garisbawah">
              UNIT
            </td>
            <td align="right" width="100px" class="garisatas garisbawah">
              HARGA
            </td>
            <td align="right" width="70px" class="garisatas garisbawah">
              JUMLAH
            </td>
          </tr>
          <?php 
		        $i	= 0;
		        $hrg= 0;
		        $total=0;
		      foreach($detail as $rowi){
			    $i++;
			      ?>
          <tr>
            <td width="25">
              <?php echo $i;?>
            </td>
            <td align="center" width="20px">
              <?php echo $rowi->i_product;?>
            </td>
            <td>
              <?php 
			        if(strlen($rowi->e_product_name )>50){
				        $nam	= substr($rowi->e_product_name,0,50);
			        }else{
				        $nam	= $rowi->e_product_name.str_repeat(" ",50-strlen($rowi->e_product_name ));
			        }
              echo $nam;?>
            </td>
            <td align="center">
              <?php 
              echo number_format($rowi->n_deliver);?>
            </td>
            <td align="right">
              <?php            
              if($row->f_plus_ppn=='t'){
                 echo number_format($rowi->v_unit_price);
              }else{
                 echo number_format($rowi->v_unit_price/1.1);
              };?>
            </td>
            <td align="right"><?php 
              if($row->f_plus_ppn=='t'){
                echo number_format($rowi->n_deliver*$rowi->v_unit_price);
                $tot  = $rowi->n_deliver*$rowi->v_unit_price;
              }else{
                echo number_format($rowi->n_deliver*($rowi->v_unit_price/1.1));
                $tot  = $rowi->n_deliver*($rowi->v_unit_price/1.1);
              }
              ?>
            </td>
          </tr>
<?php 
      $total = $total+$tot;
      }?>
    <tr>
      <td colspan="6" class="garisbawah">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
      <td>TOTAL</td>
      <td align="right">:</td> 
      <td align="right">
      <?php 
      $row->v_nota_gross=$total;
      echo number_format($row->v_nota_gross);
?></td>
    </tr>
<?php 
      if( ($row->n_nota_discount1+$row->n_nota_discount2+$row->n_nota_discount3+$row->n_nota_discount4==0) && $row->v_nota_discounttotal <> 0 )
      {
        $vdisc1=$row->v_nota_discounttotal;
        $vdisc2=0;
        $vdisc3=0;
        $vdisc4=0;
        $vdistot   = round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
        if( ($row->f_plus_ppn=='f') ){
            $vdistot=$vdistot/1.1;
        }
      }
      else
      {
         $vdisc1=($total*$row->n_nota_discount1)/100;
         $vdisc2=((($total-$vdisc1)*$row->n_nota_discount2)/100);
         $vdisc3=((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100);
         $vdisc4=((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100);
         $vdistot   = round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
      }
      $row->v_nota_discounttotal=$vdistot;
?>
    <tr>
      <td colspan="3">&nbsp;</td>
      <td>POTONGAN </td>
      <td align="right">:</td>
      <td align="right"><?php echo number_format($row->v_nota_discounttotal); ?></td>
    </tr>
<?php 
  if($row->f_plus_ppn=='f'){
    $vppn=(round($total)-round($vdistot))*0.1;
    $row->v_nota_ppn=$vppn;
?>
    <tr>
      <td><?php 
      echo $row->v_nota_ppn;
      ?></td>
    </tr>
<?php 
  }
?>    
    <tr>
      <td colspan="5">&nbsp;</td>
      <td align="right">--------------------</td>
    </tr>
<?php 
  if($row->f_plus_ppn=='f'){
    $row->v_nota_netto=round($row->v_nota_gross)-round($row->v_nota_discounttotal)+$vppn;
  }else{
    $row->v_nota_netto=$row->v_nota_gross-$row->v_nota_discounttotal;
  }
?>
    <tr>
      <td colspan="3">&nbsp;</td>
      <td class="">NILAI FAKTUR</td>
      <td align="right">:</td>
      <td align="right">
      <?php 
        echo number_format($row->v_nota_netto);
      ?></td>
    </tr>
    <tr>
      <td colspan="5">&nbsp;</td>
      <td align="right">--------------------</td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
      <td class="" colspan="2">DASAR PENGENAAN PAJAK&nbsp;:</td>
      <td align="right">
      <?php 
        $ndpp=round($row->v_nota_netto/1.1);
        $nppn=round($row->v_nota_netto/1.1*0.1);
        echo number_format($ndpp);
      ?></td>
    </tr>
    <tr>
      <td colspan="3">&nbsp;</td>
      <td class="">PPN 10%</td>
      <td align="right">:</td>
      <td align="right">
      <?php 
        echo number_format($nppn);
      ?></td>
    </tr>
    <tr>
      <td colspan="5">&nbsp;</td>
      <td align="right">--------------------</td>
    </tr>
</table>
  <tr>
    <td colspan=3>(<?php 
		$bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($row->v_nota_netto));	
    $tmp=explode("-",$row->d_nota);
    $th=$tmp[0];
    $bl=$tmp[1];
    $hr=$tmp[2];
    $dnota=$hr." ".mbulan($bl)." ".$th;
    echo $kata." Rupiah";?>)</td>
  </tr>
</table >
<table width="100%" class="nmper" border="0">
  <tr>
    <td colspan="4" width="200px" align="center">
    <?php echo "Bandung, ".$dnota;?>
    </td>
  </tr>
  <tr>
    <td colspan="4" width="200px" align="center">
      S E & O
    </td>
  </tr>
  <tr><td colspan="4">&nbsp;</tr>
  <tr><td colspan="4">&nbsp;</tr>
  <tr><td colspan="4">&nbsp;</tr>
  <tr>
    <?php $nama=str_replace("%20"," ",$nama);?>
    <td colspan="4" width="200px" align="center">
    <?php echo ucwords(strtolower($nama));?></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">Catatan :</td>
  </tr>
  <tr>
    <td colspan="3">1. Barang-barang yang sudah dibeli tidak dapat ditukar/dikembalikan, kecuali ada perjanjian terlebih dahulu</td>
  </tr>
  <tr>
    <td colspan="3">2. Faktur asli merupakan bukti pembayaran yang sah.</td>
  </tr> 
  <tr>
    <td colspan="3">3. Pembayaran dengan cek/giro berharga baru dianggap sah setelah diuangkan/cair.</td>
  </tr>
</table>

<?php    
  }
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
