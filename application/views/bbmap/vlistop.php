<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
<!--	<?php echo form_open('bbmap/cform/cariop', array('id' => 'listform'));?> -->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'bbmap/cform/cariop','update'=>'#tmp','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
		<th>No OP</th>
		<th>No Reff</th>
		<th>Tgl Reff</th>
		<th>Supplier</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  	$tmp=explode('-',$row->d_op);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$tglop=$tgl.'-'.$bln.'-'.$thn;
			  	$tmp=explode('-',$row->d_reff);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$tglspb=$tgl.'-'.$bln.'-'.$thn;

			  $nama=str_replace("'","\'",$row->e_supplier_name);
			  echo "<tr> 
					<td><a href=\"javascript:setValue('$row->i_op','$row->i_area','$row->e_area_name','$row->i_supplier','$nama')\">$row->i_op</a></td>
					<td><a href=\"javascript:setValue('$row->i_op','$row->i_area','$row->e_area_name','$row->i_supplier','$nama')\">$row->i_reff</a></td>
					<td><a href=\"javascript:setValue('$row->i_op','$row->i_area','$row->e_area_name','$row->i_supplier','$nama')\">$tglspb</a></td>
				  	<td><a href=\"javascript:setValue('$row->i_op','$row->i_area','$row->e_area_name','$row->i_supplier','$nama')\">$row->e_supplier_name</a></td>
					</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	   <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e)
  {
    
    document.getElementById("iop").value=a;
    document.getElementById("iarea").value=b;
    document.getElementById("eareaname").value=c;
    document.getElementById("isupplier").value=d;
    document.getElementById("esuppliername").value=e;
    jsDlgHide("#konten *", "#fade", "#light");
  }

   function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
