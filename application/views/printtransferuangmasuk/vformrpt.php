<body onload="window.print(); closeMe();">

<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">

    	  <table cellspacing="0" cellpadding="0" width="100%">
    	   <tr>
      	    <td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>No Bukti</b></td>
      	    <td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>No DT</b></td>
      	    <td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Tgl DT</b></td>
	    	<td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Tgl Transfer</b></td>
      	    <td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Nama Bank</b></td>
	    	<td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Customer</b></td>
			<td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Penyetor</b></td>
      	    <td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Jumlah</b></td>
	    	<td STYLE="padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; border-right: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;"><b>Sisa</b></td>
	    </tr>
	    <?php 
	    $no=1;  
		if($isi){
			foreach($isi as $row){
			  if($no<$item) {
				$style_row	= "padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; border-right: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;";	
				$style_row1	= "padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;";
			  }	else {
				$style_row	= "padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-bottom: solid 1px #000000; border-left: solid 1px #000000; border-right: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;";	
				$style_row1	= "padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-bottom: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#000000;";
			  }					
			  $icust = $row->icustomer==''?'Belum Ada':$row->icustomer;	
			  $tmp=explode('-',$row->dkum);
			  $tgl=$tmp[2];
			  $bln=$tmp[1];
			  $thn=$tmp[0];
			  $row->dkum=$tgl.'-'.$bln.'-'.$thn;
				
			  if($row->ddt!='') {	
				  $tmp=explode('-',$row->ddt);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->ddt=$tgl.'-'.$bln.'-'.$thn;
			  } else {
				  $tgl=" ";
				  $bln=" ";
				  $thn=" ";
			  }
			  			  
			  echo "<tr>";
				if($row->fkumcancel=='t'){
			  echo "<td style=\"padding-left:2px; padding-right:2px; padding-top:1px; border-top: solid 1px #000000; border-left: solid 1px #000000; font-family:arial; font-size:12px; color:#ff0000;\"><span style=\"\">$row->ikum&nbsp;</span></td>";
				}else{
			  echo "<td style=\"$style_row1\">$row->ikum&nbsp;</td>";
				}
			  echo "
				  <td style=\"$style_row1\">$row->idt&nbsp;</td>
				  <td style=\"$style_row1\">$row->ddt&nbsp;</td>
				  <td style=\"$style_row1\">$row->dkum&nbsp;</td>
				  <td style=\"$style_row1\">$row->ebankname&nbsp;</td>
				  <td style=\"$style_row1\">($icust) $row->ecustomername&nbsp;</td>
				  <td style=\"$style_row1\">$row->ecustomersetor/$row->eremark&nbsp;</td>	
				  <td style=\"$style_row1\" align=right>".number_format($row->vjumlah)."&nbsp;</td>
				  <td style=\"$style_row\" align=right>".number_format($row->vsisa)."&nbsp;</td>
				  ";
echo "
				</tr>";
				$no+=1;	
			}
		}
	      ?>
	    </tbody>
	  </table>

    </td>
  </tr>
</table>

<script language="javascript">

// (C) 2001 www.CodeLifter.com
// http://www.codelifter.com
// Free for all users, but leave in this header

var howLong = 7200;

t = null;
function closeMe(){
t = setTimeout("self.close()",howLong);
}

</script>
