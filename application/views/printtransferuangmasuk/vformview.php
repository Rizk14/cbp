<div id="tmp">
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printtransferuangmasuk/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : 
			<input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >
			&nbsp;
			<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No Bukti</th>
      	    <th>No DT</th>
      	    <th>Tgl DT</th>
	    	<th>Tgl Transfer</th>
      	    <th>Nama Bank</th>
	    	<th>Customer</th>
		<th>Penyetor/Ket</th>
      	    <th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $icust = $row->icustomer==''?'Belum Ada':$row->icustomer;	
			  $tmp=explode('-',$row->dkum);
			  $tgl=$tmp[2];
			  $bln=$tmp[1];
			  $thn=$tmp[0];
			  $row->dkum=$tgl.'-'.$bln.'-'.$thn;
			  $penyetor=trim($row->ecustomersetor).'/'.trim($row->eremark);	
			  if($row->ddt!='') {	
				  $tmp=explode('-',$row->ddt);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->ddt=$tgl.'-'.$bln.'-'.$thn;
			  } else {
				  $tgl=" ";
				  $bln=" ";
				  $thn=" ";
			  }
			  			  
			  echo "<tr>";
				if($row->fkumcancel=='t'){
			  echo "<td><h1>$row->ikum</h1></td>";
				}else{
			  echo "<td>$row->ikum</td>";
				}
			  echo "
				  <td>$row->idt</td>
				  <td>$row->ddt</td>
				  <td>$row->dkum</td>
				  <td>$row->ebankname</td>
				  <td>($icust) $row->ecustomername</td>
				  <td>$penyetor</td>	
				  <td align=right>".number_format($row->vjumlah)."</td>
				  <td align=right>".number_format($row->vsisa)."</td>
				  ";
echo "
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <center><input type="button" id="btncetak" name="btncetak" value="Cetak" onclick="return ccc('<?=$dfrom?>','<?=$dto?>','<?=$iarea?>');"></center>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>

<script language="javascript" type="text/javascript">
  function ccc(a,b,c){
	lebar =900;
    tinggi=600;
    eval('window.open("<?php echo site_url(); ?>"+"/printtransferuangmasuk/cform/cetak/"+a+"/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
