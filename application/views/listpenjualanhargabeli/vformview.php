<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanhargabeli/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
		$i=0;
?>
	  <th align="center">NO</th>
	  <th align="center">KODE<br>BARANG</th>
		<th align="center">NAMA BARANG</th>
		<th align="center">QTY<br>PENJUALAN</th>
		<th align="center">HARGA BELI</th>
		<th align="center">TOTAL RP. BELI</th>
	    <tbody>
<?php 
    $total=0;
    $totalbeli=0;
			foreach($isi as $row){
			$i++;
			echo "<tr>
		  <td>$i</td>			
		  <td>$row->i_product</td>
		  <td>$row->e_product_name</td>
		  <td align='right'>".number_format($row->n_deliver)."</td>
		  <td align='right'>".number_format($row->v_product_mill)."</td>
		  <td align='right'>".number_format($row->total_harga_beli)."</td>
			</tr>";
			$total=$total + $row->n_deliver;
			$totalbeli=$totalbeli + $row->total_harga_beli;
  		}
/*  		echo
  		"<tr><th colspan='2'>TOTAL</th>
		<th>$total</th>
		<th>&nbsp;</th>
		<th>$totalbeli</th>";*/
		}
	      ?>
	    </tbody>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
