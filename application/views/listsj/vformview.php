<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h4><marquee><i>*Jika nomor Surat Jalan (SJ) berubah warna dan membesar, SJ telah dibatalkan</i></marquee></h4>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listsj/cform/view','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="13" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
		<th>No SJ</th>
			<th>Tgl SJ</th>
			<th>Lang</th>
			<th>Nilai</th>
			<th>Area</th>
			<th>SPB</th>
			<th>DKB</th>
			<th>Tgl DKB</th>
			<th>Tgl SJ Rec</th>
			<!-- <th>BAPB</th> -->
			<th class="action">Action</th>
	    <tbody>
		<?php 
		if($isi){
			foreach($isi as $row){
				$tmp=explode('-',$row->d_sj);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_sj=$tgl.'-'.$bln.'-'.$thn;
				
				if($row->d_dkb){
					$tmp=explode('-',$row->d_dkb);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$row->d_dkb=$tgl.'-'.$bln.'-'.$thn;
				}

				if($row->d_sj_receive){
					$tmp=explode('-',$row->d_sj_receive);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$row->d_sj_receive = $tgl.'-'.$bln.'-'.$thn;
				}

			  echo "<tr> ";
			if($row->f_nota_cancel=='t'){
			  echo "<td><h2>$row->i_sj</h2></td>";
			}else{
			  echo "<td>$row->i_sj</td>";
			}
			  echo "  <td>$row->d_sj</td>";
			if(substr($row->i_customer,2,3)!='000'){
					echo "
				  <td valign=top>$row->e_customer_name</td>";
				}
				echo "
          <td align=right>".number_format($row->v_nota_netto)."</td>
				  <td>$row->e_area_name</td>
				  <td>$row->i_spb</td>
				  <td>$row->i_dkb</td>
				  <td>$row->d_dkb</td>
				  <td>$row->d_sj_receive</td>";
			  /* echo "
				  <td class=\"action\">";
  					echo "<a href=\"#\" onclick='show(\"sj/cform/edit/$row->i_sj/$row->i_area/$dfrom/$dto/$row->i_spb/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					    if($row->f_nota_cancel == 'f' && trim($row->i_nota)=='' && trim($row->i_dkb)==''){
					echo "&nbsp;&nbsp;
<a href=\"#\" onclick='hapus(\"listsj/cform/delete/$row->i_sj/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
				   	    } */
				echo "
					<td class=\"action\">";
						echo "<a href=\"#\" onclick='show(\"sj/cform/edit/$row->i_sj/$row->i_area/$dfrom/$dto/$row->i_spb/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
							if($row->f_nota_cancel == 'f' && trim($row->i_nota)=='' && trim($row->i_dkb)=='' 
							&& (($this->session->userdata('departement')=='7' && $this->session->userdata('i_area')=='00' ) || 
								($this->session->userdata('departement')=='3' && $this->session->userdata('i_area')!='00' ) ) ){
						echo "&nbsp;&nbsp;
					<a href=\"#\" onclick='hapus(\"listsj/cform/delete/$row->i_sj/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
									}
					echo "</td></tr>";	
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      </div>	  
      <?=form_close()?>
    </td>
  </tr>
</table>
<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick='show("listsj/cform/","#tmpx")'>
</div>
