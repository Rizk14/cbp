<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
    <?php echo form_open('cekdt/cform/update', array('id' => 'daftartagihanformupdate', 'name' => 'daftartagihanformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width='100px'>No</td>
		<td><input readonly id="idt" name="idt" value="<?php echo $isi->i_dt;?>"></td>
		  </tr>
	      <tr>
		<td width='100px'>Tanggal</td>
		<?php 
			$tmp=explode("-",$isi->d_dt);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$isi->d_dt=$hr."-".$bl."-".$th;
		?>
        <input hidden id="bdt" name="bdt" value="<?php echo $bl; ?>">
		<td><input readonly id="ddt" name="ddt" onclick="showCalendar('',this,this,'','ddt',0,20,1)" value="<?php echo $isi->d_dt;?>">
		    <input hidden id="tgldt" name="tgldt" value="<?php echo $isi->d_dt;?>">
        <input type="hidden" id="xddt" name="xddt" value="<?php echo $isi->d_dt;?>"></td>
	      </tr>
		  <tr>
		<td width='100px'>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name;?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area;?>"></td>
	      </tr>
		  <tr>
		<td width='100px'>Jumlah</td>
		<td><input style="text-align:right;" readonly id="vjumlah" name="vjumlah" value="<?php echo number_format($isi->v_jumlah);?>"></td>
	      </tr>
		  <tr>
		<td width='100px'>Keterangan</td>
		<td><input name="ecek1" id="ecek1" value="<?php echo $isi->e_cek; ?>" type="text"></td>
	      </tr>
		<tr>
		  <td width='100px'>&nbsp;</td>
		  <td>
		    <input name="login" id="login" value="Cek" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("cekdt/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main");'></td>
		</tr>
	    </table>
		<div id="detailheader" align="center">
			<table id="itemtem" class="listtable" style="width:850px;">
			<th style="width:23px;" align="center">No</th>
			<th style="width:116px;" align="center">Nota</th>
			<th style="width:76px;" align="center">Tgl Nota</th>
			<th style="width:76px;" align="center">Tgl JT</th>
			<th style="width:244px;" align="center">Pelanggan</th>
			<th style="width:100px;" align="center">Jml Nota</th>
			<th style="width:100px;" align="center">Jml DT</th>
			<th style="width:100px;" align="center">Sisa</th>
			<th style="width:38px;" align="center">Act</th>
			</table>
		</div>
		<div id="detailisi" align="center">
			<?php 				
				$i=0;
        if(isset($detail)){
				  foreach($detail as $row)
				  {
					  $i++;
					  $tmp=explode("-",$row->d_nota);
					  $th=$tmp[0];
					  $bl=$tmp[1];
					  $hr=$tmp[2];
					  $row->d_nota=$hr."-".$bl."-".$th;
					  $tmp=explode("-",$row->d_jatuh_tempo);
					  $th=$tmp[0];
					  $bl=$tmp[1];
					  $hr=$tmp[2];
					  $row->d_jatuh_tempo=$hr."-".$bl."-".$th;
					  $not=trim($row->i_nota);
					  $jum=$isi->v_jumlah-$row->v_jumlah;
					  $njum=$row->v_jumlah;
					  $nsisa=$row->v_sisa;
					  $row->v_jumlah=number_format($row->v_jumlah);
					  $row->v_sisa=number_format($row->v_sisa);
            		$row->sisanota=number_format($row->sisanota);
					  	echo '<table class="listtable" align="center" style="width:850px;">';
				    	echo "<tbody><tr>
											  <td style=\"width:20px;\">
											  <input style=\"width:20px;font-size:12px;\" type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
											  <td style=\"width:106px;\">
											  <input style=\"width:106px; font-size:11px;\" readonly type=\"text\" id=\"inota$i\" name=\"inota$i\" value=\"$row->i_nota\"></td>
											  <td style=\"width:69px;\">
											  <input style=\"width:69px; font-size:11px;\" readonly type=\"text\" id=\"dnota$i\" name=\"dnota$i\" value=\"$row->d_nota\"></td>
											  <td style=\"width:69px;\">
											  <input readonly style=\"width:69px; font-size:11px;\"  type=\"text\" id=\"djatuhtempo$i\" name=\"djatuhtempo$i\" value=\"$row->d_jatuh_tempo\"></td>
											  <td style=\"width:224px;\">
											  <input readonly style=\"width:224px;font-size:12px;\"  type=\"text\" id=\"ecustomername$i\" name=\"ecustomername$i\" value=\"($row->i_customer) $row->e_customer_name ($row->e_customer_city)\">
											  <input type=\"hidden\" id=\"icustomer$i\" name=\"icustomer$i\" value=\"$row->i_customer\"></td>
											  <td style=\"width:90px;\">
											  <input readonly style=\"text-align:right; width:90px;font-size:12px;\" type=\"text\" id=\"vjumlah$i\" name=\"vjumlah$i\" value=\"$row->v_jumlah\"></td>

											  <td style=\"width:90px;\">
                        <input readonly style=\"text-align:right; width:90px;font-size:12px;\" type=\"text\" id=\"vsisadt$i\" name=\"vsisadt$i\" value=\"$row->v_sisa\"></td>

											  <td style=\"width:90px;\">
											  <input readonly style=\"text-align:right; width:90px;font-size:12px;\" type=\"text\" id=\"vsisa$i\" name=\"vsisa$i\" value=\"$row->sisanota\"></td>
											  <td style=\"width:36px;\" align=\"center\">";
            echo "</td></tr></tbody></table>";
				  }
        }
				?>
			</div>
		<div id="pesan"></div>
		<input type="hidden" name="jml" id="jml" value="<?php echo $i; ?>">
	  </div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function view_area(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/cekdt/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("idt").value!='') &&
  	 	(document.getElementById("ddt").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
		(document.getElementById("vjumlah").value!='0')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if(
					(document.getElementById("inota"+i).value=='') ||
					(document.getElementById("icustomer"+i).value=='')
				  )
				{
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
  	  		document.getElementById("cmdtambahitem").disabled=true;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function hitungnilai(isi,jml){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductmill"+i).value);
			nqty=formatulang(document.getElementById("nreceive"+i).value);
			vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		}
		document.getElementById("vapgross").value=formatcemua(vtot);
	}
  }
  function afterSetDateValue(ref_field, target_field, date) {
    dspb=document.getElementById('ddt').value;
    bspb=document.getElementById('bdt').value;
    dtmp=dspb.split('-');
    per=dtmp[2]+dtmp[1]+dtmp[0];
    bln = dtmp[1];
    if( (bspb!='') && (dspb!='') ){
      if(bspb != bln)
      {
        alert("Tanggal DT tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("ddt").value=document.getElementById("tgldt").value;
      }
    }
  }
</script>
