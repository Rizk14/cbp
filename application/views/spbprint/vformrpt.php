<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
	$isi=$master;
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab	= str_repeat(" ",9);
		$ipp = new PrintIPP();
//		$ipp->setHost("localhost");
//		$ipp->setPrinterURI("/printers/Epson-lq-1170-24-pin");
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$ipp->setData(CHR(18)."\n");
		$ipp->printJob();
		$ipp->setData($nor.NmPerusahaan."                  \n\n");
		$ipp->printJob();
		$ipp->setData($nor." SURAT PEMESANAN BARANG                   No.SPB    :".$row->i_spb."\n");
		$ipp->printJob();
		$tmp=explode("-",$row->d_spb);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dspb=$hr."-".$bl."-".$th;
		$ipp->setData($nor."       ( SPB )                            Tgl.Pesan :".$dspb."\n");
		$ipp->printJob();
		$pjg=strlen($nor."Data Pelanggan : ".$row->e_customer_classname);
		$jrk=47-$pjg;
		$ipp->setData($nor."Data Pelanggan : ".$row->e_customer_classname.str_repeat(" ",$jrk)."Kode Sales: ".$row->i_salesman." - ".$row->e_salesman_name."\n");
		$ipp->printJob();
		$pjg=strlen("( ".$row->i_customer." )".$row->e_customer_name)+$nor;
		$jrk=34-$pjg;
		$ipp->setData($nor."( ".$row->i_customer." )".$row->e_customer_name.str_repeat(" ",$pjg)."Top : ".$row->n_spb_toplength." hari"." KdHarga : ".$row->i_price_group."\n");
		$ipp->printJob();
		$ipp->setData($nor.$row->e_customer_address."\n");
		$ipp->printJob();
		$ipp->setData($nor.$row->e_customer_city."\n");
		$ipp->printJob();
		if($row->f_customer_pkp=='t'){
			$ipp->setData($nor."N.P.W.P : ".$row->e_customer_pkpnpwp.CHR(15)."\n\n");
		}else{
			$ipp->setData($nor.''.CHR(15)."\n\n");
		}
		$ipp->printJob();
		$ipp->setData($ab.str_repeat("=",115)."\n");
		$ipp->printJob();
		$ipp->setData($ab."No.  Kode     Nama Barang                                Banyak yg   Harga   Banyak yg     Motif\n");
		$ipp->printJob();
   		$ipp->setData($ab."Urut Barang                                               dipesan    Satuan  dipenuhi\n");
		$ipp->printJob();
		$ipp->setData($ab.str_repeat("-",115)."\n");
		$ipp->printJob();
		$i=0;	
		$detail	= $this->mmaster->bacadetail($area,$row->i_spb);
		foreach($detail as $rowi){
			$i++;
			$hrg=number_format($rowi->v_unit_price);
			$prod	= $rowi->i_product;
			$name	= $rowi->e_product_name.str_repeat(" ",46-strlen($rowi->e_product_name ));
			$motif	= $rowi->e_product_motifname;
			$orde	= number_format($rowi->n_order);
			$deli	= number_format($rowi->n_deliver);
			$aw		= 13;
			$pjg	= strlen($i);
			for($xx=1;$xx<=$pjg;$xx++){
				$aw=$aw-1;
			}
			$pjg	= strlen($orde);
			$spcord	= 4;			
			for($xx=1;$xx<=$pjg;$xx++){
				$spcord	= $spcord-1;
			}
			$pjg	= strlen($hrg);
			$spcprc	= 13;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcprc	= $spcprc-1;
			}
			$ipp->setData(str_repeat(" ",$aw).$i.str_repeat(" ",1).$prod.str_repeat(" ",2).$name.str_repeat(" ",$spcord).$orde.str_repeat(" ",$spcprc).$hrg.CHR(27).CHR(45).CHR(1).str_repeat(" ",10).CHR(27).CHR(45).CHR(0).str_repeat(" ",4).$motif."\n");
			$ipp->printJob();
		}
		$ipp->setData($ab.str_repeat("-",115)."\n");
		$ipp->printJob();
		$kotor=$row->v_spb+$row->v_spb_discounttotal;
		$kotor=number_format($kotor);
		$pjg=strlen($kotor);
		$spckot=14;
		for($xx=1;$xx<=$pjg;$xx++){
			$spckot=$spckot-1;
		}
		$spckot=str_repeat(" ",$spckot);
		$ipp->setData(str_repeat(" ",86)."NILAI KOTOR           : ".$spckot.$kotor."\n");
		$ipp->printJob();
		$dis=$row->n_spb_discount1+$row->n_spb_discount2+$row->n_spb_discount3+$row->n_spb_discount4;
		$dis	= number_format($dis,2);
		$pjg	= strlen($dis);
		$spcdis	= 6;
		for($xx=1;$xx<=$pjg;$xx++){
			$spcdis	= $spcdis-1;
		}
		$spcdis=str_repeat(" ",$spcdis);
		$vdis	= number_format($row->v_spb_discounttotal);
		$pjg	= strlen($vdis);
		$spcvdis	= 14;
		for($xx=1;$xx<=$pjg;$xx++){
			$spcvdis = $spcvdis-1;
		}
		$spcvdis=str_repeat(" ",$spcvdis);
		$ipp->setData(str_repeat(" ",86)."POTONGAN ( ".$dis." % )".$spcdis." : ".$spcvdis.$vdis."\n");
		$ipp->printJob();
		$ipp->setData(str_repeat(" ",112).str_repeat("-",12)."\n");
		$ipp->printJob();
		$nb	= number_format($row->v_spb);
		$pjg=strlen($nb);
		$spcnb=14;
		for($xx=1;$xx<=$pjg;$xx++){
			$spcnb=$spcnb-1;
		}
		$spcnb=str_repeat(" ",$spcnb);
		$ipp->setData(str_repeat(" ",86)."NILAI BERSIH          : ".$spcnb.$nb."\n\n");
		$ipp->printJob();
		$ipp->setData($ab."Plafon                            : Rp "."\n");
		$ipp->printJob();
		$ipp->setData($ab."Rata-rata Keterlambatan Pelunasan :       hari\n");
		$ipp->printJob();
		$ipp->setData($ab."Saldo Piutang                     : \n");
		$ipp->printJob();
		$ipp->setData($ab."Penjualan                         : \n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(218).str_repeat(CHR(196),10).CHR(194).str_repeat(CHR(196),27).CHR(194).str_repeat(CHR(196),26).CHR(194).str_repeat(CHR(196),8).CHR(194).str_repeat(CHR(196),20).CHR(194).str_repeat(CHR(196),23).CHR(191)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179)."Tgl & Jam ".CHR(179)."       G U D A N G         ".CHR(179)."  Serah terima GUDANG     ".CHR(179).str_repeat(" ",8).CHR(179)."Tgl&Jam Terima Nota ".CHR(179)."      CEK PLAFON       ".CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179)."Terima SPB".CHR(195).str_repeat(CHR(196),8).CHR(194).str_repeat(CHR(196),8).CHR(194).str_repeat(CHR(196),9).CHR(197).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),9).CHR(194).str_repeat(CHR(196),8).CHR(180)."   MD   ".CHR(195).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),6).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),7).CHR(180)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179).str_repeat(" ",10).CHR(179)." CEK I  ".CHR(179)." CEK II ".CHR(179)."CEK AKHIR".CHR(179)."   I   ".CHR(179)."   II    ".CHR(179)."  III   ".CHR(179).str_repeat(" ",8).CHR(179)."   I  ".CHR(179)."  II  ".CHR(179)."  III ".CHR(179)."   AR  ".CHR(179)."  FADH ".CHR(179)."  SDH  ".CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(195).str_repeat(CHR(196),10).CHR(197).str_repeat(CHR(196),8).CHR(197).str_repeat(CHR(196),8).CHR(197).str_repeat(CHR(196),9).CHR(197).str_repeat(CHR(196),7).CHR(197).str_repeat(CHR(196),9).CHR(197).str_repeat(CHR(196),8).CHR(197).str_repeat(CHR(196),8).CHR(197).str_repeat(CHR(196),6).CHR(197).str_repeat(CHR(196),6).CHR(197).str_repeat(CHR(196),6).CHR(197).str_repeat(CHR(196),7).CHR(197).str_repeat(CHR(196),7).CHR(197).str_repeat(CHR(196),7).CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179).str_repeat(" ",10).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179).str_repeat(" ",10).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(179).str_repeat(" ",10).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",9).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",8).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",6).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179).str_repeat(" ",7).CHR(179)."\n");
		$ipp->printJob();
		$ipp->setData($ab.CHR(192).str_repeat(CHR(196),10).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),9).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),9).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),8).CHR(193).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),6).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),7).CHR(217)."\n");
		$ipp->printJob();
		$ipp->setFormFeed();
		$ipp->setData(CHR(18)."\n");
		$ipp->printJob();
		$ipp->setBinary();
	}
	echo "<script>this.close();</script>";
?>
