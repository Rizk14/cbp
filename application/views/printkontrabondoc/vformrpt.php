<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
*{
	size: potrait;
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusi {
  font-size: 14px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
	$tmp=explode("-",$dfrom);
	$th=$tmp[2];
	$bl=$tmp[1];
	$hr=$tmp[0];
	$dfroms=$hr." ".mbulan($bl)." ".$th;
	$tmp=explode("-",$dto);
	$th=$tmp[2];
	$bl=$tmp[1];
	$hr=$tmp[0];
	$dtos=$hr." ".mbulan($bl)." ".$th;

	$tmp 		= explode("-", $dto);
	$tahun	= $tmp[2];
	$bulan	= $tmp[1];
	$tanggal= $tmp[0];
	$dsaw2	= $tahun."/".$bulan."/".$tanggal;
	$dtoq		=$this->mmaster->dateAdd("d",-1,$dsaw2);
	$tmp 		= explode("-", $dtoq);
	$th			= $tmp[0];
	$bl			= $tmp[1];
	$dt 		= $tmp[2];
	$dsaw2	= $dt."-".$bl."-".$th;

  $coa='';
	if($isi){
    $jml=0;
    $baris=0;
    $siap='';
		foreach($isi as $raw){
			$jml++;
      if($siap!=$raw->i_dtap){
        $baris++;            
      }
      $siap=$raw->i_dtap;
			if($jml<2){
?>
        <table width="1160" class="garisx">
        <tr>
          <td width=15%></td>
          <td><div align="left"><strong>DAFTAR KONTRA BON</strong></div></td>
          <td width=15%></td>
        </tr>
        <tr>
          <td width=15%></td>
          <td><div align="left"><strong><?php echo NmPerusahaan; ?></strong></div></td>
          <td width=15%></td>
        </tr>
        <tr>
          <td width=15%></td>
          <td><strong><?php echo $isupplier."  ".$raw->e_supplier_name; ?></strong></td>
          <td width=15%></td>
        </tr>
        <tr>
          <td width=15%></td>
          <td><div><strong>Dari Faktur : <?php echo $notafrom; ?>&nbsp;s/d&nbsp;<?php echo $notato; ?></strong></div></td>
          <td width=15%></td>
        </tr>
        </table>
        <table width="1160" border="0" class="garisx huruf">
          <tr>
            <td colspan=2></td>
            <td><div align="right">Hal : <?php echo $jml; ?></div></td>
          </tr>
        </table>
<?php 
			}
		}
	}
?>
<table width="1160px" cellspacing=0 cellpadding=0 class="garis huruf">
<tr>
	<td rowspan="2" align="center" class="huruf isi">NO</td>
	<td rowspan="2" align="center" class="huruf isi">NO. FAKTUR</td>
	<td colspan="2" align="center" class="huruf isi">TANGGAL</td>
	<td rowspan="2" align="center" class="huruf isi">KOTA</td>
	<td rowspan="2" align="center" class="huruf isi">NOMOR DO</td>
	<td rowspan="2" align="center" class="huruf isi">JUMLAH</td>
</tr>
<tr>
	<td align="center" class="huruf isi">FAKTUR</td>
	<td align="center" class="huruf isi">JT. TEMPO</td>
</tr>
<tbody>
<?php 
  $i=0;
  $x=1;
  if($isi){
		$siap='';
    $sub=0;
    $total=0;
  	foreach($isi as $row){
			if($siap!=$row->i_dtap){
        $i++;
				$tmp=explode('-',$row->d_pajak);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_pajak=$tgl.'-'.ucfirst(substr(mbulan($bln),0,3)).'-'.$thn;
				if($row->d_due_date!=''){
					$tmp=explode('-',$row->d_due_date);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$row->d_due_date=$tgl.'-'.ucfirst(substr(mbulan($bln),0,3)).'-'.$thn;
				}
				echo "<tr>
								<td>$i</td>
								<td>".$row->i_dtap."</td>
								<td>".$row->d_pajak."</td>
								<td>".$row->d_due_date."</td>
								<td>".$row->e_area_shortname."</td>
								<td>".$row->i_do."</td>
								<td align=right>".number_format($row->v_netto)."</td>
							</tr>";
				$siap=$row->i_dtap;
        $sub=$sub+$row->v_netto;
        $total=$total+$row->v_netto;
        if( ($i%40==0) && ($i!=$baris) ){
				  echo "<tr>
								  <td colspan=6>JUMLAH SUB TOTAL</td>
								  <td align=right>".number_format($sub)."</td>
							  </tr>";
          echo "</tbody></table>";
          $x++;
          $sub=0;

        ?>
          <table width="1160px" class="garisx">
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr>
              <td width=100px>&nbsp;</td>
              <td width=28px>&nbsp;</td>
              <td colspan=5>Penerima</td>
            </tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td width=100px>&nbsp;</td><td colspan=6>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></tr>
          </table>
          <table width="1160" border="0" class="garisx huruf">
            <br class="pagebreak">
              <tr>
                <td colspan=2></td>
                <td><div align="right">Hal : <?php echo $x; ?></div></td>
              </tr>
          </table>
       	  <table width="1160px" cellspacing=0 cellpadding=0 class="garis huruf">
              <tr>
	              <td rowspan="2" align="center" class="huruf isi">NO</td>
	              <td rowspan="2" align="center" class="huruf isi">NO. FAKTUR</td>
	              <td colspan="2" align="center" class="huruf isi">TANGGAL</td>
	              <td rowspan="2" align="center" class="huruf isi">KOTA</td>
	              <td rowspan="2" align="center" class="huruf isi">NOMOR DO</td>
	              <td rowspan="2" align="center" class="huruf isi">JUMLAH</td>
              </tr>
              <tr>
	              <td align="center" class="huruf isi">FAKTUR</td>
	              <td align="center" class="huruf isi">JT. TEMPO</td>
              </tr>
            <tbody>
        <?php 
        }
      }
    }
	  echo "<tr>
					  <td colspan=6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;JUMLAH SUB TOTAL</td>
					  <td align=right>".number_format($sub)."</td>
				  </tr>";
	  echo "<tr>
					  <td colspan=6>&nbsp;&nbsp;&nbsp;J U M L A H    T O T A L</td>
					  <td align=right>".number_format($total)."</td>
				  </tr>";
    echo "</tbody></table>";
	}
?>
	    </tbody>
	  </table>
    <table width="1160px" class="garisx">
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>

            <tr>
              <td width=100px>&nbsp;</td>
              <td width=28px>&nbsp;</td>
              <td colspan=5>Penerima</td>
            </tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td colspan=7>&nbsp;</td></tr>
            <tr><td width=100px>&nbsp;</td><td colspan=6>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td></tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
