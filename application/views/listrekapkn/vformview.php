<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>
<div id='tmp'>
  <table class="maintable">
    <tr>
      <td align="left">
        <h2><?php echo $page_title; ?></h2>
        <?php
        include("php/fungsi.php");

        ?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : ' . substr($dfrom, 0, 2) . ' ' . mbulan(substr($dfrom, 3, 2)) . ' ' . substr($dfrom, 6, 4) . ' s/d ' . substr($dto, 0, 2) . ' ' . mbulan(substr($dto, 3, 2)) . ' ' . substr($dto, 6, 4); ?></h3>

        <?php echo $this->pquery->form_remote_tag(array('url' => 'listrekapkn/cform/view', 'update' => '#main', 'type' => 'post')); ?>
        <div class="effect">
          <div class="accordion2">
            <table class="listtable" id="sitabel">
              <?php
              if ($isi) {
              ?>
                <tr>
                  <th>Area</th>
                  <th>Lang</th>
                  <th>No KNDN</th>
                  <th>Batal</th>
                  <th>Toko</th>
                  <th>KS</th>
                  <th>NPWP</th>
                  <th>Tgl Dok</th>
                  <th>Kotor</th>
                  <th>Potongan</th>
                  <th>Jumlah</th>
                  <th>DPP</th>
                  <th>PPN</th>
                  <th>Ket</th>
                  <th>Ket PKP</th>
                  <th>Cat</th>
                </tr>
                <tbody>
                <?php
                foreach ($isi as $row) {
                  if ($row->d_pajak != "") {
                    $row->d_pajak = date('Y-m-d', strtotime($row->d_pajak));

                    $ndpp = get_tax($row->d_pajak)->excl_divider;
                    $nppn = get_tax($row->d_pajak)->n_tax_val;
                  } else {
                    $row->d_dn = date('Y-m-d', strtotime($row->d_kn));

                    $ndpp = get_tax($row->d_dn)->excl_divider;
                    $nppn = get_tax($row->d_dn)->n_tax_val;
                  }

                  $tmp = explode('-', $row->d_kn);
                  switch ($tmp[1]) {
                    case '01':
                      $romawi = 'I';
                      break;
                    case '02':
                      $romawi = 'II';
                      break;
                    case '03':
                      $romawi = 'III';
                      break;
                    case '04':
                      $romawi = 'IV';
                      break;
                    case '05':
                      $romawi = 'V';
                      break;
                    case '06':
                      $romawi = 'VI';
                      break;
                    case '07':
                      $romawi = 'VII';
                      break;
                    case '08':
                      $romawi = 'VIII';
                      break;
                    case '09':
                      $romawi = 'IX';
                      break;
                    case '10':
                      $romawi = 'X';
                      break;
                    case '11':
                      $romawi = 'XI';
                      break;
                    case '12':
                      $romawi = 'XII';
                      break;
                  }
                  if (substr($row->i_kn, 0, 1) == 'K') {
                    $kode = 'KN';
                  } else {
                    $kode = 'DN';
                  }
                  if (substr($row->i_kn, 0, 2) != 'KP') {
                    $no = substr($row->i_kn, 1, 5) . '/' . $kode . '/' . $row->e_area_shortname . '/' . $romawi . '/' . $tmp[2];
                  } else {
                    $no = substr($row->i_kn, 2, 4) . '/' . $kode . '/' . $row->e_area_shortname . '/' . $romawi . '/' . $tmp[2];
                  }
                  if ($row->e_customer_pkpnpwp != '') {
                    $pkp = 'PKP';
                  } else {
                    $pkp = 'Non PKP';
                  }

                  $dpp = $row->v_netto / $ndpp;
                  $ppn = $nppn * $dpp;

                  if ($row->f_kn_cancel == 't') {
                    $batal = 'YA';
                    $row->v_gross = 0;
                    $row->v_discount = 0;
                    $row->v_netto = 0;
                    $dpp = 0;
                    $ppn = 0;
                  } else {
                    $batal = 'TIDAK';
                  }
                  echo '<tr><td>' . $row->e_area_shortname . '</td>';
                  echo '<td>' . $row->i_customer . '</td>';
                  // echo "<td>'$row->i_customer</td>";
                  echo '<td>' . $no . '</td>';
                  echo '<td>' . $batal . '</td>';
                  echo '<td>' . $row->e_customer_name . '</td>';
                  echo '<td>' . $row->i_salesman . '</td>';
                  echo '<td>' . $row->e_customer_pkpnpwp . '</td>';
                  echo '<td>' . $row->d_kn . '</td>';
                  echo '<td align=right>' . number_format($row->v_gross) . '</td>';
                  echo '<td align=right>' . number_format($row->v_discount) . '</td>';
                  echo '<td align=right>' . number_format($row->v_netto) . '</td>';
                  echo '<td align=right>' . number_format($dpp) . '</td>';
                  echo '<td align=right>' . number_format($ppn) . '</td>';
                  echo '<td>' . $kode . '</td>';
                  echo '<td>' . $pkp . '</td>';
                  echo '<td>' . $row->i_kn . '</td><tr>';
                }
              }
                ?>
                </tbody>
            </table>
            <!-----------------------------------------------------------------------------TABEL EXPORT---------------------------------------------------------------------------------------->
            <table class="listtable" id="sitabel2" hidden="true">
              <?php
              if ($isi) {
              ?>
                <tr>
                  <th>Areaa</th>
                  <th>Lang</th>
                  <th>No KNDN</th>
                  <th>Batal</th>
                  <th>Toko</th>
                  <th>KS</th>
                  <th>NPWP</th>
                  <th>Tgl Dok</th>
                  <th>Kotor</th>
                  <th>Potongan</th>
                  <th>Jumlah</th>
                  <th>DPP</th>
                  <th>PPN</th>
                  <th>Ket</th>
                  <th>Ket PKP</th>
                  <th>Cat</th>
                </tr>
                <tbody>
                <?php
                foreach ($isi as $row) {
                  $tmp = explode('-', $row->d_kn);
                  switch ($tmp[1]) {
                    case '01':
                      $romawi = 'I';
                      break;
                    case '02':
                      $romawi = 'II';
                      break;
                    case '03':
                      $romawi = 'III';
                      break;
                    case '04':
                      $romawi = 'IV';
                      break;
                    case '05':
                      $romawi = 'V';
                      break;
                    case '06':
                      $romawi = 'VI';
                      break;
                    case '07':
                      $romawi = 'VII';
                      break;
                    case '08':
                      $romawi = 'VIII';
                      break;
                    case '09':
                      $romawi = 'IX';
                      break;
                    case '10':
                      $romawi = 'X';
                      break;
                    case '11':
                      $romawi = 'XI';
                      break;
                    case '12':
                      $romawi = 'XII';
                      break;
                  }
                  if (substr($row->i_kn, 0, 1) == 'K') {
                    $kode = 'KN';
                  } else {
                    $kode = 'DN';
                  }
                  if (substr($row->i_kn, 0, 2) != 'KP') {
                    $no = substr($row->i_kn, 1, 5) . '/' . $kode . '/' . $row->e_area_shortname . '/' . $romawi . '/' . $tmp[2];
                  } else {
                    $no = substr($row->i_kn, 2, 4) . '/' . $kode . '/' . $row->e_area_shortname . '/' . $romawi . '/' . $tmp[2];
                  }
                  if ($row->e_customer_pkpnpwp != '') {
                    $pkp = 'PKP';
                  } else {
                    $pkp = 'Non PKP';
                  }
                  $dpp = round($row->v_netto / $ndpp);
                  $ppn = round($dpp * $nppn);
                  if ($row->f_kn_cancel == 't') {
                    $batal = 'YA';
                    $row->v_gross = 0;
                    $row->v_discount = 0;
                    $row->v_netto = 0;
                    $dpp = 0;
                    $ppn = 0;
                  } else {
                    $batal = 'TIDAK';
                  }
                  echo '<tr><td>' . $row->e_area_shortname . '</td>';
                  //echo '<td>'.$row->i_customer.'</td>';
                  echo "<td>'$row->i_customer</td>";
                  echo '<td>' . $no . '</td>';
                  echo '<td>' . $batal . '</td>';
                  echo '<td>' . $row->e_customer_name . '</td>';
                  echo '<td>' . $row->i_salesman . '</td>';
                  echo '<td>' . $row->e_customer_pkpnpwp . '</td>';
                  echo '<td>' . $row->d_kn . '</td>';
                  echo '<td align=right>' . number_format($row->v_gross) . '</td>';
                  echo '<td align=right>' . number_format($row->v_discount) . '</td>';
                  echo '<td align=right>' . number_format($row->v_netto) . '</td>';
                  echo '<td align=right>' . number_format($dpp) . '</td>';
                  echo '<td align=right>' . number_format($ppn) . '</td>';
                  echo '<td>' . $kode . '</td>';
                  echo '<td>' . $pkp . '</td>';
                  echo '<td>' . $row->i_kn . '</td><tr>';
                }
              }
                ?>
                </tbody>
            </table>
            <!--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->












            <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
          </div>
        </div>
        <?= form_close() ?>
      </td>
    </tr>
  </table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x, a, g) {
    if (confirm(g) == 1) {
      document.getElementById("ispbdelete").value = a;
      document.getElementById("inotadelete").value = x;
      formna = document.getElementById("listform");
      formna.action = "<?php echo site_url(); ?>" + "/listrekapkn/cform/delete";
      formna.submit();
    }
  }

  function yyy(x, b) {
    document.getElementById("ispbedit").value = b;
    document.getElementById("inotaedit").value = x;
    formna = document.getElementById("listform");
    formna.action = "<?php echo site_url(); ?>" + "/nota/cform/edit";
    formna.submit();
  }
  $("#cmdreset").click(function() {
    var Contents = $('#sitabel2').html();
    //    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel2').html()) + '</table>');
    //    alert('exporting records...');
  });
</script>