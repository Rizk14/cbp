<div id='tmp'>
<h2><?php echo $page_title.' Periode : '.$iperiode; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'kartupiutang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
			  <tr>
		<th rowspan="2">No</th>
		<th rowspan="2">Area</th>
		<th rowspan="2">Sld Awal</th>
		<th colspan="3">Penjualan</th>
		<!--<th>Debit Nota</th>-->
		<th colspan="3">Retur Jual</th>
		<th rowspan="2">Penjualan Netto</th>
		<th rowspan="2">Pelunasan</th>
		<th rowspan="2">Sld Akhir</th>
		</tr>
		<tr>
			<th>DPP</th>
			<th>PPN</th>
			<th>Subtotal</th>
			<th>DPP</th>
			<th>PPN</th>
			<th>Subtotal</th>
		</tr>
	    <tbody>
	      <?php 
		if($isi){
      if($hal=='')$hal=0;
      $no=$hal;
      $akhir=0;
      $totsa=0;
      $totno=0;
      $totdn=0;
      $totpl=0;
      $totkn=0;
      $totdpp1=0;
      $totppn1=0;
      $totdpp2=0;
      $totppn2=0;
      $totpenjualannetto=0;
      $total=0;
		  foreach($isi as $row){
        $no++;
        $query=$this->db->query(" select e_area_name from tr_area where i_area='$row->i_area'");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $nama=$tmp->e_area_name;
          }
		    }
        $akhir=$row->saldo+($row->nota+$row->dn)-($row->pl+$row->kn);

        $totsa=$totsa+$row->saldo;
        $totno=$totno+$row->nota;
        $totdn=$totdn+$row->dn;
        $totpl=$totpl+$row->pl;
        $totkn=$totkn+$row->kn;
        $totdpp1=$totdpp1+$row->dpp1;
        $totppn1=$totppn1+$row->ppn1;
        $totdpp2=$totdpp2+$row->dpp2;
        $totppn2=$totppn2+$row->ppn2;
        $penjualan_netto = $row->nota-$row->kn;
        $totpenjualannetto+=$penjualan_netto;
        
        $total=$total+$akhir;
        $nama = str_replace("'","",$nama);
        $nama	= str_replace("(","tandakurungbuka",$nama);
        $nama	= str_replace("&","tandadan",$nama);
        $nama	= str_replace(")","tandakurungtutup",$nama);
        $nama = str_replace('.','tandatitik',$nama);
        $nama = str_replace(',','tandakoma',$nama);
        $nama = str_replace('/','tandagaring',$nama);
			  echo "<tr><td align=right>$no</td>
				  <td>".$row->i_area." - ".$nama."</td>
				  <td align=right>".number_format($row->saldo)."</td>
				  <td align=right>".number_format($row->dpp1)."</td>
				  <td align=right>".number_format($row->ppn1)."</td>
				  <td align=right>".number_format($row->nota)."</td>
				  <td align=right>".number_format($row->dpp2)."</td>
				  <td align=right>".number_format($row->ppn2)."</td>
				  <td align=right>".number_format($row->kn)."</td>
				  <td align=right>".number_format($penjualan_netto)."</td>
				  <td align=right>".number_format($row->pl)."</td>
				  
				  <td align=right>".number_format($akhir)."</td>
				</tr>";
				// <td align=right>".number_format($row->dn)."</td>
			}
		  echo "<tr><td align=right colspan=2>Total</td>
			  <td align=right>".number_format($totsa)."</td>
			  <td align=right>".number_format($totdpp1)."</td>
			  <td align=right>".number_format($totppn1)."</td>
			  <td align=right>".number_format($totno)."</td>
			  <td align=right>".number_format($totdpp2)."</td>
			  <td align=right>".number_format($totppn2)."</td>
			  <td align=right>".number_format($totkn)."</td>
			  <td align=right>".number_format($totpenjualannetto)."</td>
			  <td align=right>".number_format($totpl)."</td>
			  
			  <td align=right>".number_format($total)."</td>
			</tr>";
			// <td align=right>".number_format($totdn)."</td>
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>

<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
