<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printnotakelompok/cform/cetak','update'=>'#main','type'=>'post'));?>
	<div class="printnotakelompokform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Date From</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dfrom',
			              'id'          => 'dfrom',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Date To</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dto',
			              'id'          => 'dto',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Area</td>
		<td width="1%">:</td>
		<td width="80%">
			<input type="hidden" id="iarea" name="iarea" value="">
			<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("printnotakelompok/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    			</td>
        </tr>
	      <tr>
		<td width="19%">Nota From</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'notafrom',
			              'id'          => 'notafrom',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => 'view_notafrom();');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Nota To</td>
		<td width="1%">:</td>
		<td width="80%">
						<input type="text" id="notato" name="notato" value="" onclick='view_notato()'></td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="Cetak" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('printnotakelompok/cform/','#main')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">

  function view_notafrom(){
    dfrom=document.getElementById("dfrom").value;
    dto=document.getElementById("dto").value;
    area=document.getElementById("iarea").value;
    if( (dfrom!='') && (dto!='') && (area!='') ){
      isi="printnotakelompok/cform/notafrom/"+dfrom+"/"+dto+"/"+area+"/";
      showModal(isi,"#light");
    }else{
      alert('Isi dulu Periode dan Area');
    }
  }

  function view_notato(){
    dfrom=document.getElementById("dfrom").value;
    dto=document.getElementById("dto").value;
    area=document.getElementById("iarea").value;

    if( (dfrom!='') && (dto!='') && (area!='') ){
      isi="printnotakelompok/cform/notato/"+dfrom+"/"+dto+"/"+area+"/";
      showModal(isi,"#light");
    }else{
      alert('Isi dulu Periode dan Area');
    }

  }
</script>
