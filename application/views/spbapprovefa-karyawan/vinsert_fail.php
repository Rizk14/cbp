<?php 
$this->load->view('header');
?>
<h2>
<?php 
echo $page_title;
?></h2>
<p class="error">
<?php 
echo $this->lang->line('listspb_wrong_input');
?></p>
<?php 
$this->load->view('listspb/vform');
$this->load->view('footer');
?>
