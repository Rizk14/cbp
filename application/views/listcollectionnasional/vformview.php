<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>

	<?php echo $this->pquery->form_remote_tag(array('url'=>'listcollectionnasional/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
	  <th rowspan=2>Keterangan</th>
	  <th align=center colspan="<?php echo $interval; ?>">Bulan</th>
	  </tr><tr>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($blasal,'integer');
	  }
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th>Jan</th>';
        break;
      case '2' :
        echo '<th>Feb</th>';
        break;
      case '3' :
        echo '<th>Mar</th>';
        break;
      case '4' :
        echo '<th>Apr</th>';
        break;
      case '5' :
        echo '<th>Mei</th>';
        break;
      case '6' :
        echo '<th>Jun</th>';
        break;
      case '7' :
        echo '<th>Jul</th>';
        break;
      case '8' :
        echo '<th>Agu</th>';
        break;
      case '9' :
        echo '<th>Sep</th>';
        break;
      case '10' :
        echo '<th>Okt</th>';
        break;
      case '11' :
        echo '<th>Nov</th>';
        break;
      case '12' :
        echo '<th>Des</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    $bl=$blasal;
    echo '</tr>';
?>
    <tbody>
<?php 
    echo '<tr><td>Target</td>';
		foreach($isi as $row){
  		echo '<td align=right>'.number_format($row->total).'</td>';
		}
    echo '</tr><tr><td>Realisasi</td>';
 		foreach($isi as $row){
  		echo '<td align=right>'.number_format($row->realisasi).'</td>';
 		}
    echo '</tr><tr><td>Tidak tertagih</td>';
		foreach($isi as $row){
		  echo '<td align=right>'.number_format($row->total-$row->realisasi).'</td>';
		}
    echo '<tr><td>% tertagih</td>';
		foreach($isi as $row){
		  if($row->realisasi>0){
        $persen=($row->realisasi*100)/$row->total;
      }else{
        $persen=0;
      }
		  echo '<td align=right>'.number_format($persen,2).' %</td>';
		}
    echo '<tr><td>% Tidak tertagih</td>';
		foreach($isi as $row){
      $nontagih=$row->total-$row->realisasi;
      $persennontagih=($nontagih*100)/$row->total;
		  echo '<td align=right>'.number_format($persennontagih,2).' %</td>';	
		}
		?>
	    </tbody>
<?php 
  }
    ?>

	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listcollectionnasional/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
