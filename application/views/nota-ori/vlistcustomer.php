<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'nota/cform/customer/'.$iarea;
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Customer</th>
	    <th>Nama</th>
	    <th>Area</th>
	    <th>Sales</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $nama	= str_replace("'","\'",$row->e_customer_name);
        $almt	= str_replace("'","\'",$row->e_customer_address);
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount','$row->f_customer_first','$row->i_salesman','$almt')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount','$row->f_customer_first','$row->i_salesman','$almt')\">$row->e_customer_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount','$row->f_customer_first','$row->i_salesman','$almt')\">$row->e_area_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->i_price_group','$row->e_price_groupname','$row->e_customer_pkpnpwp','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3','$row->n_customer_toplength','$row->f_customer_plusppn','$row->f_customer_plusdiscount','$row->f_customer_first','$row->i_salesman','$almt')\">$row->e_salesman_name</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q)
  {
	  if( (parseFloat(i)==0) && (parseFloat(j)==0) && (parseFloat(i)==0) ){
		  document.getElementById("vspbdiscounttotal").readonly=false;
	  }else{
		  document.getElementById("vspbdiscounttotal").value='0';
		  document.getElementById("vspbdiscounttotal").readonly=true;
	  }

	  document.getElementById("icustomer").value=a;
	  document.getElementById("ecustomername").value=b;
	  document.getElementById("ipricegroup").value=c;
	  document.getElementById("epricegroupname").value=d;
	  if(e!=''){
		  document.getElementById("ecustomerpkpnpwp").value=e;
		  document.getElementById("fspbpkp").checked=true;
	  }else{
		  document.getElementById("ecustomerpkpnpwp").value='';
		  document.getElementById("fspbpkp").checked=false;
	  }
    document.getElementById("iarea").value=f;
    document.getElementById("isalesman").value=g;
    document.getElementById("esalesmanname").value=h+'-'+p;

    document.getElementById("ncustomerdiscount1").value=i;
    document.getElementById("ncustomerdiscount2").value=j;
    document.getElementById("ncustomerdiscount3").value=k;
    document.getElementById("nspbtoplength").value=l;
    document.getElementById("fspbplusppn").value=m;
   	document.getElementById("fspbplusdiscount").value=n;
	  document.getElementById("ecumstomeraddress").value=q;
    if(document.getElementById("vspbbersih").value!=''){
		  hitungnilai(0,0);
    }
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
