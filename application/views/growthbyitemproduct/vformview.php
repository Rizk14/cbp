<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'growthbyitemproduct/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo mbulan($bl).' '.$th; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totcustly=0;
            $totcustcy=0;
            $totcustlm=0;
            $totcustcm=0;
            $totnotaly=0;
            $totnotalm=0;
            $totnotacy=0;
            $totnotacm=0;
            foreach($isi as $ro){
              $totcustly=$totcustly+$ro->custly;
              $totcustlm=$totcustlm+$ro->custlm;
              $totcustcm=$totcustcm+$ro->custcm;
              $totcustcy=$totcustcy+$ro->custcy;
              $totnotaly=$totnotaly+$ro->notaly;
              $totnotalm=$totnotalm+$ro->notalm;
              $totnotacm=$totnotacm+$ro->notacm;
              $totnotacy=$totnotacy+$ro->notacy;
            }
          }
          if($total){
            $headcustly=0;
            $headcustlm=0;
            $headcustcy=0;
            $headcustcm=0;
            foreach($total as $r){
              $headcustly=$headcustly+$r->custly;
              $headcustlm=$headcustlm+$r->custlm;
              $headcustcy=$headcustcy+$r->custcy;
              $headcustcm=$headcustcm+$r->custcm;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan=3 align=center>No</th>
         <th rowspan=3 align=center>Kode Produk</th>
         <th colspan=2 align=center><?php echo $prevth; ?></th>
         <th colspan=2 align=center><?php echo $th; ?></th>
         <th rowspan=3 align=center>Growth</th>
         </tr>
         <tr>
         <th>Vol</th>
         <th>%</th>
         <th>Vol</th>
         <th>%</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totpersencustly=0;
        $totpersencustlm=0;
        $totpersencustcm=0;
        $totpersennotacy=0;
        $totpersencustcy=0;
        $totpersennotaly=0;
        foreach($isi as $row){
          $i++;
          $persennotaselm=0;
          $persencustselm=0;
          $persennotasely=0;
          $persencustsely=0;

          $persencustly=($row->custly/$totcustly)*100;
          $persencustlm=($row->custlm/$totcustlm)*100;
          $persencustcm=($row->custcm/$totcustcm)*100;
          $persencustcy=($row->custcy/$totcustcy)*100;
          $persennotaly=($row->notaly/$totnotaly)*100;
          $persennotacy=($row->notacy/$totnotacy)*100;
          $totpersencustly=$totpersencustly+$persencustly;
          $totpersencustlm=$totpersencustlm+$persencustlm;
          $totpersencustcm=$totpersencustcm+$persencustcm;
          $totpersencustcy=$totpersencustcy+$persencustcy;
          $totpersennotaly=$totpersennotaly+$persennotaly;
          $totpersennotacy=$totpersennotacy+$persennotacy;
          $notaselm=$row->notacm-$row->notalm;
          $custselm=$row->custcm-$row->custlm;
          $notasely=$row->notacy-$row->notaly;
          $custsely=$row->custcy-$row->custly;

          if($row->notalm==0){
            $persennotaselm=0;
          }else{
            $persennotaselm=($notaselm/$row->notalm)*100;
          }
          if($row->custlm==0){
            $persencustselm=0;
          }else{
            $persencustselm=($custselm/$row->custlm)*100;
          }
          if($row->notacy==0){
            $persennotasely=0;
          }else{
            $persennotasely=($notasely/$row->notacy)*100;
          }
          if($row->custly==0){
            $persencustsely=0;
          }else{
            $persencustsely=($custsely/$row->custly)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->i_product</td>
              <td style='font-size:12px;' align=right>".number_format($row->notaly)."</td>
              <td style='font-size:12px;' align=right>".number_format($persennotaly,2)."%</td>
              <td style='font-size:12px;' align=right>".number_format($row->notacy)."</td>
              <td style='font-size:12px;' align=right>".number_format($persennotacy,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persennotasely,2)."%</td>
              </tr>";
         }
        $persentotcustly=($totcustly/$totcustly)*100;
        echo "<tr>
        <td style='font-size:12px;' colspan=2><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotaly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersennotaly)."%</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotacy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersennotacy)."%</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersennotacy)."%</b></td>
        </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
