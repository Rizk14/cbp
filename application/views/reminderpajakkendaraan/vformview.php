 <?php 
	include ("php/fungsi.php");
?>
<!--<h2><?php echo $page_title; ?></h2>-->
<style>
.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
</style>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'reminderpajakkendaraan/cform/cari','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="9" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="iperiode" name="iperiode" value="<?php echo $m2; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>No. Polisi</th>
			<th>Pengguna</th>
			<th>Merek Kendaraan</th>
			<th>Jenis Kendaraan</th>
			<th>Asuransi</th>
			<th>No Asuransi</th>
			<th>TLO</th>
			<th>Area</th>
			<th>Tgl Pajak</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $per=mbulan(substr($row->i_periode,4,2))." ".substr($row->i_periode,0,4);
				if($row->d_pajak!=''){
					$tmp=explode('-',$row->d_pajak);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$row->d_pajak=$tgl.'-'.$bln.'-'.$thn;
				}else{
					$row->d_pajak='';
				}
			  echo "<tr> 
				  <td align=\"center\">$row->i_kendaraan</td>
				  <td align=\"center\">$row->e_pengguna</td>				  
				  <td align=\"center\">$row->e_merek_kendaraan</td>
				  <td align=\"center\">$row->e_jenis_kendaraan</td>
				  <td align=\"center\">$row->e_kendaraan_asuransi</td>
				  <td align=\"center\">$row->e_nomor_polisasuransi</td>
				  <td align=\"center\">$row->e_tlo</td>
				  <td align=\"center\">$row->e_area_name</td>
				  <td align=\"center\"><span class=\"blink\" style=\"color:red;\">$row->d_pajak</span></td>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
