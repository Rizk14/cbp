<div id="tmp">
<div id="tmpx">
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'oaobbyitemproduct_new/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
          <tr>
            <td style="text-align:center;" colspan="12">Sales Performa By Item Produk</td>
            </td>
            <td>
            </td>
         </tr>
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $tahun; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
 <table class='listtable'>       
<?php if($isi){
        $grwoa=0;
        $grwqty=0;
        $grwrp=0;
        $totnota=0;
        $totrp=0;
        $ctrrp=0;
        
        
        $totpersennetitem=0;
        
        $group='';

        foreach($isi as $row){
            $totnota+=$row->netitem;
        }
        $totrp=$totnota;         ?>

        
         <?php          
          foreach($isi as $row){  
                if($group==''){
              echo "<tr><td colspan=18 align=center style=\"font-size:16px;\">".strtoupper($row->e_product_groupname)."</td></tr>";
                  $prevth= $tahun-1;
              echo "
                     <tr>
                     <th rowspan='2'>Produk</th>
                     <th rowspan='2'>Nama Produk</th>
                     <th colspan='3'>OA</th>
                     <th colspan='3'>Sales Qty(Unit)</th>
                     <th colspan='3'>Net Sales(Rp.)</th>
                     <th rowspan='2'>%Ctr Net Sales(Rp.)</th>
                     </tr>
                     <tr>
                       
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth OA</th>
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth Qty</th>
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth Rp</th>
                     </tr>
                   <tbody>";
                   $totoaprev=0;
                   $totoa=0;
                   $totgrwoa=0;
                   $totjmlprev=0;
                   $totjml=0;
                   $totgrwqty=0;
                   $totnetitemprev=0;
                   $totnetitem=0;
                   $totgrwrp=0;
                   $totctrrp=0;
                }else{

                  if($group!=$row->e_product_groupname){
              echo "<tr>
                  <td style='font-size:12px;' colspan='2'><b>Total</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totgrwoa,2)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totjmlprev)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totjml)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totgrwqty,2)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totnetitemprev)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totnetitem)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totgrwrp,2)."</b></td>
                  <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
                  </tr>";
              echo "<tr><td colspan=12 >&nbsp;</td></tr>";
              echo "<tr><td colspan=12 align=center style=\"font-size:16px;\">".strtoupper($row->e_product_groupname)."</td></tr>";
                   $totoaprev=0;
                   $totoa=0;
                   $totgrwoa=0;
                   $totjmlprev=0;
                   $totjml=0;
                   $totgrwqty=0;
                   $totnetitemprev=0;
                   $totnetitem=0;
                   $totgrwrp=0;
                   $totctrrp=0;
                   $prevth= $tahun-1;
              echo "<table class='listtable'>
                     <tr>
                     <th rowspan='2'>Produk</th>
                     <th rowspan='2'>Nama Produk</th>
                     <th colspan='3'>OA</th>
                     <th colspan='3'>Sales Qty(Unit)</th>
                     <th colspan='3'>Net Sales(Rp.)</th>
                     <th rowspan='2'>%Ctr Net Sales(Rp.)</th>
                     </tr>
                     <tr>
                       
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth OA</th>
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth Qty</th>
                        <th>$prevth</th>
                        <th>$tahun</th>
                        <th>Growth Rp</th>
                     </tr>
                   <tbody>";
                  

            }
          }
          $group=$row->e_product_groupname;

          if($totnetitem==0){
            $persennetitem=0;
          }else{
            $persennetitem=($row->netitem/$totnetitem)*100;
          }
          $totpersennetitem=$totpersennetitem+$persennetitem;

          if ($row->oaprev == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->oaprev)/$row->oaprev);
          }

          if ($row->jmlprev == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->jml-$row->jmlprev)/$row->jmlprev);
          }

          if ($row->netitemprev == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->netitem-$row->netitemprev)/$row->netitemprev);
          }

          $ctrrp= $row->netitem/$totrp;
          $totoaprev=$totoaprev+$row->oaprev;
          $totoa=$totoa+$row->oa;
          $totjmlprev=$totjmlprev+$row->jmlprev;
          $totnetitemprev=$totnetitemprev+$row->netitemprev;
          $totctrrp=$totctrrp+$ctrrp;
          $totnetitem=$totnetitem+$row->netitem;
          $totjml=$totjml+$row->jml;


          echo "<tr>
              <td style='font-size:12px;'>$row->i_product</td>
              <td style='font-size:12px;'>$row->e_product_name</td>
              <td style='font-size:12px;' align=right>".number_format($row->oaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwoa,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->jmlprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->jml)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwqty,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->netitemprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->netitem)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwrp,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
         
       }

         if ($totoaprev == 0) {
              $totgrwoa = 0;
          } else { //jika pembagi tidak 0
              $totgrwoa = (($totoa-$totoaprev)/$totoaprev);
          }

          if ($totjmlprev == 0) {
              $totgrwqty = 0;
          } else { //jika pembagi tidak 0
              $totgrwqty = (($totjml-$totjmlprev)/$totjmlprev);
          }

          if ($totnetitemprev == 0) {
              $totgrwrp = 0;
          } else { //jika pembagi tidak 0
              $totgrwrp = (($totnetitem-$totnetitemprev)/$totnetitemprev);
          }

        echo "<tr>
        <td style='font-size:12px;' colspan='2'><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwoa,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totjmlprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totjml)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwqty,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnetitemprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnetitem)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwrp,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>";
      }
         ?>
       </tbody>
     </table>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
