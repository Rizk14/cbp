<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'oaobbyitemproduct_new/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
           <tr align="center">
              <td style="width:100px;" colspan="13"><b>Sales Performa By Item Produk</b></td>
           </tr>
           <tr>
            <td style="width:100px;" colspan="13"><b>
              <?php 
                    echo "Dari Tanggal   : ".$dfrom."<pre>"; 
                    echo "Sampai Tanggal : ".$dto;
              ?>
            </b></td>
         </tr>
        </table>
        <table class="listtable">
         <tr>
          <td>
            <?php 
              if($ob){
                foreach ($ob as $riw) {
                  echo "<h1>Total OB : ".number_format($riw->ob)."</h1>";
                }
              }
            ?>
          </td>
         </tr>
       </table>
        <table class="listtable">
            <tr align="center">
                <th rowspan="2">No.</th>
                <th rowspan="2">Kode Product</th>
                <th rowspan="2">Nama Product</th>
                <th colspan="3">OA</th>
                <th colspan="3">Sales Qty (Unit)</th>
                <th colspan="3">Net Sales (Rp.)</th>
                <th rowspan="2">% Ctr <br> Net Sales (Rp.)</th>
                <th rowspan="2">% Ctr <br> Sales Qty </th>
            </tr>

            <?php 
                $pecah1       = explode('-', $dfrom);
                $tgl1       = $pecah1[0];
                $bln1       = $pecah1[1];
                $tahun1     = $pecah1[2];
                $tahunprev1 = intval($tahun1) - 1;

                $pecah2       = explode('-', $dto);
                $tgl2       = $pecah2[0];
                $bln2       = $pecah2[1];
                $tahun2     = $pecah2[2];
                $tahunprev2 = intval($tahun2) - 1;

                $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
                $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;
           ?>
            <tr align="center">
                <th><?php echo $tahunprev1; ?></th>
                <th><?php echo $tahun1; ?></th>
                <th>% Growth</th>
                <th><?php echo $tahunprev1; ?></th>
                <th><?php echo $tahun1; ?></th>
                <th>% Growth</th>
                <th><?php echo $tahunprev1;?></th>
                <th><?php echo $tahun1; ?></th>
                <th>% Growth</th>
            </tr>
       <tbody>
              <?php 
                  $no = 1;
                  $totalob          = 0;
                  $totaloaprev      = 0;
                  $totaloa          = 0;
                  $totalnetitemprev = 0;  
                  $totalnetitem     = 0;  
                  $totalqtyprev     = 0;  
                  $totalqty         = 0;  
                  $totalctrsales    = 0;  
                  $totalctrqty      = 0;  
                  foreach ($isi as $key ) {
                    $totalnetitem += $key->netitem;
                    //$totalqty += $key->jml;
                    $totalnetitemprev   += $key->netitemprev;
                  
                  }
                  //echo "atas :".$totalnetitem."<pre>";
                  foreach ($isi as $row) {
                    $growthoa    = 0;
                    $growthjml   = 0;
                    $growthvnota = 0;

                    //untuk OA
                    if($row->oaprev == 0){
                        $growthoa = 0;
                    }else{
                        $growthoa = (($row->oa-$row->oaprev)/$row->oaprev)*100;
                    }

                    //untuk Qty
                    if($row->jmlprev == 0){
                        $growthjml = 0;
                    }else{
                        $growthjml = (($row->jml-$row->jmlprev)/$row->jmlprev)*100;
                    }

                    //untuk net
                    if($row->netitemprev == 0){
                        $growthnetitem = 0;
                    }else{
                        $growthnetitem = (($row->netitem-$row->netitemprev)/$row->netitemprev)*100;
                    }

                    $ctrnetsales = ($row->netitem/$totalnetitem)*100;
                    if($totalqty == 0){
                        $ctrqty = 0;
                    }else{
                      $ctrqty = ($row->jml/$totalqty)*100;
                    }
                    
                    echo "<tr>
                              <td>".$no."</td>
                              <td>".$row->i_product."</td>
                              <td>".$row->e_product_name."</td>
                              
                              <td align='right'>".number_format($row->oaprev)."</td>
                              <td align='right'>".number_format($row->oa)."</td>
                              <td align='right'>".number_format($growthoa,2)."%</td>
                              <td align='right'>".number_format($row->jmlprev)."</td>
                              <td align='right'>".number_format($row->jml)."</td>
                              <td align='right'>".number_format($growthjml,2)."%</td>
                              <td align='right'>".number_format($row->netitemprev,2)."</td>
                              <td align='right'>".number_format($row->netitem,2)."</td>
                              <td align='right'>".number_format($growthnetitem,2)."%</td>
                              <td align='right'>".number_format($ctrnetsales,2)."%</td>
                              <td align='right'>".number_format($ctrqty,2)."%</td>
                          </tr>";
                          //<td align='right'>".$row->ob."</td>
                  $no++;
                  $totalob            += $row->ob;
                  $totaloaprev        += $row->oaprev;
                  $totaloa            += $row->oa;
                  $totalqtyprev       += $row->jmlprev;
                  $totalqty           += $row->jml;
                  $totalctrsales      += $ctrnetsales;
                  $totalctrqty        += $ctrqty;
                  }
              ?>
       </tbody>

       <tbody>
            <?php 
                $totalgrowthoa      = (($totaloa-$totaloaprev)/$totaloaprev)*100;
                $totalgrowthqty     = (($totalqty-$totalqtyprev)/$totalqtyprev)*100;
                $totalgrowthnetitem = (($totalnetitem-$totalnetitemprev)/$totalnetitemprev)*100;
                //echo "bawah :".$totalnetitem;
                //<td><b><?php echo number_format($totalob,0);</b></td>

            ?>
           <tr align="center">
             <td colspan="3"><b>Total</b></td>
             
             <td><b><?php echo number_format($totaloaprev,0);?></b></td>
             <td><b><?php echo number_format($totaloa,0);?></b></td>
             <td><b><?php echo number_format($totalgrowthoa,2);?>%</b></td>
             <td><b><?php echo number_format($totalqtyprev,0);?></b></td>
             <td><b><?php echo number_format($totalqty,0);?></b></td>
             <td><b><?php echo number_format($totalgrowthqty,2);?>%</b></td>
             <td><b><?php echo number_format($totalnetitemprev,2);?></b></td>
             <td><b><?php echo number_format($totalnetitem,2);?></b></td>
             <td><b><?php echo number_format($totalgrowthnetitem,2);?>%</b></td>
             <td><b><?php echo number_format($totalctrsales,2);?>%</b></td>
             <td><b><?php echo number_format($totalctrqty,2);?>%</b></td>
           </tr> 
           </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
