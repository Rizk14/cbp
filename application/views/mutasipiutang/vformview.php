<div id='tmp'>
<h2><?php echo $page_title.' Periode : '.$iperiode; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'mutasipiutang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
	      <tr>
			<td colspan="10" align="center" style="font-size: 20px;"><b>MUTASI PIUTANG DAGANG<b></td>
	      </tr>
	    </thead>
	    <tr>
			<th rowspan="2" align="center">No</th>
			<th rowspan="2" align="center">Area</th>
			<th rowspan="2" align="center">Saldo Awal</th>
			<th rowspan="2" align="center">Penjualan</th>
			<th rowspan="2" align="center">Pembayaran</th>
			<th rowspan="2" align="center">KN Retur</th>
			<th colspan="3" align="center">KN Non Retur</th>
			<th rowspan="2" align="center">Saldo Akhir</th>
		</tr>
		<tr> 
			<th rowspan="2" align="center">Pembulatan</th>
			<th rowspan="2" align="center">Admin Bank</th>
			<th rowspan="2" align="center">Lain-lain</th>
		</tr>
	    	<tbody>
	    <?php 
	    		if($isi) {
	    			$no 				= 0;
	    			$totalSA 			= 0;
	    			$totalpenjualan 	= 0;
	    			$totalpembayaran 	= 0;
	    			$totalretur  		= 0;
	    			$totalpembulatan 	= 0;
	    			$totalbank			= 0;
	    			$totallain			= 0;
	    			$saldoakhir			= 0;
	    			$totalsaldoakhir		= 0;

	    			foreach ($isi as $row){
	    				$no++;
	    				$totalSA 			+= $row->v_saldo_awal;
	    				$totalpenjualan 	+= $row->penjualan;
	    				$totalpembayaran 	+= $row->pembayaran;
	    				$totalretur	 		+= $row->retur;
	    				$totalpembulatan	+= $row->pembulatan;
	    				$totalbank	 		+= $row->adminbank;
	    				$totallain	 		+= $row->lainlain;

	    				$saldoakhir 	= $row->v_saldo_awal + $row->penjualan - $row->pembayaran - $row->retur - $row->pembulatan - $row->adminbank - $row->lainlain;  
	    				$totalsaldoakhir += $saldoakhir;

	    				echo "<tr>
	    							<td align='center'>".$no."</td>
	    							<td>".$row->e_area_name."</td>
	    							<td align='right'>".number_format($row->v_saldo_awal)."</td>
	    							<td align='right'>".number_format($row->penjualan)."</td>
	    							<td align='right'>".number_format($row->pembayaran)."</td>
	    							<td align='right'>".number_format($row->retur)."</td>
	    							<td align='right'>".number_format($row->pembulatan)."</td>
	    							<td align='right'>".number_format($row->adminbank)."</td>
	    							<td align='right'>".number_format($row->lainlain)."</td>
	    							<td align='right'>".number_format($saldoakhir)."</td>
	    				      </tr>";
	    			}

	 				echo "<tr style='font-weight:bold;'>
	 						<td colspan='2' align='right'> TOTAL </td>
	 						<td align='right'>".number_format($totalSA)."</td>
	 						<td align='right'>".number_format($totalpenjualan)."</td>
	 						<td align='right'>".number_format($totalpembayaran)."</td>
	 						<td align='right'>".number_format($totalretur)."</td>
	 						<td align='right'>".number_format($totalpembulatan)."</td>
	 						<td align='right'>".number_format($totalbank)."</td>
	 						<td align='right'>".number_format($totallain)."</td>
	 						<td align='right'>".number_format($totalsaldoakhir)."</td>
	 					</tr>";
	    		}
	 	?>
	    </tbody>
	  </table>
    <input align="center" name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
    <input align="center" name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('mutasipiutang/cform/','#main')">
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<div id="pesan">
<script language="javascript" type="text/javascript">
  function yyy(b,d,c){
    showModal("mutasipiutang/cform/detail/"+b+"/"+c+"/"+d+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
