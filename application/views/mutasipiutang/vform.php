<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url'=>'mutasipiutang/cform/view','update'=>'#main','type'=>'post'));?>
	<div id="spbperareaform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Periode</td>
		<td width="1%">:</td>
		<td width="80%"><input type="hidden" id="iperiode" name="iperiode" value="">
						<select name="bulan" id="bulan" onmouseup="buatperiode()">
							<option></option>
							<option value='01'>Januari</option>
							<option value='02'>Pebruari</option>
							<option value='03'>Maret</option>
							<option value='04'>April</option>
							<option value='05'>Mei</option>
							<option value='06'>Juni</option>
							<option value='07'>Juli</option>
							<option value='08'>Agustus</option>
							<option value='09'>September</option>
							<option value='10'>Oktober</option>
							<option value='11'>November</option>
							<option value='12'>Desember</option>
						</select>
						<select name="tahun" id="tahun" onMouseUp="buatperiode()">
							<option></option>
							<option value='2016'>2016</option>
							<option value='2017'>2017</option>
							<option value='2018'>2018</option>
							<option value='2019'>2019</option>
						</select>
			</td>
	      </tr>
	    <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="View" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('mutasipiutang/cform/','#main')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>


<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url'=>'mutasipiutang/cform/transfer','update'=>'#main','type'=>'post'));?>
	<div id="spbperareaform">
	<div class="effect">
	  <div class="accordion2">
	  	<table>
	  		<tr><td style="font-size:20px;">Transfer Saldo</td></tr>
	  	</table>
	    <table class="mastertable">
	    <tr>
			<td width="19%">Dari Periode</td>
			<td width="1%">:</td>
			<td width="80%"><input type="hidden" id="iperiodefrom" name="iperiodefrom" value="">
							<select name="bulanfrom" id="bulanfrom" onmouseup="buatperiode1()">
								<option></option>
								<option value='01'>Januari</option>
								<option value='02'>Pebruari</option>
								<option value='03'>Maret</option>
								<option value='04'>April</option>
								<option value='05'>Mei</option>
								<option value='06'>Juni</option>
								<option value='07'>Juli</option>
								<option value='08'>Agustus</option>
								<option value='09'>September</option>
								<option value='10'>Oktober</option>
								<option value='11'>November</option>
								<option value='12'>Desember</option>
							</select>
							<select name="tahunfrom" id="tahunfrom" onMouseUp="buatperiode1()">
								<option></option>
								<option value='2016'>2016</option>
								<option value='2017'>2017</option>
								<option value='2018'>2018</option>
								<option value='2019'>2019</option>
							</select>
				</td>
	    </tr>

	     <tr>
			<td width="19%">Ke Periode</td>
			<td width="1%">:</td>
			<td width="80%"><input type="hidden" id="iperiodeto" name="iperiodeto" value="">
							<select name="bulanto" id="bulanto" onmouseup="buatperiode2()">
								<option></option>
								<option value='01'>Januari</option>
								<option value='02'>Pebruari</option>
								<option value='03'>Maret</option>
								<option value='04'>April</option>
								<option value='05'>Mei</option>
								<option value='06'>Juni</option>
								<option value='07'>Juli</option>
								<option value='08'>Agustus</option>
								<option value='09'>September</option>
								<option value='10'>Oktober</option>
								<option value='11'>November</option>
								<option value='12'>Desember</option>
							</select>
							<select name="tahunto" id="tahunto" onMouseUp="buatperiode2()">
								<option></option>
								<option value='2016'>2016</option>
								<option value='2017'>2017</option>
								<option value='2018'>2018</option>
								<option value='2019'>2019</option>
							</select>
				</td>
	    </tr>
	    <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="View" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('mutasipiutang/cform/','#main')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>


<script language="javascript" type="text/javascript">
  function buatperiode(){
	  periode=document.getElementById("tahun").value+document.getElementById("bulan").value;
	  document.getElementById("iperiode").value=periode;
  }

  function buatperiode1(){
	  periode=document.getElementById("tahunfrom").value+document.getElementById("bulanfrom").value;
	  document.getElementById("iperiodefrom").value=periode;
  }
  function buatperiode2(){
	  periode=document.getElementById("tahunto").value+document.getElementById("bulanto").value;
	  document.getElementById("iperiodeto").value=periode;
  }
</script>
