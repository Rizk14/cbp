<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'cekpelunasanhutang/cform/updatepelunasan','update'=>'#pesan','type'=>'post'));?>
	<div id="pelunasanform">
	<div class="effect">
	  <div class="accordion2">
		<?php foreach($isi as $row){
			  if($row->d_giro!=''){
				  $tmp=explode('-',$row->d_giro);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $tah=$tmp[0];
				  $row->d_giro=$tgl.'-'.$bln.'-'.$thn;
			  }
			  if($row->d_bukti!=''){
				  $tmp=explode('-',$row->d_bukti);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $tah=$tmp[0];
				  $row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
			  }
        if($row->d_cair!=''){
				  $tmp=explode('-',$row->d_cair);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_cair=$tgl.'-'.$bln.'-'.$thn;
          
			  }
		?>
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Nomor Pelunasan</td>
		<td><input type="text" id="ipelunasanap" name="ipelunasanap" value="<?php echo $row->i_pelunasanap; ?>" readonly>
			</td>			
		<td>Giro/Cek</td>
		<td><input type="hidden" id ="ijenisbayar"name="ijenisbayar" value="<?php echo $row->i_jenis_bayar; ?>">
			<input readonly id="ejenisbayarname" name="ejenisbayarname" value="<?php echo $row->e_jenis_bayarname; ?>">
			<input readonly id="igiro" name="igiro" value="<?php echo $row->i_giro; ?>">
			</td>
	      </tr>
	      <tr>
		<!--<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $row->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $row->i_area; ?>"></td>-->
    <td>Tgl Pelunasan</td>
		<td><input readonly id="dpelunasanap" name="dpelunasanap" value="<?php echo $row->d_bukti; ?>" onclick="showCalendar('',this,this,'','ddt',0,20,1)"></td>
		<td>Bank</td>
		<td><input readonly id="egirobank" name="egirobank" value="<?php echo $row->e_bank_name; ?>">
			<input type="hidden" id="nkuyear" name="nkuyear" value="<?php if(isset($tah)) echo $tah; ?>"></td>
	      </tr>
	      <tr>
		<td>Debitur</td>
		<td><input type="text" readonly id="esuppliername" name="esuppliername" value="<?php echo $row->e_supplier_name; ?>">
			<input type="hidden" readonly id="isupplier" name="isupplier" value="<?php echo $row->i_supplier; ?>"></td>
		<td>Tanggal JT</td>
		<td><input readonly id="djt" name="djt" value="<?php echo $row->d_giro ?>"></td>
	      </tr>
	      <tr>
		<td>Alamat</td>
		<td><input id="esupplieraddress" name="esupplieraddress" readonly value="<?php echo $row->e_supplier_address; ?>"></td>
		<td>Jumlah</td>
		<td><input type="text" id="vjumlah" name="vjumlah" value="<?php echo number_format($row->v_jumlah); ?>" onKeyUp="reformat(this);hitung();">
			<input type="hidden" id="vsisa" name="vsisa" value="<?php echo $vsisa; ?>" >
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>"></td>
	      </tr>
	      <tr>
		<td>Kota</td>
		<td><input readonly id="esuppliercity" name="esuppliercity" value="<?php echo $row->e_supplier_city; ?>"></td>
		<td>Lebih</td>
		<td><input type="text" readonly id="vlebih" name="vlebih" value="<?php echo number_format($row->v_lebih); ?>" ></td>
	      </tr>
	      <tr>
		<td>Ket Cek</td>
		<td><input name="ecek1" id="ecek1" value="" type="text"></td>
		<td colspan="2">&nbsp;</td>
	      </tr>	 	
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input <?php if($row->i_cek!='') echo 'disabled'; ?> name="login" id="login" value="di Cek" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick="show('cekpelunasanhutang/cform/view/<?php echo $dfrom."/".$dto."/".$isupplier."/"; ?>','#main')">
		    <!-- <input <?php if($row->f_pelunasan_cancel=='t') echo 'disabled'; ?> name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'> --></td>
		</tr>
	    </table>
		<?php }?>
			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:760px;">
					<th style="width:30px;" align="center">No</th>
					<th style="width:120px;" align="center">Nota</th>
					<th style="width:80px;" align="center">Tgl Nota</th>
					<th style="width:130px;" align="center">Nilai</th>
					<th style="width:130px;" align="center">Bayar</th>
					<th style="width:130px;" align="center">Sisa</th>
					<th style="width:30px;" align="center">Act</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 
					$i=0;
					foreach($detail as $row){ 
					$i++;
					  if($row->d_dtap!=''){
						  $tmp=explode('-',$row->d_dtap);
						  $tgl=$tmp[2];
						  $bln=$tmp[1];
						  $thn=$tmp[0];
						  $row->d_dtap=$tgl.'-'.$bln.'-'.$thn;
					  }
				?><table id="itemtem" class="listtable" style="width:760px;"><tbody><tr><td style="width:32px;"><input style="width:32px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>"></td><td style="width:132px;"><input style="width:132px;" readonly type="text" id="inota<?php echo $i; ?>" name="inota<?php echo $i; ?>" value="<?php echo $row->i_dtap; ?>"></td><td style="width:87px;"><input style="width:87px;" readonly type="text" id="dnota<?php echo $i; ?>" name="dnota<?php echo $i; ?>" value="<?php echo $row->d_dtap; ?>"></td><td style="width:144px;"><input readonly style="width:144px;text-align:right;" type="text" id="vnota<?php echo $i; ?>" name="vnota<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>"></td><td style="width:144px;"><input style="width:144px;text-align:right;" type="text" id="vjumlah<?php echo $i; ?>" name="vjumlah<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>" onkeyup="reformat(this);hetangs(<?php echo $i; ?>);"></td><td style="width:144px;"><input type="hidden" id="vsesa<?php echo $i; ?>" name="vsesa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa_nota); ?>"><input readonly style="width:144px;text-align:right;" type="text" id="vsisa<?php echo $i; ?>" name="vsisa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa_nota); ?>"><input type="hidden" id="vlebih<?php echo $i; ?>" name="vlebih<?php echo $i; ?>" value=""><input type="hidden" id="vasal<?php echo $i; ?>" name="vasal<?php echo $i; ?>" value="<?php echo $row->v_jumlah+$row->v_sisa_nota; ?>"></td><td style="width:50px;">&nbsp;</td></tr></tbody></table>
				<?php }?>
			</div>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
	  cek='false';
	  if((document.getElementById("idt").value!='') &&
	    (document.getElementById("ijenisbayar").value!='') &&
	    (document.getElementById("vjumlah").value!='') &&
	    (document.getElementById("isupplier").value!='')) {
	    var a=parseFloat(document.getElementById("jml").value);
      for(i=1;i<=a;i++){
	      if(document.getElementById("vjumlah"+i).value!='0'){
	        cek='true';
	        break;
	      }else{

	      cek='false';
	      } 
      }
	    if(cek=='true'){
	      document.getElementById("login").disabled=true;
		    document.getElementById("cmdtambahitem").disabled=true;
	    }else{
		    alert('Isi jumlah detail pelunasan minimal 1 item !!!');
	      document.getElementById("login").disabled=false;
	    }
	  }else{
	    alert('Data header masih ada yang salah !!!');
	  }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function hitung(){
	jml=document.getElementById("jml").value;
	vjml=formatulang(document.getElementById("vjumlah").value);
	vjmlitem=0;
	for(i=1;i<=jml;i++){
		vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
	}
	document.getElementById("vlebih").value=parseFloat(formatulang(vjml))-vjmlitem;
  }
  function viewgiro()
  {
	jenis=document.getElementById("ijenisbayar").value;
	icustomer=document.getElementById("icustomer").value;
	iarea=document.getElementById("iarea").value;
	if(jenis=='01'){
		showModal("cekpelunasan/cform/giro/"+icustomer+"/"+iarea+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='02'){
		
//		showModal("pelunasan/cform/tunai/","#light"); 
//		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='03'){
		showModal("cekpelunasanhutang/cform/ku/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='04'){
		showModal("cekpelunasanhutang/cform/kn/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='05'){
		showModal("cekpelunasanhutang/cform/lebihbayar/"+isupplier+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}
  }
  function hetang(a)
  {	
		vjmlbyr	  = parseFloat(formatulang(document.getElementById("vjumlah").value));
		vlebih 	  = parseFloat(formatulang(document.getElementById("vlebih").value));
		vsisadt   = parseFloat(formatulang(document.getElementById("vsisa").value));
		vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
		vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
		jml		= document.getElementById("jml").value;
		if(vjmlitem>vsisaitem){
			alert("jumlah bayar tidak boleh melebihi sisa nota !!!!!");
			if(vlebih>vsisaitem){
				document.getElementById("vjumlah"+a).value=formatcemua(vsisaitem);
			}else{
				document.getElementById("vjumlah"+a).value=formatcemua(vlebih);
			}
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
			document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
		}else if(vjmlitem>vjmlbyr){
			alert("jumlah bayar item tidak boleh melebihi jumlah bayar total !!!!!");
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				if(i!=a){
					vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
				}
			}	
			document.getElementById("vjumlah"+a).value=vjmlbyr-vjmlitem;
			document.getElementById("vlebih").value="0";
		}else if(vjmlitem>vsisadt){
			alert("jumlah bayar tidak boleh melebihi sisa tagihan !!!!!");
			document.getElementById("vjumlah"+a).value="0";
		}else{
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
//      alert(vjmlitem);
//      alert(vjmlbyr);
			if(vjmlitem>vjmlbyr){
				alert("jumlah item tidak boleh melebihi jumlah bayar !!!!!");
				document.getElementById("vjumlah"+a).value="0";
				vjmlitem=0;
				for(i=1;i<=jml;i++){
					vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
				}
			}
			document.getElementById("vlebih"+a).value=formatcemua(vjmlbyr-vjmlitem);
			document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
		}
		sesa=parseFloat(formatulang(document.getElementById("vsisa"+a).value))-parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
		if(sesa<=0){
			document.getElementById("vsesa"+a).value='0';
		}else{
			document.getElementById("vsesa"+a).value=formatcemua(sesa);
		}
  }
 function hetangs(a)
  {	
	vjmlbyr	  = parseFloat(formatulang(document.getElementById("vjumlah").value));
	vlebih 	  = parseFloat(formatulang(document.getElementById("vlebih").value));
	vsisadt   = parseFloat(formatulang(document.getElementById("vsisa").value));
	vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
	vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
	vasal	  = parseFloat(formatulang(document.getElementById("vasal"+a).value));
	jml		  = document.getElementById("jml").value;
/*	if(vjmlitem>vsisaitem){ */
	if(vjmlitem>vasal){
		alert("jumlah bayar tidak boleh melebihi sisa nota !!!!!");
/*		if(vlebih>vsisaitem){ */
		if(vlebih>vasal){
/*			document.getElementById("vjumlah"+a).value=formatcemua(vsisaitem); */
			document.getElementById("vjumlah"+a).value=formatcemua(vasal);
		}else{
			document.getElementById("vjumlah"+a).value=formatcemua(vlebih);
		}
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
		}
/* 		document.getElementById("vlebih").value=vjmlbyr-vjmlitem; */
		document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
	}else if(vjmlitem>vjmlbyr){
		alert("jumlah bayar item tidak boleh melebihi jumlah bayar total !!!!!");
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			if(i!=a){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
		}	
		document.getElementById("vjumlah"+a).value=vjmlbyr-vjmlitem;
		document.getElementById("vlebih").value="0";
	}else if(vjmlitem>vsisadt){
		alert("jumlah bayar tidak boleh melebihi sisa tagihan !!!!!");
		document.getElementById("vjumlah"+a).value="0";
	}else{
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
		}
		if(vjmlitem>vjmlbyr){
			alert("jumlah item tidak boleh melebihi jumlah bayar !!!!!");
			document.getElementById("vjumlah"+a).value="0";
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
		}
		document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
	}
  }
</script>
