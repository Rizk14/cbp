<?php 
//      require_once("php/fungsi.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title." s/d ".$dto; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'opnnotanas/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
		<th>Area</th>
	  <th>No Nota</th>
		<th>Tgl Nota</th>
		<th>Tgl JT</th>
		<th>Divisi</th>
		<th>Salesman</th>
		<th>Customer</th>
    <th>PKP</th>
		<th>Nilai</th>
		<th>Sisa</th>
    <th>Status</th>
    <!--<th>Lama</th>-->
		<th>Ket</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
		  	$sisa=number_format($row->v_sisa);
		  	$nilai=number_format($row->v_nota_netto);
				$tmp=explode('-',$row->d_nota);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_nota=$tgl.'-'.$bln.'-'.$thn;
				$tmp=explode('-',$row->d_jatuh_tempo);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_jatuh_tempo=$tgl.'-'.$bln.'-'.$thn;
        //$lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);
        //$umurpiutang=datediff("d",$row->d_nota,$dto("Y-m-d"),false);
        if($row->f_customer_pkp=='t'){
          $pkp="Ya";
        }else{
          $pkp="Tdk";
        }
        $status=$nilai-$sisa;
        if($status==0){
          $ketstatus="Utuh";
        }else{
          $ketstatus="Sisa";
        }
			  echo "<tr>
              <td>$row->i_area - $row->e_area_shortname</td>";
				if($row->f_nota_cancel=='t'){
			  echo "<td><h1>$row->i_nota</h1></td>";
				}else{
			  echo "<td>$row->i_nota</td>";
				}
			  echo "<td>$row->d_nota</td>
				  <td>$row->d_jatuh_tempo</td>";
        echo "<td>$row->e_product_groupname</td>";
        echo "<td>($row->i_salesman) $row->e_salesman_name</td>";
        echo "
				  <td>($row->i_customer) $row->e_customer_name</td>
          <td>$pkp</td>
				  <td align=right>$nilai</td>
				  <td align=right>$sisa</td>
          <td>$ketstatus</td>";
        if($row->ket!='' || $row->e_pelunasan_remark!=''){
          echo "<td>$row->e_pelunasan_remark ($row->ket)</td>";
        }else{
          echo "<td>$row->e_remark</td>";
        }
			  echo "</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
