<?php 
$totjum = 0;
$totlebih = 0;
if($total){

	foreach ($total as $row) {
		$totjum = $totjum + $row->v_jumlah;
		$totlebih = $totlebih + $row->v_lebih;
	}
}
	?>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listallocdebetnota-ap/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >
		<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" >
		<input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Area</th>
			<th>No DN A/P</th>
 	    	<th>No Alokasi</th>
			<th>Tgl Alokasi</th>
			<th>Supplier</th>
			<th>Jumlah</th>
			<th>Lebih</th>
			<th class="action">Action</th>
	    <tbody>
	    <?php 
	      	$tmpjumlah = 0;
	      	$tmplebih = 0;
    		$query3	= $this->db->query(" select i_periode from tm_periode ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$i_periode	= $hasilrow->i_periode;
			}
		if($isi){
			foreach($isi as $row){
				$tmpjumlah = $tmpjumlah + $row->v_jumlah;
				$tmplebih = $tmplebih + $row->v_lebih;
				$tmp=explode('-',$row->d_alokasi);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$thbl=$thn.$bln;
				$row->d_alokasi=$tgl.'-'.$bln.'-'.$thn;
				if($i_periode <= $thbl)
					$bisahapus = true;
				else
					$bisahapus = false;

			  echo "<tr> 
				  <td>$row->i_area - $row->e_area_name</td>
				  <td>$row->i_dn</td>";
				if($row->f_alokasi_cancel=='t'){
				  echo "<td><h1>$row->i_alokasi</h1></td>";
        }else{
				  echo "<td>$row->i_alokasi</td>";
				}
				echo "
				  <td>$row->d_alokasi</td>
				  <td>$row->e_supplier_name</td>
				  <td align=right>".number_format($row->v_jumlah)."</td>
				  <td align=right>".number_format($row->v_lebih)."</td>
				  <td class=\"action\">";
				  if($row->f_alokasi_cancel=='f'){
					  echo "<a href=\"#\" onclick='show(\"dnalokasi/cform/edit/$row->i_alokasi/$row->i_dn/$row->i_supplier/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					  if($bisahapus && $this->session->userdata('i_area') == '00'){
              echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"listallocdebetnota-ap/cform/delete/$row->i_alokasi/$row->i_dn/$row->i_supplier/$dfrom/$dto/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
            }
          }
			  echo "
				  </td>
				</tr>";
			}
		}
		echo "<tr>
			<td colspan='5'>Total</td>
			<td align=right>".number_format($tmpjumlah)."</td>
			<td align=right>".number_format($tmplebih)."</td>
			<td></td>
		</tr>";
		echo "<tr>
			<td colspan='5'>Total Semua</td>
			<td align=right>".number_format($totjum)."</td>
			<td align=right>".number_format($totlebih)."</td>
			<td></td>
		</tr>";
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
  	<marquee direction="left" scrolldelay="120" onmouseover="this.setAttribute('scrollamount', 0, 0);" onmouseout="this.setAttribute('scrollamount', 6, 0);">*Jika Nomor Alokasi Membesar Maka Nomor Alokasi Tersebut Telah Di Batalkan.</marquee>
      </div>
	  <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('listallocdebetnota-ap/cform/','#tmpx')">
      <?=form_close()?>
    </td>
  </tr>
</table>
