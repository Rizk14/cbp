<?php
echo "<h2>$page_title</h2>";

$thcloseperiode = substr($closingperiode, 0, 4);
$blcloseperiode = substr($closingperiode, 4, 2);

?>
<style type="text/css" media="all">
	.font {
		font-size: 16px;
		text-align: left;
	}

	.mtable {
		text-align: left;
		font-size: 16px;
		width: 40%;
		border: 0px solid #f2f5f7;
		padding: 4px;
	}

	.mtable2 {
		text-align: left;
		font-size: 16px;
		width: 90%;
		border: 1px solid #f2f5f7;
		padding: 4px;
	}

	.luar {
		background: #ffafe0;
		width: 100%;
		border: 1px solid #f2f5f7;
		padding: 4px;
	}
</style>

<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'closingkasbank/cform/closing', 'update' => '#pesan', 'type' => 'post', 'id' => 'cekbox')); ?> -->
<table class="luar">
	<tr>
		<td>
			<table class="mtable">
				<tr <?php echo $this->session->userdata('level') == "0" ? "" : "hidden" ?>>
					<td style="width:54px"><b>Date Closing</b></td>
					<td style="width:2px">:</td>
					<td style="width:15px">
						<input type="hidden" id="dopennow" name="dopennow" value="<?= $dopennow; ?>">
						<input type="hidden" id="dclosingx" name="dclosingx" value="<?= $dclosing; ?>">
						<input type="hidden" id="iperiode" name="iperiode" value="<?= $closingperiode; ?>">
						<?php $data = array(
							'name'        => 'dclosing',
							'id'          => 'dclosing',
							'value'       => $dclosing,
							'placeholder' => 'Date ...',
							'readonly'    => 'true',
							'onclick'	  => "showCalendar('',this,this,'','dclosing',0,20,1)"
						);
						echo form_input($data);
						?>
					</td>
				</tr>
				<!-- <tr>
					<td style="width:54px"><b>Date Open</b></td>
					<td style="width:2px">:</td>
					<td style="width:15px">
						<input type="hidden" id="dopenx" name="dopenx" value="<?= $dopen; ?>">
						<?php
						$data = array(
							'name'        => 'dopen',
							'id'          => 'dopen',
							'value'       => $dopen,
							'placeholder' => 'Date ...',
							'readonly'    => 'true',
							'onclick'	  => "showCalendar('',this,this,'','dopen',0,20,1)"
						);
						echo form_input($data);
						?>
					</td>
				</tr> -->

				<?php if ($this->session->userdata('level') == "0") { ?>
					<tr>
						<td style="width:84px"><b>Closing Periode (All)</b></td>
						<td style="width:2px">:</td>
						<td style="width:15px">
							<input type="hidden" id="iperiodex" name="iperiodex" value="<?= $closingperiode; ?>">

							<select name="bulan" id="bulan" onmouseup="buatperiode()">
								<option></option>
								<option value='01' <?php echo $blcloseperiode == "01" ? "selected" : "" ?>>Januari</option>
								<option value='02' <?php echo $blcloseperiode == "02" ? "selected" : "" ?>>Februari</option>
								<option value='03' <?php echo $blcloseperiode == "03" ? "selected" : "" ?>>Maret</option>
								<option value='04' <?php echo $blcloseperiode == "04" ? "selected" : "" ?>>April</option>
								<option value='05' <?php echo $blcloseperiode == "05" ? "selected" : "" ?>>Mei</option>
								<option value='06' <?php echo $blcloseperiode == "06" ? "selected" : "" ?>>Juni</option>
								<option value='07' <?php echo $blcloseperiode == "07" ? "selected" : "" ?>>Juli</option>
								<option value='08' <?php echo $blcloseperiode == "08" ? "selected" : "" ?>>Agustus</option>
								<option value='09' <?php echo $blcloseperiode == "09" ? "selected" : "" ?>>September</option>
								<option value='10' <?php echo $blcloseperiode == "10" ? "selected" : "" ?>>Oktober</option>
								<option value='11' <?php echo $blcloseperiode == "11" ? "selected" : "" ?>>November</option>
								<option value='12' <?php echo $blcloseperiode == "12" ? "selected" : "" ?>>Desember</option>
							</select>

							<select name="tahun" id="tahun" onMouseUp="buatperiode()">
								<option></option>
								<?php
								$tahun1 = date('Y') - 3;
								$tahun2 = date('Y');

								for ($i = $tahun1; $i <= $tahun2; $i++) {
									echo "<option value='$i'";
									if ($thcloseperiode == $i) {
										echo "selected";
									};
									echo ">$i</option>";
								}
								?>
							</select>

							<input name="saveperiode" id="saveperiode" value="Close" type="submit" onclick="clperiode()">
						</td>
					</tr>
				<?php } ?>
			</table>

			<table class="mtable2" id="tabledata">
				<?php
				if ($isi) {
					foreach ($isi as $row) {
						$btkbin 	= $row->d_open_kbin == $dopennow ? "Unclose" : "Closing";
						$btkbankin 	= $row->d_open_kbankin == $dopennow ? "Unclose" : "Closing";
						$btpkk 		= $row->d_open_kkin == $dopennow ? "Unclose" : "Closing";
						$btkb 		= $row->d_open_kb == $dopennow ? "Unclose" : "Closing";
						$btkkk 		= $row->d_open_kk == $dopennow ? "Unclose" : "Closing";
						$btkbank 	= $row->d_open_kbank == $dopennow ? "Unclose" : "Closing";
				?>
						<tr>
							<td style="width:116px"><b>Kas Besar (Masuk)</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="kbin" id="kbin" onclick="showCalendar('',this,this,'','kbin',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kbin)) ?>">
							</td>
							<td><input name="save1" id="save1" value="<?= $btkbin ?>" type="submit" onclick="clkbin(this.value)"></td>

							<td style="width:85px"><b>Bank (Masuk)</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="kbankin" id="kbankin" onclick="showCalendar('',this,this,'','kbankin',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kbankin)) ?>">
							</td>
							<td><input name="save2" id="save2" value="<?= $btkbankin ?>" type="submit" onclick="clkbankin(this.value)"></td>

							<td style="width:124px"><b>Pengisian Kas Kecil</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="pkk" id="pkk" onclick="showCalendar('',this,this,'','pkk',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kkin)) ?>">
							</td>
							<td><input name="save3" id="save3" value="<?= $btpkk ?>" type="submit" onclick="clpkk(this.value)"></td>
						</tr>

						<tr>
							<td style="width:116px"><b>Kas Besar (Keluar)</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="kb" id="kb" onclick="showCalendar('',this,this,'','kb',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kb)) ?>">
							</td>
							<td><input name="save4" id="save4" value="<?= $btkb ?>" type="submit" onclick="clkb(this.value)"></td>

							<td style="width:85px"><b>Bank (Keluar)</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="kbank" id="kbank" onclick="showCalendar('',this,this,'','kbank',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kbank)) ?>">
							</td>
							<td><input name="save5" id="save5" value="<?= $btkbank ?>" type="submit" onclick="clkbank(this.value)"></td>

							<td style="width:124px"><b>Kas Kecil</b></td>
							<td style="width:1px">:</td>
							<td style="width:30px">
								<input name="kk" id="kk" onclick="showCalendar('',this,this,'','kk',0,20,1)" value="<?= date('d-m-Y', strtotime($row->d_open_kk)) ?>">
							</td>
							<td><input name="save6" id="save6" value="<?= $btkkk ?>" type="submit" onclick="clkk(this.value)"></td>
						</tr>
				<?php }
				} ?>
			</table>
			<!-- <table class="mtable2" id="tabledata" hidden>
				<tr>
					<td style="width:81px"><b>Kas Besar (Keluar)</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kb" id="f_kb">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>

					<td style="width:81px"><b>Kas Besar (Masuk)</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kbin" id="f_kbin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>

					<td style="width:81px"><b>Bank (Keluar)</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kbank" id="f_kbank">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>

				<tr>
					<td style="width:81px"><b>Bank (Masuk)</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kbankin" id="f_kbankin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>

					<td style="width:81px"><b>Kas Kecil</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kk" id="f_kk">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>

					<td style="width:83px"><b>Pengisian Kas Kecil</b></td>
					<td style="width:1px">:</td>
					<td style="width:30px">&nbsp;&nbsp;
						<input type="checkbox" name="f_kkin" id="f_kkin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table> -->
		</td>
	</tr>
</table>


<!-- <table>
	<tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
			<input name="login" id="login" value="Simpan" type="submit">
			<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('closingkasbank/cform/','#main');">
		</td>
	</tr>
</table> -->
<!-- <#?= form_close() ?> -->
<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('closingkasbank/cform/','#main');">
<div id="pesan"></div>

<script type="text/javascript">
	/* $('#login').click(function(event) {
		// if ($("#tabledata input:checkbox:checked").length > 0) {
		$('#login').hidden = true;
		return true;
		// } else {
		// 	alert('Pilih Transaksi Kas/Bank minimal satu!');
		// 	return false;
		// }
	}); */

	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}

	function clkbin(a) {
		if (a == "Closing") {
			var kbin = document.getElementById('dopennow').value;
		} else {
			var kbin = document.getElementById('kbin').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				kbin: kbin,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clkbankin(a) {
		if (a == "Closing") {
			var kbankin = document.getElementById('dopennow').value;
		} else {
			var kbankin = document.getElementById('kbankin').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				kbankin: kbankin,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clpkk(a) {
		if (a == "Closing") {
			var pkk = document.getElementById('dopennow').value;
		} else {
			var pkk = document.getElementById('pkk').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				pkk: pkk,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clkb(a) {
		if (a == "Closing") {
			var kb = document.getElementById('dopennow').value;
		} else {
			var kb = document.getElementById('kb').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				kb: kb,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clkbank(a) {
		if (a == "Closing") {
			var kbank = document.getElementById('dopennow').value;
		} else {
			var kbank = document.getElementById('kbank').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				kbank: kbank,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clkk(a) {
		if (a == "Closing") {
			var kk = document.getElementById('dopennow').value;
		} else {
			var kk = document.getElementById('kk').value;
		}

		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				kk: kk,
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function clperiode() {
		var dclosing = document.getElementById('dclosing').value;
		var iperiode = document.getElementById('iperiode').value;

		$.ajax({
			type: "POST",
			url: "<?php echo site_url('closingkasbank/cform/closing_new'); ?>",
			data: {
				dclosing: dclosing,
				iperiode: iperiode,
			},
			success: function(data) {
				refreshview()
			},

			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}

		})
	};

	function refreshview() {
		show('<?= $folder ?>/cform/', '#main');
	}
</script>