<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  	$cetak='';
	$nor	= str_repeat(" ",5);
	$abn	= str_repeat(" ",12);
	$ab	  = str_repeat(" ",9);
	$ipp  = new PrintIPP();
	$ipp->setHost($host);
	$ipp->setPrinterURI($uri);
	$ipp->setRawText();
	$ipp->unsetFormFeed();
	$isi = $master;
	$jml = count($isi);
	$nomor=0;
	foreach($isi as $row){
    $nomor++;
		$xmp=CetakHeader($row,$nor,$abn,$ab,$ipp);
	  $cetak.=$xmp;
		$ymp  ='';
		$ibm	= $row->i_refference;
    $tmp=explode("-",$row->d_refference);
    $th=$tmp[0];
    $bl=$tmp[1];
    $hr=$tmp[2];
    $drefference=$hr." ".substr(mbulan($bl),0,3)." ".$th;
    $bbm	= $this->mmaster->bacabbm($row->i_refference);
    $i=0;
    foreach ($bbm as $rowbbm){
      $i++;
	    $cetak.=$nor.NmPerusahaan."\n";		
	    $cetak.=$nor."                  BUKTI BARANG MASUK            No. : ".$row->i_refference."\n";		
	    $cetak.=$nor."                  ( B B M ) - RETUR             Tgl.: ".$drefference."\n\n";
	    $cetak.=$nor."Telah diterima dari : ".$row->e_customer_name."\n";
	    $cetak.=$nor."Referensi  : ".$row->i_kn."\n";
	    $cetak.=$nor."Keterangan : ".$rowbbm->i_refference_document."\n".CHR(15);
      $cetak.=$ab.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),55).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),10).CHR(194).str_repeat(CHR(196),18).CHR(191)."\n";
      $cetak.=$ab.CHR(179)."NO. ".CHR(179)." KODE".str_repeat(" ",50).CHR(179)." UNIT  ".CHR(179)." HARGA    ".CHR(179)." NILAI            ".CHR(179)."\n";
      $cetak.=$ab.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G                  ".CHR(179)."       ".CHR(179)."  (PC)    ".CHR(179)."                  ".CHR(179)."\n";
      $cetak.=$ab.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),55).CHR(216).str_repeat(CHR(205),7).CHR(216).str_repeat(CHR(205),10).CHR(216).str_repeat(CHR(205),18).CHR(181)."\n";
      
      $bbmi	    = $this->mmaster->bacadetailbbm($row->i_refference);
      $jml_bbmi = count($bbmi);
      $i=1;
      $rn=0;
      $totsub=0;
    	foreach($bbmi as $rowbbmi){
      $rn++;
#    	echo $i.'<br>';
#    	echo $rowbbmi->i_bbm.'<br>';
        $pjg=strlen($i);
        $spcno=3;
        for($xx=1;$xx<=$pjg;$xx++){
			    $spcno=$spcno-1;
		    }
        $pjg=strlen($rowbbmi->i_product.$rowbbmi->e_product_name);
        $spcnm=54;
        for($xx=1;$xx<=$pjg;$xx++){
			    $spcnm=$spcnm-1;
		    }
        $pjg=strlen($rowbbmi->n_quantity);
        $spcjml=6;
        for($xx=1;$xx<=$pjg;$xx++){
			    $spcjml=$spcjml-1;
		    }
        $pjg=strlen(number_format($rowbbmi->v_unit_price));
        $spchrg=10;
        for($xx=1;$xx<=$pjg;$xx++){
			    $spchrg=$spchrg-1;
		    }
        $sub=$rowbbmi->n_quantity*$rowbbmi->v_unit_price;
        $pjg=strlen(number_format($sub));
        $spcsub=18;
        for($xx=1;$xx<=$pjg;$xx++){
			    $spcsub=$spcsub-1;
		    }
        $totsub=$totsub+$sub;
        $cetak.=$ab.CHR(179).str_repeat(" ",$spcno).$i." ".CHR(179).$rowbbmi->i_product." ".$rowbbmi->e_product_name.str_repeat(" ",$spcnm).CHR(179).str_repeat(" ",$spcjml).number_format($rowbbmi->n_quantity)." ".CHR(179).str_repeat(" ",$spchrg).number_format($rowbbmi->v_unit_price).CHR(179).str_repeat(" ",$spcsub).number_format($sub).CHR(179)."\n";
        if($jml_bbmi>7){
          if(($rn%15)==0){
            $cetak.=$ab.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),55).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),10).CHR(197).str_repeat(CHR(196),18).CHR(180)."\n\n\n\n\n\n\n";/*kalau item ada 11 tapi header page ke 2 kejauhan atau deket edit disini*/
	          $cetak.=CHR(18).$nor.NmPerusahaan."\n";		
	          $cetak.=CHR(18).$nor."                  BUKTI BARANG MASUK            No. : ".$row->i_refference."\n";		
	          $cetak.=CHR(18).$nor."                  ( B B M ) - RETUR             Tgl.: ".$drefference."\n\n";
	          $cetak.=CHR(18).$nor."Telah diterima dari : ".$row->e_customer_name."\n";
	          $cetak.=CHR(18).$nor."Referensi  : ".$row->i_kn."\n";
	          $cetak.=CHR(18).$nor."Keterangan : ".$rowbbm->i_refference_document."\n".CHR(15);
            $cetak.=$ab.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),55).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),10).CHR(194).str_repeat(CHR(196),18).CHR(191)."\n";
            $cetak.=$ab.CHR(179)."NO. ".CHR(179)." KODE".str_repeat(" ",50).CHR(179)." UNIT  ".CHR(179)." HARGA    ".CHR(179)." NILAI            ".CHR(179)."\n";
            $cetak.=$ab.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G                  ".CHR(179)."       ".CHR(179)."  (PC)    ".CHR(179)."                  ".CHR(179)."\n";
            $cetak.=$ab.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),55).CHR(216).str_repeat(CHR(205),7).CHR(216).str_repeat(CHR(205),10).CHR(216).str_repeat(CHR(205),18).CHR(181)."\n";
				    $tm	= "\n\n\n\n\n\n\n\n\n";
          }elseif(($rn<15)&&($rn==$jml_bbmi)){
            $cetak.=$ab.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),55).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),10).CHR(193).str_repeat(CHR(196),18).CHR(217)."\n\n\n\n\n";/*kalau item ada 11 tapi header page ke 2 kejauhan atau deket edit disini*/
				    $rn=0;
			        $m=$jml_bbmi-7;
                if($m>0){
			            switch($m){
			             case 1:
				            $tr="\n\n\n\n\n\n\n\n\n";
				            break;
			            case 2:
				            $tr="\n\n\n\n\n\n\n\n";
				            break;
			            case 3:
				            $tr="\n\n\n\n\n\n\n";
				            break;
                  case 4:
				            $tr="\n\n\n\n\n\n";
				            break;
			            case 5:
				            $tr="\n\n\n\n\n";
				            break;
			            case 6:
				            $tr="\n\n\n\n";
				            break;
			            case 7:
				            $tr="\n\n\n";
				            break;
			            case 8:
				            $tr="\n\n";
				            break;
		             }
		           }
		          $cetak.=$tr;
        		$cetak.=CHR(18);
            $cetak.=$nor.NmPerusahaan."\n";		
            $cetak.=$nor."                  BUKTI BARANG MASUK            No. : ".$row->i_refference."\n";		
            $cetak.=$nor."                  ( B B M ) - RETUR             Tgl.: ".$drefference."\n\n";
            $cetak.=$nor."Telah diterima dari : ".$row->e_customer_name."\n";
            $cetak.=$nor."Referensi  : ".$row->i_kn."\n";
            $cetak.=$nor."Keterangan : ".$rowbbm->i_refference_document."\n".CHR(15);
            $cetak.=$ab.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),55).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),10).CHR(194).str_repeat(CHR(196),18).CHR(191)."\n";
            $cetak.=$ab.CHR(179)."NO. ".CHR(179)." KODE".str_repeat(" ",50).CHR(179)." UNIT  ".CHR(179)." HARGA    ".CHR(179)." NILAI            ".CHR(179)."\n";
            $cetak.=$ab.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G                  ".CHR(179)."       ".CHR(179)."  (PC)    ".CHR(179)."                  ".CHR(179)."\n";
            $cetak.=$ab.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),55).CHR(216).str_repeat(CHR(205),7).CHR(216).str_repeat(CHR(205),10).CHR(216).str_repeat(CHR(205),18).CHR(181)."\n";
            $tm = "\n\n\n\n\n\n\n\n\n";
          }
        }
        $i++;
      }

      if($rn>0){
			  switch($rn){
			   case 1:
				  $tm="\n\n\n\n\n\n\n\n";
				  break;
			  case 2:
				  $tm="\n\n\n\n\n\n\n";
				  break;
			  case 3:
				  $tm="\n\n\n\n\n\n";
				  break;
			  case 4:
				  $tm="\n\n\n\n\n";
				  break;
			  case 5:
				  $tm="\n\n\n\n";
				  break;
			  case 6:
				  $tm="\n\n\n";
				  break;
			  case 7:
				  $tm="\n\n";
				  break;
				  }
			  }

#      $cetak.="\n\n\n\n\n\n";
    }
    $cetak.=$ab.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),55).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),10).CHR(197).str_repeat(CHR(196),18).CHR(180)."\n";
    $pjg=strlen(number_format($totsub));
    $spctotsub=15;
    for($xx=1;$xx<=$pjg;$xx++){
	    $spctotsub=$spctotsub-1;
    }
#	    echo $spctotsub.' || '.$totsub;die;
    $cetak.=$ab.str_repeat(" ",64)."Jumlah          ".CHR(179)." Rp".str_repeat(" ",$spctotsub).number_format($totsub).CHR(179)."\n";
    $pjg=strlen(number_format($row->v_discount));
    $spcdis=15;
    for($xx=1;$xx<=$pjg;$xx++){
	    $spcdis=$spcdis-1;
    }
    $cetak.=$ab.str_repeat(" ",64)."Potongan        ".CHR(179)." Rp".str_repeat(" ",$spcdis).number_format($row->v_discount).CHR(179)."\n";
    $cetak.=$ab.str_repeat(" ",80).CHR(195).str_repeat(CHR(196),18).CHR(180)."\n";
    $pjg=strlen(number_format($row->v_netto));
    $spcnet=15;
    for($xx=1;$xx<=$pjg;$xx++){
	    $spcnet=$spcnet-1;
    }
    $cetak.=$ab.str_repeat(" ",64)."Jumlah Bersih   ".CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_netto).CHR(179)."\n";
    $cetak.=$ab.str_repeat(" ",80).CHR(192).str_repeat(CHR(196),18).CHR(217)."\n";
    $cetak.=$ab."    Mengetahui                                                                        Hormat kami,     \n\n\n\n\n";
    $cetak.=$ab."( ".TahuKN." )                                                                  ( ".BikinKN." ) \n";
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $cetak.=$ab."TANGGAL CETAK : ".$tgl."\n".CHR(18);
	  $cetak.=$tm;
		if($nomor==$jml){
#      echo $cetak;
      $ipp->setdata($cetak);
      $ipp->printJob();
      $ipp->unsetFormFeed();
      }
		}
	function CetakHeader($row,$nor,$abn,$ab,$ipp){
#		$cetak=CHR(18);
		$tmp=explode("-",$row->d_kn);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dkn=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$tmp=explode("-",$row->d_refference);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$drefference=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$cetak=CHR(18);
		$cetak.=$nor.NmPerusahaan."        Tgl. : ".$dkn."\n\n";		
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."K R E D I T    N O T A".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0).str_repeat(" ",13)."Kepada Yth.\n";
		$cetak.=$nor."No.: ".$row->i_kn.str_repeat(" ",22).$row->i_customer."-".$row->e_customer_name."\n";
    $pjg=9+strlen($row->e_salesman_name);
    $spcsls=35;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcsls=$spcsls-1;
		}
    $pjg=6+strlen($drefference);
    $spcref=54;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcref=$spcref-1;
		}
    $pjg=strlen(number_format($row->v_netto));
    $spcnet=13;
    for($xx=1;$xx<=$pjg;$xx++){
			$spcnet=$spcnet-1;
		}
		$cetak.=$nor."KS.: ".$row->i_salesman."  ".$row->e_salesman_name.str_repeat(" ",$spcsls).CHR(15).$row->e_customer_address." ".$row->e_customer_city.CHR(18)."\n";
    $cetak.=$nor.CHR(218).str_repeat(CHR(196),54).CHR(194).str_repeat(CHR(196),16).CHR(191)."\n";
    $cetak.=$nor.CHR(179)." Telah kami Kredit Rekening Saudara atas ".str_repeat(" ",13).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179)." Sesuai dengan BBM Nomor ".$row->i_refference.str_repeat(" ",14).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179)." Tgl. ".$drefference.str_repeat(" ",$spcref).CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_netto).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(179).str_repeat(" ",54).CHR(179).str_repeat(" ",16).CHR(179)."\n";
    $cetak.=$nor.CHR(192).str_repeat(CHR(196),54).CHR(197).str_repeat(CHR(196),16).CHR(180)."\n";
    $cetak.=$nor.str_repeat(" ",55).CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_netto).CHR(179)."\n";
    $cetak.=$nor.str_repeat(" ",55).CHR(192).str_repeat(CHR(196),16).CHR(217)."\n";
		$bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($row->v_netto));	
		$cetak.=$nor.CHR(15)."Terbilang : ".$kata." RUPIAH)\n\n".CHR(18);				
    $cetak.=$nor."    Mengetahui                                           Hormat kami,   \n\n\n\n\n";
    $cetak.=$nor."( ".TahuKN." )                                     ( ".BikinKN." ) \n".CHR(15);
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $cetak.=$ab."TANGGAL CETAK : ".$tgl."\n\n\n\n\n\n\n\n".CHR(18);
    return $cetak;
	}

	function CetakHeaderBBM($row,$nor,$abn,$ab,$ipp){
		$tmp=explode("-",$row->d_refference);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$drefference=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		
		$cetak=CHR(18);
	  $cetak.=$nor.NmPerusahaan."\n";		
	  $cetak.=$nor."                  BUKTI BARANG MASUK            No. : ".$row->i_refference."\n";		
	  $cetak.=$nor."                  ( B B M ) - RETUR             Tgl.: ".$drefference."\n\n";
	  $cetak.=$nor."Telah diterima dari : ".$row->e_customer_name."\n";
	  $cetak.=$nor."Referensi  : ".$row->i_kn."\n";
	  $cetak.=$nor."Keterangan : ".$rowbbm->i_refference_document."\n".CHR(15);
    $cetak.=$ab.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),55).CHR(194).str_repeat(CHR(196),7).CHR(194).str_repeat(CHR(196),10).CHR(194).str_repeat(CHR(196),18).CHR(191)."\n";
    $cetak.=$ab.CHR(179)."NO. ".CHR(179)." KODE".str_repeat(" ",50).CHR(179)." UNIT  ".CHR(179)." HARGA    ".CHR(179)." NILAI            ".CHR(179)."\n";
    $cetak.=$ab.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G                  ".CHR(179)."       ".CHR(179)."  (PC)    ".CHR(179)."                  ".CHR(179)."\n";
    $cetak.=$ab.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),55).CHR(216).str_repeat(CHR(205),7).CHR(216).str_repeat(CHR(205),10).CHR(216).str_repeat(CHR(205),18).CHR(181)."\n";
    return $cetak;
	}




	function CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp){
      /*JUMLAH POTONGAN DAN JUMLAH BERSIH*/
      
      $cetak.=$ab.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),55).CHR(193).str_repeat(CHR(196),7).CHR(193).str_repeat(CHR(196),10).CHR(197).str_repeat(CHR(196),18).CHR(180)."\n";
      $pjg=strlen(number_format($totsub));
      $spctotsub=15;
      for($xx=1;$xx<=$pjg;$xx++){
		    $spctotsub=$spctotsub-1;
	    }
#	    echo $spctotsub.' || '.$totsub;die;
      $cetak.=$ab.str_repeat(" ",64)."Jumlah          ".CHR(179)." Rp".str_repeat(" ",$spctotsub).number_format($totsub).CHR(179)."\n";
      $pjg=strlen(number_format($row->v_discount));
      $spcdis=15;
      for($xx=1;$xx<=$pjg;$xx++){
		    $spcdis=$spcdis-1;
	    }
      $cetak.=$ab.str_repeat(" ",64)."Potongan        ".CHR(179)." Rp".str_repeat(" ",$spcdis).number_format($row->v_discount).CHR(179)."\n";
      $cetak.=$ab.str_repeat(" ",80).CHR(195).str_repeat(CHR(196),18).CHR(180)."\n";
      $pjg=strlen(number_format($row->v_netto));
      $spcnet=15;
      for($xx=1;$xx<=$pjg;$xx++){
		    $spcnet=$spcnet-1;
	    }
      $cetak.=$ab.str_repeat(" ",64)."Jumlah Bersih   ".CHR(179)." Rp".str_repeat(" ",$spcnet).number_format($row->v_netto).CHR(179)."\n";
      $cetak.=$ab.str_repeat(" ",80).CHR(192).str_repeat(CHR(196),18).CHR(217)."\n";
      $cetak.=$ab."    Mengetahui                                                                        Hormat kami,     \n\n\n\n\n";
      $cetak.=$ab."( ".TahuKN." )                                                                  ( ".BikinKN." ) \n";
      $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
      $cetak.=$ab."TANGGAL CETAK : ".$tgl."\n".CHR(18);
		return $cetak;
	}
