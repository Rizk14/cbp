<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'list-tokopareto/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $th; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totvolly=0;
            $totvolcm=0;
            $totvolcy=0;
            $totnotaly=0;
            $totnotacm=0;
            $totvollm=0;
            $totnotalm=0;
            $totnotacy=0;
            foreach($isi as $ro){
              $totvolly=$totvolly+$ro->volly;
              $totvolcm=$totvolcm+$ro->volcm;
              $totvolcy=$totvolcy+$ro->volcy;
              $totvollm=$totvollm+$ro->vollm;
              $totnotaly=$totnotaly+$ro->notaly;
              $totnotacm=$totnotacm+$ro->notacm;
              $totnotalm=$totnotalm+$ro->notalm;
              $totnotacy=$totnotacy+$ro->notacy;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan=3>No</th>
         <th rowspan=3>area</th>
         <th rowspan=3>Nama Lang</th>
         <th colspan=3><?php echo $prevth; ?></th>
         <th colspan=6><?php echo $th; ?></th>
         <th colspan=4>% Growth</th>
         </tr>
         <tr>
         <th>YTD</th>
         <th align=right><?php echo number_format($totvolly); ?></th>
         <th></th>
         <th><?php echo mbulan($bl); ?></th>
         <th align=right><?php echo number_format($totvolcm); ?></th>
         <th></th>
         <th>YTD</th>
         <th align=right><?php echo number_format($totvolcy); ?></th>
         <th></th>
         <th colspan=2>MTD</th>
         <th colspan=2>YTD</th>
         </tr>
         <tr>
         <th>Vol</th>
         <th>Value</th>
         <th>%</th>
         <th>Vol</th>
         <th>Value</th>
         <th>%</th>
         <th>Vol</th>
         <th>Value</th>
         <th>%</th>
         <th>Vol</th>
         <th>Value</th>
         <th>Vol</th>
         <th>Value</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totpersenvolly=0;
        $totpersenvolcm=0;
        $totpersenvolcy=0;
        foreach($isi as $row){
          $i++;
          
          $persenspbselm=0;
          $persencustselm=0;
          $persenspbsely=0;
          $persencustsely=0;

          $persenvolly=($row->volly/$totvolly)*100;
          $persenvolcm=($row->volcm/$totvolcm)*100;
          $persenvolcy=($row->volcy/$totvolcy)*100;
          $totpersenvolly=$totpersenvolly+$persenvolly;
          $totpersenvolcm=$totpersenvolcm+$persenvolcm;
          $totpersenvolcy=$totpersenvolcy+$persenvolcy;
          $spbselm=$row->notacm-$row->notalm;
          $custselm=$row->volcm-$row->vollm;
          $spbsely=$row->notacy-$row->notaly;
          $custsely=$row->volcy-$row->volly;
###################################################
          $growthmtdvol=$totvolcm/$totvollm*100-100;
          $growthmtdvalue=$totnotacm/$totnotalm*100-100;
          $growthytdvol=$totvolcy/$totvolly*100-100;
          $growthytdvalue=$totnotacy/$totnotaly*100-100;


          if($row->notalm==0){
            $persenspbselm=0;
          }else{
            $persenspbselm=($spbselm/$row->notalm)*100;
          }
          if($row->vollm==0){
            $persencustselm=0;
          }else{
            $persencustselm=($custselm/$row->vollm)*100;
          }
          if($row->notaly==0){
            $persenspbsely=0;
          }else{
            $persenspbsely=($spbsely/$row->notaly)*100;
          }
          if($row->volly==0){
            $persencustsely=0;
          }else{
            $persencustsely=($custsely/$row->volly)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->eareaname</td>
              <td style='font-size:12px;'>$row->namapel</td>
              <td style='font-size:12px;' align=right>".number_format($row->volly)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->notaly)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenvolly,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->volcm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->notacm)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenvolcm,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->volcy)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->notacy)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenvolcy,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persenspbselm,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustselm,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenspbsely,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustsely,2)."</td>
              </tr>";
         }
        echo "<tr>
        <td style='font-size:12px;' colspan=3><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotaly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvolly,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolcm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotacm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvolcm,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolcy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totnotacy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvolcy,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdvol,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdvalue,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdvol,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthytdvalue,2)."</b></td>
        </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
