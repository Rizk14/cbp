<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<!--<?php echo form_open('updatepassword/cform', array('id' => 'updatepasswordform')); ?>-->
			<?php echo $this->pquery->form_remote_tag(array('url' => 'updatepassword/cform', 'update' => '#konten', 'type' => 'post')); ?>
			<div id="updatepasswordform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="20%">Nama User</td>
								<td width="1%">:</td>
								<td width="79%"><input type=text name=eusername id=eusername value="<?php echo $this->session->userdata('user_name') ?>" maxlength=30 readonly></td>
							</tr>
							<tr>
								<td width="20%">Password Lama</td>
								<td width="1%">:</td>
								<td width="79%"><input type="password" name="epasswordold" id="epasswordold" value="" maxlength="40"></td>
							</tr>
							<tr>
								<td width="20%">Password Baru</td>
								<td width="1%">:</td>
								<td width="79%"><input type="password" name="epasswordnew1" id="epasswordnew1" value="" maxlength="40"></td>
							</tr>
							<tr>
								<td width="20%">Konfirmasi Password Baru</td>
								<td width="1%">:</td>
								<td width="79%"><input type="password" name="epasswordnew2" id="epasswordnew2" value="" maxlength="40"></td>
							</tr>
							<tr>
								<td width="20%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="79%">
									<input name="login" id="login" value="Simpan" type="submit" onclick="return dipales()">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("gantipassword/cform/","#main")'>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<?= form_close() ?>
		</td>
	</tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">
	function dipales() {
		if (document.getElementById('epasswordold').value == "") {
			alert("Password Lama Masih Kosong!");
			return false;
		} else if (document.getElementById('epasswordnew1').value == "") {
			alert("Password Baru Masih Kosong!");
			return false;
		} else if (document.getElementById('epasswordnew2').value == "") {
			alert("Konfirmasi Password Baru Masih Kosong!");
			return false;
		} else {
			setTimeout(() => {
				$("input").attr("disabled", true);
				$("select").attr("disabled", true);
				$("#submit").attr("disabled", true);
				document.getElementById("cmdreset").removeAttribute('disabled');
			}, 100);
		}
	}

	function xxx(a, b) {
		if (confirm(b) == 1) {
			document.getElementById("iclassdelete").value = a;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/class/cform/delete";
			formna.submit();
		}
	}

	function yyy(b) {
		document.getElementById("iclassedit").value = b;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/class/cform/edit";
		formna.submit();
	}

	function cek() {
		oldpass = document.getElementById('epasswordold').value;
		newpass = document.getElementById('epasswordnew1').value;
		if (oldpass == newpass) {
			alert('Password lama dan baru tidak boleh sama');
			document.getElementById('epasswordnew1').value = '';
			document.getElementById('epasswordnew2').value = '';
		}
	}
</script>