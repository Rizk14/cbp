<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'akt-pkk/cform/simpan', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="aktkkform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable">
              <tr>
                <td width="12%">Area</td>
                <td width="1%">:</td>
                <td width="87%"><input type="hidden" name="iarea" id="iarea" value="<?php if ($iarea) {
    echo $iarea;
}
?>"><input readonly name="eareaname" id="eareaname" value="<?php if ($eareaname) {
    echo $eareaname;
}
?>"></td>
              </tr>
              <tr>
                <td width="12%">Periode</td>
                <td width="1%">:</td>
                <td width="87%"><select disabled onclick="cekperiode(); cektanggal();">
                    <?php if ($iperiode) {
    $bl = substr($iperiode, 4, 2);
}
?>
                    <option <?php if ($bl == '01') {
    echo 'selected';
}
?>>01</option>
                    <option <?php if ($bl == '02') {
    echo 'selected';
}
?>>02</option>
                    <option <?php if ($bl == '03') {
    echo 'selected';
}
?>>03</option>
                    <option <?php if ($bl == '04') {
    echo 'selected';
}
?>>04</option>
                    <option <?php if ($bl == '05') {
    echo 'selected';
}
?>>05</option>
                    <option <?php if ($bl == '06') {
    echo 'selected';
}
?>>06</option>
                    <option <?php if ($bl == '07') {
    echo 'selected';
}
?>>07</option>
                    <option <?php if ($bl == '08') {
    echo 'selected';
}
?>>08</option>
                    <option <?php if ($bl == '09') {
    echo 'selected';
}
?>>09</option>
                    <option <?php if ($bl == '10') {
    echo 'selected';
}
?>>10</option>
                    <option <?php if ($bl == '11') {
    echo 'selected';
}
?>>11</option>
                    <option <?php if ($bl == '12') {
    echo 'selected';
}
?>>12</option>
                  </select>&nbsp;<input readonly name="iperiodeth" id="iperiodeth" value="<?php if ($iperiode) {
    echo substr($iperiode, 0, 4);
}
?>" maxlength="4" onkeyup="cekperiode(); cektanggal();">
                  <input type="hidden" name="iperiodebl" id="iperiodebl" value="<?=$bl;?>" /></td>
              </tr>
              <tr>
                <td width="12%">Tgl Kas Kecil</td>
                <td width="1%">:</td>
                <td width="87%"><input readonly name="dkk" id="dkk" value="<?php if ($tanggal) {
    echo $tanggal;
}
?>">
                  Saldo : <input readonly name="vsaldo" id="vsaldo" value="<?php if ($saldo) {
    echo number_format($saldo);
}
?>"></td>
              </tr>
              <td width="12%">Tgl Bukti</td>
              <td width="1%">:</td>
              <td width="87%"><input readonly name="dbukti" id="dbukti" value="<?php if ($dbukti) {
    echo $dbukti;
}
?>"></td>
  </tr>
  <tr>
    <td width="12%">Keterangan</td>
    <td width="1%">:</td>
    <td width="87%"><input name="edescription" id="edescription" value="Terima dari pusat"></td>
  </tr>
  <tr>
    <td width="12%">Jumlah</td>
    <td width="1%">:</td>
    <td width="87%"><input name="vkk" id="vkk" value="0" onkeyup="reformat(this);"></td>
  </tr>
  <td width="12%">&nbsp;</td>
  <td width="1%">&nbsp;</td>
  <td width="87%">
    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
    <input name="cmdprint" id="cmdprint" value="Cetak Voucher" type="button" onclick="view_rv();">
    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('akt-pkk/cform/','#main');">
  </td>
  </tr>
</table>
</div>
<div id="pesan"></div>
</div>
</div>
<?=form_close()?>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
function dipales() {
  if (
    (document.getElementById("iperiodeth").value == '') ||
    (document.getElementById("iperiodebl").value == '') ||
    (document.getElementById("iarea").value == '') ||
    (document.getElementById("dkk").value == '')
  ) {
    alert("Data Header belum lengkap !!!");
  } else {
    document.getElementById("login").hidden = true;
  }
}

function kendaraan() {
  area = document.getElementById("iarea").value;
  showModal("akt-kk/cform/kendaraan/" + area + "/", "#light");
  jsDlgShow("#konten *", "#fade", "#light");
}

function cekperiode() {
  dkk = document.getElementById('dkk').value;
  periode = document.getElementById('iperiodeth').value + document.getElementById('iperiodebl').value;
  if (periode != '' && dkk != '') {
    dtmp = dkk.split('-');
    per = dtmp[2] + dtmp[1];
    if (periode != per) {
      alert("Periode harus sama dengan tanggal kas kecil !!!");
      document.getElementById("dkk").value = '';
    }
  }
}

function afterSetDateValue(ref_field, target_field, date) {
  cekperiode();
  cektanggal();
}

function cektanggal() {
  var dbukti = document.getElementById('dbukti').value;
  var dkk = document.getElementById('dkk').value;
  if (dbukti != '' && dkk != '') {
    dtmp = dbukti.split('-');
    thnbk = dtmp[2];
    blnbk = dtmp[1];
    hrbk = dtmp[0];
    dtmp = dkk.split('-');
    thnkk = dtmp[2];
    blnkk = dtmp[1];
    hrkk = dtmp[0];
    if (thnbk > thnkk) {
      alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
      document.getElementById('dbukti').value = '';
    } else {
      if (blnbk > blnkk) {
        alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
        document.getElementById('dbukti').value = '';
      } else if (blnbk == blnkk) {
        if (hrbk > hrkk) {
          alert('Tanggal bukti tidak boleh lebih dari tanggal kas kecil !!!');
          document.getElementById('dbukti').value = '';
        }
      }
    }
  }
}

function view_rv() {
  area = document.getElementById("iarea").value;
  periode = document.getElementById("iperiodeth").value + document.getElementById("iperiodebl").value;
  showModal("akt-pkk/cform/rv/" + area + "/" + periode + "/", "#light");
  jsDlgShow("#konten *", "#fade", "#light");
}
</script>