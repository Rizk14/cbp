<?php 
	echo "<h2>".$page_title."</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-pkk/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="aktkkform">
	<div class="effect">
	  <div class="accordion2">
		 <?php foreach($isi as $row){ ?>
	    <table class="mastertable">
	      <tr>
		<td width="12%">Nomor</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="ikk" id="ikk" value="<?php echo $row->i_kk; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>"><input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="iperiodebl" id="iperiodebl" value="<?php echo substr($row->i_periode,4,2);?>" maxlength="2">
						&nbsp;<input readonly name="iperiodeth" id="iperiodeth" value="<?php echo substr($row->i_periode,0,4);?>" maxlength="4"></td>
	      </tr>
		<tr>
		<?php 
			$tmp=explode('-',$row->d_kk);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_kk=$tgl.'-'.$bln.'-'.$thn;
		?>
		<td width="12%">Tgl Kas Kecil</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dkk" id="dkk" value="<?php echo $row->d_kk; ?>" onclick="showCalendar('',this,this,'','dkk',0,20,1)"></td>
	      </tr>
		<tr>
		<?php 
      if($row->d_bukti!=''){
			  $tmp=explode('-',$row->d_bukti);
			  $tgl=$tmp[2];
			  $bln=$tmp[1];
			  $thn=$tmp[0];
				$row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
				$thnbln = $thn.$bln;
      }        
		?>
		<td width="12%">Tgl Bukti</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="dbukti" id="dbukti" value="<?php echo $row->d_bukti; ?>" onclick="showCalendar('',this,this,'','dbukti',0,20,1)"></td>
	      </tr>
	      <tr>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="87%"><input name="edescription" id="edescription" value="<?php echo $row->e_description; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="87%"><input name="vkk" id="vkk" value="<?php echo number_format($row->v_kk); ?>" onkeyup="reformat(this);"></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		<?php 
		$periode = $this->db->query("select i_periode from tm_periode")->row()->i_periode;
		    if($bisaedit && $periode <= $thnbln){
		?>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
    <?php 
        }
    ?>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('list-akt-kk/cform/view/<?php echo $dfrom.'/'.$dto.'/'.$iarea; ?>','#main');">
		</td>
	      </tr>
		</table>
		<?php }?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("iarea").value=='') ||
			(document.getElementById("icoa").value==''))
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function kendaraan()
	{
		area=document.getElementById("iarea").value;
		showModal("akt-kk/cform/kendaraan/"+area+"/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
</script>
