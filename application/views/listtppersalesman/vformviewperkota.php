<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listtppersalesman/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    $eareaname='';
    if($isi){
			foreach($isi as $row){
        $eareaname=$row->e_area_name;
      }
    }
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Target Penjualan Per Kota Area $eareaname</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
#		echo "<center><h3>Tanggal proses : $tglproses</h3></center>";
?>
    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Kota</th>
	   	    <th>Salesman</th>
	   	    <th>Target</th>
			    <th>Penjualan</th>
			    <th>% Penjualan</th>
			    <th>Reguler</th>
			    <th>% Reguler</th>
			    <th>Baby</th>
			    <th>% Baby</th>
			    <th>Retur</th>
			    <th>% Retur</th>
			    <th>Jual Non Ins</th>
			    <th>Retur Non Ins</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
			foreach($isi as $row){
        if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_grossinsentif/$row->v_target)*100,2);
        }else{
          $persen='0.00';
        }
        if($row->v_real_regularinsentif==null || $row->v_real_regularinsentif=='')$row->v_real_regularinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenreg=number_format(($row->v_real_regularinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenreg='0.00';
        }
        if($row->v_real_babyinsentif==null || $row->v_real_babyinsentif=='')$row->v_real_babyinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenbaby=number_format(($row->v_real_babyinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenbaby='0.00';
        }
        if($row->v_retur_insentif==null || $row->v_retur_insentif=='')$row->v_retur_insentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenretur=number_format(($row->v_retur_insentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenretur='0.00';
        }
        if($row->v_spb_gross==null || $row->v_spb_gross=='')$row->v_spb_gross=0;
        if($row->v_target!=0){
          $persenspb=number_format(($row->v_spb_gross/$row->v_target)*100,2);
        }else{
          $persenspb='0.00';
        }
	      echo "<tr>
          <td align=right>$i</td>
          <td>$row->e_city_name</td>
          <td>$row->i_salesman-$row->e_salesman_name</td>
          <td align=right>".number_format($row->v_target)."</td>
          <td align=right>".number_format($row->v_nota_grossinsentif)."</td>
			    <td align=right>".$persen." %</td>
			    <td align=right>".number_format($row->v_real_regularinsentif)."</td>
			    <td align=right>".$persenreg." %</td>
			    <td align=right>".number_format($row->v_real_babyinsentif)."</td>
			    <td align=right>".$persenbaby." %</td>
			    <td align=right>".number_format($row->v_retur_insentif)."</td>
			    <td align=right>".$persenretur." %</td>
          <td align=right>".number_format($row->v_nota_grossnoninsentif)."</td>
          <td align=right>".number_format($row->v_retur_noninsentif)."</td>";
        $i++;
				echo "</tr>";	
			}
		}
	      ?>
	    </tbody>
	  </table>
        <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='xxx()' ></center>
</div>
<script language="javascript" type="text/javascript">
  function xxx(){
    this.close();
  }
</script>
