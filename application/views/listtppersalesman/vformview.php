<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'updatepenjualan/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    echo "<center><h2>PT. DIALOGUE GARMINDO UTAMA</h2></center>";
		echo "<center><h3>Target Penjualan Per Area</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
    $tglproses='';
    if($isi){
			foreach($isi as $row){
        $tglproses=$row->d_process;
      }
    }
		echo "<center><h3>Tanggal proses : $tglproses</h3></center>";
?>
    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Area</th>
	   	    <th>Target</th>
			    <th>Penjualan</th>
			    <th>% Penjualan</th>
			    <th>Reguler</th>
			    <th>% Reguler</th>
			    <th>Baby</th>
			    <th>% Baby</th>
			    <th>Retur</th>
			    <th>% Retur</th>
			    <th>Jual Non Ins</th>
			    <th>Retur Non Ins</th>
			    <th>SPB Bln ini</th>
			    <th>% SPB</th>
        	<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){

      $ttarget=0;
      $tgrossinsentif=0;
	    $tpersen=0;
	    $tregularinsentif=0;
	    $tpersenreg=0;
	    $tbabyinsentif=0;
	    $tpersenbaby=0;
	    $treturinsentif=0;
	    $tpersenretur=0;
      $tgrossnoninsentif=0;
      $treturnoninsentif=0;
      $tvspbgross=0;
      $tpersenspb=0;

      $i=1;
			foreach($isi as $row){
        if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_grossinsentif/$row->v_target)*100,2);
        }else{
          $persen='0.00';
        }
        if($row->v_real_regularinsentif==null || $row->v_real_regularinsentif=='')$row->v_real_regularinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenreg=number_format(($row->v_real_regularinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenreg='0.00';
        }
        if($row->v_real_babyinsentif==null || $row->v_real_babyinsentif=='')$row->v_real_babyinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenbaby=number_format(($row->v_real_babyinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenbaby='0.00';
        }
        if($row->v_retur_insentif==null || $row->v_retur_insentif=='')$row->v_retur_insentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenretur=number_format(($row->v_retur_insentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenretur='0.00';
        }
        if($row->v_spb_gross==null || $row->v_spb_gross=='')$row->v_spb_gross=0;
        if($row->v_target!=0){
          $persenspb=number_format(($row->v_spb_gross/$row->v_target)*100,2);
        }else{
          $persenspb='0.00';
        }
	      echo "<tr>
          <td align=right>$i</td>
          <td>$row->i_area-$row->e_area_name</td>
          <td align=right>".number_format($row->v_target)."</td>
          <td align=right>".number_format($row->v_nota_grossinsentif)."</td>
			    <td align=right>".$persen." %</td>
			    <td align=right>".number_format($row->v_real_regularinsentif)."</td>
			    <td align=right>".$persenreg." %</td>
			    <td align=right>".number_format($row->v_real_babyinsentif)."</td>
			    <td align=right>".$persenbaby." %</td>
			    <td align=right>".number_format($row->v_retur_insentif)."</td>
			    <td align=right>".$persenretur." %</td>
          <td align=right>".number_format($row->v_nota_grossnoninsentif)."</td>
          <td align=right>".number_format($row->v_retur_noninsentif)."</td>
          <td align=right>".number_format($row->v_spb_gross)."</td>
          <td align=right>".$persenspb." %</td>";
        $i++;
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick=\"persales('$iperiode','$row->i_area');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" title=\"per Sales\" alt=\"per Sales\"></a><a href=\"#\" onclick=\"pernota('$iperiode','$row->i_area');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" title=\"per Nota\" alt=\"per Sales\"></a><a href=\"#\" onclick=\"perkota('$iperiode','$row->i_area');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" title=\"per Kota\" alt=\"per Kota\"></a><a href=\"#\" onclick=\"retur('$iperiode','$row->i_area');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" title=\"Detail KN\" alt=\"Detail KN\"></a><a href=\"#\" onclick=\"allsales('$iperiode');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" title=\"all Sales\" alt=\"all Sales\"></a>";
				echo "</td></tr>";
        $ttarget=$ttarget+$row->v_target;
        $tgrossinsentif=$tgrossinsentif+$row->v_nota_grossinsentif;
	      $tpersen=$tpersen+$persen;
	      $tregularinsentif=$tregularinsentif+$row->v_real_regularinsentif;
	      $tpersenreg=$tpersenreg+$persenreg;
	      $tbabyinsentif=$tbabyinsentif+$row->v_real_babyinsentif;
	      $tpersenbaby=$tpersenbaby+$persenbaby;
	      $treturinsentif=$treturinsentif+$row->v_retur_insentif;
	      $tpersenretur=$tpersenretur+$persenretur;
        $tgrossnoninsentif=$tgrossnoninsentif+$row->v_nota_grossnoninsentif;
        $treturnoninsentif=$treturnoninsentif+$row->v_retur_noninsentif;
        $tvspbgross=$tvspbgross+$row->v_spb_gross;
        $tpersenspb=$tpersenspb+$persenspb;
      
			}

        $tpersen=number_format($tpersen/$i,2);
	      $tpersenreg=number_format($tpersenreg/$i,2);
	      $tpersenbaby=number_format($tpersenbaby/$i,2);
	      $tpersenretur=number_format($tpersenretur/$i,2);
        $tpersenspb=number_format($tpersenspb/$i,2);

	      echo "<tr>
          <td></td>
          <td>Total</td>
          <td align=right>".number_format($ttarget)."</td>
          <td align=right>".number_format($tgrossinsentif)."</td>
			    <td align=right>".$tpersen." %</td>
			    <td align=right>".number_format($tregularinsentif)."</td>
			    <td align=right>".$tpersenreg." %</td>
			    <td align=right>".number_format($tbabyinsentif)."</td>
			    <td align=right>".$tpersenbaby." %</td>
			    <td align=right>".number_format($treturinsentif)."</td>
			    <td align=right>".$tpersenretur." %</td>
          <td align=right>".number_format($tgrossnoninsentif)."</td>
          <td align=right>".number_format($treturnoninsentif)."</td>
          <td align=right>".number_format($tvspbgross)."</td>
          <td align=right>".$tpersenspb." %</td>
          <td></td>";


		}
	      ?>
	    </tbody>
	  </table>
<!--    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("updatepenjualan/cform/index","#main");' ></center>-->
        <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='xxx()' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/updatepenjualan/cform/viewdetail";
	  formna.submit();
  }
  function xxx(){
    this.close();
  }
  function persales(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/updatepenjualan/cform/persales/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function pernota(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/updatepenjualan/cform/pernota/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function perkota(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/updatepenjualan/cform/perkota/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function retur(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/updatepenjualan/cform/retur/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function allsales(iperiode){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/updatepenjualan/cform/allsales/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
