<div id='tmp'>
<h2><?php echo $page_title.'  ( '.$total_rows.' record )'; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listttbblmbbm/cform/index','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th align="Center">Area</th>
			<th align="Center">No TTB</th>
			<th align="Center">Tgl TTB</th>
			<th align="Center">Customer</th>
			<th align="Center">Nilai TTB (Rp)</th>
			<th align="Center">No BBM</th>
			<th align="Center">Tgl BBM</th>
			<th align="Center">Tgl Trm Admin PST</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
				$dttb = $row->d_ttb;
				$dbbm=$row->d_bbm;
				$dreceive1=$row->d_receive1;	
				$v_ttb_netto = number_format($row->v_ttb_netto,0,',','.');
			  	echo "<tr>";
			   echo "<td>$row->i_area</td>";
			   echo "<td>$row->i_ttb</td>";
			  	echo "
				  <td>$dttb</td>
				  <td>$row->i_customer - $row->e_customer_name</td>
				  <td align='right'>$v_ttb_netto</td>
				  <td>$row->i_bbm</td>
				  <td>$dbbm</td>
				  <td>$dreceive1</td>";
			  	echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php //echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
