<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h3><?php 
    if($dfrom){
	    $tmp=explode('-',$dfrom);
	    $tgl=$tmp[2];
	    $bln=$tmp[1];
	    $thn=$tmp[0];
	    $dfrom=$tgl.'-'.$bln.'-'.$thn;
    }
    if($dto){
	    $tmp=explode('-',$dto);
	    $tgl=$tmp[2];
	    $bln=$tmp[1];
	    $thn=$tmp[0];
	    $dto=$tgl.'-'.$bln.'-'.$thn;
    }
echo 'Dari Tanggal : '.$dfrom.' Sampai Tanggal : '.$dto; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperdivisikelas/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
	if($isi){
      $group='';
?>
	  <th>AREA</th>
<?php 
    foreach($kelasnya as $rw){
      echo "<th>$rw->e_customer_classname</th>";
    }
    echo '<tbody>';
    $grandatu=0;
    $grandua=0;
    $grandiga=0;
    $grandampa=0;
    $grandima=0;
    $grandanam=0;
    $granduju=0;
    $grandapan=0;
    $grandalan=0;
    $grandsiji=0;
    $grandloro=0;
    $grandtelu=0;
    $grandpapat=0;
    $grandlimo=0;
    $grandanem=0;
    $grandpitu=0;
    $grandlapan=0;
    $grandsanga=0;
    foreach($prodnya as $row)
    {
      if($group==''){
        echo "<tr><td colspan=10><h2>$row->e_product_groupname</h2></td></tr>";
      }
      if($group!='' && $group!=$row->e_product_groupname){
        echo "<tr><td colspan=10><h2>$row->e_product_groupname</h2></td></tr>";
      }
      $subatu=0;
      $subua=0;
      $subiga=0;
      $subampa=0;
      $subima=0;
      $subanam=0;
      $subuju=0;
      $subapan=0;
      $subalan=0;
      $subsiji=0;
      $subloro=0;
      $subtelu=0;
      $subpapat=0;
      $sublimo=0;
      $subanem=0;
      $subpitu=0;
      $sublapan=0;
      $subsanga=0;
      foreach($areanya as $raw)
      { 
        echo "<tr><td>".$raw->i_area."-".$raw->e_area_name."</td>";
        $ada=false;
        foreach($isi as $riw)
        {
          if( ($riw->area==$raw->i_area) && ($riw->grup==$row->i_product_group)){
            $ada=true;
            echo "<td align=right>".number_format($riw->atu)." - (".number_format($riw->siji)." toko)</td>";
            echo "<td align=right>".number_format($riw->ua)." - (".number_format($riw->loro)." toko)</td>";
            echo "<td align=right>".number_format($riw->iga)." - (".number_format($riw->telu)." toko)</td>";
            echo "<td align=right>".number_format($riw->ampa)." - (".number_format($riw->papat)." toko)</td>";
            echo "<td align=right>".number_format($riw->ima)." - (".number_format($riw->limo)." toko)</td>";
            echo "<td align=right>".number_format($riw->anam)." - (".number_format($riw->anem)." toko)</td>";
            echo "<td align=right>".number_format($riw->uju)." - (".number_format($riw->pitu)." toko)</td>";
            echo "<td align=right>".number_format($riw->apan)." - (".number_format($riw->lapan)." toko)</td>";
            echo "<td align=right>".number_format($riw->alan)." - (".number_format($riw->sanga)." toko)</td>";
            echo "</tr>";
            $subatu=$subatu+$riw->atu;
            $subua=$subua+$riw->ua;
            $subiga=$subiga+$riw->iga;
            $subampa=$subampa+$riw->ampa;
            $subima=$subima+$riw->ima;
            $subanam=$subanam+$riw->anam;
            $subuju=$subuju+$riw->uju;
            $subapan=$subapan+$riw->apan;
            $subalan=$subalan+$riw->alan;
            $grandatu=$grandatu+$riw->atu;
            $grandua=$grandua+$riw->ua;
            $grandiga=$grandiga+$riw->iga;
            $grandampa=$grandampa+$riw->ampa;
            $grandima=$grandima+$riw->ima;
            $grandanam=$grandanam+$riw->anam;
            $granduju=$granduju+$riw->uju;
            $grandapan=$grandapan+$riw->apan;
            $grandalan=$grandalan+$riw->alan;
            $subsiji=$subsiji+$riw->siji;
            $subloro=$subloro+$riw->loro;
            $subtelu=$subtelu+$riw->telu;
            $subpapat=$subpapat+$riw->papat;
            $sublimo=$sublimo+$riw->limo;
            $subanem=$subanem+$riw->anem;
            $subpitu=$subpitu+$riw->pitu;
            $sublapan=$sublapan+$riw->lapan;
            $subsanga=$subsanga+$riw->sanga;
            $grandsiji=$grandsiji+$riw->siji;
            $grandloro=$grandloro+$riw->loro;
            $grandtelu=$grandtelu+$riw->telu;
            $grandpapat=$grandpapat+$riw->papat;
            $grandlimo=$grandlimo+$riw->limo;
            $grandanem=$grandanem+$riw->anem;
            $grandpitu=$grandpitu+$riw->pitu;
            $grandlapan=$grandlapan+$riw->lapan;
            $grandsanga=$grandsanga+$riw->sanga;
          }
        }
        if(!$ada){
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "<td align=right>0</td>";
          echo "</tr>";
        }
      }
      echo "<tr><td>Sub Total</td>";
      echo "<td align=right>".number_format($subatu)." - (".number_format($subsiji)." toko)</td>";
      echo "<td align=right>".number_format($subua)." - (".number_format($subloro)." toko)</td>";
      echo "<td align=right>".number_format($subiga)." - (".number_format($subtelu)." toko)</td>";
      echo "<td align=right>".number_format($subampa)." - (".number_format($subpapat)." toko)</td>";
      echo "<td align=right>".number_format($subima)." - (".number_format($sublimo)." toko)</td>";
      echo "<td align=right>".number_format($subanam)." - (".number_format($subanem)." toko)</td>";
      echo "<td align=right>".number_format($subuju)." - (".number_format($subpitu)." toko)</td>";
      echo "<td align=right>".number_format($subapan)." - (".number_format($sublapan)." toko)</td>";
      echo "<td align=right>".number_format($subalan)." - (".number_format($subsanga)." toko)</td>";
      echo "</tr>";

    }
    echo "<tr><td>Grand Total</td>";
    echo "<td align=right>".number_format($grandatu)." - (".number_format($grandsiji)." toko)</td>";
    echo "<td align=right>".number_format($grandua)." - (".number_format($grandloro)." toko)</td>";
    echo "<td align=right>".number_format($grandiga)." - (".number_format($grandtelu)." toko)</td>";
    echo "<td align=right>".number_format($grandampa)." - (".number_format($grandpapat)." toko)</td>";
    echo "<td align=right>".number_format($grandima)." - (".number_format($grandlimo)." toko)</td>";
    echo "<td align=right>".number_format($grandanam)." - (".number_format($grandanem)." toko)</td>";
    echo "<td align=right>".number_format($granduju)." - (".number_format($grandpitu)." toko)</td>";
    echo "<td align=right>".number_format($grandapan)." - (".number_format($grandlapan)." toko)</td>";
    echo "<td align=right>".number_format($grandalan)." - (".number_format($grandsanga)." toko)</td>";
    echo "</tr>";
	}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
