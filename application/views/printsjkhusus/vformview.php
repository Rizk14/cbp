<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'printsjkhusus/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
			<div class="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>"><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
								</tr>
							</thead>
							<th>No SJ</th>
							<th>Customer</th>
							<th>Area</th>
							<th>Print</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$nama	= str_replace("'", "", $row->e_customer_name);
										echo "<tr> 
												<td>$row->i_sj</td>
												<td>$nama</td>
												<td>$row->e_area_name</td>";

										if ($row->f_sj_printed == 't') {
											echo "<td align='center'>Ya</td>";
										} else {
											echo "<td align='center'>Tidak</td>";
										}

										echo "<td class=\"action\">";
										if ($row->f_sj_printed == 'f') {
											echo "<a href=\"javascript:yyy('" . $row->i_sj . "');\"><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/print.gif\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;&nbsp;";
										}

										/* KONDISI KHUSUS CBP KARENA KURANG SDM UNTUK BUKA AKSES CETAK DITAMBAH DI USER NURUL (staff.salesadm2) & FARAH (spv.fin) 21 Jun 2023 */
										if (
											$row->f_sj_printed == 't' && (($this->session->userdata('level') == '5' && $this->session->userdata('departement') == '7') ||
												$this->session->userdata('level') == '0' || ($this->session->userdata('level') == '3') ||
												$this->session->userdata('user_id') == 'staff.salesadm2' || $this->session->userdata('user_id') == 'spv.fin')
										) {
											echo	"	<a href=\"#\" onclick='reprint(\"$folder/cform/undo/$row->i_sj/$row->i_area/$dfrom/$dto/\",\"#main\")' title=\"Cancel Print (Cetak Ulang)\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/no-printer.png\" border=\"0\" alt=\"Cancel Print\">
														</a>";
										}
									}
									echo "<input type=\"hidden\" id=\"ispbprint\" name=\"ispbprint\" value=\"\">
		     ";
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
					</div>
				</div>
			</div>
			<?= form_close() ?>
			<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('printsjkhusus/cform/','#main')">
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function yyy(b, c) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/printsjkhusus/cform/cetak/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function refreshview() {
		show('printsjkhusus/cform/view/<?= $dfrom ?>/<?= $dto ?>', '#main');
	}
</script>