<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Untitled Document</title>
</head>

<body>
  <style type="text/css" media="all">
    /*
@page land {size: landscape;}
*/
    * {
      size: landscape;
    }

    .huruf {
      FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
    }

    .miring {
      font-style: italic;

    }

    .ceKotak {
      - background-color: #f0f0f0;
      border-bottom: #000000 1px solid;
      border-top: #000000 1px solid;
      border-left: #000000 1px solid;
      border-right: #000000 1px solid;
    }

    .marmar {
      padding: 10px 5px 5px 5px;
    }

    .garis {
      background-color: #000000;
      width: 100%;
      height: 50%;
      font-size: 100px;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garis td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .garisx {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: none;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisx td {
      background-color: #FFFFFF;
      border-style: none;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .judul {
      font-size: 20px;
      FONT-WEIGHT: normal;
    }

    .nmper {
      font-size: 18px;
      FONT-WEIGHT: normal;
    }

    .isi {
      font-size: 14px;
      font-weight: normal;
      padding: 1px;
    }

    .eusinya {
      font-size: 10px;
      font-weight: normal;
    }

    .ici {
      font-size: 12px;
      font-weight: normal;
    }

    .garisbawah {
      border-top: #000000 0.1px solid;
    }
  </style>
  <style type="text/css" media="print">
    @page {
      size: 8.5in 11in;
      /* auto is the initial value */
      margin: 0.6cm;
      /* this affects the margin in the printer settings */
    }

    .pagebreak {
      page-break-before: always;
    }

    .noDisplay {
      display: none;
    }
  </style>

  <?php
  foreach ($isi as $row) {
  ?>
    <table width="100%" border="0" class="eusinya">
      <tr>
        <td colspan="3" class="huruf isi"><?php echo NmPerusahaan; ?></td>
        <td width="26">&nbsp;</td>
        <td width="86">&nbsp;</td>
        <td width="36">&nbsp;</td>
        <td width="354" class="huruf isi">KEPADA Yth. </td>
        <td width="87">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3" class="huruf isi"><?php echo AlmtPerusahaan; ?> </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="huruf isi"><?php echo $row->e_customer_name; ?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="53" class="huruf isi">Telp.</td>
        <td width="8" class="huruf isi">:</td>
        <td width="359" class="huruf isi"><?php echo TlpPerusahaan; ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <?php
        if (strlen($row->e_customer_address) < 35) {
        ?>
          <td class="huruf isi"><?php echo $row->e_customer_address; ?></td>
        <?php
        } else {
        ?>
          <td class="huruf isi"><?php echo $row->e_customer_address; ?></td>
        <?php
        }
        ?>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="huruf isi">Fax</td>
        <td class="huruf isi">:</td>
        <td class="huruf isi"><?php echo FaxPerusahaan; ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="huruf isi"><?php echo $row->e_city_name; ?></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td class="huruf isi">NPWP</td>
        <td class="huruf isi">:</td>
        <td class="huruf isi">31.569.482.8-421.000</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td class="huruf isi"><?php echo "Telp. " . $row->e_customer_phone; ?></td>
        <td>&nbsp;</td>
      </tr>
    </table>
    <table width="506" border="0" class="eusinya">
      <tr>
        <td width="28" class="huruf isi">No. </td>
        <td width="140" class="huruf isi">Surat Jalan </td>
        <td width="8" class="huruf isi">:</td>
        <td width="302" class="huruf isi"><?php echo $row->i_sj; ?></td>
      </tr>
      <tr>
        <td class="huruf ici">No. </td>
        <td class="huruf ici">PO</td>
        <td class="huruf ici">:</td>
        <td class="huruf ici"><?php echo $row->i_spb_po; ?></td>
      </tr>
      <tr>
        <td class="huruf ici">No. </td>
        <td class="huruf ici">SPB</td>
        <td class="huruf ici">:</td>
        <td class="huruf ici"><?php echo $row->i_spb; ?></td>
      </tr>

      <tr>
        <td class="huruf ici" colspan="2">Total Jumlah Packing </td>
        <!-- <td class="huruf ici"></td> -->
        <td class="huruf ici">:</td>
        <td class="huruf ici"></td>
      </tr>

      <tr>
        <td colspan=4 class="huruf ici">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" class="huruf ici">Harap diterima barang-barang berikut ini : </td>
      </tr>
    </table>
    <table width="100%" border="0" class="ceKotak huruf ici">
      <tr bordercolor="1">
        <td width="24">No.</td>
        <td width="58">KD-BARANG</td>
        <td width="850" align=center>NAMA BARANG</td>
        <td align=center width="90">UNIT</td>
        <td align=center width="100">HARGA</td>
        <td align=center width="110">JUMLAH</td>
      </tr>
    </table>
    <?php
    $i  = 0;
    $j  = 0;
    $hrg    = 0;
    //$sub    = 0;
    $sj      = $row->i_sj;
    $iarea  = substr($row->i_sj, 8, 2);
    $query   = $this->db->query(" select * from tm_nota_item where i_sj='$sj' and i_area='$iarea'", false);
    $jml   = $query->num_rows();
    $vgross = 0;
    $vdistot  = 0;
    $vtdpp    = 0;
    $vtppn    = 0;
    $vtdisc   = 0;
    foreach ($detail as $rowi) {
      if ($rowi->n_deliver > 0) {
        $i++;
        $j++;
        $group = '';
        $qu  = $this->db->query(" select i_customer_plugroup from tr_customer_plugroup 
                               where i_customer='$row->i_customer'");
        if ($qu->num_rows() > 0) {
          foreach ($qu->result() as $ts) {
            $group = $ts->i_customer_plugroup;
          }
          $qx  = $this->db->query("select i_customer_plu from tr_customer_plu
                                where i_customer_plugroup='$group' and i_product='$rowi->i_product'");
          if ($qx->num_rows() > 0) {
            foreach ($qx->result() as $tx) {
              $plu = $tx->i_customer_plu;
            }
          }
        } else {
          $plu = '';
        }
        $hrg  = $hrg + ($rowi->n_deliver * $rowi->v_unit_price);
        if ($rowi->i_product == '') $rowi->i_product = $rowi->product;
        $pro  = $rowi->i_product;
        if ($plu != '') {
          $pro = $plu;
          if (strlen($pro) > 10) {
            $pro  = substr($pro, 0, 10);
          } else {
            $pro  = $pro . str_repeat(" ", 10 - strlen($pro));
          }
        }
        if (strlen($rowi->e_product_name) > 65) {
          $nam  = substr($rowi->e_product_name, 0, 65);
        } else {
          $nam  = $rowi->e_product_name . str_repeat(" ", 65 - strlen($rowi->e_product_name));
        }
        $del  = number_format($rowi->n_deliver);
        $pjg  = strlen($del);
        $spcdel  = 4;
        for ($xx = 1; $xx <= $pjg; $xx++) {
          $spcdel  = $spcdel - 1;
        }
        if ($row->f_plus_ppn == 't') {
          $pric  = $rowi->v_unit_price;
        } else {
          $pric  = round($rowi->v_unit_price / $rowi->excl_divider);
        }

        $pjg  = strlen($pric);
        $spcpric = 15;
        for ($xx = 1; $xx <= $pjg; $xx++) {
          $spcpric = $spcpric - 1;
        }

        if ($row->f_plus_ppn == 't') {
          $gros  = $rowi->n_deliver * $rowi->v_unit_price;
        } else {
          $gros  = $rowi->v_dpp + round($rowi->v_nota_discount / $rowi->excl_divider);
        }
        $pjg  = strlen(number_format($gros));
        $spctot = 20;
        for ($xx = 1; $xx <= $pjg; $xx++) {
          $spctot  = $spctot - 1;
        }
        $aw  = 3;
        $pjg  = strlen($i);
        for ($xx = 1; $xx <= $pjg; $xx++) {
          $aw = $aw - 1;
        }
        $aw = str_repeat(" ", $aw);
        $vgross  = $vgross + $gros;
        $vtdpp  = $vtdpp + $rowi->v_dpp;
        $vtppn  = $vtppn + $rowi->v_ppn;
        $vtdisc = $vtdisc + round($rowi->v_nota_discount / $rowi->excl_divider);
    ?>
        <table width="98%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="1" width="3%" class="huruf ici" align=right><?php echo $i; ?></td>
            <td height="1" width="12%" class="huruf ici">&nbsp;&nbsp;<?php echo $pro; ?></td>
            <td height="1" width="57%" class="huruf ici">&nbsp;&nbsp;<?php echo $nam; ?></td>
            <td height="1" align=center width="7%" class="huruf ici"><?php echo $del; ?></td>
            <td height="1" align=right width="9%" class="huruf ici"><?php echo number_format($pric, 2, ',', '.'); ?></td>
            <td height="1" align=right width="12%" class="huruf ici"><?php echo number_format($gros, 2, ',', '.'); ?></td>
          </tr>
        </table>
    <?php
      }
    }
    ?>
    <table width="100%" border="0" class="huruf ici">
      <tr>
        <td colspan=6 class="garisbawah">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3">
          <?php if ($row->v_nota_netto > 5000000) { ?>
            <b>CATATAN : Nilai belum termasuk Bea Meterai Rp. 10.000,-</b>
          <?php } ?>
        </td>
        <td width="7%" align=left>TOTAL</td>
        <td width="9%" align=center>:</td>
        <td align=right width="10%"><?php echo number_format($vgross); ?></td>
        <td width="12%">&nbsp;</td>
      </tr>
      <tr>
        <td width="2%">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        <td width="61%">&nbsp;</td>
        <td width="7%" align=left>POTONGAN</td>
        <td width="9%" align=center>:</td>
        <td align=right width="10%"><?php echo number_format($vtdisc); ?></td>
        <td>&nbsp;</td>
      </tr>

      <?php
      if ($row->f_plus_ppn == 'f') {
      ?>
        <tr>
          <td width="2%">&nbsp;</td>
          <td width="9%">&nbsp;</td>
          <td width="61%">&nbsp;</td>
          <td width="7%" align=left>&nbsp;</td>
          <td width="9%" align=center></td>
          <td align=right width="10%">--------------</td>
          <td>-</td>
        </tr>
        <tr>
          <td width="2%">&nbsp;</td>
          <td width="9%">&nbsp;</td>
          <td width="61%">&nbsp;</td>
          <td width="7%" align=left>DPP</td>
          <td width="9%" align=center>:</td>
          <td align=right width="10%"><?php echo number_format(($vtdpp)); ?></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="2%">&nbsp;</td>
          <td width="9%">&nbsp;</td>
          <td width="61%">&nbsp;</td>
          <td width="7%" align=left>PPN (<?= $row->n_tax; ?>%)</td>
          <td width="9%" align=center>:</td>
          <td align=right width="10%"><?php echo number_format($vtppn); ?></td>
          <td>&nbsp;</td>
        </tr>
      <?php
      }
      ?>
      <tr>
        <td width="2%">&nbsp;</td>
        <td width="9%">&nbsp;</td>
        <td width="61%">&nbsp;</td>
        <td width="7%" align=left>&nbsp;</td>
        <td width="9%" align=center></td>
        <td align=right width="10%">--------------</td>
        <td>+</td>
      </tr>
      <?php
      if ($row->f_plus_ppn == 'f') {
      ?>
        <tr>
          <td width="2%">&nbsp;</td>
          <td width="9%">&nbsp;</td>
          <td width="61%">&nbsp;</td>
          <td width="7%" align=left>NILAI</td>
          <td width="9%" align=center>:</td>
          <td align=right width="10%"><?php echo number_format($row->v_nota_netto, 2, ',', '.'); ?></td>
          <td>&nbsp;</td>
        </tr>
    </table>
  <?php
      }
  ?>
  <?php
    $tmp = explode("-", $row->d_sj);
    $th = $tmp[0];
    $bl = $tmp[1];
    $hr = $tmp[2];
    $row->d_sj = $hr . " " . substr(mbulan($bl), 0, 3) . " " . $th;
  ?>
  <table width="100%" border="0">
    <tr>
      <td colspan="2" align=right width="20%" class="huruf ici">Bandung, <?php echo $row->d_sj; ?></td>
    </tr>
  </table>
  <table width="100%" border="0">
    <tr>
      <td colspan="2" align=center class="huruf ici">Penerima</td>
      <td colspan="2" align=center class="huruf ici">Mengetahui</td>
      <td colspan="2" align=center class="huruf ici">Pengirim</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align=center class="huruf ici">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align=center class="huruf ici">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
    </tr>
  </table>
  <br>

  <div class="ceKotak marmar">
    <table width="30%" border="0" align="center" class="ceKotak">
      <tr>
        <td>
          <div align="center" class="huruf ici">S U R A T J A L A N</div>
        </td>
      </tr>
      <tr>
        <td>
          <div align="center" class="huruf ici">Bukan Untuk Tagihan</div>
        </td>
      </tr>
    </table>
    <tr>
      <td>
        <div align="left" class="huruf ici">Perhatian !</div>
      </td>
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici"> 1. Barang yang sudah diterima tidak dapat dikembalikan, kecuali cacat produksi.</div>
      </td>
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici">2. Tidak berlaku claim kekurangan tolakan barang setelah barang diterima.</div>
      </td>
    </tr>
    <tr>
      <!-- <td><div align="left" class="huruf ici">(BCA-CIMAHI) NO.REK. 139.300.1236 A/N PT.DIALOGUE GARMINDO UTAMA</div></td> -->
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici">3. Penerima wajib ttd / cap toko.</div>
      </td>
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici">4. PEMBAYARAN DAPAT DI TRANSFER KE: (BCA-CIMAHI) NO.REK. 7771881819 A/N PT.Chintaka Bumi Pertiwi</div>
      </td>
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici">5. Mohon untuk konfirmasi apabila sudah melakukan pembayaran ke bagian keuangan pusat</div>
      </td>
    </tr>
    <tr>
      <td>
        <div align="left" class="huruf ici">HP (<?php echo $row->e_area_phone; ?>).</div>
      </td>
    </tr>
    <!-- <tr>
      <td><div align="center" class="huruf ici">TERIMA KASIH ATAS KERJASAMANYA</div></td>
    </tr> -->
  </div>

  <!--   <table width="90%" border="0" align="center" class="ceKotak">
    <tr>
      <td><div align="center" class="huruf ici">P E N T I N G</div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici"> TIDAK BERLAKU CLAIM KEKURANGAN/TOLAKAN BARANG SETELAH BRG DITERIMA </div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici">PENERIMA WAJIB TTD&/CAP TOKO,       PEMBAYARAN DAPAT DI TRANSFER KE:</div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici">BCA-CIMAHI NO.REK. 139.300.1236 A/N PT.DIALOGUE GARMINDO UTAMA</div></td>
      <td><div align="center" class="huruf ici">BCA-CIMAHI NO.REK. 7771881819 A/N PT.Chintaka Bumi Pertiwi</div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici">MOHON UNTUK KONFIRMASI APABILA SUDAH MELAKUKAN PEMBAYARAN KE :</div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici">BAGIAN KEUANGAN PUSAT/CABANG  : (<?php echo $row->e_area_phone; ?>)</div></td>
    </tr>
    <tr>
      <td><div align="center" class="huruf ici">TERIMA KASIH ATAS KERJASAMANYA</div></td>
    </tr>
  </table> -->
<?php
  }
?>
<div class="noDisplay">
  <center><b><a href="#" onClick="window.print()">Print</a></b></center>
</div>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.3.2.js"></script>
<script>
  window.onafterprint = function() {
    var isj = '<?php echo $isj ?>';
    $.ajax({
      type: "POST",
      url: "<?php echo site_url('printsjkhusus/cform/update'); ?>",
      data: "isj=" + isj,
      success: function(data) {
        opener.window.refreshview();
        setTimeout(window.close, 0);
      },
      error: function(XMLHttpRequest) {
        alert('fail');
      }
    });
  }
</script>