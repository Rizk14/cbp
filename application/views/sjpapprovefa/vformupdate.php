<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="spbform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
              <tr>
                <td width="10%">Tgl SJ</td>
                <?php

                if ($isi->d_sjp) {
                  $tmp = explode("-", $isi->d_sjp);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_sjp = $hr . "-" . $bl . "-" . $th;
                }

                if ($isi->d_spmb) {
                  $tmp = explode("-", $isi->d_spmb);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_spmb = $hr . "-" . $bl . "-" . $th;
                }

                if ($isi->d_sjp_receive) {
                  $tmp = explode("-", $isi->d_sjp_receive);
                  $hr = $tmp[2];
                  $bl = $tmp[1];
                  $th = $tmp[0];
                  $isi->d_sjp_receive = $hr . "-" . $bl . "-" . $th;
                }

                $i_segel_pulangkirim = null;

                // if ($isi->i_bapb) {
                //   if ($isi->i_bapb != '' || $isi->i_bapb != null) {
                //     $bapb = $isi->i_bapb;
                //     $areaa = $isi->i_area;
                //     $data_bapb = $this->db->query("select * from tm_bapbsjp where i_bapb = '$bapb' and i_area = '$areaa'");

                //     if ($data_bapb->num_rows() > 0) {
                //       $data_bapb = $data_bapb->row();

                //       if ($data_bapb->i_segel_pulangkirim != '' || $data_bapb->i_segel_pulangkirim != null) {
                //         $i_segel_pulangkirim = $data_bapb->i_segel_pulangkirim;
                //       }
                //     }
                //   }
                // }
                ?>
                <td>
                  <input readonly id="dsj" name="dsj" value="<?php if ($isi->d_sjp) echo $isi->d_sjp; ?>">
                  <input readonly id="isj" name="isj" value="<?php if ($isi->i_sjp) echo $isi->i_sjp; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">Area</td>
                <td>
                  <input readonly id="eareaname" name="eareaname" value="<?php if ($isi->e_area_name) echo $isi->e_area_name; ?>">
                  <input id="iarea" name="iarea" type="hidden" value="<?php if ($isi->i_area) echo $isi->i_area; ?>">
                  <input id="istore" name="istore" type="hidden" value="<?php if ($isi->i_store) echo $isi->i_store; ?>">
                  <input id="i_bapb" name="i_bapb" type="hidden" value="<?php if ($isi->i_bapb) echo $isi->i_bapb; ?>">
                  <input id="i_segel_pulangkirim" name="i_segel_pulangkirim" type="hidden" value="<?php if ($i_segel_pulangkirim) echo $i_segel_pulangkirim; ?>">
                  <input id="istorelocation" name="istorelocation" type="hidden" value="<?php if ($isi->i_store_location) echo $isi->i_store_location; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">SPmB</td>
                <td>
                  <input readonly id="ispmb" name="ispmb" value="<?php if ($isi->i_spmb) echo $isi->i_spmb; ?>">
                  <input readonly id="dspmb" name="dspmb" type="text" value="<?php if ($isi->d_spmb) echo $isi->d_spmb; ?>">
                </td>
              </tr>

              <!-- <tr>
                <td width="10%">SJ Lama</td>
                <td> -->
              <input readonly id="isjold" name="isjold" type="hidden" value="<?php echo $isi->i_sjp_old; ?>">
              <!-- </td>
              </tr> -->

              <tr>
                <td width="10%">Tgl Approve</td>
                <td>
                  <input readonly id="dapprovefa" name="dapprovefa" type="text" value="" onclick="showCalendar('',this,this,'','dsjp',0,20,1)">
                </td>
                <input hidden id="dreceive" name="dreceive" type="text" value="<?php echo $isi->d_sjp_receive; ?>" readonly>
                <input hidden id="dapprovefax" name="dapprovefax" type="text" value="<?php echo date('d-m-Y'); ?>">
                <input hidden id="ntoleransi" name="ntoleransi" type="text" value="<?php echo $isi->n_tolerance; ?>">
                <input hidden id="dreceivemin" name="dreceivemin" type="text" value="">
              </tr>

              <tr>
                <td width="10%">Nilai Kirim</td>
                <td>
                  <input readonly style="text-align:right;" readonly id="vsj" name="vsj" value="<?php if ($isi->v_sjp >= 0) {
                                                                                                  echo number_format((float)$isi->v_sjp);
                                                                                                }; ?>">
                  <input type="hidden" name="jml" id="jml" value="<?php if ($jmlitem) echo $jmlitem; ?>">
                </td>
              </tr>

              <tr>
                <td width="10%">Nilai Terima</td>
                <td>
                  <input readonly style="text-align:right;" readonly id="vsjrec" name="vsjrec" value="<?php if ($isi->v_sjp > 0) {
                                                                                                        echo number_format($isi->v_sjp);
                                                                                                      }; ?>">
                </td>
              </tr>

              <tr <?php if ($i_segel_pulangkirim == null) { ?>style="display: none;" <?php } ?>>
                <td width="10%">No Segel Pulang</td>
                <td><input id="i_segel_pulangkirimx" name="i_segel_pulangkirimx" value=""></td>
              </tr>

              <tr>
                <td width="100%" align="center" colspan="4">
                  <input name="login" id="login" value="Approve" type="submit" onclick="return dipales()">
                  <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick='show("<?= $folder ?>/cform/index/","#main")'>
                </td>
              </tr>
            </table>

            <div id="detailheader" align="center">
              <?php
              if ($detail) {
              ?>
                <table class="listtable" align="center" style="width: 750px;">
                  <th style="width:25px;" align="center">No</th>
                  <th style="width:63px;" align="center">Kode</th>
                  <th style="width:300px;" align="center">Nama Barang</th>
                  <th style="width:100px;" align="center">Ket</th>
                  <th style="width:73px;" align="center">Jml Krm</th>
                  <th style="width:73px;" align="center">Jml Trm</th>
                  <th style="width:32px;" align="center" class="action">Action</th>
                </table>
              <?php
              }
              ?>
            </div>
            <div id="detailisi" align="center">
              <?php
              if ($detail) {
                $i = 0;
                foreach ($detail as $row) {
                  $jmlterima = $row->n_quantity_deliver;
                  #              if($row->n_quantity_receive=='' || $row->n_quantity_receive==0) $row->n_quantity_receive=$row->n_quantity_deliver;
                  if ($row->n_quantity_receive == '' || $row->n_quantity_receive == 0) $jmlterima = $row->n_quantity_deliver;
                  if ($row->n_quantity_receive == '') $row->n_quantity_receive == 0;
                  $vtotal = $row->v_unit_price * $row->n_quantity_deliver;
                  echo '<table class="listtable" align="center" style="width:750px;">';
                  $i++;
                  echo "<tbody>
                          <tr>
                              <td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\">
                                <input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\">
                              </td>
                              <td style=\"width:66px;\">
                                <input style=\"width:66px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\">
                              </td>
                              <td style=\"width:314px;\">
                                <input style=\"width:314px;\" readonly type=\"text\" id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\">
                                <input type=\"hidden\" id=\"emotifname$i\" name=\"emotifname$i\" value=\"$row->e_product_motifname\">
                              </td>
                              <td style=\"width:103px;\">
                                <input style=\"width:103px;\" type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\">
                                <input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$row->v_unit_price\">
                              </td>
                              <td style=\"width:74px;\">
                                <input readonly style=\"text-align:right; width:74px;\" type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_quantity_deliver\">
                                <input type=\"hidden\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_quantity_order\">
                              </td>
                              <td style=\"width:74px;\">
                                <input readonly style=\"text-align:right; width:74px;\" type=\"text\" id=\"nreceive$i\" name=\"nreceive$i\" value=\"$jmlterima\" onkeyup=\"hitungnilai(" . $i . ")\" autocomplete=\"off\"><input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_quantity_receive\">
                                <input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$vtotal\">
                              </td>
                              <td style=\"width:74px;\"></td>
                            </tr>
                          </tbody>
                        </table>";
                }
              }
              ?>
            </div>
            <div id="pesan"></div>
          </div>
        </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
  function dipales() {
    if ((document.getElementById("dapprovefa").value != '')) {
      setTimeout(() => {
        $("input").attr("disabled", true);
        $("select").attr("disabled", true);
        $("#submit").attr("disabled", true);
        document.getElementById("cmdreset").removeAttribute('disabled');
      }, 100);
    } else {
      alert('Harap isi tanggal Approvess !!!');
      return false;
    }
  }

  function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
  }

  function cekminimalterima() {
    isjp = document.getElementById('isj').value;
    dsjp = document.getElementById('dsj').value;
    iarea = document.getElementById('iarea').value;
    dreceive = document.getElementById('dreceive').value;
    dapprovefa = document.getElementById('dapprovefa').value;
    fretur = '<?= $isi->f_retur ?>';

    $.ajax({
      type: "POST",
      url: "<?php echo site_url($folder . '/cform/hitungminimalterima'); ?>",
      data: {
        'isjp': isjp,
        'dsjp': dsjp,
        'iarea': iarea,
        'fretur': fretur,
        'dapprovefa': dreceive,
      },
      success: function(data) {
        cektanggal(data);
      },

      error: function(XMLHttpRequest) {
        alert(XMLHttpRequest.responseText);
      }
    })
  }

  function cektanggal(a) {
    dsj = document.getElementById('dsj').value;
    dreceive = document.getElementById('dreceive').value;
    dapprovefa = document.getElementById('dapprovefa').value;
    dapprovefax = document.getElementById('dapprovefax').value;
    document.getElementById('dreceivemin').value = a;

    /* VALIDASI TANGGAL TERIMA < TANGGAL HARI INI */
    dtmp = dsj.split('-');
    thnsj = dtmp[2];
    blnsj = dtmp[1];
    hrsj = dtmp[0];

    /* SPLIT TGL APPROVE FM */
    dtmpfa = dapprovefa.split('-');
    thnfa = dtmpfa[2];
    blnfa = dtmpfa[1];
    hrfa = dtmpfa[0];

    if (dreceive != '') {
      dtmp = dreceive.split('-');
      thnrec = dtmp[2];
      blnrec = dtmp[1];
      hrrec = dtmp[0];

      if (thnrec > thnfa) {
        alert('Tanggal Approve tidak boleh lebih kecil dari tanggal terima SJP !!!');
        document.getElementById('dapprovefa').value = '';
      } else if (thnrec == thnfa) {
        if (blnrec > blnfa) {
          alert('Tanggal Approve tidak boleh lebih kecil dari tanggal terima SJP !!!');
          document.getElementById('dapprovefa').value = '';
        } else if (blnrec == blnfa) {
          if (hrrec > hrfa) {
            alert('Tanggal Approve tidak boleh lebih kecil dari tanggal terima SJP !!!');
            document.getElementById('dapprovefa').value = '';
          }
        }
      }
    } else {
      if (thnsj > thnfa) {
        alert('Tanggal Approve tidak boleh lebih kecil dari tanggal SJP !!!');
        document.getElementById('dapprovefa').value = '';
      } else if (thnsj == thnfa) {
        if (blnsj > blnfa) {
          alert('Tanggal Approve tidak boleh lebih kecil dari tanggal SJP !!!');
          document.getElementById('dapprovefa').value = '';
        } else if (blnsj == blnfa) {
          if (hrsj > hrfa) {
            alert('Tanggal Approve tidak boleh lebih kecil dari tanggal SJP !!!');
            document.getElementById('dapprovefa').value = '';
          }
        }
      }
    }
  }
</script>