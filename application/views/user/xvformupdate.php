<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'user/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="userform">
	  <div class="effect">
	    <div class="accordion2">
	      <table class="mastertable" width="100%" border=0>
	        <tr>
			  <td class="bat" width="50px">User ID</td>
			  <td class="bat" width="2px">:</td>
			  <td><?php 
				$data = array(
			              'name'        => 'iuser',
			              'id'          => 'iuser',
			              'value'       => $isi->i_user,
			              'maxlength'   => '30');
				echo form_input($data);?></td>
			  <td class="bat" width="50px">Area 1</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea1" name="earea1" readonly value="<?php echo $eareaname1; ?>" onclick="view_area('1')">
				  <input type="hidden" id="area1" name="area1" value="<?php echo $isi->i_area1?>"></td>
	      	</tr>
      	  	<tr>
			  <td class="bat" width="50px">User Name</td>
			  <td class="bat" width="2px">:</td>
			  <td><?php 
				$data = array(
			              'name'        => 'eusername',
			              'id'          => 'eusername',
			              'value'       => $isi->e_user_name,
			              'maxlength'   => '30');
				echo form_input($data);?></td>
			  <td class="bat" width="50px">Area 2</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea2" name="earea2" value="<?php echo $eareaname2; ?>" 
				   readonly onclick="view_area('2')">
				<input type="hidden" id="area2" name="area2" value="<?php echo $isi->i_area2?>"></td>
	      	</tr>
	      	<tr>
			  <td class="bat" width="50px">Aktif</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="checkbox" id="faktif" name="faktif" <?php if($isi->f_aktif=='t') echo "checked"; ?>></td>
			  <td class="bat" width="50px">Area 3</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea3" name="earea3" readonly value="<?php echo $eareaname3; ?>"
				   onclick="view_area('3')">
				<input type="hidden" id="area3" name="area3" value="<?php echo $isi->i_area3?>"></td>
	      	</tr>
	      	<tr>
			  <td class="bat" width="50px">Departement</td>
			  <td class="bat" width="2px">:</td>
			  <td><select id="idepartment" name="idepartment">
					<option value='0' <?php if($isi->i_departement=='0') echo 'selected';?>>M I S</option>
					<option value='1' <?php if($isi->i_departement=='1') echo 'selected';?>>Internal Audit</option>
					<option value='2' <?php if($isi->i_departement=='2') echo 'selected';?>>Personalia</option>
					<option value='3' <?php if($isi->i_departement=='3') echo 'selected';?>>Sales</option>
					<option value='4' <?php if($isi->i_departement=='4') echo 'selected';?>>Keuangan</option>
				</select></td>
			  <td class="bat" width="50px">Area 4</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea4" name="earea4" readonly value="<?php echo $eareaname4; ?>" 
				   onclick="view_area('4')">
				<input type="hidden" id="area4" name="area4" value="<?php echo $isi->i_area4?>"></td>
	      	</tr>
	      	<tr>
		  	  <td class="bat" width="50px">Jabatan</td>
			  <td class="bat" width="2px">:</td>
			  <td><select id="ilevel" name="ilevel">
					<option value='0' <?php if($isi->i_level=='0') echo 'selected';?>>Administrator</option>
					<option value='1' <?php if($isi->i_level=='1') echo 'selected';?>>C E O</option>
					<option value='2' <?php if($isi->i_level=='2') echo 'selected';?>>Direktur</option>
					<option value='3' <?php if($isi->i_level=='3') echo 'selected';?>>General Manager</option>
					<option value='4' <?php if($isi->i_level=='4') echo 'selected';?>>Manager</option>
					<option value='5' <?php if($isi->i_level=='5') echo 'selected';?>>Supervisor</option>
					<option value='6' <?php if($isi->i_level=='6') echo 'selected';?>>User</option>
					<option value='7' <?php if($isi->i_level=='7') echo 'selected';?>>Guest</option>
				</select></td>
			  <td class="bat" width="50px">Area 5</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea5" name="earea5" readonly value="<?php echo $eareaname5; ?>" 
				   onclick="view_area('5')">
				<input type="hidden" id="area5" name="area5" value="<?php echo $isi->i_area5?>"></td>
	      	</tr>
	      	<tr>
			  <td width="100%" colspan="6">
				<table width="100%" border=0 cellspacing=0 cellpadding=0>
			  	  <tr>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
			  	  </tr>
<!-- Master -->
			  	  <tr>
					<td class="bat" width="50px">Master</td>
					<td class="bat" width="2px">:</td>
					<td class="batas bat" align="right">Grp Pemasok
						<input type="checkbox" id="fmenu1" name="fmenu1" <?php if($isi->f_menu1=='t') echo "checked"; ?> onclick="cek('1')"></td>
					<td class="batas bat" align="right">Pemasok
						<input type="checkbox" id="fmenu2" name="fmenu2" <?php if($isi->f_menu2=='t') echo "checked"; ?> onclick="cek('2')"></td>
                    <td class="batas bat" align="right">Pelanggan
						<input type="checkbox" id="fmenu220" name="fmenu220" <?php if($isi->f_menu220=='t') echo "checked"; ?> onclick="cek('220')"></td>
                    <td class="batas bat" align="right">Harga HJP
						<input type="checkbox" id="fmenu225" name="fmenu225" <?php if($isi->f_menu225=='t') echo "checked"; ?> onclick="cek('225')"></td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
			  	  </tr>
			  	  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Group Brg
						<input type="checkbox" id="fmenu9" name="fmenu9" <?php if($isi->f_menu9=='t') echo "checked"; ?> onclick="cek('9')"></td>
					<td class="bat" align="right">Status Brg
						<input type="checkbox" id="fmenu10" name="fmenu10" <?php if($isi->f_menu10=='t') echo "checked"; ?> onclick="cek('10')"></td>
					<td class="bat" align="right">Kelas Brg
						<input type="checkbox" id="fmenu11" name="fmenu11" <?php if($isi->f_menu11=='t') echo "checked"; ?> onclick="cek('11')"></td>
					<td class="bat" align="right">Kat Brg
						<input type="checkbox" id="fmenu12" name="fmenu12" <?php if($isi->f_menu12=='t') echo "checked"; ?> onclick="cek('12')"></td>
					<td class="bat" align="right">Jenis Brg
						<input type="checkbox" id="fmenu13" name="fmenu13" <?php if($isi->f_menu13=='t') echo "checked"; ?> onclick="cek('13')"></td>
					<td class="bat" align="right">Barang
						<input type="checkbox" id="fmenu14" name="fmenu14" <?php if($isi->f_menu14=='t') echo "checked"; ?> onclick="cek('14')"></td>
					<td class="bat" align="right">Motif Brg
						<input type="checkbox" id="fmenu15" name="fmenu15" <?php if($isi->f_menu15=='t') echo "checked"; ?> onclick="cek('15')"></td>
			  	  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">Harga Brg
						<input type="checkbox" id="fmenu17" name="fmenu17" <?php if($isi->f_menu17=='t') echo "checked"; ?> onclick="cek('17')"></td>
					<td class="batas bat" align="right">Grade Brg
						<input type="checkbox" id="fmenu16" name="fmenu16" <?php if($isi->f_menu16=='t') echo "checked"; ?> onclick="cek('16')"></td>
					<td class="batas bat" align="right">Klp Hrg
						<input type="checkbox" id="fmenu28" name="fmenu28" <?php if($isi->f_menu28=='t') echo "checked"; ?> onclick="cek('28')"></td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Gudang
						<input type="checkbox" id="fmenu3" name="fmenu3" <?php if($isi->f_menu3=='t') echo "checked"; ?> onclick="cek('3')"></td>
					<td class="bat" align="right">Lokasi
						<input type="checkbox" id="fmenu4" name="fmenu4" <?php if($isi->f_menu4=='t') echo "checked"; ?> onclick="cek('4')"></td>
					<td class="bat" align="right">Jns BBK
						<input type="checkbox" id="fmenu5" name="fmenu5" <?php if($isi->f_menu5=='t') echo "checked"; ?> onclick="cek('5')"></td>
					<td class="bat" align="right">Jns BBM
						<input type="checkbox" id="fmenu6" name="fmenu6" <?php if($isi->f_menu6=='t') echo "checked"; ?> onclick="cek('6')"></td>
				  </tr>
                  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
				    <td class="batas bat" align="right">Upload SO
					    <input type="checkbox" id="fmenu223" name="fmenu223" <?php if($isi->f_menu223=='t') echo "checked"; ?> onclick="cek('2237')"></td>
				    <td class="batas bat" align="right">Transfer SO
					    <input type="checkbox" id="fmenu224" name="fmenu224" <?php if($isi->f_menu224=='t') echo "checked"; ?> onclick="cek('224')"></td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
                  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Jenis Area
						<input type="checkbox" id="fmenu18" name="fmenu18" <?php if($isi->f_menu18=='t') echo "checked"; ?> onclick="cek('18')"></td>
					<td class="bat" align="right">Area
						<input type="checkbox" id="fmenu19" name="fmenu19" <?php if($isi->f_menu19=='t') echo "checked"; ?> onclick="cek('19')"></td>
					<td class="bat" align="right">Salesman
						<input type="checkbox" id="fmenu20" name="fmenu20" <?php if($isi->f_menu20=='t') echo "checked"; ?> onclick="cek('20')"></td>
					<td class="bat" align="right">Negara
						<input type="checkbox" id="fmenu21" name="fmenu21" <?php if($isi->f_menu21=='t') echo "checked"; ?> onclick="cek('21')"></td>
					<td class="bat" align="right">Teritori
						<input type="checkbox" id="fmenu22" name="fmenu22" <?php if($isi->f_menu22=='t') echo "checked"; ?> onclick="cek('22')"></td>
					<td class="bat" align="right">Jns Kota
						<input type="checkbox" id="fmenu23" name="fmenu23" <?php if($isi->f_menu23=='t') echo "checked"; ?> onclick="cek('23')"></td>
					<td class="bat" align="right">Stat Kota
						<input type="checkbox" id="fmenu24" name="fmenu24" <?php if($isi->f_menu24=='t') echo "checked"; ?> onclick="cek('24')"></td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="batas" align="right">Jns Kota/Area
						<input type="checkbox" id="fmenu25" name="fmenu25" <?php if($isi->f_menu25=='t') echo "checked"; ?> onclick="cek('25')"></td>
					<td class="batas" align="right">Group Kota
						<input type="checkbox" id="fmenu26" name="fmenu26" <?php if($isi->f_menu26=='t') echo "checked"; ?> onclick="cek('26')"></td>
					<td class="batas" align="right">Kota
						<input type="checkbox" id="fmenu27" name="fmenu27" <?php if($isi->f_menu27=='t') echo "checked"; ?> onclick="cek('27')"></td>
					<td class="batas" align="right">Area Plg
						<input type="checkbox" id="fmenu29" name="fmenu29" <?php if($isi->f_menu29=='t') echo "checked"; ?> onclick="cek('29')"></td>
					<td class="batas" align="right">Slsman / Plg
						<input type="checkbox" id="fmenu30" name="fmenu30" <?php if($isi->f_menu30=='t') echo "checked"; ?> onclick="cek('30')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
				  </tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Layanan
						<input type="checkbox" id="fmenu31" name="fmenu31" <?php if($isi->f_menu31=='t') echo "checked"; ?> onclick="cek('31')"></td>
					<td class="bat" align="right">Ukuran
						<input type="checkbox" id="fmenu32" name="fmenu32" <?php if($isi->f_menu32=='t') echo "checked"; ?> onclick="cek('32')"></td>
					<td class="bat" align="right">Cara Jualan
						<input type="checkbox" id="fmenu33" name="fmenu33" <?php if($isi->f_menu33=='t') echo "checked"; ?> onclick="cek('33')"></td>
					<td class="bat" align="right">Kelas Plg
						<input type="checkbox" id="fmenu34" name="fmenu34" <?php if($isi->f_menu34=='t') echo "checked"; ?> onclick="cek('34')"></td>
					<td class="bat" align="right">Tipe Produk
						<input type="checkbox" id="fmenu35" name="fmenu35" <?php if($isi->f_menu35=='t') echo "checked"; ?> onclick="cek('35')"></td>
					<td class="bat" align="right">Produk Khusus
						<input type="checkbox" id="fmenu36" name="fmenu36" <?php if($isi->f_menu36=='t') echo "checked"; ?> onclick="cek('36')"></td>
					<td class="bat" align="right">Group Plg
						<input type="checkbox" id="fmenu37" name="fmenu37" <?php if($isi->f_menu37=='t') echo "checked"; ?> onclick="cek('37')"></td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Bank
						<input type="checkbox" id="fmenu38" name="fmenu38" <?php if($isi->f_menu38=='t') echo "checked"; ?> onclick="cek('38')"></td>
					<td class="bat" align="right">Group PLU
						<input type="checkbox" id="fmenu39" name="fmenu39" <?php if($isi->f_menu39=='t') echo "checked"; ?> onclick="cek('39')"></td>
					<td class="bat" align="right">PLU
						<input type="checkbox" id="fmenu40" name="fmenu40" <?php if($isi->f_menu40=='t') echo "checked"; ?> onclick="cek('40')"></td>
					<td class="bat" align="right">Pemilik
						<input type="checkbox" id="fmenu41" name="fmenu41" <?php if($isi->f_menu41=='t') echo "checked"; ?> onclick="cek('41')"></td>
					<td class="bat" align="right">PKP
						<input type="checkbox" id="fmenu42" name="fmenu42" <?php if($isi->f_menu42=='t') echo "checked"; ?> onclick="cek('42')"></td>
					<td class="bat" align="right">Discount
						<input type="checkbox" id="fmenu43" name="fmenu43" <?php if($isi->f_menu43=='t') echo "checked"; ?> onclick="cek('43')"></td>
					<td class="bat" align="right">Status
						<input type="checkbox" id="fmenu44" name="fmenu44" <?php if($isi->f_menu44=='t') echo "checked"; ?> onclick="cek('44')"></td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">Pelanggan
						<input type="checkbox" id="fmenu45" name="fmenu45" <?php if($isi->f_menu45=='t') echo "checked"; ?> onclick="cek('45')"></td>
					<td class="batas bat" align="right">Appr Plg
						<input type="checkbox" id="fmenu46" name="fmenu46" <?php if($isi->f_menu46=='t') echo "checked"; ?> onclick="cek('46')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>					
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">Jenis Promo
						<input type="checkbox" id="fmenu88" name="fmenu88" <?php if($isi->f_menu88=='t') echo "checked"; ?> onclick="cek('88')"></td>
					<td class="batas bat" align="right">Status OP
						<input type="checkbox" id="fmenu53" name="fmenu53" <?php if($isi->f_menu53=='t') echo "checked"; ?> onclick="cek('53')"></td>
					<td class="batas bat" align="right">Kendaraan
						<input type="checkbox" id="fmenu144" name="fmenu144" <?php if($isi->f_menu144=='t') echo "checked"; ?> onclick="cek('144')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>					
				  </tr>
<!-- End Master -->
<!-- Gudang -->
			  	  <tr>
					<td class="bat" width="50px">Gudang</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">SO
						<input type="checkbox" id="fmenu7" name="fmenu7" <?php if($isi->f_menu7=='t') echo "checked"; ?> onclick="cek('7')"></td>
					<td class="bat" align="right">KS
						<input type="checkbox" id="fmenu75" name="fmenu75" <?php if($isi->f_menu75=='t') echo "checked"; ?> onclick="cek('75')"></td>
					<td class="bat" align="right">BBM Ret
						<input type="checkbox" id="fmenu109" name="fmenu109" <?php if($isi->f_menu109=='t') echo "checked"; ?> onclick="cek('109')"></td>
					<td class="bat" align="right">SJ Pinjaman
						<input type="checkbox" id="fmenu156" name="fmenu156" <?php if($isi->f_menu156=='t') echo "checked"; ?> onclick="cek('156')"></td>
					<td class="bat" align="right">Real SPB
						<input type="checkbox" id="fmenu50" name="fmenu50" <?php if($isi->f_menu50=='t') echo "checked"; ?> onclick="cek('50')"></td>
					<td class="bat" align="right">Pemnh SPB
						<input type="checkbox" id="fmenu51" name="fmenu51" <?php if($isi->f_menu51=='t') echo "checked"; ?> onclick="cek('51')"></td>
			  	  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Siap Nota gdg
						<input type="checkbox" id="fmenu87" name="fmenu87" <?php if($isi->f_menu87=='t') echo "checked"; ?> onclick="cek('87')"></td>
					<td class="bat" align="right">SPmB
						<input type="checkbox" id="fmenu8" name="fmenu8" <?php if($isi->f_menu8=='t') echo "checked"; ?> onclick="cek('8')"></td>
					<td class="bat" align="right">Real SPmB
						<input type="checkbox" id="fmenu73" name="fmenu73" <?php if($isi->f_menu73=='t') echo "checked"; ?> onclick="cek('73')"></td>
					<td class="bat" align="right">Pemnh SPmB
						<input type="checkbox" id="fmenu74" name="fmenu74" <?php if($isi->f_menu74=='t') echo "checked"; ?> onclick="cek('74')"></td>
					<td class="bat" align="right">TTB Tolakan
						<input type="checkbox" id="fmenu79" name="fmenu79" <?php if($isi->f_menu79=='t') echo "checked"; ?> onclick="cek('79')"></td>
					<td class="bat" align="right">TTB Retur
						<input type="checkbox" id="fmenu84" name="fmenu84" <?php if($isi->f_menu84=='t') echo "checked"; ?> onclick="cek('84')"></td>
					<td class="bat" align="right">DKB
						<input type="checkbox" id="fmenu160" name="fmenu160" <?php if($isi->f_menu160=='t') echo "checked"; ?> onclick="cek('160')"></td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">Surat Jalan
						<input type="checkbox" id="fmenu86" name="fmenu86" <?php if($isi->f_menu86=='t') echo "checked"; ?> onclick="cek('86')"></td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
				  </tr>
<!-- End Gudang -->
<!-- Penjualan -->
				  <tr>
					<td class="bat" width="50px">Penjualan</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">SPB Baby
						<input type="checkbox" id="fmenu47" name="fmenu47" <?php if($isi->f_menu47=='t') echo "checked"; ?> onclick="cek('47')"></td>
					<td class="bat" align="right">SPB Promo Baby
						<input type="checkbox" id="fmenu91" name="fmenu91" <?php if($isi->f_menu91=='t') echo "checked"; ?> onclick="cek('91')"></td>
					<td class="bat" align="right">SPB Appr Sls
						<input type="checkbox" id="fmenu48" name="fmenu48" <?php if($isi->f_menu48=='t') echo "checked"; ?> onclick="cek('48')"></td>
					<td class="bat" align="right">Target Penjualan
						<input type="checkbox" id="fmenu97" name="fmenu97" 
						<?php if($isi->f_menu97=='t') echo "checked"; ?> onclick="cek('97')"></td>
					<td class="bat" align="right">Siap Nota sls
						<input type="checkbox" id="fmenu103" name="fmenu103"
						<?php if($isi->f_menu103=='t') echo "checked"; ?> onclick="cek('103')"></td>
					<td class="bat" align="right">Promo
						<input type="checkbox" id="fmenu89" name="fmenu89" <?php if($isi->f_menu89=='t') echo "checked"; ?> onclick="cek('89')"></td>
					<td class="bat">&nbsp;</td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">SPmB Appr Sls
						<input type="checkbox" id="fmenu71" name="fmenu71" <?php if($isi->f_menu71=='t') echo "checked"; ?> onclick="cek('71')"></td>
					<td class="batas bat" align="right">SPB Reg
						<input type="checkbox" id="fmenu159" name="fmenu159" <?php if($isi->f_menu159=='t') echo "checked"; ?> onclick="cek('159')"></td>
					<td class="batas bat" align="right">SPB Promo Reg
						<input type="checkbox" id="fmenu91" name="fmenu161" <?php if($isi->f_menu161=='t') echo "checked"; ?> onclick="cek('161')"></td>
				    <td class="bat" align="right">Transfer SPB Lama
					    <input type="checkbox" id="fmenu218" name="fmenu218" <?php if($isi->f_menu218=='t') echo "checked"; ?> onclick="cek('218')"></td>
				    <td class="bat" align="right">Transfer SPB Baru
					    <input type="checkbox" id="fmenu219" name="fmenu219" <?php if($isi->f_menu219=='t') echo "checked"; ?> onclick="cek('219')"></td>
				    <td class="bat" align="right">Upload SPB
					    <input type="checkbox" id="fmenu221" name="fmenu221" <?php if($isi->f_menu221=='t') echo "checked"; ?> onclick="cek('221')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
				  </tr>

<!-- End Penjualan -->
<!-- Pembelian -->
				  <tr>
					<td class="batas bat" width="50px">Pembelian</td>
					<td class="batas bat" width="2px">:</td>
					<td class="batas bat" align="right">Create OP
						<input type="checkbox" id="fmenu52" name="fmenu52" <?php if($isi->f_menu52=='t') echo "checked"; ?> onclick="cek('52')"></td>
					<td class="batas bat" align="right">Closing OP
						<input type="checkbox" id="fmenu54" name="fmenu54" <?php if($isi->f_menu54=='t') echo "checked"; ?> onclick="cek('54')"></td>
					<td class="batas bat" align="right">Cancel OP
						<input type="checkbox" id="fmenu55" name="fmenu55" <?php if($isi->f_menu55=='t') echo "checked"; ?> onclick="cek('55')"></td>
					<td class="batas bat" align="right">Transfer DO
						<input type="checkbox" id="fmenu65" name="fmenu65" <?php if($isi->f_menu65=='t') echo "checked"; ?> onclick="cek('65')"></td>
					<td class="batas bat" align="right">Upload DO
						<input type="checkbox" id="fmenu66" name="fmenu66" <?php if($isi->f_menu66=='t') echo "checked"; ?> onclick="cek('66')"></td>
					<td class="batas bat" align="right">DO Manual
						<input type="checkbox" id="fmenu67" name="fmenu67" <?php if($isi->f_menu67=='t') echo "checked"; ?> onclick="cek('67')"></td>
					<td class="batas bat" align="right">BBM-AP
						<input type="checkbox" id="fmenu77" name="fmenu77" <?php if($isi->f_menu77=='t') echo "checked"; ?> onclick="cek('77')"></td>
				    <td class="batas bat" align="right">Transfer DO Other
					    <input type="checkbox" id="fmenu222" name="fmenu222" <?php if($isi->f_menu222=='t') echo "checked"; ?> onclick="cek('222')"></td>
				  </tr>
<!-- End Pembelian -->
<!-- Keuangan -->
				  <tr>
					<td class="bat" width="50px">Keuangan</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">Create Nota
						<input type="checkbox" id="fmenu56" name="fmenu56" <?php if($isi->f_menu56=='t') echo "checked"; ?> onclick="cek('56')"></td>
					<td class="bat" align="right">Koreksi Nota
						<input type="checkbox" id="fmenu81" name="fmenu81" <?php if($isi->f_menu81=='t') echo "checked"; ?> onclick="cek('81')"></td>
					<td class="bat" align="right">Daftar Tagihan
						<input type="checkbox" id="fmenu101" name="fmenu101" <?php if($isi->f_menu101=='t') echo "checked"; ?> onclick="cek('101')"></td>
					<td class="bat" align="right">Pelunasan
						<input type="checkbox" id="fmenu105" name="fmenu105" <?php if($isi->f_menu105=='t') echo "checked"; ?> onclick="cek('105')"></td>
					<td class="bat" align="right">Giro
						<input type="checkbox" id="fmenu106" name="fmenu106" <?php if($isi->f_menu106=='t') echo "checked"; ?> onclick="cek('106')"></td>
					<td class="bat" align="right">Daftar Giro
						<input type="checkbox" id="fmenu107" name="fmenu107" <?php if($isi->f_menu107=='t') echo "checked"; ?> onclick="cek('107')"></td>
					<td class="bat" align="right">Daftar Pelunasan
						<input type="checkbox" id="fmenu108" name="fmenu108" <?php if($isi->f_menu108=='t') echo "checked"; ?> onclick="cek('108')"></td>
				    <td class="batas bat" align="right">Approve Pelanggan	
                        <input type="checkbox" id="fmenu226" name="fmenu226" <?php if($isi->f_menu226=='t') echo "checked"; ?> onclick="cek('226')"></td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">KN Retur 
						<input type="checkbox" id="fmenu111" name="fmenu111" <?php if($isi->f_menu111=='t') echo "checked"; ?> onclick="cek('111')"></td>
					<td class="bat" align="right">KN non Retur
						<input type="checkbox" id="fmenu113" name="fmenu113" <?php if($isi->f_menu113=='t') echo "checked"; ?> onclick="cek('113')"></td>
					<td class="bat" align="right">Giro DGU
						<input type="checkbox" id="fmenu136" name="fmenu136" <?php if($isi->f_menu136=='t') echo "checked"; ?> onclick="cek('136')"></td>
					<td class="bat" align="right">KU / Transfer M
						<input type="checkbox" id="fmenu140" name="fmenu140" <?php if($isi->f_menu140=='t') echo "checked"; ?> onclick="cek('140')"></td>
					<td class="bat" align="right">Hutang Dagang
						<input type="checkbox" id="fmenu134" name="fmenu134" <?php if($isi->f_menu134=='t') echo "checked"; ?> onclick="cek('134')"></td>
					<td class="bat" align="right">Pelunasan Hutang
						<input type="checkbox" id="fmenu135" name="fmenu135" <?php if($isi->f_menu135=='t') echo "checked"; ?> onclick="cek('135')"></td>
					<td class="bat" align="right">SPmB  Appr Ku
						<input type="checkbox" id="fmenu72" name="fmenu72" <?php if($isi->f_menu72=='t') echo "checked"; ?> onclick="cek('72')"></td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">SPB Appr Ku
						<input type="checkbox" id="fmenu49" name="fmenu49" <?php if($isi->f_menu49=='t') echo "checked"; ?> onclick="cek('49')"></td>
					<td class="batas bat" align="right">Trans KU Klr
						<input type="checkbox" id="fmenu165" name="fmenu165" <?php if($isi->f_menu165=='t') echo "checked"; ?> onclick="cek('165')"></td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
					<td class="batas bat" align="right">&nbsp;</td>
				  </tr>
<!-- End Keuangan -->
<!-- Akunting -->
				  <tr>
					<td class="bat" width="50px">Akunting</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">Master Saldo Account
						<input type="checkbox" id="fmenu115" name="fmenu115" <?php if($isi->f_menu115=='t') echo "checked"; ?> onclick="cek('115')"></td>
					<td class="bat" align="right">CoA
						<input type="checkbox" id="fmenu118" name="fmenu118" <?php if($isi->f_menu118=='t') echo "checked"; ?> onclick="cek('118')"></td>
					<td class="bat" align="right">Jurnal Umum
						<input type="checkbox" id="fmenu125" name="fmenu125" <?php if($isi->f_menu125=='t') echo "checked"; ?> onclick="cek('125')"></td>
					<td class="bat" align="right">Jurnal Trans Harian
						<input type="checkbox" id="fmenu129" name="fmenu129" <?php if($isi->f_menu129=='t') echo "checked"; ?> onclick="cek('129')"></td>
					<td class="bat" align="right">Buku Besar
						<input type="checkbox" id="fmenu130" name="fmenu130" <?php if($isi->f_menu130=='t') echo "checked"; ?> onclick="cek('130')"></td>
					<td class="bat" align="right">Neraca Saldo
						<input type="checkbox" id="fmenu131" name="fmenu131" <?php if($isi->f_menu131=='t') echo "checked"; ?> onclick="cek('131')"></td>
					<td class="bat" align="right">Neraca
						<input type="checkbox" id="fmenu132" name="fmenu132" <?php if($isi->f_menu132=='t') echo "checked"; ?> onclick="cek('132')"></td>
			  	  </tr>
			  	  <tr>	
					<td class="bat" width="50px"></td>
					<td class="bat" width="2px"></td>
					<td class="bat" align="right">Laba Rugi
						<input type="checkbox" id="fmenu133" name="fmenu133" <?php if($isi->f_menu133=='t') echo "checked"; ?> onclick="cek('133')"></td>
					<td class="bat" align="right">Kas Kecil Multi Row
						<input type="checkbox" id="fmenu146" name="fmenu146" <?php if($isi->f_menu146=='t') echo "checked"; ?> onclick="cek('146')"></td>
				    <td class="batas bat" align="right">Kas Kecil Tes
					    <input type="checkbox" id="fmenu231" name="fmenu231" <?php if($isi->f_menu231=='t') echo "checked"; ?> onclick="cek('231')"></td>
					<td class="bat" align="right">Pengisian Kas Kecil
						<input type="checkbox" id="fmenu151" name="fmenu151" <?php if($isi->f_menu151=='t') echo "checked"; ?> onclick="cek('151')"></td>
					<td class="bat" align="right">Transfer KK
						<input type="checkbox" id="fmenu154" name="fmenu154" <?php if($isi->f_menu154=='t') echo "checked"; ?> onclick="cek('154')"></td>
					<td class="bat" align="right">Transfer KK (Cab)
						<input type="checkbox" id="fmenu155" name="fmenu155" <?php if($isi->f_menu155=='t') echo "checked"; ?> onclick="cek('155')"></td>
				    <td class="batas bat" align="right">KUM Cabang
					    <input type="checkbox" id="fmenu230" name="fmenu230" <?php if($isi->f_menu230=='t') echo "checked"; ?> onclick="cek('230')"></td>
					<td >&nbsp;</td>
					<td >&nbsp;</td>
			  	  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">Pst Plnsn Piutang
						<input type="checkbox" id="fmenu119" name="fmenu119" <?php if($isi->f_menu119=='t') echo "checked"; ?> onclick="cek('119')"></td>
					<td class="bat" align="right">Pst Penjualan
						<input type="checkbox" id="fmenu121" name="fmenu121" <?php if($isi->f_menu121=='t') echo "checked"; ?> onclick="cek('121')"></td>
					<td class="bat" align="right">Pst KN
						<input type="checkbox" id="fmenu123" name="fmenu123" <?php if($isi->f_menu123=='t') echo "checked"; ?> onclick="cek('123')"></td>
					<td class="bat" align="right">Pst Jurnal Umum
						<input type="checkbox" id="fmenu127" name="fmenu127" <?php if($isi->f_menu127=='t') echo "checked"; ?> onclick="cek('127')")></td>
					<td class="bat" align="right">Pst Plnsn Hutang
						<input type="checkbox" id="fmenu142" name="fmenu142" <?php if($isi->f_menu142=='t') echo "checked"; ?> onclick="cek('142')"></td>
					<td class="bat" align="right">Pst Kas Kecil
						<input type="checkbox" id="fmenu148" name="fmenu148" <?php if($isi->f_menu148=='t') echo "checked"; ?> onclick="cek('148')"></td>
					<td class="bat" align="right">Pst Pengisian KK
						<input type="checkbox" id="fmenu152" name="fmenu152" <?php if($isi->f_menu152=='t') echo "checked"; ?> onclick="cek('152')"></td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">UnPst Plnsn Piutang
						<input type="checkbox" id="fmenu120" name="fmenu120" <?php if($isi->f_menu120=='t') echo "checked"; ?> onclick="cek('120')"></td>
					<td class="batas bat" align="right">UnPst Penjualan
						<input type="checkbox" id="fmenu122" name="fmenu122" <?php if($isi->f_menu122=='t') echo "checked"; ?> onclick="cek('122')"></td>
					<td class="batas bat" align="right">UnPst KN
						<input type="checkbox" id="fmenu124" name="fmenu124" <?php if($isi->f_menu124=='t') echo "checked"; ?> onclick="cek('124')"></td>
					<td class="batas bat" align="right">UnPst Jurnal Umum
						<input type="checkbox" id="fmenu128" name="fmenu128" <?php if($isi->f_menu128=='t') echo "checked"; ?> onclick="cek('128')")></td>
					<td class="batas bat" align="right">UnPst Plnsn Hutang
						<input type="checkbox" id="fmenu143" name="fmenu143" <?php if($isi->f_menu143=='t') echo "checked"; ?> onclick="cek('143')"></td>
					<td class="batas bat" align="right">UnPst Kas Kecil
						<input type="checkbox" id="fmenu149" name="fmenu149" <?php if($isi->f_menu149=='t') echo "checked"; ?> onclick="cek('149')"></td>
					<td class="batas bat" align="right">UnPst Pengisian KK
						<input type="checkbox" id="fmenu153" name="fmenu153" <?php if($isi->f_menu153=='t') echo "checked"; ?> onclick="cek('153')"></td>
				  </tr>
<!-- End Akunting -->
<!-- Informasi -->
				  <tr>
					<td class="bat" width="50px">Informasi</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">SPB
						<input type="checkbox" id="fmenu57" name="fmenu57" <?php if($isi->f_menu57=='t') echo "checked"; ?> onclick="cek('57')"></td>
					<td class="bat" align="right">OP
						<input type="checkbox" id="fmenu58" name="fmenu58" <?php if($isi->f_menu58=='t') echo "checked"; ?> onclick="cek('58')"></td>
					<td class="bat" align="right">DO
						<input type="checkbox" id="fmenu59" name="fmenu59" <?php if($isi->f_menu59=='t') echo "checked"; ?> onclick="cek('59')"></td>
					<td class="bat" align="right">Nota
						<input type="checkbox" id="fmenu60" name="fmenu60" <?php if($isi->f_menu60=='t') echo "checked"; ?> onclick="cek('60')"></td>
					<td class="bat" align="right">SPmB
						<input type="checkbox" id="fmenu70" name="fmenu70" <?php if($isi->f_menu70=='t') echo "checked"; ?> onclick="cek('70')"></td>
					<td class="bat" align="right">Stock
						<input type="checkbox" id="fmenu61" name="fmenu61" <?php if($isi->f_menu61=='t') echo "checked"; ?> onclick="cek('61')"></td>
					<td class="bat" align="right">KS
						<input type="checkbox" id="fmenu76" name="fmenu76" <?php if($isi->f_menu76=='t') echo "checked"; ?> onclick="cek('76')"></td>
				  </tr>
				  <tr>
					<td class="bat" width="50px">&nbsp;</td>
					<td class="bat" width="2px">&nbsp;</td>
					<td class="bat" align="right">BBM-AP
						<input type="checkbox" id="fmenu78" name="fmenu78" <?php if($isi->f_menu78=='t') echo "checked"; ?> onclick="cek('78')"></td>
					<td class="bat" align="right">TTB Tolakan
						<input type="checkbox" id="fmenu80" name="fmenu80" <?php if($isi->f_menu80=='t') echo "checked"; ?> onclick="cek('80')"></td>
					<td class="bat" align="right">TTB Retur
						<input type="checkbox" id="fmenu85" name="fmenu85" <?php if($isi->f_menu85=='t') echo "checked"; ?> onclick="cek('85')"></td>
					<td class="bat" align="right">Koreksi Nota
						<input type="checkbox" id="fmenu82" name="fmenu82" <?php if($isi->f_menu82=='t') echo "checked"; ?> onclick="cek('82')"></td>
					<td class="bat" align="right">Promo
						<input type="checkbox" id="fmenu90" name="fmenu90" <?php if($isi->f_menu90=='t') echo "checked"; ?> onclick="cek('90')"></td>
					<td class="bat" align="right">Stock Opname
						<input type="checkbox" id="fmenu92" name="fmenu92" <?php if($isi->f_menu92=='t') echo "checked"; ?> onclick="cek('92')"></td>
					<td class="bat" align="right">SPB per Area
						<input type="checkbox" id="fmenu94" name="fmenu94" <?php if($isi->f_menu94=='t') echo "checked"; ?> onclick="cek('94')"></td>

				  </tr>
				  <tr>
					<td width="50px">&nbsp;</td>
					<td width="2px">&nbsp;</td>
					<td class="bat" align="right">SPB per Pelanggan
						<input type="checkbox" id="fmenu95" name="fmenu95" <?php if($isi->f_menu95=='t') echo "checked"; ?> onclick="cek('95')"></td>
					<td class="bat" align="right">SPB per Salesman
						<input type="checkbox" id="fmenu96" name="fmenu96" <?php if($isi->f_menu96=='t') echo "checked"; ?> onclick="cek('96')"></td>
					<td class="bat" align="right">Target Per Area
						<input type="checkbox" id="fmenu98" name="fmenu98" <?php if($isi->f_menu98=='t') echo "checked"; ?> onclick="cek('98')"></td>
					<td class="bat" align="right">Target Per Salesman
						<input type="checkbox" id="fmenu99" name="fmenu99" <?php if($isi->f_menu99=='t') echo "checked"; ?> onclick="cek('99')"></td>
					<td class="bat" align="right">Target Per Kota
						<input type="checkbox" id="fmenu100" name="fmenu100" <?php if($isi->f_menu100=='t') echo "checked"; ?> onclick="cek('100')"></td>
					<td class="bat" align="right">Daftar Tagihan
						<input type="checkbox" id="fmenu102" name="fmenu102" <?php if($isi->f_menu102=='t') echo "checked"; ?> onclick="cek('102')"></td>
					<td class="bat" align="right">BBM Retur
						<input type="checkbox" id="fmenu110" name="fmenu110" <?php if($isi->f_menu110=='t') echo "checked"; ?> onclick="cek('110')"></td>
				  </tr>
				  <tr>
					<td width="50px">&nbsp;</td>
					<td width="2px">&nbsp;</td>
					<td class="bat" align="right">Master Saldo Account
						<input type="checkbox" id="fmenu116" name="fmenu116" <?php if($isi->f_menu116=='t') echo "checked"; ?> onclick="cek('116')"></td>
					<td class="bat" align="right">CoA
						<input type="checkbox" id="fmenu117" name="fmenu117" <?php if($isi->f_menu117=='t') echo "checked"; ?> onclick="cek('117')"></td>
					<td class="bat" align="right">Jurnal Umum
						<input type="checkbox" id="fmenu126" name="fmenu126" <?php if($isi->f_menu126=='t') echo "checked"; ?> onclick="cek('126')"></td>
					<td class="bat" align="right">Pelunasan Hutang
						<input type="checkbox" id="fmenu138" name="fmenu138" <?php if($isi->f_menu138=='t') echo "checked"; ?> onclick="cek('138')"></td>
					<td class="bat" align="right">Giro DGU
						<input type="checkbox" id="fmenu137" name="fmenu137" <?php if($isi->f_menu137=='t') echo "checked"; ?> onclick="cek('137')"></td>
					<td class="bat" align="right">KU / Transfer M
						<input type="checkbox" id="fmenu141" name="fmenu141" <?php if($isi->f_menu141=='t') echo "checked"; ?> onclick="cek('141')"></td>
					<td class="bat" align="right">Kas Kecil
						<input type="checkbox" id="fmenu147" name="fmenu147" <?php if($isi->f_menu147=='t') echo "checked"; ?> onclick="cek('147')"></td>
				    <td class="batas bat" align="right">KUM Cabang
					    <input type="checkbox" id="fmenu230" name="fmenu230" <?php if($isi->f_menu230=='t') echo "checked"; ?> onclick="cek('230')"></td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">Kendaraan
						<input type="checkbox" id="fmenu145" name="fmenu145" <?php if($isi->f_menu145=='t') echo "checked"; ?> onclick="cek('145')"></td>
					<td class="batas bat" align="right">OP vs DO
						<input type="checkbox" id="fmenu157" name="fmenu157" <?php if($isi->f_menu157=='t') echo "checked"; ?> onclick="cek('157')"></td>
					<td class="batas bat" align="right">KN Retur
						<input type="checkbox" id="fmenu112" name="fmenu112" <?php if($isi->f_menu112=='t') echo "checked"; ?> onclick="cek('112')"></td>
					<td class="batas bat" align="right">KN non Retur
						<input type="checkbox" id="fmenu114" name="fmenu114" <?php if($isi->f_menu114=='t') echo "checked"; ?> onclick="cek('114')"></td>
					<td class="batas bat" align="right">Trans KU Klr
						<input type="checkbox" id="fmenu166" name="fmenu166" <?php if($isi->f_menu166=='t') echo "checked"; ?> onclick="cek('166')"></td>
					<td class="batas bat" align="right">DKB
						<input type="checkbox" id="fmenu163" name="fmenu163" <?php if($isi->f_menu163=='t') echo "checked"; ?> onclick="cek('163')"></td>
					<td class="batas bat" align="right">SJP
						<input type="checkbox" id="fmenu158" name="fmenu158" <?php if($isi->f_menu158=='t') echo "checked"; ?> onclick="cek('158')"></td>
				  </tr>
<!-- End Informasi -->
<!-- Cetak -->
				  <tr>
					<td class="bat" width="50px">Cetak</td>
					<td class="bat" width="2px">:</td>
					<td class="bat" align="right">SPB
						<input type="checkbox" id="fmenu62" name="fmenu62" <?php if($isi->f_menu62=='t') echo "checked"; ?> onclick="cek('62')"></td>
					<td class="bat" align="right">OP
						<input type="checkbox" id="fmenu63" name="fmenu63" <?php if($isi->f_menu63=='t') echo "checked"; ?> onclick="cek('63')"></td>
					<td class="bat" align="right">Nota
						<input type="checkbox" id="fmenu64" name="fmenu64" <?php if($isi->f_menu64=='t') echo "checked"; ?> onclick="cek('64')"></td>
					<td class="bat" align="right">SPMB
						<input type="checkbox" id="fmenu69" name="fmenu69" <?php if($isi->f_menu69=='t') echo "checked"; ?> onclick="cek('69')"></td>
					<td class="bat" align="right">Koreksi Nota
						<input type="checkbox" id="fmenu83" name="fmenu83" <?php if($isi->f_menu83=='t') echo "checked"; ?> onclick="cek('83')"></td>
					<td class="bat" align="right">Surat Jalan
						<input type="checkbox" id="fmenu104" name="fmenu104" <?php if($isi->f_menu104=='t') echo "checked"; ?> onclick="cek('104')"></td>
					<td class="bat" align="right">DKB
						<input type="checkbox" id="fmenu164" name="fmenu164" <?php if($isi->f_menu164=='t') echo "checked"; ?> onclick="cek('164')"></td>
				    <td class="batas bat" align="right">Opname Nota Per Pelanggan
					    <input type="checkbox" id="fmenu228" name="fmenu228" <?php if($isi->f_menu228=='t') echo "checked"; ?> onclick="cek('228')"></td>
				    <td class="batas bat" align="right">Opname Nota Per Area
					    <input type="checkbox" id="fmenu229" name="fmenu229" <?php if($isi->f_menu229=='t') echo "checked"; ?> onclick="cek('229')"></td>
				  </tr>
				  <tr>
					<td class="batas bat" width="50px">&nbsp;</td>
					<td class="batas bat" width="2px">&nbsp;</td>
					<td class="batas bat" align="right">BBM-AP
						<input type="checkbox" id="fmenu162" name="fmenu162" <?php if($isi->f_menu162=='t') echo "checked"; ?> onclick="cek('162')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
				  </tr>
<!-- End Cetak -->
<!-- Export -->
			  	  <tr>
					<td class="batas bat" width="50px">Export</td>
					<td class="batas bat" width="2px">:</td>
					<td class="batas bat" align="right">Kas Kecil
						<input type="checkbox" id="fmenu150" name="fmenu150" <?php if($isi->f_menu150=='t') echo "checked"; ?> onclick="cek('150')"></td>
                    <td class="batas bat" align="right">Mutasi
					<input type="checkbox" id="fmenu227" name="fmenu146" <?php if($isi->f_menu227=='t') echo "checked"; ?> onclick="cek('227')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
			  	  </tr>
<!-- End Export -->
<!-- Setting -->
				  <tr>
					<td class="batas bat" width="50px">Administrator</td>
					<td class="batas bat" width="2px">:</td>
					<td class="batas bat" align="right">User Admin
						<input type="checkbox" id="fmenu0" name="fmenu0" <?php if($isi->f_menu0=='t') echo "checked"; ?> onclick="cek('0')"></td>
					<td class="batas bat" align="right">Printer Setting
						<input type="checkbox" id="fmenu93" name="fmenu93" <?php if($isi->f_menu93=='t') echo "checked"; ?> onclick="cek('93')"></td>
					<td class="batas bat" align="right">Ganti Password
						<input type="checkbox" id="fmenu68" name="fmenu68" <?php if($isi->f_menu68=='t') echo "checked"; ?> onclick="cek('68')"></td>
                    <td class="batas bat" align="right">User Log
                        <input type="checkbox" id="fmenu232" name="fmenu232" <?php if($isi->f_menu232=='t') echo "checked"; ?> onclick="cek('232')"></td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
				  </tr>
<!-- End Setting -->
				</table>
			  </td>
	      	<tr>
			  <td width="100%" colspan="9" align="center">
			    <input name="pil" id="pil" value="Pilih Semua" type="button" onclick="pilihsemua()">
			    <input name="login" id="login" value="Simpan" type="submit">
			    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("user/cform/","#main")'>
			    <input name="unpil" id="unpil" value="Hapus Semua" type="button" onclick="tidakpilihsemua()">
			  </td>
			</tr>
		  </table>
		</div>
	  </div>
	  </div>
	  <?=form_close()?>
    </td>
  </tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">
  window.onunload=function(){
	for(i=0;i<=157;i++){
	  if(document.getElementById("fmenu"+i).value==''){
		document.getElementById("fmenu"+i).value='f';
	  }else{
		document.getElementById("fmenu"+i).value='t';
	  }
	}
	if(document.getElementById("faktif").value==''){
		document.getElementById("faktif").value='f';
	}else{
		document.getElementById("faktif").value='t';
	}
  }
  function view_area(a){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/user/cform/area/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function batal(){
	self.location="<?php echo site_url() ?>"+"/user/cform";
  }
  function pilihsemua(){
		var jml=166;
		for(i=0;i<=jml;i++){
			if(i!=139){
				document.getElementById("fmenu"+i).checked=true;
				document.getElementById("fmenu"+i).value='t';
			}
		}
	}
	function tidakpilihsemua(){
		var jml=166;
		for(i=0;i<=jml;i++){
			if(i!=139){
				document.getElementById("fmenu"+i).checked=false;
				document.getElementById("fmenu"+i).value='f';
			}
		}
	}
</script>
