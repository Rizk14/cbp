<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'user/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="userform">
	  <div class="effect">
	    <div class="accordion2">
	      <table class="mastertable" width="100%" border=0>
	        <tr>
			  <td class="bat" width="50px">User ID</td>
			  <td class="bat" width="2px">:</td>
			  <td><?php 
				$data = array(
			              'name'        => 'iuser',
			              'id'          => 'iuser',
			              'value'       => $isi->i_user,
			              'maxlength'   => '30');
				echo form_input($data);?></td>
			  <td class="bat" width="50px">Area 1</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea1" name="earea1" readonly value="<?php echo $eareaname1; ?>" 
             onclick='showModal("user/cform/area/1/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input type="hidden" id="area1" name="area1" value="<?php echo $isi->i_area1?>"></td>
	      	</tr>
      	  	<tr>
			  <td class="bat" width="50px">User Name</td>
			  <td class="bat" width="2px">:</td>
			  <td><?php 
				$data = array(
			              'name'        => 'eusername',
			              'id'          => 'eusername',
			              'value'       => $isi->e_user_name,
			              'maxlength'   => '30');
				echo form_input($data);?></td>
			  <td class="bat" width="50px">Area 2</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea2" name="earea2" value="<?php echo $eareaname2; ?>" readonly 
             onclick='showModal("user/cform/area/2/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" id="area2" name="area2" value="<?php echo $isi->i_area2?>"></td>
	      	</tr>
	      	<tr>
			  <td class="bat" width="50px">Aktif</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="checkbox" id="faktif" name="faktif" <?php if($isi->f_aktif=='t') echo "checked"; ?>></td>
			  <td class="bat" width="50px">Area 3</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea3" name="earea3" readonly value="<?php echo $eareaname3; ?>"
				     onclick='showModal("user/cform/area/3/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" id="area3" name="area3" value="<?php echo $isi->i_area3?>"></td>
	      	</tr>
	      	<tr>
			  <td class="bat" width="50px">Departement</td>
			  <td class="bat" width="2px">:</td>
			  <td><select id="idepartment" name="idepartment">
          <option>--Pilih</option>
					<option value='0' <?php if($isi->i_departement=='0') echo 'selected';?>>M I S</option>
					<option value='1' <?php if($isi->i_departement=='1') echo 'selected';?>>Internal Audit</option>
					<option value='2' <?php if($isi->i_departement=='2') echo 'selected';?>>Personalia</option>
					<option value='3' <?php if($isi->i_departement=='3') echo 'selected';?>>Sales</option>
					<option value='4' <?php if($isi->i_departement=='4') echo 'selected';?>>Keuangan</option>
					<option value='5' <?php if($isi->i_departement=='5') echo 'selected';?>>Gudang</option>
					<option value='6' <?php if($isi->i_departement=='6') echo 'selected';?>>Pembelian</option>
				</select></td>
			  <td class="bat" width="50px">Area 4</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea4" name="earea4" readonly value="<?php echo $eareaname4; ?>" 
				     onclick='showModal("user/cform/area/4/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" id="area4" name="area4" value="<?php echo $isi->i_area4?>"></td>
	      	</tr>
	      	<tr>
		  	  <td class="bat" width="50px">Jabatan</td>
			  <td class="bat" width="2px">:</td>
			  <td><select id="ilevel" name="ilevel">
					<option>--Pilih--</option>
					<option value='0' <?php if($isi->i_level=='0') echo 'selected';?>>Administrator</option>
					<option value='1' <?php if($isi->i_level=='1') echo 'selected';?>>C E O</option>
					<option value='2' <?php if($isi->i_level=='2') echo 'selected';?>>Direktur</option>
					<option value='3' <?php if($isi->i_level=='3') echo 'selected';?>>General Manager</option>
					<option value='4' <?php if($isi->i_level=='4') echo 'selected';?>>Kabag</option>
					<option value='5' <?php if($isi->i_level=='5') echo 'selected';?>>Supervisor</option>
					<option value='6' <?php if($isi->i_level=='6') echo 'selected';?>>Staff</option>
					<option value='7' <?php if($isi->i_level=='7') echo 'selected';?>>Guest</option>
				</select></td>
			  <td class="bat" width="50px">Area 5</td>
			  <td class="bat" width="2px">:</td>
			  <td><input type="text" id="earea5" name="earea5" readonly value="<?php echo $eareaname5; ?>" 
				     onclick='showModal("user/cform/area/5/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				<input type="hidden" id="area5" name="area5" value="<?php echo $isi->i_area5?>"></td>
	      	</tr>
	      	<tr>
			  <td width="100%" colspan="6">
				<table width="100%" border=0 cellspacing=0 cellpadding=0>
			  	  <tr>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
					<td class="batas">&nbsp;</td>
			  	  </tr>
<!-- Master -->
			  	  <tr>
					<td class="batas" width="50px">Master</td>
					<td class="batas" width="2px">:</td>
					<td class="batas bat" align="right">Grp Pemasok
						<input type="checkbox" id="fmenu1" name="fmenu1" <?php if($isi->f_menu1=='t') echo "checked"; ?> onclick="cek('1')"></td>
					<td class="batas bat" align="right">Pemasok
						<input type="checkbox" id="fmenu2" name="fmenu2" <?php if($isi->f_menu2=='t') echo "checked"; ?> onclick="cek('2')"></td>
          <td class="batas bat" align="right">Pelanggan
						<input type="checkbox" id="fmenu220" name="fmenu220" <?php if($isi->f_menu220=='t') echo "checked"; ?> onclick="cek('220')"></td>
          <td class="batas bat" align="right">Harga HJP
						<input type="checkbox" id="fmenu225" name="fmenu225" <?php if($isi->f_menu225=='t') echo "checked"; ?> onclick="cek('225')"></td>
					<td class="batas bat" align="right">Pelanggan Non SPB
						<input type="checkbox" id="fmenu357" name="fmenu357" <?php if($isi->f_menu357=='t') echo "checked"; ?> onclick="cek('357')"></td>
					<td class="batas bat">&nbsp;</td>
					<td class="batas bat">&nbsp;</td>
			  	  </tr>
			  <tr>
				<td class="bat" width="50px">Gudang</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Gudang
					<input type="checkbox" id="fmenu3" name="fmenu3" <?php if($isi->f_menu3=='t') echo "checked"; ?> onclick="cek('3')"></td>
				<td class="bat" align="right">Lokasi
					<input type="checkbox" id="fmenu4" name="fmenu4" <?php if($isi->f_menu4=='t') echo "checked"; ?> onclick="cek('4')"></td>
				<td class="bat" align="right">Jns BBK
					<input type="checkbox" id="fmenu5" name="fmenu5" <?php if($isi->f_menu5=='t') echo "checked"; ?> onclick="cek('5')"></td>
				<td class="bat" align="right">Jns BBM
					<input type="checkbox" id="fmenu6" name="fmenu6" <?php if($isi->f_menu6=='t') echo "checked"; ?> onclick="cek('6')"></td>
				<td class="bat" align="right">SO
					<input type="checkbox" id="fmenu7" name="fmenu7" <?php if($isi->f_menu7=='t') echo "checked"; ?> onclick="cek('7')"></td>
				<td class="bat" align="right">KS
					<input type="checkbox" id="fmenu75" name="fmenu75" <?php if($isi->f_menu75=='t') echo "checked"; ?> onclick="cek('75')"></td>
				<td class="bat" align="right">BBM-AP
					<input type="checkbox" id="fmenu77" name="fmenu77" <?php if($isi->f_menu77=='t') echo "checked"; ?> onclick="cek('77')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">BBM Retur
					<input type="checkbox" id="fmenu109" name="fmenu109" <?php if($isi->f_menu109=='t') echo "checked"; ?> onclick="cek('109')"></td>
				<td class="bat" align="right">SJ Pinjaman
					<input type="checkbox" id="fmenu156" name="fmenu156" <?php if($isi->f_menu156=='t') echo "checked"; ?> onclick="cek('156')"></td>
				<td class="bat" align="right">SJ
					<input type="checkbox" id="fmenu86" name="fmenu86" <?php if($isi->f_menu86=='t') echo "checked"; ?> onclick="cek('86')"></td>
				<td class="bat" align="right">DKB
					<input type="checkbox" id="fmenu160" name="fmenu160" <?php if($isi->f_menu160=='t') echo "checked"; ?> onclick="cek('160')"></td>
				<td class="bat" align="right">BAPB
					<input type="checkbox" id="fmenu172" name="fmenu172" <?php if($isi->f_menu172=='t') echo "checked"; ?> onclick="cek('172')"></td>
				<td class="bat" align="right">BAPB-SJP
					<input type="checkbox" id="fmenu179" name="fmenu179" <?php if($isi->f_menu179=='t') echo "checked"; ?> onclick="cek('179')"></td>
				<td class="bat" align="right">DKB Appr
					<input type="checkbox" id="fmenu182" name="fmenu182" <?php if($isi->f_menu182=='t') echo "checked"; ?> onclick="cek('182')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Batch Mutasi
					<input type="checkbox" id="fmenu194" name="fmenu194" <?php if($isi->f_menu194=='t') echo "checked"; ?> onclick="cek('194')"></td>
				<td class="bat" align="right">Expedisi
					<input type="checkbox" id="fmenu206" name="fmenu206" <?php if($isi->f_menu206=='t') echo "checked"; ?> onclick="cek('206')"></td>
				<td class="bat" align="right">SJP Receive
					<input type="checkbox" id="fmenu207" name="fmenu207" <?php if($isi->f_menu207=='t') echo "checked"; ?> onclick="cek('207')"></td>
				<td class="bat" align="right">SJ Retur
					<input type="checkbox" id="fmenu208" name="fmenu208" <?php if($isi->f_menu208=='t') echo "checked"; ?> onclick="cek('208')"></td>
				<td class="bat" align="right">SJR Receive
					<input type="checkbox" id="fmenu210" name="fmenu210" <?php if($isi->f_menu210=='t') echo "checked"; ?> onclick="cek('210')"></td>
				<td class="bat" align="right">SJR Toko
					<input type="checkbox" id="fmenu213" name="fmenu213" <?php if($isi->f_menu213=='t') echo "checked"; ?> onclick="cek('213')"></td>
				<td class="bat" align="right">SJ Tolak
					<input type="checkbox" id="fmenu215" name="fmenu215" <?php if($isi->f_menu215=='t') echo "checked"; ?> onclick="cek('215')"></td>
			  </tr>
        <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Upload SO
					<input type="checkbox" id="fmenu223" name="fmenu223" <?php if($isi->f_menu223=='t') echo "checked"; ?> onclick="cek('223')"></td>
				<td class="bat" align="right">Transfer SO
					<input type="checkbox" id="fmenu224" name="fmenu224" <?php if($isi->f_menu224=='t') echo "checked"; ?> onclick="cek('224')"></td>
        <td class="bat" align="right">Transfer SJ
					<input type="checkbox" id="fmenu234" name="fmenu234" <?php if($isi->f_menu234=='t') echo "checked"; ?> onclick="cek('234')"></td>
				 <td class="bat" align="right">Daftar Ekspedisi
					<input type="checkbox" id="fmenu257" name="fmenu257" <?php if($isi->f_menu257=='t') echo "checked"; ?> onclick="cek('257')"></td>
				<td class="bat" align="right">SJ-PB
					<input type="checkbox" id="fmenu289" name="fmenu289" <?php if($isi->f_menu289=='t') echo "checked"; ?> onclick="cek('289')"></td>
				<td class="bat" align="right">BBK Hadiah
					<input type="checkbox" id="fmenu349" name="fmenu349" <?php if($isi->f_menu349=='t') echo "checked"; ?> onclick="cek('349')"></td>
				<td class="bat" align="right">List BBK Hadiah
					<input type="checkbox" id="fmenu350" name="fmenu350" <?php if($isi->f_menu350=='t') echo "checked"; ?> onclick="cek('350')"></td>
			  </tr>
        <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Upload SO Gudang PB
					<input type="checkbox" id="fmenu419" name="fmenu419" <?php if($isi->f_menu419=='t') echo "checked"; ?> onclick="cek('419')"></td>
				<td class="batas bat" align="right">Transfer SO Gudang PB
					<input type="checkbox" id="fmenu420" name="fmenu420" <?php if($isi->f_menu420=='t') echo "checked"; ?> onclick="cek('420')"></td>
				<td class="batas bat" align="right">BON Masuk
					<input type="checkbox" id="fmenu424" name="fmenu424" <?php if($isi->f_menu424=='t') echo "checked"; ?> onclick="cek('424')"></td>
				<td class="batas bat" align="right">BON Keluar
					<input type="checkbox" id="fmenu425" name="fmenu425" <?php if($isi->f_menu425=='t') echo "checked"; ?> onclick="cek('425')"></td>
				<td class="batas bat" align="right">BBM Ubah Grade B
					<input type="checkbox" id="fmenu432" name="fmenu432" <?php if($isi->f_menu432=='t') echo "checked"; ?> onclick="cek('432')"></td>
				<td class="batas bat" align="right">Print Konversi Stock
					<input type="checkbox" id="fmenu459" name="fmenu459" <?php if($isi->f_menu459=='t') echo "checked"; ?> onclick="cek('459')"></td>
				<td class="batas">&nbsp;</td>
			  </tr>			
			  <tr>
				<td class="bat" width="50px">Barang</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Group Brg
					<input type="checkbox" id="fmenu9" name="fmenu9" <?php if($isi->f_menu9=='t') echo "checked"; ?> onclick="cek('9')"></td>
				<td class="bat" align="right">Status Brg
					<input type="checkbox" id="fmenu10" name="fmenu10" <?php if($isi->f_menu10=='t') echo "checked"; ?> onclick="cek('10')"></td>
				<td class="bat" align="right">Kelas Brg
					<input type="checkbox" id="fmenu11" name="fmenu11" <?php if($isi->f_menu11=='t') echo "checked"; ?> onclick="cek('11')"></td>
				<td class="bat" align="right">Kat Brg
					<input type="checkbox" id="fmenu12" name="fmenu12" <?php if($isi->f_menu12=='t') echo "checked"; ?> onclick="cek('12')"></td>
				<td class="bat" align="right">Jenis Brg
					<input type="checkbox" id="fmenu13" name="fmenu13" <?php if($isi->f_menu13=='t') echo "checked"; ?> onclick="cek('13')"></td>
				<td class="bat" align="right">Barang
					<input type="checkbox" id="fmenu14" name="fmenu14" <?php if($isi->f_menu14=='t') echo "checked"; ?> onclick="cek('14')"></td>
				<td class="bat" align="right">Motif Brg
					<input type="checkbox" id="fmenu15" name="fmenu15" <?php if($isi->f_menu15=='t') echo "checked"; ?> onclick="cek('15')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Grade Brg
					<input type="checkbox" id="fmenu16" name="fmenu16" <?php if($isi->f_menu16=='t') echo "checked"; ?> onclick="cek('16')"></td>
				<td class="batas bat" align="right">Harga Brg
					<input type="checkbox" id="fmenu17" name="fmenu17" <?php if($isi->f_menu17=='t') echo "checked"; ?> onclick="cek('17')"></td>
				<td class="batas bat" align="right">Harga Beli
					<input type="checkbox" id="fmenu260" name="fmenu260" <?php if($isi->f_menu260=='t') echo "checked"; ?> onclick="cek('260')"></td>
				<td class="batas bat" align="right">Master Harga (Manual)
					<input type="checkbox" id="fmenu408" name="fmenu408" <?php if($isi->f_menu408=='t') echo "checked"; ?> onclick="cek('408')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Area</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Jenis
					<input type="checkbox" id="fmenu18" name="fmenu18" <?php if($isi->f_menu18=='t') echo "checked"; ?> onclick="cek('18')"></td>
				<td class="bat" align="right">Area
					<input type="checkbox" id="fmenu19" name="fmenu19" <?php if($isi->f_menu19=='t') echo "checked"; ?> onclick="cek('19')"></td>
				<td class="bat" align="right">Salesman
					<input type="checkbox" id="fmenu20" name="fmenu20" <?php if($isi->f_menu20=='t') echo "checked"; ?> onclick="cek('20')"></td>
				<td class="bat" align="right">Negara
					<input type="checkbox" id="fmenu21" name="fmenu21" <?php if($isi->f_menu21=='t') echo "checked"; ?> onclick="cek('21')"></td>
				<td class="bat" align="right">Teritori
					<input type="checkbox" id="fmenu22" name="fmenu22" <?php if($isi->f_menu22=='t') echo "checked"; ?> onclick="cek('22')"></td>
				<td class="bat" align="right">Jns Kota
					<input type="checkbox" id="fmenu23" name="fmenu23" <?php if($isi->f_menu23=='t') echo "checked"; ?> onclick="cek('23')"></td>
				<td class="bat" align="right">Stat Kota
					<input type="checkbox" id="fmenu24" name="fmenu24" <?php if($isi->f_menu24=='t') echo "checked"; ?> onclick="cek('24')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Jns Kota/Area
					<input type="checkbox" id="fmenu25" name="fmenu25" <?php if($isi->f_menu25=='t') echo "checked"; ?> onclick="cek('25')"></td>
				<td class="batas bat" align="right">Group Kota
					<input type="checkbox" id="fmenu26" name="fmenu26" <?php if($isi->f_menu26=='t') echo "checked"; ?> onclick="cek('26')"></td>
				<td class="batas bat" align="right">Kota
					<input type="checkbox" id="fmenu27" name="fmenu27" <?php if($isi->f_menu27=='t') echo "checked"; ?> onclick="cek('27')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Pelanggan</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Klp Hrg
					<input type="checkbox" id="fmenu28" name="fmenu28" <?php if($isi->f_menu28=='t') echo "checked"; ?> onclick="cek('28')"></td>
				<td class="bat" align="right">Area Pelanggan
					<input type="checkbox" id="fmenu29" name="fmenu29" <?php if($isi->f_menu29=='t') echo "checked"; ?> onclick="cek('29')"></td>
				<td class="bat" align="right">Salesman per Pel
					<input type="checkbox" id="fmenu30" name="fmenu30" <?php if($isi->f_menu30=='t') echo "checked"; ?> onclick="cek('30')"></td>
				<td class="bat" align="right">Layanan
					<input type="checkbox" id="fmenu31" name="fmenu31" <?php if($isi->f_menu31=='t') echo "checked"; ?> onclick="cek('31')"></td>
				<td class="bat" align="right">Ukuran
					<input type="checkbox" id="fmenu32" name="fmenu32" <?php if($isi->f_menu32=='t') echo "checked"; ?> onclick="cek('32')"></td>
				<td class="bat" align="right">Cara Jualan
					<input type="checkbox" id="fmenu33" name="fmenu33" <?php if($isi->f_menu33=='t') echo "checked"; ?> onclick="cek('33')"></td>
				<td class="bat" align="right">Kelas
					<input type="checkbox" id="fmenu34" name="fmenu34" <?php if($isi->f_menu34=='t') echo "checked"; ?> onclick="cek('34')"></td>
			  </td>
			  <tr>
				<td width="50px">&nbsp;</td>
				<td width="2px">&nbsp;</td>
				<td class="bat" align="right">Tipe Produk
					<input type="checkbox" id="fmenu35" name="fmenu35" <?php if($isi->f_menu35=='t') echo "checked"; ?> onclick="cek('35')"></td>
				<td class="bat" align="right">Produk Khusus
					<input type="checkbox" id="fmenu36" name="fmenu36" <?php if($isi->f_menu36=='t') echo "checked"; ?> onclick="cek('36')"></td>
				<td class="bat" align="right">Group
					<input type="checkbox" id="fmenu37" name="fmenu37" <?php if($isi->f_menu37=='t') echo "checked"; ?> onclick="cek('37')"></td>
				<td class="bat" align="right">Bank
					<input type="checkbox" id="fmenu38" name="fmenu38" <?php if($isi->f_menu38=='t') echo "checked"; ?> onclick="cek('38')"></td>
				<td class="bat" align="right">Group PLU
					<input type="checkbox" id="fmenu39" name="fmenu39" <?php if($isi->f_menu39=='t') echo "checked"; ?> onclick="cek('39')"></td>
				<td class="bat" align="right">PLU
					<input type="checkbox" id="fmenu40" name="fmenu40" <?php if($isi->f_menu40=='t') echo "checked"; ?> onclick="cek('40')"></td>
				<td class="bat" align="right">Pemilik
					<input type="checkbox" id="fmenu41" name="fmenu41" <?php if($isi->f_menu41=='t') echo "checked"; ?> onclick="cek('41')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">PKP
					<input type="checkbox" id="fmenu42" name="fmenu42" <?php if($isi->f_menu42=='t') echo "checked"; ?> onclick="cek('42')"></td>
				<td class="bat" align="right">Discount
					<input type="checkbox" id="fmenu43" name="fmenu43" <?php if($isi->f_menu43=='t') echo "checked"; ?> onclick="cek('43')"></td>
				<td class="bat" align="right">Status
					<input type="checkbox" id="fmenu44" name="fmenu44" <?php if($isi->f_menu44=='t') echo "checked"; ?> onclick="cek('44')"></td>
				<td class="bat" align="right">Pelanggan
					<input type="checkbox" id="fmenu45" name="fmenu45" <?php if($isi->f_menu45=='t') echo "checked"; ?> onclick="cek('45')"></td>
				<td class="bat" align="right">Approval
					<input type="checkbox" id="fmenu46" name="fmenu46" <?php if($isi->f_menu46=='t') echo "checked"; ?> onclick="cek('46')"></td>
				<td class="bat" align="right">Penyetor
					<input type="checkbox" id="fmenu198" name="fmenu198" <?php if($isi->f_menu198=='t') echo "checked"; ?> onclick="cek('198')"></td>
				<td class="bat" align="right">Update Kode
					<input type="checkbox" id="fmenu217" name="fmenu217" <?php if($isi->f_menu217=='t') echo "checked"; ?> onclick="cek('217')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Grup Bayar
					<input type="checkbox" id="fmenu437" name="fmenu437" <?php if($isi->f_menu437=='t') echo "checked"; ?> onclick="cek('437')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">SPB</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">SPB Baby Bedding
					<input type="checkbox" id="fmenu47" name="fmenu47" <?php if($isi->f_menu47=='t') echo "checked"; ?> onclick="cek('47')"></td>
				<td class="bat" align="right">Promo Baby Bedding
					<input type="checkbox" id="fmenu91" name="fmenu91" <?php if($isi->f_menu91=='t') echo "checked"; ?> onclick="cek('91')"></td>
				<td class="bat" align="right">Appr Sales
					<input type="checkbox" id="fmenu48" name="fmenu48" <?php if($isi->f_menu48=='t') echo "checked"; ?> onclick="cek('48')"></td>
				<td class="bat" align="right">Appr Keuangan
					<input type="checkbox" id="fmenu49" name="fmenu49" <?php if($isi->f_menu49=='t') echo "checked"; ?> onclick="cek('49')"></td>
				<td class="bat" align="right">Realisasi
					<input type="checkbox" id="fmenu50" name="fmenu50" <?php if($isi->f_menu50=='t') echo "checked"; ?> onclick="cek('50')"></td>
				<td class="bat" align="right">Pemenuhan
					<input type="checkbox" id="fmenu51" name="fmenu51" <?php if($isi->f_menu51=='t') echo "checked"; ?> onclick="cek('51')"></td>
				<td class="bat" align="right">Siap Nota gdg
					<input type="checkbox" id="fmenu87" name="fmenu87" <?php if($isi->f_menu87=='t') echo "checked"; ?> onclick="cek('87')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Target Penjualan
					<input type="checkbox" id="fmenu97" name="fmenu97" <?php if($isi->f_menu97=='t') echo "checked"; ?> onclick="cek('97')"></td>
				<td class="bat" align="right">Siap Nota sls
					<input type="checkbox" id="fmenu103" name="fmenu103" <?php if($isi->f_menu103=='t') echo "checked"; ?> onclick="cek('103')"></td>
				<td class="bat" align="right">KN Retur
					<input type="checkbox" id="fmenu111" name="fmenu111" <?php if($isi->f_menu111=='t') echo "checked"; ?> onclick="cek('111')"></td>
				<td class="bat" align="right">List KN Retur
					<input type="checkbox" id="fmenu112" name="fmenu112" <?php if($isi->f_menu112=='t') echo "checked"; ?> onclick="cek('112')"></td>
				<td class="bat" align="right">KN non Retur
					<input type="checkbox" id="fmenu113" name="fmenu113" <?php if($isi->f_menu113=='t') echo "checked"; ?> onclick="cek('113')"></td>
				<td class="bat" align="right">List KN non Ret
					<input type="checkbox" id="fmenu114" name="fmenu114" <?php if($isi->f_menu114=='t') echo "checked"; ?> onclick="cek('114')"></td>
				<td class="bat" align="right">Dialogue Home
					<input type="checkbox" id="fmenu159" name="fmenu159" <?php if($isi->f_menu159=='t') echo "checked"; ?> onclick="cek('159')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Promo Dialogue Home
					<input type="checkbox" id="fmenu161" name="fmenu161" <?php if($isi->f_menu161=='t') echo "checked"; ?> onclick="cek('161')"></td>
				<td class="bat" align="right">RRKH
					<input type="checkbox" id="fmenu168" name="fmenu168" <?php if($isi->f_menu168=='t') echo "checked"; ?> onclick="cek('168')"></td>
				<td class="bat" align="right">Cek SPB
					<input type="checkbox" id="fmenu187" name="fmenu187" <?php if($isi->f_menu187=='t') echo "checked"; ?> onclick="cek('187')"></td>
				<td class="bat" align="right">Pelanggan Baru
					<input type="checkbox" id="fmenu197" name="fmenu197" <?php if($isi->f_menu197=='t') echo "checked"; ?> onclick="cek('197')"></td>
				<td class="bat" align="right">Transfer SPB Lama
					<input type="checkbox" id="fmenu218" name="fmenu218" <?php if($isi->f_menu218=='t') echo "checked"; ?> onclick="cek('218')"></td>
				<td class="bat" align="right">Transfer SPB Baru
					<input type="checkbox" id="fmenu219" name="fmenu219" <?php if($isi->f_menu219=='t') echo "checked"; ?> onclick="cek('219')"></td>
				<td class="bat" align="right">Upload SPB
					<input type="checkbox" id="fmenu221" name="fmenu221" <?php if($isi->f_menu221=='t') echo "checked"; ?> onclick="cek('221')"></td>
			  </tr>
        <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">SPB Cek Cabang
					<input type="checkbox" id="fmenu239" name="fmenu239" <?php if($isi->f_menu239=='t') echo "checked"; ?> onclick="cek('239')"></td>
				<td class="bat" align="right">Transfer RRKH
					<input type="checkbox" id="fmenu245" name="fmenu245" <?php if($isi->f_menu245=='t') echo "checked"; ?> onclick="cek('245')"></td>
				<td class="bat" align="right">SPB Baby Non Bedding
					<input type="checkbox" id="fmenu249" name="fmenu249" <?php if($isi->f_menu249=='t') echo "checked"; ?> onclick="cek('249')"></td>
				<td class="bat" align="right">Promo Baby Non Bedding
					<input type="checkbox" id="fmenu250" name="fmenu250" <?php if($isi->f_menu250=='t') echo "checked"; ?> onclick="cek('250')"></td>
				<td class="bat" align="right">Debet Nota
					<input type="checkbox" id="fmenu253" name="fmenu253" <?php if($isi->f_menu253=='t') echo "checked"; ?> onclick="cek('253')"></td>
				<td class="bat" align="right">Promo Khusus
					<input type="checkbox" id="fmenu261" name="fmenu261" <?php if($isi->f_menu261=='t') echo "checked"; ?> onclick="cek('261')"></td>
				<td class="bat" align="right">Terima SPB
					<input type="checkbox" id="fmenu370" name="fmenu370" <?php if($isi->f_menu370=='t') echo "checked"; ?> onclick="cek('370')"></td>
			  </tr>
        <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">SPB Pindah Periode
					<input type="checkbox" id="fmenu341" name="fmenu341" <?php if($isi->f_menu341=='t') echo "checked"; ?> onclick="cek('341')"></td>
				<td class="bat" align="right">List Penjualan
					<input type="checkbox" id="fmenu343" name="fmenu343" <?php if($isi->f_menu343=='t') echo "checked"; ?> onclick="cek('343')"></td>
				<td class="bat" align="right">Hitung Insentif AS
					<input type="checkbox" id="fmenu346" name="fmenu346" <?php if($isi->f_menu346=='t') echo "checked"; ?> onclick="cek('346')"></td>
				<td class="bat" align="right">List Insentif
					<input type="checkbox" id="fmenu347" name="fmenu347" <?php if($isi->f_menu347=='t') echo "checked"; ?> onclick="cek('347')"></td>
				<td class="bat" align="right">List Insentif AS
					<input type="checkbox" id="fmenu348" name="fmenu348" <?php if($isi->f_menu348=='t') echo "checked"; ?> onclick="cek('348')"></td>
				<td class="bat" align="right">Laporan Penjualan
					<input type="checkbox" id="fmenu352" name="fmenu352" <?php if($isi->f_menu352=='t') echo "checked"; ?> onclick="cek('352')"></td>
				<td class="bat" align="right">Laporan Penjualan Cabang
					<input type="checkbox" id="fmenu354" name="fmenu354" <?php if($isi->f_menu354=='t') echo "checked"; ?> onclick="cek('354')"></td>
			  </tr>
        <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Realisasi SPB Pemenuhan
					<input type="checkbox" id="fmenu406" name="fmenu406" <?php if($isi->f_menu406=='t') echo "checked"; ?> onclick="cek('406')"></td>
				<td class="batas bat" align="right">Rekap SPB Pemenuhan
					<input type="checkbox" id="fmenu407" name="fmenu407" <?php if($isi->f_menu407=='t') echo "checked"; ?> onclick="cek('407')"></td>
				<td class="batas bat" align="right">Laporan AS
					<input type="checkbox" id="fmenu410" name="fmenu410" <?php if($isi->f_menu410=='t') echo "checked"; ?> onclick="cek('410')"></td>
				<td class="batas bat" align="right">Ubah Kode Sales
					<input type="checkbox" id="fmenu412" name="fmenu412" <?php if($isi->f_menu412=='t') echo "checked"; ?> onclick="cek('412')"></td>
				<td class="batas bat" align="right">Pindah Periode Sales
					<input type="checkbox" id="fmenu428" name="fmenu428" <?php if($isi->f_menu428=='t') echo "checked"; ?> onclick="cek('428')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">Promo</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Promo
					<input type="checkbox" id="fmenu89" name="fmenu89" <?php if($isi->f_menu89=='t') echo "checked"; ?> onclick="cek('89')"></td>
				<td class="batas bat" align="right">Jenis Promo
					<input type="checkbox" id="fmenu88" name="fmenu88" <?php if($isi->f_menu88=='t') echo "checked"; ?> onclick="cek('88')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">SPmB</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">SPmB
					<input type="checkbox" id="fmenu8" name="fmenu8" <?php if($isi->f_menu8=='t') echo "checked"; ?> onclick="cek('8')"></td>
				<td class="bat" align="right">Appr Sales
					<input type="checkbox" id="fmenu71" name="fmenu71" <?php if($isi->f_menu71=='t') echo "checked"; ?> onclick="cek('71')"></td>
				<td class="bat" align="right">Appr Keuangan
					<input type="checkbox" id="fmenu72" name="fmenu72" <?php if($isi->f_menu72=='t') echo "checked"; ?> onclick="cek('72')"></td>
				<td class="bat" align="right">Realisasi
					<input type="checkbox" id="fmenu73" name="fmenu73" <?php if($isi->f_menu73=='t') echo "checked"; ?> onclick="cek('73')"></td>
				<td class="bat" align="right">Pemenuhan
					<input type="checkbox" id="fmenu74" name="fmenu74" <?php if($isi->f_menu74=='t') echo "checked"; ?> onclick="cek('74')"></td>
				<td class="bat" align="right">SPmB Acc
					<input type="checkbox" id="fmenu183" name="fmenu183" <?php if($isi->f_menu183=='t') echo "checked"; ?> onclick="cek('183')"></td>
				<td class="bat" align="right">Closing SPmB
					<input type="checkbox" id="fmenu193" name="fmenu193" <?php if($isi->f_menu193=='t') echo "checked"; ?> onclick="cek('193')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Import SPmB
					<input type="checkbox" id="fmenu195" name="fmenu195" <?php if($isi->f_menu195=='t') echo "checked"; ?> onclick="cek('195')"></td>
				<td class="batas bat" align="right">Upload SPmB
					<input type="checkbox" id="fmenu196" name="fmenu196" <?php if($isi->f_menu196=='t') echo "checked"; ?> onclick="cek('196')"></td>
				<td class="batas bat" align="right">SPmB Manual
					<input type="checkbox" id="fmenu256" name="fmenu256" <?php if($isi->f_menu256=='t') echo "checked"; ?> onclick="cek('256')"></td>			
        <td class="batas bat" align="right">Import SPMB Konsinyasi
					<input type="checkbox" id="fmenu288" name="fmenu288" <?php if($isi->f_menu288=='t') echo "checked"; ?> onclick="cek('288')"></td>
				<td class="batas bat" align="right">Transfer SPMB
					<input type="checkbox" id="fmenu298" name="fmenu298" <?php if($isi->f_menu298=='t') echo "checked"; ?> onclick="cek('298')"></td>
				<td class="batas bat" align="right">List SPMB Tdk Terealisasi
					<input type="checkbox" id="fmenu342" name="fmenu342" <?php if($isi->f_menu342=='t') echo "checked"; ?> onclick="cek('342')"></td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">OP</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Create
					<input type="checkbox" id="fmenu52" name="fmenu52" <?php if($isi->f_menu52=='t') echo "checked"; ?> onclick="cek('52')"></td>
				<td class="batas bat" align="right">Status
					<input type="checkbox" id="fmenu53" name="fmenu53" <?php if($isi->f_menu53=='t') echo "checked"; ?> onclick="cek('53')"></td>
				<td class="batas bat" align="right">Closing
					<input type="checkbox" id="fmenu54" name="fmenu54" <?php if($isi->f_menu54=='t') echo "checked"; ?> onclick="cek('54')"></td>
				<td class="batas bat" align="right">Cancel
					<input type="checkbox" id="fmenu55" name="fmenu55" <?php if($isi->f_menu55=='t') echo "checked"; ?> onclick="cek('55')"></td>
				<td class="batas bat" align="right">OP (BBM-AP)
					<input type="checkbox" id="fmenu181" name="fmenu181" <?php if($isi->f_menu181=='t') echo "checked"; ?> onclick="cek('181')"></td>
				<td class="batas bat" align="right">Rekap SPB
					<input type="checkbox" id="fmenu379" name="fmenu379" <?php if($isi->f_menu379=='t') echo "checked"; ?> onclick="cek('379')"></td>
				<td class="batas bat" align="right">OP Rekap SPB
					<input type="checkbox" id="fmenu381" name="fmenu381" <?php if($isi->f_menu381=='t') echo "checked"; ?> onclick="cek('381')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">TTB</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">TTB Tolakan
					<input type="checkbox" id="fmenu79" name="fmenu79" <?php if($isi->f_menu79=='t') echo "checked"; ?> onclick="cek('79')"></td>
				<td class="batas bat" align="right">TTB Retur
					<input type="checkbox" id="fmenu84" name="fmenu84" <?php if($isi->f_menu84=='t') echo "checked"; ?> onclick="cek('84')"></td>
				<td class="batas bat" align="right">Transfer TTB Retur
					<input type="checkbox" id="fmenu246" name="fmenu246" <?php if($isi->f_menu246=='t') echo "checked"; ?> onclick="cek('246')"></td>
				<td class="batas bat" align="right">List TTB Blm BBM
					<input type="checkbox" id="fmenu423" name="fmenu423" <?php if($isi->f_menu423=='t') echo "checked"; ?> onclick="cek('423')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Konsinyasi</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Penjualan
					<input type="checkbox" id="fmenu293" name="fmenu293" <?php if($isi->f_menu293=='t') echo "checked"; ?> onclick="cek('293')"></td>
				<td class="bat" align="right">Order
					<input type="checkbox" id="fmenu295" name="fmenu295" <?php if($isi->f_menu295=='t') echo "checked"; ?> onclick="cek('295')"></td>
				<td class="bat" align="right">SJPB Receive
					<input type="checkbox" id="fmenu299" name="fmenu299" <?php if($isi->f_menu299=='t') echo "checked"; ?> onclick="cek('299')"></td>
				<td class="bat" align="right">SJPB Retur
					<input type="checkbox" id="fmenu301" name="fmenu301" <?php if($isi->f_menu301=='t') echo "checked"; ?> onclick="cek('301')"></td>
				<td class="bat" align="right">SJPB Retur Receive
					<input type="checkbox" id="fmenu309" name="fmenu309" <?php if($isi->f_menu309=='t') echo "checked"; ?> onclick="cek('309')"></td>
				<td class="bat" align="right">SJB Retur
					<input type="checkbox" id="fmenu311" name="fmenu311" <?php if($isi->f_menu311=='t') echo "checked"; ?> onclick="cek('311')"></td>
				<td class="bat" align="right">SJB Retur Receive
					<input type="checkbox" id="fmenu314" name="fmenu314" <?php if($isi->f_menu314=='t') echo "checked"; ?> onclick="cek('314')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Stock Opname
					<input type="checkbox" id="fmenu319" name="fmenu319" <?php if($isi->f_menu319=='t') echo "checked"; ?> onclick="cek('319')"></td>
				<td class="bat" align="right">List SO
					<input type="checkbox" id="fmenu320" name="fmenu320" <?php if($isi->f_menu320=='t') echo "checked"; ?> onclick="cek('320')"></td>
				<td class="bat" align="right">Update Mutasi Kons
					<input type="checkbox" id="fmenu321" name="fmenu321" <?php if($isi->f_menu321=='t') echo "checked"; ?> onclick="cek('321')"></td>
				<td class="bat" align="right">List Mutasi Kons
					<input type="checkbox" id="fmenu322" name="fmenu322" <?php if($isi->f_menu322=='t') echo "checked"; ?> onclick="cek('322')"></td>
				<td class="bat" align="right">Export Order
					<input type="checkbox" id="fmenu323" name="fmenu323" <?php if($isi->f_menu323=='t') echo "checked"; ?> onclick="cek('323')"></td>
				<td class="bat" align="right">Transfer SO Kons
					<input type="checkbox" id="fmenu366" name="fmenu366" <?php if($isi->f_menu366=='t') echo "checked"; ?> onclick="cek('366')"></td>
				<td class="bat" align="right">SPB Konsinyasi
					<input type="checkbox" id="fmenu442" name="fmenu442" <?php if($isi->f_menu442=='t') echo "checked"; ?> onclick="cek('442')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Master SPG
					<input type="checkbox" id="fmenu443" name="fmenu443" <?php if($isi->f_menu443=='t') echo "checked"; ?> onclick="cek('443')"></td>
				<td class="bat" align="right">Master Toko Konsinyasi
					<input type="checkbox" id="fmenu444" name="fmenu444" <?php if($isi->f_menu444=='t') echo "checked"; ?> onclick="cek('444')"></td>
				<td class="bat" align="right">Master Harga
					<input type="checkbox" id="fmenu445" name="fmenu445" <?php if($isi->f_menu445=='t') echo "checked"; ?> onclick="cek('445')"></td>
				<td class="bat" align="right">Rekap Order (SPMB)
					<input type="checkbox" id="fmenu446" name="fmenu446" <?php if($isi->f_menu446=='t') echo "checked"; ?> onclick="cek('446')"></td>
				<td class="bat" align="right">Realisasi SPB
					<input type="checkbox" id="fmenu447" name="fmenu447" <?php if($isi->f_menu447=='t') echo "checked"; ?> onclick="cek('447')"></td>
				<td class="bat" align="right">Cek Penjualan
					<input type="checkbox" id="fmenu449" name="fmenu449" <?php if($isi->f_menu449=='t') echo "checked"; ?> onclick="cek('449')"></td>
				<td class="bat" align="right">List Barang
					<input type="checkbox" id="fmenu448" name="fmenu448" <?php if($isi->f_menu448=='t') echo "checked"; ?> onclick="cek('448')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Print SJPB Retur
					<input type="checkbox" id="fmenu441" name="fmenu441" <?php if($isi->f_menu441=='t') echo "checked"; ?> onclick="cek('441')"></td>
				<td class="batas bat" align="right">Print SJB Retur
					<input type="checkbox" id="fmenu458" name="fmenu458" <?php if($isi->f_menu458=='t') echo "checked"; ?> onclick="cek('458')"></td>
				<td class="batas bat" align="right">Lap Penjualan Grup Konsinyasi
					<input type="checkbox" id="fmenu457" name="fmenu457" <?php if($isi->f_menu457=='t') echo "checked"; ?> onclick="cek('457')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Nota</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Create
					<input type="checkbox" id="fmenu56" name="fmenu56" <?php if($isi->f_menu56=='t') echo "checked"; ?> onclick="cek('56')"></td>
				<td class="bat" align="right">Koreksi Nota
					<input type="checkbox" id="fmenu81" name="fmenu81" <?php if($isi->f_menu81=='t') echo "checked"; ?> onclick="cek('81')"></td>
				<td class="bat" align="right">Daftar Tagihan
					<input type="checkbox" id="fmenu101" name="fmenu101" <?php if($isi->f_menu101=='t') echo "checked"; ?> onclick="cek('101')"></td>
				<td class="bat" align="right">Pelunasan
					<input type="checkbox" id="fmenu105" name="fmenu105" <?php if($isi->f_menu105=='t') echo "checked"; ?> onclick="cek('105')"></td>
				<td class="bat" align="right">Giro
					<input type="checkbox" id="fmenu106" name="fmenu106" <?php if($isi->f_menu106=='t') echo "checked"; ?> onclick="cek('106')"></td>
				<td class="bat" align="right">Daftar Giro
					<input type="checkbox" id="fmenu107" name="fmenu107" <?php if($isi->f_menu107=='t') echo "checked"; ?> onclick="cek('107')"></td>
				<td class="bat" align="right">Daftar Pelunasan
					<input type="checkbox" id="fmenu108" name="fmenu108" <?php if($isi->f_menu108=='t') echo "checked"; ?> onclick="cek('108')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Cek Pelunasan
					<input type="checkbox" id="fmenu188" name="fmenu188" <?php if($isi->f_menu188=='t') echo "checked"; ?> onclick="cek('188')"></td>
				<td class="bat" align="right">Cek IKHP
					<input type="checkbox" id="fmenu189" name="fmenu189" <?php if($isi->f_menu189=='t') echo "checked"; ?> onclick="cek('189')"></td>
				<td class="bat" align="right">Cek IKHP Klr
					<input type="checkbox" id="fmenu190" name="fmenu190" <?php if($isi->f_menu190=='t') echo "checked"; ?> onclick="cek('190')"></td>
				<td class="bat" align="right">Cek Kas Kcl
					<input type="checkbox" id="fmenu191" name="fmenu191" <?php if($isi->f_menu191=='t') echo "checked"; ?> onclick="cek('191')"></td>
				<td class="bat" align="right">Giro Cair
					<input type="checkbox" id="fmenu202" name="fmenu202" <?php if($isi->f_menu202=='t') echo "checked"; ?> onclick="cek('202')"></td>
				<td class="bat" align="right">Daftar Giro Cair
					<input type="checkbox" id="fmenu203" name="fmenu203" <?php if($isi->f_menu203=='t') echo "checked"; ?> onclick="cek('203')"></td>
				<td class="bat" align="right">Daftar Giro blm Cair
					<input type="checkbox" id="fmenu204" name="fmenu204" <?php if($isi->f_menu204=='t') echo "checked"; ?> onclick="cek('204')"></td>
			  </tr>
        <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Transfer Nota
					<input type="checkbox" id="fmenu271" name="fmenu271" <?php if($isi->f_menu271=='t') echo "checked"; ?> onclick="cek('271')"></td>
				<td class="batas bat" align="right">Transfer KN
					<input type="checkbox" id="fmenu272" name="fmenu272" <?php if($isi->f_menu272=='t') echo "checked"; ?> onclick="cek('272')"></td>
				<td class="batas bat" align="right">Cetak Nota Referensi
          <input type="checkbox" id="fmenu438" name="fmenu438" <?php if($isi->f_menu438=='t') echo "checked"; ?> onclick="cek('438')"></td>
				<td class="batas bat" align="right">Bon Masuk Lain2
          <input type="checkbox" id="fmenu453" name="fmenu453" <?php if($isi->f_menu453=='t') echo "checked"; ?> onclick="cek('453')">;</td>
				<td class="batas bat" align="right">Proses Bon Masuk Lain2
          <input type="checkbox" id="fmenu454" name="fmenu454" <?php if($isi->f_menu454=='t') echo "checked"; ?> onclick="cek('454')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Informasi</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">SPB
					<input type="checkbox" id="fmenu57" name="fmenu57" <?php if($isi->f_menu57=='t') echo "checked"; ?> onclick="cek('57')"></td>
				<td class="bat" align="right">OP
					<input type="checkbox" id="fmenu58" name="fmenu58" <?php if($isi->f_menu58=='t') echo "checked"; ?> onclick="cek('58')"></td>
				<td class="bat" align="right">DO
					<input type="checkbox" id="fmenu59" name="fmenu59" <?php if($isi->f_menu59=='t') echo "checked"; ?> onclick="cek('59')"></td>
				<td class="bat" align="right">Nota Per Area
					<input type="checkbox" id="fmenu60" name="fmenu60" <?php if($isi->f_menu60=='t') echo "checked"; ?> onclick="cek('60')"></td>
				<td class="bat" align="right">SPMB
					<input type="checkbox" id="fmenu70" name="fmenu70" <?php if($isi->f_menu70=='t') echo "checked"; ?> onclick="cek('70')"></td>
				<td class="bat" align="right">Stock
					<input type="checkbox" id="fmenu61" name="fmenu61" <?php if($isi->f_menu61=='t') echo "checked"; ?> onclick="cek('61')"></td>
				<td class="bat" align="right">KS
					<input type="checkbox" id="fmenu76" name="fmenu76" <?php if($isi->f_menu76=='t') echo "checked"; ?> onclick="cek('76')"></td>
			  </tr>
			  <tr>
				<td width="50px">&nbsp;</td>
				<td width="2px">&nbsp;</td>
				<td class="bat" align="right">BBM-AP
					<input type="checkbox" id="fmenu78" name="fmenu78" <?php if($isi->f_menu78=='t') echo "checked"; ?> onclick="cek('78')"></td>
				<td class="bat" align="right">TTB Tolakan
					<input type="checkbox" id="fmenu80" name="fmenu80" <?php if($isi->f_menu80=='t') echo "checked"; ?> onclick="cek('80')"></td>
				<td class="bat" align="right">TTB Retur
					<input type="checkbox" id="fmenu85" name="fmenu85" <?php if($isi->f_menu85=='t') echo "checked"; ?> onclick="cek('85')"></td>
				<td class="bat" align="right">Koreksi Nota
					<input type="checkbox" id="fmenu82" name="fmenu82" <?php if($isi->f_menu82=='t') echo "checked"; ?> onclick="cek('82')"></td>
				<td class="bat" align="right">Promo
					<input type="checkbox" id="fmenu90" name="fmenu90" <?php if($isi->f_menu90=='t') echo "checked"; ?> onclick="cek('90')"></td>
				<td class="bat" align="right">Stock Opname
					<input type="checkbox" id="fmenu92" name="fmenu92" <?php if($isi->f_menu92=='t') echo "checked"; ?> onclick="cek('92')"></td>
				<td class="bat" align="right">SPB per Area
					<input type="checkbox" id="fmenu94" name="fmenu94" <?php if($isi->f_menu94=='t') echo "checked"; ?> onclick="cek('94')"></td>
			  </tr>
			  <tr>
				<td width="50px">&nbsp;</td>
				<td width="2px">&nbsp;</td>
				<td class="bat" align="right">SPB per Pelanggan
					<input type="checkbox" id="fmenu95" name="fmenu95" <?php if($isi->f_menu95=='t') echo "checked"; ?> onclick="cek('95')"></td>
				<td class="bat" align="right">SPB per Salesman
					<input type="checkbox" id="fmenu96" name="fmenu96" <?php if($isi->f_menu96=='t') echo "checked"; ?> onclick="cek('96')"></td>
				<td class="bat" align="right">List Target/Area
					<input type="checkbox" id="fmenu98" name="fmenu98" <?php if($isi->f_menu98=='t') echo "checked"; ?> onclick="cek('98')"></td>
				<td class="bat" align="right">List Target/Sales
					<input type="checkbox" id="fmenu99" name="fmenu99" <?php if($isi->f_menu99=='t') echo "checked"; ?> onclick="cek('99')"></td>
				<td class="bat" align="right">List Target/Kota
					<input type="checkbox" id="fmenu100" name="fmenu100" <?php if($isi->f_menu100=='t') echo "checked"; ?> onclick="cek('100')"></td>
				<td class="bat" align="right">List D Tagihan
					<input type="checkbox" id="fmenu102" name="fmenu102" <?php if($isi->f_menu102=='t') echo "checked"; ?> onclick="cek('102')"></td>
				<td class="bat" align="right">List BBM Ret
					<input type="checkbox" id="fmenu110" name="fmenu110" <?php if($isi->f_menu110=='t') echo "checked"; ?> onclick="cek('110')"></td>
			  </tr>
			  <tr>
				<td width="50px">&nbsp;</td>
				<td width="2px">&nbsp;</td>
				<td class="bat" align="right">Master Saldo Acc
					<input type="checkbox" id="fmenu116" name="fmenu116" <?php if($isi->f_menu116=='t') echo "checked"; ?> onclick="cek('116')"></td>
				<td class="bat" align="right">CoA
					<input type="checkbox" id="fmenu117" name="fmenu117" <?php if($isi->f_menu117=='t') echo "checked"; ?> onclick="cek('117')"></td>
				<td class="bat" align="right">Jurnal Umum
					<input type="checkbox" id="fmenu126" name="fmenu126" <?php if($isi->f_menu126=='t') echo "checked"; ?> onclick="cek('126')"></td>
				<td class="bat" align="right">Pelunasan Hutang
					<input type="checkbox" id="fmenu138" name="fmenu138" <?php if($isi->f_menu138=='t') echo "checked"; ?> onclick="cek('138')"></td>			
				<td class="bat" align="right">Giro DGU
					<input type="checkbox" id="fmenu137" name="fmenu137" <?php if($isi->f_menu137=='t') echo "checked"; ?> onclick="cek('137')"></td>
				<td class="bat" align="right">KU / Transfer M
					<input type="checkbox" id="fmenu141" name="fmenu141" <?php if($isi->f_menu141=='t') echo "checked"; ?> onclick="cek('141')"></td>
				<td class="bat" align="right">Kas Kecil
					<input type="checkbox" id="fmenu147" name="fmenu147" <?php if($isi->f_menu147=='t') echo "checked"; ?> onclick="cek('147')"></td>
			  </tr>	
			  <tr>
				<td width="50px">&nbsp;</td>
				<td width="2px">&nbsp;</td>
				<td class="bat" align="right">Kendaraan
					<input type="checkbox" id="fmenu145" name="fmenu145" <?php if($isi->f_menu145=='t') echo "checked"; ?> onclick="cek('145')"></td>
				<td class="bat" align="right">OP vs DO
					<input type="checkbox" id="fmenu157" name="fmenu157" <?php if($isi->f_menu157=='t') echo "checked"; ?> onclick="cek('157')"></td>
				<td class="bat" align="right">SJP
					<input type="checkbox" id="fmenu158" name="fmenu158" <?php if($isi->f_menu158=='t') echo "checked"; ?> onclick="cek('158')"></td>
				<td class="bat" align="right">DKB
					<input type="checkbox" id="fmenu163" name="fmenu163" <?php if($isi->f_menu163=='t') echo "checked"; ?> onclick="cek('163')"></td>
				<td class="bat" align="right">KU / TRansfer K
					<input type="checkbox" id="fmenu166" name="fmenu166" <?php if($isi->f_menu166=='t') echo "checked"; ?> onclick="cek('166')"></td>
				<td class="bat" align="right">SJ
					<input type="checkbox" id="fmenu167" name="fmenu167" <?php if($isi->f_menu167=='t') echo "checked"; ?> onclick="cek('167')"></td>
				<td class="bat" align="right">RRKH
					<input type="checkbox" id="fmenu169" name="fmenu169" <?php if($isi->f_menu169=='t') echo "checked"; ?> onclick="cek('169')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">IKHP
					<input type="checkbox" id="fmenu170" name="fmenu170" <?php if($isi->f_menu170=='t') echo "checked"; ?> onclick="cek('170')"></td>
				<td class="bat" align="right">BAPB
					<input type="checkbox" id="fmenu173" name="fmenu173" <?php if($isi->f_menu173=='t') echo "checked"; ?> onclick="cek('173')"></td>
				<td class="bat" align="right">IKHP (Keluar)
					<input type="checkbox" id="fmenu174" name="fmenu174" <?php if($isi->f_menu174=='t') echo "checked"; ?> onclick="cek('174')"></td>
				<td class="bat" align="right">Pencapaian Tgt SPB
					<input type="checkbox" id="fmenu177" name="fmenu177" <?php if($isi->f_menu177=='t') echo "checked"; ?> onclick="cek('177')"></td>
				<td class="bat" align="right">BAPB-SJP
					<input type="checkbox" id="fmenu180" name="fmenu180" <?php if($isi->f_menu180=='t') echo "checked"; ?> onclick="cek('180')"></td>
				<td class="bat" align="right">SJ Retur
					<input type="checkbox" id="fmenu209" name="fmenu209" <?php if($isi->f_menu209=='t') echo "checked"; ?> onclick="cek('209')"></td>
				<td class="bat" align="right">SJR	Receive
					<input type="checkbox" id="fmenu211" name="fmenu211" <?php if($isi->f_menu211=='t') echo "checked"; ?> onclick="cek('211')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bbat" align="right">SJP	Receive
					<input type="checkbox" id="fmenu212" name="fmenu212" <?php if($isi->f_menu212=='t') echo "checked"; ?> onclick="cek('212')"></td>
				<td class="bat" align="right">SJR Toko
					<input type="checkbox" id="fmenu214" name="fmenu214" <?php if($isi->f_menu214=='t') echo "checked"; ?> onclick="cek('214')"></td>
				<td class="bat" align="right">History Jual
					<input type="checkbox" id="fmenu216" name="fmenu216" <?php if($isi->f_menu216=='t') echo "checked"; ?> onclick="cek('216')"></td>
				<td class="bat" align="right">Mutasi
					<input type="checkbox" id="fmenu237" name="fmenu237" <?php if($isi->f_menu237=='t') echo "checked"; ?> onclick="cek('237')"></td>
				<td class="bat" align="right">SJP not Receive
					<input type="checkbox" id="fmenu252" name="fmenu252" <?php if($isi->f_menu252=='t') echo "checked"; ?> onclick="cek('252')"></td>
				<td class="bat" align="right">Master Pelanggan
					<input type="checkbox" id="fmenu254" name="fmenu254" <?php if($isi->f_menu254=='t') echo "checked"; ?> onclick="cek('254')"></td>
				<td class="bat" align="right">Debet Nota
					<input type="checkbox" id="fmenu255" name="fmenu255" <?php if($isi->f_menu255=='t') echo "checked"; ?> onclick="cek('255')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">SJP	GIT
					<input type="checkbox" id="fmenu259" name="fmenu259" <?php if($isi->f_menu259=='t') echo "checked"; ?> onclick="cek('259')"></td>
				<td class="bat" align="right">Daftar Bayar
					<input type="checkbox" id="fmenu263" name="fmenu263" <?php if($isi->f_menu263=='t') echo "checked"; ?> onclick="cek('263')"></td>
				<td class="bat" align="right">Daftar FKOM
					<input type="checkbox" id="fmenu269" name="fmenu269" <?php if($isi->f_menu269=='t') echo "checked"; ?> onclick="cek('269')"></td>
				<td class="bat" align="right">Nota Per Salesman
					<input type="checkbox" id="fmenu270" name="fmenu270" <?php if($isi->f_menu270=='t') echo "checked"; ?> onclick="cek('270')"></td>
				<td class="bat" align="right">Pengiriman Barang
					<input type="checkbox" id="fmenu277" name="fmenu277" <?php if($isi->f_menu277=='t') echo "checked"; ?> onclick="cek('277')"></td>
				<td class="bat" align="right">SJ Pusat to Cbg
					<input type="checkbox" id="fmenu280" name="fmenu280" <?php if($isi->f_menu280=='t') echo "checked"; ?> onclick="cek('280')"></td>
				<td class="bat" align="right">SPB Blm Jd Nota
					<input type="checkbox" id="fmenu283" name="fmenu283" <?php if($isi->f_menu283=='t') echo "checked"; ?> onclick="cek('283')"></td>
			  </tr>
		    <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Lebih Bayar
					<input type="checkbox" id="fmenu290" name="fmenu290" <?php if($isi->f_menu290=='t') echo "checked"; ?> onclick="cek('290')"></td>
				<td class="bat" align="right">Daftar Barang
					<input type="checkbox" id="fmenu291" name="fmenu291" <?php if($isi->f_menu291=='t') echo "checked"; ?> onclick="cek('291')"></td>
				<td class="bat" align="right">Penjualan Kons(spg)
					<input type="checkbox" id="fmenu294" name="fmenu294" <?php if($isi->f_menu294=='t') echo "checked"; ?> onclick="cek('294')"></td>
				<td class="bat" align="right">Order Kons(spg)
					<input type="checkbox" id="fmenu296" name="fmenu296" <?php if($isi->f_menu296=='t') echo "checked"; ?> onclick="cek('296')"></td>
				<td class="bat" align="right">SJPB Receive
					<input type="checkbox" id="fmenu300" name="fmenu300" <?php if($isi->f_menu300=='t') echo "checked"; ?> onclick="cek('300')"></td>
				<td class="bat" align="right">SJPB Retur
					<input type="checkbox" id="fmenu302" name="fmenu302" <?php if($isi->f_menu302=='t') echo "checked"; ?> onclick="cek('302')"></td>
				<td class="bat" align="right">SJPB
					<input type="checkbox" id="fmenu317" name="fmenu317" <?php if($isi->f_menu317=='t') echo "checked"; ?> onclick="cek('317')"></td>
			  </tr>
        <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">BON Kons(pst)
					<input type="checkbox" id="fmenu306" name="fmenu306" <?php if($isi->f_menu306=='t') echo "checked"; ?> onclick="cek('306')"></td>
				<td class="bat" align="right">Order Kons(pst)
					<input type="checkbox" id="fmenu307" name="fmenu307" <?php if($isi->f_menu307=='t') echo "checked"; ?> onclick="cek('307')"></td>
				<td class="bat" align="right">SJPB Retur Receive
					<input type="checkbox" id="fmenu309" name="fmenu309" <?php if($isi->f_menu309=='t') echo "checked"; ?> onclick="cek('309')"></td>
				<td class="bat" align="right">SJB Retur
					<input type="checkbox" id="fmenu312" name="fmenu312" <?php if($isi->f_menu312=='t') echo "checked"; ?> onclick="cek('312')"></td>
				<td class="bat" align="right">SJB Retur Receive
					<input type="checkbox" id="fmenu314" name="fmenu314" <?php if($isi->f_menu314=='t') echo "checked"; ?> onclick="cek('314')"></td>
				<td class="bat" align="right">Master Barang(cbg)
					<input type="checkbox" id="fmenu308" name="fmenu308" <?php if($isi->f_menu308=='t') echo "checked"; ?> onclick="cek('308')"></td>
				<td class="bat" align="right">Nota(cbg)
					<input type="checkbox" id="fmenu325" name="fmenu325" <?php if($isi->f_menu325=='t') echo "checked"; ?> onclick="cek('325')"></td>
			  </tr>
        <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">SPB Pending
					<input type="checkbox" id="fmenu327" name="fmenu327" <?php if($isi->f_menu327=='t') echo "checked"; ?> onclick="cek('327')"></td>
				<td class="bat" align="right">Hitung Plafon
					<input type="checkbox" id="fmenu328" name="fmenu328" <?php if($isi->f_menu328=='t') echo "checked"; ?> onclick="cek('328')"></td>
				<td class="bat" align="right">Hitung Insentif
					<input type="checkbox" id="fmenu309" name="fmenu329" <?php if($isi->f_menu329=='t') echo "checked"; ?> onclick="cek('329')"></td>
				<td class="bat" align="right">Tanda Terima SJ
					<input type="checkbox" id="fmenu312" name="fmenu312" <?php if($isi->f_menu312=='t') echo "checked"; ?> onclick="cek('312')"></td>
				<td class="bat" align="right">Penjualan/Salesman
					<input type="checkbox" id="fmenu359" name="fmenu359" <?php if($isi->f_menu359=='t') echo "checked"; ?> onclick="cek('359')"></td>
				<td class="bat" align="right">Sales to Market/Kota
					<input type="checkbox" id="fmenu360" name="fmenu360" <?php if($isi->f_menu360=='t') echo "checked"; ?> onclick="cek('360')"></td>
				<td class="bat" align="right">Penjualan/Produk
					<input type="checkbox" id="fmenu361" name="fmenu361" <?php if($isi->f_menu361=='t') echo "checked"; ?> onclick="cek('361')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Penjualan/Pelanggan SPB
					<input type="checkbox" id="fmenu362" name="fmenu362" <?php if($isi->f_menu362=='t') echo "checked"; ?> onclick="cek('362')"></td>
				<td class="bat" align="right">SJ Belum DKB
					<input type="checkbox" id="fmenu363" name="fmenu363" <?php if($isi->f_menu363=='t') echo "checked"; ?> onclick="cek('363')"></td>
				<td class="bat" align="right">Penjualan/Produk SPB
					<input type="checkbox" id="fmenu364" name="fmenu364" <?php if($isi->f_menu364=='t') echo "checked"; ?> onclick="cek('364')"></td>
				<td class="bat" align="right">Penjualan Per Pelanggan
					<input type="checkbox" id="fmenu292" name="fmenu292" <?php if($isi->f_menu292=='t') echo "checked"; ?> onclick="cek('292')"></td>
				<td class="bat" align="right">Penjualan Per Nota
					<input type="checkbox" id="fmenu371" name="fmenu371" <?php if($isi->f_menu371=='t') echo "checked"; ?> onclick="cek('371')"></td>
				<td class="bat" align="right">Penjualan Per Produk/th
					<input type="checkbox" id="fmenu372" name="fmenu372" <?php if($isi->f_menu372=='t') echo "checked"; ?> onclick="cek('372')"></td>
				<td class="bat" align="right">DO All Area
					<input type="checkbox" id="fmenu375" name="fmenu375" <?php if($isi->f_menu375=='t') echo "checked"; ?> onclick="cek('375')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Terima SPB
          <input type="checkbox" id="fmenu376" name="fmenu376" <?php if($isi->f_menu376=='t') echo "checked"; ?> onclick="cek('376')"></td>
				<td class="bat" align="right">Pengiriman SJP
          <input type="checkbox" id="fmenu380" name="fmenu380" <?php if($isi->f_menu380=='t') echo "checked"; ?> onclick="cek('380')"></td>
				<td class="bat" align="right">SJP Beda Terima
          <input type="checkbox" id="fmenu383" name="fmenu383" <?php if($isi->f_menu383=='t') echo "checked"; ?> onclick="cek('383')"></td>
				<td class="bat" align="right">Bayar Hutang Dagang
          <input type="checkbox" id="fmenu385" name="fmenu385" <?php if($isi->f_menu385=='t') echo "checked"; ?> onclick="cek('385')"></td>
				<td class="bat" align="right">Lebih Bayar Hutang Dagang
          <input type="checkbox" id="fmenu386" name="fmenu386" <?php if($isi->f_menu386=='t') echo "checked"; ?> onclick="cek('386')"></td>
				<td class="bat" align="right">Penjualan/Divisi
          <input type="checkbox" id="fmenu387" name="fmenu387" <?php if($isi->f_menu387=='t') echo "checked"; ?> onclick="cek('387')"></td>
				<td class="bat" align="right">Aging Piutang
          <input type="checkbox" id="fmenu388" name="fmenu388" <?php if($isi->f_menu388=='t') echo "checked"; ?> onclick="cek('388')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">Opname Nota Hutang
          <input type="checkbox" id="fmenu389" name="fmenu389" <?php if($isi->f_menu389=='t') echo "checked"; ?> onclick="cek('389')"></td>
				<td class="bat" align="right">Kartu Hutang
          <input type="checkbox" id="fmenu390" name="fmenu390" <?php if($isi->f_menu390=='t') echo "checked"; ?> onclick="cek('390')"></td>
				<td class="bat" align="right">HPP
          <input type="checkbox" id="fmenu391" name="fmenu391" <?php if($isi->f_menu391=='t') echo "checked"; ?> onclick="cek('391')"></td>
				<td class="bat" align="right">History Nota (DT)
					<input type="checkbox" id="fmenu396" name="fmenu396" <?php if($isi->f_menu396=='t') echo "checked"; ?> onclick="cek('396')"></td>
				<td class="bat" align="right">SJP Nasional
					<input type="checkbox" id="fmenu398" name="fmenu398" <?php if($isi->f_menu398=='t') echo "checked"; ?> onclick="cek('398')"></td>
				<td class="bat" align="right">SPB/Divisi
					<input type="checkbox" id="fmenu403" name="fmenu403" <?php if($isi->f_menu403=='t') echo "checked"; ?> onclick="cek('403')"></td>
				<td class="bat" align="right">Customer List
					<input type="checkbox" id="fmenu405" name="fmenu405" <?php if($isi->f_menu405=='t') echo "checked"; ?> onclick="cek('405')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">List Customer Pareto
          <input type="checkbox" id="fmenu413" name="fmenu413" <?php if($isi->f_menu413=='t') echo "checked"; ?> onclick="cek('413')"></td>
				<td class="bat" align="right">Target Toko Pareto
          <input type="checkbox" id="fmenu414" name="fmenu414" <?php if($isi->f_menu414=='t') echo "checked"; ?> onclick="cek('414')"></td>
				<td class="bat" align="right">Penjualan Pareto
          <input type="checkbox" id="fmenu415" name="fmenu415" <?php if($isi->f_menu415=='t') echo "checked"; ?> onclick="cek('415')"></td>
				<td class="bat" align="right">Cetak Target Penjualan
          <input type="checkbox" id="fmenu421" name="fmenu421" <?php if($isi->f_menu421=='t') echo "checked"; ?> onclick="cek('421')"></td>
				<td class="bat" align="right">Lap Penjualan/Bulan
          <input type="checkbox" id="fmenu422" name="fmenu422" <?php if($isi->f_menu422=='t') echo "checked"; ?> onclick="cek('422')"></td>
				<td class="bat" align="right">BON Masuk
          <input type="checkbox" id="fmenu426" name="fmenu426" <?php if($isi->f_menu426=='t') echo "checked"; ?> onclick="cek('426')"></td>
				<td class="bat" align="right">BON Keluar
          <input type="checkbox" id="fmenu427" name="fmenu427" <?php if($isi->f_menu427=='t') echo "checked"; ?> onclick="cek('427')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">SPB Kurang Pemenuhan (Sdh Nota)
          <input type="checkbox" id="fmenu431" name="fmenu431" <?php if($isi->f_menu431=='t') echo "checked"; ?> onclick="cek('431')"></td>
				<td class="batas bat" align="right">Pelunasan AP/Nota
          <input type="checkbox" id="fmenu434" name="fmenu444" <?php if($isi->f_menu434=='t') echo "checked"; ?> onclick="cek('434')"></td>
				<td class="batas bat" align="right">Nota vs Pelunasan (PB)
          <input type="checkbox" id="fmenu435" name="fmenu435" <?php if($isi->f_menu435=='t') echo "checked"; ?> onclick="cek('435')"></td>
				<td class="batas bat" align="right">DKB Approve
          <input type="checkbox" id="fmenu439" name="fmenu439" <?php if($isi->f_menu439=='t') echo "checked"; ?> onclick="cek('439')"></td>
				<td class="batas bat" align="right">Retur vs Omset
          <input type="checkbox" id="fmenu440" name="fmenu440" <?php if($isi->f_menu440=='t') echo "checked"; ?> onclick="cek('440')"></td>
				<td class="batas bat" align="right">Rekap SPB
          <input type="checkbox" id="fmenu450" name="fmenu450" <?php if($isi->f_menu450=='t') echo "checked"; ?> onclick="cek('450')"></td>
				<td class="batas bat" align="right">Penjualan Konsinyasi/Hari
          <input type="checkbox" id="fmenu451" name="fmenu451" <?php if($isi->f_menu451=='t') echo "checked"; ?> onclick="cek('451')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Cetak</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">SPB
					<input type="checkbox" id="fmenu62" name="fmenu62" <?php if($isi->f_menu62=='t') echo "checked"; ?> onclick="cek('62')"></td>
				<td class="bat" align="right">OP
					<input type="checkbox" id="fmenu63" name="fmenu63" <?php if($isi->f_menu63=='t') echo "checked"; ?> onclick="cek('63')"></td>
				<td class="bat" align="right">Nota
					<input type="checkbox" id="fmenu64" name="fmenu64" <?php if($isi->f_menu64=='t') echo "checked"; ?> onclick="cek('64')"></td>
				<td class="bat" align="right">SPMB
					<input type="checkbox" id="fmenu69" name="fmenu69" <?php if($isi->f_menu69=='t') echo "checked"; ?> onclick="cek('69')"></td>
				<td class="bat" align="right">Koreksi Nota
					<input type="checkbox" id="fmenu83" name="fmenu83" <?php if($isi->f_menu83=='t') echo "checked"; ?> onclick="cek('83')"></td>
				<td class="bat" align="right">Surat Jalan
					<input type="checkbox" id="fmenu104" name="fmenu104" <?php if($isi->f_menu104=='t') echo "checked"; ?> onclick="cek('104')"></td>
				<td class="bat" align="right">BBM-AP
					<input type="checkbox" id="fmenu162" name="fmenu162" <?php if($isi->f_menu162=='t') echo "checked"; ?> onclick="cek('162')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">DKB
					<input type="checkbox" id="fmenu164" name="fmenu164" <?php if($isi->f_menu164=='t') echo "checked"; ?> onclick="cek('164')"></td>
				<td class="bat" align="right">BAPB
					<input type="checkbox" id="fmenu139" name="fmenu139" <?php if($isi->f_menu139=='t') echo "checked"; ?> onclick="cek('139')"></td>
				<td class="bat" align="right">SJP
					<input type="checkbox" id="fmenu175" name="fmenu175" <?php if($isi->f_menu175=='t') echo "checked"; ?> onclick="cek('175')"></td>
				<td class="bat" align="right">KN Retur
					<input type="checkbox" id="fmenu178" name="fmenu178" <?php if($isi->f_menu178=='t') echo "checked"; ?> onclick="cek('178')"></td>
				<td class="bat" align="right">F Komersial
					<input type="checkbox" id="fmenu186" name="fmenu186" <?php if($isi->f_menu186=='t') echo "checked"; ?> onclick="cek('186')"></td>
				<td class="bat" align="right">F Pajak Std
					<input type="checkbox" id="fmenu192" name="fmenu192" <?php if($isi->f_menu192=='t') echo "checked"; ?> onclick="cek('192')"></td>
				<td class="bat" align="right">Daftar Tagih
					<input type="checkbox" id="fmenu176" name="fmenu176" <?php if($isi->f_menu176=='t') echo "checked"; ?> onclick="cek('176')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">KU
					<input type="checkbox" id="fmenu199" name="fmenu199" <?php if($isi->f_menu199=='t') echo "checked"; ?> onclick="cek('199')"></td>
				<td class="bat" align="right">Giro
					<input type="checkbox" id="fmenu200" name="fmenu200" <?php if($isi->f_menu200=='t') echo "checked"; ?> onclick="cek('200')"></td>
				<td class="bat" align="right">LKH
					<input type="checkbox" id="fmenu201" name="fmenu201" <?php if($isi->f_menu201=='t') echo "checked"; ?> onclick="cek('201')"></td>
				<td class="bat" align="right">IKHP
					<input type="checkbox" id="fmenu205" name="fmenu205" <?php if($isi->f_menu205=='t') echo "checked"; ?> onclick="cek('205')"></td>
				<td class="bat" align="right">Opname Nota Per Pelanggan
					<input type="checkbox" id="fmenu228" name="fmenu228" <?php if($isi->f_menu228=='t') echo "checked"; ?> onclick="cek('228')"></td>
				<td class="bat" align="right">Opname Nota Per Area
					<input type="checkbox" id="fmenu229" name="fmenu229" <?php if($isi->f_menu229=='t') echo "checked"; ?> onclick="cek('229')"></td>
        <td class="bat" align="right">BBM Retur
					<input type="checkbox" id="fmenu247" name="fmenu247" <?php if($isi->f_menu247=='t') echo "checked"; ?> onclick="cek('247')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">&nbsp;</td>
				<td class="bat" width="2px">&nbsp;</td>
				<td class="bat" align="right">SJ Retur
					<input type="checkbox" id="fmenu248" name="fmenu248" <?php if($isi->f_menu248=='t') echo "checked"; ?> onclick="cek('248')"></td>
				<td class="bat" align="right">Nota Retur
					<input type="checkbox" id="fmenu251" name="fmenu251" <?php if($isi->f_menu251=='t') echo "checked"; ?> onclick="cek('251')"></td>
				<td class="bat" align="right">BAPB SJP
					<input type="checkbox" id="fmenu262" name="fmenu262" <?php if($isi->f_menu262=='t') echo "checked"; ?> onclick="cek('262')"></td>
				<td class="bat" align="right">Pelanggan Baru
					<input type="checkbox" id="fmenu284" name="fmenu284" <?php if($isi->f_menu284=='t') echo "checked"; ?> onclick="cek('284')"></td>
				<td class="bat" align="right">SPB Pelanggan Baru
					<input type="checkbox" id="fmenu285" name="fmenu285" <?php if($isi->f_menu285=='t') echo "checked"; ?> onclick="cek('285')"></td>
				<td class="bat" align="right">SJ Retur Toko
					<input type="checkbox" id="fmenu297" name="fmenu297" <?php if($isi->f_menu297=='t') echo "checked"; ?> onclick="cek('297')"></td>
				<td class="bat" align="right">SJ Khusus
					<input type="checkbox" id="fmenu345" name="fmenu345" <?php if($isi->f_menu345=='t') echo "checked"; ?> onclick="cek('345')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">SPB Khusus
					<input type="checkbox" id="fmenu353" name="fmenu353" <?php if($isi->f_menu353=='t') echo "checked"; ?> onclick="cek('353')"></td>
				<td class="batas bat" align="right">BBK hadiah
					<input type="checkbox" id="fmenu355" name="fmenu355" <?php if($isi->f_menu355=='t') echo "checked"; ?> onclick="cek('355')"></td>
				<td class="batas bat" align="right">DKB Khusus
					<input type="checkbox" id="fmenu358" name="fmenu358" <?php if($isi->f_menu358=='t') echo "checked"; ?> onclick="cek('358')"></td>
				<td class="batas bat" align="right">BAPB Khusus
					<input type="checkbox" id="fmenu373" name="fmenu373" <?php if($isi->f_menu373=='t') echo "checked"; ?> onclick="cek('373')"></td>
				<td class="batas bat" align="right">FK (Pajak Pengganti)
					<input type="checkbox" id="fmenu367" name="fmenu367" <?php if($isi->f_menu367=='t') echo "checked"; ?> onclick="cek('367')"></td>
				<td class="batas bat" align="right">OP Rekap SPB
					<input type="checkbox" id="fmenu384" name="fmenu384" <?php if($isi->f_menu384=='t') echo "checked"; ?> onclick="cek('384')"></td>
				<td class="batas bat" align="right">KN Non Retur
					<input type="checkbox" id="fmenu436" name="fmenu436" <?php if($isi->f_menu436=='t') echo "checked"; ?> onclick="cek('436')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">DO</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Transfer
					<input type="checkbox" id="fmenu65" name="fmenu65" <?php if($isi->f_menu65=='t') echo "checked"; ?> onclick="cek('65')"></td>
				<td class="batas bat" align="right">Upload
					<input type="checkbox" id="fmenu66" name="fmenu66" <?php if($isi->f_menu66=='t') echo "checked"; ?> onclick="cek('66')"></td>
				<td class="batas bat" align="right">DO Manual
					<input type="checkbox" id="fmenu67" name="fmenu67" <?php if($isi->f_menu67=='t') echo "checked"; ?> onclick="cek('67')"></td>
				<td class="batas bat" align="right">Transfer DO Other
					<input type="checkbox" id="fmenu222" name="fmenu222" <?php if($isi->f_menu222=='t') echo "checked"; ?> onclick="cek('222')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
        <tr>
				<td class="bat" width="50px">Pembelian</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Transfer Harga Baru
					<input type="checkbox" id="fmenu240" name="fmenu240" <?php if($isi->f_menu240=='t') echo "checked"; ?> onclick="cek('240')"></td>
				<td class="bat" align="right">Upload Harga Baru

					<input type="checkbox" id="fmenu241" name="fmenu241" <?php if($isi->f_menu241=='t') echo "checked"; ?> onclick="cek('241')"></td>
				<td class="bat" align="right">Daftar Harga Barang
					<input type="checkbox" id="fmenu244" name="fmenu244" <?php if($isi->f_menu244=='t') echo "checked"; ?> onclick="cek('244')"></td>
				<td class="bat" align="right">Transfer OP
					<input type="checkbox" id="fmenu278" name="fmenu278" <?php if($isi->f_menu278=='t') echo "checked"; ?> onclick="cek('278')"></td>
        <td class="bat" align="right">Transfer DO
					<input type="checkbox" id="fmenu279" name="fmenu279" <?php if($isi->f_menu279=='t') echo "checked"; ?> onclick="cek('279')"></td>
				<td class="bat" align="right">Transfer OP to Supplier
					<input type="checkbox" id="fmenu281" name="fmenu281" <?php if($isi->f_menu281=='t') echo "checked"; ?> onclick="cek('281')"></td>
				<td class="bat" align="right">Daftar Kontra Bon
					<input type="checkbox" id="fmenu286" name="fmenu286" <?php if($isi->f_menu286=='t') echo "checked"; ?> onclick="cek('286')"></td>
			  </tr>
        <tr>
				<td class="batas bat" width="50px">
				<td class="batas bat" width="2px"></td>
				<td class="batas bat" align="right">SPmB BBM-AP
					<input type="checkbox" id="fmenu331" name="fmenu331" <?php if($isi->f_menu331=='t') echo "checked"; ?> onclick="cek('331')"></td>
				<td class="batas bat" align="right">Hutang Dagang BBM-AP
					<input type="checkbox" id="fmenu332" name="fmenu332" <?php if($isi->f_menu332=='t') echo "checked"; ?> onclick="cek('332')"></td>
				<td class="batas bat" align="right">List Lap OP vs DO
					<input type="checkbox" id="fmenu333" name="fmenu333" <?php if($isi->f_menu333=='t') echo "checked"; ?> onclick="cek('333')"></td>
				<td class="batas bat" align="right">Exp Lap OP vs DO
					<input type="checkbox" id="fmenu334" name="fmenu334" <?php if($isi->f_menu334=='t') echo "checked"; ?> onclick="cek('334')"></td>
				<td class="batas bat" align="right">Laporan Pembelian
					<input type="checkbox" id="fmenu339" name="fmenu339" <?php if($isi->f_menu339=='t') echo "checked"; ?> onclick="cek('339')"></td>
				<td class="batas bat" align="right">Transfer Hutang Dagang
					<input type="checkbox" id="fmenu374" name="fmenu374" <?php if($isi->f_menu374=='t') echo "checked"; ?> onclick="cek('374')"></td>
				<td class="batas bat" align="right">Price List
					<input type="checkbox" id="fmenu397" name="fmenu397" <?php if($isi->f_menu397=='t') echo "checked"; ?> onclick="cek('397')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">Administrator</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">User Admin
					<input type="checkbox" id="fmenu0" name="fmenu0" <?php if($isi->f_menu0=='t') echo "checked"; ?> onclick="cek('0')"></td>
				<td class="batas bat" align="right">Printer Setting
					<input type="checkbox" id="fmenu93" name="fmenu93" <?php if($isi->f_menu93=='t') echo "checked"; ?> onclick="cek('93')"></td>
				<td class="batas bat" align="right">Ganti Password
					<input type="checkbox" id="fmenu68" name="fmenu68" <?php if($isi->f_menu68=='t') echo "checked"; ?> onclick="cek('68')"></td>
        <td class="batas bat" align="right">User Log
          <input type="checkbox" id="fmenu232" name="fmenu232" <?php if($isi->f_menu232=='t') echo "checked"; ?> onclick="cek('232')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Akunting</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Master Saldo Account
					<input type="checkbox" id="fmenu115" name="fmenu115" <?php if($isi->f_menu115=='t') echo "checked"; ?> onclick="cek('115')"></td>
				<td class="bat" align="right">CoA
					<input type="checkbox" id="fmenu118" name="fmenu118" <?php if($isi->f_menu118=='t') echo "checked"; ?> onclick="cek('118')"></td>
				<td class="bat" align="right">Jurnal Umum
					<input type="checkbox" id="fmenu125" name="fmenu125" <?php if($isi->f_menu125=='t') echo "checked"; ?> onclick="cek('125')"></td>
				<td class="bat" align="right">Jurnal Trans Harian
					<input type="checkbox" id="fmenu129" name="fmenu129" <?php if($isi->f_menu129=='t') echo "checked"; ?> onclick="cek('129')"></td>
				<td class="bat" align="right">Buku Besar
					<input type="checkbox" id="fmenu130" name="fmenu130" <?php if($isi->f_menu130=='t') echo "checked"; ?> onclick="cek('130')"></td>
				<td class="bat" align="right">Neraca Saldo
					<input type="checkbox" id="fmenu131" name="fmenu131" <?php if($isi->f_menu131=='t') echo "checked"; ?> onclick="cek('131')"></td>
				<td class="bat" align="right">Neraca
					<input type="checkbox" id="fmenu132" name="fmenu132" <?php if($isi->f_menu132=='t') echo "checked"; ?> onclick="cek('132')"></td>
			  </tr>
			  <tr>	
				<td class="bat" width="50px">
				<td class="bat" width="2px"></td>
				<td class="bat" align="right">Laba Rugi
					<input type="checkbox" id="fmenu133" name="fmenu133" <?php if($isi->f_menu133=='t') echo "checked"; ?> onclick="cek('133')"></td>
				<td class="bat" align="right">Kas Kecil
					<input type="checkbox" id="fmenu146" name="fmenu146" <?php if($isi->f_menu146=='t') echo "checked"; ?> onclick="cek('146')"></td>
				<td class="bat" align="right">Kas Kecil Multi Row
					<input type="checkbox" id="fmenu231" name="fmenu231" <?php if($isi->f_menu231=='t') echo "checked"; ?> onclick="cek('231')"></td>
				<td class="bat" align="right">Pengisian Kas Kecil
					<input type="checkbox" id="fmenu151" name="fmenu151" <?php if($isi->f_menu151=='t') echo "checked"; ?> onclick="cek('151')"></td>
				<td class="bat" align="right">Transfer KK
					<input type="checkbox" id="fmenu154" name="fmenu154" <?php if($isi->f_menu154=='t') echo "checked"; ?> onclick="cek('154')"></td>
				<td class="bat" align="right">Transfer KK (Cab)
					<input type="checkbox" id="fmenu155" name="fmenu155" <?php if($isi->f_menu155=='t') echo "checked"; ?> onclick="cek('155')"></td>
				<td class="bat" align="right">KUM Cabang
					<input type="checkbox" id="fmenu230" name="fmenu230" <?php if($isi->f_menu230=='t') echo "checked"; ?> onclick="cek('230')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
        <tr>	
				<td class="batas bat" width="50px">
				<td class="batas bat" width="2px"></td>
        <td class="batas bat" align="right">Close Periode KK
					<input type="checkbox" id="fmenu242" name="fmenu242" <?php if($isi->f_menu242=='t') echo "checked"; ?> onclick="cek('242')"></td>
        <td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
        <td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
        <td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
        </tr>
			  <tr>
				<td class="bat" width="50px">Laporan</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Export Kas Kecil
					<input type="checkbox" id="fmenu150" name="fmenu150" <?php if($isi->f_menu150=='t') echo "checked"; ?> onclick="cek('150')"></td>
				<td class="bat" align="right">Export Mutasi
					<input type="checkbox" id="fmenu227" name="fmenu227" <?php if($isi->f_menu227=='t') echo "checked"; ?> onclick="cek('227')"></td>
				<td class="bat" align="right">Export SPmB
					<input type="checkbox" id="fmenu233" name="fmenu233" <?php if($isi->f_menu233=='t') echo "checked"; ?> onclick="cek('233')"></td>
				<td class="bat" align="right">Export IKHP
					<input type="checkbox" id="fmenu235" name="fmenu235" <?php if($isi->f_menu235=='t') echo "checked"; ?> onclick="cek('235')"></td>
				<td class="bat" align="right">Export Giro Pelanggan
					<input type="checkbox" id="fmenu236" name="fmenu236" <?php if($isi->f_menu236=='t') echo "checked"; ?> onclick="cek('236')"></td>
				<td class="bat" align="right">Export Opname Nota
					<input type="checkbox" id="fmenu238" name="fmenu238" <?php if($isi->f_menu238=='t') echo "checked"; ?> onclick="cek('238')"></td>
				<td class="bat" align="right">Rekap SJ
					<input type="checkbox" id="fmenu264" name="fmenu264" <?php if($isi->f_menu264=='t') echo "checked"; ?> onclick="cek('264')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px"></td>
				<td class="bat" width="2px"></td>
				<td class="bat" align="right">Export Faktur Komersial
					<input type="checkbox" id="fmenu268" name="fmenu268" <?php if($isi->f_menu268=='t') echo "checked"; ?> onclick="cek('268')"></td>
				<td class="bat" align="right">Export SJP GIT
					<input type="checkbox" id="fmenu274" name="fmenu274" <?php if($isi->f_menu274=='t') echo "checked"; ?> onclick="cek('274')"></td>
				<td class="bat" align="right">Export DT
					<input type="checkbox" id="fmenu275" name="fmenu275" <?php if($isi->f_menu275=='t') echo "checked"; ?> onclick="cek('275')"></td>
				<td class="bat" align="right">Export IKHP Pengeluaran
					<input type="checkbox" id="fmenu276" name="fmenu276" <?php if($isi->f_menu276=='t') echo "checked"; ?> onclick="cek('276')"></td>
				<td class="bat" align="right">Export IKHP Non Cek
					<input type="checkbox" id="fmenu282" name="fmenu282" <?php if($isi->f_menu282=='t') echo "checked"; ?> onclick="cek('282')"></td>
				<td class="bat" align="right">Export Transfer Uang Masuk
					<input type="checkbox" id="fmenu287" name="fmenu287" <?php if($isi->f_menu287=='t') echo "checked"; ?> onclick="cek('287')"></td>
				<td class="bat" align="right">Export Lap. AP
					<input type="checkbox" id="fmenu316" name="fmenu316" <?php if($isi->f_menu316=='t') echo "checked"; ?> onclick="cek('316')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px"></td>
				<td class="bat" width="2px"></td>
				<td class="bat" align="right">Export Container
					<input type="checkbox" id="fmenu326" name="fmenu326" <?php if($isi->f_menu326=='t') echo "checked"; ?> onclick="cek('326')"></td>
				<td class="bat" align="right">Update Penjualan(Insentif)
					<input type="checkbox" id="fmenu340" name="fmenu340" <?php if($isi->f_menu340=='t') echo "checked"; ?> onclick="cek('340')"></td>
				<td class="bat" align="right">Export Lap Gudang Per Divisi
					<input type="checkbox" id="fmenu344" name="fmenu344" <?php if($isi->f_menu344=='t') echo "checked"; ?> onclick="cek('344')"></td>
				<td class="bat" align="right">Export Penjualan Kons
					<input type="checkbox" id="fmenu368" name="fmenu368" <?php if($isi->f_menu368=='t') echo "checked"; ?> onclick="cek('368')"></td>
				<td class="bat" align="right">Export Mutasi Kons
					<input type="checkbox" id="fmenu369" name="fmenu369" <?php if($isi->f_menu369=='t') echo "checked"; ?> onclick="cek('369')"></td>
				<td class="bat" align="right">Rekap RRKH
					<input type="checkbox" id="fmenu402" name="fmenu402" <?php if($isi->f_menu402=='t') echo "checked"; ?> onclick="cek('402')"></td>
				<td class="bat" align="right">Exp LKH All Area
					<input type="checkbox" id="fmenu409" name="fmenu409" <?php if($isi->f_menu409=='t') echo "checked"; ?> onclick="cek('409')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px"></td>
				<td class="batas bat" width="2px"></td>
				<td class="batas bat" align="right">Export KU Keluar
					<input type="checkbox" id="fmenu429" name="fmenu429" <?php if($isi->f_menu429=='t') echo "checked"; ?> onclick="cek('429')"></td>
				<td class="batas bat" align="right">Export OP
					<input type="checkbox" id="fmenu433" name="fmenu433" <?php if($isi->f_menu433=='t') echo "checked"; ?> onclick="cek('433')"></td>
				<td class="batas bat" align="right">Laporan SPB/Bulan
					<input type="checkbox" id="fmenu430" name="fmenu430" <?php if($isi->f_menu430=='t') echo "checked"; ?> onclick="cek('430')"></td>
				<td class="batas bat" align="right">Export SJ Retur
          <input type="checkbox" id="fmenu455" name="fmenu455" <?php if($isi->f_menu455=='t') echo "checked"; ?> onclick="cek('455')"></td>
				<td class="batas bat" align="right">Export SPMB Tidak Terealisasi
          <input type="checkbox" id="fmenu456" name="fmenu456" <?php if($isi->f_menu456=='t') echo "checked"; ?> onclick="cek('456')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
			  </tr>
				<tr>
				<td class="batas bat" width="50px">Kendaraan</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Master Kendaraan
					<input type="checkbox" id="fmenu144" name="fmenu144" <?php if($isi->f_menu144=='t') echo "checked"; ?> onclick="cek('144')"></td>
				<td class="batas bat" align="right">Ganti Periode Kendaraan
					<input type="checkbox" id="fmenu185" name="fmenu185" <?php if($isi->f_menu185=='t') echo "checked"; ?> onclick="cek('185')"></td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
				<td class="batas">&nbsp;</td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">Posting</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Pelunasan Piutang
					<input type="checkbox" id="fmenu119" name="fmenu119" <?php if($isi->f_menu119=='t') echo "checked"; ?> onclick="cek('119')"></td>
				<td class="batas bat" align="right">Penjualan
					<input type="checkbox" id="fmenu121" name="fmenu121" <?php if($isi->f_menu121=='t') echo "checked"; ?> onclick="cek('121')"></td>
				<td class="batas bat" align="right">KN
					<input type="checkbox" id="fmenu123" name="fmenu123" <?php if($isi->f_menu123=='t') echo "checked"; ?> onclick="cek('123')"></td>
				<td class="batas bat" align="right">Jurnal Umum
					<input type="checkbox" id="fmenu127" name="fmenu127" <?php if($isi->f_menu127=='t') echo "checked"; ?> onclick="cek('127')")></td>
				<td class="batas bat" align="right">Pelunasan Hutang
					<input type="checkbox" id="fmenu142" name="fmenu142" <?php if($isi->f_menu142=='t') echo "checked"; ?> onclick="cek('142')"></td>
				<td class="batas bat" align="right">Kas Kecil
					<input type="checkbox" id="fmenu148" name="fmenu148" <?php if($isi->f_menu148=='t') echo "checked"; ?> onclick="cek('148')"></td>
				<td class="batas bat" align="right">Pengisian KK
					<input type="checkbox" id="fmenu152" name="fmenu152" <?php if($isi->f_menu152=='t') echo "checked"; ?> onclick="cek('152')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">Unposting</td>
				<td class="batas bat" width="2px">:</td>
				<td class="batas bat" align="right">Pelunasan Piutang
					<input type="checkbox" id="fmenu120" name="fmenu120" <?php if($isi->f_menu120=='t') echo "checked"; ?> onclick="cek('120')"></td>
				<td class="batas bat" align="right">Penjualan
					<input type="checkbox" id="fmenu122" name="fmenu122" <?php if($isi->f_menu122=='t') echo "checked"; ?> onclick="cek('122')"></td>
				<td class="batas bat" align="right">KN
					<input type="checkbox" id="fmenu124" name="fmenu124" <?php if($isi->f_menu124=='t') echo "checked"; ?> onclick="cek('124')"></td>
				<td class="batas bat" align="right">Jurnal Umum
					<input type="checkbox" id="fmenu128" name="fmenu128" <?php if($isi->f_menu128=='t') echo "checked"; ?> onclick="cek('128')")></td>
				<td class="batas bat" align="right">Pelunasan Hutang
					<input type="checkbox" id="fmenu143" name="fmenu143" <?php if($isi->f_menu143=='t') echo "checked"; ?> onclick="cek('143')"></td>
				<td class="batas bat" align="right">Kas Kecil
					<input type="checkbox" id="fmenu149" name="fmenu149" <?php if($isi->f_menu149=='t') echo "checked"; ?> onclick="cek('149')"></td>
				<td class="batas bat" align="right">Pengisian KK
					<input type="checkbox" id="fmenu153" name="fmenu153" <?php if($isi->f_menu153=='t') echo "checked"; ?> onclick="cek('153')"></td>
			  </tr>
			  <tr>
				<td class="bat" width="50px">Keuangan</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Giro DGU
					<input type="checkbox" id="fmenu136" name="fmenu136" <?php if($isi->f_menu136=='t') echo "checked"; ?> onclick="cek('136')"></td>
				<td class="bat" align="right">KU / Transfer M
					<input type="checkbox" id="fmenu140" name="fmenu140" <?php if($isi->f_menu140=='t') echo "checked"; ?> onclick="cek('140')"></td>
				<td class="bat" align="right">Hutang Dagang
					<input type="checkbox" id="fmenu134" name="fmenu134" <?php if($isi->f_menu134=='t') echo "checked"; ?> onclick="cek('134')"></td>
				<td class="bat" align="right">Pelunasan Hutang
					<input type="checkbox" id="fmenu135" name="fmenu135" <?php if($isi->f_menu135=='t') echo "checked"; ?> onclick="cek('135')"></td>
				<td class="bat" align="right">KU / Transfer K
					<input type="checkbox" id="fmenu165" name="fmenu165" <?php if($isi->f_menu165=='t') echo "checked"; ?> onclick="cek('165')"></td>
				<td class="bat" align="right">Input IKHP
				    <input type="checkbox" id="fmenu171" name="fmenu171" <?php if($isi->f_menu171=='t') echo "checked"; ?> onclick="cek('171')"></td>
				<td class="bat" align="right">Approve Pelanggan	
            <input type="checkbox" id="fmenu226" name="fmenu226" <?php if($isi->f_menu226=='t') echo "checked"; ?> onclick="cek('226')"></td>
				<td class="batas">&nbsp;</td>
			  </tr>
        <tr>	
				<td class="bat" width="50px">
				<td class="bat" width="2px"></td>
        <td class="bat" align="right">Transfer Pelunasan Piutang
					<input type="checkbox" id="fmenu243" name="fmenu243" <?php if($isi->f_menu243=='t') echo "checked"; ?> onclick="cek('243')"></td>
        <td class="bat" align="right">Aging Piutang	
            <input type="checkbox" id="fmenu258" name="fmenu258" <?php if($isi->f_menu258=='t') echo "checked"; ?> onclick="cek('258')"></td>
				<td class="bat" align="right">Aging Pelunasan	
            <input type="checkbox" id="fmenu265" name="fmenu265" <?php if($isi->f_menu265=='t') echo "checked"; ?> onclick="cek('265')"></td>
        <td class="bat" align="right">Kartu Piutang/Periode	
            <input type="checkbox" id="fmenu266" name="fmenu266" <?php if($isi->f_menu266=='t') echo "checked"; ?> onclick="cek('266')"></td>
				<td class="bat" align="right">Kartu Piutang/Tanggal
            <input type="checkbox" id="fmenu267" name="fmenu267" <?php if($isi->f_menu267=='t') echo "checked"; ?> onclick="cek('267')"></td>
        <td class="bat" align="right">BBM Pending
					<input type="checkbox" id="fmenu273" name="fmenu273" <?php if($isi->f_menu273=='t') echo "checked"; ?> onclick="cek('273')"></td>
				<td class="bat" align="right">Opname KN/DN
					<input type="checkbox" id="fmenu304" name="fmenu304" <?php if($isi->f_menu304=='t') echo "checked"; ?> onclick="cek('304')"></td>
        </tr>
        <tr>
        <td class="bat" width="50px">
        <td class="bat" width="2px">
        <td class="bat" align="right">Daftar Bayar KN
					<input type="checkbox" id="fmenu313" name="fmenu313" <?php if($isi->f_menu313=='t') echo "checked"; ?> onclick="cek('313')"></td>
        <td class="bat" align="right">Export Lebih Bayar
					<input type="checkbox" id="fmenu303" name="fmenu303" <?php if($isi->f_menu303=='t') echo "checked"; ?> onclick="cek('303')"></td>
        <td class="bat" align="right">Export KN/DN
					<input type="checkbox" id="fmenu305" name="fmenu305" <?php if($isi->f_menu305=='t') echo "checked"; ?> onclick="cek('305')"></td>
				<td class="bat" align="right">Target Collection Cash
					<input type="checkbox" id="fmenu335" name="fmenu335" <?php if($isi->f_menu335=='t') echo "checked"; ?> onclick="cek('335')"></td>
				<td class="bat" align="right">Target Collection Credit
					<input type="checkbox" id="fmenu336" name="fmenu336" <?php if($isi->f_menu336=='t') echo "checked"; ?> onclick="cek('336')"></td>
				<td class="bat" align="right">List Target Collection Cash
					<input type="checkbox" id="fmenu337" name="fmenu337" <?php if($isi->f_menu337=='t') echo "checked"; ?> onclick="cek('337')"></td>
				<td class="bat" align="right">List Target Collection Credit
					<input type="checkbox" id="fmenu338" name="fmenu338" <?php if($isi->f_menu338=='t') echo "checked"; ?> onclick="cek('338')"></td>
        </tr>
        <tr>
        <td class="bat" width="50px">&nbsp;</td>
        <td class="bat" width="2px">&nbsp;</td>
        <td class="bat" align="right">Koreksi Nota Header
					<input type="checkbox" id="fmenu393" name="fmenu393" <?php if($isi->f_menu393=='t') echo "checked"; ?> onclick="cek('393')"></td>
        <td class="bat" align="right">Koreksi Nota Edit Detail
					<input type="checkbox" id="fmenu394" name="fmenu394" <?php if($isi->f_menu394=='t') echo "checked"; ?> onclick="cek('394')"></td>
        <td class="bat" align="right">Koreksi Nota Tambah Detail
					<input type="checkbox" id="fmenu395" name="fmenu395" <?php if($isi->f_menu395=='t') echo "checked"; ?> onclick="cek('395')"></td>
				<td class="bat" align="right">Cek Pelunasan Hutang
					<input type="checkbox" id="fmenu399" name="fmenu399" <?php if($isi->f_menu399=='t') echo "checked"; ?> onclick="cek('399')"></td>
				<td class="bat" align="right">Update Nota (Insentif/Masalah)
					<input type="checkbox" id="fmenu400" name="fmenu400" <?php if($isi->f_menu400=='t') echo "checked"; ?> onclick="cek('400')"></td>
				<td class="bat" align="right">Opname Nota Nasional
					<input type="checkbox" id="fmenu404" name="fmenu337" <?php if($isi->f_menu404=='t') echo "checked"; ?> onclick="cek('404')"></td>
				<td class="bat" align="right">DT Manual
					<input type="checkbox" id="fmenu318" name="fmenu318" <?php if($isi->f_menu318=='t') echo "checked"; ?> onclick="cek('318')"></td>
        </tr>
        <tr>
        <td class="batas bat" width="50px">&nbsp;</td>
        <td class="batas bat" width="2px">&nbsp;</td>
        <td class="batas bat" align="right">TTD Nota
					<input type="checkbox" id="fmenu417" name="fmenu417" <?php if($isi->f_menu417=='t') echo "checked"; ?> onclick="cek('417')"></td>
        <td class="batas bat" align="right">Ubah Ket diCek Pelunasan
					<input type="checkbox" id="fmenu418" name="fmenu418" <?php if($isi->f_menu418=='t') echo "checked"; ?> onclick="cek('418')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
        </tr>
			  <tr>
				<td class="bat" width="50px">Pajak</td>
				<td class="bat" width="2px">:</td>
				<td class="bat" align="right">Faktur Komersial
					<input type="checkbox" id="fmenu184" name="fmenu184" <?php if($isi->f_menu184=='t') echo "checked"; ?> onclick="cek('184')"></td>
				<td class="bat" align="right">Lap Buku Penjualan
					<input type="checkbox" id="fmenu324" name="fmenu324" <?php if($isi->f_menu324=='t') echo "checked"; ?> onclick="cek('324')"></td>
				<td class="bat" align="right">PPN Keluaran
					<input type="checkbox" id="fmenu351" name="fmenu351" <?php if($isi->f_menu351=='t') echo "checked"; ?> onclick="cek('351')"></td>
				<td class="bat" align="right">FK (Pajak Pengganti)
					<input type="checkbox" id="fmenu365" name="fmenu365" <?php if($isi->f_menu365=='t') echo "checked"; ?> onclick="cek('365')"></td>
				<td class="bat" align="right">Faktur Komersial (BBK Hadiah)
					<input type="checkbox" id="fmenu382" name="fmenu382" <?php if($isi->f_menu382=='t') echo "checked"; ?> onclick="cek('382')"></td>
				<td class="bat" align="right">Faktur Komersial (Auto)
					<input type="checkbox" id="fmenu377" name="fmenu377" <?php if($isi->f_menu377=='t') echo "checked"; ?> onclick="cek('377')"></td>
				<td class="bat" align="right">PJNA
					<input type="checkbox" id="fmenu378" name="fmenu378" <?php if($isi->f_menu378=='t') echo "checked"; ?> onclick="cek('378')"></td>
			  </tr>
			  <tr>
				<td class="batas bat" width="50px">&nbsp;</td>
				<td class="batas bat" width="2px">&nbsp;</td>
				<td class="batas bat" align="right">Approve Koreksi Nota
					<input type="checkbox" id="fmenu392" name="fmenu392" <?php if($isi->f_menu392=='t') echo "checked"; ?> onclick="cek('392')"></td>
				<td class="batas bat" align="right">Exp PPN Keluaran (CSV)
					<input type="checkbox" id="fmenu401" name="fmenu401" <?php if($isi->f_menu401=='t') echo "checked"; ?> onclick="cek('401')"></td>
				<td class="batas bat" align="right">Batal Faktur Pajak
					<input type="checkbox" id="fmenu411" name="fmenu411" <?php if($isi->f_menu411=='t') echo "checked"; ?> onclick="cek('411')"></td>
				<td class="batas bat" align="right">TTD Pajak
					<input type="checkbox" id="fmenu416" name="fmenu416" <?php if($isi->f_menu416=='t') echo "checked"; ?> onclick="cek('416')"></td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
				<td class="batas bat" align="right">&nbsp;</td>
			  </tr>
				</table>
			  </td>
	      	<tr>
			  <td width="100%" colspan="9" align="center">
			    <input name="pil" id="pil" value="Pilih Semua" type="button" onclick="pilihsemua()">
			    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
			    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("user/cform/","#main")'>
			    <input name="unpil" id="unpil" value="Hapus Semua" type="button" onclick="tidakpilihsemua()">
			  </td>
			</tr>
		  </table>
		</div>
	  </div>
	  </div>
	  <?=form_close()?>
    </td>
  </tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">
  window.onunload=function(){
	for(i=0;i<=439;i++){
	  if(document.getElementById("fmenu"+i).value==''){
		document.getElementById("fmenu"+i).value='f';
	  }else{
		document.getElementById("fmenu"+i).value='t';
	  }
	}
	if(document.getElementById("faktif").value==''){
		document.getElementById("faktif").value='f';
	}else{
		document.getElementById("faktif").value='t';
	}
  }
  function view_area(a){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/user/cform/area/"+a+"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function batal(){
	self.location="<?php echo site_url() ?>"+"/user/cform";
  }
  function pilihsemua(){
		var jml=439;
		for(i=0;i<=jml;i++){
			document.getElementById("fmenu"+i).checked=true;
			document.getElementById("fmenu"+i).value='t';
		}
	}
	function tidakpilihsemua(){
		var jml=439;
		for(i=0;i<=jml;i++){
			document.getElementById("fmenu"+i).checked=false;
			document.getElementById("fmenu"+i).value='f';
		}
	}
  function dipales(){
		cek='false';
		if((document.getElementById("iuser").value!='') &&
  	 	(document.getElementById("area1").value!='')) {
				cek='true';	
		}
		if(cek=='true'){
  		document.getElementById("login").disabled=true;
			document.getElementById("pil").disabled=true;
			document.getElementById("unpil").disabled=true;
    }else{
	   	document.getElementById("login").disabled=false;
			alert('Data masih ada yang salah !!!');
    }
  }
</script>
