<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'list-akt-bk-area/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="icoabank" name="icoabank" value="<?php echo $icoabank; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Area</th>
 	    <th>No bank</th>
			<th>Tgl bank</th>
			<th>Tgl Input</th>
			<th>CoA</th>
			<th>Keterangan</th>
			<th>Nilai</th>
			<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
				$tmp=explode('-',$row->d_bank);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_bank=$tgl.'-'.$bln.'-'.$thn;
				$dbank = $thn.$bln.$tgl;
				$thnbln = $thn . $bln;
				$row->d_entry = substr($row->d_entry, 0, 10);

				$tmp=explode('-',$row->d_entry);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_entry=$tgl.'-'.$bln.'-'.$thn;
				
				
			  echo "<tr> 
				  <td>$row->i_area - $row->e_area_name</td>";
				  if($row->f_kbank_cancel == 't') {
					echo "<td><h1>$row->i_kbank</h1></td>";
				  }else {
					echo "<td>$row->i_kbank</td>";
				  }
					echo "<td>$row->d_bank</td>
					<td>$row->d_entry</td>
				  <td>$row->i_coa</td>
				  <td>$row->e_description</td>
				  <td align=right>".number_format($row->v_bank)."</td>
				  <td class=\"action\">";
			  if($row->f_posting!='t'){
				  if($row->f_debet=='t'){
					if ($dbank>=$d_open_kbank) {
						echo "<a href=\"#\" onclick='show(\"akt-bk-area/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$row->i_coa_bank/$row->i_bank/$row->d_bank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						if($row->i_cek=='' && $row->f_kbank_cancel=='f' && $row->v_bank==$row->v_sisa && $bisaedit) echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-bk-area/cform/delete/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$row->i_coa_bank/$row->i_bank/$row->d_bank/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					}
				  }else{
					if ($dbank>=$d_open_kbankin) {
						echo "<a href=\"#\" onclick='show(\"akt-pbk-area/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$row->i_coa_bank/$row->i_bank/$row->d_bank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						if($row->i_cek==''  && $row->f_kbank_cancel=='f' && $row->v_bank==$row->v_sisa && $bisaedit) echo "&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"list-akt-bk-area/cform/delete/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$row->i_coa_bank/$row->i_bank/\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					}
				  }
			  }else{
				  if($row->f_debet=='t'){
					if ($dbank>=$d_open_kbank) {
						echo "<a href=\"#\" onclick='show(\"akt-bk-area/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$row->i_coa_bank/$row->i_bank/$row->d_bank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				  }else{
					if ($dbank>=$d_open_kbankin) {
						echo "<a href=\"#\" onclick='show(\"akt-pbk-area/cform/edit/$row->i_kbank/$row->i_periode/$row->i_area/$dfrom/$dto/$lepel/$row->i_coa_bank/$row->i_bank/$row->d_bank/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				  }
			  }
			  echo "
				  </td>
				</tr>";
			}
		   }
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	   <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

</script>
