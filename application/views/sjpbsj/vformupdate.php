<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
    <tr>
        <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url'=>'sjpbsj/cform/simpan','update'=>'#pesan','type'=>'post'));?>
            <div id="spbformupdate">
                <div class="effect">
                    <div class="accordion2">
                        <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
                            <tr>
                                <?php 
                            $tmp=explode("-",$header->d_sjpb_receive);
                            $th=$tmp[2];
                            $bl=$tmp[1];
                            $hr=$tmp[0];
                            $header->d_sjpb_receive=$th."-".$bl."-".$hr;

                            ?>
                                <td>Tgl SPB</td>
                                <td><input readonly id="ispb" name="ispb" type="text" value="">
                                    <input readonly id="dspb" name="dspb" value="<?= $header->d_sjpb_receive; ?>"></td>
                                <td>Kelompok Harga</td>
                                <td><input readonly id="epricegroupname" name="epricegroupname"
                                        value="Harga Luar Pulau">
                                    <input id="ipricegroup" name="ipricegroup" type="hidden" value="03">
                                    <input id="i_kode_harga" name="i_kode_harga" type="hidden"
                                        value="<?= $i_kode_harga; ?>">
                                    <input id="i_sjpb" name="i_sjpb" type="hidden" value="<?= $i_sjpb; ?>">
                                    <input id="pilihan" name="pilihan" type="hidden" value="<?= $pilihan; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Area</td>
                                <td><input readonly id="eareaname" name="eareaname" value="PUSAT BABY">
                                    <input type="hidden" id="iarea" name="iarea" value="PB">
                                    <input id="istorelocation" name="istorelocation" type="hidden" value="00">

                                </td>
                                <td>Nilai Kotor</td>
                                <td><input readonly id="vspb" name="vspb" value="<?= $nilaikotor->nilaikotor; ?>"></td>
                            </tr>
                            <tr>
                                <td>Pelanggan</td>
                                <td><input readonly id="ecustomername" name="ecustomername"
                                        value="<?= $header->e_customer_name; ?>">
                                    <input type="hidden" id="icustomer" name="icustomer"
                                        value="<?= $header->i_customer; ?>">
                                </td>
                                <?php 
                                if($pilihan == 'biasa'){                               
                                    $diskon1persen = ($nilaikotor->nilaikotor * 0.01);
                                    $nilaibersih = ($nilaikotor->nilaikotor - $diskon1persen);
                                }else{
                                    $diskon1persen = 0;
                                    $nilaibersih = $nilaikotor->nilaikotor;
                                }
								?>
                                <td>Discount 1</td>
                                <td><input id="ncustomerdiscount1" name="ncustomerdiscount1"
                                        value="<?php if($pilihan == 'biasa'){ echo number_format(1, 2); }else{ echo number_format(0, 2); } ?>"
                                        readonly>
                                    <input id="vcustomerdiscount1" name="vcustomerdiscount1"
                                        value="<?= number_format($diskon1persen, 0); ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td><input type="text" readonly value=""></td>
                                <td>Discount 2</td>
                                <td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2"
                                        value="<?= number_format(0, 2); ?>">
                                    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2"
                                        value="<?= number_format(0, 0); ?>"></td>
                            </tr>
                            <tr>
                                <td>PO</td>
                                <td><input readonly id="ispbpo" name="ispbpo" value="" maxlength="30"></td>
                                <td>Discount 3</td>
                                <td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3"
                                        value="<?= number_format(0, 2); ?>">
                                    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3"
                                        value="<?= number_format(0, 0); ?>"></td>
                            </tr>
                            <tr>
                                <td>Konsiyasi</td>
                                <td><input id="fspbconsigment" name="fspbconsigment" type="checkbox" checked value="on">
                                    SPB Lama&nbsp;&nbsp;&nbsp;<input readonly id="ispbold" name="ispbold" type="text"
                                        value=""></td>
                                <td>Discount Total</td>
                                <td><input id="vspbdiscounttotal" name="vspbdiscounttotal"
                                        value="<?= number_format($diskon1persen, 0); ?>" readonly></td>
                            </tr>
                            <tr>
                                <td>TOP</td>
                                <td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly value="36">
                                    Stock Daerah<input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox"
                                        checked value="on">
                                </td>
        </td>
        <td>Nilai Bersih</td>
        <td><input readonly id="vspbbersih" name="vspbbersih" value="<?= number_format($nilaibersih, 0); ?>">
        </td>
    </tr>
    <tr>
        <td>Salesman</td>
        <td><input readonly id="esalesmanname" name="esalesmanname" value="KONSINYASI-TL">
            <input id="isalesman" name="isalesman" type="hidden" value="TL">
        </td>
        <td>Discount Total (realisasi)</td>
        <td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly
                value="<?= number_format(0, 0); ?>" readonly></td>
    </tr>
    <tr>
        <td>Stok Daerah</td>
        <td>
            <input id="isj" name="isj" readonly value="">
            <input readonly id="dsj" name="dsj" value=""></td>
        <td>Nilai SPB (realisasi)</td>
        <?php 
		?>
        <td><input id="vspbafter" name="vspbafter" readonly value="<?= number_format(0, 0); ?>"></td>
    </tr>
    <tr>
        <td>PKP</td>
        <td>
            <input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly value=""></td>
        <td>Keterangan</td>
        <td><input id="eremarkx" name="eremarkx" maxlength="100" value=""></td>
    </tr>
    <tr>
        <td width="100%" align="center" colspan="4">
            <div id='ketppn'>

            </div>
        </td>
    </tr>
    <tr>
        <td width="100%" align="center" colspan="4">
            <?php 
        if($header->i_spb == '' || $header->i_spb == null){ ?>
            <input name="login" id="login" value="Simpan" type="submit" onclick="dipales();">
            <?php } ?>
            <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("sjpbsj/cform/","#main")'>
        </td>
    </tr>
</table>
<div id="detailisi" align="center">
    <?php 
	$i=0;
	if($detail){
        echo '<table class="listtable" id="sitabel">';
        echo '<thead><th style="width:25px;" align="center">No</th>
        <th style="width:250px;" align="center">No SJ</th>
        <th style="width:250px;" align="center">No Nota</th>
        <th style="width:63px;" align="center">Kode</th>
        <th style="width:300px;" align="center">Nama Barang</th>
        <th style="width:70px;" align="center">Motif</th>
        <th style="width:90px;" align="center">Harga Sebelum</th>
        <th style="width:90px;" align="center">Harga Sesudah</th>
        <th style="width:46px;" align="center">Jml Psn</th>
        <th style="width:46px;" align="center">Jml Pmnhn</th>
        <th style="width:94px;" align="center">Total</th>
         </thead><tbody>';
			  foreach($detail as $row)
			  {	
					$i++;
				  $pangaos=number_format($row->bersih,2);
				  $total=$row->bersih*$row->n_receive;
				  $total=number_format($total,2);
				  $v_product_retail = number_format($row->v_product_retail, 2);
					echo "
						  <tr>
                          <td style=\"width:21px;\">$i</td>";
                          if($sj_nota){
                              echo "<td style=\"width:250px;\">$sj_nota->i_sj</td>
                              <td style=\"width:250px;\">$sj_nota->i_nota</td>";
                          }else{
                            echo "<td style=\"width:250px;\"></td>
                            <td style=\"width:250px;\"></td>";
                          }
					echo "<td style=\"width:60px;\">$row->i_product</td>
						  <td style=\"width:282px;\">$row->e_product_name</td>
						  <td style=\"width:68px;\">00</td>
							<td style=\"width:85px;\">$v_product_retail</td>
						  <td style=\"width:85px;\">$pangaos</td>
						  <td style=\"width:43px;\">$row->n_receive</td>
						<td style=\"width:50px;\">$row->n_receive</td>							
						  <td style=\"width:88px;\">$total</td>
						  </tr>
						";
              }
              echo '</tbody>';
              echo "</table>";  
	}

?>
</div>
</div>
</div>
</div>
<?=form_close()?>
<input name="export" id="export" value="Export to Excel" type="button">
<div id="pesan"></div>
</td>
</tr>
</table>
<script>
function dipales() {
    document.getElementById("login").hidden = true;
}
</script>
<script language="javascript" type="text/javascript">
$("#export").click(function() {
    var Contents = $('#sitabel').html();
    window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) +
        '</table>');
});
</script>