<?php if ($isi) { ?>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'spmbacc/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
				<div id="spmbform">
					<div class="effect">
						<div class="accordion2">
							<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
								<tr>
									<td width="10%">Tgl SPMB</td>
									<?php
									$tmp = explode("-", $isi->d_spmb);
									$th = $tmp[0];
									$bl = $tmp[1];
									$hr = $tmp[2];
									$dspmb = $hr . "-" . $bl . "-" . $th;

									$btn_simpan	= $isi->i_approve1 != '' ? 'disabled=true' : '';
									$btn_tambah_item	= (($isi->i_store != null) || ($isi->i_approve1 != '')) ? 'disabled=true' : '';
									?>
									<td><input id="ispmb" name="ispmb" type="text" readonly value="<?php echo $isi->i_spmb; ?>">
										<input readonly id="dspmb" name="dspmb" value="<?php echo $dspmb; ?>">
									</td>
								</tr>
								<tr>
									<td width="10%">Area</td>
									<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
										<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
									</td>
								</tr>
								<tr>
									<td width="10%">Keterangan</td>
									<td><input id="eremark" name="eremark" value="<?php echo $isi->e_remark; ?>" readonly></td>
								</tr>
								<tr>
									<td width="10%">SPMB Lama</td>
									<td><input id="ispmbold" name="ispmbold" value="<?php echo $isi->i_spmb_old; ?>"></td>
								</tr>
								<tr>
									<td width="10%">Nilai Acc</td>
									<td><input style="text-align:right;" id="vacc" name="vacc" value="0" readonly></td>
								</tr>
								<input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
																				echo "value=\"$jmlitem\"";
																			} else {
																				echo "value=\"0\"";
																			} ?>>
								<tr>
									<td width="100%" align="center" colspan="4">
										<input name="login" id="login" value="Simpan" type="submit" onclick="return dipales();" <?= $btn_simpan ?>>
										<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spmbacc/cform/","#main");'>
										<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" <?= $btn_tambah_item ?> onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
									</td>
								</tr>
							</table>
							<div id="detailheader" align="center">
								<table class="listtable" align="center" style="width: 1056px;">
									<th style="width:25px;" align="center">No</th>
									<th style="width:89px;" align="center">Kategori Penjualan</th>
									<th style="width:65px;" align="center">Kode</th>
									<th style="width:293px;" align="center">Nama Barang</th>
									<th style="width:100px;" align="center">Motif</th>
									<th style="width:46px;" align="center">Jml Psn</th>
									<th style="width:46px;" align="center">Jml Acc</th>
									<th style="width:46px;" align="center">Jml Stk</th>
									<th style="width:46px;" align="center">Jml Rt2</th>
									<th style="width:100px;" align="center">Nilai Rata2</th>
									<th style="width:184px;" align="center">Keterangan</th>
								</table>
							</div>
							<div id="detailisi" align="center">
								<?php
								$i = 0;
								foreach ($detail as $row) {
									$fpaw = 'FP-' . $peraw;
									$fpak = 'FP-' . $perak;
									$query = $this->db->query(" select trunc(sum(n_deliver*v_unit_price)/3) as vrata, trunc(sum(n_deliver)/3) as nrata, i_product 
                                      from tm_nota_item
                                      where i_nota>'$fpaw' and i_nota<'$fpak' and i_product='$row->i_product'
                                      and i_product_motif='$row->i_product_motif'
                                      and i_area in (select i_area from tr_area where i_area_parent='$row->i_area')
                                      group by i_product
                                      order by i_product");
									#                                     and i_area='$row->i_area'
									if ($query->num_rows() > 0) {
										foreach ($query->result() as $raw) {
											$vrata = number_format($raw->vrata);
											$nrata = number_format($raw->nrata);
										}
									} else {
										$vrata = 0;
										$nrata = 0;
									}
									if ($isi->i_area == '00') {
										$store = 'AA';
									} else {
										$store = $isi->i_area;
									}
									$nstock = 0;
									$query = $this->db->query(" select n_quantity_stock
                                      from tm_ic
                                      where i_store='$store' and i_product='$row->i_product'");
									if ($query->num_rows() > 0) {
										foreach ($query->result() as $raw) {
											$nstock = number_format($raw->n_quantity_stock);
										}
									}
									$i++;
									$pangaos = number_format($row->v_unit_price, 2);
									//$pangaos=$row->v_unit_price;
									$total = $row->v_unit_price * $row->n_order;
									$total = number_format($total, 2);
									if ($row->n_acc == '' || $row->n_acc == 0) $row->n_acc = $row->n_order;
									echo "<table class=\"listtable\" align=\"center\" style=\"width:991px;\"><tbody>
							<tr>
		    				<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:62px;\">
									<input style=\"width:85px;\" readonly type=\"text\" id=\"esalescategory$i\" name=\"esalescategory$i\" value=\"$row->e_sales_categoryname\">
								</td>
								<td style=\"width:62px;\">
									<input style=\"width:62px;\" readonly type=\"text\" id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\">
								</td>
							<td style=\"width:280px;\"><input style=\"width:280px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:95px;\">
								<input readonly style=\"width:95px;\" type=\"text\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$pangaos\"></td>
							<td style=\"width:42px;\"><input readonly style=\"text-align:right; width:42px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_order\"></td>
							<td style=\"width:42px;\"><input style=\"text-align:right; width:42px;\" 
								type=\"text\" id=\"nacc$i\" name=\"nacc$i\" value=\"$row->n_acc\" 
								onkeyup=\"hitungtung()\"></td>
              <td style=\"width:45px;\"><input style=\"text-align:right; width:45px;\" readonly 
                type=\"text\" id=\"jmlstock$i\" name=\"jmlstock$i\" value=\"$nstock\"></td>
              <td style=\"width:44px;\"><input style=\"text-align:right; width:44px;\" readonly 
                type=\"text\" id=\"jmlrata$i\" name=\"jmlrata$i\" value=\"$nrata\"></td>
              <td style=\"width:96px;\"><input style=\"text-align:right; width:96px;\" readonly 
                type=\"text\" id=\"nilairata$i\" name=\"nilairata$i\" value=\"$vrata\"></td>
							<td style=\"width:146px;\">
								<textarea id=\"eremark$i\" name=\"eremark$i\">$row->e_remark</textarea>
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td></tr>
						  </tbody></table>"; /* <input style=\"text-align:left; width:146px;\" type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\"> */
								}
								?>
							</div>
							<div id="pesan"></div>
						</div>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
<?php } ?>
<script language="javascript" type="text/javascript">
	function tambah_item(a) {
		if (a <= 30) {
			so_inner = document.getElementById("detailheader").innerHTML;
			si_inner = document.getElementById("detailisi").innerHTML;
			if (so_inner == '') {
				so_inner = '<table class="listtable" align="center" style="width:750px;">';
				so_inner += '<th style="width:25px;"  align="center">No</th>';
				so_inner += '<th style="width:63px;" align="center">Kode</th>';
				so_inner += '<th style="width:300px;" align="center">Nama Barang</th>';
				so_inner += '<th style="width:100px;" align="center">Motif</th>';
				so_inner += '<th style="width:46px;"  align="center">Jml Pesan</th>';
				so_inner += '<th style="width:46px;"  align="center">Jml Acc</th>';
				so_inner += '<th style="width:100px;"  align="center">Keterangan</th></table>';
				document.getElementById("detailheader").innerHTML = so_inner;
			} else {
				so_inner = '';
			}
			if (si_inner == '') {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
				si_inner += '<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
				si_inner += '<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
				si_inner += '<td style="width:103px;"><input readonly style="width:103px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value="">';
				si_inner += '<input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
				si_inner += '<td style="width:47px;"><input style="text-align:right; width:47px;" type="text" id="norder' + a + '" name="norder' + a + '" value="">';
				si_inner += '<td style="width:47px;"><input style="text-align:right; width:47px;" type="text" id="nacc' + a + '" name="nacc' + a + '" value="" onkeyup="hitungnilai(' + a + ');">';
				si_inner += '<input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value="">';
				si_inner += '</td>';
				si_inner += '<td style="width:104px;"><input style="width:104px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value="">';
				/* si_inner+='<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td></tr></tbody></table>'; */
				si_inner += '</td></tr></tbody></table>';
			} else {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = si_inner + '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
				si_inner += '<td style="width:66px;"><input style="width:66px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
				si_inner += '<td style="width:314px;"><input style="width:314px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
				si_inner += '<td style="width:103px;"><input readonly style="width:103px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value="">';
				si_inner += '<input type="hidden" id="vproductmill' + a + '" name="vproductmill' + a + '" value=""></td>';
				si_inner += '<td style="width:47px;"><input style="text-align:right; width:47px;" type="text" id="norder' + a + '" name="norder' + a + '" value="">';
				si_inner += '<td style="width:47px;"><input style="text-align:right; width:47px;" type="text" id="nacc' + a + '" name="nacc' + a + '" value="" onkeyup="hitungnilai(' + a + ');">';
				si_inner += '<input type="hidden" id="vtotal' + a + '" name="vtotal' + a + '" value="">';
				si_inner += '</td>';
				si_inner += '<td style="width:104px;"><input style="width:104px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value="">';
				/* si_inner+='<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td></tr></tbody></table>'; */
				si_inner += '</td></tr></tbody></table>';
			}
			j = 0;
			var baris = Array();
			var iproduct = Array();
			var eproductname = Array();
			var vproductmill = Array();
			var norder = Array();
			var nacc = Array();
			var motif = Array();
			var motifname = Array();
			var vtotal = Array();
			for (i = 1; i < a; i++) {
				j++;
				baris[j] = document.getElementById("baris" + i).value;
				iproduct[j] = document.getElementById("iproduct" + i).value;
				eproductname[j] = document.getElementById("eproductname" + i).value;
				vproductmill[j] = document.getElementById("vproductmill" + i).value;
				norder[j] = document.getElementById("norder" + i).value;
				nacc[j] = document.getElementById("nacc" + i).value;
				motif[j] = document.getElementById("motif" + i).value;
				motifname[j] = document.getElementById("emotifname" + i).value;
				vtotal[j] = document.getElementById("vtotal" + i).value;
			}
			document.getElementById("detailisi").innerHTML = si_inner;
			j = 0;
			for (i = 1; i < a; i++) {
				j++;
				document.getElementById("baris" + i).value = baris[j];
				document.getElementById("iproduct" + i).value = iproduct[j];
				document.getElementById("eproductname" + i).value = eproductname[j];
				document.getElementById("vproductmill" + i).value = vproductmill[j];
				document.getElementById("norder" + i).value = norder[j];
				document.getElementById("nacc" + i).value = nacc[j];
				document.getElementById("motif" + i).value = motif[j];
				document.getElementById("emotifname" + i).value = motifname[j];
				document.getElementById("vtotal" + i).value = vtotal[j];
			}
			showModal("spmbacc/cform/product/" + a, "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		} else {
			alert("Maksimal 30 item");
		}
	}

	function dipales() {
		a = document.getElementById("jml").value;

		cek = 'false';
		//cek=0;
		if ((document.getElementById("dspmb").value != '') && (document.getElementById("iarea").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("iproduct" + i).value == '') ||
						(document.getElementById("eproductname" + i).value == '') ||
						(document.getElementById("norder" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						exit();
						cek = 'false';
						//cek=0;
					} else {
						//alert(document.getElementById("iproduct"+i).value+' '+document.getElementById("eproductname"+i).value+' : '+document.getElementById("vproductmill"+i).value+' '+document.getElementById("norder"+i).value);	
						cek = 'true';
						//cek=1;
					}
				}
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}

		if (cek == 'true') {
			var kon = window.confirm("yakin SPmB tsb akan di acc ?");
			if (kon) {
				document.getElementById("login").hidden = true;
				document.getElementById("cmdtambahitem").hidden = true;
				return true;
			} else {
				document.getElementById("login").hidden = false;
				return false;
			}
		} else {
			document.getElementById("login").hidden = false;
		}
	}

	/*
  function hitungtung(){
	  jml=document.getElementById("jml").value;
	  if (isNaN(parseFloat(jml))){
		  alert("Input harus numerik");
	  }else{
      total=0;
      for(i=1;i<=jml;i++){
		    hrg=formatulang(document.getElementById("vproductmill"+i).value);
		    qty=formatulang(document.getElementById("nacc"+i).value);
		    harga=parseFloat(hrg)*parseFloat(qty);
  		  document.getElementById("vtotal"+i).value=formatcemua(harga);
        total=total+harga;
      }
		  if(parseFloat(total) || parseInt(total)) {	
		  	document.getElementById("vacc").value=formatcemua(total);
		  }
	  }
  }
  */

	function hitungtung() {
		jml = document.getElementById("jml").value;
		if (isNaN(parseFloat(jml))) {
			alert("Input harus numerik");
		} else {
			total = 0;
			for (i = 1; i <= jml; i++) {
				hrg = formatulang(document.getElementById("vproductmill" + i).value);
				qty = formatulang(document.getElementById("nacc" + i).value);
				//hrg=document.getElementById("vproductmill"+i).value;
				//qty=document.getElementById("nacc"+i).value;		    
				harga = parseFloat(hrg) * parseFloat(qty);
				document.getElementById("vtotal" + i).value = formatcemua(harga);
				//document.getElementById("vtotal"+i).value=harga;
				total = total + harga;
			}

			document.getElementById("vacc").value = formatcemua(total);
		}
	}

	$(document).ready(function() {
		hitungtung();
	});

	function hitungnilai(brs) {
		acc = document.getElementById("nacc" + brs).value;
		if (isNaN(parseFloat(acc))) {
			alert("Input harus numerik");
		} else {
			hrg = formatulang(document.getElementById("vproductmill" + brs).value);
			qty = formatulang(acc);
			//hrg=document.getElementById("vproductmill"+brs).value;
			//qty=acc;
			vhrg = parseFloat(hrg) * parseFloat(qty);
			document.getElementById("vtotal" + brs).value = formatcemua(vhrg);
			//document.getElementById("vtotal"+brs).value=vhrg;
			total = total + vhrg;
		}
		document.getElementById("vacc").value = formatcemua(total);
	}
</script>