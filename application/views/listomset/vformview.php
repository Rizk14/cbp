<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listomset/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
	  <th rowspan=2>AREA</th>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($bl,'integer');
	  }
    $bl=$blasal;
?>
    <th colspan=<?php echo $interval; ?> align=center>SPB</th>
<?php 
#    echo '<th rowspan=2>Total SPB</th>';
?>
    </tr>
    <tr>
<?php 
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th>Jan</th>';
        break;
      case '2' :
        echo '<th>Feb</th>';
        break;
      case '3' :
        echo '<th>Mar</th>';
        break;
      case '4' :
        echo '<th>Apr</th>';
        break;
      case '5' :
        echo '<th>Mei</th>';
        break;
      case '6' :
        echo '<th>Jun</th>';
        break;
      case '7' :
        echo '<th>Jul</th>';
        break;
      case '8' :
        echo '<th>Agu</th>';
        break;
      case '9' :
        echo '<th>Sep</th>';
        break;
      case '10' :
        echo '<th>Okt</th>';
        break;
      case '11' :
        echo '<th>Nov</th>';
        break;
      case '12' :
        echo '<th>Des</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
#    echo '<th>Rata2 Nota</th>';
?>
    </tr>
      <tbody>
<?php 
    $grandtot01=0;
    $grandtot02=0;
    $grandtot03=0;
    $grandtot04=0;
    $grandtot05=0;
    $grandtot06=0;
    $grandtot07=0;
    $grandtot08=0;
    $grandtot09=0;
    $grandtot10=0;
    $grandtot11=0;
    $grandtot12=0;
		foreach($isi as $row){
	    echo "<tr>
              <td>".$row->iarea."-".$row->area."</td>";
      $bl=$blasal;
      for($i=1;$i<=$interval;$i++){
        switch ($bl){
        case '1' :
          echo '<th align=right>'.number_format($row->spbjan).'</th>';
          $grandtot01=$grandtot01+$row->spbjan;
          break;
        case '2' :
          echo '<th align=right>'.number_format($row->spbfeb).'</th>';
          $grandtot02=$grandtot02+$row->spbfeb;
          break;
        case '3' :
          echo '<th align=right>'.number_format($row->spbmar).'</th>';
          $grandtot03=$grandtot03+$row->spbmar;
          break;
        case '4' :
          echo '<th align=right>'.number_format($row->spbapr).'</th>';
          $grandtot04=$grandtot04+$row->spbapr;
          break;
        case '5' :
          echo '<th align=right>'.number_format($row->spbmay).'</th>';
          $grandtot05=$grandtot05+$row->spbmay;
          break;
        case '6' :
          echo '<th align=right>'.number_format($row->spbjun).'</th>';
          $grandtot06=$grandtot06+$row->spbjun;
          break;
        case '7' :
          echo '<th align=right>'.number_format($row->spbjul).'</th>';
          $grandtot07=$grandtot07+$row->spbjul;
          break;
        case '8' :
          echo '<th align=right>'.number_format($row->spbaug).'</th>';
          $grandtot08=$grandtot08+$row->spbaug;
          break;
        case '9' :
          echo '<th align=right>'.number_format($row->spbsep).'</th>';
          $grandtot09=$grandtot09+$row->spbsep;
          break;
        case '10' :
          echo '<th align=right>'.number_format($row->spboct).'</th>';
          $grandtot10=$grandtot10+$row->spboct;
          break;
        case '11' :
          echo '<th align=right>'.number_format($row->spbnov).'</th>';
          $grandtot11=$grandtot11+$row->spbnov;
          break;
        case '12' :
          echo '<th align=right>'.number_format($row->spbdes).'</th>';
          $grandtot12=$grandtot12+$row->spbdes;
          break;
        }
        $bl++;
      }
      echo "<tr>";    
    }
    echo "<tr>
          <td style='background-color:#F2F2F2;' align=center>G r a n d   T o t a l</td>";
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot01).'</th>';
        break;
      case '2' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot02).'</th>';
        break;
      case '3' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot03).'</th>';
        break;
      case '4' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot04).'</th>';
        break;
      case '5' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot05).'</th>';
        break;
      case '6' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot06).'</th>';
        break;
      case '7' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot07).'</th>';
        break;
      case '8' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot08).'</th>';
        break;
      case '9' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot09).'</th>';
        break;
      case '10' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot10).'</th>';
        break;
      case '11' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot11).'</th>';
        break;
      case '12' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot12).'</th>';
        break;
      }
      $bl++;
    }
	}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listomset/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
