<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-opnnota/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Pilihan</td>
								<td width="1%">:</td>
								<td width="80%">Tgl Nota&nbsp;
									<input type='checkbox' name='chknt' id='chknt' value='xxx' onclick='muter1();'>
									<input type='hidden' name='chkntx' id='chkntx' value='xxx'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									Tgl JT&nbsp;
									<input type='checkbox' name='chkjt' id='chkjt' value='xxx' onclick='muter2();'>
									<input type='hidden' name='chkjtx' id='chkjtx' value='xxx'>
								</td>
							</tr>
							<tr>
								<td width="19%">s/d Tanggal</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php 
									$data = array(
										'name'        => 'dto',
										'id'          => 'dto',
										'value'       => '',
										'readonly'   	=> 'true',
										'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Area</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("exp-opnnota/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<!-- <input name="login" id="login" value="Transfer" type="submit"> -->
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-opnnota/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function muter1() {
		nt = document.getElementById("chknt");
		if (nt.checked) {
			document.getElementById("chkjt").checked = false;
			document.getElementById("chkjtx").value = 'xxx';
			document.getElementById("chkntx").value = 'qqq';
		} else {
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'xxx';
		}
	}

	function muter2() {
		jt = document.getElementById("chkjt");
		if (jt.checked) {
			document.getElementById("chknt").checked = false;
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'qqq';
		} else {
			document.getElementById("chkntx").value = 'xxx';
			document.getElementById("chkjtx").value = 'xxx';
		}
	}

	function exportexcel() {
		var iarea = document.getElementById('iarea').value;
		var eareaname = document.getElementById('eareaname').value;
		var dateto = document.getElementById('dto').value;
		var chkjtx = document.getElementById("chkjtx").value;
		var chkntx = document.getElementById("chkntx").value;

		if (dateto == '') {
			alert('Pilih Tanggal Terlebih Dahulu!!!');
			return false;
		} else if (iarea == '') {
			alert('Pilih Area Terlebih Dahulu!!!');
			return false;
		} else if (chkjtx == '' || chkntx == '') {
			alert('Pilih Berdasarkan Nota / Jatuh Tempo Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url('exp-opnnota/cform/export/'); ?>" + iarea + "/" + eareaname + "/" + dateto + "/" + chkntx + "/" + chkjtx;
			console.log(abc);
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>