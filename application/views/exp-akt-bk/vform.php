<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<!-- <input type="hidden" id="areafrom" name="areafrom" value=""> -->
									<?php
									$data = array(
										'name'      => 'dfrom',
										'id'        => 'dfrom',
										'value'     => date('01-m-Y'),
										'readonly'	=> 'true',
										'onclick'	=> "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'      => 'dto',
										'id'        => 'dto',
										'value'     => date('d-m-Y'),
										'readonly'  => 'true',
										'onclick'	=> "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<div id="coba" style="display: none;">
									<td width="19%">Bank</td>
									<td width="1%">:</td>
									<td width="80%">
										<input type="text" id="ebankname" name="ebankname" value="" onclick='showModal("<?= $folder ?>/cform/bank/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
										<input type="hidden" id="ibank" name="ibank" value="">
										<input type="hidden" id="icoa" name="icoa" value="">
									</td>
								</div>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Download</button></a>
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('<?= $folder ?>/cform/','#tmpx')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script>
	function exportexcel() {
		var dfrom = document.getElementById('dfrom').value;
		var dto = document.getElementById('dto').value;
		var icoa = document.getElementById("icoa").value;
		var bank = document.getElementById("ibank").value;

		if (bank == "") {
			alert("Bank Wajib diisi!!");
			return false;
		} else {
			var abc = "<?= site_url($folder . '/cform/export/'); ?>" + dfrom + "/" + dto + "/" + icoa + "/" + bank;
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>