<div id="tmp">
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div id="listform">
					<div class="effect">
						<div class="accordion2">
							<table class="listtable">
								<thead>
									<tr>
										<td colspan="8" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">
											<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
											<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
											<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
											<input type="submit" id="bcari" name="bcari" value="Cari">
										</td>
									</tr>
								</thead>
								<th>No DKB</th>
								<th>Tgl DKB</th>
								<th>Area</th>
								<th>Nama Supir</th>
								<th>No Polisi</th>
								<th>Jumlah</th>
								<th class="action">Action</th>
								<tbody>
									<?php
									if ($isi) {
										foreach ($isi as $row) {
											$row->jumlah = number_format($row->jumlah);
											$tmp = explode('-', $row->d_dkb);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_dkb = $tgl . '-' . $bln . '-' . $thn;

											echo "<tr> ";
											if ($row->f_dkb_batal == 't') {
												echo "<td><h1>$row->i_dkb</h1></td>";
											} else {
												echo "<td>$row->i_dkb</td>";
											}
											echo " 	<td>$row->d_dkb</td>
													<td>$row->e_area_name</td>
													<td>$row->e_sopir_name</td>
													<td>$row->i_kendaraan</td>
													<td>$row->jumlah</td>
													<td class=\"action\">
														<a href=\"#\" onclick='show(\"$tujuan/cform/edit/$row->i_dkb/$row->i_area/$dfrom/$dto/$iarea/\",\"#main\")'>
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
														</a> ";
											if ($row->f_dkb_batal == 'f') {
												echo "<a href=\"#\" onclick='hapus(\"$folder/cform/delete/$row->i_dkb/$row->i_area/$dfrom/$dto/$iarea/\",\"#main\")'>
														<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
													</a>";
											}
											echo "</td></tr>";
										}
									}
									?>
								</tbody>
							</table>
							<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
						</div>
					</div>
				</div>
				<?= form_close() ?>
				<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/','#main')">
			</td>
		</tr>
	</table>
</div>