<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbycategory/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $th; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totvolcy=0;
            $totvolcm=0;
            $totvolly=0;
            $totvollm=0;
            foreach($isi as $ro){
              $totvolcy=$totvolcy+$ro->volcy;
              $totvolcm=$totvolcm+$ro->volcm;
              $totvolly=$totvolly+$ro->volly;
              $totvollm=$totvollm+$ro->vollm;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan=3>No</th>
         <th rowspan=3>Kategory</th>
         <th colspan=2><?php echo $prevth; ?></th>
         <th colspan=2  ><?php echo $th; ?></th>
         <th rowspan=3>% Ctr YTD</th>
         <th colspan=2></th>
         </tr>
         <tr>
         <th><?php echo mbulan($bl); ?></th>
         <th>YTD</th>
         <th><?php echo mbulan($bl); ?></th>
         <th>YTD</th>
         <th colspan=2>%Growth</th>
         </tr>
         <tr>
         <th> </th>
         <th> </th>
         <th> </th>
	 <th> </th>
	 <th>MTD</th>
	 <th>YTD</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $i=0;
        $totpersenvolcy=0;

        foreach($isi as $row){
          $i++;
          
          $persenspbselm=0;
          $persencustselm=0;
          $persenspbsely=0;
          $persencustsely=0;

          $persenvolcy=($row->volcy/$totvolcy)*100;
          $totpersenvolcy=$totpersenvolcy+$persenvolcy;
          $custselm=$row->volcm-$row->vollm;
          $custsely=$row->volcy-$row->volly;
          ###################################################
          $growthmtdvol=$totvolcm/$totvollm*100-100;
          $growthmtdvalue=$totvolcy/$totvolly*100-100;


          if($row->vollm==0){
            $persencustselm=0;
          }else{
            $persencustselm=($custselm/$row->vollm)*100;
          }
          if($row->volly==0){
            $persencustsely=0;
          }else{
            $persencustsely=($custsely/$row->volly)*100;
          }
          echo "<tr>
              <td style='font-size:12px;'>$i</td>
              <td style='font-size:12px;'>$row->namakategory</td>
              <td style='font-size:12px;' align=right>".number_format($row->vollm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->volly)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->volcm)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->volcy)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenvolcy,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($persencustselm,2)."</td>
              <td style='font-size:12px;' align=right>".number_format($persencustsely,2)."</td>

              </tr>";
         }
        echo "<tr>
        <td style='font-size:12px;' colspan=2><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvollm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolly)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolcm)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvolcy)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvolcy,2)."%</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdvol,2)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($growthmtdvalue,2)."</b></td>

        </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
