<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
  if($isi){
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab		= str_repeat(" ",9);
		$hal	= 1;
		$ipp  = new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$tmp=explode("-",$row->d_bapb);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_bapb=$hr." ".substr(mbulan($bl),0,3)." ".$th;
#############
		$cetak.=CHR(18);
		$cetak.=$nor.NmPerusahaan."\n\n";
		$cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."BERITA ACARA PENERIMAAN BARANG".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n";
		$cetak.=$nor."No. ".$row->i_bapb."                    Tanggal : ".$row->d_bapb."\n\n";
		$cetak.=$nor."Kepada YTH\n";
    $cust=false;
    if( (trim($row->i_customer)=='') || ($row->i_customer==null) ){
      /* Disabled 14042011
      $cetak.=$nor.CHR(27).CHR(120).CHR(1)."KP/PS ".$row->i_area."-".$row->e_area_name."\n\n";
      */
	  $qarea = $this->db->query(" select * from tr_area where i_area='$row->i_area' ");
	  if ($qarea->num_rows() > 0)
	  {
		  $rowarea	= $qarea->row();
		  $areaname	= $rowarea->e_area_name;
	  }      
      $cetak.=$nor.CHR(27).CHR(120).CHR(1)."KP/PS - ".$row->i_area."-".$areaname."\n\n";
    }else{
      $cust=true;
      $cetak.=$nor.CHR(27).CHR(120).CHR(1).$row->i_customer."-".$row->e_customer_name."\n";
      $cetak.=$nor.CHR(27).CHR(120).CHR(1).$row->e_customer_address."-".$row->e_customer_city."\n\n";
    }
    $cetak.=$nor.CHR(15)."Kami telah menerima kiriman barang sebagai berikut:\n";
    $cetak.=$ab.CHR(15)."1. Melalui:\n";
    $dataeks = $this->db->query(" select a.*, b.e_ekspedisi from tm_bapb_ekspedisi a, tr_ekspedisi b
                                  where a.i_bapb='$row->i_bapb' and a.i_area='$row->i_area' and a.i_ekspedisi=b.i_ekspedisi
                                  order by a.ctid desc");
		if ($dataeks->num_rows() > 0)
    {
			$eks=$dataeks->row();
      $namaekspedisi=$eks->e_ekspedisi;
    }
    $cetak.=$ab."   Jasa angkutan : ".CHR(18).$namaekspedisi.CHR(15)."\n";
    if($row->n_berat<=0){
      $berat="-";
    }else{
      $berat=number_format($row->n_berat,2);
    }
    $cetak.=$ab."2. Dengan berat keseluruhan : ".$berat." Kg dan terdiri dari : ".CHR(18).CHR(27).CHR(120).CHR(1).$row->n_bal.CHR(15)." BALL, dengan rincian Nota/Surat Jalan sbb:\n";
    $datasj = $this->db->query(" select * from tm_bapb_item where i_bapb='$row->i_bapb' and i_area='$row->i_area'",false);
		if ($datasj->num_rows() > 0)
    {
      $sj1="";
      $sj2="";
      $sj3="";
      $sj4="";
      $sj5="";
      $jsj=0;
			foreach($datasj->result() as $item)
      {
        $jsj++;
        if($sj1==''){
          $sj1=$sj1.$item->i_sj;
        }elseif($jsj<5){
          $sj1=$sj1.','.$item->i_sj;
        }elseif($sj2==''){
          $sj2=$sj2.$item->i_sj;
        }elseif($jsj<9){
          $sj2=$sj2.','.$item->i_sj;
        }elseif($sj3==''){
          $sj3=$sj3.$item->i_sj;
        }elseif($jsj<13){
          $sj3=$sj3.','.$item->i_sj;
        }elseif($sj4==''){
          $sj4=$sj4.$item->i_sj;
        }elseif($jsj<17){
          $sj4=$sj4.','.$item->i_sj;
        }elseif($sj5==''){
          $sj5=$sj5.$item->i_sj;
        }elseif($jsj<21){
          $sj5=$sj5.','.$item->i_sj;
        }
      }
    }
		$cetak.=$ab."   ".CHR(18).$sj1.CHR(15)."\n";
    if($sj2!=''){
		  $cetak.=$ab."   ".CHR(18).$sj2.CHR(15)."\n";
    }
    if($sj3!=''){
		  $cetak.=$ab."   ".CHR(18).$sj3.CHR(15)."\n";
    }
    if($sj4!=''){
		  $cetak.=$ab."   ".CHR(18).$sj4.CHR(15)."\n";
    }
    if($sj5!=''){
		  $cetak.=$ab."   ".CHR(18).$sj5.CHR(15)."\n";		  
    }
		$cetak.=$ab."3. Keadaan Pembungkusnya (*\n";
		$cetak.=$ab."  ( a ) Masih sangat baik dan terjahit rapi\n";
		$cetak.=$ab."  ( b ) Terjahit, tetapi tidak rapi\n";
		$cetak.=$ab."  ( c ) Sedikit terbuka ( Ada jahitan yang terlepas )\n";
		$cetak.=$ab."  ( d ) Terbuka jahitannya & pembungkusnya robek\n";
		$cetak.=$ab."  ( e ) Lain - lainnya ( Beri Penjelasan )\n";
		$cetak.=$ab.str_repeat(".",100)."\n";
		$cetak.=$ab.str_repeat(".",100)."\n";
		$cetak.=$ab.str_repeat(".",100)."\n\n";
		$cetak.=$ab."    (* Beri tanda silang pada huruf sesuai kondisi sebenarnya\n\n";
		$cetak.=$ab."4. Barang yang tercantum dibawah ini terdapat ketidaksesuaian\n";
		$cetak.=$ab."   antara Nota/Surat Jalan dengan jumlah yang diterima\n";
		$cetak.=$ab.CHR(218).str_repeat(CHR(196),2).CHR(194).str_repeat(CHR(196),51).CHR(194).str_repeat(CHR(196),18).CHR(194).str_repeat(CHR(196),20).CHR(191)."\n";
		$cetak.=$ab.CHR(179)."NO".CHR(179)."            N A M A   B A R A N G                  ".CHR(179)." JUMLAH di Nota/SJ".CHR(179)." JUMLAH YG DITERIMA ".CHR(179)."\n";
		$cetak.=$ab.CHR(195).str_repeat(CHR(196),2).CHR(197).str_repeat(CHR(196),51).CHR(197).str_repeat(CHR(196),18).CHR(197).str_repeat(CHR(196),20).CHR(180)."\n";
    for($xx=1;$xx<9;$xx++){
		  $cetak.=$ab.CHR(179)."  ".CHR(179)."                                                   ".CHR(179)."                  ".CHR(179)."                    ".CHR(179)."\n";
		  $cetak.=$ab.CHR(195).str_repeat(CHR(196),2).CHR(197).str_repeat(CHR(196),51).CHR(197).str_repeat(CHR(196),18).CHR(197).str_repeat(CHR(196),20).CHR(180)."\n";
    }
    $cetak.=$ab.CHR(179)."  ".CHR(179)."                                                   ".CHR(179)."                  ".CHR(179)."                    ".CHR(179)."\n";
	  $cetak.=$ab.CHR(192).str_repeat(CHR(196),2).CHR(193).str_repeat(CHR(196),51).CHR(193).str_repeat(CHR(196),18).CHR(193).str_repeat(CHR(196),20).CHR(217)."\n";
    $cetak.=$ab."KLAIM DILAYANI APABILA BAPB INI DIISI PIHAK PENERIMA DAN DIKIRIM KE ".NmPerusahaan."\n";
    $cetak.=$ab.str_repeat(" ",65)."..............................\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(218).str_repeat(CHR(196),21).CHR(191).CHR(15).str_repeat(" ",37)."Tanda tangan\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(179)."     P E N T I N G   ".CHR(179).CHR(15).str_repeat(" ",15)." Admin Gudang,".str_repeat(" ",15)."pembawa,".str_repeat(" ",15)."penerima,\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(179)."  CLAIM KEKURANGAN / ".CHR(179).CHR(15)."\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(179)."   TOLAKAN BARANG    ".CHR(179).CHR(15)."\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(179)."  MAX 7 HARI SETELAH ".CHR(179).CHR(15)."\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(179)."   BARANG DI TERIMA  ".CHR(179).CHR(15)."\n";
    $cetak.=$ab.CHR(18).CHR(27).CHR(120).CHR(1).CHR(192).str_repeat(CHR(196),21).CHR(217).CHR(15)."\n";
    $enter='';
    if($cust){
      if($jsj<5){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<9){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<13){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<17){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<21){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }
    }else{
      if($jsj<5){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<9){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<13){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<17){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }elseif($jsj<21){
        $cetak.=$ab.str_repeat(" ",50)."(...............)       (...............)       (...............)\n\n\n\n\n".$enter.CHR(27).CHR(120).CHR(0).CHR(18);
      }
    }
#############
    }
    $ipp->setdata($cetak);
    $ipp->printJob();
	}
#    echo $cetak;  
		echo "<script>this.close();</script>";
?>
