<div id='tmp'>
  <h2><?php echo $page_title . ' Periode : ' . $iperiode; ?></h2>
  <table class="maintable">
    <tr>
      <td align="left">
        <?php echo $this->pquery->form_remote_tag(array('url' => 'listhpp/cform/view', 'update' => '#main', 'type' => 'post')); ?>
        <div class="effect">
          <div class="accordion2">
            <table class="listtable" id="sitabel">
              <tr>
              <th rowspan=2 align="center">No</th>
              <th rowspan=2 align="center">Kode</th>
              <th rowspan=2 align="center">Harga</th>
              <th colspan=2 align="center">Saldo Awal</th>
              <th colspan=2 align="center">Beli</th>
              <th colspan=2 align="center">Jual</th>
              <th colspan=2 align="center">Saldo Akhir</th>
              </tr>
              <tr>
              <th align="center">Qty</th><th align="center">RP</th><th align="center">Qty</th><th align="center">RP</th><th align="center">Qty</th><th align="center">RP</th><th align="center">Qty</th><th align="center">RP</th>
              </tr>
              <tbody>
                <?php 
if ($isi) {
    $no = 0;
    $gtawal = 0;
    $gtbeli = 0;
    $gtjual = 0;
    $gtakhir = 0;
    $xgtawal = 0;
    $xgtbeli = 0;
    $xgtjual = 0;
    $xgtakhir = 0;
    foreach ($isi as $row) {
        $no++;
        $gtawal = $gtawal + $row->n_awal;
        $gtbeli = $gtbeli + $row->n_beli;
        $gtjual = $gtjual + $row->n_jual;
        $gtakhir = $gtakhir + $row->n_akhir;
        $xgtawal = ($row->v_harga * $row->n_awal) + $xgtawal;
        $xgtbeli = ($row->v_harga * $row->n_beli) + $xgtbeli;
        $xgtjual = ($row->v_harga * $row->n_jual) + $xgtjual;
        $xgtakhir = ($row->v_harga * $row->n_akhir) + $xgtakhir;
        echo "<tr><td align=right>$no</td>
				  <td>" . $row->i_product . "</td>
				  <td align=right>" . number_format($row->v_harga) . "</td>
				  <td align=right>" . number_format($row->n_awal) . "</td>
					 <td align=right>" . number_format($row->v_harga * $row->n_awal) . "</td>
				  <td align=right>" . number_format($row->n_beli) . "</td>
					 <td align=right>" . number_format($row->v_harga * $row->n_beli) . "</td>
				  <td align=right>" . number_format($row->n_jual) . "</td>
					 <td align=right>" . number_format($row->v_harga * $row->n_jual) . "</td>
					 <td align=right>" . number_format($row->n_akhir) . "</td>
						<td align=right>" . number_format($row->v_harga * $row->n_akhir) . "</td>
				</tr>";
    }
    echo "<tr><td align=right colspan=3>Total</td>
		<td align=right>" . number_format($gtawal) . "</td>
		<td align=right>" . number_format($xgtawal) . "</td>
		<td align=right>" . number_format($gtbeli) . "</td>
		<td align=right>" . number_format($xgtbeli) . "</td>
		<td align=right>" . number_format($gtjual) . "</td>
		<td align=right>" . number_format($xgtjual) . "</td>
		<td align=right>" . number_format($gtakhir) . "</td>
		<td align=right>" . number_format($xgtakhir) . "</td>
	</tr>";

}
?>
              </tbody>
            </table>
            <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
            <?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
          </div>
        </div>
        <?=form_close()?>
      </td>
    </tr>
  </table>
</div>
<script language="javascript" type="text/javascript">
$("#cmdreset").click(function() {
  var Contents = $('#sitabel').html();
  window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) +
    '</table>');
});
</script>
