<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body id="bodylist">
	<div id="main">
		<div id="tmp">
			<?php echo "<center><h2>$page_title</h2></center>"; ?>
			<table class="maintable">
				<tr>
					<td align="left">
						<?php
						$tujuan = 'spbpromobaby/cform/cariproduct/' . $baris . '/' . $kdharga . '/' . $kdgroup . '/' . $promo;
						?>
						<?php echo $this->pquery->form_remote_tag(array('url' => $tujuan, 'update' => '#light', 'type' => 'post')); ?>
						<div id="listform">
							<div class="effect">
								<div class="accordion2">
									<table class="listtable">
										<thead>
											<tr>
												<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;
													<input type="submit" id="bcari" name="bcari" value="Cari">
													<input type="hidden" id="baris" name="baris" value="<?php echo $baris; ?>">
												</td>
											</tr>
										</thead>
										<th>Kode Product</th>
										<th>Nama</th>
										<th>Motif</th>
										<th>Status</th>
										<th>Kategori Penjualan</th>
										<tbody>
											<?php
											if ($isi) {
												foreach ($isi as $row) {
													echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','" . number_format($row->harga, 2) . "',$baris,'$row->motif','$row->namamotif','$row->i_product_status','$row->n_quantity_min')\">$row->kode</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','" . number_format($row->harga, 2) . "',$baris,'$row->motif','$row->namamotif','$row->i_product_status','$row->n_quantity_min')\">$row->nama</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','" . number_format($row->harga, 2) . "',$baris,'$row->motif','$row->namamotif','$row->i_product_status','$row->n_quantity_min')\">$row->namamotif</a></td>
          		  <td><a href=\"javascript:setValue('$row->kode','$row->nama','" . number_format($row->harga, 2) . "',$baris,'$row->motif','$row->namamotif','$row->i_product_status','$row->n_quantity_min')\">$row->e_product_statusname</a></td>
          		  <td><a href=\"javascript:setValue('$row->kode','$row->nama','" . number_format($row->harga, 2) . "',$baris,'$row->motif','$row->namamotif','$row->i_product_status','$row->n_quantity_min')\">$row->e_sales_categoryname</a></td>
				</tr>";
												}
											}
											?>
										</tbody>
									</table>
									<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
									<br>
									<center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
								</div>
							</div>
						</div>
						<?= form_close() ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
	function setValue(a, b, c, d, e, f, g, h) {
		/* ada=false; */
		/* for(i=1;i<=d;i++){
		if( document.getElementById("iproduct"+i).value != null || document.getElementById("iproduct"+i).value != ''){
			if(
				(a==document.getElementById("iproduct"+i).value) && 
				(i!==d) && 
				(e==document.getElementById("motif"+i).value)
				){
				alert ("kode : "+a+" sudah ada !!!!!");
				ada=true;
				break;
			}else{
				ada=false;	   
			}
		}
    } */

		var productlist = '';

		for (i = 1; i <= d; i++) {
			productlist += $('#iproduct' + i).val();
		}

		var ada = productlist.includes(a);

		/* alert(ada); */

		if (!ada) {
			var tr = $('#the-table').find('tbody tr');
			tr.find('input.no').each(function(x) {
				$(this).val(x + 1);
			})
			document.getElementById("iproduct" + d).value = a;
			document.getElementById("eproductname" + d).value = b;
			document.getElementById("vproductretail" + d).value = c;
			document.getElementById("motif" + d).value = e;

			document.getElementById("emotifname" + d).value = f;
			document.getElementById("iproductstatus" + d).value = g;

			document.getElementById("v_product_min" + d).value = h;
			/* alert("gatau ah"); */
			jsDlgHide("#konten *", "#fade", "#light");
			hitungnilai(0, d);
		} else {
			alert("kode : " + a + " sudah ada !!!!!");
		}
	}

	/*   $('#Keluar').on('click',function(){
			var a = "<?= $baris; ?>";
			$('#jml').val(parseFloat($('#jml').val())-1);
			jsDlgHide("", "#fade", "#light");
			$('#trnumber'+a).remove();
		}); */

	function bbatal() {
		var a = "<?= $baris; ?>";
		$('#jml').val(parseFloat($('#jml').val()) - 1);
		jsDlgHide("", "#fade", "#light");
		$('#trnumber' + a).remove();
	}
</script>