<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?>
<h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpelunasanappernota/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
	  <th>No Bukti</th>
	  <th>Supplier</th>
	  <th>Area</th>
	  <th>Bln</th>
	  <th>Thn</th>
		<th>Nota</th>
		<th>Tgl Nota</th>
		<th>Tgl Bukti</th>
		<th>Jumlah</th>
		<th>Sisa</th>
    <tbody>
<?php 
		foreach($isi as $row){
  	    echo "<tr>
                <td>$row->i_pelunasanap</td>
                <td>".$row->i_supplier."-".$row->e_supplier_name."</td>
                <td>$row->i_area</td>
                <td>$row->bln</td>
                <td>$row->thn</td>
            	  <td>$row->i_dtap</td>
                <td>$row->d_dtap</td>
                <td>$row->d_bukti</td>
                <td>$row->v_jumlah</td>
                <td>$row->v_sisa</td>
              </tr>";
		}
  }
?>
    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
