<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-pelunasan-pembelian/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="domanualform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'        => 'dfrom',
										'id'          => 'dfrom',
										'value'       => date('01-m-Y'),
										'readonly'    => 'true',
										'onclick'	  => "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'        => 'dto',
										'id'          => 'dto',
										'value'       => date('d-m-Y'),
										'readonly'    => 'true',
										'onclick'	  => "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Supplier</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="isupplier" name="isupplier" value="">
									<input type="text" id="esuppliername" name="esuppliername" value="" onclick='showModal("<?= $folder ?>/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
									<button onclick="show('<?= $folder ?>/cform/','#tmpx')">Refresh</button>
									<!-- <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('<#?= $folder ?>/cform/','#tmpx')"> -->
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function exportexcel() {
		var isupplier = document.getElementById('isupplier').value;
		var dfrom = document.getElementById("dfrom").value;
		var dto = document.getElementById("dto").value;

		if (isupplier == '') {
			alert('Pilih Supplier Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url($folder . '/cform/export/'); ?>" + isupplier + "/" + dfrom + "/" + dto;
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>