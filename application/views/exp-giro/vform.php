<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-giro/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div class="effect">
				<div class="accordion2">
					<table class="mastertable">
						<tr>
							<td width="19%">Date From</td>
							<td width="1%">:</td>
							<td width="80%">
								<?php
								$data = array(
									'name'        => 'dfrom',
									'id'          => 'dfrom',
									'value'       => date('01-m-Y'),
									'readonly'   	=> 'true',
									'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)"
								);
								echo form_input($data); ?>
							</td>
						</tr>
						<tr>
							<td width="19%">Date To</td>
							<td width="1%">:</td>
							<td width="80%">
								<?php
								$data = array(
									'name'        => 'dto',
									'id'          => 'dto',
									'value'       => date('d-m-Y'),
									'readonly'   	=> 'true',
									'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)"
								);
								echo form_input($data); ?>
							</td>
						</tr>
						<tr>
							<td width="19%">&nbsp;</td>
							<td width="1%">&nbsp;</td>
							<td width="80%">
								<a href="#" id="href" value="Download" target="blank" onclick="return exportexcel();"><button>Download</button></a>
								<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-giro/cform/','#main')">
							</td>
						</tr>
					</table>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script languge=javascript type=text/javascript>
	function afterSetDateValue(ref_field, target_field, date) {

	}

	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}

	function exportexcel() {
		var dfrom = document.getElementById('dfrom').value;
		var dto = document.getElementById('dto').value;

		var abc = "<?= site_url($folder . '/cform/export/'); ?>" + dfrom + "/" + dto;

		$("#href").attr("href", abc);
		return true;
	}
</script>