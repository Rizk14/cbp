<?php 
$this->load->view('header');
?>
<h2>
    <?php 
    echo $page_title . " Gagal";
    ?></h2>
<p class="error">
    <?php 
    echo $this->lang->line('login_wrong_password');
    ?></p>
<?php 
$this->load->view('login/loginform');
?>
<p class="error">
    <?php 
    echo $this->lang->line('login_caps_lock');
    ?>
</p>
<?php 
$this->load->view('footer');
?>