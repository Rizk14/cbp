<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'karyawancutiinput/cform/carikaryawan','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	<tr>
      	<th rowspan="3">NIK</th>
	    <th rowspan="3">Nama Karyawan</th>
	    <th colspan="2" align="center">Tanggal Berlaku Cuti</th>
	    <th rowspan="3" align="center">Saldo Cuti</th>
	    </tr>
	    <tr>
	    <th align="center">Awal</th>
	    <th align="center">Akhir</th>
	    </tr>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_nik','$row->e_nama_karyawan_lengkap','$row->d_masaberlaku_cutiawal','$row->v_saldo_cuti')\">$row->i_nik</a></td>
				  <td><a href=\"javascript:setValue('$row->i_nik','$row->e_nama_karyawan_lengkap','$row->d_masaberlaku_cutiawal','$row->v_saldo_cuti')\">$row->e_nama_karyawan_lengkap</a></td>
				  <td><a href=\"javascript:setValue('$row->i_nik','$row->e_nama_karyawan_lengkap','$row->d_masaberlaku_cutiawal','$row->v_saldo_cuti')\">$row->d_masaberlaku_cutiawal</a></td>
				  <td><a href=\"javascript:setValue('$row->i_nik','$row->e_nama_karyawan_lengkap','$row->d_masaberlaku_cutiawal','$row->v_saldo_cuti')\">$row->d_masaberlaku_cutiakhir</a></td>
				  <td><a href=\"javascript:setValue('$row->i_nik','$row->e_nama_karyawan_lengkap','$row->d_masaberlaku_cutiawal','$row->v_saldo_cuti')\">$row->v_saldo_cuti</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d)
  {
    document.getElementById("i_nik").value=a;
    document.getElementById("e_nama_karyawan_lengkap").value=b;
    document.getElementById("d_masaberlaku_cutiawal").value=c;
    document.getElementById("v_saldo_cuti").value=d;
	jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
