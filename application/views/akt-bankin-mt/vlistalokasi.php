<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/dgu.css" />
</head>

<body id="bodylist">
    <div id="main">
        <div id="tmp">
            <?php echo "<center><h2>No Alokasi</h2></center>"; ?>
            <table class="maintable">
                <tr>
                    <td align="left">
                        <?php echo $this->pquery->form_remote_tag(array('url' => 'akt-bankin-mt/cform/alokasi/'.$iarea."/".$irv."/".$icoabank."/".$iperiode."/", 'update' => '#tmp', 'type' => 'post')); ?>
                        <div id="listform">
                            <div class="effect">
                                <div class="accordion2">
                                    <table class="listtable">
                                        <thead>
                                            <tr>
                                                <td colspan="5" align="center">Cari data : 
                                                	<input type="text" id="cari" name="cari" value="<?php echo $cari?>" placeholder = "No Bank / No Alokasi / Nm Pelanggan ">
                                                	<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
                                                	<input type="hidden" id="icoabank" name="icoabank" value="<?php echo $icoabank; ?>">
                                                	<input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
                                                	<input type="hidden" id="iperiode2" name="iperiode2" value="<?php echo $iperiode2; ?>">
                                                	<input type="submit" id="bcari" name="bcari" value="Cari">
                                                </td>
                                            </tr>
                                        </thead>
                                        <th>Pelanggan</th>
                                        <th>No Alokasi</th>
                                        <th>Tgl Alokasi</th>
                                        <th>Jumlah</th>
                                        <th>Sisa</th>
                                        <tbody>
                                            <?php 
											if ($isi) {
												foreach ($isi as $row) {
                                                    $dalokasi = date('d-m-Y',strtotime($row->d_alokasi));
													echo "<tr> 
                                                            <td>
                                                                <a href=\"javascript:setValue('$row->i_alokasi','$row->i_customer','$row->e_customer_name','$row->e_customer_address','$row->e_customer_city','$row->i_giro')\">".$row->i_customer." - ".$row->e_customer_name."</a>
                                                            </td>
				  											<td>
				  												<a href=\"javascript:setValue('$row->i_alokasi','$row->i_customer','$row->e_customer_name','$row->e_customer_address','$row->e_customer_city','$row->i_giro')\">$row->i_alokasi</a>
				  											</td>
                                                            <td>
                                                                <a href=\"javascript:setValue('$row->i_alokasi','$row->i_customer','$row->e_customer_name','$row->e_customer_address','$row->e_customer_city','$row->i_giro')\">$dalokasi</a>
                                                            </td>
				  											<td>
				  												<a href=\"javascript:setValue('$row->i_alokasi','$row->i_customer','$row->e_customer_name','$row->e_customer_address','$row->e_customer_city','$row->i_giro')\">".number_format($row->v_jumlah)."</a>
                                                            </td>
                                                            <td>
                                                                <a href=\"javascript:setValue('$row->i_alokasi','$row->i_customer','$row->e_customer_name','$row->e_customer_address','$row->e_customer_city','$row->i_giro')\">".number_format($row->v_sisa)."</a>
                                                            </td>
														 </tr>";
												}
											}
											?>
                                        </tbody>
                                    </table>
                                    <?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
                                    <br>
                                    <center><input type="button" id="Keluar" name="Keluar" value="Keluar"
                                            onclick="bbatal()"></center>
                                </div>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </td>
                </tr>
            </table>
        </div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
function setValue(a,b,c,d,e,f) {
    document.getElementById("ialokasi").value           = a;
    document.getElementById("icustomer").value          = b;
    document.getElementById("ecustomername").value      = c;
    document.getElementById("ecustomeraddress").value   = d;
    document.getElementById("ecustomercity").value      = e;
    document.getElementById("igiro").value      = f;
    //document.getElementById("esalesmanname").value = b;
    jsDlgHide("#konten *", "#fade", "#light");
}

function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
}
</script>