<table class="maintable">
    <tr>
        <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url' => 'akt-bankin-mt/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
            <div id="pelunasanform">
                <div class="effect">
                    <div class="accordion2">
                        <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
                            <tr>
                                <td>No Bank / Tgl Bank</td>
                                <td>
                                    <input type="text" id="ikbank" name="ikbank" value="<?php echo $ikbank; ?>"
                                        readonly>
                                    <input type="hidden" id="irv" name="irv" value="<?php echo $irv; ?>" readonly>
                                    <input readonly id="dbank" name="dbank" value="<?php echo $dbank; ?>">
                                    <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
                                </td>

                                <td>No Alokasi</td>
                                <td>
                                    <input readonly id="ialokasi" name="ialokasi" onclick='cekalokasi();'
                                        placeholder="Harap diisi">
                                </td>
                            </tr>

                            <tr>
                                <td>Area / Tgl Alokasi</td>
                                <td>
                                    <input readonly id="eareaname" name="eareaname" value="<?php echo $eareaname; ?>">
                                    <input id="iarea" name="iarea" type="hidden" value="<?php echo $iarea; ?>">
                                    <input readonly id="dalokasi" name="dalokasi" value="<?php echo $dbank; ?>">
                                </td>

                                <td>Debitur</td>
                                <td>
                                    <!-- <input type="text" readonly id="ecustomername" name="ecustomername" value="" onclick='return tiasa();'> -->
                                    <input type="text" readonly id="ecustomername" name="ecustomername" value="">
                                    <input type="hidden" readonly id="icustomer" name="icustomer" value="">
                                    <input type="hidden" id="icoabank" name="icoabank" value="<?php echo $icoabank; ?>">
                                </td>

                            </tr>

                            <tr>
                                <td>Bank</td>
                                <td>
                                    <input readonly id="ebankname" name="ebankname" value="<?php echo $ebankname; ?>">
                                </td>

                                <td>Alamat</td>
                                <td>
                                    <input id="ecustomeraddress" name="ecustomeraddress" readonly value="">
                                </td>

                            </tr>

                            <tr>
                                <td>Jumlah</td>
                                <td>
                                    <input readonly type="text" id="vjumlah" name="vjumlah" value="<?php echo number_format($vsisa); ?>">
                                    <input type="hidden" id="vlebih" name="vlebih" value="0">
                                    <input type="hidden" id="vsisa" name="vsisa" value="<?php echo $vsisa; ?>">
                                    <input type="hidden" id="vtotal" name="vtotal" value="0">
                                    <input type="hidden" name="jml" id="jml" value="0">
                                </td>

                                <td>Kota</td>
                                <td>
                                    <input readonly id="ecustomercity" name="ecustomercity" value="">
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td>KU/Giro/Tunai</td>
                                <td>
                                    <input readonly type="text" id="igiro" name="igiro" value="" disabled>
                                </td>
                                <!--  onclick="view_bayar('<?php #echo $iarea; ?>')" -->
                            </tr>

                            <tr>
                                <td width="100%" align="center" colspan="4">
                                    <input name="login" id="login" value="Simpan" type="submit"
                                        onclick="return dipales()">
                                    <input name="cmdreset" id="cmdreset" value="Keluar" type="button"
                                        onclick='show("akt-bankin-mt/cform/view/<?php echo $dfrom . '/' . $dto . '/' . $iarea . '/'; ?>","#main")'>
                                    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
                                        onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'>
                                </td>
                            </tr>
                        </table>

                        <div id="detailheader" align="center"></div>
                        <div id="detailisi" align="center"></div>
</table>
</div>
</div>
</div>
<?= form_close() ?>
<div id="pesan"></div>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
    function cekalokasi() {
        //if (document.getElementById("ialokasi").value != ''  && document.getElementById("icustomer").value != '' ) {

        // document.getElementById("icustomer").value = '';
        // document.getElementById("ecustomername").value = '';
        // document.getElementById("icoabank").value = '';
        // document.getElementById("ecustomeraddress").value = '';
        // document.getElementById("ecustomercity").value = '';
        // document.getElementById("igiro").value = '';
        // document.getElementById("iperiode").value = '';

        showModal("akt-bankin-mt/cform/alokasi/" + document.getElementById("iarea").value + "/" + document
            .getElementById("irv").value + "/" + document
            .getElementById("icoabank").value + "/" + document
            .getElementById("iperiode").value + "/", "#light");
        jsDlgShow("#konten *", "#fade", "#light");
        // } else {
        //     showModal("akt-bankin-mt/cform/alokasi/" + document.getElementById("iarea").value + "/" + document
        //         .getElementById("irv").value +  "/" + document
        //         .getElementById("icoabank").value + "/" + document
        //         .getElementById("iperiode").value + "/", "#light");
        //     jsDlgShow("#konten *", "#fade", "#light");
        // }
    }

    function tiasa() {
        $s = 0;
        if (document.getElementById("idt").value == '') {
            alert('No DT Tidak Boleh Kosong!');
            $s = 1;
            return false;
        } else {
            /*showModal("akt-bankin-mt/cform/customer/<?php #echo $iarea; 
            													?>/<?php #echo 'sikasep'; 
            																			?>/","#light"); jsDlgShow("#konten *", "#fade", "#light");*/

            showModal("akt-bankin-mt/cform/customer/" + document.getElementById("areadt").value + "/" + document
                .getElementById("idt").value + "/" + document.getElementById("icustomer").value + "/sikasep/",
                "#light");
            jsDlgShow("#konten *", "#fade", "#light");
        }
    }

    function dipales() {
        cek = 'false';
        cok = 'false';
        $s = 0;

        if (
            (document.getElementById("ikbank").value != '') &&
            (document.getElementById("dbank").value != '') &&
            (document.getElementById("dalokasi").value != '') &&
            (document.getElementById("vjumlah").value != '') &&
            (document.getElementById("vjumlah").value != '0') &&
            (document.getElementById("icustomer").value != '')
        ) {
            var a = parseFloat(document.getElementById("jml").value);
            for (i = 1; i <= a; i++) {
                if (document.getElementById("vjumlah" + i).value != '0') {
                    sisa = parseFloat(formatulang(document.getElementById("vsisa" + i).value));
                    sesa = parseFloat(formatulang(document.getElementById("vsesa" + i).value));
                    awal = parseFloat(formatulang(document.getElementById("vjumlah" + i).value));
                    ket = document.getElementById("eremark" + i).value;
                    if ((sisa - awal > 0) || (sisa > 0 && (ket == '' || ket == "NULL"))) {
                        if ((sesa > 0 && ket == '')) {
                            alert('Keterangan sisa wajib diisi !!!');
                            cok = 'false';
                            cek = 'false';
                            $s = 1;
                            return false;
                        } else {
                            cok = 'true';
                            cek = 'true';
                        }
                    } else {
                        cok = 'true';
                        cek = 'true';
                    }
                } else {
                    cek = 'false';
                }
            }
            if (cek == 'true' && cok == 'true') {
                document.getElementById("login").hidden = true;
                document.getElementById("cmdtambahitem").hidden = true;
            } else if (cok == 'false') {} else {
                alert('Isi jumlah detail pelunasan minimal 1 item !!!');
                document.getElementById("login").hidden = false;
            }
        } else {
            alert('Data header masih ada yang salah !!!');
        }
    }

    function tambah_item(a) {
        so_inner = document.getElementById("detailheader").innerHTML;
        si_inner = document.getElementById("detailisi").innerHTML;

        //    alert(si_inner);
        if (so_inner == '') {
            so_inner = '<table id="itemtem" class="listtable" style="width:920px;">';
            so_inner += '<th style="width:21px;" align="center">No</th>';
            so_inner += '<th style="width:81px;" align="center">Nota</th>';
            so_inner += '<th style="width:81px;" align="center">Tgl Nota</th>';
            so_inner += '<th style="width:71px;" align="center">Meterai</th>';
            so_inner += '<th style="width:71px;" align="center">Bayar</th>';
            so_inner += '<th style="width:71px;" align="center">Sisa</th>';
            //so_inner += '<th style="width:115px;" align="center">Lebih</th>';
            //		  so_inner+= '<th style="width:115px;" align="center">ket1</th>';
            //so_inner += '<th style="width:115px;" align="center">Ket2</th>';
            document.getElementById("detailheader").innerHTML = so_inner;
        } else {
            so_inner = '';
        }
        if (si_inner == '') {
            document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
            juml = document.getElementById("jml").value;
            si_inner =
                '<table id="itemdet"><tbody><tr><td style="width:27px;"><input style="width:47px;font-size:12px;" readonly type="text" id="baris' +
                a + '" name="baris' + a + '" value="' + a + '"></td>';
            si_inner +=
                '<td style="width:140px;"><input style="width:183px;font-size:12px;" readonly type="text" id="inota' +
                a +
                '" name="inota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:86px;"><input style="width:184px;font-size:12px;" readonly type="text" id="dnota' +
                a + '" name="dnota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:128px;"><input readonly style="width:161px;text-align:right;font-size:12px;" type="text" id="vnota' +
                a + '" name="vnota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:128px;"><input style="width:160px;text-align:right;font-size:12px;" type="text" id="vjumlah' +
                a + '" name="vjumlah' + a + '" value="" onKeyUp="hetang(' + a + ');reformat(this);" onpaste="return false;" readonly></td>';
            si_inner +=
                '<td style="width:125px;"><input readonly style="width:159px;text-align:right;font-size:12px;" type="text" id="vsesa' +
                a + '" name="vsesa' + a + '" value=""><input type="hidden" id="vsisa' + a + '" name="vsisa' + a +
                '" value=""></td></tbody></table>';
            // si_inner +=
            // '<td style="width:128px;"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vlebih' +
            // a + '" name="vlebih' + a + '" value=""></td>';
            //  		si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-mt/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
            /*si_inner+='<td style="width:120px;"><input style="width:120px;font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody></table>';*/
            // si_inner += '<td style="width:120px;"><select id="eremark' + a + '" name="eremark' + a +
            //     '"><option value=""></option><option value="Biaya Promo">Biaya Promo</option><option value="Biaya Adm">Biaya Adm</option><option value="Biaya Transfer">Biaya Transfer</option><option value="Biaya Materai">Biaya Materai</option><option value="Biaya Barcode">Biaya Barcode</option><option value="Biaya Listing Fee">Biaya Listing Fee</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Pembulatan">Pembulatan</option><option value="Cicil">Cicil</option><option value="Returan">Returan</option><option value="Giro">Giro</option></select></td></tr></tbody></table>';
        } else {
            document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
            juml = document.getElementById("jml").value;
            si_inner +=
                '<table id="itemdet"><tbody><tr><td style="width:27px;"><input style="width:47px;font-size:12px;" readonly type="text" id="baris' +
                a + '" name="baris' + a + '" value="' + a + '"></td>';
            si_inner +=
                '<td style="width:140px;"><input style="width:183px;font-size:12px;" readonly type="text" id="inota' +
                a +
                '" name="inota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:86px;"><input style="width:184px;font-size:12px;" readonly type="text" id="dnota' +
                a + '" name="dnota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:128px;"><input readonly style="width:161px;text-align:right;font-size:12px;" type="text" id="vnota' +
                a + '" name="vnota' + a + '" value=""></td>';
            si_inner +=
                '<td style="width:128px;"><input style="width:160px;text-align:right;font-size:12px;" type="text" id="vjumlah' +
                a + '" name="vjumlah' + a + '" value="" onKeyUp="hetang(' + a + ');reformat(this)" onpaste="return false;" readonly></td>';
            si_inner +=
                '<td style="width:125px;"><input readonly style="width:159px;text-align:right;font-size:12px;" type="text" id="vsesa' +
                a + '" name="vsesa' + a + '" value=""><input type="hidden" id="vsisa' + a + '" name="vsisa' + a +
                '" value=""></td></tbody></table>';
            //si_inner +=
            //    '<td style="width:128px;"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vlebih' +
            //    a + '" name="vlebih' + a + '" value=""></td>';
            //   		si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-mt/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
            /*si_inner+='<td style="width:120px;"><input style="width:120px;font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td></tr></tbody></table>';*/
            // si_inner += '<td style="width:120px;"><select id="eremark' + a + '" name="eremark' + a +
            //     '"><option value=""></option><option value="Biaya promo">Biaya promo</option><option value="Biaya Adm">Biaya Adm</option><option value="Biaya Transfer">Biaya Transfer</option><option value="Biaya Materai">Biaya Materai</option><option value="Biaya Barcode">Biaya Barcode</option><option value="Biaya Listing Fee">Biaya Listing Fee</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Pembulatan">Pembulatan</option><option value="Cicil">Cicil</option><option value="Returan">Returan</option><option value="Giro">Giro</option></select></td></tr></tbody></table>';
        }
        j = 0;
        var baris = Array();
        var inota = Array();
        var dnota = Array();
        var vnota = Array();
        var vjumlah = Array();
        var vsesa = Array();
        var vsisa = Array();
        //var vlebih = Array();
        //    var ipelunasanremark = Array();
        //    var epelunasanremark = Array();
        //var eremark = Array();

        for (i = 1; i < a; i++) {
            j++;
            baris[j] = document.getElementById("baris" + i).value;
            inota[j] = document.getElementById("inota" + i).value;
            dnota[j] = document.getElementById("dnota" + i).value;
            vnota[j] = document.getElementById("vnota" + i).value;
            vjumlah[j] = document.getElementById("vjumlah" + i).value;
            vsesa[j] = document.getElementById("vsesa" + i).value;
            vsisa[j] = document.getElementById("vsisa" + i).value;
            //vlebih[j] = document.getElementById("vlebih" + i).value;
            //		  ipelunasanremark[j]	= document.getElementById("ipelunasanremark"+i).value;
            //		  epelunasanremark[j]	= document.getElementById("epelunasanremark"+i).value;
            //eremark[j] = document.getElementById("eremark" + i).value;

        }
        document.getElementById("detailisi").innerHTML = si_inner;
        j = 0;
        for (i = 1; i < a; i++) {
            j++;
            document.getElementById("baris" + i).value = baris[j];
            document.getElementById("inota" + i).value = inota[j];
            document.getElementById("dnota" + i).value = dnota[j];
            document.getElementById("vnota" + i).value = vnota[j];
            document.getElementById("vjumlah" + i).value = vjumlah[j];
            document.getElementById("vsesa" + i).value = vsesa[j];
            document.getElementById("vsisa" + i).value = vsisa[j];
            //document.getElementById("vlebih" + i).value = vlebih[j];
            //		  document.getElementById("ipelunasanremark"+i).value=ipelunasanremark[j];
            //		  document.getElementById("epelunasanremark"+i).value=epelunasanremark[j];
            //document.getElementById("eremark" + i).value = eremark[j];
        }
        area = document.getElementById("iarea").value;
        ikbank = document.getElementById("ikbank").value;
        cust = document.getElementById("icustomer").value;
        ialokasi = document.getElementById("ialokasi").value;
        icoabank = document.getElementById("icoabank").value;

        showModal("akt-bankin-mt/cform/nota/" + a + "/" + area + "/" + cust + "/" + ialokasi + "/" + icoabank + "/",
            "#light");
        jsDlgShow("#konten *", "#fade", "#light");
        s
    }

    function hetang(x) {
        num = document.getElementById("vjumlah" + x).value.replace(/\,/g, '');;
        if (!isNaN(num)) {
            vjmlbyr = parseFloat(formatulang(document.getElementById("vjumlah").value));
            vlebihitem = vjmlbyr;
            vsisadt = parseFloat(formatulang(document.getElementById("vsisa").value));
            jml = document.getElementById("jml").value;
            vtotal = 0;
            for (a = 1; a <= jml; a++) {

                vnota = parseFloat(formatulang(document.getElementById("vsisa" + a).value));
                vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
                
                if (vjmlitem == 0) {
                    bbotol();
                }

                vtotal += vjmlitem
                
                vsisaitem = vnota - vjmlitem;
                if (vsisaitem < 0) {
                    alert("jumlah bayar tidak bisa lebih besar dari nilai meterai !!!!!");
                    document.getElementById("vjumlah" + a).value = 0;
                    vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
                    vsisaitem = parseFloat(formatulang(document.getElementById("vsisa" + a).value));
                }
                vlebihitem = vlebihitem - vjmlitem;
                if (vlebihitem < 0) {
                    vlebihitem = vlebihitem + vjmlitem;
                    vsisaitem = vnota - vlebihitem;
                    alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
                    document.getElementById("vjumlah" + a).value = formatcemua(vlebihitem);
                    vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
                    vlebihitem = 0;
                }
                document.getElementById("vsesa" + a).value = formatcemua(vsisaitem);
                //document.getElementById("vlebih" + a).value = formatcemua(vlebihitem);
            }
            document.getElementById("vlebih").value = formatcemua(vlebihitem);
            document.getElementById("vtotal").value = vtotal;
        } else {
            alert('input harus numerik !!!');
            document.getElementById("vjumlah" + x).value = 0;
        }
    }

    function afterSetDateValue(ref_field, target_field, date) {
        cektanggal();
    }

    function cektanggal() {
        ddt = document.getElementById('ddt').value;
        dbukti = document.getElementById('dbukti').value;
        dtmpdt = ddt.split('-');
        dtmpbukti = dbukti.split('-');
        perdt = dtmpdt[2] + dtmpdt[1] + dtmpdt[0];
        perbukti = dtmpbukti[2] + dtmpbukti[1] + dtmpbukti[0];
        if ((perdt != '') && (perbukti != '')) {
            if (compareDates(document.getElementById('dbukti').value, document.getElementById('ddt').value) == -1) {
                alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
                document.getElementById("dbukti").value = '';
            }
        } else if (perdt > perbukti) {
            alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
            document.getElementById("dbukti").value = '';
        }
        document.getElementById('dcair').value = document.getElementById('dcairx').value;
    }

    function bbotol() {
        baris = document.getElementById("baris").value;
        si_inner = document.getElementById("detailisi").innerHTML;
        var temp = new Array();
        temp = si_inner.split('<tbody disabled="disabled">');
        if ((document.getElementById("vjumlah" + baris).value == '0')) {
            si_inner = '';
            for (x = 1; x < baris; x++) {
                si_inner = si_inner + '<tbody>' + temp[x];
            }
            j = 0;
            var barbar = Array();
            var inota = Array();
            var dnota = Array();
            var vnota = Array();
            var vjumlah = Array();
            var vsesa = Array();
            var vsisa = Array();
            //var vlebih = Array();
            for (i = 1; i < baris; i++) {
                j++;
                barbar[j] = document.getElementById("baris" + i).value;
                inota[j] = document.getElementById("inota" + i).value;
                dnota[j] = document.getElementById("dnota" + i).value;
                vnota[j] = document.getElementById("vnota" + i).value;
                vjumlah[j] = document.getElementById("vjumlah" + i).value;
                vsesa[j] = document.getElementById("vsesa" + i).value;
                vsisa[j] = document.getElementById("vsisa" + i).value;
                //vlebih[j] = document.getElementById("vlebih" + i).value;
            }
            document.getElementById("detailisi").innerHTML = si_inner;
            j = 0;
            for (i = 1; i < baris; i++) {
                j++;
                document.getElementById("baris" + i).value = barbar[j];
                document.getElementById("inota" + i).value = inota[j];
                document.getElementById("dnota" + i).value = dnota[j];
                document.getElementById("vnota" + i).value = vnota[j];
                document.getElementById("vjumlah" + i).value = vjumlah[j];
                document.getElementById("vsesa" + i).value = vsesa[j];
                document.getElementById("vsisa" + i).value = vsisa[j];
                //document.getElementById("vlebih" + i).value = vlebih[j];
            }
            document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) - 1;
            jsDlgHide("#konten *", "#fade", "#light");
        }
    }

    function validasi() {
        jml = document.getElementById("jml").value;
        vjmlbyr = parseFloat(formatulang(document.getElementById("vjumlah").value));
        jmlitem = 0;
        for (i = 1; i <= jml; i++) {
            vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + i).value));
            jmlitem = jmlitem + vjmlitem;
        }
        if (vjmlbyr < jmlitem) {
            return false;
        } else {
            return true;
        }
    }

    function view_bayar(a) {
        a = document.getElementById("iarea").value;
        b = document.getElementById("icustomer").value;
        c = document.getElementById("dalokasi").value;
        d = parseFloat(formatulang(document.getElementById("vjumlah").value));
        showModal("akt-bankin-mt/cform/giro/" + a + "/" + b + "/" + c + "/" + d + "/", "#light");
        jsDlgShow("#konten *", "#fade", "#light");
    }
</script>

<?php 
# hutang lain2 (piutang dagang) khusus yg belum ketahuan customernya
?>