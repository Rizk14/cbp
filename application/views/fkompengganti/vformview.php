<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'fkompengganti/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="10" align="center">Cari data :
										<input type="text" id="cari" name="cari" value="">
										<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
										<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari">
									</td>
								</tr>
							</thead>
							<th>No Faktur</th>
							<th>No Nota</th>
							<th>Tgl Nota</th>
							<th>Sales</th>
							<th>Customer</th>
							<th>Jth tmp</th>
							<th>Kotor</th>
							<th>Discount</th>
							<th>Netto</th>
							<th>Act</th>
							<tbody>
								<?php 
								if ($isi) {
									foreach ($isi as $row) {
										$tmp = explode('-', $row->d_nota);
										$tgl = $tmp[2];
										$bln = $tmp[1];
										$thn = $tmp[0];
										$row->d_nota = $tgl . '-' . $bln . '-' . $thn;
										if ($row->d_jatuh_tempo != '') {
											$tmp = explode('-', $row->d_jatuh_tempo);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_jatuh_tempo = $tgl . '-' . $bln . '-' . $thn;
										}
										echo "<tr> 
				  <td>$row->i_faktur_komersial</td>
				  <td>$row->i_nota</td>
				  <td>$row->d_nota</td>
				  <td>$row->i_salesman</td>
				  <td>$row->e_customer_name</td>
				  <td>$row->d_jatuh_tempo</td>
				  <td align=right>" . number_format($row->v_nota_gross) . "</td>
				  <td align=right>" . number_format($row->v_nota_discounttotal) . "</td>
          <td align=right>" . number_format($row->v_nota_netto) . "</td>";
										echo "  
				  <td class=\"action\">
					<a href=\"#\" onclick='show(\"fkompengganti/cform/approve/$row->i_nota/$row->i_area/$dfrom/$dto/$row->i_faktur_komersial/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a></td></tr>";
									}
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('fkompengganti/cform/','#main')">
<script language="javascript" type="text/javascript">
	function yyy(b, c) {
		document.getElementById("ispbedit").value = b;
		document.getElementById("iareaedit").value = c;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/fkompengganti/cform/approve";
		formna.submit();
	}
</script>