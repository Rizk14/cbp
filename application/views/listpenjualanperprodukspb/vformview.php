<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperprodukspb/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
      $ntotal=0;
      $vtotal=0;
      $nsubtotal=0;
      $vsubtotal=0;
      $ngrandtotal=0;
      $vgrandtotal=0;
?>
	  <th>KODE</th>
		<th>NAMA PRODUK</th>
        
    <?php 
       $kol=4;
       foreach($areanya as $row)
       {
  		    echo "<th>".$row->i_area."</th>";
          $kol++;
	     }
       $sub=$kol-2;
      echo "<th>Jml Total</th>";
      echo "<th>Nilai Total</th>";
	    echo '<tbody>';
      $group='';
      foreach($prodnya as $row)
      {
        if($group==''){
          echo "<tr><td colspan=$kol><h2>$row->e_product_groupname</h2></td></tr>";
        }
        
        if($group!='' && $group!=$row->e_product_groupname){
          echo "<tr><td colspan=$sub><h2>Sub Total</h2></td>
                <td align=right>".number_format($nsubtotal)."</td>
                <td align=right>".number_format($vsubtotal)."</td></tr>";
          echo "<tr><td colspan=$kol><h2>$row->e_product_groupname</h2></td></tr>";
          $nsubtotal=0;
          $vsubtotal=0;
        }
        echo "<tr><td>$row->i_product</td><td>$row->e_product_name</td>";
        foreach($areanya as $raw)
        {
          $ada=false;
          foreach($isi as $riw)
          {
            if( ($riw->i_product==$row->i_product) && ($raw->i_area==$riw->i_area) ){
              $ada=true;
              echo "<td align=right>$riw->jumlah</td>";
              $ntotal=$ntotal+$riw->jumlah;
              $vtotal=$vtotal+$riw->nilai;
              $nsubtotal=$nsubtotal+$riw->jumlah;
              $vsubtotal=$vsubtotal+$riw->nilai;
              $ngrandtotal=$ngrandtotal+$riw->jumlah;
              $vgrandtotal=$vgrandtotal+$riw->nilai;
            }
            if($ada)break;
          }
          if(!$ada){
            echo "<td align=right>0</td>";
          }
	      }
        echo "<td align=right>".number_format($ntotal)."</td>";
        echo "<td align=right>".number_format($vtotal)."</td>";
        echo "</tr>";
        $ntotal=0;
        $vtotal=0;
        $group=$row->e_product_groupname;
      }
      echo "<tr><td colspan=$sub><h2>Sub Total</h2></td>
                <td align=right>".number_format($nsubtotal)."</td>
                <td align=right>".number_format($vsubtotal)."</td></tr>";
      echo "<tr><td colspan=$sub><h2>Grand Total</h2></td>
                <td align=right>".number_format($ngrandtotal)."</td>
                <td align=right>".number_format($vgrandtotal)."</td></tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listpenjualanperprodukspb/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
