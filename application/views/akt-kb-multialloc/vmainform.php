<div id="tmpx">
	<?php
	echo "<h2>$page_title</h2>";
	if (($ikb == '') && (($dfrom == '') || ($dto == '')) && ($ialokasi == '')) {
		$data['dfrom']	= $dfrom;
		$data['dto']		= $dto;
		$data['ikb']		= '';
		$data['isi']		= $isi;
		$this->load->view('akt-kb-multialloc/vform', $data);
	} elseif (($ikb != '') && (($dfrom != '') || ($dto != '')) && ($ialokasi == '')) {
		$data['jmlitem']	= $jmlitem;
		$data['isi']			= $isi;
		$data['detail']		= $detail;
		$data['ikb']		= $ikb;
		$data['eareaname'] = $eareaname;
		$data['vsisa']		= $vsisa;
		$data['dfrom']		= $dfrom;
		$data['dto']			= $dto;
		$data['dkb']		= $dkb;
		$this->load->view('akt-kb-multialloc/vformapprove', $data);
	} elseif ((($dfrom != '') || ($dto != '')) && ($ikb == '') && ($ialokasi == '')) {
		$data['isi'] 		= $isi;
		$data['ikb'] 		= '';
		$data['ialokasi']   = '';
		$data['dfrom']	    = $dfrom;
		$data['dto']		= $dto;
		$this->load->view('akt-kb-multialloc/vformview', $data);
	} else {
		$data['jmlitem'] = $jmlitem;
		$data['isi']		= $isi;
		$data['detail']	= $detail;
		$data['ikb']		= $ikb;
		$data['isupplier']	= $isupplier;
		$data['dfrom']	= $dfrom;
		$data['dto']		= $dto;
		$data['vbulat'] = $vbulat;
		$data['vsisa']	= $vsisa;
		$data['bisaedit'] = $bisaedit;
		$this->load->view('akt-kb-multialloc/vformupdate', $data);
	}
	?>
</div>