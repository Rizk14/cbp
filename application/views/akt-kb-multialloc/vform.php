<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'akt-kb-multialloc/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="areafrom" name="areafrom" value="">
									<?php
									$data = array(
										'name'        => 'dfrom',
										'id'          => 'dfrom',
										'value'       => date('01-m-Y'),
										'readonly'    => 'true',
										'onclick'	  => "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'        => 'dto',
										'id'          => 'dto',
										'value'       => date('d-m-Y'),
										'readonly'    => 'true',
										'onclick'	  => "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Kode CoA</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="text" id="icoa" name="icoa" value="" onkeyup="bacacoa(this.value)" maxlength="9" placeholder="Ketik Kd CoA">
									<font style="color:#FF0000;"><b>*Kode harus lengkap</b></font>
								</td>
							</tr>
							<tr>
								<td width="19%">Nama CoA</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="text" style="width:22%" id="ecoaname" name="ecoaname" value="" readonly>
								</td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<input name="login" id="login" value="View" type="submit" onclick="return validasi()">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('akt-kb-multialloc/cform/','#tmpx')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script>
	function bacacoa(coa) {
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('akt-kb-multialloc/cform/bacacoa'); ?>",
			data: "icoa=" + coa,
			success: function(data) {
				$("#ecoaname").val(data);
			},
			error: function(XMLHttpRequest) {
				alert(XMLHttpRequest.responseText);
			}
		})
	}

	function validasi() {
		if (document.getElementById('dfrom').value == "" || document.getElementById('dto').value == "") {
			alert("Pilih Tanggal terlebih dahulu!!!");
			return false;
		} else if (document.getElementById('icoa').value == "") {
			alert("Kode CoA tidak boleh kosong!!!");
			return false;
		} else if (document.getElementById('ecoaname').value == "") {
			alert("Kode CoA tidak diketahui!!!");
			return false;
		}
	}
</script>