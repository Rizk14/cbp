<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'akt-kb-multialloc/cform/view', 'update' => '#tmpx', 'type' => 'post')); ?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
										<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
										<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
										<input type="hidden" id="icoa" name="icoa" value="<?php echo $icoa; ?>">
										<input type="hidden" id="ecoaname" name="ecoaname" value="<?php echo $ecoaname; ?>">
									</td>
								</tr>
							</thead>
							<th>Kas Besar (Keluar / alokasi)</th>
							<th>Tanggal</th>
							<th>Area</th>
							<th>Jumlah</th>
							<th>sisa</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$tmp = explode("-", $row->d_kb);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$dkb = $hr . "-" . $bl . "-" . $th;
										$nama = str_replace('%20', ' ', $row->e_area_name);
										echo "<tr>
												<td>$row->i_kb</td>
												<td>$dkb</td>
												<td>$nama</td>
												<td align=right>" . number_format($row->v_kb) . "</td>
												<td align=right>" . number_format($row->v_sisa) . "</td>
												<td class=\"action\">
													<a href=\"#\" onclick='show(\"akt-kb-multialloc/cform/approve/$row->i_kb/$row->i_area/$nama/$row->v_kb/$dfrom/$dto/$dkb/$row->v_sisa/$icoa/$ecoaname/\",\"#main\")'>
														<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
													</a>
												</td>
											</tr>";
									}
									echo "<input type=\"hidden\" id=\"inotaedit\" name=\"inotaedit\" value=\"\">";
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
					</div>
				</div>
			</div>
			<?= form_close() ?>
			<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('akt-kb-multialloc/cform/','#tmpx')">
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function yyy(b) {
		document.getElementById("inotaedit").value = b;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/akt-kb-multialloc/cform/approve";
		formna.submit();
	}
</script>