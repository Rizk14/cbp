<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'pelunasan-ap/cform/updatepelunasan','update'=>'#pesan','type'=>'post'));?>
	<div id="pelunasanhutangform">
	<div class="effect">
	  <div class="accordion2">
		<?php foreach($isi as $row){
			if($row->d_bukti!=''){
				$tmp=explode('-',$row->d_bukti);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
			}
			if($row->d_giro!=''){
				$tmp=explode('-',$row->d_giro);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_giro=$tgl.'-'.$bln.'-'.$thn;
			}
			if($row->d_cair!=''){
				$tmp=explode('-',$row->d_cair);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_cair=$tgl.'-'.$bln.'-'.$thn;
			}
		?>
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Nomor DT</td>
		<td><input type="text" id="ipelunasanap" name="ipelunasanap" value="<?php echo $row->i_pelunasanap; ?>" readonly>
			<input readonly id="dbukti" name="dbukti" value="<?php echo $row->d_bukti; ?>" onclick="showCalendar('',this,this,'','ddt',0,20,1)">
			</td>
		<td>Giro/Cek</td>
		<td><input type="hidden" id ="ijenisbayar"name="ijenisbayar" value="<?php echo $row->i_jenis_bayar; ?>">
			<input readonly id="ejenisbayarname" name="ejenisbayarname" value="<?php echo $row->e_jenis_bayarname; ?>"
			 onclick='showModal("pelunasan/cform/girocek/","#light"); jsDlgShow("#konten *", "#fade", "#light");'>
			<input readonly id="igiro" name="igiro" value="<?php echo $row->i_giro; ?>" onclick="viewgiro();">
			</td>
	      </tr>
	      <tr>
		<td><!--Area--></td>
		<td><!--<input readonly id="eareaname" name="eareaname" value="<?php echo $row->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $row->i_area; ?>">--></td>
		<td>Bank</td>
		<td><input readonly id="egirobank" name="egirobank" value="<?php echo $row->e_bank_name; ?>"></td>
	      </tr>
	      <tr>
		<td>Debitur</td>
		<td><input type="text" readonly id="esuppliername" name="esuppliername" value="<?php echo $row->e_supplier_name; ?>" 
			onclick='showModal("pelunasan-ap/cform/supplier/<?php echo $row->i_dt; ?>/<?php echo $row->i_area; ?>/","#light"); jsDlgShow("#konten *", "#fade", "#light");'>
			<input type="hidden" readonly id="isupplier" name="isupplier" value="<?php echo $row->i_supplier; ?>"></td>
		<td>Tanggal JT</td>
		<td><input readonly id="djt" name="djt" value="">
			<input readonly type="hidden" id="dgiro" name="dgiro" value="<?php echo $row->d_giro ?>"></td>
	      </tr>
	      <tr>
		<td>ALamat</td>
		<td><input id="esupplieraddress" name="esupplieraddress" readonly value="<?php echo $row->e_supplier_address; ?>"></td>
		<td>Tanggal Cair</td>
		<td><input readonly id ="dcair"name="dcair" value="<?php if($row->d_cair!='0') echo $row->d_cair; ?>"></td>
	      </tr>
	      <tr>
		<td>Kota</td>
		<td><input readonly id="esuppliercity" name="esuppliercity" value="<?php echo $row->e_supplier_city; ?>"></td>
		<td>Jumlah</td>
		<td><input type="text" id="vjumlah" name="vjumlah" value="<?php echo number_format($row->v_jumlah); ?>" onKeyUp="reformat(this);hitung();">
			<input type="hidden" id="vlebih" name="vlebih" value="<?php echo $row->v_lebih; ?>" >
			<input type="hidden" id="vsisa" name="vsisa" value="<?php echo $vsisa; ?>" >
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <!--<input disabled name="login" id="login" value="Simpan" type="submit" onclick="dipales()">-->
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listpelunasanap/cform/view/<?php echo $dfrom."/".$dto."/".$isupplier."/"; ?>","#main")'>
		    <!--<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'>--></td>
		</tr>
	    </table>
		<?php }?>
			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:760px;">
					<th style="width:30px;" align="center">No</th>
					<th style="width:120px;" align="center">Nota</th>
					<th style="width:80px;" align="center">Tgl Nota</th>
					<th style="width:265px;" align="center">Jumlah bayar</th>
					<th style="width:265px;" align="center">Sisa</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<table id="itemtem" class="listtable" style="width:750px;">
				<?php 
					$i=0;
					foreach($detail as $row){ 
					$i++;
					  if($row->d_dtap!=''){
						  $tmp=explode('-',$row->d_dtap);
						  $tgl=$tmp[2];
						  $bln=$tmp[1];
						  $thn=$tmp[0];
						  $row->d_dtap=$tgl.'-'.$bln.'-'.$thn;
					  }
	
				?>
					<tbody><tr><td style="width:27px;"><input style="width:27px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>"></td><td style="width:115px;"><input style="width:115px;" readonly type="text" id="inota<?php echo $i; ?>" name="inota<?php echo $i; ?>" value="<?php echo $row->i_dtap; ?>"></td><td style="width:76px;"><input style="width:76px;" readonly type="text" id="dnota<?php echo $i; ?>" name="dnota<?php echo $i; ?>" value="<?php echo $row->d_dtap; ?>"></td><td style="width:256px;"><input style="width:256px;text-align:right;"  type="text" id="vjumlah<?php echo $i; ?>" name="vjumlah<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>" onkeyup="reformat(this);hetang(<?php echo $i; ?>);"></td><td style="width:255px;"><input readonly style="width:255px;text-align:right;"  type="text" id="vsisa<?php echo $i; ?>" name="vsisa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa); ?>"></td></tr></tbody>
				<?php }?>
			</div>
			</table>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
	cek='false';
	if((document.getElementById("ipelunasanap").value!='') &&
	  (document.getElementById("ijenisbayar").value!='') &&
	  (document.getElementById("vjumlah").value!='') &&
	  (document.getElementById("isupplier").value!='')) {
	  var a=parseFloat(document.getElementById("jml").value);
	  for(i=1;i<=a;i++){
		if(document.getElementById("vjumlah"+i).value!='0'){
		  cek='true';
		  break;
		}else{
		  cek='false';
		} 
	  }
	
	  if(cek=='true'){
	    document.getElementById("login").disabled=true;
	    document.getElementById("cmdtambahitem").disabled=true;
	  }else{
		alert('Isi jumlah detail pelunasan minimal 1 item !!!');
	    document.getElementById("login").disabled=false;
	  }
	}else{
	  alert('Data header masih ada yang salah !!!');
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:760px;">';
		so_inner+= '<th style="width:30px;" align="center">No</th>';
		so_inner+= '<th style="width:120px;" align="center">Nota</th>';
		so_inner+= '<th style="width:80px;" align="center">Tgl Nota</th>';
		so_inner+= '<th style="width:265px;" align="center">Jumlah bayar</th>';
		so_inner+= '<th style="width:265px;" align="center">Sisa</th>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<tbody><tr><td style="width:29px;"><input style="width:29px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		si_inner+='<td style="width:118px;"><input style="width:118px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""></td>';
		si_inner+='<td style="width:78px;"><input style="width:78px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		si_inner+='<td style="width:257px;"><input style="width:257px;text-align:right;"  type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeyup="reformat(this);hetang('+a+');"></td>';
		si_inner+='<td style="width:257px;"><input readonly style="width:257px;text-align:right;"  type="text" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td></tr></tbody>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner+='<tbody><tr><td style="width:29px;"><input style="width:29px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		si_inner+='<td style="width:118px;"><input style="width:118px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""></td>';
		si_inner+='<td style="width:78px;"><input style="width:78px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		si_inner+='<td style="width:257px;"><input style="width:257px;text-align:right;"  type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeyup="reformat(this);hetang('+a+');"></td>';
		si_inner+='<td style="width:257px;"><input readonly style="width:257px;text-align:right;"  type="text" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td></tr></tbody>';
    }
    j=0;
    var baris			=Array();
    var inota			=Array();
    var dnota			=Array();
	var vjumlah			=Array();
    var vsisa			=Array();
    for(i=1;i<a;i++){
		j++;
		baris[j]			= document.getElementById("baris"+i).value;
		inota[j]			= document.getElementById("inota"+i).value;
		dnota[j]			= document.getElementById("dnota"+i).value;
		vjumlah[j]			= document.getElementById("vjumlah"+i).value;	
		vsisa[j]			= document.getElementById("vsisa"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("inota"+i).value=inota[j];
		document.getElementById("dnota"+i).value=dnota[j];
		document.getElementById("vjumlah"+i).value=vjumlah[j];	
		document.getElementById("vsisa"+i).value=vsisa[j];	
    }
	area=document.getElementById("iarea").value;
	supp=document.getElementById("isupplier").value;
	showModal("pelunasan-ap/cform/idtap/"+a+"/"+supp+"/"+area+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function hitung(){
	jml=document.getElementById("jml").value;
	vjml=formatulang(document.getElementById("vjumlah").value);
	vjmlitem=0;
	for(i=1;i<=jml;i++){
		vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
	}
	document.getElementById("vlebih").value=parseFloat(formatulang(vjml))-vjmlitem;
  }
  function viewgiro()
  {
	jenis=document.getElementById("ijenisbayar").value;
	isupplier=document.getElementById("isupplier").value;
	iarea=document.getElementById("iarea").value;
	if(jenis=='01'){
		showModal("pelunasan/cform/giro/"+isupplier+"/"+iarea+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='02'){
		
//		showModal("pelunasan/cform/tunai/","#light"); 
//		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='03'){
		showModal("pelunasan/cform/ku/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='04'){
		showModal("pelunasan/cform/kn/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}else if(jenis=='05'){
		showModal("pelunasan/cform/lebihbayar/"+isupplier+"/"+iarea+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}
  }
  function hetang(a)
  {	
	vjmlbyr	  = parseFloat(formatulang(document.getElementById("vjumlah").value));
	vlebih 	  = parseFloat(formatulang(document.getElementById("vlebih").value));
	vsisadt   = parseFloat(formatulang(document.getElementById("vsisa").value));
	vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
	vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
	jml		= document.getElementById("jml").value;
	if(vjmlitem>vsisaitem){
		alert("jumlah bayar tidak boleh melebihi sisa nota !!!!!");
		if(vlebih>vsisaitem){
			document.getElementById("vjumlah"+a).value=formatcemua(vsisaitem);
		}else{
			document.getElementById("vjumlah"+a).value=formatcemua(vlebih);
		}
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
		}
		document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
	}else if(vjmlitem>vjmlbyr){
		alert("jumlah bayar item tidak boleh melebihi jumlah bayar total !!!!!");
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			if(i!=a){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
		}	
		document.getElementById("vjumlah"+a).value=vjmlbyr-vjmlitem;
		document.getElementById("vlebih").value="0";
	}else if(vjmlitem>vsisadt){
		alert("jumlah bayar tidak boleh melebihi sisa tagihan !!!!!");
		document.getElementById("vjumlah"+a).value="0";
	}else{
		vjmlitem=0;
		for(i=1;i<=jml;i++){
			vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
		}
		if(vjmlitem>vjmlbyr){
			alert("jumlah item tidak boleh melebihi jumlah bayar !!!!!");
			document.getElementById("vjumlah"+a).value="0";
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
		}
		document.getElementById("vlebih").value=vjmlbyr-vjmlitem;
	}
  }
</script>
