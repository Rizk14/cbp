<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'pelunasan-ap/cform/girocek';
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode jenis Bayar</th>
	    	<th>Nama</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_jenis_bayar','$row->e_jenis_bayarname')\">$row->i_jenis_bayar</a></td>
				  <td><a href=\"javascript:setValue('$row->i_jenis_bayar','$row->e_jenis_bayarname')\">$row->e_jenis_bayarname</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
    var dpl;
    if(a=='02'){
      dpl	= document.getElementById("dbukti").value;
	    document.getElementById("dcair").value = dpl;
	    document.getElementById("dcairx").value = dpl;
	    document.getElementById("egirobank").value = '';
	    document.getElementById("dgiro").value = '';
	    document.getElementById("djt").value = '';
	    document.getElementById("dku").value = '';
    }else {
    	document.getElementById("dcair").value = '';
    	document.getElementById("dcairx").value = '';
      document.getElementById("egirobank").value = '';
	    document.getElementById("dku").value = '';
	    document.getElementById("dgiro").value = '';
	    document.getElementById("djt").value = '';
    }
    document.getElementById("ijenisbayar").value=a;
    document.getElementById("ejenisbayarname").value=b;
    jsDlgHide("#konten *", "#fade", "#light");
    disdis();
  }
  function bbatal(){
	  jsDlgHide("#konten *", "#fade", "#light");
  }
  function disdis(){
    a=document.getElementById("ijenisbayar").value;
    if(a=='02'){
      document.getElementById("dcairx").disabled=false;
      document.getElementById("vjumlah").readOnly=false;
    }else{
      document.getElementById("vjumlah").readOnly=true;
    }
    document.getElementById("igiro").value='';
    document.getElementById("vjumlah").value='';
  }
</script>
