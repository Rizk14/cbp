<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'pelunasan-ap/cform/giro';
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No Giro</th>
	    	<th>Tgl JT</th>
	    	<th>Tgl Cair</th>
	    	<th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_giro);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_giro=$dd."-".$mm."-".$yy;
			  $tmp=explode("-",$row->d_giro_duedate);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_giro_duedate=$dd."-".$mm."-".$yy;
			  if(isset($row->d_giro_cair)){
				  $tmp=explode("-",$row->d_giro_cair);
				  $dd=$tmp[2];
				  $mm=$tmp[1];
				  $yy=$tmp[0];
				  $row->d_giro_cair=$dd."-".$mm."-".$yy;
			  }
			  $row->v_jumlah=number_format($row->v_jumlah);
			  $row->v_sisa=number_format($row->v_sisa);
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_giro','$row->d_giro_cair','$row->d_giro_duedate','$row->v_jumlah','$row->v_sisa','$row->e_giro_bank','$row->d_giro','$row->i_pv')\">$row->i_giro</a></td>
			  <td><a href=\"javascript:setValue('$row->i_giro','$row->d_giro_cair','$row->d_giro_duedate','$row->v_jumlah','$row->v_sisa','$row->e_giro_bank','$row->d_giro','$row->i_pv')\">$row->d_giro_duedate</a></td>
			  <td><a href=\"javascript:setValue('$row->i_giro','$row->d_giro_cair','$row->d_giro_duedate','$row->v_jumlah','$row->v_sisa','$row->e_giro_bank','$row->d_giro','$row->i_pv')\">$row->d_giro_cair</a></td>
			  <td><a href=\"javascript:setValue('$row->i_giro','$row->d_giro_cair','$row->d_giro_duedate','$row->v_jumlah','$row->v_sisa','$row->e_giro_bank','$row->d_giro','$row->i_pv')\">$row->v_jumlah</a></td>
			  <td><a href=\"javascript:setValue('$row->i_giro','$row->d_giro_cair','$row->d_giro_duedate','$row->v_jumlah','$row->v_sisa','$row->e_giro_bank','$row->d_giro','$row->i_pv')\">$row->v_sisa</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h)
  {
    document.getElementById("igiro").value=a;
    document.getElementById("ipv").value=h;
    document.getElementById("egirobank").value=f;
    document.getElementById("dcair").value=b;
    document.getElementById("djt").value=c;
	document.getElementById("dgiro").value=g;
    document.getElementById("vjumlah").value=e;//d;
//    document.getElementById("vsisa").value=e;
    document.getElementById("vlebih").value=e;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
