<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listcustomercardspb/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
	  <th rowspan=2>AREA</th>
	  <th rowspan=2>K-LANG</th>
	  <th rowspan=2>KOTA/KAB</th>
    <th rowspan=2>DIVISI</th>
	  <th rowspan=2>SALESMAN</th>
	  <th rowspan=2>JENIS</th>
		<th rowspan=2>NAMA LANG</th>
		<th rowspan=2>ALAMAT</th>
		<th rowspan=2>Kode</th>
		<th rowspan=2>Product</th>
    <th rowspan=2>Harga</th>
    <th rowspan=2>Supplier</th>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($bl,'integer');
	  }
    $bl=$blasal;
?>
    <th colspan=<?php echo $interval; ?> align=center>SPB</th>
<?php 
    echo '<th rowspan=2>Total SPB</th>';
#    echo '<th rowspan=2>Nilai</th>';
?>
    </tr>
    <tr>
<?php 
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th>Jan</th>';
        break;
      case '2' :
        echo '<th>Feb</th>';
        break;
      case '3' :
        echo '<th>Mar</th>';
        break;
      case '4' :
        echo '<th>Apr</th>';
        break;
      case '5' :
        echo '<th>Mei</th>';
        break;
      case '6' :
        echo '<th>Jun</th>';
        break;
      case '7' :
        echo '<th>Jul</th>';
        break;
      case '8' :
        echo '<th>Agu</th>';
        break;
      case '9' :
        echo '<th>Sep</th>';
        break;
      case '10' :
        echo '<th>Okt</th>';
        break;
      case '11' :
        echo '<th>Nov</th>';
        break;
      case '12' :
        echo '<th>Des</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
#    echo '<th>Rata2 spb</th>';
?>
    </tr>
      <tbody>
<?php 
    
    $subtot01=0;
    $subtot02=0;
    $subtot03=0;
    $subtot04=0;
    $subtot05=0;
    $subtot06=0;
    $subtot07=0;
    $subtot08=0;
    $subtot09=0;
    $subtot10=0;
    $subtot11=0;
    $subtot12=0;
#    $vsubtot01=0;
#    $vsubtot02=0;
#    $vsubtot03=0;
#    $vsubtot04=0;
#    $vsubtot05=0;
#    $vsubtot06=0;
#    $vsubtot07=0;
#    $vsubtot08=0;
#    $vsubtot09=0;
#    $vsubtot10=0;
#    $vsubtot11=0;
#    $vsubtot12=0;
    $grandtot01=0;
    $grandtot02=0;
    $grandtot03=0;
    $grandtot04=0;
    $grandtot05=0;
    $grandtot06=0;
    $grandtot07=0;
    $grandtot08=0;
    $grandtot09=0;
    $grandtot10=0;
    $grandtot11=0;
    $grandtot12=0;
    $totarea01=0;
    $totarea02=0;
    $totarea03=0;
    $totarea04=0;
    $totarea05=0;
    $totarea06=0;
    $totarea07=0;
    $totarea08=0;
    $totarea09=0;
    $totarea10=0;
    $totarea11=0;
    $totarea12=0;
    $icity='';
    $kode='';
    $totkota=0;
#    $vtotkota=0;
    $totarea=0;
#    $vtotarea=0;
    $grandtotkota=0;
#    $vgrandtotkota=0;

		foreach($isi as $row){
      $total=0;
      if($icity=='' || ($icity==$row->icity && $kode==substr($row->kode,0,2)) ){
	    echo "<tr>
              <td>".substr($row->kode,0,2)."-".$row->area."</td>
              <td>$row->kode</td>
              <td>$row->kota</td>
              <td>$row->group</td>
              <td>$row->sales</td>
          	  <td>$row->jenis</td>
              <td>$row->nama</td>
              <td>$row->alamat</td>
              <td>$row->product</td>
              <td>$row->productname</td>
              <td>$row->unitprice</td>
              <td>$row->supplier</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            $total=$total+$row->spbjan*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjan).'</th>';
            $subtot01=$subtot01+$row->spbjan;
#            $vsubtot01=$vsubtot01+($row->spbjan*$row->harga);
            $totarea01=$totarea01+$row->spbjan;
            $grandtot01=$grandtot01+$row->spbjan;
            $totkota=$totkota+$row->spbjan;
#            $vtotkota=$vtotkota+($row->spbjan*$row->harga);
            $totarea=$totarea+$row->spbjan;
#            $vtotarea=$vtotarea+($row->spbjan*$row->harga);
            break;
          case '2' :
            $total=$total+$row->spbfeb*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbfeb).'</th>';
            $subtot02=$subtot02+$row->spbfeb;
#            $vsubtot02=$vsubtot02+($row->spbfeb*$row->harga);
            $totarea02=$totarea02+$row->spbfeb;
            $grandtot02=$grandtot02+$row->spbfeb;
            $totkota=$totkota+$row->spbfeb;
#            $vtotkota=$vtotkota+($row->spbfeb*$row->harga);
            $totarea=$totarea+$row->spbfeb;
#            $vtotarea=$vtotarea+($row->spbfeb*$row->harga);
            break;
          case '3' :
            $total=$total+$row->spbmar*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmar).'</th>';
            $subtot03=$subtot03+$row->spbmar;
#            $vsubtot03=$vsubtot03+($row->spbmar*$row->harga);
            $totarea03=$totarea03+$row->spbmar;
            $grandtot03=$grandtot03+$row->spbmar;
            $totkota=$totkota+$row->spbmar;
#            $vtotkota=$vtotkota+($row->spbmar*$row->harga);
            $totarea=$totarea+$row->spbmar;
#            $vtotarea=$vtotarea+($row->spbmar*$row->harga);
            break;
          case '4' :
            $total=$total+$row->spbapr*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbapr).'</th>';
            $subtot04=$subtot04+$row->spbapr;
#            $vsubtot04=$vsubtot04+($row->spbapr*$row->harga);
            $totarea04=$totarea04+$row->spbapr;
            $grandtot04=$grandtot04+$row->spbapr;
            $totkota=$totkota+$row->spbapr;
#            $vtotkota=$vtotkota+($row->spbapr*$row->harga);
            $totarea=$totarea+$row->spbapr;
#            $vtotarea=$vtotarea+($row->spbapr*$row->harga);
            break;
          case '5' :
            $total=$total+$row->spbmay*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmay).'</th>';
            $subtot05=$subtot05+$row->spbmay;
#            $vsubtot05=$vsubtot05+($row->spbmay*$row->harga);
            $totarea05=$totarea05+$row->spbmay;
            $grandtot05=$grandtot05+$row->spbmay;
            $totkota=$totkota+$row->spbmay;
#            $vtotkota=$vtotkota+($row->spbmay*$row->harga);
            $totarea=$totarea+$row->spbmay;
#            $vtotarea=$vtotarea+($row->spbmay*$row->harga);
            break;
          case '6' :
            $total=$total+$row->spbjun*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjun).'</th>';
            $subtot06=$subtot06+$row->spbjun;
#            $vsubtot06=$vsubtot06+($row->spbjun*$row->harga);
            $totarea06=$totarea06+$row->spbjun;
            $grandtot06=$grandtot06+$row->spbjun;
            $totkota=$totkota+$row->spbjun;
#            $vtotkota=$vtotkota+($row->spbjun*$row->harga);
            $totarea=$totarea+$row->spbjun;
#            $vtotarea=$vtotarea+($row->spbjun*$row->harga);
            break;
          case '7' :
            $total=$total+$row->spbjul*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjul).'</th>';
            $subtot07=$subtot07+$row->spbjul;
#            $vsubtot07=$vsubtot07+($row->spbjul*$row->harga);
            $totarea07=$totarea07+$row->spbjul;
            $grandtot07=$grandtot07+$row->spbjul;
            $totkota=$totkota+$row->spbjul;
#            $vtotkota=$vtotkota+($row->spbjul*$row->harga);
            $totarea=$totarea+$row->spbjul;
#            $vtotarea=$vtotarea+($row->spbjul*$row->harga);
            break;
          case '8' :
            $total=$total+$row->spbaug*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbaug).'</th>';
            $subtot08=$subtot08+$row->spbaug;
#            $vsubtot08=$vsubtot08+($row->spbaug*$row->harga);
            $totarea08=$totarea08+$row->spbaug;
            $grandtot08=$grandtot08+$row->spbaug;
            $totkota=$totkota+$row->spbaug;
#            $vtotkota=$vtotkota+($row->spbaug*$row->harga);
            $totarea=$totarea+$row->spbaug;
#            $vtotarea=$vtotarea+($row->spbaug*$row->harga);
            break;
          case '9' :
            $total=$total+$row->spbsep*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbsep).'</th>';
            $subtot09=$subtot09+$row->spbsep;
#            $vsubtot09=$vsubtot09+($row->spbsep*$row->harga);
            $totarea09=$totarea09+$row->spbsep;
            $grandtot09=$grandtot09+$row->spbsep;
            $totkota=$totkota+$row->spbsep;
#            $vtotkota=$vtotkota+($row->spbsep*$row->harga);
            $totarea=$totarea+$row->spbsep;
#            $vtotarea=$vtotarea+($row->spbsep*$row->harga);
            break;
          case '10' :
            $total=$total+$row->spbokt*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbokt).'</th>';
            $subtot10=$subtot10+$row->spbokt;
#            $vsubtot10=$vsubtot10+($row->spbokt*$row->harga);
            $totarea10=$totarea10+$row->spbokt;
            $grandtot10=$grandtot10+$row->spbokt;
            $totkota=$totkota+$row->spbokt;
#            $vtotkota=$vtotkota+($row->spbokt*$row->harga);
            $totarea=$totarea+$row->spbokt;
#            $vtotarea=$vtotarea+($row->spbokt*$row->harga);
            break;
          case '11' :
            $total=$total+$row->spbnov*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbnov).'</th>';
            $subtot11=$subtot11+$row->spbnov;
#            $vsubtot11=$vsubtot11+($row->spbnov*$row->harga);
            $totarea11=$totarea11+$row->spbnov;
            $grandtot11=$grandtot11+$row->spbnov;
            $totkota=$totkota+$row->spbnov;
#            $vtotkota=$vtotkota+($row->spbnov*$row->harga);
            $totarea=$totarea+$row->spbnov;
#            $vtotarea=$vtotarea+($row->spbnov*$row->harga);
            break;
          case '12' :
            $total=$total+$row->spbdes*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbdes).'</th>';
            $subtot12=$subtot12+$row->spbdes;
#            $vsubtot12=$vsubtot12+($row->spbdes*$row->harga);
            $totarea12=$totarea12+$row->spbdes;
            $grandtot12=$grandtot12+$row->spbdes;
            $totkota=$totkota+$row->spbdes;
#            $vtotkota=$vtotkota+($row->spbdes*$row->harga);
            $totarea=$totarea+$row->spbdes;
#            $vtotarea=$vtotarea+($row->spbdes*$row->harga);
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
########
#      if($kode='01') echo 'kode = '.substr($row->kode,0,2).'<br>';
#*#*#*#*#*
      }elseif( $kode!=substr($row->kode,0,2) ){
  	    echo "<tr>
                <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   K o t a</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot01).'</th>';
            break;
          case '2' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot02).'</th>';
            break;
          case '3' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot03).'</th>';
            break;
          case '4' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot04).'</th>';
            break;
          case '5' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot05).'</th>';
            break;
          case '6' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot06).'</th>';
            break;
          case '7' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot07).'</th>';
            break;
          case '8' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot08).'</th>';
            break;
          case '9' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot09).'</th>';
            break;
          case '10' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot10).'</th>';
            break;
          case '11' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot11).'</th>';
            break;
          case '12' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot12).'</th>';
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totkota).'</th></tr>';
#        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotkota).'</th></tr>';
        $grandtotkota=$grandtotkota+$totkota;
#        $vgrandtotkota=$vgrandtotkota+$vtotkota;
########
#        if($kode!=substr($row->kode,0,2)){
          echo "<tr>
                <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   A r e a</td>";
          $bl=$blasal;
          for($i=1;$i<=$interval;$i++){
            switch ($bl){
            case '1' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea01).'</th>';
              break;
            case '2' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea02).'</th>';
              break;
            case '3' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea03).'</th>';
              break;
            case '4' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea04).'</th>';
              break;
            case '5' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea05).'</th>';
              break;
            case '6' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea06).'</th>';
              break;
            case '7' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea07).'</th>';
              break;
            case '8' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea08).'</th>';
              break;
            case '9' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea09).'</th>';
              break;
            case '10' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea10).'</th>';
              break;
            case '11' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea11).'</th>';
              break;
            case '12' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea12).'</th>';
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
          echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea).'</th></tr>';
#          echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotarea).'</th></tr>';
          $totarea01=0;
          $totarea02=0;
          $totarea03=0;
          $totarea04=0;
          $totarea05=0;
          $totarea06=0;
          $totarea07=0;
          $totarea08=0;
          $totarea09=0;
          $totarea10=0;
          $totarea11=0;
          $totarea12=0;
          $vtotarea01=0;
          $vtotarea02=0;
          $vtotarea03=0;
          $vtotarea04=0;
          $vtotarea05=0;
          $vtotarea06=0;
          $vtotarea07=0;
          $vtotarea08=0;
          $vtotarea09=0;
          $vtotarea10=0;
          $vtotarea11=0;
          $vtotarea12=0;
          $totarea=0;
          $vtotarea=0;
#        }
########
        $subtot01=0;
        $subtot02=0;
        $subtot03=0;
        $subtot04=0;
        $subtot05=0;
        $subtot06=0;
        $subtot07=0;
        $subtot08=0;
        $subtot09=0;
        $subtot10=0;
        $subtot11=0;
        $subtot12=0;
########
#        $vsubtot01=0;
#        $vsubtot02=0;
#        $vsubtot03=0;
#        $vsubtot04=0;
#        $vsubtot05=0;
#        $vsubtot06=0;
#        $vsubtot07=0;
#        $vsubtot08=0;
#        $vsubtot09=0;
#        $vsubtot10=0;
#        $vsubtot11=0;
#        $vsubtot12=0;
########
        $totkota=0;
#        $vtotkota=0;
  	    echo "<tr>
                <td>".substr($row->kode,0,2)."-".$row->area."</td>
                <td>$row->kode</td>
                <td>$row->kota</td>
                <td>$row->sales</td>
            	  <td>$row->jenis</td>
                <td>$row->nama</td>
                <td>$row->alamat</td>
                <td>$row->product</td>
                <td>$row->productname</td>
                <td>$row->unitprice</td>
                <td>$row->supplier</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            $total=$total+$row->spbjan*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjan).'</th>';
            $subtot01=$subtot01+$row->spbjan;
            $totarea01=$totarea01+$row->spbjan;
            $grandtot01=$grandtot01+$row->spbjan;
            $totkota=$totkota+$row->spbjan;
#            $vtotkota=$vtotkota+($row->spbjan*$row->harga);
            $totarea=$totarea+$row->spbjan;
#            $vtotarea=$vtotarea+($row->spbjan*$row->harga);
            break;
          case '2' :
            $total=$total+$row->spbfeb*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbfeb).'</th>';
            $subtot02=$subtot02+$row->spbfeb;
            $totarea02=$totarea02+$row->spbfeb;
            $grandtot02=$grandtot02+$row->spbfeb;
            $totkota=$totkota+$row->spbfeb;
#            $vtotkota=$vtotkota+($row->spbfeb*$row->harga);
            $totarea=$totarea+$row->spbfeb;
#            $vtotarea=$vtotarea+($row->spbfeb*$row->harga);
            break;
          case '3' :
            $total=$total+$row->spbmar*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmar).'</th>';
            $subtot03=$subtot03+$row->spbmar;
            $totarea03=$totarea03+$row->spbmar;
            $grandtot03=$grandtot03+$row->spbmar;
            $totkota=$totkota+$row->spbmar;
#            $vtotkota=$vtotkota+($row->spbmar*$row->harga);
            $totarea=$totarea+$row->spbmar;
#            $vtotarea=$vtotarea+($row->spbmar*$row->harga);
            break;
          case '4' :
            $total=$total+$row->spbapr*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbapr).'</th>';
            $subtot04=$subtot04+$row->spbapr;
            $totarea04=$totarea04+$row->spbapr;
            $grandtot04=$grandtot04+$row->spbapr;
            $totkota=$totkota+$row->spbapr;
#            $vtotkota=$vtotkota+($row->spbapr*$row->harga);
            $totarea=$totarea+$row->spbapr;
#            $vtotarea=$vtotarea+($row->spbapr*$row->harga);
            break;
          case '5' :
            $total=$total+$row->spbmay*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmay).'</th>';
            $subtot05=$subtot05+$row->spbmay;
            $totarea05=$totarea05+$row->spbmay;
            $grandtot05=$grandtot05+$row->spbmay;
            $totkota=$totkota+$row->spbmay;
#            $vtotkota=$vtotkota+($row->spbmay*$row->harga);
            $totarea=$totarea+$row->spbmay;
#            $vtotarea=$vtotarea+($row->spbmay*$row->harga);
            break;
          case '6' :
            $total=$total+$row->spbjun*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjun).'</th>';
            $subtot06=$subtot06+$row->spbjun;
            $totarea06=$totarea06+$row->spbjun;
            $grandtot06=$grandtot06+$row->spbjun;
            $totkota=$totkota+$row->spbjun;
#            $vtotkota=$vtotkota+($row->spbjun*$row->harga);
            $totarea=$totarea+$row->spbjun;
#            $vtotarea=$vtotarea+($row->spbjun*$row->harga);
            break;
          case '7' :
            $total=$total+$row->spbjul*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjul).'</th>';
            $subtot07=$subtot07+$row->spbjul;
            $totarea07=$totarea07+$row->spbjul;
            $grandtot07=$grandtot07+$row->spbjul;
            $totkota=$totkota+$row->spbjul;
#            $vtotkota=$vtotkota+($row->spbjul*$row->harga);
            $totarea=$totarea+$row->spbjul;
#            $vtotarea=$vtotarea+($row->spbjul*$row->harga);
            break;
          case '8' :
            $total=$total+$row->spbaug*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbaug).'</th>';
            $subtot08=$subtot08+$row->spbaug;
            $totarea08=$totarea08+$row->spbaug;
            $grandtot08=$grandtot08+$row->spbaug;
            $totkota=$totkota+$row->spbaug;
#            $vtotkota=$vtotkota+($row->spbaug*$row->harga);
            $totarea=$totarea+$row->spbaug;
#            $vtotarea=$vtotarea+($row->spbaug*$row->harga);
            break;
          case '9' :
            $total=$total+$row->spbsep*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbsep).'</th>';
            $subtot09=$subtot09+$row->spbsep;
            $totarea09=$totarea09+$row->spbsep;
            $grandtot09=$grandtot09+$row->spbsep;
            $totkota=$totkota+$row->spbsep;
#            $vtotkota=$vtotkota+($row->spbsep*$row->harga);
            $totarea=$totarea+$row->spbsep;
#            $vtotarea=$vtotarea+($row->spbsep*$row->harga);
            break;
          case '10' :
            $total=$total+$row->spbokt*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbokt).'</th>';
            $subtot10=$subtot10+$row->spbokt;
            $totarea10=$totarea10+$row->spbokt;
            $grandtot10=$grandtot10+$row->spbokt;
            $totkota=$totkota+$row->spbokt;
#            $vtotkota=$vtotkota+($row->spbokt*$row->harga);
            $totarea=$totarea+$row->spbokt;
#            $vtotarea=$vtotarea+($row->spbokt*$row->harga);
            break;
          case '11' :
            $total=$total+$row->spbnov*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbnov).'</th>';
            $subtot11=$subtot11+$row->spbnov;
            $totarea11=$totarea11+$row->spbnov;
            $grandtot11=$grandtot11+$row->spbnov;
            $totkota=$totkota+$row->spbnov;
#            $vtotkota=$vtotkota+($row->spbnov*$row->harga);
            $totarea=$totarea+$row->spbnov;
#            $vtotarea=$vtotarea+($row->spbnov*$row->harga);
            break;
          case '12' :
            $total=$total+$row->spbdes*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbdes).'</th>';
            $subtot12=$subtot12+$row->spbdes;
            $totarea12=$totarea12+$row->spbdes;
            $grandtot12=$grandtot12+$row->spbdes;
            $totkota=$totkota+$row->spbdes;
#            $vtotkota=$vtotkota+($row->spbdes*$row->harga);
            $totarea=$totarea+$row->spbdes;
#            $vtotarea=$vtotarea+($row->spbdes*$row->harga);
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
#*#*#*#*#*
      }elseif( ($icity!='' && $icity!=$row->icity) ){
  	    echo "<tr>
                <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   K o t a</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot01).'</th>';
            break;
          case '2' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot02).'</th>';
            break;
          case '3' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot03).'</th>';
            break;
          case '4' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot04).'</th>';
            break;
          case '5' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot05).'</th>';
            break;
          case '6' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot06).'</th>';
            break;
          case '7' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot07).'</th>';
            break;
          case '8' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot08).'</th>';
            break;
          case '9' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot09).'</th>';
            break;
          case '10' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot10).'</th>';
            break;
          case '11' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot11).'</th>';
            break;
          case '12' :
            echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot12).'</th>';
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totkota).'</th></tr>';
#        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotkota).'</th></tr>';
        $grandtotkota=$grandtotkota+$totkota;
#        $vgrandtotkota=$vgrandtotkota+$vtotkota;
########
        if($kode!=substr($row->kode,0,2)){
          echo "<tr>
                <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   A r e a</td>";
          $bl=$blasal;
          for($i=1;$i<=$interval;$i++){
            switch ($bl){
            case '1' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea01).'</th>';
              break;
            case '2' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea02).'</th>';
              break;
            case '3' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea03).'</th>';
              break;
            case '4' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea04).'</th>';
              break;
            case '5' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea05).'</th>';
              break;
            case '6' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea06).'</th>';
              break;
            case '7' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea07).'</th>';
              break;
            case '8' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea08).'</th>';
              break;
            case '9' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea09).'</th>';
              break;
            case '10' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea10).'</th>';
              break;
            case '11' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea11).'</th>';
              break;
            case '12' :
              echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea12).'</th>';
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
          echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea).'</th></tr>';
#          echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotarea).'</th></tr>';
          $totarea01=0;
          $totarea02=0;
          $totarea03=0;
          $totarea04=0;
          $totarea05=0;
          $totarea06=0;
          $totarea07=0;
          $totarea08=0;
          $totarea09=0;
          $totarea10=0;
          $totarea11=0;
          $totarea12=0;
          $totarea=0;
#          $vtotarea=0;
        }
########
        $subtot01=0;
        $subtot02=0;
        $subtot03=0;
        $subtot04=0;
        $subtot05=0;
        $subtot06=0;
        $subtot07=0;
        $subtot08=0;
        $subtot09=0;
        $subtot10=0;
        $subtot11=0;
        $subtot12=0;
        $totkota=0;
#        $vtotkota=0;
  	    echo "<tr>
                <td>".substr($row->kode,0,2)."-".$row->area."</td>
                <td>$row->kode</td>
                <td>$row->kota</td>
                <td>$row->group</td>
                <td>$row->sales</td>
            	  <td>$row->jenis</td>
                <td>$row->nama</td>
                <td>$row->alamat</td>
                <td>$row->product</td>
                <td>$row->productname</td>
                <td>$row->unitprice</td>
                <td>$row->supplier</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            $total=$total+$row->spbjan*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjan).'</th>';
            $subtot01=$subtot01+$row->spbjan;
            $totarea01=$totarea01+$row->spbjan;
            $grandtot01=$grandtot01+$row->spbjan;
            $totkota=$totkota+$row->spbjan;
#            $vtotkota=$vtotkota+($row->spbjan*$row->harga);
            $totarea=$totarea+$row->spbjan;
#            $vtotarea=$vtotarea+($row->spbjan*$row->harga);
            break;
          case '2' :
            $total=$total+$row->spbfeb*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbfeb).'</th>';
            $subtot02=$subtot02+$row->spbfeb;
            $totarea02=$totarea02+$row->spbfeb;
            $grandtot02=$grandtot02+$row->spbfeb;
            $totkota=$totkota+$row->spbfeb;
#            $vtotkota=$vtotkota+($row->spbfeb*$row->harga);
            $totarea=$totarea+$row->spbfeb;
#            $vtotarea=$vtotarea+($row->spbfeb*$row->harga);
            break;
          case '3' :
            $total=$total+$row->spbmar*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmar).'</th>';
            $subtot03=$subtot03+$row->spbmar;
            $totarea03=$totarea03+$row->spbmar;
            $grandtot03=$grandtot03+$row->spbmar;
            $totkota=$totkota+$row->spbmar;
#            $vtotkota=$vtotkota+($row->spbmar*$row->harga);
            $totarea=$totarea+$row->spbmar;
#            $vtotarea=$vtotarea+($row->spbmar*$row->harga);
            break;
          case '4' :
            $total=$total+$row->spbapr*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbapr).'</th>';
            $subtot04=$subtot04+$row->spbapr;
            $totarea04=$totarea04+$row->spbapr;
            $grandtot04=$grandtot04+$row->spbapr;
            $totkota=$totkota+$row->spbapr;
#            $vtotkota=$vtotkota+($row->spbapr*$row->harga);
            $totarea=$totarea+$row->spbapr;
#            $vtotarea=$vtotarea+($row->spbapr*$row->harga);
            break;
          case '5' :
            $total=$total+$row->spbmay*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbmay).'</th>';
            $subtot05=$subtot05+$row->spbmay;
            $totarea05=$totarea05+$row->spbmay;
            $grandtot05=$grandtot05+$row->spbmay;
            $totkota=$totkota+$row->spbmay;
#            $vtotkota=$vtotkota+($row->spbmay*$row->harga);
            $totarea=$totarea+$row->spbmay;
#            $vtotarea=$vtotarea+($row->spbmay*$row->harga);
            break;
          case '6' :
            $total=$total+$row->spbjun*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjun).'</th>';
            $subtot06=$subtot06+$row->spbjun;
            $totarea06=$totarea06+$row->spbjun;
            $grandtot06=$grandtot06+$row->spbjun;
            $totkota=$totkota+$row->spbjun;
#            $vtotkota=$vtotkota+($row->spbjun*$row->harga);
            $totarea=$totarea+$row->spbjun;
#            $vtotarea=$vtotarea+($row->spbjun*$row->harga);
            break;
          case '7' :
            $total=$total+$row->spbjul*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbjul).'</th>';
            $subtot07=$subtot07+$row->spbjul;
            $totarea07=$totarea07+$row->spbjul;
            $grandtot07=$grandtot07+$row->spbjul;
            $totkota=$totkota+$row->spbjul;
#            $vtotkota=$vtotkota+($row->spbjul*$row->harga);
            $totarea=$totarea+$row->spbjul;
#            $vtotarea=$vtotarea+($row->spbjul*$row->harga);
            break;
          case '8' :
            $total=$total+$row->spbaug*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbaug).'</th>';
            $subtot08=$subtot08+$row->spbaug;
            $totarea08=$totarea08+$row->spbaug;
            $grandtot08=$grandtot08+$row->spbaug;
            $totkota=$totkota+$row->spbaug;
#            $vtotkota=$vtotkota+($row->spbaug*$row->harga);
            $totarea=$totarea+$row->spbaug;
#            $vtotarea=$vtotarea+($row->spbaug*$row->harga);
            break;
          case '9' :
            $total=$total+$row->spbsep*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbsep).'</th>';
            $subtot09=$subtot09+$row->spbsep;
            $totarea09=$totarea09+$row->spbsep;
            $grandtot09=$grandtot09+$row->spbsep;
            $totkota=$totkota+$row->spbsep;
#            $vtotkota=$vtotkota+($row->spbsep*$row->harga);
            $totarea=$totarea+$row->spbsep;
#            $vtotarea=$vtotarea+($row->spbsep*$row->harga);
            break;
          case '10' :
            $total=$total+$row->spbokt*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbokt).'</th>';
            $subtot10=$subtot10+$row->spbokt;
            $totarea10=$totarea10+$row->spbokt;
            $grandtot10=$grandtot10+$row->spbokt;
            $totkota=$totkota+$row->spbokt;
#            $vtotkota=$vtotkota+($row->spbokt*$row->harga);
            $totarea=$totarea+$row->spbokt;
#            $vtotarea=$vtotarea+($row->spbokt*$row->harga);
            break;
          case '11' :
            $total=$total+$row->spbnov*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbnov).'</th>';
            $subtot11=$subtot11+$row->spbnov;
            $totarea11=$totarea11+$row->spbnov;
            $grandtot11=$grandtot11+$row->spbnov;
            $totkota=$totkota+$row->spbnov;
#            $vtotkota=$vtotkota+($row->spbnov*$row->harga);
            $totarea=$totarea+$row->spbnov;
#            $vtotarea=$vtotarea+($row->spbnov*$row->harga);
            break;
          case '12' :
            $total=$total+$row->spbdes*$row->unitprice;
            echo '<th align=right>'.number_format($row->spbdes).'</th>';
            $subtot12=$subtot12+$row->spbdes;
            $totarea12=$totarea12+$row->spbdes;
            $grandtot12=$grandtot12+$row->spbdes;
            $totkota=$totkota+$row->spbdes;
#            $vtotkota=$vtotkota+($row->spbdes*$row->harga);
            $totarea=$totarea+$row->spbdes;
#            $vtotarea=$vtotarea+($row->spbdes*$row->harga);
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
      }
#      $total=$total/$interval;
      echo '<th align=right>'.number_format($total).'</th>';
#      echo '<th align=right>'.number_format($total*$row->harga).'</th>';
      echo "</tr>";
      $icity=$row->icity;
      $kode=substr($row->kode,0,2);
		}
#####
    echo "<tr>
          <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   K o t a</td>";
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot01).'</th>';
        break;
      case '2' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot02).'</th>';
        break;
      case '3' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot03).'</th>';
        break;
      case '4' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot04).'</th>';
        break;
      case '5' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot05).'</th>';
        break;
      case '6' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot06).'</th>';
        break;
      case '7' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot07).'</th>';
        break;
      case '8' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot08).'</th>';
        break;
      case '9' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot09).'</th>';
        break;
      case '10' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot10).'</th>';
        break;
      case '11' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot11).'</th>';
        break;
      case '12' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($subtot12).'</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totkota).'</th></tr>';
#    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotkota).'</th></tr>';
    $grandtotkota=$grandtotkota+$totkota;
#    $vgrandtotkota=$vgrandtotkota+$vtotkota;
    echo "<tr>
          <td style='background-color:#F2F2F2;' colspan=10 align=center>T o t a l   A r e a</td>";
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea01).'</th>';
        break;
      case '2' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea02).'</th>';
        break;
      case '3' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea03).'</th>';
        break;
      case '4' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea04).'</th>';
        break;
      case '5' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea05).'</th>';
        break;
      case '6' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea06).'</th>';
        break;
      case '7' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea07).'</th>';
        break;
      case '8' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea08).'</th>';
        break;
      case '9' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea09).'</th>';
        break;
      case '10' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea10).'</th>';
        break;
      case '11' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea11).'</th>';
        break;
      case '12' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea12).'</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($totarea).'</th></tr>';
#    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vtotarea).'</th></tr>';
    echo "<tr>
          <td style='background-color:#F2F2F2;' colspan=10 align=center>G r a n d   T o t a l</td>";
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot01).'</th>';
        break;
      case '2' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot02).'</th>';
        break;
      case '3' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot03).'</th>';
        break;
      case '4' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot04).'</th>';
        break;
      case '5' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot05).'</th>';
        break;
      case '6' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot06).'</th>';
        break;
      case '7' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot07).'</th>';
        break;
      case '8' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot08).'</th>';
        break;
      case '9' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot09).'</th>';
        break;
      case '10' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot10).'</th>';
        break;
      case '11' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot11).'</th>';
        break;
      case '12' :
        echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtot12).'</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($grandtotkota).'</th></tr>';
#    echo '<th style="background-color:#F2F2F2;" align=right>'.number_format($vgrandtotkota).'</th></tr>';
#####
	}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
