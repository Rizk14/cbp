<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab		= str_repeat(" ",9);
		$hal	= 1;
		$ipp    = new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$tmp=explode("-",$row->d_sjr);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_sjr=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$xmp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
		$ymp='';
		$i	= 0;
		$j	= 0;
		$hrg    = 0;
		$sj			= $row->i_sjr;
		$iarea	= substr($row->i_sjr,9,2);
		$query 	= $this->db->query(" select * from tm_sjr_item where i_sjr='$sj'",false);
		$jml 	= $query->num_rows();
		foreach($detail as $rowi){
      $cetak.=CHR(18);
      if($rowi->n_quantity_retur>0){
			  $i++;
			  $j++;
			  $pro	= $rowi->i_product;
			  if(strlen($rowi->e_product_name )>46){
				  $nam	= substr($rowi->e_product_name,0,46);
			  }else{
				  $nam	= $rowi->e_product_name.str_repeat(" ",46-strlen($rowi->e_product_name ));
			  }			
			  $del	= number_format($rowi->n_quantity_retur);
			  $pjg	= strlen($del);
			  $spcdel	= 7;
			  for($xx=1;$xx<=$pjg;$xx++){
				  $spcdel	= $spcdel-1;
			  }
			  $pric	= number_format($rowi->v_unit_price);
			  $pjg	= strlen($pric);
			  $aw	= 3;
			  $pjg	= strlen($i);
			  for($xx=1;$xx<=$pjg;$xx++){
				  $aw=$aw-1;
			  }+
			  $aw=str_repeat(" ",$aw);

			  $cetak.=$nor.CHR(179).$aw.$i." ".CHR(179).$pro."  ".$nam.CHR(179).str_repeat(" ",$spcdel).$del."  ".CHR(179)."\n";			  
			  if(($i%50)==0){
				  $cetak.=CHR(18).$nor.str_repeat('-',73)."\n";					  		
				  $hal=$hal+1;
				  $ymp=CetakHeader($row,$hal,$nor,$abn,$ab,$ipp);
				  $j	= 0;
			  }
      }
		}
		$cetak=$xmp.$cetak.$ymp;
		$zmp=CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp);
		$cetak=$cetak.$zmp;
	}
	$ipp->setBinary();
#	echo $cetak;
  $ipp->setFormFeed();
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";

	function CetakHeader($row,$hal,$nor,$abn,$ab,$ipp){
		$cetak =CHR(18);
		$cetak.=$nor.NmPerusahaan."\n\n";
    $cetak.=$nor.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1)."S U R A T   J A L A N                           ".CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."Kepada Yth.\n";
		$cetak.=$nor."PENGEMBALIAN STOK KE GUDANG PUSAT\n";
		$cetak.=$nor."No. ".$row->i_sjr."                            GUDANG PUSAT\n\n";
		$cetak.=$nor.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),55).CHR(194).str_repeat(CHR(196),9).CHR(191)."\n";
		$cetak.=$nor.CHR(179)."NO. ".CHR(179)." KODE                                                  ".CHR(179)." JUMLAH  ".CHR(179)."\n";
		$cetak.=$nor.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G                  ".CHR(179)." DIKIRIM ".CHR(179)."\n";				
		$cetak.=$nor.CHR(195).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),55).CHR(216).str_repeat(CHR(205),9).CHR(181)."\n";
		return $cetak;
	}
	function CetakFooter($row,$nor,$abn,$ab,$hrg,$j,$ipp){
#		$cetak =CHR(18);
		$cetak=$nor.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),55).CHR(193).str_repeat(CHR(196),9).CHR(217)."\n";
    $cetak.=$nor.str_repeat(' ',52)."Bandung, ".$row->d_sjr."\n";
    $cetak.=$nor." Penerima        Mengetahui                              Pengirim    \n\n\n\n";
    $cetak.=$nor."(          )    (           )                        (              )\n".CHR(15);
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    $cetak.=$ab."TANGGAL CETAK : ".$tgl.CHR(18);
		return $cetak;		
	}
?>
