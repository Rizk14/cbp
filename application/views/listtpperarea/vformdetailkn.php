<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>Detail KN $page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listtpperarea/cform/detailkn','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
          <th>Area</th>
     	    <th>No KN</th>
	        <th>Tgl KN</th>
	        <th>Jumlah</th>
     	    <th>Kodelang</th>
	        <th>Customer</th>
	        <th>Alamat</th>
	        <th>Kota</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->d_kn!=''){
          $tmp=explode('-',$row->d_kn);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_kn=$tgl.'-'.$bln.'-'.$thn;
        }
			  echo "<tr> 
          <td>$row->i_area</td>
				  <td>$row->i_kn</td>
				  <td>$row->d_kn</td>
				  <td align=right>".number_format($row->v_netto)."</td>
				  <td>$row->i_customer</td>
				  <td>$row->e_customer_name</td>
				  <td>$row->e_customer_address</td>
				  <td>$row->e_city_name</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <br>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
