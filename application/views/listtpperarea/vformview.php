<table class="maintable">
  <tr>
    <td align="left">
<!--  <?php echo form_open('listtpperarea/cform/cari', array('id' => 'listform'));?>-->
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listtpperarea/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
      <?php 
    $periode='';
    $tgl_proses='';
      if($isi)
      {
         $total=0;
         $real=0;
         foreach($isi as $row)
         {
            $periode=$row->i_periode;
            $total=$total+$row->v_target;
#           $real=$real+$row->v_nota_netto;
            $real=$real+$row->v_nota_gross;
            $tgl_proses = $row->d_process;
         }
      }
      else
      {
         $total=0;
         $real=0;
         $periode=$iperiode;
      }
      if($periode=='') $periode=$iperiode;
      if($total>0)
      {
         $persenrea=($real/$total)*100;
         $persenrea=number_format($persenrea,2);
      }
      else
      {
         $persenrea="0.00";
      }
      $a=substr($periode,0,4);
      $b=substr($periode,4,2);
      $periode=mbulan($b)." - ".$a;
      $total=number_format($total);
      $real=number_format($real);
      ?>
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $periode; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
               <?php if( empty($tgl_proses))
                  { echo "Belum Proses Update Penjualan";
                  }
                  else
                  { echo "Tgl Proses : " . date_format(date_create($tgl_proses),"d-m-Y H:i:s");
                  }
                ?>
            </td>
            <td>
            </td>
         </tr>
         <tr>
            <td style="width:100px;">Total Target</td>
            <td style="width:5px;">:</td>
            <td style="width:5px;"><?php echo "Rp."; ?></td>
            <td style="width:90px;text-align:right;"><?php echo $total; ?></td>
            <td style="width:5px;"> </td>
            <td style="width:500px"></td>
            <td></td>
         </tr>
         <tr>
            <td style="width:100px;">Total Realisasi</td>
            <td style="width:5px;">:</td>
            <td style="width:5px;"><?php echo "Rp."; ?></td>
            <td style="width:90px;text-align:right;"><?php echo $real ?></td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;"><?php echo "( ".$persenrea."% )"; ?></td>
            <td></td>
         </tr>
        </table>
        <table class="listtable">
       <thead>
         <tr>
      <td colspan="12" align="center">Cari data :  <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>" ><input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari">&nbsp;<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('listtpperarea/cform/','#main')"></td>
         </tr>
       </thead>
         <th>Area</th>
         <th>Target</th>
         <th>Penjualan</th>
         <th>%</th>
         <th>Reguler</th>
         <th>%</th>
         <th>Baby</th>
         <th>%</th>
         <th>SPB</th>
         <th>%</th>
       <tbody>
         <?php 
      if($isi){
         $ret=0;
      $target=0;
      $spb=0;
      $ntgross=0;
      $ntreguler=0;
      $ntbaby=0;
         foreach($isi as $row){
           if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_grossinsentif/$row->v_target)*100,2);
        }else{
          $persen='0.00';
        }
        if($row->v_real_regularinsentif==null || $row->v_real_regularinsentif=='')$row->v_real_regularinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenreg=number_format(($row->v_real_regularinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenreg='0.00';
        }
        if($row->v_real_babyinsentif==null || $row->v_real_babyinsentif=='')$row->v_real_babyinsentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenbaby=number_format(($row->v_real_babyinsentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenbaby='0.00';
        }
        if($row->v_spb_gross==null || $row->v_spb_gross=='')$row->v_spb_gross=0;
        if($row->v_target!=0){
          $persenspb=number_format(($row->v_spb_gross/$row->v_target)*100,2);
        }else{
          $persenspb='0.00';
        }
        $iarea=$row->i_area;
        $period=substr($iperiode,2,6);
           echo "<tr>
              <td style='font-size:12px;'><a href=\"#\" onclick='chartx(\"$iperiode\");'>$row->e_area_name</a></td>
              <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_target)."</td>
              <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_nota_grossinsentif)."&nbsp;
            <a href=\"#\" onclick='window.open(\"listtpperarea/cform/detailnota/$iarea/$period/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;<a href=\"#\" onclick='window.open(\"listtpperarea/cform/detailkn/$iarea/$row->i_periode/\",\"#main\")'>KN</a></td>
              <td style='font-size:12px;' align=right>".$persen." %</td>
              <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_real_regularinsentif)."</td>
              <td style='font-size:12px;' align=right>".$persenreg." %</td>
              <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_real_babyinsentif)."</td>
              <td style='font-size:12px;' align=right>".$persenbaby." %</td>
              <td style='font-size:12px;' align=right>Rp. ".number_format($row->v_spb_gross)."</td>
              <td style='font-size:12px;' align=right>".$persenspb." %</td></tr>";
        $target=$target+$row->v_target;
        $ntgross=$ntgross+$row->v_nota_grossinsentif;
        $ntreguler=$ntreguler+$row->v_real_regularinsentif;
        $ntbaby=$ntbaby+$row->v_real_babyinsentif;
        $spb=$spb+$row->v_spb_gross;
         }
        echo "<tr>
        <td style='font-size:12px;'><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>Rp. ".number_format($target)."</b></td>
        <td style='font-size:12px;' align=right><b>Rp. ".number_format($ntgross)."</b></td>
        <td style='font-size:12px;' align=right>&nbsp;</td>
        <td style='font-size:12px;' align=right><b>Rp. ".number_format($ntreguler)."</b></td>
        <td style='font-size:12px;' align=right>&nbsp;</td>
        <td style-='font-size:12px;' align=right><b>Rp. ".number_format($ntbaby)."</b></td>
        <td style='font-size:12px;' align=right>&nbsp;</td>
        <td style='font-size:12px;' align=right><b>Rp. ".number_format($spb)."</b></td>
        <td style='font-size:12px;' align=right>&nbsp;</td></tr>";

      }
      echo "<input type=\"hidden\" id=\"iperiode\" name=\"iperiode\" value=\"$iperiode\">
           <input type=\"hidden\" id=\"iarea\" name=\"iarea\" value=\"\">
           ";
         ?>
       </tbody>
     </table>
     <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function chartx(iperiode){
    lebar =1366;
    tinggi=768;
/*
    eval('window.open("<?php echo site_url(); ?>"+"/listtpperarea/cform/chartx/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
*/
/*
    eval('window.open("<?php echo site_url(); ?>"+"/listtpperarea/cform/fcf/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
*/
    show("listtpperarea/cform/fcf/"+iperiode,"#main");
  }
</script>
