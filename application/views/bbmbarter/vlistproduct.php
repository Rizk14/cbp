<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
</head>
<body id="bodylist">
<div id="main">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
<!--
	<?php 
		$tujuan='bbmbarter/cform/cariproduct/'.$baris.'/'.$ttb;
		echo form_open($tujuan, array('id' => 'listform'));
	?>
-->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'bbmbarter/cform/cariproduct/'.$baris.'/'.$ttb,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"></td>
	      </tr>
	    </thead>
      	    <th>Kode Product</th>
	    	<th>Nama</th>
			<th>Motif</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_quantity','$row->i_product_grade','$row->i_nota')\">$row->kode</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_quantity','$row->i_product_grade','$row->i_nota')\">$row->nama</a></td>
				  <td><a href=\"javascript:setValue('$row->kode','$row->nama','".number_format($row->harga)."',$baris,'$row->motif','$row->namamotif','$row->n_quantity','$row->i_product_grade','$row->i_nota')\">$row->namamotif</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h,x)
  {
    ada=false;
    for(i=1;i<=d;i++){
		if(
			(a==document.getElementById("iproduct"+i).value) && 
			(i!==d) && 
			(e==document.getElementById("iproductmotif"+i).value)
		  ){
			alert ("kode : "+a+" sudah ada !!!!!");
			ada=true;
			break;
		}else{
			ada=false;	   
		}
    }
    if(!ada){
		document.getElementById("iproduct"+d).value=a;
		document.getElementById("eproductname"+d).value=b;
		document.getElementById("vunitprice"+d).value=c;
		document.getElementById("iproductmotif"+d).value=e;
		document.getElementById("emotifname"+d).value=f;
		document.getElementById("nttb"+d).value=g;
		document.getElementById("iproductx"+d).value=a;
		document.getElementById("eproductnamex"+d).value=b;
		document.getElementById("vunitpricex"+d).value=c;
		document.getElementById("iproductmotifx"+d).value=e;
		document.getElementById("emotifnamex"+d).value=f;
		document.getElementById("nbbm"+d).value=g;
		document.getElementById("iproductgrade"+d).value=h;
		document.getElementById("iproductgradex"+d).value=h;
		document.getElementById("inota"+d).value=x;
		jsDlgHide("#konten *", "#fade", "#light");
	}
  }
  function bbatal(){
	barisxx	= document.getElementById("baris").value;
 	si_inner= document.getElementById("detailisi").innerHTML;
	var temp= new Array();
//	alert(si_inner);
	temp	= si_inner.split('<table disabled="disabled" class="listtable" style="width: 750px;"><tbody disabled="disabled">');
	if( (document.getElementById("iproduct"+barisxx).value=='')){
		si_inner='';
		for(x=1;x<barisxx;x++){
			si_inner=si_inner+'<table disabled="disabled" class="listtable" style="width: 750px;"><tbody disabled="disabled">'+temp[x];
		}
		j=0;
		var baris						= Array();
		var iproduct				= Array();
		var iproductmotif		= Array();
		var iproductgrade		= Array();
		var eproductname		= Array();
		var emotifname			= Array();
		var vunitprice			= Array();
		var nttb						= Array();
		var iproductx				= Array();
		var iproductmotifx	= Array();
		var iproductgradex	= Array();
		var eproductnamex		= Array();
		var emotifnamex			= Array();
		var vunitpricex			= Array();
		var nbbm						= Array();
		var inota						= Array();
		for(i=1;i<barisxx;i++){
			j++;
			baris[j]					= document.getElementById("baris"+i).value;
			iproduct[j]				= document.getElementById("iproduct"+i).value;
			iproductmotif[j]	= document.getElementById("iproductmotif"+i).value;
			iproductgrade[j]	= document.getElementById("iproductgrade"+i).value;
			eproductname[j]		= document.getElementById("eproductname"+i).value;
			emotifname[j]			= document.getElementById("emotifname"+i).value;
			vunitprice[j]			= document.getElementById("vunitprice"+i).value;
			nttb[j]						= document.getElementById("nttb"+i).value;
			iproductx[j]			= document.getElementById("iproductx"+i).value;
			iproductmotifx[j]	= document.getElementById("iproductmotifx"+i).value;
			iproductgradex[j]	= document.getElementById("iproductgradex"+i).value;
			eproductnamex[j]	= document.getElementById("eproductnamex"+i).value;
			emotifnamex[j]		= document.getElementById("emotifnamex"+i).value;
			vunitpricex[j]		= document.getElementById("vunitpricex"+i).value;
			nbbm[j]						= document.getElementById("nbbm"+i).value;
			inota[j]					= document.getElementById("inota"+i).value;
		}
		document.getElementById("detailisi").innerHTML=si_inner;
		j=0;
		for(i=1;i<barisxx;i++){
			j++;
			document.getElementById("iproduct"+i).value				= iproduct[j];
			document.getElementById("iproductmotif"+i).value	= iproductmotif[j];
			document.getElementById("iproductgrade"+i).value	= iproductgrade[j];
			document.getElementById("eproductname"+i).value		= eproductname[j];
			document.getElementById("emotifname"+i).value			= emotifname[j];
			document.getElementById("vunitprice"+i).value			= vunitprice[j];
			document.getElementById("nttb"+i).value						= nttb[j];
			document.getElementById("iproductx"+i).value			= iproductx[j];
			document.getElementById("iproductmotifx"+i).value	= iproductmotifx[j];
			document.getElementById("iproductgradex"+i).value	= iproductgradex[j];
			document.getElementById("eproductnamex"+i).value	= eproductnamex[j];
			document.getElementById("emotifnamex"+i).value		= emotifnamex[j];
			document.getElementById("vunitpricex"+i).value		= vunitpricex[j];
			document.getElementById("nbbm"+i).value						= nbbm[j];
			document.getElementById("inota"+i).value					= inota[j];
		}
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
		jsDlgHide("#konten *", "#fade", "#light");
	}
  }
</script>
