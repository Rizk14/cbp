<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php  echo  NmPerusahaan;?> : <?php  echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php  echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
</head>
<body id="bodylist">
<div id="main">
<?php  echo  "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php  echo form_open('op/cform/carisupplier', array('id' => 'listform'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
			<td colspan="3" align="center">Cari data : 
						<input type="text" id="cari" name="cari" value="">
						&nbsp;
						<input type="submit" id="bcari" name="bcari" value="Cari">
			</td>
	      </tr>
	    </thead>
      	    <th>Kode Supplier</th>
	    	<th>Nama Supplier</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
				echo "<tr> 
						  <td>
							<a href=\"javascript:setValue('$row->i_supplier','$row->e_supplier_name','$row->n_supplier_toplength')\">$row->i_supplier</a></td>
						  <td>
							<a href=\"javascript:setValue('$row->i_supplier','$row->e_supplier_name','$row->n_supplier_toplength')\">$row->e_supplier_name</a></td>
					   	  </tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php  echo  "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c)
  {
	opener.document.getElementById("isupplier").value=a;
	opener.document.getElementById("esuppliername").value=b;
	this.close();
  }
</script>
