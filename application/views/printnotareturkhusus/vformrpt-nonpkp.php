<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
    <title>Cetak KN Retur Non PKP</title>
</head>

<body>
    <style type="text/css" media="all">
        /*
/*
@page land {size: landscape;}
@media print {
input.noPrint { display: none; }
}
@page
        {
            size: auto;   /* auto is the initial value 
            margin: 0mm;   this affects the margin in the printer settings 
        */
        * {
            size: landscape;
        }

        @page {
            /*size: Letter; */
            /*margin: 0mm;*/
            /* this affects the margin in the printer settings */
            margin: 0.08in 0.37in 0.07in 0.26in;
        }


        .huruf {
            FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
        }

        .miring {
            font-style: italic;

        }

        .wrap {
            margin: 0 auto;
            text-align: left;
        }

        .ceKotak {
            background-color: #f0f0f0;
            border-bottom: #80c0e0 1px solid;
            border-top: #80c0e0 1px solid;
            border-left: #80c0e0 1px solid;
            border-right: #80c0e0 1px solid;
        }

        .garis {
            background-color: #000000;
            width: 100%;
            height: 50%;
            font-size: 100px;
            border-style: solid;
            border-width: 0.01px;
            border-collapse: collapse;
            spacing: 1px;
        }

        .garis td {
            background-color: #FFFFFF;
            border-style: solid;
            border-width: 0.01px;
            font-size: 10px;
            FONT-WEIGHT: normal;
            padding: 1px;
        }

        .garisy {
            background-color: #000000;
            width: 100%;
            height: 50%;
            border-style: solid;
            border-width: 0.01px;
            border-collapse: collapse;
            spacing: 1px;
        }

        .garisy td {
            background-color: #FFFFFF;
            border-style: solid;
            border-width: 0.01px;
            padding: 1px;
        }

        .garisx {
            background-color: #000000;
            width: 100%;
            height: 50%;
            border-style: none;
            border-collapse: collapse;
            spacing: 1px;
        }

        .garisx td {
            background-color: #FFFFFF;
            border-style: none;
            font-size: 10px;
            FONT-WEIGHT: normal;
            padding: 1px;
        }

        .judul {
            font-size: 18px;
            FONT-WEIGHT: normal;
        }

        .font-company {
            font-size: 12px;
            FONT-WEIGHT: normal;
        }

        .catatan {
            font-size: 14px;
            FONT-WEIGHT: normal;
        }

        .nmper {
            margin-top: 0;
            font-size: 11px;
            FONT-WEIGHT: normal;
        }

        .isi {
            font-size: 12px;
            font-weight: normal;
        }

        .eusinya {
            font-size: 8px;
            font-weight: normal;
        }

        .garisbawah {
            border-bottom: #000000 0.1px solid;
        }

        .garisatas {
            border-top: #000000 0.1px solid;
        }

        .gariskiri {
            border-left: #000000 0.1px solid;
        }

        .gariskanan {
            border-right: #000000 0.1px solid;
        }

        .kotak {
            border-collapse: collapse;
            border-bottom-width: 0px;
            border-right-width: 0px;
            border-bottom: #000000 0.1px solid;
            border-left: #000000 0.1px solid;
            border-right: #000000 0.1px solid;
            border-top: #000000 0.1px solid;
        }

        hr {
            display: block;
            border: none;
            height: 2px;
            width: 80%;
            /* Set the hr color */
            color: #333;
            /* old IE */
            background-color: #333;
            /* Modern Browsers */

            background: transparent;
            border-top: solid 1px #333;
        }

        .hrisi {
            border: none;
            height: 1px;
            width: 10%;
            /* Set the hr color */
            color: #333;
            /* old IE */
            background-color: #333;
            /* Modern Browsers */
        }

        img {
            /* height : 20%; */
            width: 30%;
            /* margin-left: 20px; */
            /* margin-right: 20px; */
        }

        .header-kanan {
            font-size: 12px;
            font-weight: normal;
        }

        .header-kanan .col1 {
            width: 1px;
            text-align: left;
            /* margin-right: 20px; */
            padding: 1px 35px;
            padding-right: 12px;
        }

        .header-kanan .col2 {
            width: 1px;
            text-align: left;
            /* padding: 8px 20px; */
        }

        h3 {
            font-size: 14px
        }

        h3:before {
            display: inline-block;
            content: "";
            border-top: 1.5px solid black;
            width: 13px;
            margin: 0 1px;
            transform: translateY(-3px);
        }

        .underline {
            font-weight: 300;
            display: inline-block;
            padding-bottom: 1px;
            position: relative;
        }

        .underline:before {
            content: "";
            position: absolute;
            width: 530%;
            height: 1px;
            bottom: 0;
            border-bottom: 1px solid black;
        }
    </style>

    <style type="text/css" media="print">
        .noDisplay {
            display: none;
        }

        .pagebreak {
            /* page-break-before: always; */
            page-break-after: always;
        }
    </style>

    <?php
    foreach ($isi as $row) {

        if ($row->e_customer_name == '') {
            $row->e_customer_name = $row->customer_name;
        }

        if ($row->e_customer_pkpnpwp == '' or $row->e_customer_pkpnpwp == null) {
            // $row->e_customer_pkpnpwp='00.000.000.0.000.000';
            // if($row->i_nik!=""){
            $row->e_customer_pkpnpwp = $row->i_nik;
            // }
        }

        $tmp        = explode("-", $row->d_kn);
        $th         = $tmp[0];
        $bl         = $tmp[1];
        $hr         = $tmp[2];
        $row->d_kn  = $hr . " " . substr(mbulan($bl), 0, 3) . " " . $th;

        if ($row->d_pajak != "") {
            $tmp1    = explode("-", $row->d_pajak);
            $th1     = $tmp1[0];
            $bl1     = $tmp1[1];
            $hr1     = $tmp1[2];
            $row->d_pajak    = $hr1 . "-" . $bl1 . "-" . $th1;
        }

        $ndpp = get_tax($row->d_nota)->excl_divider;
        $nppn = get_tax($row->d_nota)->n_tax_val;
    ?>
        <table width="100%" class="nmper" border="0">
            <!-- START TABLE -->
            <tr>
                <td colspan="4">
                    <!-- TD AWAL -->
                    <!-- START TABEL 1 -->
                    <table width="100%" class="nmper kotak" border="0">
                        <tr>
                            <td rowspan="2" width='50%'>
                                <center>
                                    <img src="<?php echo base_url() . 'img/Logo_DGU.png'; ?>" border="0">
                                </center>
                            </td>

                            <td colspan='3' style="padding-top:20px;" class="gariskiri garisatas gariskanan judul">
                                <center>
                                    <b>NOTA RETUR<b>
                                            <hr>
                                </center>
                            </td>
                        </tr>

                        <tr class="header-kanan">
                            <td class="gariskiri col1">Nomor</td>
                            <td class="col2">:</td>
                            <td width="120px">&nbsp; <?php echo $row->i_kn; ?></td>
                        </tr>

                        <tr class="header-kanan">
                            <td rowspan="2" class="font-company">
                                <center>
                                    <?php echo NmPerusahaan; ?>
                                    <center>
                            </td>

                            <td class="gariskiri col1">No. Faktur Pajak</td>
                            <td class="col2">:</td>
                            <td width="90px">&nbsp; <?php echo $row->i_pajak; ?></td>
                        </tr>

                        <tr class="header-kanan">
                            <td class="gariskiri col1">Tanggal F.Pajak</td>
                            <td class="col2">:</td>
                            <td width="90px">&nbsp; <?php if ($row->d_pajak != "") echo $row->d_pajak; ?></td>
                        </tr>
                    </table>
                    <!-- END TABEL 1 -->

                    <!-- TABEL 2 -->
                    <table width="100%" border="0" class="gariskanan gariskiri">
                        <!-- ------------------------------------START PEMBELI BKP-------------------------------- -->
                        <tr>
                            <td colspan='3'>
                                <h3>PEMBELI BKP</h3>
                            </td>
                        </tr>

                        <tr class="isi">
                            <td style="width:100px">Nama</td>
                            <td style="width:2px">:</td>
                            <td>
                                <?php echo $row->e_customer_name; ?>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <tr class="isi">
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo $row->e_customer_address . " " . $row->e_customer_city; ?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <tr class="isi">
                            <td>N.P.W.P / N.I.K</td>
                            <td>:</td>
                            <td><?php echo $row->e_customer_pkpnpwp; ?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <!-- ------------------------------------END PEMBELI BKP-------------------------------- -->

                        <!-- ------------------------------------START PEMBERI BKP-------------------------------- -->
                        <tr>
                            <td colspan='3'>
                                <h3>PEMBERI BKP</h3>
                            </td>
                        </tr>

                        <tr class="isi">
                            <td style="width:100px">Nama</td>
                            <td style="width:2px">:</td>
                            <td><?php echo NmPerusahaan; ?></td>

                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <tr class="isi">
                            <td>Alamat</td>
                            <td>:</td>
                            <td><?php echo AlmtPerusahaan; ?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <tr class="isi">
                            <td>N.P.W.P</td>
                            <td>:</td>
                            <td><?php echo NPWPPerusahaan; ?></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                <hr style="height:1px;margin-top: 0px;margin-bottom: 0px;width:95%" align="left">
                            </td>
                        </tr>

                        <tr>
                            <td colspan="3"></td>
                        </tr>

                        <tr>
                            <td colspan="3"></td>
                        </tr>

                        <tr>
                            <td colspan="3"></td>
                        </tr>

                        <tr>
                            <td colspan="3"></td>
                        </tr>

                        <!-- ------------------------------------END PEMBERI BKP-------------------------------- -->
                    </table>
                    <!-- END TABEL 2 -->

                    <!-- START TABEL 3 -->
                    <!-- HEADER ITEM -->
                    <table width="100%" border="1" class="kotak">
                        <tr>
                            <th rowspan="2" style="width: 5%">NO.</th>
                            <th colspan="2">MACAM DAN JENIS PKP</th>
                            <th rowspan="2" style="width: 13%">KUANTITAS</th>
                            <th rowspan="2" style="width: 15%">HARGA SATUAN MENURUT FAKTUR PAJAK</th>
                            <th rowspan="2">HARGA JUAL BKP</th>
                        </tr>
                        <tr>
                            <th style="width: 13%">KODE PRODUK</th>
                            <th style="width: 40%">NAMA PRODUK</th>
                        </tr>
                    </table>
                    <!-- END HEADER ITEM -->

                    <!-- DETAIL ITEM -->
                    <?php
                    $i      = 0;
                    $total  = 0;
                    $gtotal = 0;
                    $vppn   = 0;
                    foreach ($detail as $rowi) {
                        $sab    = $rowi->n_quantity * ($rowi->v_unit_price / $ndpp);
                        $sub    = number_format(($rowi->n_quantity * ($rowi->v_unit_price / $ndpp)), 2);
                        $total    = $total + $sab;
                        $i++;
                    ?>
                        <table width="100%" border="0" class="gariskiri gariskanan" style="border-collapse: collapse">
                            <tr align="center">
                                <td width="4.8%" class="huruf nmper"><?php echo  $i; ?></td>
                                <td align="left" width="12.9%" class="huruf nmper gariskiri">&nbsp;<?php echo $rowi->i_product; ?></td>
                                <td align="left" width="39.9%" class="huruf nmper gariskiri">&nbsp;
                                    <?php
                                    // if (strlen($rowi->e_product_name) > 50) {
                                    //     $nam  = substr($rowi->e_product_name, 0, 50);
                                    // } else {
                                    //     $nam  = $rowi->e_product_name . str_repeat(" ", 50 - strlen($rowi->e_product_name));
                                    // }
                                    echo $rowi->e_product_name;
                                    ?>
                                </td>
                                <td width="7%" class="huruf nmper gariskiri"><?php echo $rowi->n_quantity; ?></td>
                                <td width="6%" class="huruf nmper">pcs</td>

                                <td width="4%" class="huruf nmper gariskiri" style="text-align: left;">&nbsp;Rp</td>
                                <td width="11%" class="huruf nmper" style="text-align: right;"><?php echo number_format($rowi->v_unit_price / $ndpp, 2); ?></td>

                                <td width="4%" class="huruf nmper gariskiri" style="text-align: left;">&nbsp;Rp</td>
                                <td width="11%" class="huruf nmper" style="text-align: right;"><?php echo $sub; ?></td>
                            </tr>
                        </table>
                    <?php
                    }

                    if ($i < 25) {
                        $jmlbaris = 27 - $i;
                    ?>
                        <table width="99.9%" border="0" class="gariskiri gariskanan garisbawah" style="border-collapse: collapse">
                            <tr align="center">
                                <td width='4.8%' rowspan="<?= $jmlbaris; ?>" class="huruf nmper gariskiri">&nbsp;</td>
                                <?php
                                for ($jml = $i + 1; $jml <= 24; $jml++) {
                                    echo "<tr> 
                                            <td width='12.9%' class='huruf nmper gariskiri '></td>
                                            <td align='left' width='39.9%' class='huruf nmper gariskiri'>&nbsp;</td>
                                            <td width='7%' class='huruf nmper gariskiri'></td>
                                            <td width='6%' class='huruf nmper'></td>
                                            
                                            <td width='4%' class='huruf nmper gariskiri' style='text-align: left;'>&nbsp;</td>
                                            <td width='11%' class='huruf nmper' style = 'text-align: right;'></td>
                                            
                                            <td width='4%' class='huruf nmper gariskiri' style='text-align: left;'></td>
                                            <td width='11%' class='huruf nmper' style = 'text-align: right;'></td>
                                        </tr>";
                                }
                                ?>
                            </tr>

                            <tr align="center">
                                <td width='4.8%' class="huruf nmper gariskiri"></td>
                                <td align='left' width='39.9%' class='huruf nmper gariskiri'>&nbsp;</td>
                                <td width='7%' class='huruf nmper gariskiri'></td>
                                <td width='6%' class='huruf nmper'></td>

                                <td colspan='2' width='15%' class='huruf nmper gariskiri' style='text-align: left;'>&nbsp;Jumlah</td>

                                <td width='4%' class='huruf nmper gariskiri' style='text-align: left;'>&nbsp;&nbsp;Rp</td>
                                <td width='11%' class='huruf nmper' style='text-align: right;'><?php echo number_format($row->v_gross, 2); ?></td>

                                <?php
                                echo "<tr> 
                                <td width='12.9%' class='huruf nmper gariskiri '></td>
                                <td align='left' width='39.9%' class='huruf nmper gariskiri'>&nbsp;</td>
                                <td width='7%' class='huruf nmper gariskiri'></td>
                                <td width='6%' class='huruf nmper'></td>
                                
                                <td colspan='2' width='15%' class='huruf nmper gariskiri' style='text-align: left;'>&nbsp;Potongan</td>
                                
                                <td width='4%' class='huruf nmper gariskiri' style='text-align: left;'>&nbsp;&nbsp;Rp</td>
                                <td width='11%' class='huruf nmper' style = 'text-align: right;'>" . number_format($row->v_discount, 2) . "</td>
                            </tr>";
                                ?>
                            </tr>
                        </table>
                    <?php } ?>
                    <!-- END DETAIL ITEM -->
                    <!-- END TABEL 3 -->

                    <!-- START TABEL 4 -->
                    <?php
                    $net    = $row->v_netto / $ndpp;
                    $pjk    = $nppn * $net;
                    ?>
                    <table width="100%" border="0" class="gariskiri gariskanan garisbawah" style="border-collapse: collapse">
                        <tr>
                            <td class="huruf nmper gariskiri" style="width: 85.7%">
                                <i>Jumlah Penggantian BKP Yang Dibatalkan</i>
                            </td>
                            <td class="huruf nmper gariskiri" style="width: 4%">&nbsp; Rp</td>
                            <td class="huruf nmper" style="text-align: right"><?= number_format($net, 2); ?></td>
                        </tr>

                        <tr>
                            <td class="huruf nmper">
                                <i>PPN Yang Diminta Kembali</i>
                            </td>
                            <td class="huruf nmper gariskiri">&nbsp; Rp</td>
                            <td class="huruf nmper" style="text-align: right"><?= number_format($pjk, 2); ?></td>
                        </tr>

                        <tr>
                            <td class="huruf nmper">
                                <i>PPNBM Yang Diminta Kembali</i>
                            </td>
                            <td class="huruf nmper gariskiri">&nbsp; Rp</td>
                            <td class="huruf nmper" style="text-align: right">-</td>
                        </tr>
                    </table>
                    <!-- END TABEL 4 -->

                    <!-- START TABEL 5 -->
                    <table width="100%" border="0" class="gariskiri gariskanan garisbawah" style="border-collapse: collapse">
                        <tr>
                            <td class="huruf nmper gariskiri" style="width: 80%;text-align: right">
                                <?php echo $row->e_customer_city; ?>, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class="huruf nmper" style="width: 13.7%">
                                <?php
                                echo $row->d_kn; ?>
                            </td>
                        </tr>

                        <?php
                        for ($i = 0; $i < 5; $i++) {
                            echo "  <tr>
                                        <td colspan = '2'>&nbsp;</td>
                                    </tr>";
                        }
                        ?>

                        <tr>
                            <td colspan="2" class="huruf nmper gariskiri" style="text-align: right;">
                                (..............................................................)
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2"></td>
                        </tr>

                        <tr>
                            <td colspan="2"></td>
                        </tr>
                    </table> <br>
                    <!-- END TABEL 5 -->
                    <p class="huruf nmper">
                        catatan : <br>
                        dibuat rangkap 3 (tiga), sebagai berikut : <br>
                        lembar 1 : perusahaan <br>
                        lembar 2 : pembeli / pelanggan / toko <br>
                        lembar 3 : kantor pajak terdekat dari lokasi pembeli ( dimasukkan ke customer service dan dimintakan tanda terima surat )
                    </p>
                </td> <!-- END TD AWAL -->
            </tr>
        </table> <!-- END TABLE AWAL -->

        <br class="pagebreak">

    <?php } ?>

    <div class="noDisplay">
        <center><b><a href="#" onClick="window.print()">Print</a></b></center>
    </div>