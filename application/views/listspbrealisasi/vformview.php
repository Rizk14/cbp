<title>Informasi Realisasi Pemenuhan SPB</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/jquery.js"></script>
<div id='tmp'>
	<h2><?php echo  $page_title; ?></h2>
	<?php
	include("php/fungsi.php");
	?>
	<h3>&nbsp;&nbsp;&nbsp;<?php echo  'Periode : ' . substr($dfrom, 0, 2) . ' ' . mbulan(substr($dfrom, 3, 2)) . ' ' . substr($dfrom, 6, 4) . ' s/d ' . substr($dto, 0, 2) . ' ' . mbulan(substr($dto, 3, 2)) . ' ' . substr($dto, 6, 4); ?></h3>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo  $this->pquery->form_remote_tag(array('url' => 'listspbrealisasi/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<table class="listtable" id="sitabel">
							<?php
							if ($isi) {
								$vpesan 		= 0;
								$vkirim 		= 0;
								$vpending 		= 0;

								$vpesanrp 		= 0;
								$vkirimrp 		= 0;
								$vpendingrp		= 0;
							?>
								<tr>
									<th>Area</th>
									<th>SPB</th>
									<th>Tgl SPB</th>
									<th>Stock Daerah</th>
									<th>Kode Pelanggan</th>
									<th>Nama Pelanggan</th>
									<th>Sls</th>
									<th>Divisi</th>
									<th>Kode Prod</th>
									<th>Nama Prod</th>
									<th>Pesan</th>
									<!-- <th>Netto</th> -->
									<th>Kirim</th>
									<!-- <th>Netto</th> -->
									<!-- <th>Pending</th> -->
									<!-- <th>Netto</th> -->
									<th>Harga</th>
									<th>Status</th>
									<th>No SJ</th>
									<th>Tgl SJ</th>
									<th>Nota</th>
									<th>Tgl Nota</th>
									<th>SPB Approve 1</th>
									<th>Tgl Approve 1</th>
									<th>Approve</th>
									<th>SPB Approve 2</th>
									<th>Tgl Approve 2</th>
									<th>Approve</th>
									<th>SPB-SJ</th>
									<th>SJ-DKB</th>
									<th>DKB-Nota</th>
									<th>SPB-DKB</th>
									<th>Keterangan</th>
								</tr>
								<tr>
									<tbody>
										<?php
										foreach ($isi as $row) {
											if ($row->f_spb_stockdaerah == 't') {
												$stok = "Ya";
											} else {
												$stok = "Tidak";
											}

											$dspb 		= strtotime($row->d_spb);
											$dsales  	= strtotime($row->d_approvesales);
											$dkeuangan  = strtotime($row->d_approve);
											$sj  		= strtotime($row->d_sj);
											$dkb 		= strtotime($row->d_dkb);
											$nota  		= strtotime($row->d_nota);

											$pesan	 	= $row->n_order;
											$kirim 		= $row->n_deliver;
											$pending 	= $kirim - $pesan;

											//HITUNG JML HARI SPB APPROVE 1		
											if ($dsales != '' && $dsales > $dkeuangan) {
												$spb1 	= $dkeuangan - $dspb;
												$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
											} else if ($dsales != '' && $dsales < $dkeuangan) {
												$spb1 	= $dsales - $dspb;
												$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
											} else if ($dsales != '' && $dsales == $dkeuangan) {
												$spb1 	= $dsales - $dspb;
												$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
											} else if ($dsales == '' && $dkeuangan != '') {
												$spb1 	= $dkeuangan - $dspb;
												$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
											} else {
												$dspb1 	= '-';
											}
											//END JML HARI SPB APPROVE 1

											//SPB APPROVE
											if ($dsales > $dkeuangan /*|| $dsales == $dkeuangan*/) {
												$app1 	= "Keuangan";
												$app2 	= "Sales";
											} else if ($dsales == '' || $dkeuangan == '') {
												$app1 	= '-';
												$app2 	= '-';
											} else if ($dsales == $dkeuangan) {
												$app1 	= "Keuangan";
												$app2 	= "Sales";
											} else {
												$app1 	= "Sales";
												$app2 	= "Keuangan";
											}
											//END HITUNG JML HARI SPB APPROVE 1
											if ($dsales != '') {
												$spbx2 	= $dkeuangan - $dsales;
											}
											//HITUNG JML HARI SPB APPROVE 2
											if ($dkeuangan != '' && $dkeuangan > $dsales) {
												$spb2 	= $dkeuangan - $dspb;
												$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
											} else if ($dkeuangan != '' && $dkeuangan < $dsales) {
												$spb2 	= $dsales - $dspb;
												$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
											} else if ($dkeuangan != '' && $dkeuangan == $dsales) {
												$spb2 	= $dkeuangan - $dspb;
												$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
											} else if ($dkeuangan == '' && $dsales != '') {
												$spb2 	= $dsales - $dspb;
												$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
											} else {
												$dspb2 	= '-';
											}
											//END JML HARI SPB APPROVE 1
											/*if($dsales!=''){
			
			#$spb2 	= $dsales-$dkeuangan;
			#$dspb2	= floor($spb2 / (60 * 60 * 24))." hari";
		if($spbx2<0){
			$spb2 	= $dsales-$dkeuangan;
			$dspb2	= floor($spb2 / (60 * 60 * 24))." hari";
		}else if($spbx2==0){
			$dspb2	= $dspb1;
		}
		else{
			$spbx2 	= $dkeuangan-$dsales;
			$dspb2	= floor($spbx2 / (60 * 60 * 24))." hari";
			}
		}
		else{
			$dspb2 	= '-';
		}*/
											//SPB APPROVE 2
											#		if($dku > $dsls){
											#			$app2 	= "Sales";
											#		}else{
											#			$app2 	= "Keuangan";
											#		}
											//END HITUNG JML HARI SALES - APPROVE 2

											/*		if($spbx2<0){
			$tglapp	= $row->d_approvesales;
		}else{
			$tglapp	= $row->d_approve;
		}*/
											if ($app1 == 'Keuangan') {
												$tglapp1	= $row->d_approve;
												$tglapp2	= $row->d_approvesales;
											} else {
												$tglapp1	= $row->d_approvesales;
												$tglapp2	= $row->d_approve;
											}
											//HITUNG JML HARI SPB - SJ
											if ($sj != '') {
												#$spb2 	= $dkeuangan-$dsales;
												#$keusj 	= $sj-$dkeuangan;
												#$dspbsj	= floor($keusj / (60 * 60 * 24))." hari";
												if ($spbx2 < 0) {
													$salesj	= $sj - $dsales;
													$dspbsj	= floor($salesj / (60 * 60 * 24)) . " hari";
												} else {
													$salesj	= $sj - $dkeuangan;
													$dspbsj	= floor($salesj / (60 * 60 * 24)) . " hari";
												}
											} else {
												$dspbsj	= '-';
											}
											//END HITUNG JML HARI SPB - SJ

											//HITUNG JML HARI SJ - DKB
											if ($dkb != '') {
												#$spb2 	= $dkeuangan-$dsales;
												$sjdkb 	= $dkb - $sj;
												$dsjdkb	= floor($sjdkb / (60 * 60 * 24)) . " hari";
											} else {
												$dsjdkb	= '-';
											}
											//END HITUNG JML HARI SJ - DKB

											//HITUNG JML HARI SPB - DKB
											if ($dkb != '') {
												$spdkb	= $dkb - $dspb;
												$spdkb	= floor($spdkb / (60 * 60 * 24)) . " hari";
												#$spdkb	= $sjdkb + $spb2 . " hari";
												#$dsjdkb	= floor($sjdkb / (60 * 60 * 24))." hari";
											} else {
												$spdkb	= '-';
											}
											//END HITUNG JML HARI SPB - DKB

											//HITUNG JML HARI DKB - Nota
											if ($nota != '') {
												#$spb2 	= $dkeuangan-$dsales;
												$dkbnota 	= $nota - $dkb;
												$ddkbnota	= floor($dkbnota / (60 * 60 * 24)) . " hari";
											} else {
												$ddkbnota	= '-';
											}
											//END HITUNG JML HARI DKB - Nota


											if ($row->n_deliver == '') $row->n_deliver = 0;
											if (
												($row->f_spb_cancel == 't')
											) {
												$status = 'Batal';
											} elseif (
												($row->i_approve1 == null) && ($row->i_notapprove == null)
											) {
												$status = 'Sales';
											} elseif (
												($row->i_approve1 == null) && ($row->i_notapprove != null)
											) {
												$status = 'Reject (sls)';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 == null) &
												($row->i_notapprove == null)
											) {
												$status = 'Keuangan';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 == null) &&
												($row->i_notapprove != null)
											) {
												$status = 'Reject (ar)';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store == null)
											) {
												$status = 'Gudang';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
											) {
												$status = 'Pemenuhan SPB';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
											) {
												$status = 'Proses OP';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
											) {
												$status = 'OP Close';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
											) {
												$status = 'Siap SJ (sales)';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
											) {
												#			  	$status='Siap SJ (gudang)';
												$status = 'Siap SJ';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
											) {
												$status = 'Siap SJ';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
											) {
												$status = 'Siap DKB';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
												($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
											) {
												$status = 'Siap Nota';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) &&
												($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
											) {
												$status = 'Siap SJ';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) &&
												($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
											) {
												$status = 'Siap DKB';
											} elseif (
												($row->i_approve1 != null) && ($row->i_approve2 != null) &&
												($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) &&
												($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
											) {
												$status = 'Siap Nota';
											} elseif (
												($row->i_approve1 != null) &&
												($row->i_approve2 != null) &&
												($row->i_store != null) &&
												($row->i_nota != null)
											) {
												$status = 'Sudah dinotakan';
											} elseif (($row->i_nota != null)) {
												$status = 'Sudah dinotakan';
											} else {
												$status = 'Unknown';
											}

											echo "<tr>
												<td>" . $row->i_area . "-" . $row->e_area_name . "</td>
												<td>$row->i_spb</td>
												<td>$row->d_spb</td>
												<td>$stok</td>
												<td>$row->i_customer</td>
												<td>$row->e_customer_name</td>
												<td>" . $row->i_salesman . " - " . $row->e_salesman_name . "</td>
												<td>$row->e_product_groupname</td>
												<td>$row->i_product</td>
												<td>$row->e_product_name</td>
												<td>$row->n_order</td>
												<td>$row->n_deliver</td>
												<td>" . number_format($row->v_unit_price) . "</td>
												<td>$status</td>
												<td>$row->i_sj</td>
												<td>$row->d_sj</td>
												<td>$row->i_nota</td>
												<td>$row->d_nota</td>
												<td>$dspb1</td>
												<td>$tglapp1</td>
												<td>$app1</td>
												<td>$dspb2</td>
												<td>$tglapp2</td>
												<td>$app2</td>
												<td>$dspbsj</td>
												<td>$dsjdkb</td>
												<td>$ddkbnota</td>
												<td>$spdkb</td>
												<td>$row->e_remark</td>
											</tr>";

											/* TOTAL QTY */
											$vpesan 	+= $pesan;
											$vkirim 	+= $kirim;
											$vpending 	+= $pending;
											/* $vpesanrp 	= $vpesanrp + $row->vpesan;
										$vkirimrp 	= $vkirimrp + $row->vkirim; */
										} /*<td>".IntervalDays($date1,$date2)."</td>*/ /* END FOREACH */

										?>
										<tr>
											<th colspan="10" align="center"><b>GRAND TOTAL</b></th>
											<th><b><?php echo number_format($vpesan); ?></b></th>
											<th><b><?php echo number_format($vkirim); ?></b></th>
											<!-- <th><b><?php echo number_format($vpending); ?></b></th> -->
											<th colspan="17" style="background-color: #30839E"></th>
										</tr>
									</tbody>
									<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
								<?php } else {
								echo "<h2>Belum ada Data!!!</h2>";
							} ?>
						</table>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
</div>
<script language="javascript" type="text/javascript">
	$("#cmdreset").click(function() {
		var Contents = $('#sitabel').html();
		window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
	});
</script>