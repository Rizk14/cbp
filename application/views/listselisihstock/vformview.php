<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h3><?php 
echo 'Periode : '.$iperiode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listselisihstock/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>Gudang</th>
		<th>Lokasi</th>
		<th>Divisi</th>
    <th>Kode</th>
    <th>Nama Barang</th>
    <th>Selisih Qty</th>
    <th>Selisih Rp</th>
    <?php 
	    echo '<tbody>';
      foreach($isi as $raw)
      {
        if($raw->i_store=='PB' || $raw->i_store_location=='PB') 
          $lokasi='Konsinyasi';
        else
          $lokasi='Reguler';
        echo "<tr><td>$raw->i_store</td><td>$lokasi</td>";
        echo "<td>$raw->e_product_groupname</td>";
        echo "<td>$raw->i_product</td>";
        echo "<td>$raw->e_product_name</td>";
        echo "<td align=right>".number_format($raw->selisih)."</td>";
        echo "<td align=right>".number_format($raw->selisihrp)."</td></tr>";
      }
    }
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
