<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Print DKB-SJP</title>
</head>

<body>
  <style type="text/css" media="all">
    /*
@page land {size: landscape;}
*/
    /* * {
      size: landscape;
    } */

    /* @page {
      size: Letter;
    } */

    .huruf {
      FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
    }

    .miring {
      font-style: italic;

    }

    .ceKotak {
      - background-color: #f0f0f0;
      border-bottom: #80c0e0 1px solid;
      border-top: #80c0e0 1px solid;
      border-left: #80c0e0 1px solid;
      border-right: #80c0e0 1px solid;
    }

    .garisy {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisy td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      padding: 1px;
    }

    .garisx {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: none;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisx td {
      background-color: #FFFFFF;
      border-style: 0;
      /* font-size: 10px; */
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .judul {
      font-size: 20px;
      FONT-WEIGHT: normal;
    }

    .nmper {
      font-size: 18px;
      FONT-WEIGHT: normal;
    }

    .isi {
      font-size: 14px;
      font-weight: normal;
      padding: 1px;
    }

    .eusinya {
      font-size: 10px;
      font-weight: normal;
    }

    td.empty {
      /* border-bottom: #fff 1px solid; */
      border-top: #fff 1px solid;
      border-left: #fff 1px solid;
      border-right: #fff 1px solid;
    }
  </style>
  <style type="text/css" media="print">
    .noDisplay {
      display: none;
    }

    .pagebreak {
      page-break-before: always;
    }
  </style>

  <?php
  foreach ($isi as $row) {
  ?>
    <table width="931px" border="0" class="garisx">
      <tr>
        <td colspan="4" class="nmper"><?php echo NmPerusahaan; ?></td>
      </tr>
      <tr>
        <td colspan="4" class="judul">DAFTAR KIRIMAN BARANG </td>
      </tr>
      <tr>
        <td colspan="4">No. <?php echo "$row->i_dkb/$row->i_area-$row->e_area_name"; ?></td>
      </tr>
      <tr>
        <td>Kirim ke : <?php echo $row->e_dkb_kirim; ?></td>
        <td align="right">Alamat : </td>
        <td><?php echo $row->e_area_address; ?></td>
      </tr>
      <?php if ($row->i_dkb_via == '1') {
        echo "<tr><td width=\"339\">Via Angkutan : $row->e_dkb_via</td>";
        echo "<td width=\"333\" align=\"right\">Nama Ekspedisi : $row->i_ekspedisi-$row->e_ekspedisi</td></tr>";
      } else {
        echo "<tr><td width=\"339\">Via Angkutan : $row->e_dkb_via " . " / " . $row->i_kendaraan . "</td>";
        echo "<td width=\"333\" align=\"right\">Nama Supir/Helper : </td>";
        echo "<td width=\"333\">$row->e_sopir_name</td></tr>";
      } ?>
    </table>

    <?php
    $idkb  = $row->i_dkb;
    $iarea  = $row->i_area;
    $query   = $this->db->query(" select * from tm_dkb_sjp_item where i_dkb='$idkb' and i_area='$iarea'", false);
    $jml   = $query->num_rows();
    $i = 0;
    $kes = 0;
    $kre = 0;
    $vkes = 0;
    $vkre = 0;
    ?>

    <table width="931px" border="0" class="garisy ceKotak">
      <tr>
        <td width="31" rowspan="2">
          <div align="center">No</div>
        </td>
        <!-- <td colspan="3">
          <div align="center">CABANG</div>
        </td> -->
        <td colspan="2">
          <div align="center">SURAT JALAN PINJAMAN</div>
        </td>
        <td rowspan="2">
          <div align="center">TANGGAL SPmB</div>
        </td>
        <td rowspan="2">
          <div align="center">NILAI SJP</div>
        </td>
        <td colspan="3">
          <div align="center">KEMASAN </div>
        </td>
      </tr>

      <tr>
        <!-- <td width="63">
          <div align="center">KODE</div>
        </td>
        <td width="158">
          <div align="center">AREA</div>
        </td>
        <td width="250px">
          <div align="center">ALAMAT</div>
        </td> -->
        <td width="101">
          <div align="center">NOMOR</div>
        </td>
        <td width="89">
          <div align="center">TANGGAL</div>
        </td>
        <!-- <td width="101">
          <div align="center">TANGGAL SPmB</div>
        </td>
        <td width="110">
          <div align="center">NILAI</div>
        </td> -->
        <td width="50">
          <div align="center">KOLI</div>
        </td>
        <td width="50">
          <div align="center">IKAT</div>
        </td>
        <td width="50">
          <div align="center">KARDUS</div>
        </td>
      </tr>

      <?php
      if ($detail) {
        $cust = "";
        $name = "";
        foreach ($detail as $rowi) {

          // foreach ($detail as $cell) {
          //   if (isset($total[$cell['i_area']]['jml'])) {
          //     $total[$cell['i_area']]['jml']++;
          //   } else {
          //     $total[$cell['i_area']]['jml'] = 1;
          //   }
          // }

          $i++;

          $kre++;
          $vkre = $vkre + $rowi->v_jumlah;

          if (strlen($i) == 2) $aw = 0;
          if (strlen($i) == 1) $aw = 1;
          $aw = str_repeat(" ", $aw);
          $cust  = $rowi->i_area;
          $name  = $rowi->e_area_name;
          if (strlen($name) > 27) {
            $name = substr($name, 0, 27);
          }

          $name   = $name . str_repeat(" ", 27 - strlen($name));

          // $isjp   = substr($rowi->i_sjp, 8, 6);

          $tmp    = explode("-", $rowi->d_sjp);
          $th     = $tmp[0];
          $bl     = $tmp[1];
          $hr     = $tmp[2];
          $dsjp    = $hr . "-" . $bl . "-" . $th;

          $vsj  = number_format($rowi->v_jumlah);
          $vsj  = str_repeat(" ", 14 - strlen($vsj)) . $vsj;

          $ket  = $rowi->e_remark;
          if (strlen($ket) > 0) {
            $ket  = $ket . str_repeat(" ", 24 - strlen($ket));
          } else {
            $ket  = $ket . str_repeat(" ", 24 - 1);
          }

          // $n = count($row);
      ?>
          <!-- <table width="1201"  border="0" class="garisy ceKotak huruf ici"> -->
          <tr>
            <td style="text-align:center" width="35"><?= $i ?></td>
            <?php
            // if ($cust != $rowi->i_area) {
            //   echo '<td' . ($total[$cell['i_area']]['jml'] > 1 ? ' rowspan="' . ($total[$cell['i_area']]['jml']) . '">' : '>') . $cell['instansi'] . '</td>';
            //   $cust = $cell['i_area'];
            // }
            ?>
            <!-- <td style="text-align:center" width="68"><#?= $cust ?></td>
            <td style="text-align:center" width="170"><#?= $name ?></td>
            <td width="550px"><#?= $rowi->e_area_address ?></td> -->
            <td style="text-align:center" width="280px"><?= $rowi->i_sjp ?></td>
            <td style="text-align:center" width="96"><?= $dsjp ?></td>
            <td style="text-align:center" width="280px"><?= date('d-m-Y', strtotime($rowi->d_spmb)) ?></td>
            <td style="text-align:right" width="180px" align="right"><?= $vsj ?></td>
            <td style="text-align:center" width="50px"><?= $rowi->n_koli == 0 ? "" : $rowi->n_koli ?></td>
            <td style="text-align:center" width="50px"><?= $rowi->n_ikat == 0 ? "" : $rowi->n_ikat ?></td>
            <td style="text-align:center" width="50px"><?= $rowi->n_kardus == 0 ? "" : $rowi->n_kardus ?></td>
          </tr>

      <?php
        }
      }
      $vsj  = number_format($row->v_dkb);
      $vsj  = str_repeat(" ", 14 - strlen($vsj)) . $vsj;
      $tmp = explode("-", $row->d_dkb);
      $th = $tmp[0];
      $bl = $tmp[1];
      $hr = $tmp[2];
      $ddkb = $hr . " " . mbulan($bl) . " " . $th;
      $vkes = number_format($vkes);
      $vkre = number_format($vkre);
      $tgl = date("d") . " " . mbulan(date("m")) . " " . date("Y") . "  Jam : " . date("H:i:s");
      ?>
      <tr>
        <td colspan="4" width="570" align="right">Jumlah Rp. </td>
        <td align="right" width="172"><?php echo $vsj; ?></td>
        <td colspan="3" width="172"></td>
      </tr>
    </table>
    <br>
    <!-- <table width="931px" border="0">

    </table> -->
    <table width="987" border="0" class="garisy td ceKotak">
      <tr>
        <td class="empty" colspan="3">&nbsp;</td>
        <td class="empty" colspan="3" style="width:100%">
          <div align="right">Bandung, <?php echo $ddkb; ?></div>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <div align="center">Berangkat</div>
        </td>
        <td colspan="2">
          <div align="center">Kembali</div>
        </td>
        <td>
          <div align="center">Diterima Oleh, </div>
        </td>
      </tr>
      <tr height="150">
        <td style='width:16%' align="center"><br><br><br><br>(Staf Gudang)</td>
        <td style='width:19%' align="center"><br><br><br><br>(Driver/Pihak Perantara Driver)*</td>
        <td style='width:16%' align="center"><br><br><br><br>(Staf Gudang)</td>
        <td style='width:19%' align="center"><br><br><br><br>(Driver/Pihak Perantara Driver)*</td>
        <td width="100" align="center"><br><br><br><br>(Staf Gudang/AKD/Ekspedisi Eksternal)*</td>
        <!-- <td width="156" align="center"><br><br>(Adm Piutang)</td> -->
      </tr>
    </table>

    <table width="720" border="0">
      <tr>
        <td>TANGGAL CETAK </td>
        <td>:</td>
        <td><?php echo $tgl; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>
            <font color="red">Keterangan : *Coret yang tidak perlu</font>
          </b></td>
      </tr>
    </table>
  <?php
  }
  ?>
  <div class="noDisplay">
    <!-- <center><b><a href="#" onClick="window.print()">Print</a></b></center> -->
    <center><button onClick="window.print()">Print</button></center>
  </div>
</BODY>

</html>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.3.2.js"></script>
<script type="text/javascript">
  window.onafterprint = function() {
    var idkb = '<?php echo $idkb ?>';
    $.ajax({
      type: "POST",
      url: "<?php echo site_url($folder . '/cform/update'); ?>",
      data: "idkb=" + idkb,
      success: function(data) {
        opener.window.refreshview();
        setTimeout(window.close, 0);
      },
      error: function(XMLHttpRequest) {
        alert('fail');
      }
    });
  }
</script>