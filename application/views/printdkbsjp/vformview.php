<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="8" align="center">Cari data :
										<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">
										<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
										<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
										<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
									</td>
								</tr>
							</thead>
							<th>No DKB</th>
							<th>Tgl DKB</th>
							<th>Area</th>
							<th>Cetak</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$cetak = $row->n_print > 0 ? "Ya" : "Tidak";
										echo "<tr> 
												<td>$row->i_dkb</td>
												<td>" . date('d-m-Y', strtotime($row->d_dkb)) . "</td>
												<td>$row->e_area_name</td>
												<td>$cetak</td>
												<td class=\"action\">";
										if ($row->n_print == 0) {
											echo "	<a href=\"javascript:yyy('" . $row->i_dkb . "','" . $row->i_area . "');\">
														<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/print.gif\" border=\"0\" alt=\"edit\">
													</a>";
										}

										if (
											$row->n_print > 0 && (($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '4') ||
												($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '8') ||
												$this->session->userdata('level') == '0' || ($this->session->userdata('level') == '3'))
										) {
											echo	"	<a href=\"#\" onclick='reprint(\"$folder/cform/undo/$row->i_dkb/$row->i_area/$dfrom/$dto/$iarea/\",\"#main\")' title=\"Cancel Print (Cetak Ulang)\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/no-printer.png\" border=\"0\" alt=\"Cancel Print\">
														</a>";
										}
										echo "</td>";
									}
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
					</div>
				</div>
			</div>
			<?= form_close() ?>
			<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/','#main')">
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function yyy(b, c) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/<?= $folder ?>/cform/cetak/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function refreshview() {
		show('<?= $folder ?>/cform/view/<?= $dfrom ?>/<?= $dto ?>/<?= $iarea ?>', '#main');
	}
</script>