<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>

	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpencapaiancollectionpersalesman/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
 	  <th rowspan=2>Area</th>
	  <th rowspan=2>Salesman</th>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($blasal,'integer');
	  }
    echo '<th align=center colspan=3>Kredit</th>';
    echo '<th align=center colspan=3>Tunai</th>';
    echo '<th align=center colspan=3>Gabungan</th>';
    echo '</tr><tr>';
    echo '<th>Target</th><th>Realisasi</th><th>Persen</th>';
    echo '<th>Target</th><th>Realisasi</th><th>Persen</th>';
    echo '<th>Target</th><th>Realisasi</th><th>Persen</th>';
?>
    </tr>
    <tbody>
<?php 
    $bl=$blasal;
    $grandtargetcredit=0;
    $grandrealcredit=0;
    $grandtargetcash=0;
    $grandrealcash=0;
    $grandtargetgabung=0;
    $grandrealgabung=0;

		foreach($isi as $row){
      $grandtargetcredit=$grandtargetcredit+$row->totalcredit;
      $grandrealcredit=$grandrealcredit+$row->realisasicredit;
      $grandtargetcash=$grandtargetcash+$row->totalcash;
      $grandrealcash=$grandrealcash+$row->realisasicash;
      $grandtargetgabung=$grandtargetgabung+$row->totalcredit+$row->totalcash;
      $grandrealgabung=$grandrealgabung+$row->realisasicredit+$row->totalcash;

      if($row->realisasicredit>0){
        $persen=($row->realisasicredit*100)/$row->totalcredit;
      }else{
        $persen=0;
      }
      echo '<tr><td>'.$row->i_area.' - '.$row->e_area_name.'</td><td>'.$row->i_salesman.' - '.$row->e_salesman_name.'</td>';
      echo '<td align=right>'.number_format($row->totalcredit).'</td>';
      echo '<td align=right>'.number_format($row->realisasicredit).'</td>';
      echo '<td align=right>'.number_format($persen,2).' %</td>';
      if($row->realisasicash>0){
        $persen=($row->realisasicash*100)/$row->totalcash;
      }else{
        $persen=0;
      }
      echo '<td align=right>'.number_format($row->totalcash).'</td>';
      echo '<td align=right>'.number_format($row->realisasicash).'</td>';
      echo '<td align=right>'.number_format($persen,2).' %</td>';
      if(($row->realisasicash+$row->realisasicredit)>0){
        $persen=(($row->realisasicash+$row->realisasicredit)*100)/($row->totalcredit+$row->totalcash);
      }else{
        $persen=0;
      }
      echo '<td align=right>'.number_format($row->totalcredit+$row->totalcash).'</td>';
      echo '<td align=right>'.number_format($row->realisasicredit+$row->totalcash).'</td>';
      echo '<td align=right>'.number_format($persen,2).' %</td></tr>';
    }
    
    echo '<tr><td colspan=2 align=center><b>Total</b></td>';
    echo '<td align=right><b>'.number_format($grandtargetcredit).'</b></td>';
    echo '<td align=right><b>'.number_format($grandrealcredit).'</b></td>';
    if($grandrealcredit>0){
      $persen=($grandrealcredit*100)/$grandtargetcredit;
    }else{
      $persen=0;
    }
    echo '<td align=right><b>'.number_format($persen,2).' %</b></td>';
    echo '<td align=right><b>'.number_format($grandtargetcash).'</b></td>';
    echo '<td align=right><b>'.number_format($grandrealcash).'</b></td>';
    if($grandrealcash>0){
      $persen=($grandrealcash*100)/$grandtargetcash;
    }else{
      $persen=0;
    }
    echo '<td align=right><b>'.number_format($persen,2).' %</b></td>';
    echo '<td align=right><b>'.number_format($grandtargetgabung).'</b></td>';
    echo '<td align=right><b>'.number_format($grandrealgabung).'</b></td>';
    if($grandrealgabung>0){
      $persen=($grandrealgabung*100)/$grandtargetgabung;
    }else{
      $persen=0;
    }
    echo '<td align=right><b>'.number_format($persen,2).' %</b></td></tr>';
  }
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
