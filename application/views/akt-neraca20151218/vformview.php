<?php 
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>Per $tanggal $namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" width="750px">
	   	    <th width="250px">Aktiva</th>
			<th width="125px">&nbsp;</th>
			<th width="250px">Passiva</th>
			<th width="125px">&nbsp;</th>
		<?php 
		echo "<tr valign=top><td colspan=2><table width=\"100%\">";
		$aktiva	= 0;
		$passiva= 0;
		if($kas){
			$this->load->model('akt-bb/mmaster');
			foreach($kas as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutang){
			
			foreach($piutang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "</td><td colspan=2><table width=\"100%\">";
		if($hutangusaha){
			$totaldebet	 = 0;
			$totalkredit = 0;
			foreach($hutangusaha as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
			}
		}
		echo "</tr></table></td></tr>";
		echo "<tr><td colspan=2>&nbsp;</td><td colspan=2><table width=\"100%\">";
		if($modal){
			$totaldebet	 = 0;
			$totalkredit = 0;
			foreach($modal as $tmp)
			{
				echo "<tr valign=top>
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
			}
		}
		echo "</tr></table></td></tr>";
		?>
		  <tr>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px" align=right style="font-weight: bold;"><?php echo number_format($aktiva); ?></td>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px" align=right style="font-weight: bold;"><?php echo number_format($passiva); ?></td>
		  </tr>
		</table>
      </tbody>
      </div>
	</div>
    </td>
  </tr>
</table>
