<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
<body id="bodylist">
<div id="main">
<div id="tmpx">

<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'kartupiutangpertgl/cform/detail','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
			  <?php 
  $query=$this->db->query(" select e_customer_name_groupbayar from v_ar_kartu_piutang where i_customer_groupbayar='$icustomer'");
  if ($query->num_rows() > 0){
    foreach($query->result() as $tmp){
      $nama=$tmp->e_customer_name_groupbayar;
    }
  }
  echo "<center><h3>$page_title ($icustomer-$nama) Periode $dfrom s/d $dto</h3></center>"; 
?>
      <th>Tanggal</th>
	    <th>No Bukti</th>
	    <th>Keterangan</th>
	    <th>Penjualan</th>
	    <th>DN</th>
	    <th>Pelunasan</th>
	    <th>KN</th>
	    <th>Saldo Akhir</th>
	    <tbody>
	      <?php 
		if($isi){
      $saldoakhir=$saldo;
      echo "<tr> 
				    <td></td>
				    <td></td>
				    <td>Saldo</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>0</td>
				    <td align=right>".number_format($saldoakhir)."</td>
	  			  </tr>";
			foreach($isi as $row){
        $tmp 	= explode("-", $row->doc_date);
        $det	= $tmp[2];
        $mon	= $tmp[1];
        $yir 	= $tmp[0];
        $row->doc_date	= $det."-".$mon."-".$yir;
        if($row->jenis!='2. LEBIH BAYAR' && $row->jenis!='2. NOTA KREDIT'){
          $saldoakhir=$saldoakhir+$row->penjualan+$row->notadebit-($row->pelunasan+$row->notakredit);
        }
			  echo "<tr> 
				  <td>$row->doc_date</td>
				  <td>$row->doc_no</td>
				  <td>$row->jenis</td>
				  <td align=right>".number_format($row->penjualan)."</td>
				  <td align=right>".number_format($row->notadebit)."</td>
				  <td align=right>".number_format($row->pelunasan)."</td>
				  <td align=right>".number_format($row->notakredit)."</td>
				  <td align=right>".number_format($saldoakhir)."</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="windowClose()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
function windowClose() {
window.open('','_parent','');
window.close();
}
 $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
