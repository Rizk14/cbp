<table class="maintable">
  <tr>
    <td align="left">
  <?php  echo $this->pquery->form_remote_tag(array('url'=>'ctrbycategory/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $dfrom." s/d ".$dto; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
         <table class="listtable">
         <tr>
          <td>
            <?php 
              if($ob){
                foreach ($ob as $riw) {
                  echo "<h1>Total OB : ".number_format($riw->ob)."</h1>";
                }
              }
            ?>
          </td>
         </tr>
       </table> 
        <table class="listtable">
          <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Kode Series</th>
            <th rowspan="2">Nama Series</th>
            <th colspan="3">OA</th>
            <th colspan="3">Sales Qty (Unit)</th>
            <th colspan="3">Net Sales (Rp.)</th>
            <th rowspan="2">% Ctr<br>Net Sales (Rp.)</th>
          <tr>
            <th><?php echo $prevthn ?></th>
            <th><?php echo $thn ?></th>
            <th>Growth</th>
            <th><?php echo $prevthn ?></th>
            <th><?php echo $thn ?></th>
            <th>Growth</th>
            <th><?php echo $prevthn ?></th>
            <th><?php echo $thn ?></th>
            <th>Growth</th>
          </tr>
        </tr>
        <?php 
          if($isi) {
              $totnota=0;
              $totrp=0;
              $totoaprev=0;
              $totoa=0;
              $totqty=0;
              $totqtyprev=0;
              $totvnota=0;
              $totvnotaprev=0;
              $totctrrp=0;
              $i=0;
                foreach($isi as $rew){
                    $totnota+=$rew->netsls;
                }
                $totrp = $totnota; 
                
                foreach($isi as $row){
                  $i++;

                    if ($row->oaprev == 0) {
                        $grwoa = 0;
                    } else { //jika pembagi tidak 0
                        $grwoa = (($row->oa-$row->oaprev)/$row->oaprev)*100;
                    }

                    if ($row->slsqtyprev == 0) {
                        $grwqty = 0;
                    } else { //jika pembagi tidak 0
                        $grwqty = (($row->slsqty-$row->slsqtyprev)/$row->slsqtyprev)*100;
                    }

                    if ($row->netslsprev == 0) {
                        $grwrp = 0;
                    } else { //jika pembagi tidak 0
                        $grwrp = (($row->netsls-$row->netslsprev)/$row->netslsprev)*100;
                    }

                    $ctrrp = ($row->netsls/$totrp)*100;

                    echo "<tr>
                            <td>$i</td>
                            <td>$row->iseri</td>
                            <td>$row->seriname</td>
                            <td align='right'>".number_format($row->oaprev)."</td>
                            <td align='right'>".number_format($row->oa)."</td>
                            <td align='right'>".number_format($grwoa,2) ."%</td>
                            <td align='right'>".number_format($row->slsqtyprev)."</td>
                            <td align='right'>".number_format($row->slsqty)."</td>
                            <td align='right'>".number_format($grwqty,2)."%</td>
                            <td align='right'>".number_format($row->netslsprev)."</td>
                            <td align='right'>".number_format($row->netsls)."</td>
                            <td align='right'>".number_format($grwrp,2)."%</td>
                            <td align='right'>".number_format($ctrrp,2)."%</td>
                          </tr>";
                    
                    $totoaprev = $totoaprev+$row->oaprev;
                    $totoa = $totoa+$row->oa;
                    $totqty = $totqty+$row->slsqty;
                    $totqtyprev = $totqtyprev+$row->slsqtyprev;
                    $totvnota = $totvnota+$row->netsls;
                    $totvnotaprev = $totvnotaprev+$row->netslsprev;
                    $totctrrp=$totctrrp+$ctrrp;

                    if ($totoaprev == 0) {
                        $totgrwoa = 0;
                    } else { //jika pembagi tidak 0
                        $totgrwoa = (($totoa-$totoaprev)/$totoaprev)*100;
                    }

                    if ($totqtyprev == 0) {
                        $totgrwqty = 0;
                    } else { //jika pembagi tidak 0
                        $totgrwqty = (($totqty-$totqtyprev)/$totqtyprev)*100;
                    }

                    if ($totvnotaprev == 0) {
                        $totgrwrp = 0;
                    } else { //jika pembagi tidak 0
                        $totgrwrp = (($totvnota-$totvnotaprev)/$totvnotaprev)*100;
                    }
                }
                  echo "<tr>
                            <td colspan='3'><b>Grand Total</b></td>
                            <td align='right'><b>".number_format($totoaprev)."</b></td>
                            <td align='right'><b>".number_format($totoa)."</b></td>
                            <td align='right'><b>".number_format($totgrwoa,2)."%</b></td>
                            <td align='right'><b>".number_format($totqtyprev)."</b></td>
                            <td align='right'><b>".number_format($totqty)."</b></td>
                            <td align='right'><b>".number_format($totgrwqty,2)."%</b></td>
                            <td align='right'><b>".number_format($totvnotaprev)."</b></td>
                            <td align='right'><b>".number_format($totvnota)."</b></td>
                            <td align='right'><b>".number_format($totgrwrp,2)."%</b></td>
                            <td align='right'><b>".number_format($totctrrp,2)."%</b></td>
                        </tr>";
              }
        ?>
     </table>
     <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('ctrbyseries_new/cform/','#main')">
   </div>
      </div>
    </td>
  </tr>
</table>
