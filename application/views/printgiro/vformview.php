<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printgiro/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr> 
		<td colspan="11" align="center"><span align="center">Cari data : 
			<input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >
			&nbsp;
			<input type="submit" id="bcari" name="bcari" value="Cari"></span></td>
	      </tr>
	    </thead>
	    	<th>Area</th>
      	    <th>No Giro</th>
	    	<th>Tgl Giro</th>
      	    <th>No RV</th>
	    	<th>Tgl RV</th>
      	    <th>No DT</th>
	    	<th>Tgl DT</th>	    	
      	    <th>Customer</th>
      	    <th>Bank</th>
      	    <th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  if($row->d_giro!='') {		
				  $tmp=explode('-',$row->d_giro);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
			  } else {
				  $tgl=" ";
				  $bln=" ";
				  $thn=" ";				  
			  }
			  $row->d_giro=!empty($row->d_giro)?$tgl.'-'.$bln.'-'.$thn:"";
			  
			  if($row->d_rv!='') {
				  $tmp=explode('-',$row->d_rv);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
			  } else {
				  $tgl=" ";
				  $bln=" ";
				  $thn=" ";
			  }
			  $row->d_rv=!empty($row->d_rv)?$tgl.'-'.$bln.'-'.$thn:"";

			  if($row->d_dt!='') {
				  $tmp=explode('-',$row->d_dt);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
			  } else {
				  $tgl=" ";
				  $bln=" ";
				  $thn=" ";
			  }
			  $row->d_dt=!empty($row->d_dt)?$tgl.'-'.$bln.'-'.$thn:"";
			  			  
			  echo "<tr> 
				  <td>$row->e_area_name</td>";
				if($row->f_giro_batal=='t'){
			  echo "<td><h1>$row->i_giro</h1></td>";
				}else{
			  echo "<td>$row->i_giro</td>";
				}
			  echo "
				  <td>$row->d_giro</td>
				  <td>$row->i_rv</td>
				  <td>$row->d_rv</td>
				  <td>$row->i_dt</td>
				  <td>$row->d_dt</td>
				  <td>($row->i_customer) $row->e_customer_name</td>
				  <td>$row->e_giro_bank</td>
				  <td align=right>".number_format($row->v_jumlah)."</td>
				  <td align=right>".number_format($row->v_sisa)."</td>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <center><input type="button" id="btncetak" name="btncetak" value="Cetak" onclick="return ccc('<?=$dfrom?>','<?=$dto?>','<?=$iarea?>');"></center>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function ccc(a,b,c){
	lebar =900;
    tinggi=600;
    eval('window.open("<?php echo site_url(); ?>"+"/printgiro/cform/cetak/"+a+"/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
</script>
