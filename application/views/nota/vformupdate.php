<?php
include("php/fungsi.php");
echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo form_open('nota/cform/koreksinota', array('id' => 'notasformupdate', 'name' => 'notasformupdate', 'onsubmit' => 'sendRequest(); return false')); ?>
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
            <tr>
              <td>Nota</td>
              <td><?php if ($isi->i_nota != '') {
                    echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
  readonly>";
                  } else {
                    echo "<input type=\"hidden\" id=\"inota\" name=\"inota\" value=\"\"
  readonly>";
                  }
                  $per_nota = '';
                  if ($isi->d_nota != '') {
                    $tmp = explode("-", $isi->d_nota);
                    $th = $tmp[0];
                    $bl = $tmp[1];
                    $hr = $tmp[2];
                    $per_nota = $th . $bl;
                    $dnota = $hr . "-" . $bl . "-" . $th;
                    echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" readonly 
  onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">
  ";
                    echo " <input hidden id=\"tglnota\" name=\"tglnota\" value=\"$dnota\" readonly>
  ";
                  } else {
                  ?>
                  <input readonly id="dnota" name="dnota" value="" readonly onclick="showCalendar('',this,this,'','dnota',0,20,1)">
                <?php } ?>
              </td>
              <td>Nilai Kotor</td>
              <?php
              //$enin=number_format($isi->v_nota_gross);
              $enin = number_format($isi->v_spb);
              ?>
              <td><input id="vspb" name="vspb" readonly value="<?php echo $enin; ?>"></td>
            </tr>
            <tr>
              <td>SPB</td>
              <?php
              $tmp = explode("-", $isi->d_spb);
              $th = $tmp[0];
              $bl = $tmp[1];
              $hr = $tmp[2];
              $dspb = $hr . "-" . $bl . "-" . $th;

              if ($isi->d_dkb != '') {
                $tmp = explode("-", $isi->d_dkb);
                $th = $tmp[0];
                $bl = $tmp[1];
                $hr = $tmp[2];
                $isi->d_dkb = $hr . "-" . $bl . "-" . $th;
              }
              if ($isi->d_bapb != '') {
                $tmp = explode("-", $isi->d_bapb);
                $th = $tmp[0];
                $bl = $tmp[1];
                $hr = $tmp[2];
                $isi->d_bapb = $hr . "-" . $bl . "-" . $th;
              }
              ?>
              <td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
                <input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly>
              </td>
              <?php $tmp = explode("-", $isi->d_nota);
              $th = $tmp[2];
              $bl = $tmp[1];
              $hr = $tmp[0];
              ?>
              <input type="text" id="bnota" hidden name="bnota" value="<?php echo $bl; ?>" readonly>
              <td>Discount 1</td>
              <td>
                <input id="ncustomerdiscount1" name="ncustomerdiscount1" value="<?php echo $isi->n_nota_discount1; ?>" onkeyup="formatcemua(this.value);editnilai();">
                <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" value="<?php echo number_format($isi->v_nota_discount1); ?>" data-value="<?php echo $isi->v_nota_discount1; ?>">
              </td>
            </tr>
            <tr>
              <td>Area</td>
              <td><input readonly id="arena" name="arena" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
                <input readonly id="spbold" name="spbold" value="<?php echo $isi->i_spb_old; ?>">
                <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
                <input id="idkb" name="idkb" type="hidden" value="<?php echo $isi->i_dkb; ?>">
                <input id="ddkb" name="ddkb" type="hidden" value="<?php echo $isi->d_dkb; ?>">
                <input id="ibapb" name="ibapb" type="hidden" value="<?php echo $isi->i_bapb; ?>">
                <input id="dbapb" name="dbapb" type="hidden" value="<?php echo $isi->d_bapb; ?>">
                <input id="ecumstomeraddress" name="ecumstomeraddress" type="hidden" value="">
              </td>
              <td>Discount 2</td>
              <td><input id="ncustomerdiscount2" name="ncustomerdiscount2" value="<?php echo $isi->n_nota_discount2; ?>" onkeyup="formatcemua(this.value);editnilai();">
                <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="<?php echo number_format($isi->v_nota_discount2); ?>">
              </td>
            </tr>
            <tr>
              <td>Pelanggan</td>
              <td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>">
                <!--		onclick='showModal("nota/cform/customer/"+document.getElementById("iarea").value+"/x01/","#light");jsDlgShow("#konten *", "#fade", "#light");'-->

                <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>">
                <input type="hidden" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly value="">
                <input id="fspbpkp" name="fspbpkp" type="hidden" value="">
              </td>
              <td>Discount 3</td>
              <td><input id="ncustomerdiscount3" name="ncustomerdiscount3" value="<?php echo $isi->n_nota_discount3; ?>" onkeyup="formatcemua(this.value);editnilai();">
                <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="<?php echo number_format($isi->v_nota_discount3); ?>">
              </td>
            </tr>
            <tr>
              <td>PO</td>
              <td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly>
                <input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>" onclick='showModal("nota/cform/salesman/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
                <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>">
              </td>
              <td>Discount 4</td>
              <td><input id="ncustomerdiscount4" name="ncustomerdiscount4" value="<?php echo $isi->n_nota_discount4; ?>" onkeyup="formatcemua(this.value);editnilai();">
                <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" value="<?php echo number_format($isi->v_nota_discount4); ?>">
              </td>
            </tr>
            <tr>
              <td>Konsiyasi</td>
              <td><input id="fspbconsigment" name="fspbconsigment" type="hidden" value="<?php echo $isi->f_spb_consigment; ?>"><input id="chkfspbconsigment" name="chkfspbconsigment" type="checkbox" readonly <?php if ($isi->f_spb_consigment == 't') echo "checked"; ?>>&nbsp;Masalah&nbsp;
                <input id="fmasalah" name="fmasalah" type="checkbox" value="" <?php if ($isi->f_masalah == 't') echo 'value="on" checked';
                                                                              else echo 'value=""'; ?> onclick="cekmasalah();">&nbsp;Cicil&nbsp;
                <input id="fcicil" name="fcicil" type="checkbox" <?php if ($isi->f_cicil == 't') echo 'value="on" checked';
                                                                  else echo 'value=""'; ?> onclick="cekcicil();">
                <input type="hidden" id="konsi" value="<?php echo $isi->f_spb_consigment; ?>">
              </td>
              <td>Discount Total</td>
              <td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal" value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
            </tr>
            <tr>
              <td>Insentif</td>
              <td>
                <!--<input id="finsentif" name="finsentif" type="checkbox" value="on" checked>-->
                <input id="finsentif" name="finsentif" type="checkbox" <?php if ($isi->f_insentif == 't') echo 'value="on" checked';
                                                                        else echo 'value=""'; ?> onclick="cekinsentif();">&nbsp;Nota Lama&nbsp;<input id="inotaold" name="inotaold" value="<?php echo $isi->i_nota_old; ?>">
              </td>
              <td>Nilai Bersih</td>
              <?php
              /*$tmp=$isi->v_nota-$isi->v_nota_discounttotal;*/
              $tmp = $isi->v_nota_netto;
              ?>
              <td><input readonly id="vspbbersih" name="vspbbersih" readonly value="<?php echo number_format($isi->v_spb - $isi->v_spb_discounttotal); ?>"></td>
            </tr>
            <tr>
              <td>TOP</td>
              <?php
              $tmp = explode("-", $isi->d_nota);
              $det  = $tmp[2];
              $mon  = $tmp[1];
              $yir   = $tmp[0];
              $dsj  = $yir . "/" . $mon . "/" . $det;
              $isi->n_nota_toplength = $isi->n_spb_toplength;
              if (substr($isi->i_sj, 8, 2) == '00') {
                $topnya = $isi->n_nota_toplength; #+$isi->n_toleransi_pusat;
              } else {
                $topnya = $isi->n_nota_toplength; #+$isi->n_toleransi_cabang;
              }
              $dudet  = dateAdd("d", $topnya, $dsj);
              $dudet   = explode("-", $dudet);
              $det1  = $dudet[2];
              $mon1  = $dudet[1];
              $yir1   = $dudet[0];
              $dudet  = $det1 . "-" . $mon1 . "-" . $yir1;
              ?>
              <td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly value="<?php echo $isi->n_nota_toplength; ?>">
                &nbsp;&nbsp;Jatuh tempo&nbsp;
                <input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $isi->d_jatuh_tempo; #$dudet; 
                                                                            ?>">
              </td>
              <td>Nilai Kotor (realisasi)</td>
              <td><input id="vnotagross" name="vnotagross" readonly value="<?php echo number_format($isi->v_nota_gross); ?>"></td>
            </tr>
            <tr>
              <td>Surat Jalan</td>
              <?php
              if ($isi->d_sj != '') {
                $tmp = explode("-", $isi->d_sj);
                $th = $tmp[0];
                $bl = $tmp[1];
                $hr = $tmp[2];
                $dsj = $hr . "-" . $bl . "-" . $th;
              } else {
                $dsj = '';
                $th = '';
                $bl = '';
              }
              $periodesj = $th . $bl;
              ?>
              <td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
                <input id="isj" name="isj" readonly value="<?php if ($isi->i_sj) echo $isi->i_sj; ?>">
                <input readonly readonly id="dsj" name="dsj" value="<?php echo $dsj; ?>">
              </td>
              <td>Discount Total (realisasi)</td>
              <td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" onkeyup="hitungnilai();reformat(this);" value="<?php echo number_format($isi->v_nota_discounttotal); ?>"></td>
            </tr>
            <tr>
              <td>Keterangan</td>
              <td><input id="eremark" name="eremark" value="<?php echo $isi->e_remark; ?>"></td>
              <td>Nilai SPB (realisasi)</td>
              <td>
                <input id="vspbafter" name="vspbafter" onkeyup="hitungdiscount();reformat(this);" value="<?php echo number_format($isi->v_nota_netto); ?>">
                <input type="hidden" id="vspbafterx" name="vspbafterx" value="<?= $isi->v_nota_netto ?>" readonly>
                <!-- <input id="vdpp" name="vdpp" type="hidden" value = "0" readonly>
  <input id="vppn" name="vppn" type="hidden" value = "0" readonly> -->
                <input type="hidden" id="fspbplusppn" name="fspbplusppn" value="<?php echo $isi->f_spb_plusppn; ?>">
                <input type="hidden" id="fspbplusdiscount" name="fspbplusdiscount" value="<?php echo $isi->f_spb_plusdiscount; ?>">
                <input type="hidden" id="nprice" name="nprice" value="<?php echo $isi->n_price; ?>">
                <input type="hidden" id="vnotappn" name="vnotappn" value="<?php echo number_format($isi->v_nota_ppn); ?>">
              </td>
            </tr>
            <tr>
              <td>Kelompok Harga</td>
              <td><input readonly onclick="pricegroup();" id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
                <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>">
              </td>
              <td>Alasan Revisi</td>
              <td><input id="ealasan" name="ealasan" value="<?php echo $isi->e_alasan; ?>"></td>
            </tr>
            <?php
            #	    echo 'pernota='.$per_nota.'<br>';
            #	    echo 'perskrg='.$per_skrg.'<br>';
            ?>
            <tr>
              <td width="100%" align="center" colspan="4">
                <input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
                                                            echo "value=\"$jmlitem\"";
                                                          } else {
                                                            echo "value=\"0\"";
                                                          } ?>>
                <?php #if( ($this->session->userdata('departement') == '4') && ($isi->v_sisa==$isi->v_nota_netto) && 
                #   (($this->session->userdata('level') == '5') || ($this->session->userdata('level') == '4')) &&
                #    (($isi->i_approve_pajak!=null && $isi->i_faktur_komersial!=null) || ($isi->i_faktur_komersial==null || $isi->i_faktur_komersial=='')) && ($per_nota==$per_skrg) ){
                ?>
                <?php if (($this->session->userdata('departement') == '4') && ($isi->v_sisa == $isi->v_nota_netto) &&
                  (($this->session->userdata('level') == '5') || ($this->session->userdata('level') == '4')) &&
                  (($isi->i_approve_pajak != null && $isi->i_faktur_komersial != null) || ($isi->i_faktur_komersial == null || $isi->i_faktur_komersial == ''))
                ) { ?>
                  <input name="login" id="login" value="Simpan" type="submit" onclick="return dipales()">
                  <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
                <?php } ?>
                <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listnota/cform/view/<?php echo $dfrom . "/" . $dto . "/" . $iarea . "/"; ?>","#main")'>
              </td>
            </tr>
          </table>
          <div id="detailheader" align="center">
            <table class="listtable" style="width:750px;">
              <th style="width:25px;" align="center">No</th>
              <th style="width:63px;" align="center">Kd Barang</th>
              <th style="width:300px;" align="center">Nama Barang</th>
              <th style="width:100px;" align="center">Motif</th>
              <th style="width:100px;" align="center">Harga</th>
              <!--					<th style="width:60px;"  align="center">Jml Pesan</th> -->
              <th style="width:60px;" align="center">Jml Dlv</th>
            </table>
          </div>
          <div id="detailisi" align="center">
            <?php
            echo "<table class=\"listtable\" style=\"width:750px;\">";
            $i = 0;
            $totalppn = 0;
            if ($detail) {
              foreach ($detail as $row) {
                $harga  = number_format($row->v_unit_price);
                //					$norder	=number_format($row->n_order,0);
                $ndeliv  = number_format($row->n_deliver);
                $vtotal = $row->v_unit_price * $row->n_deliver;

                if ($row->n_deliver > 0) {
                  $i++;
                  echo "<tbody>
            <tr>
            <td>
            <input style=\"width:25px;\" readonly type=\"text\" 
            id=\"baris$i\" name=\"baris$i\" value=\"$i\">
            <input type=\"hidden\" 
            id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
            <td><input style=\"width:68px;\" readonly type=\"text\" 
            id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
            <td><input style=\"width:333px;\" readonly type=\"text\" 
            id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
            <td><input style=\"width:110px;\" type=\"text\" 
            id=\"eproductmotifname$i\" readonly
            name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
            <td><input style=\"text-align:right; width:109px;\" type=\"text\" 
            id=\"vproductretail$i\" onkeyup=\"editnilai()\"
            name=\"vproductretail$i\" value=\"$harga\"></td>";
                  echo "	<td><input style=\"text-align:right; width:66px;\" onkeyup=\"editnilai()\"
            type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\">";
                  // * 21 MAR 2022
                  echo "	
            <input type=\"hidden\" id=\"iproductcategory$i\" name=\"iproductcategory$i\" value=\"$row->i_product_category\">
            <input type=\"hidden\" id=\"iproductclass$i\" name=\"iproductclass$i\" value=\"$row->i_product_class\">
            <input type=\"hidden\" id=\"eproductcategoryname$i\" name=\"eproductcategoryname$i\" value=\"$row->e_product_categoryname\">
            <input type=\"hidden\" id=\"eproductclassname$i\" name=\"eproductclassname$i\" value=\"$row->e_product_classname\">
            <input type=\"hidden\" id=\"vdiscount$i\" name=\"vdiscount$i\" value=\"$row->v_nota_discount\">
            <input type=\"hidden\" id=\"vdpp$i\" name=\"vdpp$i\" value=\"$row->v_dpp\">
            <input type=\"hidden\" id=\"vppn$i\" name=\"vppn$i\" value=\"$row->v_ppn\">
            <input type=\"hidden\" id=\"vnetto$i\" name=\"vnetto$i\" value=\"$row->v_netto\"> ";
                  // *********
                  echo "<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$vtotal\"></td>
            </tr>
            </tbody>";
                }
              }
            }
            ?>
</table>
</div>
</div>
</div>
<?= form_close() ?>
<div id="pesan"></div>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
  function hitungnilai() {
    var nilaispb = document.getElementById("vnotagross").value.replace(/\,/g, ''); //document.getElementById("vspb").value.replace(/\,/g,'');
    var nilaidis = document.getElementById("vspbdiscounttotalafter").value.replace(/\,/g, '');
    var defaultDisc1 = document.querySelector('#vcustomerdiscount1');
    var defDisc1 = parseFloat(defaultDisc1.dataset.value);

    if (!isNaN(nilaidis)) {
      if ((nilaispb - nilaidis) < 0) {
        alert("Nilai discount tidak valid !!!!!");
        document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0, input.value.length - 1);
      } else {
        document.getElementById("vspbafter").value = formatcemua(nilaispb - nilaidis);
        // document.getElementById("vcustomerdiscount1").value = formatcemua(disc1 + nilaidis);
        defaultDisc1.value = (defDisc1 + parseFloat(nilaidis));
      }
    } else {
      alert('input harus numerik !!!');
      document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0, input.value.length - 1);
    }
  }

  function hitungdiscount() {
    var nilaispb = document.getElementById("vspb").value.replace(/\,/g, '');
    var nilaitot = document.getElementById("vspbafter").value.replace(/\,/g, '');
    if (!isNaN(nilaitot)) {
      if ((nilaispb - nilaitot) < 0) {
        alert("Nilai total tidak valid !!!!!");
        document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0, input.value
          .length - 1);
      } else {
        document.getElementById("vspbdiscounttotalafter").value = formatcemua(nilaispb - nilaitot);
      }
    } else {
      alert('input harus numerik !!!');
      document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0, input.value
        .length - 1);
    }
  }

  function dipales() {
    if (document.getElementById("dnota").value != '') {
      var vtotal = parseFloat(formatulang(document.getElementById("vspbafter").value));
      var vtotalx = parseFloat(formatulang(document.getElementById("vspbafterx").value));

      if (vtotal == vtotalx) {
        document.getElementById("login").hidden = false;
        alert("Mohon cek kembali data yang akan direvisi!");
        return false;
      } else if (document.getElementById("ealasan").value != '') {
        document.getElementById("login").hidden = true;
      } else {
        document.getElementById("login").hidden = false;
        alert("Alasan koreksi tidak boleh kosong");
      }
    } else {
      document.getElementById("login").hidden = false;
      alert("Tanggal nota tidak boleh kosong");
    }
  }

  function clearitem() {
    document.getElementById("detailisi").innerHTML = '';
    document.getElementById("pesan").innerHTML = '';
    document.getElementById("jml").value = '0';
    document.getElementById("login").hidden = false;
  }

  function editnilaiori() {
    var fppn = document.getElementById("fspbplusppn").value;
    var jml = parseFloat(document.getElementById("jml").value);
    var totdis = 0;
    var totnil = 0;
    var hrg = 0;
    var ndis1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
    var ndis2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
    var ndis3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
    var ndis4 = parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
    var vdis1 = 0;
    var vdis2 = 0;
    var vdis3 = 0;
    var vdis4 = 0;
    for (i = 1; i <= jml; i++) {
      var hrgtmp = parseFloat(formatulang(document.getElementById("vproductretail" + i).value)) * parseFloat(formatulang(
        document.getElementById("ndeliver" + i).value));
      hrg = hrg + hrgtmp;
    }
    //	  hrg=Math.round(hrg);
    vdis1 = vdis1 + (hrg * ndis1) / 100; //Math.round(vdis1+(hrg*ndis1)/100);
    vdis2 = vdis2 + (((hrg - vdis1) * ndis2) / 100); //Math.round(vdis2+(((hrg-vdis1)*ndis2)/100));
    vdis3 = vdis3 + (((hrg - (vdis1 + vdis2)) * ndis3) / 100); //Math.round(vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100));
    vdis4 = vdis4 + (((hrg - (vdis1 + vdis2 + vdis3)) * ndis4) /
      100); //Math.round(vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100));
    if (fppn == 't') {
      vdistot = Math.round(vdis1 + vdis2 + vdis3 + vdis4);
    } else {
      vdistot = vdis1 + vdis2 + vdis3 + vdis4; //Math.round(vdis1+vdis2+vdis3+vdis4);
    }
    vhrgreal = hrg - vdistot; //Math.round(hrg-vdistot);
    if (document.getElementById("fspbconsigment").value == 'f') {
      document.getElementById("vnotagross").value = formatcemua(Math.round(hrg));
      document.getElementById("vspbdiscounttotalafter").value = formatcemua(Math.round(vdistot));
      document.getElementById("vspbafter").value = formatcemua(Math.round(vhrgreal));
    }
    var fdis = document.getElementById("fspbplusdiscount").value;
    var bersih = parseFloat(formatulang(document.getElementById("vspbafter").value));
    var kotor = parseFloat(formatulang(document.getElementById("vspb").value));
    if ((fppn == 't') && (fdis == 'f')) {
      document.getElementById("nprice").value = 1;
      document.getElementById("vnotappn").value = 0;
    } else if ((fppn == 't') && (fdis == 't')) {
      document.getElementById("nprice").value = bersih / kotor;
      document.getElementById("vnotappn").value = 0;
    } else if ((fppn == 'f') && (fdis == 't')) {
      document.getElementById("nprice").value = 1 / 1.1;
      kotorminppn = Math.round(hrg / 1.1);
      vhrgreal = kotorminppn; //Math.round(kotorminppn);
      pepen = vhrgreal * 0.1; //Math.round(vhrgreal*0.1);
      berrsih = kotorminppn + pepen; //Math.round(kotorminppn+pepen);
      if (document.getElementById("fspbconsigment").value == 'f') {
        document.getElementById("vnotagross").value = formatcemua(Math.round(kotorminppn));
        document.getElementById("vspbdiscounttotalafter").value = 0; //formatcemua(vdistot);
        document.getElementById("vspbafter").value = formatcemua(Math.round(berrsih));
        document.getElementById("vnotappn").value = formatcemua(Math.round(pepen));
      }
    } else if ((fppn == 'f') && (fdis == 'f')) {
      document.getElementById("nprice").value = 1 / 1.1;
      kotorminppn = Math.round(hrg / 1.1);
      //      alert(kotorminppn);
      vdis1 = 0;
      vdis2 = 0;
      vdis3 = 0;
      vdis4 = 0;
      vdis1 = (vdis1 + (kotorminppn * ndis1) / 100); //Math.round(vdis1+(kotorminppn*ndis1)/100);
      vdis2 = (vdis2 + (((kotorminppn - vdis1) * ndis2) / 100)); //Math.round(vdis2+(((kotorminppn-vdis1)*ndis2)/100));
      vdis3 = (vdis3 + (((kotorminppn - (vdis1 + vdis2)) * ndis3) /
        100)); //Math.round(vdis3+(((kotorminppn-(vdis1+vdis2))*ndis3)/100));
      vdis4 = (vdis4 + (((kotorminppn - (vdis1 + vdis2 + vdis3)) * ndis4) /
        100)); //Math.round(vdis4+(((kotorminppn-(vdis1+vdis2+vdis3))*ndis4)/100));
      if (document.getElementById("fspbconsigment").value == 'f') {
        document.getElementById("vcustomerdiscount1").value = formatcemua(Math.round(vdis1));
        document.getElementById("vcustomerdiscount2").value = formatcemua(Math.round(vdis2));
        document.getElementById("vcustomerdiscount3").value = formatcemua(Math.round(vdis3));
        document.getElementById("vcustomerdiscount4").value = formatcemua(Math.round(vdis4));
        vdistot = vdis1 + vdis2 + vdis3 + vdis4; //Math.round(vdis1+vdis2+vdis3+vdis4);
        vhrgreal = kotorminppn - vdistot; //Math.round(kotorminppn-vdistot);
        pepen = vhrgreal * 0.1; //Math.round(vhrgreal*0.1);
        berrsih = kotorminppn - vdistot + pepen; //Math.round(kotorminppn-vdistot+pepen);
        document.getElementById("vnotagross").value = formatcemua(Math.round(kotorminppn));
        document.getElementById("vspbdiscounttotalafter").value = formatcemua(Math.round(vdistot));
        document.getElementById("vspbafter").value = formatcemua(Math.round(berrsih));
        document.getElementById("vnotappn").value = formatcemua(Math.round(pepen));
      }
    }
  }

  function cekmasalah() {
    masalah = document.getElementById("fmasalah").value;
    if (masalah == 'on') {
      document.getElementById("fmasalah").checked = false;
      document.getElementById("fmasalah").value = '';
    } else {
      document.getElementById("fmasalah").checked = true;
      document.getElementById("fmasalah").value = 'on';
    }
  }

  function cekinsentif() {
    masalah = document.getElementById("finsentif").value;
    if (masalah == 'on') {
      document.getElementById("finsentif").checked = false;
      document.getElementById("finsentif").value = '';
    } else {
      document.getElementById("finsentif").checked = true;
      document.getElementById("finsentif").value = 'on';
    }
  }

  function cekcicil() {
    masalah = document.getElementById("fcicil").value;
    if (masalah == 'on') {
      document.getElementById("fcicil").checked = false;
      document.getElementById("fcicil").value = '';
    } else {
      document.getElementById("fcicil").checked = true;
      document.getElementById("fcicil").value = 'on';
    }
  }

  function pricegroup() {
    nota = document.getElementById("inota").value;
    if (nota == '') nota = document.getElementById("ispb").value;
    area = document.getElementById("iarea").value;
    showModal("nota/cform/pricegroup/" + nota + "/" + area, "#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  $(document).ready(function() {
    editnilai();
  });

  function tambah_item(a) {
    konsi = document.getElementById("konsi").value;
    alert(konsi);
    if (a < 22 && konsi == 'f') {
      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
      si_inner = document.getElementById("detailisi").innerHTML;
      //    alert(si_inner);
      si_inner = si_inner + "<table class=\"listtable\" style=\"width:750px;\">";
      si_inner = si_inner + "<tbody><tr><td><input style=\"width:25px;\" readonly type=\"text\" id=\"baris" + a +
        "\" name=\"baris" + a + "\" value=\"" + a + "\">";
      si_inner = si_inner + "<input type=\"hidden\" id=\"motif" + a + "\" name=\"motif" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:68px;\" readonly type=\"text\" id=\"iproduct" + a +
        "\" name=\"iproduct" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:333px;\" readonly type=\"text\"	id=\"eproductname" + a +
        "\" name=\"eproductname" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:110px;\" type=\"text\" id=\"eproductmotifname" + a +
        "\" readonly name=\"eproductmotifname" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"text-align:right; width:109px;\" type=\"text\" id=\"vproductretail" + a +
        "\" readonly name=\"vproductretail" + a + "\" value=\"\"></td>";
      si_inner = si_inner +
        "<td><input style=\"text-align:right; width:66px;\" onkeyup=\"editnilai()\" type=\"text\" id=\"ndeliver" + a +
        "\" name=\"ndeliver" + a + "\" value=\"\"><input type=\"hidden\" id=\"vdiscount" + a + "\" name=\"vdiscount" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vdpp" + a + "\" name=\"vdpp" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vppn" + a + "\" name=\"vppn" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vnetto" + a + "\" name=\"vnetto" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vtotal" + a + "\" name=\"vtotal" + a +
        "\" value=\"\"></td></tr></tbody></table>";
      //**********************//
      j = 0;
      var baris = Array()
      var motif = Array();
      var iproduct = Array();
      var eproductname = Array();
      var eproductmotifname = Array();
      var vproductretail = Array();
      var ndeliver = Array();
      // * 21 MAR 2022
      var vdiscount = Array();
      var vdpp = Array();
      var vppn = Array();
      var vnetto = Array();
      //*********** */
      var vtotal = Array();
      for (i = 1; i < a; i++) {
        j++;
        baris[j] = document.getElementById("baris" + i).value;
        motif[j] = document.getElementById("motif" + i).value;
        iproduct[j] = document.getElementById("iproduct" + i).value;
        eproductname[j] = document.getElementById("eproductname" + i).value;
        eproductmotifname[j] = document.getElementById("eproductmotifname" + i).value;
        vproductretail[j] = document.getElementById("vproductretail" + i).value;
        ndeliver[j] = document.getElementById("ndeliver" + i).value;
        vdiscount[j] = document.getElementById("vdiscount" + i).value;
        vdpp[j] = document.getElementById("vdpp" + i).value;
        vppn[j] = document.getElementById("vppn" + i).value;
        vnetto[j] = document.getElementById("vnetto" + i).value;
        vtotal[j] = document.getElementById("vtotal" + i).value;
      }
      document.getElementById("detailisi").innerHTML = si_inner;
      j = 0;
      for (i = 1; i < a; i++) {
        j++;
        document.getElementById("baris" + i).value = baris[j];
        document.getElementById("motif" + i).value = motif[j];
        document.getElementById("iproduct" + i).value = iproduct[j];
        document.getElementById("eproductname" + i).value = eproductname[j];
        document.getElementById("eproductmotifname" + i).value = eproductmotifname[j];
        document.getElementById("vproductretail" + i).value = vproductretail[j];
        document.getElementById("ndeliver" + i).value = ndeliver[j];
        // * 21 MAR 2022
        document.getElementById("vdiscount" + i).value = vdiscount[j];
        document.getElementById("vdpp" + i).value = vdpp[j];
        document.getElementById("vppn" + i).value = vppn[j];
        document.getElementById("vnetto" + i).value = vnetto[j];
        // ***************
        document.getElementById("vtotal" + i).value = vtotal[j];
      }
      //**********************//
      //    document.getElementById("detailisi").innerHTML=si_inner;
      //    alert(si_inner);    
      showModal("nota/cform/product/" + a + "/" + document.getElementById("ipricegroup").value + "/x01/", "#light");
      jsDlgShow("#konten *", "#fade", "#light");
    } else if (a < 10000 && konsi == 't') {
      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
      si_inner = document.getElementById("detailisi").innerHTML;
      //    alert(si_inner);
      si_inner = si_inner + "<table class=\"listtable\" style=\"width:750px;\">";
      si_inner = si_inner + "<tbody><tr><td><input style=\"width:25px;\" readonly type=\"text\" id=\"baris" + a +
        "\" name=\"baris" + a + "\" value=\"" + a + "\">";
      si_inner = si_inner + "<input type=\"hidden\" id=\"motif" + a + "\" name=\"motif" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:68px;\" readonly type=\"text\" id=\"iproduct" + a +
        "\" name=\"iproduct" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:333px;\" readonly type=\"text\"	id=\"eproductname" + a +
        "\" name=\"eproductname" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"width:110px;\" type=\"text\" id=\"eproductmotifname" + a +
        "\" readonly name=\"eproductmotifname" + a + "\" value=\"\"></td>";
      si_inner = si_inner + "<td><input style=\"text-align:right; width:109px;\" type=\"text\" id=\"vproductretail" + a +
        "\" readonly name=\"vproductretail" + a + "\" value=\"\"></td>";
      si_inner = si_inner +
        "<td><input style=\"text-align:right; width:66px;\" onkeyup=\"editnilai()\" type=\"text\" id=\"ndeliver" + a +
        "\" name=\"ndeliver" + a + "\" value=\"\"><input type=\"hidden\" id=\"vdiscount" + a + "\" name=\"vdiscount" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vdpp" + a + "\" name=\"vdpp" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vppn" + a + "\" name=\"vppn" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vnetto" + a + "\" name=\"vnetto" + a +
        "\" value=\"\"><input type=\"hidden\" id=\"vtotal" + a + "\" name=\"vtotal" + a +
        "\" value=\"\"></td></tr></tbody></table>";
      //**********************//
      j = 0;
      var baris = Array()
      var motif = Array();
      var iproduct = Array();
      var eproductname = Array();
      var eproductmotifname = Array();
      var vproductretail = Array();
      var ndeliver = Array();
      // * 21 MAR 2022
      var vdiscount = Array();
      var vdpp = Array();
      var vppn = Array();
      var vnetto = Array();
      //*********** */
      var vtotal = Array();
      for (i = 1; i < a; i++) {
        j++;
        baris[j] = document.getElementById("baris" + i).value;
        motif[j] = document.getElementById("motif" + i).value;
        iproduct[j] = document.getElementById("iproduct" + i).value;
        eproductname[j] = document.getElementById("eproductname" + i).value;
        eproductmotifname[j] = document.getElementById("eproductmotifname" + i).value;
        vproductretail[j] = document.getElementById("vproductretail" + i).value;
        ndeliver[j] = document.getElementById("ndeliver" + i).value;
        vdiscount[j] = document.getElementById("vdiscount" + i).value;
        vdpp[j] = document.getElementById("vdpp" + i).value;
        vppn[j] = document.getElementById("vppn" + i).value;
        vnetto[j] = document.getElementById("vnetto" + i).value;
        vtotal[j] = document.getElementById("vtotal" + i).value;
      }
      document.getElementById("detailisi").innerHTML = si_inner;
      j = 0;
      for (i = 1; i < a; i++) {
        j++;
        document.getElementById("baris" + i).value = baris[j];
        document.getElementById("motif" + i).value = motif[j];
        document.getElementById("iproduct" + i).value = iproduct[j];
        document.getElementById("eproductname" + i).value = eproductname[j];
        document.getElementById("eproductmotifname" + i).value = eproductmotifname[j];
        document.getElementById("vproductretail" + i).value = vproductretail[j];
        document.getElementById("ndeliver" + i).value = ndeliver[j];
        // * 21 MAR 2022
        document.getElementById("vdiscount" + i).value = vdiscount[j];
        document.getElementById("vdpp" + i).value = vdpp[j];
        document.getElementById("vppn" + i).value = vppn[j];
        document.getElementById("vnetto" + i).value = vnetto[j];
        // ***************
        document.getElementById("vtotal" + i).value = vtotal[j];
      }
      //**********************//
      //    document.getElementById("detailisi").innerHTML=si_inner;
      //    alert(si_inner);    
      showModal("nota/cform/product/" + a + "/" + document.getElementById("ipricegroup").value + "/x01/", "#light");
      jsDlgShow("#konten *", "#fade", "#light");
    } else {
      konsi = document.getElementById("fspbconsigment").value;
      // if(konsi == false){
      alert('Maksimum 21 item');
      // }
    }
  }

  function editnilai() {
    taxParse(document.getElementById("dsj").value);

    ndpp = exlValue
    nppn = taxValue

    jml = document.getElementById("jml").value;
    // f_pkp = document.getElementById("f_pkp").value;
    nsjdisc1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
    nsjdisc2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
    nsjdisc3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
    nsjdisc4 = parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
    vsjdiscounttotal = parseFloat(formatulang(document.getElementById("vspbdiscounttotal").value))

    if (jml <= 0) {} else {
      salah = false;
      // kons    = document.getElementById("fspbconsigment").value;

      if (!salah) {
        vdisc1 = 0;
        vdisc2 = 0;
        vdisc3 = 0;
        vdisc4 = 0;
        vdpp = 0;
        vppn = 0;
        vnetto = 0;
        vtppn = 0;
        vtnetto = 0;
        vsjdisc1 = 0;
        vsjdisc2 = 0;
        vsjdisc3 = 0;
        vsjdisc4 = 0;
        gros = 0;
        grosgr = 0;

        for (i = 1; i <= jml; i++) {
          hrg = formatulang(document.getElementById("vproductretail" + i).value);
          hrggr = formatulang(document.getElementById("vproductretail" + i).value);
          qty = formatulang(document.getElementById("ndeliver" + i).value);
          if (isNaN(qty)) {
            qty = 0;
          }

          vhrg = parseFloat(hrg) * parseFloat(qty);
          vhrggr = parseFloat(hrggr) * parseFloat(qty);
          vdisc1 = Math.round(((vhrggr * nsjdisc1) / 100));
          vdisc2 = Math.round((((vhrggr - vdisc1) * nsjdisc2) / 100));
          vdisc3 = Math.round((((vhrggr - (vdisc1 + vdisc2)) * nsjdisc3) / 100));
          vdisc4 = Math.round((((vhrggr - (vdisc1 + vdisc2 + vdisc3)) * nsjdisc4) / 100));

          vsjdisc1 = vsjdisc1 + vdisc1;
          vsjdisc2 = vsjdisc2 + vdisc2;
          vsjdisc3 = vsjdisc3 + vdisc3;
          vsjdisc4 = vsjdisc4 + vdisc4;
          vdiscount = vdisc1 + vdisc2 + vdisc3 + vdisc4;
          gros = gros + vhrg;
          grosgr = grosgr + vhrggr;

          // Rumus ini supaya sewaktu export ke CSV utk EFaktur tidak selisih
          // if(f_pkp == 't'){
          vnetto = Math.round(vhrg - Math.round(vdiscount, 0), 0);
          vppn = Math.round(vnetto / ndpp * nppn, 0);
          vdpp = vnetto - vppn;
          // }else{
          // vdpp = Math.round(vhrg - Math.round(vdiscount, 0), 0);
          // vppn = 0;
          // }

          vnetto = vdpp + vppn;
          vtnetto = vtnetto + vnetto;
          vtppn = vtppn + vppn;

          document.getElementById("vdpp" + i).value = vdpp;
          document.getElementById("vppn" + i).value = vppn;
          document.getElementById("vdiscount" + i).value = vdiscount;
          document.getElementById("vnetto" + i).value = vnetto;
          document.getElementById("vtotal" + i).value = formatcemua(vhrg);
        } // END FOR ITEM
        document.getElementById("vnotagross").value = formatcemua(grosgr);
        document.getElementById("vnotappn").value = vtppn;
        if ((nsjdisc1 + nsjdisc2 + nsjdisc3 == 0) && (vsjdiscounttotal != 0)) {
          vsjdisc1 = vsjdiscounttotal;
        }
        vtot = 0;
        if (gros > 0) {
          document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
          document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
          document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
          document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);

          vdis1 = parseFloat(vsjdisc1);
          vdis2 = parseFloat(vsjdisc2);
          vdis3 = parseFloat(vsjdisc3);
          vdis4 = parseFloat(vsjdisc4);

          nTDisc1 = nsjdisc1 + nsjdisc2 * (100 - nsjdisc1) / 100;
          nTDisc2 = nsjdisc3 * (100 - nsjdisc3) / 100;
          nTDisc = nTDisc1 + nTDisc2 * (100 - nTDisc1) / 100;

          if ((nTDisc == 0) && (vsjdiscounttotal != 0)) {
            vtotdis = vsjdiscounttotal;
          } else {
            vtotdis = vsjdisc1 + vsjdisc2 + vsjdisc3 + vsjdisc4;
          }

          document.getElementById("vspbdiscounttotalafter").value = formatcemua(vtotdis);
          document.getElementById("vspbafter").value = formatcemua(vtnetto);
          // document.getElementById("vsjnettoprint").value = formatcemua(vtnetto);
        } else {
          document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
          document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
          document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
          document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);
          document.getElementById("vspbafter").value = 0;
          // document.getElementById("vsjnettoprint").value = formatcemua(vtnetto);
          if (document.getElementById("fspbconsigment").value == 'f') {
            document.getElementById("vspbdiscounttotalafter").value = 0;
          }
        }
      } // END != SALAH
    } // END ELSE
  }

  // FUNCTION ORI BELUM NGIKUTIN CARA HITUNG DARI PAK SANTO
  function editnilai_ori() {
    jml = document.getElementById("jml").value;
    if (jml <= 0) {} else {
      salah = false;
      /*    gud=document.getElementById("istore").value;
      kons=document.getElementById("fspbconsigment").value;
      if(gud!='AA' && gud!='PB' && kons!='t'){
        for(i=1;i<=jml;i++){
          stock  =formatulang(document.getElementById("ndeliverhidden"+i).value);
          deliver=formatulang(document.getElementById("ndeliver"+i).value);
          if(parseFloat(stock)<0)stock=0;
          if(parseFloat(deliver)>parseFloat(stock)){
            alert('Jumlah Kirim melebihi jumlah stock');
            document.getElementById("ndeliver"+i).value=0;
            //            salah=true;
            break;
          }
        }
      }
      */
      if (!salah) {
        gros = 0;
        grosgr = 0;
        for (i = 1; i <= jml; i++) {
          //          if(document.getElementById("fspbplusppn").value=='f' && document.getElementById("fspbconsigment").value=='f'){
          //	          hrg=parseFloat(formatulang(document.getElementById("vproductretail"+i).value))/1.1;
          //          }else{
          hrg = formatulang(document.getElementById("vproductretail" + i).value);
          //          }
          hrggr = formatulang(document.getElementById("vproductretail" + i).value);
          qty = formatulang(document.getElementById("ndeliver" + i).value);
          vhrg = parseFloat(hrg) * parseFloat(qty);
          vhrggr = parseFloat(hrggr) * parseFloat(qty);
          gros = gros + vhrg;
          grosgr = grosgr + vhrggr;
          document.getElementById("vtotal" + i).value = formatcemua(vhrg);
        }
        //        document.getElementById("vsjgross").value=formatcemua(gros);
        document.getElementById("vnotagross").value = formatcemua(grosgr);
        nsjdisc1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
        nsjdisc2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
        nsjdisc3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
        nsjdisc4 = parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
        //        if(nsjdisc1==0)
        if ((nsjdisc1 + nsjdisc2 + nsjdisc3 + nsjdisc4 == 0))
          vsjdisc1 = parseFloat(formatulang(document.getElementById("vcustomerdiscount1").value));
        else
          vsjdisc1 = 0;
        vsjdisc2 = 0;
        vsjdisc3 = 0;
        vsjdisc4 = 0;
        vtot = 0;
        if (gros > 0) {
          //          if(nsjdisc1>0) vsjdisc1=vsjdisc1+((gros*nsjdisc1)/100);
          //	        vsjdisc2=vsjdisc2+(((gros-vsjdisc1)*nsjdisc2)/100);
          //	        vsjdisc3=vsjdisc3+(((gros-(vsjdisc1+vsjdisc2))*nsjdisc3)/100);
          if (nsjdisc1 > 0) vsjdisc1 = vsjdisc1 + ((grosgr * nsjdisc1) / 100);
          vsjdisc2 = vsjdisc2 + (((grosgr - vsjdisc1) * nsjdisc2) / 100);
          vsjdisc3 = vsjdisc3 + (((grosgr - (vsjdisc1 + vsjdisc2)) * nsjdisc3) / 100);
          vsjdisc4 = vsjdisc4 + (((grosgr - (vsjdisc1 + vsjdisc2 + vsjdisc3)) * nsjdisc4) / 100);
          document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
          document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
          document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
          document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);
          vdis1 = parseFloat(vsjdisc1);
          vdis2 = parseFloat(vsjdisc2);
          vdis3 = parseFloat(vsjdisc3);
          vdis4 = parseFloat(vsjdisc4);
          vtotdis = vdis1 + vdis2 + vdis3 + vdis4;

          nTDisc1 = nsjdisc1 + nsjdisc2 * (100 - nsjdisc1) / 100;
          nTDisc2 = nsjdisc3 + nsjdisc4 * (100 - nsjdisc3) / 100;
          //          nTDisc   = nTDisc1 + nsjdisc3 * (100-nTDisc1)/100;
          nTDisc = nTDisc1 + nTDisc2 * (100 - nTDisc1) / 100;
          //          vtotdis = nTDisc * gros / 100;
          //          alert(nTDisc);
          //          alert(grosgr);
          //          vtotdis = nTDisc * grosgr / 100;
          //          if(document.getElementById("fspbplusppn").value=='t'){
          //          	vtotdis=Math.round(vtotdis);
          //          }
          if (document.getElementById("fspbconsigment").value == 'f') {
            document.getElementById("vspbdiscounttotalafter").value = formatcemua(Math.round(vtotdis));
            //            if(document.getElementById("fspbplusppn").value=='f'){
            //              vppn=(Math.round(gros)-Math.round(vtotdis))*0.1;
            //              vtotbersih=Math.round(gros)-Math.round(vtotdis)+Math.round(vppn);
            //            }else{	
            //              vtotbersih=parseFloat(gros)-parseFloat(vtotdis);
            //            }
            vtotbersih = parseFloat(grosgr) - parseFloat(vtotdis);
            document.getElementById("vspbafter").value = formatcemua(Math.round(vtotbersih));

            /* HITUNG ULANG DPP & PPN KHUSUS CV MILLENIUM (13099) 27 AGS 2021 */
            var icustomer = document.getElementById("icustomer").value;

            if (icustomer == "13099" || icustomer == "13114") {
              var ttdpp = Math.floor(parseFloat(gros / 1.1) - parseFloat(vtotdis / 1.1));
              var ttppn = (parseFloat(ttdpp) * parseFloat(0.1));
              // console.log(icustomer);

              document.getElementById("vdpp").value = ttdpp;
              document.getElementById("vppn").value = ttppn;
            }
            /* ************************************************************** */
          } else if (document.getElementById("fspbplusppn").value == 't') {
            vtotbersih = parseFloat(gros) - parseFloat(formatulang(document.getElementById("vspbdiscounttotalafter")
              .value));
            document.getElementById("vspbafter").value = formatcemua(Math.round(vtotbersih));
          } else {
            vtotbersih = parseFloat(grosgr) - parseFloat(Math.round(vtotdis));
            document.getElementById("vspbafter").value = formatcemua(Math.round(vtotbersih));

            /* HITUNG ULANG DPP & PPN KHUSUS CV MILLENIUM (13099) 27 AGS 2021 */
            var icustomer = document.getElementById("icustomer").value;

            if (icustomer == "13099" || icustomer == "13114") {
              var ttdpp = Math.floor(parseFloat(gros / 1.1) - parseFloat(vtotdis / 1.1));
              var ttppn = (parseFloat(ttdpp) * parseFloat(0.1));
              // console.log(icustomer);

              document.getElementById("vdpp").value = ttdpp;
              document.getElementById("vppn").value = ttppn;
            }
            /* ************************************************************** */
          }
        } else {
          document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
          document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
          document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
          document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);
          document.getElementById("vspbafter").value = 0;
          if (document.getElementById("fspbconsigment").value == 'f') {
            document.getElementById("vspbdiscounttotalafter").value = 0;
          }
        }
      }
    }
  }

  function afterSetDateValue(ref_field, target_field, date) {
    dnota = document.getElementById('dnota').value;
    bnota = document.getElementById('bnota').value;
    dtmp = dnota.split('-');
    per = dtmp[2] + dtmp[1] + dtmp[0];
    bln = dtmp[1];
    if ((bnota != '') && (dnota != '')) {
      if (bnota != bln) {
        alert("Tanggal Nota tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("dnota").value = document.getElementById("tglnota").value;
      }
    }
  }
</script>