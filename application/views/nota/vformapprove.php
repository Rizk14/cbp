<?php
include("php/fungsi.php");
?>
<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'nota/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="spbformupdate">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td>Nota</td>
								<td><?php if ($isi->i_nota != '') {
										echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
										   readonly>";
									} else {
										echo "<input type=\"hidden\" id=\"inota\" name=\"inota\" value=\"\"
										   readonly>";
									}
									if ($isi->d_nota != '') {
										$tmp = explode("-", $isi->d_nota);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$dnota = $hr . "-" . $bl . "-" . $th;
										if ($isi->d_sj_receive >= $isi->d_sj) {
											$tmp = explode("-", $isi->d_sj_receive);
										} else {
											$tmp = explode("-", $isi->d_sj_receive);
										}
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$xdkb = $hr . "-" . $bl . "-" . $th;
										echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">";
										echo " <input type=\"hidden\" id=\"dtmp\" name=\"dtmp\" value=\"$xdkb\">";
									} elseif ($isi->d_sj < '2019-10-01') {

										$tmp = explode("-", $isi->d_sj_receive);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$dnota = $hr . "-" . $bl . "-" . $th;
										echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">";
										echo " <input type=\"hidden\" id=\"dtmp\" name=\"dtmp\" value=\"$dnota\">";
									} elseif ($isi->d_sj >= '2019-10-01') {

										$tmp = explode("-", $isi->d_sj);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$dnota = $hr . "-" . $bl . "-" . $th;
										#echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">";
										echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" >";
										echo " <input type=\"hidden\" id=\"dtmp\" name=\"dtmp\" value=\"$dnota\">";
									} else {
									?>
										<input readonly id="dnota" name="dnota" value="<?php echo $tgl; ?>" onclick="showCalendar('',this,this,'','dnota',0,20,1)">
										<input type="hidden" id="dtmp" name="dtmp" value="<?php echo $tgl; ?>">
									<?php } ?>
									<input id="inotaold" name="inotaold" value="">
								</td>
								<td>Kelompok Harga</td>
								<td><input readonly onclick="pricegroup();" id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
									<input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>">
									<input id="fspbconsigment" name="fspbconsigment" type="hidden" value="<?php echo $isi->f_spb_consigment; ?>">
								</td>
							</tr>
							<tr>
								<td>SPB</td>
								<?php
								$tmp = explode("-", $isi->d_spb);
								$th = $tmp[0];
								$bl = $tmp[1];
								$hr = $tmp[2];
								$dspb = $hr . "-" . $bl . "-" . $th;
								?>
								<td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
									<input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly>
								</td>
								<td>Nilai Kotor</td>
								<?php
								$enin = number_format($isi->v_spb);
								?>
								<td><input id="vspb" name="vspb" readonly value="<?php echo $enin; ?>"></td>
							</tr>
							<tr>
								<td width="12%">Promo</td>
								<td width="38%"><input readonly id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name; ?>" readonly>
									<input id="ispbprogram" name="ispbprogram" type="hidden" value="<?php echo $isi->i_spb_program; ?>">
								</td>
								<td>Discount 1</td>
								<td><input id="ncustomerdiscount1" name="ncustomerdiscount1" value="<?php echo $isi->n_spb_discount1; ?>" onkeyup="formatcemua(this.value); editnilai();">
									<input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" value="<?php echo number_format($isi->v_spb_discount1); ?>">
								</td>
							</tr>
							<tr>
								<td>Area</td>
								<td><input readonly id="arena" name="arena" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>" readonly>
									<input readonly id="spbold" name="spbold" value="<?php echo $isi->i_spb_old; ?>" readonly>
									<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
								</td>
								<td>Discount 2</td>
								<td><input id="ncustomerdiscount2" name="ncustomerdiscount2" value="<?php echo $isi->n_spb_discount2; ?>" onkeyup="formatcemua(this.value);editnilai();">
									<input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="<?php echo number_format($isi->v_spb_discount2); ?>">
								</td>
							</tr>
							<tr>
								<td>Pelanggan</td>
								<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan()" value="<?php echo $isi->e_customer_name; ?>" readonly>
									<input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>">
								</td>
								<td>Discount 3</td>
								<td><input id="ncustomerdiscount3" name="ncustomerdiscount3" value="<?php echo $isi->n_spb_discount3; ?>" onkeyup="formatcemua(this.value);editnilai();">
									<input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="<?php echo number_format($isi->v_spb_discount3); ?>">
								</td>
							</tr>
							<tr>
								<td>PO</td>
								<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly></td>
								<td>Discount 4</td>
								<td><input id="ncustomerdiscount4" name="ncustomerdiscount4" value="<?php echo $isi->n_spb_discount4; ?>" onkeyup="formatcemua(this.value);editnilai();">
									<input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" value="<?php echo number_format($isi->v_spb_discount4); ?>">
								</td>
							</tr>
							<tr>
								<td>Masalah</td>
								<td><!--<input id="fspbconsigment" name="fspbconsigment" type="checkbox" readonly 
				   <?php if ($isi->f_spb_consigment == 't') echo "checked"; ?>>&nbsp;-->
									<input id="fmasalah" name="fmasalah" type="checkbox" value="">&nbsp;Insentif&nbsp;
									<input id="finsentif" name="finsentif" type="checkbox" value="on" checked>&nbsp;Cicil&nbsp;
									<input id="fcicil" name="fcicil" type="checkbox" value="on" <?php if ($isi->f_customer_cicil == 't') echo "checked"; ?>>
								</td>
								<td>Discount Total</td>
								<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal" value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
							</tr>
							<tr>
								<td>TOP</td>
								<?php
								$tmp = explode("-", $isi->d_sj);
								$det	= $tmp[2];
								$mon	= $tmp[1];
								$yir 	= $tmp[0];
								$dsj	= $yir . "/" . $mon . "/" . $det;
								if (substr($isi->i_sj, 8, 2) == '00') {
									$topnya = $isi->n_spb_toplength; #+$isi->n_toleransi_pusat;
								} else {
									$topnya = $isi->n_spb_toplength; #+$isi->n_toleransi_cabang;
								}
								$dudet	= dateAdd("d", $topnya, $dsj);
								$dudet 	= explode("-", $dudet);
								$det1	= $dudet[2];
								$mon1	= $dudet[1];
								$yir1 	= $dudet[0];
								$dudet	= $det1 . "-" . $mon1 . "-" . $yir1;
								?>
								<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly value="<?php echo $isi->n_spb_toplength; ?>">
									&nbsp;&nbsp;Jatuh tempo&nbsp;
									<input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $dudet; ?>">
								</td>
								<td>Nilai Bersih</td>
								<?php
								$tmp = $isi->v_spb - $isi->v_spb_discounttotal;
								?>
								<td><input readonly id="vspbbersih" name="vspbbersih" readonly value="<?php echo number_format($tmp); ?>"></td>
							</tr>
							<tr>
								<td>Salesman</td>
								<td><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
									<input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>">
								</td>
								<td>Discount Total (realisasi)</td>
								<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" onkeyup="hitungnilai();reformat(this);" value="<?php echo number_format($isi->v_nota_discounttotal); ?>"></td>
							</tr>
							<tr>
								<td>Surat Jalan</td>
								<?php
								$tmp = explode("-", $isi->d_sj);
								$th = $tmp[0];
								$bl = $tmp[1];
								$hr = $tmp[2];
								$dsj = $hr . "-" . $bl . "-" . $th;
								?>
								<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
									<input id="isj" name="isj" readonly value="<?php echo $isi->i_sj; ?>">
									<input readonly readonly id="dsj" name="dsj" value="<?php echo $dsj; ?>">
								</td>
								<td>Nilai SPB (realisasi)</td>
								<td><input id="vspbafter" name="vspbafter" onkeyup="hitungdiscount();reformat(this);" value="<?php echo number_format($isi->v_nota_netto); ?>"></td>
							</tr>
							<tr>
								<td>PKP</td>
								<td colspan=""><!--<input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn; ?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount; ?>">-->
									<input id="fspbpkp" name="fspbpkp" type="hidden" value="<?php echo $isi->f_spb_pkp; ?>">
									<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly value="<?php echo $isi->e_customer_pkpnpwp; ?>">
									<input type="hidden" id="fspbplusppn" name="fspbplusppn" value="<?php echo $isi->f_spb_plusppn; ?>">
									<input type="hidden" id="fspbplusdiscount" name="fspbplusdiscount" value="<?php echo $isi->f_spb_plusdiscount; ?>">
									<input type="hidden" id="nprice" name="nprice" value="1">
									<input type="hidden" id="vnotagross" name="vnotagross" value="0">
									<input type="hidden" id="vnotappn" name="vnotappn" value="0">
								</td>
								<td>Keterangan</td>
								<td><input id="eremark" name="eremark" value=""></td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4">
									<input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("nota/cform/view/<?php echo $dfrom . "/" . $dto . "/" . $iarea . "/"; ?>","#main")'>
									<input name="cmdbalik" id="cmdbalik" value="Kembalikan SPB" type="button" onclick='show("nota/cform/balikspb/<?php echo $isi->i_spb . "/" . $isi->i_area . "/" . $dfrom . "/" . $dto . "/"; ?>","#main")'>
								</td>
							</tr>
						</table>
						<div id="detailheader" align="center">
							<table class="listtable" style="width:750px;">
								<th style="width:25px;" align="center">No</th>
								<th style="width:63px;" align="center">Kd Barang</th>
								<th style="width:300px;" align="center">Nama Barang</th>
								<th style="width:100px;" align="center">Motif</th>
								<th style="width:100px;" align="center">Harga</th>
								<th style="width:60px;" align="center">Jml Pesan</th>
								<th style="width:60px;" align="center">Jml Dlv</th>
							</table>
						</div>
						<div id="detailisi" align="center">
							<?php
							echo "<table class=\"listtable\" style=\"width:750px;\"";
							$i = 0;
							foreach ($detail as $row) {
								$i++;
								$harga	= number_format($row->v_unit_price, 2);
								$norder	= number_format($row->n_order, 0);
								$ndeliv	= number_format($row->n_deliver, 0);
								//					$ndeliv	=number_format($row->n_stock,0);

								//* TAMBAHAN 07 MAR 2022
								$vtot = round($row->v_unit_price * $row->n_deliver);
								$vdisc1 = round((($vtot * $isi->n_spb_discount1) / 100));
								$vdisc2 = round(((($vtot - $vdisc1) * $isi->n_spb_discount2) / 100));
								$vdisc3 = round(((($vtot - ($vdisc1 + $vdisc2)) * $isi->n_spb_discount3) / 100));
								$vdiscount = $vdisc1 + $vdisc2 + $vdisc3;

								$vnetto = round($vtot - $vdiscount, 0);
								$vppn = round($vnetto / $row->excl_divider * $row->n_tax_val, 0);
								$vdpp = $vnetto - $vppn;

								$vnetto = $vdpp + $vppn;
								// ****************
								echo "<tbody>
							<tr>
		    				<td width=\"23px\">
								<input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td width=\"64px\"><input style=\"width:64px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td width=\"302px\"><input style=\"width:302px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td width=\"98px\"><input style=\"width:98px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td width=\"100px\"><input style=\"text-align:right; width:100px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>
							<td width=\"60px\"><input style=\"text-align:right; width:60px;\"  type=\"text\" 
								id=\"norder$i\" readonly
								name=\"norder$i\" value=\"$norder\"></td>
							<td width=\"60px\"><input style=\"text-align:right; width:60px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\">
								<input type=\"hidden\" id=\"vdiscount$i\" name=\"vdiscount$i\" value=\"$vdiscount\">
								<input type=\"hidden\" id=\"vdpp$i\" name=\"vdpp$i\" value=\"$vdpp\">
								<input type=\"hidden\" id=\"vppn$i\" name=\"vppn$i\" value=\"$vppn\">
								<input type=\"hidden\" id=\"vnetto$i\" name=\"vnetto$i\" value=\"$vnetto\">
                <input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>
							</tr>
						  </tbody>";
							}
							echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
							?>
						</div>
</table>
</div>
</div>
</div>
<input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
												echo "value=\"$jmlitem\"";
											} else {
												echo "value=\"0\"";
											} ?>>
<?= form_close() ?>
<div id="pesan"></div>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
	function hitungnilai() {
		var nilaispb = document.getElementById("vspb").value.replace(/\,/g, '');
		var nilaidis = document.getElementById("vspbdiscounttotalafter").value.replace(/\,/g, '');
		if (!isNaN(nilaidis)) {
			if ((nilaispb - nilaidis) < 0) {
				alert("Nilai discount tidak valid !!!!!");
				document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0, input.value.length - 1);
			} else {
				document.getElementById("vspbafter").value = formatcemua(nilaispb - nilaidis);
			}
		} else {
			alert('input harus numerik !!!');
			document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0, input.value.length - 1);
		}
	}

	function hitungdiscount() {
		var nilaispb = document.getElementById("vspb").value.replace(/\,/g, ''); //    alert(vdistot);
		var nilaitot = document.getElementById("vspbafter").value.replace(/\,/g, '');
		if (!isNaN(nilaitot)) {
			if ((nilaispb - nilaitot) < 0) {
				alert("Nilai total tidak valid !!!!!");
				document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0, input.value.length - 1);
			} else {
				document.getElementById("vspbbersih").value = document.getElementById("vspbafter").value;
				document.getElementById("vspbdiscounttotal").value = formatcemua(nilaispb - nilaitot);
				document.getElementById("vspbdiscounttotalafter").value = formatcemua(nilaispb - nilaitot);
			}
		} else {
			alert('input harus numerik !!!');
			document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0, input.value.length - 1);
		}
	}

	$(document).ready(function() {
		editnilai();

		/* DISABLED SUBMIT SAAT ENTER KEYBOARD */
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		/* *********************************** */
	});

	function dipales() {
		if (document.getElementById("dnota").value != '') {

			// document.getElementById("login").hidden = true;

			setTimeout(() => {
				$("input").attr("disabled", true);
				$("select").attr("disabled", true);
				$("#submit").attr("disabled", true);
				document.getElementById("cmdreset").removeAttribute('disabled');
			}, 100);
		} else {
			document.getElementById("login").disabled = false;
			alert("Tanggal nota tidak boleh kosong");
		}
	}

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").hidden = false;
	}

	function back() {
		self.location = "<?php echo site_url() . '/nota/cform'; ?>";
	}

	function editnilai() {
		taxParse(document.getElementById("dsj").value);

		ndpp = exlValue
		nppn = taxValue

		jml = document.getElementById("jml").value;
		// f_pkp = document.getElementById("f_pkp").value;
		nsjdisc1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		nsjdisc2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		nsjdisc3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		nsjdisc4 = parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
		vsjdiscounttotal = parseFloat(formatulang(document.getElementById("vspbdiscounttotal").value))

		if (jml <= 0) {} else {
			salah = false;
			// kons    = document.getElementById("fspbconsigment").value;

			if (!salah) {
				vdisc1 = 0;
				vdisc2 = 0;
				vdisc3 = 0;
				vdisc4 = 0;
				vdpp = 0;
				vppn = 0;
				vnetto = 0;
				vtppn = 0;
				vtnetto = 0;
				vsjdisc1 = 0;
				vsjdisc2 = 0;
				vsjdisc3 = 0;
				vsjdisc4 = 0;
				gros = 0;
				grosgr = 0;

				for (i = 1; i <= jml; i++) {
					hrg = formatulang(document.getElementById("vproductretail" + i).value);
					hrggr = formatulang(document.getElementById("vproductretail" + i).value);
					qty = formatulang(document.getElementById("ndeliver" + i).value);
					if (isNaN(qty)) {
						qty = 0;
					}

					vhrg = parseFloat(hrg) * parseFloat(qty);
					vhrggr = parseFloat(hrggr) * parseFloat(qty);
					vdisc1 = Math.round(((vhrggr * nsjdisc1) / 100));
					vdisc2 = Math.round((((vhrggr - vdisc1) * nsjdisc2) / 100));
					vdisc3 = Math.round((((vhrggr - (vdisc1 + vdisc2)) * nsjdisc3) / 100));
					vdisc4 = Math.round((((vhrggr - (vdisc1 + vdisc2 + vdisc3)) * nsjdisc4) / 100));

					vsjdisc1 = vsjdisc1 + vdisc1;
					vsjdisc2 = vsjdisc2 + vdisc2;
					vsjdisc3 = vsjdisc3 + vdisc3;
					vsjdisc4 = vsjdisc4 + vdisc4;
					vdiscount = vdisc1 + vdisc2 + vdisc3 + vdisc4;
					gros = gros + vhrg;
					grosgr = grosgr + vhrggr;

					// Rumus ini supaya sewaktu export ke CSV utk EFaktur tidak selisih
					// if(f_pkp == 't'){
					vnetto = Math.round(vhrg - Math.round(vdiscount, 0), 0);
					vppn = Math.round(vnetto / ndpp * nppn, 0);
					vdpp = vnetto - vppn;
					// }else{
					// vdpp = Math.round(vhrg - Math.round(vdiscount, 0), 0);
					// vppn = 0;
					// }

					vnetto = vdpp + vppn;
					vtnetto = vtnetto + vnetto;
					vtppn = vtppn + vppn;

					document.getElementById("vdpp" + i).value = vdpp;
					document.getElementById("vppn" + i).value = vppn;
					document.getElementById("vdiscount" + i).value = vdiscount;
					document.getElementById("vnetto" + i).value = vnetto;
					document.getElementById("vtotal" + i).value = formatcemua(vhrg);
				} // END FOR ITEM
				document.getElementById("vnotagross").value = formatcemua(grosgr);
				document.getElementById("vnotappn").value = vtppn;
				if ((nsjdisc1 + nsjdisc2 + nsjdisc3 == 0) && (vsjdiscounttotal != 0)) {
					vsjdisc1 = vsjdiscounttotal;
				}
				vtot = 0;
				if (gros > 0) {
					document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
					document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
					document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
					document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);

					vdis1 = parseFloat(vsjdisc1);
					vdis2 = parseFloat(vsjdisc2);
					vdis3 = parseFloat(vsjdisc3);
					vdis4 = parseFloat(vsjdisc4);

					nTDisc1 = nsjdisc1 + nsjdisc2 * (100 - nsjdisc1) / 100;
					nTDisc2 = nsjdisc3 * (100 - nsjdisc3) / 100;
					nTDisc = nTDisc1 + nTDisc2 * (100 - nTDisc1) / 100;

					if ((nTDisc == 0) && (vsjdiscounttotal != 0)) {
						vtotdis = vsjdiscounttotal;
					} else {
						vtotdis = vsjdisc1 + vsjdisc2 + vsjdisc3 + vsjdisc4;
					}

					// // * HITUNG ULANG DPP PPN PER ITEM (DISC RP) 28 MAR 2022
					// var temp_cut = 0;
					// var nnominalcut = (vtotdis / gros * 100);

					//         for(var x=1; x<=jml; x++){
					//             if((formatulang(document.getElementById("ndeliver"+x).value) > 0)){
					//                 if(vtotdis > 0){
					//                     // $('#itemdiscountother' + x).val(Math.round(parseInt(formatulang(document.getElementById("vtotal"+x).value)) * nnominalcut / 100));
					//                     // temp_cut += parseInt($('#itemdiscountother' + x).val());
					//                     $('#vdiscount' + x).val(Math.round(parseInt(formatulang(document.getElementById("vtotal"+x).value)) * nnominalcut / 100));
					//                     temp_cut += parseInt($('#vdiscount' + x).val());
					//                 }
					//             } else {
					//                 document.getElementById("vdiscount" + x).value  = 0;
					//                 document.getElementById("vtotal" + x).value     = 0;
					//                 document.getElementById("vdpp" + x).value       = 0;
					//                 document.getElementById("vppn" + x).value       = 0;
					//                 document.getElementById("vnetto" + x).value     = 0;
					//             }
					//         }

					//         if((parseInt(temp_cut) - parseInt(vtotdis)) != 0 ){
					//             for(n=1; n <= jmlx; n++){
					//                 var checked = document.getElementById('chk' + n).checked;

					//                 if(checked == true){
					//                     jmlx = n;

					//                     // var thisvalue = document.getElementById('itemdiscountother' + n).value;
					//                     var thisvalue = document.getElementById('vdiscount' + n).value;

					//                     // document.getElementById('itemdiscountother' + n).value=(parseInt(thisvalue) - parseInt(temp_cut - vtotdis));                                
					//                     document.getElementById('vdiscount' + n).value=(parseInt(thisvalue) - parseInt(temp_cut - vtotdis));                                
					//                 }
					//             }
					//         }

					//         for(v=1;v<=jml;v++){
					//             if(document.getElementById("chk"+v).value=='on'){

					//                 vtotal2    = parseInt(formatulang(document.getElementById('vtotal' + v).value));
					//                 vdiscount2 = parseInt(document.getElementById('vdiscount' + v).value);

					//                 // Rumus ini supaya sewaktu export ke CSV utk EFaktur tidak selisih
					//                 vnetto2  = Math.round(vtotal2 - Math.round(vdiscount2, 0), 0);
					//                 vppn2    = Math.round(vnetto2 / ndpp * nppn, 0);
					//                 vdpp2    = vnetto2 - vppn2;
					//                 // console.log('DPP '+ document.getElementById('iproduct' + v).value + ":" + vdpp2);
					//                 // console.log('PPN '+ document.getElementById('iproduct' + v).value + ":" + vppn2);

					//                 vnetto2  = vdpp2 + vppn2;
					//                 vtnetto2 = vtnetto2 + vnetto2;
					//                 vtppn2   = vtppn2 + vppn2;

					//                 document.getElementById("vdpp" + v).value = vdpp2;
					//                 document.getElementById("vppn" + v).value = vppn2;
					//                 document.getElementById("vnetto" + v).value = vnetto2;
					//             } // END CEKLIS
					//         } // END FOR
					//     // * END HITUNG ULANG DPP PPN PER ITEM (DISC RP)

					document.getElementById("vspbdiscounttotalafter").value = formatcemua(vtotdis);
					document.getElementById("vspbafter").value = formatcemua(vtnetto);
					// document.getElementById("vsjnettoprint").value = formatcemua(vtnetto);
				} else {
					document.getElementById("vcustomerdiscount1").value = formatcemua(vsjdisc1);
					document.getElementById("vcustomerdiscount2").value = formatcemua(vsjdisc2);
					document.getElementById("vcustomerdiscount3").value = formatcemua(vsjdisc3);
					document.getElementById("vcustomerdiscount4").value = formatcemua(vsjdisc4);
					document.getElementById("vspbafter").value = 0;
					// document.getElementById("vsjnettoprint").value = formatcemua(vtnetto);
					if (document.getElementById("fspbconsigment").value == 'f') {
						document.getElementById("vspbdiscounttotalafter").value = 0;
					}
				}
			} // END != SALAH
		} // END ELSE
	}

	function editnilai_24032022() {
		var fppn = document.getElementById("fspbplusppn").value;
		var jml = parseFloat(document.getElementById("jml").value);
		var totdis = 0;
		var totnil = 0;
		var hrg = 0;
		var ndis1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
		var ndis2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
		var ndis3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
		var ndis4 = parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
		if ((ndis1 + ndis2 + ndis3 + ndis4 == 0)) {
			vdis1 = parseFloat(formatulang(document.getElementById("vspbdiscounttotalafter").value));
			document.getElementById("vspbdiscounttotal").value = document.getElementById("vspbdiscounttotalafter").value;
		} else {
			vdis1 = 0;
		}
		var vdis2 = 0;
		var vdis3 = 0;
		var vdis4 = 0;
		for (i = 1; i <= jml; i++) {
			//      if(fppn=='f'){
			//        var vhrg = parseFloat(formatulang(document.getElementById("vproductretail"+i).value))/1.1;
			//      }else{
			var vhrg = parseFloat(formatulang(document.getElementById("vproductretail" + i).value));
			//      }
			qty = formatulang(document.getElementById("ndeliver" + i).value);
			hrgtmp = parseFloat(vhrg) * parseFloat(qty);
			//      	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
			hrg = hrg + hrgtmp;
		}
		//  hrg=Math.round(hrg);
		if (ndis1 > 0) vdis1 = vdis1 + (hrg * ndis1) / 100; //Math.round(vdis1+(hrg*ndis1)/100);
		vdis2 = vdis2 + (((hrg - vdis1) * ndis2) / 100); //Math.round(vdis2+(((hrg-vdis1)*ndis2)/100));
		vdis3 = vdis3 + (((hrg - (vdis1 + vdis2)) * ndis3) / 100); //Math.round(vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100));
		vdis4 = vdis4 + (((hrg - (vdis1 + vdis2 + vdis3)) * ndis4) / 100); //Math.round(vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100));
		//    if(fppn=='t'){
		vdistot = Math.round(vdis1 + vdis2 + vdis3 + vdis4);
		//    }else{
		//      vdistot	= vdis1+vdis2+vdis3+vdis4;
		//    }

		nTDisc1 = ndis1 + ndis2 * (100 - ndis1) / 100;
		nTDisc2 = ndis3 + ndis4 * (100 - ndis3) / 100;
		nTDisc = nTDisc1 + nTDisc2 * (100 - nTDisc1) / 100;
		//    vdistot = Math.round(nTDisc * hrg / 100);
		//    vdistot = (nTDisc * hrg)/ 100;
		//    alert(vdistot);
		//    if( (fppn=='f') && (ndis1==0) ){
		//      vdistot=Math.round(vdistot/1.1);
		//    }    
		//    alert(vdistot);
		//    vhrgreal= hrg-vdistot;//Math.round(hrg-vdistot);
		document.getElementById("vcustomerdiscount1").value = formatcemua(Math.round(vdis1));
		document.getElementById("vcustomerdiscount2").value = formatcemua(Math.round(vdis2));
		document.getElementById("vcustomerdiscount3").value = formatcemua(Math.round(vdis3));
		document.getElementById("vcustomerdiscount4").value = formatcemua(Math.round(vdis4));
		if (document.getElementById("fspbconsigment").value == 'f') {
			//      if(fppn=='f'){
			////        vppn=(Math.round(hrg)-Math.round(vdistot))*0.1;
			//        vppn=Math.round((hrg-vdistot)*0.1);
			//        vtotbersih=Math.round(hrg)-Math.round(vdistot)+Math.round(vppn);
			//        vtotbersih=hrg-vdistot+vppn;
			//      }else{
			vtotbersih = parseFloat(hrg) - parseFloat(Math.round(vdistot));
			//      }
			document.getElementById("vspbdiscounttotalafter").value = formatcemua(Math.round(vdistot));
			//      document.getElementById("vspbafter").value=formatcemua(Math.round(vhrgreal));
			document.getElementById("vspbafter").value = formatcemua(Math.round(vtotbersih));
		}
		var fdis = document.getElementById("fspbplusdiscount").value;
		var bersih = vtotbersih; //parseFloat(formatulang(document.getElementById("vspbafter").value));
		var kotor = hrg; //parseFloat(formatulang(document.getElementById("vspb").value));
		if ((fppn == 't') && (fdis == 'f')) {
			document.getElementById("nprice").value = 1;
			document.getElementById("vnotappn").value = 0;
		} else if ((fppn == 't') && (fdis == 't')) {
			document.getElementById("nprice").value = bersih / kotor;
			document.getElementById("vnotappn").value = 0;
		} else if ((fppn == 'f') && (fdis == 't')) {
			document.getElementById("nprice").value = 1 / 1.1;
			kotorminppn = Math.round(hrg / 1.1);
			if (document.getElementById("fspbconsigment").value == 'f') {
				document.getElementById("vspbdiscounttotalafter").value = 0;
				document.getElementById("vnotappn").value = formatcemua(Math.round(vppn));
			}
		} else if ((fppn == 'f') && (fdis == 'f')) {
			document.getElementById("nprice").value = 1 / 1.1;
			if (document.getElementById("fspbconsigment").value == 'f') {
				//	      document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vdistot));
				//	      document.getElementById("vspbafter").value=formatcemua(Math.round(vtotbersih));
				document.getElementById("vnotappn").value = formatcemua(Math.round(vppn));
			} else {
				//        vdistot=vdistot*1.1;
				//        hrg=hrg*1.1;
				vppn = (Math.round(hrg) - Math.round(vdistot)) * 0.1;
				//        vtotbersih=Math.round(Math.round(hrg)-Math.round(vdistot)+vppn);
				vtotbersih = document.getElementById("vspbbersih").value;
				document.getElementById("vspbdiscounttotalafter").value = formatcemua(Math.round(vdistot));
				document.getElementById("vspbafter").value = formatcemua(vtotbersih);

			}
		}
	}

	function pricegroup() {
		nota = document.getElementById("inota").value;
		if (nota == '') nota = document.getElementById("ispb").value;
		area = document.getElementById("iarea").value;
		showModal("nota/cform/pricegroup/" + nota + "/" + area, "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function afterSetDateValue(ref_field, target_field, date) {
		if (date != "") {
			var startDate = document.getElementById('dnota').value;
			var endDate = document.getElementById('dtmp').value;
			tes = startDate.split('-');
			startDate = tes[2] + '-' + tes[1] + '-' + tes[0];
			tesx = endDate.split('-');
			endDate = tesx[2] + '-' + tesx[1] + '-' + tesx[0];
			if (startDate < endDate) {
				tes = endDate.split('-');
				dnota = tes[2] + '-' + tes[1] + '-' + tes[0];
				alert("Tidak boleh lebih kecil dari Tanggal DKB = " + dnota + " !!!");
				document.getElementById('dnota').value = dnota;
			} else if (tes[1] != tesx[1]) {
				//        tes=endDate.split('-');
				//        dnota=tes[2]+'-'+tes[1]+'-'+tes[0];
				//        alert("Harus dalam periode yang sama !!!");
				//        document.getElementById('dnota').value=dnota;
			}
		}
	}
</script>