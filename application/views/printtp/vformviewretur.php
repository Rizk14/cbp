<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css" media="all">
@media print {
   body {margin: 0;}
}
*{
size: landscape;
}
.pagebreak {
    page-break-before: always;
}
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
/*	width: 100%; */
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  cellspacing:0.1px;
}
/*
.garistd { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 12px;
  FONT-WEIGHT: normal; 
}
*/
.garisx{ 
	background-color:#FFFFFF;
	font-size: 12px;
  FONT-WEIGHT: normal; 
	border-right:#000000 0.1px solid;
	border-left:#000000 0.1px solid;
}
.garisy{ 
	background-color:#FFFFFF;
	font-size: 12px;
  FONT-WEIGHT: normal; 
	border-top:#000000 0.1px solid;
	border-bottom:#000000 0.1px solid;
}

.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.lapor {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
}
.eusi {
  font-size: 12px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'printtp/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
    $tglproses='';
    if($isi){
			foreach($isi as $row){
        $tglproses=$row->d_process;
      }
    }
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <table width="700px" border="0">
      <tr>
        <td><strong class="judul huruf"><?php echo NmPerusahaan; ?></strong></td>
      </tr>
      <tr>
        <td class="huruf lapor">Laporan Retur Penjualan Per Area Periode <?php echo $periode; ?></td>
      </tr>
      <tr>
        <td class="huruf isi">Tanggal proses : <?php echo $tglproses; ?></td>
      </tr>
    </table>
    	  <table border=0 class="garis huruf" width="700px">
        <tr>
	   	    <td align="center" class="garisx garisy huruf isi">No</th>
	   	    <td align="center" class="garisx garisy huruf isi">Area</th>
			    <td align="center" class="garisx garisy huruf isi">Penjualan</th>
			    <td align="center" class="garisx garisy huruf isi">Retur</th>
			    <td align="center" class="garisx garisy huruf isi">% Retur</th>
        </tr>
        
      
	      <?php 
		if($isi){
      $i=1;
      $tgross=0;
      $tretur=0;
      $tpersen=0;
			foreach($isi as $row){
        if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_retur_insentif==null || $row->v_retur_insentif=='')$row->v_retur_insentif=0;
        if($row->v_nota_grossinsentif!=0){
          $persenretur=number_format(($row->v_retur_insentif/$row->v_nota_grossinsentif)*100,2);
        }else{
          $persenretur='0.00';
        }
	      echo "<tr>
          <td align=right class=\"garisx huruf eusi\">$i</td>
          <td class=\"garisx huruf eusi\">$row->i_area-$row->e_area_name</td>
          <td align=right class=\"garisx huruf eusi\">".number_format($row->v_nota_grossinsentif)."</td>
			    <td align=right class=\"garisx huruf eusi\">".number_format($row->v_retur_insentif)."</td>
			    <td align=right class=\"garisx huruf eusi\">".$persenretur." %</td></tr>";
          $tretur=$tretur+$row->v_retur_insentif;
          $tgross=$tgross+$row->v_nota_grossinsentif;
#          $tpersen=$tpersen+$persenretur;
        $i++;
			}
      $i=$i-1;
      $tpersen=number_format(($tretur/$tgross)*100,2);
#      $tpersen=number_format($tpersen/$i,2);
      echo "<tr>
      <td align=right class=\"garisx garisy huruf eusi\"></td>
      <td class=\"garisx garisy huruf eusi\">Total</td>
      <td align=right class=\"garisx garisy huruf eusi\">".number_format($tgross)."</td>
      <td align=right class=\"garisx garisy huruf eusi\">".number_format($tretur)."</td>
	    <td align=right class=\"garisx garisy huruf eusi\">".$tpersen." %</td></tr>";
      $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
		}
	      ?>
	    </tbody>
	  </table>
    <table width="700px" border="0">
      <tr>
        <td class="huruf isi">Tanggal Cetak : <?php echo $tgl; ?></td>
      </tr>
	  </table>
<div class="noDisplay"><center><input name="cmdprint" id="cmdprint" value="Cetak" type="button" onclick='window.print()' ><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='xxx()' ></center></div>
</div>
<script language="javascript" type="text/javascript">
  function xxx(){
    this.close();
  }
</script>
</BODY>
</html>
