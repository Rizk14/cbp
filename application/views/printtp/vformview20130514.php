<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listtppersalesman/cform/view','update'=>'#main','type'=>'post'));?>
<!--	<?php echo form_open('listtppersalesman/cform/cari', array('id' => 'listform'));?> -->
	<div class="effect">
	  <div class="accordion2">
		<?php 
		if($isi){
			foreach($isi as $row){
			  $a=substr($row->i_periode,0,4);
			  $b=substr($row->i_periode,4,2);
			  if(!isset($per)) $per='';
		  	  if($row->i_periode!=$per){
					$periode=$row->i_periode;
					$per=$row->i_periode;
					$per=mbulan($b)." - $a";
			  }
//			  if(!isset($area)) $area='';
//		  	  if($row->i_area!=$area){
//					$area=$row->i_area."&nbsp;-&nbsp;".$row->e_area_name;
//			  }
			}
		}else{
//			$area=$iarea;
			$a=substr($iperiode,0,4);
			$b=substr($iperiode,4,2);
			$per=mbulan($b)." - $a";
		}
		?>
		  <table class="mastertable">
<!--			<tr><td style="width:100px;">Area</td><td style="width:5px;">:</td><td style="width:90px;"><?php echo $area; ?></td><td></td></tr>	-->
			<tr><td style="width:100px;">Periode</td><td style="width:5px;">:</td><td colspan=2><?php echo $per; ?></td><td></td></tr>
		  </table>
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>" ><input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Area</th>
			<th>salesman</th>
			<th>Target Salesman</th>
			<th>SPB bln ini</th>
			<th>% Sls vs Tgt</th>
			<th>% Ret vs Sls</th>
			<th>% Reguler</th>
			<th>% Baby</th>
	    <tbody>
	      <?php 
		if($isi){
			$ret=0;
			$bab=0;
			$reg=0;
			$target=0;
			$spb=0;
			foreach($isi as $row){
			  $ret=$row->v_retur_insentif+$row->v_retur_noninsentif;
			  $bab=$row->v_real_babynoninsentif+$row->v_real_babyinsentif;
			  $reg=$row->v_real_regularnoninsentif+$row->v_real_regularinsentif;
			  if($row->v_target>0){
				  $persenptt=($row->v_nota_netto/$row->v_target)*100;
				  $persenptt=number_format($persenptt,2);
				  $persenreg2=($reg/$row->v_target)*100;
				  $persenreg2=number_format($persenreg2,2);
				  $persenbab2=($bab/$row->v_target)*100;
				  $persenbab2=number_format($persenbab2,2);
	 	  	  }else{
				  $persenptt="0.00";
				  $persenreg2="0.00";
				  $persenbab2="0.00";
			  }
			  if($row->v_nota_netto>0){
				  $persenrtp=($ret/$row->v_nota_netto)*100;
				  $persenrtp=number_format($persenrtp,2);
				  $persenreg=($reg/$row->v_nota_netto)*100;
				  $persenreg=number_format($persenreg,2);
				  $persenbab=($bab/$row->v_nota_netto)*100;
				  $persenbab=number_format($persenbab,2);
			  }else{
				  $persenrtp="0.00";
				  $persenreg="0.00";
				  $persenbab="0.00";
			  }
			  $target=$target+$row->v_target;
			  $spb=$spb+$row->v_spb_gross;
#			  $spb=$spb+$row->v_spb_netto;
################################
/*
        $query=$this->db->query(" select sum(v_spb-v_spb_discounttotal) as nilaispb from tm_spb
                                  where to_char(d_spb,'yyyymm')='$periode'
                                  and i_area='$row->i_area' and i_salesman='$row->i_salesman'");
        if($query->num_rows()>0){
          foreach($query->result() as $data){
            $row->v_spb_netto=$data->nilaispb;
          }
        }else{
          $row->v_spb_netto=0;
        }
*/
        if(!is_numeric($row->v_target))settype($row->v_target,"float");
        if(!is_numeric($row->v_spb_gross))settype($row->v_spb_gross,"float");
#        if(!is_numeric($row->v_spb_netto))settype($row->v_spb_netto,"float");
################################
			  echo "<tr> 
				  <td valign=top style=\"font-size:12px;\">$row->e_area_name</td>
				  <td valign=top style=\"font-size:12px;\">($row->i_salesman) $row->e_salesman_name</td>
				  <td valign=top style=\"font-size:12px;\" align=right>Rp. ".number_format($row->v_target,2)."</td>
				  <td valign=top style=\"font-size:12px;\" align=right>Rp. ".number_format($row->v_spb_gross,2)."</td>
				  <td valign=top style=\"font-size:12px;\" align=right>$persenptt %</td>
				  <td valign=top style=\"font-size:12px;\" align=right>$persenrtp %</td>
				  <td valign=top style=\"font-size:12px;\" align=right>$persenreg % - $persenreg2 %</td>
				  <td valign=top style=\"font-size:12px;\" align=right>$persenbab % - $persenbab2 %</td></tr>";
			}
			  echo "<tr> 
				  <td colspan=2 align=center>TOTAL</td>
				  <td valign=top style=\"font-size:12px;\" align=right>Rp. ".number_format($target,2)."</td>
				  <td valign=top style=\"font-size:12px;\" align=right>Rp. ".number_format($spb,2)."</td>
				  <td colspan=4>&nbsp;</td></tr>";
						
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(a,c){
	document.getElementById("iperiode").value=a;
	document.getElementById("isalesman").value=c;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/listtppersalesman/cform/viewdetail";
	formna.submit();
  }
</script>
