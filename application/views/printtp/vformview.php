<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css" media="all">
@media print {
   body {margin: 0;}
}
*{
size: landscape;
}
.pagebreak {
    page-break-before: always;
}
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
/*	width: 100%; */
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  cellspacing:0.1px;
}
/*
.garistd { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 12px;
  FONT-WEIGHT: normal; 
}
*/
.garisx{ 
	background-color:#FFFFFF;
	font-size: 12px;
  FONT-WEIGHT: normal; 
	border-right:#000000 0.1px solid;
	border-left:#000000 0.1px solid;
}
.garisy{ 
	background-color:#FFFFFF;
	font-size: 12px;
  FONT-WEIGHT: normal; 
	border-top:#000000 0.1px solid;
	border-bottom:#000000 0.1px solid;
}

.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.lapor {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
}
.eusi {
  font-size: 12px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'printtp/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <table width="700px" border="0">
      <tr>
        <td class="judul huruf" colspan=2><strong><?php echo NmPerusahaan; ?></strong></td>
      </tr>
      <tr>
        <td class="huruf lapor" colspan=2>Laporan Realisasi Target Penjualan Per Area Periode <?php echo $periode; ?></td>
      </tr>
<?php 
/*
    echo "<center><h2>PT. DIALOGUE GARMINDO UTAMA</h2></center>";
		echo "<center><h3>Laporan Realisasi Target Penjualan Per Area</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
*/
    $tglproses='';
    if($isi){
			foreach($isi as $row){
        $tglproses=$row->d_process;
      }
    }
?>
      <tr>
        <td class="huruf isi">Tanggal proses : <?php echo $tglproses; ?></td>
        <td></td>
      </tr>
<?php 
#		echo "<center><h3>Tanggal proses : $tglproses</h3></center>";
?>
    </table>
    	  <table border=0 class="garis huruf" width="700px">
        <tr>
          <td align="center" class="garisx garisy huruf isi">No</td>
          <td align="center" class="garisx garisy huruf isi">Area</td>
          <td align="center" class="garisx garisy huruf isi">Target Penjualan</td>
          <td align="center" class="garisx garisy huruf isi">Realisasi Penjualan</td>
          <td align="center" class="garisx garisy huruf isi">% Realisasi Penjualan</td>
        </tr>
	      <?php 
		if($isi){
      $i=1;
      $ttarget=0;
      $tgross=0;
      $tpersen=0;
			foreach($isi as $row){
        if($row->v_nota_grossinsentif==null || $row->v_nota_grossinsentif=='')$row->v_nota_grossinsentif=0;
        if($row->v_target!=0){
          $persen=number_format(($row->v_nota_grossinsentif/$row->v_target)*100,2);
        }else{
          $persen='0.00';
        }
	      echo "<tr>
          <td align=right class=\"garisx huruf eusi\">$i</td>
          <td class=\"garisx huruf eusi\">$row->i_area-$row->e_area_name</td>
          <td align=right class=\"garisx huruf eusi\">".number_format($row->v_target)."</td>
          <td align=right class=\"garisx huruf eusi\">".number_format($row->v_nota_grossinsentif)."</td>
			    <td align=right class=\"garisx huruf eusi\">".$persen." %</td></tr>";
        $ttarget=$ttarget+$row->v_target;
        $tgross=$tgross+$row->v_nota_grossinsentif;

        $i++;
			}
      $i=$i-1;
      $tpersen=number_format(($tgross/$ttarget)*100,2);
#      $tpersen=number_format($tpersen/$i,2);
      echo "<tr>
      <td class=\"garisx garisy\"></td>
      <td class=\"garisx garisy huruf eusi\">Total</td>
      <td align=right class=\"garisx garisy huruf eusi\">".number_format($ttarget)."</td>
      <td align=right class=\"garisx garisy huruf eusi\">".number_format($tgross)."</td>
	    <td align=right class=\"garisx garisy huruf eusi\">".$tpersen." %</td></tr>";
		}
        $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
	      ?>
	  </table>
    <table width="700px" border="0">
      <tr>
        <td class="huruf eusi">Tanggal Cetak : <?php echo $tgl; ?></td>
      </tr>
	  </table>
<!--    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("printtp/cform/index","#main");' ></center>-->
        <div class="noDisplay"><center><input name="cmdprint" id="cmdprint" value="Cetak" type="button" onclick='window.print()' ><input name="cmdsales" id="cmdsales" value="per Sales" type="button" onclick="persales('<?php echo $iperiode; ?>')"><input name="cmdretur" id="cmdretur" value="Retur" type="button" onclick="retur('<?php echo $iperiode; ?>')"><input name="cmdretursales" id="cmdretursales" value="Retur per sales" type="button" onclick="retursales('<?php echo $iperiode; ?>')"><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='xxx()'></center></div>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/printtp/cform/viewdetail";
	  formna.submit();
  }
  function xxx(){
    this.close();
  }
  function persales(iperiode){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/printtp/cform/persales/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function pernota(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/printtp/cform/pernota/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function perkota(iperiode,area){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/printtp/cform/perkota/"+iperiode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function retur(iperiode){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/printtp/cform/retur/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function retursales(iperiode){
    lebar =1366;
    tinggi=768;
    eval('window.open("<?php echo site_url(); ?>"+"/printtp/cform/retursales/"+iperiode,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
// 44:25
</script>
</BODY>
</html>
