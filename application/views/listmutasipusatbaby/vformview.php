<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listmutasipusatbaby/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
		if($isi){
			foreach($isi as $row){
				$periode=$row->e_mutasi_periode;
			}
		}else{
			$periode=$iperiode;
		}
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <input name="pperiode" id="pperiode" value="<?php echo $row->e_mutasi_periode; ?>" type="hidden">
    <input name="cmdexport" id="cmdexport" value="Export to Excel" type="submit">
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>LAPORAN MUTASI STOCK PUSAT BABY - $periode</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
    $rpselisih=0;
    $rpsaldoakhir=0;
    $rpstockopname=0;
		if($isi){
			foreach($isi as $row){

        $this->db->select("	i_product, v_product_retail
                          from tr_product_price
						              where i_product='$row->i_product' and i_price_group='00'");
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $row->v_product_retail=$tmp->v_product_retail;
          }
        }else{
          $row->v_product_retail=0;
        }
        $rpselisih=$rpselisih+(($row->n_saldo_stockopname-$row->n_saldo_akhir)*$row->v_product_retail);
        $rpsaldoakhir=$rpsaldoakhir+($row->n_saldo_akhir*$row->v_product_retail);
        $rpstockopname=$rpstockopname+($row->n_saldo_stockopname*$row->v_product_retail);

      }
    }
		?>
<h3>Saldo Akhir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpsaldoakhir); ?><br>
    Saldo Stock Opname <?php echo 'Rp. '.number_format($rpstockopname); ?><br>
Selisih &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpselisih); ?><h3>

    	  <table class="listtable" border=none>
	   	  <tr>
	   	    <th align="center" rowspan="2">No</th>
	   	    <th align="center" rowspan="2">Kode</th>
	   	    <th align="center" rowspan="2">Nama</th>
			    <th align="center" rowspan="2">Saldo Awal</th>
			    <th align="center" colspan="2">Penerimaan</th>
			    <th align="center" colspan="2">Pengeluaran</th>
			    <th align="center" rowspan="2">Saldo Akhir</th>
			    <th align="center" rowspan="2">Stock Opname</th>
			    <th align="center" rowspan="2">Selisih</th>
        	<th class="action" rowspan="2">Action</th>
			  </tr>
			    <th align="center">Dari Gudang Pusat</th>
			    <th align="center">Dari Toko</th>
          <th align="center">Retur ke Gudang Pusat</th>
          <th align="center">Ke Toko</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
      $selisih=0;
      $tsaldoawal=0;
      $tbbm=0;
      $tdrtk=0;
      $tbbk=0;
      $tktk=0;
      $tsaldoakhir=0;
      $tso=0;
      $tselisih=0;
			foreach($isi as $row){
        $selisih=($row->n_saldo_stockopname)-$row->n_saldo_akhir;
		      echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_product</td>
            <td>$row->e_product_name</td>
				    <td align=right>$row->n_saldo_awal</td>
				    <td align=right>$row->n_mutasi_bbm</td>
				    <td align=right>$row->n_mutasi_daritoko</td>
				    <td align=right>$row->n_mutasi_bbk</td>
				    <td align=right>$row->n_mutasi_ketoko</td>
            <td align=right>$row->n_saldo_akhir</td>
				    <td align=right>$row->n_saldo_stockopname</td>
            <td align=right>$selisih</td>";
        $i++;
        $tsaldoawal=$tsaldoawal+$row->n_saldo_awal;
        $tbbm=$tbbm+$row->n_mutasi_bbm;
        $tdrtk=$tdrtk+$row->n_mutasi_daritoko;
        $tbbk=$tbbk+$row->n_mutasi_bbk;
        $tktk=$tktk+$row->n_mutasi_ketoko;
        $tsaldoakhir=$tsaldoakhir+$row->n_saldo_akhir;
        $tso=$tso+$row->n_saldo_stockopname;
        $tselisih=$tselisih+$selisih;

			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='show(\"listmutasipusatbaby/cform/detail/$iperiode/$row->i_product/$row->n_saldo_awal/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a></td>";
				echo "</tr>";	
			}
      echo "<tr>
      <td align=center colspan=3>Total</td>
	    <td align=right>$tsaldoawal</td>
	    <td align=right>$tbbm</td>
	    <td align=right>$tdrtk</td>
	    <td align=right>$tbbk</td>
	    <td align=right>$tktk</td>
      <td align=right>$tsaldoakhir</td>
	    <td align=right>$tso</td>
      <td align=right>$tselisih<td></td></tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasipusatbaby/cform/index","#main");' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listmutasipusatbaby/cform/viewdetail";
	  formna.submit();
  }
</script>
