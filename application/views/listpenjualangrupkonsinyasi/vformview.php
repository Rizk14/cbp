<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualangrupkonsinyasi/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>K-LANG GRUP</th>
		<th>NAMA LANG GRUP</th>
<?php 
    if($dfrom!=''){
		  $tmp=explode("-",$dfrom);
		  $blasal=$tmp[1];
      settype($bl,'integer');
	  }
    $bl=$blasal;
    for($i=1;$i<=$interval;$i++){
      switch ($bl){
      case '1' :
        echo '<th>Jan</th>';
        break;
      case '2' :
        echo '<th>Feb</th>';
        break;
      case '3' :
        echo '<th>Mar</th>';
        break;
      case '4' :
        echo '<th>Apr</th>';
        break;
      case '5' :
        echo '<th>Mei</th>';
        break;
      case '6' :
        echo '<th>Jun</th>';
        break;
      case '7' :
        echo '<th>Jul</th>';
        break;
      case '8' :
        echo '<th>Agu</th>';
        break;
      case '9' :
        echo '<th>Sep</th>';
        break;
      case '10' :
        echo '<th>Okt</th>';
        break;
      case '11' :
        echo '<th>Nov</th>';
        break;
      case '12' :
        echo '<th>Des</th>';
        break;
      }
      $bl++;
      if($bl==13)$bl=1;
    }
    echo '<th>Rata2</th>';
?>
      <tbody>
<?php 
			foreach($isi as $row){
        $rata=0;
  	    echo "<tr>
                <td>$row->kode</td>
                <td>$row->nama</td>";
        $bl=$blasal;
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' :
            $rata=$rata+$row->notajan;
            echo '<th align=right>'.number_format($row->notajan).'</th>';
            break;
          case '2' :
            $rata=$rata+$row->notafeb;
            echo '<th align=right>'.number_format($row->notafeb).'</th>';
            break;
          case '3' :
            $rata=$rata+$row->notamar;
            echo '<th align=right>'.number_format($row->notamar).'</th>';
            break;
          case '4' :
            $rata=$rata+$row->notaapr;
            echo '<th align=right>'.number_format($row->notaapr).'</th>';
            break;
          case '5' :
            $rata=$rata+$row->notamay;
            echo '<th align=right>'.number_format($row->notamay).'</th>';
            break;
          case '6' :
            $rata=$rata+$row->notajun;
            echo '<th align=right>'.number_format($row->notajun).'</th>';
            break;
          case '7' :
            $rata=$rata+$row->notajul;
            echo '<th align=right>'.number_format($row->notajul).'</th>';
            break;
          case '8' :
            $rata=$rata+$row->notaaug;
            echo '<th align=right>'.number_format($row->notaaug).'</th>';
            break;
          case '9' :
            $rata=$rata+$row->notasep;
            echo '<th align=right>'.number_format($row->notasep).'</th>';
            break;
          case '10' :
            $rata=$rata+$row->notaoct;
            echo '<th align=right>'.number_format($row->notaoct).'</th>';
            break;
          case '11' :
            $rata=$rata+$row->notanov;
            echo '<th align=right>'.number_format($row->notanov).'</th>';
            break;
          case '12' :
            $rata=$rata+$row->notades;
            echo '<th align=right>'.number_format($row->notades).'</th>';
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
        $rata=$rata/$interval;
        echo '<th align=right>'.number_format($rata).'</th>';
        echo "</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listpenjualangrupkonsinyasi/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
