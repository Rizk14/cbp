<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listumurpiutangnota/cform/export','update'=>'#main','type'=>'post'));
		$a=substr($iperiode,0,4);
	  $b=substr($iperiode,4,2);
		$periode=mbulan($b)." - ".$a;
	echo "<center><h2>LAPORAN UMUR PIUTANG PER NOTA PERIODE $periode </h2></center>";?>
<!--  <input name="cmdexport" id="cmdexport" value="Export To CSV" type="submit"> -->
  	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
          <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>">
	      </tr>
	    </thead>
	    <tr>
	    <th align="center">No</th>
 	    <th align="center">Area</th>
			<th align="center">Customer</th>
			<th align="center">TOP</th>
	    <th align="center">Nota</th>
 	    <th align="center">Tgl Nota</th>
			<th align="center">Jth Tempo<br>Plus Toleransi</th>
			<th align="center">Telat<br>(hari)</th>
			<th align="center">Kelompok</th>
			<th align="center">Keterangan</th>
			<th align="center" rowspan=2>Jml Saldo</th>
			</tr>
<!--			<th class="action">Action</th>-->
	    <tbody>
    <?php 
		if($isi){
		$i=0;
#		$j=0;
		$area='';
		$eareaname='';
		$total=0;
		$grandtotal=0;
		$jmltotal = count($isi);
			foreach($isi as $row){
          $i++;
    	    echo "<tr>";
			    echo "<td>$i</td>
				        <td>$row->e_area_name</td>
				        <td>$row->i_customer-$row->e_customer_name</td>
				        <td>$row->n_top</td>
				        <td>$row->i_nota</td>
				        <td>$row->d_nota</td>
				        <td>$row->d_jtempo_plustoleransi</td>
				        <td align='right'>$row->umurpiutang</td>
				        <td>$row->e_umur_piutangname</td>
				        <td>$row->ketsisa</td>
				        <td align='right'>".number_format($row->v_sisa)."</td>
				        </tr>
				        ";
				  $total = $total + $row->v_sisa;
          $grandtotal = $grandtotal+ $row->v_sisa;
				  $area=$row->i_area;
				  $eareaname = $row->e_area_name;
				if($i==$jmltotal){
    	    echo "<tr>";
			    echo "<th colspan=10>GRAND TOTAL</th>
				        <th align='right'>".number_format($grandtotal)."</th></tr>";				
				}
  	  }
	  }
    ?>
	    </tbody>
	  </table>
  	</div>
      </div>
      </div>	  
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
