<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$nor		= str_repeat(" ",5);
		$abn		= str_repeat(" ",12);
		$ab			= str_repeat(" ",9);
		$hal		= 1;
		$ipp    = new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$cetak.=CHR(18);
		
		$cetak.=CetakHeader($row,$ipp,$nor,$hal);
		$i		 = 0;
		$j		 = 0;
		$ap		 = $row->i_ap;
		$query = $this->db->query(" select * from tm_ap_item where i_ap='$ap'",false);
		$jml 	 = $query->num_rows();
		foreach($detail as $rowi){
			$i++;
			$j++;
			$nam="";
			if(strlen($i)==1) $no="  ".$i." ";
			if(strlen($i)==2) $no=" ".$i." ";
			if(strlen($i)==3) $no=$i." ";
			$pro	= $rowi->i_product;
			if(strlen($rowi->e_product_name )>37){
				$nam	= substr($rowi->e_product_name,0,37);
			}else{
				$nam	= $rowi->e_product_name;
				while(strlen($nam)<37){
					$nam	= $nam." ";
				}
			}
			if(strlen(number_format($rowi->n_receive))==1) $rec="       ".number_format($rowi->n_receive)." ";
			if(strlen(number_format($rowi->n_receive))==2) $rec="      ".number_format($rowi->n_receive)." ";
			if(strlen(number_format($rowi->n_receive))==3) $rec="     ".number_format($rowi->n_receive)." ";
			if(strlen(number_format($rowi->n_receive))==4) $rec="    ".number_format($rowi->n_receive)." ";
			if(strlen(number_format($rowi->n_receive))==5) $rec="   ".number_format($rowi->n_receive)." ";
			if(strlen(number_format($rowi->n_receive))==6) $rec="  ".number_format($rowi->n_receive)." ";
			$opew=" 000000     ";
			$cetak.=$nor.CHR(179).$no.CHR(179).$pro." ".$nam.CHR(179).$rec.CHR(179).$opew.CHR(179)."\n";
			
		}
		$cetak.=CetakFooter($ipp,$nor,$i);
#    echo $cetak;

#		$ipp->setBinary();
    $ipp->setdata($cetak);
    $ipp->printJob();
		echo "<script>this.close();</script>";
	}
	function CetakHeader($row,$ipp,$nor,$hal){
    $cetak='';
		$tmp=explode("-",$row->d_ap);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_ap=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		$cetak.=$nor.NmPerusahaan."\n";
		$cetak.=$nor.str_repeat(" ",18)."BUKTI BARANG MASUK            No. : ".$row->i_ap."\n";
		$cetak.=$nor.str_repeat(" ",18)."    ( B B M ) AP ".str_repeat(" ",13)."Tgl.: ".$row->d_ap."\n";
		$cetak.=$nor."Telah diterima dari : ".$row->i_supplier."-".$row->e_supplier_name."\n\n";
		$cetak.=$nor."Keterangan : \n\n";
		$cetak.=$nor.str_repeat(" ",68)."Hal: ".$hal."\n";
		$cetak.=$nor.CHR(218).str_repeat(CHR(196),4).CHR(194).str_repeat(CHR(196),45).CHR(194).str_repeat(CHR(196),9).CHR(194).str_repeat(CHR(196),12).CHR(191)."\n";
		$cetak.=$nor.CHR(179)."NO. ".CHR(179)." KODE                                        ".CHR(179)." JUMLAH  ".CHR(179)." KETERANGAN ".CHR(179)."\n";
		$cetak.=$nor.CHR(179)."URUT".CHR(179)." BARANG         N A M A   B A R A N G        ".CHR(179)."         ".CHR(179)."            ".CHR(179)."\n";
		$cetak.=$nor.CHR(198).str_repeat(CHR(205),4).CHR(216).str_repeat(CHR(205),45).CHR(216).str_repeat(CHR(205),9).CHR(216).str_repeat(CHR(205),12).CHR(181)."\n";
    return $cetak;
	}

	function CetakFooter($ipp,$nor,$i){
    $cetak='';
		$bar=$i+13;
		$cetak.=$nor.CHR(192).str_repeat(CHR(196),4).CHR(193).str_repeat(CHR(196),45).CHR(193).str_repeat(CHR(196),9).CHR(193).str_repeat(CHR(196),12).CHR(217)."\n";
		$cetak.=$nor."    Mengetahui                Yang menerima               Yang menyerahkan \n\n\n\n\n";
		$cetak.=$nor."(                )          (                )           (                )\n";
		$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
		$cetak.=$nor.CHR(15)."TANGGAL CETAK : ".$tgl.CHR(18);
		$total=$bar+8;
		while($total<35){
			$cetak.="\n";
			$total++;
		}
    return $cetak;
	}
?>
