<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'exp-notapb/cform/export', 'update' => '#pesan', 'type' => 'post')); ?>
			<div class="expkkform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="9%">Tgl Awal</td>
								<td width="1%">:</td>
								<td width="90%"><input readonly id="datefrom" name="datefrom" value="<?= date('01-m-Y') ?>" onclick="showCalendar('',this,this,'','datefrom',0,20,1)"></td>
							</tr>
							<tr>
								<td width="9%">Tgl Akhir</td>
								<td width="1%">:</td>
								<td width="90%"><input readonly id="dateto" name="dateto" value="<?= date('d-m-Y') ?>" onclick="showCalendar('',this,this,'','dateto',0,20,1)"></td>
							</tr>
							<tr>
								<td width="9%">Pelanggan</td>
								<td width="1%">:</td>
								<td width="90%">
									<input type="hidden" id="icustomer" name="icustomer" value="">
									<input type="text" id="ecustname" name="ecustname" value="" onclick='showModal("exp-notapb/cform/customer/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							</tr>
							<tr>
								<td width="9%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="90%">
									<input name="login" id="login" value="Export to Excel" type="submit">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-notapb/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
			<div id="pesan"></div>
		</td>
	</tr>
</table>