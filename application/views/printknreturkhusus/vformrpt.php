<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
@media print {
input.noPrint { display: none; }
}
@page
        {
            size: auto;   /* auto is the initial value 
            margin: 0mm;   this affects the margin in the printer settings 
        */
*{
size: landscape;
}

@page { size: Letter; 
        margin: 0mm;0mm;0mm;10mm;  /* this affects the margin in the printer settings */
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.wrap {
	margin: 0 auto;
	text-align: left;
}

.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
 border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.catatan {
  font-size: 14px;
  FONT-WEIGHT: normal; 
}
.nmper {
	margin-top: 0;
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 12px;
  font-weight:normal;
}
.eusinya {
  font-size: 8px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.garisatas { 
	border-top:#000000 0.1px solid;
}
.gariskiri { 
	border-left:#000000 0.1px solid;
}
.gariskanan { 
	border-right:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
?>
  <table width="100%" class="nmper" border="0">
    <tr>
      <td colspan="3" ><?php echo NmPerusahaan; ?></td>
      <td >Tgl. : <?php 
		    $tmp=explode("-",$row->d_kn);
		    $th=$tmp[0];
		    $bl=$tmp[1];
		    $hr=$tmp[2];
		    $dkn=$hr." ".substr(mbulan($bl),0,3)." ".$th;
		    echo $dkn;
      ?></td>
    </tr>
    <tr>
      <td colspan="3" class="huruf judul">K R E D I T &nbsp;&nbsp; N O T A</td>
      <td>Kepada Yth.</td>
    </tr>
    <tr>
      <td colspan="3">No. : <?php echo $ikn;?></td>
      <td><?php echo $row->i_customer."-".$row->e_customer_name;?></td>
    </tr>
    <tr>
      <td colspan="3">KS. : <?php echo $row->i_salesman."  ".$row->e_salesman_name;?></td>
      <td><?php echo $row->e_customer_address." ".$row->e_customer_city;?></td>
    </tr>
    <tr>
    <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">
        <table width="100%" class="nmper" border="0">
          <tr>
            <td class="gariskiri garisatas"><?php echo "Telah kami Kredit Rekening Saudara atas";?>
            </td>
            <td width="10px" class="gariskiri garisatas">&nbsp;</td>
            <td class="gariskanan garisatas">&nbsp;</td>
          </tr>
          <tr>
            <td class="gariskiri"><?php echo "Sesuai dengan BBM Nomor ".$row->i_refference;?>
            </td>
            <td width="10px" class="gariskiri" >&nbsp;</td>
            <td class="gariskanan ">&nbsp;</td>
          </tr>
          <tr>
            <td class="gariskiri"><?php echo "Tgl. ".$dkn;?>
            </td>
            <td width="10px" class="gariskiri" >Rp.</td>
            <td width="120px" align="right" class="gariskanan">
              <?php echo number_format($row->v_netto);?>
            </td>          </tr>
          <tr>
            <td class="gariskiri">&nbsp;</td>
            <td width="10px" class="gariskiri" >&nbsp;</td>
            <td class="gariskanan">&nbsp;</td>
          </tr>
          <tr>
            <td class="garisbawah gariskiri">&nbsp;</td>
            <td class="gariskiri garisbawah" width="10px">&nbsp;</td>
            <td class="gariskanan garisbawah">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td width="10px"class="gariskiri garisbawah" >Rp.</td>
            <td width="120px" align="right" class="gariskanan garisbawah">
              <?php echo number_format($row->v_netto);?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <table width="100%" class="nmper" border="0">
          <tr>
            <td colspan="3">Terbilang : <?php $bilangan = new Terbilang;
            $kata=ucwords($bilangan->eja($row->v_netto));	
            echo $kata.' RUPIAH'?>            </td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td width="500px" align="center">
              Mengetahui
            </td>
            <td>&nbsp;</td>
            <td  width="500px" colspan="2" align="center">
              Hormat Kami,
            </td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="200px" align="center">(&nbsp;&nbsp;&nbsp;&nbsp; Budi Ayu Ristiani &nbsp;&nbsp;&nbsp;&nbsp;)</td>
            <td>&nbsp;</td>
            <td align="center" width="200px"  colspan="2">(&nbsp;&nbsp;&nbsp;&nbsp; Admin AR &nbsp;&nbsp;&nbsp;&nbsp;)</td>
          </tr>
          <tr>
            <td colspan="3">TANGGAL CETAK : <?php $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    echo $tgl; ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
</table >
<?php 
    $bbm	= $this->mmaster->bacabbm($row->i_refference);
		foreach($bbm as $rowbbm){?>
  <table width="100%" class="nmper" border="0">
    <tr>
      <td colspan="2"><?php echo NmPerusahaan; ?></td>
    </tr>
    <tr>
      <td>BUKTI BARANG MASUK</td>
      <td>No : <?php echo $row->i_refference;?></td>
    </tr>    
    <tr>
      <td>( B B M ) - RETUR</td>
      <td>Tgl : <?php 
		      $tmp=explode("-",$row->d_refference);
		      $th=$tmp[0];
		      $bl=$tmp[1];
		      $hr=$tmp[2];
		      $drefference=$hr." ".substr(mbulan($bl),0,3)." ".$th;
         echo $drefference;?></td>
    </tr>
    <tr>
      <td colspan="2">Telah diterima dari : <?php echo $row->e_customer_name;?></td>
    </tr>    
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">Referensi : <?php echo $ikn;?></td>
    </tr>
    <tr>
      <td colspan="2">Keterangan : <?php echo $rowbbm->i_refference_document;?></td>
    </tr>
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr >
      <td colspan="2">
        <table width="100%" class="nmper" border="0">
          <tr>
            <td width="50px" class="gariskiri garisatas garisbawah">
              NO. URUT
            </td>
            <td width="75px" class="gariskiri garisatas garisbawah">
              KODE BARANG
            </td>
            <td width="350px" class="garisatas garisbawah">
              NAMA BARANG
            </td>
            <td width="75px"class="gariskiri garisatas garisbawah">
              UNIT
            </td>
            <td width="75px" class="gariskiri garisatas garisbawah">
              HARGA (PC)
            </td>
            <td colspan="2" width="100px" class="gariskiri gariskanan garisatas garisbawah">
              NILAI
            </td>
          </tr>
          <?php 
            $bbmi	= $this->mmaster->bacadetailbbm($rowbbm->i_bbm);
            $i=0;
            $totsub=0;
        	foreach($bbmi as $rowbbmi){
			    $i++;
#			    $hrg	= $hrg+($rowi->n_order*$rowi->v_product_mill);
			      ?>
          <tr>
            <td width="25" class="gariskiri">
              <?php echo $i;?>
            </td>
            <td class="gariskiri">
              <?php echo $rowbbmi->i_product;?>
            </td>
            <td>
              <?php 
			        if(strlen($rowbbmi->e_product_name )>50){
				        $nam	= substr($rowbbmi->e_product_name,0,50);
			        }else{
				        $nam	= $rowbbmi->e_product_name.str_repeat(" ",50-strlen($rowbbmi->e_product_name ));
			        }
              echo $nam;?>
            </td>
            <td class="gariskiri">
              <?php echo number_format($rowbbmi->n_quantity);?>
            </td>
            <td class="gariskiri" width="75px">
              <?php echo number_format($rowbbmi->v_unit_price);?>
            </td>
            <td align="right" colspan="2" class="gariskiri gariskanan">
              <?php $sub=$rowbbmi->n_quantity*$rowbbmi->v_unit_price;
                  echo number_format($sub);
                  $totsub=$totsub+$sub;
              ?>
            </td>
          </tr>
          <?php }?>
          <tr>
            <td colspan="3" class="garisatas">&nbsp;</td>
            <td colspan="2" class="garisatas">Jumlah</td>
            <td width="10px" class="garisatas gariskiri">Rp.</td>
            <td width="70px" class="garisatas gariskanan" align="right"><?php echo number_format($totsub);?></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">Potongan</td>
            <td width="10px" class="gariskiri">Rp.</td>
            <td width="70px" class="gariskanan" align="right"><?php echo number_format($row->v_discount);?></td>
          </tr>
          <tr>
            <td colspan="3">&nbsp;</td>
            <td colspan="2">Jumlah Bersih</td>
            <td width="10px" class="gariskiri garisatas garisbawah">Rp.</td>
            <td width="70px" align="right" class="garisatas gariskanan garisbawah"><?php echo number_format($row->v_netto);?></td>
          </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" class="nmper" border="0">
          <tr>
            <td width="500px" align="center">
              Mengetahui
            </td>
            <td>&nbsp;</td>
            <td  width="500px" colspan="2" align="center">
              Hormat Kami,
            </td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr align="center">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

                  </tr>
                  <tr>
                    <td width="200px" align="center">(&nbsp;&nbsp;&nbsp;&nbsp; Budi Ayu Ristiani &nbsp;&nbsp;&nbsp;&nbsp;)</td>
                    <td>&nbsp;</td>
                    <td width="200px"  align="center" colspan="2">(&nbsp;&nbsp;&nbsp;&nbsp; Admin AR &nbsp;&nbsp;&nbsp;&nbsp;)</td>
                  </tr>
                  <tr>
                    <td colspan="3">TANGGAL CETAK : <?php $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
            echo $tgl; ?></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
        </table >
      <td>
    </tr>
</table >
<?php    
    }
  }
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
