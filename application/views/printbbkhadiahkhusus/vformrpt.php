<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: landscape;
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{
	background-color:#f0f0f0;
	border-bottom:#000000 1px solid;
	border-top:#000000 1px solid;
	border-left:#000000 1px solid;
	border-right:#000000 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garistd {
	border-bottom:0.01px solid;
	border-left:0.01px solid;
	font-size: 10px;
}
.garistd td { 
	border-bottom:0.01px;
	border-left:0.01px;
	border-right:0.01px solid;
	border-top:0.01px solid;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.ici {
  font-size: 12px;
  font-weight:normal;
}
.kecil {
  font-size: 10px;
  font-weight:normal;
}
.garisbawah { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
?>
  <table width="100%" class="eusinya">
    <tr>
      <td colspan="3" class="huruf isi" ><?php echo NmPerusahaan; ?></td>
      <td width="26">&nbsp;</td>
      <td width="86">&nbsp;</td>
      <td width="36">&nbsp;</td>
      <td width="354" class="huruf isi">&nbsp;</td>
      <td width="87">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" class="huruf isi">BUKTI BARANG KELUAR</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf isi"><?php echo "No.  : ".$row->i_bbk; ?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" width="53" class="huruf isi">( B B K )</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf isi">Tgl : <?php $tmp=explode("-",$row->d_bbk);
          $th=$tmp[0];
          $bl=$tmp[1];
          $hr=$tmp[2];
          $dbbk=$hr." ".mbulan($bl)." ".$th;
          echo $dbbk;?></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" class="huruf isi">Perwakilan : GD-PST</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf isi">Kepada Yth.</td>
    </tr>
    <tr>
      <td colspan="3" class="huruf isi">Referensi : <?php echo $row->e_remark."/".$row->i_area;?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td class="huruf isi"><?php echo $row->e_customer_name;?></td>
    </tr>
  </table>
  <table width="100%" class="garistd">
    <tr bordercolor="1">
      <td class="huruf ici" width="24">NO.<br>URUT</td>
      <td class="huruf ici" width="58">KODE<br>BARANG</td>
      <td class="huruf ici" width="400" align=center>N A M A  B A R A N G</td>
      <td class="huruf ici" align=center width="90">JUMLAH</td>
      <td class="huruf ici" align=center width="100">KETERANGAN</td>
    </tr>
<?php 
  $i	= 0;
	foreach($detail as $rowi){
      $i++;
?>
    <tr>
      <td height="1" width="3%" class="huruf ici" align=center><?php echo $i; ?></td>
      <td height="1" width="12%" class="huruf ici">&nbsp;&nbsp;<?php echo $rowi->i_product; ?></td>
      <td height="1" width="57%" class="huruf ici">&nbsp;&nbsp;<?php echo $rowi->e_product_name; ?></td>
      <td height="1" align=center width="7%" class="huruf ici"><?php echo number_format($rowi->n_quantity);; ?></td>
      <td height="1" align=right width="9%" class="huruf ici"><?php echo $rowi->e_remark; ?></td>
    </tr>
<?php 
  }
?>
  </table>
  <table width="100%" border="0">
    <tr><td colspan="8">&nbsp;</td></tr>
    <tr>
      <td colspan="2" align=center class="huruf ici">Penerima</td>
      <td colspan="2" align=center class="huruf ici">Pembawa</td>
      <td colspan="2" align=center class="huruf ici">Yang Menyetujui</td>
      <td colspan="2" align=center class="huruf ici">Kepala Gudang</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
    </tr>
    <?php 
    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
    ?>
    <tr>
      <td colspan="8" class="huruf kecil">Tanggal cetak : <?php echo $tgl;?></td>
    </tr>
  </table>
<?php 
}
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
