<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyisland_new/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $tahun; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totvnota=0;
            $totqnota=0;
            foreach($isi as $ro){
              $totvnota=$totvnota+$ro->vnota;
              $totqnota=$totqnota+$ro->qnota;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan="2">Island</th>
         <th colspan="3">OA</th>
         <th colspan="3">Sales Qty(Unit)</th>
         <th colspan="3">Net Sales(Rp.)</th>
         <th rowspan="2">%Ctr Net Sales(Rp.)</th>
         </tr>
         <tr>
            <?php $prevth= $tahun-1; ?>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth OA</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Qty</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Rp</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $totpersenvnota=0;
        foreach($isi as $row){
          
          if($totvnota==0){
            $persenvnota=0;
          }else{
            $persenvnota=($row->vnota/$totvnota)*100;
          }
          $totpersenvnota=$totpersenvnota+$persenvnota;

          echo "<tr>
              <td style='font-size:12px;'>$row->e_area_island</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevoa)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevqnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevvnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($persenvnota,2)." %</td>
              </tr>";
         }
        echo "<tr>
        <td style='font-size:12px;' ><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totpersenvnota)." %</b></td>
        </tr>";
      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
<?php echo $graph ; ?>
