<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'bapbsjp/cform/carisj/'.$baris.'/'.$area,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $area;?>"></td>
	      </tr>
	    </thead>
      	<th align="center">No SJ</th>
	    	<th align="center">Tgl SJ</th>
	    	<th align="center">Nilai</th>
      	<th align="center">SJ Lama</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $nilai=number_format($row->v_sjp);
			  $tmp=explode('-',$row->d_sjp);
			  $tgl=$tmp[2];
			  $bln=$tmp[1];
			  $thn=$tmp[0];
			  $row->d_sjp=$tgl.'-'.$bln.'-'.$thn;
			  echo "<tr>
				  <td><a href=\"javascript:setValue('$row->i_sjp','$row->d_sjp',$baris,'$row->v_sjp')\">$row->i_sjp</a></td>
				  <td><a href=\"javascript:setValue('$row->i_sjp','$row->d_sjp',$baris,'$row->v_sjp')\">$row->d_sjp</a></td>
				  <td align=right><a href=\"javascript:setValue('$row->i_sjp','$row->d_sjp',$baris,'$row->v_sjp')\">$nilai</a></td>
				  <td><a href=\"javascript:setValue('$row->i_sjp','$row->d_sjp',$baris,'$row->v_sjp')\">$row->i_sjp_old</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,d,e)
  {
    ada=false;
    for(i=1;i<=d;i++){
			if(
				(a==document.getElementById("isj"+i).value) && 
				(i!==d) && 
				(b==document.getElementById("dsj"+i).value)
				){
				alert ("SJ : "+a+" sudah ada !!!!!");
				ada=true;
				break;
			}else{
				ada=false;	   
			}
    }
    if(!ada){
			document.getElementById("isj"+d).value=a;
			document.getElementById("dsj"+d).value=b;
			document.getElementById("vsj"+d).value=formatcemua(e);
      x=0;
      for(i=1;i<=d;i++){
        x=x+parseFloat(formatulang(document.getElementById("vsj"+i).value));
      }
			document.getElementById("vbapb").value=formatcemua(x);
			jsDlgHide("#konten *", "#fade", "#light");
    }
  }
  function bbatal(){
		baris		= document.getElementById("jml").value;
		si_inner= document.getElementById("detailisi").innerHTML;
		var temp= new Array();
		temp	= si_inner.split('<tbody disabled="disabled">');
		if( (document.getElementById("isj"+baris).value=='')){
			si_inner='';
			for(x=1;x<baris;x++){
				si_inner=si_inner+'<tbody>'+temp[x];
			}
			j=0;
			var barbar	=Array();
			var isj			= Array();
			var dsj			= Array();
			var vsj		  = Array();
			var eremark	= Array();
			for(i=1;i<baris;i++){
				j++;
				barbar[j]	= document.getElementById("baris"+i).value;
				isj[j]		= document.getElementById("isj"+i).value;
				dsj[j]		= document.getElementById("dsj"+i).value;
				vsj[j]		= document.getElementById("vsj"+i).value;
				eremark[j]	= document.getElementById("eremark"+i).value;	
			}
			document.getElementById("detailisi").innerHTML=si_inner;
			j=0;
			for(i=1;i<baris;i++){
				j++;
				document.getElementById("baris"+i).value=barbar[j];
				document.getElementById("isj"+i).value=isj[j];
				document.getElementById("dsj"+i).value=dsj[j];
        document.getElementById("vsj"+i).value=formatcemua(vsj[j]);
				document.getElementById("eremark"+i).value=eremark[j];
			}
			document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;
			jsDlgHide("#konten *", "#fade", "#light");
		}
  }
</script>
