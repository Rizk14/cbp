<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbybrand_new/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">

            <h2>Sales Performa By Brand</h2>

         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $dfrom." s/d ". $dto ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
         
        </table>
        <?php 
          if($isi){
            $totvnota=0;
            $totqnota=0;
            foreach($isi as $ro){
              $totvnota=$totvnota+$ro->vnota;
              $totqnota=$totqnota+$ro->qnota;
            }
          }
        ?>
        <table class="listtable">
         <tr>
          <td>
            <?php 
              if($ob){
                foreach ($ob as $riw) {
                  echo "<h1>Total OB : ".number_format($riw->ob)."</h1>";
                }
              }
            ?>
          </td>
         </tr>
       </table>
        <table class="listtable">
         <tr>
         <th rowspan="2">Brand</th>
         <!--<th rowspan="2">Tot Sales</th>-->
         <!--<th rowspan="2">OB</th>-->
         <th colspan="3">OA</th>
         <th colspan="3">Sales Qty(Unit)</th>
         <th colspan="3">Net Sales (Rp.)</th>
         <th rowspan="2">% Ctr Net Sales(Rp.)</th>
         </tr>
         <tr>
          <?php $prevtahun = $tahun-1 ?>
            <th><?php echo $prevtahun ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth OA</th>
            <th><?php echo $prevtahun ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Qty</th>
            <th><?php echo $prevtahun ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Rp</th>
         </tr>

       <tbody>
         <?php 
      if($isi){
        $totpersenvnota=0;
        $grwoa=0;
        $grwqty=0;
        $grwrp=0;
        $totrp=0;
        $ctrrp=0;
        $totnota=0;
        $totoaprev=0;
        $totoa=0;
        $totgrwoa=0;
        $totgrwqty=0;
        $totgrwrp=0;
        $totoaprev=0;
        $totob=0;
        $totqnotaprev=0;
        $totvnotaprev=0;
        $totctrrp=0;
        $totsales=0;
        foreach($isi as $row){
            $totnota+=$row->vnota;
        }

        $totrp=$totnota;

        foreach($isi as $row){
          
          if($totvnota==0){
            $persenvnota=0;
          }else{
            $persenvnota=($row->vnota/$totvnota)*100;
          }
          $totpersenvnota=$totpersenvnota+$persenvnota;

          
          if ($row->oaprev == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->oaprev)/$row->oaprev)*100;
          }

          if ($row->qnotaprev == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->qnota-$row->qnotaprev)/$row->qnotaprev)*100;
          }

          if ($row->vnotaprev == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->vnota-$row->vnotaprev)/$row->vnotaprev)*100;
          }

          
          $ctrrp= ($row->vnota/$totrp)*100;
          $totoaprev=$totoaprev+$row->oaprev;
          $totsales=$totsales+$row->totsales;
          $totob=$totob+$row->ob;
          $totoa=$totoa+$row->oa;
          $totqnotaprev= $totqnotaprev+$row->qnotaprev;
          $totvnotaprev= $totvnotaprev+$row->vnotaprev;
          $totctrrp=$totctrrp+$ctrrp;

          echo "<tr>
              <td style='font-size:12px;'>$row->group</td>
              <td style='font-size:12px;' align=right>".number_format($row->oaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwoa,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwqty,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnotaprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwrp,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
              //<td style='font-size:12px;' align=right>".number_format($row->ob)."</td>
              //<td style='font-size:12px;' align=right>".number_format($row->totsales)."</td>
         }

         if ($totoaprev == 0) {
              $totgrwoa = 0;
          } else { //jika pembagi tidak 0
              $totgrwoa = (($totoa-$totoaprev)/$totoaprev)*100;
          }

          if ($totqnotaprev == 0) {
              $totgrwqty = 0;
          } else { //jika pembagi tidak 0
              $totgrwqty = (($totqnota-$totqnotaprev)/$totqnotaprev)*100;
          }

          if ($totvnotaprev == 0) {
              $totgrwrp = 0;
          } else { //jika pembagi tidak 0
              $totgrwrp = (($totvnota-$totvnotaprev)/$totvnotaprev)*100;
          }

        echo "<tr>
        <td style='font-size:12px;' ><b>Total</b></td>

        
        <td style='font-size:12px;' align=right><b>".number_format($totoaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwoa,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnotaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwqty,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnotaprev)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwrp,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>";
        //<td style='font-size:12px;' align=right><b>".number_format($totob)."</b></td>
        //<td style='font-size:12px;' align=right><b>".number_format($totsales)."</b></td>
      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?php echo form_close()?>
    </td>
  </tr>
</table>
<?php echo $graph ; ?>
<!--<select name="selfile" id="selfile" onchange="pilih()">
  <?php 
/*
    for($i=0;$i<count($eusi);$i++)
    {
      $esi=str_replace(".","tandatitik",$eusi[$i]);
      echo '<option label="'.$eusi[$i].'">'.$eusi[$i].'</option>';
    }
*/
  ?>
</select>
<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("<?php echo $modul; ?>/cform/view/<?php echo $tahun; ?>","#main")'>
</form>
<script language="javascript">
/*
	function pilih(){
		tipe=document.getElementById("selfile").value;
    a="<?php echo $modul; ?>/cform/view/"+<?php echo $tahun; ?>+"/"+tipe+"/";
    show(a,"#main");
	}
*/
</script>
-->
