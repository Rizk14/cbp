<table class="maintable">
	<tr>
		<td align="left">
			<div class="effect">
				<div class="accordion2">
					<center>
					<?php echo form_open('transspblama/cform/load', array('name' => 'transdofileform', 'id' => 'transdofileform', 'onsubmit' => 'sendRequest(); return false'));?>
					<table class="mastertable">
            <tr>
							<td style="width:140px; font-size:15px;">Periode : </td>
							<td style="width:350px;"><input id="bulan" style="width:20px;" name="bulan" type="text" onkeyup='buatperiode()'> (bulan)&nbsp;&nbsp;&nbsp;<input id="tahun" style="width:40px;" name="tahun" type="text" onkeyup='buatperiode()'> (tahun)<input id="periode" name="periode" type="hidden">
							</td>
							<td style="width:100px;">&nbsp;</td>
						</tr>
            <tr>
							<td style="width:140px; font-size:15px;">Ada Pelanggan Baru : </td>
							<td style="width:350px;"><input type='checkbox' id='baru' name='baru' value="" onclick='isbaru();'></td>
							<td style="width:100px;">&nbsp;</td>
						</tr>
						<tr>
							<td style="width:140px; font-size:15px;">Pilih file yang akan di transfer : </td>
							<td style="width:350px;">
								<select name="selfile" id="selfile" onclick="pilih()">
                  <?php 
	                  for($i=0;$i<count($isi);$i++)
	                  {
                      $ext=substr($isi[$i],strlen($isi[$i])-3,3);
                      $nam=substr($isi[$i],0,5);
		                  if( (strtoupper($ext)=='DBF') && (strtoupper($nam)=='FDSPB') ){
			                  echo '<option label="'.$isi[$i].'">'.$isi[$i].'</option>';
		                  }
	                  }
                  ?>
								</select>
								<input type="text" id="namafile" name="namafile" value="">
								<input type="submit" id="cmdselfile" name="cmdselfile" value="Lihat Detail">
							</td>
							<td style="width:100px;">&nbsp;</td>
						</tr>
					</table>
					<?=form_close()?> 
					</center>
					<div id="pesan"></div>
					<div id="hasil"></div>
				</div>
			</div>
		</td>
	</tr>
</table>
<script language="javascript">
	function pilih(){
		document.getElementById("namafile").value=document.getElementById("selfile").value
	}
	function pilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=true;
			document.getElementById("chk"+i).value='on';
		}
	}
	function tidakpilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=false;
			document.getElementById("chk"+i).value='';
		}
	}
	function batal(){
		document.getElementById("pesan").innerHTML="";
		show("transspblama/cform/","#main");
	}
	function transfer(){
		var jml=document.getElementById("jml").value;
		var ada=false;
		for(i=1;i<jml;i++){
			if(document.getElementById("chk"+i).checked){	
				ada=true;
			}
		}
		if(ada){
			var formna=document.getElementById("transdoform");
			formna.onsubmit=sendRequestx();
			formna.submit();
		}else{
			alert("Pilih dulu SPB yang akan di transfer !!!!!");
			exit();
		}
		
	}
	function pilihan(a,b){
		if(document.getElementById("chk"+b).checked){
			document.getElementById("chk"+b).value='on';
		}else{
			document.getElementById("chk"+b).value='';
		}
	}
  function buatperiode(){
    tahun=document.getElementById("tahun").value;
    bulan=document.getElementById("bulan").value;
    bulan=trim(bulan);
    if(bulan.length==1) bulan='0'+bulan;
    document.getElementById("periode").value=tahun+bulan;
  }
  function isbaru(){
    if(document.getElementById('baru').checked){
      document.getElementById('baru').value='on';
    }else{
      document.getElementById('baru').value='';
    }
  }
</script>
