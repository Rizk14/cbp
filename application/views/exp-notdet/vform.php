<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => 'exp-notdet/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="9%">Date From</td>
								<td width="1%">:</td>
								<td width="90%"><input readonly id="datefrom" name="datefrom" value="<?= date('01-m-Y') ?>" onclick="showCalendar('',this,this,'','datefrom',0,20,1)"></td>
							</tr>
							<tr>
								<td width="9%">Date To</td>
								<td width="1%">:</td>
								<td width="90%"><input readonly id="dateto" name="dateto" value="<?= date('d-m-Y') ?>" onclick="showCalendar('',this,this,'','dateto',0,20,1)"></td>
							</tr>
							<tr>
								<td width="19%">Area</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("exp-notdet/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Download" target="blank" onclick="return exportexcel();">
										<input value="Download" type="button">
									</a>
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-notdet/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function exportexcel() {
		var datefrom = document.getElementById('datefrom').value;
		var dateto = document.getElementById('dateto').value;
		var iarea = document.getElementById('iarea').value;

		if (datefrom == '' || dateto == '') {
			alert('Pilih Tanggal Terlebih Dahulu!!!');
			return false;
		} else if (iarea == '') {
			alert('Pilih Area Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url($folder . '/cform/export/'); ?>" + iarea + "/" + datefrom + "/" + dateto;
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>