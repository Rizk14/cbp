<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
	<table class="maintable">
  	<tr>
   <td align="left">
  	<?php echo $this->pquery->form_remote_tag(array('url'=>'exp-customerlist/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	<div class="accordion2">
	<?php 
  	echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Customerlist</h3></center>";
		echo "<center><h3>Area $iarea</h3></center>";
	?>
    <table class="listtable" border=none id="sitabel">
    <tr>
      <th align="center">No</th>
      <th align="center">Kd Lang</th>
      <th align="center">Nama</th>
      <th align="center">Alamat</th>
      <th align="center">Kota</th>
      <th align="center">No Tlp</th>
      <th align="center">TOP</th>
      <th align="center">Plafon</th>
      <th align="center">Disc 1</th>
      <th align="center">Disc 2</th>
      <th align="center">Salesman</th>
      <th align="center">Contact Person</th>
      <th align="center">Tgl Daftar</th>
      <th align="center">Aktif</th>
      <th align="center">Klp Harga</th>
      <th align="center">Nama PKP</th>
      <th align="center">Alamat PKP</th>
      <th align="center">NPWP</th>
      <th align="center">NIK</th>
      <th align="center">Jenis Pelanggan</th>
      <th align="center">Status Pelanggan</th>
      <th align="center">Metode Pembayaran</th>
      <th align="center">Grade Toko</th>
      <th align="center">Status Toko</th>
      <th align="center">Kelengakapan</th>
 	  </tr>
	<tbody>
	<?php 
		if($isi){
	      $i=1;

      foreach($isi as $row){
        if($row->f_customer_aktif=='t'){
          $aktif="Ya";
        }else{
          $aktif="Tdk";
        }

        if($row->e_customer_payment=='0'){
          $row->e_customer_payment = "";
        }

        if(($row->f_customer_pkp=='t')){
          if(($row->e_customer_pkpnpwp!="" || $row->e_customer_pkpnpwp!=NULL)&&($row->e_customer_phone!="" || $row->e_customer_phone!=NULL)){
            $kelengkapan = "Lengkap";
          }else{
            $kelengkapan = "Tidak Lengkap";
          }
        }

        if(($row->f_customer_pkp=='f')){
          if(($row->i_nik!="" || $row->i_nik!=NULL)&&($row->e_customer_phone!="" || $row->e_customer_phone!=NULL)){
            $kelengkapan = "Lengkap";
          }else{
            $kelengkapan = "Tidak Lengkap";
          }
        }

        $cektmp = $this->db->query("select i_customer from tr_customer_tmp where i_customer='$row->i_customer'");
        if($cektmp->num_rows() > 0){
          $tmp = $this->db->query(" SELECT i_customer, a.i_shop_status, e_shop_status
                                    FROM tr_customer_tmp a
                                    LEFT JOIN tr_shop_status b ON(a.i_shop_status=b.i_shop_status)
                                    WHERE i_customer='$row->i_customer'",FALSE);

          if($tmp->num_rows > 0){
            foreach ($tmp->result() as $bb) {
              if(($bb->e_shop_status=='')||($bb->e_shop_status==NULL)){
                $eshop = "";  
              }else{
                $eshop = $bb->e_shop_status;
              }
            }
          }
        }else{
          $tmp = $this->db->query(" SELECT i_customer, a.i_shop_status, e_shop_status
                                    FROM tr_customer_tmpnonspb a
                                    LEFT JOIN tr_shop_status b ON(a.i_shop_status=b.i_shop_status)
                                    WHERE i_customer='$row->i_customer'",FALSE);

          if($tmp->num_rows > 0){
            foreach ($tmp->result() as $bb) {
              if(($bb->e_shop_status=='')||($bb->e_shop_status==NULL)){
                $eshop = "";  
              }else{
                $eshop = $bb->e_shop_status;
              }
            }
          }
        }

        echo "<tr>
          		  <td align=right>$i</td>
                <td>'".$row->i_customer."</td>
                <td>$row->e_customer_name</td>
          		  <td>$row->e_customer_address</td>
                <td>$row->e_city_name</td>
                <td>$row->e_customer_phone</td>
                <td>$row->n_customer_toplength</td>
                <td>".number_format($row->v_flapond)."</td>
                <td>$row->n_customer_discount1</td>
                <td>$row->n_customer_discount2</td>
                <td>$row->i_salesman</td>
                <td>$row->e_customer_contact</td>
                <td>".date("d-m-Y", strtotime($row->d_signin))."</td>
                <td>$aktif</td>
                <td>$row->e_price_groupname</td>
                <td>$row->e_customer_pkpname</td>
                <td>$row->e_customer_pkpaddress</td>
                <td>$row->e_customer_pkpnpwp</td>
                <td>$row->i_nik</td>
                <td>$row->e_customer_classname</td>
                <td>$row->e_customer_statusname</td>
                <td>$row->e_customer_payment</td>
                <td>$row->e_customer_gradename</td>
                <td>$eshop</td>
                <td>$kelengkapan</td>
                ";
        		$i++;
				echo "</tr>";	
			}
		}
	   ?>
     <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	</tbody>
	</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>