<?php 
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>$namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel" width="750px">
	   	    <th width="750px" colspan=5>&nbsp;</th>
		<?php 
		echo "<tr valign=top><td colspan=2>";

		echo "<table width=\"100%\">
			  <tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Hasil Penjualan :</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";
		#--------Hasil Penjualan
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Hasil Penjualan</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";

		  if($hasilpenj){
			echo "<td width=\"100px\" align=right>".number_format($hasilpenj)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
			echo "</tr>";

		#--------Retur Penjualan
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>&nbsp;&nbsp;&nbsp;Retur Penjualan</td>";
		  if($returpenj){
			echo "<td width=\"100px\" align=right>".number_format($returpenj)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

		#--------Potongan Penjualan
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>&nbsp;&nbsp;&nbsp;Potongan Penjualan</td>";
		  if($potpejualan){
			echo "<td width=\"100px\" align=right>".number_format($potpejualan)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";
		$retpoto=$potpejualan+$returpenj;

		#--------Retur dan potongan Penjualan
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Retur Dan Potongan Penjualan</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		if($retpoto){
			echo "<td width=\"100px\" align=right style=\"font-weight: ;\">".number_format($retpoto)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";
		$penjbersih=$hasilpenj-$retpoto;

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

		#--------Hasil Penjualan Bersih
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Hasil Penjualan Bersih</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		if($potpejualan){
			echo "<td width=\"100px\" align=right style=\"font-weight: bold;\">Rp.".number_format($penjbersih)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
			echo "</tr>";
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

    echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Harga Pokok Penjualan:</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>
			  <td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

		#----------Persediaan Awal Barang Jadi
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Persediaan Awal Barang Jadi</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		if($persediaanawalbarangjadi){
		echo "<td width=\"100px\" align=right>".number_format($persediaanawalbarangjadi)."</td>";
		}else{	
			echo "<td width=\"100px\" align=right>0</td>";
		}	
		#if(){
			#echo "<td width=\"100px\" align=right>0</td>";
		#}else{
			echo "<td width=\"100px\" align=right>&nbsp;</td>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "</tr>";


		#--------Pembelian Barang Jadi
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Pembelian Barang Jadi</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		if($pembelianbarangjadi){
			echo "<td width=\"100px\" align=right style=\"font-weight: ;\">".number_format($pembelianbarangjadi)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";


		#--------Retur Pembelian
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>&nbsp;&nbsp;&nbsp;Retur Pembelian</td>";
		  if($returpembelian){
			echo "<td width=\"100px\" align=right>".number_format($returpembelian)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

		#--------Potongan Pembelian
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>&nbsp;&nbsp;&nbsp;Potongan Pembelian</td>";
		  if($potonganpembelian){
			echo "<td width=\"100px\" align=right>".number_format($potonganpembelian)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";
		$retpotopembelian=$returpembelian+$potonganpembelian;

		#--------Retur dan Potongan Pembelian
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Retur dan Potongan Pembelian</td>";
			  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($retpotopembelian){
			echo "<td width=\"100px\" align=right>(".number_format($retpotopembelian).")</td>";
		}else{
			echo "<td width=\"100px\" align=right>(0)</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";
		$pembbarangjadibersih=$pembelianbarangjadi-$retpotopembelian;
#--------Pembelian Barang Jadi Bersih
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Pembelian Barang Jadi Bersih</td>";
			  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($pembbarangjadibersih){
			echo "<td width=\"100px\" align=right>".number_format($pembbarangjadibersih)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

#--------Pemberian Cuma - Cuma
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Pemberian Cuma - Cuma</td>";
			  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($hadiah){
			echo "<td width=\"100px\" align=right>(".number_format($hadiah).")</td>";
		}else{
			echo "<td width=\"100px\" align=right>(0)</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

#--------Barang Yang Tersedia Untuk Dijual
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Barang Yang Tersedia Untuk Dijual</td>";
			  echo "<td width=\"100px\" align=right>&nbsp;</td>";
			  $barangygtersediauntukdijual=$pembbarangjadibersih+$persediaanawalbarangjadi-$hadiah;
		  if($barangygtersediauntukdijual){
			echo "<td width=\"100px\" align=right>".number_format($barangygtersediauntukdijual)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right></td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

#--------Persediaan Akhir Barang Jadi
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2>Persediaan Akhir Barang Jadi</td>";
			  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($persediaanakhirbarangjadi){
			echo "<td width=\"100px\" align=right>".number_format($persediaanakhirbarangjadi)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>(0)</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "</tr>";

#--------Harga Pokok Barang Yang Dijual
		$hpbarang=$barangygtersediauntukdijual-$persediaanakhirbarangjadi;
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Harga Pokok Barang Yang Dijual :</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($retpotopembelian){
			echo "<td width=\"100px\" align=right>".number_format($hpbarang)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
			echo "</tr>";

# ---------------------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "</tr>";
		$labakotor=$penjbersih-$hpbarang;
#--------Laba Kotor
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Laba Kotor :</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($labakotor){
			echo "<td width=\"100px\" align=right>".number_format($labakotor)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
			echo "</tr>";


#--------Biaya Operasional
    echo "<tr valign=top> 
          <td colspan=5>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Biaya Operasional</td>
          <td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "</tr>";

#--------Biaya Penjualan
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Biaya Penjualan</td>";
		  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($biayapenjualan){
			echo "<td width=\"100px\" align=right>".number_format($biayapenjualan)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";


#--------Biaya Administrasi dan Umum
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Biaya Administrasi dan Umum </td>";
		    echo "<td width=\"100px\" align=right>&nbsp;</td>";
		  if($biayaadmumum){
			echo "<td width=\"100px\" align=right>".number_format($biayaadmumum)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
			echo "</tr>";

#-----------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "</tr>";

#--------Jumlah Biaya Operasional
		$biayaoperasional=$biayapenjualan+$biayaadmumum;
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Jumlah Biaya Operasional </td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($biayaoperasional){
			echo "<td width=\"100px\" align=right>".number_format($biayaoperasional)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
			echo "</tr>";

#-----------
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "</tr>";	
		
#--------Laba Operasional
		$labaoperasional=$labakotor-$biayaoperasional;
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Laba Operasional </td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($labaoperasional){
			echo "<td width=\"100px\" align=right>".number_format($labaoperasional)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
			echo "</tr>";

#-----------Pendapatan/Biaya Lainnya		
    echo "<tr valign=top> 
          <td colspan=5>&nbsp;</td>";
		echo "</tr>";
    echo "<tr valign=top> 
          <td width=\"400px\"colspan=2 style=\"font-weight: bold;\">Pendapatan/Biaya Lainnya :</td>
          <td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
    echo "<td width=\"175px\" align=right>&nbsp;</td>";
		echo "</tr>";

		
#--------Pendapatan Lain-Lain
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Pendapatan Lain-Lain </td>";
		 echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($pendapatanlainlain){
			echo "<td width=\"100px\" align=right>".number_format($pendapatanlainlain)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";
			
#--------Biaya Bunga Bank & Biaya Lainnya
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Biaya Bunga Bank & Biaya Lainnya </td>";
		  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  if($biayabungabank){
			echo "<td width=\"100px\" align=right>".number_format($biayabungabank)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
			echo "</tr>";
			
#--------------			
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		echo "</tr>";	
		$totalpend_biaya=$pendapatanlainlain+$biayabungabank;
#--------Total pendapatan Lain-Lain + Biaya Bunga Bank & Biaya Lainnya
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\"></td>";
				echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		  if($totalpend_biaya){
			echo "<td width=\"100px\" align=right>Rp.".number_format($totalpend_biaya)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}	
			echo "</tr>";

#--------------			
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2></td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>&nbsp;</td>";
		echo "<td width=\"100px\" align=right>_______________</td>";	
		echo "</tr>";
		
#--------Pendapatan Bersih Sebelum Pajak
			$pendapatanbersih=$labaoperasional+$totalpend_biaya;
			echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: ;\">Pendapatan Bersih Sebelum Pajak</td>";
		  echo "<td width=\"100px\" align=right>&nbsp;</td>";	
		  echo "<td width=\"100px\" align=right>&nbsp;</td>";
		  if($pendapatanbersih){
			  echo "<td width=\"100px\" align=right><b>".number_format($pendapatanbersih)."</b></td>";
			  $query=$this->db->query("select * from tm_lr where i_periode = '$periode'",false);
		    if ($query->num_rows() > 0){
			    $this->db->query("update tm_lr set v_lr=$pendapatanbersih where i_periode = '$periode'");
		    }else{
			    $this->db->query("insert into tm_lr values ('$periode',$pendapatanbersih)");
		    }
		  }else{
			  echo "<td width=\"100px\" align=right>0</td>";
			  $query=$this->db->query("select * from tm_lr where i_periode = '$periode'",false);
		    if ($query->num_rows() > 0){
			    $this->db->query("update tm_lr set v_lr=0 where i_periode = '$periode'");
		    }else{
			    $this->db->query("insert into tm_lr values ('$periode',0)");
		    }
		  }
			echo "</tr>";		
		
		?>
		</table>
		<input name="cmdreset" id="cmdreset" value="Export to excel" type="button">
      </tbody>
      </div>
	</div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
 $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
