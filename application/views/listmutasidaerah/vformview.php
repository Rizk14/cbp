<style>
	.container {
		position: relative;
		overflow: scroll;
		height: 500px;
	}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tablefixheader.css" />
<div id='tmp'>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'listmutasidaerah/cform/view', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<div style="overflow-x:auto;">
							<div class="container">
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$periode = $row->e_mutasi_periode;
									}
								} else {
									$periode = $iperiode;
								}
								$a = substr($periode, 0, 4);
								$b = substr($periode, 4, 2);
								$periode = mbulan($b) . " - " . $a;
								?>
								<input name="iperiode" id="iperiode" value="<?php echo $iperiode; ?>" type="hidden">
								<input name="istore" id="istore" value="<?php echo $istore; ?>" type="hidden">
								<input name="pperiode" id="pperiode" value="<?php echo $row->e_mutasi_periode; ?>" type="hidden">
								<input name="iarea" id="iarea" value="<?php echo $iarea; ?>" type="hidden">
								<input name="istorelocation" id="istorelocation" value="<?php echo $istorelocation; ?>" type="hidden">

								<?php
								echo "<center><h2>" . NmPerusahaan . "</h2></center>";
								echo "<center><h3>LAPORAN MUTASI STOCK - $row->i_store ($istorelocation)</h3></center>";
								echo "<center><h3>Periode $periode</h3></center>";
								?>

								<center>
									<input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasidaerah/cform/index","#main");'>
									<input name="login" id="login" value="Refresh" type="submit">
								</center>

								<?php if ($saldo) { ?>
									<table class="listtablex" width="948px;">
										<tr>
											<thead>
												<th colspan='2' style="width:316px">Saldo Akhir</th>
												<th colspan='2' style="width:316px">Stock Opname</th>
												<th colspan='2' style="width:316px">Selisih</th>
											</thead>

											<tbody>
												<?php
												$rpselisih 			= 0;
												$rpsaldoakhir 		= 0;
												$rpstockopname 		= 0;

												$totsahir 	= 0;
												$totso 		= 0;
												$totselisih	= 0;

												foreach ($saldo as $riw) {
													echo "<tr>
													<td style='width:105px'>" . $riw->i_sales_category . " " . $riw->e_sales_categoryname . "</td>
													<td style='width:205px'> : " . number_format($riw->saldoakhir) . " Pcs / Rp. " . number_format($riw->rpsaldoakhir) . "</td>

													<td style='width:100px'>" . $riw->i_sales_category . " " . $riw->e_sales_categoryname . "</td>
													<td style='width:205px'> : " . number_format($riw->saldostockopname) . " Pcs / Rp. " . number_format($riw->rpstockopname) . "</td>

													<td style='width:100px'>" . $riw->i_sales_category . " " . $riw->e_sales_categoryname . "</td>
													<td style='width:205px'> : " . number_format($riw->selisih) . " Pcs / Rp. " . number_format($riw->rpselisih) . "</td>
												</tr>";
													$totsahir 		+= $riw->saldoakhir;
													$rpsaldoakhir 	+= $riw->rpsaldoakhir;
													$totso 			+= $riw->saldostockopname;
													$rpstockopname 	+= $riw->rpstockopname;
													$totselisih 	+= $riw->selisih;
													$rpselisih 		+= $riw->rpselisih;
												}
												echo "<tr>
												<td align='center' style='font-weight: bold;'>TOTAL</td>
												<td> : " . number_format($totsahir) . " Pcs / Rp. " . number_format($rpsaldoakhir) . "</td>
												<td align='center' style='font-weight: bold;'>TOTAL</td>
												<td> : " . number_format($totso) . " Pcs / Rp. " . number_format($rpstockopname) . "</td>
												<td align='center' style='font-weight: bold;'>TOTAL</td>
												<td> : " . number_format($totselisih) . " Pcs / Rp. " . number_format($rpselisih) . "</td>
											</tr>";
												?>
											</tbody>
										</tr>
									</table>
								<?php }
								echo "<br>"; ?>

								<table id="tabledata" class="listtable" border=none>
									<thead class="sticky-head">
										<tr>
											<th rowspan="2">No</th>
											<th rowspan="2">Kategori Penjualan</th>
											<th rowspan="2">Kode</th>
											<th rowspan="2">Nama</th>
											<th colspan="3" align="center">Saldo Awal</th>
											<th colspan="1" align="center">Penerimaan</th>
											<th colspan="3" align="center">Pengeluaran</th>
											<th rowspan="2" align="center">Adj</th>
											<th colspan="1" align="center">Saldo</th>
											<th colspan="1">Stock</th>
											<th colspan="1">Selisih</th>
											<th rowspan="2" align="center">GIT</th>
											<th rowspan="2" align="center">GIT Penj</th>
											<th rowspan="2" align="center" class="action">Action</th>
										</tr>
										<tr>
											<th>Saldo Awal</th>
											<th>GIT Awal</th>
											<th>GIT Jual Awal</th>
											<th>Dari Pusat</th>
											<th>Penjualan</th>
											<th>Ke Pusat</th>
											<th>MO</th>
											<th>Akhir</th>
											<th>Opname</th>
											<th>(pcs)</th>
										</tr>
									</thead>

									<tbody>
										<?php
										if ($isi) {
											$i = 1;
											$selisih = 0;
											$rpsaldoakhir = 0;
											$rpstockopname = 0;
											$group = '';
											foreach ($isi as $row) {
												$this->db->select("	v_product_retail
																	from tr_product_price
																	where i_product='$row->i_product' and i_price_group='00'");
												$query = $this->db->get();
												if ($query->num_rows() > 0) {
													foreach ($query->result() as $tmp) {
														$row->v_product_retail = $tmp->v_product_retail;
													}
												}
												$selisih = ($row->n_saldo_stockopname + $row->n_saldo_git + $row->n_git_penjualan) - $row->n_saldo_akhir;
												$rpsaldoakhir = $row->n_saldo_akhir * $row->v_product_retail;
												$rpstockopname = $row->n_saldo_stockopname * $row->v_product_retail;
												$totsaldoawal = $row->n_saldo_awal; #+$row->n_mutasi_gitasal+$row->n_git_penjualanasal;
												$saldoawal = $row->n_saldo_awal;
												$gitawal = $row->n_mutasi_gitasal;
												$gitjualawal = $row->n_git_penjualanasal;
												echo "<tr>
												<td align=right>$i</td>
												<td>$row->e_sales_categoryname</td>
												<td>$row->i_product</td>
												<td>$row->e_product_name</td>
												<td bgcolor=red align=right >$saldoawal</td>
												<td bgcolor=red align=right>$gitawal</td>
												<td bgcolor=red align=right>$gitjualawal</td>
												<td align=right>$row->n_mutasi_bbm</td>
												<td align=right>$row->n_mutasi_penjualan</td>
												<td align=right>$row->n_mutasi_bbk</td>
												<td align=right>$row->n_mutasi_ketoko</td>
												<td align=right>$row->adjustment</td>
												<td align=right>$row->n_saldo_akhir</td>
												<td align=right>$row->n_saldo_stockopname</td>
												<td align=right>$selisih</td>
												<td align=right>$row->n_saldo_git</td>
												<td align=right>$row->n_git_penjualan</td>";
												$i++;
												echo "<td class=\"action\">";
												echo "<a href=\"#\" onclick='show(\"listmutasidaerah/cform/detail/$iperiode/$iarea/$row->i_product/$totsaldoawal/$istorelocation/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												echo "</td></tr>";
											}
										}
										?>
									</tbody>
								</table>
								<center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasidaerah/cform/index","#main");'></center>
							</div>
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
	function yyy(a, c) {
		document.getElementById("iperiode").value = a;
		document.getElementById("iarea").value = c;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/listmutasidaerah/cform/viewdetail";
		formna.submit();
	}
</script>