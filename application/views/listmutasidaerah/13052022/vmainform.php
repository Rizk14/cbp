<?php 
  	if( ($iperiode=='')||($iarea=='') )
  	{
    	echo "<h2>$page_title</h2>";
	  	$this->load->view('listmutasidaerah/vform');
  	}
  	elseif(isset($isi))
  	{
	  	$data['cari']		  = $cari;
	  	$data['iperiode']	= $iperiode;
	  	$data['iarea']  	= $iarea;
	  	$data['istore']	  = $istore;
	  	$data['isi']		  = $isi;
		$data['istorelocation']	= $istorelocation;
	  $this->load->view('listmutasidaerah/vformview',$data);
  	}
  	elseif(isset($detail))
  	{
	  	$data['iperiode']	= $iperiode;
	  	$data['iarea']  	= $iarea;
	  	$data['iproduct']	= $iproduct;
	  	$data['detail']   = $detail;
		$data['saldo']    = $saldo;
	  	$this->load->view('listmutasidaerah/vformdetail',$data);
  	}
  	else
  	{
    	echo "<h2>Belum ada data mutasi !!!</h2>";
  	}
?>
