<table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'listaccplafond/cform/update','update'=>'#main','type'=>'post'));?>
	<div id="plafondform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	    <?php if ($isi){
	        foreach($isi as $row){
	    ?>
	      <tr>
		      <td>Area</td>
		      <td><input readonly id="eareaname" name="eareaname" value="<?php echo $row->e_area_name; ?>">
    		      <input hidden id="iarea" name="iarea" value="<?php echo $row->iarea; ?>">
		      </td>
		      <td>Max Penjualan</td>
		      <td><input id="vmaxpenjualan" name="vmaxpenjualan" readonly value="<?php echo number_format($row->v_max_penjualan);?>"></td>
        </tr>
        <tr>
		      <td>Customer</td>
		      <td><input readonly id="ecustomer" name="ecustomer" readonly value="<?php echo $row->e_customer_name;?>">
		          <input hidden id="icustomer" name="icustomer" readonly value="<?php echo $row->i_customer;?>">
		      </td>
		      <td>Rata Penjualan</td>
		      <td><input readonly id="vratapenjualan" name="vratapenjualan" value="<?php echo number_format($row->v_rata_penjualan);?>">
        </tr>
        <tr>
		      <td>TOP</td>
		      <td><input id="ntop" name="ntop" readonly value="<?php echo $row->n_customer_toplength;?>"> &nbsp;&nbsp;Rata Telat &nbsp; <input id="nratatelat" name="nratatelat" readonly value="<?php echo number_format($row->n_rata_telat);?>">
          </td>                    
		      <td>Plafond Program</td>
		      <td><input id="vplafondprogram" name="vplafondprogram" readonly value="<?php echo number_format($row->v_plafond);?>">
	      </tr>
        <tr>
		      <td>Kategori</td>
		      <td><input id="ekategori" name="ekategori" readonly value="<?php echo $row->e_kategori;?>">
          </td>
		      <td>Plafond Sebelumnya</td>
		      <td><input id="vplafondbefore" name="vplafondbefore" readonly value="<?php echo number_format($row->v_plafond_before);?>">
	      </tr>
        <tr>
		      <td>Total Penjualan</td>
		      <td><input readonly id="vtotalpenjualan" name="vtotalpenjualan" value="<?php echo number_format($row->v_total_penjualan); ?>"></td>
		      <td>Plafond Acc</td>
		      <td><input id="vplafondacc" name="vplafondacc" value="<?php echo number_format($row->v_plafond_acc);?>" autocomplete="off" onkeyup="reformat(this);">
		      <input hidden id="iperiodeawal" name="iperiodeawal" value="<?php echo $iperiodeawal?>">
		      <input hidden id="iperiodeakhir" name="iperiodeakhir" value="<?php echo $iperiodeakhir;?>">
		      <input id="iarea" name="iarea" hidden value="<?php echo $iarea;?>">
          </td>
	      </tr>
	      <tr>
		        <td width="100%" align="center" colspan="4">
		          <input name="login" id="login" value="Simpan" type="submit">
		          <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listaccplafond/cform/view/<?php echo $iperiodeawal."/".$iperiodeakhir."/".$iarea."/" ?>","#main")'>
		        </td>
    		</tr>
	      <?php }}?>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<div id="pesan"></div>
<script>
  function format(){
    	vplafondacc=document.getElementById("vplafondacc").value;
			document.getElementById("vplafondacc").value=formatcemua(vplafondacc);
  }
</script>
