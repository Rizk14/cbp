<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printfkomkelompok/cform/fakturto','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>"><input type="hidden" id="area" name="area" value="<?php echo $area; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
 	    <th>Faktur</th>
	    <th>Nota</th>
	    <th>Tgl Nota</th>
	    <th>Lang</th>
	    <th>Sls</th>
	    <th>Kotor</th>
	    <th>Potongan</th>
	    <th>DPP</th>
	    <th>PPN</th>
	    <th>Bersih</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->i_faktur_komersial</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->i_nota</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->d_nota</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->i_salesman</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->v_nota_gross</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->v_nota_discounttotal</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->v_nota_netto</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->v_nota_netto</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->i_nota','$row->i_sj')\">$row->v_nota_netto</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c)
  {
    document.getElementById("fakturto").value=a;
    document.getElementById("inotato").value=b;
    document.getElementById("isjto").value=c;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
