<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printbbmreturkelompok/cform/cetak','update'=>'#main','type'=>'post'));?>
	<div class="printbbmreturkelompokform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="19%">Date From</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dfrom',
			              'id'          => 'dfrom',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Date To</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'dto',
			              'id'          => 'dto',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)");
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">Area</td>
		<td width="1%">:</td>
		<td width="80%">
			<input type="hidden" id="iarea" name="iarea" value="">
			<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("printbbmreturkelompok/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    			</td>
        </tr>
	      <tr>
		<td width="19%">BBM Retur From</td>
		<td width="1%">:</td>
		<td width="80%">
						<?php 
				$data = array(
			              'name'        => 'bbmreturfrom',
			              'id'          => 'bbmreturfrom',
			              'value'       => '',
			              'readonly'   	=> 'true',
				      	  'onclick'	    => 'view_bbmreturfrom();');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="19%">BBM Retur To</td>
		<td width="1%">:</td>
		<td width="80%">
						<input type="text" id="bbmreturto" name="bbmreturto" value="" onclick='view_bbmreturto()'></td>
	      </tr>
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="Cetak" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('printbbmreturkelompok/cform/','#main')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">

  function view_bbmreturfrom(){
    dfrom=document.getElementById("dfrom").value;
    dto=document.getElementById("dto").value;
    area=document.getElementById("iarea").value;
    if( (dfrom!='') && (dto!='') && (area!='') ){
      isi="printbbmreturkelompok/cform/bbmreturfrom/"+dfrom+"/"+dto+"/"+area+"/";
      showModal(isi,"#light");
    }else{
      alert('Isi dulu Periode dan Area');
    }
  }

  function view_bbmreturto(){
    dfrom=document.getElementById("dfrom").value;
    dto=document.getElementById("dto").value;
    area=document.getElementById("iarea").value;

    if( (dfrom!='') && (dto!='') && (area!='') ){
      isi="printbbmreturkelompok/cform/bbmreturto/"+dfrom+"/"+dto+"/"+area+"/";
      showModal(isi,"#light");
    }else{
      alert('Isi dulu Periode dan Area');
    }

  }
</script>
