<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4).' s/d '.substr($dto,0,2).' '.mbulan(substr($dto,3,2)).' '.substr($dto,6,4); ?></h3>

	<?php echo $this->pquery->form_remote_tag(array('url'=>'listcollectionpernotacash/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
  	  <table class="listtable" id="sitabel">
<?php 
	if($isi){
?>
    <tr>
 	  <th>Area</th>
	  <th>Toko</th>
	  <th>Jenis</th>
	  <th>Tanggal</th>
	  <th>Salesman</th>
	  <th>Target</th>
	  <th>Realisasi</th>
	  <th>Persen</th>
    </tr>
    <tbody>
<?php 
    $grandtarget=0;
    $grandreal=0;

		foreach($isi as $row){
      if($row->realisasi>0){
        $persen=($row->realisasi*100)/$row->total;
      }else{
        $persen=0;
      }
      echo '<tr><td>'.$row->i_area.' - '.$row->e_area_name.'</td>';
      echo '<td>'.$row->i_customer.' - '.$row->e_customer_name.'</td>';
      echo '<td>'.$row->d_nota.'</td>';
      echo '<td>'.$row->i_nota.'</td>';
      echo '<td>'.$row->i_salesman.' - '.$row->e_salesman_name.'</td>';
      echo '<td align=right>'.number_format($row->total).'</td>';
      echo '<td align=right>'.number_format($row->realisasi).'</td>';
      echo '<td align=right>'.number_format($persen,2).'%</td><tr>';
      $grandtarget=$grandtarget+$row->total;
      $grandreal=$grandreal+$row->realisasi;
    }
    echo '<tr><td colspan=5 align=center><b>Total</td>';
    if($grandtarget>0){
      $persen=($grandreal*100)/$grandtarget;
    }else{
      $persen=0;
    }
    echo '<td align=right>'.number_format($grandtarget).'</td>';
    echo '<td align=right>'.number_format($grandreal).'</td>';
    echo '<td align=right>'.number_format($persen,2).'%</td></td>';
  }
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function xxx(x,a,g){
    if (confirm(g)==1){
	    document.getElementById("ispbdelete").value=a;
   	  document.getElementById("inotadelete").value=x;
	    formna=document.getElementById("listform");
	    formna.action="<?php echo site_url(); ?>"+"/listcollectionpernotacash/cform/delete";
  	  formna.submit();
    }
  }
  function yyy(x,b){
	  document.getElementById("ispbedit").value=b;
	  document.getElementById("inotaedit").value=x;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/nota/cform/edit";
	  formna.submit();
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
