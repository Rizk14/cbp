<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h3><?php 
echo 'Periode : '.$iperiode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperdivisistockcabang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>AREA</th>
		<th>NAMA AREA</th>
    <th>PENJUALAN Rp</th>
    <th>SISA SALDO</th>
    <th>GIT STOCK</th>
    <?php 
	    echo '<tbody>';
      $group='';
      $grandj=0;
      $grands=0;
      $grandg=0;
      foreach($prodnya as $row)
      {
        if($group==''){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
        }
        if($group!='' && $group!=$row->e_product_groupname){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
        }
        $totalj=0;
        $totals=0;
        $totalg=0;
        foreach($areanya as $raw)
        { 
          if($raw->i_area!='00'){
            echo "<tr><td>$raw->i_area</td><td>$raw->e_area_name</td>";
            $ada=false;
            foreach($isi as $riw)
            {
#              echo 'jml:'.$riw->jumlah.'<br>';
              if( ($riw->i_area==$raw->i_area) && ($riw->i_product_group==$row->i_product_group)){
                $ada=true;
                echo "<td align=right>".number_format($riw->jumlah)."</td>";
                echo "<td align=right>".number_format($riw->sisasaldo)."</td>";
                echo "<td align=right>".number_format($riw->git)."</td>";
                $totalj=$totalj+$riw->jumlah;
                $totals=$totals+$riw->sisasaldo;
                $totalg=$totalg+$riw->git;
              }
              if($ada)break;
            }
            if(!$ada){
              echo "<td align=right>0</td>";
              echo "<td align=right>0</td>";
              echo "<td align=right>0</td>";
            }
          }
        }
        echo "</tr>";
        echo "<tr><td colspan=2 align=right><b>Total</td>
                  <td align=right><b>".number_format($totalj)."</td>
                  <td align=right><b>".number_format($totals)."</td>
                  <td align=right><b>".number_format($totalg)."</td></tr>";
        $group=$row->e_product_groupname;
        $grandj=$grandj+$totalj;
        $grands=$grands+$totals;
        $grandg=$grandg+$totalg;
      }
        echo "<tr><td colspan=2 align=right><b>Grand Total</td>
                  <td align=right><b>".number_format($grandj)."</td>
                  <td align=right><b>".number_format($grands)."</td>
                  <td align=right><b>".number_format($grandg)."</td></tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
