<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <!--	<link href="<?php echo base_url() . 'assets/css/bootstrap.css'; ?>" rel="stylesheet">
	<link href="<?php echo base_url() . 'assets/themes/default/css/bootstrap.min.css'; ?>" rel="stylesheet"> -->
  <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body>
  <div id="main">
    <div id="tmpx">
      <!-- <h2>Mengapa No <?= $ispb ?> (<?= $icustomer . "-" . $ecustomer ?>) ini dibatalkan?</h2> -->
      <h2>Mengapa SPB Toko (<?= $icustomer ?>) <?= $ecustomer ?> </br> No <?= $ispb ?> ini dibatalkan?</h2>
      <table class="maintable">
        <tr>
          <td align="left">
            <?php
            // echo $this->pquery->form_remote_tag(array('url' => 'listspb/cform/delete', 'update' => '#pesan', 'type' => 'post'));
            $is_cari = 1;
            ?>
            <div id="aktkkform">
              <div class="effect">
                <div class="accordion2">
                  <center>
                    <input type="hidden" id="ispb" name="ispb" value="<?php echo $ispb ?>" readonly>
                    <input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea ?>" readonly>
                    <input type="hidden" id="iareax" name="iareax" value="<?php echo $iareax ?>" readonly>
                    <input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom ?>" readonly>
                    <input type="hidden" id="dto" name="dto" value="<?php echo $dto ?>" readonly>
                    <input type="hidden" id="ireffadvo" name="ireffadvo" value="<?php echo $ireffadvo ?>" readonly>

                    <select name="eremark1" id="eremark1">
                      <option value="">-</option>
                      <option value="Masih Ada Piutang">Masih Ada Piutang</option>
                      <option value="Data Toko Tidak Lengkap">Data Toko Tidak Lengkap</option>
                      <option value="Salah Input SPB">Salah Input SPB</option>
                      <option value="Error System">Error System</option>
                      <option value="SPB Double">SPB Double</option>
                    </select>

                    <input type="text" name="eremark2" id="eremark2" value="" placeholder="Alasan Lainnya..">

                    <br><br>
                    <input name="login" id="login" value="Update" type="submit" onclick="return dipales();">&nbsp;&nbsp;
                    <input type="button" id="batal" name="batal" value="Tutup" onclick="bbatal()">
                  </center>
                  <?= form_close() ?>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>
<script language="javascript" type="text/javascript">
  function dipales() {
    var ispb = document.getElementById('ispb').value;
    var iarea = document.getElementById('iarea').value;
    var iareax = document.getElementById('iareax').value;
    var dfrom = document.getElementById('dfrom').value;
    var dto = document.getElementById('dto').value;
    var eremark1 = document.getElementById('eremark1').value;
    var eremark2 = document.getElementById('eremark2').value;
    var ireffadvo = document.getElementById('ireffadvo').value;

    if ((eremark1 == '') && (eremark2 == '')) {
      alert('Silahkan Isi Salah Satu Kolom Alasan!!!');
      return false
    } else if ((eremark1 != '') && (eremark2 != '')) {
      alert('Isi Salah Satu Kolom Saja!!!');
      return false
    } else {
      /* document.getElementById("login").hidden = true;
      bbatal();
      refreshview(dfrom, dto, iarea); */

      $.ajax({
        type: "POST",
        url: "<?php echo site_url('listspb/cform/delete'); ?>",
        data: {
          ispb: ispb,
          iarea: iarea,
          dfrom: dfrom,
          dto: dto,
          eremark1: eremark1,
          eremark2: eremark2,
          ireffadvo: ireffadvo,
        },
        success: function(data) {
          bbatal()
          refreshview(dfrom, dto, iareax)
        },

        error: function(XMLHttpRequest) {
          alert(XMLHttpRequest.responseText);
        }

      })
    }
  }

  function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
  }

  function refreshview(a, b, c) {
    show('listspb/cform/view2/' + a + "/" + b + "/" + c, '#main');
  }
</script>