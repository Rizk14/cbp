<style>
.naonwaebebas{
	display: block;
	width: 100% !important;
	white-space: nowrap;
	overflow-x: auto;
	-webkit-overflow-scrolling: touch;
}
</style>

<h2><?php echo $page_title; ?></h2>
<table class="maintable naonwaebebas">
	<tr>
		<td align="left">
			<?php
			$tujuan = 'listspb/cform/view2';
			?>
			<?php echo $this->pquery->form_remote_tag(array('url' => $tujuan, 'update' => '#main', 'type' => 'post')); ?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable" id="sitabel">
							<thead>
								<tr>
									<td colspan="22" align="left">
										Cari data :
										<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">
										&nbsp; <input type="hidden" name="dfrom" value="<?php echo $dfrom; ?>">
										<input type="hidden" name="dto" value="<?php echo $dto; ?>">
										<input type="hidden" name="iarea" value="<?php echo $iarea; ?>">
										<input type="hidden" name="is_cari" value="1">
										<input type="submit" id="bcari" name="bcari" value="Cari">
									</td>
								</tr>
							</thead>
							<th>No SPB</th>
							<th>Tgl SPB</th>
							<th>Tgl App Sls</th>
							<th>Tgl App AR</th>
							<th>Sls</th>
							<th>Lang</th>
							<th>Area</th>
							<th>SPB (Rp)</th>
							<th>Nota (Rp)</th>
							<th>Pendingan (Rp)</th>
							<th>%</th>
							<th>Stat</th>
							<th>SJ</th>
							<th>Tgl SJ</th>
							<th>Tgl DKB</th>
							<th>Nota</th>
							<th>Tgl Nota</th>
							<th>Tgl Terima Toko</th>
							<th>SPB Referensi</th>
							<th>Drh</th>
							<th>Jns</th>
							<th class="action">Act</th>
							<tbody>
								<?php
								$tmpspb = 0;
								$tmpnota = 0;
								$tmppendingan = 0;
								if ($isi) {
									foreach ($isi as $row) {
										$que = $this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spb_item
                                where i_area = '$row->i_area' and i_spb='$row->i_spb'");
										if ($que->num_rows() > 0) {
											foreach ($que->result() as $riw) {
												$order = $riw->order;
											}
										} else {
											$order = 0;
										}
										$que = $this->db->query(" select sum(b.n_deliver*b.v_unit_price)as deliver from tm_nota_item b, tm_nota c
                                where c.i_area = '$row->i_area' and c.i_spb='$row->i_spb' and
                                b.i_sj=c.i_sj and b.i_area=c.i_area");
										if ($que->num_rows() > 0) {
											foreach ($que->result() as $riw) {
												$deliv = $riw->deliver;
											}
										} else {
											$deliv = 0;
										}
										if ($deliv == '') {
											$persen = '0.00';
										} else {
											$persen = number_format(($deliv / $order) * 100, 2);
										}
										if ($row->f_spb_stockdaerah == 't') {
											$daerah = 'Ya';
										} else {
											$daerah = 'Tidak';
										}
										if ($row->d_spb) {
											$tmp = explode('-', $row->d_spb);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_spb = $tgl . '-' . $bln . '-' . $thn;
										}
										if ($row->d_appsales) {
											$tmp = explode('-', $row->d_appsales);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_appsales = $tgl . '-' . $bln . '-' . $thn;
										}
										if ($row->d_appar) {
											$tmp = explode('-', $row->d_appar);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_appar = $tgl . '-' . $bln . '-' . $thn;
										}
										if ($row->d_sj) {
											$tmp = explode('-', $row->d_sj);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_sj = $tgl . '-' . $bln . '-' . $thn;
										}
										if ($row->d_dkb) {
											$tmp = explode('-', $row->d_dkb);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_dkb = $tgl . '-' . $bln . '-' . $thn;
										}
										if ($row->d_sj_receive) {
											$tmp = explode('-', $row->d_sj_receive);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_sj_receive = $tgl . '-' . $bln . '-' . $thn;
										} else {
											$row->d_sj_receive = '';
										}
										if (
											($row->f_spb_cancel == 't')
										) {
											$status = 'Batal';
										} elseif (
											($row->i_approve1 == null) && ($row->i_notapprove == null)
										) {
											$status = 'Sales';
										} elseif (
											($row->i_approve1 == null) && ($row->i_notapprove != null)
										) {
											$status = 'Reject (sls)';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 == null) &
											($row->i_notapprove == null)
										) {
											$status = 'Keuangan';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 == null) &&
											($row->i_notapprove != null)
										) {
											$status = 'Reject (ar)';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store == null)
										) {
											$status = 'Gudang';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
										) {
											$status = 'Pemenuhan SPB';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
										) {
											$status = 'Proses OP';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
										) {
											$status = 'OP Close';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
										) {
											$status = 'Siap SJ (sales)';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
										) {
											#			  	$status='Siap SJ (gudang)';
											$status = 'Siap SJ';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
										) {
											$status = 'Siap SJ';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
										) {
											$status = 'Siap DKB';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
											($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
										) {
											$status = 'Siap Nota';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) &&
											($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
										) {
											$status = 'Siap SJ';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) &&
											($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
										) {
											$status = 'Siap DKB';
										} elseif (
											($row->i_approve1 != null) && ($row->i_approve2 != null) &&
											($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) &&
											($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
										) {
											$status = 'Siap Nota';
										} elseif (
											($row->i_approve1 != null) &&
											($row->i_approve2 != null) &&
											($row->i_store != null) &&
											($row->i_nota != null) && ($row->d_sj_receive != null)
										) {
											$status = 'Sudah diterima';
										} elseif (
											($row->i_approve1 != null) &&
											($row->i_approve2 != null) &&
											($row->i_store != null) &&
											($row->i_nota != null) && ($row->d_sj_receive != null)
										) {
											$status = 'Sudah dinotakan';
										} elseif (($row->i_nota != null)) {
											$status = 'Sudah dinotakan';
										} else {
											$status = 'Unknown';
										}
										$bersih1 = floatval($row->v_spb) - floatval($row->v_spb_discounttotal);
										$nota1 = $row->v_nota_netto;
										if ($status == 'Batal') {
											$tmpspb = $tmpspb + 0;
											$tmpnota = $tmpnota + 0;
											$tmppendingan = $tmppendingan + 0;
											$pendingan = 0;
										} else {
											$tmpspb = $tmpspb + $bersih1;
											$tmpnota = $tmpnota + $nota1;
											$tmppendingan = $tmppendingan + ($bersih1 - $nota1);
											$pendingan = number_format($bersih1 - $nota1);
										}
										$bersih	= number_format(floatval($row->v_spb) - floatval($row->v_spb_discounttotal));
										if ($row->v_spb > 0) {
											$row->v_spb	= number_format($row->v_spb);
										} else {
											$row->v_spb = $row->v_spb;
										}
										if ($row->v_spb_discounttotal > 0) {
											$row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);
										} else {
											$row->v_spb_discounttotal = $row->v_spb_discounttotal;
										}
										$nota	= number_format($row->v_nota_netto);
										if ($row->i_product_group == '00') {
											$jenis = 'Home';
										} elseif ($row->i_product_group == '01') {
											$jenis = 'Bd';
										} elseif ($row->i_product_group == '06') {
											$jenis = 'Fs';
										} else {
											$jenis = 'NB';
										}
										echo "<tr> 
				  <td valign=top style=\"font-size: 13px;\">$row->i_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_appsales</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_appar</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_salesman</td>";
										if (substr($row->i_customer, 2, 3) != '000') {
											echo "
				  <td valign=top style=\"font-size: 13px;\">($row->i_customer) $row->e_customer_name</td>";
										} else {
											echo "
				  <td valign=top style=\"font-size: 13px;\">$row->xname</td>";
										}
										echo "
				  <td valign=top style=\"font-size: 13px;\">$row->i_area</td>
				  <td valign=top align=right style=\"font-size: 13px;\">$bersih</td>
          		  <td valign=top align=right style=\"font-size: 13px;\">$nota</td>
          		  <td valign=top align=right style=\"font-size: 13px;\">$pendingan</td>
				  <td valign=top align=right style=\"font-size: 13px;\">$persen%</td>
				  <td valign=top style=\"font-size: 13px;\">$status</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_sj</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_sj</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_dkb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_nota</td>
				<td valign=top style=\"font-size: 13px;\">$row->d_nota</td>
				<td valign=top style=\"font-size: 13px;\">$row->d_sj_receive</td>
				<td valign=top style=\"font-size: 13px;\">$row->i_spb_refference</td>
				  <td valign=top style=\"font-size: 13px;\">$daerah</td>
				  <td valign=top style=\"font-size: 13px;\">$jenis</td>
				  <td valign=top class=\"action\">";
										if ($row->i_spb_program != null) {
											if ($row->i_product_group == '00') {
												echo "	
							<a href=\"#\" onclick='show(\"spbpromoreguler/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											} elseif ($row->i_product_group == '01') {
												echo "	
							<a href=\"#\" onclick='show(\"spbpromobaby/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											} elseif ($row->i_product_group == '06') {
												echo "	
							<a href=\"#\" onclick='show(\"spbpromofashion/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											} else {
												echo "	
							<a href=\"#\" onclick='show(\"spbpromonb/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											}
										} else {
											if ($row->xname != '') {
												echo "
								<a href=\"#\" onclick='show(\"customernew/cform/edit/$row->i_spb/$row->i_area/$row->i_price_group/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
											} else {
												if ($row->i_product_group == '00') {
													echo "
								<a href=\"#\" onclick='show(\"spbreguler/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												} elseif ($row->i_product_group == '01' and substr($row->i_spb, 9, 2) == 'CA') {
													echo "
								<a href=\"#\" onclick='show(\"spbmo/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												} elseif ($row->i_product_group == '01') {
													echo "
								<a href=\"#\" onclick='show(\"spbbaby/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												} elseif ($row->i_product_group == '06') {
													echo "	
							<a href=\"#\" onclick='show(\"spbfashion/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												} else {
													echo "	
								<a href=\"#\" onclick='show(\"spbnb/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>";
												}
											}
										}
										if (($row->i_store == null) && ($row->i_approve1 == null) && ($row->i_approve2 == null)) {
											if (($row->f_spb_stockdaerah == 'f') && ($row->f_spb_op == 't')) {
												if ($iarea == '00') {
													/*echo "&nbsp;&nbsp;
						  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>"; */
													echo "&nbsp;&nbsp;
														<a href=\"javascript:zzz('" . $row->i_spb . "','" . $row->i_area . "','" . $dfrom . "','" . $dto . "','" . $iarea . "','" . trim($row->i_reff_advo) . "');\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
														</a>";
												}
											} else {
												if ($row->f_spb_cancel == 'f') {
													/* echo "&nbsp;&nbsp;
						  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>"; */
													echo "&nbsp;&nbsp;
														<a href=\"javascript:zzz('" . $row->i_spb . "','" . $row->i_area . "','" . $dfrom . "','" . $dto . "','" . $iarea . "','" . trim($row->i_reff_advo) . "');\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
														</a>";
												}
											}
										} elseif ($row->i_sj == null && $row->f_spb_cancel == 'f') {
											/* echo "&nbsp;&nbsp;
				  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\"></a>"; */
											echo "&nbsp;&nbsp;
														<a href=\"javascript:zzz('" . $row->i_spb . "','" . $row->i_area . "','" . $dfrom . "','" . $dto . "','" . $iarea . "','" . trim($row->i_reff_advo) . "');\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
														</a>";
										}
										if (($row->i_store == null) && ($row->i_sj == null) && ($row->f_spb_cancel == 'f') && ($this->session->userdata('user_id') == 'staff.salesadm1' || $this->session->userdata('user_id') == 'admin')) {
											echo "&nbsp;&nbsp;
					<a href=\"#\" onclick='balik(\"listspb/cform/balik/$row->i_spb/$row->i_area/$dfrom/$dto/\",\"#main\")'>
						<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/reverse.jpg\" border=\"0\" alt=\"balik\">
					</a>";
										}
										echo "</td></tr>";
									}
								}
								?>
							</tbody>
						</table>
						<?php // echo "<center>".$this->pagination->create_links()."</center>";
						?>
					</div>
				</div>
			</div>
			<?= form_close() ?>

			<?php
			if (($this->session->userdata("departement") == '4') || ($this->session->userdata('i_area') == '00')) {
				// $tujuan = 'listspb/cform/export';
				// echo $this->pquery->form_remote_tag(array('url' => $tujuan, 'update' => '#pesan', 'type' => 'post'));
			?>
				<input type="hidden" id="xcari" id="xcari" name="xcari" value="<?php echo $cari; ?>">
				&nbsp;<input type="hidden" id="xdfrom" name="xdfrom" value="<?php echo $dfrom; ?>">
				<input type="hidden" id="xdto" name="xdto" value="<?php echo $dto; ?>">
				<input type="hidden" id="xiarea" name="xiarea" value="<?php echo $iarea; ?>">
				<input type="hidden" id="xis_cari" name="xis_cari" value="1">
				<a href="#" id="href" value="Transfer" target="blank" onclick="return exportexcel();"><button>Export to Excel</button></a>
				<?= form_close() ?>
			<?php
			}
			?>
		</td>
	</tr>
</table>
<div id="pesan"></div>
<table class="listtable">
	<thead>
		<tr>
			<th>Total SPB</th>
			<th>Total Nota</th>
			<th>Total Pendingan</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo number_format($tmpspb); ?></td>
			<td><?php echo number_format($tmpnota); ?></td>
			<td><?php echo number_format($tmppendingan); ?></td>
		</tr>
	</tbody>
</table>
<br>
<input name="cmdreset" id="cmdreset" value="Export View" type="button">
<input name="kembali" id="kembali" value="Kembali" type="button" onclick='show("listspb/cform/index","#main");'>
<script language="javascript" type="text/javascript">
	$("#cmdreset").click(function() {
		var Contents = $('#sitabel').html();
		window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
	});

	function zzz(b, c, d, e, f, g) {
		showModal("listspb/cform/si_delete/" + b + "/" + c + "/" + d + "/" + e + "/" + f + "/" + g + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function exportexcel() {
		console.log("Masuk")
		var datefrom = document.getElementById('xdfrom').value;
		var dateto = document.getElementById('xdto').value;
		var iarea = document.getElementById('xiarea').value;
		var xis_cari = document.getElementById('xis_cari').value;
		var xcari = document.getElementById('xcari').value;

		var abc = "<?php echo site_url('listspb/cform/export/'); ?>/" + datefrom + "/" + dateto + "/" + iarea + "/" + xis_cari + "/" + xcari;
		$("#href").attr("href", abc);
		return true;
	}


	  fixedTable('table', 6);

	function fixedTable(table, left, right){
	var left     = left - 1;
	var tdlength = $($(table).find('tbody tr')[0]).find('td').length;
	var thWidth  = 0;
	var thWidthR = 0;

	if(window.screen.availWidth > 610){

		$($(table).find('th')).each(function(x, elm){
			if(elm.cellIndex <= left){
				elm.style.position = 'sticky';
				elm.style.zIndex = '1';
				elm.style.outline = '2px solid #f2f2f2';
				if(elm.previousElementSibling != null){
					thWidth = thWidth + elm.previousElementSibling.offsetWidth + 2;
					elm.style.left = thWidth + 'px';
				}else{
					elm.style.left = '0px';
				}
			}

			if(elm.cellIndex >= (tdlength-right)){
				elm.style.position = 'sticky';
				elm.style.outline = '2px solid #f2f2f2';
				if(elm.nextElementSibling != null){
					thWidthR = thWidthR + elm.nextElementSibling.offsetWidth + 2;
					elm.style.right = thWidthR + 'px';
				}else{
					elm.style.right = '0px';
				}
			}

		});

		$($(table).find('tbody tr')).each(function(xx, elm){

			var tdWidth  = 0;
			var tdWidthR = 0;
			$(elm).find('td').each(function(x, td){
				if(td.cellIndex <= left){
					td.style.position = 'sticky';
					td.style.outline = '2px solid #f2f2f2';
					if(td.previousElementSibling != null){
						tdWidth = tdWidth + td.previousElementSibling.offsetWidth + 2;
						td.style.left = tdWidth + 'px';
					}else{
						td.style.left = '0px';
					}
				}

				if(td.cellIndex >= (tdlength-right)){
					td.style.position = 'sticky';
					td.style.outline = '2px solid #f2f2f2';
					if(td.nextElementSibling != null){
						tdWidthR = tdWidthR + td.nextElementSibling.offsetWidth + 2;
						td.style.right = tdWidthR + 'px';
					}else{
						td.style.right = '0px';
					}
				}
			})
		})
	}
}
</script>