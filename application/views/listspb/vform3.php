<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
	$tujuan = 'listspb/cform/view2';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	    <thead>
	      <tr>
		<td colspan="20" align="center">
			Cari data : 
			<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" >
			&nbsp; <input type="hidden" name="dfrom" value= "<?php echo $dfrom; ?>" >
			<input type="hidden" name="dto" value= "<?php echo $dto; ?>" >
			<input type="hidden" name="iarea" value= "<?php echo $iarea; ?>" >
			<input type="hidden" name="is_cari" value= "1" >
			<input type="submit" id="bcari" name="bcari" value="Cari">
		</td>
	      </tr>
	    </thead>
	   	<th>No SPB</th>
			<th>Tgl SPB</th>
			<th>Tgl App Sls</th>
			<th>Tgl App AR</th>
			<th>Sls</th>
			<th>Lang</th>
			<th>Area</th>
			<th>SPB (Rp)</th>
			<th>Nota (Rp)</th>
			<th>Pendingan (Rp)</th>
			<th>%</th>
			<th>Stat</th>
			<th>SJ</th>
			<th>Tgl SJ</th>
			<th>Tgl DKB</th>
			<th>Nota</th>
			<th>Tgl Nota</th>
			<th>Drh</th>
			<th>Jns</th>
			<th class="action">Act</th>
	    <tbody>
	      <?php 
	      $tmpspb = 0;
	      $tmpnota = 0;
	      $tmppendingan = 0;
		if($isi){
			foreach($isi as $row){
        $que=$this->db->query(" select sum(n_order*v_unit_price) as order
                                from tm_spb_item
                                where i_area = '$row->i_area' and i_spb='$row->i_spb'");
		    if ($que->num_rows() > 0){
			    foreach($que->result() as $riw){
            $order=$riw->order;
          }
        }else{
          $order=0;
        }
        $que=$this->db->query(" select sum(b.n_deliver*b.v_unit_price)as deliver from tm_nota_item b, tm_nota c
                                where c.i_area = '$row->i_area' and c.i_spb='$row->i_spb' and
                                b.i_sj=c.i_sj and b.i_area=c.i_area");
		    if ($que->num_rows() > 0){
			    foreach($que->result() as $riw){
            $deliv=$riw->deliver;
          }
        }else{
          $deliv=0;
        }
        if($deliv==''){
          $persen='0.00';
        }else{
          $persen=number_format(($deliv/$order)*100,2);
        }
        if($row->f_spb_stockdaerah=='t')
        {
          $daerah='Ya';
        }else{
          $daerah='Tidak';
        }
        if($row->d_spb){
			    $tmp=explode('-',$row->d_spb);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_spb=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_appsales){
			    $tmp=explode('-',$row->d_appsales);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_appsales=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_appar){
			    $tmp=explode('-',$row->d_appar);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_appar=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_sj){
			    $tmp=explode('-',$row->d_sj);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_sj=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_dkb){
			    $tmp=explode('-',$row->d_dkb);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_dkb=$tgl.'-'.$bln.'-'.$thn;
        }
			  if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			  	$status='Batal';
			  }elseif(
				 	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					 ){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					  ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					  ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ar)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null)
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					 ){
			  	$status='OP Close';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap SJ (sales)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
#			  	$status='Siap SJ (gudang)';
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
        }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) 
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }
			  $bersih1 = $row->v_spb-$row->v_spb_discounttotal;
			  $nota1 = $row->v_nota_netto;
			  if($status == 'Batal'){
			  	$tmpspb = $tmpspb + 0;
			  	$tmpnota = $tmpnota + 0;
			  	$tmppendingan = $tmppendingan + 0;
			  	$pendingan = 0;
			  }else{
			  	$tmpspb = $tmpspb + $bersih1;
			  	$tmpnota = $tmpnota + $nota1;
			  	$tmppendingan = $tmppendingan + ($bersih1 - $nota1);
			  	$pendingan = number_format($bersih1 - $nota1);
			  }
			  $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
			  $row->v_spb	= number_format($row->v_spb);
 			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);
			  $nota	= number_format($row->v_nota_netto);
        if($row->i_product_group=='00') {
          $jenis='Home';
        } elseif($row->i_product_group=='01') {
          $jenis='Bd';
      	}elseif($row->i_product_group=='06') {
      		$jenis='Fs';
        } else {
          $jenis='NB';
        }
			  echo "<tr> 
				  <td valign=top style=\"font-size: 13px;\">$row->i_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_spb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_appsales</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_appar</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_salesman</td>";
				if(substr($row->i_customer,2,3)!='000'){
					echo "
				  <td valign=top style=\"font-size: 13px;\">($row->i_customer) $row->e_customer_name</td>";
				}else{
					echo "
				  <td valign=top style=\"font-size: 13px;\">$row->xname</td>";
				}
			  echo "
				  <td valign=top style=\"font-size: 13px;\">$row->i_area</td>
				  <td valign=top align=right style=\"font-size: 13px;\">$bersih</td>
          		  <td valign=top align=right style=\"font-size: 13px;\">$nota</td>
          		  <td valign=top align=right style=\"font-size: 13px;\">$pendingan</td>
				  <td valign=top align=right style=\"font-size: 13px;\">$persen%</td>
				  <td valign=top style=\"font-size: 13px;\">$status</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_sj</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_sj</td>
				  <td valign=top style=\"font-size: 13px;\">$row->d_dkb</td>
				  <td valign=top style=\"font-size: 13px;\">$row->i_nota</td>
  			  <td valign=top style=\"font-size: 13px;\">$row->d_nota</td>
				  <td valign=top style=\"font-size: 13px;\">$daerah</td>
				  <td valign=top style=\"font-size: 13px;\">$jenis</td>
				  <td valign=top class=\"action\">";
			  if($row->i_spb_program!=null){
					if($row->i_product_group=='00'){
						echo "	
							<a href=\"#\" onclick='show(\"spbpromoreguler/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}elseif($row->i_product_group=='01'){
						echo "	
							<a href=\"#\" onclick='show(\"spbpromobaby/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}elseif($row->i_product_group=='06'){
						echo "	
							<a href=\"#\" onclick='show(\"spbpromofashion/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";		
					}else{
						echo "	
							<a href=\"#\" onclick='show(\"spbpromonb/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}
				}else{
					if($row->xname!=''){
						echo "
								<a href=\"#\" onclick='show(\"customernew/cform/edit/$row->i_spb/$row->i_area/$row->i_price_group/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
					}else{
						if($row->i_product_group=='00'){
						echo "
								<a href=\"#\" onclick='show(\"spbreguler/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}elseif($row->i_product_group=='01' and substr($row->i_spb,9,2)=='CA'){
						echo "
								<a href=\"#\" onclick='show(\"spbmo/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}elseif($row->i_product_group=='01'){
						echo "
								<a href=\"#\" onclick='show(\"spbbaby/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}elseif($row->i_product_group=='06'){
						echo "	
							<a href=\"#\" onclick='show(\"spbpromofashion/cform/edit/$row->i_spb/$row->i_area/$row->i_spb_program/$dfrom/$dto/$iarea/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";		
					}else{
						echo "	
								<a href=\"#\" onclick='show(\"spbnb/cform/edit/$row->i_spb/$row->i_area/$dfrom/$dto/$iarea/$row->i_price_group/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
						}
					}
				}
			  if( ($row->i_store == null) && ($row->i_approve1==null) && ($row->i_approve2==null) )
			  {
				  if( ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_op == 't') )
          {
            if($iarea == '00')
				    {
          		echo "&nbsp;&nbsp;
						  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					  }
          }else{
					  if($row->f_spb_cancel == 'f')
				    {
          		echo "&nbsp;&nbsp;
						  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
					  }
          }
				}elseif($row->i_sj==null){
      		echo "&nbsp;&nbsp;
				  <a href=\"#\" onclick='hapus(\"listspb/cform/delete/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_spb_program/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";

        }
				echo "</td></tr>";				  
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php // echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>

<?php 
  if($this->session->userdata("departement")=='4'){
    $tujuan = 'listspb/cform/export';
    echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#pesan','type'=>'post'));
?>
			<input type="hidden" id="xcari" name="xcari" value="<?php echo $cari; ?>">
			&nbsp;<input type="hidden" name="xdfrom" value= "<?php echo $dfrom; ?>">
			<input type="hidden" name="xdto" value= "<?php echo $dto; ?>">
			<input type="hidden" name="xiarea" value= "<?php echo $iarea; ?>">
			<input type="hidden" name="xis_cari" value= "1">
			<input type="submit" id="xbcari" name="xbcari" value="TRansfer">
   <?=form_close()?>
<?php 
  }
?>
    </td>
  </tr>
</table>
	  <table class="listtable">
	  	<thead>
	  		<tr >
	  			<th>Total SPB</th>
	  			<th>Total Nota</th>
	  			<th>Total Pendingan</th>
	  		</tr>
	  	</thead>
	  	<tbody>
	  		<tr>
	  			<td><?php echo number_format($tmpspb); ?></td>
	  			<td><?php echo number_format($tmpnota); ?></td>
	  			<td><?php echo number_format($tmppendingan); ?></td>
	  		</tr>
	  	</tbody>
	  </table>
		<br>
		<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
		<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

</script>