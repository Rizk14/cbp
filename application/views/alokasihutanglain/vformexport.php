<title>Export List Hutang Lain-lain</title>
<link rel="stylesheet" type="text/css" href="<?php  echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php  echo "List Hutang Lain-lain Periode : ".$iperiode; ?></h2>
<?php 
  #include ("php/fungsi.php");
?>
<table class="maintable">
  <tr>
  <td align="left">
  <?php  echo  $this->pquery->form_remote_tag(array('url'=>'exp-dkb/cform/view','update'=>'#main','type'=>'post'));?>
  <div class="effect">
    <div class="accordion2">
      <table class="listtable" id="sitabel">
		<thead>
			<tr>
				<th style="color: white" width="2%"><div align="center">No</div></th>
				<th style="color: white" width="5%"><div align="center">Periode</div></th>
				<th style="color: white" width="5%"><div align="center">Kd Area</div></th>
				<th style="color: white" width="8%"><div align="center">No Bank Masuk</div></th>
				<th style="color: white" width="5%"><div align="center">Jumlah</div></th>
				<th style="color: white" width="5%"><div align="center">Sisa</div></th>
				<th style="color: white" width="5%"><div align="center">Lebih Bayar</div></th>
				<th style="color: white" width="5%"><div align="center">Coa Bank</div></th>
			</tr>
		</thead>
  
<?php 
$i=0;
$vtotjml=0;
$vtotsis=0;
foreach($isi as $row)
{
?>
	
  
<?php 
		$i++;
		if($row->f_lebih_bayar=='f'){
			$elebih = "Tidak";
		}else{
			$elebih = "Ya";
		}

		$vtotjml += $row->v_bank;
		$vtotsis += $row->v_sisa;
?>
  <tr align="center">
    <td width="2%"><?php echo $i; ?></td>
    <td width="5%"><?php echo $row->i_periode; ?></td>
    <td width="5%"><?php echo "'".$row->i_area; ?></td>
    <td width="8%"><?php echo $row->i_kbank; ?></td>
    <td style="width=5%; text-align-last: right" ><?php echo number_format($row->v_bank); ?></td>
    <td style="width=5%; text-align-last: right" ><?php echo number_format($row->v_sisa); ?></td>
    <td width="5%"><?php echo $elebih; ?></td>
    <td width="5%"><?php echo $row->i_coa_bank; ?></td>
  </tr>
<?php 
    }
?>
	<tr>
		<th style="color: white" colspan="4" align="center"><b>GRAND TOTAL</b></th>
		<th style="color: white"><b><?php echo number_format($vtotjml);?></b></th>
		<th style="color: white"><b><?php echo number_format($vtotsis);?></b></th>
		<th colspan="2"></th>
	</tr>
  </table>

<input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
</BODY>
</html>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>