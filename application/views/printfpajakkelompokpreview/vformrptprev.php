<?php 
	include ("php/fungsi.php");
  $isi=$master;
  if($isi){
    echo "<h2>$page_title</h2>";
    echo '<table class="listtable">';
    echo '<tr>
            <th>No</th>
            <th>Komersial</th>
            <th>Nota</th>
            <th>Kotor</th>
            <th>Discount</th>
            <th>Dpp</th>
            <th>PPN</th>
          </tr>';

    settype($iseri,"string");
    $satu =substr($iseri,0,14);
    $dua  =substr($iseri,14,5);
	  foreach($isi as $row){
      $cetak='';
		  $nor	= str_repeat(" ",5);
		  $abn	= str_repeat(" ",12);
		  $ab	  = str_repeat(" ",9);
		  $x1 	= str_repeat(" ",25);
		  $x2 	= str_repeat(" ",30);
		  $cetak.=CHR(15)."\n\n\n\n".CHR(18);
      if($row->f_pajak_pengganti=='t'){ 
        $ganti='1';
      }else{ 
        $ganti='0';
      }
      $tmp=explode("-",$row->d_nota);
		  $thn=substr($tmp[0],2,2);
      $bl=$tmp[1];
      $gabung=$thn.$bl;
      if($gabung>'1303'){
#        $nopajak='010.900-'.$thn.'.489'.$iseri;
        settype($dua,"string");
        $a=strlen($dua);
        while($a<5){
          $dua="0".$dua;
          $a=strlen($dua);
        }
        $iseri=$satu.$dua;
        $nopajak=$iseri;
      }else{
        $nopajak='01'.$ganti.'.000-'.$thn.'.00'.$iseri;
      }
  #    $nopajak='01'.$ganti.'.000-'.$thn.'.00'.$iseri;
		  $cetak.=$x2.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).$nopajak.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n\n\n";
		  $cetak.=$x1.NmPerusahaan."\n";
		  $cetak.=$x1.AlmtPerusahaan." ".KotaPerusahaan."\n\n";
		  $cetak.=$x1.NPWPPerusahaan."\n\n\n\n";
		  if( ($row->f_customer_pkp=='f') ){
        $cetak.=$x1.$row->e_customer_name."\n";
      }else{
    		$cetak.=$x1.$row->e_customer_pkpname."\n";
      }
      if($row->f_customer_pkp=='f') $row->e_customer_pkpaddress=$row->e_customer_city;
   		$pjg=strlen($row->e_customer_pkpaddress);
      if($pjg<=55){
        $cetak.=$x1.$row->e_customer_pkpaddress."\n\n";
      }else{
        $xx=str_repeat(" ",48);
        $cetak.=CHR(15).$xx.$row->e_customer_pkpaddress.CHR(18)."\n\n";
      }
      if( ($row->e_customer_pkpnpwp=='') || ($row->e_customer_pkpnpwp==null) ){
        $row->e_customer_pkpnpwp="00.000.000.0.000.000";
      }
		  $cetak.=$x1.$row->e_customer_pkpnpwp."\n\n\n\n";
		  $i=0;
      $total=0;
      $row->i_area=substr($row->i_nota,8,2);
		  $detail = $this->mmaster->bacadetail($row->i_sj,$row->i_nota,$row->i_area);
		  foreach($detail as $rowi){
        if($rowi->n_deliver>0){
				  $i++;
				  $name	= $rowi->e_product_name;
				  $pjg	= strlen($name);
				  $spcname	= 111;
				  for($xx=1;$xx<=$pjg;$xx++){
					  $spcname	= $spcname-1;
				  }
				  if($row->f_plus_ppn=='t'){
					  $tot	= number_format($rowi->n_deliver*$rowi->v_unit_price);
				  }else{
					  $tot	= number_format($rowi->n_deliver*($rowi->v_unit_price/1.1));
				  }
				  $pjg	= strlen($tot);
				  $spctot = 9;
				  for($xx=1;$xx<=$pjg;$xx++){
					  $spctot	= $spctot-1;
				  }
				  $aw=9;
				  $pjg	= strlen($i);
				  for($xx=1;$xx<=$pjg;$xx++){
					  $aw=$aw-1;
				  }
				  $aw=str_repeat(" ",$aw);
          $total	= $total+str_replace(',','',$tot);
  #        $cetak.=CHR(15).$aw.$i.".".str_repeat(" ",5).$name;.str_repeat(" ",$spcname).str_repeat(" ",$spctot).$tot."\n";
          $cetak.=CHR(15).$aw.$i.".".str_repeat(" ",5).$name;
          if($spcname>0){
				    $cetak.=str_repeat(" ",$spcname);
          }
          if($spctot>0){
            $cetak.=str_repeat(" ",$spctot).$tot."\n";
          }else{
            $cetak.=$tot."\n";
          }
			  }
		  }
      $enter="";
      $y=22-$i;
      for($x=1;$x<=$y;$x++){
        $enter.="\n";
      }
      $cetak.=CHR(18).$aw." (Faktur No. ".substr($row->i_faktur_komersial,8,6)."/".substr($row->i_nota,8,8).")";
      $cetak.=CHR(15)."$enter".CHR(18);
  ###################################

      $row->v_nota_gross=$total;
		  if(strlen(number_format($row->v_nota_gross))==1){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",48).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==4){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",45).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==5){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",44).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==6){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",43).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==7){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",42).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==8){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",41).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==9){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",40).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==10){
			  $cetak.=str_repeat(" ",11)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",39).number_format($row->v_nota_gross)."\n\n";
		  }
      $vdisc1=0;
      $vdisc2=0;
      $vdisc3=0;
      $vdisc4=0;
      if( ($row->n_nota_discount1+$row->n_nota_discount2+$row->n_nota_discount3+$row->n_nota_discount4==0) && $row->v_nota_discounttotal <> 0 )
      {
        if($row->f_plus_ppn=='t'){
          $vdisc1=$row->v_nota_discounttotal;
        }else{
          $vdisc1=$row->v_nota_discounttotal/1.1;
        }
      }else{
        $vdisc1=round($vdisc1+($total*$row->n_nota_discount1)/100);
      }

  #    $vdisc1=round($vdisc1+($total*$row->n_nota_discount1)/100);
	    $vdisc2=round($vdisc2+((($total-$vdisc1)*$row->n_nota_discount2)/100));
	    $vdisc3=round($vdisc3+((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100));
    	$vdisc4=round($vdisc4+((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100));
      $vdistot	= round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
    
      $row->v_nota_discount=$vdistot;
		  if(strlen(number_format($row->v_nota_discount))==1){
			  $cetak.=str_repeat(" ",45)."                                 ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==3){
			  $cetak.=str_repeat(" ",45)."                               ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==4){
			  $cetak.=str_repeat(" ",45)."                              ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==5){
			  $cetak.=str_repeat(" ",45)."                             ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==6){
			  $cetak.=str_repeat(" ",45)."                            ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==7){
			  $cetak.=str_repeat(" ",45)."                           ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==8){
			  $cetak.=str_repeat(" ",45)."                          ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==9){
			  $cetak.=str_repeat(" ",45)."                         ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==10){
			  $cetak.=str_repeat(" ",45)."                        ".number_format($row->v_nota_discount)."\n\n\n";
		  }
  #		$ndpp=round($row->v_nota_netto/1.1);
  #    $nppn=round($row->v_nota_netto/1.1*0.1);
      if($row->f_plus_ppn=='t'){
		    $ndpp=round(($row->v_nota_gross-$row->v_nota_discount)/1.1);
        $nppn=round((($row->v_nota_gross-$row->v_nota_discount)/1.1)*0.1);
        if(strlen(number_format($ndpp))==1){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."              ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==4){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."           ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==5){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."          ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==6){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."         ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==7){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."        ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==8){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."       ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==9){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."      ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==10){
		      $cetak.=str_repeat(" ",45)."100/110 X ".number_format($row->v_nota_gross-$row->v_nota_discount)."     ".number_format($ndpp)."\n";
	      }
      }else{
     		$ndpp=round($row->v_nota_gross-$row->v_nota_discount);
        $nppn=round(($row->v_nota_gross-$row->v_nota_discount)*0.1);
        if(strlen(number_format($ndpp))==1){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."              ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==4){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."           ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==5){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."          ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==6){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."         ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==7){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."        ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==8){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."       ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==9){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."      ".number_format($ndpp)."\n";
	      }elseif(strlen(number_format($ndpp))==10){
		      $cetak.=str_repeat(" ",55).str_repeat(" ",strlen(number_format($row->v_nota_gross-$row->v_nota_discount)))."     ".number_format($ndpp)."\n";
	      }
      }
      if(strlen(number_format($nppn))==1){
		    $cetak.=str_repeat(" ",45)."                                 ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==4){                                                   
		    $cetak.=str_repeat(" ",45)."                              ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==5){
		    $cetak.=str_repeat(" ",45)."                             ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==6){
		    $cetak.=str_repeat(" ",45)."                            ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==7){
		    $cetak.=str_repeat(" ",45)."                           ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==8){
		    $cetak.=str_repeat(" ",45)."                          ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==9){
		    $cetak.=str_repeat(" ",45)."                         ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==10){
		    $cetak.=str_repeat(" ",45)."                        ".number_format($nppn)."\n\n";
	    }else{
        $cetak.=str_repeat(" ",45)."                        ".number_format($nppn)."\n\n";
      }
      $tmp=explode("-",$row->d_nota);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dnota=$hr." ".mbulan($bl)." ".$th;
	    $cetak.=str_repeat(" ",45)."BANDUNG,       ".$dnota."\n\n\n\n\n";
      $cetak.=str_repeat(" ",55).ucwords(strtolower($nama))."\n\n";
      $cetak.=str_repeat(" ",57).ucwords(strtolower($jabatan));#"Spv. Adm.";
      $iseri=$iseri+1;
      settype($iseri,"string");
      $a=strlen($iseri);
      $thbl=$th.$bl;
      if($thbl>'201303'){
        settype($dua,"integer");
        $dua++;
#        while($a<5){
#          $iseri="0".$iseri;
#          $a=strlen($iseri);
#        }
      }else{
        while($a<6){
          $iseri="0".$iseri;
          $a=strlen($iseri);
        }
      }
      echo '<tr>
              <td>'.$nopajak.'</td>
              <td>'.substr($row->i_faktur_komersial,8,6).'</td>
              <td>'.substr($row->i_nota,8,8).'</td>
              <td align="right">'.number_format($row->v_nota_gross).'</td>
              <td align="right">'.number_format($row->v_nota_discount).'</td>
              <td align="right">'.number_format($ndpp).'</td>
              <td align="right">'.number_format($nppn).'</td>
            </tr>';
    }
    echo '</table>';
  }
?>
