<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: potrait;}
*/
*{
size: potrait;
}

@page { size: Letter; }
@page { size: Letter; margin: 0.29in 0.29in 0.3in 0.30in;}

.huruf {
  --FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
  FONT-FAMILY: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif; 
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.kotak {
  border-collapse: collapse;
  border-bottom-width: 0px; 
  border-right-width: 0px; 
  border-bottom:#000000 0.1px solid;
  border-left:#000000 0.1px solid;
  border-right:#000000 0.1px solid;
  border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
if($isi){
foreach($isi as $row)
{
  $tesi=0;
        $this->db->select(" sum(v_sisa) as total from tm_nota
                            where i_customer='$row->i_customer' 
                            and f_nota_cancel='f'",false);
        $query = $this->db->get();
        if ($query->num_rows() > 0){
          foreach($query->result() as $tes){
            $tesi=$tes->total;
          }
        }

        $tmp      = explode("-",$dto);
        $th       = $tmp[0];
        $bl       = $tmp[1];
        $hr       = $tmp[2];
        $pertgl   = $th." ".mbulan($bl)." ".$hr;

        $tmp1     = explode("-",date('d-m-Y'));
        $th1      = $tmp1[0];
        $bl1      = $tmp1[1];
        $hr1      = $tmp1[2];
        $tgl      = $th1." ".mbulan($bl1)." ".$hr1;

        $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $bulan = $array_bulan[date('n')];
        
}
}else{
  echo "Toko Tidak ada Piutang";
}
?>

<!--HEADER-->
  <table width="100%" border="0" class="nmper">
    <tr>
      <td width="18">&nbsp;</td>
      <td width="10">&nbsp;</td>
      <td width="144">&nbsp;</td>
      <td width="16">&nbsp;</td>
      <td width="326">&nbsp;</td>
    </tr>

    <tr>
      <td align="center" colspan="8" class="huruf judul">SURAT KONFIRMASI PIUTANG</td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3">Cimahi, <?php echo $tgl;?></td>
    </tr>

    <tr>  
      <td class="huruf nmper">No.</td>
      <td>:</td>
      <td>
        <?php echo 
          $no."/".$bulan."/"."AR"."/".$hr1
        ;?>
        </td>
    </tr>

    <tr>  
      <td class="huruf nmper">Hal</td>
      <td>:</td>
      <td>Konfirmasi Piutang</td>
    </tr>

    <tr>
      <td width="130" class="huruf nmper"></td>
      <td width="11">&nbsp;</td>
      <td width="329">&nbsp;</td>
    </tr>

    <tr>
      <td colspan="3" class="huruf nmper">Kepada Yang Terhormat</td>
    </tr>

    <tr>
      <td class="huruf nmper">Pimpinan Toko</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
    <tr>
      <td class="huruf nmper" colspan=3><?php  echo "(".$row->i_customer.") ".$row->e_customer_name; ?></td> <!-- ( <?php  echo  $row->i_customer; ?> )  -->
    </tr>

    <tr>
      <td class="huruf nmper" colspan=5>
        <?php  
            echo $row->e_customer_address." "; 
            echo "- ".$row->e_customer_city;
        ?>
      </td>
      <td>&nbsp;</td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td colspan="3" class="huruf nmper">Dengan Hormat,</td>
    </tr>
    
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td colspan="6" class="huruf nmper">
        <p>
          Bersama surat ini kami mohon bantuan dan kerjasama Bapak/Ibu untuk memeriksa kebenaran dari data piutang dibawah ini :
        </p>
        <p>
          Menurut catatan kami Saldo Piutang Toko <?php echo $row->e_customer_name;?> kepada <?php echo NmPerusahaan;?></br>
          sebesar Rp. <?php echo number_format("$tesi");?> 
    (<?php 
        $bilangan = new Terbilang;
        $kata     = ucwords($bilangan->eja($tesi));  
        $tmp      = explode("-",$row->d_nota);
        $th       = $tmp[0];
        $bl       = $tmp[1];
        $hr       = $tmp[2];
        $dnota    = $hr." ".mbulan($bl)." ".$th;
          echo $kata." Rupiah";
    ?>) 
          per tanggal <?php echo $pertgl ;?> dengan rincian sebagai berikut :
        </p>
      </td>
    </tr>


    </tr>
  </table>

 <!--END HEADER--> 
  

<!--HEADER 2-->
<table border="1" align="center" class="kotak" ">
  <!-- <tr>
    <td colspan=6 class=garisbawah>&nbsp;</td>
  </tr> -->
  <tr align="center">
    <td width="30" class="huruf nmper ">No</td>
    <td width="160" class="huruf nmper ">No Nota</td>
    <td width="100" class="huruf nmper ">Tanggal Nota</td>
    <td width="100" class="huruf nmper ">Jumlah</td>
    <!-- <td width="103" class="huruf nmper">Harga Satuan</td>
    <td width="152" class="huruf nmper">Banyak yg dipenuhi</td>
    <td width="70" class="huruf nmper">Motif</td> 
    <td colspan=6 class=garisbawah>&nbsp;</td>-->
  </tr>
    <tr>
  </tr>
</table>
<!--END HEADER 2-->

<?php 
		$i=0;	
		foreach($isi as $rowi){
			$i++;
			//$hrg=number_format($rowi->v_unit_price);
			$inota	= $rowi->i_nota;
      $dnota  = $rowi->d_nota;
      $jml    = $rowi->v_sisa;

      //if($rowi->i_product_status=='4') $rowi->e_product_name='* '.$rowi->e_product_name;
			//$name	= $rowi->e_product_name.str_repeat(" ",46-strlen($rowi->e_product_name ));
			//$motif	= $rowi->e_remark;
			//$orde	= number_format($rowi->n_order);
			//$deli	= number_format($rowi->n_deliver);
			//$aw		= 13;
			/*$pjg	= strlen($i);
			for($xx=1;$xx<=$pjg;$xx++){
				$aw=$aw-1;
			}
			$pjg	= strlen($orde);
			$spcord	= 4;			
			for($xx=1;$xx<=$pjg;$xx++){
				$spcord	= $spcord-1;
			}
			$pjg	= strlen($hrg);
			$spcprc	= 13;
			for($xx=1;$xx<=$pjg;$xx++){
				$spcprc	= $spcprc-1;
			}*/
?>

<!--ISINYA-->
<table border="1" align="center" class="kotak">
  <tr align="center" >
    <td width="30" class="huruf nmper"><?php  echo  $i; ?></td>
    <td align="left" width="160" class="huruf nmper"><?php  echo  $inota; ?></td>
    <td width="100" class="huruf nmper"><?php  echo  $dnota; ?></td>
    <td align="left" width="100" class="huruf nmper" align="center"><?php  echo  "Rp. ". number_format($jml); ?> </td>
    <!-- <td width="25"></td>
    <td width="90" class="huruf nmper"><?php  echo  $hrg; ?></td>
    <td width="95" class="huruf nmper">_____________</td>
    <td width="100" class="huruf nmper"> &nbsp;&nbsp;&nbsp;<?php  echo  $motif; ?></td> -->
  </tr>
</table>
<!--END ISINYA-->

<?php 
}
?>

<!--FOOTER-->
<table width="100%" border="0" class="nmper">
    <tr>
      <td width="18">&nbsp;</td>
      <td width="24">&nbsp;</td>
      <td width="144">&nbsp;</td>
      <td width="16">&nbsp;</td>
      <td width="326">&nbsp;</td>
    </tr>

    <tr>
      <td colspan="6" class="huruf nmper">
        <p>
          Setelah Bapak/Ibu menerima surat konfirmasi piutang ini, sudi kiranya Bapak/Ibu memberikan jawaban atas kebenaran piutang tersebut dan segera melunasinya. Jika saldo piutang tersebut tidak cocok dengan catatan Bapak/Ibu, mohon untuk menjelaskannya dengan menggunakan formulir di lembar kedua.
        </p>

        <p>
          Demikian kami sampikan atas bantuan dan kerjasama yang Bapak/Ibu berikan kami ucapkan terimakasih.
        </p>        
      </td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3">Hormat Kami,</td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3"><?php echo TahuKN ;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" colspan="3"><?php echo jabatan ;?></td>
    </tr>
</table>
<!--END FOOTER-->

<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
