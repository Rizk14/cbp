<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Cetak Konfirmasi Piutang</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: potrait;}
*/
*{
size: potrait;
}

/*@page { size: Letter; margin: 0.29in 0.29in 0.3in 0.60in;}*/
@page{
    size: 21cm 29.7cm;
    margin: 0.10in 0.79in 0.10in 0.79in /*(29mm top) (29mm right) (30mm bottom) (30mm left)*/
    ;}

.huruf {
  /*FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;*/
  FONT-FAMILY: TimesNewRoman,Times New Roman,Times,Baskerville,Georgia,serif; 
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 15px;
  FONT-WEIGHT: normal; 
}
p {
  text-align: justify;
  text-align-last: left;
  /*white-space: pre-line;*/
    -moz-text-align-last: left;
    resize: none;
  }
.isi {
  font-size: 15px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.kotak {
  border-collapse: collapse;
  border-bottom-width: 0px; 
  border-right-width: 0px; 
  border-bottom:#000000 0.1px solid;
  border-left:#000000 0.1px solid;
  border-right:#000000 0.1px solid;
  border-top:#000000 0.1px solid;
}
</style>

<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>

<?php 
if($isi){
foreach($isi as $row)
{
    $tesi=0;
    
        $this->db->select(" sum(total) as totalpiutang from(
                            select sum(v_sisa)as total from tm_nota
                            where (i_customer='$row->i_customer' or 
                            i_customer in(select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$row->i_customer'))
                            and f_nota_cancel='f') as a",false);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
          foreach($query->result() as $tes)
          {
            $tesi=$tes->totalpiutang;
          }
        }
  
    $tmp      = explode("-",$dto);
    $th       = $tmp[0];
    $bl       = $tmp[1];
    $hr       = $tmp[2];
    $pertgl   = $th." ".mbulan($bl)." ".$hr;

    $tmp1     = explode("-",date('d-m-Y'));
    $th1      = $tmp1[0];
    $bl1      = $tmp1[1];
    $hr1      = $tmp1[2];
    $tgl      = $th1." ".mbulan($bl1)." ".$hr1;

    $array_bulan  = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
    $bulan        = $array_bulan[date('n')];

    $bilangan = new Terbilang;
    $kata     = ucwords($bilangan->eja($tesi));  
};
?>

<!--HEADER-->
  <table width="100%" border="0" class="nmper">
    <tr>
      <td width="18">&nbsp;</td>
      <td width="10">&nbsp;</td>
      <td width="144">&nbsp;</td>
      <td width="16">&nbsp;</td>
      <td width="326">&nbsp;</td>
    </tr>

    <tr>
      <td colspan="6">
       <center>
        <img src="<?php echo base_url().'img/kop.png';?>" border="0" >
       </center>
      </td>
    </tr>

    <tr>
      <td align="center" colspan="8" class="huruf judul">
        <b>SURAT KONFIRMASI PIUTANG</b>
      </td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3">Cimahi, <?php echo $tgl;?></td>
    </tr>

    <tr>  
      <td class="huruf nmper" colspan="3">
        No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
        <?php echo 
          $no."/".$bulan."/"."AR"."/".$hr1
        ;?>
      </td>
    </tr>

    <tr>  
      <td class="huruf nmper" colspan="3">
        Hal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
        Konfirmasi Piutang
      </td>
    </tr>

    <tr>
      <td width="130" class="huruf nmper"></td>
      <td width="11">&nbsp;</td>
      <td width="329">&nbsp;</td>
    </tr>

    <tr>
      <td colspan="3" class="huruf nmper">Kepada Yang Terhormat</td>
    </tr>

    <tr>
      <td class="huruf nmper">Pimpinan Toko</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
    <tr>
      <td class="huruf nmper" colspan=3><?php  echo "(".$row->i_customer.") ".$row->e_customer_name; ?></td>
    </tr>

    <tr>
      <td class="huruf nmper" colspan=4>
        <?php  
            echo $row->e_customer_address." "; 
            echo "- ".$row->e_customer_city;
        ?>
      </td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td colspan="3" class="huruf nmper">Dengan Hormat,</td>
    </tr>
    
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td colspan="6" class="huruf nmper">
        <p>
          Bersama surat ini kami mohon bantuan dan kerjasama Bapak/Ibu untuk memeriksa kebenaran dari data piutang dibawah ini :
        </p>
        
        <p>
          Menurut catatan kami Saldo Piutang Toko <?php echo $row->e_customer_name;?> kepada <?php echo NmPerusahaan;?></br>
          sebesar Rp. <?php echo number_format("$tesi");?> (<?php echo $kata." Rupiah";?>) 
          per tanggal <?php echo $pertgl ;?> dengan rincian sebagai berikut :
        </p>
      </td>
    </tr>
    </tr>
  </table>
<!--END HEADER--> 
  
<!--HEADER TABEL-->
<table border="1" align="center" class="kotak" ">
  <tr align="center">
    <td width="30" class="huruf nmper">No</td>
    <td width="120" class="huruf nmper">No SJ</td>
    <td width="120" class="huruf nmper">No Nota</td>
    <td width="100" class="huruf nmper">Tanggal Nota</td>
    <td width="100" class="huruf nmper">Jumlah</td>
  </tr>
  <tr></tr>
</table>
<!--END HEADER TABEL-->

<?php 
$i=0;
foreach($isi as $row)
{
  $i++;
  $tmp2     = explode("-",$row->d_nota);
  $th2      = $tmp2[0];
  $bl2      = $tmp2[1];
  $hr2      = $tmp2[2];
  $dnota    = $hr2."-".$bl2."-".$th2;

  $inota    = $row->i_nota;
  $sj       = $row->i_sj;
  $jml      = $row->v_sisa;
;?>

<!--ISINYA-->
<table border="1" align="center" class="kotak">
  <tr align="center" >
    <td width="30" class="huruf nmper"><?php  echo  $i; ?></td>
    <td align="left" width="120" class="huruf nmper"><?php  echo  $sj; ?></td>
    <td align="left" width="120" class="huruf nmper"><?php  echo  $inota; ?></td>
    <td width="100" class="huruf nmper"><?php  echo  $dnota; ?></td>
    <td align="left" width="100" class="huruf nmper" align="center"><?php  echo  "Rp. ". number_format($jml); ?> 
    </td>
  </tr>
</table>
<!--END ISINYA-->
<?php };?>

<!--FOOTER-->
<table width="100%" border="0" class="nmper">
    <tr>
      <td colspan="6" class="huruf nmper">
        <p>
          Setelah Bapak/Ibu menerima surat konfirmasi piutang ini, sudi kiranya Bapak/Ibu memberikan jawaban atas kebenaran piutang tersebut dan segera melunasinya. Jika saldo piutang tersebut tidak cocok dengan catatan Bapak/Ibu, mohon untuk menjelaskannya dengan menggunakan formulir di lembar kedua.
        </p>

        <p>
          Demikian kami sampikan atas bantuan dan kerjasama yang Bapak/Ibu berikan kami ucapkan terimakasih.
        </p>        
      </td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3">Hormat Kami,</td>
    </tr>

    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>

    <tr>
      <td class="huruf nmper" colspan="3"><?php echo TahuKN ;?></td>
    </tr>
    <tr>
      <td class="huruf nmper" colspan="3"><?php echo jabatan ;?></td>
    </tr>
</table>
<!--END FOOTER-->

<div class="noDisplay">
    <center>
      <b>
        <a href="#" onClick="window.print()">Print</a>
      </b>
    </center>
</div>

<?php 
  }else{
    echo "Toko Tidak Ada Piutang";
  }
;?>