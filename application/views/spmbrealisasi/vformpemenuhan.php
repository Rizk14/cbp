<?php
echo "<h2>$page_title</h2>";
?>
<table class="maintable">
	<tr>
		<td align="left">
			<!--<?php echo form_open('spmbrealisasi/cform/update', array('id' => 'spmbform', 'name' => 'spmbform', 'onsubmit' => 'sendRequest(); return false')); ?>-->
			<?php echo $this->pquery->form_remote_tag(array('url' => 'spmbrealisasi/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="spbformupdate">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td width="10%">Tgl SPMB</td>
								<?php
								$tmp = explode("-", $isi->d_spmb);
								$th = $tmp[0];
								$bl = $tmp[1];
								$hr = $tmp[2];
								$dspmb = $hr . "-" . $bl . "-" . $th;
								?>
								<td><input id="ispmb" name="ispmb" type="text" readonly value="<?php echo $isi->i_spmb; ?>">
									<input readonly id="dspmb" name="dspmb" onclick="showCalendar('',this,this,'','dspmb',0,20,1)" value="<?php echo $dspmb; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Area</td>
								<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
									<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
								</td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4">
									<input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
									<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spmbrealisasi/cform/","#tmp")'>
								</td>
							</tr>
						</table>
						<div id="detailheader" align="center">
							<table class="listtable" align="center" style="width: 820px;">
								<th style="width:30px;" align="center">No</th>
								<th style="width:70px;" align="center">Kategori Penjualan</th>
								<th style="width:70px;" align="center">Kode</th>
								<th style="width:350px;" align="center">Nama Barang</th>
								<th style="width:50px;" align="center">Motif</th>
								<th style="width:47px;" align="center">Jml Pesan</th>
								<th style="width:75px;" align="center">Keterangan</th>
								<th style="width:46px;" align="center" class="action">Jml Stock</th>
								<th style="width:46px;" align="center" class="action">Pemnhn</th>
							</table>
						</div>
						<div id="detailisi" align="center">
							<?php
							$i = 0;
							$jmlitem = 0;
							foreach ($detail as $row) {
								if ($row->n_acc > 0) {
									$jmlitem++;
									echo '<table class="listtable" align="center" style="width:820px;">';
									$this->db->select("n_quantity_stock from tm_ic
									             where i_product='$row->i_product' and i_product_grade='$row->i_product_grade' 
							                 and i_product_motif='$row->i_product_motif' and i_store='AA' 
							                 and i_store_location='01' and i_store_locationbin='00'
								               order by e_product_name asc ", false);
									$query = $this->db->get();
									if ($query->num_rows() > 0) {
										foreach ($query->result() as $ic) {
											if ($ic->n_quantity_stock < 0) {
												$nstock = 0;
											} else {
												$nstock = $ic->n_quantity_stock;
											}
										}
									} else {
										$nstock = 0;
									}
									$i++;
									$pangaos = number_format($row->v_unit_price);
									$total = $row->v_unit_price * $row->n_acc;
									$total = number_format($total);
									$acc = number_format($row->n_acc);
									$ord = number_format($row->n_order);
									if ($nstock >= $row->n_acc) {
										$nstock = number_format($row->n_acc);
										$stock = number_format($row->n_acc);
									} else {
										$stock = $nstock; #number_format($ic->n_quantity_stock);
									}
									echo "<tbody>
								<tr>
				  				<td style=\"width:25px;\"><input style=\"width:25px;\" readonly type=\"text\" 
									id=\"baris$i\" name=\"baris$i\" value=\"$i\">
															<input type=\"hidden\" 
									id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
									<td style=\"width:67px;\"><input style=\"width:67px;\" readonly type=\"text\" 
									id=\"esalescategory$i\" name=\"esalescategory$i\" value=\"$row->e_sales_categoryname\"></td>
									<td style=\"width:67px;\"><input style=\"width:67px;\" readonly type=\"text\" 
									id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:320px;\"><input style=\"width:320px;\" readonly type=\"text\" 
									id=\"eproductname$i\"
									name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:48px;\">
									<input readonly style=\"width:48px;\" type=\"text\" id=\"emotifname$i\"
									name=\"emotifname$i\" value=\"$row->e_product_motifname\">
									<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$pangaos\"></td>
								<td style=\"width:47px;\"><input style=\"text-align:right; width:47px;\" readonly
									type=\"text\" id=\"nacc$i\" name=\"nacc$i\" value=\"$acc\"><input type=\"hidden\" id=\"norder$i\" name=\"norder$i\" value=\"$ord\"></td>
								<td style=\"width:86px;\">
									<input style=\"text-align:left; width:86px;\" readonly
									type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\">
									<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
								<td style=\"width:45px;\"><input readonly style=\"text-align:right; width:45px;\"  
									type=\"text\" id=\"nqtystock$i\" name=\"nqtystock$i\" value=\"$stock\"></td>
								<td style=\"width:62px;\"><input style=\"text-align:right; width:62px;\"  
									type=\"text\" id=\"nstock$i\" name=\"nstock$i\" value=\"$nstock\"></td>
								</tr>
								</tbody></table>";
								}
							}
							?>
						</div>
						<div id="pesan"></div>
						<input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
																		echo "value=\"$jmlitem\"";
																	} else {
																		echo "value=\"0\"";
																	} ?>>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(a) {
		cek = 'false';
		if ((document.getElementById("dspmb").value != '') &&
			(document.getElementById("iarea").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("iproduct" + i).value == '') ||
						(document.getElementById("eproductname" + i).value == '') ||
						(document.getElementById("nacc" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						exit();
						cek = 'false';
					} else {
						cek = 'true';
					}
				}
			}
			if (cek == 'true') {
				document.getElementById("login").hidden = true;
			} else {
				document.getElementById("login").hidden = false;
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}
	}

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").hidden = false;
	}
</script>