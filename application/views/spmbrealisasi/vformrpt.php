<?php include("php/fungsi.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Print Form Realisasi SPB</title>
</head>

<body>
  <style type="text/css" media="all">
    {
      size: potrait;
    }

    @page {
      size: Letter;
    }

    @page {
      size: Letter;
      margin: 0.29in 0.29in 0.3in 0.30in;
    }

    .huruf {
      FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
    }

    .miring {
      font-style: italic;

    }

    .ceKotak {
      border-collapse: collapse;
      border-bottom-width: 0px;
      border-right-width: 0px;
      border-bottom: #000000 0.1px solid;
      border-left: #000000 0.1px solid;
      border-right: #000000 0.1px solid;
      border-top: #000000 0.1px solid;
    }

    .garis {
      background-color: #000000;
      width: 100%;
      height: 50%;
      font-size: 100px;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garis td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .garisy {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisy td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      padding: 1px;
    }

    .garisx {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: none;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisx td {
      background-color: #FFFFFF;
      border-style: none;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .judul {
      font-size: 20px;
      FONT-WEIGHT: normal;
    }

    .nmper {
      font-size: 14px;
      FONT-WEIGHT: normal;
    }

    .isi {
      font-size: 14px;
      font-weight: normal;
      padding: 1px;
    }

    .eusinya {
      font-size: 10px;
      font-weight: normal;
    }

    .garisbawah {
      border-bottom: #000000 0.1px solid;
    }

    .prioritas {
      font-size: 50px;
    }
  </style>
  <style type="text/css" media="print">
    .noDisplay {
      display: none;
    }

    .pagebreak {
      page-break-before: always;
    }
  </style>

  <table width="100%" border="0" class="nmper">
    <tr>
      <td colspan="8" style="width:550px" class="judul">
        <?php echo NmPerusahaan; ?>
      </td>
    </tr>
    <tr>
      <td colspan="8">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="8" class="nmper"><b>Formulir Penyiapan SPmB</b></td>
    </tr>
    <tr>
      <td colspan="8">&nbsp;</td>
    </tr>

    <tr>
      <td style="width:350px" class="nmper">Data Cabang</td>
      <td style="width:8px" class="nmper">:</td>
      <td style="width:950px" class="nmper"><?= $isi->e_area_address ?></td>
      <td style="width:80px">&nbsp;</td>
      <td style="width:80px">&nbsp;</td>
      <td style="width:520px" class="nmper">No. SPmB</td>
      <td style="width:8px" class="nmper">:</td>
      <td style="width:700px" class="nmper"><?= $isi->i_spmb ?></td>
    </tr>

    <tr>
      <td style="width:350px" class="nmper"></td>
      <td style="width:8px" class="nmper"></td>
      <td rowspan="2" colspan="2" style="width:950px" class="nmper"></td>
      <td style="width:80px">&nbsp;</td>
      <td style="width:520px" class="nmper">Tanggal</td>
      <td style="width:8px" class="nmper">:</td>
      <td style="width:700px" class="nmper">
        <?php
        $tmp = explode("-", $isi->d_spmb);
        $th = $tmp[0];
        $bl = $tmp[1];
        $hr = $tmp[2];
        $dspb = $hr . " " . mbulan($bl) . " " . $th;
        echo $dspb; ?>
      </td>
    </tr>

    <tr>
      <td style="width:350px" class="nmper"></td>
      <td style="width:8px" class="nmper"></td>
      <td style="width:950px" class="nmper"></td>
      <td style="width:520px" class="nmper"></td>
      <td style="width:8px" class="nmper"></td>
      <td style="width:700px" class="nmper">

      </td>
    </tr>

    <tr>
      <td style="width:350px" class="nmper"></td>
      <td style="width:8px" class="nmper"></td>
      <td style="width:950px" class="nmper"></td>
      <td style="width:80px">&nbsp;</td>
      <td style="width:80px">&nbsp;</td>
      <td style="width:520px" class="nmper">Inisial Nama <i>Helper</i></td>
      <td style="width:8px" class="nmper">:</td>
      <td style="width:700px" class="nmper"></td>
    </tr>
  </table>

  <table border="1" class="ceKotak nmper" width="100%">
    <tr>
      <thead>
        <th>No Urut</th>
        <th>Kode Barang</th>
        <th>Nama Barang</th>
        <th>Motif</th>
        <th>Banyak yang dipesan</th>
        <th>Banyak yang diambil di Rak</th>
        <th>Banyak pemenuhan dari <i>Supplier</i></th>
      </thead>

      <tbody>
        <?php
        if ($detail) {
          $no = 1;
          foreach ($detail as $row) {
            if (strlen($row->e_product_name) > 46) {
              $name = $row->e_product_name . str_repeat(" ", 1);
            } else {
              $name = $row->e_product_name . str_repeat(" ", 46 - strlen($row->e_product_name));
            }

            echo "
                    <tr>
                      <td align='center'>$no</td>
                      <td align='center'>$row->i_product</td>
                      <td style=\"width:320px\">$name</td>
                      <td align='center'>$row->e_remark</td>
                      <td align='center'>$row->n_acc</td>
                      <td></td>
                      <td></td>
                    </tr>
                  ";
            $no++;
          }
        }
        ?>
      </tbody>
    </tr>
  </table>

  <h4>Keterangan : <?= $isi->e_remark ?></h2> </br>

    <center>
      <table border="1" class="ceKotak nmper" width="80%">
        <tr>
          <tfoot>
            <tr align="center">
              <td colspan="2">Penyiapan Barang Sesuai SPMB</td>
              <td colspan="3">Gudang</td>
              <td>Total Jumlah Packing</td>
            </tr>
            <tr align="center">
              <td>Tgl. & Jam Terima</td>
              <td>Tgl. & Jam Selesai</td>
              <td style="width:60px">Cek I</td>
              <td style="width:60px">Cek II</td>
              <td style="width:60px">Cek III</td>
              <td rowspan="2">&nbsp;</td>
            </tr>
            <tr align="center">
              <td class="garisbawah">&nbsp;</td>
              <td class="garisbawah">&nbsp;</td>
              <td class="garisbawah">&nbsp;</td>
              <td class="garisbawah">&nbsp;</td>
              <td class="garisbawah" style="height:50px">&nbsp;</td>
            </tr>

          </tfoot>
        </tr>
      </table>
    </center>
    </br>

    <div class="noDisplay">
      <center><button class="noDisplay" onClick="window.print()">Print</button></center>
    </div>
</body>

</html>

<script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.3.2.js"></script>
<script type="text/javascript">
  window.onafterprint = function() {
    var ispmb = '<?php echo $ispmb ?>';
    $.ajax({
      type: "POST",
      url: "<?php echo site_url('spmbrealisasi/cform/updateprint'); ?>",
      data: "ispmb=" + ispmb,
      success: function(data) {
        opener.window.refreshview();
        setTimeout(window.close, 0);
      },
      error: function(XMLHttpRequest) {
        alert('fail');
      }
    });
  }
</script>