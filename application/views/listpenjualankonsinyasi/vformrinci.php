<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<h2><?php echo $page_title; ?></h2>
<?php 
foreach($isi as $row)
{
  $namacust=$row->e_customer_name;
  break;
}
?>
<h3>&nbsp;&nbsp;&nbsp;<?php echo 'Nama Pelanggan : '.$cust.' - '.$namacust; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
	$tujuan = 'listpenjualankonsinyasi/cform/simpan';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#pesan','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	      <?php 
		if($isi){
?>
	   	<th>Tgl Bon</th>
<?php 
      foreach($diskon as $row)
      {
  		  echo "<th> Jml Pcs ".number_format($row->diskon)." %</th>";
  		  echo "<th> Kotor ".number_format($row->diskon)." %</th>";
      }
?>
	    <tbody>
	      <?php 
        $i=0;
        $disc='';
        $date='';
        $jmltot=count($diskon);
        $pos=0;
        foreach($isi as $raw)
        {
          $x=0;
          if($date==''){
            $i++;
            echo "<tr><td style='font-size:11px;'>$raw->d_notapb</td>";
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->jumlah)."</td>
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }else{
                echo "
                      <td style='font-size:11px;text-align:right;'>0</td>
                      <td style='font-size:11px;text-align:right;'>0</td>";
              }
            }
          }
          if($date==$raw->d_notapb){
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->jumlah)."</td>
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }elseif($x>$pos){
                echo "
                      <td style='font-size:11px;'text-align:right;>0</td>
                      <td style='font-size:11px;text-align:right;'>0</td>";
              }
            }
          }
          if($date!=$raw->d_notapb && $date!=''){
            while($pos<$jmltot){
              echo "  <td style='font-size:11px;text-align:right;'>0</td>
                      <td style='font-size:11px;text-align:right;'>0</td>";
              $pos++;
            }
            if($pos==$jmltot){
              echo "</tr>";
            }
            $i++;
            echo "<tr><td style='font-size:11px;'>$raw->d_notapb</td>";
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->jumlah)."</td>
                        <td style='font-size:11px;text-align:right;'>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }elseif($x<$pos){
                echo "
                      <td style='font-size:11px;text-align:right;'>0</td>
                      <td style='font-size:11px;text-align:right;'>0</td>";
              }
            }
          }
          $date=$raw->d_notapb;
        }
        while($pos<$jmltot){
          echo "  <td style='font-size:11px;text-align:right;'>0</td>
                  <td style='font-size:11px;text-align:right;'>0</td>";
          $pos++;
        }
        if($pos==$jmltot){
          echo "</tr>";
        }
        $col=($jmltot*2)+3;
		}
	          ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    <?=form_close()?>
    </div>
    </div>
    </td>
  </tr>
</table>
