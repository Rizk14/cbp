<h2><?php echo $page_title;?></h2>
<div id="tmp">
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url'=>'spbrealisasiulang/cform/view','id'=>'listform','update'=>'#main','type'=>'post'));?>
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="10" align="center">Cari data :
										<input type="text" id="cari" name="cari" value="<?php echo $cari?>">&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
										<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom?>">
										<input type="hidden" id="dto" name="dto" value="<?php echo $dto?>">
										<input type="hidden" id="iarea" name="iarea" value="<?php echo $area?>">
									</td>
								</tr>
							</thead>

						</table>
						<?=form_close()?>
						<?php echo $this->pquery->form_remote_tag(array('url'=>'spbrealisasiulang/cform/closing','id'=>'listform','update'=>'#main','type'=>'post'));?>
						
						<table class="listtable">
							<th>Area</th>
							<th>Pelanggan</th>
							<th>No SPB</th>
							<th>Tgl SPB</th>
							<th>Daerah</th>
							<th class="action">Action</th>
							<tbody>
								<?php 
									if($isi){
										$i=0;
										foreach($isi as $row){
											
											if($row->f_spb_stockdaerah=='t')
											{
												$daerah='Ya';
											}else{
												$daerah='Tidak';
											}

											$i++;
											echo "  <tr> 
														<td>
															<input type='hidden' id='iarea".$i."' name='iarea".$i."' value='$row->i_area'>
															$row->e_area_name
														</td>
														<td>$row->i_customer". ' - ' ."$row->e_customer_name</td>
														<td>
															<input type='hidden' id='ispb".$i."' name='ispb".$i."' value='$row->i_spb'>
															$row->i_spb
														</td>
														<td>$row->d_spb</td>
														<td>$daerah</td>
														<td class=\"action\">
															<input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'>
														</td>
													</tr>
											";
										}
									echo "<tr>
											<td colspan='10' align='center'>
												<input type='submit' id='transfer' name='transfer' value='Proses'>
											</td>
										  </tr>
										<input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">";
									}?>
									<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom?>">
									<input type="hidden" id="dto" name="dto" value="<?php echo $dto?>">
									<input type="hidden" id="iarea" name="iarea" value="<?php echo $area?>">
							</tbody>
						</table>
						<?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
					</div>
				</div>
				<?=form_close()?>
				<table class="listtable">
					<tr>
						<td align="center">
							<input type="button" id="pilihsemua" name="pilihsemua" value="Pilih Semua"
								onclick="pilihsemua()">
							<input type="button" id="tidakpilihsemua" name="tidakpilihsemua" value="Tidak Semua"
								onclick="tidakpilihsemua()">
							<!-- <input type="button" id="refresh" name="refresh" value="Refresh" onclick="batal()"> -->
							<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('spbrealisasiulang/cform/','#main')">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
function pilihsemua(){
var jml=parseFloat(document.getElementById("jml").value);
for(i=1;i<=jml;i++){
document.getElementById("chk"+i).checked=true;
document.getElementById("chk"+i).value='on';
}
}
function tidakpilihsemua(){
var jml=parseFloat(document.getElementById("jml").value);
for(i=1;i<=jml;i++){
document.getElementById("chk"+i).checked=false;
document.getElementById("chk"+i).value='';
}
}
function batal(){
show('spbrealisasiulang/cform/','#tmpx');
}
function pilihan(a,b){
if(a==''){
document.getElementById("chk"+b).value='on';
}else{
document.getElementById("chk"+b).value='';
}
}
</script>
