<script type="text/javascript" src="<?php  echo base_url()?>js/jquery.js"></script>
<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
@media print {
input.noPrint { display: none; }
}
@page
        {
            size: auto;   /* auto is the initial value */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
*/
*{
size: landscape;
}

@page { size: Letter; 
        margin: 0mm;  /* this affects the margin in the printer settings */
}

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.wrap {
	margin: 0 auto;
	text-align: left;
}

.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
 border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
	margin-top: 0;
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 10.5px;
  font-weight:normal;
}
.eusinya {
  font-size: 8px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.garisatas { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
$hal=1;
?>
  <table width="100%" class="nmper" border="0" id="sitabel">
    <tr>
      <td><?php echo NmPerusahaan; ?></td>
    </tr>
    <tr>
      <td class="huruf judul" align="center" >S U R A T &nbsp;&nbsp;&nbsp;&nbsp;P E R M I N T A A N&nbsp;&nbsp;&nbsp;&nbsp;B A R A N G</td>
    </tr>
    <tr>
      <td align="center">Nomor Permintaan : <?php echo $no; ?></td>
    </tr>
    <tr>
      <td align="center">Tanggal : <?php 
		    $th=date("Y");
		    $bl=date("m");
		    $hr=date("d");
		    $dspmb=$hr." ".mbulan($bl)." ".$th;
        echo $dspmb;?></td>
    </tr>
    <tr>
      <td>Kepada Yth.</td>
    </tr>
    <tr>
      <td>Bag. Gudang Packing</td>
    </tr>
    <tr>
      <td><?php echo NmPerusahaan;?></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Dengan hormat,</td>
    </tr>
    <tr>
      <td>Bersama surat ini kami mohon dikirimkan barang-barang sbb :</td>
    </tr>
    <tr align="center">
      <td>
        <table width="100%" class="nmper" border="0">
          <tr>
            <td width="60px" class="garisatas garisbawah isi">
              NO. SPB
            </td>
            <td width="50px" class="garisatas garisbawah isi">
              AREA
            </td>
            <td width="200px" class="garisatas garisbawah isi">
              CUSTOMER
            </td>
            <td width="75px" class="garisatas garisbawah isi">
              KODE BARANG
            </td>
            <td class="garisatas garisbawah isi">
              NAMA BARANG
            </td>
            <td width="75px" class="garisatas garisbawah isi">
              SPB
            </td>
            <td width="75px" class="garisatas garisbawah isi">
              REALISASI
            </td>
            <td width="75px" class="garisatas garisbawah isi">
              PENDING
            </td>
            <td width="100px" class="garisatas garisbawah isi">
              KETERANGAN
            </td>
          </tr>
          <?php 
          $pending=0;
          if($isi){
            
		        $i	= 0;
		        $j	= 0;
		        $hrg= 0;
		        $jml=count($isi);

		      foreach($isi as $rowi){
			    $i++;
			    $j++;
          $pending=$rowi->n_order-$rowi->n_deliver;
          $ispb = substr($rowi->i_spb,9,6);
#			    $hrg	= $hrg+($rowi->n_order*$rowi->v_product_mill);
			      ?>
          <tr id="panel<?php echo $i;?>" name="panel<?php echo $i;?>">
            <td class="isi">
              <?php echo $ispb;?>
            </td>
            <td class="isi">
              <?php echo $rowi->e_area_name;?>
            </td>
            <td class="isi">
              <?php echo $rowi->e_customer_name;?>
            </td>
            <td class="isi">
              <?php echo $rowi->i_product;?>
            </td>
            <td class="isi">
              <?php 
			        if(strlen($rowi->e_product_name )>50){
				        $nam	= substr($rowi->e_product_name,0,50);
			        }else{
				        $nam	= $rowi->e_product_name.str_repeat(" ",50-strlen($rowi->e_product_name ));
			        }
              echo $nam;?>
            <td class="isi">
              <?php echo number_format($rowi->n_order);?>
            </td>
            <td class="isi">
              <?php echo number_format($rowi->n_deliver);?>
            </td>
            <td class="isi">
              <?php echo number_format($pending);?>
            </td>
              <td>
              _____________
            </td>
            <td>
              <button type="button" class="noDisplay" onclick="myFunction(<?php echo $i ?>)">Lengit</button>
            </td>
          </tr>
          <?php }
        }?>
      </td>
    </tr>
</table>
  <tr>
    <td colspan=5 class=garisatas>&nbsp;</td>
  </tr>
</table >
<table width="100%" class="nmper" border="0">
  <tr>
    <td colspan ="4">Demikian surat ini kami sampaikan, atasa perhatian dan kerjasamanya kami ucapkan terima kasih.</td>
  </tr>  
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="center" width="300px">
      Hormat kami,
    </td>
    <td align="center" width="300px">
      Mengetahui,
    </td>   
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
      &nbsp;
    </td>
    <td align="center">
      &nbsp;
    </td>   
    <td>
      &nbsp;
    </td>
    <td align="center">
      &nbsp;
    </td>   
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td>
      &nbsp;
    </td>
    <td align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepala Gudang&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
    <td align="center">(&nbsp;WIP&nbsp;)</td>
    <td>
      &nbsp;
    </td>
  </tr>
  <tr>
    <td colspan ="4" align="left"><?php echo "TANGGAL CETAK : ".$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
?></td>
  </tr>
</table>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
<div class="noDisplay"><center><b><input name="cmdreset" id="cmdreset" value="Export to Excel" type="button"></b></center>
  <script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
<script>
function myFunction(i) {
    document.getElementById("panel"+i).style.display = "none";    
}
</script>
