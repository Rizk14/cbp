<?php 
	echo "<h2>$page_title</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'transferuangkeluar/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="transferuangform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
			<?php 
				if($isi->d_kuk!=''){
					$tmp=explode('-',$isi->d_kuk);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$isi->d_kuk=$tgl.'-'.$bln.'-'.$thn;
				}
			?>
	      <tr>
		<td width="12%">Bukti transfer</td>
		<td width="1%">:</td>
		<td width="37%"><input name="ikuk" id="ikuk" value="<?php echo $isi->i_kuk; ?>" maxlength="15"><input readonly id="dkuk" name="dkuk" value="<?php echo $isi->d_kuk; ?>" onclick="showCalendar('',this,this,'','dkuk',0,20,1)"></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value="<?php echo $isi->e_remark; ?>"></td>
	      </tr>
		  <tr>
		<td width="12%">Pemasok</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="esuppliername" id="esuppliername" value="<?php echo $isi->e_supplier_name; ?>"
                    <?php if($isi->v_jumlah==$isi->v_sisa)  echo "onclick=\"nsupp()\""; ?>>
						<input type="hidden" name="isupplier" id="isupplier" value="<?php echo $isi->i_supplier; ?>"></td>
		<td width="12%">Jumlah</td>
		<td width="1%">:</td>
		<td width="37%"><input name="vjumlah" id="vjumlah" value="<?php echo number_format($isi->v_jumlah); ?>" onkeyup="reformat(this);hetang(this);"></td>
		  </tr>
		  <tr>
		  </tr>
		  <tr>
		<td width="12%">Nama Bank</td>
		<td width="1%">:</td>
		<td width="37%"><input name="ebankname" id="ebankname" value="<?php echo $isi->e_bank_name; ?>" maxlength="50"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="vsisa" id="vsisa" value="<?php echo number_format($isi->v_sisa); ?>"></td>
		  </tr>
	      <tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan="4">
      <?php if($isi->v_sisa==$isi->v_jumlah && $isi->v_sisa!=0 && $isi->f_kuk_cancel=='f'){ ?>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
      <?php }?>
		   <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick='show("listtransferuangkeluar/cform/view/<?php echo $dfrom."/".$dto."/".$isupplier."/"; ?>","#main")'>
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("ikuk").value=='')||
			(document.getElementById("dkuk").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
		}
	}
	function nsupp()
	{
		supplier=document.getElementById("isupplier").value;
		showModal("transferuangkeluar/cform/supplier/"+supplier+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function hetang()
	{
		document.getElementById("vsisa").value=document.getElementById("vjumlah").value;
	}
</script>
