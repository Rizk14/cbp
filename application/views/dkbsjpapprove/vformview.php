<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/cari', 'update' => '#tmp', 'type' => 'post')); ?>
				<div id="listform">
					<div class="effect">
						<div class="accordion2">
							<table class="listtable">
								<thead>
									<tr>
										<td colspan="8" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="<?php echo $cari; ?>">
											<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
											<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
											<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;
											<input type="submit" id="bcari" name="bcari" value="Cari">
										</td>
									</tr>
								</thead>
								<th>No DKB</th>
								<th>Tgl DKB</th>
								<th>Area</th>
								<th class="action">Action</th>
								<tbody>
									<?php
									if ($isi) {
										foreach ($isi as $row) {
											$tmp = explode('-', $row->d_dkb);
											$tgl = $tmp[2];
											$bln = $tmp[1];
											$thn = $tmp[0];
											$row->d_dkb = $tgl . '-' . $bln . '-' . $thn;
											echo "<tr> ";
											if ($row->f_dkb_batal == 't') {
												echo "<td><h1>$row->i_dkb</h1></td>";
											} else {
												echo "<td>$row->i_dkb</td>";
											}
											echo "	<td>$row->d_dkb</td>
													<td>$row->e_area_name</td>
													<td class=\"action\">
														<a href=\"#\" onclick='show(\"$folder/cform/approve/$row->i_dkb/$dfrom/$dto/$row->i_area/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a>
													</td>
												</tr>";
										}
									}
									?>
								</tbody>
							</table>
							<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
						</div>
					</div>
				</div>
				<?= form_close() ?>
				<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/','#tmpx')">
			</td>
		</tr>
	</table>
</div>