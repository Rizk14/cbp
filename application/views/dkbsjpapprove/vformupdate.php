<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/simpan', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="dkbform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td width="10%">Tgl DKB</td>
								<?php
								$tmp = explode("-", $isi->d_dkb);
								$th = $tmp[0];
								$bl = $tmp[1];
								$hr = $tmp[2];
								$ddkb = $hr . "-" . $bl . "-" . $th;
								?>
								<td><input readonly id="ddkb" name="ddkb" value="<?php echo $ddkb; ?>">
									<input readonly id="idkb" name="idkb" type="text" value="<?php echo $isi->i_dkb; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Area</td>
								<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
									<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
									<!-- <input id="idkbold" name="idkbold" type="hidden" value="<?php echo $isi->i_dkb_old; ?>"> -->
								</td>
							</tr>
							<tr>
								<td width="10%">Kirim</td>
								<td><input readonly id="edkbkirim" name="edkbkirim" value="<?php echo $isi->e_dkb_kirim; ?>">
									<input id="idkbkirim" name="idkbkirim" type="hidden" value="<?php echo $isi->i_dkb_kirim; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Via</td>
								<td><input readonly id="edkbvia" name="ekbvia" value="<?php echo $isi->e_dkb_via; ?>">
									<input id="idkbvia" name="idkbvia" type="hidden" value="<?php echo $isi->i_dkb_via; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Supir</td>
								<td><input readonly id="esupirname" name="esupirname" value="<?php echo $isi->e_sopir_name; ?>"></td>
							</tr>
							<tr>
								<td width="10%">Nopol</td>
								<td><input readonly id="ikendaraan" name="ikendaraan" value="<?php echo $isi->i_kendaraan; ?>"></td>
							</tr>
							<tr>
								<td width="10%">Jumlah</td>
								<?php
								if ($jmlx == $isi->v_dkb) {
									$totaldkb = $isi->v_dkb;
								} elseif ($isi->v_dkb < $jmlx) {
									$totaldkb = $jmlx;
								} else {
									$totaldkb = $jmlx;
								}
								?>
								<td><input style="text-align:right;" readonly id="vdkb" name="vdkb" value="<?php echo number_format($jmlx); ?>"></td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4">
									<input name="login" id="login" value="Approve" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
									<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick='show("<?= $folder ?>/cform/view/<?php echo $dfrom . "/" . $dto . "/" . $user . "/"; ?>","#main")'>
								</td>
							</tr>
						</table>

						<div id="detailheader" align="center">
							<table id="itemtem" class="listtable" style="width:950px;">
								<tr>
									<th style="width:28px;" align="center">No</th>
									<th style="width:238px;" align="center">No SJP</th>
									<th style="width:140px;" align="center">Tgl SJP</th>
									<th style="width:250px;" align="center">Cabang</th>
									<th style="width:130px;" align="center">Jml</th>
									<th style="width:200px;" align="center">Keterangan</th>
									<th style="width:70px;" align="center">Koli</th>
									<th style="width:69px;" align="center">Ikat</th>
									<th style="width:69px;" align="center">Kardus</th>
									<th style="width:32px;" align="center" class="action">Action</th>
								</tr>
							</table>
						</div>

						<div id="detailisi" align="center">
							<?php
							$i = 0;
							if ($detail) {
								foreach ($detail as $row) {
									echo '<table class="listtable" align="center" style="width:950px;">';
									$i++;
									$pangaos = number_format($row->v_jumlah);
									$tmp = explode("-", $row->d_sjp);
									$th = $tmp[0];
									$bl = $tmp[1];
									$hr = $tmp[2];
									$row->d_sjp = $hr . "-" . $bl . "-" . $th;
									echo "<tbody>
											<tr>
												<td style=\"width:22px;\"><input style=\"width:22px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
												<td style=\"width:150px;\"><input style=\"width:152px;\" readonly type=\"text\" id=\"isjp$i\" name=\"isjp$i\" value=\"" . $row->i_sjp . "\"></td>
												<td style=\"width:89px;\"><input style=\"width:94px;\" readonly type=\"text\" id=\"dsjp$i\" name=\"dsjp$i\" value=\"" . $row->d_sjp . "\"></td>
												<td style=\"width:176px;\"><input style=\"width:169px;\" readonly type=\"text\" id=\"icustomer$i\" name=\"icustomer$i\" value=\"" . $isi->e_area_name . "\"></td>
												<td style=\"width:90px;\"><input readonly style=\"text-align:right; width:87px;\"  type=\"text\" id=\"vsjnetto$i\" name=\"vsjnetto$i\" value=\"" . $pangaos . "\"></td>
												<td style=\"width:150px;\"><input style=\"width:153px;\"  type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"" . $row->e_remark . "\"></td>
												<td style=\"width:54px;\"><input style=\"width:51px;text-align:right;\" onkeyup='hanyaAngka(this)' type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"" . $row->n_koli . "\"></td>
												<td style=\"width:50px;\"><input style=\"width:50px;text-align:right;\" onkeyup='hanyaAngka(this)' type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"" . $row->n_ikat . "\"></td>
												<td style=\"width:55px;\"><input style=\"width:60px;text-align:right;\" onkeyup='hanyaAngka(this)' type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"" . $row->n_kardus . "\"></td>
												<td style=\"width:303px;\" align=\"center\"><input type='checkbox' name='chk" . $i . "' id='chk" . $i . "' value='on' checked onclick='pilihan(this.value," . $i . ")'</td>
											</tr>
										</tbody>
									</table>";
								}
							}
							?>
						</div>

						<div id="detailheaderx" align="center">
							<table id="itemtemx" class="listtable" style="width:750px;">
								<tr>
									<th style="width:25px;" align="center">No</th>
									<th style="width:60px;" align="center">Kode</th>
									<th style="width:300px;" align="center">Nama</th>
									<th style="width:300px;" align="center">Keterangan</th>
									<th style="width:32px;" align="center" class="action">Action</th>
								</tr>
							</table>
						</div>
						<div id="detailisix" align="center">
							<?php
							$i = 0;
							if ($detailx) {
								foreach ($detailx as $row) {
									$i++;
									echo '	<table class="listtable" align="center" style="width:750px;">
												<tbody>
													<tr>
														<td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx' . $i . '" name="barisx' . $i . '" value="' . $i . '"></td>
														<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi' . $i . '" name="iekspedisi' . $i . '" value="' . $row->i_ekspedisi . '"></td>
														<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname' . $i . '" name="eekspedisiname' . $i . '" value="' . $row->e_ekspedisi . '"></td>
														<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx' . $i . '" name="eremarkx' . $i . '" value="' . $row->e_remark . '"></td>
														<td style=\"width:50px;\" align=\"center\"></td>
													</tr>
												</tbody>
											</table>';
								}
							}
							?>
						</div>

						<div id="pesan"></div>

						<input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
																		echo "value=\"$jmlitem\"";
																	} else {
																		echo "value=\"0\"";
																	} ?>>
						<input type="hidden" name="jmlx" id="jmlx" <?php if (isset($jmlitemx)) {
																		echo "value=\"$jmlitemx\"";
																	} else {
																		echo "value=\"0\"";
																	} ?>>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function tambah_item(a) {
		if (a <= 30) {
			so_inner = document.getElementById("detailheader").innerHTML;
			si_inner = document.getElementById("detailisi").innerHTML;
			if (si_inner == '') {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = '<table class="listtable" style="width:100%;"><tbody><tr><td style="width:23px;"><input style="width:21px;height:29px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
				si_inner += '<td style="width:104px;"><input style="width:111px;height:29px;" readonly type="text" id="isjp' + a + '" name="isjp' + a + '" value="" onclick=\'showModal("<?= $folder ?>/cform/sjp/' + a + '/' + b + '/' + c + '/","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
				si_inner += '<td style="width:90px;"><input style="width:92px;height:29px;" readonly type="text" id="dsjp' + a + '" name="dsjp' + a + '" value=""><input type="hidden" name="dtelat' + a + '" id="dtelat' + a + '" readonly></td>';
				si_inner += '<td style="width:177px;"><input style="width:174px;height:29px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
				si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:76px;height:29px;"  type="text" id="vsjnetto' + a + '" name="vsjnetto' + a + '" value=""></td>';
				si_inner += '<td style="width:163px;"><input style="width:162px;height:29px;"  type="text" id="eremark' + a + '" name="eremark' + a + '" value=""><input type="hidden" id="finputpst' + a + '" name="finputpst' + a + '" value=""><input type="hidden" id="iarearef' + a + '" name="iarearef' + a + '" value=""></td>';
				si_inner += '<td style="width:253px;"><select id="etelat' + a + '" name="etelat' + a + '" type="text" style="width:49%"><option value="">-</option><option value="Menunggu Armada">Menunggu Armada</option><option value="Menunggu Jalur">Menunggu Jalur</option><option value="Menunggu Ekspedisi">Menunggu Ekspedisi</option><option value="Menunggu Rasio">Menunggu Rasio</option><option value="Menunggu Barcode" >Menunggu Barcode</option><option value="Permintaan Toko">Permintaan Toko</option></select><input id="rtelat' + a + '" name="rtelat' + a + '" value="" type="text" placeholder="Keterangan lain" style="width:50%"></td>';
				si_inner += '<td style="width:70px;"><input id="nkoli' + a + '" name="nkoli' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nikat' + a + '" name="nikat' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nkardus' + a + '" name="nkardus' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td></tr></tbody></table>';
			} else {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner += '<table class="listtable" style="width:100%;"><tbody><tr><td style="width:23px;"><input style="width:21px;height:29px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
				si_inner += '<td style="width:104px;"><input style="width:111px;height:29px;" readonly type="text" id="isjp' + a + '" name="isjp' + a + '" value="" onclick=\'showModal("<?= $folder ?>/cform/sjp/' + a + '/' + b + '/' + c + '/","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
				si_inner += '<td style="width:90px;"><input style="width:92px;height:29px;" readonly type="text" id="dsjp' + a + '" name="dsjp' + a + '" value=""><input type="hidden" name="dtelat' + a + '" id="dtelat' + a + '" readonly></td>';
				si_inner += '<td style="width:177px;"><input style="width:174px;height:29px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
				si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:76px;height:29px;"  type="text" id="vsjnetto' + a + '" name="vsjnetto' + a + '" value=""></td>';
				si_inner += '<td style="width:163px;"><input style="width:162px;height:29px;"  type="text" id="eremark' + a + '" name="eremark' + a + '" value=""><input type="hidden" id="finputpst' + a + '" name="finputpst' + a + '" value=""><input type="hidden" id="iarearef' + a + '" name="iarearef' + a + '" value=""></td>';
				si_inner += '<td style="width:253px;"><select id="etelat' + a + '" name="etelat' + a + '" type="text" style="width:49%"><option value="">-</option><option value="Menunggu Armada">Menunggu Armada</option><option value="Menunggu Jalur">Menunggu Jalur</option><option value="Menunggu Ekspedisi">Menunggu Ekspedisi</option><option value="Menunggu Rasio">Menunggu Rasio</option><option value="Menunggu Barcode" >Menunggu Barcode</option><option value="Permintaan Toko">Permintaan Toko</option></select><input id="rtelat' + a + '" name="rtelat' + a + '" value="" type="text" placeholder="Keterangan lain" style="width:50%"></td>';
				si_inner += '<td style="width:70px;"><input id="nkoli' + a + '" name="nkoli' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nikat' + a + '" name="nikat' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nkardus' + a + '" name="nkardus' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td></tr></tbody></table>';
			}
			j = 0;
			var baris = Array();
			var isjp = Array();
			var dsjp = Array();
			var icustomer = Array();
			var vsjnetto = Array();
			for (i = 1; i < a; i++) {
				j++;
				baris[j] = document.getElementById("baris" + i).value;
				isjp[j] = document.getElementById("isjp" + i).value;
				dsjp[j] = document.getElementById("dsjp" + i).value;
				icustomer[j] = document.getElementById("icustomer" + i).value;
				vsjnetto[j] = document.getElementById("vsjnetto" + i).value;
			}
			document.getElementById("detailisi").innerHTML = si_inner;
			j = 0;
			for (i = 1; i < a; i++) {
				j++;
				document.getElementById("baris" + i).value = baris[j];
				document.getElementById("isjp" + i).value = isj[j];
				document.getElementById("dsjp" + i).value = dsj[j];
				document.getElementById("icustomer" + i).value = icustomer[j];
				document.getElementById("vsjnetto" + i).value = vsjnetto[j];
			}
			area = document.getElementById("iarea").value;
			showModal("<?= $folder ?>/cform/sjupdate/" + a + "/" + area, "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		} else {
			alert("Maksimal 30 item");
		}
	}

	function dipales(a) {
		cek = 'false';
		if ((document.getElementById("ddkb").value != '') &&
			(document.getElementById("idkb").value != '') &&
			(document.getElementById("iarea").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("isjp" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						exit();
						cek = 'false';
					} else {
						cek = 'true';
					}
				}
			}
			if (cek == 'true') {
				document.getElementById("login").hidden = true;
			} else {
				document.getElementById("login").hidden = false;
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}
	}

	function pilihan(a, b) {
		if (a == '') {
			document.getElementById("chk" + b).value = 'on';
		} else {
			document.getElementById("chk" + b).value = '';
		}
	}

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").disabled = false;
	}
</script>