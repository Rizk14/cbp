<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'akt-bankin-multiallocnew/cform/updatealokasi','update'=>'#pesan','type'=>'post'));?>
	<div id="pelunasanform">
	<div class="effect">
	  <div class="accordion2">
	 	<?php 
			/* CEK PERIODE CLOSING */
			$query3		= $this->db->query(" select i_periode from tm_periode ");
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$i_periode	= $hasilrow->i_periode;
			}
			/* ******************* */
			foreach($isi as $row){
				if($row->d_bank!=''){
					$tmp		 = explode('-',$row->d_bank);
					$tgl		 = $tmp[2];
					$bln		 = $tmp[1];
					$thn		 = $tmp[0];
					$row->d_bank = $tgl.'-'.$bln.'-'.$thn;
				}

				if($row->d_alokasi!=''){
					$tmp	= explode('-',$row->d_alokasi);
					$tgl	= $tmp[2];
					$bln	= $tmp[1];
					$thn	= $tmp[0];
					$tah	= $tmp[0];
					$thbl 	= $thn.$bln;
					$row->d_alokasi = $tgl.'-'.$bln.'-'.$thn;
				}

				if($i_periode <= $thbl){
					$bisahapus = true;
				}else{
					$bisahapus = false;
				}
		?>
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	    	<tr>
				<td>No Bank / Tgl Bank</td>
				<td>
					<input type="text" id="ialokasi" name="ialokasi" value="<?php echo $row->i_alokasi; ?>" readonly>
					<input type="hidden" id="ikbank" name="ikbank" value="<?php echo $row->i_kbank; ?>" readonly>
					<input readonly id="dbank" name="dbank" value="<?php echo $row->d_bank; ?>">
				</td>			
		
				<td>Debitur</td>
				<td>
					<input type="text" readonly id="ecustomername" name="ecustomername" value="<?php echo $row->e_customer_name; ?>">
					<input type="hidden" readonly id="icustomer" name="icustomer" value="<?php echo $row->i_customer; ?>">
					<input type="hidden" id="icoabank" name="icoabank" value="<?php echo $row->i_coa_bank; ?>">
				</td>
	    	</tr>
	      	
			<tr>
				<td>Area / Tgl Alokasi</td>
				<td>
					<input readonly id="eareaname" name="eareaname" value="<?php echo $row->e_area_name; ?>">
		    		<input id="iarea" name="iarea" type="hidden" value="<?php echo $row->i_area; ?>">
					<input readonly id="dalokasi" name="dalokasi" value="<?php echo $row->d_alokasi; ?>" >
				</td>
				<td>Alamat</td>
				<td>
					<input id="ecustomeraddress" name="ecustomeraddress" readonly value="<?php echo $row->e_customer_address; ?>">
				</td>
	      	</tr>

	      	<tr>
				<td>Bank</td>
				<td>
					<input readonly id="ebankname" name="ebankname" value="<?php echo $row->e_bank_name; ?>">
				</td>
				<td>Kota</td>
				<td>
					<input readonly id="ecustomercity" name="ecustomercity" value="<?php echo $row->e_customer_city; ?>">
				</td>
	      	</tr>
			
	      	<tr>
				<td>Jumlah</td>
				<td>
					<input readonly type="text" id="vjumlah" name="vjumlah" value="<?php echo number_format($row->v_jumlah); ?>">
					<input type="hidden" id="vsisa" name="vsisa" value="<?php echo $vsisa; ?>" >
					<input type="hidden" name="totalmt" id="totalmt" value="<?php echo $totalmt; ?>" readonly>
					<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
				</td>
				<td>Lebih</td>
				<td>
					<input type="text" readonly id="vlebih" name="vlebih" value="<?php echo number_format($row->v_lebih); ?>" >
				</td>
	      	</tr>

			<tr>
				<td>KU/Giro/Tunai</td>
				<td>
					<input type="text" readonly id="igiro" name="igiro" value="<?php echo $row->i_giro;?>" onclick="view_bayar('<?php echo $iarea; ?>')">
				</td>
				<td>Pembulatan</td>
				<td>
					<input type="text" readonly id="vbulat" name="vbulat" value="<?php echo number_format($vbulat); ?>" >
				</td>
			</tr>

			<tr>
		  		<td width="100%" align="center" colspan="4">
				  	<?php if($bisaedit){ ?>
						<!-- <input name="login" id="login" value="Simpan" type="submit" onclick="return dipales()"> -->
		    			<!-- <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'> -->
					<?php } ?>
					<input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick='show("list-akt-bkinalloc/cform/view/<?php echo $dfrom.'/'.$dto.'/'.$iarea.'/'; ?>","#main")'>
				</td>
			</tr>
	    </table>
		<?php } ?> <!-- TUTUP FOREACH -->
		
		<!-- DETAIL HEADER -->
		<div id="detailheader" align="center">
			<table id="itemtem" class="listtable" style="width:920px;">
				<th style="width:24px;" align="center">No</th>
				<th style="width:94px;" align="center">Nota</th>
				<th style="width:68px;" align="center">Tgl Nota</th>
				<th style="width:102px;" align="center">Nilai</th>
				<th style="width:97px;" align="center">Bayar</th>
				<th style="width:97px;" align="center">Sisa</th>
				<th style="width:68px;" align="center">Meterai</th>
				<!-- <th style="width:115px;" align="center">Lebih</th> -->
				<th style="width:115px;" align="center">Ket2</th>
				<!-- <th style="width:115px;" align="center">Act</th> -->
			</table>
		</div>
		
		<div id="detailisi" align="center">
			<?php 
				$i=0;
				foreach($detail as $row){
					$i++;
					if($row->d_nota!=''){
						$tmp		 = explode('-',$row->d_nota);
						$tgl		 = $tmp[2];
						$bln		 = $tmp[1];
						$thn		 = $tmp[0];
						$row->d_nota = $tgl.'-'.$bln.'-'.$thn;
					}
			?>
				<table id="itemtem" class="listtable" style="width:920px;">
					<tbody>
						<tr>
							<td style="width:29px;">
								<input style="width:29px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>">
							</td>
							<td style="width:124px;">
								<input style="width:124px;" readonly type="text" id="inota<?php echo $i; ?>" name="inota<?php echo $i; ?>" value="<?php echo $row->i_nota; ?>">
								<input readonly style="width:128px;text-align:right;font-size:12px;" type="hidden" id="vnotaasal<?php echo $i; ?>" name="vnotaasal<?php echo $i; ?>" value="<?php echo $row->v_nota_netto; ?>">
							</td>
							<td style="width:88px;">
								<input style="width:88px;" readonly type="text" id="dnota<?php echo $i; ?>" name="dnota<?php echo $i; ?>" value="<?php echo $row->d_nota; ?>">
							</td>
							<td style="width:134px;">
								<input readonly style="width:134px;text-align:right;" type="text" id="vnota<?php echo $i; ?>" name="vnota<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah+$row->v_sisa_nota); ?>">
							</td>
							<td style="width:128px;">
								<input style="width:128px;text-align:right;" type="text" id="vjumlah<?php echo $i; ?>" name="vjumlah<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>" onkeyup="reformat(this);return hetangupdate(<?php echo $i; ?>);">
							</td>
							<td style="width:127px;text-align:right;">
								<input type="text" style="width:127px;text-align:right;" id="vsesa<?php echo $i; ?>" name="vsesa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa_nota); ?>">
								<input readonly style="width:127px;text-align:right;" type="hidden" id="vsisa<?php echo $i; ?>" name="vsisa<?php echo $i; ?>" value="<?php echo $row->v_sisa_nota; ?>">
								<!-- <input type="hidden" id="vlebih<?php echo $i; ?>" name="vlebih<?php echo $i; ?>" value=""> -->
								<input type="hidden" id="vasal<?php echo $i; ?>" name="vasal<?php echo $i; ?>" value="<?php echo $row->v_jumlah+$row->v_sisa_nota; ?>">
							</td>
							<td style="width:25px;padding-left:40px;padding-right:27px">
								<input type = "checkbox" id = "mt<?php echo $i; ?>" name = "mt<?php echo $i; ?>" value = "<?php echo $row->v_materai; ?>" <?php if($row->v_materai>0){ echo "checked"; }else{ if($row->v_materai_sisa>0||$row->v_materai>0){}else{echo "hidden disabled";}}?> "> <!-- onclick="return hetangupdate(<?php echo $i; ?>) -->
								<input type = "hidden" id = "vmeterai<?php echo $i; ?>" name = "vmeterai<?php echo $i; ?>" value = "<?php echo $row->v_materai; ?>" readonly> 
								<input type = "hidden" id = "vmeteraisaldo<?php echo $i; ?>" name = "vmeteraisaldo<?php echo $i; ?>" value = "<?php echo $row->v_materai_sisa; ?>" readonly> 
							</td>
							<!-- <td style="width:128px;">
								<input style="width:128px;" type="text" id="vlebih<?php echo $i; ?>" name="vlebih<?php echo $i; ?>" value="">
							</td> -->
							<td style="width:120px;">
								<!-- <input style="width:120px;" type="text" id="eremark<?php echo $i; ?>" name="eremark<?php echo $i; ?>" value="<?php echo $row->e_remark; ?>"> -->
								<select id="eremark<?php echo $i; ?>" name="eremark<?php echo $i; ?>" style="width:152px">
									<option <?php if($row->e_remark=='') echo 'selected'; ?> value=""></option>
									<option <?php if($row->e_remark=='Retur') echo 'selected'; ?> value="Retur">Retur</option>
									<option <?php if($row->e_remark=='Biaya Promo') echo 'selected'; ?> value="Biaya Promo">Biaya Promo</option>
									<option <?php if($row->e_remark=='Kurang Bayar') echo 'selected'; ?> value="Kurang Bayar">Kurang Bayar</option>
									<option <?php if($row->e_remark=='Biaya Ekspedisi') echo 'selected'; ?> value="Biaya Ekspedisi">Biaya Ekspedisi</option>
									<option <?php if($row->e_remark=='Biaya Administrasi') echo 'selected'; ?> value="Biaya Administrasi">Biaya Administrasi</option>
								</select>
							</td>
							<!-- DICOMMENT DULU TOMBOL HAPUSNYA -->
							<!-- <td style="width:277px;" align="center">
								<#?php 
									if($bisahapus){
										#echo "<a href=\"#\" onclick='show(\"akt-bankin-multiallocnew/cform/deletedetail/$row->i_alokasi/$row->i_area/$row->i_kbank/$row->i_nota/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
									}
								?>
							</td> -->
						</tr>
					</tbody>
				</table>
			<?php }?>
		</div><!-- END DETAIL ISI -->
</table>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
<div id="pesan"></div>
    <!-- </td>
  </tr>
</table> -->
<script language="javascript" type="text/javascript">
  function dipales(){
	cek='false';
	cok='false';
	s=0;
	if(
      (document.getElementById("ikbank").value!='') &&
	  (document.getElementById("dbank").value!='') &&
	  (document.getElementById("dalokasi").value!='') &&
	  (document.getElementById("vjumlah").value!='') &&
	  (document.getElementById("vjumlah").value!='0') &&
	  (document.getElementById("icustomer").value!='')
    ) {
	  var a=parseFloat(document.getElementById("jml").value);
	  for(i=1;i<=a;i++){
		  if(document.getElementById("vjumlah"+i).value!='0'){
        sisa=parseFloat(formatulang(document.getElementById("vsesa"+i).value));
        awal=parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
        ket=document.getElementById("eremark"+i).value;
        if(sisa > 100 && ket == ''){
        	alert('Keterangan sisa wajib diisi !!!');
            cok='false';
            cek='false';
        	s=1;
        return false;
            break;
        }
        if( (sisa-awal>0) ){
          if(ket==''){
            alert('Keterangan sisa wajib diisi !!!');
            cok='false';
            cek='false';
        	s=1;
        return false;
            break;
          }else{
            cok='true';
            cek='true';
          }
        }else{
		      cok='true';
          cek='true';
        }
		  }else{
		    cek='false';
		  }
	  }
	  if(cek=='true' && cok=='true'){
	    document.getElementById("login").hidden=true;
	    document.getElementById("cmdtambahitem").hidden=true;
	  }else if(cok=='false'){
	  }else{
		  alert('Isi jumlah detail pelunasan minimal 1 item !!!');
	    document.getElementById("login").hidden=false;
        	s=1;
        return false;
	  }
	}else{
	  alert('Data header masih ada yang salah !!!');
        	s=1;
        return false;
	}
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
//    alert(si_inner);
    if(so_inner==''){
		  so_inner = '<table id="itemtem" class="listtable" style="width:920px;">';
		  so_inner+= '<th style="width:30px;" align="center">No</th>';
		  so_inner+= '<th style="width:130px;" align="center">Nota</th>';
		  so_inner+= '<th style="width:80px;" align="center">Tgl Nota</th>';
		  so_inner+= '<th style="width:115px;" align="center">Nilai</th>';
		  so_inner+= '<th style="width:115px;" align="center">Bayar</th>';
		  so_inner+= '<th style="width:115px;" align="center">Sisa</th>';
		  so_inner+= '<th style="width:115px;" align="center">Meterai</th>';
//		  so_inner+= '<th style="width:115px;" align="center">Lebih</th>';
//		  so_inner+= '<th style="width:115px;" align="center">ket1</th>';
		  so_inner+= '<th style="width:115px;" align="center">Ket2</th>';
		  so_inner+= '<th style="width:115px;" align="center">Act</th>';
		  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		  so_inner='';
    }
    if(si_inner==''){
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		  juml=document.getElementById("jml").value;
		  si_inner='<table id="itemdet"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		  si_inner+='<td style="width:149px;"><input style="width:149px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""><input readonly type="hidden" id="cust'+a+'" name ="cust'+a+'" value=""></td>';
		  si_inner+='<td style="width:86px;"><input style="width:86px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:128px;"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""><input readonly style="width:128px;text-align:right;font-size:12px;" type="hidden" id="vnotaasal' + a + '" name="vnotaasal' + a + '" value=""></td>';
		  si_inner+='<td style="width:128px;"><input style="width:128px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetangupdate('+a+');" onpaste="return false;"></td>';
		  si_inner+='<td style="width:183px;"><input readonly style="width:183px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td>';
		  si_inner+='<td style="width:128px;"><input type="checkbox" id="mt'+a+'" name="mt'+a+'" class="mt'+a+'" value="" onclick="return hetangupdate('+ a +')" ><input type="hidden" id="vmeterai'+a+'" name="vmeterai'+a+'" value=""><input type="hidden" id="vmeteraisaldo'+a+'" name="vmeteraisaldo'+a+'" value=""></td>';
//		  si_inner+='<td style="width:128px;" hidden="true"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
//  		si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-multiallocnew/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
		  si_inner+='<td style="width:120px;"><select id="eremark'+a+'" name="eremark'+a+'"><option value=""></option><option value="Retur">Retur</option><option value="Biaya Promo">Biaya Promo</option><option value="Kurang Bayar">Kurang Bayar</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Biaya Administrasi">Biaya Administrasi</option></select></td>';//</tr></tbody></table>
		  si_inner+='<td style="width:277px;" align="center"></td></td></tr></tbody></table>';
    }else{
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		  juml=document.getElementById("jml").value;
		  si_inner+='<table id="itemdet"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		  si_inner+='<td style="width:149px;"><input style="width:149px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""><input readonly type="hidden" id="cust'+a+'" name ="cust'+a+'" value=""></td>';
		  si_inner+='<td style="width:86px;"><input style="width:86px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:128px;"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""><input readonly style="width:128px;text-align:right;font-size:12px;" type="hidden" id="vnotaasal' + a + '" name="vnotaasal' + a + '" value=""></td>';
		  si_inner+='<td style="width:128px;"><input style="width:128px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetangupdate('+a+');" onpaste="return false;"></td>';
		  si_inner+='<td style="width:183px;"><input readonly style="width:183px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td>';
		  si_inner+='<td style="width:128px;"><input type="checkbox" id="mt' + a + '" name="mt' + a + '" class="mt'+a+'" value="" onclick="return hetangupdate('+ a +')"><input type="hidden" id="vmeterai'+a+'" name="vmeterai'+a+'" value=""><input type="hidden" id="vmeteraisaldo'+a+'" name="vmeteraisaldo'+a+'" value=""></td>';
//		  si_inner+='<td style="width:128px;" hidden="true"><input readonly style="width:128px;text-align:right;font-size:12px;" type="text" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
//   		si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-multiallocnew/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
		  si_inner+='<td style="width:120px;"><select id="eremark'+a+'" name="eremark'+a+'"><option value=""></option><option value="Retur">Retur</option><option value="Biaya Promo">Biaya Promo</option><option value="Kurang Bayar">Kurang Bayar</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Biaya Administrasi">Biaya Administrasi</option></select></td>';//</tr></tbody></table>
		  si_inner+='<td style="width:277px;" align="center"></td></td></tr></tbody></table>';
    }
    j=0;
    var baris			= Array();
    var inota			= Array();
    var dnota			= Array();
    var vnota			= Array();
  	var vjumlah			= Array();
    var vsesa			= Array();
    var vsisa			= Array();
    var vlebih			= Array();
    var eremark			= Array();
    var mt				= Array();
    var vmeterai		= Array();
    var vmeteraisaldo	= Array();

    for(i=1;i<a;i++){
		  j++;
		  baris[j]			= document.getElementById("baris"+i).value;
		  inota[j]			= document.getElementById("inota"+i).value;
		  dnota[j]			= document.getElementById("dnota"+i).value;
		  vnota[j]			= document.getElementById("vnota"+i).value;
		  vjumlah[j]		= document.getElementById("vjumlah"+i).value;	
		  vsesa[j]			= document.getElementById("vsesa"+i).value;
		  vsisa[j]			= document.getElementById("vsisa"+i).value;
		  vlebih[j]			= document.getElementById("vlebih"+i).value;
		  eremark[j]		= document.getElementById("eremark"+i).value;		
		  mt[j]				= document.getElementById("mt"+i).value;		
		  vmeterai[j]		= document.getElementById("vmeterai"+i).value;		
		  vmeteraisaldo[j]	= document.getElementById("vmeteraisaldo"+i).value;		

    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		  j++;
		  document.getElementById("baris"+i).value=baris[j];
		  document.getElementById("inota"+i).value=inota[j];
		  document.getElementById("dnota"+i).value=dnota[j];
		  document.getElementById("vnota"+i).value=vnota[j];
		  document.getElementById("vjumlah"+i).value=vjumlah[j];	
		  document.getElementById("vsesa"+i).value=vsesa[j];	
		  document.getElementById("vsisa"+i).value=vsisa[j];
		  document.getElementById("vlebih"+i).value=vlebih[j];
		  document.getElementById("eremark"+i).value=eremark[j];
		  document.getElementById("mt"+i).value=mt[j];
		  document.getElementById("vmeterai"+i).value=vmeterai[j];
		  document.getElementById("vmeteraisaldo"+i).value=vmeteraisaldo[j];
    }
	area=document.getElementById("iarea").value;
	ikbank=document.getElementById("ikbank").value;
	cust=document.getElementById("icustomer").value;
	showModal("akt-bankin-multiallocnew/cform/notaupdate/"+a+"/"+area+"/"+cust+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function hetangupdate(x)
  {	
    num=document.getElementById("vjumlah"+x).value.replace(/\,/g,'');
	mt  = document.getElementById("mt"+x); /* 10 APR 2021 */
    if(!isNaN(num)){
		vjmlbyr	    = parseFloat(formatulang(document.getElementById("vjumlah").value));
		vlebihitem  = vjmlbyr;
		totalmt 	= parseFloat(document.getElementById("totalmt").value);
		vtotalmt 	= 0;
		jml			= document.getElementById("jml").value;
		
		if(mt.checked) {
			if(formatulang(document.getElementById("vlebih").value) > 0){
				if(document.getElementById("vmeterai"+x).value > 0){
					document.getElementById("mt"+x).value = document.getElementById("vmeterai"+x).value;
				}else{
					document.getElementById("mt"+x).value = document.getElementById("vmeteraisaldo"+x).value;
					document.getElementById("vmeterai"+x).value = document.getElementById("vmeteraisaldo"+x).value;
					document.getElementById("vmeteraisaldo"+x).value = parseFloat(document.getElementById("vmeteraisaldo"+x).value)-parseFloat(document.getElementById("vmeterai"+x).value);
				}
				
				document.getElementById("mt"+x).setAttribute('checked','true');
				for (a = 1; a <= jml; a++) {
					cek         = document.getElementById("mt"+a).value;
					vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
					
					vjmlitem    = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
					
					if(cek > 0){
						vlebihitem = (vlebihitem - vjmlitem) - vmt;
						vtotalmt += vmt;
					}else{
						vlebihitem = (vlebihitem - vjmlitem);
					}
					document.getElementById("vlebih").value = formatcemua(vlebihitem);
				}
				
				document.getElementById("totalmt").value = vtotalmt;
				document.getElementById("vlebih").value = formatcemua(vlebihitem);
			}else{
				alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
				return false;
			}
        }else{
            document.getElementById("mt"+x).value = 0;
			document.getElementById("mt"+x).removeAttribute('checked');
			hitmeterai = totalmt-parseFloat(document.getElementById("vmeterai"+x).value);

			if(hitmeterai < 0){
				document.getElementById("totalmt").value = 0;
			}else{
				document.getElementById("totalmt").value = hitmeterai;
			}
			
			/* BALIKIN SALDO METERAI */
			document.getElementById("vmeteraisaldo"+x).value = parseFloat(document.getElementById("vmeteraisaldo"+x).value)+parseFloat(document.getElementById("vmeterai"+x).value);
			if(document.getElementById("vmeterai"+x).value>0){
				document.getElementById("vmeterai"+x).value = parseFloat(document.getElementById("vmeterai"+x).value)-parseFloat(document.getElementById("vmeteraisaldo"+x).value);
			}
			/*  */

			for(a=1;a<=jml;a++){
				vnota   = parseFloat(formatulang(document.getElementById("vnota"+a).value));
				vsiso   = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				vjmlitem= parseFloat(formatulang(document.getElementById("vjumlah"+a).value));

				cek         = document.getElementById("mt"+a).value;
                vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
				
				if(vjmlitem==0){
					bbotol();
				}
				
				vsisaitem =(vnota+vsiso)-vjmlitem;
				if(vsisaitem<0){
					alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
					document.getElementById("vjumlah"+a).value=0;
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				}
				alert (vlebihitem+"="+"("+vlebihitem+"-"+vjmlitem+")-"vmt);
				if(cek > 0){
                    vlebihitem = (vlebihitem - vjmlitem) - vmt;
                }else{
                    vlebihitem = (vlebihitem - vjmlitem);
                }
				if(vlebihitem<0){
					vlebihitem=vlebihitem+vjmlitem;
					vsisaitem =vnota-vlebihitem;
					alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
					document.getElementById("vjumlah"+a).value=formatcemua(vlebihitem);
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vlebihitem=0;
				}
				
				document.getElementById("vsesa"+a).value=formatcemua(vsisaitem);
				document.getElementById("vsisa"+a).value=vsisaitem;
			}
			document.getElementById("vlebih").value=formatcemua(vlebihitem);
		}
    }else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vjumlah"+x).value=0;
	}
  }
  function hetang(x)
  {	
    num=document.getElementById("vjumlah"+x).value.replace(/\,/g,'');
	mt  = document.getElementById("mt"+x); /* 10 APR 2021 */
    if(!isNaN(num)){
		vjmlbyr	    = parseFloat(formatulang(document.getElementById("vjumlah").value));
		vlebihitem  = vjmlbyr;
		totalmt 	= parseFloat(document.getElementById("totalmt").value);
		vtotalmt 	= 0;
		//vsisadt     = parseFloat(formatulang(document.getElementById("vsisa").value));
		jml		= document.getElementById("jml").value;
		
		if(mt.checked) {
			if(formatulang(document.getElementById("vlebih").value) > 0){
				document.getElementById("mt"+x).value = document.getElementById("vmeterai"+x).value;
				document.getElementById("mt"+x).setAttribute('checked','true');
				for (a = 1; a <= jml; a++) {
					cek         = document.getElementById("mt"+a).value;
					vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
					
					vjmlitem    = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
					
					if(cek > 0){
						vlebihitem = (vlebihitem - vjmlitem) - vmt;
						vtotalmt += vmt;
					}else{
						vlebihitem = (vlebihitem - vjmlitem);
						//vtotalmt = vtotalmt;
					}
					// if(vlebihitem<0){
					// 	document.getElementById("mt"+a).value = 0;
					// 	document.getElementById("mt"+a).removeAttribute('checked');
					// 	alert("jumlah meterai tidak bisa lebih besar dari nilai bayar !!!!!");
					// 	return false;
					// }

					document.getElementById("vlebih").value = formatcemua(vlebihitem);
				}
				document.getElementById("totalmt").value = vtotalmt;
				document.getElementById("vlebih"+a).value = formatcemua(vlebihitem);
			}else{
				alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
				return false;
			}
        }else{
            document.getElementById("mt"+x).value = 0;
			document.getElementById("mt"+x).removeAttribute('checked');
			hitmeterai = totalmt-parseFloat(document.getElementById("vmeterai"+x).value);

			if(hitmeterai < 0){
				document.getElementById("totalmt").value = 0;
			}else{
				document.getElementById("totalmt").value = hitmeterai;
			}

			for(a=1;a<=jml;a++){
				vnota   = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				vjmlitem= parseFloat(formatulang(document.getElementById("vjumlah"+a).value));

				cek         = document.getElementById("mt"+a).value;
                vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
				
				if(vjmlitem==0){
					bbotol();
				}
				
				vsisaitem =vnota-vjmlitem;
				if(vsisaitem<0){
					alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
					document.getElementById("vjumlah"+a).value=0;
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				}
				
				if(cek > 0){
                    vlebihitem = (vlebihitem - vjmlitem) - vmt;
                }else{
                    vlebihitem = (vlebihitem - vjmlitem);
					//vtotalmt = vtotalmt;
                }
				if(vlebihitem<0){
					vlebihitem=vlebihitem+vjmlitem;
					vsisaitem =vnota-vlebihitem;
					alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
					document.getElementById("vjumlah"+a).value=formatcemua(vlebihitem);
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vlebihitem=0;
				}
				document.getElementById("vsesa"+a).value=formatcemua(vsisaitem);
				document.getElementById("vlebih"+a).value=formatcemua(vlebihitem);
			}
			document.getElementById("vlebih").value=formatcemua(vlebihitem);
		}
    }else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vjumlah"+x).value=0;
	}
  }
  function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
  }
  function cektanggal()
  {
    ddt=document.getElementById('ddt').value;
    dbukti=document.getElementById('dbukti').value;
    dtmpdt=ddt.split('-');
    dtmpbukti=dbukti.split('-');
    perdt=dtmpdt[2]+dtmpdt[1]+dtmpdt[0];
    perbukti=dtmpbukti[2]+dtmpbukti[1]+dtmpbukti[0];
    if( (perdt!='') && (perbukti!='') ){
      if(compareDates(document.getElementById('dbukti').value,document.getElementById('ddt').value)==-1)
      {
        alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
        document.getElementById("dbukti").value='';
      }
    }else if( perdt>perbukti ){
        alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
        document.getElementById("dbukti").value='';
    }
    document.getElementById('dcair').value=document.getElementById('dcairx').value;
  }
  function bbotol(){
	  baris	= document.getElementById("baris").value;
	  si_inner= document.getElementById("detailisi").innerHTML;
	  var temp= new Array();
	  temp	= si_inner.split('<tbody disabled="disabled">');
	  if( (document.getElementById("vjumlah"+baris).value=='0')){
		  si_inner='';
		  for(x=1;x<baris;x++){
			  si_inner=si_inner+'<tbody>'+temp[x];
		  }
		  j=0;
		  var barbar		= Array();
		  var inota			= Array();
		  var dnota			= Array();
		  var vnota			= Array();
		  var vjumlah		= Array();
		  var vsesa			= Array();
		  var vsisa			= Array();
		  var vlebih		= Array();
		  for(i=1;i<baris;i++){
			  j++;
			  barbar[j]			= document.getElementById("baris"+i).value;
			  inota[j]			= document.getElementById("inota"+i).value;
			  dnota[j]			= document.getElementById("dnota"+i).value;
			  vnota[j]			= document.getElementById("vnota"+i).value;
			  vjumlah[j]			= document.getElementById("vjumlah"+i).value;
			  vsesa[j]			= document.getElementById("vsesa"+i).value;
			  vsisa[j]			= document.getElementById("vsisa"+i).value;
			  vlebih[j]			= document.getElementById("vlebih"+i).value;
		  }
		  document.getElementById("detailisi").innerHTML=si_inner;
		  j=0;
		  for(i=1;i<baris;i++){
			  j++;
			  document.getElementById("baris"+i).value			= barbar[j];
			  document.getElementById("inota"+i).value			= inota[j];
			  document.getElementById("dnota"+i).value			= dnota[j];
			  document.getElementById("vnota"+i).value			= vnota[j];
			  document.getElementById("vjumlah"+i).value		= vjumlah[j];
			  document.getElementById("vsesa"+i).value			= vsesa[j];
			  document.getElementById("vsisa"+i).value			= vsisa[j];
			  document.getElementById("vlebih"+i).value			= vlebih[j];
		  }
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;		
		  jsDlgHide("#konten *", "#fade", "#light");
	  }
  }
  function validasi()
  {
    jml	= document.getElementById("jml").value;
    vjmlbyr	  = parseFloat(formatulang(document.getElementById("vjumlah").value));
    jmlitem=0;
    for(i=1;i<=jml;i++){
  		vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
      jmlitem=jmlitem+vjmlitem;
    }
    if(vjmlbyr<jmlitem){
      return false;
    }else{
      return true;
    }
  }
  
  function view_bayar(a){
  	a=document.getElementById("iarea").value;
    b=document.getElementById("icustomer").value;
    c=document.getElementById("dalokasi").value;
    d=parseFloat(formatulang(document.getElementById("vjumlah").value));
    showModal("akt-bankin-multiallocnew/cform/giro/"+a+"/"+b+"/"+c+"/"+d+"/","#light"); 
    jsDlgShow("#konten *", "#fade", "#light");
  }

</script>

<?php 
# hutang lain2 (piutang dagang) khusus yg belum ketahuan customernya
?>
