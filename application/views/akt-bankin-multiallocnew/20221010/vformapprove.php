<table class="maintable">
    <tr>
        <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url' => 'akt-bankin-multiallocnew/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
            <div id="pelunasanform">
                <div class="effect">
                    <div class="accordion2">
                        <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
                            <tr>
                                <td>No Bank / Tgl Bank</td>
                                <td>
                                    <input type="text" id="ikbank" name="ikbank" value="<?php echo $ikbank; ?>"
                                        readonly>
                                    <input readonly id="dbank" name="dbank" value="<?php echo $dbank; ?>">
                                </td>

                                <td>No DT</td>
                                <td>
                                    <!-- <input type="text" readonly id="idt" name="idt" value="" onclick='showModal("akt-bankin-multiallocnew/cform/dt/<?php #echo $iarea;?>/<?php #echo 'sikasep'; ?>/","#light"); jsDlgShow("#konten *", "#fade", "#light");'> -->
                                    <input readonly id="idt" name="idt" onclick='cekdt();' placeholder="Harap diisi">
                                    <input readonly id="areadt" name="areadt" type="hidden">
                                </td>
                            </tr>

                            <tr>
                                <td>Area / Tgl Alokasi</td>
                                <td>
                                    <input readonly id="eareaname" name="eareaname" value="<?php echo $eareaname; ?>">
                                    <input id="iarea" name="iarea" type="hidden" value="<?php echo $iarea; ?>">
                                    <input readonly id="dalokasi" name="dalokasi" value="<?php echo $dbank; ?>">
                                </td>

                                <td>Debitur</td>
                                <td>
                                    <input type="text" readonly id="ecustomername" name="ecustomername" value=""
                                        onclick='return tiasa();'>
                                    <input type="hidden" readonly id="icustomer" name="icustomer" value="">
                                    <input type="hidden" id="icoabank" name="icoabank" value="<?php echo $icoabank; ?>">
                                </td>

                            </tr>

                            <tr>
                                <td>Bank</td>
                                <td>
                                    <input readonly id="ebankname" name="ebankname" value="<?php echo $ebankname; ?>">
                                </td>

                                <td>Alamat</td>
                                <td>
                                    <input id="ecustomeraddress" name="ecustomeraddress" readonly value="">
                                </td>

                            </tr>

                            <tr>
                                <td>Jumlah</td>
                                <td>
                                    <input readonly type="text" id="vjumlah" name="vjumlah"
                                        value="<?php echo number_format($vsisa); ?>">
                                    <input type="hidden" id="vlebih" name="vlebih" value="0">
                                    <input type="hidden" id="vsisa" name="vsisa" value="<?php echo $vsisa; ?>">
                                    <input type="hidden" name="jml" id="jml" value="0">
                                    <!-- //* PENAMBAHAN 07 JUN 2021 -->
                                    <input type="hidden" name="totalmt" id="totalmt" value="0" readonly>
			                        <input type="hidden" name="jmlmt" id="jmlmt" value="0">
                                </td>

                                <td>Kota</td>
                                <td>
                                    <input readonly id="ecustomercity" name="ecustomercity" value="">
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td>KU/Giro/Tunai</td>
                                <td>
                                    <input readonly type="text" id="igiro" name="igiro" value="<?php echo $igiro; ?>" onclick="view_bayar('<?php echo $iarea; ?>')">
                                </td>
                            </tr>

                            <tr>
                                <td width="100%" align="center" colspan="4">
                                    <input name="login" id="login" value="Simpan" type="submit" onmouseover="hetangmt();"
                                        onclick="return dipales()">
                                    <input name="cmdreset" id="cmdreset" value="Keluar" type="button"
                                        onclick='show("akt-bankin-multiallocnew/cform/view/<?php echo $dfrom . '/' . $dto . '/' . $iarea . '/'; ?>","#main")'>
                                    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
                                        onclick='tambah_item(parseFloat(document.getElementById("jml").value)+1);'>
                                </td>
                            </tr>
                        </table>

                        <div id="detailheader" align="center"></div>
                        <div id="detailisi" align="center"></div>
</table>
</div>
</div>
</div>
<?= form_close() ?>
<div id="pesan"></div>
</td>
</tr>
</table>
<script language="javascript" type="text/javascript">
function cekdt() {
    if (document.getElementById("idt").value != '' && document.getElementById("icustomer").value != '') {
        
        document.getElementById("icustomer").value = '';
        document.getElementById("ecustomername").value = '';
        document.getElementById("icoabank").value = '';
        document.getElementById("ecustomeraddress").value = '';
        document.getElementById("ecustomercity").value = '';

        showModal("akt-bankin-multiallocnew/cform/dt/" + document.getElementById("iarea").value + "/" + document
            .getElementById("dbank").value + "/", "#light");
        jsDlgShow("#konten *", "#fade", "#light");
    } else {
        showModal("akt-bankin-multiallocnew/cform/dt/" + document.getElementById("iarea").value + "/" + document
            .getElementById("dbank").value + "/", "#light");
        jsDlgShow("#konten *", "#fade", "#light");
    }
}

function tiasa() {
    $s = 0;
    if (document.getElementById("idt").value == '') {
        alert('No DT Tidak Boleh Kosong!');
        $s = 1;
        return false;
    } else {
        /*showModal("akt-bankin-multiallocnew/cform/customer/<?php #echo $iarea; 
        													?>/<?php #echo 'sikasep'; 
        																			?>/","#light"); jsDlgShow("#konten *", "#fade", "#light");*/

        showModal("akt-bankin-multiallocnew/cform/customer/" + document.getElementById("areadt").value + "/" + document
            .getElementById("idt").value + "/" + document.getElementById("icustomer").value + "/sikasep/", "#light");
        jsDlgShow("#konten *", "#fade", "#light");
    }
}

function dipales() {
    cek = 'false';
    cok = 'false';
    $s = 0;

    if (
        (document.getElementById("ikbank").value != '') &&
        (document.getElementById("dbank").value != '') &&
        (document.getElementById("dalokasi").value != '') &&
        (document.getElementById("vjumlah").value != '') &&
        (document.getElementById("vjumlah").value != '0') &&
        (document.getElementById("icustomer").value != '')
    ) {
        var a = parseFloat(document.getElementById("jml").value);
        for (i = 1; i <= a; i++) {
            if (document.getElementById("vjumlah" + i).value != '0') {
                sisa = parseFloat(formatulang(document.getElementById("vsisa" + i).value));
                sesa = parseFloat(formatulang(document.getElementById("vsesa" + i).value));
                awal = parseFloat(formatulang(document.getElementById("vjumlah" + i).value));
                ket = document.getElementById("eremark" + i).value;
                if ((sisa - awal > 0) || (sisa > 0 && (ket == '' || ket == "NULL"))) {
                    if ((sesa > 0 && ket == '')) {
                        alert('Keterangan sisa wajib diisi !!!');
                        cok = 'false';
                        cek = 'false';
                        $s = 1;
                        return false;
                    } else {
                        cok = 'true';
                        cek = 'true';
                    }
                } else {
                    cok = 'true';
                    cek = 'true';
                }
            } else {
                cek = 'false';
            }
        }
        if (cek == 'true' && cok == 'true') {
            document.getElementById("login").hidden = true;
            document.getElementById("cmdtambahitem").hidden = true;
        } else if (cok == 'false') {} else {
            alert('Isi jumlah detail pelunasan minimal 1 item !!!');
            document.getElementById("login").hidden = false;
        }
    } else {
        alert('Data header masih ada yang salah !!!');
    }
}

function tambah_item(a) {
    so_inner = document.getElementById("detailheader").innerHTML;
    si_inner = document.getElementById("detailisi").innerHTML;

    //    alert(si_inner);
    if (so_inner == '') {
        so_inner = '<table id="itemtem" class="listtable" style="width:946px;">';
        so_inner+= '<th style="width:30px;" align="center">No</th>';
        so_inner+= '<th style="width:130px;" align="center">Nota</th>';
        so_inner+= '<th style="width:80px;" align="center">Tgl Nota</th>';
        so_inner+= '<th style="width:115px;" align="center">Nilai</th>';
        so_inner+= '<th style="width:115px;" align="center">Bayar</th>';
        so_inner+= '<th style="width:115px;" align="center">Sisa</th>';
        so_inner+= '<th style="width:81px;" align="center">Meterai</th>';
        so_inner+= '<th style="width:115px;" align="center">Lebih</th>';
//		so_inner+= '<th style="width:115px;" align="center">ket1</th>';
        so_inner+= '<th style="width:129px;" align="center">Ket2</th>';
        document.getElementById("detailheader").innerHTML = so_inner;
    } else {
        so_inner = '';
    }
    if (si_inner == '') {
        document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
        juml = document.getElementById("jml").value;
        si_inner='<table id="itemdet"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
        si_inner+='<td style="width:129px;"><input style="width:129px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""><input readonly type="hidden" id="cust'+a+'" name ="cust'+a+'" value=""></td>';
        si_inner+='<td style="width:80x;"><input style="width:80px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
        si_inner+='<td style="width:115px;"><input readonly style="width:115px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""><input readonly style="width:115px;text-align:right;font-size:12px;" type="hidden" id="vnotaasal' + a + '" name="vnotaasal' + a + '" value=""></td>';
        si_inner+='<td style="width:115px;"><input style="width:115px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onKeyUp="hetangjml('+a+');reformat(this);" onpaste="return false;" placeholder="0"></td>';
        si_inner+='<td style="width:115px;"><input readonly style="width:115px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td>';
        si_inner+='<td style="width:68px;padding-left: 35px;padding-right: 11px;"><input type="checkbox" id="mt'+a+'" name="mt'+a+'" class="mt'+a+'" value="" onclick="return hetang('+ a +');" disabled><input type="hidden" id="vmeterai'+a+'" name="vmeterai'+a+'" value=""><input type="hidden" id="vmeteraisaldo'+a+'" name="vmeteraisaldo'+a+'" value=""><input type="hidden" id="jmt'+a+'" name="jmt'+a+'" value="0"></td>';
        si_inner+='<td style="width:114px;"><input readonly style="width:114px;text-align:right;font-size:12px;" type="text" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
//  	si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-multiallocnew/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
        si_inner+='<td style="width:120px;"><select id="eremark'+a+'" name="eremark'+a+'"><option value=""></option><option value="Retur">Retur</option><option value="Biaya Promo">Biaya Promo</option><option value="Kurang Bayar">Kurang Bayar</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Biaya Administrasi">Biaya Administrasi</option></select></td></tr></tbody></table>';
    } else {
        document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
        juml = document.getElementById("jml").value;
        si_inner+='<table id="itemdet"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
        si_inner+='<td style="width:129px;"><input style="width:129px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""><input readonly type="hidden" id="cust'+a+'" name ="cust'+a+'" value=""></td>';
        si_inner+='<td style="width:80x;"><input style="width:80px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
        si_inner+='<td style="width:115px;"><input readonly style="width:115px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""><input readonly style="width:115px;text-align:right;font-size:12px;" type="hidden" id="vnotaasal' + a + '" name="vnotaasal' + a + '" value=""></td>';
        si_inner+='<td style="width:115px;"><input style="width:115px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onKeyUp="hetangjml('+a+');reformat(this);" onpaste="return false;" placeholder="0"></td>';
        si_inner+='<td style="width:115px;"><input readonly style="width:115px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""></td>';
        si_inner+='<td style="width:68px;padding-left: 35px;padding-right: 11px;"><input type="checkbox" id="mt'+a+'" name="mt'+a+'" class="mt'+a+'" value="" onclick="return hetang('+ a +');" disabled><input type="hidden" id="vmeterai'+a+'" name="vmeterai'+a+'" value=""><input type="hidden" id="vmeteraisaldo'+a+'" name="vmeteraisaldo'+a+'" value=""><input type="hidden" id="jmt'+a+'" name="jmt'+a+'" value="0"></td>';
        si_inner+='<td style="width:114px;"><input readonly style="width:114px;text-align:right;font-size:12px;" type="text" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
//   	si_inner+='<td style="width:109px;"><input readonly style="width:109px;font-size:12px;" type="text" id="epelunasanremark'+a+'" name="epelunasanremark'+a+'" value="" onclick=\'showModal("akt-bankin-multiallocnew/cform/remark/'+a+'/","#light"); jsDlgShow("#konten *", "#fade", "#light");\'><input type="hidden" id="ipelunasanremark'+a+'" name="ipelunasanremark'+a+'" value=""></td>';
        si_inner+='<td style="width:120px;"><select id="eremark'+a+'" name="eremark'+a+'"><option value=""></option><option value="Retur">Retur</option><option value="Biaya Promo">Biaya Promo</option><option value="Kurang Bayar">Kurang Bayar</option><option value="Biaya Ekspedisi">Biaya Ekspedisi</option><option value="Biaya Administrasi">Biaya Administrasi</option></select></td></tr></tbody></table>';
    }
    j = 0;
    var baris = Array();
    var inota = Array();
    var dnota = Array();
    var vnota = Array();
    var vjumlah = Array();
    var vsesa = Array();
    var vsisa = Array();
    var vlebih = Array();
    //    var ipelunasanremark = Array();
    //    var epelunasanremark = Array();
    var eremark = Array();
    //* PENAMBAHAN 07 JUN 2021
    var mt				= Array();
    var vmeterai		= Array();
    var vmeteraisaldo	= Array();

    for (i = 1; i < a; i++) {
        j++;
        baris[j] = document.getElementById("baris" + i).value;
        inota[j] = document.getElementById("inota" + i).value;
        dnota[j] = document.getElementById("dnota" + i).value;
        vnota[j] = document.getElementById("vnota" + i).value;
        vjumlah[j] = document.getElementById("vjumlah" + i).value;
        vsesa[j] = document.getElementById("vsesa" + i).value;
        vsisa[j] = document.getElementById("vsisa" + i).value;
        vlebih[j] = document.getElementById("vlebih" + i).value;
        //		  ipelunasanremark[j]	= document.getElementById("ipelunasanremark"+i).value;
        //		  epelunasanremark[j]	= document.getElementById("epelunasanremark"+i).value;
        eremark[j] = document.getElementById("eremark" + i).value;
        //* PENAMBAHAN 07 JUN 2021 
        mt[j]				= document.getElementById("mt"+i).value;		
        vmeterai[j]		= document.getElementById("vmeterai"+i).value;		
        vmeteraisaldo[j]	= document.getElementById("vmeteraisaldo"+i).value;		

    }
    document.getElementById("detailisi").innerHTML = si_inner;
    j = 0;
    for (i = 1; i < a; i++) {
        j++;
        document.getElementById("baris" + i).value = baris[j];
        document.getElementById("inota" + i).value = inota[j];
        document.getElementById("dnota" + i).value = dnota[j];
        document.getElementById("vnota" + i).value = vnota[j];
        document.getElementById("vjumlah" + i).value = vjumlah[j];
        document.getElementById("vsesa" + i).value = vsesa[j];
        document.getElementById("vsisa" + i).value = vsisa[j];
        document.getElementById("vlebih" + i).value = vlebih[j];
        //		  document.getElementById("ipelunasanremark"+i).value=ipelunasanremark[j];
        //		  document.getElementById("epelunasanremark"+i).value=epelunasanremark[j];
        document.getElementById("eremark" + i).value = eremark[j];
        //* PENAMBAHAN 07 JUN 2021 
        document.getElementById("mt"+i).value=mt[j];
        document.getElementById("vmeterai"+i).value=vmeterai[j];
        document.getElementById("vmeteraisaldo"+i).value=vmeteraisaldo[j];
    }
    area = document.getElementById("areadt").value;
    ikbank = document.getElementById("ikbank").value;
    cust = document.getElementById("icustomer").value;
    idt = document.getElementById("idt").value;
    showModal("akt-bankin-multiallocnew/cform/nota/" + a + "/" + area + "/" + cust + "/" + idt + "/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
}

function hetang(x) {
    num = document.getElementById("vjumlah" + x).value.replace(/\,/g, '');
    nam	= document.getElementById("vnota"+x).value.replace(/\,/g,''); /* 07 JUN 2021 */
	mt  = document.getElementById("mt"+x); /* 10 APR 2021 */
	jmlmt = document.getElementById("jmlmt").value; /* 04 JUN 2021 */

    //* PENAMBAHAN 07 JUN 2021 : UNTUK DISABLE CEK BOX JIKA NILAI LEBIH = 0
    if(formatulang(document.getElementById("vlebih").value)==0){
		document.getElementById("mt"+x).setAttribute('disabled','true');
	}

    if (!isNaN(num)) {
        vjmlbyr = parseFloat(formatulang(document.getElementById("vjumlah").value));
        vlebihitem = vjmlbyr;
        vsisadt = parseFloat(formatulang(document.getElementById("vsisa").value));
        jml = document.getElementById("jml").value;
        //* PENAMBAHAN 07 JUN 2021
        totalmt 	= parseFloat(document.getElementById("totalmt").value);
		vtotalmt 	= 0;
        
        if(mt.checked) {
			if(num == nam){
				if((formatulang(document.getElementById("vsesa"+x).value) == 0)&&(formatulang(document.getElementById("vlebih").value) > 0)){
					document.getElementById("mt"+x).value = document.getElementById("vmeterai"+x).value;
					document.getElementById("mt"+x).setAttribute('checked','true');

					for (a = 1; a <= jml; a++) {
						cek         = parseFloat(document.getElementById("mt"+a).value);
						vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
						vnota   	= parseFloat(formatulang(document.getElementById("vsisa"+a).value));
						vjmlitem    = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
						
						vsisaitem 	= vnota-vjmlitem;
						if(vsisaitem<0){
							alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
							document.getElementById("vjumlah"+a).value=0;
							vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
							vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
							document.getElementById("mt"+a).setAttribute('disabled','true');
							document.getElementById("mt"+a).checked =  false;
						}
						
						if(cek > 0){
                            ceknilai2 = (vlebihitem - vjmlitem) - vmt;
                            //* FILTER ADA SISA BANK < 10.000, TIDAK BISA UNTUK BAYAR METERAI
                            if(ceknilai2<0){
                                alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
                                vlebihitem = (vlebihitem - vjmlitem);
                                document.getElementById("mt" + a).value = 0;
                                document.getElementById("mt" + a).checked =  false;
                            }else{
                                vlebihitem = (vlebihitem - vjmlitem) - vmt;
                            }
                            
							vtotalmt += vmt;
						}else{
							vlebihitem = (vlebihitem - vjmlitem);
						}
                        if (vlebihitem < 0) {
                            vlebihitem = vlebihitem + vjmlitem ;
                            vsisaitem = vnota - vlebihitem;
                            alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
                            document.getElementById("vjumlah" + a).value = formatcemua(vlebihitem);
                            vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
                            vlebihitem = 0;

                            document.getElementById("mt" + a).value = 0;
                            document.getElementById("mt" + a).checked =  false;
                        }
						document.getElementById("vsesa" + a).value = formatcemua(vsisaitem);
						document.getElementById("vlebih" + a).value = formatcemua(vlebihitem);
					}
					document.getElementById("totalmt").value = vtotalmt;
					document.getElementById("vlebih").value = formatcemua(vlebihitem);
				}else{
					alert("jumlah bayar tidak bisa lebih besar dari nilai bank !!!!!");
					return false;
				}
			}

			/* CEK JML BAYAR KURANG DARI SISA NOTA / TIDAK */
			if(num < nam){
				vjmlbyr	    = parseFloat(formatulang(document.getElementById("vjumlah").value));
				vlebihitem  = vjmlbyr;
				document.getElementById("mt"+x).value = 0;
        		document.getElementById("mt"+x).checked =  false;
				document.getElementById("mt"+x).disabled = true;
				//hitmeterai = totalmt-parseFloat(document.getElementById("vmeterai"+x).value);

				//if(hitmeterai < 0){
				//	document.getElementById("totalmt").value = 0;
				//}else{
				//	document.getElementById("totalmt").value = hitmeterai;
				//}

				for(a=1;a<=jml;a++){
					vnota   = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
					vjmlitem= parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
					cek         = document.getElementById("mt"+a).value;
					
					if(vjmlitem==0){
						bbotol();
					}

					vsisaitem =vnota-vjmlitem;
					if(vsisaitem<0){
						alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
						document.getElementById("vjumlah"+a).value=0;
						vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
						vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
						document.getElementById("mt"+a).setAttribute('disabled','true');
						document.getElementById("mt"+a).checked =  false;
					}
					
					if(cek > 0){
						vlebihitem = (vlebihitem - vjmlitem) - vmt;
            			vtotalmt += vmt;
					}else{
						vlebihitem = (vlebihitem - vjmlitem);
						//vtotalmt = vtotalmt;
					}
					
					if(vlebihitem<0){
						vlebihitem=vlebihitem+vjmlitem;
						vsisaitem =vnota-vlebihitem;
						alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
						document.getElementById("vjumlah"+a).value=formatcemua(vlebihitem);
						vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
						vlebihitem=0;
						document.getElementById("mt"+a).setAttribute('disabled','true');
						document.getElementById("mt"+a).checked =  false;
					}
					document.getElementById("vsesa"+a).value=formatcemua(vsisaitem);
					document.getElementById("vlebih"+a).value=formatcemua(vlebihitem);
				}
        		document.getElementById("totalmt").value = vtotalmt;
				document.getElementById("vlebih").value=formatcemua(vlebihitem);
			}/* else{
				document.getElementById("mt"+x).disabled = false;
			} */
			
			if(formatulang(document.getElementById("vlebih").value) == 0){
				for(a=1;a<=jmlmt;a++){
					// alert ("vmeterai"+a+" : "+document.getElementById("mt"+a).checked);
					if(document.getElementById("mt"+a).checked==0){
						document.getElementById("mt"+a).setAttribute('disabled','true');
					}
				}
			}
			/* ******************************************************************************** */
        }else{
			document.getElementById("mt"+x).value = 0;
			document.getElementById("mt"+x).removeAttribute('checked');

			if(num < nam){
				hitmeterai = totalmt-parseFloat(document.getElementById("vmeterai"+x).value);

				if(hitmeterai < 0){
					document.getElementById("totalmt").value = 0;
				}else{
					document.getElementById("totalmt").value = hitmeterai;
				}
			}

			var jmlmt = 0
			for(a=1;a<=jml;a++){
				vnota   = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				vjmlitem= parseFloat(formatulang(document.getElementById("vjumlah"+a).value));

				cek         = document.getElementById("mt"+a).value;
        		vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
				
				if(vjmlitem==0){
					bbotol();
				}

				// * KALAU ADA NILAI MATERAI SET VALUE 1
				if(document.getElementById("vmeterai"+a).value > 0){
					document.getElementById("jmt"+a).value = 1;
					jmlmt += parseFloat(document.getElementById("jmt"+a).value);
				}

				/* JIKA JUMLAH BAYAR KURANG DARI SISA NOTA */
				if(vjmlitem < vnota){
					document.getElementById("mt"+a).disabled = true;
				}else{
					document.getElementById("mt"+a).disabled = false;
				}
				
				vsisaitem =vnota-vjmlitem;
				if(vsisaitem<0){
					alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
					document.getElementById("vjumlah"+a).value=0;
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
					document.getElementById("mt"+a).setAttribute('disabled','true');
					document.getElementById("mt"+a).checked =  false;
				}
				
				if(cek > 0){
                    vlebihitem = (vlebihitem - vjmlitem) - vmt;
                    vtotalmt += vmt;
                }else{
                    vlebihitem = (vlebihitem - vjmlitem);
					//vtotalmt = vtotalmt;
                }
				if(vlebihitem<0){
					vlebihitem=vlebihitem+vjmlitem;
					vsisaitem =vnota-vlebihitem;
					alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
					document.getElementById("vjumlah"+a).value=formatcemua(vlebihitem);
					vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
					vlebihitem=0;
					document.getElementById("mt"+a).setAttribute('disabled','true');
					document.getElementById("mt"+a).checked =  false;
				}
				document.getElementById("vsesa"+a).value=formatcemua(vsisaitem);
				document.getElementById("vlebih"+a).value=formatcemua(vlebihitem);
				
			}
			document.getElementById("vlebih").value=formatcemua(vlebihitem);
			document.getElementById("totalmt").value=vtotalmt;
			document.getElementById("jmlmt").value=jmlmt;
		}
		// if(formatulang(document.getElementById("vlebih").value)==0){
		// 	document.getElementById("mt"+x).setAttribute('disabled','true');
		// }
		
		if(formatulang(document.getElementById("vlebih").value) == 0){
			for(a=1;a<=jmlmt;a++){
				// alert ("vmeterai"+a+" : "+document.getElementById("mt"+a).checked);
				if(document.getElementById("mt"+a).checked==0){
					document.getElementById("mt"+a).setAttribute('disabled','true');
				}
			}
		}
    }else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vjumlah"+x).value=0;
	}
}
function hetangjml(x)
  {	
    num		= document.getElementById("vjumlah"+x).value.replace(/\,/g,'');
	// mt  	= document.getElementById("mt"+x); /* 10 APR 2021 */
	// jmlmt 	= document.getElementById("jmlmt").value; /* 04 JUN 2021 */
    if(!isNaN(num)){
		vjmlbyr	    = parseFloat(formatulang(document.getElementById("vjumlah").value));
		vlebihitem  = vjmlbyr;
		//totalmt 	= parseFloat(document.getElementById("totalmt").value);
		//vtotalmt 	= 0;		
		jml			= document.getElementById("jml").value;
		
		for (a = 1; a <= jml; a++) {
			cek         = document.getElementById("mt"+a).value;
			vmt         = parseFloat(document.getElementById("vmeterai"+a).value);
			//vmt         = parseFloat(document.getElementById("mt"+a).value);
			vnota   	= parseFloat(formatulang(document.getElementById("vsisa"+a).value));
			vjmlitem    = parseFloat(formatulang(document.getElementById("vjumlah" + a).value));
			if(vjmlitem==0){
				bbotol();
			}
			vsisaitem 	= vnota-vjmlitem;
			if(vsisaitem<0){
				alert("jumlah bayar tidak bisa lebih besar dari nilai nota !!!!!");
				document.getElementById("vjumlah"+a).value=0;
				vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
				vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
				document.getElementById("mt"+a).setAttribute('disabled','true');
				document.getElementById("mt"+a).checked =  false;
			}
			
			if(vsisaitem>0){
				// vtotalmt = totalmt-document.getElementById("mt"+a).value;
				document.getElementById("mt"+a).value = 0;
				document.getElementById("mt"+a).setAttribute('disabled','true');
				document.getElementById("mt"+a).checked =  false;
			}else{
				document.getElementById("mt"+a).disabled = false;
			}
			
			if(cek > 0){
				vlebihitem = (vlebihitem - vjmlitem) - vmt;
                // ceknilai2 = (vlebihitem - vjmlitem) - vmt;
                //* FILTER ADA SISA BANK < 10.000, TIDAK BISA UNTUK BAYAR METERAI
                // if(ceknilai2<0){
                //     vlebihitem = (vlebihitem - vjmlitem);
                //     vsisaitem = vnota-vlebihitem;
                //     alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
                //     document.getElementById("mt" + a).value = 0;
                //     document.getElementById("mt" + a).checked =  false;
                // }else{
                //     vlebihitem = (vlebihitem - vjmlitem) - vmt;
                // }
				//vtotalmt += vmt;
			}else{
				vlebihitem = (vlebihitem - vjmlitem);
			}
			if(vlebihitem<0){
				vlebihitem=vlebihitem+vjmlitem;
				vsisaitem =vnota-vlebihitem;
				alert("jumlah item tidak bisa lebih besar dari nilai bayar !!!!!");
				document.getElementById("vjumlah"+a).value=formatcemua(vlebihitem);
				vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
				vlebihitem=0;

                document.getElementById("mt" + a).value = 0;
                document.getElementById("mt" + a).checked =  false;
			}
			document.getElementById("vsesa"+a).value=formatcemua(vsisaitem);
			document.getElementById("vlebih" + a).value = formatcemua(vlebihitem);
		}
		//document.getElementById("totalmt").value = vtotalmt;
		document.getElementById("vlebih").value = formatcemua(vlebihitem);
	}else{
		alert("jumlah bayar tidak bisa lebih besar dari nilai bank !!!!!");
		return false;
	}
  }
//!------
function hetangmt(){
    jml		= document.getElementById("jml").value;
    totmt 	= 0;
    for(a=1;a<=jml;a++){
        totmt += parseFloat(document.getElementById('mt'+a).value);
    }
    document.getElementById("totalmt").value=totmt;
}
function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
}
function cektanggal() {
    ddt = document.getElementById('ddt').value;
    dbukti = document.getElementById('dbukti').value;
    dtmpdt = ddt.split('-');
    dtmpbukti = dbukti.split('-');
    perdt = dtmpdt[2] + dtmpdt[1] + dtmpdt[0];
    perbukti = dtmpbukti[2] + dtmpbukti[1] + dtmpbukti[0];
    if ((perdt != '') && (perbukti != '')) {
        if (compareDates(document.getElementById('dbukti').value, document.getElementById('ddt').value) == -1) {
            alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
            document.getElementById("dbukti").value = '';
        }
    } else if (perdt > perbukti) {
        alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
        document.getElementById("dbukti").value = '';
    }
    document.getElementById('dcair').value = document.getElementById('dcairx').value;
}

function bbotol() {
    baris = document.getElementById("baris").value;
    si_inner = document.getElementById("detailisi").innerHTML;
    var temp = new Array();
    temp = si_inner.split('<tbody disabled="disabled">');
    if ((document.getElementById("vjumlah" + baris).value == '0')) {
        si_inner = '';
        for (x = 1; x < baris; x++) {
            si_inner = si_inner + '<tbody>' + temp[x];
        }
        j = 0;
        var barbar = Array();
        var inota = Array();
        var dnota = Array();
        var vnota = Array();
        var vjumlah = Array();
        var vsesa = Array();
        var vsisa = Array();
        var vlebih = Array();
        for (i = 1; i < baris; i++) {
            j++;
            barbar[j] = document.getElementById("baris" + i).value;
            inota[j] = document.getElementById("inota" + i).value;
            dnota[j] = document.getElementById("dnota" + i).value;
            vnota[j] = document.getElementById("vnota" + i).value;
            vjumlah[j] = document.getElementById("vjumlah" + i).value;
            vsesa[j] = document.getElementById("vsesa" + i).value;
            vsisa[j] = document.getElementById("vsisa" + i).value;
            vlebih[j] = document.getElementById("vlebih" + i).value;
        }
        document.getElementById("detailisi").innerHTML = si_inner;
        j = 0;
        for (i = 1; i < baris; i++) {
            j++;
            document.getElementById("baris" + i).value = barbar[j];
            document.getElementById("inota" + i).value = inota[j];
            document.getElementById("dnota" + i).value = dnota[j];
            document.getElementById("vnota" + i).value = vnota[j];
            document.getElementById("vjumlah" + i).value = vjumlah[j];
            document.getElementById("vsesa" + i).value = vsesa[j];
            document.getElementById("vsisa" + i).value = vsisa[j];
            document.getElementById("vlebih" + i).value = vlebih[j];
        }
        document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) - 1;
        jsDlgHide("#konten *", "#fade", "#light");
    }
}

function validasi() {
    jml = document.getElementById("jml").value;
    vjmlbyr = parseFloat(formatulang(document.getElementById("vjumlah").value));
    jmlitem = 0;
    for (i = 1; i <= jml; i++) {
        vjmlitem = parseFloat(formatulang(document.getElementById("vjumlah" + i).value));
        jmlitem = jmlitem + vjmlitem;
    }
    if (vjmlbyr < jmlitem) {
        return false;
    } else {
        return true;
    }
}

function view_bayar(a) {
    a = document.getElementById("iarea").value;
    b = document.getElementById("icustomer").value;
    c = document.getElementById("dalokasi").value;
    d = parseFloat(formatulang(document.getElementById("vjumlah").value));
    showModal("akt-bankin-multiallocnew/cform/giro/" + a + "/" + b + "/" + c + "/" + d + "/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
}
</script>

<?php 
# hutang lain2 (piutang dagang) khusus yg belum ketahuan customernya
?>