<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>css/dgu.css" />
</head>

<body id="bodylist">
    <div id="main">
        <div id="tmp">
            <?php echo "<center><h2>No Daftar Tagihan</h2></center>"; ?>
            <table class="maintable">
                <tr>
                    <td align="left">
                        <?php echo $this->pquery->form_remote_tag(array('url' => 'akt-bankin-multiallocnew/cform/dt/' . $iarea . '/' . $dbank, 'update' => '#tmp', 'type' => 'post')); ?>
                        <div id="listform">
                            <div class="effect">
                                <div class="accordion2">
                                    <table class="listtable">
                                        <thead>
                                            <tr>
                                                <td colspan="4" align="center">Cari data : 
                                                	<input type="text" id="cari" name="cari" value="">
                                                	<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">
                                                	<input type="hidden" id="dbank" name="dbank" value="<?php echo $dbank; ?>">
                                                	<input type="submit" id="bcari" name="bcari" value="Cari">
                                                </td>
                                            </tr>
                                        </thead>
                                        <th>Kd Area</th>
                                        <th>No DT</th>
                                        <th>Tgl DT</th>
                                        <th>Jumlah</th>
                                        <tbody>
                                            <?php 
											if ($isi) {
												foreach ($isi as $row) {
                                                    $ddt = date('d-m-Y',strtotime($row->d_dt));
													echo "<tr> 
                                                            <td>
                                                                <a href=\"javascript:setValue('$row->i_dt','$row->i_area')\">$row->i_area</a>
                                                            </td>
				  											<td>
				  												<a href=\"javascript:setValue('$row->i_dt','$row->i_area')\">$row->i_dt</a>
				  											</td>
                                                            <td>
                                                                <a href=\"javascript:setValue('$row->i_dt','$row->i_area')\">$ddt</a>
                                                            </td>
				  											<td>
				  												<a href=\"javascript:setValue('$row->i_dt','$row->i_area')\">".number_format($row->v_jumlah)."</a>
				  											</td>
														 </tr>";
												}
											}
											?>
                                        </tbody>
                                    </table>
                                    <?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
                                    <br>
                                    <center><input type="button" id="Keluar" name="Keluar" value="Keluar"
                                            onclick="bbatal()"></center>
                                </div>
                            </div>
                        </div>
                        <?= form_close() ?>
                    </td>
                </tr>
            </table>
        </div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
function setValue(a,b) {
    document.getElementById("idt").value = a;
    document.getElementById("areadt").value = b;
    //document.getElementById("esalesmanname").value = b;
    jsDlgHide("#konten *", "#fade", "#light");
}

function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
}
</script>