<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'cekkumasuk/cform/cariku';
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">
                                               <input type="hidden" id="customer" name="customer" value="<?php echo $icustomer; ?>">
                                               <input type="hidden" id="area" name="area" value="<?php echo $iarea; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>No Bukti</th>
	    	<th>Tgl Transfer</th>
	    	<th>Bank</th>
	    	<th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_kum);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_ku=$dd."-".$mm."-".$yy;
			  $row->v_jumlah=number_format($row->v_jumlah);
			  $row->v_sisa=number_format($row->v_sisa);
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_kum','$row->d_ku','$row->v_sisa','$row->e_bank_name','$yy')\">$row->i_kum</a></td>
			  <td><a href=\"javascript:setValue('$row->i_kum','$row->d_ku','$row->v_sisa','$row->e_bank_name','$yy')\">$row->d_ku</a></td>
			  <td><a href=\"javascript:setValue('$row->i_kum','$row->d_ku','$row->v_sisa','$row->e_bank_name','$yy')\">$row->e_bank_name</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_kum','$row->d_ku','$row->v_sisa','$row->e_bank_name','$yy')\">$row->v_jumlah</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_kum','$row->d_ku','$row->v_sisa','$row->e_bank_name','$yy')\">$row->v_sisa</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,d,x,y)
  {
    document.getElementById("igiro").value=a;
	document.getElementById("dgiro").value=b;
	document.getElementById("dku").value=b;
	document.getElementById("egirobank").value=x;
	document.getElementById("nkuyear").value=y;
    document.getElementById("vjumlah").value=d;
    document.getElementById("vlebih").value=d;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
