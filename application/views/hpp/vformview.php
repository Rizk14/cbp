<div id='tmp'>
<?php 
$tahunbelakang = substr($iperiode,2,2);
$tahun = '20'.$tahunbelakang;
$bulan = substr($iperiode,4,2);
if($bulan == '01'){
	$namabulan = 'Januari';
}else if($bulan == '02'){
	$namabulan = 'Februari';
}else if($bulan == '03'){
	$namabulan = 'Maret';
}else if($bulan == '04'){
	$namabulan = 'April';
}else if($bulan == '05'){
	$namabulan = 'Mei';
}else if($bulan == '06'){
	$namabulan = 'Juni';
}else if($bulan == '07'){
	$namabulan = 'Juli';
}else if($bulan == '08'){
	$namabulan = 'Agustus';
}else if($bulan == '09'){
	$namabulan = 'September';
}else if($bulan == '10'){
	$namabulan = 'Oktober';
}else if($bulan == '11'){
	$namabulan = 'November';
}else if($bulan == '12'){
	$namabulan = 'Desember';
}
?>

<h2><?php echo $page_title.' Periode : '.$namabulan.' '.$tahun; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'hpp/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	  	 <a id="downloadLink" onclick="exportF(this);"><input type="button" value="Export to Excel"></a>
	  	  <input type="hidden" name="bulan" id="bulan" value="<?=$bulan;?>">
		  <input type="hidden" name="tahun" id="tahun" value="<?=$tahun;?>">
    	  <table class="listtable" id="sitabel" border="1">
		<th>No</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>Beli</th>
		<th>Opname</th>
		<th>Harga</th>
		<th>Total</th>
	    <tbody>
	      <?php 
		if($isi){
      $no=0;
      $grandtotal=0;
      $gtso=0;
      $gtbeli=0;
      foreach($isi as $row){
        $no++;
        $total=$row->n_opname_total*$row->v_harga;
        $grandtotal=$grandtotal+$total;
        $gtso=$gtso+$row->n_opname_total;
        $gtbeli=$gtbeli+$row->n_beli;
			  echo "<tr><td align=right>$no</td>
				  <td>".$row->i_product."</td>
				  <td>".$row->e_product_name."</td>
				  <td align=right>".number_format($row->n_beli)."</td>
				  <td align=right>".number_format($row->n_opname_total)."</td>
				  <td align=right>".number_format($row->v_harga)."</td>
 				  <td align=right>".number_format($total)."</td>
				</tr>";
			}
		  echo "<tr><td align=right colspan=3>Total</td>
		    <td align=right>".number_format($gtbeli)."</td>
	      <td align=right>".number_format($gtso)."</td>
        <td></td>
			  <td align=right>".number_format($grandtotal)."</td>
			</tr>";

		}
	      ?>
	    </tbody>
	  </table>
    <!-- <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button"> -->
	<a id="downloadLink" onclick="exportF(this);"><input type="button" value="Export to Excel"></a>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
//   $( "#cmdreset" ).click(function() {  
//     var Contents = $('#sitabel').html();    
//     window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//   });

  function exportF(elem) {
    var bulan       = $('#bulan').val();
    var tahun       = $('#tahun').val();
    var table       = document.getElementById("sitabel");
    var html        = table.outerHTML;
    var url         = 'data:application/vnd.ms-Excel,' + escape(html); // Set your html table into url 
    elem.setAttribute("href", url);
    elem.setAttribute("download", "Nilai Stock (HPP) ( "+bulan+"-"+tahun+").xls"); // Choose the file name
    return false;
}


</script>

