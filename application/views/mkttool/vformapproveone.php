
<?php
// include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <!--	<link href="<?php echo base_url() . 'assets/css/bootstrap.css'; ?>" rel="stylesheet">
	<link href="<?php echo base_url() . 'assets/themes/default/css/bootstrap.min.css'; ?>" rel="stylesheet"> -->
  <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body>
  <div id="main">
    <div id="tmpx">
      <h2>Informasi</h2>
      <table class="maintable">
        <tr>
          <td align="left">
            
            <?php echo $this->pquery->form_remote_tag(array('url' => 'mkttool/cform/approveas', 'update' => '#pesan', 'type' => 'post')); ?>
            <input type="hidden" id="userid" value="<?php echo $this->session->userdata('user_id');?>">
            <input type="hidden" id="level" value="<?php echo $this->session->userdata('level');?>">
            <input type="hidden" id="dept" value="<?php echo $this->session->userdata('departement');?>">
            <div id="aktkkform">
              <div class="effect">
                <div class="accordion2">
                  <?php
                  if ($isi->d_mkt) {
                    $tmp           = explode("-", $isi->d_mkt);
                    $th            = $tmp[0];
                    $bl            = $tmp[1];
                    $hr            = $tmp[2];
                    $isi->d_mkt = $hr . '-' . $bl . '-' . $th;
                  }
                  ?>
                  <table class="maintable">
                    <tr>
                      <td align="left">
                        <table class="mastertable" border="0">
                          <tr>
                            <td width="12%">Kode Marketing Tool</td>
                            <td style="width:20%">
                            <input style="width:190px" readonly value="<?php echo $isi->i_mkt; ?>" id="imkt" name="imkt">
                            <input type="hidden" id="jenis" name="jenis" value = '1'>
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr><!-- END tr baris 1 -->

                          <tr>
                            <td width="12%">Tanggal Pengajuan</td>
                            <td style="width:20%">
                              <input style="width:190px" readonly type="text" value="<?php echo $isi->d_mkt; ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr><!-- END tr baris 1 -->

                          <tr>
                            <td width="12%">Customer</td>
                            <td style="width:20%">
                              <input style="width:300px" readonly type="text" value="<?php echo $isi->i_customer . " - " . $isi->e_customer_name; ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr>

                          <tr>
                            <td width="12%">Salesman</td>
                            <td style="width:20%">
                              <input style="width:190px" readonly type="text" value="<?php echo $isi->i_salesman . " - " . $isi->e_salesman_name; ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr>

                          <tr>
                            <td width="15%">Estimasi Yang Diajukan</td>
                            <td style="width:20%" class="huruf nmper">
                              <input style='width:190px' type="text" value="Rp <?php echo number_format($isi->biaya); ?>" readonly>
                            </td>
                          </tr>

<!-- INI DIKOMEN
                          <?php if($note){?>
                          <tr>
                            <td width="15%">Daftar Note</td>
                            <td style="width:20%" class="huruf nmper">
                               <input name="note1" id="note1" style='width:190px' type="text" value="" > 
                              <?php 
                              
                                  foreach($note as $row){	                      ?>
                                      <div style="display: flex; justify-content: space-between; width: 250px;">
                                      <span>Departement (<?php echo $row->e_user_name?>)</span> <span>: <?echo($row->e_note);?></span></div>
                                  <?}
                            }?> -->
                          <?php if($note){?>
                          <tr>
                            <td width="15%">Daftar Note</td>
                            <td style="width:20%" class="huruf nmper">
                              <!-- <input name="note1" id="note1" style='width:190px' type="text" value="" > -->
                              <?php 
                              if($note){
                                  foreach($note as $row){
                                    if($row->f_mkt_rollback == 't'){
                                      $roll = 'Rollback';
                                    }else{
                                      $roll = 'Note';
                                    } 
                                    ?>
                                      <!-- <div style="display: flex; justify-content: space-between; width: 250px;"> -->
                                      <div style="display: flex; margin-bottom:0.25rem;">
                                      <label style="display: block;width:54%"><?php echo nl2br($roll); ?> Dari <?php echo nl2br($row->e_user_name)?></label> : 
                                    <span> <?echo nl2br(($row->e_note));?>
                                    </div>
                                      <!-- <span><?php echo nl2br($roll); ?> Dari <?php echo nl2br($row->e_user_name)?> </span> <span>: <?echo nl2br(($row->e_note));?></span></div> -->
                                  <?}
                            }
                          }?>
                            </td>
                          </tr>
                          <tr>
                            <td width="15%">Note</td>
                            <td style="width:20%" class="huruf nmper">
                              <!-- <input name="note1" id="note1" style='width:190px' type="text" value="" > -->
                              <textarea name="note1" id="note1" style='width:190px' type="textarea" value="" ></textarea>
                            </td>
                          </tr>

                        </table><br>
                        <center>
                          <input name="login" id="login" value="Approve" type="submit" onclick="return dipales();" <?php if ($isi->f_mkt_approve == FALSE ) {
                                                                                                                      echo "disabled";
                                                                                                                    } ?>>&nbsp;&nbsp;&nbsp;
                          <input type="button" id="batal" name="batal" value="Kembali" onclick="bbatal()">&nbsp;&nbsp;
                          <input type="button" id="reject" name="reject" value="Reject" onclick="rejectttt(); dipales();">&nbsp;&nbsp;
                          <input name="balik" id="balik" style="display: none;" value="Keluar" type="button" onclick='show("mkttool/cform/view/","#tmp");bbatal();'>
                        </center>
                        <!-- <center>
            
            <br><br>
	    			<input name="login" id="login" value="Simpan" type="submit" onclick="return dipales();">&nbsp;&nbsp;
            <input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"> 
	    		</center> -->
                        <div id="pesan"></div>
                        <?= form_close() ?>
                      </td>
                    </tr>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>
<script language="javascript" type="text/javascript">




function dipales(){
  	 /* if((document.getElementById("iloyal").value!='') &&
  	 	(document.getElementById("eloyal").value!='') &&
  	 	(document.getElementById("vestimasi").value!=''))
		  { */
  	  	  document.getElementById("login").hidden=true;
          document.getElementById("reject").hidden=true;
/*       }else{
          document.getElementById("login").hidden=false;
   		    alert('LENGKAPI DATA ISIAN !!!');
    } */
  }


/*   function dipales() {
    $s = 0;
    var str = document.getElementById("etelat2").value;
    var etelat2 = str.replace(/\s+/g, '');
    var show = document.getElementById('balik');

    if (document.getElementById("dterima").value == '' || document.getElementById("dterima").value == null || document.getElementById("dterima").value == 'null') {
      alert('Tanggal Approve Keuangan belum diisi..!!');
      return false;
    } else if ((document.getElementById("terima").value > 2) && (document.getElementById("etelat").value == '') && (etelat2 == '')) {
      alert('Keterangan Terlambat Belum diisi!');
      $s = 1;
      return false;
    } else {
      document.getElementById("login").hidden = true;
      if (show.style.display === 'none') {
        show.style.display = 'block';
      }
    }
  } */

  function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
    
		$.ajax({
			url      : '<?php echo site_url('mkttool/cform/view/')?>',
			success  : function(response){$('#main').html(response);},
			type     : 'post',
			dataType : 'html'
		});
  }

  function rejectttt() {
    /* alert("xxxxxx"); */
    /* jsDlgHide("#konten *", "#fade", "#light"); */
    note    = document.getElementById('note1').value;
    imkt    = document.getElementById('imkt').value;
    userid  = document.getElementById('userid').value;
    dept    = document.getElementById('dept').value;
    level   = document.getElementById('level').value;
    jabatan = 5;
    /* roll    = document.getElementById('roll').value; */
    $.ajax({
			url      : '<?php echo site_url('mkttool/cform/reject/')?>',
			type     : 'post',
			dataType : 'html',
      data:{
        note    : note,
        imkt    : imkt,
        userid  : userid,
        dept    : dept,
        level   : level,
        jabatan : jabatan
      },
      success  : function(response){
        alert("Marketing Tools Reject");
        $('#pesan').html(response);
        document.getElementById("login").hidden=true;
        document.getElementById("roll").hidden=true;
      },
		});
  }
</script>