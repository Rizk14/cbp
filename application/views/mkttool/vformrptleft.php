<style type="text/css">
	.all-border {
		border: 1px solid #000;
	}

	.mint-green {
		color: #fff;
		background-color: #528f9a;
		font-weight: bold;
	}

	.yellow{
		background-color: #F9E628;
		font-weight: bold;
	}

	.red {
		color: #f00;
	}
</style>
<?php
/* function monthindo($month){
	switch ($month) {
		case '01': return 'Januari';   break;
		case '02': return 'Februari';  break;
		case '03': return 'Maret';     break;
		case '04': return 'April';     break;
		case '05': return 'Mei';       break;
		case '06': return 'Juni';      break;
		case '07': return 'Juli';      break;
		case '08': return 'Agustus';   break;
		case '09': return 'September'; break;
		case '10': return 'Oktober';   break;
		case '11': return 'November';  break;
		case '12': return 'Desember';  break;
		default: return 'Undefined'; break;
	}
}

function dateindo($param, $sub = ''){
	$step1 = dmY($param);

	$step2 = explode('-',$step1);

	if($sub == ''){
		$step3 = array($step2[0], monthindo($step2[1]), $step2[2]);
	}else{
		$step3 = array($step2[0], substr(monthindo($step2[1]), 0, 3), $step2[2]);
	}

	$step4 = implode(' ', $step3);

	$result = $step4;

	return $result;
}

function dmY($date){
	return ($date != '') ? date('d-m-Y',strtotime(str_replace('/','-',$date))) : null;
} */


?>
<table>
	<tr><td align="center"><?=$isi->e_customer_name;?></td></tr>
	<tr><td align="center"><?=$isi->e_customer_address;?></td></tr>
	<tr><td align="center"><?=$isi->e_city_name;?></td></tr>
</table>

<br><br>

<table>
	<tr>
		<td style="width:140px;">
			<table border="1" style="width:120px;">
				<tbody>
					<tr>
						<td style="height: 23px;" align="center"><div style="font-size: 5px;">&nbsp;</div>KODE LANG</td>
						<td style="height: 23px;" align="center"><div style="font-size: 5px;">&nbsp;</div><?=$isi->i_customer;?></td>
					</tr>
				</tbody>
			</table>
		</td>
		<td style="width:250px;">
			<table border="1" align="center">
				<tr>
					<th class="mint-green" width="100px">Outlet</th>
					<th class="mint-green" width="40px">TOP</th>
					<th class="mint-green" width="154px">Discount (%)</th>
					<th class="mint-green" width="50px">Harga</th>
				</tr>
				<tr>
					<td><?=$isi->e_customer_classname;?></td>
					<td><?=$isi->n_customer_toplength;?></td>
					<td><?=$isi->n_customer_discount1;?> + <?=$isi->n_customer_discount2;?> + <?=$isi->n_customer_discount3;?></td>
					<td>H<?=$isi->i_price_group;?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<br><br>

<table>
	<tbody>
		<tr>
			<td align="center" class="all-border mint-green" width="106px">BULAN</td>
			<td align="center" class="all-border mint-green" width="76px"><?=$year->year1;?></td>
			<td align="center" class="all-border mint-green" width="76px"><?=$year->year2;?></td>
			<td align="center" class="all-border mint-green" width="76px"><?=$year->year3;?></td>
			<td align="center" class="all-border mint-green" width="75px">BIAYA ETA</td>
			<td align="center" class="all-border mint-green" width="75px">BIAYA REAL</td>
		</tr>
		<?php
		$total1   = 0;
		$total2   = 0;
		$total3   = 0;
		$totalbya = 0;
		$totalbyareal = 0;

		foreach($netto as $n => $row){
			$biaya     = 0;
			$biayareal = 0;
			

			if(isset($biayath) && $biayath != 'zero'){
				foreach($biayath as $bya){
					if($bya->n_month == $row->n_month){
						$biaya     = $bya->v_biaya;
						$biayareal = $bya->v_biaya_realisasi;
					}
				}
			}
			echo '
			<tr>
			<td class="all-border"> '.strtoupper(mbulan($row->n_month)).'</td>
			<td align="right" class="all-border">'.number_format($row->v_netto_first).'&nbsp;&nbsp;</td>
			<td align="right" class="all-border">'.number_format($row->v_netto_mid).'&nbsp;&nbsp;</td>
			<td align="right" class="all-border">'.number_format($row->v_netto_last).'&nbsp;&nbsp;</td>
			<td align="right" class="all-border">'.number_format($biaya).'&nbsp;&nbsp;</td>
			<td align="right" class="all-border">'.number_format($biayareal).'&nbsp;&nbsp;</td>
			</tr>
			';

			$total1       += $row->v_netto_first;
			$total2       += $row->v_netto_mid;
			$total3       += $row->v_netto_last;
			$totalbya     += $biaya;
			$totalbyareal += $biayareal;
		}

		?>
	</tbody>
	<tfoot>
		<tr>
			<td class="all-border yellow">&nbsp;TOTAL</td>
			<td align="right" class="all-border yellow"><?=number_format($total1);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border yellow"><?=number_format($total2);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border yellow"><?=number_format($total3);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border yellow"><?=number_format($totalbya);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border yellow"><?=number_format($totalbyareal);?>&nbsp;&nbsp;</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="right" colspan="2"><div style="font-size: 5px;">&nbsp;</div>% Biaya To Nota This Year<div style="font-size: 5px;">&nbsp;</div></td>
			<td align="right">
				<div style="font-size: 5px;">&nbsp;</div>
				<?php echo number_format(($totalbya / ( ($total2 != 0) ? $total2 : 2)) * 100, 2);?> %
				<div style="font-size: 5px;">&nbsp;</div>
			</td>
			<td align="right">
				<div style="font-size: 5px;">&nbsp;</div>
				<?php echo number_format(($totalbyareal / ( ($total2 != 0) ? $total2 : 2)) * 100, 2);?> %
				<div style="font-size: 5px;">&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td class="all-border">&nbsp;TARGET PERTAHUN</td>
			<td align="right" class="all-border"><?=number_format($ytarget[0]);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border"><?=number_format($ytarget[1]);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border"><?=number_format($ytarget[2]);?>&nbsp;&nbsp;</td>
			<td align="right"></td>
		</tr>
		<tr>
			<td class="all-border">&nbsp;TARGET PERBULAN</td>
			<td align="right" class="all-border"><?=number_format($mtarget[0]);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border"><?=number_format($mtarget[1]);?>&nbsp;&nbsp;</td>
			<td align="right" class="all-border"><?=number_format($mtarget[2]);?>&nbsp;&nbsp;</td>
			<td align="right"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>

	</tfoot>
</table>

<!-- <table>
	<tr>
		<td width="130px">TARGET MARKETING</td>
		<td width="10px">:</td>
		<td width="100px">00,000,000</td>
	</tr>
</table> -->

<br>

<table>
	<tr>
		<td width="105px"><div style="font-size: 3px;">&nbsp;</div>PERMINTAAN<div style="font-size: 3px;">&nbsp;</div></td>
		<td width="10px"><div style="font-size: 3px;">&nbsp;</div>:<div style="font-size: 3px;">&nbsp;</div></td>
		<td width="370px" class="all-border"><div style="font-size: 3px;">&nbsp;</div>&nbsp;<?=$isi->e_tool_remark;?><div style="font-size: 3px;">&nbsp;</div></td>
	</tr>
</table>

<?php if($isi->i_mkt_type != '2'):?>
	<table>
		<tr>
			<td width="105px"><div style="font-size: 3px;">&nbsp;</div>ESTIMASI BIAYA</td>
			<td width="10px"></td>
			<td width="370px"><div style="font-size: 3px;">&nbsp;</div>&nbsp;<?=number_format($isi->v_biaya_eta);?></td>
		</tr>
	</table>
<?php endif;?>

<p><?php echo $isi->d_mkt;?></p>
<!-- #=dateindo($isi->d_mkt); -->


<table>
	<tr>
		<td align="center">
			<table>
				<tr><td>Hormat Kami</td></tr>
				<tr><td><br><br><br><br></td></tr>
				<tr><td><!-- NAMA MARKETING STAFF --></td></tr>
				<tr><td>Marketing Staff</td></tr>
			</table>
		</td>
		<td></td>
		<td align="center">
			<table>
				<tr><td>Mengetahui</td></tr>
				<tr><td><br><br><br><br></td></tr>
				<tr><td> <!-- NAMA Regional Sales Manager --><?#=$isi->e_pic_name;?></td></tr>
				<tr><td>RSM</td></tr>
			</table>
		</td>
		<td></td>
		<td align="center">
			<table>
				<tr><td>Menyetujui</td></tr>
				<tr><td><br><br><br><br></td></tr>
				<tr><td><!-- NAMA GM --></td></tr>
				<tr><td>General Manager</td></tr>
			</table>
		</td>
	</tr>
</table>

<?php
/* if($isi->i_mkt_type == '2'){ */
	$filepath = 'mkttool/' . $isi->e_attch4;
	if($isi->e_attch4 == '' || $isi->e_attch4 == NULL){
	}else{
	if(is_image($filepath) == true){
		echo '
		<br><br>
		<div>Foto Space yang Akan Digunakan :</div>
		<img src="'.base_url($filepath).'" width="280px" height="150px">
		';
		}
		/* <img src="'.base_url($filepath).'" width="280px" height="150px"> */
		/* <img src="'.base_url($filepath).'"> */
	}

/* } */
?>