<style type="text/css">
	.all-border {
		border: 1px solid #000;
	}

	.bold {
		font-weight: bold;
	}

	.yellow{
		background-color: #F9E628;
		font-weight: bold;
	}
</style>
<br><br><br><br><br><br>
<div style="font-size: 10px;">&nbsp;</div>
<table>
	<tbody>
		<tr>
			<td class="yellow" width="70px" align="center">KODE LANG</td>
			<td class="yellow" width="252px" align="center">KETERANGAN BIAYA</td>
			<td class="yellow" width="70px" align="center">BULAN</td>
			<td class="yellow" width="90px" align="center">NOMINAL</td>
		</tr>
		<?php
		$n = 0;
		if($history != 'zero'){
			foreach($history as $ht){
				$n++;
				$bold   = ($imkt == $ht->i_ref) ? 'class="bold"' : '';

				echo '
				<tr>
				<td '.$bold.' align="center">'.$ht->i_customer.'</td>
				<td '.$bold.'>'.$ht->e_tool_remark.'</td>
				<td '.$bold.' align="center">'.$ht->n_month.'</td>
				<td '.$bold.' align="right">'.number_format($ht->v_biaya).'&nbsp;&nbsp;</td>
				</tr>
				';
				// <td '.$bold.' align="center">'.strtoupper(monthindo($ht->n_month)).'</td>
			}
		}
		?>
	</tbody>
</table>