<style>
.button {
  background-color: #bad6f0;
  border: 1;
  color: black;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 6px 4px;
  cursor: pointer;
  font-weight: bold;
}
.focus{
  color: #333;
  text-decoration: none;
}
.naonwaebebas{
	display: block;
	width: 100% !important;
	white-space: nowrap;
	overflow-x: auto;
	-webkit-overflow-scrolling: touch;
}
</style>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<div align="right">

	<input class="button" name="listdata" id="listdata" value="kembali" type="button" onclick='show("mkttool/cform/","#main")'>

	<!-- <input class="button" name="listdata1" id="listdata1" value="Export" type="button" onclick='show("mkttool/cform/export/"+document.getElementById("tahun").value+"/","#main")'> -->
	<a href="#" id="href" value="Transfer" target="blank" onclick="return exportexcel();"><button type="button" style="background-color: #bad6f0; font-weight: bold;" >Export</button></a>

</div>
<?php echo $this->pquery->form_remote_tag(array('url' => 'mkttool/cform/cariview', 'update' => '#main', 'type' => 'post')); ?>
<a href="" id="download-btn" class="noDisplay" download=""></a>
<table class="maintable naonwaebebas">
  <tr>
    <td align="left">
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table id="table" class="listtable">
	    <thead>
	      <tr>
		<td colspan="16" align="left">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;
			<select name="tahun" id="tahun">
  				<?php 
					$tahun1 = date('Y') - 3;
					$tahun2 = date('Y');
					for ($i = $tahun1; $i <= $tahun2; $i++) {
						echo "<option value='$i'";
						if ($thn == $i) {
							echo "selected";
						}
						echo " >$i</option>";
					}
					?>
  			</select>
		<input type="submit" id="bcari" name="bcari" value="Cari"></td>

	      </tr>
	    </thead>
		<!-- <th width = "100px">No Entry</th> -->
		<th>No Entry</th>
		<th>Approve</th>
		<th>Approve MKT</th>
		<th>STATUS</th>
		<th>TANGGAL</th>
		<th>SALESMAN</th>
		<th>TOKO</th>
		<th>JENIS PENGAJUAN</th>
		<th>BIAYA ESTIMASI</th>
		<th>BIAYA REALISASI</th>
		<th>ATTACH 1</th>
		<th>ATTACH 2</th>
		<th>ATTACH 3</th>
		<th class="action" >EDIT</th>
		<th class="action" >DELETE</th>
		<th class="action" >CETAK</th>
	    <tbody>
    <?php 
		
		$dept		= $this->session->userdata('departement');
		$userlevel	= $this->session->userdata('level');
		$directory 	= 'mkttool/';
		if($isi){
			foreach($isi as $row){
			$btndelete    	= '<a href="#" class="focus" onclick="hapusatuh(\''.$row->i_mkt.'\')"><img height=15px; style="cursor:hand;" src="'.base_url("img/delete.png").'" border="0" alt="delete"></a>';
			$btnappr    	= '<a href="#" class="focus" onclick="approveThis(\''.$row->i_mkt.'\')">Approve</a>';
			$btnapprmkt    	= '<a href="#" class="focus" onclick="approveThismkt(\''.$row->i_mkt.'\')">Approve</a>';
			$att1 = ($row->e_attch1 != '') ? '<a href="#" data-href="'.base_url($directory.$row->e_attch1).'" class="focus" download="" onclick="downloadthis(this)">Download</a>' : '';
			$att2 = ($row->e_attch2 != '') ? '<a href="#" data-href="'.base_url($directory.$row->e_attch2).'" class="focus" download="" onclick="downloadthis(this)">Download</a>' : '';
			$att3 = ($row->e_attch3 != '') ? '<a href="#" data-href="'.base_url($directory.$row->e_attch3).'" class="focus" download="" onclick="downloadthis(this)">Download</a>' : '';
			$rapprlevel = ($row->i_approve_level == null) ? '6' : $row->i_approve_level;
			  $sales=$row->i_salesman.'-'.$row->e_salesman_name;
			  $customer=$row->i_customer.'-'.$row->e_customer_name;
			  if($row->v_biaya_eta == ''){
				$biaya = 0;
			  }else{
				$biaya = number_format($row->v_biaya_eta);
			  }

				if($userlevel == '6'){
					$btnapprove = '';
				}
				else if($row->f_cancel != 't' && ($userlevel < $rapprlevel)){
					if(($rapprlevel - $userlevel) == '1' || $userlevel <= '3'){
						if($rapprlevel == '5' && $row->f_mkt_approve != 't'){
							$btnapprove = '';
						}else if($userlevel <= '3' && $rapprlevel <= '3'){
							$btnapprove = '';
						}else{
							$btnapprove = $btnappr;
						}
					}else{
						$btnapprove = '';
					}
				}else{
					$btnapprove = '';
				}

				if(
					($dept == '9' || $dept == 'A' || $dept <= '5') &&
					(
						strtolower($row->e_status) != 'selesai' &&
						strtolower($row->e_status) != 'batal'
					) &&
					$row->f_mkt_approve == 't'
				){
					$btnapprove = '<a href="#" class="btn" onclick="approveThisAdv(\''.$row->i_mkt.'\')">Approve</a>';
				}
				
				if($rapprlevel == '0'){
					$btnapprove = '';
				}

				if($row->f_mkt_approve == 't'){
					$btnapprovemkt = 'Sudah Approve';
				}else if($dept == '9' && $rapprlevel == '5' && $row->f_mkt_approve != 't'){
					$btnapprovemkt = $btnapprmkt;
				}else{
					$btnapprovemkt = '';
				}

				if($row->f_cancel != 't'){
					if($userlevel == '6' && $rapprlevel == '6'){
						$delete = $btndelete;
					}else if($userlevel == '6' && $rapprlevel != '6'){
						$delete = '';
					}else if(($userlevel < '6' && $userlevel >= '3') && ($rapprlevel - $userlevel) == 1){
						$delete = $btndelete;
					}else if($userlevel < '3' && $rapprlevel >= '4'){
						$delete = $btndelete;
					}else{
						$delete = '';
					}
					if($dept == '8' && $rapprlevel == '5' && $row->f_mkt_approve != 't'){
						$delete = $btndelete;
					}
				}else{
					$delete = '';
				}

				if($row->i_mkt_type == 1){
					$jenis = $row->i_loyalitas_type;
				}else if($row->i_mkt_type == 2){
					$jenis = $row->i_promo_type2;
				}else if($row->i_mkt_type == 3){
					$jenis = $row->e_jenis_market;
				}

			  	  echo "<tr> 
				  <td>$row->i_mkt</td>";
					$this->db->select("	* from tm_marketing_tool_loa 
				  					 	where i_mkt_type = '$row->i_mkt_type' and i_level = '$rapprlevel' 
				  					 	and i_jenis = trim('$jenis')",false);
						$query = $this->db->get();
						/* echo $this->db->last_query(); */
						if ($query->num_rows() > 0){
							foreach($query->result() as $siw){
								$awal=$siw->v_loa_awal;
								$akhir=$siw->v_loa_akhir;
							}
						}else{
							$awal 	= 0;
							$akhir 	= 0;
						}
				
				if($row->f_reject == 'f' ){		
				/*buat loyalitas sama marketing tools*/		
				 if($row->v_biaya_eta < $awal && $row->i_mkt_type != 2){
					echo "
					<td>$btnapprove</td>
					<td>$btnapprovemkt</td>";	
				 }else if($row->v_biaya_eta >= $awal && $row->v_biaya_eta <= $akhir && $row->f_mkt_approve == 't' && $row->i_mkt_type != 2){
					echo "
					<td></td>
					<td></td>";	
				 }else if($row->v_biaya_eta >= $akhir && $row->i_mkt_type != 2 ){
					if($rapprlevel <= '2'){
						echo "
						<td></td>
						<td></td>";	
					}else{
						echo "
						<td>$btnapprove</td>
						<td>$btnapprovemkt</td>";					
					}
				 }
				 
				 /*buat yang promo*/
				 else if($row->e_diskon_request < $awal &&$row->i_mkt_type == 2){
					echo "
					<td></td>
					<td></td>";
				 }else if($row->e_diskon_request >= $awal && $row->e_diskon_request <= $akhir && $row->f_mkt_approve == 't' && $row->i_mkt_type = 2){
					echo "
					<td></td>
					<td></td>";	
				 }else if($row->e_diskon_request >= $akhir && $row->i_mkt_type = 2){
					echo "
					<td>$btnapprove</td>
					<td>$btnapprovemkt</td>";	
				 }else{
					echo "
					<td>$btnapprove</td>
					<td>$btnapprovemkt</td>";	
				 }
				}else{
					echo "
					<td></td>
					<td></td>";	
				}

				 if($row->v_biaya_realisasi > 0 && $row->v_biaya_eta >= $awal && $row->v_biaya_eta <= $akhir && $row->f_mkt_approve == 't'){
					$status = 'Selesai (LOA)';
				 }else{
					$status = $row->e_status;
				 }
				 
				 $real = number_format($row->v_biaya_realisasi);	
				  echo "
				  <td>$status</td>
				  <td>$row->d_mkt</td>
				  <td>$sales</td>
				  <td>$customer</td>
				  <td>$row->marketing_type</td>
				  <td>$biaya</td>
				  <td>$real</td>
				  <td>$att1</td>
				  <td>$att2</td>
				  <td>$att3</td>
				  <td class=\"action\">
					<a href=\"#\" onclick=\"editatuh('$row->i_mkt');\">
					<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\">
					</a></td>
					<td class=\"action\">$delete</td>";
					echo "
					<td class=\"action\">
					<a href=\"#\" onclick=\"yyy('$row->i_mkt');\">
					<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/print.gif\" border=\"0\" alt=\"cetak\">
					</a></td>";
			}
			?>
			<?php
		}
	      ?>
	    </tbody>
	  </table>
  	</div>
      </div>
	  </div>
    </td>
  </tr>
</table>
<br>
<?php echo "<center>".$this->pagination->create_links()."</center>";?>
<br>
<?=form_close()?>
</div>
<script language="javascript" type="text/javascript">
  function hapusatuh(a){
    show("mkttool/cform/delete/"+a+"/","#main");
  }

  function editatuh(a){
    show("mkttool/cform/edit/"+a+"/","#main");
  }

  function cetakatuh(a){
    show("mkttool/cform/cetak/"+a+"/","#main");
  }

  function downloadthis(elm){
		event.preventDefault();
		let url = $(elm).data('href');
		$('#download-btn').attr('href', url);
		$('#download-btn')[0].click();
	}

  function yyy(b){
	lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/mkttool/cform/cetak/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }

  $(document).ready(function() {
  		$('#tahun').change(function() {
  			$('#bcari').click();
  		});
  	});

//   fixedTable('table', 2, 3);

  function fixedTable(table, left, right){
	var left     = left - 1;
	var tdlength = $($(table).find('tbody tr')[0]).find('td').length;
	var thWidth  = 0;
	var thWidthR = 0;

	if(window.screen.availWidth > 610){

		$($(table).find('th')).each(function(x, elm){
			if(elm.cellIndex <= left){
				elm.style.position = 'sticky';
				elm.style.zIndex = '1';
				elm.style.outline = '2px solid #f2f2f2';
				if(elm.previousElementSibling != null){
					thWidth = thWidth + elm.previousElementSibling.offsetWidth + 2;
					elm.style.left = thWidth + 'px';
				}else{
					elm.style.left = '0px';
				}
			}

			if(elm.cellIndex >= (tdlength-right)){
				elm.style.position = 'sticky';
				elm.style.outline = '2px solid #f2f2f2';
				if(elm.nextElementSibling != null){
					thWidthR = thWidthR + elm.nextElementSibling.offsetWidth + 2;
					elm.style.right = thWidthR + 'px';
				}else{
					elm.style.right = '0px';
				}
			}

		});

		$($(table).find('tbody tr')).each(function(xx, elm){

			var tdWidth  = 0;
			var tdWidthR = 0;
			$(elm).find('td').each(function(x, td){
				if(td.cellIndex <= left){
					td.style.position = 'sticky';
					td.style.outline = '2px solid #f2f2f2';
					if(td.previousElementSibling != null){
						tdWidth = tdWidth + td.previousElementSibling.offsetWidth + 2;
						td.style.left = tdWidth + 'px';
					}else{
						td.style.left = '0px';
					}
				}

				if(td.cellIndex >= (tdlength-right)){
					td.style.position = 'sticky';
					td.style.outline = '2px solid #f2f2f2';
					if(td.nextElementSibling != null){
						tdWidthR = tdWidthR + td.nextElementSibling.offsetWidth + 2;
						td.style.right = tdWidthR + 'px';
					}else{
						td.style.right = '0px';
					}
				}
			})
		})
	}
}

function approveThis(a) {
		showModal("mkttool/cform/approveone/" + a + "/" , "#light");
		jsDlgShow("#konten *", "#fade", "#light");
}

function approveThismkt(a) {
	showModal("mkttool/cform/approvetwo/" + a + "/" , "#light");
	jsDlgShow("#konten *", "#fade", "#light");
}

function approveThisAdv(a) {
	showModal("mkttool/cform/approvetri/" + a + "/" , "#light");
	jsDlgShow("#konten *", "#fade", "#light");
}

function exportexcel() {
		var tahun = document.getElementById('tahun').value;
		var abc = "<?php echo site_url('mkttool/cform/export/'); ?>/" + tahun;
		$("#href").attr("href", abc);
		return true;
	}


/*   function approveThis(imkt){
		event.preventDefault();
				$.ajax({
					type     : 'post',
					url      : '<#?=site_url(module('cform/approve/'));?>',
					data     : {imkt : imkt},
					dataType : 'json',
					success  : function(res){
						swal('', res.msg, res.stat).then(()=>{
							$('#btnrefresh').click();
						});
					}
				})
	} */


</script>
