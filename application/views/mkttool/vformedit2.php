<style>
.button {
  background-color: #bad6f0;
  border: 1;
  color: black;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  /* display: inline-block; */
  font-size: 16px;
  margin: 6px 4px;
  cursor: pointer;
  font-weight: bold;
}

.buttondownload {
  background-color: #bad6f0;
  border: 1;
  color: black;
  padding: 6px 16px;
  text-align: center;
  text-decoration: none;
  /* display: inline-block; */
  font-size: 10px;
  margin: 3px 2px;
  cursor: pointer;
  font-weight: bold;
}
</style>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
<tr>
  <td align="left">
  <?#php echo $this->pquery->form_remote_tag(array('url'=>'mkttool/cform/update','update'=>'#pesan','type'=>'post'));?>
  <?php echo $this->pquery->form_enctype_tag(array('url'=>site_url('mkttool/cform/update'),'id'=>'the-form','update'=>'#pesan','type'=>'post')); ?>
  <a href="" id="download-btn" class="noDisplay" download=""></a>
  <div id="masterproductpriceform">
<div class="effect">
  <div class="accordion2">
  <div align="right">
      <input class="button" name="listdata" id="listdata" value="List Data" type="button" onclick='show("mkttool/cform/view/","#main")'>
    </div>
  
    <table class="mastertable"> 
  <tr>
	<td width="12%"></td>
  <td width="1%"></td>
	<td width="87%"><b>Promo<b></td>
  <input type="hidden" name="i_mkt" id="i_mkt" value="<?php echo $imkt; ?>">
  <input type="hidden" name="dmkt" id="dmkt" value="<?php echo $data->d_mkt; ?>">
  <input type="hidden" name="isalesman" id="isalesman" value="<?php echo $isalesman; ?>">
  <input type="hidden" name="icustomer" id="icustomer" value="<?php echo $icustomer; ?>">
  <input type="hidden" name="imkttype" id="imkttype" value="<?php echo $jenispromo; ?>">
  <input type="hidden" name="d_entry" id="d_entry" value="<?php echo $data->d_entry; ?>">
  <input type="hidden" name="iapprove" id="iapprove" value="<?php echo $iapprove; ?>">
  <input type="hidden" name="dapprove" id="dapprove" value="<?php echo $dapprove; ?>">
  <input type="hidden" name="ilevel" id="ilevel" value="<?php echo $ilevel; ?>">
  <input type="hidden" name="fmktapp" id="fmktapp" value="<?php echo $data->f_mkt_approve; ?>">
  <input type="hidden" name="itype" id="itype" value="<?php echo $data->i_promo_type2; ?>">
  <input type="hidden" name="buktay" id="buktay" value="<?php echo $data->e_tanda_bukti; ?>">
  <input type="hidden" name="tahun" id="tahun" value="<?php echo $data->tahun; ?>">
  </tr>      
<tr>
  <td width="12%">Jenis Promo</td>
	<td width="1%"></td>
    <td width="87%">
    <input type="hidden" name="jenispromo2" value="<?=$data->i_mkt_type;?>">
      <input type="radio" id="jenispromo1" name="jenispromow" value="1" style="display : inline; width: 20px !important;"<?=(isset($data->i_promo_type2) && $data->i_promo_type2 == '1') ? 'checked="checked"' : '';?> disabled>
      <label class="mx-2" for="jenispromo1">SELL IN</label><br>
<br>
      <input type="radio" id="jenispromo2" name="jenispromow" value="2" style="display : inline; width: 20px !important;"<?=(isset($data->i_promo_type2) && $data->i_promo_type2 == '2') ? 'checked="checked"' : '';?>disabled>
      <label class="mx-2" for="jenispromo2">SELL OUT</label><br>
<br>      
      <input type="radio" id="jenispromo3" name="jenispromow" value="3" style="display : inline; width: 20px !important;"<?=(isset($data->i_promo_type2) && $data->i_promo_type2 == '3') ? 'checked="checked"' : '';?>disabled>
      <label class="mx-2" for="jenispromo3">LISTING FEE</label><br>
    </td>
</tr>  

  <td width="12%">Besarann Diskon / Pengajuan Yang Diminta</td>
	<td width="1%"></td>
	<td width="87%">
  <input type="text" name="diskon" id="diskon" value="<?=(isset($data->e_diskon_request)) ? $data->e_diskon_request : '';?>" onkeyup="reformat(this);hitungeta();" maxlength="2"></td>
      </td>
       </tr>
       <tr>
	<td width="12%">Target</td>
	<td width="1%">:</td>
	<td width="87%"><input type="text" name="target" id="target" value="<?=(isset($data->v_target)) ? number_format($data->v_target) : '';?>"  onkeyup="reformat(this);hitungeta();"></td>
      </tr>
      <tr>
	<td width="12%">Estimasi</td>
	<td width="1%">:</td>
	<td width="87%"><input type="text" name="vestimasi" id="vestimasi" value="<?=(isset($data->v_biaya_eta)) ? number_format($data->v_biaya_eta) : '';?>" onkeyup="reformat(this);"></td>
      </tr>
      <tr>
	<td width="12%">Jenis Tanda Bukti Yang Akan Di Berikan Toko</td>
	<td width="1%">:</td>
	<td width="87%">
			<!--  <input readonly name="tandabukti" id="tandabukti" value="<?#=(isset($data->e_tanda_bukti)) ? $data->e_tanda_bukti : '';?>" 
			 onclick='showModal("mkttool/cform/mktpromo/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
      </tr> -->
      <input type="text" name="tandabukti" id="tandabukti" value="<?=(isset($data->e_tanda_bukti)) ? $data->e_tanda_bukti : '';?>" readonly>
      <tr>
	<td width="12%">Keterangan Pengajuan</td>
	<td width="1%">:</td>
	<td width="87%"><input type="text" name="remark2" id="remark2" value="<?=(isset($data->e_tool_remark)) ? $data->e_tool_remark : '';?>"></td>
      </tr>
      <?php 
      
      if($data->i_mkt_type == 1){
        $jenis = $data->i_loyalitas_type;
      }else if($data->i_mkt_type == 2){
        $jenis = $data->i_promo_type2;
      }else if($data->i_mkt_type == 3){
        $jenis = $data->e_jenis_market;
      }

    $this->db->select("	* from tm_marketing_tool_loa 
              where i_mkt_type = '$data->i_mkt_type' and i_level = '$data->i_approve_level' 
              and i_jenis = trim('$jenis')",false);
   $query = $this->db->get();
   /* echo $this->db->last_query(); */
   if ($query->num_rows() > 0){
     foreach($query->result() as $siw){
       $awal=$siw->v_loa_awal;
       $akhir=$siw->v_loa_akhir;
       $level=$siw->i_level;
     }
   }else{
     $awal 	= 0;
     $akhir 	= 0;
   }

      
      
      if(($data->f_mkt_approve == 't' && $data->f_cancel == 'f' && $data->i_approve_level <= '3' && $data->f_reject == 'f')
      || ($data->e_diskon_request <= $awal && $level == $data->i_approve_level && $data->f_reject == 'f')
      || ($data->e_diskon_request >= $awal && $data->e_diskon_request <= $akhir && $level == $data->i_approve_level && $data->f_reject == 'f')
      ){?>
  <tr>
			<td width="19%" id="x" hidden >Nomor Promo</td>
			<td width="1%" id="y"hidden>:</td>
			<td width="80%" id="z"hidden>
				<!-- <input type="hidden" id="iarea" name="iarea" value=""> -->
				<input type="text" id="ipromo" name="ipromo" value="" onclick='showModal("mkttool/cform/promo/"+document.getElementById("icustomer").value+"/"+document.getElementById("tahun").value+"/"+document.getElementById("diskon").value+"/","#light");jsDlgShow("", "#fade", "#light");' hidden="true">
			</td>
  </tr>
  <tr>
  <td width="12%">Realisasi</td>
	<td width="1%">:</td>
	<td width="87%"><input type="text" name="realisai" id="realisai" value="<?=(isset($data->v_biaya_realisasi)) ? number_format($data->v_biaya_realisasi) : '';?>" readonly onkeyup="reformat(this);"></td>
      </tr>
      <?php }else{?>
        <input type="hidden" name="realisai" id="realisai" value="">
        <input type="hidden" name="ipromo" id="ipromo" value="">
        <input type="hidden" id="x" value="">
        <input type="hidden" id="y" value="">
        <input type="hidden" id="z" value="">
      <?php } ?>  
  <tr>
	<td width="12%"></td>
  <td width="1%"></td>
	<td width="87%"><b>Upload Dokumen<b></td>
  </tr>    
  <?php
			$directory = 'mkttool/';
			$att1 = ($data->e_attch1 != '') ? '<a href="#" data-href="'.base_url($directory.$data->e_attch1).'" class="buttondownload" download="" onclick="downloadthis(this)">Download</a>' : '';
			$att2 = ($data->e_attch2 != '') ? '<a href="#" data-href="'.base_url($directory.$data->e_attch2).'" class="buttondownload" download="" onclick="downloadthis(this)">Download</a>' : '';
			$att3 = ($data->e_attch3 != '') ? '<a href="#" data-href="'.base_url($directory.$data->e_attch3).'" class="buttondownload" download="" onclick="downloadthis(this)">Download</a>' : '';
			$att4 = ($data->e_attch4 != '') ? '<a href="#" data-href="'.base_url($directory.$data->e_attch4).'" class="buttondownload" download="" onclick="downloadthis(this)">Download</a>' : '';
	?>
      <tr>
	<td width="12%">Surat Pengajuan</td>
	<td width="1%">:</td>
	<td width="87%"><input type="file" name="userfile1" size="20">
  <?php echo $att1; ?>
</td>
      </tr>
      <tr>
	<td width="12%">Proposal Toko</td>
	<td width="1%">:</td>
	<td width="87%"><input type="file" name="userfile2" size="20">
  <?php echo $att2; ?>
</td>


<?php if(($data->f_mkt_approve == 't' && $data->f_cancel == 'f' && $data->i_approve_level <= '3' && $data->f_reject == 'f')
|| ($data->e_diskon_request <= $awal && $level == $data->i_approve_level && $data->f_reject == 'f')
|| ($data->e_diskon_request >= $awal && $data->e_diskon_request <= $akhir && $level == $data->i_approve_level && $data->f_reject == 'f')
){?>
      <tr>
	<td width="12%"></td>
  <td width="1%"></td>
	<td width="87%"><b>Upload Dokumen Realisasi<b></td>
  </tr>      
      <tr>
	<td width="12%">Dokumen</td>
	<td width="1%">:</td>
	<td width="87%"><input type="file" name="userfile4" size="20">
  <?php echo $att4; ?>
</td>
      </tr>   
      <?php }?>  



  
      </tr>
<td width="100%" align="center" colspan="4">
	  <input class="button" name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick='show("mkttool/cform/edit/<?=$data->i_mkt;?>/","#main")'>
    <?php if( $data->i_approve_level <= '3' && $data->f_reject == 'f' ){?>
          <b><input class="button" name="login" id="login" value="Simpan" type="submit" onclick="dipales();"></b></td>
    <?php }elseif ($data->i_approve_level == '5' && $data->f_mkt_approve == 'f') {?>  
            
    <?php }elseif ($data->i_approve_level == '5' && $data->f_mkt_approve == 't' && $data->f_reject == 'f') {?>    
      <b><input class="button" name="login" id="login" value="Simpan" type="submit" onclick="dipales();"></b></td>
    <?php }elseif($data->i_approve_level == '6' && $data->f_reject == 'f'){?>
      <b><input class="button" name="login" id="login" value="Simpan" type="submit" onclick="dipales();"></b></td>
    <?php }else if(($data->e_diskon_request <= $awal && $level == $data->i_approve_level && $data->f_reject == 'f' && $data->f_reject == 'f') 
    || ($data->e_diskon_request >= $awal && $data->e_diskon_request <= $akhir && $level == $data->i_approve_level && $data->f_reject == 'f')){ ?>
      <b><input class="button" name="login" id="login" value="Simpan" type="submit" onclick="dipales();"></b></td>
    <?php }?>
  </table>  
	</td>
      </tr>
    </table>
  </div>
</div>
</div>
<?=form_close()?>
  </td>
</tr>
</table>
<div id="pesan"></div>
<script language="javascript" type="text/javascript">

$( document ).ready(function() {
  itype   = document.getElementById("itype").value;
  buktay  = document.getElementById("buktay").value;

  if(itype == 1){
      document.getElementById("x").hidden = false;
      document.getElementById("y").hidden = false;
      document.getElementById("z").hidden = false;
      document.getElementById("ipromo").hidden = false;
      document.getElementById("realisai").disabled = false;
      document.getElementById("vestimasi").readOnly = true;
      document.getElementById("target").readOnly = true;
      document.getElementById("diskon").readOnly = true;
      document.getElementById("remark2").readOnly = true;
  }else if(itype == 2){
    if(buktay == 'Kode Promo'){
      document.getElementById("x").hidden = false;
      document.getElementById("y").hidden = false;
      document.getElementById("z").hidden = false;
      document.getElementById("ipromo").hidden = false;
    }else if(buktay == 'Kwitansi Promo'){
      document.getElementById("realisai").readOnly = false;
    }
  }else if(itype == 3) {
      document.getElementById("x").hidden = false;
      document.getElementById("y").hidden = false;
      document.getElementById("z").hidden = false;
      document.getElementById("ipromo").hidden = false;
      document.getElementById("realisai").disabled = false;
  }else{
    if(typeof document.getElementById("realisai") != 'undefined'){
      document.getElementById("realisai").readOnly = false;
    }
  }
});


  function dipales(){
  	 if((document.getElementById("jenispromo2").value!='') &&
  	 	(document.getElementById("diskon").value!='') &&
  	 	(document.getElementById("tandabukti").value!='') && 
       (document.getElementById("ipromo").value!=''))
		   {
  	  	  document.getElementById("login").hidden=true;
      }else if((document.getElementById("jenispromo2").value!='') &&
  	 	(document.getElementById("diskon").value!='') &&
  	 	(document.getElementById("tandabukti").value!='')){
          document.getElementById("login").hidden=true;
          document.getElementById("cmdreset").hidden=true;
      }else{
          document.getElementById("login").hidden=false;
   		    alert('LENGKAPI DATA ISIAN !!!');
    }
  }

  function hitungeta(){
    diskon = document.getElementById("diskon").value;
    target = formatulang(document.getElementById("target").value);
    estimasi = '';
    if(diskon != '' && target != ''){
      estimasi = formatcemua((diskon * target)/100);
      document.getElementById("vestimasi").value = estimasi;
    }
  }

  function downloadthis(elm){
		event.preventDefault();
		let url = $(elm).data('href');
		$('#download-btn').attr('href', url);
		$('#download-btn')[0].click();
	}
</script>
