<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'debetnota-ap/cform/simpan','update'=>'#pesan','type'=>'post'));?>
	<div id="debetnotaform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Debet Nota AP</td>
		<td width="1%">:</td>
		<td width="20%"><input readonly name="idnap" id="idnap" value="" >
						<input readonly name="ddnap" id="ddnap" value="" onclick="showCalendar('',this,this,'','ddn',0,20,1)"></td>
	   
	 	<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value=""></td> 
	      </tr>
	      <tr>
		<td width="12%">Supplier</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="isupplier" id="isupplier" value="">
						<input readonly name="esuppliername" id="esuppliername" value="" 
							   onclick="view_supplier();"></td>
	    <td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value=""></td>  
	      </tr>
	      <tr>
		<td width="12%">No Refferensi</td>
		<td width="1%">:</td>
		<td width="37%"><input name="irefference" id="irefference" value="" maxlength="15">
						<input readonly name="drefference" id="drefference" value="" onclick="showCalendar('',this,this,'','drefference',0,20,1)"></td>

	      </tr>
	      <tr>
		<td width="12%">Kotor</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" name="vgross" id="vgross" value="" onkeyup="reformat(this);hitung();"></td>

	      </tr>
	      <tr>
		<td width="12%">Potongan</td>
		<td width="1%">:</td>
	  <input type="hidden" name="ncustomerdiscount1" id="ncustomerdiscount1" value="0">
		<input type="hidden" name="ncustomerdiscount2" id="ncustomerdiscount2" value="0">
		<input type="hidden" name="ncustomerdiscount3" id="ncustomerdiscount3" value="0">
		<input type="hidden" name="vcustomerdiscount1" id="vcustomerdiscount1" value="0">
		<input type="hidden" name="vcustomerdiscount2" id="vcustomerdiscount2" value="0">
		<input type="hidden" name="vcustomerdiscount2" id="vcustomerdiscount3" value="0">
		<td width="37%"><input style="text-align:right;" name="vdiscount" id="vdiscount" value="" onkeyup="reformat(this);hitung();"></td>
	      </tr>
	      <tr>
		<td width="12%">Bersih</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vnetto" id="vnetto" value=""></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('debetnota-ap/cform/','#main')">
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("ddnap").value=='') ||
			(document.getElementById("isupplier").value=='') ||
			(document.getElementById("irefference").value=='') ||
			(document.getElementById("finsentif").checked==false)
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
		}
	}
	function view_supplier(){
		showModal("debetnota-ap/cform/supplier/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_customer(){
		area=document.getElementById("iarea").value;
		if(area!=''){
			showModal("debetnota-ap/cform/customer/"+area+"/","#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	} 
	function insentif(a){
		if(a==''){
			document.getElementById("finsentif").value='on';
		}else{
			document.getElementById("finsentif").value='';
		}
	}
	function masalah(a){
		if(a==''){
			document.getElementById("fmasalah").value='on';
		}else{
			document.getElementById("fmasalah").value='';
		}
	}
	/*function tesss(){
		document.getElementById("ikn").value="";
		document.getElementById("dkn").value="";
		document.getElementById("icustomer").value="";
		document.getElementById("icustomergroupar").value="";
		document.getElementById("ecustomername").value="";
		document.getElementById("iarea").value="";
		document.getElementById("eareaname").value="";
		document.getElementById("ecustomeraddress").value="";
		document.getElementById("irefference").value="";
		document.getElementById("drefference").value="";
		document.getElementById("isalesman").value="";
		document.getElementById("esalesmanname").value="";
		document.getElementById("vgross").value="";
		document.getElementById("finsentif").value="";
		document.getElementById("finsentif").checked=false;
		document.getElementById("vdiscount").value="";
		document.getElementById("fmasalah").value="";
		document.getElementById("fmasalah").checked=false
		document.getElementById("vnetto").value="";
		document.getElementById("vsisa").value="";
		document.getElementById("eremark").value="";
		document.getElementById("login").disabled=false;
		document.getElementById("pesan").innerHTML='';
	} */
	function hitung(){
		if(document.getElementById("vgross").value=='') document.getElementById("vgross").value='0';
		if(document.getElementById("vdiscount").value=='') document.getElementById("vdiscount").value='0';
		kotor=formatulang(document.getElementById("vgross").value);
		disco=formatulang(document.getElementById("vdiscount").value);
		if(parseFloat(disco)<parseFloat(kotor)){
			document.getElementById("vnetto").value=formatcemua(kotor-disco);
			document.getElementById("vsisa").value=formatcemua(kotor-disco);
		}else{
			alert("Discount tidak boleh lebih besar dari nilai kotor");
			document.getElementById("vdiscount").value="0";
			document.getElementById("vnetto").value=formatcemua(kotor);
			document.getElementById("vsisa").value=formatcemua(kotor);
		}
	}
</script>
