<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Periode Awal</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iperiodeawal" name="iperiodeawal" value="<?= date('Ym') ?>">
									<select name="bulanawal" id="bulanawal" onchange="buatperiodeawal()">
										<option value='01' <?php echo date('m') == '01' ? 'selected' : '' ?>>Januari</option>
										<option value='02' <?php echo date('m') == '02' ? 'selected' : '' ?>>Februari</option>
										<option value='03' <?php echo date('m') == '03' ? 'selected' : '' ?>>Maret</option>
										<option value='04' <?php echo date('m') == '04' ? 'selected' : '' ?>>April</option>
										<option value='05' <?php echo date('m') == '05' ? 'selected' : '' ?>>Mei</option>
										<option value='06' <?php echo date('m') == '06' ? 'selected' : '' ?>>Juni</option>
										<option value='07' <?php echo date('m') == '07' ? 'selected' : '' ?>>Juli</option>
										<option value='08' <?php echo date('m') == '08' ? 'selected' : '' ?>>Agustus</option>
										<option value='09' <?php echo date('m') == '09' ? 'selected' : '' ?>>September</option>
										<option value='10' <?php echo date('m') == '10' ? 'selected' : '' ?>>Oktober</option>
										<option value='11' <?php echo date('m') == '11' ? 'selected' : '' ?>>November</option>
										<option value='12' <?php echo date('m') == '12' ? 'selected' : '' ?>>Desember</option>
									</select>
									<select name="tahunawal" id="tahunawal" onchange="buatperiodeawal()">
										<?php
										$tahunawal1 = date('Y') - 3;
										$tahunawal2 = date('Y');
										for ($i = $tahunawal1; $i <= $tahunawal2; $i++) {
											$selected = date('Y') == $i ? "selected" : "";

											echo "<option value='$i' $selected>$i</option>";
										}
										?>
									</select>
								</td>
							</tr>

							<tr>
								<td width="19%">Periode Akhir</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iperiodeakhir" name="iperiodeakhir" value="<?= date('Ym') ?>">
									<select name="bulanakhir" id="bulanakhir" onchange="buatperiodeakhir()">
										<option value='01' <?php echo date('m') == '01' ? 'selected' : '' ?>>Januari</option>
										<option value='02' <?php echo date('m') == '02' ? 'selected' : '' ?>>Februari</option>
										<option value='03' <?php echo date('m') == '03' ? 'selected' : '' ?>>Maret</option>
										<option value='04' <?php echo date('m') == '04' ? 'selected' : '' ?>>April</option>
										<option value='05' <?php echo date('m') == '05' ? 'selected' : '' ?>>Mei</option>
										<option value='06' <?php echo date('m') == '06' ? 'selected' : '' ?>>Juni</option>
										<option value='07' <?php echo date('m') == '07' ? 'selected' : '' ?>>Juli</option>
										<option value='08' <?php echo date('m') == '08' ? 'selected' : '' ?>>Agustus</option>
										<option value='09' <?php echo date('m') == '09' ? 'selected' : '' ?>>September</option>
										<option value='10' <?php echo date('m') == '10' ? 'selected' : '' ?>>Oktober</option>
										<option value='11' <?php echo date('m') == '11' ? 'selected' : '' ?>>November</option>
										<option value='12' <?php echo date('m') == '12' ? 'selected' : '' ?>>Desember</option>
									</select>
									<select name="tahunakhir" id="tahunakhir" onchange="buatperiodeakhir()">
										<option></option>
										<?php
										$tahunakhir1 = date('Y') - 3;
										$tahunakhir2 = date('Y');
										for ($i = $tahunakhir1; $i <= $tahunakhir2; $i++) {
											$selected = date('Y') == $i ? "selected" : "";

											echo "<option value='$i' $selected>$i</option>";
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Download" target="blank" onclick="return exportexcel();"><button>Download</button></a>
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('<?= $folder ?>/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script languge=javascript type=text/javascript>
	function buatperiodeawal() {
		periodeawal = document.getElementById("tahunawal").value + document.getElementById("bulanawal").value;
		document.getElementById("iperiodeawal").value = periodeawal;
	}

	function buatperiodeakhir() {
		periodeakhir = document.getElementById("tahunakhir").value + document.getElementById("bulanakhir").value;
		document.getElementById("iperiodeakhir").value = periodeakhir;
	}

	function exportexcel() {
		var iperiodeawal = document.getElementById('iperiodeawal').value;
		var iperiodeakhir = document.getElementById('iperiodeakhir').value;

		var abc = "<?= site_url($folder . '/cform/export/'); ?>" + iperiodeawal + "/" + iperiodeakhir;
		$("#href").attr("href", abc);
		return true;
	}
</script>