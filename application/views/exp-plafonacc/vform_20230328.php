<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url'=>'exp-plafonacc/cform/export','update'=>'#main','type'=>'post'));?>
	<div id="spbperareaform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		      <td width="19%">Periode Awal</td>
		      <td width="1%">:</td>
		      <td width="80%"><input type="hidden" id="iperiodeawal" name="iperiodeawal" value="">
						<select name="bulanawal" id="bulanawal" onblur="buatperiodeawal()">
							<option></option>
							<option value='01'>Januari</option>
							<option value='02'>Pebruari</option>
							<option value='03'>Maret</option>
							<option value='04'>April</option>
							<option value='05'>Mei</option>
							<option value='06'>Juni</option>
							<option value='07'>Juli</option>
							<option value='08'>Agustus</option>
							<option value='09'>September</option>
							<option value='10'>Oktober</option>
							<option value='11'>November</option>
							<option value='12'>Desember</option>
						</select>
            <select name="tahunawal" id="tahunawal" onblur="buatperiodeawal()">
               <option></option>
               <?php 
                  $tahunawal1 = date('Y')-3;
                  $tahunawal2 = date('Y');
                  for($i=$tahunawal1;$i<=$tahunawal2;$i++)
                  {
                     echo "<option value='$i'>$i</option>";
                  }
               ?>
            </select>
    			</td>
	      </tr>
	      <tr>
		<td width="19%">Periode Akhir</td>
		<td width="1%">:</td>
		<td width="80%"><input type="hidden" id="iperiodeakhir" name="iperiodeakhir" value="">
						<select name="bulanakhir" id="bulanakhir" onblur="buatperiodeakhir()">
							<option></option>
							<option value='01'>Januari</option>
							<option value='02'>Pebruari</option>
							<option value='03'>Maret</option>
							<option value='04'>April</option>
							<option value='05'>Mei</option>
							<option value='06'>Juni</option>
							<option value='07'>Juli</option>
							<option value='08'>Agustus</option>
							<option value='09'>September</option>
							<option value='10'>Oktober</option>
							<option value='11'>November</option>
							<option value='12'>Desember</option>
						</select>
            <select name="tahunakhir" id="tahunakhir" onblur="buatperiodeakhir()">
               <option></option>
               <?php 
                  $tahunakhir1 = date('Y')-3;
                  $tahunakhir2 = date('Y');
                  for($i=$tahunakhir1;$i<=$tahunakhir2;$i++)
                  {
                     echo "<option value='$i'>$i</option>";
                  }
               ?>
            </select>
			</td>
        </tr>
	      
	      
	      <tr>
		<td width="19%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="80%">
		  <input name="login" id="login" value="Export" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('exp-plafonacc/cform/','#main')">
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script languge=javascript type=text/javascript>
  function buatperiodeawal(){
	  periodeawal=document.getElementById("tahunawal").value+document.getElementById("bulanawal").value;
	  document.getElementById("iperiodeawal").value=periodeawal;
  }
  function buatperiodeakhir(){
	  periodeakhir=document.getElementById("tahunakhir").value+document.getElementById("bulanakhir").value;
	  document.getElementById("iperiodeakhir").value=periodeakhir;
  }
</script>
