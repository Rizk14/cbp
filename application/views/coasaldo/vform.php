<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'coasaldo/cform/simpan','update'=>'#pesan','type'=>'post'));?>
	<div id="coasaldoform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><select name="iperiodebl" id="iperiodebl">
							<option>01</option>
							<option>02</option>
							<option>03</option>
							<option>04</option>
							<option>05</option>
							<option>06</option>
							<option>07</option>
							<option>08</option>
							<option>09</option>
							<option>10</option>
							<option>11</option>
							<option>12</option>
						</select>&nbsp;<input name="iperiodeth" id="iperiodeth" value="" onkeyup="cektahun(this)" maxlength="4"></td>
	      </tr>
	      <tr>
		<td width="12%">CoA</td>
		<td width="1%">:</td>
		<td width="87%"><input name="icoa" id="icoa" value="" 
							   onclick='showModal("coasaldo/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'><input readonly name="ecoaname" id="ecoaname" value="" 
							   onclick='showModal("coasaldo/cform/coa/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Saldo Awal</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vsaldoawal" id="vsaldoawal" value="0"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Mutasi Debet</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vmutasidebet" id="vmutasidebet" value="0"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Mutasi Kredit</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vmutasikredit" id="vmutasikredit" value="0"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Saldo Akhir</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vsaldoakhir" id="vsaldoakhir" value="0"
							   onkeyup="reformat(this);sinkronak();"></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="tesss()">
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("icoa").value=='')||
			(document.getElementById("vsaldoawal").value=='')||
			(document.getElementById("vsaldoakhir").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function tesss(){
		document.getElementById("icoa").value="";
		document.getElementById("iperiodebl").value="";
		document.getElementById("iperiodeth").value="";
		document.getElementById("ecoaname").value="";
		document.getElementById("vsaldoawal").value="0";
		document.getElementById("vmutasidebet").value="0";
		document.getElementById("vmutasikredit").value="0";
		document.getElementById("vsaldoakhir").value="0";
		document.getElementById("login").hidden=false;
		document.getElementById("pesan").innerHTML='';
	}
	function sinkronaw(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		sak = saw+vmd-vmk;
		document.getElementById("vsaldoakhir").value = formatcemua(sak);
	}
	function sinkronak(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		saw = sak+vmk-vmd;
		document.getElementById("vsaldoawal").value = formatcemua(saw);
	}
</script>
