<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'coasaldo/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="coasaldoform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Periode</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="iperiodeblx" id="iperiodeblx" value="<?php echo substr($isi->i_periode,4,2); ?>"><input name="iperiodeth" id="iperiodeth" value="<?php echo substr($isi->i_periode,0,4); ?>" onkeyup="cektahun(this)" maxlength="4"></td>
	      </tr>
	      <tr>
		<td width="12%">CoA</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="icoa" id="icoa" value="<?php echo $isi->i_coa; ?>"><input readonly name="ecoaname" id="ecoaname" value="<?php echo $isi->e_coa_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Saldo Awal</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vsaldoawal" id="vsaldoawal" value="<?php echo number_format($isi->v_saldo_awal); ?>"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Mutasi Debet</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vmutasidebet" id="vmutasidebet" value="<?php echo number_format($isi->v_mutasi_debet); ?>"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Mutasi Kredit</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vmutasikredit" id="vmutasikredit" value="<?php echo number_format($isi->v_mutasi_kredit); ?>"
							   onkeyup="reformat(this);sinkronaw();"></td>
	      </tr>
	      <tr>
		<td width="12%">Saldo Akhir</td>
		<td width="1%">:</td>
		<td width="87%"><input style="text-align:right;" name="vsaldoakhir" id="vsaldoakhir" value="<?php echo number_format($isi->v_saldo_akhir); ?>"
							   onkeyup="reformat(this);sinkronak();"></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%">
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listcoasaldo/cform/','#main')">
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("iperiodeth").value=='') ||
			(document.getElementById("iperiodebl").value=='') ||
			(document.getElementById("icoa").value=='')||
			(document.getElementById("vsaldoawal").value=='')||
			(document.getElementById("vsaldoakhir").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").hidden=true;
		}
	}
	function sinkronaw(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		sak = saw+vmd-vmk;
		document.getElementById("vsaldoakhir").value = formatcemua(sak);
	}
	function sinkronak(){
		saw = parseFloat(formatulang(document.getElementById("vsaldoawal").value));
		vmd = parseFloat(formatulang(document.getElementById("vmutasidebet").value));
		vmk = parseFloat(formatulang(document.getElementById("vmutasikredit").value));
		sak = parseFloat(formatulang(document.getElementById("vsaldoakhir").value));
		saw = sak+vmk-vmd;
		document.getElementById("vsaldoawal").value = formatcemua(saw);
	}
</script>
