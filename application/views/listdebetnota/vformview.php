<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listdebetnota/cform/cari','update'=>'#tmpx','type'=>'post'));?>
	<div id="debetnotaform">
	<div class="effect">
	  <div class="accordion2">
	  	<input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('listdebetnota/cform/','#main')">&nbsp;
		<input name="cmdreset" id="cmdreset" value="Export" type="button">

		<table class="listtable" id = "sitabel">
			<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" >
			<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" >
			<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;
			<!-- <thead>
				<tr>
					<td colspan="12" align="center">Cari data :
						<input type="text" id="cari" name="cari" value="" >
						<input type="hidden" id="dfrom" name="dfrom" value="<#?php echo $dfrom; ?>" >
						<input type="hidden" id="dto" name="dto" value="<#?php echo $dto; ?>" >
						<input type="hidden" id="iarea" name="iarea" value="<#?php echo $iarea; ?>" >&nbsp;
						<input type="submit" id="bcari" name="bcari" value="Cari">
					</td>
				</tr>
			</thead> -->

			<th>No DN</th>
			<th>Tgl DN</th>
			<th>No Reff</th>
			<th>Tgl Reff</th>
			<th>Area</th>
			<th>Customer</th>
			<th>Salesman</th>
			<th>Nilai Bersih</th>
			<th>Sisa</th>
<!--			<th class="action">Action</th>-->
	    <tbody>
	<?php 
		if($isi){
			$totnetto = 0;
			$totsisa  = 0;
			foreach($isi as $row){
				if($row->n_kn_year=='')$row->n_kn_year=null;#'20'.substr($row->i_kn,6,2);
				if($row->d_kn!=''){
						$tmp=explode('-',$row->d_kn);
						$tgl=$tmp[2];
						$bln=$tmp[1];
						$thn=$tmp[0];
						$row->d_kn=$tgl.'-'.$bln.'-'.$thn;
				}
				if($row->d_refference!=''){
						$tmp=explode('-',$row->d_refference);
						$tgl=$tmp[2];
						$bln=$tmp[1];
						$thn=$tmp[0];
						$row->d_refference=$tgl.'-'.$bln.'-'.$thn;
				}
				$row->i_kn=trim($row->i_kn);
				$row->i_refference=trim($row->i_refference);
				echo "<tr>";
				if($row->f_kn_cancel=='t'){
					echo "<td><h1>$row->i_kn</h1></td>";
				}else{
					echo "<td>$row->i_kn</td>";
				}
				
				echo " 
				  <td>$row->d_kn</td>
				  <td>$row->i_refference</td>
				  <td>$row->d_refference</td>
				  <td>'$row->i_area</td>
				  <td>($row->i_customer) - $row->e_customer_name</td>
				  <td>$row->i_salesman</td>
				  <td align='right'>".number_format($row->v_netto)."</td>
				  <td align='right'>".number_format($row->v_sisa)."</td>";
//				  <td class=\"action\">";
//			  echo "<a href=\"#\" onclick='show(\"debetnota/cform/edit/$row->i_kn/$row->n_kn_year/$row->i_area/$dfrom/$dto/$row->i_refference/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;";
//				if($row->f_kn_cancel!='t'){
//			  echo "<a href=\"#\" onclick='hapus(\"listdebetnota/cform/delete/$row->i_kn/$row->n_kn_year/$row->i_area/$dfrom/$dto/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
//				}
//			  echo "</td></tr>";
			  echo "</tr>";

				$totnetto += $row->v_netto;
				$totsisa += $row->v_sisa;
			}
			echo "<td colspan = 7><b><center>TOTAL</center></b></td>";
			echo "	<td align='right'>".number_format($totnetto)."</td>
				 	<td align='right'>".number_format($totsisa)."</td>";
		}else{
			echo "<td colspan = 9><h1><center>BELUM ADA DATA</center></h1></td>";
		}
	?>
	    </tbody>
	  </table>
	  <!-- <?php echo "<center>".$this->pagination->create_links()."</center>";?> -->
  	</div>
    </div>
    </div>
      <?=form_close()?>
	  <input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('listdebetnota/cform/','#main')">&nbsp;
	  <input name="cmdreset" id="cmdreset" value="Export" type="button">
    </td>
  </tr>
</table>
</div>

<script type="text/javascript" language="javascript">
$("#cmdreset").click(function() {
	//getting values of current time for generating the file name
	var dt = new Date();
	var day = dt.getDate();
	var month = dt.getMonth() + 1;
	var year = dt.getFullYear();
	var hour = dt.getHours();
	var mins = dt.getMinutes();
	var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
	//creating a temporary HTML link element (they support setting file names)
	var a = document.createElement('a');
	//getting data from our div that contains the HTML table
	var data_type = 'data:application/vnd.ms-excel';
	var table_div = $('#sitabel').html();
	// var table_html = table_div.outerHTML.replace(/ /g, '%20');
	a.href = data_type + ', <table>' + encodeURIComponent(table_div) + '</table>';

	//setting the file name
	// var nofaktur = document.getElementById('no_faktur').value;
	var dfrom 	= document.getElementById('dfrom').value;
	var dto 	= document.getElementById('dto').value;

	a.download = 'export_dn_' + dfrom + "-" + dto + '.xls';
	//triggering the function
	a.click();
	//just in case, prevent default behaviour
	
    // var Contents = $('#sitabel').html();
    // window.open('data:application/vnd.ms-excel, ' + '<table>' + encodeURIComponent($('#sitabel').html()) + '</table>');
});
</script>