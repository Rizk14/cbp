    <table class="maintable">
      <tr>
    	<td align="left">
	      <?php echo $this->pquery->form_remote_tag(array('url'=>'listbrgall/cform/update','update'=>'#main','type'=>'post'));?>
	      <div id="masterproductbaseform">
	      <table class="mastertable">
	      	<tr>
		  <td width="19%">Kode Barang</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" readonly name="iproduct" id="iproduct" value="<?php echo $kode; ?>" maxlength='7'></td>
		  <td width="19%">Nama</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="eproductname" id="eproductname" value="<?php echo $isi->e_product_name; ?>" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Kode Produk (Pemasok)</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="iproductsupplier" id="iproductsupplier" value="<?php echo $isi->i_product_supplier; ?>" maxlength='7'></td>
		  <td width="19%">Nama Produk (Pemasok)</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="eproductsuppliername" id="eproductsuppliername" value="<?php echo $isi->e_product_suppliername; ?>" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Supplier</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="isupplier" id="isupplier" value="<?php echo $isi->i_supplier; ?>" 
					   onclick='showModal("listbrgall/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="esuppliername" id="esuppliername" value="<?php echo $isi->e_supplier_name; ?>" 
					   onclick='showModal("listbrgall/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Status Produk</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductstatus" id="iproductstatus" value="<?php echo $isi->i_product_status; ?>" 
					   onclick='showModal("listbrgall/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductstatusname" id="eproductstatusname" value="<?php echo $isi->e_product_statusname; ?>" 
					   onclick='showModal("listbrgall/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Group Produk</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductgroup" id="iproductgroup" value="<?php echo $isi->i_product_group; ?>" 
					   onclick='showModal("listbrgall/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductgroupname" id="eproductgroupname" value="<?php echo $isi->e_product_groupname; ?>" 
					   onclick='showModal("listbrgall/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Jenis</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproducttype" id="iproducttype" value="<?php echo $isi->i_product_type; ?>" 
					   onclick="view_producttype(document.getElementById('iproductgroup').value);">
				  <input readonly name="eproducttypename" id="eproducttypename" value="<?php echo $isi->e_product_typename; ?>" 
					   onclick="view_producttype(document.getElementById('iproductgroup').value);"></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Kelas</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductclass" id="iproductclass" value="<?php echo $isi->i_product_class; ?>" 
					   onclick='showModal("listbrgall/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductclassname" id="eproductclassname" value="<?php echo $isi->e_product_classname; ?>" 
					   onclick='showModal("listbrgall/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Kategori</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductcategory" id="iproductcategory" value="<?php echo $isi->i_product_category; ?>" 
					   onclick="view_productcategory(document.getElementById('iproductclass').value);">
				  <input readonly name="eproductcategoryname" id="eproductcategoryname" value="<?php echo $isi->e_product_categoryname; ?>" 
					   onclick="view_productcategory(document.getElementById('iproductclass').value);"></td>
	      	</tr>
		<tr>
		  <td width="19%">Harga Eceran</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="vproductretail" id="vproductretail" value="<?php echo number_format($isi->v_product_retail,2); ?>" maxlength='10' onkeyup="reformat(this)"></td>
		  <td width="19%">Harga Pabrik</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="vproductmill" id="vproductmill" value="<?php echo number_format($isi->v_product_mill,2); ?>" maxlength='10' onkeyup="reformat(this)"></td>
	      	</tr>
		<tr>
		  <td width="19%">Tanggal Stop Produksi</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="dproductstopproduction" id="dproductstopproduction" value="<?php echo $isi->d_product_stopproduction; ?>" readonly onclick="showCalendar('',this,this,'','dproductstopproduction',0,18,1)"></td>
		  <td width="19%">Tanggal Pendaftaran</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="dproductregister" id="dproductregister" value="<?php echo $isi->d_product_register; ?>" readonly onclick="showCalendar('',this,this,'','dproductregister',0,18,1)"></td>
	      	</tr>
		<tr>
		  <td width="19%">Price List</td>
		  <td width="1%">:</td>
		  <td colspan="4" width="80%">
		    <input type="checkbox" name="fproductpricelist" id="fproductpricelist" value="on" <?php if($isi->f_product_pricelist=='t') echo "checked";?>></td>
	      	</tr>
	      	<tr>
		  <td width="19%">&nbsp;</td>
		  <td width="1%">&nbsp;</td>
		  <td colspan=4 width="80%">
		    <?php 
		      if($this->session->userdata('departement')=='6'){
		    ?>
		    <input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
		    <?php 
		      }
		    ?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listbrgall/cform/','#main');">
		  </td>
	       </tr>
	      </table>
	      </div>
	<?=form_close()?>
        </td>
      </tr> 
    </table>
<script language="javascript" type="text/javascript">
  function view_productgroup(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/productgroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_productstatus(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/productstatus","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_producttype(a){
    lebar =450;
    tinggi=400;
//    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/producttype/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
    showModal("listbrgall/cform/producttype/"+a+"/","#light");
  	jsDlgShow("#konten *", "#fade", "#light");

  }
  function view_productcategory(a){
//    lebar =450;
//    tinggi=400;
//    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/productcategory/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
    showModal("listbrgall/cform/productcategory/"+a+"/","#light");
  	jsDlgShow("#konten *", "#fade", "#light");
  }
  function view_productclass(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgall/cform/productclass","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function cek(){
    kode=document.getElementById("iproduct").value;
    nama=document.getElementById("eproductname").value;
    if(kode=='' || nama==''){
	alert("Minimal kode Produk dan nama Produk diisi terlebih dahulu !!!");
    }
  }
</script>
