<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'sjreceive/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="sjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">No SJ</td>
		<?php if($isi->d_sj){
			if($isi->d_sj!=''){
				$tmp=explode("-",$isi->d_sj);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				$isi->d_sj=$hr."-".$bl."-".$th;
			}
		   }
		   if($isi->d_spb){
			  if($isi->d_spb!=''){
				  $tmp=explode("-",$isi->d_spb);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_spb=$hr."-".$bl."-".$th;
			  }
		   }
		   if($isi->d_dkb){
			  if($isi->d_dkb!=''){
				  $tmp=explode("-",$isi->d_dkb);
				  $hr=$tmp[2];
				  $bl=$tmp[1];
				  $th=$tmp[0];
				  $isi->d_dkb=$hr."-".$bl."-".$th;
			  }
		   }
		?>
		<td><input id="isj" name="isj" type="text" value="<?php if($isi->i_sj) echo $isi->i_sj; ?>">
        <input readonly id="dsj" name="dsj" value="<?php if($isi->d_sj) echo $isi->d_sj; ?>">
        </td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>">
		    <input id="istore" name="istore" type="hidden" value="<?php if($istore) echo $istore; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SPB</td>
		<td><input readonly id="ispb" name="ispb" value="<?php if($isi->i_spb) echo $isi->i_spb; ?>">
		    <input readonly id="dspb" name="dspb" type="text" value="<?php if($isi->d_spb) echo $isi->d_spb; ?>">
			  <input id="vsjgross" name="vsjgross" type="hidden" value="<?php if($vsjgross) echo $vsjgross; ?>">
		    <input id="nsjdiscount1" name="nsjdiscount1" type="hidden" value="<?php if($nsjdiscount1) echo $nsjdiscount1; ?>">
		    <input id="nsjdiscount2" name="nsjdiscount2" type="hidden" value="<?php if($nsjdiscount2) echo $nsjdiscount2; ?>">
		    <input id="nsjdiscount3" name="nsjdiscount3" type="hidden" value="<?php if($nsjdiscount3) echo $nsjdiscount3; ?>">
		    <input id="vsjdiscount1" name="vsjdiscount1" type="hidden" value="<?php if($vsjdiscount1) {echo $vsjdiscount1;}else{echo '0';} ?>">
		    <input id="vsjdiscount2" name="vsjdiscount2" type="hidden" value="<?php if($vsjdiscount2) {echo $vsjdiscount2;}else{echo '0';} ?>">
		    <input id="vsjdiscount3" name="vsjdiscount3" type="hidden" value="<?php if($vsjdiscount3) {echo $vsjdiscount3;}else{echo '0';} ?>">
		    <input id="vsjdiscounttotal" name="vsjdiscounttotal" type="hidden" value="<?php if($vsjdiscounttotal) {echo $vsjdiscounttotal;}else{echo '0';} ?>">
        <input id="fspbconsigment" name="fspbconsigment" type="hidden" value="<?php if($fspbconsigment) echo $fspbconsigment; ?>">
        <input id="fplusppn" name="fplusppn" type="hidden" value="<?php if($fplusppn) echo $fplusppn; ?>">
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php if($icustomer) echo $icustomer; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php if($isalesman) echo $isalesman; ?>">
        <input id="ntop" name="ntop" type="hidden" value="<?php if($ntop) echo $ntop; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SJ Lama</td>
		<td><input id="isjold" name="isjold" type="text" value="<?php echo $isi->i_sj_old; ?>">
        <input style="text-align:right" id="vsjnetto" name="vsjnetto" type="text" value="<?php if($isi->v_nota_netto) echo number_format($isi->v_nota_netto); ?>"></td>
    </tr>
		<tr>
		  <td width="10%">Nama Toko</td>
		  <td><input readonly id="ecustomername" name="ecustomername" value="<?php if($ecustomername) echo $ecustomername; ?>"></td>
    </tr>
		<tr>
		  <td class="batas" colspan="2">&nbsp;</td>
		</tr>
    <tr>
		  <td width="10%">Tgl Terima</td>
		  <td>
		  <input id="ddkb" name="ddkb" type="hidden" value="<?php if($isi->d_dkb) echo $isi->d_dkb; ?>">
		  <input readonly id="dsjreceive" name="dsjreceive" type="text" value="<?php echo $isi->d_sj_receive; ?>" 
		  onclick="showCalendar('',this,this,'','d_sjreceive',0,20,1)">
 		  <input id="libur" name="libur" type="hidden" value="<?php if($libur) echo $libur; ?>" readonly>
		  </td>
    </tr>
    <tr>
		  <td width="10%">Keterangan</td>
		  <td><input id="eremark" name="eremark" type="text" value="<?php echo $isi->e_sj_receive; ?>">
		  </td>
    </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales();">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("sjreceive/cform/index/","#main")'>
		  </td>
 	    </tr>
	    </table>
			<div id="detailheader" align="center">
				<?php 
					if($detail){
					?>
						<table class="listtable" align="center" style="width: 690px;">
							<th style="width:20px;" align="center">No</th>
							<th style="width:63px;" align="center">Kode</th>
							<th style="width:300px;" align="center">Nama Barang</th>
							<th style="width:100px;" align="center">Motif</th>
							<th style="width:73px;" align="center">Jml Ord</th>
							<th style="width:73px;" align="center">Jml Krm</th>
<!--							<th style="width:32px;" align="center" class="action">Action</th>-->
						</table>
					<?php 
					}		
				?>
			</div>
			<div id="detailisi" align="center">
				<?php 
					if($detail){
						$i=0;
						foreach($detail as $row)
						{
						  $query=$this->db->query(" select f_spb_stockdaerah from tm_spb
																				where i_spb='$ispb' and i_area='$iarea'",false);
							if ($query->num_rows() > 0){
								foreach($query->result() as $qq){
									$stockdaerah=$qq->f_spb_stockdaerah;
								}
							}
							if($stockdaerah=='f'){
								$query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															where i_product='$row->i_product'
															and i_product_motif='$row->i_product_motif'
															and i_product_grade='$row->i_product_grade'
															and i_store='AA' and i_store_location='01' and i_store_locationbin='00'",false);
							}else{
#								$query=$this->db->query("SELECT * from f_stock_onhand('$istore','00', '$row->i_product')",false);

								$query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															where i_product='$row->i_product'
															and i_product_motif='$row->i_product_motif'
															and i_product_grade='$row->i_product_grade'
															and i_store='$istore' and i_store_location='00' and i_store_locationbin='00'",false);

							}
              
							if ($query->num_rows() > 0){
								foreach($query->result() as $tt){
                  if($tt->qty>=0){
  									$stock=$tt->qty+$row->n_deliver;
                  }else{
                    $stock=$row->n_deliver;
                  }
								}
							}else{
								$stock=0;
							}
							if($stock>$row->n_qty)$stock=$row->n_qty;
							if($stock<0)$stock=0;
              $vtot=$row->harga*$stock;
#              if($stock>$row->n_deliver) $stock=$row->n_deliver;
							$stock=number_format($stock);

														echo '<table class="listtable" align="center" style="width:690px;">';
						  	$i++;
						  	echo "<tbody>
								<tr>
		    						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:103px;\"><input readonly style=\"width:103px;\" type=\"text\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$row->v_unit_price\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_qty\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_deliver\"
								onblur=\"hitungnilai(); pembandingnilai(".$i.");\" onkeyup=\"hitungnilai(); pembandingnilai(".$i.");\"
                				onpaste=\"hitungnilai(); pembandingnilai(".$i.");\" autocomplete=\"off\">
                				<input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_deliver\">
                				<input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$stock\">
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>";
#								<td style=\"width:60px;\" align=\"center\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='on' checked onclick='pilihan(this.value,".$i.")'</td>    
							echo "
							</tr></tbody></table>";
						}
            $cquery=$this->db->query("  select a.*, b.e_product_motifname from tm_spb_item a, tr_product_motif b, tm_spb c
                                        where a.i_spb = '$isi->i_spb' and a.i_area='$isi->i_area' 
                                        and a.i_product not in (select i_product from tm_nota_item 
                                        where i_sj='$isi->i_sj' and i_area='$isi->i_area')
                                        and a.i_spb=c.i_spb and a.i_area=c.i_area
                                        and a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                                        order by a.i_product", false);
            if ($cquery->num_rows() > 0){
              foreach($cquery->result() as $tmp){
							echo '<table class="listtable" align="center" style="width:690px;">';
						  	$i++;
                $jmlitem++;
						    $query=$this->db->query(" select f_spb_stockdaerah from tm_spb
																				  where i_spb='$ispb' and i_area='$iarea'",false);
							  if ($query->num_rows() > 0){
								  foreach($query->result() as $qq){
									  $stockdaerah=$qq->f_spb_stockdaerah;
								  }
							  }
							  if($stockdaerah=='f'){
								  $query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															  where i_product='$tmp->i_product'
															  and i_product_motif='$tmp->i_product_motif'
															  and i_product_grade='$tmp->i_product_grade'
															  and i_store='AA' and i_store_location='01' and i_store_locationbin='00'",false);
							  }else{
								  $query=$this->db->query(" 	select n_quantity_stock as qty from tm_ic
															  where i_product='$tmp->i_product'
															  and i_product_motif='$tmp->i_product_motif'
															  and i_product_grade='$tmp->i_product_grade'
															  and i_store='$istore' and i_store_location='00' and i_store_locationbin='00'",false);
							  }
							  if ($query->num_rows() > 0){
								  foreach($query->result() as $tt){
#									  $stock=$tt->qty+$row->n_deliver;
                    if($tt->qty>=0){
  									  $stock=$tt->qty;#+$row->n_deliver;
                    }else{
                      $stock=0;#$row->n_deliver;
                    }
								  }
							  }else{
								  $stock=0;
							  }
#							  if($stock>$row->n_qty)$stock=$row->n_qty;
							  if($stock<0)$stock=0;
                $vtot=$row->harga*$stock;
#                if($stock>$row->n_deliver) $stock=$row->n_deliver;
							  $stock=number_format($stock);

                echo "<tbody>
						          <tr>
            						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
							          id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" 
							          id=\"motif$i\" name=\"motif$i\" value=\"$tmp->i_product_motif\"></td>
							          <td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
							          id=\"iproduct$i\" name=\"iproduct$i\" value=\"$tmp->i_product\"></td>
							          <td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
							          id=\"eproductname$i\"
							          name=\"eproductname$i\" value=\"$tmp->e_product_name\"></td>
							          <td style=\"width:103px;\"><input readonly style=\"width:103px;\" type=\"text\" id=\"emotifname$i\"
							          name=\"emotifname$i\" value=\"$tmp->e_product_motifname\">
							          <input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$tmp->v_unit_price\"></td>
							          <td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
							          type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$tmp->n_order\"></td>
							          <td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
							          type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$tmp->n_deliver\"
							          onblur=\"hitungnilai(); pembandingnilai(".$i.");\" onkeyup=\"hitungnilai(); pembandingnilai(".$i.");\"
                        onpaste=\"hitungnilai(); pembandingnilai(".$i.");\" autocomplete=\"off\">
                        <input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"0\">
                        <input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$stock\">
							          <input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>";
#							          <td style=\"width:60px;\" align=\"center\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'</td>
						          echo "
						          </tr>
                      </tbody></table>";
              }
            }
					}		
				?>
			</div>
			<div id="pesan"></div>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function TambahHari(theDate, numDays) {
//    alert(theDate);
//    alert(numDays);
	  numDays = parseInt(numDays);
	  if(theDate.indexOf("/") !== -1) var seperator = '/';
	  else if(theDate.indexOf("-") !== -1) var seperator = '-';
	  else return false;
	  var parts = theDate.split(seperator);
	  var month = parts[1];
	  if(parts[2].length==4) {
		  var day = parts[0];
		  var year = parts[2];
	  } else if(parts[0].length==4) {
		  var day = parts[2];
		  var year = parts[0];
	  }
	  var date = new Date(year, month-1, day);
//	  alert(date);
	  var newdate = new Date(date);
//	  alert(newdate);
	  newdate.setDate(newdate.getDate() + numDays);
	  var dd = newdate.getDate();
	  var mm = newdate.getMonth() + 1;
	  var y = newdate.getFullYear();
//	  sss=dd + '-'+ mm + '-'+ y;
	  return dd + '-'+ mm + '-'+ y;
  }
  function dipales(){
	 if(document.getElementById("dsjreceive").value==''){
		  alert('Tanggal terima belum diisi..!!');
		  exit();
		}
	}
  function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
  }
	function cektanggal()
  {
		ddkb=document.getElementById('ddkb').value;
    dsjreceive=document.getElementById('dsjreceive').value;
	  dtmp=ddkb.split('-');
	  thndkb=dtmp[2];
		blndkb=dtmp[1];
		hrdkb=dtmp[0];
	  dtmp=dsjreceive.split('-');
	  thndsj=dtmp[2];
		blndsj=dtmp[1];
		hrdsj=dtmp[0];

//alert (dsjreceive);

    if(dsjreceive=='01-06-2018'){
  	  alert('Tidak Ada Receive Pada Tanggal 01 Juni 2018  !!!');
      document.getElementById('dsjreceive').value='';
	  }else if( thndkb>thndsj ){
			alert('Tahun Receive tidak boleh lebih kecil dari DKB !!!');
			document.getElementById('dsjreceive').value='';
	  }else if(thndkb==thndsj){
      if( blndkb>blndsj){
				alert('Bulan Receive tidak boleh lebih kecil dari DKB !!!');
				document.getElementById('dsjreceive').value='';
			}else if( blndkb==blndsj ){
        if( hrdkb>hrdsj ){
					alert('Tanggal Receive tidak boleh lebih kecil dari DKB !!!');
					document.getElementById('dsjreceive').value='';
				}else{
          jmllibur = document.getElementById('libur').value;
          if(jmllibur=='')jmllibur=0;
          jmllibur = parseFloat(jmllibur)+3;
          jmllibur = jmllibur*-1;
          var tgl  = new Date();
          var day   = tgl.getDate();
          var month = tgl.getMonth()+1;
          if ( month < 10 ) month = '0' + month;
          var yy    = tgl.getYear();
          var year  = (yy < 1000) ? yy + 1900 : yy;
          tgl=day + "-" + month + "-" + year;
          var dmax = TambahHari(tgl, jmllibur);
          var dmax2 = TambahHari(tgl, jmllibur);
          dtmp=dmax.split('-');
	        thnmax=dtmp[2];
		      blnmax=dtmp[1];
		      hrmax =dtmp[0];
		      dtmp2=dmax2.split('-');
	        thnmax2=dtmp2[2];
		      blnmax2=dtmp2[1];
		      hrmax2 =dtmp2[0];
//alert(blndsj);
//alert(hrdsj);
          if ( hrmax < 10 ) hrmax = '0' + hrmax;
          if ( blnmax < 10 ) blnmax = '0' + blnmax;
		      if( thnmax>thndsj ){
			      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!w');
			      document.getElementById('dsjreceive').value='';
	        }else if( thnmax>=thndsj ){
            if( blnmax>blndsj ){
				      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!s');
				      document.getElementById('dsjreceive').value='';
			      }else if( blnmax==blndsj ){
              if( hrmax>hrdsj ){
					      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!x');
					      document.getElementById('dsjreceive').value='';
				      }else if((blndsj==12) && (hrdsj>29)){
			      	  alert('Tidak Ada Receive Dari Tanggal 30-31 Desember !!!');
					      document.getElementById('dsjreceive').value='';
				      }//else if(hrmax2==hrdsj){
				      //	  alert('Tanggal Receive tidak boleh lebih dari hari ini !!!x');
					  //    document.getElementById('dsjreceive').value='';
				     // }
				    }
				  }
				}
      }else{
/**/
          jmllibur = document.getElementById('libur').value;
          if(jmllibur=='')jmllibur=0;
          jmllibur = parseFloat(jmllibur)+3;
          jmllibur = jmllibur*-1;
          var tgl  = new Date();
          var day   = tgl.getDate();
          var month = tgl.getMonth()+1;
          if ( month < 10 ) month = '0' + month;
          var yy    = tgl.getYear();
          var year  = (yy < 1000) ? yy + 1900 : yy;
          tgl=day + "-" + month + "-" + year;
          var dmax = TambahHari(tgl, jmllibur);
          var dmax2 = TambahHari(tgl, jmllibur);
          dtmp=dmax.split('-');
	        thnmax=dtmp[2];
		      blnmax=dtmp[1];
		      hrmax =dtmp[0];
		      dtmp2=dmax2.split('-');
	        thnmax2=dtmp2[2];
		      blnmax2=dtmp2[1];
		      hrmax2 =dtmp2[0];
          if ( hrmax < 10 ) hrmax = '0' + hrmax;
          if ( blnmax < 10 ) blnmax = '0' + blnmax;
		      if( thnmax>thndsj ){
			      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!w');
			      document.getElementById('dsjreceive').value='';
	        }else if( thnmax<=thndsj ){
            if( blnmax>blndsj ){
				      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!s');
				      document.getElementById('dsjreceive').value='';
			      }else if( blnmax==blndsj ){
              if( hrmax>hrdsj ){
					      alert('Tanggal Receive tidak boleh lebih dari 3 hari kerja sebelum hari ini !!!x');
					      document.getElementById('dsjreceive').value='';
				      }else if((blndsj==12) && (hrdsj>29)){
				      	  alert('Tidak Ada Receive Dari Tanggal 30-31 Desember !!!');
					      document.getElementById('dsjreceive').value='';
				      }//else if(hrmax2==hrdsj){
				      //	  alert('Tanggal Receive tidak boleh lebih dari hari ini !!!x');
					  //    document.getElementById('dsjreceive').value='';
				     // }
				    }
				  }
/**/      
      }
    }
  }
</script>
