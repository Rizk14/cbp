<table class="maintable">
  <tr>
    <td align="left">
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable" width="100%" border=0>
            <!-- <a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();">
              <input type="button" value="Export" class="float-right" />
            </a> -->

            <?php
            if ($isi->num_rows() > 0) {
              foreach ($isi->result() as $row) {
                $pkp = $row->f_customer_pkp == "t" ? "PKP" : "NON PKP";
            ?>
                <tr>
                  <td style="width:115px"><b>Nama Toko</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="width:250px" value="<?= $row->i_customer . " - " . $row->e_customer_name ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>TOP</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right" value="<?= $row->n_customer_toplength ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Plafon</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right" value="<?= number_format($row->v_flapond, 0) ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Terdaftar</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input value="<?= date('d-m-Y', strtotime($row->d_signin)) ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px" colspan="6">
                    <hr>
                  </td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Alamat</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <textarea id="alamat" style="width:250px;height:90px;" readonly>
                      <?= $row->e_customer_address ?>
                    </textarea>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px" colspan="6">
                    <hr>
                  </td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Jumlah Order</b></td>
                  <td style="width:1px">:</td>
                  <td colspan="2" style="width:30px">
                    <input style="text-align:right;width: 118px;" value="<?= number_format($row->min) ?>" readonly> -
                    <input style="text-align:right;width: 118px;" value="<?= number_format($row->max) ?>" readonly> per bulan
                  </td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Rata - rata Order</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right;width: 147px;" value="<?= number_format($row->avg) ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Lama Bayar</b></td>
                  <td style="width:1px">:</td>
                  <td colspan="2" style="width:30px">
                    <input style="text-align:right;width: 118px;" value="<?= number_format($lama->row()->min_byr, 0) . " hari" ?>" readonly> -
                    <input style="text-align:right;width: 118px;" value="<?= number_format($lama->row()->max_byr, 0) . " hari" ?>" readonly> per bulan
                  </td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Rata - rata Bayar</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right;width: 147px;" value="<?= number_format($lama->row()->avg_byr, 0) . " hari" ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Order Terendah</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right;width: 147px;" value="<?= number_format($isi2->row()->min_nota, 0) ?>" readonly> per nota
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Order Tertinggi</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="text-align:right;width: 147px;" value="<?= number_format($isi2->row()->max_nota, 0) ?>" readonly> per nota
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Cara Bayar</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="width: 147px;" value="<?= $isi3->row()->jenis_byr ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px"><b>PKP</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="width: 147px;" value="<?= $pkp ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px" colspan="6">
                    <hr>
                  </td>
                </tr>

                <tr>
                  <td style="width:115px"><b>Terakhir Order</b></td>
                  <td style="width:1px">:</td>
                  <td style="width:30px">
                    <input style="width: 147px;" value="<?= number_format($isi4->row()->v_nota_netto) . " (" . date('d-m-Y', strtotime($isi4->row()->d_nota)) . ")" ?>" readonly>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>

                <tr>
                  <td style="width:115px" colspan="6">
                    <hr>
                  </td>
                </tr>
              <?php } ?>
          </table>
          <!--------------------------------------------------------------------------------------->
          <table class="listtable">
            <thead>
              <th>Bulan & Tahun</th>
              <?php
              if ($bulan->num_rows() > 0) {
                $nbulan = 0;
                foreach ($bulan->result() as $key) {
                  if ($nbulan < 6) {
                    echo "<th style=\"width:150px;\"> $key->bulan </th>";
                  }
                  $nbulan++;
                }
              } ?>
            </thead>

            <tbody>
              <td style="text-align:center;">Order</td>
              <?php
              foreach ($isi->result() as $riwx) {
                $detbulanx = 0;
                foreach (json_decode($riwx->v_nota_netto) as $datax) {
                  $tmpx   = explode(',', $datax);
                  $vnotax   = $tmpx[0] == 0 ? "Belum Order" : number_format($tmpx[0]);

                  if ($detbulanx < 6) {
                    echo "<td value='$vnotax' align=\"right\">" . $vnotax . "</td>";
                  }
                  $detbulanx++;
                }
              }
              ?>
              <tr>
                <td style="text-align:center;">Lama Bayar</td>
                <?php
                foreach ($lama->result() as $riw1) {
                  $detbulan1a = 0;
                  foreach (json_decode($riw1->lama) as $data1) {
                    $tmp1   = explode(',', $data1);
                    $lama1   = $tmp1[0] == 0 ? "Belum Bayar" : number_format($tmp1[0]);

                    if ($detbulan1a < 6) {
                      echo "<td value='$lama1' align=\"right\">" . $lama1 . "</td>";
                    }
                    $detbulan1a++;
                  }
                }
                ?>
              </tr>
            </tbody>
            <!--------------------------------------------------------------------------------------->

            <thead>
              <th>Bulan & Tahun</th>
              <?php
              if ($bulan->num_rows() > 0) {
                $nbulan = 0;
                foreach ($bulan->result() as $key) {
                  if ($nbulan > 5) {
                    echo "<th style=\"width:150px;\"> $key->bulan </th>";
                  }
                  $nbulan++;
                }
              } ?>
            </thead>

            <tbody>
              <td style="text-align:center;">Order</td>
              <?php
              foreach ($isi->result() as $rewy) {
                $detbulany = 0;
                foreach (json_decode($rewy->v_nota_netto) as $datay) {
                  $tmpy   = explode(',', $datay);
                  $vnotay   = $tmpy[0] == 0 ? "Belum Order" : number_format($tmpy[0]);

                  if ($detbulany > 5) {
                    echo "<td value='$vnotay' align=\"right\">" . $vnotay . "</td>";
                  }
                  $detbulany++;
                }
              }
              ?>

              <tr>
                <td style="text-align:center;">Lama Bayar</td>
                <?php
                foreach ($lama->result() as $rew2) {
                  $detbulan2a = 0;
                  foreach (json_decode($rew2->lama) as $data2) {
                    $tmp2   = explode(',', $data2);
                    $lama2   = $tmp2[0] == null ? "Belum Bayar" : number_format($tmp2[0]);

                    if ($detbulan2a > 5) {
                      echo "<td value='$lama2' align=\"right\">" . $lama2 . "</td>";
                    }
                    $detbulan2a++;
                  }
                }
                ?>
              </tr>
            </tbody>
          </table>

        <?php
            } else {
              echo "<tr>
                      <td>Belum Ada Data!!</td>
                      <input hidden id=\"alamat\" value=\"\" readonly>
                    </tr>";
            } ?>
        <!-- end mastertable -->
        </div>
      </div>
      <!-- <div>
        <div id="div-chart-area" style="position: relative; width:100% ; height: 300px;"><canvas id="chart-area"></canvas></div>
      </div> -->
    </td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
  // $(function() {
  //   initOverallChart();
  // })

  $(document).ready(function() {
    s = document.getElementById("alamat").value;
    s = s.replace(/(^\s*)|(\s*$)/gi, "");
    s = s.replace(/[ ]{2,}/gi, " ");
    s = s.replace(/\n /, "\n");
    document.getElementById("alamat").value = s;
  });

  /* function initOverallChart() {
    // var dslabel = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var dslabel = [<#?php
                    foreach ($bulan->result() as $key) {
                      echo $key->bulan . ",";
                    }
                    ?>];
    var dscolor = ['#E86464CC', '#1400CCCC', '#5DC067CC'];

    var dataSet = [];

    $('#div-chart-area').parent().show();
    var dataReal = [<#?php
                    foreach ($isi->result() as $riwx) {
                      $detbulanx = 0;
                      foreach (json_decode($riwx->v_nota_netto) as $datax) {
                        $tmpx   = explode(',', $datax);
                        $vnotax   = $tmpx[0];

                        echo $vnotax;

                        $detbulanx++;
                      }
                    }
                    ?>];

    var dataData = {
      label: '',
      backgroundColor: [],
      data: []
    }

    for (let n = 0; n < dslabel.length; n++) {
      dataData.backgroundColor.push(dscolor[n]);
      dataData.data.push(dataReal[n]);
    }

    dataSet.push(dataData);

    var config = {
      type: 'line',
      data: {
        labels: dslabel,
        datasets: dataSet,
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            position: 'top'
          }
        }
      }

    }

    var BarChart = new Chart(document.querySelector('#chart-area'), config);
  } */
</script>