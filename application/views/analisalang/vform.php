<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => 'analisalang/cform/view', 'update' => '#pesan', 'type' => 'post')); ?>
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable">
            <tr>
              <td width="89%">
                <!-- <input type="text" id="icustomer" name="icustomer" value="" placeholder="Kd Lang" maxlength="5"> -->
                <input readonly id="icustomer" name="icustomer" type="hidden">
                <input id="ecustomername" name="ecustomername" onclick='showModal("analisalang/cform/customer/","#light");jsDlgShow("#konten *", "#fade", "#light");' placeholder="Nm Lang" readonly>

                <select name="tahun" id="tahun">
                  <?php
                  $tahun1 = date('Y') - 3;
                  $tahun2 = date('Y');
                  for ($i = $tahun1; $i <= $tahun2; $i++) {
                    echo "<option value='$i'";
                    if ($tahun2 == $i) {
                      echo "selected";
                    };
                    echo ">$i</option>";
                  }
                  ?>
                </select>
                <span class="px-3"></span>
                <input type="submit" class="px-3 btn btn-default" id="login" value="View" onclick="return dipales()">
                <span class="px-2"></span>
                <input name="cmdreset" id="cmdreset" value="Reset" type="button" onclick="show('analisalang/cform/','#main')">
              </td>
            </tr>
          </table>
        </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>

<div id="pesan"></div>

<script languge=javascript type=text/javascript>
  $(document).ready(function() {
    $('#tahun').change(function() {
      $('#login').click();
    });
  });

  xcDateFormat = "yyyy-mm";

  function dipales() {
    // var icustomer = $('#icustomer').val() /* .replace('-', '') */ ;

    // if (icustomer == '') {
    //   alert('Kolom Kd Lang harus diisi!!!');
    //   return false;
    // } else {
    $('#login').click();
    // }
  }

  function buatperiode1() {
    periode = document.getElementById("tahun1").value + document.getElementById("bulan1").value;
    document.getElementById("iperiode1").value = periode;
  }

  function buatperiode2() {
    periode = document.getElementById("tahun2").value + document.getElementById("bulan2").value;
    document.getElementById("iperiode2").value = periode;
  }
</script>