<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'bapb/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="bapbform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Tgl bapb</td>
		<?php 
			$tmp=explode("-",$isi->d_bapb);
			$th =$tmp[0];
			$bl =$tmp[1];
			$hr =$tmp[2];
			$dbapb=$hr."-".$bl."-".$th;
		?>
        <input hidden id="bbapb" name="bbapb" value="<?php echo $bl; ?>">
		<td><input readonly id="dbapb" name="dbapb" value="<?php echo $dbapb; ?>"
			   onclick="showCalendar('',this,this,'','dspmb',0,20,1)">
        <input hidden id="tglbapb" name="tglbapb" value="<?php echo $dbapb; ?>">
			<input id="ibapbold" name="ibapbold" type="text" value="<?php echo $isi->i_bapb_old; ?>">
		    <input id="ibapb" name="ibapb" type="hidden"  value="<?php echo $isi->i_bapb; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>"
			onclick='showModal("bapb/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Kirim</td>
		<td><input readonly id="edkbkirim" name="edkbkirim" value="<?php echo $isi->e_dkb_kirim; ?>"
			onclick='showModal("bapb/cform/kirim/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="idkbkirim" name="idkbkirim" type="hidden" value="<?php echo $isi->i_dkb_kirim; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>"
			onclick='showModal("bapb/cform/customer/"+document.getElementById("iarea").value+"/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Jumlah Bal</td>
		<td><input style="text-align:right;" id="nbal" name="nbal" value="<?php echo $isi->n_bal; $areabapb=substr($isi->i_bapb,9,2);?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Nilai BAPB</td>
		<td><input style="text-align:right;" id="vbapb" name="vbapb" value="<?php echo $isi->v_bapb; ?>" readonly></td>
	      </tr>
	      <tr>
		<td width="10%">Biaya Kirim</td>
		<td><input style="text-align:right;" id="vkirim" name="vkirim" value="<?php echo $isi->v_kirim; ?>"></td>
	      </tr>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
			<input type="hidden" name="jmlx" id="jmlx" value="<?php echo $jmlitemx; ?>">
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" 
         onclick='show("listbapb/cform/view/<?php echo $dfrom."/".$dto."/".$areabapb."/"; ?>","#main")'>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
		    <input name="cmdtambahekspedisi" id="cmdtambahekspedisi" value="Tambah Ekspedisi" type="button" 
			   onclick="tambah_ekspedisi(parseFloat(document.getElementById('jmlx').value)+1);"></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:750px;">
				<tr><th style="width:25px;"  align="center">No</th>
				<th style="width:180px;" align="center">No SJ</th>
				<th style="width:80px;" align="center">Tgl SJ</th>
				<th style="width:200px;" align="center">Pelanggan</th>
				<th style="width:100px;" align="center">Nilai</th>
				<th style="width:100px;" align="center">Keterangan</th>
				<th style="width:32px;"  align="center" class="action">Action</th></tr>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
					$qtodetail	= $this->mmaster->customertodetail($row->i_sj,$row->d_sj,$iarea);
					if($qtodetail->num_rows()>0){
						$rtodetail=$qtodetail->row();
						$e_customer_name = $rtodetail->e_customer_name;
					}
					echo '<table class="listtable" align="center" style="width:750px;">';
				  	$i++;
						$tmp=explode("-",$row->d_sj);
						$th =$tmp[0];
						$bl =$tmp[1];
						$hr =$tmp[2];
						$row->d_sj=$hr."-".$bl."-".$th;
						echo '<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'.$i.'" name="baris'.$i.'" value="'.$i.'"></td>
						<td style="width:174px;"><input style="width:174px;" readonly type="text" id="isj'.$i.'" name="isj'.$i.'" value="'.$row->i_sj.'"></td>
						<td style="width:77px;"><input style="width:77px;" readonly type="text" id="dsj'.$i.'" name="dsj'.$i.'" value="'.$row->d_sj.'"></td>
						<td style="width:196px;"><input style="width:196px;" readonly type="text" id="icustomerx'.$i.'" name="icustomerx'.$i.'" value="'.$e_customer_name.'"></td>
            <td style="width:98px;"><input style="width:98px;" readonly type="text" id="vsj'.$i.'" name="vsj'.$i.'" value="'.$row->v_sj.'"></td>
						<td style="width:100px;"><input style="width:100px;"  type="text" id="eremark'.$i.'" name="eremark'.$i.'" value="'.$row->e_remark.'"></td>';
						echo "<td style=\"width:50px;\" align=\"center\"><a href=\"#\" onclick='hapus(\"bapb/cform/deletedetail/$row->i_bapb/$row->i_area/$row->i_sj/$dfrom/$dto/$isi->i_customer/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>";
						echo '</tr></tbody></table>';
				}
				?>
			</div>
			<div id="detailheaderx" align="center">
				<table id="itemtemx" class="listtable" style="width:750px;">
				<tr><th style="width:25px;"  align="center">No</th>
				<th style="width:60px;" align="center">Kode</th>
				<th style="width:300px;" align="center">Nama</th>
				<th style="width:300px;" align="center">Keterangan</th>
				<th style="width:32px;"  align="center" class="action">Action</th></tr>
				</table>
			</div>
			<div id="detailisix" align="center">
				<?php 				
				$i=0;
				foreach($detailx as $row)
				{
					echo '<table class="listtable" align="center" style="width:750px;">';
				  	$i++;
						echo '<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx'.$i.'" name="barisx'.$i.'" value="'.$i.'"></td>
						<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi'.$i.'" name="iekspedisi'.$i.'" value="'.$row->i_ekspedisi.'"></td>
						<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname'.$i.'" name="eekspedisiname'.$i.'" value="'.$row->e_ekspedisi.'"></td>
						<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx'.$i.'" name="eremarkx'.$i.'" value="'.$row->e_remark.'"></td>';
						echo "<td style=\"width:50px;\" align=\"center\"><a href=\"#\" onclick='hapus(\"bapb/cform/deletedetailekspedisi/$row->i_bapb/$row->i_area/$row->i_ekspedisi/$dfrom/$dto/$isi->i_customer/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a></td>";
						echo '</tr></tbody></table>';
				}
				?>
			</div>
			<div id="pesan"></div>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  /*function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
			so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
			so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner+= '<th style="width:200px;" align="center">No SJ</th>';
			so_inner+= '<th style="width:100px;" align="center">Tgl SJ</th>';
			so_inner+= '<th style="width:230px;" align="center">Pelanggan</th>';
			so_inner+= '<th style="width:200px;" align="center">Keterangan</th>';
			so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
			document.getElementById("detailheader").innerHTML=so_inner;
    }else{
			so_inner=''; 
    }
    if(si_inner==''){
			document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			juml=document.getElementById("jml").value;	
			si_inner='<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
			si_inner+='<td style="width:160px;"><input style="width:160px;" readonly type="text" id="isj'+a+'" name="isj'+a+'" value=""></td>';
			si_inner+='<td style="width:92px;"><input style="width:92px;" readonly type="text" id="dsj'+a+'" name="dsj'+a+'" value=""></td>';
			si_inner+='<td style="width:210px;"><input style="width:210px;" readonly type="text" id="icustomerx'+a+'" name="icustomerx'+a+'" value=""></td>';
			si_inner+='<td style="width:185px;"><input style="width:185px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
			si_inner+='<td style="width:51px;">&nbsp;</td></tr></tbody></tabel>';
    }else{
			document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
			juml=document.getElementById("jml").value;
			si_inner+='<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
			si_inner+='<td style="width:160px;"><input style="width:160px;" readonly type="text" id="isj'+a+'" name="isj'+a+'" value=""></td>';
			si_inner+='<td style="width:92px;"><input style="width:92px;" readonly type="text" id="dsj'+a+'" name="dsj'+a+'" value=""></td>';
			si_inner+='<td style="width:210px;"><input style="width:210px;" readonly type="text" id="icustomerx'+a+'" name="icustomerx'+a+'" value=""></td>';
			si_inner+='<td style="width:185px;"><input style="width:185px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
			si_inner+='<td style="width:51px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris		= Array()
    var isj		= Array();
    var dsj		= Array();
    var icustomerx	= Array();	
    var eremark	= Array();
    for(i=1;i<a;i++){
			j++;
			baris[j]	= document.getElementById("baris"+i).value;
			isj[j]		= document.getElementById("isj"+i).value;
			dsj[j]		= document.getElementById("dsj"+i).value;
			icustomerx[j]	= document.getElementById("icustomerx"+i).value;
			eremark[j]	= document.getElementById("eremark"+i).value;
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
			j++;
			document.getElementById("baris"+i).value=baris[j];
			document.getElementById("isj"+i).value=isj[j];
			document.getElementById("dsj"+i).value=dsj[j];
			document.getElementById("icustomerx"+i).value=icustomerx[j];
			document.getElementById("eremark"+i).value=eremark[j];
    }
		area=document.getElementById("iareax").value;
		//area=document.getElementById("arealogin").value;
		customer=document.getElementById("icustomer").value;
		showModal("bapb/cform/sjupdate/"+a+"/"+area+"/"+customer+"/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
  } */
  function tambah_item(a){
//    if(a<=30){
	    so_inner=document.getElementById("detailheader").innerHTML;
	    si_inner=document.getElementById("detailisi").innerHTML;
	    if(so_inner==''){
				so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
				so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
				so_inner+= '<th style="width:200px;" align="center">No SJ</th>';
				so_inner+= '<th style="width:100px;" align="center">Tgl SJ</th>';
				so_inner+= '<th style="width:230px;" align="center">Pelanggan</th>';
				so_inner+= '<th style="width:100px;" align="center">Nilai</th>';
				so_inner+= '<th style="width:200px;" align="center">Keterangan</th>';
				so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
				document.getElementById("detailheader").innerHTML=so_inner;
	    }else{
				so_inner=''; 
	    }
	    if(si_inner==''){
				document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
				juml=document.getElementById("jml").value;	
				si_inner='<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
				si_inner+='<td style="width:174px;"><input style="width:174px;" readonly type="text" id="isj'+a+'" name="isj'+a+'" value=""></td>';
				si_inner+='<td style="width:77px;"><input style="width:77px;" readonly type="text" id="dsj'+a+'" name="dsj'+a+'" value=""></td>';
				si_inner+='<td style="width:196px;"><input style="width:196px;" readonly type="text" id="icustomerx'+a+'" name="icustomerx'+a+'" value=""></td>';
				si_inner+='<td style="width:98px;"><input readonly style="width:98px; text-align:right;"  type="text" id="vsj'+a+'" name="vsj'+a+'" value=""></td>';
				si_inner+='<td style="width:100px;"><input style="width:100px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
				si_inner+='<td style="width:50px;">&nbsp;</td></tr></tbody>';
	    }else{
				document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
				juml=document.getElementById("jml").value;
				si_inner+='<tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
				si_inner+='<td style="width:174px;"><input style="width:174px;" readonly type="text" id="isj'+a+'" name="isj'+a+'" value=""></td>';
				si_inner+='<td style="width:77px;"><input style="width:77px;" readonly type="text" id="dsj'+a+'" name="dsj'+a+'" value=""></td>';
				si_inner+='<td style="width:196px;"><input style="width:196px;" readonly type="text" id="icustomerx'+a+'" name="icustomerx'+a+'" value=""></td>';
				si_inner+='<td style="width:98px;"><input readonly style="width:98px; text-align:right;"  type="text" id="vsj'+a+'" name="vsj'+a+'" value=""></td>';
				si_inner+='<td style="width:100px;"><input style="width:100px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
				si_inner+='<td style="width:50px;">&nbsp;</td></tr></tbody>';
	    }
	    j=0;
	    var baris		= Array()
	    var isj		= Array();
	    var dsj		= Array();
	    var vsj		= Array();
	    var icustomerx	= Array();	
	    var eremark	= Array();
	    for(i=1;i<a;i++){
				j++;
				baris[j]	= document.getElementById("baris"+i).value;
				isj[j]		= document.getElementById("isj"+i).value;
				dsj[j]		= document.getElementById("dsj"+i).value;
				vsj[j]		= document.getElementById("vsj"+i).value;
				icustomerx[j]	= document.getElementById("icustomerx"+i).value;
				eremark[j]	= document.getElementById("eremark"+i).value;
	    }
	    document.getElementById("detailisi").innerHTML=si_inner;
	    j=0;
	    for(i=1;i<a;i++){
				j++;
				document.getElementById("baris"+i).value=baris[j];
				document.getElementById("isj"+i).value=isj[j];
				document.getElementById("dsj"+i).value=dsj[j];
				document.getElementById("vsj"+i).value=vsj[j];
				document.getElementById("icustomerx"+i).value=icustomerx[j];
				document.getElementById("eremark"+i).value=eremark[j];
	    }
			area=document.getElementById("iarea").value;
			//area=document.getElementById("arealogin").value;
			customer=document.getElementById("icustomer").value;
			showModal("bapb/cform/sj/"+a+"/"+area+"/"+customer+"/","#light");
			jsDlgShow("#konten *", "#fade", "#light");
//		}else{
//			alert("Maksimal 30 item");
//		}
  }
  function dipales(a){
  	cek='false';
		if(
			(document.getElementById("dbapb").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
			(document.getElementById("idkbkirim").value!='') &&
  	 	(document.getElementById("jmlx").value!='') &&
			(document.getElementById("jml").value!='') &&
  	 	(document.getElementById("nbal").value!='')
			) 
		{
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
					if((document.getElementById("isj"+i).value=='') ||
						(document.getElementById("dsj"+i).value=='') ){
						alert('Data item masih ada yang salah !!!');
						exit();
						cek='false';
					}else{
						cek='true';	
					} 
				}
			}
			if(cek=='true'){
  	  	document.getElementById("login").disabled=true;
				document.getElementById("cmdtambahitem").disabled=true;
				document.getElementById("cmdtambahekspedisi").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
			}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function tambah_ekspedisi(a){
    so_inner=document.getElementById("detailheaderx").innerHTML;
    si_inner=document.getElementById("detailisix").innerHTML;
    if(so_inner==''){
			so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
			so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner+= '<th style="width:60px;" align="center">Kode</th>';
			so_inner+= '<th style="width:300px;" align="center">Nama</th>';
			so_inner+= '<th style="width:300px;" align="center">Keterangan</th>';
			so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
			document.getElementById("detailheaderx").innerHTML=so_inner;
    }else{
			so_inner=''; 
    }
    if(si_inner==''){
			document.getElementById("jmlx").value=parseFloat(document.getElementById("jmlx").value)+1;
			juml=document.getElementById("jmlx").value;	
			si_inner='<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx'+a+'" name="barisx'+a+'" value="'+a+'"></td>';
			si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi'+a+'" name="iekspedisi'+a+'" value=""></td>';
			si_inner+='<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname'+a+'" name="eekspedisiname'+a+'" value=""></td>';
			si_inner+='<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx'+a+'" name="eremarkx'+a+'" value=""></td>';
			si_inner+='<td style="width:50px;">&nbsp;</td></tr></tbody></table>';
    }else{
			document.getElementById("jmlx").value=parseFloat(document.getElementById("jmlx").value)+1;
			juml=document.getElementById("jmlx").value;
			si_inner=si_inner+'<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx'+a+'" name="barisx'+a+'" value="'+a+'"></td>';
			si_inner+='<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi'+a+'" name="iekspedisi'+a+'" value=""></td>';
			si_inner+='<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname'+a+'" name="eekspedisiname'+a+'" value=""></td>';
			si_inner+='<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx'+a+'" name="eremarkx'+a+'" value=""></td>';
			si_inner+='<td style="width:50px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var barisx				= Array()
    var iekspedisi		= Array();
    var eekspedisiname= Array();
    var eremark				= Array();
    for(i=1;i<a;i++){
			j++;
			barisx[j]					= document.getElementById("barisx"+i).value;
			iekspedisi[j]			= document.getElementById("iekspedisi"+i).value;
			eekspedisiname[j]	= document.getElementById("eekspedisiname"+i).value;
			eremark[j]				= document.getElementById("eremarkx"+i).value;
    }
    document.getElementById("detailisix").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
			j++;
			document.getElementById("barisx"+i).value=barisx[j];
			document.getElementById("iekspedisi"+i).value=iekspedisi[j];
			document.getElementById("eekspedisiname"+i).value=eekspedisiname[j];
			document.getElementById("eremarkx"+i).value=eremark[j];
    }
		showModal("bapb/cform/ekspedisiupdate/"+a+"/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
  }
  function afterSetDateValue(ref_field, target_field, date) {
    dspb=document.getElementById('dbapb').value;
    bspb=document.getElementById('bbapb').value;
    dtmp=dspb.split('-');
    per=dtmp[2]+dtmp[1]+dtmp[0];
    bln = dtmp[1];
    if( (bspb!='') && (dspb!='') ){
      if(bspb != bln)
      {
        alert("Tanggal BAPB tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("dbapb").value=document.getElementById("tglbapb").value;
      }
    }
  }
</script>
