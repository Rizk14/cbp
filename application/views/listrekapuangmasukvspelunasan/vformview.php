<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listrekapuangmasukvspelunasan/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  	<th align="center">KODE AREA</th>
		<th align="center">NAMA AREA</th>
		<th align="center">UANG MASUK</th>
		<th align="center">PELUNASAN</th>
		<th align="center">SELISIH</th>
	    <tbody>
			<?php
			$jumvbank = 0;
			$jumalokasi = 0;
			$jumselisih = 0;
			foreach ($isi as $row) {
				$jumvbank = $jumvbank + $row->v_bank;
				$jumalokasi = $jumalokasi + $row->alokasi;
				$jumselisih = $jumselisih + $row->selisih;
				
				$row->v_bank = number_format($row->v_bank);
				$row->alokasi = number_format($row->alokasi);
				$row->selisih = number_format($row->selisih);
				echo "<tr>
				<td>$row->i_area</td>
				<td>$row->e_area_name</td>
				<td align='right'>$row->v_bank</td>
				<td align='right'>$row->alokasi</td>
				<td align='right'>$row->selisih</td>
				</tr>";
			}
			$jumvbank = number_format($jumvbank);
			$jumalokasi = number_format($jumalokasi);
			$jumselisih = number_format($jumselisih);
			echo "<tr>
			<td colspan='2'>Jumlah</td>
			<td align='right'>$jumvbank</td>
			<td align='right'>$jumalokasi</td>
			<td align='right'>$jumselisih</td>
			</tr>";
			?>
	    </tbody>
		<?php } ?>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
  <tr>
      <td><input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listrekapuangmasukvspelunasan/cform/','#main')"></td>
  </tr>
</table>
</div>
