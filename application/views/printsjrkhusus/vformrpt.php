<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: landscape;
}

@page { size: Letter; }

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper3 {
  font-size: 14px;
  FONT-WEIGHT: normal; 
}
.nmper2 {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 12px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 12px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 8px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.garisatas { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
$i=0;
foreach($isi as $row)
{
?>
  <table width="60%" class="nmper">
    <tr>
      <td colspan="4" class="huruf judul" ><?php echo NmPerusahaan; ?></td>
    </tr>
    <tr>
      <td class="huruf nmper2" colspan="2" width="10"><b>SURAT JALAN</b></td>
      <td width=150 align="center" colspan=2>Kepada Yth.</td>
    </tr>
    <tr>
      <td colspan="4" class="huruf nmper">PENGEMBALIAN STOK KE GUDANG PUSAT </td>
    </tr>
    <tr>
      <td width="10" class="huruf nmper"colspan=3>No. <?php echo $isj; ?></td>
      <td align="center">GUDANG PUSAT</td>
    </tr>
</table>
<br>
<table align="left">
  <tr>
    <td colspan="4" class="garisbawah">&nbsp;</td>
  </tr>
  <tr>
    <td width="39" class="huruf nmper3 garisbawah">NO URUT.</td>
    <td width="142" class="huruf nmper3 garisbawah">KODE BARANG</td>
    <td width="450" class="huruf nmper3 garisbawah">NAMA BARANG</td>
    <td align="center" width="150" class="huruf nmper3 garisbawah">JUMLAH DIKIRIM</td>
  </tr>
  <?php 
  foreach ($detail as $row1){
  $i++;
?><tr>
    <td class="huruf nmper"><?php echo $i;?></td>
    <td class="huruf nmper"><?php echo $row1->i_product;?></td>
    <td class="huruf nmper"><?php echo $row1->e_product_name;?></td>
    <td class="huruf nmper" align="center"><?php echo $row1->n_quantity_retur;?></td>
  </tr>
<?php 
  }
  ?>
  <tr>
    <td colspan="4" class="garisbawah">&nbsp;</td>
  </tr>
  <tr>
    <td align="right"colspan=4>Bandung, <?php echo $row->d_sjr; ?></td>
  </tr>
  <tr>
  <td align="center" colspan="2">Penerima</td>
  <td align="center">Mengetahui</td>
  <td align="center">Pengirim</td>
  </tr>
  <tr>
  <td><?php echo '<br><br><br><br>'?></td>
  </tr>
  <tr>
    <td colspan=2 align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
    <td align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
    <td align="center">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
  </tr>
  <tr>
<?php    $tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");?>
    <td colspan="4">Tanggal Cetak : <?php echo $tgl; ?></td>
  </tr>
  <tr>
    <td colspan=3>&nbsp;</td>
    <td align="right">
      <div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
    </td>
  </tr>
</table>
<?php }?>
