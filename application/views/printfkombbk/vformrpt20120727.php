<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
	foreach($isi as $row){
		$nor	= str_repeat(" ",5);
		$abn	= str_repeat(" ",12);
		$ab	= str_repeat(" ",9);
		$ipp 	= new PrintIPP();
		$ipp->setHost($host);
		$ipp->setPrinterURI($uri);
		$ipp->setRawText();
		$ipp->unsetFormFeed();
		$cetak.=CHR(18)."\n";
		$cetak.=$nor.CHR(27).CHR(71).NmPerusahaan."                  ".CHR(27).CHR(72)."KEPADA Yth.".CHR(27).CHR(71)."\n";
    if($row->e_customer_pkpname=='') $row->e_customer_pkpname=$row->e_customer_name;
		$cetak.=$nor.AlmtPerusahaan."                           ".CHR(27).CHR(72).$row->e_customer_pkpname.CHR(27).CHR(71)."\n";
    if( ($row->e_customer_pkpnpwp=='') || ($row->e_customer_pkpnpwp==null) ){
      $row->e_customer_pkpnpwp="00.000.000.0.000.000";
      $row->e_customer_pkpaddress=$row->e_customer_city;
    }
    if($row->e_customer_pkpaddress=='')$row->e_customer_pkpaddress=$row->e_customer_address;
 		$pjg=strlen($row->e_customer_pkpaddress);
    if($pjg>40){
  		$cetak.=$nor.CHR(15).KotaPerusahaan."                                                                    ".$row->e_customer_pkpaddress.CHR(18).CHR(27).CHR(71)."\n";
    }else{
  		$cetak.=$nor.KotaPerusahaan."                                      ".$row->e_customer_pkpaddress.CHR(27).CHR(71)."\n";
    }
		$cetak.=$nor."Telp. ".TlpPajak."\n";
		$cetak.=$nor."NPWP : ".NPWPPerusahaan.CHR(27).CHR(72)."                  NPWP : ".$row->e_customer_pkpnpwp."\n\n";
		$cetak.=$nor."                          NO. DOK/FAKTUR : ".substr($row->i_nota,8,8)."/".substr($row->i_faktur_komersial,8,6)."\n";
		$cetak.=$nor."                          KODE SL/LANGG. : 01/".$row->i_salesman."-".$row->i_area."/".substr($row->i_customer,2,3)."\n";
		$cetak.=$nor.str_repeat("=",78)."\n";
    $cetak.=$nor."NO. KD-BARANG       NAMA BARANG                      UNIT    HARGA     JUMLAH\n";
    $cetak.=$nor.str_repeat("-",78).CHR(15)."\n";
		$i=0;
    $total=0;
		foreach($detail as $rowi){
      if($rowi->n_deliver>0){
				$i++;
				$prod	= $rowi->i_product;
				$name	= $rowi->e_product_name." ".str_repeat(".",66-strlen($rowi->e_product_name ));
				$deli	= number_format($rowi->n_deliver);
				$pjg	= strlen($deli);
				$spcdel	= 7;
				for($xx=1;$xx<=$pjg;$xx++){
					$spcdel	= $spcdel-1;
				}
#				if($row->f_plus_ppn=='t'){
					$pric	= number_format($rowi->v_unit_price);
#				}else{
#					$pric	= number_format($rowi->v_unit_price/1.1);
#				}
				$pjg	= strlen($pric);
				$spcpric= 18;
				for($xx=1;$xx<=$pjg;$xx++){
					$spcpric= $spcpric-1;
				}
#				if($row->f_plus_ppn=='t'){
					$tot	= number_format($rowi->n_deliver*$rowi->v_unit_price);
#				}else{
#					$tot	= number_format($rowi->n_deliver*($rowi->v_unit_price/1.1));
#				}
				$pjg	= strlen($tot);
				$spctot = 19;
				for($xx=1;$xx<=$pjg;$xx++){
					$spctot	= $spctot-1;
				}
				$aw=13;
				$pjg	= strlen($i);
				for($xx=1;$xx<=$pjg;$xx++){
					$aw=$aw-1;
				}
				$aw=str_repeat(" ",$aw);
        $total	= $total+str_replace(',','',$tot);
				$cetak.=CHR(15).$aw.$i.str_repeat(" ",5).$prod.str_repeat(" ",5).$name.str_repeat(" ",$spcdel).$deli.str_repeat(" ",$spcpric).$pric.str_repeat(" ",$spctot).$tot."\n";
			}
		}
		$cetak.=CHR(18).$nor.str_repeat("-",78)."\n";
###################################
		$row->v_nota_gross=$total;
		if(strlen(number_format($row->v_nota_gross))==1){
			$cetak.=str_repeat(" ",48)."TOTAL                 :                ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==4){
			$cetak.=str_repeat(" ",48)."TOTAL                 :       ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==5){
			$cetak.=str_repeat(" ",48)."TOTAL                 :      ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==6){
			$cetak.=str_repeat(" ",48)."TOTAL                 :     ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==7){
			$cetak.=str_repeat(" ",48)."TOTAL                 :    ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==8){
			$cetak.=str_repeat(" ",48)."TOTAL                 :   ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==9){
			$cetak.=str_repeat(" ",48)."TOTAL                 :  ".number_format($row->v_nota_gross)."\n";
		}elseif(strlen(number_format($row->v_nota_gross))==10){
			$cetak.=str_repeat(" ",48)."TOTAL                 : ".number_format($row->v_nota_gross)."\n";
		}
    $vdisc1=0;
    $vdisc2=0;
    $vdisc3=0;
    $vdisc4=0;
    $vdisc1=round($vdisc1+($total*$row->n_nota_discount1)/100);
	  $vdisc2=round($vdisc2+((($total-$vdisc1)*$row->n_nota_discount2)/100));
	  $vdisc3=round($vdisc3+((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100));
  	$vdisc4=round($vdisc4+((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100));
    $vdistot	= round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
  
    $row->v_nota_discount=$vdistot;
		if(strlen(number_format($row->v_nota_discount))==1){
			$cetak.=str_repeat(" ",48)."POTONGAN              :          ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==3){
			$cetak.=str_repeat(" ",48)."POTONGAN              :        ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==4){
			$cetak.=str_repeat(" ",48)."POTONGAN              :       ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==5){
			$cetak.=str_repeat(" ",48)."POTONGAN              :      ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==6){
			$cetak.=str_repeat(" ",48)."POTONGAN              :     ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==7){
			$cetak.=str_repeat(" ",48)."POTONGAN              :    ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==8){
			$cetak.=str_repeat(" ",48)."POTONGAN              :   ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==9){
			$cetak.=str_repeat(" ",48)."POTONGAN              :  ".number_format($row->v_nota_discount)."\n";
		}elseif(strlen(number_format($row->v_nota_discount))==10){
			$cetak.=str_repeat(" ",48)."POTONGAN              : ".number_format($row->v_nota_discount)."\n";
		}
		$cetak.=str_repeat(" ",72).str_repeat("-",10)." -\n";
    if($row->f_plus_ppn=='f'){
      $row->v_nota_netto=$total-$vdistot+$vppn;
		  if(strlen(number_format($row->v_nota_netto))==1){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :          ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==4){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :       ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==5){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :      ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==6){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :     ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==7){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :    ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==8){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :   ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==9){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :  ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==10){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          : ".number_format($row->v_nota_netto)."\n";
		  }
    }else{
		  if(strlen(number_format($row->v_nota_netto))==1){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :          ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==4){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :       ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==5){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :      ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==6){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :     ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==7){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :    ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==8){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :   ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==9){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          :  ".number_format($row->v_nota_netto)."\n";
		  }elseif(strlen(number_format($row->v_nota_netto))==10){
			  $cetak.=str_repeat(" ",48)."NILAI FAKTUR          : ".number_format($row->v_nota_netto)."\n";
		  }
    }
    $cetak.=str_repeat(" ",72).str_repeat("=",10)."\n";
    $ndpp=round($row->v_nota_netto/1.1);
    $nppn=round($row->v_nota_netto/1.1*0.1);
    if(strlen(number_format($ndpp))==1){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :          ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==4){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :       ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==5){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :      ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==6){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :     ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==7){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :    ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==8){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :   ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==9){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK :  ".number_format($ndpp)."\n";
	  }elseif(strlen(number_format($ndpp))==10){
		  $cetak.=str_repeat(" ",48)."DASAR PENGENAAN PAJAK : ".number_format($ndpp)."\n";
	  }
    if(strlen(number_format($nppn))==1){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :          ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==4){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :       ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==5){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :      ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==6){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :     ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==7){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :    ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==8){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :   ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==9){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              :  ".number_format($nppn)."\n";
	  }elseif(strlen(number_format($nppn))==10){
		  $cetak.=str_repeat(" ",48)."PPN 10 %              : ".number_format($nppn)."\n";
	  }
    $cetak.=str_repeat(" ",72).str_repeat("=",10).CHR(15)."\n\n";
    $bilangan = new Terbilang;
		$kata=ucwords($bilangan->eja($row->v_nota_netto));
		$cetak.=$ab."(".$kata." Rupiah)".CHR(18)."\n\n\n";
    $tmp=explode("-",$row->d_nota);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$dnota=$hr."/".$bl."/".$th;
	  $cetak.=str_repeat(" ",45)."BANDUNG, ".$dnota."\n";
    $cetak.=str_repeat(" ",54)."S E & O\n\n\n\n\n\n";
    $cetak.=str_repeat(" ",45).ucwords(strtolower($nama)).CHR(15)."\n";
    $cetak.=str_repeat(" ",5)."Catatan :\n";
    $cetak.=str_repeat(" ",5)."1. Barang-barang yang sudah dibeli tidak dapat ditukar/dikembalikan, kecuali ada perjanjian terlebih dahulu\n";
    $cetak.=str_repeat(" ",5)."2. Faktur asli merupakan bukti pembayaran yang sah.\n";
    $ipp->setFormFeed();
    $cetak.=str_repeat(" ",5)."3. Pembayaran dengan cek/giro berharga baru dianggap sah setelah diuangkan/cair.".CHR(18)."\n";
  }
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";
?>
