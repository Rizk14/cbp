<?php 
  include ("php/fungsi.php");
  require_once("printipp/PrintIPP.php");
  $cetak='';
  $nama=str_replace("%20"," ",$nama);
  $x=0;
  foreach($isi as $row){
    if($x==0){
      $x++;
      $nor  = str_repeat(" ",5);
      $abn  = str_repeat(" ",12);
      $ab   = str_repeat(" ",9);
      $ipp  = new PrintIPP();
      $ipp->setHost($host);
      $ipp->setPrinterURI($uri);
      $ipp->setRawText();
      $ipp->unsetFormFeed();
      $cetak.=CHR(18)."\n";
      $cetak.=$nor.CHR(27).CHR(71).NmPerusahaan."                ".CHR(27).CHR(72).PajakBBK.CHR(27).CHR(71)."\n";
      $cetak.=$nor.AlmtPerusahaan."\n";
      $cetak.=$nor.KotaPerusahaan."\n";
      $cetak.=$nor."Telp. ".TlpPajak."\n";
      $cetak.=$nor."NPWP : ".NPWPPerusahaan.CHR(27).CHR(72)."                NPWP : 00.000.000.0.000.000\n\n";
      $cetak.=$nor."                          NO. DOK/FAKTUR : 0000".substr($row->d_pajak,5,2)."/".substr($row->i_faktur_komersial,8,6)."\n\n";
      $cetak.=$nor.str_repeat("=",73)."\n";
      $cetak.=$nor."NO. KD-BARANG       NAMA BARANG                   UNIT    HARGA    JUMLAH\n";
      $cetak.=$nor.str_repeat("-",73).CHR(15)."\n";
      $i=0;
      $total=0;
      $jmlbrs=count($detail);
      foreach($detail as $rowi){
        if($rowi->n_quantity>0){
          $i++;
          $prod = $rowi->i_product;
          $name = $rowi->e_product_name." ".str_repeat(".",62-strlen($rowi->e_product_name ));
          $deli = number_format($rowi->n_quantity);
          $pjg  = strlen($deli);
          $spcdel  = 7;
          for($xx=1;$xx<=$pjg;$xx++){
            $spcdel  = $spcdel-1;
          }
          $pric = number_format($rowi->v_unit_price);
          $pjg  = strlen($pric);
          $spcpric= 18;
          for($xx=1;$xx<=$pjg;$xx++){
            $spcpric= $spcpric-1;
          }
          $tot  = number_format($rowi->n_quantity*$rowi->v_unit_price);
          $pjg  = strlen($tot);
          $spctot = 15;
          for($xx=1;$xx<=$pjg;$xx++){
            $spctot  = $spctot-1;
          }
          $aw=13;
          $pjg  = strlen($i);
          for($xx=1;$xx<=$pjg;$xx++){
            $aw=$aw-1;
          }
          $aw=str_repeat(" ",$aw);
          $total = $total+str_replace(',','',$tot);
          $cetak.=CHR(15).$aw.$i.str_repeat(" ",5).$prod.str_repeat(" ",5).$name.str_repeat(" ",$spcdel).$deli.str_repeat(" ",$spcpric).$pric.str_repeat(" ",$spctot).$tot."\n";
        }
########
        if($jmlbrs>40){
          if($i%40==0){
            $cetak.=(CHR(18).$nor.str_repeat('-',73)."\n");
            $cetak.=($nor.str_repeat(" ",13)."jumlah Dipindahkan .......                         ".number_format($total)."\n");
            $ipp->setFormFeed();
            $ipp->setdata($cetak);
            $ipp->printJob();
            $cetak='';
            $ipp->unsetFormFeed();
            $cetak.=CHR(18)."\n";
            $cetak.=$nor.CHR(27).CHR(71).NmPerusahaan."                ".CHR(27).CHR(72).PajakBBK.CHR(27).CHR(71)."\n";
            $cetak.=$nor.AlmtPerusahaan."\n";
            $cetak.=$nor.KotaPerusahaan."\n";
            $cetak.=$nor."Telp. ".TlpPajak."\n";
            $cetak.=$nor."NPWP : ".NPWPPerusahaan.CHR(27).CHR(72)."                NPWP : 00.000.000.0.000.000\n\n";
            $cetak.=$nor."                          NO. DOK/FAKTUR : 0000".substr($row->d_pajak,5,2)."/".substr($row->i_faktur_komersial,8,6)."\n\n";
            $cetak.=$nor.str_repeat("=",73)."\n";
            $cetak.=$nor."NO. KD-BARANG       NAMA BARANG                   UNIT    HARGA    JUMLAH\n";
            $cetak.=$nor.str_repeat("-",73).CHR(15)."\n";
            $cetak.=(CHR(18).$nor.str_repeat(" ",13)."jumlah pindahan .......                            ".number_format($total)."\n");
          }
        }elseif($jmlbrs>25 && $jmlbrs<=40){
          if($i%25==0){
            $cetak.=(CHR(18).$nor.str_repeat('-',73)."\n");
            $cetak.=($nor.str_repeat(" ",13)."Jumlah Dipindahkan .......                         ".number_format($total)."\n");
            $ipp->setFormFeed();
            $ipp->setdata($cetak);
            $ipp->printJob();
            $cetak='';
            $ipp->unsetFormFeed();
            $cetak.=CHR(18)."\n";
            $cetak.=$nor.CHR(27).CHR(71).NmPerusahaan."                ".CHR(27).CHR(72).PajakBBK.CHR(27).CHR(71)."\n";
            $cetak.=$nor.AlmtPerusahaan."\n";
            $cetak.=$nor.KotaPerusahaan."\n";
            $cetak.=$nor."Telp. ".TlpPajak."\n";
            $cetak.=$nor."NPWP : ".NPWPPerusahaan.CHR(27).CHR(72)."                NPWP : 00.000.000.0.000.000\n\n";
            $cetak.=$nor."                          NO. DOK/FAKTUR : 0000".substr($row->d_pajak,5,2)."/".substr($row->i_faktur_komersial,8,6)."\n\n";
            $cetak.=$nor.str_repeat("=",73)."\n";
            $cetak.=$nor."NO. KD-BARANG       NAMA BARANG                   UNIT    HARGA    JUMLAH\n";
            $cetak.=$nor.str_repeat("-",73).CHR(15)."\n";
            $cetak.=(CHR(18).$nor.str_repeat(" ",13)."jumlah pindahan .......                            ".number_format($total)."\n");
          }
        }
########
      }
########
      if($jmlbrs>40){
        if( ($i%40)>25){
          $cetak.=(CHR(18).$nor.str_repeat('-',73)."\n");
          $cetak.=($nor.str_repeat(" ",13)."jumlah Dipindahkan .......                         ".number_format($total)."\n");
          $ipp->setFormFeed();
          $ipp->setdata($cetak);
          $ipp->printJob();
          $cetak='';
          $ipp->unsetFormFeed();
          $cetak.=CHR(18)."\n";
          $cetak.=$nor.CHR(27).CHR(71).NmPerusahaan."                ".CHR(27).CHR(72).PajakBBK.CHR(27).CHR(71)."\n";
          $cetak.=$nor.AlmtPerusahaan."\n";
          $cetak.=$nor.KotaPerusahaan."\n";
          $cetak.=$nor."Telp. ".TlpPajak."\n";
          $cetak.=$nor."NPWP : ".NPWPPerusahaan.CHR(27).CHR(72)."                NPWP : 00.000.000.0.000.000\n\n";
          $cetak.=$nor."                          NO. DOK/FAKTUR : 0000".substr($row->d_pajak,5,2)."/".substr($row->i_faktur_komersial,8,6)."\n\n";
          $cetak.=$nor.str_repeat("=",73)."\n";
          $cetak.=$nor."NO. KD-BARANG       NAMA BARANG                   UNIT    HARGA    JUMLAH\n";
          $cetak.=$nor.str_repeat("-",73).CHR(15)."\n";
          $cetak.=(CHR(18).$nor.str_repeat(" ",13)."jumlah pindahan .......                            ".number_format($total)."\n");
        }
      }
########

      $cetak.=CHR(18).$nor.str_repeat("-",73)."\n";
      $LenNilai=strlen(number_format($total));
      $cetak.=str_repeat(" ",43)."TOTAL                  : ".str_repeat(" ",10-$LenNilai).number_format($total)."\n";
      $cetak.=str_repeat(" ",43)."POTONGAN               \n";
      $cetak.=str_repeat(" ",68).str_repeat("-",10)." -\n";
      $ndpp=round($total);
      $nppn=round($total*0.1);
      $LenNilai=strlen(number_format($ndpp));
      $cetak.=str_repeat(" ",43)."DASAR PENGENAAN PAJAK  : ".str_repeat(" ",10-$LenNilai).number_format($ndpp)."\n";
      $LenNilai=strlen(number_format($nppn));
      $cetak.=str_repeat(" ",43)."PPN 10 %               : ".str_repeat(" ",10-$LenNilai).number_format($nppn)."\n";
      $cetak.=str_repeat(" ",68).str_repeat("-",10)." -\n";
      $nf=$total+$nppn;
      $LenNilai=strlen(number_format($nf));
      $cetak.=str_repeat(" ",43)."NILAI FAKTUR           : ".str_repeat(" ",10-$LenNilai).number_format($nf)."\n";
      $cetak.=str_repeat(" ",68).str_repeat("=",10).CHR(15)."\n\n";
      $bilangan = new Terbilang;
      $kata=ucwords($bilangan->eja($nf));
      $cetak.=$ab."(".$kata." Rupiah)".CHR(18)."\n\n\n";
      $tmp=explode("-",$row->d_pajak);
      $th=$tmp[0];
      $bl=$tmp[1];
      $hr=$tmp[2];
      $dpajak=$hr."/".$bl."/".$th;
      $cetak.=str_repeat(" ",45)."BANDUNG, ".$dpajak."\n";
      $cetak.=str_repeat(" ",54)."S E & O\n\n\n\n\n\n\n";
  #    $cetak.=str_repeat(" ",45).ucwords(strtolower($nama)).CHR(15)."\n";
      $cetak.=str_repeat(" ",5)."Catatan :\n";
      $bbk='';
      $y=0;
      $mm=false;
      foreach($isi as $rw){
        $bbk.=$rw->i_bbk.".";
        $y++;
        if($y%4==0){
          $cetak.=str_repeat(" ",5)."No. BBK : ".$bbk.CHR(18)."\n";
          $mm=true;
          $bbk='';
        }
      }
#      $bbk=substr($bbk,0,strlen($bbk)-1);
      if($mm){
        $cetak.=str_repeat(" ",15).$bbk.CHR(18)."\n";
      }else{
        $cetak.=str_repeat(" ",5)."No. BBK : ".$bbk.CHR(18)."\n";
      }
      $ipp->setFormFeed();
    }
  }
# echo $cetak;
  $ipp->setdata($cetak);
  $ipp->printJob();
  echo "<script>this.close();</script>";
?>
