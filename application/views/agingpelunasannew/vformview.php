<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title.' per Periode '.$dfrom.' s/d '.$dto; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'agingpelunasannew/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="18" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>" ><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>" ><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
		<th>No</th>
		<th>Customer</th>
		<th>Saldo Awal</th>
		<th>Penjualan</th>
		<th>DN</th>
		<th>Pelunasan</th>
		<th>KN</th>
		<th>Saldo Akhir</th>
		<th>0</th>		
		<th>1-7</th>
		<th>8-14</th>
		<th>15-21</th>
		<th>22-28</th>
		<th>>28</th>
		<th>Terlambat</th>
		<th>Lunas</th>
		<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
		  if($djt){
        $tmp 	= explode("-", $djt);
        $det	= $tmp[0];
        $mon	= $tmp[1];
        $yir 	= $tmp[2];
        $djtx	= $yir."/".$mon."/".$det;
      }else{
        $djtx='';
      }
      if($dfrom){
        $tmp 	= explode("-", $dfrom);
        $det	= $tmp[0];
        $mon	= $tmp[1];
        $yir 	= $tmp[2];
        $dfromx	= $yir."/".$mon."/".$det;
      }else{
        $dfromx='';
      }
      if($dto){
        $tmp 	= explode("-", $dto);
        $det	= $tmp[0];
        $mon	= $tmp[1];
        $yir 	= $tmp[2];
        $dtox	= $yir."/".$mon."/".$det;
      }else{
        $dtox='';
      }
      $i=0;
		  foreach($isi as $row){
######
        $query=$this->db->query(" select a.n_customer_toplength from tr_customer a where a.i_customer='$row->i_customer'");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $top=$tmp->n_customer_toplength;
          }
		    }
        if($top<0) $top=$top*-1;
        $topsatu=-7;
        $toplapan=-14;
        $toplimabelas=-22;
        $topduadua=-28;
        $dudet	= $this->fungsi->dateAdd("d",0,$djtx);
        $djatuhtempo=$dudet;
#		  	$sisa=number_format($row->v_sisa);
        $vsisaaman=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo = '$djatuhtempo' and v_sisa>0 and f_nota_cancel='f'
                                  and f_ttb_tolak='f' and not i_nota isnull group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisaaman=$tmp->v_sisa;
          }
        }else{
          $vsisaaman=0;
        }
        $vsisaaman=number_format($vsisaaman);
        $dudetsatu	= $this->fungsi->dateAdd("d",$topsatu,$djtx);
        $vsisasatu=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$djatuhtempo' and d_jatuh_tempo >= '$dudetsatu' 
                                  and v_sisa>0 and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisasatu=$tmp->v_sisa;
          }
        }else{
          $vsisasatu=0;
        }
        $vsisasatu=number_format($vsisasatu);
        $dudetlapan	= $this->fungsi->dateAdd("d",$toplapan,$djtx);
        $vsisalapan=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetsatu' and d_jatuh_tempo >= '$dudetlapan' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisalapan=$tmp->v_sisa;
          }
        }else{
          $vsisalapan=0;
        }
        $vsisalapan=number_format($vsisalapan);
        $dudetlimabelas	= $this->fungsi->dateAdd("d",$toplimabelas,$djtx);
        $vsisalimabelas=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetlapan' and d_jatuh_tempo >= '$dudetlimabelas' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisalimabelas=$tmp->v_sisa;
          }
        }else{
          $vsisalimabelas=0;
        }
        $vsisalimabelas=number_format($vsisalimabelas);
        $dudetduadua	= $this->fungsi->dateAdd("d",$topduadua,$djtx);
        $vsisaduadua=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetlimabelas' and d_jatuh_tempo >= '$dudetduadua' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisaduadua=$tmp->v_sisa;
          }
        }else{
          $vsisaduadua=0;
        }
        $vsisaduadua=number_format($vsisaduadua);
        $dudetdualapan	= $this->fungsi->dateAdd("d",$topduadua,$djtx);
        $vsisadualapan=0;
        $query=$this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
                                  and d_jatuh_tempo < '$dudetduadua' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
                                  group by i_customer");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisadualapan=$tmp->v_sisa;
          }
        }else{
          $vsisadualapan=0;
        }
        $vsisadualapan=number_format($vsisadualapan);
/*
        $query=$this->db->query(" select i_customer_groupar
                                  from tr_customer_groupar
                                  where i_customer='$row->i_customer'");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $groupar=$tmp->i_customer_groupar;
          }
		    }
*/
######		  
		    $akhir=$row->saldo+($row->nota+$row->dn)-($row->pl+$row->kn);
        $nama = $row->e_customer_name;
        $nama = str_replace("'","",$nama);
        $nama	= str_replace("(","tandakurungbuka",$nama);
        $nama	= str_replace("&","tandadan",$nama);
        $nama	= str_replace(")","tandakurungtutup",$nama);
        $nama = str_replace('.','tandatitik',$nama);
        $nama = str_replace(',','tandakoma',$nama);
        $nama = str_replace('/','tandagaring',$nama);
        $i++;
			  echo "<td>$i</td><td>($row->i_customer) $row->e_customer_name</td>
				  <td align=right>".number_format($row->saldo)."</td>
				  <td align=right>".number_format($row->nota)."</td>
				  <td align=right>".number_format($row->dn)."</td>
				  <td align=right>".number_format($row->pl)."</td>
				  <td align=right>".number_format($row->kn)."</td>
				  <td align=right>".number_format($akhir)."</td>
				  <td align=right>".$vsisaaman."</td>
				  <td align=right>".$vsisasatu."</td>
				  <td align=right>".$vsisalapan."</td>
				  <td align=right>".$vsisalimabelas."</td>
				  <td align=right>".$vsisaduadua."</td>
				  <td align=right>".$vsisadualapan."</td>
				  <td align=right>0</td>
				  <td align=right>0</td>
				  <td class=\"action\">";
          echo "<a href=\"javascript:yyy('".$row->i_customer."','".$nama."');\"><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
			  echo "</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php #echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(b,d,c){
    showModal("agingpelunasannew/cform/detail/"+b+"/"+c+"/"+d+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
</script>
