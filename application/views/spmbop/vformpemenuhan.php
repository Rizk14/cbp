<?php 
	echo "<h2>$page_title</h2>";
?>
<table class="maintable">
  <tr>
    <td align="left">
    <!--<?php echo form_open('spmbop/cform/update', array('id' => 'spmbform', 'name' => 'spmbform', 'onsubmit' => 'sendRequest(); return false'));?>-->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'spmbop/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">Tgl SPMB</td>
		<?php 
			$tmp=explode("-",$isi->d_spmb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspmb=$hr."-".$bl."-".$th;
		?>
		<td><input id="ispmb" name="ispmb" type="text" readonly value="<?php echo $isi->i_spmb; ?>">
			<input readonly id="dspmb" name="dspmb" onclick="showCalendar('',this,this,'','dspmb',0,20,1)" value="<?php echo $dspmb; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spmbop/cform/","#tmp")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:66px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:46px;" align="center">Jml Pesan</th>
					<th style="width:100px;" align="center">Keterangan</th>
					<th style="width:32px;" align="center" class="action">Jml Stock</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
					if($row->n_acc>0)
					{
						echo '<table class="listtable" align="center" style="width:750px;">';
							$i++;
						$pangaos=number_format($row->v_unit_price);
						$total=$row->v_unit_price*$row->n_acc;
						$total=number_format($total);
		        $ord=number_format($row->n_order);
						if($row->n_stock>=$row->n_acc)
							$stock=number_format($row->n_acc);
						else			
							$stock=number_format($row->n_stock);
							echo "<tbody>
								<tr>
				  				<td style=\"width:25px;\"><input style=\"width:25px;\" readonly type=\"text\" 
									id=\"baris$i\" name=\"baris$i\" value=\"$i\">
															<input type=\"hidden\" 
									id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
									id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
									id=\"eproductname$i\"
									name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:104px;\">
									<input readonly style=\"width:104px;\" type=\"text\" id=\"emotifname$i\"
									name=\"emotifname$i\" value=\"$row->e_product_motifname\">
									<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$pangaos\"></td>
								<td style=\"width:47px;\"><input style=\"text-align:right; width:47px;\" readonly
									type=\"text\" id=\"nacc$i\" name=\"nacc$i\" value=\"$row->n_acc\"><input type=\"hidden\" id=\"norder$i\" name=\"norder$i\" value=\"$ord\"></td>
								<td style=\"width:105px;\">
									<input style=\"text-align:left; width:105px;\" readonly
									type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\">
									<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
								<td style=\"width:46px;\"><input style=\"text-align:right; width:46px;\" readonly 
									type=\"text\" id=\"nstock$i\" name=\"nstock$i\" value=\"$stock\"></td>
								</tr>
								</tbody></table>";
					}
				}
				echo "<input type=\"hidden\" id=\"ispmbdelete\" 		name=\"ispmbdelete\" 		 value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 	 value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" value=\"\">
		     		 ";
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" <?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspmb").value!='') &&
  	 	(document.getElementById("iarea").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
</script>
