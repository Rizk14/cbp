<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'knnonretur/cform/caricustomer/'.$iarea;
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode Customer</th>
		    <th>Nama</th>
			<th>Salesman</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $nama	= str_replace("'","\'",$row->e_customer_name);
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->e_customer_address','$row->i_salesman','$row->e_salesman_name','$row->i_customer_groupar','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->e_customer_address','$row->i_salesman','$row->e_salesman_name','$row->i_customer_groupar','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3')\">$row->e_customer_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_customer','$nama','$row->e_customer_address','$row->i_salesman','$row->e_salesman_name','$row->i_customer_groupar','$row->n_customer_discount1','$row->n_customer_discount2','$row->n_customer_discount3')\">($row->i_salesman) $row->e_salesman_name</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,x,y,z)
  {
    document.getElementById("icustomer").value=a;
    document.getElementById("ecustomername").value=b;
    document.getElementById("ecustomeraddress").value=c;
    document.getElementById("isalesman").value=d;
    document.getElementById("esalesmanname").value=e;
    document.getElementById("icustomergroupar").value=f;
	  x=formatulang(x);
	  y=formatulang(y);
	  z=formatulang(z);
    document.getElementById("ncustomerdiscount1").value=x;
    document.getElementById("ncustomerdiscount2").value=y;
    document.getElementById("ncustomerdiscount3").value=z;
	  dtmp1=x;
	  dtmp2=y;
	  dtmp3=z;
	  vdis1=0;
	  vdis2=0;
	  vdis3=0;
	  if(document.getElementById("vgross").value==''){
		  vtot =0;
	  }else{
		  vtot=parseFloat(formatulang(document.getElementById("vgross").value));
	  }
	  vdis1=vdis1+((vtot*dtmp1)/100);
	  vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
	  vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
	  document.getElementById("vcustomerdiscount1").value=formatcemua(vdis1);
	  document.getElementById("vcustomerdiscount2").value=formatcemua(vdis2);
	  document.getElementById("vcustomerdiscount3").value=formatcemua(vdis3);
	  document.getElementById("vdiscount").value=formatcemua(vdis1+vdis2+vdis3);
	  kotor=formatulang(document.getElementById("vgross").value);
	  disco=formatulang(document.getElementById("vdiscount").value);
	  document.getElementById("vnetto").value=formatcemua(kotor-disco);
	  document.getElementById("vsisa").value=formatcemua(kotor-disco);
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
