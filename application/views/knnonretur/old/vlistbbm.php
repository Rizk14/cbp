<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<!--
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
-->
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">

	<?php 
		$tujuan = 'knretur/cform/caribbm/'.$iarea;
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>BBM</th>
	    <th>Pelanggan</th>
	    <th>Area</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_bbm);
			  $th=$tmp[0];
			  $bl=$tmp[1];
			  $hr=$tmp[2];
			  $row->d_bbm=$hr."-".$bl."-".$th;
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$row->e_customer_name','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','".number_format($row->v_ttb_gross)."','".number_format($row->v_ttb_discounttotal)."','".number_format($row->v_ttb_netto)."','".number_format($row->v_ttb_sisa)."','$row->d_bbm','$row->i_customer_groupar')\">$row->i_bbm</a></td>
				  <td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$row->e_customer_name','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','".number_format($row->v_ttb_gross)."','".number_format($row->v_ttb_discounttotal)."','".number_format($row->v_ttb_netto)."','".number_format($row->v_ttb_sisa)."','$row->d_bbm','$row->i_customer_groupar')\">$row->i_customer</a></td>
				  <td><a href=\"javascript:setValue('$row->i_bbm','$row->i_customer','$row->e_customer_name','$row->i_area','$row->i_salesman','$row->e_salesman_name','$row->e_area_name','$row->e_customer_address','".number_format($row->v_ttb_gross)."',".number_format($row->v_ttb_discounttotal).",'".number_format($row->v_ttb_netto)."','".number_format($row->v_ttb_sisa)."','$row->d_bbm','$row->i_customer_groupar')\">$row->e_area_name</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h,i,j,k,l,m,n)
  {
    document.getElementById("irefference").value=a;
    document.getElementById("icustomer").value=b;
    document.getElementById("ecustomername").value=c;
    document.getElementById("iarea").value=d;
    document.getElementById("isalesman").value=e;
    document.getElementById("esalesmanname").value=f;
    document.getElementById("eareaname").value=g;
    document.getElementById("ecustomeraddress").value=h;
    document.getElementById("vgross").value=i;
    document.getElementById("vdiscount").value=j;
    document.getElementById("vnetto").value=k;
    document.getElementById("vsisa").value=l;
	document.getElementById("drefference").value=m;
    document.getElementById("icustomergroupar").value=n;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
