<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'knnonretur/cform/simpan','update'=>'#pesan','type'=>'post'));?>
	<div id="masterknreturform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Kredit Nota</td>
		<td width="1%">:</td>
		<td width="37%"><input maxlength="2" name="ikn" id="ikn" value="" >
						<input readonly name="dkn" id="dkn" value="" onclick="showCalendar('',this,this,'','dkn',0,20,1)"></td>
		<td width="12%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="icustomer" id="icustomer" value="">
						<input type="hidden" name="icustomergroupar" id="icustomergroupar" value="">
						<input readonly name="ecustomername" id="ecustomername" value=""
							   onclick="view_customer();"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="iarea" id="iarea" value="">
						<input readonly name="eareaname" id="eareaname" value="" onclick="view_area();"></td>
		<td width="12%">Alamat</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ecustomeraddress" id="ecustomeraddress" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">No Refferensi</td>
		<td width="1%">:</td>
		<td width="37%"><input name="irefference" id="irefference" value="" maxlength="15">
						<input readonly name="drefference" id="drefference" value="" onclick="showCalendar('',this,this,'','drefference',0,20,1)"></td>
		<td width="12%">Salesman</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="isalesman" id="isalesman" value="">
						<input readonly name="esalesmanname" id="esalesmanname" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">Kotor</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" name="vgross" id="vgross" value="" onkeyup="hitung();"></td>
		<td width="12%">Insentif</td>
		<td width="1%">:</td>
		<td width="37%"><input type="checkbox" name="finsentif" id="finsentif" value="" onclick="insentif(this.value)">&nbsp;Masalah&nbsp;<input type="checkbox" name="fmasalah" id="fmasalah" value="" onclick="masalah(this.value)"></td>
	      </tr>
	      <tr>
		<td width="12%">Potongan</td>
		<td width="1%">:</td>
		<input type="hidden" name="ncustomerdiscount1" id="ncustomerdiscount1" value="0">
		<input type="hidden" name="ncustomerdiscount2" id="ncustomerdiscount2" value="0">
		<input type="hidden" name="ncustomerdiscount3" id="ncustomerdiscount3" value="0">
		<input type="hidden" name="vcustomerdiscount1" id="vcustomerdiscount1" value="0">
		<input type="hidden" name="vcustomerdiscount2" id="vcustomerdiscount2" value="0">
		<input type="hidden" name="vcustomerdiscount2" id="vcustomerdiscount3" value="0">
		<td width="37%"><input  style="text-align:right;" name="vdiscount" id="vdiscount" value="" onkeyup="hitung();"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">Bersih</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vnetto" id="vnetto" value=""></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value=""></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('knnonretur/cform/','#main')">
		</td>
	      </tr>
		</table>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("dkn").value=='') ||
			(document.getElementById("iarea").value=='') ||
			(document.getElementById("irefference").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{
			document.getElementById("login").hidden=true;
		}
	}
	function view_area(){
		showModal("knnonretur/cform/area/","#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
	function view_customer(){
		area=document.getElementById("iarea").value;
		if(area!=''){
			showModal("knnonretur/cform/customer/"+area+"/","#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	} 
	function view_salesman(){
		area=document.getElementById("iarea").value;
		if(area!=''){
			showModal("knnonretur/cform/salesman/"+area+"/","#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	}
	function insentif(a){
		if(a==''){
			document.getElementById("finsentif").value='on';
		}else{
			document.getElementById("finsentif").value='';
		}
	}
	function masalah(a){
		if(a==''){
			document.getElementById("fmasalah").value='on';
		}else{
			document.getElementById("fmasalah").value='';
		}
	}
	function hitung(){
		if(document.getElementById("vgross").value=='') document.getElementById("vgross").value='0';
		if(document.getElementById("vdiscount").value=='') document.getElementById("vdiscount").value='0';
		kotor=formatulang(document.getElementById("vgross").value);
		disco=formatulang(document.getElementById("vdiscount").value);
		if(parseFloat(disco)<parseFloat(kotor)){
			document.getElementById("vnetto").value=formatcemua(kotor-disco);
			document.getElementById("vsisa").value=formatcemua(kotor-disco);
		}else{
			alert("Discount tidak boleh lebih besar dari nilai kotor");
			document.getElementById("vdiscount").value="0";
			document.getElementById("vnetto").value=formatcemua(kotor);
			document.getElementById("vsisa").value=formatcemua(kotor);
		}
	}
</script>
