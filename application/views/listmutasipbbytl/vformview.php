<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'listmutasipbbytl/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
		if($isi){
			foreach($isi as $row){
				$periode=$row->e_mutasi_periode;
			}
		}else{
			$periode=$iperiode;
		}
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
    <input name="pperiode" id="pperiode" value="<?php echo $row->e_mutasi_periode; ?>" type="hidden">
<!--    <input name="cmdexport" id="cmdexport" value="Export to Excel" type="submit">-->
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>LAPORAN MUTASI STOCK AREA ".$this->session->userdata('i_area')."</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
    $rpselisih=0;
    $rpsaldoakhir=0;
    $rpstockopname=0;
		if($isi){
			foreach($isi as $row){

        $this->db->select("	i_product, v_product_retail
                          from tr_product_price
						              where i_product='$row->i_product' and i_price_group='00'");
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $row->v_product_retail=$tmp->v_product_retail;
          }
        }else{
          $row->v_product_retail=0;
        }
        $rpselisih=$rpselisih+(($row->n_saldo_stockopname-$row->n_saldo_akhir)*$row->v_product_retail);
        $rpsaldoakhir=$rpsaldoakhir+($row->n_saldo_akhir*$row->v_product_retail);
        $rpstockopname=$rpstockopname+($row->n_saldo_stockopname*$row->v_product_retail);

      }
    }
		?>
<h3>Saldo Akhir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpsaldoakhir); ?><br>
    Saldo Stock Opname <?php echo 'Rp. '.number_format($rpstockopname); ?><br>
Selisih &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo 'Rp. '.number_format($rpselisih); ?><h3>

    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Kode</th>
	   	    <th>Nama</th>
			    <th>Saldo Awal</th>
			    <th>Dari Pusat</th>
          <th>Dari Lang</th>
          <th>Ke Lang</th>
			    <th>Penjualan</th>
			    <th>ke Pusat</th>
			    <th>Sld Akhir</th>
			    <th>Sld Opname</th>
			    <th>Selisih</th>
        	<th class="action">Action</th>
	    <tbody>
    <?php 
		if($isi){
			$gtawal        = 0;
			$gtdaripusat   = 0;
			$gtdarilang    = 0;
			$gtkelang      = 0;
			$gtpenjualan   = 0;
			$gtkepusat     = 0;
			$gtakhir       = 0;
			$gtstockopname = 0;
			$gtselisih     = 0;
      $tawal         = 0;
	    $tdaripusat    = 0;
	    $tdarilang     = 0;
	    $tkelang       = 0;
	    $tpenjualan    = 0;
	    $tkepusat      = 0;
	    $takhir        = 0;
	    $tstockopname  = 0;
      $tselisih      = 0;
      $i=1;
      $selisih=0;
		if($iarea=='00'||$iarea=='PB'){
    $iarea='';
    foreach($isi as $row){
    if($iarea=='' || $iarea==$row->i_area){
      if($iarea==''){
          echo "<tr><th colspan='13'>AREA ".$row->e_area_name."</th></tr>";
      }
      $selisih=($row->n_saldo_stockopname)-$row->n_saldo_akhir;
      echo "
        <tr>
        <td align=right>$i</td>
        <td>$row->i_product</td>
        <td>$row->e_product_name</td>
        <td align=right>$row->n_saldo_awal</td>
        <td align=right>$row->n_mutasi_daripusat</td>
        <td align=right>$row->n_mutasi_darilang</td>
        <td align=right>$row->n_mutasi_kelang</td>
        <td align=right>$row->n_mutasi_penjualan</td>
        <td align=right>$row->n_mutasi_kepusat</td>
        <td align=right>$row->n_saldo_akhir</td>
        <td align=right>$row->n_saldo_stockopname</td>
        <td align=right>$selisih</td>
        <td class=\"action\"><a href=\"#\" onclick='show(\"listmutasipbbytl/cform/detail/$iperiode/$row->i_product/$row->n_saldo_awal/$row->i_area/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a></td>";
      echo "</tr>";
      $tawal        = $tawal+$row->n_saldo_awal;
	    $tdaripusat   = $tdaripusat+$row->n_mutasi_daripusat;
	    $tdarilang    = $tdarilang+$row->n_mutasi_darilang;
	    $tkelang      = $tkelang+$row->n_mutasi_kelang;
	    $tpenjualan   = $tpenjualan+$row->n_mutasi_penjualan;
	    $tkepusat     = $tkepusat+$row->n_mutasi_kepusat;
	    $takhir       = $takhir+$row->n_saldo_akhir;
	    $tstockopname = $tstockopname+$row->n_saldo_stockopname;
      $tselisih     = $tselisih+$selisih;
      $tareaname    = $row->e_area_name;
      }elseif($iarea!='' || $iarea!=$row->i_area){
      echo "<tr>
        <td align='center' colspan='3'>TOTAL AREA ".$tareaname."</td>
        <td align=right>$tawal</td>
        <td align=right>$tdaripusat</td>
        <td align=right>$tdarilang</td>
        <td align=right>$tkelang</td>
        <td align=right>$tpenjualan</td>
        <td align=right>$tkepusat</td>
        <td align=right>$takhir</td>
        <td align=right>$tstockopname</td>
        <td align=right>$tselisih</td>
        <td></td>
      ";
      echo "</tr>";
			$gtawal        = $gtawal+$tawal;
			$gtdaripusat   = $gtdaripusat+$tdaripusat;
			$gtdarilang    = $gtdarilang+$tdarilang;
			$gtkelang      = $gtkelang+$tkelang;
			$gtpenjualan   = $gtpenjualan+$tpenjualan;
			$gtkepusat     = $gtkepusat+$tkepusat;
			$gtakhir       = $gtakhir+$takhir;
			$gtstockopname = $gtstockopname+$tstockopname;
			$gtselisih     = $gtselisih+$tselisih;
			
      $tawal        = 0;
	    $tdaripusat   = 0;
	    $tdarilang    = 0;
	    $tkelang      = 0;
	    $tpenjualan   = 0;
	    $tkepusat     = 0;
	    $takhir       = 0;
	    $tstockopname = 0;
      $tselisih     = 0;
      $i=1;
      
      $selisih=($row->n_saldo_stockopname)-$row->n_saldo_akhir;
      echo "
      <tr><th colspan='13'>AREA ".$row->e_area_name."</th></tr>
      <tr>
        <td align=right>$i</td>
        <td>$row->i_product</td>
        <td>$row->e_product_name</td>
        <td align=right>$row->n_saldo_awal</td>
        <td align=right>$row->n_mutasi_daripusat</td>
        <td align=right>$row->n_mutasi_darilang</td>
        <td align=right>$row->n_mutasi_kelang</td>
        <td align=right>$row->n_mutasi_penjualan</td>
        <td align=right>$row->n_mutasi_kepusat</td>
        <td align=right>$row->n_saldo_akhir</td>
        <td align=right>$row->n_saldo_stockopname</td>
        <td align=right>$selisih</td>
        <td class=\"action\"><a href=\"#\" onclick='show(\"listmutasipbbytl/cform/detail/$iperiode/$row->i_product/$row->n_saldo_awal/$row->i_area/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a></td>";
      echo "</tr>";
      $tawal        = $tawal+$row->n_saldo_awal;
	    $tdaripusat   = $tdaripusat+$row->n_mutasi_daripusat;
	    $tdarilang    = $tdarilang+$row->n_mutasi_darilang;
	    $tkelang      = $tkelang+$row->n_mutasi_kelang;
	    $tpenjualan   = $tpenjualan+$row->n_mutasi_penjualan;
	    $tkepusat     = $tkepusat+$row->n_mutasi_kepusat;
	    $takhir       = $takhir+$row->n_saldo_akhir;
	    $tstockopname = $tstockopname+$row->n_saldo_stockopname;
      $tselisih     = $tselisih+$selisih;
      
      
      }
      $i++;
      $iarea=$row->i_area;
	    }
		}else{
		$sareanya = $this->session->userdata('i_area');
			foreach($isi as $row){
        $selisih=($row->n_saldo_stockopname)-$row->n_saldo_akhir;
		      echo "<tr>
            <td align=right>$i</td>
            <td>$row->i_product</td>
            <td>$row->e_product_name</td>
				    <td align=right>$row->n_saldo_awal</td>
				    <td align=right>$row->n_mutasi_daripusat</td>
				    <td align=right>$row->n_mutasi_darilang</td>
				    <td align=right>$row->n_mutasi_kelang</td>
				    <td align=right>$row->n_mutasi_penjualan</td>
				    <td align=right>$row->n_mutasi_kepusat</td>
            <td align=right>$row->n_saldo_akhir</td>
				    <td align=right>$row->n_saldo_stockopname</td>
				    <td class=\"action\"><a href=\"#\" onclick='show(\"listmutasipbbytl/cform/detail/$iperiode/$row->i_product/$row->n_saldo_awal/$areanya/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a></td>";
        $i++;
				echo "</tr>";
				$tawal        = $tawal+$row->n_saldo_awal;
				$tdaripusat   = $tdaripusat+$row->n_mutasi_daripusat;
				$tdarilang    = $tdarilang+$row->n_mutasi_darilang;
				$tkelang      = $tkelang+$row->n_mutasi_kelang;
				$tpenjualan   = $tpenjualan+$row->n_mutasi_penjualan;
				$tkepusat     = $tkepusat+$row->n_mutasi_kepusat;
				$takhir       = $takhir+$row->n_saldo_akhir;
				$tstockopname = $tstockopname+$row->n_saldo_stockopname;
				$tselisih     = $tselisih+$selisih;
			}
    }
      echo "<tr>
        <td align='center' colspan='3'>TOTAL AREA ".$row->e_area_name."</td>
        <td align=right>$tawal</td>
        <td align=right>$tdaripusat</td>
        <td align=right>$tdarilang</td>
        <td align=right>$tkelang</td>
        <td align=right>$tpenjualan</td>
        <td align=right>$tkepusat</td>
        <td align=right>$takhir</td>
        <td align=right>$tstockopname</td>
        <td align=right>$tselisih</td>
        <td></td>
        </tr>
      ";
      if($iarea=='00' || $iarea='PB'){
      echo "<tr>
        <td align='center' colspan='3'>GRAND TOTAL ".$row->e_area_name."</td>
        <td align=right>$gtawal</td>
        <td align=right>$gtdaripusat</td>
        <td align=right>$gtdarilang</td>
        <td align=right>$gtkelang</td>
        <td align=right>$gtpenjualan</td>
        <td align=right>$gtkepusat</td>
        <td align=right>$gtakhir</td>
        <td align=right>$gtstockopname</td>
        <td align=right>$gtselisih</td>
        <td></td>
        </tr>
      ";      
      }
		}
	      ?>
	    </tbody>
	  </table>
    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("listmutasipbbytl/cform/index","#main");' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listmutasipbbytl/cform/viewdetail";
	  formna.submit();
  }
</script>
