<table class="maintable">
  <tr>
    <td align="left">
    <?php echo form_open('spmbapprovefa/cform/update', array('id' => 'spmbform', 'name' => 'spmbform', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="15%">Tgl SPMB</td>
		<?php 
			$tmp=explode("-",$isi->d_spmb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspmb=$hr."-".$bl."-".$th;
		?>
		<td><input id="ispmb" name="ispmb" type="text" readonly value="<?php echo $isi->i_spmb; ?>">
			<input readonly id="dspmb" name="dspmb" onclick="showCalendar('',this,this,'','dspmb',0,20,1)" value="<?php echo $dspmb; ?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Keterangan (sales)</td>
		<td><input readonly id="eapprove1" name="eapprove1" value="<?php echo $isi->e_approve1; ?>"></td>
	      </tr>
	      <tr>
		<td width="15%">Keterangan</td>
		<td><input id="eapprove2" name="eapprove2" ></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Approve" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spmbapprovefa/cform/","#tmp")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:46px;" align="center">Jml Pesan</th>
          <th style="width:46px;" align="center">Jml Acc</th>
					<th style="width:100px;" align="center">Keterangan</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
			  	$i++;
					$pangaos=number_format($row->v_unit_price,2);
					$total=$row->v_unit_price*$row->n_order;
					$total=number_format($total,2);
				  	echo "<table class=\"listtable\" align=\"center\" style=\"width:750px;\"><tbody>
							<tr>
		    				<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
													  <input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:103px;\">
								<input readonly style=\"width:103px;\" type=\"text\" id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\">
								<input type=\"hidden\" id=\"vproductmill$i\" name=\"vproductmill$i\" value=\"$pangaos\"></td>
							<td style=\"width:47px;\"><input readonly style=\"text-align:right; width:47px;\"
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_order\"></td>
							<td style=\"width:47px;\"><input style=\"text-align:right; width:47px;\" readonly
								type=\"text\" id=\"nacc$i\" name=\"nacc$i\" value=\"$row->n_acc\"></td>
							<td style=\"width:104px;\">
								<input style=\"text-align:left; width:104px;\"
								type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_remark\" readonly>
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td></tr>
						  </tbody></table>";
				}
				echo "<input type=\"hidden\" id=\"ispmbdelete\" 		name=\"ispmbdelete\" 		 value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 	 value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" value=\"\">
		     		 ";
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" <?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	  </div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,g){
    if (confirm(g)==1){
	  document.getElementById("ispmbdelete").value=a;
	  document.getElementById("iproductdelete").value=b;
	  document.getElementById("iproductgradedelete").value=c;
	  document.getElementById("iproductmotifdelete").value=d;
	  formna=document.getElementById("spmbform");
	  formna.action="<?php echo site_url(); ?>"+"/spmb/cform/deletedetail";
	  formna.submit();
    }
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
		so_inner+= '<tr><th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		so_inner+= '<th style="width:100px;" align="center">Motif</th>';
		so_inner+= '<th style="width:46px;"  align="center">Jml Pesan</th>';
		so_inner+= '<th style="width:100px;"  align="center">Keterangan</th>';
		so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<tbody><tr><td style="width:27px;"><input style="width:27px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:65px;"><input style="width:65px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:316px;"><input style="width:316px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';
		si_inner+='<td style="width:106px;"><input style="width:106px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:40px;">&nbsp;</td></tr></tbody>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<tbody><tr><td style="width:27px;"><input style="width:27px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:65px;"><input style="width:65px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:316px;"><input style="width:316px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:105px;"><input readonly style="width:105px;"  type="text" id="emotifname'+a+'" name="emotifname'+a+'" value="">';
		si_inner+='<input type="hidden" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:50px;"><input style="text-align:right; width:50px;" type="text" id="norder'+a+'" name="norder'+a+'" value="" onkeyup="hitungnilai('+a+');">';
		si_inner+='<input type="hidden" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td>';
		si_inner+='<td style="width:106px;"><input style="width:106px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td>';
		si_inner+='<td style="width:40px;">&nbsp;</td></tr></tbody>';
    }
    j=0;
    var baris			=Array()
    var iproduct		=Array();
    var eproductname	=Array();
	var vproductmill	=Array();
    var norder			=Array();
	var motif			=Array();
	var motifname		=Array();
    var vtotal			=Array();
    for(i=1;i<a;i++){
	j++;
	baris[j]			=document.getElementById("baris"+i).value;
	iproduct[j]			=document.getElementById("iproduct"+i).value;
	eproductname[j]		=document.getElementById("eproductname"+i).value;
	vproductmill[j]		=document.getElementById("vproductmill"+i).value;
	norder[j]			=document.getElementById("norder"+i).value;
	motif[j]			=document.getElementById("motif"+i).value;
	motifname[j]		=document.getElementById("emotifname"+i).value;
	vtotal[j]			=document.getElementById("vtotal"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vproductmill"+i).value=vproductmill[j];
		document.getElementById("norder"+i).value=norder[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("emotifname"+i).value=motifname[j];
		document.getElementById("vtotal"+i).value=vtotal[j];	
    }
    lebar =500;
    tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/spmb/cform/product/"+a,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspmb").value!='') &&
  	 	(document.getElementById("iarea").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").hidden=true;
    	}else{
		   	document.getElementById("login").hidden=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").hidden=false;
  }
  function view_area(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/spmb/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function hitungnilai(brs){
	ord=document.getElementById("norder"+brs).value;
	if (isNaN(parseFloat(ord))){
		alert("Input harus numerik");
	}else{
		hrg=formatulang(document.getElementById("vproductmill"+brs).value);
		qty=formatulang(ord);
		vhrg=parseFloat(hrg)*parseFloat(qty);
		document.getElementById("vtotal"+brs).value=formatcemua(vhrg);
	}
  }
  function diskonrupiah(isi){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot   =parseFloat(formatulang(document.getElementById("vspmb").value));
		vtotdis=parseFloat(formatulang(isi));
		vtotbersih=vtot-vtotdis;
		document.getElementById("vspmbbersih").value=formatcemua(vtotbersih);
	}
  }
</script>
