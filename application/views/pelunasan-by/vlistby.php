<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		$tujuan = 'pelunasan-by/cform/caribiaya';
	  echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));
	?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="5" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="icustomer" name="icustomer" value="<?php echo $icustomer; ?>">&nbsp;<input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>"><input type="hidden" id="dbukti" name="dbukti" value="<?php echo $dbukti; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
   	    <th>No Biaya</th>
	    	<th>Tgl</th>
	    	<th>Keterangan</th>
	    	<th>Jumlah</th>
	    	<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_by);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_by=$dd."-".$mm."-".$yy;
			  $row->v_by=number_format($row->v_by);
			  $row->v_sisa=number_format($row->v_sisa);
			  echo "<tr> 
			  <td><a href=\"javascript:setValue('$row->i_by','$row->v_by','$row->v_sisa','$row->d_by','$row->e_remark','$row->i_coa')\">$row->i_by</a></td>
			  <td><a href=\"javascript:setValue('$row->i_by','$row->v_by','$row->v_sisa','$row->d_by','$row->e_remark','$row->i_coa')\">$row->d_by</a></td>
			  <td><a href=\"javascript:setValue('$row->i_by','$row->v_by','$row->v_sisa','$row->d_by','$row->e_remark','$row->i_coa')\">$row->e_remark</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_by','$row->v_by','$row->v_sisa','$row->d_by','$row->e_remark','$row->i_coa')\">$row->v_by</a></td>
			  <td align=right><a href=\"javascript:setValue('$row->i_by','$row->v_by','$row->v_sisa','$row->d_by','$row->e_remark','$row->i_coa')\">$row->v_sisa</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f)
  {
    document.getElementById("igiro").value=a;
   	document.getElementById("dcair").value=d;
   	document.getElementById("dcairx").value=d;
  	document.getElementById("dgiro").value=d;
  	document.getElementById("egirobank").value='';
    document.getElementById("vjumlah").value=b;
    document.getElementById("vlebih").value=b;
    document.getElementById("icoa").value=f;
    document.getElementById("ibank").value='';
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
