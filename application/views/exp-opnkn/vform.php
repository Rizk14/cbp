<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<!-- <#?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/export', 'update' => '#main', 'type' => 'post')); ?> -->
			<div class="expkkform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="9%">s/d Tanggal</td>
								<td width="1%">:</td>
								<td width="90%">
									<input readonly id="dateto" name="dateto" value="<?= date("d-m-Y") ?>" onclick="showCalendar('',this,this,'','dateto',0,20,1)">
								</td>
							</tr>
							<tr>
								<td width="9%">Area</td>
								<td width="1%">:</td>
								<td width="90%">
									<input type="hidden" id="iarea" name="iarea" value="">
									<input type="text" id="eareaname" name="eareaname" value="" onclick='showModal("<?= $folder ?>/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							</tr>
							<tr>
								<td width="9%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="90%">
									<a href="#" id="href" value="Download" target="blank" onclick="return exportexcel();"><button>Download</button></a>
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick='show("<?= $folder ?>/cform/","#main")'>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function exportexcel() {
		var dto = document.getElementById('dateto').value;
		var iarea = document.getElementById('iarea').value;

		if (iarea == "") {
			alert("Area Wajib diisi!!");
			return false;
		} else {
			var abc = "<?= site_url($folder . '/cform/export/'); ?>" + iarea + "/" + dto;

			$("#href").attr("href", abc);
			return true;
		}
	}
</script>