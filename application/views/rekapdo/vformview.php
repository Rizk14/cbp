<h2><?php echo $page_title; ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php 
			$tujuan = 'rekapdo/cform/rekap';
			?>
			<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#pesan','type'=>'post'));?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<th>No</th>
							<th>Kode Supplier</th>
							<th>Area</th>
							<th>SPMB</th>
							<th>Tanggal DO</th>
							<th>No DO</th>
							<th>No OP</th>
							<th class="action">Act</th>
							<tbody>
								<?php 
									$no = 0;
								if($isi){
									foreach($isi as $row){
										$no++;
										echo "<tr>
										<td>$no</td>
										<td>$row->i_supplier</td>
										<td>$row->i_area</td>
										<td>$row->i_spmb</td>
										<td>$row->d_do</td>
										<td>$row->i_do</td>
										<td>$row->i_op</td>
										<td align='center'>
											<input type='checkbox' name='chk".$no."' id='chk".$no."' value='' onclick='pilihan(this.value,".$no.")'>
											<input type='hidden' name='i_supplier".$no."' id='i_supplier".$no."' value='$row->i_supplier'>
											<input type='hidden' name='i_area".$no."' id='i_area".$no."' value='$row->i_area'>
											<input type='hidden' name='i_spmb".$no."' id='i_spmb".$no."' value='$row->i_spmb'>
											<input type='hidden' name='d_do".$no."' id='d_do".$no."' value='$row->d_do'>
											<input type='hidden' name='i_do".$no."' id='i_do".$no."' value='$row->i_do'>
											<input type='hidden' name='i_op".$no."' id='i_op".$no."' value='$row->i_op'>
											<input type='hidden' name='i_store".$no."' id='i_store".$no."' value='$row->i_store'>
											<input type='hidden' name='i_store_location".$no."' id='i_store_location".$no."' value='$row->i_store_location'>
											<input type='hidden' name='d_spmb".$no."' id='d_spmb".$no."' value='$row->d_spmb'>
											<input type='hidden' name='i_spmb_old".$no."' id='i_spmb_old".$no."' value='$row->i_spmb_old'>
										</td>
									</tr>";
									
								}
							} ?>
							<tr>
								<td colspan='10' align='center'><input type='submit' id='transfer' name='transfer' value='Rekap' onclick="return dipales(parseFloat(document.getElementById('jml').value));"></td>
								<?php if($no){
									echo"</tr>
								<input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$no\">";
								}
								?>
							</tbody>
						</table>
						<?php //echo "<center>".$this->pagination->create_links()."</center>";?>
					</div>
				</div>
			</div>
			<?=form_close()?>
			<table class="listtable">
				<tr>
					<td align="center">
						<input type="button" id="pilihsemua" name="pilihsemua" value="Pilih Semua" 
						onclick="pilihsemua()">
						<input type="button" id="tidakpilihsemua" name="tidakpilihsemua" value="Tidak Semua"
						onclick="tidakpilihsemua()">
						<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('rekapdo/cform/','#main')">
					</td>
				</tr>
			</table>
			<div id="pesan"></div>
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function pilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=true;
			document.getElementById("chk"+i).value='on';
		}
	}
	function tidakpilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=false;
			document.getElementById("chk"+i).value='';
		}
	}
	function pilihan(a,b){
		if(a==''){
			document.getElementById("chk"+b).value='on';
		}else{
			document.getElementById("chk"+b).value='';
		}
	}
	function dipales(a){
		var b = 0;
		for(i=1;i<=a;i++){
			if(document.getElementById("chk"+i).value=='on') {
				b = b + 1;
		}
	}
	if(b > 0){
		document.getElementById("transfer").disabled=true;
	}else{
		alert("Pilih Salah Satu Do !!!");
		return false;
	}
}

</script>
