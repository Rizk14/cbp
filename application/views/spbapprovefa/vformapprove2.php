<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <!--	<link href="<?php echo base_url() . 'assets/css/bootstrap.css'; ?>" rel="stylesheet">
	<link href="<?php echo base_url() . 'assets/themes/default/css/bootstrap.min.css'; ?>" rel="stylesheet"> -->
  <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body>
  <div id="main">
    <div id="tmpx">
      <h2>Informasi</h2>
      <table class="maintable">
        <tr>
          <td align="left">
            <?php echo $this->pquery->form_remote_tag(array('url' => 'spbapprovefa/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
            <div id="aktkkform">
              <div class="effect">
                <div class="accordion2">
                  <?php
                  if ($isi->d_signin) {
                    $tmp           = explode("-", $isi->d_signin);
                    $th            = $tmp[0];
                    $bl            = $tmp[1];
                    $hr            = $tmp[2];
                    $isi->d_signin = $hr . '-' . $bl . '-' . $th;
                  }
                  ?>
                  <table class="maintable">
                    <tr>
                      <td align="left">
                        <table class="mastertable" border="0">
                          <input hidden id="ispb" name="ispb" value="<?php echo $ispb; ?>" readonly>
                          <input hidden id="icustomer" name="icustomer" value="<?php echo $icustomer; ?>" readonly>
                          <input hidden id="dterima" name="dterima" value="<?php echo $dterima; ?>" readonly>
                          <input hidden id="terima" name="terima" value="<?php echo $terima; ?>" readonly>
                          <input hidden id="etelat" name="etelat" value="<?php echo $etelat; ?>" readonly>
                          <input hidden id="etelat2" name="etelat2" value="<?php echo $etelat2; ?>" readonly>
                          <input hidden id="iarea" name="iarea" value="<?php echo $iarea; ?>" readonly>

                          <tr>
                            <td width="12%">Pelanggan</td>
                            <td style="width:20%">
                              <input style="width:190px" readonly type="text" value="<?php echo $isi->i_customer . " - " . $isi->e_customer_name; ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr><!-- END tr baris 1 -->

                          <tr>
                            <td width="12%">Tanggal Daftar</td>
                            <td style="width:20%">
                              <input style="width:80px" readonly type="text" value="<?php echo $isi->d_signin; ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr><!-- END tr baris 1 -->

                          <tr>
                            <td width="12%">Plafon</td>
                            <td style="width:20%">
                              <input style="width:100px" readonly value="<?php echo number_format($isi->v_flapond); ?>">
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr>

                          <tr>
                            <td width="15%">Rata-rata Keterlambatan</td>
                            <td style="width:20%">
                              <?php
                              if ($isi->n_ratatelat == '' || $isi->n_ratatelat == NULL) {
                                echo "<input style = 'width:25px' readonly value='0'>";
                              } else {
                                echo "<input style = 'width:25px' readonly value='$isi->n_ratatelat'>";
                              }; ?> Hari
                            </td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr>

                          <tr>
                            <td width="15%">Piutang</td>
                            <td style="width:20%" class="huruf nmper">
                              <input style='width:190px' type="text" value="Rp <?php echo number_format($saldopiutang); ?>" readonly>
                            </td>
                          </tr>

                          <tr>
                            <td width="15%">Penjualan</td>
                            <?php
                            $per = substr($isi->i_spb, 4, 4);
                            $perth = substr($per, 0, 2);
                            $perbl = substr($per, 2, 2);
                            for ($q = 1; $q <= 6; $q++) {
                              settype($perth, "integer");
                              settype($perbl, "integer");
                              $perbl = $perbl - 1;
                              if ($perbl == 0) {
                                $perbl = 12;
                                $perth = $perth - 1;
                              }
                              settype($perth, "string");
                              settype($perbl, "string");
                              $a = strlen($perth);
                              while ($a < 2) {
                                $perth = "0" . $perth;
                                $a = strlen($perth);
                              }
                              $a = strlen($perbl);
                              while ($a < 2) {
                                $perbl = "0" . $perbl;
                                $a = strlen($perbl);
                              }
                              $isi->i_area = substr($isi->i_customer, 0, 2);
                              $nota = 'FP-' . $perth . $perbl . '-' . $isi->i_area . '%';
                              $tesi = 0;
                              $this->db->select(" sum(v_nota_netto) as total, substring(i_nota,1,10) as no from tm_nota
                                      where i_nota like '$nota' and i_customer='$isi->i_customer' 
                                      and f_nota_cancel='f' group by no", false);
                              $query = $this->db->get();
                              if ($query->num_rows() > 0) {
                                foreach ($query->result() as $tes) {
                                  $tesi = $tes->total;
                                }
                              }
                              //====================hitungrataratabayarhari================================//

                              $this->db->select(" round(totalharibyr/totalalokasi) as ratarata from(
                                    select sum(haribayar) as totalharibyr, sum(bykalokasi) as totalalokasi from(
                                    select distinct a.total, a.no, a.i_alokasi, a.haribayar, count(a.i_alokasi) as bykalokasi
                                    from(
                                    SELECT distinct sum(d.v_nota_netto) as total, substring(d.i_nota, 1, 10) as no, c.i_alokasi, (c.d_alokasi)-(d.d_nota) as haribayar, 0 as bykalokasi
                                    from tm_nota_item a, tm_alokasi_item b, tm_alokasi c, tm_nota d 
                                    where a.i_nota=b.i_nota and b.i_alokasi=c.i_alokasi and a.i_nota=d.i_nota and c.f_alokasi_cancel='f' 
                                    and d.i_nota like '%$nota%' and d.i_customer='$isi->i_customer' group by d.v_nota_netto, d.i_nota, c.i_alokasi, c.d_alokasi, d.d_nota) as a
                                    group by a.total, a.no, a.i_alokasi, a.haribayar) as b
                                    group by b.i_alokasi) as c", false);

                              $query = $this->db->get();
                              if ($query->num_rows() > 0) {
                                foreach ($query->result() as $tes2) {
                                  $ratarata = $tes2->ratarata;
                                }
                              } else {
                                $ratarata = "0";
                              }
                              //$ratarata="-";
                              $peri = substr(mbulan($perbl), 0, 3) . '-20' . $perth;
                              $spasi    = 12;
                              $pjg  = strlen(number_format($tesi));
                              $tesi = number_format($tesi);
                              for ($xx = 1; $xx <= $pjg; $xx++) {
                                $spasi = $spasi - 1;
                              }
                              $spasi = str_repeat(" ", $spasi);

                              switch ($q) {
                                case 1:
                                  echo "<td><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'>";
                                  break;
                                case 2:
                                  echo "<td><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'>";
                                  break;
                                case 3:
                                  echo "<td><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'></tr>";
                                  break;
                                case 4:
                                  echo "<tr><td width=\"120\"></td><td width=\"120\"><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'>";
                                  break;
                                case 5:
                                  echo "<td width=\"120\"><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'>";
                                  break;
                                case 6:
                                  echo "<td width=\"120\"><input readonly type = 'text' value = '$peri --> $tesi ($ratarata/Hari)'></tr>";
                                  break;
                              }
                            }
                            ?>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                            <td style="width:20%"></td>
                          </tr>
                        </table><br>
                        <center>
                          <input name="login" id="login" value="Approve" type="submit" onclick="return dipales();" <?php if ($isi->i_approve2 != "") {
                                                                                                                      echo "disabled";
                                                                                                                    } ?>>&nbsp;&nbsp;&nbsp;
                          <input type="button" id="batal" name="batal" value="Kembali" onclick="bbatal()">&nbsp;&nbsp;
                          <input name="balik" id="balik" style="display: none;" value="Keluar" type="button" onclick='show("spbapprovefa/cform/","#tmp");bbatal();'>
                        </center>
                        <!-- <center>
            
            <br><br>
	    			<input name="login" id="login" value="Simpan" type="submit" onclick="return dipales();">&nbsp;&nbsp;
            <input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"> 
	    		</center> -->
                        <div id="pesan"></div>
                        <?= form_close() ?>
                      </td>
                    </tr>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>
<script language="javascript" type="text/javascript">
  function dipales() {
    $s = 0;
    var str = document.getElementById("etelat2").value;
    var etelat2 = str.replace(/\s+/g, '');
    var show = document.getElementById('balik');

    if (document.getElementById("dterima").value == '' || document.getElementById("dterima").value == null || document.getElementById("dterima").value == 'null') {
      alert('Tanggal Approve Keuangan belum diisi..!!');
      return false;
    } else if ((document.getElementById("terima").value > 2) && (document.getElementById("etelat").value == '') && (etelat2 == '')) {
      alert('Keterangan Terlambat Belum diisi!');
      $s = 1;
      return false;
    } else {
      document.getElementById("login").hidden = true;
      if (show.style.display === 'none') {
        show.style.display = 'block';
      }
    }
  }

  function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
  }
</script>