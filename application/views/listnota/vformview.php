<div id='tmp'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => 'listnota/cform/cari', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value=""><input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>"><input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>"><input type="hidden" id="iarea" name="iarea" value="<?php echo $iarea; ?>">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
								</tr>
							</thead>
							<th>No Nota</th>
							<th>Tgl Nota</th>
							<th>No SPB</th>
							<th>Tgl SPB</th>
							<th>Customer</th>
							<th>Nilai</th>
							<th>Sisa</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$discount = number_format($row->v_nota_discounttotal);
										$nilai = number_format($row->v_nota_netto);
										$sisa = number_format($row->v_sisa);
										$tmp = explode('-', $row->d_nota);
										$tgl = $tmp[2];
										$bln = $tmp[1];
										$thn = $tmp[0];
										$row->d_nota = $tgl . '-' . $bln . '-' . $thn;
										$tmp = explode('-', $row->d_spb);
										$tgl = $tmp[2];
										$bln = $tmp[1];
										$thn = $tmp[0];
										$row->d_spb = $tgl . '-' . $bln . '-' . $thn;
										echo "<tr>";
										if ($row->f_nota_cancel == 't') {
											echo "<td><h1>$row->i_nota</h1></td>";
										} else {
											echo "<td>$row->i_nota</td>";
										}
										echo "<td>$row->d_nota</td>
				  <td>$row->i_spb</td>
				  <td>$row->d_spb</td>";
										#				  <td>$row->i_nota_old</td>
										#				  <td>$row->i_area</td>
										echo "<td>($row->i_customer) $row->e_customer_name</td>";
										#				  <td align=right>$discount</td>
										echo "<td align=right>$nilai</td>
												<td align=right>$sisa</td>
												<td class=\"action\">";

										/* Lvl 4 & Dpt 4 = Finance Manager, Lvl 4 & Dpt 8 = FIS Manager, Lvl 0 = Administrator, Lvl 3 = GM */
										if (
											$row->n_print > 0 && (($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '4') ||
												($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '8') ||
												$this->session->userdata('level') == '0' || ($this->session->userdata('level') == '3'))
										) {
											echo	"	<a href=\"#\" onclick='reprint(\"listnota/cform/undo/$row->i_nota/$row->i_area/$dfrom/$dto/$row->i_sj/$iarea/\",\"#main\")' title=\"Cancel Print (Cetak Ulang)\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/no-printer.png\" border=\"0\" alt=\"Cancel Print\">
														</a>";
										} else {
											if ((($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '4') ||
												($this->session->userdata('level') == '4' && $this->session->userdata('departement') == '8') ||
												$this->session->userdata('level') == '0' || $this->session->userdata('level') == '3')) {
												echo	"	<a href=\"javascript:print('" . $row->i_nota . "');\">
																<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/print.gif\" border=\"0\" alt=\"Print\">
															</a>";
											}
										}

										echo	"	<a href=\"#\" onclick='show(\"nota/cform/edit/$row->i_nota/$row->i_spb/$row->i_area/$dfrom/$dto/$row->i_sj/\",\"#main\")' title=\"Edit\">
																<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\">
															</a>";
										if ($row->f_nota_cancel == 'f') {
											if (
												$this->session->userdata('departement') == '4' &&
												($this->session->userdata('level') == '5' or $this->session->userdata('level') == '4') &&
												(($row->i_approve_pajak != null && $row->i_faktur_komersial != null) || ($row->i_faktur_komersial == null || $row->i_faktur_komersial == '')) && ($row->v_nota_netto == $row->v_sisa)
											) {
												echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick='hapus(\"listnota/cform/delete/$row->i_nota/$row->i_spb/$row->i_area/$dfrom/$dto/\",\"#main\")' title=\"Cancel Nota\">
																<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
															</a>";
											}
										}
										echo "</td></tr>";
									}
									echo "<input type=\"hidden\" id=\"ispbdelete\" name=\"ispbdelete\" value=\"\">
											<input type=\"hidden\" id=\"ispbedit\" name=\"ispbedit\" value=\"\">
											<input type=\"hidden\" id=\"inotadelete\" name=\"inotadelete\" value=\"\">
											<input type=\"hidden\" id=\"inotaedit\" name=\"inotaedit\" value=\"\">";
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
</div>
<script language="javascript" type="text/javascript">
	function xxx(x, a, g) {
		if (confirm(g) == 1) {
			document.getElementById("ispbdelete").value = a;
			document.getElementById("inotadelete").value = x;
			formna = document.getElementById("listform");
			formna.action = "<?php echo site_url(); ?>" + "/listnota/cform/delete";
			formna.submit();
		}
	}

	function yyy(x, b) {
		document.getElementById("ispbedit").value = b;
		document.getElementById("inotaedit").value = x;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/nota/cform/edit";
		formna.submit();
	}

	function print(b) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/printnotakhusus/cform/cetak/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function refreshview() {
		show('listnota/cform/view/<?= $dfrom ?>/<?= $dto ?>/<?= $iarea ?>', '#main');
	}
</script>