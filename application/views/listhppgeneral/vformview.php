<div id='tmp'>
<h2><?php echo $page_title.' Periode : '.$iperiode; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listhppgeneral/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
		<th>No</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>Beli</th>
		<th>Opname</th>
		<th>Harga</th>
		<th>Total</th>
	    <tbody>
	      <?php 
		if($isi){
      $no=0;
      $grandtotal=0;
      $gtso=0;
      $gtbeli=0;
      foreach($isi as $row){
        $no++;
        $total=$row->n_opname_total*$row->v_harga;
        $grandtotal=$grandtotal+$total;
        $gtso=$gtso+$row->n_opname_total;
        $gtbeli=$gtbeli+$row->n_beli;
			  echo "<tr><td align=right>$no</td>
				  <td>".$row->i_product."</td>
				  <td>".$row->e_product_name."</td>
				  <td align=right>".number_format($row->n_beli)."</td>
				  <td align=right>".number_format($row->n_opname_total)."</td>
				  <td align=right>".number_format($row->v_harga)."</td>
 				  <td align=right>".number_format($total)."</td>
				</tr>";
			}
		  echo "<tr><td align=right colspan=3>Total</td>
		    <td align=right>".number_format($gtbeli)."</td>
	      <td align=right>".number_format($gtso)."</td>
        <td></td>
			  <td align=right>".number_format($grandtotal)."</td>
			</tr>";

		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
