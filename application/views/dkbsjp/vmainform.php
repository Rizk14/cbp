<?php
if ($idkb != '') {
	$data['jmlitem'] 	= $jmlitem;
	$data['isi']		= $isi;
	$data['detail']		= $detail;
	$data['dfrom'] 		= $dfrom;
	$data['dto']		= $dto;
	$data['iarea'] 		= $iarea;
	$data['bisaedit'] 	= $bisaedit;
	$data['folder'] 	= $folder;

	echo "<h2>$page_title</h2>";
	$this->load->view($folder . '/vformupdate', $data);
} else {
	if ($isi) {
		$data['isi'] 		= $isi;
		$data['detail'] 	= $detail;
		$data['jmlitem'] 	= $jmlitem;
		$data['tgl'] 		= $tgl;
		$data['iarea'] 		= $iarea;
		$data['eareaname'] 	= $eareaname;
		$data['folder'] 	= $folder;

		echo "<div id='tmpx'>";
		echo "<h2>$page_title</h2>";
		$this->load->view($folder . '/vform', $data);
		echo "</div>";
	} else {
		echo "<h2>$page_title</h2>";
		$this->load->view($folder . '/vform');
	}
}
