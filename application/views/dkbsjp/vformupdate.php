<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="dkbform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td width="10%">Tgl DKB</td>
								<?php
								if ($isi->d_dkb != '') {
									$tmp = explode("-", $isi->d_dkb);
									$th = $tmp[0];
									$bl = $tmp[1];
									$hr = $tmp[2];
									$ddkb = $hr . "-" . $bl . "-" . $th;
								}

								if ($isi->tglentry != '') {
									$tmp = explode("-", $isi->tglentry);
									$th = $tmp[0];
									$bl = $tmp[1];
									$hr = $tmp[2];
									$ddkbx = $hr . "-" . $bl . "-" . $th;
								}
								?>
								<input hidden id="bdkb" name="bdkb" value="<?php echo $bl; ?>">
								<td>
									<input readonly id="ddkb" name="ddkb" value="<?php echo $ddkb; ?>" <?php if ($isi->i_approve1 == '' && $bisaedit) {
																											echo "onclick=\"showCalendar('',this,this,'','ddkb',0,20,1)\"";
																										} ?>>
									<input hidden id="tgldkb" name="tgldkb" value="<?php echo $ddkb; ?>">
									<input type="hidden" id="ddkbx" name="ddkbx" value="<?php echo $ddkbx; ?>">
									<input id="idkb" name="idkb" type="text" value="<?php echo $isi->i_dkb; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Area</td>
								<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>" <?php if ($isi->i_approve1 == '' && $bisaedit) {
																																	echo "onclick='cleararea();showModal(\"dkb/cform/area/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'";
																																} ?>>
									<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>">
									<input id="idkbold" name="idkbold" type="hidden" value="">
								</td>
							</tr>
							<tr>
								<td width="10%">Kirim</td>
								<td><input readonly id="edkbkirim" name="edkbkirim" value="<?php echo $isi->e_dkb_kirim; ?>" <?php if ($isi->i_approve1 == '' && $bisaedit) {
																																	echo "onclick='showModal(\"dkb/cform/kirim/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");'";
																																} ?>>
									<input id="idkbkirim" name="idkbkirim" type="hidden" value="<?php echo $isi->i_dkb_kirim; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Via</td>
								<td><input readonly id="edkbvia" name="ekbvia" value="<?php echo $isi->e_dkb_via; ?>" <?php if ($isi->i_approve1 == '' && $bisaedit) {
																															echo "onclick='return get_via1();'";
																														} ?>>
									<input id="idkbvia" name="idkbvia" type="hidden" value="<?php echo $isi->i_dkb_via; ?>">
									<input id="ntoleransi" name="ntoleransi" type="hidden" value="">
								</td>
							</tr>
							<tr>
								<td width="10%">Supir</td>
								<td><input id="esupirname" name="esupirname" value="<?php echo $isi->e_sopir_name; ?>" onkeyup="gede(this)"></td>
							</tr>
							<tr>
								<td width="10%">Nopol</td>
								<td><input id="ikendaraan" name="ikendaraan" value="<?php echo $isi->i_kendaraan; ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Jumlah</td>
								<?php
								if ($jmlx == $isi->v_dkb) {
									$totaldkb = $isi->v_dkb;
								} elseif ($isi->v_dkb < $jmlx) {
									$totaldkb = $jmlx;
								} else {
									$totaldkb = $jmlx;
								}
								$areadkb = substr($isi->i_dkb, 9, 2);
								?>
								<td><input style="text-align:right;" readonly id="vdkb" name="vdkb" value="<?php echo number_format($jmlx); ?>"></td>
							</tr>
							<tr>
								<td width="100%" align="center" colspan="4">
									<input <?php if ($isi->i_approve1 != '' || $isi->f_dkb_batal == 't') echo 'hidden'; ?> name="login" id="login" value="Simpan" type="submit" onclick="return dipales(parseFloat(document.getElementById('jml').value));"> <!-- <#?php if ($isi->i_approve1 != '' || $isi->f_dkb_batal == true) echo 'hidden'; ?> -->
									<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick='show("<?= $balik ?>/cform/view/<?php echo $dfrom . "/" . $dto . "/" . $iareax . "/"; ?>","#main")'>
									<input <?php if ($isi->i_approve1 != '') echo 'hidden'; ?> name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
									<input <?php if ($isi->i_approve1 != '') echo 'hidden'; ?> name="cmdtambahekspedisi" id="cmdtambahekspedisi" value="Tambah Ekspedisi" type="button" onclick="tambah_ekspedisi(parseFloat(document.getElementById('jmlx').value)+1);">
								</td>
							</tr>
						</table>

						<div id="detailheader" align="center">
							<table id="itemtem" class="listtable" style="width:100%;">
								<tr>
									<th style="width:23px;" align="center">No</th>
									<th style="width:184px;" align="center">No SJP</th>
									<th style="width:149px;" align="center">Tgl SJP</th>
									<!-- <th style="width:284px;" align="center">Cabang</th> -->
									<th style="width:123px;" align="center">Jml</th>
									<th style="width:123px;" align="center">Qty Kirim</th>
									<th style="width:235px;" align="center">Keterangan</th>
									<th style="width:408px;" align="center">Ket Terlambat</th>
									<th style="width:119px;" align="center">Koli</th>
									<th style="width:98px;" align="center">Ikat</th>
									<th style="width:79px;" align="center">Kardus</th>
									<th style="width:57px;" align="center" class="action">Action</th>
								</tr>
							</table>
						</div>

						<div id="detailisi" align="center">
							<?php
							$i = 0;
							foreach ($detail as $row) {
								echo '<table class="listtable" align="center" style="width:100%;">';
								$i++;
								$pangaos = number_format($row->v_jumlah);
								$tmp = explode("-", $row->d_sjp);
								$th = $tmp[0];
								$bl = $tmp[1];
								$hr = $tmp[2];
								$row->d_sjp = $hr . "-" . $bl . "-" . $th;

								$start_date	= new DateTime($row->d_dkb);
								$end_date 	= new Datetime($row->d_sjp);
								$interval 	= $start_date->diff($end_date);
								$dtelat 	= $interval->days;

								// <td style=\"width:177px;\"><input style=\"width:174px;\" readonly type=\"text\" id=\"icustomer$i\" name=\"icustomer$i\" value=\"" . $isi->e_area_name . "\"></td>
								echo "<tbody>
										<tr>
											<td style=\"width:23px;\"><input style=\"width:21px;\" readonly type=\"text\" id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
											<td style=\"width:104px;\"><input style=\"width:111px;\" readonly type=\"text\" id=\"isjp$i\" name=\"isjp$i\" value=\"" . $row->i_sjp . "\"></td>
											<td style=\"width:90px;\"><input style=\"width:92px;\" readonly type=\"text\" id=\"dsjp$i\" name=\"dsjp$i\" value=\"" . $row->d_sjp . "\">
											<input style=\"width:92px;\" readonly type=\"hidden\" id=\"dtelat$i\" name=\"dtelat$i\" value=\"$dtelat\"></td>
											<td style=\"width:78px;\"><input readonly style=\"text-align:right; width:76px;\" type=\"text\" id=\"vsjnetto$i\" name=\"vsjnetto$i\" value=\"" . $pangaos . "\"></td>
											<td style=\"width:78px;\"><input readonly style=\"text-align:right; width:76px;\" type=\"text\" id=\"ndeliv$i\" name=\"ndeliv$i\" value=\"" . $row->n_deliver . "\"></td>
											<td style=\"width:163px;\"><input style=\"width:162px;\"  type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"" . $row->e_remark . "\"></td>
											<td style=\"width:253px;\">
												<select id=\"etelat$i\" name=\"etelat$i\" type=\"text\" style=\"width:49%\">"; ?>
								<option value="" <?= $row->e_telat2 == '' ? 'selected' : '' ?>>-</option>";
								<option value="Menunggu Armada" <?= $row->e_telat2 == "Menunggu Armada" ? "selected" : "" ?>>Menunggu Armada</option>
								<option value="Menunggu Jalur" <?= $row->e_telat2 == "Menunggu Jalur" ? "selected" : "" ?>>Menunggu Jalur</option>
								<option value="Menunggu Ekspedisi" <?= $row->e_telat2 == "Menunggu Ekspedisi" ? "selected" : "" ?>>Menunggu Ekspedisi</option>
								<option value="Menunggu Rasio" <?= $row->e_telat2 == "Menunggu Rasio" ? "selected" : "" ?>>Menunggu Rasio</option>
								<option value="Menunggu Barcode" <?= $row->e_telat2 == "Menunggu Barcode" ? "selected" : "" ?>>Menunggu Barcode</option>
								<option value="Permintaan Toko" <?= $row->e_telat2 == "Permintaan Toko" ? "selected" : "" ?>>Permintaan Toko</option>
							<?php
								echo "</select>
											<input id=\"rtelat$i\" name=\"rtelat$i\" value=\"";
								if (
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Armada") &&
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Jalur") &&
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Ekspedisi") &&
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Rasio") &&
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Barcode") &&
									($row->e_telat2 != "" && $row->e_telat2 != "Menunggu Toko")
								) {
									echo $row->e_telat2;
								}
								echo "\" type=\"text\" placeholder=\"Keterangan lain\" style=\"width:48%\">
									</td>";

								echo "	<td style=\"width:70px;\"><input style=\"width:97%;text-align:right;\" type=\"text\" id=\"nkoli$i\" name=\"nkoli$i\" value=\"" . $row->n_koli . "\" onkeyup=\"hanyaAngka(this)\"></td>
										<td style=\"width:69px;\"><input style=\"width:97%;text-align:right;\" type=\"text\" id=\"nikat$i\" name=\"nikat$i\" value=\"" . $row->n_ikat . "\" onkeyup=\"hanyaAngka(this)\"></td>
										<td style=\"width:69px;\"><input style=\"width:96%;text-align:right;\" type=\"text\" id=\"nkardus$i\" name=\"nkardus$i\" value=\"" . $row->n_kardus . "\" onkeyup=\"hanyaAngka(this)\"></td>";

								if ($bisaedit && $isi->i_approve1 == "") {
									echo "<td style=\"width:53px;\" align=\"center\">
											<a href=\"#\" onclick='show(\"$folder/cform/deletedetail/$row->i_dkb/$row->i_area/$row->i_sjp/$dfrom/$dto/$isi->d_dkb/$iareax/\",\"#main\")'>
												<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
											</a>
										</td>
									</tr>
								</tbody>
							</table>";
								} else {
									echo "<td style=\"width:51px;\" align=\"center\"></td>";
								}
							}
							?>
						</div>

						<div id="detailheaderx" align="center">
							<table id="itemtemx" class="listtable" style="width:750px;">
								<tr>
									<th style="width:25px;" align="center">No</th>
									<th style="width:60px;" align="center">Kode</th>
									<th style="width:300px;" align="center">Nama</th>
									<th style="width:300px;" align="center">Keterangan</th>
									<th style="width:32px;" align="center" class="action">Action</th>
								</tr>
							</table>
						</div>

						<div id="detailisix" align="center">
							<?php
							$i = 0;
							if ($detailx) {
								foreach ($detailx as $row) {
									echo '<table class="listtable" align="center" style="width:750px;">';
									$i++;
									echo '<tbody>
											<tr>
												<td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx' . $i . '" name="barisx' . $i . '" value="' . $i . '"></td>
												<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi' . $i . '" name="iekspedisi' . $i . '" value="' . $row->i_ekspedisi . '"></td>
												<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname' . $i . '" name="eekspedisiname' . $i . '" value="' . $row->e_ekspedisi . '"></td>
												<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx' . $i . '" name="eremarkx' . $i . '" value="' . $row->e_remark . '"></td>
												<td style=\"width:50px;\" align=\"center\">';
									echo "			<a href=\"#\" onclick='show(\"$folder/cform/deletedetailekspedisi/$row->i_dkb/$iarea/$row->i_ekspedisi/$dfrom/$dto/$isi->d_dkb/\",\"#main\")'>
														<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/delete.png\" border=\"0\" alt=\"delete\">
													</a>
												</td>
											</tr>
										</tbody>
								</table>";
								}
							}
							?>
						</div>

						<div id="pesan"></div>

						<input type="hidden" name="jml" id="jml" <?php if (isset($jmlitem)) {
																		echo "value=\"$jmlitem\"";
																	} else {
																		echo "value=\"0\"";
																	} ?>>
						<input type="hidden" name="jmlx" id="jmlx" <?php if (isset($jmlitemx)) {
																		echo "value=\"$jmlitemx\"";
																	} else {
																		echo "value=\"0\"";
																	} ?>>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function tambah_item(a) {
		if (a <= 30) {
			so_inner = document.getElementById("detailheader").innerHTML;
			si_inner = document.getElementById("detailisi").innerHTML;

			b = document.getElementById("iarea").value;
			c = document.getElementById("ddkb").value;

			if (si_inner == '') {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = '<table class="listtable" style="width:100%;"><tbody><tr><td style="width:23px;"><input style="width:21px;height:29px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
				si_inner += '<td style="width:104px;"><input style="width:111px;height:29px;" readonly type="text" id="isjp' + a + '" name="isjp' + a + '" value="" onclick=\'showModal("<?= $folder ?>/cform/sjp/' + a + '/' + b + '/' + c + '/","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
				si_inner += '<td style="width:90px;"><input style="width:92px;height:29px;" readonly type="text" id="dsjp' + a + '" name="dsjp' + a + '" value=""><input type="hidden" name="dtelat' + a + '" id="dtelat' + a + '" readonly></td>';
				si_inner += '<td style="width:177px;"><input style="width:174px;height:29px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
				si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:76px;height:29px;"  type="text" id="vsjnetto' + a + '" name="vsjnetto' + a + '" value=""></td>';
				si_inner += '<td style="width:163px;"><input style="width:162px;height:29px;"  type="text" id="eremark' + a + '" name="eremark' + a + '" value=""><input type="hidden" id="finputpst' + a + '" name="finputpst' + a + '" value=""><input type="hidden" id="iarearef' + a + '" name="iarearef' + a + '" value=""></td>';
				si_inner += '<td style="width:253px;"><select id="etelat' + a + '" name="etelat' + a + '" type="text" style="width:49%"><option value="">-</option><option value="Menunggu Armada">Menunggu Armada</option><option value="Menunggu Jalur">Menunggu Jalur</option><option value="Menunggu Ekspedisi">Menunggu Ekspedisi</option><option value="Menunggu Rasio">Menunggu Rasio</option><option value="Menunggu Barcode" >Menunggu Barcode</option><option value="Permintaan Toko">Permintaan Toko</option></select><input id="rtelat' + a + '" name="rtelat' + a + '" value="" type="text" placeholder="Keterangan lain" style="width:50%"></td>';
				si_inner += '<td style="width:70px;"><input id="nkoli' + a + '" name="nkoli' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nikat' + a + '" name="nikat' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nkardus' + a + '" name="nkardus' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:53px;"></td></tr></tbody></table>';
			} else {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;

				si_inner += '<table class="listtable" style="width:100%;"><tbody><tr><td style="width:23px;"><input style="width:21px;height:29px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
				si_inner += '<td style="width:104px;"><input style="width:111px;height:29px;" readonly type="text" id="isjp' + a + '" name="isjp' + a + '" value="" onclick=\'showModal("<?= $folder ?>/cform/sjp/' + a + '/' + b + '/' + c + '/","#light");jsDlgShow("#konten *", "#fade", "#light");\'></td>';
				si_inner += '<td style="width:90px;"><input style="width:92px;height:29px;" readonly type="text" id="dsjp' + a + '" name="dsjp' + a + '" value=""><input type="hidden" name="dtelat' + a + '" id="dtelat' + a + '" readonly></td>';
				si_inner += '<td style="width:177px;"><input style="width:174px;height:29px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
				si_inner += '<td style="width:78px;"><input readonly style="text-align:right; width:76px;height:29px;"  type="text" id="vsjnetto' + a + '" name="vsjnetto' + a + '" value=""></td>';
				si_inner += '<td style="width:163px;"><input style="width:162px;height:29px;"  type="text" id="eremark' + a + '" name="eremark' + a + '" value=""><input type="hidden" id="finputpst' + a + '" name="finputpst' + a + '" value=""><input type="hidden" id="iarearef' + a + '" name="iarearef' + a + '" value=""></td>';
				si_inner += '<td style="width:253px;"><select id="etelat' + a + '" name="etelat' + a + '" type="text" style="width:49%"><option value="">-</option><option value="Menunggu Armada">Menunggu Armada</option><option value="Menunggu Jalur">Menunggu Jalur</option><option value="Menunggu Ekspedisi">Menunggu Ekspedisi</option><option value="Menunggu Rasio">Menunggu Rasio</option><option value="Menunggu Barcode" >Menunggu Barcode</option><option value="Permintaan Toko">Permintaan Toko</option></select><input id="rtelat' + a + '" name="rtelat' + a + '" value="" type="text" placeholder="Keterangan lain" style="width:50%"></td>';
				si_inner += '<td style="width:70px;"><input id="nkoli' + a + '" name="nkoli' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nikat' + a + '" name="nikat' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:69px;"><input id="nkardus' + a + '" name="nkardus' + a + '" value="0" onkeyup="hanyaAngka(this)" type="text" style="width:97%;text-align:right;"></td>';
				si_inner += '<td style="width:53px;"></td></tr></tbody></table>';
			}
			j = 0;
			var baris = Array();
			var isjp = Array();
			var dsjp = Array();
			var icustomer = Array();
			var vsjnetto = Array();
			for (i = 1; i < a; i++) {
				j++;
				baris[j] = document.getElementById("baris" + i).value;
				isjp[j] = document.getElementById("isjp" + i).value;
				dsjp[j] = document.getElementById("dsjp" + i).value;
				icustomer[j] = document.getElementById("icustomer" + i).value;
				vsjnetto[j] = document.getElementById("vsjnetto" + i).value;
			}
			document.getElementById("detailisi").innerHTML = si_inner;
			j = 0;
			for (i = 1; i < a; i++) {
				j++;
				document.getElementById("baris" + i).value = baris[j];
				document.getElementById("isjp" + i).value = isjp[j];
				document.getElementById("dsjp" + i).value = dsjp[j];
				document.getElementById("icustomer" + i).value = icustomer[j];
				document.getElementById("vsjnetto" + i).value = vsjnetto[j];
			}
			area = document.getElementById("iarea").value;
			ddkb = document.getElementById("ddkb").value;
			showModal("<?= $folder ?>/cform/sjp/" + a + "/" + area + "/" + ddkb, "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		} else {
			alert("Maksimal 30 item");
		}
	}

	// function dipales(a) {
	// 	cek = 'false';
	// 	if ((document.getElementById("ddkb").value != '') &&
	// 		(document.getElementById("iarea").value != '') &&
	// 		(document.getElementById("idkbkirim").value != '') &&
	// 		(document.getElementById("idkbvia").value != '')) {
	// 		if (a == 0) {
	// 			alert('Isi data item minimal 1 !!!');
	// 		} else {
	// 			for (i = 1; i <= a; i++) {
	// 				if ((document.getElementById("isjp" + i).value == '')) {
	// 					alert('Data item masih ada yang salah !!!');
	// 					exit();
	// 					cek = 'false';
	// 				} else {
	// 					cek = 'true';
	// 				}
	// 			}
	// 		}
	// 		if (cek == 'true') {
	// 			document.getElementById("login").hidden = true;
	// 			document.getElementById("cmdtambahitem").hidden = true;
	// 			document.getElementById("cmdtambahekspedisi").hidden = true;
	// 		} else {
	// 			document.getElementById("login").hidden = false;
	// 		}
	// 	} else {
	// 		alert('Data header masih ada yang salah !!!');
	// 	}
	// }

	function dipales(a) {
		/*cek 		= 'false';*/
		$s = 0;
		if ((document.getElementById("ddkb").value != '') &&
			(document.getElementById("iarea").value != '') &&
			(document.getElementById("idkbkirim").value != '') &&
			(document.getElementById("idkbvia").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("dtelat" + i).value > 2 && document.getElementById("etelat" + i).value == '' &&
							document.getElementById("rtelat" + i).value == '')) {
						alert("Harap Isi Kolom Terlambat No. " + i);
						$s = 1;
						return false;
					}

					if ((document.getElementById("etelat" + i).value != '' && document.getElementById("rtelat" + i).value != '')) {
						alert("Isi 1 Keterangan Saja");
						$s = 1;
						return false;
					}

					if ((document.getElementById("isjp" + i).value == '') ||
						(document.getElementById("dsjp" + i).value == '') ||
						(document.getElementById("vsjnetto" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						$s = 1;
						return false;
					}
				}

				document.getElementById("login").hidden = true;
				document.getElementById("cmdtambahitem").hidden = true;
				document.getElementById("cmdtambahekspedisi").hidden = true;
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}
	}

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").hidden = false;
	}

	function pilihan(a, b) {
		if (a == '') {
			document.getElementById("chk" + b).value = 'on';
		} else {
			document.getElementById("chk" + b).value = '';
		}
	}

	function tambah_ekspedisi(a) {
		so_inner = document.getElementById("detailheaderx").innerHTML;
		si_inner = document.getElementById("detailisix").innerHTML;
		if (so_inner == '') {
			so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
			so_inner += '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner += '<th style="width:60px;" align="center">Kode</th>';
			so_inner += '<th style="width:300px;" align="center">Nama</th>';
			so_inner += '<th style="width:300px;" align="center">Keterangan</th>';
			so_inner += '<th style="width:32px;"  align="center" class="action">Action</th></tr>';
			document.getElementById("detailheaderx").innerHTML = so_inner;
		} else {
			so_inner = '';
		}
		if (si_inner == '') {
			document.getElementById("jmlx").value = parseFloat(document.getElementById("jmlx").value) + 1;
			juml = document.getElementById("jmlx").value;
			si_inner = '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx' + a + '" name="barisx' + a + '" value="' + a + '"></td>';
			si_inner += '<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi' + a + '" name="iekspedisi' + a + '" value=""></td>';
			si_inner += '<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname' + a + '" name="eekspedisiname' + a + '" value=""></td>';
			si_inner += '<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx' + a + '" name="eremarkx' + a + '" value=""></td>';
			si_inner += '<td style="width:50px;">&nbsp;</td></tr></tbody></table>';
		} else {
			document.getElementById("jmlx").value = parseFloat(document.getElementById("jmlx").value) + 1;
			juml = document.getElementById("jmlx").value;
			si_inner = si_inner + '<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="barisx' + a + '" name="barisx' + a + '" value="' + a + '"></td>';
			si_inner += '<td style="width:60px;"><input style="width:60px;" readonly type="text" id="iekspedisi' + a + '" name="iekspedisi' + a + '" value=""></td>';
			si_inner += '<td style="width:297px;"><input style="width:297px;" readonly type="text" id="eekspedisiname' + a + '" name="eekspedisiname' + a + '" value=""></td>';
			si_inner += '<td style="width:298px;"><input style="width:298px;"  type="text" id="eremarkx' + a + '" name="eremarkx' + a + '" value=""></td>';
			si_inner += '<td style="width:50px;">&nbsp;</td></tr></tbody></table>';
		}
		j = 0;
		var barisx = Array()
		var iekspedisi = Array();
		var eekspedisiname = Array();
		var eremark = Array();
		for (i = 1; i < a; i++) {
			j++;
			barisx[j] = document.getElementById("barisx" + i).value;
			iekspedisi[j] = document.getElementById("iekspedisi" + i).value;
			eekspedisiname[j] = document.getElementById("eekspedisiname" + i).value;
			eremark[j] = document.getElementById("eremarkx" + i).value;
		}
		document.getElementById("detailisix").innerHTML = si_inner;
		j = 0;
		for (i = 1; i < a; i++) {
			j++;
			document.getElementById("barisx" + i).value = barisx[j];
			document.getElementById("iekspedisi" + i).value = iekspedisi[j];
			document.getElementById("eekspedisiname" + i).value = eekspedisiname[j];
			document.getElementById("eremarkx" + i).value = eremark[j];
		}
		showModal("bapb/cform/ekspedisiupdate/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function afterSetDateValue(ref_field, target_field, date) {
		cektanggal();
		dspb = document.getElementById('ddkb').value;
		bspb = document.getElementById('bdkb').value;
		dtmp = dspb.split('-');
		per = dtmp[2] + dtmp[1] + dtmp[0];
		bln = dtmp[1];
		if ((bspb != '') && (dspb != '')) {
			if (bspb != bln) {
				alert("Tanggal DKB tidak boleh dalam bulan yang berbeda !!!");
				document.getElementById("ddkb").value = document.getElementById("tgldkb").value;
			}
		}
	}

	function cektanggal() {
		ddkb = document.getElementById('ddkb').value;
		ddkbx = document.getElementById('ddkbx').value;
		dtmp = ddkb.split('-');
		thndkb = dtmp[2];
		blndkb = dtmp[1];
		hrdkb = dtmp[0];
		dtmp = ddkbx.split('-');
		thndkbx = dtmp[2];
		blndkbx = dtmp[1];
		hrdkbx = dtmp[0];
		if (thndkb < thndkbx) {
			alert('Tanggal DKB tidak boleh lebih kecil dari tanggal input !!!');
			document.getElementById('ddkb').value = document.getElementById('tgldkb').value;
		} else {
			if (blndkb < blndkbx) {
				alert('Tanggal DKB tidak boleh lebih kecil dari tanggal input !!!');
				document.getElementById('ddkb').value = document.getElementById('tgldkb').value;
			} else if (blndkb == blndkbx) {
				if (hrdkb < hrdkbx) {
					alert('Tanggal DKB tidak boleh lebih kecil dari tanggal input !!!');
					document.getElementById('ddkb').value = document.getElementById('tgldkb').value;
				}
			}
		}
	}

	function cleararea() {
		document.getElementById("edkbvia").value = '';
		document.getElementById("idkbvia").value = '';
		document.getElementById("ntoleransi").value = '';
	}

	function get_via1() {
		iarea = document.getElementById("iarea").value;

		if (iarea == "") {
			alert("Pilih Areanya dulu !!");
			return false;
		} else {
			showModal("<?= $folder ?>/cform/via/" + iarea + "/", "#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	}
</script>