<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmpx">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printfpajakbbk/cform/faktur','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="hidden" name="dfrom" value= "<?php echo $dfrom; ?>" >
			<input type="hidden" name="dto" value= "<?php echo $dto; ?>" >
			<input type="hidden" name="area" value= "<?php echo $area; ?>" ><input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
 	    <th>Faktur</th>
	    <th>BBK</th>
	    <th>Tgl BBK</th>
	    <th>Kotor</th>
	    <th>DPP</th>
	    <th>PPN</th>
	    <th>Bersih</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        $tmp=explode('-',$row->d_bbk);
        $hr=$tmp[2];
        $bl=$tmp[1];
        $th=$tmp[0];
        $row->d_bbk=$hr.'-'.$bl.'-'.$th;
        $tmp=explode('-',$row->d_pajak);
        $hr=$tmp[2];
        $bl=$tmp[1];
        $th=$tmp[0];
        $row->d_pajak=$hr.'-'.$bl.'-'.$th;
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$row->i_faktur_komersial</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$row->i_bbk</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$row->d_bbk</a></td>";
        $total=0;
        $this->db->select(" i_bbk from tm_bbk_pajak
                            where i_faktur_komersial='$row->i_faktur_komersial'", false);
		    $query = $this->db->get();
		    if ($query->num_rows() > 0){
			    foreach($query->result() as $ro){
            $this->db->select(" sum(n_quantity*v_unit_price) as jml from tm_bbk_item
                                where i_bbk='$ro->i_bbk'", false);
		        $query = $this->db->get();
		        if ($query->num_rows() > 0){
			        foreach($query->result() as $ro){
                $total=$total+$ro->jml;
              }
            }
          }
        }
        $potongan=0;
        $dpp=$total-$potongan;
        $ppn=$total*0.1;
        $bersih=$dpp+$ppn;
        echo "
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$total</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$dpp</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$ppn</a></td>
				  <td><a href=\"javascript:setValue('$row->i_faktur_komersial','$row->d_pajak')\">$bersih</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,c)
  {
    document.getElementById("ifkom").value=a;
    document.getElementById("dpajak").value=c;
    jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
