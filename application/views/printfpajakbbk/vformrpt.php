<?php 
	include ("php/fungsi.php");
	require_once("printipp/PrintIPP.php");
  $cetak='';
  $x=0;
	foreach($isi as $row){
    if($x==0){
		  $nor	= str_repeat(" ",5);
		  $abn	= str_repeat(" ",12);
		  $ab	  = str_repeat(" ",9);
		  $x1 	= str_repeat(" ",25);
		  $x2 	= str_repeat(" ",28);
		  $ipp 	= new PrintIPP();
		  $ipp->setHost($host);
		  $ipp->setPrinterURI($uri);
		  $ipp->setRawText();
		  $ipp->unsetFormFeed();
		  $cetak.=CHR(15)."\n\n\n\n".CHR(18);
      if($row->f_pajak_pengganti=='t'){ 
        $ganti='1';
      }else{ 
        $ganti='0';
      }
      $tmp=explode("-",$row->d_pajak);
		  $thn=substr($tmp[0],2,2);
      $bl=$tmp[1];
      $gabung=$thn.$bl;
      if($gabung>'1303'){
#        $nopajak='010.900-'.$thn.'.489'.$iseri;
        $nopajak=$iseri;
      }else{
        $nopajak='01'.$ganti.'.000-'.$thn.'.00'.$iseri;
      }
#      $nopajak='01'.$ganti.'.000-'.$thn.'.00'.$iseri;
		  $cetak.=$x2.CHR(27).CHR(120).CHR(1).CHR(27).CHR(119).CHR(1).$nopajak.CHR(27).CHR(120).CHR(0).CHR(27).CHR(119).CHR(0)."\n\n\n";
		  $cetak.=$x1.NmPerusahaan."\n";
		  $cetak.=$x1.AlmtPerusahaan." ".KotaPerusahaan."\n\n";
		  $cetak.=$x1.NPWPPerusahaan."\n\n\n\n";
      $cetak.=$x1.PajakBBK."\n";
      $cetak.="\n\n";
      $row->e_customer_pkpnpwp="00.000.000.0.000.000";
		  $cetak.=$x1.$row->e_customer_pkpnpwp."\n\n\n\n";		
		  $i=0;
      $total=0;
		  foreach($detail as $rowi){
        if($rowi->n_quantity>0){
				  $i++;
				  $tot	= $rowi->n_quantity*$rowi->v_unit_price;
          $total	= $total+$tot;
			  }
		  }
      $enter="";
#      $y=23-$i;
      $y=21;
      for($x=1;$x<=$y;$x++){
        $enter.="\n";
      }
      $aw=str_repeat(" ",5);
      $cetak.=$aw."   Sesuai dengan faktur no\n";
      $cetak.=$aw."   ".substr($faktur,8,6).". Faktur terlampir".str_repeat(" ",37).number_format($total);
      $cetak.=CHR(15)."$enter".CHR(18);
  ###################################
      $row->v_nota_gross=$total;
		  if(strlen(number_format($row->v_nota_gross))==1){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",48).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==4){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",45).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==5){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",44).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==6){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",43).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==7){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",42).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==8){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",41).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==9){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",40).number_format($row->v_nota_gross)."\n\n";
		  }elseif(strlen(number_format($row->v_nota_gross))==10){
			  $cetak.=str_repeat(" ",10)."XXXXXXXXXXXXXXXXXXX".str_repeat(" ",39).number_format($row->v_nota_gross)."\n\n";
		  }
   
      $row->v_nota_discount=0;
		  if(strlen(number_format($row->v_nota_discount))==1){
			  $cetak.=str_repeat(" ",44)."                                 ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==3){
			  $cetak.=str_repeat(" ",44)."                               ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==4){
			  $cetak.=str_repeat(" ",44)."                              ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==5){
			  $cetak.=str_repeat(" ",44)."                             ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==6){
			  $cetak.=str_repeat(" ",44)."                            ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==7){
			  $cetak.=str_repeat(" ",44)."                           ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==8){
			  $cetak.=str_repeat(" ",44)."                          ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==9){
			  $cetak.=str_repeat(" ",44)."                         ".number_format($row->v_nota_discount)."\n\n\n";
		  }elseif(strlen(number_format($row->v_nota_discount))==10){
			  $cetak.=str_repeat(" ",44)."                        ".number_format($row->v_nota_discount)."\n\n\n";
		  }
		  $ndpp=round($row->v_nota_gross);
      $nppn=round($row->v_nota_gross*0.1);
      $nilai=$row->v_nota_gross+$nppn;
      if(strlen(number_format($ndpp))==1){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."              ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==4){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."           ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==5){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."          ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==6){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."         ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==7){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."        ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==8){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."       ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==9){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."      ".number_format($ndpp)."\n";
	    }elseif(strlen(number_format($ndpp))==10){
		    $cetak.=str_repeat(" ",44)."100/110 X ".number_format($nilai)."     ".number_format($ndpp)."\n";
	    }
      if(strlen(number_format($nppn))==1){
		    $cetak.=str_repeat(" ",44)."                                 ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==4){                                                   
		    $cetak.=str_repeat(" ",44)."                              ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==5){
		    $cetak.=str_repeat(" ",44)."                             ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==6){
		    $cetak.=str_repeat(" ",44)."                            ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==7){
		    $cetak.=str_repeat(" ",44)."                           ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==8){
		    $cetak.=str_repeat(" ",44)."                          ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==9){
		    $cetak.=str_repeat(" ",44)."                         ".number_format($nppn)."\n\n";
	    }elseif(strlen(number_format($nppn))==10){
		    $cetak.=str_repeat(" ",44)."                        ".number_format($nppn)."\n\n";
	    }else{
        $cetak.=str_repeat(" ",44)."                        ".number_format($nppn)."\n\n";
      }
      $tmp=explode("-",$row->d_pajak);
		  $th=$tmp[0];
		  $bl=$tmp[1];
		  $hr=$tmp[2];
		  $dpajak=$hr." ".mbulan($bl)." ".$th;
	    $cetak.=str_repeat(" ",45)."BANDUNG,       ".$dpajak."\n\n\n\n\n";
      $cetak.=str_repeat(" ",57).ucwords(strtolower($nama))."\n\n";
      $ipp->setFormFeed();
      $cetak.=str_repeat(" ",56).ucwords(strtolower($jabatan));#"Spv. Adm.";
    }
  }
#  echo $cetak;
  $ipp->setdata($cetak);
  $ipp->printJob();
	echo "<script>this.close();</script>";
?>
