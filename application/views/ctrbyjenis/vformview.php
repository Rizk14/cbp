<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyjenis/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode Tahun</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $tahun; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;"></td>
            <td></td>
         </tr>
         <tr>
            <td style="width:110px;">Sampai Bulan</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $bulan; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;"></td>
            <td></td>
         </tr>
         <tr>
            <td style="width:500px;" colspan="7">Penghitungan selalu di mulai dari bulan <b><u>Januari</u></b></td>
            <td></td>
         </tr>
        </table>
        <?php 
           $tahunprev = intval($tahun) - 1;
        ?>
        <table class="listtable">
         <tr align="center">
           <th>No</th>
           <th>Jenis</th>
           <th>Value YTD Tahun <?php echo $tahunprev; ?></th>
           <th>Value YTD Tahun <?php echo $tahun; ?></th>
           <th>% Growth</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $totnilaiprev=0;
        $totnilai=0;
        $i = 0;
        foreach($isi as $row){
          $i++;
          $growth = (($row->nilai - $row->nilaiprev)/$row->nilaiprev)*100;
          $totnilaiprev += $row->nilaiprev;
          $totnilai += $row->nilai;

          echo "<tr>
              <td style='font-size:12px; align=center'>".$i."</td>
              <td style='font-size:12px;' align=center>".$row->nama."</td>
              <td style='font-size:12px;' align=right>".number_format($row->nilaiprev)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->nilai)."</td>
              <td style='font-size:12px;' align=right>".number_format($growth,2)." %</td>
              </tr>";
        }
        echo "<tr>
              <td colspan=2 style='font-size:12px;' align='center' ><b>Total</b></td>
              <td style='font-size:12px;' align=right><b>".number_format($totnilaiprev)."</b></td>
              <td style='font-size:12px;' align=right><b>".number_format($totnilai)."</b></td>
              <td style='font-size:12px;' align=right><b>-</b></td>
              </tr>";

      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
