<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/FusionCharts.js"></script>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyisland_new/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr>
            <td style="width:100px;">Periode</td>
            <td style="width:5px;">:</td>
            <td colspan=2> <?php echo $tahun.' ('.$dfrom.' s/d '.$dto.')'; ?></td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
         <tr>
            <td style="width:100px;">Group</td>
            <td style="width:5px;">:</td>
            <?php $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
                if ($query->num_rows() > 0){
                  foreach($query->result() as $tmp){
                     $group=$tmp->e_product_groupname;
                 }
               }

               if($group=="NA"){
                $group="NASIONAL";
               }
               
                ?>
            <td colspan=2> <?php echo $group; ?> </td>
            <td style="width:5px;"> </td>
            <td style="width:500px; text-align:left;">
            </td>
            <td>
            </td>
         </tr>
        </table>
        <?php 
          if($isi){
            $totvnota=0;
            $totqnota=0;
            foreach($isi as $ro){
              $totvnota=$totvnota+$ro->vnota;
              $totqnota=$totqnota+$ro->qnota;
            }
          }
        ?>
        <table class="listtable">
         <tr>
         <th rowspan="2">Island</th>
         <th rowspan="2">Provinsi</th>
         <th rowspan="2">OB</th>
         <th colspan="3">OA</th>
         <th colspan="3">Sales Qty(Unit)</th>
         <th colspan="3">Net Sales(Rp.)</th>
         <th rowspan="2">%Ctr Net Sales(Rp.)</th>
         </tr>
         <tr>
            <?php $prevth= $tahun-1; ?>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth OA</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Qty</th>
            <th><?php echo $prevth ?></th>
            <th><?php echo $tahun ?></th>
            <th>Growth Rp</th>
         </tr>
       <tbody>
         <?php 
      if($isi){
        $grwoa=0;
        $grwqty=0;
        $grwrp=0;
        $totnota=0;
        $totrp=0;
        $ctrrp=0;
        $totprevoa=0;
        $totoa=0;
        $totprevqnota=0;
        $totprevvnota=0;
        $totctrrp=0;
        $totgrwoa=0;
        $totgrwqty=0;
        $totgrwrp=0;
        $totpersenvnota=0;
        $totob=0;

        foreach($isi as $row){
            $totnota+=$row->vnota;
        }
        $totrp=$totnota;


        foreach($isi as $row){
          
          if($totvnota==0){
            $persenvnota=0;
          }else{
            $persenvnota=($row->vnota/$totvnota)*100;
          }
          $totpersenvnota=$totpersenvnota+$persenvnota;

          if ($row->prevoa == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->prevoa)/$row->prevoa)*100;
          }

          if ($row->prevqnota == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->qnota-$row->prevqnota)/$row->prevqnota)*100;
          }

          if ($row->prevvnota == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->vnota-$row->prevvnota)/$row->prevvnota)*100;
          }

          $totob=$totob+$row->ob;
          $ctrrp= ($row->vnota/$totrp)*100;
          $totprevoa=$totprevoa+$row->prevoa;
          $totoa=$totoa+$row->oa;
          $totprevqnota=$totprevqnota+$row->prevqnota;
          $totprevvnota=$totprevvnota+$row->prevvnota;
          $totctrrp=$totctrrp+$ctrrp;


          echo "<tr>
              <td style='font-size:12px;'>$row->e_area_island</td>
              <td style='font-size:12px;'>$row->e_provinsi</td>
              <td style='font-size:12px;' align=right>".number_format($row->ob)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevoa)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->oa)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwoa,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevqnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwqty,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($row->prevvnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($row->vnota)."</td>
              <td style='font-size:12px;' align=right>".number_format($grwrp,2)." %</td>
              <td style='font-size:12px;' align=right>".number_format($ctrrp,2)." %</td>
              </tr>";
         }

         if ($totprevoa == 0) {
              $totgrwoa = 0;
          } else { //jika pembagi tidak 0
              $totgrwoa = (($totoa-$totprevoa)/$totprevoa)*100;
          }

          if ($totprevqnota == 0) {
              $totgrwqty = 0;
          } else { //jika pembagi tidak 0
              $totgrwqty = (($totqnota-$totprevqnota)/$totprevqnota)*100;
          }

          if ($totprevvnota == 0) {
              $totgrwrp = 0;
          } else { //jika pembagi tidak 0
              $totgrwrp = (($totvnota-$totprevvnota)/$totprevvnota)*100;
          }

        echo "<tr>
        <td style='font-size:12px;' colspan='2'><b>Total</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totob)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totprevoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totoa)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwoa,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totprevqnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totqnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwqty,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totprevvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totvnota)."</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totgrwrp,2)." %</b></td>
        <td style='font-size:12px;' align=right><b>".number_format($totctrrp,2)." %</b></td>
        </tr>";
      }
         ?>
       </tbody>
     </table>
   </div>
      </div>
      <?php //echo $graph ; ?>
      <?=form_close()?>
    </td>
  </tr>
</table>
