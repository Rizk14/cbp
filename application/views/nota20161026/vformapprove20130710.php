<?php 
include ("php/fungsi.php");
?>
<?php echo "<h2>$page_title</h2>";?>
<table class="maintable">
  <tr>
    <td align="left">
	   <?php echo $this->pquery->form_remote_tag(array('url'=>'nota/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>Nota</td>
		<td><?php 	if($isi->i_nota!='') {
					echo "<input type=\"text\" id=\"inota\" name=\"inota\" value=\"$isi->i_nota\"
										   readonly>"; 
				}else{
					echo "<input type=\"hidden\" id=\"inota\" name=\"inota\" value=\"\"
										   readonly>"; 
				}	
				if($isi->d_nota!=''){				
					$tmp=explode("-",$isi->d_nota);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dnota=$hr."-".$bl."-".$th;
          if($isi->d_dkb>=$isi->d_sj){
					  $tmp=explode("-",$isi->d_dkb);
          }else{
            $tmp=explode("-",$isi->d_sj);
          }
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$xdkb=$hr."-".$bl."-".$th;
					echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">";
          echo " <input type=\"hidden\" id=\"dtmp\" name=\"dtmp\" value=\"$xdkb\">";
				}elseif($isi->d_dkb!=''){
          if($isi->d_dkb>=$isi->d_sj){
  					$tmp=explode("-",$isi->d_dkb);
          }else{
  					$tmp=explode("-",$isi->d_sj);
          }
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$dnota=$hr."-".$bl."-".$th;
					echo " <input readonly id=\"dnota\" name=\"dnota\" value=\"$dnota\" onclick=\"showCalendar('',this,this,'','dnota',0,20,1)\">";
          echo " <input type=\"hidden\" id=\"dtmp\" name=\"dtmp\" value=\"$dnota\">";
				
				}else{
			?>
			<input readonly id="dnota" name="dnota" value="<?php echo $tgl; ?>" onclick="showCalendar('',this,this,'','dnota',0,20,1)">
      <input type="hidden" id="dtmp" name="dtmp" value="<?php echo $tgl; ?>">
			<?php }?>
			<input id="inotaold" name="inotaold" value="">
			</td>
		<td>Kelompok Harga</td>
		<td><input readonly onclick="pricegroup();" id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>">
        <input id="fspbconsigment" name="fspbconsigment" type="hidden" value="<?php echo $isi->f_spb_consigment; ?>"></td>
	      </tr>
	      <tr>
		<td>SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input type="text" readonly id="ispb" name="ispb" value="<?php echo $isi->i_spb; ?>">
			<input type="text" id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
		<td>Nilai Kotor</td>
		<?php 
			$enin=number_format($isi->v_spb);
		?>
		<td><input id="vspb" name="vspb" readonly value="<?php echo $enin; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Promo</td>
		<td width="38%"><input readonly id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name; ?>" 
			 	   readonly >
		    <input id="ispbprogram" name="ispbprogram" type="hidden" value="<?php echo $isi->i_spb_program; ?>"></td>
		<td>Discount 1</td>
		<td><input id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>" onkeyup="formatcemua(this.value); editnilai();">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_spb_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="arena" name="arena" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>" readonly >
		    <input readonly id="spbold" name="spbold" value="<?php echo $isi->i_spb_old; ?>"readonly >
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 2</td>
		<td><input id="ncustomerdiscount2" name="ncustomerdiscount2"
				   value="<?php echo $isi->n_spb_discount2; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_spb_discount2); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan()" 
				   value="<?php echo $isi->e_customer_name; ?>" readonly >
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 3</td>
		<td><input id="ncustomerdiscount3" name="ncustomerdiscount3"
				   value="<?php echo $isi->n_spb_discount3; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_spb_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
		<td>Discount 4</td>
		<td><input id="ncustomerdiscount4" name="ncustomerdiscount4"
				   value="<?php echo $isi->n_spb_discount4; ?>" onkeyup="formatcemua(this.value);editnilai();">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" 
				   value="<?php echo number_format($isi->v_spb_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td>Masalah</td>
		<td><!--<input id="fspbconsigment" name="fspbconsigment" type="checkbox" readonly 
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>&nbsp;-->
			<input id="fmasalah" name="fmasalah" type="checkbox" value="">&nbsp;Insentif&nbsp;
			<input id="finsentif" name="finsentif" type="checkbox" value="on" checked>&nbsp;Cicil&nbsp;
			<input id="fcicil" name="fcicil" type="checkbox" value="on" <?php if($isi->f_customer_cicil=='t') echo "checked";?>></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<?php 
			$tmp = explode("-", $isi->d_sj);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dsj	= $yir."/".$mon."/".$det;
      if(substr($isi->i_sj,8,2)=='00'){
        $topnya=$isi->n_spb_toplength+$isi->n_toleransi_pusat;
      }else{
        $topnya=$isi->n_spb_toplength+$isi->n_toleransi_cabang;
      }
			$dudet	=dateAdd("d",$topnya,$dsj);
			$dudet 	= explode("-", $dudet);
			$det1	= $dudet[2];
			$mon1	= $dudet[1];
			$yir1 	= $dudet[0];
			$dudet	= $det1."-".$mon1."-".$yir1;
		?>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_spb_toplength; ?>">
			&nbsp;&nbsp;Jatuh tempo&nbsp;
			<input id="djatuhtempo" name="djatuhtempo" readonly value="<?php echo $dudet; ?>"></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" readonly
				   value="<?php echo number_format($tmp); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname"
				   value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden"
				   value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" onkeyup="hitungnilai();reformat(this);"
			 value="<?php echo number_format($isi->v_nota_discounttotal); ?>"></td>
	      </tr>
		<tr>
		<td>Surat Jalan</td>
		<?php 
			$tmp=explode("-",$isi->d_sj);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dsj=$hr."-".$bl."-".$th;
		?>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly value="<?php echo $isi->i_sj; ?>">
		    <input readonly readonly id="dsj" name="dsj" value="<?php echo $dsj; ?>"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input id="vspbafter" name="vspbafter" onkeyup="hitungdiscount();reformat(this);"
			 value="<?php echo number_format($isi->v_nota_netto); ?>"></td>
		</tr>
		<tr>
		  <td>PKP</td>
		<td colspan=""><!--<input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">-->
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
				   value="<?php echo $isi->e_customer_pkpnpwp;?>">
			<input type="hidden" id="fspbplusppn" name="fspbplusppn" value="<?php echo $isi->f_spb_plusppn;?>">
			<input type="hidden" id="fspbplusdiscount" name="fspbplusdiscount" value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input type="hidden" id="nprice" name="nprice" value="1">
			<input type="hidden" id="vnotagross" name="vnotagross" value="0">
			<input type="hidden" id="vnotappn" name="vnotappn" value="0"></td>
		<td>Keterangan</td>
		<td><input id="eremark" name="eremark" value=""></td>
		</tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("nota/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'>
        		    <input name="cmdbalik" id="cmdbalik" value="Kembalikan SPB" type="button" onclick='show("nota/cform/balikspb/<?php echo $isi->i_spb."/".$isi->i_area."/".$dfrom."/".$dto."/"; ?>","#main")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:100px;"  align="center">Harga</th>
					<th style="width:60px;"  align="center">Jml Pesan</th>
					<th style="width:60px;"  align="center">Jml Dlv</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\"";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$harga	=number_format($row->v_unit_price,2);
					$norder	=number_format($row->n_order,0);
					$ndeliv	=number_format($row->n_deliver,0);
//					$ndeliv	=number_format($row->n_stock,0);
				  	echo "<tbody>
							<tr>
		    				<td width=\"23px\">
								<input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td width=\"64px\"><input style=\"width:64px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td width=\"302px\"><input style=\"width:302px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td width=\"98px\"><input style=\"width:98px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td width=\"100px\"><input style=\"text-align:right; width:100px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>
							<td width=\"60px\"><input style=\"text-align:right; width:60px;\"  type=\"text\" 
								id=\"norder$i\" readonly
								name=\"norder$i\" value=\"$norder\"></td>
							<td width=\"60px\"><input style=\"text-align:right; width:60px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\">
                <input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>
							</tr>
						  </tbody>";
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function hitungnilai(){
	var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');
	var nilaidis=document.getElementById("vspbdiscounttotalafter").value.replace(/\,/g,'');
	if(!isNaN(nilaidis)){
		if((nilaispb-nilaidis)<0){
			alert("Nilai discount tidak valid !!!!!");
			document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
		}else{
			document.getElementById("vspbafter").value=formatcemua(nilaispb-nilaidis);
		}
	}else{ 
		alert('input harus numerik !!!');
     	document.getElementById("vspbdiscounttotalafter").value = document.getElementById("vspbdiscounttotalafter").value.substring(0,input.value.length-1);
	}
  }
  function hitungdiscount(){
	  var nilaispb=document.getElementById("vspb").value.replace(/\,/g,'');//    alert(vdistot);
	  var nilaitot=document.getElementById("vspbafter").value.replace(/\,/g,'');
	  if(!isNaN(nilaitot)){
		  if((nilaispb-nilaitot)<0){
			  alert("Nilai total tidak valid !!!!!");
			  document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
		  }else{
        document.getElementById("vspbbersih").value=document.getElementById("vspbafter").value;
        document.getElementById("vspbdiscounttotal").value=formatcemua(nilaispb-nilaitot);
			  document.getElementById("vspbdiscounttotalafter").value=formatcemua(nilaispb-nilaitot);
		  }
	  }else{ 
		  alert('input harus numerik !!!');
       	document.getElementById("vspbafter").value = document.getElementById("vspbafter").value.substring(0,input.value.length-1);
	  }
  }
  $(document).ready(function () {
//    awall();
  editnilai();
//	tungnilai();
  });
  function awall(){
	var jml 	= parseFloat(document.getElementById("jml").value);
	var totdis 	= 0;
	var totnil	= 0;
	var hrg		= 0;
	var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	var ndis4	= parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
	var vdis1	= 0;
	var vdis2	= 0;
	var vdis3	= 0;
	var vdis4	= 0;
	for(i=1;i<=jml;i++){
	  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
		hrg		= hrg+hrgtmp;
	}
	hrg=Math.round(hrg);
	vdis1=vdis1+((hrg*ndis1)/100);
	vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
	vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
	vdis4=vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100);
	vdistot	= Math.round(vdis1+vdis2+vdis3+vdis4);
	vhrgreal= Math.round(hrg-vdistot);
	document.getElementById("vspbdiscounttotalafter").value=formatcemua(vdistot);
	document.getElementById("vspbafter").value=formatcemua(vhrgreal);
	var fppn = document.getElementById("f_spb_plusppn").value;
	var fdis = document.getElementById("f_spb_plusdiscount").value;
	var bersih = parseFloat(formatulang(document.getElementById("vspbafter").value));
	var kotor  = hrg;//parseFloat(formatulang(document.getElementById("vspb").value));
	if( (fppn=='t') && (fdis=='f') ){
		document.getElementById("nprice").value=1;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='t') && (fdis=='t') ){
		document.getElementById("nprice").value=bersih/kotor;
		document.getElementById("vnotappn").value=0;
	}else if( (fppn=='f') && (fdis=='t') ){
		document.getElementById("nprice").value=1/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}else if( (fppn=='f') && (fdis=='f') ){
		document.getElementById("nprice").value=(bersih/kotor)/1.1;
		document.getElementById("vnotappn").value=bersih*0.1;
	}
  }
  function dipales(){
  	if(document.getElementById("dnota").value!=''){
   		document.getElementById("login").disabled=true;
	}else{
		document.getElementById("login").disabled=false;
		alert("Tanggal nota tidak boleh kosong");
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function back(){
  	self.location="<?php echo site_url().'/nota/cform'; ?>";
  }
  function editnilai(){
    var fppn = document.getElementById("fspbplusppn").value;
    var jml 	= parseFloat(document.getElementById("jml").value);
    var totdis 	= 0;
    var totnil	= 0;
    var hrg		= 0;
    var ndis1	= parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
    var ndis2	= parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
    var ndis3	= parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
    var ndis4	= parseFloat(formatulang(document.getElementById("ncustomerdiscount4").value));
    if( (ndis1+ndis2+ndis3+ndis4==0) ){
      vdis1=parseFloat(formatulang(document.getElementById("vspbdiscounttotalafter").value));
      document.getElementById("vspbdiscounttotal").value=document.getElementById("vspbdiscounttotalafter").value;
    }else{
      vdis1=0;
    }
//    alert(vdis1);
//    var vdis1	= 0;
    var vdis2	= 0;
    var vdis3	= 0;
    var vdis4	= 0;
    for(i=1;i<=jml;i++){
      if(fppn=='f'){
        var vhrg = parseFloat(formatulang(document.getElementById("vproductretail"+i).value))/1.1;
      }else{
        var vhrg = parseFloat(formatulang(document.getElementById("vproductretail"+i).value));
      }
      qty=formatulang(document.getElementById("ndeliver"+i).value);
      hrgtmp=parseFloat(vhrg)*parseFloat(qty);
//      	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("ndeliver"+i).value));
	    hrg			= hrg+hrgtmp;
    }
//    hrg=Math.round(hrg);
    if(ndis1>0) vdis1=vdis1+(hrg*ndis1)/100;//Math.round(vdis1+(hrg*ndis1)/100);
    vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);//Math.round(vdis2+(((hrg-vdis1)*ndis2)/100));
    vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);//Math.round(vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100));
    vdis4=vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100);//Math.round(vdis4+(((hrg-(vdis1+vdis2+vdis3))*ndis4)/100));
//    if(fppn=='t'){
      vdistot	= Math.round(vdis1+vdis2+vdis3+vdis4);
//    }else{
//      vdistot	= vdis1+vdis2+vdis3+vdis4;
//    }

    nTDisc1  = ndis1  + ndis2  * (100-ndis1)/100;
    nTDisc2  = ndis3  + ndis4  * (100-ndis3)/100;
    nTDisc   = nTDisc1 + nTDisc2 * (100-nTDisc1)/100;
//    vdistot = Math.round(nTDisc * hrg / 100);
//    vdistot = (nTDisc * hrg)/ 100;
//    alert(vdistot);
    if( (fppn=='f') && (ndis1==0) ){
      vdistot=Math.round(vdistot/1.1);
    }    
//    alert(vdistot);
//    vhrgreal= hrg-vdistot;//Math.round(hrg-vdistot);
    document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
    document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
    document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
    document.getElementById("vcustomerdiscount4").value=formatcemua(Math.round(vdis4));
    if(document.getElementById("fspbconsigment").value=='f'){
      if(fppn=='f'){
//        vppn=(Math.round(hrg)-Math.round(vdistot))*0.1;
        vppn=Math.round((hrg-vdistot)*0.1);
//        vtotbersih=Math.round(hrg)-Math.round(vdistot)+Math.round(vppn);
        vtotbersih=hrg-vdistot+vppn;
      }else{
        vtotbersih=parseFloat(hrg)-parseFloat(Math.round(vdistot));
      }
      document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vdistot));
//      document.getElementById("vspbafter").value=formatcemua(Math.round(vhrgreal));
      document.getElementById("vspbafter").value=formatcemua(Math.round(vtotbersih));
    }
    var fdis = document.getElementById("fspbplusdiscount").value;
    var bersih = parseFloat(formatulang(document.getElementById("vspbafter").value));
    var kotor  = parseFloat(formatulang(document.getElementById("vspb").value));
    if( (fppn=='t') && (fdis=='f') ){
	    document.getElementById("nprice").value=1;
	    document.getElementById("vnotappn").value=0;
    }else if( (fppn=='t') && (fdis=='t') ){
	    document.getElementById("nprice").value=bersih/kotor;
	    document.getElementById("vnotappn").value=0;
    }else if( (fppn=='f') && (fdis=='t') ){
	    document.getElementById("nprice").value=1/1.1;
	    kotorminppn=Math.round(hrg/1.1);
      if(document.getElementById("fspbconsigment").value=='f'){
	      document.getElementById("vspbdiscounttotalafter").value=0;
	      document.getElementById("vnotappn").value=formatcemua(Math.round(vppn));
      }
    }else if( (fppn=='f') && (fdis=='f') ){
	    document.getElementById("nprice").value=1/1.1;
      if(document.getElementById("fspbconsigment").value=='f'){
	      document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vdistot));
	      document.getElementById("vspbafter").value=formatcemua(Math.round(vtotbersih));
	      document.getElementById("vnotappn").value=formatcemua(Math.round(vppn));
      }else{
        vdistot=vdistot*1.1;
        hrg=hrg*1.1;
        vppn=(Math.round(hrg)-Math.round(vdistot))*0.1;
        vtotbersih=Math.round(Math.round(hrg)-Math.round(vdistot)+vppn);
        document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vdistot));
	      document.getElementById("vspbafter").value=formatcemua(vtotbersih);

      }
    }
  }
  function pricegroup()
  {
    nota=document.getElementById("inota").value;
    if(nota=='')nota=document.getElementById("ispb").value;
    area=document.getElementById("iarea").value;
    showModal("nota/cform/pricegroup/"+nota+"/"+area,"#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  function tungnilai(){
    jml=document.getElementById("jml").value;
	  dtmp1=parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
	  dtmp2=parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
	  dtmp3=parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
	  vdis1=0;
	  vdis2=0;
	  vdis3=0;
	  vdis1x=0;
	  vdis2x=0;
	  vdis3x=0;
	  vtot =0;
	  vtotx=0;
	  for(i=1;i<=jml;i++){
		  vhrg=formatulang(document.getElementById("vproductretail"+i).value);
		  vhrgx=formatulang(document.getElementById("vproductretail"+i).value);
		  nqty=formatulang(document.getElementById("norder"+i).value);
      nqtyx=formatulang(document.getElementById("ndeliver"+i).value);
		  vhrg=parseFloat(vhrg)*parseFloat(nqty);
		  vhrgx=parseFloat(vhrgx)*parseFloat(nqtyx);
		  vtot=vtot+vhrg;
		  vtotx=vtotx+vhrgx;
		  document.getElementById("vtotal"+i).value=formatcemua(vhrg);
	  }
	  vdis1=vdis1+((vtot*dtmp1)/100);
	  vdis2=vdis2+(((vtot-vdis1)*dtmp2)/100);
	  vdis3=vdis3+(((vtot-(vdis1+vdis2))*dtmp3)/100);
	  vdis1x=vdis1x+((vtotx*dtmp1)/100);
	  vdis2x=vdis2x+(((vtotx-vdis1x)*dtmp2)/100);
	  vdis3x=vdis3x+(((vtotx-(vdis1x+vdis2x))*dtmp3)/100);

	  document.getElementById("vcustomerdiscount1").value=formatcemua(Math.round(vdis1));
	  document.getElementById("vcustomerdiscount2").value=formatcemua(Math.round(vdis2));
	  document.getElementById("vcustomerdiscount3").value=formatcemua(Math.round(vdis3));
	  vdis1=parseFloat(vdis1);
	  vdis2=parseFloat(vdis2);
	  vdis3=parseFloat(vdis3);
	  vtotdis=vdis1+vdis2+vdis3;
	  vdis1x=parseFloat(vdis1x);
	  vdis2x=parseFloat(vdis2x);
	  vdis3x=parseFloat(vdis3x);
	  vtotdisx=vdis1x+vdis2x+vdis3x;

	  if(document.getElementById("fspbconsigment").value=='f'){
	    document.getElementById("vspbdiscounttotal").value=formatcemua(Math.round(vtotdis));
	    document.getElementById("vspb").value=formatcemua(vtot);
	    vtotbersih=parseFloat(formatulang(formatcemua(vtot)))-parseFloat(formatulang(formatcemua(Math.round(vtotdis))));
	    document.getElementById("vspbbersih").value=formatcemua(vtotbersih);
      document.getElementById("vspbdiscounttotalafter").value=formatcemua(Math.round(vtotdisx));
	    vtotbersihx=parseFloat(formatulang(formatcemua(vtotx)))-parseFloat(formatulang(formatcemua(Math.round(vtotdisx))));
	    document.getElementById("vspbafter").value=formatcemua(vtotbersihx);
    }else{
      document.getElementById("vspbdiscounttotalafter").value=document.getElementById("vspbdiscounttotal").value;
    }
  }
  
  function afterSetDateValue(ref_field, target_field, date) {
    if (date!="") {
      var startDate=document.getElementById('dnota').value;
      var endDate=document.getElementById('dtmp').value;
      tes=startDate.split('-');
      startDate=tes[2]+'-'+tes[1]+'-'+tes[0];
      tesx=endDate.split('-');
      endDate=tesx[2]+'-'+tesx[1]+'-'+tesx[0];
      if(startDate<endDate){
        tes=endDate.split('-');
        dnota=tes[2]+'-'+tes[1]+'-'+tes[0];
        alert("Tidak boleh lebih kecil dari Tanggal DKB = "+dnota+" !!!");
        document.getElementById('dnota').value=dnota;
      }else if(tes[1]!=tesx[1]){
        tes=endDate.split('-');
        dnota=tes[2]+'-'+tes[1]+'-'+tes[0];
        alert("Harus dalam periode yang sama !!!");
        document.getElementById('dnota').value=dnota;
      }
    }
  }
</script>
