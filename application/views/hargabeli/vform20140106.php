<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Harga Beli</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Harga Beli</a></li>  
	  </ul>  
  <div id="content_1" class="content">
  <table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'hargabeli/cform/cari','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
   	    <th align=center>Kode</th>
   	    <th align=center>Nama Barang</th>
   	    <th align=center>Kode Harga</th>
	    <th align=center>Grade</th>
	    <th align=center>Harga</th>
	    <th align=center>Tgl.Daftar</th>	
	    <th align=center>Tgl.Perubahan</th>
	    <th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if($row->d_product_priceentry!=''){
			    $tmpent=explode(' ',$row->d_product_priceentry);
			    $tmpentry=explode('-',$tmpent[0]);
			    $tglentry=$tmpentry[2];
			    $blnentry=$tmpentry[1];
			    $thnentry=$tmpentry[0];
			    $row->d_product_priceentry=$tglentry.'-'.$blnentry.'-'.$thnentry;
        }
			  
			  if($row->d_product_priceupdate!='') {
				  $tmpupt=explode(' ',$row->d_product_priceupdate);
				  $tmpupdate=explode('-',$tmpupt[0]);
				  $tglupt=$tmpupdate[2];
				  $blnupt=$tmpupdate[1];
				  $thnupt=$tmpupdate[0];
				  $d_product_priceupdate = $tglupt.'-'.$blnupt.'-'.$thnupt;
			  } else {
				  $d_product_priceupdate = '';
			  }

			  echo "<tr>
				  <td>$row->i_product</td>
				  <td>$row->e_product_name</td>
				  <td>$row->i_price_group</td>
				  <td>$row->i_product_grade</td>
				  <td align=right>".number_format($row->v_product_mill,2)."</td>";
			  echo	  "<td align=center>$row->d_product_priceentry</td>
				   <td align=center>$d_product_priceupdate</td>";
			  echo "<td class=\"action\"><a href=\"#\" onclick='show(\"hargabeli/cform/edit/$row->i_product/$row->i_price_group\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";#"&nbsp;&nbsp;";
#			  echo "<a href=\"#\" onclick='hapus(\"hargabeli/cform/delete/$row->i_product/$row->i_price_group\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
  </table>
  </div>
  <div id="content_2" class="content">
  <table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'hargabeli/cform/simpan','update'=>'#main','type'=>'post'));?>
    <div id="masterproductpriceform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable">
	      <tr>
		<td width="12%">Kode Barang</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly type="text" name="iproduct" id="iproduct" value="" 
				 onclick='showModal("hargabeli/cform/product/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Kode Harga</td>
		<td width="1%">:</td>
		<td width="87%"><input maxlength=2 type="text" name="ipricegroup" id="ipricegroup" value=""></td>
	      </tr>
	      <tr>
		<td width="12%">Nama Barang</td>
		<td width="1%">:</td>
		<td width="87%"><input readonly name="eproductname" id="eproductname" value="" 
				 onclick='showModal("hargabeli/cform/product/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Kode Grade</td>
		<td width="1%">:</td>
		<td width="87%"><input type="hidden" name="iproductgrade" id="iproductgrade" value="" 
				 onclick='showModal("hargabeli/cform/productgrade/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				 <input readonly name="eproductgradename" id="eproductgradename" value="" 
				 onclick='showModal("hargabeli/cform/productgrade/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      </tr>
	      <tr>
		<td width="12%">Harga Pabrik</td>
		<td width="1%">:</td>
		<td width="87%"><?php 
				$data = array(
			              'name'        => 'vproductmill',
			              'id'          => 'vproductmill',
			              'value'       => '',
				      'onkeyup'	    => 'reformat(this)',
			              'maxlength'   => '9');
				echo form_input($data);?></td>
	      </tr>
	      <tr>
		<td width="10%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="89%">
		  <input name="login" id="login" value="Simpan" type="submit">
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('hargabeli/cform/','#main');">
		</td>
		</td>
	      </tr>
	    </table>
	  </div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
  </table>
  </div>
</div>
</div>
