<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*/
*{
size: landscape;
}

@page { size: Letter; }

.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisy { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garisy td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
.garisatas { 
	border-top:#000000 0.1px solid;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
  if($row->d_ic_convertion){
    $tmp=explode('-',$row->d_ic_convertion);
    $tgl=$tmp[2];
    $bln=$tmp[1];
    $thn=$tmp[0];
    $row->d_ic_convertion=$tgl.'-'.$bln.'-'.$thn;
  }
  if($row->d_refference!='' or $row->d_refference!=null){
    $tmp=explode('-',$row->d_refference);
    $tgl=$tmp[2];
    $bln=$tmp[1];
    $thn=$tmp[0];
    $row->d_refference=$tgl.'-'.$bln.'-'.$thn;
  }else{
    $row->d_refference='';
  }
?>
  <table width="76%" border="0" class="nmper">
    <tr>
      <td colspan="4" class="huruf judul" ><?php echo NmPerusahaan; ?></td>
    </tr>
    <tr>
      <td colspan="4" class="huruf nmper"><b>Konversi Stock</b></td>
    </tr>
    <tr>
      <td width="220" class="huruf nmper">No Konversi </td>
      <td width="13" align="center" class="huruf nmper">:</td>
      <td width="225" class="huruf nmper"><?php echo $row->i_ic_convertion;?></td>
      <td width="559" class="huruf nmper">&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper">Tanggal</td>
      <td align="center" class="huruf nmper">:</td>
      <td class="huruf nmper"><?php echo $row->d_ic_convertion;?></td>
      <td class="huruf nmper">&nbsp;</td>
    </tr>
    <tr>
      <td class="huruf nmper">Referensi</td>
      <td align="center" class="huruf nmper">:</td>
      <td class="huruf nmper" colspan=2><?php echo $row->i_refference;?> / <?php echo $row->d_refference;?></td>
    </tr>
</table>
<br>
<table>
  <tr>
    <td colspan=5 class=garisbawah><div align="center">*** Produk Asal *** </div></td>
  </tr>
  <tr>
    <td width="39" class="huruf nmper garisbawah">No</td>
    <td width="142" class="huruf nmper garisbawah">Kode Barang</td>
    <td width="370" class="huruf nmper garisbawah">Nama Barang</td>
    <td width="117" class="huruf nmper garisbawah">Grade</td>
    <td width="103" class="huruf nmper garisbawah">Jumlah</td>
  </tr>
</table>
<?php 
    if($row->f_ic_convertion=='t'){
		  $i=0;	
?>		  
<table width="793">
  <tr>
    <td width="39" class="huruf nmper">1</td>
    <td width="142" class="huruf nmper"><?php echo $row->i_product; ?></td>
    <td width="370" class="huruf nmper"><?php echo $row->e_product_name; ?></td>
    <td width="119" class="huruf nmper"><?php echo $row->i_product_grade; ?></td>
    <td width="99" class="huruf nmper"><?php echo $row->n_ic_convertion; ?></td>
  </tr>
  <tr>
    <td colspan=5 class=garisatas>&nbsp;</td>
  </tr>
</table>
<br>
<?php 
		  foreach($detail as $rowi){
			  $i++;
?>
<table>
  <tr>
    <td colspan=5 class=garisbawah><div align="center">*** Produk Jadi *** </div></td>
  </tr>
  <tr>
    <td width="56" class="huruf nmper garisbawah">No</td>
    <td width="125" class="huruf nmper garisbawah">Kode Barang</td>
    <td width="370" class="huruf nmper garisbawah">Nama Barang</td>
    <td width="117" class="huruf nmper garisbawah">Grade</td>
    <td width="103" class="huruf nmper garisbawah">Jumlah</td>
  </tr>
</table>
<table width="794">
  <tr>
    <td width="54" class="huruf nmper"><?php echo $i; ?></td>
    <td width="129" class="huruf nmper"><?php echo $rowi->i_product; ?></td>
    <td width="371" class="huruf nmper"><?php echo $rowi->e_product_name; ?></td>
    <td width="114" class="huruf nmper"><?php echo $rowi->i_product_grade; ?></td>
    <td width="100" class="huruf nmper"><?php echo $rowi->n_ic_convertion; ?></td>
  </tr>
  <tr>
    <td colspan=5 class=garisatas>&nbsp;</td>
  </tr>
</table>
<br>
<?php 
    }
  }else{
		  $i=0;	
		  foreach($detail as $rowi){
			  $i++;
?>
<table width="794">
  <tr>
    <td width="54" class="huruf nmper"><?php echo $i; ?></td>
    <td width="129" class="huruf nmper"><?php echo $rowi->i_product; ?></td>
    <td width="371" class="huruf nmper"><?php echo $rowi->e_product_name; ?></td>
    <td width="114" class="huruf nmper"><?php echo $rowi->i_product_grade; ?></td>
    <td width="100" class="huruf nmper"><?php echo $rowi->n_ic_convertion; ?></td>
  </tr>
<?php 
    }
?>
</table>
<table width="794">
  <tr>
    <td colspan=5 class=garisatas>&nbsp;</td>
  </tr>
</table>
<table>
  <tr>
    <td colspan=5 class=garisbawah><div align="center">*** Produk Jadi *** </div></td>
  </tr>
  <tr>
    <td width="56" class="huruf nmper garisbawah">No</td>
    <td width="125" class="huruf nmper garisbawah">Kode Barang</td>
    <td width="370" class="huruf nmper garisbawah">Nama Barang</td>
    <td width="117" class="huruf nmper garisbawah">Grade</td>
    <td width="103" class="huruf nmper garisbawah">Jumlah</td>
  </tr>
</table>
<table width="794">
  <tr>
    <td width="54" class="huruf nmper">1</td>
    <td width="129" class="huruf nmper"><?php echo $row->i_product; ?></td>
    <td width="371" class="huruf nmper"><?php echo $row->e_product_name; ?></td>
    <td width="114" class="huruf nmper"><?php echo $row->i_product_grade; ?></td>
    <td width="100" class="huruf nmper"><?php echo $row->n_ic_convertion; ?></td>
  </tr>
  <tr>
    <td colspan=5 class=garisatas>&nbsp;</td>
  </tr>
</table>

<?php    
  }
}
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
