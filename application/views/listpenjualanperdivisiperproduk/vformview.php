<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<h3><?php 
echo 'Periode '.$iperiode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listpenjualanperdivisiperproduk/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
?>
	  <th>Kode</th>
		<th>Nama Barang</th>
    <th>Qty Penjualan</th>
    <th>Qty Sisa Saldo</th>
    <th>Qty Git Stock </th>
    <?php 
	    echo '<tbody>';
      $group='';
      $grandt=0;
      $grandtn=0;
      $grandts=0;
      $i=0;
      foreach($prodnya as $row)
      {
        $total = 0;
        $totaln = 0;
        $totals = 0;
        if($group==''){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
        }
        if($group!='' && $group!=$row->e_product_groupname){
          echo "<tr><td colspan=5><h2>$row->e_product_groupname</h2></td></tr>";
        }
          foreach($isi as $riw)
          {
              if($riw->i_product_group == $row->i_product_group){
              $i++;
              $ada=true;
              echo "<tr><td align=right>".($riw->i_product)."</td>";
              echo "<td align=right>".($riw->e_product_name)."</td>";
              echo "<td align=right>".number_format($riw->jumlah)."</td>";
              echo "<td align=right>".number_format($riw->sisasaldo)."</td>";
              echo "<td align=right>".number_format($riw->git)."</td></tr>";
              $total=$total+$riw->jumlah;
              $totaln=$totaln+$riw->sisasaldo;
              $totals=$totals+$riw->git;
              $grandt=$grandt+$riw->jumlah;
              $grandtn=$grandtn+$riw->sisasaldo;
              $grandts=$grandts+$riw->git;
            }
          }
        echo "<tr><td colspan=2 align=right><b>Total Divisi</td>
              <td align=right><b>".number_format($total)."</td>
              <td align=right><b>".number_format($totaln)."</td>
              <td align=right><b>".number_format($totals)."</td></tr>";
              $group=$row->e_product_groupname;
      }
        echo "<tr><td colspan=2 align=right><b>Grand Total</td>
                  <td align=right><b>".number_format($grandt)."</td>
                  <td align=right><b>".number_format($grandtn)."</td>
                  <td align=right><b>".number_format($grandts)."</td></tr>";
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
//    alert(''+Contents);
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
//    alert('exporting records...');
  });
</script>
