    <table class="maintable">
      <tr>
    	<td align="left">
    		<!--       <?php echo form_open('karyawancuti/cform/update', array('id' => 'karyawancutiupdate', 'name' => 'karyawancutiupdate', 'onsubmit' => 'sendRequest(); return false'));?> -->
	      <?php echo $this->pquery->form_remote_tag(array('url'=>'karyawancuti/cform/update','update'=>'#main','type'=>'post'));?>
	      <div id="masterkaryawanform">
	      <table class="mastertable">
	      	<tr>
		  <td width="19%">NIK</td>
		  <td width="1%">:</td>
		  <td width="30%">
		     <input type="text" name="i_nik" id="i_nik" value="<?php echo $isi->i_nik;?>" readonly>
		  </td>
		  </td>
		  <td width="19%">Nama Karyawan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nama_karyawan_lengkap" id="e_nama_karyawan_lengkap" value="<?php echo $isi->e_nama_karyawan_lengkap;?>" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Department</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    	  <input type="hidden" name="i_department" id="i_department" value="<?php echo $isi->i_department;?>" 
					   onclick='showModal("karyawan/cform/department/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_department_name" id="e_department_name" value="<?php echo $isi->e_department_name;?>" 
					   onclick='showModal("karyawan/cform/department/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Nama Panggilan</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nama_karyawan_panggilan" id="e_nama_karyawan_panggilan" value="<?php echo $isi->e_nama_karyawan_panggilan;?>" maxlength='15'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Area</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="i_area" id="i_area" value="<?php echo $isi->i_area;?>" 
					   onclick='showModal("karyawan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_area_name" id="e_area_name" value="<?php echo $isi->e_area_name;?>" 
					   onclick='showModal("karyawan/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Jenis Kelamin</td>
		  <td width="1%">:</td>
		  <td width="30%">
		  	<select name="e_jenis_kelamin" id="e_jenis_kelamin">
		  		<?php if ($isi->e_jenis_kelamin=='P'){?>
		  		<option selected value="P">Perempuan</option>
		  		<?php }else{?>
		  		<option selected value="L">Laki-Laki</option>
		  		<?php }?>
		  	</select>
		  	</td>
	      	<tr>
		  <td width="19%">Status Karyawan</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="i_karyawan_status" id="i_karyawan_status" value="<?php echo $isi->i_karyawan_status;?>" 
					   onclick='showModal("karyawan/cform/karyawan_status/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="e_karyawan_status" id="e_karyawan_status" value="<?php echo $isi->e_karyawan_status;?>" 
					   onclick='showModal("karyawan/cform/karyawan_status/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Hak Cuti Lebaran</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="v_cuti_lebaran" id="v_cuti_lebaran" value="<?php echo $isi->v_cuti_lebaran;?>">
		    <input type="hidden" name="v_cuti_lebaranx" id="v_cuti_lebaranx" value="<?php echo $isi->v_cuti_lebaran;?>"><b><i> /Hari</i></b></td>
		     	</tr>
	      	<tr>
		  <td width="19%">No Telepon</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="e_nomor_telepon" id="e_nomor_telepon" value="<?php echo $isi->e_nomor_telepon;?>"></td>
		  <td width="19%">jumlah Saldo Cuti</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input readonly type="text" name="v_saldo_cuti" id="v_saldo_cuti" value="<?php echo $isi->v_saldo_cuti;?>"><b><i> /Hari</i></b></td>
		  </tr>
	    <tr>
		  <td class="batas" width="100%" colspan="4">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<?php if($isi->v_saldo_cuti!='0'){?>
		    <input name="login" 	 id="login" 	 value="Simpan" 	 type="submit" onclick="dipales();">
		    <?php }else{?>
		    <input disabled="true" name="login" 	 id="login" 	 value="Simpan" 	 type="submit" onclick="dipales();">
		    <?php } ?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("karyawancuti/cform/","#main")'>
			</td>
		</tr>
	      <?=form_close()?>
        </td>
      </tr> 
    </table>
	<div id="detailheader" align="center">
				<table class="listtable" style="width:920px;">
          <th style="width:25px;" align="center">No</th>
					<th style="width:75px;" align="center">Nik</th>
					<th style="width:75px;" align="center">Tanggal Cuti Awal</th>
					<th style="width:70px;" align="center">Tanggal Cuti Akhir</th>
					<th style="width:25px;" align="center">Jumlah Cuti</th>
					<th style="width:150px;" align="center">Keterangan</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 	
				if($detail!=NULL){			
				$i=0;
				foreach($detail as $row)
				{
			  	$i++;
          echo "<table class=\"listtable\" style=\"width:920px;\">";
			  	echo '<tbody>
							<tr>
		    				<td style="width:21px;"><input style="font-size:12px; width:21px;" readonly type="text" 
								id="baris'.$i.'" name="baris'.$i.'" value="'.$i.'"></td>
							<td style="width:75px;"><input style="font-size:12px; width:75px;" readonly type="text" 
								id="i_nik'.$i.'" name="i_nik'.$i.'" value="'.$row->i_nik.'"></td>
							<td style="width:75px;"><input style="font-size:12px; width:75px;" readonly type="text" 
								id="d_cuti_awal'.$i.'"
								name="d_cuti_awal'.$i.'" value="'.$row->d_cuti_awal.'"></td>
							<td style="width:75px;"><input readonly style="font-size:12px; width:75px;" type="text" 
								id="d_cuti_akhir'.$i.'"
								name="d_cuti_akhir'.$i.'" value="'.$row->d_cuti_akhir.'"></td>
							<td style="width:25px;"><input readonly style="font-size:12px; text-align:right; 
								width:25px;"  type="text" id="v_jumlah_cuti'.$i.'"
								name="v_jumlah_cuti'.$i.'" value="'.$row->v_jumlah_cuti.'"></td>
							<td style="width:135px;"><input style="font-size:12px; text-align:right; width:150px;"
								type="text" id="e_alasan'.$i.'" name="e_alasan'.$i.'" value="'.$row->e_alasan.'"></td>
							</tr>
						  </tbody></table>';
				}
			}else{


			}
				?>
			</div>
		</div>
</div>
<script language="javascript" type="text/javascript">
  function cek(){
    kode=document.getElementById("iproduct").value;
    nama=document.getElementById("eproductname").value;
    if(kode=='' || nama==''){
	alert("Minimal kode Produk dan nama Produk diisi terlebih dahulu !!!");
    }
  }
</script>
