<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'cekknalokasi/cform/updatepelunasan','update'=>'#pesan','type'=>'post'));?>
	<div id="pelunasanform">
	<div class="effect">
	  <div class="accordion2">
		<?php foreach($isi as $row){
			  if($row->d_alokasi!=''){
				  $tmp=explode('-',$row->d_alokasi);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $tah=$tmp[0];
				  $row->d_alokasi=$tgl.'-'.$bln.'-'.$thn;
			  }
		?>
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>No Alokasi / Tgl</td>
		<td><input type="text" id="ialokasi" name="ialokasi" value="<?php echo $row->i_alokasi; ?>" readonly>
			<input type="hidden" id="ikn" name="ikn" value="<?php echo $row->i_kn; ?>" readonly>
			<input readonly id="dalokasi" name="dalokasi" value="<?php echo $row->d_alokasi; ?>">
			</td>			
		<td>Debitur</td>
		<td><input type="text" readonly id="ecustomername" name="ecustomername" value="<?php echo $row->e_customer_name; ?>">
			<input type="hidden" readonly id="icustomer" name="icustomer" value="<?php echo $row->i_customer; ?>"></td>
	      </tr>
	      <tr>
		<td>KN / Tgl </td>
		<td><input readonly id="ikn" name="ikn" value="<?php echo $row->i_kn; ?>">
        <input readonly id="dkn" name="dkn" value="<?php echo $row->d_kn; ?>"></td>
    <td>Alamat</td>
		<td><input id="ecustomeraddress" name="ecustomeraddress" readonly value="<?php echo $row->e_customer_address; ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $row->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $row->i_area; ?>">
			<input type="hidden" id="nkuyear" name="nkuyear" value="<?php if(isset($tah)) echo $tah; ?>"></td>
		<td>Kota</td>
		<td><input readonly id="ecustomercity" name="ecustomercity" value="<?php echo $row->e_customer_city; ?>"></td>
		    </tr>
	      <tr>
		<td>Jumlah</td>
		<td><input readonly type="text" id="vjumlah" name="vjumlah" value="<?php echo number_format($row->v_jumlah); ?>" onKeyUp="reformat(this);hitung();">
			<input type="hidden" id="vsisa" name="vsisa" value="0" >
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>"></td>
	  <td>Lebih</td>
		<td><input type="text" readonly id="vlebih" name="vlebih" value="<?php echo number_format($row->v_lebih); ?>" ></td>
	      </tr>
	      <tr>
    <td>Ket Cek</td>
		<td><input name="ecek1" id="ecek1" value="<?php echo $row->e_cek; ?>" type="text"></td>
		<td>Pembulatan</td>
		<td><input type="text" readonly id="vbulat" name="vbulat" value="0" ></td>
	      </tr>

		<tr>
		  <td width="100%" align="center" colspan="4">
		  <?php if($row->i_cek==''){?>
		      <input name="login" id="login" value="Cek" type="submit" onclick="dipales()">
		  <?php }?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button"  onclick="show('cekknalokasi/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>','#main')">
      </td>
		</tr>
	    </table>
		<?php }?>
			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:920px;">
					<th style="width:30px;" align="center">No</th>
					<th style="width:120px;" align="center">Nota</th>
					<th style="width:80px;" align="center">Tgl Nota</th>
					<th style="width:130px;" align="center">Nilai</th>
					<th style="width:130px;" align="center">Bayar</th>
					<th style="width:130px;" align="center">Sisa</th>
		      <th style="width:170px;" align="center">Ket2</th>
					<th style="width:60px;" align="center">Act</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 
					$i=0;
					foreach($detail as $row){ 
					$i++;
					  if($row->d_nota!=''){
						  $tmp=explode('-',$row->d_nota);
						  $tgl=$tmp[2];
						  $bln=$tmp[1];
						  $thn=$tmp[0];
						  $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
					  }
				?><table id="itemtem" class="listtable" style="width:920px;"><tbody><tr><td style="width:29px;"><input style="width:29px;" readonly type="text" id="baris<?php echo $i; ?>" name="baris<?php echo $i; ?>" value="<?php echo $i; ?>"></td><td style="width:122px;"><input style="width:122px;" readonly type="text" id="inota<?php echo $i; ?>" name="inota<?php echo $i; ?>" value="<?php echo $row->i_nota; ?>"></td><td style="width:82px;"><input style="width:82px;" readonly type="text" id="dnota<?php echo $i; ?>" name="dnota<?php echo $i; ?>" value="<?php echo $row->d_nota; ?>"></td><td style="width:134px;"><input readonly style="width:134px;text-align:right;" type="text" id="vnota<?php echo $i; ?>" name="vnota<?php echo $i; ?>" value="<?php echo number_format($row->v_nota); ?>"></td><td style="width:132px;"><input style="width:132px;text-align:right;" type="text" id="vjumlah<?php echo $i; ?>" name="vjumlah<?php echo $i; ?>" value="<?php echo number_format($row->v_jumlah); ?>" readonly></td><td style="width:136px;"><input type="hidden" id="vsesa<?php echo $i; ?>" name="vsesa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa_nota); ?>"><input readonly style="width:136px;text-align:right;" type="text" id="vsisa<?php echo $i; ?>" name="vsisa<?php echo $i; ?>" value="<?php echo number_format($row->v_sisa_nota); ?>"><input type="hidden" id="vlebih<?php echo $i; ?>" name="vlebih<?php echo $i; ?>" value=""><input type="hidden" id="vasal<?php echo $i; ?>" name="vasal<?php echo $i; ?>" value="<?php echo $row->v_jumlah+$row->v_sisa_nota; ?>"></td><td style="width:174px;"><input readonly style="width:174px;" type="text" id="eremark<?php echo $i; ?>" name="eremark<?php echo $i; ?>" value="<?php echo $row->e_remark; ?>"></td><td style="width:76px;"></td></tr></tbody></table>
				<?php }?>
			</div>
	  </div>
	</div>
	</div>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
	cek='false';
	cok='false';
	if(
    (document.getElementById("ikbank").value!='') &&
	  (document.getElementById("dbank").value!='') &&
	  (document.getElementById("dalokasi").value!='') &&
	  (document.getElementById("vjumlah").value!='') &&
	  (document.getElementById("vjumlah").value!='0') &&
	  (document.getElementById("icustomer").value!='')
    ) {
	  var a=parseFloat(document.getElementById("jml").value);
	  for(i=1;i<=a;i++){
		  if(document.getElementById("vjumlah"+i).value!='0'){
        sisa=parseFloat(formatulang(document.getElementById("vsisa"+i).value));
        awal=parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
        if( (sisa-awal>0) ){
          alert('Keterangan sisa wajib diisi !!!');
          cok='false';
          cek='false';
          break;
        }else{
		      cok='true';
          cek='true';
        }
		  }else{
		    cek='false';3
		  }
	  }
	  if(cek=='true'){
	    document.getElementById("login").disabled=true;
	    document.getElementById("cmdtambahitem").disabled=true;
	  }else if(cok=='false'){
	  }else{
		  alert('Isi jumlah detail pelunasan minimal 1 item !!!');
	    document.getElementById("login").disabled=false;
	  }
	}else{
	  alert('Data header masih ada yang salah !!!');
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
//    alert(si_inner);
    if(so_inner==''){
		  so_inner = '<table id="itemtem" class="listtable" style="width:920px;">';
		  so_inner+= '<th style="width:30px;" align="center">No</th>';
		  so_inner+= '<th style="width:120px;" align="center">Nota</th>';
		  so_inner+= '<th style="width:80px;" align="center">Tgl Nota</th>';
		  so_inner+= '<th style="width:130px;" align="center">Nilai</th>';
		  so_inner+= '<th style="width:130px;" align="center">Bayar</th>';
		  so_inner+= '<th style="width:130px;" align="center">Sisa</th>';
		  so_inner+= '<th style="width:170px;" align="center">Ket2</th>';
		  so_inner+= '<th style="width:60px;" align="center">Act</th>';
		  document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		  so_inner='';
    }
    if(si_inner==''){
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		  juml=document.getElementById("jml").value;
		  si_inner+='<table id="itemtem3" class="listtable" style="width:920px;"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		  si_inner+='<td style="width:122px;"><input style="width:122px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""></td2,103,030>';
		  si_inner+='<td style="width:82px;"><input style="width:82px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:134px;"><input readonly style="width:134px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:132px;"><input style="width:132px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetang('+a+');" onpaste="return false;"></td>';
		  si_inner+='<td style="width:136px;"><input readonly style="width:136px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""><input type="hidden" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
		  si_inner+='<td style="width:174px;"><input style="width:174px;font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td><td style="width:76px;"></td></tr></tbody></table>';
    }else{
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		  juml=document.getElementById("jml").value;
		  si_inner+='<table id="itemtem" class="listtable" style="width:920px;"><tbody><tr><td style="width:29px;"><input style="width:29px;font-size:12px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		  si_inner+='<td style="width:122px;"><input style="width:122px;font-size:12px;" readonly type="text" id="inota'+a+'" name="inota'+a+'" value=""></td>';
		  si_inner+='<td style="width:82px;"><input style="width:82px;font-size:12px;" readonly type="text" id="dnota'+a+'" name="dnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:134px;"><input readonly style="width:134px;text-align:right;font-size:12px;" type="text" id="vnota'+a+'" name="vnota'+a+'" value=""></td>';
		  si_inner+='<td style="width:132px;"><input style="width:132px;text-align:right;font-size:12px;" type="text" id="vjumlah'+a+'" name="vjumlah'+a+'" value="" onkeydown="reformat(this);" onKeyUp="hetang('+a+');" onpaste="return false;"></td>';
		  si_inner+='<td style="width:136px;"><input readonly style="width:136px;text-align:right;font-size:12px;" type="text" id="vsesa'+a+'" name="vsesa'+a+'" value=""><input type="hidden" id="vsisa'+a+'" name="vsisa'+a+'" value=""><input type="hidden" id="vlebih'+a+'" name="vlebih'+a+'" value=""></td>';
		  si_inner+='<td style="width:174px;"><input style="width:174px;font-size:12px;" type="text" id="eremark'+a+'" name="eremark'+a+'" value=""></td><td style="width:76px;"></td></tr></tbody></table>';
    }
    j=0;
    var baris			= Array();
    var inota			= Array();
    var dnota			= Array();
    var vnota			= Array();
  	var vjumlah		= Array();
    var vsesa			= Array();
    var vsisa			= Array();
    var vlebih		= Array();
    var eremark		= Array();

    for(i=1;i<a;i++){
		  j++;
		  baris[j]			= document.getElementById("baris"+i).value;
		  inota[j]			= document.getElementById("inota"+i).value;
		  dnota[j]			= document.getElementById("dnota"+i).value;
		  vnota[j]			= document.getElementById("vnota"+i).value;
		  vjumlah[j]		= document.getElementById("vjumlah"+i).value;	
		  vsesa[j]			= document.getElementById("vsesa"+i).value;
		  vsisa[j]			= document.getElementById("vsisa"+i).value;
		  vlebih[j]			= document.getElementById("vlebih"+i).value;
		  eremark[j]		= document.getElementById("eremark"+i).value;		

    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		  j++;
		  document.getElementById("baris"+i).value=baris[j];
		  document.getElementById("inota"+i).value=inota[j];
		  document.getElementById("dnota"+i).value=dnota[j];
		  document.getElementById("vnota"+i).value=vnota[j];
		  document.getElementById("vjumlah"+i).value=vjumlah[j];	
		  document.getElementById("vsesa"+i).value=vsesa[j];	
		  document.getElementById("vsisa"+i).value=vsisa[j];
		  document.getElementById("vlebih"+i).value=vlebih[j];
		  document.getElementById("eremark"+i).value=eremark[j];
    }
	area=document.getElementById("iarea").value;
	ikbank=document.getElementById("ikbank").value;
	cust=document.getElementById("icustomer").value;
	showModal("akt-bankin-multialloc/cform/notaupdate/"+a+"/"+area+"/"+cust+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function hitung(){
	jml=document.getElementById("jml").value;
	vjml=formatulang(document.getElementById("vjumlah").value);
	vjmlitem=0;
	for(i=1;i<=jml;i++){
		vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
	}
	document.getElementById("vlebih").value=parseFloat(formatulang(vjml))-vjmlitem;
  }
  function hetang(a)
  {	
		vjmlbyr	  = parseFloat(formatulang(document.getElementById("vjumlah").value));
		vlebih 	  = parseFloat(formatulang(document.getElementById("vlebih").value));
		vsisadt   = parseFloat(formatulang(document.getElementById("vsisa").value));
		vjmlitem  = parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
		vsisaitem = parseFloat(formatulang(document.getElementById("vsisa"+a).value));
		jml		  = document.getElementById("jml").value;
		if(vjmlitem>vsisaitem){
			alert("jumlah bayar tidak boleh melebihi sisa nota !!!!!");
			if(vlebih>vsisaitem){
				document.getElementById("vjumlah"+a).value=formatcemua(vsisaitem);
			}else{
				document.getElementById("vjumlah"+a).value=formatcemua(vlebih);
			}
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
			document.getElementById("vlebih").value=formatcemua(vjmlbyr-vjmlitem);
      bbotol();
		}else if(vjmlitem>vjmlbyr){
			alert("jumlah bayar item tidak boleh melebihi jumlah bayar total !!!!!");
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				if(i!=a){
					vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
				}
			}	
			document.getElementById("vjumlah"+a).value=formatcemua(vjmlbyr-vjmlitem);
			document.getElementById("vlebih").value="0";
		}else if(vjmlitem>vsisadt){
			alert("jumlah bayar tidak boleh melebihi sisa tagihan !!!!!");
			document.getElementById("vjumlah"+a).value="0";
	  }else if(vjmlitem==0){
			alert("jumlah bayar tidak kosong !!!!!");
			document.getElementById("vjumlah"+a).value="0";
		}else{
			vjmlitem=0;
			for(i=1;i<=jml;i++){
				vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
			}
			if(vjmlitem>vjmlbyr){
				alert("jumlah item tidak boleh melebihi jumlah bayar !!!!!");
				document.getElementById("vjumlah"+a).value="0";
				vjmlitem=0;
				for(i=1;i<=jml;i++){
					vjmlitem=vjmlitem+parseFloat(formatulang(document.getElementById("vjumlah"+i).value));
				}
			}
			document.getElementById("vlebih"+a).value=formatcemua(vjmlbyr-vjmlitem);
			document.getElementById("vlebih").value=formatcemua(vjmlbyr-vjmlitem);
      bbotol();
		}
		sesa=parseFloat(formatulang(document.getElementById("vsisa"+a).value))-parseFloat(formatulang(document.getElementById("vjumlah"+a).value));
		if(sesa<=0){
			document.getElementById("vsesa"+a).value='0';
		}else{
			document.getElementById("vsesa"+a).value=formatcemua(sesa);
		}
  }
  function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
  }
  function cektanggal()
  {
    ddt=document.getElementById('ddt').value;
    dbukti=document.getElementById('dbukti').value;
    dtmpdt=ddt.split('-');
    dtmpbukti=dbukti.split('-');
    perdt=dtmpdt[2]+dtmpdt[1]+dtmpdt[0];
    perbukti=dtmpbukti[2]+dtmpbukti[1]+dtmpbukti[0];
    if( (perdt!='') && (perbukti!='') ){
      if(compareDates(document.getElementById('dbukti').value,document.getElementById('ddt').value)==-1)
      {
        alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
        document.getElementById("dbukti").value='';
      }
    }else if( perdt>perbukti ){
        alert("Tanggal bukti pelunasan tidak boleh lebih kecil dari tanggal DT !!!");
        document.getElementById("dbukti").value='';
    }
  }
  function bbotol(){
	  baris	= document.getElementById("baris").value;
	  si_inner= document.getElementById("detailisi").innerHTML;
	  var temp= new Array();
//	  alert(si_inner);
 	  temp	= si_inner.split('<table disabled="disabled" id="itemtem" class="listtable" style="width:920px;"><tbody disabled="disabled">');
	  if( (document.getElementById("vjumlah"+baris).value=='0')){
		  si_inner='';
		  for(x=1;x<baris;x++){
			  si_inner=si_inner+'<table id="itemtem" class="listtable" style="width:920px;"><tbody>'+temp[x];
		  }
		  j=0;
		  var barbar		= Array();
		  var inota			= Array();
		  var dnota			= Array();
		  var vnota			= Array();
		  var vjumlah		= Array();
		  var vsesa			= Array();
		  var vsisa			= Array();
		  var vlebih		= Array();
		  for(i=1;i<baris;i++){
			  j++;
			  barbar[j]		= document.getElementById("baris"+i).value;
			  inota[j]		= document.getElementById("inota"+i).value;
			  dnota[j]		= document.getElementById("dnota"+i).value;
			  vnota[j]		= document.getElementById("vnota"+i).value;
			  vjumlah[j]	= document.getElementById("vjumlah"+i).value;
			  vsesa[j]		= document.getElementById("vsesa"+i).value;
			  vsisa[j]		= document.getElementById("vsisa"+i).value;
			  vlebih[j]		= document.getElementById("vlebih"+i).value;
		  }
		  document.getElementById("detailisi").innerHTML=si_inner;
		  j=0;
		  for(i=1;i<baris;i++){
			  j++;
			  document.getElementById("baris"+i).value			= barbar[j];
			  document.getElementById("inota"+i).value			= inota[j];
			  document.getElementById("dnota"+i).value			= dnota[j];
			  document.getElementById("vnota"+i).value			= vnota[j];
			  document.getElementById("vjumlah"+i).value		= vjumlah[j];
			  document.getElementById("vsesa"+i).value			= vsesa[j];
			  document.getElementById("vsisa"+i).value			= vsisa[j];
			  document.getElementById("vlebih"+i).value			= vlebih[j];
		  }
		  document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;		
		  jsDlgHide("#konten *", "#fade", "#light");
	  }
  }
</script>
