<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listbayarap/cform/bayar','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	 <table class="listtable">
   	    <th>No Nota</th>
	      <th>Tgl Nota</th>
	      <th>Jumlah</th>
   	    <th>No bukti</th>
	      <th>Tgl Bukti</th>
	      <th>No Giro</th>
	      <th>Bank</th>
	      <th>Jml Bayar</th>
	      <th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        if(!isset($saldo)){
          $saldo=$row->v_netto;
        }else{
          $saldo=$saldo-$jmltmp;
        }
        if($row->i_jenis_bayar=='2'){
          $row->i_giro='Tunai';
        }
        if($row->d_dtap!=''){
          $tmp=explode('-',$row->d_dtap);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_dtap=$tgl.'-'.$bln.'-'.$thn;
        }
        if($row->d_bukti!=''){
          $tmp=explode('-',$row->d_bukti);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $row->d_bukti=$tgl.'-'.$bln.'-'.$thn;
        }
			  echo "<tr> 
				  <td>$row->i_dtap</td>
				  <td>$row->d_dtap</td>
				  <td align=right>".number_format($saldo)."</td>
				  <td>$row->i_pelunasanap</td>
				  <td>$row->d_bukti</td>
				  <td>$row->i_giro</td>
				  <td>$row->e_bank_name</td>
				  <td align=right>".number_format($row->v_jumlah)."</td>";
          $jmltmp=$row->v_jumlah;
          $sisa=$saldo-$jmltmp;
        echo "
				  <td align=right>".number_format($sisa)."</td>
				</tr>";

			}
		}
	      ?>
	    </tbody>
	  </table>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
