<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'cekikhpkeluar/cform/cariuraian','update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="2" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Kode area</th>
	    <th>Nama area</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_ikhp_type','$row->e_ikhp_typename','$row->i_coa','$row->e_coa_name')\">$row->i_ikhp_type</a></td>
				  <td><a href=\"javascript:setValue('$row->i_ikhp_type','$row->e_ikhp_typename','$row->i_coa','$row->e_coa_name')\">$row->e_ikhp_typename</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d)
  {
    document.getElementById("iikhptype").value=a;
    document.getElementById("eikhptypename").value=b;
    document.getElementById("icoa").value=c;
    document.getElementById("ecoaname").value=d;
		switch(a)
		{
			case '1':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '2':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '3':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '4':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '5':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '6':
				document.getElementById("vterimatunai").readOnly=false;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=true;
				break;
			case '7':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=false;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=true;
				break;
			case '8':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case '9':
				document.getElementById("vterimatunai").readOnly=false;
				document.getElementById("vterimagiro").readOnly=false;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case 'A':
				document.getElementById("vterimatunai").readOnly=true;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
			case 'B':
				document.getElementById("vterimatunai").readOnly=false;
				document.getElementById("vterimagiro").readOnly=true;
				document.getElementById("vkeluartunai").readOnly=true;
				document.getElementById("vkeluargiro").readOnly=true;
				break;
			default:
				document.getElementById("vterimatunai").readOnly=false;
				document.getElementById("vterimagiro").readOnly=false;
				document.getElementById("vkeluartunai").readOnly=false;
				document.getElementById("vkeluargiro").readOnly=false;
				break;
		}
		jsDlgHide("#konten *", "#fade", "#light");
  }
  function bbatal(){
	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>
