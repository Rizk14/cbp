<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'kartuhutang/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Periode</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="iperiode" name="iperiode" value="<?= date('Ym') ?>">
									<select name="bulan" id="bulan" onchange="buatperiode()">
										<option value='01' <?php echo date('m') == '01' ? 'selected' : '' ?>>Januari</option>
										<option value='02' <?php echo date('m') == '02' ? 'selected' : '' ?>>Februari</option>
										<option value='03' <?php echo date('m') == '03' ? 'selected' : '' ?>>Maret</option>
										<option value='04' <?php echo date('m') == '04' ? 'selected' : '' ?>>April</option>
										<option value='05' <?php echo date('m') == '05' ? 'selected' : '' ?>>Mei</option>
										<option value='06' <?php echo date('m') == '06' ? 'selected' : '' ?>>Juni</option>
										<option value='07' <?php echo date('m') == '07' ? 'selected' : '' ?>>Juli</option>
										<option value='08' <?php echo date('m') == '08' ? 'selected' : '' ?>>Agustus</option>
										<option value='09' <?php echo date('m') == '09' ? 'selected' : '' ?>>September</option>
										<option value='10' <?php echo date('m') == '10' ? 'selected' : '' ?>>Oktober</option>
										<option value='11' <?php echo date('m') == '11' ? 'selected' : '' ?>>November</option>
										<option value='12' <?php echo date('m') == '12' ? 'selected' : '' ?>>Desember</option>
									</select>
									<select name="tahun" id="tahun" onchange="buatperiode()">
										<?php
										$tahun1 = date('Y') - 4;
										$tahun2 = date('Y');
										for ($i = $tahun1; $i <= $tahun2; $i++) {
											$selected = date('Y') == $i ? "selected" : "";

											echo "<option value='$i' $selected>$i</option>";
										}
										?>
									</select>
								</td>
							</tr>

							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<input name="login" id="login" value="View" type="submit">
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('kartuhutang/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}
</script>