<SCRIPT LANGUAGE="javascript">
function ckbrg(nomor){

	$.ajax({
	
	type: "POST",
	url: "<?php echo site_url('listbrgkhusus/cform/cari_barang');?>",
	data:"nbrg="+nomor,
	success: function(data){
		$("#confnomor").html(data);
	},
	
	error:function(XMLHttpRequest){
		alert(XMLHttpRequest.responseText);
	}
	
	})
};
</SCRIPT>

<div id="tabbed_box_1" class="tabbed_box">  
    <div class="tabbed_area">  
      <ul class="tabs">  
        <li><a href="#" class="tab active" onclick="sitab('content_1')">Daftar Barang</a></li>  
	    <li><a href="#" class="tab" onclick="sitab('content_2')">Barang</a></li>  
	  </ul>  
  <div id="content_1" class="content">
  <table class="maintable">
    <tr>
      <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listbrgkhusus/cform/cari','update'=>'#main','type'=>'post'));?>
    	    <table class="listtable">
	      <thead>
	      <tr>
		<td colspan="6" align="center">
		  Cari data : 
			<input type="text" id="cari" name="cari" value="" >&nbsp;
			<input type="submit" id="bcari" name="bcari" value="Cari">
		</td>
	      </tr>
	      </thead>
      	      <th>Kode Produk</th>
	      <th>Nama</th>
	      <th class="action">Action</th>
	      <tbody>
	      <?php 
		if($isi){
		  foreach($isi as $row){
		    echo "<tr> 
			  <td>$row->i_product</td>
			  <td>$row->e_product_name</td>";
			  echo "<td class=\"action\"><a href=\"#\" onclick='show(\"listbrgkhusus/cform/edit/$row->i_product\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;";
			//  echo "<a href=\"#\" onclick='hapus(\"listbrgkhusus/cform/delete/$row->i_product\",\"#tmpx\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
			  echo "</td></tr>";
			}
		}
	      ?>
	      </tbody>
	    </table>
	    <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
        <?=form_close()?>
      </td>
    </tr>
  </table>
  </div>
  <div id="content_2" class="content">
    <table class="maintable">
      <tr>
    	<td align="left">
          <?php echo $this->pquery->form_remote_tag(array('url'=>'listbrgkhusus/cform/simpan','update'=>'#main','type'=>'post'));?>
	  <div id="masterproductbaseform">
	      <table class="mastertable">
	      	<tr>
		  <td width="19%">Kode Produk</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="iproduct" id="iproduct" value="" maxlength='7' onkeyup="ckbrg(document.getElementById('iproduct').value);">
		  <div id="confnomor"></div>  
		  </td>
		  <td width="19%">Nama</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="eproductname" id="eproductname" value="" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Kode Produk (Pemasok)</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="iproductsupplier" id="iproductsupplier" value="" maxlength='7'></td>
		  <td width="19%">Nama Produk (Pemasok)</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="eproductsuppliername" id="eproductsuppliername" value="" maxlength='50'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Supplier</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="isupplier" id="isupplier" value="" 
					   onclick='showModal("listbrgkhusus/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="esuppliername" id="esuppliername" value="" 
					   onclick='showModal("listbrgkhusus/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Status Produk</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductstatus" id="iproductstatus" value="" 
					   onclick='showModal("listbrgkhusus/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductstatusname" id="eproductstatusname" value="" 
					   onclick='showModal("listbrgkhusus/cform/productstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Group Produk</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductgroup" id="iproductgroup" value="" 
					   onclick='showModal("listbrgkhusus/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductgroupname" id="eproductgroupname" value="" 
					   onclick='showModal("listbrgkhusus/cform/productgroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Jenis</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproducttype" id="iproducttype" value="" 
					   onclick="view_producttype(document.getElementById('iproductgroup').value);">
				  <input readonly name="eproducttypename" id="eproducttypename" value="" 
					   onclick="view_producttype(document.getElementById('iproductgroup').value);"></td>
	      	</tr>
	      	<tr>
		  <td width="19%">Kelas</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductclass" id="iproductclass" value="" 
					   onclick='showModal("listbrgkhusus/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
				  <input readonly name="eproductclassname" id="eproductclassname" value="" 
					   onclick='showModal("listbrgkhusus/cform/productclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		  <td width="19%">Kategori</td>
		  <td width="1%">:</td>
		  <td width="30%"><input type="hidden" name="iproductcategory" id="iproductcategory" value="" 
					   onclick="view_productcategory(document.getElementById('iproductclass').value);">
				  <input readonly name="eproductcategoryname" id="eproductcategoryname" value="" 
					   onclick="view_productcategory(document.getElementById('iproductclass').value);"></td>
	      	</tr>
		<tr>
		  <td width="19%">Harga Eceran</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="vproductretail" id="vproductretail" value="" maxlength='10' onkeyup="reformat(this)"></td>
		  <td width="19%">Harga Pabrik</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="vproductmill" id="vproductmill" value="" maxlength='10' onkeyup="reformat(this)"></td>
	      	</tr>
		<tr>
		  <td width="19%">Tanggal Stop Produksi</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="dproductstopproduction" id="dproductstopproduction" value="" readonly onclick="showCalendar('',this,this,'','dproductstopproduction',0,18,1)"></td>
		  <td width="19%">Tanggal Pendaftaran</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="dproductregister" id="dproductregister" value=""readonly onclick="showCalendar('',this,this,'','dproductregister',0,18,1)"></td>
	      	</tr>
		<tr>
		  <td width="19%">Price List</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="checkbox" name="fproductpricelist" id="fproductpricelist" value="on"></td>
		  <td width="19%">Margin</td>
		  <td width="1%">:</td>
		  <td width="30%">
		    <input type="text" name="nproductmargin" id="nproductmargin" value=""></td>
	      	</tr>
	      	<tr>
		  <td width="19%">&nbsp;</td>
		  <td width="1%">&nbsp;</td>
		  <td colspan=4 width="80%">
		    <input name="login" id="login" value="Simpan" type="submit" onclick="cek()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listbrgkhusus/cform/','#main');">
		  </td>
	       </tr>
	      </table>
	    </div>
	<?=form_close()?>
        </td>
      </tr> 
    </table>
  </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
  function xxx(a,b){
    if (confirm(b)==1){
	  document.getElementById("iproductdelete").value=a;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/listbrgkhusus/cform/delete";

	  formna.submit();
    }
  }
  function yyy(a){
	document.getElementById("iproductedit").value=a;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/listbrgkhusus/cform/edit";
	formna.submit();
  }
  function view_productgroup(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgkhusus/cform/productgroup","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgkhusus/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_productstatus(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgkhusus/cform/productstatus","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_producttype(a){
	showModal("listbrgkhusus/cform/producttype/"+a+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function view_productcategory(a){
	showModal("listbrgkhusus/cform/productcategory/"+a+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }
  function view_productclass(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/listbrgkhusus/cform/productclass","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function cek(){
    kode=document.getElementById("iproduct").value;
    nama=document.getElementById("eproductname").value;
    if(kode=='' || nama==''){
	alert("Minimal kode Pelanggan dan nama Pelanggan diisi terlebih dahulu !!!");
    }
  }
</script>
