<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<?php 
	include ("php/fungsi.php");
  $th=substr($iperiode,0,4);
  $bl=substr($iperiode,4,2);
  $pahir=mbulan($bl).'-'.$th;
  $periode=$pahir;
?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.$periode; ?></h3>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'rekapstockopname/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	      <?php 
		if($isi){
		$nsaldoakhir=0;
		$vsaldoakhir=0;
		$nopname=0;
		$vopname=0;
		$nselisih=0;
		$vselisih=0;
?>
	  <th align="center">Store</th>
		<th align="center">Jenis</th>
    <th align="center">Saldo<br>Akhir (Qty)</th>
    <th align="center">Saldo<br>Akhir (Rp)</th>
    <th align="center">Total<br>Opname (Qty)</th>
    <th align="center">Total<br>Opname (Rp)</th>
    <th align="center">Selisih (Qty)</th>
    <th align="center">Selisih (Rp)</th>
    <?php 
	    echo '<tbody>';
      foreach($isi as $raw)
      {
        echo "<tr><td>$raw->i_store - $raw->e_store_name</td>
              <td>$raw->i_jenis - $raw->e_jenis_name</td>";
        echo "<td align=right>".number_format($raw->n_saldoakhir)."</td>
              <td align=right>".number_format($raw->v_saldoakhir_rp)."</td>
              <td align=right>".number_format($raw->n_totalopname)."</td>
              <td align=right>".number_format($raw->v_totalopname_rp)."</td>
              <td align=right>".number_format($raw->n_selisih)."</td>
              <td align=right>".number_format($raw->v_selisih_rp)."</td></tr>";
		          
		          $nsaldoakhir=$nsaldoakhir+$raw->n_saldoakhir;
		          $vsaldoakhir=$vsaldoakhir+$raw->v_saldoakhir_rp;
		          $nopname=$nopname+$raw->n_totalopname;
		          $vopname=$vopname+$raw->v_totalopname_rp;
		          $nselisih=$nselisih+$raw->n_selisih;
		          $vselisih=$vselisih+$raw->v_selisih_rp;
        }?>
        <tr>
	      <th colspan="2" align="center">TOTAL</th>
	      <?php 
            echo "<th align=right>".number_format($nsaldoakhir)."</th>
                  <th align=right>".number_format($vsaldoakhir)."</th>
                  <th align=right>".number_format($nopname)."</th>
                  <th align=right>".number_format($vopname)."</th>
                  <th align=right>".number_format($nselisih)."</th>
                  <th align=right>".number_format($vselisih)."</th></tr>";
      }
?>
	    </tbody>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
