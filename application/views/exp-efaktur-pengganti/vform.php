<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'exp-efaktur-pengganti/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php 
									$data = array(
										'name'        => 'dfrom',
										'id'          => 'dfrom',
										'value'       => '',
										'readonly'   	=> 'true',
										'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php 
									$data = array(
										'name'        => 'dto',
										'id'          => 'dto',
										'value'       => '',
										'readonly'   	=> 'true',
										'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="80%" colspan="3">
									<input type="submit" class="px-3 btn btn-default" id="login" value="View" onclick="return dipales1()">
									<input name="kembali" id="kembali" value="Reset" type="button" onclick='show("exp-efaktur-pengganti/cform/index","#main");'>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<div id="pesan"></div>

<script type="text/javascript">
	function dipales1() {
		var dfrom = $('#dfrom').val();
		var dto = $('#dto').val();

		if (dfrom == '' || dto == '') {
			alert('Pilih Tanggal Dulu !!');
			return false;
		}
	}

	function exportexcel() {
		var datefrom = document.getElementById('dfrom').value;
		var dateto = document.getElementById('dto').value;

		if (datefrom == '' || dateto == '') {
			alert('Pilih Tanggal Terlebih Dahulu!!!');
			return false;
		} else {
			var abc = "<?= site_url('exp-efaktur-pengganti/cform/export/'); ?>/" + datefrom + "/" + dateto;
			$("#href").attr("href", abc);
			return true;
		}
	}
</script>