<?php echo "<h2>$page_title</h2>"; ?>
<?php echo $this->pquery->form_remote_tag(array('url' => 'exp-efaktur-pengganti/cform/export', 'id' => 'formx', 'update' => '#pesan', 'type' => 'post')); ?>
<table class="maintable">
  <tr>
    <td align="left">
      <div class="effect">
        <div class="accordion2">
          <table class="mastertable">
            <!-- <a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();">
              <input type="button" value="Export" class="float-right" />
            </a> -->
            <div id="detailheader" align="center">
              <table id="itemtem" class="listtable" style="width: 100%;">
                <thead>
                  <tr align="center">
                    <th style="width: 50px">No</th>
                    <th style="width: 50px">Kode Area</th>
                    <th style="width: 98px">Kode Pelanggan</th>
                    <th style="width: 225px">Nama Pelanggan</th>
                    <th style="width: 98px">No Nota</th>
                    <th style="width: 90px">Tanggal Nota</th>
                    <th style="width: 98px">No Faktur Komersial</th>
                    <th style="width: 12px" class="action">Act
                      <?php echo "<input type='checkbox' name='chkall' id='chkall' value='' onclick='pilihsemua(this.value)'>"; ?>
                    </th>
                  </tr>
                </thead>

                <tbody>
                  <?php 
                  if ($isi) {
                    $i = 0;
                    $x = 1;

                    foreach ($isi as $row) {
                      $tmp    = explode("-", $row->d_nota);
                      $th     = $tmp[0];
                      $bl     = $tmp[1];
                      $hr     = $tmp[2];
                      $thbl   = $th . $bl;
                      $dnota  = $hr . "-" . $bl . "-" . $th;

                      echo "<tr>
                              <td style='text-align:center'>$x</td>
                              <td style='text-align:center'>$row->i_area</td>
                              <td style='text-align:center'>$row->i_customer</td>
                              <td>$row->e_customer_name</td>
                              <td style='text-align:center'>$row->i_nota</td>
                              <td style='text-align:center'>$dnota</td>
                              <td style='text-align:center'>$row->i_faktur_komersial</td>
                              <td class=\"action\">
                                <input type='checkbox' name='chk" . $i . "' id='chk" . $i . "' value='' onclick='pilihan(this.value," . $i . ")'>
                                <input hidden type='checkbox' name='chk" . $i . "' id='chk" . $i . "' value=''>
                                <input hidden type='text' name='inota" . $i . "' id='inota" . $i . "' value='$row->i_nota'>
                              </td>
                            </tr>";
                      $x++;
                      $i++;
                    }
                    echo "<tr>
                            <td colspan='10' align='center'>
                            <input type='submit' id='proses' value='Export' target='blank' onclick='return dipales2()'>
                            </td>
                          </tr>
                          <input type=\"hidden\" id=\"dfrom\" name=\"dfrom\" value=\"$dfrom\">
                          <input type=\"hidden\" id=\"dto\" name=\"dto\" value=\"$dto\">
                          <input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">
                          <input type=\"hidden\" id=\"cekon\" name=\"cekon\" value='0'>";
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </table> <!-- end mastertable -->
          <div id="pesan"></div>
        </div> <!-- end accordion2 -->
      </div> <!-- end effect -->
    </td> <!-- end td -->
  </tr> <!-- end tr -->
</table> <!-- end table -->
<?= form_close() ?>
<input name='kembali' id='kembali' value='Reset' type='button' onclick='show("exp-efaktur-pengganti/cform/index","#main");'>

<script language="javascript" type="text/javascript">
  function pilihan(a, b) {
    var jml = document.getElementById("jml").value;
    var cekon = document.getElementById("cekon").value;
    var counter = 1;

    if (a == '') {
      document.getElementById("chk" + b).value = 'on';
      for (var i = 0; i < jml; i++) {
        //alert(document.getElementById("chk"+i).value);
        if (document.getElementById("chk" + i).value == 'on') {
          document.getElementById("cekon").value = counter++;
        }
      }
    } else {
      document.getElementById("chk" + b).value = '';
      for (var i = 0; i < jml; i++) {
        if (document.getElementById("chk" + i).value == '') {
          document.getElementById("cekon").value = cekon - 1;
        }
      }
    }
  }

  function pilihsemua(a) {
    var jml = document.getElementById("jml").value;
    var cekon = document.getElementById("cekon").value;
    var counter = 1;
    if (a == '') {
      for (var i = 0; i < jml; i++) {
        // if ((document.getElementById("thbl" + i).value) >= (document.getElementById("periode" + i).value)) {
        document.getElementById("chk" + i).value = 'on';
        document.getElementById("chkall").value = 'on';
        document.getElementById("chk" + i).checked = true;

        if (document.getElementById("chk" + i).value == 'on') {
          document.getElementById("cekon").value = counter++;
        }
        // } else {
        //   document.getElementById("chk" + i).value = '';
        //   document.getElementById("chk" + i).checked = false;
        // }
      }
    } else {
      for (var i = 0; i < jml; i++) {
        document.getElementById("chk" + i).value = '';
        document.getElementById("chk" + i).checked = false;
      }
      document.getElementById("cekon").value = 0;
      document.getElementById("chkall").value = '';
    }
  }

  function dipales2() {
    cek = 'false';
    $s = 0;
    if ((document.getElementById("cekon").value == 0)) {
      alert('Cek data minimal 1 !!!');
      $s = 1;
      return false;
    } else {
      // $('#formx').submit();
      cek = 'true';
      return true;
    }
  }
</script>