<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/dialogue.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
<!--	<?php echo form_open('printersetting/cform/carihost', array('id' => 'listform'));?>-->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'printersetting/cform/carihost','update'=>'#tmp','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="3" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
      	    <th>Printer Host</th>
      	    <th>Host IP</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_host','$row->e_host_name')\">$row->e_host_name</a></td>
				  <td><a href=\"javascript:setVal('$row->i_host','$row->e_host_name')\">$row->e_ip_address</a></td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
  	  <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b)
  {
	document.getElementById("ihost").value=a;
	document.getElementById("eprinterhost").value=b;
	jsDlgHide("#konten *","#fade","#light");
  }
  function setVal(a,b)
  {
	document.getElementById("ihost").value=a;
	document.getElementById("eprinterhost").value=b;
	jsDlgHide("#konten *","#fade","#light");
  }
  function bbatal()
  {
  	jsDlgHide("#konten *", "#fade", "#light");
  }
</script>

