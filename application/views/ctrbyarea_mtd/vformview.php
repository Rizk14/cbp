<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'ctrbyarea_mtd/cform/view','update'=>'#main','type'=>'post'));?>
   <div class="effect">
     <div class="accordion2">
        <table class="mastertable">
         <tr align="center">
            <td style="width:100px;" colspan="13"><b>Sales Performa By Area MTD</b></td>
         </tr>
         <tr>
            <td style="width:100px;" colspan="13"><b>
              <?php 
              if($iproductgroup == "01"){ $iproductgroup = "Dialogue Baby Bedding";}
              elseif($iproductgroup == "02"){$iproductgroup = "Dialogue Baby Non Bedding";}
              elseif($iproductgroup == "MO"){$iproductgroup = "Modern Outlet";}
              elseif($iproductgroup == "NA"){$iproductgroup = "Nasional";}
              elseif($iproductgroup == "00"){$iproductgroup = "Dialogue Home";}
              elseif($iproductgroup == "06"){$iproductgroup = "Dialogue Fashion";}
              else{$iproductgroup = " ";};
                    echo "Dari Tanggal   : ".$dfromprev."<pre>"; 
                    echo "Sampai Tanggal : ".$dto."<pre>";
                    echo "Product : ".$iproductgroup;
              ?>
            </b></td>
         </tr>
        </table>
       
        <table class="listtable">
           <tr align="center">
              <th rowspan="2">No.</th>
              <th rowspan="2">Island</th>
              <th rowspan="2">Provinsi</th>
              <th rowspan="2">Area</th>
              <th rowspan="2">OB</th>
              <th colspan="3">OA</th>
              <th colspan="3">Sales Qty(Unit)</th>
              <th colspan="3">Net Sales (Rp.)</th>
              <th rowspan="2">% Ctr <br> Net Sales (Rp.)</th>
           </tr>
           <?php 
                $pecah1       = explode('-', $dfrom);
                $tgl1       = $pecah1[0];
                $bln1       = $pecah1[1];
                $tahun1     = $pecah1[2];
                $tahunprev1 = intval($tahun1) - 1;

                $pecah2       = explode('-', $dto);
                $tgl2       = $pecah2[0];
                $bln2       = $pecah2[1];
                $tahun2     = $pecah2[2];
                $tahunprev2 = intval($tahun2) - 1;

                $gabung1 = $tgl1.'-'.$bln1.'-'.$tahunprev1;
                $gabung2 = $tgl2.'-'.$bln2.'-'.$tahunprev2;
           ?>
           <tr align="center">
           <?php #php $prevbl= mbulan(substr($masalalu,4,2))
            $blprev =mbulan($masalalu);
            $blnow  =mbulan($now);?>
             <th><?php echo $blprev; ?></th>
             <th><?php echo $blnow; ?></th>
             <th>% Growth</th>
             <th><?php echo $blprev; ?></th>
             <th><?php echo $blnow; ?></th>
             <th>% Growth</th>
             <th><?php echo $blprev; ?></th>
             <th><?php echo $blnow; ?></th>
             <th>% Growth</th>
           </tr>

          <?php 
      if($isi){
            $no = 0;
            $totalob            = 0;
            $totaloaprev        = 0;
            $totaloa            = 0;
            $totalqtyprev       = 0;
            $totalqty           = 0;
            $totalvnotaprev     = 0;
            $totalvnota         = 0;
            $totalctrsales      = 0;
            $totalnotaberjalan  = 0;
            foreach ($isi as $key ) {
              $totalnotaberjalan += $key->vnota;
            }

            
            foreach ($isi as $row) {
            $growthoa    = 0;
            $growthqty   = 0;
            $growthvnota = 0;
            
            //untuk OA
            if($row->oaprev == 0){
                $growthoa = 0;
            }else{
                $growthoa = (($row->oa-$row->oaprev)/$row->oaprev)*100;
            }

            //untuk QTY
            if($row->qtyprev == 0){
                $growthqty = 0;
            }else{
                $growthqty = (($row->qty-$row->qtyprev)/$row->qtyprev)*100;
            }

            //untuk Vnota
            if($row->vnotaprev == 0){
                $growthvnota = 0;
            }else{
                $growthvnota = (($row->vnota-$row->vnotaprev)/$row->vnotaprev)*100;
            }      
            if($row->vnota == 0){
                $ctrsales = 0; 
            }else{
                $ctrsales =  ($row->vnota/$totalnotaberjalan)*100;      
            }
            $no++;
                echo "<tr>
                        <td>".$no."</td>
                        <td>".$row->e_area_island."</td>
                        <td>".$row->e_provinsi."</td>
                        <td>".$row->e_area_name."</td>
                        <td align='right'>".number_format($row->ob)."</td>
                        <td align='right'>".number_format($row->oaprev)."</td>
                        <td align='right'>".number_format($row->oa)."</td>
                        <td align='right'>".number_format($growthoa,2)." %</td>
                        <td align='right'>".number_format($row->qtyprev)."</td>
                        <td align='right'>".number_format($row->qty)."</td>
                        <td align='right'>".number_format($growthqty,2)." %</td>
                        <td align='right'>".number_format($row->vnotaprev)."</td>
                        <td align='right'>".number_format($row->vnota)."</td>
                        <td align='right'>".number_format($growthvnota,2)." %</td>
                        <td align='right'>".number_format($ctrsales,2)." %</td>
                      </tr>";
            
            $totalob            += $row->ob;
            $totaloaprev        += $row->oaprev;
            $totaloa            += $row->oa;
            $totalqtyprev       += $row->qtyprev;
            $totalqty           += $row->qty;
            $totalvnotaprev     += $row->vnotaprev;
            $totalvnota         += $row->vnota;
            $totalctrsales      += $ctrsales;
            }
          }

          ?>  
           <tbody>
            <?php 
                $totalgrowthoa      = (($totaloa-$totaloaprev)/$totaloaprev)*100;
                $totalgrowthqty     = (($totalqty-$totalqtyprev)/$totalqtyprev)*100;
                $totalgrowthvnota   = (($totalvnota-$totalvnotaprev)/$totalvnotaprev)*100;
            ?>
           <tr>
             <td colspan="4"><b>Total</b></td>
             <td align="right"><b><?php echo number_format($totalob);?></b></td>
             <td align="right"><b><?php echo number_format($totaloaprev);?></b></td>
             <td align="right"><b><?php echo number_format($totaloa);?></b></td>
             <td align="right"><b><?php echo number_format($totalgrowthoa,2);?> %</b></td>
             <td align="right"><b><?php echo number_format($totalqtyprev);?></b></td>
             <td align="right"><b><?php echo number_format($totalqty);?></b></td>
             <td align="right"><b><?php echo number_format($totalgrowthqty,2);?> %</b></td>
             <td align="right"><b><?php echo number_format($totalvnotaprev);?></b></td>
             <td align="right"><b><?php echo number_format($totalvnota);?></b></td>
             <td align="right"><b><?php echo number_format($totalgrowthvnota,2);?> %</b></td>
             <td align="right"><b><?php echo number_format($totalctrsales,2);?> %</b></td>
           </tr> 
           </tbody>
     </table>
     <input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('ctrbyarea_mtd/cform/','#main')">
   </div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
