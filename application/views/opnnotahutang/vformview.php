<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'opnnotahutang/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" id="sitabel">
	  <th>No Nota</th>
		<th>Tgl Nota</th>
		<th>Tgl JT</th>
		<th>Supplier</th>
		<th>Nilai</th>
		<th>Sisa</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
		  	$sisa=number_format($row->v_sisa);
		  	$nilai=number_format($row->v_netto);
				$tmp=explode('-',$row->d_dtap);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_dtap=$tgl.'-'.$bln.'-'.$thn;
				$tmp=explode('-',$row->d_due_date);
				$tgl=$tmp[2];
				$bln=$tmp[1];
				$thn=$tmp[0];
				$row->d_jatuh_tempo=$tgl.'-'.$bln.'-'.$thn;
			  echo "<tr>";
				if($row->f_dtap_cancel=='t'){
			  echo "<td><h1>$row->i_dtap</h1></td>";
				}else{
			  echo "<td>$row->i_dtap</td>";
				}
			  echo "<td>$row->d_dtap</td>
				  <td>$row->d_jatuh_tempo</td>";
        echo "
				  <td>($row->i_supplier) $row->e_supplier_name</td>
				  <td align=right>$nilai</td>
				  <td align=right>$sisa</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php # echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(b,c){
	  lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/opnnotahutang/cform/cetak/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });

</script>
