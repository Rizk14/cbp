<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listcoasaldo/cform/cari','update'=>'#tmp','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="8" align="center">Cari data : <input type="text" id="cari" name="cari" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Periode</th>
			<th>CoA</th>
			<th>Keterangan</th>
			<th>Saldo Awal</th>
			<th>Mutasi debet</th>
			<th>Mutasi Kredit</th>
			<th>Saldo Akhir</th>
			<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
				  <td>$row->i_periode</td>
				  <td>$row->i_coa</td>
				  <td>$row->e_coa_name</td>
				  <td align=\"right\">".number_format($row->v_saldo_awal)."</td>
				  <td align=\"right\">".number_format($row->v_mutasi_debet)."</td>
				  <td align=\"right\">".number_format($row->v_mutasi_kredit)."</td>				  
				  <td align=\"right\">".number_format($row->v_saldo_akhir)."</td>				  				  
				  <td class=\"action\">";
			  echo "<a href=\"#\" onclick='show(\"coasaldo/cform/edit/$row->i_periode/$row->i_coa\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>&nbsp;&nbsp;";
			  echo "<a href=\"#\" onclick='hapus(\"listcoasaldo/cform/delete/$row->i_periode/$row->i_coa\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
