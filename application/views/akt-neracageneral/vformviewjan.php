<?php 
 	include ("php/fungsi.php");
   	echo "<center><h2>$page_title</h2></center>";
	echo "<center><h3>$namabulan $tahun</h3></center>"; 
?>
<table class="maintable">
  <tr>
    <td align="left">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable" width="750px">
	   	    <th width="250px">Aktiva</th>
			<th width="125px">&nbsp;</th>
			<th width="250px">Passiva</th>
			<th width="125px">&nbsp;</th>
		<?php 
		echo "<tr valign=top><td colspan=2><table width=\"100%\">";
		$aktiva	= 0;
		$passiva= 0;
		echo "<table width=\"100%\">
 		<tr valign=top> 
		<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">A K T I V A - A K T I V A :</td>";
		echo "</tr>";
		#----------------
		echo "<table width=\"100%\">
 		<tr valign=top> 
		<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Lancar :</td>";
		echo "</tr>";

		if($kaskecil){
			$totalkaskecil=0;
			foreach($kaskecil as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalkaskecil=$totalkaskecil+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($kasbesar){
			$totalkasbesar=0;
			foreach($kasbesar as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalkasbesar=$totalkasbesar+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($bank){
			$totalbank=0;
			foreach($bank as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbank=$totalbank+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($bankbcajkt){
			$totalbankbcajkt=0;
			foreach($bankbcajkt as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbankbcajkt=$totalbankbcajkt+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		
echo "<table width=\"100%\">";
		$totalkasbank=$totalkaskecil+$totalkasbesar+$totalbank+$totalbankbcajkt;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Kas dan Bank</td>";
		  if($totalkasbank){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalkasbank)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";		

	echo "<table width=\"100%\">
 	 <tr valign=top> 
		<td width=\"450px\" align=right>&nbsp;</td>";
	echo "</tr>";
	echo "</table>";

		echo "<table width=\"100%\">";
		if($piutang){
			$totalpiutang=0;
			foreach($piutang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutang=$totalpiutang+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutangkaryawan){
			$totalpiutangkaryawan=0;
			foreach($piutangkaryawan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutangkaryawan=$totalpiutangkaryawan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($piutanglain){
			$totalpiutanglain=0;
			foreach($piutanglain as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpiutanglain=$totalpiutanglain+$tmp->v_saldo_akhir;

			}
			echo "</table>";
		}
		
		echo "<table width=\"100%\">";
		if($persediaanbarangdagang){
			$totalpersediaandagang=0;
			foreach($persediaanbarangdagang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpersediaandagang=$totalpersediaandagang+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		
		echo "<table width=\"100%\">";
		if($uangmukaleasing){
			$totaluangmukaleasing=0;
			foreach($uangmukaleasing as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totaluangmukaleasing=$totaluangmukaleasing+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($biayadibayardimuka){
			$totalbiayadibayardimuka=0;
			foreach($biayadibayardimuka as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbiayadibayardimuka=$totalbiayadibayardimuka+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($pajakbyrdmk){
			$totalpajakbyrdmk=0;
			foreach($pajakbyrdmk as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalpajakbyrdmk=$totalpajakbyrdmk+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}
		echo "<table width=\"100%\">";
		if($sewabyrdmk){
			$totalsewabyrdmk=0;
			foreach($sewabyrdmk as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalsewabyrdmk=$totalsewabyrdmk+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivasub=$totalsewabyrdmk+$totalpajakbyrdmk+$totalbiayadibayardimuka+$totaluangmukaleasing+$totalpersediaandagang+$totalpiutanglain+$totalpiutangkaryawan+$totalpiutang+$totalkasbank;
		#$totalbiayadibayardimuka+$totalpiutangkaryawan+$totalpiutang+$totalpiutanglain+$totalkaskecil+$totalkasbesar+$totalbank+$totalpersediaandagang;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Lancar :</td>";
		  if($totalaktivasub){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivasub)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "<table width=\"100%\">
 	 <tr valign=top> 
		<td width=\"450px\" align=right>&nbsp;</td>";
	echo "</tr>";
	echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Tetap :</td>";
	echo "</tr>";echo "</table>";

		echo "<table width=\"100%\">";
		if($aktivatetapkel1){
			$totalaktivatetapkel1=0;
			foreach($aktivatetapkel1 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalaktivatetapkel1=$totalaktivatetapkel1+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		if($akumaktiva1){
			$totalakumaktiva11=0;
			foreach($akumaktiva1 as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumaktiva11=$totalakumaktiva11+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivatetap=$totalaktivatetapkel1+$totalakumaktiva11;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Tetap :</td>";
		  if($totalaktivatetap){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivatetap)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "<table width=\"100%\">
	<tr valign=top> 
	<td width=\"450px\" align=right>&nbsp;</td>";
	echo "</tr>";
	echo "</table>";

	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Aktiva Lain-Lain :</td>";
	echo "</tr>";echo "</table>";

	echo "<table width=\"100%\">";
		if($biayayangditangguhkan){
			$totalbiayayangditangguhkan=0;
			foreach($biayayangditangguhkan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalbiayayangditangguhkan=$totalbiayayangditangguhkan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

			echo "<table width=\"100%\">";
		if($akumamortisasiyangditangguhkan){
			$totalakumamortisasiyangditangguhkan=0;
			foreach($akumamortisasiyangditangguhkan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$aktiva=$aktiva+$tmp->v_saldo_akhir;
				$totalakumamortisasiyangditangguhkan=$totalakumamortisasiyangditangguhkan+$tmp->v_saldo_akhir;
			}
			echo "</table>";
		}

		echo "<table width=\"100%\">";
		$totalaktivatetap=$totalbiayayangditangguhkan+$totalakumamortisasiyangditangguhkan;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Aktiva Lain-Lain :</td>";
		  if($totalaktivatetap){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalaktivatetap)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
	echo "</table>";

	echo "</td><td colspan=2><table width=\"100%\">";
	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">HUTANG-HUTANG DAN MODAL :</td>";
	echo "</tr>";
	#------------------------
	echo "<table width=\"100%\">
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Hutang Jangka Pendek :</td>";
	echo "</tr>";

		if($hutangdagang){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangdagang = 0;
			foreach($hutangdagang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangdagang=$totalhutangdagang+$tmp->v_saldo_akhir;

			}
		}
		if($hutangbank){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangbank = 0;
			foreach($hutangbank as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangbank=$totalhutangbank+$tmp->v_saldo_akhir;

			}
		}	
		
		if($yhmdibayar){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalyhmdibayar = 0;
			foreach($yhmdibayar as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalyhmdibayar=$totalyhmdibayar+$tmp->v_saldo_akhir;

		  }
	  }
		if($hutangpajak){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangpajak = 0;
			foreach($hutangpajak as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangpajak=$totalhutangpajak+$tmp->v_saldo_akhir;

		  }
	  }
			if($hutanglain){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutanglain = 0;
			foreach($hutanglain as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutanglain=$totalhutanglain+$tmp->v_saldo_akhir;

			}
		}
			if($possementara){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalpossementara = 0;
			foreach($possementara as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalpossementara=$totalpossementara+$tmp->v_saldo_akhir;

			}
		}
		$totalhutangjangkapendek=$totalpossementara+$totalhutanglain+$totalhutangpajak+$totalyhmdibayar+$totalhutangbank+$totalhutangdagang;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Hutang Jangka Pendek :</td>";
		  if($totalhutangjangkapendek){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalhutangjangkapendek)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}
				echo "
 				<tr valign=top> 
				<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">&nbsp;</td>";
				echo "</tr>";
	#----------------------------------	
	echo "
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Hutang Jangka Panjang :</td>";
	echo "</tr>";

			if($hutangbankjangkapanjang){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalhutangbankjangkapanjang = 0;
			foreach($hutangbankjangkapanjang as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalhutangbankjangkapanjang=$totalhutangbankjangkapanjang+$tmp->v_saldo_akhir;

			}
		}

		$totalhutangbankjangkapanjang=$totalhutangbankjangkapanjang;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Hutang Jangka Panjang :</td>";
		  if($totalhutangbankjangkapanjang){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalhutangbankjangkapanjang)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

	echo "
 	<tr valign=top> 
	<td width=\"234px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>";
	echo "</tr>";

	echo "
 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Modal :</td>";
	echo "</tr>";

			if($modalyangdisetor){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totalmodalyangdisetor = 0;
			foreach($modalyangdisetor as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totalmodalyangdisetor=$totalmodalyangdisetor+$tmp->v_saldo_akhir;

			}
		}
		$totalmodalyangdisetor=$totalmodalyangdisetor;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Modal Yang Disetor :</td>";
		  if($totalmodalyangdisetor){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totalmodalyangdisetor)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

		echo "
 	<tr valign=top> 
	<td width=\"234px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>
	<td width=\"116px\" align=right>&nbsp;</td>";
	echo "</tr>";
	#------------
	echo " 	<tr valign=top> 
	<td width=\"450px\"colspan=3 style=\"font-weight: bold;\">Laba/Rugi Yang Ditahan :</td>";
	echo "</tr>";

			if($labarugiyangditahan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahan = 0;
			foreach($labarugiyangditahan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahan=$totallabarugiyangditahan+$tmp->v_saldo_akhir;

			}
		}

			if($labarugiyangditahantahunberjalan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahantahunberjalan = 0;
			foreach($labarugiyangditahantahunberjalan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahantahunberjalan=$totallabarugiyangditahantahunberjalan+$tmp->v_saldo_akhir;

			}
		}

			if($labarugiyangditahanbulanberjalan){
			$totaldebet	 = 0;
			$totalkredit = 0;
			$totallabarugiyangditahanbulanberjalan = 0;
			foreach($labarugiyangditahanbulanberjalan as $tmp)
			{
				echo "<tr valign=top> 
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>
				<td width=\"116px\"></td></tr>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
				$totallabarugiyangditahanbulanberjalan=$totallabarugiyangditahanbulanberjalan+$tmp->v_saldo_akhir;

			}
		}

		$totallabarugi=$totallabarugiyangditahanbulanberjalan+$totallabarugiyangditahantahunberjalan+$totallabarugiyangditahan;
		echo "<tr valign=top> 
			  <td width=\"450px\"colspan=2 style=\"font-weight: bold;\">Jumlah Laba/Rugi Yang Ditahan :</td>";
		  if($totallabarugi){
			echo "<td width=\"100px\" style=\"font-weight: bold;\" align=right>".number_format($totallabarugi)."</td>";
		}else{
			echo "<td width=\"100px\" align=right>0</td>";
		}

	echo "</tr></table></td></tr>";
	/*echo "<tr><td colspan=2>&nbsp;</td><td colspan=2><table width=\"100%\">";
		if($modal){
			$totaldebet	 = 0;
			$totalkredit = 0;
			foreach($modal as $tmp)
			{
				echo "<tr valign=top>
				<td width=\"234px\">$tmp->ket</td>
				<td width=\"116px\" align=right>".number_format($tmp->v_saldo_akhir)."</td>";
				$passiva=$passiva+$tmp->v_saldo_akhir;
			}
		}
		echo "</tr></table></td></tr>"; */
		?>
		  <tr>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px"	 align=right style="font-weight: bold;"><?php echo number_format($aktiva); ?></td>
			<td width="250px" style="font-weight: bold;">Jumlah</td>
			<td width="125px" align=right style="font-weight: bold;"><?php echo number_format($passiva); ?></td>
		  </tr>
		</table>
      </tbody>
      </div>
	</div>
    </td>
  </tr>
</table>
