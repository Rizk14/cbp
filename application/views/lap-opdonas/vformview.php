<div id='tmpx'>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'lap-opdonas/cform/cari','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="12" align="center">Cari data : <input type="text" id="cari" name="cari" value="<?php echo $cari; ?>" >
        <input type="hidden" id="iperiode" name="iperiode" value="<?php echo $iperiode; ?>" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
			<th>Kode</th>
			<th>Nama Barang</th>
			<th>Pcs (OP)</th>
	   	<th>RP (OP)</th>
			<th>Pcs (DO)</th>
			<th>RP (DO)</th>
			<th>Persen Pcs</th>
			<th>Persen Rp</th>
      <th>supplier</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
        
        if($row->pcsdo!=0 && $row->pcsop!=0) {
          $totalpcs=($row->pcsdo/$row->pcsop)*100;
        } else{
          $totalpcs=0;
        }

        if($row->rpdo!=0 && $row->rpop!=0) {
          $totalrp=($row->rpdo/$row->rpop)*100;
        } else{
          $totalrp=0;
        }

			  echo "<tr> 
				  <td style=\"font-size: 13px;\">$row->i_product";
        echo "</td>
				  <td style=\"font-size: 13px;\">$row->e_product_name</td>
				  <td style=\"font-size: 13px;\">$row->pcsop</td>
				  <td style=\"font-size: 13px;\">".number_format($row->rpop)."</td>
				  <td style=\"font-size: 13px;\">$row->pcsdo</td>
				  <td style=\"font-size: 13px;\">".number_format($row->rpdo)."</td>
				  <td style=\"font-size: 13px;\">".number_format($totalpcs,2)." %</td>
          <td style=\"font-size: 13px;\">".number_format($totalrp,2)." %</td>
          <td style=\"font-size: 13px;\">$row->e_supplier_name</td>";
				echo"</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->paginationxx->create_links()."</center>";?>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
