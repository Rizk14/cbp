<?php require_once ('php/fungsi.php'); ?><link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listagingpiutang/cform/view','update'=>'#main','type'=>'post'));?>
<!--	<div class="effect">-->
	  <div class="accordion2">
<h2><?php echo $page_title.' per Tgl Proses '.$djt; ?></h2>
    	  <table class="listtable" id="sitabel">
    <th>Area</th>
		<th>Customer</th>
		<th>Nota</th>
		<th>Tgl Nota</th>
		<th>J Tempo</th>
		<th>Telat</th>
		<th>SJ</th>
		<th>Insf</th>
		<th>Mslh</th>
		<th>Jml Awal</th>
		<th>Blm JT</th>
		<th>0-30</th>
		<th>31-60</th>
		<th>61-90</th>
		<th>>90</th>
		<th>Keterangan</th>
	    <tbody>
	      <?php 
		if($isi){
      $tmp 	= explode("-", $djt);
      $det	= $tmp[0];
      $mon	= $tmp[1];
      $yir 	= $tmp[2];
      $djtx	= $yir."/".$mon."/".$det;
      $djt	= $yir."-".$mon."-".$det;
		  foreach($isi as $row){
        if($row->d_nota){
		      $tmp=explode('-',$row->d_nota);
		      $tgl=$tmp[2];
		      $bln=$tmp[1];
		      $thn=$tmp[0];
		      $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
        }
        $telat=datediff("d",$row->d_jatuh_tempo,$djt);
        $query=$this->db->query(" select a.n_customer_toplength, b.e_customer_classname 
                                  from tr_customer a, tr_customer_class b
                                  where a.i_customer='$row->i_customer' and a.i_customer_class=b.i_customer_class");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $top=$tmp->n_customer_toplength;
            $clas=$tmp->e_customer_classname;
          }
		    }
        if($top<0) $top=$top*-1;
        $topnol=-30;
        $toptigasatu=-60;
        $topenamsatu=-90;
        $dudet	= $this->fungsi->dateAdd("d",$top,$djtx);
        $djatuhtempo=$dudet;
		  	$sisa=number_format($row->v_sisa);
        $vsisaaman=0;
        $query=$this->db->query(" select v_sisa from tm_nota where i_customer='$row->i_customer' and i_nota='$row->i_nota'
                                  and d_jatuh_tempo > '$djt' and v_sisa>0 and f_nota_cancel='f'
                                  and f_ttb_tolak='f' and not i_nota isnull");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisaaman=$tmp->v_sisa;
          }
        }
        $vsisaaman=number_format($vsisaaman);
        $dudetnol	= $this->fungsi->dateAdd("d",$topnol,$djtx);
        $vsisanol=0;
        $query=$this->db->query(" select v_sisa from tm_nota where i_customer='$row->i_customer' and i_nota='$row->i_nota'
                                  and d_jatuh_tempo <= '$djt' and d_jatuh_tempo >= '$dudetnol' 
                                  and v_sisa>0 and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisanol=$tmp->v_sisa;
          }
        }
        $vsisanol=number_format($vsisanol);
        $dudettigasatu	= $this->fungsi->dateAdd("d",$toptigasatu,$djtx);
        $vsisatigasatu=0;
        $query=$this->db->query(" select v_sisa from tm_nota where i_customer='$row->i_customer' and i_nota='$row->i_nota'
                                  and d_jatuh_tempo < '$dudetnol' and d_jatuh_tempo >= '$dudettigasatu' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisatigasatu=$tmp->v_sisa;
          }
        }
        $vsisatigasatu=number_format($vsisatigasatu);
        $dudetenamsatu	= $this->fungsi->dateAdd("d",$topenamsatu,$djtx);
        $vsisaenamsatu=0;
        $query=$this->db->query(" select v_sisa from tm_nota where i_customer='$row->i_customer' and i_nota='$row->i_nota'
                                  and d_jatuh_tempo < '$dudettigasatu' and d_jatuh_tempo >= '$dudetenamsatu' and v_sisa>0 
                                  and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisaenamsatu=$tmp->v_sisa;
          }
        }
        $vsisaenamsatu=number_format($vsisaenamsatu);
        $vsisaenam=0;
        $query=$this->db->query(" select v_sisa from tm_nota where i_customer='$row->i_customer' and i_nota='$row->i_nota'
                                  and d_jatuh_tempo < '$dudetenamsatu' and v_sisa>0 and f_nota_cancel='f'
                                  and f_ttb_tolak='f' and not i_nota isnull");
        if ($query->num_rows() > 0){
          foreach($query->result() as $tmp){
            $vsisaenam=$tmp->v_sisa;
          }
        }
        $query=$this->db->query(" select i_customer_groupar
                                  from tr_customer_groupar
                                  where i_customer='$row->i_customer'");
        if ($query->num_rows() > 0){
			    foreach($query->result() as $tmp){
            $groupar=$tmp->i_customer_groupar;
          }
		    }
        $vsisaenam=number_format($vsisaenam);
        $nama = $row->e_customer_name;
        $nama = str_replace("'","",$nama);
        $nama	= str_replace("(","tandakurungbuka",$nama);
        $nama	= str_replace("&","tandadan",$nama);
        $nama	= str_replace(")","tandakurungtutup",$nama);
        $nama = str_replace('.','tandatitik',$nama);
        $nama = str_replace(',','tandakoma',$nama);
        $nama = str_replace('/','tandagaring',$nama);
        if($row->d_jatuh_tempo){
		      $tmp=explode('-',$row->d_jatuh_tempo);
		      $tgl=$tmp[2];
		      $bln=$tmp[1];
		      $thn=$tmp[0];
		      $row->d_jatuh_tempo=$tgl.'-'.$bln.'-'.$thn;
        }
        if($vsisaaman==0) $vsisaaman=null;
				if($vsisanol==0) $vsisanol=null;
        if($vsisatigasatu==0) $vsisatigasatu=null;
				if($vsisaenamsatu==0) $vsisaenamsatu=null;
				if($vsisaenam==0) $vsisaenam=null;
			  echo "<td>$row->i_area</td>
              <td>($row->i_customer) $row->e_customer_name</td>
              <td>$row->i_nota</td>
              <td>$row->d_nota</td>
              <td>$row->d_jatuh_tempo</td>
              <td>$telat</td>
              <td>$row->i_sj</td>
              <td>$row->f_insentif</td>
              <td>$row->f_masalah</td>
              <td align=right>".number_format($row->v_nota_netto)."</td>
				      <td align=right>$vsisaaman</td>
				      <td align=right>$vsisanol</td>
				      <td align=right>$vsisatigasatu</td>
				      <td align=right>$vsisaenamsatu</td>
				      <td align=right>$vsisaenam</td>
				      <td align=right>$row->e_remark</td>
				</tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
    <input name="cmdreset" id="cmdreset" value="Export to Excel" type="button">
	  <?php #echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
<!--      </div>-->
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(b,d,c){
    showModal("listagingpiutang/cform/detail/"+b+"/"+c+"/"+d+"/","#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  $( "#cmdreset" ).click(function() {  
    var Contents = $('#sitabel').html();    
    window.open('data:application/vnd.ms-excel, ' +  '<table>'+encodeURIComponent($('#sitabel').html()) +  '</table>' );
  });
</script>
