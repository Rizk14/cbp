<table class="maintable">
  <tr>
    <td align="left">
<!--       <?php echo form_open('spbsiapnota/cform/update', array('id' => 'spbformupdate', 'name' => 'spbformupdate', 'onsubmit' => 'sendRequest(); return false'));?> -->
	<?php echo $this->pquery->form_remote_tag(array('url'=>'spbsiapnota/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>SPB</td>
		<?php 
			$tmp=explode("-",$isi->d_spb);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dspb=$hr."-".$bl."-".$th;
		?>
		<td><input id="ispb" name="ispb" value="<?php echo $ispb; ?>" readonly>
			<input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>" readonly></td>
		    <input id="ispb" name="ispb" type="hidden" value="<?php echo $isi->i_spb; ?>"></td>
		<td>Kelompok Harga</td>
		<td><input readonly id="epricegroupname" name="epricegroupname" 
			 	   value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>" 
			 	   readonly >
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input id="vspb" name="vspb" readonly value="<?php echo $isi->v_spb; ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername"
				   value="<?php echo $isi->e_customer_name; ?>" readonly >
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1"
				   value="<?php echo $isi->n_spb_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" 
				   value="<?php echo number_format($isi->v_spb_discount1,2); ?>"></td>
	      </tr>
	      <tr>
		<td>PO</td>
		<td><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
		<td>Discount 2</td>
		<td><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2"
				   value="<?php echo $isi->n_spb_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" 
				   value="<?php echo number_format($isi->v_spb_discount2,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Gudang</td>
		<td><input id="estorename" name="estorename" value="<?php echo $isi->e_store_name; ?>" readonly>
			<input id="istore" name="istore" type="hidden" value="<?php echo $isi->i_store; ?>"></td>
		<td>Discount 3</td>
		<td><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3"
				   value="<?php echo $isi->n_spb_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" 
				   value="<?php echo number_format($isi->v_spb_discount3,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Lokasi Gudang</td>
		<td><input id="estorelocationname" name="estorelocationname" value="<?php echo $isi->e_store_locationname; ?>" readonly>
			<input id="istorelocation" name="istorelocation" type="hidden" value="<?php echo $isi->i_store_location; ?>"></td>
		<td>Discount Total</td>
		<td><input readonly id="vspbdiscounttotal" name="vspbdiscounttotal"
				   value="<?php echo number_format($isi->v_spb_discounttotal,2); ?>"></td>
	      </tr>
	      <tr>
		<td>TOP</td>
		<td><input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_spb_toplength; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Konsiyasi <input id="fspbconsigment"
				   name="fspbconsigment" type="checkbox" readonly 
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>></td>
		<td>Nilai Bersih</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td><input readonly id="vspbbersih" name="vspbbersih" readonly
				   value="<?php echo number_format($tmp,2); ?>"></td>
	      </tr>
	      <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname"
				   value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden"
				   value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount Total (realisasi)</td>
		<td><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter" readonly></td>
	      </tr>
	      <tr>
		<td>Stok Daerah</td>
		<td><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden">
			<input id="isj" name="isj" readonly>
		    <input readonly readonly id="dsj" name="dsj"></td>
		<td>Nilai SPB (realisasi)</td>
		<td><input  readonly id="vspbafter" name="vspbafter" readonly
				   value="<?php echo $isi->v_spb_after;?>"></td>
	      </tr>
		<tr>
		  <td>PKP</td>
		  <td><input id="fspbplusppn" name="fspbplusppn" type="hidden" 
				   value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden"
				   value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden"
				   value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly
				   value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		  <td>Keterangan</td>
		  <td>
		    <input name="eapprove1" id="eapprove1" value="" type="text"></td>
		</tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("spbop/cform/","#main")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:100px;"  align="center">Harga</th>
					<th style="width:60px;"  align="center">Jml Pesan</th>
					<th style="width:60px;"  align="center">Jml Dlv</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\">";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$vtemp=number_format($row->v_unit_price,2);
				  	echo "<tbody>
							<tr>
		    				<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
							<td style=\"width:61px;\"><input style=\"width:61px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:297px;\"><input style=\"width:297px;\" readonly type=\"text\" 
								id=\"eproductname$i\" name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:98px;\"><input style=\"width:98px;\" readonly type=\"text\" 
								id=\"emotifname$i\" name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:97px;\"><input style=\"text-align:right; width:97px;\" 
								type=\"text\" id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$vtemp\"></td>
							<td style=\"width:58px;\"><input style=\"text-align:right; width:58px;\" 
								type=\"text\" id=\"norder$i\" readonly
								name=\"norder$i\" value=\"$row->n_order\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem')\"></td>
							<td style=\"width:58px;\"><input style=\"text-align:right; width:58px;\"
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_deliver\"
								readonly></td>
							</tr>
						  </tbody>";
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		      		  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		     		 ";
				?>
			</div>
			</table>
	  </div>
	</div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table><script language="javascript" type="text/javascript">
  function yyy(b){
	document.getElementById("ispbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spb/cform/edit";
	formna.submit();
  }
  function view_store(a,b){
    lebar =450;
    tinggi=400;
//	alert(a);
    eval('window.open("<?php echo site_url(); ?>"+"/spbsiapnota/cform/store/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("istore").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("ndeliver"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		    document.getElementById("login").disabled=false;
		}
    	}else{
      		alert('Data header masih ada yang salah !!!');
    	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
</script>
