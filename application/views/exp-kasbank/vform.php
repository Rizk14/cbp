<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="areafrom" name="areafrom" value="">
									<?php
									$data = array(
										'name'      => 'dfrom',
										'id'        => 'dfrom',
										'value'     => date('01-m-Y'),
										'readonly'	=> 'true',
										'onclick'	=> "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'      => 'dto',
										'id'        => 'dto',
										'value'     => date('d-m-Y'),
										'readonly'  => 'true',
										'onclick'	=> "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?></td>
							</tr>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<a href="#" id="href" value="Export" target="blank" onclick="return exportexcel();"><button>Export</button></a>
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-kasbank/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	function exportexcel() {
		var dfrom = document.getElementById("dfrom").value;
		var dto = document.getElementById("dto").value;

		var abc = "<?= site_url('exp-kasbank/cform/export/'); ?>" + dfrom + "/" + dto;
		console.log(abc);
		$("#href").attr("href", abc);
		return true;
	}
</script>