<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<h2><?php echo $page_title; ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
	$tujuan = 'spbkonsinyasi/cform/simpan';
	?>
	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#pesan','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	      <?php 
		if($isi){
?>
	   	<th>KodeLang</th>
	   	<th>Customer</th>
<?php 
      foreach($diskon as $row)
      {
  		  echo "<th> Jml Pcs ".$row->diskon." %</th>";
  		  echo "<th> Kotor ".$row->diskon." %</th>";
      }
?>
			<th class="action">Act</th>
	    <tbody>
	      <?php 
        echo "<input type='hidden' id='iperiode' name='iperiode' value='$iperiode'>";
        $i=0;
        $disc='';
        $cust='';
        $jmltot=count($diskon);
        $pos=0;
        foreach($isi as $raw)
        {
          $x=0;
          if($cust==''){
            $i++;
            echo "<tr><td>$raw->i_customer<input type='hidden' id='icustomer".$i."' name='icustomer".$i."' value='$raw->i_customer'>
                      <input type='hidden' id='iarea".$i."' name='iarea".$i."' value='$raw->i_area'></td>
                      <td>$raw->e_customer_name
                      <input type='hidden' id='ecustomername".$i."' name='ecustomername".$i."' value='$raw->e_customer_name'></td>";
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td>$raw->jumlah</td>
                        <td>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }else{
                echo "
                      <td>0</td>
                      <td>0</td>";
              }
            }
          }
          if($cust==$raw->i_customer){
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td>$raw->jumlah</td>
                        <td>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }elseif($x>$pos){
                echo "
                      <td>0</td>
                      <td>0</td>";
              }
            }
          }
          if($cust!=$raw->i_customer && $cust!=''){
            while($pos<$jmltot){
              echo "  <td>0</td>
                      <td>0</td>";
              $pos++;
            }
            if($pos==$jmltot){
              echo "<td valign=top class=\"action\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'>";
			        echo "</td></tr>";	
            }
            $i++;
            echo "<tr><td>$raw->i_customer<input type='hidden' id='icustomer".$i."' name='icustomer".$i."' value='$raw->i_customer'>
                      <input type='hidden' id='iarea".$i."' name='iarea".$i."' value='$raw->i_area'></td>
                      <td>$raw->e_customer_name
                      <input type='hidden' id='ecustomername".$i."' name='ecustomername".$i."' value='$raw->e_customer_name'></td>";
            foreach($diskon as $row){
              $x++;
              if($row->diskon==$raw->n_notapb_discount){
                echo "
                        <td>$raw->jumlah</td>
                        <td>".number_format($raw->kotor)."</td>";
                $pos=$x;
                break;
              }elseif($x>$pos){
                echo "
                      <td>0</td>
                      <td>0</td>";
              }
            }
          }
          $cust=$raw->i_customer;
        }
        while($pos<$jmltot){
          echo "  <td>0</td>
                  <td>0</td>";
          $pos++;
        }
        if($pos==$jmltot){
          echo "<td valign=top class=\"action\"><input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'>";
	        echo "</td></tr>";	
        }
        $col=($jmltot*2)+3;
        echo "<tr>
							    <td colspan='$col' align='center'><input type='submit' id='transfer' name='transfer' value='Rekap'></td>
					      </tr>
					      <input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">";
		}
	          ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
      <?=form_close()?>
      <table class="listtable">
				<tr>
					<td align="center">
						<input type="button" id="pilihsemua" name="pilihsemua" value="Pilih Semua" 
							   onclick="pilihsemua()">
						<input type="button" id="tidakpilihsemua" name="tidakpilihsemua" value="Tidak Semua"
							   onclick="tidakpilihsemua()">
						<input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="batal()">
					</td>
				</tr>
			</table>
      <div id="pesan"></div>
    </div>
    </div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function pilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=true;
			document.getElementById("chk"+i).value='on';
		}
	}
	function tidakpilihsemua(){
		var jml=parseFloat(document.getElementById("jml").value);
		for(i=1;i<=jml;i++){
			document.getElementById("chk"+i).checked=false;
			document.getElementById("chk"+i).value='';
		}
	}
	function batal(){
		show('spbkonsinyasi/cform/','#main');
	}
	function pilihan(a,b){
		if(a==''){
			document.getElementById("chk"+b).value='on';
		}else{
			document.getElementById("chk"+b).value='';
		}
	}
</script>
