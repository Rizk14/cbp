<?php echo $this->pquery->form_remote_tag(array('url' => 'customer/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
<div id="customernewform">

	<table class="maintable">
		<tr>
			<td align="left">
				<table class="mastertable">
					<tr>
						<td width="16%">Kode Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="icustomer" id="icustomer" value="<?php echo $isi->i_customer; ?>" maxlength='5'>
						</td>
						<td width="16%">Sales</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="esalesmanname" id="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>" readonly onclick='view_salesman(document.getElementById("iarea").value);'>
							<input type="hidden" name="isalesman" id="isalesman" value="<?php echo $isi->i_salesman; ?>">
						</td>
					</tr>
					<?php
					if ($isi->d_survey != '') {
						$tmp = explode("-", $isi->d_survey);
						$th = $tmp[0];
						$bl = $tmp[1];
						$hr = $tmp[2];
						$dsurvey = $hr . "-" . $bl . "-" . $th;
					} else {
						$dsurvey = '';
					}
					?>
					<tr>
						<td width="16%">Tanggal Survey</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="dsurvey" id="dsurvey" value="<?php echo $dsurvey; ?>" readonly onclick="showCalendar('',this,this,'','dsurvey',0,20,1)" value="">
						</td>
						<td width="16%">Periode Kunjungan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="nvisitperiod" id="nvisitperiod" value="<?php echo $isi->n_visit_period; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Kriteria Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="chkcriterianew" id="chkcriterianew" <?php if ($isi->f_customer_new == 't') {
																									echo 'checked  value="on"';
																								} else {
																									echo 'value=""';
																								} ?> onclick='chkcriteriaa()'>
							Plg Baru / New &nbsp;&nbsp;&nbsp;&nbsp;
							<input type="checkbox" name="chkcriteriaupdate" id="chkcriteriaupdate" <?php if ($isi->f_customer_new == 'f') {
																										echo 'checked  value="on"';
																									} else {
																										echo 'value=""';
																									} ?> onclick='chkcriteriab()'>
							Plg Lama / UpDate
						</td>
						<td width="16%">Area</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="eareaname" id="eareaname" value="<?php echo $isi->e_area_name; ?>" readonly onclick='showModal("customernew/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="iarea" id="iarea" value="<?php echo $isi->i_area; ?>">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%"></td>
						<td class="batas" width="1%"></td>
						<td class="batas" width="33%">
							<input type="checkbox" name="fcustomeraktif" id="fcustomeraktif" value="on" <?php if ($isi->f_customer_aktif == 't') echo 'checked'; ?>>
							Aktif
						</td>
						<td class="batas" width="16%">Kota</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecityname" id="ecityname" value="<?php if ($isi->e_city_name) echo $isi->e_city_name; ?>" readonly onclick='view_city(document.getElementById("iarea").value);'> #
							<input type="hidden" name="icity" id="icity" value="<?php echo $isi->i_city; ?>" readonly>
						</td>
					</tr>
					<tr>
						<td colspan=6>DATA TOKO / PELANGGAN</td>
					</tr>
					<tr>
						<td width="16%">Nm Toko / Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomername" id="ecustomername" value="<?php echo $isi->e_customer_name; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">Alamat Toko</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomeraddress" id="ecustomeraddress" value="<?php echo $isi->e_customer_address; ?>" maxlength='100' onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="16%">Penanda Toko</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomersign" id="ecustomersign" value="<?php echo $isi->e_customer_sign; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">RT / RW / Kode Pos</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ert1" id="ert1" maxlength='2' value="<?php echo $isi->e_rt1; ?>">&nbsp;/&nbsp;
							<input type="text" name="erw1" id="erw1" maxlength='2' value="<?php echo $isi->e_rw1; ?>">&nbsp;/&nbsp;
							<input type="text" name="epostal1" id="epostal1" maxlength='5' value="<?php echo $isi->e_postal1; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Telepon</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerphone" id="ecustomerphone" value="<?php echo $isi->e_customer_phone; ?>" maxlength='20'>
						</td>
						<td width="16%">Fax</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="efax1" id="efax1" value="<?php echo $isi->e_fax1; ?>" maxlength='20'>
						</td>
					</tr>
					<tr>
						<td width="16%">Yang dihubungi</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomercontact" id="ecustomercontact" maxlength='30' value="<?php echo $isi->e_customer_contact; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">Jabatan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomercontactgrade" id="ecustomercontactgrade" maxlength='30' value="<?php echo $isi->e_customer_contactgrade; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="16%">Mulai Usaha</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomermonth" id="ecustomermonth" value="<?php echo $isi->e_customer_month; ?>" onkeyup='lamaberdiri()' maxlength=2>
							/
							<input type="text" name="ecustomeryear" id="ecustomeryear" value="<?php echo $isi->e_customer_year; ?>" onkeyup='lamaberdiri()' maxlength=4>(bl / tahun)
						</td>
						<td width="16%">Tahun</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerage" id="ecustomerage" readonly value="<?php echo $isi->e_customer_age; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Status Toko</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="eshopstatus" id="eshopstatus" value="<?php echo $isi->e_shop_status; ?>" readonly onclick='showModal("customernew/cform/shopstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="ishopstatus" id="ishopstatus" value="<?php echo $isi->i_shop_status; ?>"'></td>
							<td width="16%">Luas Fisik Toko</td>
							<td width="1%">:</td>
							<td width="33%">
								<input type="text" name="nshopbroad" id="nshopbroad" maxlength=' 7' value="<?php echo $isi->n_shop_broad; ?>"> M2
						</td>
					</tr>
					<tr>
						<td width="16%">Kelurahan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerkelurahan1" id="ecustomerkelurahan1" maxlength='30' value="<?php echo $isi->e_customer_kelurahan1; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">Kecamatan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerkecamatan1" id="ecustomerkecamatan1" maxlength='30' value="<?php echo $isi->e_customer_kecamatan1; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%">Kabupaten / Kodya</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerkota1" id="ecustomerkota1" maxlength='30' value="<?php echo $isi->e_customer_kota1; ?>" onkeyup="gede(this)">
						</td>
						<td class="batas" width="16%">Provinsi</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerprovinsi1" id="ecustomerprovinsi1" maxlength='30' value="<?php echo $isi->e_customer_provinsi1; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td colspan=6>DATA PEMILIK / PENGURUS TOKO / PELANGGAN</td>
					</tr>
					<tr>
						<td width="16%">Nama Pemilik</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerowner" id="ecustomerowner" value="<?php echo $isi->ownername; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">TTL / Umur</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerownerttl" id="ecustomerownerttl" value="<?php echo $isi->e_customer_ownerttl; ?>" onkeyup="gede(this)"> / <input type="text" name="ecustomerownerage" id="ecustomerownerage" value="<?php echo $isi->e_customer_ownerage; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">NIK</td>
						<td width="1%">:</td>
						<td width="33%"><input type="text" name="inik" id="inik" value="<?php echo $isi->i_nik; ?>"></td>
						<td width="16%"></td>
						<td width="1%"></td>
						<td width="33%"></td>
					</tr>
					<tr>
						<td width="16%">Status</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="emarriage" id="emarriage" value="<?php echo $isi->e_marriage; ?>" readonly onclick='showModal("customernew/cform/marriage/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="imarriage" id="imarriage" value="<?php echo $isi->i_marriage; ?>">
						</td>
						<td width="16%">Jenis Kelamin</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ejeniskelamin" id="ejeniskelamin" value="<?php echo $isi->e_jeniskelamin; ?>" readonly onclick='showModal("customernew/cform/jeniskelamin/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="ijeniskelamin" id="ijeniskelamin" value="<?php echo $isi->i_jeniskelamin; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Alamat Rumah</td>
						<td width="1%">:</td>
						<td width="33%"><input type="checkbox" name="chkidemtoko1" id="chkidemtoko1" value="" onclick='chkdemtoko1();'> Sama dengan alamat toko</td>
						<td width="16%">Agama</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ereligion" id="ereligion" value="<?php echo $isi->e_religion; ?>" readonly onclick='showModal("customernew/cform/religion/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="ireligion" id="ireligion" value="<?php echo $isi->i_religion; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%"></td>
						<td width="1%"></td>
						<td width="33%"><input type="text" name="ecustomerowneraddress" id="ecustomerowneraddress" value="<?php echo $isi->e_customer_owneraddress; ?>" maxlength='100' onkeyup="gede(this)"></td>
						<td width="16%">RT / RW / Kode Pos</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ert2" id="ert2" maxlength='2' value="<?php echo $isi->e_rt2; ?>">&nbsp;/&nbsp;
							<input type="text" name="erw2" id="erw2" maxlength='2' value="<?php echo $isi->e_rw2; ?>">&nbsp;/&nbsp;
							<input type="text" name="epostal2" id="epostal2" maxlength='5' value="<?php echo $isi->e_postal2; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Telepon / HP</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerownerphone" id="ecustomerownerphone" value="<?php echo $isi->e_customer_ownerphone; ?>"> / <input type="text" name="ecustomerownerhp" id="ecustomerownerhp" value="<?php echo $isi->e_customer_ownerhp; ?>">
						</td>
						<td width="16%">Fax / Email</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerownerfax" id="ecustomerownerfax" value="<?php echo $isi->e_customer_ownerfax; ?>"> / <input type="text" name="ecustomermail" id="ecustomermail" value="<?php echo $isi->e_customer_mail; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Nama Suami / Istri</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerownerpartner" id="ecustomerownerpartner" value="<?php echo $isi->e_customer_ownerpartner; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">TTL / Umur</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerownerpartnerttl" id="ecustomerownerpartnerttl" value="<?php echo $isi->e_customer_ownerpartnerttl; ?>" onkeyup="gede(this)"> / <input type="text" name="ecustomerownerpartnerage" id="ecustomerownerpartnerage" value="<?php echo $isi->e_customer_ownerpartnerage; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Kelurahan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerkelurahan2" id="ecustomerkelurahan2" maxlength='30' value="<?php echo $isi->e_customer_kelurahan2; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">Kecamatan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerkecamatan2" id="ecustomerkecamatan2" maxlength='30' value="<?php echo $isi->e_customer_kecamatan2; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%">Kabupaten / Kodya</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerkota2" id="ecustomerkota2" maxlength='30' value="<?php echo $isi->e_customer_kota2; ?>" onkeyup="gede(this)">
						</td>
						<td class="batas" width="16%">Provinsi</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerprovinsi2" id="ecustomerprovinsi2" maxlength='30' value="<?php echo $isi->e_customer_provinsi2; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="16%">Alamat kirim</td>
						<td width="1%">:</td>
						<td colspan=4 width="33%"><input type="checkbox" name="chkidemtoko2" id="chkidemtoko2" value="" onclick='chkdemtoko2()'> Sama dengan alamat toko
							<input type="checkbox" name="chkidemtoko3" id="chkidemtoko3" value="" onclick='chkdemtoko3()'> Sama dengan alamat rumah
						</td>
					</tr>
					<tr>
						<td width="16%"></td>
						<td width="1%"></td>
						<td width="33%">
					<tr>
						<td width="16%"></td>
						<td width="1%"></td>
						<td width="33%"><input type="text" name="ecustomersendaddress" id="ecustomersendaddress" value="<?php echo $isi->e_customer_sendaddress; ?>" maxlength='100' onkeyup="gede(this)"></td>
						<td width="16%">RT / RW / Kode Pos</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ert3" id="ert3" maxlength='2' value="<?php echo $isi->e_rt3; ?>">&nbsp;/&nbsp;
							<input type="text" name="erw3" id="erw3" maxlength='2' value="<?php echo $isi->e_rw3; ?>">&nbsp;/&nbsp;
							<input type="text" name="epostal3" id="epostal3" maxlength='5' value="<?php echo $isi->e_postal3; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Telepon</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomersendphone" id="ecustomersendphone" value="<?php echo $isi->e_customer_sendphone; ?>">
						</td>
						<td width="16%">Lokasi Bisa dilalui o/</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="etraversed" id="etraversed" value="<?php echo $isi->e_traversed; ?>" readonly onclick='showModal("customernew/cform/traversed/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="itraversed" id="itraversed" value="<?php echo $isi->i_traversed; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Ada Biaya Retribusi Parkir</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="fparkir" id="fparkir" <?php if ($isi->f_parkir == 't') {
																					echo 'checked  value="on"';
																				} else {
																					echo 'value=""';
																				} ?> onclick="cekparkir();">
						</td>
						<td width="16%">Ada Biaya Kuli</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="fkuli" id="fkuli" <?php if ($isi->f_kuli == 't') {
																				echo 'checked  value="on"';
																			} else {
																				echo 'value=""';
																			} ?> onclick="cekkuli();">
						</td>
					</tr>
					<tr>
						<td width="16%">Ekspedisi Toko 1</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="eekspedisi1" id="eekspedisi1" value="<?php echo $isi->e_ekspedisi1; ?>" onkeyup="gede(this)">
						</td>
						<td width="16%">Ekspedisi Toko 2</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="eekspedisi2" id="eekspedisi2" value="<?php echo $isi->e_ekspedisi2; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%">Kabupaten / Kodya</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerkota3" id="ecustomerkota3" maxlength='30' value="<?php echo $isi->e_customer_kota3; ?>" onkeyup="gede(this)">
						</td>
						<td class="batas" width="16%">Provinsi</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">
							<input type="text" name="ecustomerprovinsi3" id="ecustomerprovinsi3" maxlength='30' value="<?php echo $isi->e_customer_provinsi3; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td width="16%">No NPWP</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomernpwp" id="ecustomernpwp" value="<?php echo $isi->e_customer_pkpnpwp; ?>" maxlength=20>
						</td>
						<td width="16%">Nama NPWP</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomernpwpname" id="ecustomernpwpname" value="<?php echo $isi->e_customer_pkpname; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%">Alamat NPWP</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" colspan=4 width="83%">
							<input type="text" name="ecustomernpwpaddress" id="ecustomernpwpaddress" value="<?php echo $isi->e_customer_pkpaddress; ?>" onkeyup="gede(this)">
						</td>
					</tr>
					<tr>
						<td colspan=6>KUALIFIKASI PELANGGAN</td>
					</tr>
					<tr>
						<td width="16%">Type Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerclassname" id="ecustomerclassname" value="<?php echo $isi->e_customer_classname; ?>" readonly onclick='showModal("customernew/cform/customerclass/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomerclass" id="icustomerclass" value="<?php echo $isi->i_customer_class; ?>">
						</td>
						<td width="16%">Pola Pembayaran</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="epaymentmethod" id="epaymentmethod" value="<?php echo $isi->e_paymentmethod; ?>" readonly onclick='showModal("customernew/cform/paymentmethod/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="ipaymentmethod" id="ipaymentmethod" value="<?php echo $isi->i_paymentmethod; ?>">
						</td>
					</tr>
					<tr>
						<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;I. Nama Bank :&nbsp;&nbsp;<input type="text" name="ecustomerbank1" id="ecustomerbank1" value="<?php echo $isi->e_customer_bank1; ?>" onkeyup="gede(this)">
							&nbsp;&nbsp;No. A/C : <input type="text" name="ecustomerbankaccount1" id="ecustomerbankaccount1" value="<?php echo $isi->e_customer_bankaccount1; ?>" onkeyup="gede(this)">
							&nbsp;Atas Nama : <input type="text" name="ecustomerbankname1" id="ecustomerbankname1" value="<?php echo $isi->e_customer_bankname1; ?>" onkeyup="gede(this)"></td>
					</tr>
					<tr>
						<td colspan=6 width="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;II. Nama Bank :&nbsp;&nbsp;<input type="text" name="ecustomerbank2" id="ecustomerbank2" value="<?php echo $isi->e_customer_bank2; ?>" onkeyup="gede(this)">
							&nbsp;&nbsp;No. A/C : <input type="text" name="ecustomerbankaccount2" id="ecustomerbankaccount2" value="<?php echo $isi->e_customer_bankaccount2; ?>" onkeyup="gede(this)">
							&nbsp;Atas Nama : <input type="text" name="ecustomerbankname2" id="ecustomerbankname2" value="<?php echo $isi->e_customer_bankname2; ?>" onkeyup="gede(this)"></td>
					</tr>
					<tr>
						<td width="16%">Nama Kompetitor</td>
						<td width="1%">:</td>
						<td colspan=4 width="83%">
							1.<input type="text" maxlength=20 name="ekompetitor1" id="ekompetitor1" value="<?php echo $isi->e_kompetitor1; ?>" onkeyup="gede(this)">
							2.<input type="text" maxlength=20 name="ekompetitor2" id="ekompetitor2" value="<?php echo $isi->e_kompetitor2; ?>" onkeyup="gede(this)">
							3.<input type="text" maxlength=20 name="ekompetitor3" id="ekompetitor3" value="<?php echo $isi->e_kompetitor3; ?>" onkeyup="gede(this)"></td>
					</tr>
					<tr>
						<td width="16%">TOP</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ncustomertoplength" id="ncustomertoplength" value="<?php echo $isi->n_customer_toplength; ?>" maxlength='3'> Hari /
							<input type="text" name="ncustomertoplengthprint" id="ncustomertoplength" value="<?php echo $isi->n_customer_toplength_print; ?>" maxlength='3'> Hari (Utk Cetak di Nota)
						</td>
						<td width="16%">Discount</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ncustomerdiscount" id="ncustomerdiscount" value="<?php echo $isi->n_customer_discount1; ?>" maxlength='3' onkeyup='copydisc()'>%
							<input readonly type="text" name="ncustomerdiscountx" id="ncustomerdiscountx" value="<?php echo $isi->n_customer_discount2; ?>" maxlength='3'>%
						</td>
					</tr>
					<tr>
						<td width="16%">Tukar Nota (Kontra Bon)</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="fkontrabon" id="fkontrabon" <?php if ($isi->f_kontrabon == 't') {
																							echo 'checked  value="on"';
																						} else {
																							echo 'value=""';
																						} ?> onclick="cekkontrabon();">
						</td>
						<td width="16%">Waktu u/ menghubungi</td>
						<td width="1%">:</td>
						<td width="33%"><input type="text" name="ecall" id="ecall" value="<?php echo $isi->e_call; ?>" readonly onclick='showModal("customernew/cform/call/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icall" id="icall" value="<?php echo $isi->i_call; ?>">
						</td>
					</tr>
					<tr>
						<td class="batas" width="16%">Jadwal Kontra Bon</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">Hari
							<input type="text" name="ekontrabonhari" id="ekontrabonhari" value="<?php echo $isi->e_kontrabon_hari; ?>">&nbsp;Jam
							<input type="text" name="ekontrabonjam1" id="ekontrabonjam1" value="<?php echo $isi->e_kontrabon_jam1; ?>">s/d
							<input type="text" name="ekontrabonjam2" id="ekontrabonjam2" value="<?php echo $isi->e_kontrabon_jam2; ?>">
						</td>
						<td class="batas" width="16%">Jadwal Tagih</td>
						<td class="batas" width="1%">:</td>
						<td class="batas" width="33%">Hari
							<input type="text" name="etagihhari" id="etagihhari" value="<?php echo $isi->e_tagih_hari; ?>">&nbsp;Jam
							<input type="text" name="etagihjam1" id="etagihjam1" value="<?php echo $isi->e_tagih_jam1; ?>">s/d
							<input type="text" name="etagihjam2" id="etagihjam2" value="<?php echo $isi->e_tagih_jam2; ?>">
						</td>
					</tr>
					<tr>
						<td colspan=6>LAIN - LAIN</td>
					</tr>
					<tr>
						<td width="16%">Group Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomergroupname" id="ecustomergroupname" value="<?php echo $isi->e_customer_groupname; ?>" readonly onclick='showModal("customer/cform/customergroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomergroup" id="icustomergroup" value="<?php echo $isi->i_customer_group; ?>">
						</td>
						<td width="16%">PLU Group</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerplugroupname" id="ecustomerplugroupname" value="<?php echo $isi->e_customer_plugroupname; ?>" readonly onclick='showModal("customer/cform/plugroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomerplugroup" id="icustomerplugroup" value="<?php echo $isi->i_customer_plugroup; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Tipe Produk</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerproducttypename" id="ecustomerproducttypename" value="<?php echo $isi->e_customer_producttypename; ?>" readonly onclick='showModal("customer/cform/customerproducttype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomerproducttype" id="icustomerproducttype" value="<?php echo $isi->i_customer_producttype; ?>">
						</td>
						<td width="16%">Produk Khusus</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerspecialproductname" id="ecustomerspecialproductname" value="<?php echo $isi->e_customer_specialproductname; ?>" readonly onclick="view_customerspecialproduct(document.getElementById('icustomerproducttype').value)">
							<input type="hidden" name="icustomerspecialproduct" id="icustomerspecialproduct" value="<?php echo $isi->i_customer_specialproduct; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Status Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerstatusname" id="ecustomerstatusname" value="<?php echo $isi->e_customer_statusname; ?>" readonly onclick='showModal("customer/cform/customerstatus/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomerstatus" id="icustomerstatus" value="<?php echo $isi->i_customer_status; ?>">
						</td>
						<td width="16%">Tingkat Pelanggan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomergradename" id="ecustomergradename" value="<?php echo $isi->e_customer_gradename; ?>" readonly onclick='showModal("customer/cform/customergrade/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomergrade" id="icustomergrade" value="<?php echo $isi->i_customer_grade; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Jenis Pelayanan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomerservicename" id="ecustomerservicename" value="<?php echo $isi->e_customer_servicename; ?>" readonly onclick='showModal("customer/cform/customerservice/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomerservice" id="icustomerservice" value="<?php echo $isi->i_customer_service; ?>">
						</td>
						<td width="16%">Cara Penjualan</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="ecustomersalestypename" id="ecustomersalestypename" value="<?php echo $isi->e_customer_salestypename; ?>" readonly onclick='showModal("customer/cform/customersalestype/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="icustomersalestype" id="icustomersalestype" value="<?php echo $isi->i_customer_salestype; ?>">
						</td>
					</tr>
					<tr>
						<td width="16%">Group Harga</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="text" name="epricegroupname" id="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>" readonly onclick='showModal("customer/cform/pricegroup/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
							<input type="hidden" name="ipricegroup" id="ipricegroup" value="<?php echo $isi->i_price_group; ?>">
							<input type="hidden" name="nline" id="nline" value="<?php echo $isi->n_line; ?>">
						</td>

						<td width="16%">Kode Master</td>
						<td width="1%">:</td>
						<td width="33%">
							<div id="parent"><input type="text" name="irefcode" id="irefcode" value="<?= $isi->i_sps_code ?>" maxlength="8"></div>
							<div id="popup" style="display: none">
								<font color="red"><strong>* Hubungi tim SPS untuk mendapatkan kode master</strong></font>
							</div>
						</td>
					</tr>
					<tr>
						<td width="16%">PLus PPN</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="fcustomerplusppn" id="fcustomerplusppn" value="on" <?php if ($isi->f_customer_plusppn == 't') echo 'checked'; ?>>
						</td>
						<td width="16%">PKP</td>
						<td width="1%">:</td>
						<td width="33%">
							<input type="checkbox" name="fcustomerpkp" id="fcustomerpkp" value="on" <?php if ($isi->f_customer_pkp == 't') echo 'checked'; ?>>
						</td>
					</tr>
					<tr>
						<td width="100%" align="center" colspan="6">
							<!-- <input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));"> -->
							<?php if ($this->session->userdata('user_id') == "fm" || $this->session->userdata('user_id') == "staff.salesadm2" || $this->session->userdata('user_id') == "admin") { ?>
								<input name="login" id="login" value="Simpan" type="submit" onclick="return dipales();">
							<?php
							} else {
							} ?>
							<input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("customer/cform/","#main")'>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<div id="pesan"></div>

</div>
<?= form_close() ?>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		var e = document.getElementById('parent');
		e.onmouseover = function() {
			document.getElementById('popup').style.display = 'block';
		}
		e.onmouseout = function() {
			document.getElementById('popup').style.display = 'none';
		}
	});

	function view_salesman(a) {
		showModal("customernew/cform/salesman/" + a + "/zxqf/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_city(a) {
		showModal("customer/cform/city/" + a, "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_customerspecialproduct(a) {
		showModal("customernew/cform/customerspecialproduct/" + a, "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function cek() {
		area = document.getElementById("iarea").value;
		nama = document.getElementById("ecustomername").value;
		if (kode == '' || nama == '') {
			alert("Minimal kode area dan nama Pelanggan diisi terlebih dahulu !!!");
		} else {}
	}

	function chkcriteriaa() {
		if (document.getElementById('chkcriterianew').checked) {
			document.getElementById('chkcriteriaupdate').checked = false;
			document.getElementById('chkcriterianew').value = 'on';
			document.getElementById('chkcriteriaupdate').value = '';
		} else {
			document.getElementById('chkcriteriaupdate').checked = true;
			document.getElementById('chkcriterianew').value = '';
			document.getElementById('chkcriteriaupdate').value = 'on';
		}
		//alert(document.getElementById('chkcriterianew').value);
	}

	function chkcriteriab() {
		if (document.getElementById('chkcriteriaupdate').checked) {
			document.getElementById('chkcriterianew').checked = false;
			document.getElementById('chkcriteriaupdate').value = 'on';
			document.getElementById('chkcriterianew').value = '';
		} else {
			document.getElementById('chkcriterianew').checked = true;
			document.getElementById('chkcriteriaupdate').value = '';
			document.getElementById('chkcriterianew').value = 'on';
		}
	}

	function lamaberdiri() {
		var curDate = new Date();
		startDate = document.getElementById("ecustomeryear").value + '-' + document.getElementById("ecustomermonth").value + '-01';
		if (startDate.length == 10) {
			thn = curDate.getFullYear().toString();
			bl = curDate.getMonth() + 1;
			bln = '0' + bl.toString();
			endDate = thn + '-' + bln + '-01';
			curDate.DateDiff({
				interval: "yyyy",
				date1: startDate,
				date2: endDate
			});
			//		alert(curDate.difference);
			document.getElementById("ecustomerage").value = curDate.difference;
		}
	}

	function chkdemtoko1() {
		if (document.getElementById('chkidemtoko1').checked == true) {
			document.getElementById('chkidemtoko1').value = 'on';
			document.getElementById('ecustomerowneraddress').value = document.getElementById('ecustomeraddress').value;
			document.getElementById('ert2').value = document.getElementById('ert1').value;
			document.getElementById('erw2').value = document.getElementById('erw1').value;
			document.getElementById('epostal2').value = document.getElementById('epostal1').value;
			document.getElementById('ecustomerkelurahan2').value = document.getElementById('ecustomerkelurahan1').value;
			document.getElementById('ecustomerkecamatan2').value = document.getElementById('ecustomerkecamatan1').value;
			document.getElementById('ecustomerkota2').value = document.getElementById('ecustomerkota1').value;
			document.getElementById('ecustomerprovinsi2').value = document.getElementById('ecustomerprovinsi1').value;
			document.getElementById('ecustomerownerphone').value = document.getElementById('ecustomerphone').value;
			document.getElementById('ecustomerownerfax').value = document.getElementById('efax1').value;
		} else {
			document.getElementById('chkidemtoko1').value = '';
			document.getElementById('ecustomerowneraddress').value = '';
			document.getElementById('ert2').value = '';
			document.getElementById('erw2').value = '';
			document.getElementById('epostal2').value = '';
			document.getElementById('ecustomerkelurahan2').value = '';
			document.getElementById('ecustomerkecamatan2').value = '';
			document.getElementById('ecustomerkota2').value = '';
			document.getElementById('ecustomerprovinsi2').value = '';
			document.getElementById('ecustomerownerphone').value = '';
			document.getElementById('ecustomerownerfax').value = '';
		}

	}

	function chkdemtoko2() {
		if (document.getElementById('chkidemtoko2').checked == true) {
			document.getElementById('chkidemtoko3').checked == false
			document.getElementById('chkidemtoko3').value = '';
			document.getElementById('chkidemtoko2').value = 'on';
			document.getElementById('ecustomersendaddress').value = document.getElementById('ecustomeraddress').value;
			document.getElementById('ert3').value = document.getElementById('ert1').value;
			document.getElementById('erw3').value = document.getElementById('erw1').value;
			document.getElementById('epostal3').value = document.getElementById('epostal1').value;
			document.getElementById('ecustomerkota3').value = document.getElementById('ecustomerkota1').value;
			document.getElementById('ecustomerprovinsi3').value = document.getElementById('ecustomerprovinsi1').value;
			document.getElementById('ecustomersendphone').value = document.getElementById('ecustomerphone').value;
		} else {
			document.getElementById('chkidemtoko2').value = '';
			document.getElementById('ecustomersendaddress').value = '';
			document.getElementById('ert3').value = '';
			document.getElementById('erw3').value = '';
			document.getElementById('epostal3').value = '';
			document.getElementById('ecustomerkota3').value = '';
			document.getElementById('ecustomerprovinsi3').value = '';
			document.getElementById('ecustomersendphone').value = '';
		}
	}

	function chkdemtoko3() {
		if (document.getElementById('chkidemtoko3').checked == true) {
			document.getElementById('chkidemtoko2').checked == false
			document.getElementById('chkidemtoko2').value = '';
			document.getElementById('chkidemtoko3').value = 'on';
			document.getElementById('ecustomersendaddress').value = document.getElementById('ecustomerowneraddress').value;
			document.getElementById('ert3').value = document.getElementById('ert2').value;
			document.getElementById('erw3').value = document.getElementById('erw2').value;
			document.getElementById('epostal3').value = document.getElementById('epostal2').value;
			document.getElementById('ecustomerkota3').value = document.getElementById('ecustomerkota2').value;
			document.getElementById('ecustomerprovinsi3').value = document.getElementById('ecustomerprovinsi2').value;
			document.getElementById('ecustomersendphone').value = document.getElementById('ecustomerownerphone').value;
		} else {
			document.getElementById('chkidemtoko3').value = '';
			document.getElementById('ecustomersendaddress').value = '';
			document.getElementById('ert3').value = '';
			document.getElementById('erw3').value = '';
			document.getElementById('epostal3').value = '';
			document.getElementById('ecustomerkota3').value = '';
			document.getElementById('ecustomerprovinsi3').value = '';
			document.getElementById('ecustomersendphone').value = '';
		}
	}

	function copydisc() {
		document.getElementById('ncustomerdiscount1').value = document.getElementById('ncustomerdiscount').value;
		hitungnilai(0, 0);
	}

	function tambah_item(a) {
		so_inner = document.getElementById("detailheader").innerHTML;
		si_inner = document.getElementById("detailisi").innerHTML;
		if (so_inner == '') {
			so_inner = '<table id="itemtem" class="listtable" style="width:900px;">';
			so_inner += '<tr><th style="width:25px;"  align="center">No</th>';
			so_inner += '<th style="width:63px;" align="center">Kode</th>';
			so_inner += '<th style="width:300px;" align="center">Nama Barang</th>';
			so_inner += '<th style="width:100px;" align="center">Motif</th>';
			so_inner += '<th style="width:90px;"  align="center">Harga</th>';
			so_inner += '<th style="width:46px;"  align="center">Jml Psn</th>';
			so_inner += '<th style="width:94px;"  align="center">Total</th>';
			so_inner += '<th style="width:180px;"  align="center">Keterangan</th>';
			so_inner += '<th style="width:32px;"  align="center" class="Action">Act</th></tr>';
			document.getElementById("detailheader").innerHTML = so_inner;
		} else {
			so_inner = '';
		}
		if (si_inner == '') {
			document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
			juml = document.getElementById("jml").value;
			si_inner = '<tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
			si_inner += '<td style="width:60px;"><input style="width:60px; font-size:12px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
			si_inner += '<td style="width:268px;"><input style="width:268px; font-size:12px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
			si_inner += '<td style="width:94px;"><input readonly style="width:94px; font-size:12px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value=""></td>';
			si_inner += '<td style="width:84px;"><input readonly style="text-align:right; width:84px; font-size:12px;"  type="text" id="vproductretail' + a + '" name="vproductretail' + a + '" value=""></td>';
			si_inner += '<td style="width:44px;"><input style="text-align:right; width:44px; font-size:12px;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(this.value,' + juml + ')"></td>';
			si_inner += '<td style="width:87px;"><input readonly" style="text-align:right; width:87px; font-size:12px;" type="text" id="vtotal' + a + '" name="vtotal' + a + '" value=""></td';
			si_inner += '<td style="width:168px;"><input style="width:168px; font-size:12px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
			si_inner += '<td style="width:48px;">&nbsp;</td></tr></tbody>';
		} else {
			document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
			juml = document.getElementById("jml").value;
			si_inner += '<tbody><tr><td style="width:25px;"><input style="width:25px; font-size:12px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"><input type="hidden" id="motif' + a + '" name="motif' + a + '" value=""></td>';
			si_inner += '<td style="width:60px;"><input style="width:60px; font-size:12px;" readonly type="text" id="iproduct' + a + '" name="iproduct' + a + '" value=""></td>';
			si_inner += '<td style="width:268px;"><input style="width:268px; font-size:12px;" readonly type="text" id="eproductname' + a + '" name="eproductname' + a + '" value=""></td>';
			si_inner += '<td style="width:94px;"><input readonly style="width:94px; font-size:12px;"  type="text" id="emotifname' + a + '" name="emotifname' + a + '" value=""></td>';
			si_inner += '<td style="width:84px;"><input readonly style="text-align:right; width:84px; font-size:12px;"  type="text" id="vproductretail' + a + '" name="vproductretail' + a + '" value=""></td>';
			si_inner += '<td style="width:44px;"><input style="text-align:right; width:44px; font-size:12px;" type="text" id="norder' + a + '" name="norder' + a + '" value="" onkeyup="hitungnilai(this.value,' + juml + ')"></td>';
			si_inner += '<td style="width:87px;"><input readonly" style="text-align:right; width:87px; font-size:12px;" type="text" id="vtotal' + a + '" name="vtotal' + a + '" value=""></td';
			si_inner += '<td style="width:168px;"><input style="width:168px; font-size:12px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
			si_inner += '<td style="width:48px;">&nbsp;</td></tr></tbody>';
		}
		j = 0;
		var baris = Array()
		var iproduct = Array();
		var eproductname = Array();
		var vproductretail = Array();
		var norder = Array();
		var motif = Array();
		var motifname = Array();
		var vtotal = Array();
		var eremark = Array();
		for (i = 1; i < a; i++) {
			j++;
			baris[j] = document.getElementById("baris" + i).value;
			iproduct[j] = document.getElementById("iproduct" + i).value;
			eproductname[j] = document.getElementById("eproductname" + i).value;
			vproductretail[j] = document.getElementById("vproductretail" + i).value;
			norder[j] = document.getElementById("norder" + i).value;
			motif[j] = document.getElementById("motif" + i).value;
			motifname[j] = document.getElementById("emotifname" + i).value;
			vtotal[j] = document.getElementById("vtotal" + i).value;
			eremark[j] = document.getElementById("eremark" + i).value;
		}
		document.getElementById("detailisi").innerHTML = si_inner;
		j = 0;
		for (i = 1; i < a; i++) {
			j++;
			document.getElementById("baris" + i).value = baris[j];
			document.getElementById("iproduct" + i).value = iproduct[j];
			document.getElementById("eproductname" + i).value = eproductname[j];
			document.getElementById("vproductretail" + i).value = vproductretail[j];
			document.getElementById("norder" + i).value = norder[j];
			document.getElementById("motif" + i).value = motif[j];
			document.getElementById("emotifname" + i).value = motifname[j];
			document.getElementById("vtotal" + i).value = vtotal[j];
			document.getElementById("eremark" + i).value = eremark[j];
		}
		showModal("customernew/cform/product/" + a + "/" + document.getElementById("ipricegroup").value + "/xzqf/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function hitungnilai(isi, jml) {
		jml = document.getElementById("jml").value;
		if (isNaN(parseFloat(isi))) {

			alert("Input harus numerik");
		} else {
			dtmp1 = parseFloat(formatulang(document.getElementById("ncustomerdiscount1").value));
			dtmp2 = parseFloat(formatulang(document.getElementById("ncustomerdiscount2").value));
			dtmp3 = parseFloat(formatulang(document.getElementById("ncustomerdiscount3").value));
			vdis1 = 0;
			vdis2 = 0;
			vdis3 = 0;
			vtot = 0;
			for (i = 1; i <= jml; i++) {
				vhrg = formatulang(document.getElementById("vproductretail" + i).value);
				nqty = formatulang(document.getElementById("norder" + i).value);
				vhrg = parseFloat(vhrg) * parseFloat(nqty);
				vtot = vtot + vhrg;
				document.getElementById("vtotal" + i).value = formatcemua(vhrg);
			}
			vdis1 = vdis1 + ((vtot * dtmp1) / 100);
			vdis2 = vdis2 + (((vtot - vdis1) * dtmp2) / 100);
			vdis3 = vdis3 + (((vtot - (vdis1 + vdis2)) * dtmp3) / 100);
			document.getElementById("vcustomerdiscount1").value = formatcemua(vdis1);
			document.getElementById("vcustomerdiscount2").value = formatcemua(vdis2);
			document.getElementById("vcustomerdiscount3").value = formatcemua(vdis3);
			vdis1 = parseFloat(vdis1);
			vdis2 = parseFloat(vdis2);
			vdis3 = parseFloat(vdis3);
			vtotdis = vdis1 + vdis2 + vdis3;
			vtotdis = Math.round(vtotdis);
			document.getElementById("vspbdiscounttotal").value = formatcemua(vtotdis);
			document.getElementById("vspb").value = formatcemua(vtot);
			vtotbersih = parseFloat(vtot) - parseFloat(vtotdis);
			document.getElementById("vspbbersih").value = formatcemua(vtotbersih);
		}
	}

	function dipales(a) {
		// cek = 'false';
		if (
			(document.getElementById("ecustomername").value != '') &&
			(document.getElementById("dsurvey").value != '') &&
			(document.getElementById("isalesman").value != '') &&
			(document.getElementById("ncustomertoplength").value != '') &&
			(document.getElementById("ncustomerdiscount").value != '') &&
			(document.getElementById("nvisitperiod").value != '') &&
			(document.getElementById("iarea").value != '') &&
			(document.getElementById("icustomergroup").value != '') &&
			(document.getElementById("icustomerproducttype").value != '') &&
			(document.getElementById("icustomerstatus").value != '') &&
			(document.getElementById("icustomergrade").value != '') &&
			(document.getElementById("icustomerservice").value != '') &&
			(document.getElementById("icustomersalestype").value != '') &&
			(document.getElementById("ipricegroup").value != '') &&
			(document.getElementById("ipaymentmethod").value != '') &&
			/* (document.getElementById("inik").value!='') && */
			// (document.getElementById("irefcode").value != '') &&
			((document.getElementById("ecustomernpwp").value == '' && document.getElementById("inik").value != '') || (document.getElementById("ecustomernpwp").value != ''))
		) {
			// if (a == 0) {
			// 	alert('Isi data item minimal 1 !!!');
			// } else {
			// 	for (i = 1; i <= a; i++) {
			// 		if ((document.getElementById("iproduct" + i).value == '') ||
			// 			(document.getElementById("eproductname" + i).value == '') ||
			// 			(document.getElementById("norder" + i).value == '')) {
			// 			alert('Data item masih ada yang salah !!!');
			// 			exit();
			// 			cek = 'false';
			// 		} else {
			// 			cek = 'true';
			// 		}
			// 	}
			// }
			// console.log(cek)
			// if (cek == 'true') {
			document.getElementById("login").hidden = true;
			// document.getElementById("cmdtambahitem").hidden = true;
			// } else {
			// document.getElementById("login").hidden = false;
			// }
		} else {
			// alert('Data header masih ada yang salah !!!');
			if (document.getElementById("iarea").value == '') {
				alert('Area tidak boleh kosong !!!');
				document.getElementById("iarea").focus();
				return false;
			} else if (document.getElementById("isalesman").value == '') {
				alert('Sales tidak boleh kosong !!!');
				document.getElementById("isalesman").focus();
				return false;
			}
			/* else if(document.getElementById("dsurvey").value == ''){
				alert('Tanggal Survey tidak boleh kosong !!!');
				document.getElementById("dsurvey").focus();
				return false;
			} */
			else if (document.getElementById("nvisitperiod").value == '') {
				alert('Periode Kunjungan tidak boleh kosong !!!');
				document.getElementById("nvisitperiod").focus();
				return false;
			} else if (document.getElementById("icity").value == '') {
				alert('Kota tidak boleh kosong !!!');
				document.getElementById("icity").focus();
				return false;
			} else if (document.getElementById("ecustomername").value == '') {
				alert('Nama Pelanggan tidak boleh kosong !!!');
				document.getElementById("ecustomername").focus();
				return false;
			} else if (document.getElementById("inik").value == '') {
				alert('NIK tidak boleh kosong !!!');
				document.getElementById("inik").focus();
				return false;
			} else if (document.getElementById("icustomerclass").value == '') {
				alert('Tipe Pelanggan tidak boleh kosong !!!');
				document.getElementById("icustomerclass").focus();
				return false;
			} else if (document.getElementById("ipaymentmethod").value == '') {
				alert('Pola Pembayaran tidak boleh kosong !!!');
				document.getElementById("ipaymentmethod").focus();
				return false;
			} else if (document.getElementById("ncustomertoplength").value == '') {
				alert('TOP tidak boleh kosong !!!');
				document.getElementById("ncustomertoplength").focus();
				return false;
			} else if (document.getElementById("ncustomerdiscount").value == '') {
				alert('Diskon tidak boleh kosong !!!');
				document.getElementById("ncustomerdiscount").focus();
				return false;
			} else if (document.getElementById("icustomergroup").value == '') {
				alert('Grup Pelanggan tidak boleh kosong !!!');
				document.getElementById("icustomergroup").focus();
				return false;
			} else if (document.getElementById("icustomerproducttype").value == '') {
				alert('Tipe Produk tidak boleh kosong !!!');
				document.getElementById("icustomerproducttype").focus();
				return false;
			} else if (document.getElementById("icustomerstatus").value == '') {
				alert('Status Pelanggan tidak boleh kosong !!!');
				document.getElementById("icustomerstatus").focus();
				return false;
			} else if (document.getElementById("icustomergrade").value == '') {
				alert('Tingkat Pelanggan tidak boleh kosong !!!');
				document.getElementById("icustomergrade").focus();
				return false;
			} else if (document.getElementById("icustomerservice").value == '') {
				alert('Jenis Pelayanan tidak boleh kosong !!!');
				document.getElementById("icustomerservice").focus();
				return false;
			} else if (document.getElementById("icustomersalestype").value == '') {
				alert('Cara Penjualan tidak boleh kosong !!!');
				document.getElementById("icustomersalestype").focus();
				return false;
			}
			/* else if (document.getElementById("dspb").value == '') {
				alert('Tanggal SPB tidak boleh kosong !!!');
				document.getElementById("dspb").focus();
				return false;
			} */
			else if (document.getElementById("ipricegroup").value == '') {
				alert('Kelompok Harga tidak boleh kosong !!!');
				document.getElementById("ipricegroup").focus();
				return false;
			} else if (document.getElementById("irefcode").value == '') {
				alert('Kode Master tidak boleh kosong !!!');
				document.getElementById("irefcode").focus();
				return false;
			}
		}
	}

	function dipales_ori(a) {
		cek = 'false';
		if ((document.getElementById("dspb").value != '') &&
			(document.getElementById("ecustomername").value != '') &&
			(document.getElementById("iarea").value != '') &&
			(document.getElementById("ipricegroup").value != '')) {
			if (a == 0) {
				alert('Isi data item minimal 1 !!!');
			} else {
				for (i = 1; i <= a; i++) {
					if ((document.getElementById("iproduct" + i).value == '') ||
						(document.getElementById("eproductname" + i).value == '') ||
						(document.getElementById("norder" + i).value == '')) {
						alert('Data item masih ada yang salah !!!');
						exit();
						cek = 'false';
					} else {
						cek = 'true';
					}
				}
			}
			if (cek == 'true') {
				document.getElementById("login").disabled = true;
				document.getElementById("cmdtambahitem").disabled = true;
			} else {
				document.getElementById("login").disabled = false;
			}
		} else {
			alert('Data header masih ada yang salah !!!');
		}
	}

	function refreshharga() {
		jml = document.getElementById("jml").value;
		for (i = 1; i <= jml; i++) {
			document.getElementById("vproductretail" + i).value = document.getElementById("hrgnew" + i).value;
		}
		hitungnilai(0, jml);
	}

	function cekparkir() {
		if (document.getElementById("fparkir").value == 'on') {
			document.getElementById("fparkir").value = '';
		} else {
			document.getElementById("fparkir").value = 'on';
		}
	}

	function cekkuli() {
		if (document.getElementById("fkuli").value == 'on') {
			document.getElementById("fkuli").value = '';
		} else {
			document.getElementById("fkuli").value = 'on';
		}
	}

	function cekkontrabon() {
		if (document.getElementById("fkontrabon").value == 'on') {
			document.getElementById("fkontrabon").value = '';
		} else {
			document.getElementById("fkontrabon").value = 'on';
		}
	}
</script>