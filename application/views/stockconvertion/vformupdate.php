<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('stockconvertion/cform/update', array('id' => 'stockconvertionformupdate', 'name' => 'stockconvertionformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	  <div class="effect">
	    <div class="accordion2">
	      <table class="mastertable">
	        <tr>
			  <td width="10%">Tanggal KS</td>
			  <td width="1%">:</td>
			  <td width="89%"><?php 
			 	$data = array(
				  'name'        => 'iicconvertion',
				  'id'          => 'iicconvertion',
				  'value'       => $isi->i_ic_convertion,
				  'readonly'    => 'true',
				  'maxlength'   => '13');
				echo form_input($data);
				$tmp=explode("-",$isi->d_ic_convertion);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$dicconvertion=$hr."-".$bl."-".$th;
      ?>
        <input hidden id="bicconvertion" name="bicconvertion" value="<?php echo $bl; ?>">
      <?php 
        $iperiode=$th.$bl;
				$data = array(
					  'name'        => 'dicconvertion',
					  'id'          => 'dicconvertion',
					  'value'       => $dicconvertion,
					  'readonly'    => 'true',
					  'onclick'     => "showCalendar('',this,this,'','dicconvertion',0,20,1)"   );
				echo form_input($data);?>
				<input type="hidden" id="tglicconvertion" name="tglicconvertion" value="<?php echo $dicconvertion; ?>">
				<input type="hidden" id="ibbk" name="ibbk" value="<?php echo $isi->i_bbk; ?>">
				<input type="hidden" id="ibbm" name="ibbm" value="<?php echo $isi->i_bbm; ?>">
			  </td>
	      	</tr>
          <tr>
			  <td width="10%">Referensi</td>
			  <td width="1%">:</td>
        <?php 
          if($isi->d_refference!=''){		
            $tmp=explode("-",$isi->d_refference);
				    $th=$tmp[0];
				    $bl=$tmp[1];
				    $hr=$tmp[2];
				    $drefference=$hr."-".$bl."-".$th;
          }else{
            $drefference='';
          }
        ?>
			  <td width="89%"><input readonly id="irefference" name="irefference" type="text" value="<?php echo $isi->i_refference; ?>">
			                  <input readonly id="drefference" name="drefference" value="<?php echo $drefference; ?>"></td>
          </tr>
	      	<tr>
			  <td width="10%">&nbsp;</td>
			  <td width="1%">&nbsp;</td>
			  <td width="89%">
          <?php if($isi->i_refference=='' && $status=='f'){ ?>
			    	<input name="login" id="login" value="Simpan" type="submit" 
					   onclick="dipales(parseFloat(document.getElementById('jml1').value)+1);">
          <?php }?>
				  <input name="cmdreset" id="cmdreset" value="Keluar" type="reset" onclick='show("listks/cform/view/<?php echo $iperiode."/"?>","#main")'>
          <?php if($isi->i_refference==''){ ?>
				    <input name="cmdtambahitem1" id="cmdtambahitem1" value="Tambah Produk Asal" type="button" onclick="tambah_item1(parseFloat(document.getElementById('jml1').value)+1,parseFloat(document.getElementById('jml2').value));">
          <?php }?>
          <?php if($isi->i_refference==''){ ?>
				    <input name="cmdtambahitem2" id="cmdtambahitem2" value="Tambah Produk Jadi" type="button" onclick="tambah_item2(parseFloat(document.getElementById('jml2').value)+1,parseFloat(document.getElementById('jml1').value));">
          <?php }?>
			  </td>
	      	</tr>
	      </table>
		  <div id="detailheader1" align=center>
			<table class="listtable" style="width:750px;">
			  <th align="center" colspan="5">***** Produk Asal *****</th>
			  <tr>
			  <th width="100px" align="center">Kd Barang</th>
			  <th width="30px"align="center">Grade</th>
			  <th width="520px" align="center">Nama Barang</th>
			  <th width="50px" align="center">Jumlah</th>
			  <th width="50px" align="center" class="action">Action</th>
			  </tr>
		  	</table>
		  </div>
		  <div id="detailisi1" align=center>
		    <?php 
			  if($isi->f_ic_convertion=='t'){
		    ?>
		    <table class="listtable" style="width:750px;"><tbody>
			  <tr>
			    <td width="94px">
				  <input style="width:94px" readonly type="text" id="iproduct1" name="iproduct1" value="<?php echo $isi->i_product; ?>">
			    </td>
			    <td width="44px">
				  <input type="hidden" id="iproductmotif1" name="iproductmotif1" value="<?php echo $isi->i_product_motif; ?>">
				  <input type="hidden" id="vproductretail1" name="vproductretail1" value="0">
				  <input style="width:44px" readonly type="text" id="iproductgrade1" name="iproductgrade1" 
				  value="<?php echo $isi->i_product_grade; ?>">
			    </td>
			    <td width="478px">
				  <input style="width:478px" readonly type="text" id="eproductname1" name="eproductname1" value="<?php echo $isi->e_product_name; ?>">
			    </td>
			    <td width="54px">
				  <input style="width:54px; text-align:right;" type="text" id="nicconvertion1" name="nicconvertion1" value="<?php echo $isi->n_ic_convertion; ?>"><input type="hidden" id="nicconvertion1x" name="nicconvertion1x" value="<?php echo $isi->n_ic_convertion; ?>">
			    </td>
			    <td width="50px" class="action">
				  &nbsp;
			    </td>
			  </tr>
		    </table>
		    <?php 
			  }else{
			    $a=0;
			    foreach($detail as $row)
			    {
				  $a++;
		    ?>
				  <table class="listtable" style="width:750px;"><tbody>
				    <tr>
					  <td width="94px">
						  <input style="width:94px" readonly type="text" id="iproduct<?php echo $a;?>" 
						  name="iproduct<?php echo $a;?>" value="<?php echo $row->i_product; ?>"></td>
					  <td width="44px">
						  <input type="hidden" id="iproductmotif<?php echo $a;?>" name="iproductmotif<?php echo $a;?>" 
						  value="<?php echo $row->i_product_motif; ?>">
						  <input type="hidden" id="vproductretail<?php echo $a;?>" name="vproductretail<?php echo $a;?>" value="0">
						  <input style="width:44px" readonly type="text" id="iproductgrade<?php echo $a;?>" 
						  name="iproductgrade<?php echo $a;?>" value="<?php echo $row->i_product_grade; ?>"></td>
					  <td width="478px">
						  <input style="width:478px" readonly type="text" id="eproductname<?php echo $a;?>" 
						  name="eproductname<?php echo $a;?>" value="<?php echo $row->e_product_name; ?>"></td>
					  <td width="54px">
						  <input style="width:54px; text-align:right;" type="text" id="nicconvertion<?php echo $a;?>" 
						  name="nicconvertion<?php echo $a;?>" value="<?php echo $row->n_ic_convertion; ?>"><input type="hidden" id="nicconvertion<?php echo $a;?>x" 
						  name="nicconvertion<?php echo $a;?>x" value="<?php echo $row->n_ic_convertion; ?>"></td>
					  <td width="50px" class="action">
						  <?php if($status=='f'){
						  echo "<a href=\"#\" onclick='hapus(\"stockconvertion/cform/deletedetail/$row->i_ic_convertion/$row->i_product/$row->i_product_grade/$row->i_product_motif/$row->n_ic_convertion/$isi->f_ic_convertion/\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
						  }?>
					  </td>
				    </tr>
				  </table>
		    <?php 
			    }
			  }
		    ?>
		  </div>
		  <div id="detailheader2" align=center>
		    <table class="listtable" style="width:750px;">
			  <th align="center" colspan="5">***** Produk Jadi *****</th>
			  <tr>
			  <th width="100px" align="center">Kd Barang</th>
			  <th width="30px"align="center">Grade</th>
			  <th width="520px" align="center">Nama Barang</th>
			  <th width="50px" align="center">Jumlah</th>
			  <th width="50px" align="center" class="action">Action</th>
			  </tr>
		    </table>
		  </div>
		  <div id="detailisi2" align=center>
		    <?php 
			  if($isi->f_ic_convertion=='f'){
		    ?>
			    <table class="listtable" style="width:750px;"><tbody>
				  <tr>
				    <td width="94px">
					  <input style="width:94px" readonly type="text" id="2iproduct1" name="2iproduct1" value="<?php echo $isi->i_product; ?>"></td>
				    <td width="44px">
					  <input type="hidden" id="2iproductmotif1" name="2iproductmotif1" value="<?php echo $isi->i_product_motif; ?>">
					  <input type="hidden" id="2vproductretail1" name="2vproductretail1" value="0">
					  <input style="width:44px" readonly type="text" id="2iproductgrade1" name="2iproductgrade1" 
					  value="<?php echo $isi->i_product_grade; ?>"></td>
				    <td width="478px">
					  <input style="width:478px" readonly type="text" id="2eproductname1" name="2eproductname1" 
					  value="<?php echo $isi->e_product_name; ?>"></td>
				    <td width="54px">
					  <input style="width:54px; text-align:right;" type="text" id="2nicconvertion1" name="2nicconvertion1" value="<?php echo $isi->n_ic_convertion; ?>"><input type="hidden" id="2nicconvertion1x" name="2nicconvertion1x" value="<?php echo $isi->n_ic_convertion; ?>"></td>
				    <td width="50px" class="action">&nbsp;</td>
			      </tr>
			    </table>
		    <?php 
			  }else{
			    $a=0;
			    foreach($detail as $row)
			    {
				  $a++;
		    ?>
				  <table class="listtable" style="width:750px;"><tbody>
				    <tr>
					  <td width="94px">
						  <input style="width:94px" readonly type="text" id="2iproduct<?php echo $a;?>" 
						  name="2iproduct<?php echo $a;?>" value="<?php echo $row->i_product; ?>"></td>
					  <td width="44px">
<!-- baru sampe disini 17-04-2009-->
					   	  <input type="hidden" id="2iproductmotif<?php echo $a;?>" name="2iproductmotif<?php echo $a;?>" 
					      value="<?php echo $row->i_product_motif; ?>">
					  	  <input type="hidden" id="2vproductretail<?php echo $a;?>" name="2vproductretail<?php echo $a;?>" value="0">
					  	  <input style="width:44px" readonly type="text" id="2iproductgrade<?php echo $a;?>" 
					  	  name="2iproductgrade<?php echo $a;?>" value="<?php echo $row->i_product_grade; ?>"></td>
					  <td width="478px">
					  	  <input style="width:478px" readonly type="text" id="2eproductname<?php echo $a;?>" 
					  	  name="2eproductname<?php echo $a;?>" value="<?php echo $row->e_product_name; ?>"></td>
					  <td width="54px">
					  	  <input style="width:54px; text-align:right;" type="text" id="2nicconvertion<?php echo $a;?>" 
					  	  name="2nicconvertion<?php echo $a;?>" value="<?php echo $row->n_ic_convertion; ?>"><input type="hidden" id="2nicconvertion<?php echo $a;?>x" name="2nicconvertion<?php echo $a;?>x" value="<?php echo $row->n_ic_convertion; ?>"></td>
					  <td width="50px" class="action">
						  <?php if($status=='f'){
						  echo "<a href=\"#\" onclick='hapus(\"stockconvertion/cform/deletedetail/$row->i_ic_convertion/$row->i_product/$row->i_product_grade/$row->i_product_motif/$row->n_ic_convertion/$isi->f_ic_convertion/\",\"#tmp\")'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" border=\"0\" alt=\"delete\"></a>";
						  }
						  ?>
					  </td>
				  	</tr>
				  </table>
		    <?php 
			    }
		  	  }
		    ?>
		  </div>
<!--	      </table> -->
		  <div id="pesan"></div>
			<?php 
			  echo "<input type=\"hidden\" id=\"iicconvertiondelete\"	name=\"iicconvertiondelete\"	value=\"\">
		  		    <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		  		    <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
		  		    <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" 	value=\"\">
				    <input type=\"hidden\" id=\"nicconvertiondelete\"	name=\"nicconvertiondelete\"	value=\"\">
				    <input type=\"hidden\" id=\"ficconvertiondelete\"	name=\"ficconvertiondelete\"	value=\"\">";
			  if($isi->f_ic_convertion=='f'){
			?>
			<input type="hidden" name="jml1" id="jml1" value="<?php echo $a;?>">
			<input type="hidden" name="jml2" id="jml2" value="1">
			<?php 
			  }else{
			?>
			  <input type="hidden" name="jml1" id="jml1" value="1">
			  <input type="hidden" name="jml2" id="jml2" value="<?php echo $a;?>">
			<?php 
			  }
			?>
	  	  </div>
		</div>
		<?=form_close()?>
      </td>
    </tr>
  </table>
<script language="javascript" type="text/javascript">
  function xxx(a,b,c,d,e,f,g){
    if (confirm(g)==1){
	  document.getElementById("iicconvertiondelete").value=a;
	  document.getElementById("iproductdelete").value=b;
	  document.getElementById("iproductgradedelete").value=c;
	  document.getElementById("iproductmotifdelete").value=d;
	  document.getElementById("nicconvertiondelete").value=e;
	  document.getElementById("ficconvertiondelete").value=f;
	  formna=document.getElementById("stockconvertionformupdate");
	  formna.action="<?php echo site_url(); ?>"+"/stockconvertion/cform/deletedetail";
	  formna.submit();
    }
  }
  function tambah_item1(a,b){
	var jml1=parseFloat(document.getElementById("jml1").value);
	var jml2=parseFloat(document.getElementById("jml2").value);
	if(
		((jml2<=1) && (jml1<1)) ||
		((jml2>=1) && (jml1<1)) ||
		((jml2<=1) && (jml1>=1))
	  ){
		so_inner=document.getElementById("detailheader1").innerHTML;
		si_inner=document.getElementById("detailisi1").innerHTML;
		if(so_inner==''){
		so_inner='<table class="listtable" style="width:750px;"><tr><th align="center" colspan="5">***** Produk Asal *****</th></tr><tr><th width="100px" align="center">Kd Barang</th><th width="30px"align="center">Grade</th><th width="520px" align="center">Nama Barang</th><th width="50px" align="center">Jumlah</th><th width="50px" align="center" class="action">Action</th></tr></table>';
		document.getElementById("detailheader1").innerHTML=so_inner;
		}else{
		so_inner='';
		}
		if(si_inner==''){
		si_inner='<table class="listtable" style="width:750px;"><tbody><tr><td width="96px"><input style="width:96px" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td width="30px"><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input type="hidden" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""><input style="width:34px" readonly type="text" id="iproductgrade'+a+'" name="iproductgrade'+a+'" value=""></td><td width="489px"><input style="width:489px" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td width="51px"><input style="width:51px; text-align:right;" type="text" id="nicconvertion'+a+'" name="nicconvertion'+a+'" value=""></td><td width="50px">&nbsp;</td></tr></tbody></table>';
		document.getElementById("jml1").value=parseFloat(document.getElementById("jml1").value)+1;
		}else{
		document.getElementById("jml1").value=parseFloat(document.getElementById("jml1").value)+1;
		si_inner=si_inner+'<table class="listtable" style="width:750px;"><tbody><tr><td width="96px"><input style="width:96px" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td><td width="30px"><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input type="hidden" id="vproductretail'+a+'" name="vproductretail'+a+'" value=""><input style="width:34px" readonly type="text" id="iproductgrade'+a+'" name="iproductgrade'+a+'" value=""></td><td width="489px"><input style="width:489px" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td><td width="51px"><input style="width:51px; text-align:right;" type="text" id="nicconvertion'+a+'" name="nicconvertion'+a+'" value=""></td><td width="10%">&nbsp;</td></tr></tbody></table>';
		}
		j=0;
		var iproduct		=Array();
		var iproductgrade	=Array();
		var iproductmotif	=Array();	
		var vproductretail	=Array();
		var eproductname	=Array();
		var nicconvertion	=Array();
		for(i=1;i<a;i++){
			j++;
			iproduct[j]			= document.getElementById("iproduct"+i).value;
			iproductgrade[j]	= document.getElementById("iproductgrade"+i).value;
			eproductname[j]		= document.getElementById("eproductname"+i).value;
			nicconvertion[j]	= document.getElementById("nicconvertion"+i).value;	
			iproductmotif[j]	= document.getElementById("iproductmotif"+i).value;
			vproductretail[j]	= document.getElementById("vproductretail"+i).value;
		}
		document.getElementById("detailisi1").innerHTML=si_inner;
		j=0;
		for(i=1;i<a;i++){
			j++;
			document.getElementById("iproduct"+i).value=iproduct[j];
			document.getElementById("iproductgrade"+i).value=iproductgrade[j];
			document.getElementById("eproductname"+i).value=eproductname[j];
			document.getElementById("nicconvertion"+i).value=nicconvertion[j];
			document.getElementById("iproductmotif"+i).value=iproductmotif[j];
			document.getElementById("vproductretail"+i).value=vproductretail[j];
		}

		lebar =450;
		tinggi=400;
		eval('window.open("<?php echo site_url(); ?>"+"/stockconvertion/cform/productupdate/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
  }
  function tambah_item2(a,b){
	var jml1=parseFloat(document.getElementById("jml1").value);
	var jml2=parseFloat(document.getElementById("jml2").value);
	if(
		((jml2<1) && (jml1<=1)) ||
		((jml2>=1) && (jml1<=1)) ||
		((jml2<1) && (jml1>=1))
	  ){
		var so_inner=document.getElementById("detailheader2").innerHTML;
		var si_inner=document.getElementById("detailisi2").innerHTML;
		if(so_inner==''){
		so_inner='<table class="listtable" style="width:750px;"><tr><th align="center" colspan="5">***** Produk Jadi *****</th></tr><tr><th width="100px" align="center">Kd Barang</th><th width="30px"align="center">Grade</th><th width="520px" align="center">Nama Barang</th><th width="50px" align="center">Jumlah</th><th width="50px" align="center" class="action">Action</th></tr></table>';
		document.getElementById("detailheader2").innerHTML=so_inner;
		}else{
			so_inner='';
		}
		if(si_inner==''){
		si_inner='<table class="listtable" style="width:750px;"><tbody><tr><td width="96px"><input style="width:96px" readonly type="text" id="2iproduct'+a+'" name="2iproduct'+a+'" value=""></td><td width="30px"><input type="hidden" id="2iproductmotif'+a+'" name="2iproductmotif'+a+'" value=""><input type="hidden" id="2vproductretail'+a+'" name="2vproductretail'+a+'" value=""><input style="width:34px" readonly type="text" id="2iproductgrade'+a+'" name="2iproductgrade'+a+'" value=""></td><td width="489px"><input style="width:489px" readonly type="text" id="2eproductname'+a+'" name="2eproductname'+a+'" value=""></td><td width="51px"><input style="width:51px; text-align:right;" type="text" id="2nicconvertion'+a+'" name="2nicconvertion'+a+'" value=""></td><td width="50px">&nbsp;</td></tr></tbody></table>';
		document.getElementById("jml2").value=parseFloat(document.getElementById("jml2").value)+1;
		}else{
		document.getElementById("jml2").value=parseFloat(document.getElementById("jml2").value)+1;
		si_inner=si_inner+'<table class="listtable" style="width:750px;"><tbody><tr><td width="94px"><input style="width:94px" readonly type="text" id="2iproduct'+a+'" name="2iproduct'+a+'" value=""></td><td width="32px"><input type="hidden" id="2iproductmotif'+a+'" name="2iproductmotif'+a+'" value=""><input type="hidden" id="2vproductretail'+a+'" name="2vproductretail'+a+'" value=""><input style="width:32px" readonly type="text" id="2iproductgrade'+a+'" name="2iproductgrade'+a+'" value=""></td><td width="488px"><input style="width:488px" readonly type="text" id="2eproductname'+a+'" name="2eproductname'+a+'" value=""></td><td width="48px"><input style="width:48px; text-align:right;" type="text" id="2nicconvertion'+a+'" name="2nicconvertion'+a+'" value=""></td><td width="10%">&nbsp;</td></tr></tbody></table>';
		}
		j=0;
		var iproduct	  	=Array();
		var iproductgrade	=Array();
		var eproductname	=Array();
		var nicconvertion	=Array();
		var iproductmotif  	=Array();
		var vproductretail	=Array();
		for(i=1;i<a;i++){
			j++;
			iproduct[j]			= document.getElementById("2iproduct"+i).value;
			iproductgrade[j]	= document.getElementById("2iproductgrade"+i).value;
			eproductname[j]		= document.getElementById("2eproductname"+i).value;
			nicconvertion[j]	= document.getElementById("2nicconvertion"+i).value;	
			iproductmotif[j]	= document.getElementById("2iproductmotif"+i).value;
			vproductretail[j]	= document.getElementById("2vproductretail"+i).value;
		}
		document.getElementById("detailisi2").innerHTML=si_inner;
		j=0;
		for(i=1;i<a;i++){
			j++;
			document.getElementById("2iproduct"+i).value=iproduct[j];
			document.getElementById("2iproductgrade"+i).value=iproductgrade[j];
			document.getElementById("2eproductname"+i).value=eproductname[j];
			document.getElementById("2nicconvertion"+i).value=nicconvertion[j];
			document.getElementById("2iproductmotif"+i).value=iproductmotif[j];
			document.getElementById("2vproductretail"+i).value=vproductretail[j];
		}
		lebar =450;
		tinggi=400;
		eval('window.open("<?php echo site_url(); ?>"+"/stockconvertion/cform/productupdate2/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
	}
  }
  function dipales(a){
	  var b=parseFloat(document.getElementById("dicconvertion").value)+1;
	  cek='false';
	  if((document.getElementById("dicconvertion").value!='')) {
	 	  if( (a==1) || (b==1)){
	 		  alert('Isi data item minimal 1 !!!');
	 	  }else{
  			for(i=1;i<a;i++){
				  if((document.getElementById("iproduct"+i).value=='') ||
					  (document.getElementById("iproductgrade"+i).value=='') ||
					  (document.getElementById("eproductname"+i).value=='') ||
					  (document.getElementById("nicconvertion"+i).value=='')){
					  alert('Data item masih ada yang salah !!!');
					  cek='false';
					  break;
				  }else{
					  cek='true';	
				  } 
			  }
		  	cek='false';
			  for(i=1;i<b;i++){
				  if((document.getElementById("2iproduct"+i).value=='') ||
					  (document.getElementById("2iproductgrade"+i).value=='') ||
					  (document.getElementById("2eproductname"+i).value=='') ||
					  (document.getElementById("2nicconvertion"+i).value=='')){
					  alert('Data item masih ada yang salah !!!');
					  cek='false';
					  break;
				  }else{
					  cek='true';	
				  }
			  }
	    }
//      alert(cek);
	    if(cek=='true'){
	  		document.getElementById("login").disabled=true;
    	}else{
	     	document.getElementById("login").disabled=false;
	    }
   	}else{
	    alert('Data header masih ada yang salah !!!');
 	  }
  }
  function clearitem(){
    document.getElementById("detailisi1").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml1").value='0';
    document.getElementById("login").disabled=false;
  }
  function afterSetDateValue(ref_field, target_field, date) {
    dspb=document.getElementById('dicconvertion').value;
    bspb=document.getElementById('bicconvertion').value;
    dtmp=dspb.split('-');
    per=dtmp[2]+dtmp[1]+dtmp[0];
    bln = dtmp[1];
    if( (bspb!='') && (dspb!='') ){
      if(bspb != bln)
      {
        alert("Tanggal Konversi tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("dicconvertion").value=document.getElementById("tglicconvertion").value;
      }
    }
  }
</script>
