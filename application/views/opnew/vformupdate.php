<table class="maintable">
  	<tr>
    <td align="left">
       <?php echo form_open('opnew/cform/updateop', array('id' => 'spbformupdate', 'name' => 'spbformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
			<tr>
				<td>No OP</td>
				<td>
					<?php 
						$tmp=explode("-",$isi->d_op);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$dop=$hr."-".$bl."-".$th;
					?>
        			<input hidden id="bop" name="bop" value="<?php echo $bl; ?>">
				    <input type="text" id="iop" name="iop" value="<?php echo $isi->i_op; ?>">
					<input readonly id="dop" name="dop" value="<?php echo $dop; ?>" readonly 
					   onclick="showCalendar('',this,this,'','dop',0,20,1)" onblur="cektgl()">
					<input hidden id="tglop" name="tglop" value="<?php echo $dop; ?>"">
				</td>
				<td>Batas Kirim</td>
				<td><input style="text-align:right;" maxlength=3 id="ndeliverylimit" name="ndeliverylimit" value="1"></td>
	    	</tr>
			<tr>
				<td>Status</td>
				<td><input readonly onclick="view_status();" id="eopstatusname" name="eopstatusname" value="<?php echo $isi->e_op_statusname; ?>">
					<input type="hidden" id="iopstatus" name="iopstatus" value="<?php echo $isi->i_op_status; ?>"></td>
				<td>TOP</td>
				<td><input readonly style="text-align:right;" maxlength=3 id="ntoplength" name="ntoplength" value="<?php echo $isi->n_top_length; ?>">
				<input maxlength=7 id="iopold" name="iopold" value="<?php echo $isi->i_op_old; ?>"></td>
	    	</tr>
			<tr>
			  	<td>Keterangan</td>
			  	<td><input name="eopremark" id="eopremark" value="<?php echo $isi->e_op_remark; ?>" type="text" onkeyup="gede(this)"></td>
				<td>Supplier</td>
				<td><input readonly  id="esuppliername" name="esuppliername" value="<?php echo $isi->e_supplier_name; ?>">
					<input type="hidden" id="isupplier" name="isupplier" value="<?php echo $supplier; ?>">
				</td>
	    	</tr>
	    	<tr>
				<td>SPB</td>
				<?php 
					if(isset($isi->d_spb)){
						$tmp=explode("-",$isi->d_spb);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$dspb=$hr."-".$bl."-".$th;
					}else if(isset($isi->d_spmb)){
						$tmp=explode("-",$isi->d_spmb);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$dspb=$hr."-".$bl."-".$th;
					}
				?>
				<td><input id="ispb" name="ispb" value="<?php echo $ispb; ?>" readonly>
					<input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>" onclick="showCalendar('',this,this,'','dspb',0,20,1)"></td>
				<td>Pelanggan</td>
				<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan()" 
						   value="<?php echo $isi->e_customer_name; ?>">
				    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
	    	</tr>
	    	<tr>
				<td>Area</td>
				<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>" >
				    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
	    	</tr>
			<tr>
			  	<td width="100%" align="center" colspan="4">
			    <?php if($this->session->userdata("departement")=='6'){ ?><input name="login" id="login" value="Simpan" type="submit"
				   onclick="dipales(parseFloat(document.getElementById('jml').value));"><?php }?>
			    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listop/cform/view/<?php echo $dfrom."/".$dto."/"; ?>","#main")'></td>
			</tr>
	    </table>
		<div id="detailheader" align="center">
			<table class="listtable" style="width:750px;">
				<th style="width:25px;" align="center">No</th>
				<th style="width:65px;" align="center">Kd Barang</th>
				<th style="width:280px;" align="center">Nama Barang</th>
				<th style="width:160px;" align="center">Motif</th>
				<th style="width:100px;" align="center">Harga</th>
				<th style="width:40px;" align="center">Jml Psn</th>
				<th style="width:40px;" align="center">Jml Stock</th>
				<th style="width:40px;" align="center">Jml OP</th>
			</table>
		</div>
		<div id="detailisi" align="center">
			<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\">";
				$i=0;
				if(isset($detail)){
					foreach($detail as $row){
					  		$i++;
#						$this->db->select(" * from tr_product_price 
#											where i_product = '$row->i_product' 
#											and i_price_group='$row->i_price_group'", false);
#						$query = $this->db->get();
#						if ($query->num_rows() > 0){
#							foreach($query->result() as $riw){
							$pangaos=$row->v_product_mill;
							$pangaos=number_format($pangaos,2);
#						}
#						}else{
#							$pangaos='0.00';
#						}				
						$this->db->select(" * from tm_ic
											where i_product = '$row->i_product' 
											and i_product_motif='$row->i_product_motif'
											and i_store='$row->i_store'
											and i_store_location='$row->i_store_location'", false);
						$query = $this->db->get();
						if ($query->num_rows() > 0){
							foreach($query->result() as $raw){
								$jujum=number_format($raw->n_quantity_stock);
								$nop=number_format($row->n_order);
								if($jujum>0){
									if($jujum>=$row->n_order){
										$nop=0;						
									}else{
										$nop=$row->n_order-$jujum;
									}								
								}else{
									$nop=$row->n_order;
								}							
								$jujum=number_format($jujum,0);
							}
						}else{
							$jujum='0';
							$nop=$row->n_order;
						}
						echo "<tbody>
								<tr>
								<td style=\"width:22px;\"><input style=\"width:22px;\" readonly type=\"text\" 
									id=\"baris$i\" name=\"baris$i\" value=\"$i\"><input type=\"hidden\" id=\"motif$i\" 
									name=\"motif$i\" value=\"$row->i_product_motif\"></td>
								<td style=\"width:64px;\"><input style=\"width:64px;\" readonly type=\"text\" 
									id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:258px;\"><input style=\"width:258px;\" readonly 
									type=\"text\" id=\"eproductname$i\"
									name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:148px;\"><input style=\"width:148px;\" readonly 
									type=\"text\" id=\"emotifname$i\"
									name=\"emotifname$i\" value=\"($row->e_product_motifname) - $row->e_remark\"></td>
								<td style=\"width:92px;\"><input style=\"text-align:right; width:92px;\" 
									type=\"text\" id=\"vproductmill$i\" readonly
									name=\"vproductmill$i\" value=\"$pangaos\"></td>
								<td style=\"width:38px;\"><input style=\"text-align:right; width:38px;\" 
									type=\"text\" id=\"nspb$i\" readonly	name=\"nspb$i\" value=\"$row->n_order\" 
									onkeyup=\"hitungnilai(this.value,'$jmlitem')\"></td>
								<td style=\"width:42px;\"><input style=\"text-align:right; width:42px;\" 
									type=\"text\" id=\"nquantitystock$i\" name=\"nquantitystock$i\" 
									value=\"$jujum\" readonly></td>
								<td style=\"width:38px;\"><input style=\"text-align:right; width:38px;\" 
									type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$nop\"></td>
								</tr>
							  </tbody>";
					}
				}
				echo "<input type=\"hidden\" id=\"ispbdelete\" 			name=\"ispbdelete\" 			value=\"\">
		  			  <input type=\"hidden\" id=\"iproductdelete\" 		name=\"iproductdelete\" 		value=\"\">
		  			  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"vdis1\" name=\"vdis1\" value=\"\">
					  <input type=\"hidden\" id=\"vdis2\" name=\"vdis2\" value=\"\">
					  <input type=\"hidden\" id=\"vdis3\" name=\"vdis3\" value=\"\">
					  <input type=\"hidden\" id=\"vtotdis\" name=\"vtotdis\" value=\"\">
					  <input type=\"hidden\" id=\"vtot\" name=\"vtot\" value=\"\">
					  <input type=\"hidden\" id=\"vtotbersih\" name=\"vtotbersih\" value=\"\">
		 			 ";
			?>
		</div>
		</table>
	  </div> 
	</div>
		<input type="hidden" name="jml" id="jml" <?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
		<?=form_close()?>
		<div id="pesan"></div>
  	</td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function yyy(b){
	document.getElementById("ispbedit").value=b;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/spb/cform/edit";
	formna.submit();
  }

  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/op/cform/supplier","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_status(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/op/cform/status","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function dipales(a){
	cek='false';
	if((document.getElementById("isupplier").value!='') &&
	   (document.getElementById("iopstatus").value!='') &&
	   (document.getElementById("dop").value!='')
	)
	{
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
					if((document.getElementById("norder"+i).value=='')){
						alert('Data item masih ada yang salah !!!');
						cek='false';
					}else{
						cek='true';	
					} 
				}
		}
		if(cek=='true'){
  	  		document.getElementById("login").hidden=true;
//			document.getElementById("login").hidden=true;
    	}else{
		    document.getElementById("login").hidden=false;
		}
	}else{
  		alert('Data header masih ada yang salah !!!');
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").hidden=false;
  }
  function afterSetDateValue(ref_field, target_field, date) {
    dspb=document.getElementById('dop').value;
    bspb=document.getElementById('bop').value;
    dtmp=dspb.split('-');
    per=dtmp[2]+dtmp[1]+dtmp[0];
    bln = dtmp[1];
    if( (bspb!='') && (dspb!='') ){
      if(bspb != bln)
      {
        alert("Tanggal OP tidak boleh dalam bulan yang berbeda !!!");
        document.getElementById("dop").value=document.getElementById("tglop").value;
      }
    }
  }


  function cektgl(){
		setTimeout(function(){
			var dop = $('#dop').val();
			var today = '<?=date('d-m-Y')?>';

			if(date_diff_indays(today, dop) > 0){
				alert('Tanggal Melebihi Hari Ini');
				$('#dop').val(today);
			}

		}, 100);
	}
</script>
