<?php 
	include ("php/fungsi.php");
?>
<!--<h2><?php echo $page_title; ?></h2>-->
<style>
.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
</style>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'reminderkontrak/cform/cari','update'=>'#main','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="10" align="center">Cari data : <input type="text" id="cari" name="cari" value="" ><input type="hidden" id="tahun" name="tahun" value="" >&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"></td>
	      </tr>
	    </thead>
		  <th>NIK</th>
	      <th>Nama Karyawan</th>
	      <th>Jenis Kelamin</th>
	      <th>No Telp</th>
	      <th>Status Pernikahan</th>
	      <th>Status Karyawan</th>
	      <th>Departement</th>
	      <th>Jabatan</th>
	      <th>Penempatan</th>
	      <th>Tanggal Kontrak</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  echo "<tr> 
			  <td>$row->i_nik</td>
			  <td>$row->e_nama_karyawan_lengkap</td>";
			if($row->e_jenis_kelamin!='L'){

			echo "<td>Perempuan</td>";
			}else{
			echo "<td>Laki-Laki</td>";
			}
			echo "<td>$row->e_nomor_telepon</td>
			  <td align=\"center\">$row->e_status_pernikahan</td>
			  <td align=\"center\">$row->e_karyawan_status</td>
			  <td align=\"center\">$row->e_department_name</td>
			  <td align=\"center\">$row->e_karyawan_jabatan</td>
			  <td align=\"center\">$row->e_karyawan_penempatan</td>
			  <td align=\"center\"><span class=\"blink\" style=\"color:red;\">$row->d_kontrak_ke2_akhir</span></td>";
			  echo "</td></tr>";
			}
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
