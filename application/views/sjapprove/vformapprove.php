<table class="maintable">
  <tr>
    <td align="left">
    <?php echo $this->pquery->form_remote_tag(array('url'=>'sjapprove/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="spbformupdate">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="12%">SPB</td>
		<?php 
			if($isi->d_spb!='') {
				$tmp=explode("-",$isi->d_spb);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$dspb=$hr."-".$bl."-".$th;
			} else {
				$dspb='';
			}
		?>
		<td width="38%"><input id="ispb" name="ispb" value="<?php echo $ispb; ?>" readonly>
			<input readonly id="dspb" name="dspb" value="<?php echo $dspb; ?>"></td>
		    <input id="ispb" name="ispb" type="hidden" value="<?php echo $isi->i_spb; ?>"></td>
		<td width="20%">Kelompok Harga</td>
		<td width="30%"><input readonly id="epricegroupname" name="epricegroupname" value="<?php echo $isi->e_price_groupname; ?>">
		    <input id="ipricegroup" name="ipricegroup" type="hidden" value="<?php echo $isi->i_price_group; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Promo</td>
		<td width="38%"><input readonly id="epromoname" name="epromoname" value="<?php echo $isi->e_promo_name; ?>">
		    <input id="ispbprogram" name="ispbprogram" type="hidden" value="<?php echo $isi->i_spb_program; ?>"></td>
		<td width="20%">Nilai Kotor (SPB)</td>
		<td width="30%"><input id="vspb" name="vspb" readonly value="<?php echo number_format($isi->v_spb); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="38%"><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td width="20%">Discount 1</td>
		<td width="30%"><input readonly id ="ncustomerdiscount1"name="ncustomerdiscount1" value="<?php echo $isi->n_spb_discount1; ?>">
		    <input readonly id="vcustomerdiscount1" name="vcustomerdiscount1" value="<?php echo number_format($isi->v_spb_discount1); ?>"></td>
	      </tr>
		<td width="12%">Pelanggan</td>
		<td width="38%"><input readonly id="ecustomername" name="ecustomername" value="<?php echo $isi->e_customer_name; ?>" readonly >
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td width="20%">Discount 2</td>
		<td width="30%"><input readonly id="ncustomerdiscount2" name="ncustomerdiscount2" value="<?php echo $isi->n_spb_discount2; ?>">
		    <input readonly id="vcustomerdiscount2" name="vcustomerdiscount2" value="<?php echo number_format($isi->v_spb_discount2); ?>"></td>
	      </tr>
	      <tr>
	      <tr>
		<td width="12%">PO</td>
		<td width="38%"><input id="ispbpo" name="ispbpo" value="<?php echo $isi->i_spb_po; ?>" maxlength="10" readonly ></td>
		<td width="20%">Discount 3</td>
		<td width="30%"><input readonly id="ncustomerdiscount3" name="ncustomerdiscount3" value="<?php echo $isi->n_spb_discount3; ?>">
		    <input readonly id="vcustomerdiscount3" name="vcustomerdiscount3" value="<?php echo number_format($isi->v_spb_discount3); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Konsiyasi</td>
		<td width="38%"><input id="fspbconsigment" name="fspbconsigment" type="checkbox" disabled=true 
				   <?php if($isi->f_spb_consigment=='t') echo "checked";?>>
				   &nbsp; TOP &nbsp;<input maxlength="3" id="nspbtoplength" name="nspbtoplength" readonly
				   value="<?php echo $isi->n_spb_toplength; ?>">   Stock Daerah
				   <input id="fspbstockdaerah" name="fspbstockdaerah" type="checkbox" 
				   <?php if($isi->f_spb_stockdaerah=='t') echo 'checked'; ?> disabled=true></td>
		<td width="20%">Discount 4</td>
		<td width="30%"><input readonly id="ncustomerdiscount4" name="ncustomerdiscount4" value="<?php echo $isi->n_spb_discount4; ?>">
		    <input readonly id="vcustomerdiscount4" name="vcustomerdiscount4" value="<?php echo number_format($isi->v_spb_discount4); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Salesman</td>
		<td width="38%"><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td width="20%">Discount Total (SPB)</td>
		<td width="30%"><input id="vspbdiscounttotal" name="vspbdiscounttotal" value="<?php echo number_format($isi->v_spb_discounttotal); ?>">
        <input id="vsjgross" name="vsjgross" type="hidden" value="<?php echo number_format($isi->v_nota_gross); ?>">
    </td>
	      </tr>
	      <tr>
		<td width="12%">No SJ</td>
		<td width="38%"><input id="fspbstokdaerah" name="fspbstokdaerah" type="hidden"> 
			<input id="isj" name="isj" readonly value="<?php echo $isi->i_sj; ?>"> 
		    <input readonly id="dsj" name="dsj" value="<?php echo $isi->d_sj; ?>"></td>
		<td width="20%">Nilai Bersih (SPB)</td>
		<?php 
			$tmp=$isi->v_spb-$isi->v_spb_discounttotal;
		?>
		<td width="30%"><input readonly id="vspbbersih" name="vspbbersih" readonly value="<?php echo number_format($tmp); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">PKP</td>
		<td width="38%"><input id="fspbplusppn" name="fspbplusppn" type="hidden" value="<?php echo $isi->f_spb_plusppn;?>">
			<input id="fspbplusdiscount" name="fspbplusdiscount" type="hidden" value="<?php echo $isi->f_spb_plusdiscount;?>">
			<input id="fspbpkp" name="fspbpkp" type="hidden" value="<?php echo $isi->f_spb_pkp;?>">
			<input type="text" id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" readonly value="<?php echo $isi->e_customer_pkpnpwp;?>"></td>
		<td width="20%">Discount Total (SJ)</td>
		<td width="30%"><input id="vspbdiscounttotalafter" name="vspbdiscounttotalafter"  value="<?php echo number_format($isi->v_nota_discount);?>"></td>
	      </tr>
		<tr>
		  <td width="12%"></td>
		  <td width="38%"></td>
		<td width="20%">Nilai Bersih (SJ)</td>
		<td width="30%"><input readonly id="vspbafter" name="vspbafter" value="<?php echo number_format($isi->v_spb_after);?>"></td>
		</tr>
		<tr>
		  <td class="batas" width="100%" colspan="4">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("sjapprove/cform/","#tmp")'>
			<?=form_close()?> 
		  </td>
		</tr>
		<tr>
		  <td class="batas" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="batas" width="12%"></td>
      <td class="batas" width="30%"></td>
			<td class="batas" colspan="3">
			<?php echo form_open('sjapprove/cform/sjapprove', array('id' => 'spbformnotapprove', 'name' => 'spbformnotapprove', 'onsubmit' => 'sendRequestxx(); return false'));?>
				<input type="hidden" id="nospb" name="nospb" value="<?php echo $ispb; ?>">
				<input type="hidden" id="kdarea" name="kdarea" value="<?php echo $isi->i_area; ?>">
				<input name="sjapprove" id="sjapprove" value="Approve" type="submit" onclick="dipalesegein();"></td>
			<?=form_close()?>
		</tr>
    </table>

			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:63px;" align="center">Kd Barang</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:90px;"  align="center">Harga</th>
					<th style="width:46px;"  align="center">Jml Pesan</th>
					<th style="width:126px;"  align="center">Total</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\">";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
				  	$nilai=number_format($row->v_unit_price,2);
					$jujum=number_format($row->n_order,0);
					$ntot =number_format($row->v_unit_price*$row->n_order,0);
				  	echo '<tbody>
							<tr>
								<td style="width:22px;">
									<input style="width:22px;" readonly type="text" id="baris'.$i.'" 
									name="baris'.$i.'" value="'.$i.'">
									<input type="hidden" id="motif'.$i.'" name="motif'.$i.'" value="'.$row->i_product_motif.'">
								</td>
								<td style="width:62px;">
									<input style="width:62px;" readonly type="text" id="iproduct'.$i.'"
									name="iproduct'.$i.'" value="'.$row->i_product.'">
								</td>
								<td style="width:280px;">
									<input style="width:280px;" readonly type="text" id="eproductname'.$i.'"
									name="eproductname'.$i.'" value="'.$row->e_product_name.'">
								</td>
								<td style="width:92px;">
									<input readonly style="width:92px;"  type="text" id="emotifname'.$i.'"
									name="emotifname'.$i.'" value="'.$row->e_product_motifname.'">
								</td>
								<td style="width:86px;">
									<input readonly style="text-align:right; width:86px;" type="text"
									id="vproductretail'.$i.'" name="vproductretail'.$i.'" value="'.$nilai.'">
								</td>
								<td style="width:46px;">
									<input style="text-align:right; width:46px;" type="text" id="norder'.$i.'"
									name="norder'.$i.'" readonly value="'.$jujum.'">
								</td>
								<td style="width:118px;">
									<input readonly style="text-align:right; width:118px;" type="text"
									id="vtotal'.$i.'" name="vtotal'.$i.'" value="'.$ntot.'">
								</td>
							</tr>
						</tbody>';
				}
				?>
			</div>
			</table>
	  </div>
	</div>
	</div>
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
   	document.getElementById("login").disabled=true;
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function dipalesegein(){
   	document.getElementById("login").disabled=true;
   	document.getElementById("notapprove").disabled=true;
  }
</script>
