<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'listspmbblmreal/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="sjform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td width="10%">No SPMB</td>
		<?php if($isi->d_spmb){
			    if($isi->d_spmb!=''){
				    $tmp=explode("-",$isi->d_spmb);
				    $hr=$tmp[2];
				    $bl=$tmp[1];
				    $th=$tmp[0];
				    $isi->d_spmb=$hr."-".$bl."-".$th;
			    }
		   }
		?>
		<td><input id="ispmb" name="ispmb" type="text" value="<?php if($isi->i_spmb) echo $isi->i_spmb; ?>">
        <input readonly id="dspmb" name="dspmb" value="<?php if($isi->d_spmb) echo $isi->d_spmb; ?>" ></td>
	      </tr>
	      <tr>
		<td width="10%">Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php if($isi->e_area_name) echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php if($isi->i_area) echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">Keterangan</td>
		<td><input readonly id="eremark" name="eremark" value="<?php if($isi->e_remark) echo $isi->e_remark; ?>"></td>
	      </tr>
	      <tr>
		<td width="10%">SPMB Lama</td>
		<td><input id="ispmbold" name="ispmbold" type="text" value="<?php echo $isi->i_spmb_old; ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listspmbblmreal/cform/view/<?php echo $iarea."/"; ?>","#main")'>
		  </td>
 	    </tr>
	    </table>
			<div id="detailheader" align="center">
				<?php 
					if($detail){
					?>
						<table class="listtable" align="center" style="width: 660px;">
							<th style="width:25px;" align="center">No</th>
							<th style="width:63px;" align="center">Kode</th>
							<th style="width:300px;" align="center">Nama Barang</th>
							<th style="width:73px;" align="center">Jml Ord</th>
							<th style="width:73px;" align="center">Jml Acc</th>
							<th style="width:73px;" align="center">Jml Saldo</th></table>
					<?php 
					}		
				?>
			</div>
			<div id="detailisi" align="center">
				<?php 
				if($detail){
						$i=0;
						foreach($detail as $row)
						{
						  $query=$this->db->query(" select a.i_spmb, a.d_spmb, a.i_area, b.i_product, b.e_product_name, b.n_order, b.n_acc, b.n_saldo
                                        from tm_spmb a, tm_spmb_item b
                                        where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                        and not a.i_approve2 is null and not a.i_store is null
                                        and b.n_deliver<b.n_saldo and a.i_spmb='$ispmb'
                                        order by b.n_item_no",false);


							echo '<table class="listtable" align="center" style="width:630px;">';
						  	$i++;
						  	echo "<tbody>
								<tr>
		    						<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\"></td>
								<td style=\"width:66px;\"><input style=\"width:66px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
								<td style=\"width:314px;\"><input style=\"width:314px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"norder$i\" name=\"norder$i\" value=\"$row->n_order\"></td>
								<td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"nacc$i\" name=\"nacc$i\" value=\"$row->n_acc\"
							onkeyup=\"hitungnilai(this.value,'$jmlitem'); \">
								<input type=\"hidden\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"\"></td>
                <td style=\"width:74px;\"><input readonly style=\"text-align:right; width:74px;\" 
								type=\"text\" id=\"nsaldo$i\" name=\"nsaldo$i\" value=\"$row->n_saldo\"></td>
							</tr></tbody></table>";
            }
         }
	
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php if($jmlitem) echo $jmlitem; ?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("dspmb").value!='') &&
  	 	(document.getElementById("iarea").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
   			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("norder"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
    	}else{
		   	document.getElementById("login").disabled=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
 
</script>
