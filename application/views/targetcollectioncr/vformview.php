<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'targetcollectioncr/cform/export','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
	<?php 
		$periode=$iperiode;
		$a=substr($periode,0,4);
	  $b=substr($periode,4,2);
		$periode=mbulan($b)." - ".$a;
?>
    <input name="iperiode" id="iperiode" value="<?php echo $periode; ?>" type="hidden">
<?php 
    echo "<center><h2>".NmPerusahaan."</h2></center>";
		echo "<center><h3>Target Collection Credit per Area</h3></center>";
		echo "<center><h3>Periode $periode</h3></center>";
?>
    	  <table class="listtable" border=none>
	   	    <th>No</th>
	   	    <th>Area</th>
	   	    <th>Jumlah Target</th>
			    <th>Target Realisasi</th>
			    <th>Persen</th>
			    <th>Jumlah Non Insentif</th>
			    <th>Jumlah Realisasi</th>
			    <th>Persen</th>
        	<th class="action">Action</th>
	    <tbody>
	      <?php 
		if($isi){
      $i=1;
      $ttarget=0;
      $trealis=0;
      $ttarget2=0;
      $trealis2=0;
			foreach($isi as $row){
        if($row->realisasi==null || $row->realisasi=='')$row->realisasi=0;
#        if($row->total==0)$row->total=$row->realisasi;
        if($row->total!=0){
          $persen=number_format(($row->realisasi/$row->total)*100,2);
        }else{
          $persen='0.00';
        }
        if($row->realisasinon==null || $row->realisasinon=='')$row->realisasinon=0;
        if($row->totalnon!=0){
          $persennon=number_format(($row->realisasinon/$row->totalnon)*100,2);
        }else{
          $persennon='0.00';
        }
        $ttarget=$ttarget+$row->total;
        $trealis=$trealis+$row->realisasi;
        $ttarget2=$ttarget2+$row->totalnon;
        $trealis2=$trealis2+$row->realisasinon;
	      echo "<tr>
          <td align=right>$i</td>
          <td>$row->i_area-$row->e_area_name</td>
          <td align=right>Rp. ".number_format($row->total)."</td>
          <td align=right>RP. ".number_format($row->realisasi)."</td>
			    <td align=right>".$persen." %</td>
			    <td align=right>Rp. ".number_format($row->totalnon)."</td>
			    <td align=right>RP. ".number_format($row->realisasinon)."</td>
			    <td align=right>".$persennon." %</td>";
        $i++;
			  echo "<td class=\"action\">";
				echo "<a href=\"#\" onclick='show(\"targetcollectioncr/cform/detail/$iperiode/$row->i_area/\",\"#main\");'><img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/edit.png\" border=\"0\" alt=\"edit\"></a>";
				echo "</td></tr>";	
			}
      echo "<tr>
          <td colspan=2>Total</td>
          <td align=right>Rp. ".number_format($ttarget)."</td>
          <td align=right>RP. ".number_format($trealis)."</td>
			    <td align=right></td>
			    <td align=right>Rp. ".number_format($ttarget2)."</td>
			    <td align=right>RP. ".number_format($trealis2)."</td>
			    <td colspan=2></td></tr>";	
		}
	      ?>
	    </tbody>
	  </table>
    <center><input name="cmdreset" id="cmdreset" value="Kembali!!!" type="button" onclick='show("targetcollectioncr/cform/index","#main");' ></center>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c)
  {
	  document.getElementById("iperiode").value=a;
	  document.getElementById("iarea").value=c;
	  formna=document.getElementById("listform");
	  formna.action="<?php echo site_url(); ?>"+"/targetcollectioncr/cform/viewdetail";
	  formna.submit();
  }
</script>
