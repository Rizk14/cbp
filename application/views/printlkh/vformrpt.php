<?php 
  include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<body>

<div align="center">
  <style type="text/css" media="all">
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#80c0e0 1px solid;
	border-top:#80c0e0 1px solid;
	border-left:#80c0e0 1px solid;
	border-right:#80c0e0 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 12px;
  font-weight:normal;
  padding:1px;
}
.eusi {
  font-size: 14px;
  font-weight:normal;
}
.garisbawah { 
	border-bottom:#000000 0.1px solid;
}
  </style>
  <style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
  </style>
  <?php 
  $tmp	= explode("-",$dfrom);
	$th		= $tmp[2];
  $thn  = $th;
	$bl		= $tmp[1];
	$hr		= $tmp[0];
	$dfrom	= $hr." ".mbulan($bl)." ".$th;
	$tmp	= explode("-",$dto);
	$th		= $tmp[2];
	$bl		= $tmp[1];
	$hr		= $tmp[0];
	$dto	= $hr." ".mbulan($bl)." ".$th;
  ?>
  <table width="920" border="0" cellspacing=0 cellpadding=0>
    <tr>
      <td colspan=3 width="908"><div align="center"><strong>LAPORAN KAS HARIAN </strong></div></td>
    </tr>
    <tr>
      <td colspan=3><div align="center"><strong>AREA <?php echo $eareaname; ?></strong></div></td>
    </tr>
    <tr>
      <td colspan=3><div align="center"><strong>PERIODE : <?php echo $dfrom; ?>&nbsp;-&nbsp;<?php echo $dto; ?></strong></div></td>
    </tr>
    <tr>
      <td colspan=3><div align="center"><strong>NO : <?php echo $no; ?> / LKH-<?php echo $singkat.' / '.$bul.' / '.$thn; ?></strong></div></td>
    </tr>
    <tr>
      <td colspan=2></td>
      <td><div align="right"><strong>Hal : 1</strong></div></td>
    </tr>
    </table>
</div>
      <table width="920px" cellspacing=0 cellpadding=0 class="garis">
        <tr>
          <td width="24" rowspan="2" align="center" class="huruf isi">NO</td>
           <td width="40" rowspan="2" align="center" class="huruf isi">TANGGAL</td>
          <td width="330" rowspan="2" align="center" class="huruf isi">KETERANGAN</td>
          <td width="80" rowspan="2" class="huruf isi">NO. KEND </td>
          <td width="40" rowspan="2" align="center" class="huruf isi">KM</td>
          <td align="center" width="70" class="huruf isi">DEBET</td>
          <td width="70" align="center" class="huruf isi"><p>KREDIT</p></td>
          <td width="100" align="center" class="huruf isi">SALDO</td>
        </tr>
        <tr>
          <td align="center" width="84" class="huruf isi">Rp.</td>
          <td align="center" class="huruf isi"><p>Rp.</p>          </td>
          <td align="center" class="huruf isi">&nbsp;</td>
        </tr>
        <?php 
          $i=0;
          $x=1;
          $awal   =$saldo;
          $jumsisa=$awal;
          if($isi){
            echo "<tr height=27>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td colspan=4>Saldo Awal</td>
                 <td align='right'>".number_format($saldo)."</td></tr>";
		        foreach($isi as $row){
              $i++;
              $tmp=explode("-",$row->d_kk);
		          $th=$tmp[0];
		          $bl=$tmp[1];
		          $hr=$tmp[2];
		          $row->d_kk=$hr."-".$bl."-".$th;

              if($row->f_debet=='t' && substr($row->i_kk,0,2)!='BK' && substr($row->i_kk,0,2)!='KB'){
                $jumsisa=$jumsisa-$row->v_kk;
              }elseif(substr($row->i_kk,0,2)=='KB'){
                $jumsisa=$jumsisa+$row->v_kk;
              }elseif(substr($row->i_kk,0,2)=='BK'){
                $jumsisa=$jumsisa+$row->v_kk;
              }else{
                $jumsisa=$jumsisa+$row->v_kk;
              }
              echo "<tr height=27>
                 <td align=right>$i</td>
                 <td>$row->d_kk</td>
                 <td>$row->e_description</td>
                 <td>".str_replace(" ","",$row->i_kendaraan)."</td>
                 <td align=right>".number_format($row->n_km)."</td>";
              if($row->f_debet=='f' || substr($row->i_kk,0,2)=='KB'){
                 echo "
                 <td align='right'>".number_format($row->v_kk)."</td>
                 <td></td>";
              }elseif($row->f_debet=='f' || substr($row->i_kk,0,2)=='BK'){
                 echo "
                 <td align='right'>".number_format($row->v_kk)."</td>
                 <td></td>";
              }else{
                 echo "
                 <td></td>
                 <td align='right'>".number_format($row->v_kk)."</td>";
              }
              echo "<td align='right'>".number_format($jumsisa)."</td></tr>";
              if( ($i%$brs==0)&&($jml!=$i) ){
                $x++;
                echo "</table>";
                echo "<table width=\"920px\" border=0>
                      <tr rowspan=3>
                        <td colspan=3>&nbsp;</td>
                      </tr>
                      <tr>
                        <td width=\"320\" align=\"left\" class=\"huruf isi\">Diperiksa Oleh</td>
                        <td width=\"320\" align=\"center\" class=\"huruf isi\">Disetujui Oleh</td>
                        <td width=\"320\" align=\"right\" class=\"huruf isi\">Dibuat Oleh</td>
                      </tr>
                      <tr rowspan=3>
                        <td colspan=3>&nbsp;</td>
                      </tr>
                      ";
?>
                      <table width="920" border="0">
                      <br class="pagebreak">
                        <tr>
                          <td colspan=2></td>
                          <td><div align="right"><strong>Hal : <?php echo $x; ?></strong></div></td>
                        </tr>
                      </table>
                      <table width="920px"  cellspacing=0 cellpadding=0 class="garis huruf">
                              <tr>
                                <td width="24" rowspan="2" align="center" class="huruf isi">NO</td>
                                <td width="40" rowspan="2" align="center" class="huruf isi">TANGGAL</td>
                                <td width="340" rowspan="2" align="center" class="huruf isi">KETERANGAN</td>
                                <td width="60" rowspan="2">NO. KEND </td>
                                <td width="40" rowspan="2" align="center" class="huruf isi">KM</td>
                                <td align="center" width="70" class="huruf isi">DEBET</td>
                                <td width="70" align="center" class="huruf isi"><p>KREDIT</p>          </td>
                                <td width="100" align="center" class="huruf isi">SALDO</td>
                              </tr>
                              <tr>
                                <td align="center" width="94" class="huruf isi">Rp.</td>
                                <td align="center" class="huruf isi"><p>Rp.</p></td>
                                <td align="center" class="huruf isi">&nbsp;</td>
                              </tr>
<?php 

              }
            }
            echo "</table>";
            echo "<table width=\"920px\" border=0>
                  <tr rowspan=3>
                    <td colspan=3>&nbsp;</td>
                  </tr>
                  <tr>
                    <td width=\"320\" align=\"left\" class=\"huruf isi\">Diperiksa Oleh</td>
                    <td width=\"320\" align=\"center\" class=\"huruf isi\">Disetujui Oleh</td>
                    <td width=\"320\" align=\"right\" class=\"huruf isi\">Dibuat Oleh</td>
                  </tr>
                  <tr rowspan=3>
                    <td colspan=3>&nbsp;</td>
                  </tr>
                  ";
          }
          echo "</table>";
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
</BODY>
</html>
