<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<div id='tmp'>
<?php 
#	include ("php/fungsi.php");

?><h3>&nbsp;&nbsp;&nbsp;<?php echo 'Periode : '.mbulan(substr($iperiode,4,2)).' '.substr($iperiode,0,4); ?></h3><table class="maintable">
  <tr>
    <td align="left">
<!--	<?php echo form_open('srpenjualanperarea/cform/cari', array('id' => 'listform'));?>-->
  <?php echo $this->pquery->form_remote_tag(array('url'=>'srpenjualanperarea/cform/view','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php 
      $periode=$iperiode;
		  $a=substr($periode,0,4);
    	$b=substr($periode,4,2);
		  $periode=mbulan($b)." - ".$a;
		?>
<!--
		  <table class="mastertable">
			<tr><td style="width:100px;">Periode</td><td style="width:5px;">:</td><td colspan=2><?php # echo $periode; ?></td><td style="width:80px"></td><td></td></tr>
		  </table>
-->    	  <table class="listtable">
			<th>Supplier</th>
			<th>Area</th>
			<th>Target</th>
			<th>Qty SPB</th>
			<th>V SPB</th>
			<th>% SPB</th>
			<th>Qty Nota</th>
			<th>V Nota</th>
			<th>% Nota</th>
			<tbody>
	      <?php 
		if($isi){
      $tottarget=0;
      $totqspb=0;
      $totvspb=0;
      $totpersenspb=0;
      $totqnota=0;
      $totvnota=0;
      $totpersennota=0;
			foreach($isi as $row){
			  if($row->vnota==null || $row->vnota=='')$row->vnota=0;
			  if($row->vspb==null || $row->vspb=='')$row->vspb=0;
        if($row->target!=0)
        {
          $persenspb=number_format(($row->vspb/$row->target)*100,2);
          $persennota=number_format(($row->vnota/$row->target)*100,2);
        }else{
          $persenspb='0.00';
          $persennota='0.00';
        }
			  echo "<tr> 
				  <td style='font-size:12px;'>$row->e_supplier_name</td>
				  <td style='font-size:12px;'>$row->e_area_name</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->target)."</td>
				  <td style='font-size:12px;' align=right>".number_format($row->qspb)."</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->vspb)."</td>
				  <td style='font-size:12px;' align=right>".$persenspb." %</td>
				  <td style='font-size:12px;' align=right>".number_format($row->qnota)."</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($row->vnota)."</td>
				  <td style='font-size:12px;' align=right>".$persennota." %</td>
</tr>";

        $tottarget=$tottarget+$row->target;
        $totqspb=$totqspb+$row->qspb;
        $totvspb=$totvspb+$row->vspb;
        $totqnota=$totqnota+$row->qnota;
        $totvnota=$totvnota+$row->vnota;
			}
			if($tottarget>0){
			  $totpersenspb=number_format(($totvspb/$tottarget)*100,2);
        $totpersennota=number_format(($totvnota/$tottarget)*100,2);
      }else{
			  $totpersenspb='0.00';
        $totpersennota='0.00';
      }
		  echo "<tr> 
    		  <td style='font-size:12px;' colspan=2><b>Total</b></td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($tottarget)."</td>
				  <td style='font-size:12px;' align=right>".number_format($totqspb)."</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($totvspb)."</td>
				  <td style='font-size:12px;' align=right>".$totpersenspb." %</td>
				  <td style='font-size:12px;' align=right>".number_format($totqnota)."</td>
				  <td style='font-size:12px;' align=right>Rp. ".number_format($totvnota)."</td>
				  <td style='font-size:12px;' align=right>".$totpersennota." %</td></tr>";

		}
		echo "<input type=\"hidden\" id=\"iperiode\" name=\"iperiode\" value=\"$iperiode\">";
	      ?>
	    </tbody>
<tr>
		<td colspan="12" align="center"><input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('srpenjualanperarea/cform/','#main')"></td>
	      </tr>
	  </table>
  	</div>
      </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
<script language="javascript" type="text/javascript">
  function yyy(a,c){
	document.getElementById("iperiode").value=a;
	document.getElementById("iarea").value=c;
	formna=document.getElementById("listform");
	formna.action="<?php echo site_url(); ?>"+"/srpenjualanperarea/cform/viewdetail";
	formna.submit();
  }
</script>
