<table class="maintable">
  <tr>
    <td align="left">
       <?php echo form_open('ttbtolak/cform/updatettbtolak', array('id' => 'ttbtolakform', 'name' => 'ttbtolakform', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
	      <tr>
		<td>TTB</td>
		<td><?php 	if($ittb!='') {
				echo "<input type=\"text\" id=\"ittb\" name=\"ittb\" value=\"$ittb\" readonly>"; 
				}else{
					echo "<input type=\"text\" id=\"ittb\" name=\"ittb\" value=\"\" maxlength=6>"; 
				}	
				$tmp=explode("-",$isi->d_ttb);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$dttb=$hr."-".$bl."-".$th;
				echo " <input readonly id=\"dttb\" name=\"dttb\" value=\"$dttb\" readonly 
					   onclick=\"showCalendar('',this,this,'','dttb',0,20,1)\">
					   <input type=\"hidden\" id=\"nttbyear\" name=\"nttbyear\" value=\"$isi->n_ttb_year\">
					 ";
			?>
			<input type="hidden" id="ibbm" name="ibbm" value="<?php echo $isi->i_bbm; ?>">
			</td>			
		<td>nota</td>
		<?php 
			$tmp=explode("-",$isi->d_nota);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$dnota=$hr."-".$bl."-".$th;
		?>
		<td><input type="text" readonly id="inota" name="inota" value="<?php echo $isi->i_nota; ?>">
			<input type="text" id="dnota" name="dnota" value="<?php echo $dnota; ?>" readonly></td>
	      </tr>
	      <tr>
		<?php 
      if($isi->d_receive1!=''){
			  $tmp=explode("-",$isi->d_receive1);
			  $th=$tmp[0];
			  $bl=$tmp[1];
			  $hr=substr($tmp[2],0,2);
			  $dreceive1=$hr."-".$bl."-".$th;
      }else{
        $dreceive1=null;
      }
		?>
		<td>Tgl Trm Sales</td>
		<td><input 	type="text" id="dreceive1" name="dreceive1" value="<?php echo $dreceive1; ?>" readonly 
					onclick="showCalendar('',this,this,'','dreceive1',0,20,1)"></td>
		<td>Nilai Kotor</td>
		<td><input id="vttbgross" name="vttbgross" readonly value="<?php echo number_format($isi->v_ttb_gross); ?>"></td>
	      </tr>
	      <tr>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" onclick="view_area()" value="<?php echo $isi->e_area_name; ?>" 
			 	   readonly >
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
		<td>Discount 1</td>
		<td><input readonly id ="nttbdiscount1"name="nttbdiscount1" value="<?php echo $isi->n_ttb_discount1; ?>">
		    <input readonly id="vttbdiscount1" name="vttbdiscount1" value="<?php echo number_format($isi->v_ttb_discount1); ?>"></td>
	      </tr>
	      <tr>
		<td>Pelanggan</td>
		<td><input readonly id="ecustomername" name="ecustomername" onclick="view_pelanggan()" 
				   value="<?php echo $isi->e_customer_name; ?>" readonly >
		    <input id="icustomer" name="icustomer" type="hidden" value="<?php echo $isi->i_customer; ?>"></td>
		<td>Discount 2</td>
		<td><input readonly id="nttbdiscount2" name="nttbdiscount2" value="<?php echo $isi->n_ttb_discount2; ?>">
		    <input readonly id="vttbdiscount2" name="vttbdiscount2" value="<?php echo number_format($isi->v_ttb_discount2); ?>"></td>
	      </tr>
	      <tr>
			<td>NPWP</td>
			<td><input id="ecustomerpkpnpwp" name="ecustomerpkpnpwp" value="<?php echo $isi->e_customer_pkpnpwp; ?>" readonly></td>
		<td>Discount 3</td>
		<td><input readonly id="nttbdiscount3" name="nttbdiscount3" value="<?php echo $isi->n_ttb_discount3; ?>">
		    <input readonly id="vttbdiscount3" name="vttbdiscount3" value="<?php echo number_format($isi->v_ttb_discount3); ?>"></td>
	      </tr>
		  <tr>
		<td>Salesman</td>
		<td><input readonly id="esalesmanname" name="esalesmanname" value="<?php echo $isi->e_salesman_name; ?>">
		    <input id="isalesman" name="isalesman" type="hidden" value="<?php echo $isi->i_salesman; ?>"></td>
		<td>Discount Total</td>

		<td><input readonly id="vttbdiscounttotal" name="vttbdiscounttotal" value="<?php echo number_format($isi->v_ttb_discounttotal); ?>"></td>
	      </tr>
	      <tr>
			<td>Keterangan</td>
			<td><input id="fttbplusppn" name="fttbplusppn" type="hidden" 
				   value="<?php echo $isi->f_ttb_plusppn;?>">
				<input id="fttbplusdiscount" name="fttbplusdiscount" type="hidden"
					   value="<?php echo $isi->f_ttb_plusdiscount;?>">
				<input id="fttbpkp" name="fttbpkp" type="hidden" value="">
				<input id="eremark" name="eremark" value=""></td>
		  <td>Nilai Bersih</td>
		<td><input readonly id="vttbnetto" name="vttbnetto" readonly value="<?php echo number_format($isi->v_ttb_netto); ?>"></td>
		</tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		    <input name="login" id="login" value="Simpan" type="button" onclick="dipales()" <?php if($fnotakoreksi=='t') echo 'disabled=true';?>>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listttbtolak/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" style="width:750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:70px;" align="center">Kd Barang</th>
					<th style="width:305px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Motif</th>
					<th style="width:80px;"  align="center">Harga</th>
					<th style="width:40px;"  align="center">Jml Nota</th>
					<th style="width:40px;"  align="center">Jml Tolak</th>
					<th style="width:90px;"  align="center">Keterangan</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				echo "<table class=\"listtable\" style=\"width:750px;\"";
				$i=0;
				foreach($detail as $row)
				{
				  	$i++;
					$harga		= number_format($row->v_unit_price);
					$ndeliv		= number_format($row->n_deliver,0);
					if($row->n_quantity=='')$row->n_quantity='0';
					$nquantity	= number_format($row->n_quantity,0);
				  	echo "<tbody>
							<tr>
		    				<td width=\"23px\">
								<input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
								<input type=\"hidden\" id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td width=\"67px\"><input style=\"width:67px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td width=\"278px\"><input style=\"width:278px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td width=\"92px\"><input style=\"width:92px;\" type=\"text\" 
								id=\"eproductmotifname$i\" readonly
								name=\"eproductmotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td width=\"74px\"><input style=\"text-align:right; width:74px;\" type=\"text\" 
								id=\"vproductretail$i\" readonly
								name=\"vproductretail$i\" value=\"$harga\"></td>
							<td width=\"39px\"><input style=\"text-align:right; width:39px;\" readonly
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$ndeliv\"></td>
							<td width=\"39px\"><input style=\"text-align:right; width:39px;\" 
								type=\"text\" id=\"nquantity$i\" name=\"nquantity$i\" value=\"$nquantity\" 
								onkeyup=\"cekval(this.value);\"><input type=\"hidden\" id=\"nasal$i\" name=\"nasal$i\" 
                value=\"$nquantity\"></td>
							<td width=\"90px\"><input style=\"text-align:right; width:90px;\" 
								type=\"text\" id=\"eremark$i\" name=\"eremark$i\" value=\"$row->e_ttb_remark\"></td>
							</tr>
						  </tbody>";
				}
				?>
			</div>
			</table>
	  </div>
	</div>
<input type="hidden" name="jml" id="jml" 
	<?php if(isset($jmlitem)){ echo "value=\"$jmlitem\""; }else{echo "value=\"0\"";}?>>
	<?=form_close()?> 
<div id="pesan"></div>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function dipales(){
	cek='false';
	if((document.getElementById("dttb").value!='') &&
	  (document.getElementById("ittb").value!='')) {
	  var a=parseFloat(document.getElementById("jml").value);
	  for(i=1;i<=a;i++){
		if(document.getElementById("nquantity"+i).value!='0'){
		  cek='true';
		  break;
		}else{
		  cek='false';
		} 
	  }
	  if(cek=='true'){
	    document.getElementById("login").disabled=true;
		sendRequest(); 
		return false;
		document.ttbtolakform.submit();
	  }else{
		alert('Isi jumlah detail tolakan minimal 1 item !!!');
	    document.getElementById("login").disabled=false;
	  }
	}else{
	  alert('Data header masih ada yang salah !!!');
	}
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function cekval(input){
	var jml	= parseFloat(document.getElementById("jml").value);
	var num = input.replace(/\,/g,'');
//	alert(num);
	if(!isNaN(num)){
		for(j=1;j<=jml;j++){
			qty		= parseFloat(document.getElementById("nquantity"+j).value);
			del		= parseFloat(document.getElementById("ndeliver"+j).value);
			input	= document.getElementById("nquantity"+j);
			if(qty>del){
				alert('Jumlah tolakan tidak boleh lebih dari jumlah Nota !!!');
				input.value='0';
			}
			var jml 	= parseFloat(document.getElementById("jml").value);
			var totdis 	= 0;
			var totnil	= 0;
			var hrg		= 0;
			var ndis1	= parseFloat(formatulang(document.getElementById("nttbdiscount1").value));
			var ndis2	= parseFloat(formatulang(document.getElementById("nttbdiscount2").value));
			var ndis3	= parseFloat(formatulang(document.getElementById("nttbdiscount3").value));
			var vdis1	= 0;
			var vdis2	= 0;
			var vdis3	= 0;
			for(i=1;i<=jml;i++){
			  	var hrgtmp 	= parseFloat(formatulang(document.getElementById("vproductretail"+i).value))*parseFloat(formatulang(document.getElementById("nquantity"+i).value));
				hrg			= hrg+hrgtmp;
			}
			vdis1=vdis1+((hrg*ndis1)/100);
			vdis2=vdis2+(((hrg-vdis1)*ndis2)/100);
			vdis3=vdis3+(((hrg-(vdis1+vdis2))*ndis3)/100);
			vdistot	= vdis1+vdis2+vdis3;
			vhrgreal= hrg-vdistot;
			document.getElementById("vttbdiscounttotal").value=formatcemua(vdistot);
			document.getElementById("vttbnetto").value=formatcemua(vhrgreal);
			document.getElementById("vttbgross").value=formatcemua(vhrgreal-vdistot);
		}
    }else{ 
		alert('input harus numerik !!!');
     	input = input.substring(0,input.length-1);
	}
  }
</script>
