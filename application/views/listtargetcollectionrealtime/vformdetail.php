<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/dgu.css" />
<script type="text/javascript" src="<?php echo base_url()?>js/jquery.js"></script>
<style>
.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #C32148; /*f4511e, 52441C, #A70D2A = carbon, f4511e*/
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 20px;
  padding: 20px;
  width: 160px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}
.button span:after {
  content: '»';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}
</style>

<div id='tmp'>
<table class="maintable">
  <tr>
    <td align="left">
  <?php echo $this->pquery->form_remote_tag(array('url'=>'targetcollectioncash/cform/cari','update'=>'#main','type'=>'post'));?>
	<div class="effect">
	  <div class="accordion2">
		<?php
		$periode=$iperiode;
    $perper=$periode;
    if($detail){
      $perper=$periode;
      $area=$iarea;
		  $a=substr($periode,0,4);
	    $b=substr($periode,4,2);
		  $periode=mbulan($b)." - ".$a;
      echo "<input name=\"iperiode\" id=\"iperiode\" value=\"$perper\" type=\"hidden\">";
      echo "<input name=\"iarea\" id=\"iarea\" value=\"$iarea\" type=\"hidden\">";
      echo "<center><h2>".NmPerusahaan."</h2></center>";
		  echo "<center><h3>Target Collection Per Nota</h3></center>";
		  echo "<center><h3>Periode $periode</h3></center>";
		  ?>
      	  <table class="listtable">
            <center>
            <button type="button" class="button" style="cursor: pointer; padding-left:3px;padding-right:3px;padding-top:3px;padding-bottom:3px;"
          onclick="exportTableToExcel('tblData', 'Target_Collection_Per_Nota')"><span><b>Export to Excel</b></span></button>
            </center>
          
      	    <tr>
	       	    <th rowspan=2>No</th>
	       	    <th rowspan=2>Area</th>
	       	    <th rowspan=2>Salesman</th>
	       	    <th rowspan=2>Toko</th>
<!--              <th rowspan=2>Jenis</th>-->
			        <th rowspan=2>Tanggal</th>
	       	    <th rowspan=2>Nota</th>
			        <th rowspan=2>Target</th>
			        <th rowspan=2>Realisasi</th>
   			      <th rowspan=2>Blm Bayar</th>
			        <th colspan=2>Tdk Telat</th>
			        <th colspan=2>Telat</th>
            </tr>
            <tr>
			        <th>Target</th>
			        <th>Realisasi</th>
			        <th>Target</th>
			        <th>Realisasi</th>
            </tr>
	      <tbody>
	        <?php
		  if($detail){
        $no=0;
        $ttotal=0;
        $treal=0;
        $tblm=0;
        $ttelat=0;
        $trealtelat=0;
        $ttdk=0;
        $trealtdk=0;
			  foreach($detail as $row){
          if($row->total>0 || $row->realisasi>0){
            $no++;
            $tmp=explode('-',$row->d_nota);
		        $tgl=$tmp[2];
		        $bln=$tmp[1];
		        $thn=$tmp[0];
            if(strlen($tgl)==2){
        		  $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
            }
            $ttotal=$ttotal+$row->total;
            $treal =$treal+$row->realisasi;
            $tblm=$tblm+$row->blmbayar;
            $trealtelat =$trealtelat+$row->realisasitelat;
            $trealtdk =$trealtdk+$row->realisasitdktelat;
            $ttelat =$ttelat+$row->telat;
            $ttdk =$ttdk+$row->tdktelat;
		        echo "<tr>
              <td>$no</td>
              <td>$row->i_area - $row->e_area_name</td>
  				    <td>$row->i_salesman ($row->e_salesman_name)</td>
				      <td>($row->i_customer) - $row->e_customer_name</td>";
#              <td>$row->e_customer_classname</td>
            echo "
				      <td>$row->d_nota</td>
				      <td>$row->i_nota</td>
				      <td align=right> ".($row->total)."</td>
				      <td align=right> ".($row->realisasi)."</td>";
				    echo "
              <td align=right> ".($row->blmbayar)."</td>
              <td align=right> ".($row->tdktelat)."</td>
              <td align=right> ".($row->realisasitdktelat)."</td>
              <td align=right> ".($row->telat)."</td>
              <td align=right> ".($row->realisasitelat)."</td></tr>";	
          }
			  }
        echo "<tr>
              <th colspan='6'>Total</th>
				      <th align=right> ".($ttotal)."</th>
				      <th align=right> ".($treal)."</th>
				      <th align=right> ".($tblm)."</th>
				      <th align=right> ".($ttdk)."</th>
				      <th align=right> ".($trealtdk)."</th>
				      <th align=right> ".($ttelat)."</th>
				      <th align=right> ".($trealtelat)."</th>
				      </tr>";
		  }
	        ?>
	      </tbody>
	    </table>
      <!-- Start Export Data -->
      <table class="listtable" border=none id="tblData" hidden="true">
      	    <tr>
	       	    <th rowspan=2>No</th>
	       	    <th rowspan=2>Area</th>
	       	    <th rowspan=2>Salesman</th>
	       	    <th rowspan=2>Toko</th>
<!--              <th rowspan=2>Jenis</th>-->
			        <th rowspan=2>Tanggal</th>
	       	    <th rowspan=2>Nota</th>
			        <th rowspan=2>Target</th>
			        <th rowspan=2>Realisasi</th>
   			      <th rowspan=2>Blm Bayar</th>
			        <th colspan=2>Tdk Telat</th>
			        <th colspan=2>Telat</th>
            </tr>
            <tr>
			        <th>Target</th>
			        <th>Realisasi</th>
			        <th>Target</th>
			        <th>Realisasi</th>
            </tr>
	      <tbody>
	        <?php 
		  if($detail){
        $no=0;
        $ttotal=0;
        $treal=0;
        $tblm=0;
        $ttelat=0;
        $trealtelat=0;
        $ttdk=0;
        $trealtdk=0;
			  foreach($detail as $row){
          if($row->total>0 || $row->realisasi>0){
            $no++;
            $tmp=explode('-',$row->d_nota);
		        $tgl=$tmp[2];
		        $bln=$tmp[1];
		        $thn=$tmp[0];
            if(strlen($tgl)==2){
        		  $row->d_nota=$tgl.'-'.$bln.'-'.$thn;
            }
            $ttotal=$ttotal+$row->total;
            $treal =$treal+$row->realisasi;
            $tblm=$tblm+$row->blmbayar;
            $trealtelat =$trealtelat+$row->realisasitelat;
            $trealtdk =$trealtdk+$row->realisasitdktelat;
            $ttelat =$ttelat+$row->telat;
            $ttdk =$ttdk+$row->tdktelat;
		        echo "<tr>
              <td>$no</td>
              <td>$row->i_area - $row->e_area_name</td>
  				    <td>$row->i_salesman ($row->e_salesman_name)</td>
				      <td>($row->i_customer) - $row->e_customer_name</td>";
#              <td>$row->e_customer_classname</td>
            echo "
				      <td>$row->d_nota</td>
				      <td>$row->i_nota</td>
				      <td align=right>Rp. ".number_format($row->total)."</td>
				      <td align=right>Rp. ".number_format($row->realisasi)."</td>";
				    echo "
              <td align=right>Rp. ".number_format($row->blmbayar)."</td>
              <td align=right>Rp. ".number_format($row->tdktelat)."</td>
              <td align=right>Rp. ".number_format($row->realisasitdktelat)."</td>
              <td align=right>Rp. ".number_format($row->telat)."</td>
              <td align=right>Rp. ".number_format($row->realisasitelat)."</td></tr>";	
          }
			  }
        echo "<tr>
              <th colspan='6'>Total</th>
				      <th align=right>Rp. ".number_format($ttotal)."</th>
				      <th align=right>Rp. ".number_format($treal)."</th>
				      <th align=right>Rp. ".number_format($tblm)."</th>
				      <th align=right>Rp. ".number_format($ttdk)."</th>
				      <th align=right>Rp. ".number_format($trealtdk)."</th>
				      <th align=right>Rp. ".number_format($ttelat)."</th>
				      <th align=right>Rp. ".number_format($trealtelat)."</th>
				      </tr>";
		  }
	        ?>
	      </tbody>
	    </table>
      <!-- End Export Data -->
    <?php
    }else{
      echo "<center><h2>Target Collection belum ada</h2></center>";
    }
    ?>
</div>
<script language="javascript" type="text/javascript">
  function bbatal(a){
    show("listtargetcollectionrealtime/cform/view/"+a+"/","#main");
  }
  function yyy(){
  	lebar =1024;
    tinggi=768;
    periode=document.getElementById("iperiode").value;
    area   =document.getElementById("iarea").value;
    eval('window.open("<?php echo site_url(); ?>"+"/listtargetcollectionrealtime/cform/cetakdetail/"+periode+"/"+area,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
   //Export Data 06082022
   function exportTableToExcel(tableID, filename = ''){
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'excel_data.xls';
    
    // Create download link element
    downloadLink = document.createElement("a");
    
    document.body.appendChild(downloadLink);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
    
        // Setting the file name
        downloadLink.download = filename;
        
        //triggering the function
        downloadLink.click();
    }
}
</script>
