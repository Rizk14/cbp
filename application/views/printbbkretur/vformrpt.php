<?php 
 	include ("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>Untitled Document</title>
</head>
<style type="text/css" media="all">
/*
@page land {size: landscape;}
*{
size: landscape;
}
*/
.huruf {
  FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
}
.miring {
  font-style: italic;
  
}
.ceKotak{-
	background-color:#f0f0f0;
	border-bottom:#000000 1px solid;
	border-top:#000000 1px solid;
	border-left:#000000 1px solid;
	border-right:#000000 1px solid;
}
.garis { 
	background-color:#000000;
	width: 100%;
  height: 50%;
	font-size: 100px;
  border-style: solid;
  border-width:0.01px;
  border-collapse: collapse;
  spacing:1px;
}
.garis td { 
	background-color:#FFFFFF;
  border-style: solid;
  border-width:0.01px;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.garisx { 
	background-color:#000000;
	width: 100%;
  height: 50%;
  border-style: none;
  border-collapse: collapse;
  spacing:1px;
}
.garisx td { 
	background-color:#FFFFFF;
  border-style: none;
	font-size: 10px;
  FONT-WEIGHT: normal; 
  padding:1px;
}
.judul {
  font-size: 20px;
  FONT-WEIGHT: normal; 
}
.nmper {
  font-size: 18px;
  FONT-WEIGHT: normal; 
}
.isi {
  font-size: 14px;
  font-weight:normal;
  padding:1px;
}
.eusinya {
  font-size: 10px;
  font-weight:normal;
}
.ici {
  font-size: 12px;
  font-weight:normal;
}
.garis2 { 
	border-top:#000000 0.1px dotted;
	border-bottom:#000000 0.1px dotted;
}
.garisbawah { 
	border-top:#000000 0.1px dotted;
}
</style>
<style type="text/css" media="print">
.noDisplay{
	display:none;
}
.pagebreak {
    page-break-before: always;
}
</style>
<?php 
foreach($isi as $row)
{
?>
  <table width="100%" border="0" class="eusinya">
    <tr>
      <td width="70%" class="huruf isi" ><?php echo NmPerusahaan; ?></td>
      <td width="30%" class="huruf isi">KEPADA Yth. </td>
    </tr>
    <tr>
      <td width="70%" class="huruf isi"><?php echo AlmtPerusahaan; ?> </td>
      <td width="30%" class="huruf isi"><?php echo $row->e_supplier_name; ?></td>
    </tr>
  </table>
  <table width="478" border="0" class="eusinya">
    <tr>
      <td width="20" class="huruf isi">No</td>
      <td width="8" class="huruf isi">:</td>
      <td width="450" class="huruf isi"><?php echo $row->i_bbkretur; ?></td>
    </tr>
		 <tr>
      <td colspan=4 class="huruf ici">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4" class="huruf ici">Harap diterima barang-barang berikut ini : </td>
    </tr>
  </table>
  <table width="100%" border="0" class="huruf">
<!--
    <tr>
      <td colspan=6 class="garisbawah">&nbsp;</td>
    </tr>
-->
    <tr>
      <td width="24" class="garis2">No.</td>
      <td width="58" class="garis2">KD-BARANG</td>
      <td width="850" align=center class="garis2">NAMA BARANG</td>
      <td align=center width="90" class="garis2">UNIT</td>
      <td align=center width="100" class="garis2">HARGA</td>
      <td align=center width="110" class="garis2">JUMLAH</td>
    </tr>
<!--
    <tr>
      <td colspan=6 class=garisbawah>&nbsp;</td>
    </tr>
-->

<!--</table>-->
<?php 
  $i	= 0;
	$j	= 0;
	$hrg    = 0;
	$ibbkretur			= $row->i_bbkretur;
	$query 	= $this->db->query(" select i_product from tm_bbkretur_item where i_bbkretur='$ibbkretur'",false);
	$jml 	= $query->num_rows();
  $total=0;
	foreach($detail as $rowi){
	  if($rowi->n_quantity>0){
      $i++;
		  $hrg	= $hrg+($rowi->n_quantity*$rowi->v_unit_price);
 	    $pro	= $rowi->i_product;
	    $nam	= $rowi->e_product_name;
		  $del	= number_format($rowi->n_quantity);
		  $pric	= number_format($rowi->v_unit_price);
		  $tot	= $rowi->n_quantity*$rowi->v_unit_price;
      $total	= $total+$tot;
      $tot=number_format($tot);
?>
      <!--<table width="98%" border="0" cellpadding="0" cellspacing="0">-->
        <tr>
          <td height="1" class="huruf ici" align=right><?php echo $i; ?></td>
          <td height="1" class="huruf ici"><?php echo $pro; ?></td>
          <td height="1" class="huruf ici"><?php echo $nam; ?></td>
          <td height="1" align=center class="huruf ici"><?php echo $del; ?></td>
          <td height="1" align=right class="huruf ici"><?php echo $pric; ?></td>
          <td height="1" align=right class="huruf ici"><?php echo $tot; ?></td>
        </tr>
<?php 
    }
  }

?>
    <tr>
      <td colspan=6 class=garisbawah>&nbsp;</td>
    </tr>
  </table>
  <table width="100%" border="0" class="huruf ici">
    <tr>
      <td align=right>TOTAL &nbsp;&nbsp; : &nbsp;&nbsp; <?php echo number_format($total); ?></td>
    </tr>
  </table>
  <br>
<?php 
		$tmp=explode("-",$row->d_bbkretur);
		$th=$tmp[0];
		$bl=$tmp[1];
		$hr=$tmp[2];
		$row->d_bbkretur=$hr." ".substr(mbulan($bl),0,3)." ".$th;
?>
  <table width="100%" border="0">
    <tr>
      <td colspan="2" align=right width="20%" class="huruf ici">Bandung, <?php echo $row->d_bbkretur; ?></td>
    </tr>
  </table>
  <table width="100%" border="0">
    <tr>
      <td colspan="2" align=center class="huruf ici">Penerima</td>
      <td colspan="2" align=center class="huruf ici">Mengetahui</td>
      <td colspan="2" align=center class="huruf ici">Pengirim</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align=center class="huruf ici">(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td align=center class="huruf ici">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
      <td align=center class="huruf ici">(</td>
      <td align=center class="huruf ici">)</td>
    </tr>
  </table>
<?php 
}
?>
<div class="noDisplay"><center><b><a href="#" onClick="window.print()">Print</a></b></center></div>
