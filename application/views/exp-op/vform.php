<?php echo "<h2>$page_title</h2>"; ?>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'exp-op/cform/view', 'update' => '#main', 'type' => 'post')); ?>
			<div id="spbperareaform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable">
							<tr>
								<td width="19%">Date From</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="areafrom" name="areafrom" value="">
									<?php
									$data = array(
										'name'        => 'dfrom',
										'id'          => 'dfrom',
										'value'       => date('01-m-Y'),
										'readonly'   	=> 'true',
										'onclick'	    => "showCalendar('',this,this,'','dfrom',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Date To</td>
								<td width="1%">:</td>
								<td width="80%">
									<?php
									$data = array(
										'name'        => 'dto',
										'id'          => 'dto',
										'value'       => date('d-m-Y'),
										'readonly'   	=> 'true',
										'onclick'	    => "showCalendar('',this,this,'','dto',0,20,1)"
									);
									echo form_input($data); ?>
								</td>
							</tr>
							<tr>
								<td width="19%">Supplier</td>
								<td width="1%">:</td>
								<td width="80%">
									<input type="hidden" id="isupplier" name="isupplier" value="">
									<input type="text" id="esuppliername" name="esuppliername" size="40" value="" onclick='showModal("exp-op/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
								</td>
							<tr>
								<td width="19%">&nbsp;</td>
								<td width="1%">&nbsp;</td>
								<td width="80%">
									<input name="view" id="view" value="View" type="submit">
									<input name="cmdreset" id="cmdreset" value="Refresh" type="button" onclick="show('exp-op/cform/','#main')">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>
<script languge=javascript type=text/javascript>
	function buatperiode() {
		periode = document.getElementById("tahun").value + document.getElementById("bulan").value;
		document.getElementById("iperiode").value = periode;
	}

	function view_excel() {
		lebar = 1366;
		tinggi = 768;
		dfrom = document.getElementById("dfrom").value;
		dto = document.getElementById("dto").value;
		iarea = document.getElementById("isupplier").value;
		eval('window.open("<?php echo site_url(); ?>"+"/exp-op/cform/export/"+isupplier+"/"+dfrom+"/"+dto,"","width="+lebar+"px,height="+tinggi+"px,resizable=yes,scrollbars=yes,location=yes,menubar=yes,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}
</script>