<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
   <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body id="bodylist">
   <div id="main">
      <div id="tmp">
         <?php echo "<center><h2>$page_title</h2></center>"; ?>
         <table class="maintable">
            <tr>
               <td align="left">
                  <?
                  $tujuan = 'akt-bank-multialloc/cform/carinota/' . $baris . '/' . $isupplier;
                  ?>

                  <?php echo $this->pquery->form_remote_tag(array('url' => $tujuan, 'update' => '#light', 'type' => 'post')); ?>
                  <div id="listform">
                     <div class="effect">
                        <div class="accordion2">
                           <table class="listtable">
                              <thead>
                                 <tr>
                                    <td colspan="8" align="center">Cari data :
                                       <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari">
                                       <input type="hidden" id="baris" name="baris" value="<?php echo $baris; ?>">
                                       <input type="hidden" id="isupplier" name="isupplier" value="<?php echo $isupplier; ?>">
                                    </td>
                                 </tr>
                              </thead>
                              <th align="center">Nota</th>
                              <th align="center">Kode Supplier</th>
                              <th align="center">Tanggal</th>
                              <th align="center">Jumlah</th>
                              <th align="center">Sisa</th>
                              <tbody>
                                 <?php
                                 if ($isi) {
                                    foreach ($isi as $row) {
                                       $tmp = explode("-", $row->d_dtap);
                                       $dd = $tmp[2];
                                       $mm = $tmp[1];
                                       $yy = $tmp[0];
                                       $row->d_dtap = $dd . "-" . $mm . "-" . $yy;
                                       echo "<tr>
           <td><a href=\"javascript:setvalue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">$row->i_dtap</a></td>
           <td><a href=\"javascript:setvalue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">$row->i_supplier</a></td>
           <td><a href=\"javascript:setvalue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">$row->d_dtap</a></td>
           <td align=right><a href=\"javascript:setvalue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">" . number_format($row->v_netto) . "</a></td>
           <td align=right><a href=\"javascript:setvalue('$row->i_dtap','$row->d_dtap','$row->v_netto','$row->v_sisa',$baris)\">" . number_format($row->v_sisa) . "</a></td>
            </tr>";
                                    }
                                 }
                                 ?>
                              </tbody>
                           </table>
                           <?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?>
                           <br>
                           <center><input type="button" id="Keluar" name="Keluar" value="Keluar" onclick="bbatal()"></center>
                        </div>
                     </div>
                  </div>
                  <?= form_close() ?>
               </td>
            </tr>
         </table>
      </div>
   </div>
</BODY>

</html>
<script language="javascript" type="text/javascript">
   function setvalue(a, b, c, d, e) {
      ada = false;
      besar = false;
      for (z = 1; z <= e; z++) {
         if (
            (a == document.getElementById("idtap" + z).value) &&
            (z !== e)
         ) {
            alert("Nota : " + a + " sudah ada !!!!!");
            ada = true;
            break;
         } else {
            ada = false;
         }
      }
      if (!ada) {
         document.getElementById("idtap" + e).value = a;
         document.getElementById("ddtap" + e).value = b;
         document.getElementById("vsesa" + e).value = formatcemua(d);
         document.getElementById("vnota" + e).value = formatcemua(d);
         document.getElementById("vsisa" + e).value = formatcemua(d);

         tmp = formatulang(document.getElementById("vjumlah").value);
         jml = document.getElementById("jml").value;

         if (tmp > 0) {
            tmp = parseFloat(tmp);
            sisa = 0;
            jumasal = tmp;
            jumall = jumasal;
            bay = 0;

            for (x = 1; x <= jml; x++) {
               if (document.getElementById("vjumlah" + x).value == '') {
                  jum = parseFloat(formatulang(document.getElementById("vsisa" + x).value));
               } else {
                  jum = parseFloat(formatulang(document.getElementById("vjumlah" + x).value));
               }
               jumall = jumall - jum;
               if (jumall > 0) {
                  document.getElementById("vlebih").value = formatcemua(jumall);
                  if (x == e) {
                     document.getElementById("vjumlah" + e).value = formatcemua(d);
                     by = parseFloat(formatulang(document.getElementById("vjumlah" + e).value));
                     bay = jumasal - by;
                     sis = parseFloat(formatulang(document.getElementById("vsisa" + e).value));
                     document.getElementById("vlebih" + e).value = formatcemua(bay);
                  }
                  sisa = sisa + jum;
               } else {
                  document.getElementById("vlebih").value = '0';
                  document.getElementById("vlebih" + e).value = '0';
                  document.getElementById("vjumlah" + e).value = formatcemua(jumasal - sisa);
                  document.getElementById("vlebih" + e).value = '0';
               }
            }
            //          alert(document.getElementById("vjumlah"+e).value);
         }
         hetang(e);
         jsDlgHide("#konten *", "#fade", "#light");
      }
   }

   function bbatal() {
      baris = document.getElementById("baris").value;
      si_inner = document.getElementById("detailisi").innerHTML;
      //   alert(si_inner);
      var temp = new Array();
      temp = si_inner.split('<table disabled="disabled" id="itemdet"><tbody disabled="disabled">');
      if ((document.getElementById("idtap" + baris).value == '')) {
         si_inner = '';
         for (x = 1; x < baris; x++) {
            si_inner = si_inner + '<table><tbody>' + temp[x];
         }
         j = 0;
         var barbar = Array();
         var idtap = Array();
         var ddtap = Array();
         var vnota = Array();
         var vjumlah = Array();
         var vsesa = Array();
         var vsisa = Array();
         var vlebih = Array();
         for (i = 1; i < baris; i++) {
            j++;
            barbar[j] = document.getElementById("baris" + i).value;
            idtap[j] = document.getElementById("idtap" + i).value;
            ddtap[j] = document.getElementById("ddtap" + i).value;
            vnota[j] = document.getElementById("vnota" + i).value;
            vjumlah[j] = document.getElementById("vjumlah" + i).value;
            vsesa[j] = document.getElementById("vsesa" + i).value;
            vsisa[j] = document.getElementById("vsisa" + i).value;
            vlebih[j] = document.getElementById("vlebih" + i).value;
         }
         //      alert(si_inner);
         document.getElementById("detailisi").innerHTML = si_inner;
         j = 0;

         for (i = 1; i < baris; i++) {
            j++;
            document.getElementById("baris" + i).value = barbar[j];
            document.getElementById("idtap" + i).value = idtap[j];
            document.getElementById("ddtap" + i).value = ddtap[j];
            document.getElementById("vnota" + i).value = vnota[j];
            document.getElementById("vjumlah" + i).value = vjumlah[j];
            document.getElementById("vsesa" + i).value = vsesa[j];
            document.getElementById("vsisa" + i).value = vsisa[j];
            document.getElementById("vlebih" + i).value = vlebih[j];
         }
         document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) - 1;
         jsDlgHide("#konten *", "#fade", "#light");
      }
   }
</script>