<div id="tmpx">
	<?php
	echo "<h2>$page_title</h2>";
	if (($ikbank == '') && (($dfrom == '') || ($dto == '') || ($ibank == '')) && ($ialokasi == '')) {
		$data['dfrom']	= $dfrom;
		$data['dto']		= $dto;
		$data['ibank']	= $ibank;
		$data['ikbank']		= '';
		$data['isi']		= $isi;
		$this->load->view('akt-bank-multialloc/vform', $data);
	} elseif (($ikbank != '') && (($dfrom != '') || ($dto != '') || ($ibank != '')) && ($ialokasi == '')) {
		$data['jmlitem']	= $jmlitem;
		$data['isi']			= $isi;
		$data['detail']		= $detail;
		$data['ikbank']		= $ikbank;
		$data['eareaname'] = $eareaname;
		$data['ebankname'] = $ebankname;
		$data['vsisa']		= $vsisa;
		$data['dfrom']		= $dfrom;
		$data['dto']			= $dto;
		$data['dbank']		= $dbank;
		$data['icoabank']	= $icoabank;
		$data['ibank']		= $ibank;
		$this->load->view('akt-bank-multialloc/vformapprove', $data);
	} elseif ((($dfrom != '') || ($dto != '') || ($ibank != '')) && ($ikbank == '') && ($ialokasi == '')) {
		$data['isi'] 		= $isi;
		$data['ikbank'] 		= '';
		$data['ialokasi'] 		= '';
		$data['ibank']	= $ibank;
		$data['dfrom']	= $dfrom;
		$data['dto']		= $dto;

		$this->load->view('akt-bank-multialloc/vformview', $data);
	} else {
		$data['jmlitem'] = $jmlitem;
		$data['isi']		= $isi;
		$data['detail']	= $detail;
		$data['ikbank']		= $ikbank;
		$data['isupplier']	= $isupplier;
		$data['dfrom']	= $dfrom;
		$data['dto']		= $dto;
		$data['vbulat'] = $vbulat;
		$data['vsisa']	= $vsisa;
		$data['bisaedit'] = $bisaedit;
		$this->load->view('akt-bank-multialloc/vformupdate', $data);
	}
	?>
</div>