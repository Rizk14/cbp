<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => 'akt-bank-multialloc/cform/view', 'update' => '#tmpx', 'type' => 'post')); ?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table class="listtable">
							<thead>
								<tr>
									<td colspan="6" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;
										<input type="submit" id="bcari" name="bcari" value="Cari">
										<input type="hidden" id="dfrom" name="dfrom" value="<?php echo $dfrom; ?>">
										<input type="hidden" id="dto" name="dto" value="<?php echo $dto; ?>">
										<input type="hidden" id="ibank" name="ibank" value="<?php echo $ibank; ?>">
										<input type="hidden" id="icoa" name="icoa" value="<?php echo $icoa; ?>">
									</td>
								</tr>
							</thead>
							<th>Bank Keluar</th>
							<th>Tanggal</th>
							<th>Area</th>
							<th>Jumlah</th>
							<th>sisa</th>
							<th class="action">Action</th>
							<tbody>
								<?php
								if ($isi) {
									foreach ($isi as $row) {
										$tmp = explode("-", $row->d_bank);
										$th = $tmp[0];
										$bl = $tmp[1];
										$hr = $tmp[2];
										$dbank = $hr . "-" . $bl . "-" . $th;
										$nama = str_replace('%20', ' ', $row->e_area_name);
										$namabank = str_replace('%20', ' ', $row->e_bank_name);
										#        $dt= str_replace('/', 'tandagaring',$row->i_kbank);
										echo "<tr>
												<td>$row->i_kbank</td>
												<td>$dbank</td>
												<td>$nama</td>
												<td align=right>" . number_format($row->v_bank) . "</td>
												<td align=right>" . number_format($row->v_sisa) . "</td>
												<td class=\"action\"><a href=\"#\" onclick='show(\"akt-bank-multialloc/cform/approve/$row->i_kbank/$row->i_area/$nama/$row->v_bank/$dfrom/$dto/$dbank/$namabank/$row->i_coa_bank/$row->v_sisa/$ibank/$icoa/\",\"#main\")'><img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\"></a></td>
											</tr>";
									}
									echo "<input type=\"hidden\" id=\"inotaedit\" name=\"inotaedit\" value=\"\">";
								}
								?>
							</tbody>
						</table>
						<?php echo "<center>" . $this->paginationxx->create_links() . "</center>"; ?>
					</div>
				</div>
			</div>
			<?= form_close() ?>
			<input name="cmdreset" id="cmdreset" value="Kembali" type="button" onclick="show('akt-bank-multialloc/cform/','#tmpx')">
		</td>
	</tr>
</table>
<script language="javascript" type="text/javascript">
	function yyy(b) {
		document.getElementById("inotaedit").value = b;
		formna = document.getElementById("listform");
		formna.action = "<?php echo site_url(); ?>" + "/akt-bank-multialloc/cform/approve";
		formna.submit();
	}
</script>