<table class="maintable">
  <tr>
    <td align="left">
    <?php echo form_open('domanual/cform/update', array('id' => 'domanualformupdate', 'name' => 'domanualformupdate', 'onsubmit' => 'sendRequest(); return false'));?>
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
      <tr>
		<td>No OP</td>
		<td><input readonly id="iop" name="iop" onclick="view_op()" value="<?php echo $isi->i_op;?>"></td>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name; ?>">
		    <input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area; ?>"></td>
	      </tr>
	      <tr>
		<td>No DO</td>
		<td><input id="ido" name="ido" type="text" maxlength="14" value="<?php echo $isi->i_do; ?>"><input id="idoold" name="idoold" type="hidden" value="<?php echo $isi->i_do; ?>"></td>
		<td>Tanggal DO</td>
		<?php 
			$tmp=explode("-",$isi->d_do);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$ddo=$hr."-".$bl."-".$th;?>
        <input hidden id="bdo" name="bdo" value="<?php echo $bl; ?>">
<?php 			$query3	= $this->db->query(" SELECT sum(n_deliver*v_product_mill) as jum_kotor FROM tm_do_item
							WHERE i_do='$isi->i_do' AND i_supplier = '$isi->i_supplier' ");
			
			if ($query3->num_rows() > 0){
				$hasilrow = $query3->row();
				$v_do_gross = $hasilrow->jum_kotor;
			}
			// number_format($isi->v_do_gross);
			
			// 21-08-2014, cek apakah udh ada nota/AP utk DO ini. jika udh ada, maka tombol simpan ga akan muncul
			$query3	= $this->db->query(" SELECT distinct i_dtap FROM tm_dtap_item
							WHERE i_do='$isi->i_do' AND i_supplier = '$isi->i_supplier' ");
			
			if ($query3->num_rows() > 0){
				$is_ada_nota = "y";
			}
			else
				$is_ada_nota = "t";
		?>

		<td><input readonly id="ddo" name="ddo" onclick="showCalendar('',this,this,'','ddo',0,20,1)" value="<?php echo $ddo; ?>">
			<input hidden id="tgldo" name="tgldo" value="<?php echo $ddo; ?>"">
		</td>
	      </tr>
	      <tr>
		<td>Pemasok</td>
		<td><input readonly id="esuppliername" name="esuppliername" onclick="view_supplier()" value="<?php echo $isi->e_supplier_name; ?>">
		    <input id="isupplier" name="isupplier" type="hidden" value="<?php echo $isi->i_supplier; ?>"></td>
		<td>Nilai Kotor</td>
		<td><input style="text-align:right;" readonly id="vdogross" name="vdogross" value="<?php echo number_format($v_do_gross); ?>"></td>
	      </tr>
		<tr>
		  <td width="100%" align="center" colspan="4">
		<?php $departement = $this->session->userdata('departement'); 
		if($isi->f_do_cancel=='f' && $is_ada_nota == "t" && $departement != '1'){ ?>
		    <input name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
        <?php }?>
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listdo/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>","#main")'>
        <?php if($isi->f_do_cancel=='f' && $is_ada_nota == "t" && $departement != '1'){ ?>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1,document.getElementById('iop').value);">
        <?php }?></td>
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table class="listtable" align="center" style="width: 750px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:65px;" align="center">Kode</th>
					<th style="width:300px;" align="center">Nama Barang</th>
					<th style="width:100px;" align="center">Ket</th>
					<th style="width:90px;" align="center">Harga</th>
					<th style="width:46px;" align="center">Jml Kirim</th>
					<th style="width:94px;" align="center">Total</th>
					<th style="width:32px;" align="center" class="action">Action</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
				$i=0;
				foreach($detail as $row)
				{
					echo '<table class="listtable" align="center" style="width:750px;">';
				  	$i++;
					$pangaos=number_format($row->v_product_mill);
					$total=$row->v_product_mill*$row->n_deliver;
					$total=number_format($total,2);
				  	echo "<tbody>
							<tr>
		    				<td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" 
								id=\"baris$i\" name=\"baris$i\" value=\"$i\">
													  <input type=\"hidden\" 
								id=\"motif$i\" name=\"motif$i\" value=\"$row->i_product_motif\"></td>
							<td style=\"width:62px;\"><input style=\"width:62px;\" readonly type=\"text\" 
								id=\"iproduct$i\" name=\"iproduct$i\" value=\"$row->i_product\"></td>
							<td style=\"width:266px;\"><input style=\"width:266px;\" readonly type=\"text\" 
								id=\"eproductname$i\"
								name=\"eproductname$i\" value=\"$row->e_product_name\"></td>
							<td style=\"width:91px;\"><input style=\"width:91px;\" type=\"text\" 
								id=\"eremark$i\"
								name=\"eremark$i\" value=\"$row->e_remark\"><input readonly style=\"width:91px;\" type=\"hidden\" 
								id=\"emotifname$i\"
								name=\"emotifname$i\" value=\"$row->e_product_motifname\"></td>
							<td style=\"width:83px;\"><input readonly style=\"text-align:right; 
								width:83px;\"  type=\"text\" id=\"vproductmill$i\"
								name=\"vproductmill$i\" value=\"$pangaos\"></td>
							<td style=\"width:44px;\"><input style=\"text-align:right; width:44px;\" 
								type=\"text\" id=\"ndeliver$i\" name=\"ndeliver$i\" value=\"$row->n_deliver\" 
								onkeyup=\"hitungnilai(this.value,'$jmlitem'); pembandingnilai('$i');\">
                <input type=\"hidden\" id=\"ntmp$i\" name=\"ntmp$i\" value=\"$row->n_deliver\">
                <input type=\"hidden\" id=\"ndeliverhidden$i\" name=\"ndeliverhidden$i\" value=\"$row->n_order\"></td>
							<td style=\"width:85px;\"><input style=\"text-align:right; width:85px;\" readonly
								type=\"text\" id=\"vtotal$i\" name=\"vtotal$i\" value=\"$total\"></td>
							<td style=\"width:48px;\" align=\"center\">";
              if(($departement == '1')){}else if(($isi->f_do_cancel=='f')){
                echo "
								<a href=\"javascript:xxx('$row->i_do','$row->i_supplier','$row->i_product','$row->i_product_grade',
														 '$row->i_product_motif','$pangaos','$row->n_deliver','".$this->lang->line('delete_confirm')."'
														);\">";
								echo"	<img height=15px; style=\"cursor:hand;\" src=\"". base_url()."img/delete.png\" 
									 border=\"0\" alt=\"delete\"></a>";
              }
							echo	"</td>
							</tr>
						  </tbody></table>";
				}
				echo "<input type=\"hidden\" id=\"idodelete\" name=\"idodelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"isupplierdelete\" name=\"isupplierdelete\" value=\"\">
		      		  <input type=\"hidden\" id=\"iproductgradedelete\" name=\"iproductgradedelete\" 	value=\"\">
		      		  <input type=\"hidden\" id=\"iproductmotifdelete\" name=\"iproductmotifdelete\" 	value=\"\">
					  <input type=\"hidden\" id=\"iproductdelete\" name=\"iproductdelete\" value=\"\">
		     		 ";
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
	  </div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function view_supplier(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/domanual/cform/supplier/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function view_op(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/domanual/cform/op/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
  function tambah_item(a,b){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:750px;">';
		so_inner+= '<th style="width:25px;"  align="center">No</th>';
		so_inner+= '<th style="width:63px;" align="center">Kode</th>';
		so_inner+= '<th style="width:300px;" align="center">Nama Barang</th>';
		so_inner+= '<th style="width:100px;" align="center">Ket</th>';
		so_inner+= '<th style="width:90px;"  align="center">Harga</th>';
		so_inner+= '<th style="width:46px;"  align="center">Jml kirim</th>';
		so_inner+= '<th style="width:94px;"  align="center">Total</th>';
		so_inner+= '<th style="width:32px;"  align="center" class="action">Action</th></table>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:62px;"><input style="width:62px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:266px;"><input style="width:266px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:91px;"><input readonly style="width:91px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""><input readonly style="width:91px;"  type="hidden" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:83px;"><input readonly style="text-align:right; width:83px;"  type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:44px;"><input style="text-align:right; width:44px;" type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+'); pembandingnilai('+a+');"><input type="hidden" id="ndeliverhidden'+a+'" name="ndeliverhidden'+a+'" value=""><input type="hidden" id="ntmp'+a+'" name="ntmp'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value="">';
		si_inner+='</td><td style="width:48px;">&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" align="center" style="width:750px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"><input type="hidden" id="motif'+a+'" name="motif'+a+'" value=""></td>';
		si_inner+='<td style="width:62px;"><input style="width:62px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:266px;"><input style="width:266px;" readonly type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:91px;"><input readonly style="width:91px;"  type="text" id="eremark'+a+'" name="eremark'+a+'" value=""><input readonly style="width:91px;"  type="hidden" id="emotifname'+a+'" name="emotifname'+a+'" value=""></td>';
		si_inner+='<td style="width:83px;"><input readonly style="text-align:right; width:83px;" type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:44px;"><input style="text-align:right; width:44px;" type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value="" onkeyup="hitungnilai(this.value,'+juml+'); pembandingnilai('+a+');"><input type="hidden" id="ndeliverhidden'+a+'" name="ndeliverhidden'+a+'" value=""><input type="hidden" id="ntmp'+a+'" name="ntmp'+a+'" value=""></td>';
		si_inner+='<td style="width:85px;"><input readonly style="text-align:right; width:85px;" type="text" id="vtotal'+a+'" name="vtotal'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;">&nbsp;</td></tr></tbody></table>';
    }
    j=0;
    var baris			=Array()
    var iproduct		=Array();
    var eproductname	=Array();
	var vproductmill	=Array();
    var ndeliver		=Array();
    var ndeliverhidden	=Array();
	var motif			=Array();
	var remark			=Array();
	var motifname		=Array();
    var vtotal			=Array();
    for(i=1;i<a;i++){
	j++;
	baris[j]			=document.getElementById("baris"+i).value;
	iproduct[j]			=document.getElementById("iproduct"+i).value;
	eproductname[j]		=document.getElementById("eproductname"+i).value;
	vproductmill[j]		=document.getElementById("vproductmill"+i).value;
	ndeliver[j]			=document.getElementById("ndeliver"+i).value;
	ndeliverhidden[j]	=document.getElementById("ndeliverhidden"+i).value;
	motif[j]			=document.getElementById("motif"+i).value;
	remark[j]			=document.getElementById("eremark"+i).value;
	motifname[j]		=document.getElementById("emotifname"+i).value;
	vtotal[j]			=document.getElementById("vtotal"+i).value;	
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("vproductmill"+i).value=vproductmill[j];
		document.getElementById("ndeliver"+i).value=ndeliver[j];
		document.getElementById("ndeliverhidden"+i).value=ndeliverhidden[j];
		document.getElementById("motif"+i).value=motif[j];
		document.getElementById("eremark"+i).value=remark[j];
		document.getElementById("emotifname"+i).value=motifname[j];
		document.getElementById("vtotal"+i).value=vtotal[j];	
    }
/*
    lebar =500;
    tinggi=400;
	eval('window.open("<?php echo site_url(); ?>"+"/domanual/cform/productupdate/"+a+"/"+b,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
*/
    showModal("domanual/cform/product/"+a+"/"+b,"#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("ddo").value!='') &&
  	 	(document.getElementById("isupplier").value!='') &&
  	 	(document.getElementById("ido").value!='') &&
		(document.getElementById("iop").value!='')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if((document.getElementById("iproduct"+i).value=='') ||
					(document.getElementById("eproductname"+i).value=='') ||
					(document.getElementById("ndeliver"+i).value=='')){
					alert('Data item masih ada yang salah !!!');
					exit();
					cek='false';
				}else{
					cek='true';	
				} 
			}
		}
		if(cek=='true'){
  	  		document.getElementById("cmdtambahitem").hidden=true;
  	  		document.getElementById("login").hidden=true;
    	}else{
  	  		document.getElementById("cmdtambahitem").hidden=false;
		   	document.getElementById("login").hidden=false;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").hidden=false;
	self.location="<?php echo site_url().'/listdo/cform'; ?>";
  }
  function hitungnilai(isi,jml){
	if (isNaN(parseFloat(isi))){
		alert("Input harus numerik");
	}else{
		vtot =0;
		for(i=1;i<=jml;i++){
			vhrg=formatulang(document.getElementById("vproductmill"+i).value);
			nqty=formatulang(document.getElementById("ndeliver"+i).value);
			vhrg=parseFloat(vhrg)*parseFloat(nqty);
			vtot=vtot+vhrg;
			document.getElementById("vtotal"+i).value=formatcemua(vhrg);
		}
		document.getElementById("vdogross").value=formatcemua(vtot);
	}
  }

  function pembandingnilai(a) 
  {
	  var n_deliver	= document.getElementById('ndeliver'+a).value;
	  var deliverasal	= document.getElementById('ndeliverhidden'+a).value;
	  
	  if(parseFloat(n_deliver) > parseFloat(deliverasal)) {
		  alert('Jml kirim ( '+n_deliver+' item ) tdk dpt melebihi Order ( '+deliverasal+' item )');
		  document.getElementById('ndeliver'+a).value	= deliverasal;
		  document.getElementById('ndeliver'+a).focus();
		  return false;
	  }
  }

  function xxx(a,b,c,d,e,f,g,h){
    if (confirm(h)==1){
		document.getElementById("idodelete").value=a;
		document.getElementById("isupplierdelete").value=b;
		document.getElementById("iproductdelete").value=c;
		document.getElementById("iproductgradedelete").value=d;
		document.getElementById("iproductmotifdelete").value=e;
		vtot	=parseFloat(formatulang(document.getElementById("vdogross").value));
		vmin	=parseFloat(formatulang(f))*parseFloat(formatulang(g));
		vtot	=vtot-vmin;
		document.getElementById("vdogross").value=formatcemua(vtot);
		formna=document.getElementById("domanualformupdate");
		formna.action		="<?php echo site_url(); ?>"+"/domanual/cform/deletedetail/"+vtot;
		formna.submit();
    }
  }
  function afterSetDateValue(ref_field, target_field, date) {
    if (date!="") {
      var tmp=document.getElementById('ddo').value;
      var dox=document.getElementById('ido').value;
      if(dox.length==14){
        atu=tmp.substring(8);
        uwa=tmp.substring(3,5);
        ddo=atu+uwa;
        ido=dox.substring(0,3)+ddo+dox.substring(7);
        document.getElementById('ido').value=ido;
      }
      
      dspb=document.getElementById('ddo').value;
      bspb=document.getElementById('bdo').value;
      dtmp=dspb.split('-');
      per=dtmp[2]+dtmp[1]+dtmp[0];
      bln = dtmp[1];
      if( (bspb!='') && (dspb!='') ){
        if(bspb != bln)
        {
          alert("Tanggal DO tidak boleh dalam bulan yang berbeda !!!");
          document.getElementById("ddo").value=document.getElementById("tgldo").value;
        }
      }
    }
  }
</script>
