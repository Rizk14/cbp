<h2><?= $page_title ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/approve', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="rrkhform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td>No SJP</td>
								<td>
									<input readonly id="isjp" name="isjp" value="<?= $isi->i_sjp ?>">
									<input readonly id="id" name="id" type="hidden" value="<?= $isi->id ?>">
								</td>
							</tr>
							<tr>
								<td>Area</td>
								<td>
									<input readonly id="eareaname" name="eareaname" value="<?= $isi->e_area_name ?>">
									<input readonly id="iarea" name="iarea" type="hidden" value="<?= $isi->i_area ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Dari</td>
								<td>
									<input id="esender" name="esender" value="<?= $isi->e_sender ?>" placeholder="Nama Penyerah" onkeyup="gede(this)" maxlength="125" readonly> /
									<input style="width:200px" id="esendercompany" name="esendercompany" value="<?= $isi->e_sender_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125" readonly>
								</td>
							</tr>
							<tr>
								<td width="10%">Untuk</td>
								<td>
									<input id="erecipient" name="erecipient" value="<?= $isi->e_recipient ?>" placeholder="Nama Penerima" onkeyup="gede(this)" maxlength="125" readonly> /
									<input style="width:200px" id="erecipientcompany" name="erecipientcompany" value="<?= $isi->e_recipient_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125" readonly>
								</td>
							</tr>
							<tr>
								<td width="10%">Tgl SJP</td>
								<td>
									<input readonly id="dsjp" name="dsjp" value="<?= $isi->d_sjp ?>">
									<input readonly id="hari" name="hari" value="">
								</td>
							</tr>
							<tr>
								<td width="10%">Keterangan</td>
								<td>
									<input id="eremarkhead" name="eremarkhead" value="<?= $isi->e_remark ?>" onkeyup="gede(this)" readonly>
								</td>
							</tr>

							<tr>
								<td>&nbsp;</td>
								<td width="100%">
									<center>
										<input name="approve" id="approve" value="Approve" type="submit" onclick="dipales()">
										<input value="Reject" type="button" onclick="return appdetail('<?= $isi->id ?>','<?= $isi->i_sjp ?>');">
										<input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/index/','#main');">
									</center>
								</td>
							</tr>
						</table>

						<div id="detailisi" align="center">
							<table id="itemtem" class="listtable" style="width:100%;">
								<tr>
									<th style="width:20px;" align="center">No</th>
									<th style="width:82px;" align="center">Kode Barang</th>
									<th style="width:250px;" align="center">Nama Barang</th>
									<th style="width:30px;" align="center">Jumlah</th>
									<th style="width:50px;" align="center">Satuan</th>
									<th style="width:150px;" align="center">Keterangan</th>
									<th style="width:30px;" align="center" class="action">Act</th>
								</tr>

								<?php if ($detail) {
									$no = 0;
									foreach ($detail as $riw) {
										$no++;
										echo "<tr>
											  <td  style=\"width:46px;text-align:center\">$no</td>
											  <td style=\"width:77px;\"><input readonly style=\"width:143px\" id=\"iproduct$no\" name=\"iproduct$no\" value=\"$riw->i_product\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:240px;\"><input readonly style=\"width:434px\" id=\"eproductname$no\" name=\"eproductname$no\" value=\"$riw->e_product_name\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:47px;text-align:center;\"><input readonly style=\"width:85px\" id=\"qty$no\" name=\"qty$no\" value=\"$riw->n_quantity\" onkeyup=\"validasi_number();\"></td>
											  <td style=\"width:50px;text-align:center;\"><input readonly style=\"width:86px\" id=\"satuan$no\" name=\"satuan$no\" value=\"$riw->n_satuan\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:92px;\"><input readonly style=\"width:263px\" id=\"eremark$no\" name=\"eremark$no\" value=\"$riw->e_remark\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:68px;\" class=\"action\"></td>
											</tr>";
									}
								} ?>
							</table>
						</div>
						<div id="pesan"></div>
						<input readonly type="hidden" name="jml" id="jml" value="<?= $jmlitem ?>">
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		carihari()

		/* DISABLED SUBMIT SAAT ENTER KEYBOARD */
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		/* *********************************** */
	});

	function dipales() {
		// if (document.getElementById('erecipient').value == '') {
		// 	alert("Kolom Penerima Masih Kosong!!");
		// 	return false;
		// } else if (document.getElementById('erecipientcompany').value == '') {
		// 	alert("Kolom Perusahaan Penerima Masih Kosong!!");
		// 	return false;
		// } else {
		setTimeout(() => {
			$("input").attr("disabled", true);
			$("select").attr("disabled", true);
			$("#submit").attr("disabled", true);
			$("#button").attr("disabled", true);
			document.getElementById("cmdback").removeAttribute('disabled');
		}, 100);
		// }
	}

	function afterSetDateValue(ref_field, target_field, date) {

		if (date != "") {
			var startDate = document.getElementById('dsjp').value;
			tes = startDate.split('-');
			startDate = tes[2] + '-' + tes[1] + '-' + tes[0];
			var today = new Date();
			var daten = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
			var curr_date = today.getDate();
			var curr_month = (today.getMonth() + 1);
			var curr_year = today.getFullYear();

			if (curr_month < 10) curr_month = '0' + curr_month;
			if (curr_date < 10) curr_date = '0' + curr_date;
			current = curr_year + "-" + curr_month + "-" + curr_date;
			currentx = curr_date + "-" + curr_month + "-" + curr_year;
			console.log(startDate)
			console.log(current)
			if (startDate < current) {
				alert("Tanggal tidak boleh lebih kecil dari hari ini ( " + currentx + " )");
				document.getElementById('dsjp').value = currentx;
			}

			carihari()
		}
	}

	function carihari() {
		var tanggallengkap = document.getElementById("dsjp").value;
		var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
		namahari = namahari.split(" ");
		var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
		namabulan = namabulan.split(" ");
		tanggallengkap = tanggallengkap.split("-");
		tanggallengkap[1] = tanggallengkap[1] - 1;
		var tgl = new Date(tanggallengkap[2], tanggallengkap[1], tanggallengkap[0], 0, 0, 0, 0)
		var hari = tgl.getDay();
		var tanggal = tgl.getDate();
		var bulan = tgl.getMonth();
		var tahun = tgl.getFullYear();
		tanggallengkap = namahari[hari] + ", " + tanggal + " " + namabulan[bulan] + " " + tahun;
		document.getElementById("hari").value = namahari[hari];
	}

	function validasi_number() {
		jml = document.getElementById("jml").value;

		for (i = 1; i <= jml; i++) {
			y = formatulang(document.getElementById("qty" + i).value);

			if (!isNaN(y)) {

			} else {
				alert('Input harus numerik');
				document.getElementById("qty" + i).value = 0;
			}
		}
	}

	function hapuss(r) {
		document.getElementById("itemtem" + r).deleteRow(0);
		document.getElementById("jml").value = document.getElementById("jml").value - 1
	}

	function appdetail(a) {
		showModal("<?= $folder ?>/cform/detailreject/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
</script>