<h2><?= $page_title ?></h2>
<table class="maintable">
  <tr>
    <td align="left">
      <?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/receive', 'update' => '#pesan', 'type' => 'post')); ?>
      <div id="rrkhform">
        <div class="effect">
          <div class="accordion2">
            <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
              <tr>
                <td>No SJP</td>
                <td>
                  <input readonly id="isjp" name="isjp" value="<?= $isi->i_sjp ?>">
                  <input readonly id="id" name="id" type="hidden" value="<?= $isi->id ?>">
                </td>
              </tr>
              <tr>
                <td>Area</td>
                <td>
                  <input readonly id="eareaname" name="eareaname" value="<?= $isi->e_area_name ?>">
                  <input readonly id="iarea" name="iarea" type="hidden" value="<?= $isi->i_area ?>">
                </td>
              </tr>
              <tr>
                <td width="10%">Dari</td>
                <td>
                  <input id="esender" name="esender" value="<?= $isi->e_sender ?>" placeholder="Nama Penyerah" onkeyup="gede(this)" maxlength="125" readonly> /
                  <input style="width:200px" id="esendercompany" name="esendercompany" value="<?= $isi->e_sender_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125" readonly>
                </td>
              </tr>
              <tr>
                <td width="10%">Untuk</td>
                <td>
                  <input id="erecipient" name="erecipient" value="<?= $isi->e_recipient ?>" placeholder="Nama Penerima" onkeyup="gede(this)" maxlength="125" readonly> /
                  <input style="width:200px" id="erecipientcompany" name="erecipientcompany" value="<?= $isi->e_recipient_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125" readonly>
                </td>
              </tr>
              <tr>
                <td width="10%">Tgl SJP</td>
                <td>
                  <input readonly id="dsjp" name="dsjp" value="<?= $isi->d_sjp ?>">
                  <input readonly id="hari" name="hari" value="">
                </td>
              </tr>
              <tr>
                <td width="10%">Keterangan</td>
                <td>
                  <input id="eremarkhead" name="eremarkhead" value="<?= $isi->e_remark ?>" onkeyup="gede(this)" readonly>
                </td>
              </tr>
              <tr>
                <td width="10%">Tgl Terima</td>
                <td>
                  <input readonly id="dsjpreceive" name="dsjpreceive" type="text" value="<?php echo $isi->d_sjp_receive; ?>" onclick="showCalendar('',this,this,'','d_sjpreceive',0,20,1)">
                </td>
              </tr>
              <tr>
                <td width="10%">Keterangan Terima</td>
                <td>
                  <input id="ereceive" name="ereceive" value="" onkeyup="gede(this)">
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td width="100%">
                  <center>
                    <input name="receive" id="receive" value="Receive" type="submit" onclick="return dipales()">
                    <input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/index/','#main');">
                  </center>
                </td>
              </tr>
            </table>

            <div id="detailisi" align="center">
              <table id="itemtem" class="listtable" style="width:100%;">
                <tr>
                  <th style="width:20px;" align="center">No</th>
                  <th style="width:82px;" align="center">Kode Barang</th>
                  <th style="width:250px;" align="center">Nama Barang</th>
                  <th style="width:30px;" align="center">Jumlah</th>
                  <th style="width:50px;" align="center">Terima</th>
                  <th style="width:50px;" align="center">Satuan</th>
                  <th style="width:150px;" align="center">Keterangan</th>
                </tr>

                <?php if ($detail) {
                  $no = 0;
                  foreach ($detail as $riw) {
                    $no++;
                    echo "<tr>
											  <td  style=\"width:46px;text-align:center\">$no</td>
											  <td style=\"width:77px;\" hidden><input type=\"hidden\" readonly style=\"width:143px\" id=\"idproduct$no\" name=\"idproduct$no\" value=\"$riw->id\"></td>
											  <td style=\"width:77px;\"><input readonly style=\"width:143px\" id=\"iproduct$no\" name=\"iproduct$no\" value=\"$riw->i_product\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:240px;\"><input readonly style=\"width:434px\" id=\"eproductname$no\" name=\"eproductname$no\" value=\"$riw->e_product_name\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:47px;text-align:center;\"><input readonly style=\"width:85px\" id=\"qty$no\" name=\"qty$no\" value=\"$riw->n_quantity\" onkeyup=\"validasi_number();\"></td>
											  <td style=\"width:47px;text-align:center;\"><input style=\"width:85px\" id=\"qtyreceive$no\" name=\"qtyreceive$no\" value=\"0\" onkeyup=\"validasi_number();\"></td>
											  <td style=\"width:50px;text-align:center;\"><input readonly style=\"width:86px\" id=\"satuan$no\" name=\"satuan$no\" value=\"$riw->n_satuan\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:92px;\"><input readonly style=\"width:263px\" id=\"eremark$no\" name=\"eremark$no\" value=\"$riw->e_remark\" onkeyup=\"gede(this)\"></td>
											</tr>";
                  }
                } ?>
              </table>
            </div>
            <div id="pesan"></div>
            <input readonly type="hidden" name="jml" id="jml" value="<?= $jmlitem ?>">
          </div>
        </div>
      </div>
      <?= form_close() ?>
    </td>
  </tr>
</table>

<script language="javascript" type="text/javascript">
  $(document).ready(function() {
    carihari()

    /* DISABLED SUBMIT SAAT ENTER KEYBOARD */
    $(window).keydown(function(event) {
      if (event.keyCode == 13) {
        event.preventDefault();
        return false;
      }
    });
    /* *********************************** */
  });

  function dipales() {
    if (document.getElementById('dsjpreceive').value == '') {
      alert("Kolom Tgl Terima Masih Kosong!!");
      return false;
    } else {
      setTimeout(() => {
        $("input").attr("disabled", true);
        $("select").attr("disabled", true);
        $("#submit").attr("disabled", true);
        $("#button").attr("disabled", true);
        document.getElementById("cmdback").removeAttribute('disabled');
      }, 100);
    }
  }

  function afterSetDateValue(ref_field, target_field, date) {
    cektanggal();
  }

  function cektanggal() {
    dsjp = document.getElementById('dsjp').value;
    dsjpreceive = document.getElementById('dsjpreceive').value;

    dtmp = dsjp.split('-');
    thnsjp = dtmp[2];
    blnsjp = dtmp[1];
    hrsjp = dtmp[0];

    dtmp = dsjpreceive.split('-');
    thndsj = dtmp[2];
    blndsj = dtmp[1];
    hrdsj = dtmp[0];

    /* jmllibur = document.getElementById('libur').value;
    if (jmllibur == '') jmllibur = 0;
    jmllibur = parseFloat(jmllibur) + 6;
    jmllibur = jmllibur * -1;
    var tgl = new Date();
    var day = tgl.getDate();
    var month = tgl.getMonth() + 1;
    if (month < 10) month = '0' + month;
    var yy = tgl.getYear();
    var year = (yy < 1000) ? yy + 1900 : yy;
    tgl = day + "-" + month + "-" + year;
    var dmax = TambahHari(tgl, jmllibur);
    var dmax2 = TambahHari(tgl, jmllibur);
    if (dsjpreceive == '20-11-2018') {
      alert('Hari Libur !!!');
      document.getElementById('dsjpreceive').value = '';
    } else if (dsjpreceive == '01-06-2018') {
      alert('Tidak Ada Receive Pada Tanggal 01 Juni 2018  !!!');
      document.getElementById('dsjpreceive').value = '';
    } else */
    if (thnsjp > thndsj) {
      alert('Tahun Receive tidak boleh lebih kecil dari SJP !!!');
      document.getElementById('dsjpreceive').value = '';
    } else if (thnsjp == thndsj) {
      if (blnsjp > blndsj) {
        alert('Bulan Receive tidak boleh lebih kecil dari SJP !!!');
        document.getElementById('dsjpreceive').value = '';
      } else if (blnsjp == blndsj) {
        if (hrsjp > hrdsj) {
          alert('Tanggal Receive tidak boleh lebih kecil dari SJP !!!');
          document.getElementById('dsjpreceive').value = '';
        } else {
          // jmllibur = document.getElementById('libur').value;
          // if (jmllibur == '') jmllibur = 0;
          // jmllibur = parseFloat(jmllibur) + 6;
          // jmllibur = jmllibur * -1;
          // var ayena = '<?php echo date('Y/m/d H:m:s'); ?>';
          // var tgl = new Date(ayena);
          // var day = tgl.getDate();
          // var month = tgl.getMonth() + 1;
          // if (month < 10) month = '0' + month;
          // var yy = tgl.getYear();
          // var year = (yy < 1000) ? yy + 1900 : yy;
          // tgl = day + "-" + month + "-" + year;
          // var dmax = TambahHari(tgl, jmllibur);
          // var dmax2 = TambahHari(tgl, jmllibur);
          // dtmp = dmax.split('-');
          // thnmax = dtmp[2];
          // blnmax = dtmp[1];
          // hrmax = dtmp[0];
          // dtmp2 = dmax2.split('-');
          // thnmax2 = dtmp2[2];
          // blnmax2 = dtmp2[1];
          // hrmax2 = dtmp2[0];
          // if (hrmax < 10) hrmax = '0' + hrmax;
          // if (blnmax < 10) blnmax = '0' + blnmax;
          // if (thnmax > thndsj) {
          //   alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!w');
          //   document.getElementById('dsjpreceive').value = '';
          // } else if (thnmax >= thndsj) {
          //   if (blnmax > blndsj) {
          //     alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!s');
          //     document.getElementById('dsjpreceive').value = '';
          //   } else if (blnmax == blndsj) {
          //     if (hrmax > hrdsj) {
          //       alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!x');
          //       document.getElementById('dsjpreceive').value = '';
          //     } else if ((blndsj == 12) && (hrdsj > 31)) {
          //       alert('Tidak Ada Receive Dari Tanggal 30-31 Desember !!!');
          //       document.getElementById('dsjpreceive').value = '';
          //     } //else if(hrmax==hrdsj){
          //	  alert('hari ini hari libur !!!x');
          //    document.getElementById('dsjpreceive').value='';
          // }
          // }
          // }
        }
      } else {
        /**/
        // jmllibur = document.getElementById('libur').value;
        // if (jmllibur == '') jmllibur = 0;
        // jmllibur = parseFloat(jmllibur) + 6;
        // jmllibur = jmllibur * -1;
        // var ayena = '<?php echo date('Y/m/d H:m:s'); ?>';
        // var tgl = new Date(ayena);
        // var day = tgl.getDate();
        // var month = tgl.getMonth() + 1;
        // if (month < 10) month = '0' + month;
        // var yy = tgl.getYear();
        // var year = (yy < 1000) ? yy + 1900 : yy;
        // tgl = day + "-" + month + "-" + year;
        // var dmax = TambahHari(tgl, jmllibur);
        // var dmax2 = TambahHari(tgl, jmllibur);
        // dtmp = dmax.split('-');
        // thnmax = dtmp[2];
        // blnmax = dtmp[1];
        // hrmax = dtmp[0];
        // dtmp2 = dmax2.split('-');
        // thnmax2 = dtmp2[2];
        // blnmax2 = dtmp2[1];
        // hrmax2 = dtmp2[0];
        // if (hrmax < 10) hrmax = '0' + hrmax;
        // if (blnmax < 10) blnmax = '0' + blnmax;
        // if (thnmax > thndsj) {
        //   alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!w');
        //   document.getElementById('dsjpreceive').value = '';
        // } else if (thnmax <= thndsj) {
        //   if (blnmax > blndsj) {
        //     alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!s');
        //     document.getElementById('dsjpreceive').value = '';
        //   } else if (blnmax == blndsj) {
        //     if (hrmax > hrdsj) {
        //       alert('Tanggal Receive tidak boleh lebih dari 7 hari kerja sebelum hari ini !!!x');
        //       document.getElementById('dsjpreceive').value = '';
        //     } else if ((blndsj == 12) && (hrdsj > 31)) {
        //       alert('Tidak Ada Receive Dari Tanggal 30-31 Desember !!!');
        //       document.getElementById('dsjpreceive').value = '';
        //     } //else if(hrmax==hrdsj){
        //	  alert('hari ini hari libur !!!x');
        //    document.getElementById('dsjpreceive').value='';
        // }
        // }
        // }
        /**/
      }
    }
  }

  function carihari() {
    var tanggallengkap = document.getElementById("dsjp").value;
    var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
    namahari = namahari.split(" ");
    var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
    namabulan = namabulan.split(" ");
    tanggallengkap = tanggallengkap.split("-");
    tanggallengkap[1] = tanggallengkap[1] - 1;
    var tgl = new Date(tanggallengkap[2], tanggallengkap[1], tanggallengkap[0], 0, 0, 0, 0)
    var hari = tgl.getDay();
    var tanggal = tgl.getDate();
    var bulan = tgl.getMonth();
    var tahun = tgl.getFullYear();
    tanggallengkap = namahari[hari] + ", " + tanggal + " " + namabulan[bulan] + " " + tahun;
    document.getElementById("hari").value = namahari[hari];
  }

  function validasi_number() {
    jml = document.getElementById("jml").value;

    for (i = 1; i <= jml; i++) {
      x = formatulang(document.getElementById("qty" + i).value);
      y = formatulang(document.getElementById("qtyreceive" + i).value);

      if (!isNaN(y)) {} else {
        alert('Input harus numerik');
        document.getElementById("qtyreceive" + i).value = 0;
      }

      if (y > x) {
        alert('Jml Terima tidak bisa melebihi jml kirim');
        document.getElementById("qtyreceive" + i).value = 0;
      }
    }
  }

  function hapuss(r) {
    document.getElementById("itemtem" + r).deleteRow(0);
    document.getElementById("jml").value = document.getElementById("jml").value - 1
  }

  function appdetail(a) {
    showModal("<?= $folder ?>/cform/detailreject/" + a + "/", "#light");
    jsDlgShow("#konten *", "#fade", "#light");
  }
</script>