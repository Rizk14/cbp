<?php
include("php/fungsi.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
  <title>Page Preview SJP Umum</title>
</head>

<body>
  <style type="text/css" media="all">
    /*
@page land {size: landscape;}
*/
    * {
      size: landscape;
    }

    .huruf {
      FONT-FAMILY: Tahoma, Verdana, Arial, Helvetica, sans-serif;
    }

    .miring {
      font-style: italic;

    }

    .ceKotak {
      - background-color: #f0f0f0;
      border-bottom: #000000 1px solid;
      border-top: #000000 1px solid;
      border-left: #000000 1px solid;
      border-right: #000000 1px solid;
    }

    .garis {
      background-color: #000000;
      width: 100%;
      height: 50%;
      font-size: 100px;
      border-style: solid;
      border-width: 0.01px;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garis td {
      background-color: #FFFFFF;
      border-style: solid;
      border-width: 0.01px;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .garisx {
      background-color: #000000;
      width: 100%;
      height: 50%;
      border-style: none;
      border-collapse: collapse;
      spacing: 1px;
    }

    .garisx td {
      background-color: #FFFFFF;
      border-style: none;
      font-size: 10px;
      FONT-WEIGHT: normal;
      padding: 1px;
    }

    .judul {
      font-size: 18px;
      FONT-WEIGHT: normal;
    }

    .nmper {
      font-size: 18px;
      FONT-WEIGHT: normal;
    }

    .isi {
      font-size: 14px;
      font-weight: normal;
      padding: 1px;
    }

    .eusinya {
      font-size: 12px;
      font-weight: normal;
    }

    .ici {
      font-size: 12px;
      font-weight: normal;
    }

    .garisbawah {
      border-top: #000000 0.1px solid;
    }
  </style>
  <style type="text/css" media="print">
    .noDisplay {
      display: none;
    }

    .pagebreak {
      page-break-before: always;
    }
  </style>

  <table style="border-collapse: collapse;" width="100%" border="1">
    <tr>
      <td width='20%'>
        <center>
          <img src="<?php echo base_url() . 'img/logo2.png'; ?>" border="0">
        </center>
      </td>
      <td colspan="3" style="text-align:center" class="judul"><strong>FORMULIR </br> Surat Jalan Pengantar</strong></td>
    </tr>
    <tr>
      <td style="text-align:center">Nomor Dokumen: </br> Form/QAS/034/01</td>
      <td style="text-align:center">Revisi </br> 00</td>
      <td style="text-align:center">Tanggal Efektif </br> -</td>
      <td style="text-align:center">Halaman </br> 1 dari 1</td>
    </tr>
  </table>

  <table style="border-collapse: collapse;" width="100%" border="0" class="isi">
    <tr>
      <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td style="text-align:center;"></br>No. Urut: <?= $isi->i_sjp ?></td>
    </tr>
    <tr>
      <td>Dari</td>
      <td>:</td>
      <td><?= $isi->e_sender ?> / <?= $isi->e_sender_company ?></td>
      <td></td>
    </tr>
    <tr>
      <td>Untuk</td>
      <td>:</td>
      <td><?= $isi->e_recipient ?> / <?= $isi->e_recipient_company ?></td>
      <td></td>
    </tr>
    <tr>
      <td>Tanggal</td>
      <td>:</td>
      <td><?= $isi->d_sjp ?></td>
      <td></td>
    </tr>
  </table>

  </br>
  Bersama ini dikirimkan barang dengan spesifikasi sebagai berikut :
  <table style="border-collapse: collapse;" width="100%" border="1" class="isi">
    <thead>
      <th>No</th>
      <th>Kode Barang</th>
      <th>Nama Barang</th>
      <th>Jumlah</th>
      <th>Satuan</th>
      <th>Keterangan</th>
    </thead>
    <?php
    $no = 0;
    foreach ($detail as $riw) {
      $no++;
      echo "<tr>
                <td style=\"text-align:center\">$no</td>
                <td>$riw->i_product</td>
                <td>$riw->e_product_name</td>
                <td style=\"text-align:center\">$riw->n_quantity</td>
                <td style=\"text-align:center\">$riw->n_satuan</td>
                <td>$riw->e_remark</td>
              </tr>";
    }

    // if ($no < 14) {
    //   $jmlbaris = 15 - $no;
    ?>
    <!-- <table width="100%" border="1" style="border-collapse: collapse">
        <tr align="center">
          <#?php
          for ($jml = $no + 1; $jml <= 15; $jml++) {
            echo "<tr> 
                    <td style=\"text-align:center\">$jml</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style=\"text-align:center\">&nbsp;</td>
                    <td style=\"text-align:center\">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>";
          }
          ?>
        </tr>
      </table> -->
    <!-- <#?php } ?> -->
  </table>
  Mohon diperiksa kembali sebelum dilakukan penerimaan barang
  </br>
  </br>
  <table align="center">
    <tr>
      <td class="ceKotak" style="text-align:center">
        Dibuat oleh, </br></br></br></br></br>
        (Penyerah Barang) </br>
        (<?= $isi->e_sender ?>)
      </td>
      <td style="text-align:center;"></td>
      <td class="ceKotak" style="text-align:center">
        Diketahui oleh, </br></br></br></br></br>
        &nbsp;(Atasan Penyerah Barang)&nbsp; </br>
        ( <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
        <?= strtoupper($isi->e_user_name) ?>
        )
      </td>
      <td style="text-align:center;"></td>
      <td class="ceKotak" style="text-align:center">
        Diterima oleh, </br></br></br></br></br>
        (Penerima Barang) </br>
        ( &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        )
      </td>
    </tr>
  </table></br>

  <div class="noDisplay">
    <center>
      <b>
        <button onclick="window.print()">Print</button>
      </b>
    </center>
  </div>

  <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-1.3.2.js"></script>
  <script type="text/javascript">
    // console.log("Masuk")
    // window.onafterprint = function() {
    //   $.ajax({
    //     type: "POST",
    //     url: "<#?php echo site_url($folder . '/cform/updatecetak'); ?>",
    //     data: {
    //       'id': '<#?= $isi->id ?>',
    //       'isjp': '<#?= $isi->id ?>'
    //     },
    //     success: function(data) {
    //       opener.window.refreshview();
    //       setTimeout(window.close, 0);
    //     },
    //     error: function(XMLHttpRequest) {
    //       alert(XMLHttpRequest.responseText);
    //     }
    //   });
    // }

    window.onafterprint = function() {
      var id = '<?php echo $isi->id ?>';
      var isjp = '<?php echo $isi->i_sjp ?>';

      $.ajax({
        type: "POST",
        url: "<?php echo site_url($folder . '/cform/updatecetak'); ?>",
        data: {
          'id': id,
          'isjp': isjp
        },
        success: function(data) {
          opener.window.refreshview();
          setTimeout(window.close, 0);
        },
        error: function(XMLHttpRequest) {
          alert('fail');
        }
      });
    }
  </script>