<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title><?php echo NmPerusahaan; ?> : <?php echo $page_title; ?></title>
</head>

<body>
  <div id="main">
    <div id="tmpx">
      <h2>Mengapa SJP </br> No <?= $isjp ?> ini direject?</h2>
      <table class="maintable">
        <tr>
          <td align="left">
            <?php $is_cari = 1; ?>
            <div id="aktkkform">
              <div class="effect">
                <div class="accordion2">
                  <center>
                    <input type="hidden" id="isjp" name="isjp" value="<?php echo $isjp ?>" readonly>
                    <input type="hidden" id="id" name="id" value="<?php echo $id ?>" readonly>

                    <!-- <select name="eremark1" id="eremark1">
                      <option value="">-</option>
                      <option value="Masih Ada Piutang">Masih Ada Piutang</option>
                      <option value="Data Toko Tidak Lengkap">Data Toko Tidak Lengkap</option>
                      <option value="Salah Input SPB">Salah Input SPB</option>
                      <option value="Error System">Error System</option>
                      <option value="SPB Double">SPB Double</option>
                    </select> -->

                    <input type="text" name="eremark2" id="eremark2" value="" onkeyup="remark(this.value)" placeholder="Alasan..">

                    <br><br>
                    <input name="login" id="login" value="Update" type="submit" onclick="return dipales();">&nbsp;&nbsp;
                    <input type="button" id="batal" name="batal" value="Tutup" onclick="bbatal()">
                  </center>
                  <?= form_close() ?>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>
<script language="javascript" type="text/javascript">
  function remark(a) {
    console.log(a)
    document.getElementById('eremark2').value = a;
  }

  function dipales() {
    var isjp = document.getElementById('isjp').value;
    var id = document.getElementById('id').value;
    var eremark2 = document.getElementById('eremark2').value;
    // console.log(eremark2)
    // var eremark1 = document.getElementById('eremark1').value;
    // var eremark2 = document.getElementById('eremark2').value;

    // if ( /* (eremark1 == '') && */ (eremark2 == '')) {
    //   alert('Silahkan Isi Kolom Alasan!!!');
    //   return false
    // } else {
    $.ajax({
      type: "POST",
      url: "<?php echo site_url($folder . '/cform/reject'); ?>",
      data: {
        id: id,
        isjp: isjp,
        eremark: eremark2,
      },
      success: function(data) {
        bbatal()
        refreshview()
      },

      error: function(XMLHttpRequest) {
        alert(XMLHttpRequest.responseText);
      }

    })
    // }
  }

  function bbatal() {
    jsDlgHide("#konten *", "#fade", "#light");
  }

  function refreshview() {
    show('<?= $folder ?>/cform/index/', '#main');
  }
</script>