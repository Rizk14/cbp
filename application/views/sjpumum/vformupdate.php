<h2><?= $page_title ?></h2>
<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/update', 'update' => '#pesan', 'type' => 'post')); ?>
			<div id="rrkhform">
				<div class="effect">
					<div class="accordion2">
						<table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
							<tr>
								<td>No SJP</td>
								<td>
									<input readonly id="isjp" name="isjp" value="<?= $isi->i_sjp ?>">
									<input readonly id="id" name="id" type="hidden" value="<?= $isi->id ?>">
								</td>
							</tr>
							<tr>
								<td>Area</td>
								<td>
									<input readonly id="eareaname" name="eareaname" value="<?= $isi->e_area_name ?>">
									<input readonly id="iarea" name="iarea" type="hidden" value="<?= $isi->i_area ?>">
								</td>
							</tr>
							<tr>
								<td width="10%">Dari</td>
								<td>
									<input id="esender" name="esender" value="<?= $isi->e_sender ?>" placeholder="Nama Penyerah" onkeyup="gede(this)" maxlength="125"> /
									<input style="width:200px" id="esendercompany" name="esendercompany" value="<?= $isi->e_sender_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125">
								</td>
							</tr>
							<tr>
								<td width="10%">Untuk</td>
								<td>
									<input id="erecipient" name="erecipient" value="<?= $isi->e_recipient ?>" placeholder="Nama Penerima" onkeyup="gede(this)" maxlength="125"> /
									<input style="width:200px" id="erecipientcompany" name="erecipientcompany" value="<?= $isi->e_recipient_company ?>" placeholder="Nama Perusahaan/Departemen" onkeyup="gede(this)" maxlength="125">
								</td>
							</tr>
							<tr>
								<td width="10%">Tgl SJP</td>
								<td>
									<input readonly id="dsjp" name="dsjp" onclick="showCalendar('',this,this,'','dsjp',0,20,1)" value="<?= $isi->d_sjp ?>">
									<input readonly id="hari" name="hari" value="">
								</td>
							</tr>
							<tr>
								<td width="10%">Keterangan</td>
								<td>
									<input id="eremarkhead" name="eremarkhead" value="<?= $isi->e_remark ?>" onkeyup="gede(this)">
								</td>
							</tr>

							<tr>
								<td>&nbsp;</td>
								<td width="100%">
									<center>
										<?php if ($isi->d_approve == "" /* && $isi->d_notapprove == "" */) { ?>
											<input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button" onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);">
											<input name="login" id="login" value="Update" type="submit" onclick="return dipales()">
										<?php } ?>
										<input name="cmdback" id="cmdback" value="Kembali" type="button" onclick="show('<?= $folder ?>/cform/index/','#main');">
									</center>
								</td>
							</tr>
						</table>

						<div id="detailisi" align="center">
							<table id="itemtem" class="listtable" style="width:100%;">
								<tr>
									<th style="width:20px;" align="center">No</th>
									<th style="width:82px;" align="center">Kode Barang</th>
									<th style="width:250px;" align="center">Nama Barang</th>
									<th style="width:30px;" align="center">Jumlah</th>
									<th style="width:50px;" align="center">Satuan</th>
									<th style="width:150px;" align="center">Keterangan</th>
									<th style="width:30px;" align="center" class="action">Act</th>
								</tr>

								<?php if ($detail) {
									$no = 0;
									foreach ($detail as $riw) {
										$no++;
										echo "<tr>
											  <td  style=\"width:46px;text-align:center\">$no</td>
											  <td style=\"width:77px;\"><input style=\"width:143px\" id=\"iproduct$no\" name=\"iproduct$no\" value=\"$riw->i_product\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:240px;\"><input style=\"width:434px\" id=\"eproductname$no\" name=\"eproductname$no\" value=\"$riw->e_product_name\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:47px;text-align:center;\"><input style=\"width:85px\" id=\"qty$no\" name=\"qty$no\" value=\"$riw->n_quantity\" onkeyup=\"validasi_number();\"></td>
											  <td style=\"width:50px;text-align:center;\"><input style=\"width:86px\" id=\"satuan$no\" name=\"satuan$no\" value=\"$riw->n_satuan\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:92px;\"><input style=\"width:263px\" id=\"eremark$no\" name=\"eremark$no\" value=\"$riw->e_remark\" onkeyup=\"gede(this)\"></td>
											  <td style=\"width:68px;\" class=\"action\"></td>
											</tr>";
									}
								} ?>
							</table>
						</div>
						<div id="pesan"></div>
						<input type="hidden" name="jml" id="jml" value="<?= $jmlitem ?>">
						<font style="color: red;font-size:14px">* Jumlah 0 = Hapus Item</font>
					</div>
				</div>
			</div>
			<?= form_close() ?>
		</td>
	</tr>
</table>


<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		carihari()
	});

	function dipales() {
		if (document.getElementById('erecipient').value == '') {
			alert("Kolom Penerima Masih Kosong!!");
			return false;
		} else if (document.getElementById('erecipientcompany').value == '') {
			alert("Kolom Perusahaan Penerima Masih Kosong!!");
			return false;
		} else {
			setTimeout(() => {
				$("input").attr("disabled", true);
				$("select").attr("disabled", true);
				$("#submit").attr("disabled", true);
				document.getElementById("cmdback").removeAttribute('disabled');
			}, 100);
		}
	}


	function afterSetDateValue(ref_field, target_field, date) {

		if (date != "") {
			var startDate = document.getElementById('dsjp').value;
			tes = startDate.split('-');
			startDate = tes[2] + '-' + tes[1] + '-' + tes[0];
			var today = new Date();
			var daten = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();
			var curr_date = today.getDate();
			var curr_month = (today.getMonth() + 1);
			var curr_year = today.getFullYear();

			if (curr_month < 10) curr_month = '0' + curr_month;
			if (curr_date < 10) curr_date = '0' + curr_date;
			current = curr_year + "-" + curr_month + "-" + curr_date;
			currentx = curr_date + "-" + curr_month + "-" + curr_year;
			console.log(startDate)
			console.log(current)
			if (startDate < current) {
				alert("Tanggal tidak boleh lebih kecil dari hari ini ( " + currentx + " )");
				document.getElementById('dsjp').value = currentx;
			}

			carihari()
		}
	}

	function carihari() {
		var tanggallengkap = document.getElementById("dsjp").value;
		var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
		namahari = namahari.split(" ");
		var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
		namabulan = namabulan.split(" ");
		tanggallengkap = tanggallengkap.split("-");
		tanggallengkap[1] = tanggallengkap[1] - 1;
		var tgl = new Date(tanggallengkap[2], tanggallengkap[1], tanggallengkap[0], 0, 0, 0, 0)
		var hari = tgl.getDay();
		var tanggal = tgl.getDate();
		var bulan = tgl.getMonth();
		var tahun = tgl.getFullYear();
		tanggallengkap = namahari[hari] + ", " + tanggal + " " + namabulan[bulan] + " " + tahun;
		document.getElementById("hari").value = namahari[hari];
	}

	function tambah_item(a) {
		host = window.location.origin + '/dgu/';

		if (a < 16) {
			si_inner = document.getElementById("detailisi").innerHTML;
			if (si_inner == '') {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = '<table id="itemtem' + a + '" class="listtable">';
				si_inner = si_inner + '<tbody><tr><td style="width:46px;text-align:center">' + juml + '</td>';
				si_inner = si_inner + '<td style="width:77px;"><input style="width:143px" type="text" id="iproduct' + a + '" name="iproduct' + a + '" onkeyup="gede(this)"></td>';
				si_inner = si_inner + '<td style="width:240px;"><input style="width:434px" type="text" id="eproductname' + a + '" name="eproductname' + a + '" onkeyup="gede(this)"></td>';
				si_inner = si_inner + '<td style="width:47px;"><input style="width:85px" type="text" id="qty' + a + '" name="qty' + a + '" onkeyup=\"validasi_number();\"></td>';
				si_inner = si_inner + '<td style="width:50px;"><input style="width:86px" type="text" id="satuan' + a + '" name="satuan' + a + '" onkeyup="gede(this)"></td>';
				si_inner = si_inner + '<td style="width:92px;"><input style="width:263px" type="text" id="eremark' + a + '" name="eremark' + a + '" onkeyup="gede(this)"></td>';
				si_inner = si_inner + '<td style="width:68px;" class="action"><a href="#" onclick=\'hapuss("' + a + '")\'><img height=15px; style="cursor:hand;" src=' + host + 'img/delete.png border="0" alt="delete"></a></td></tr></tbody></table>';
			} else {
				document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
				juml = document.getElementById("jml").value;
				si_inner = si_inner + '<table id="itemtem' + a + '" class="listtable">';
				si_inner += '<tbody><tr><td style="width:46px;text-align:center">' + juml + '</td>';
				si_inner += '<td style="width:77px;"><input style="width:143px" type="text" id="iproduct' + a + '" name="iproduct' + a + '" onkeyup="gede(this)"></td>';
				si_inner += '<td style="width:240px;"><input style="width:434px" type="text" id="eproductname' + a + '" name="eproductname' + a + '" onkeyup="gede(this)"></td>';
				si_inner += '<td style="width:47px;"><input style="width:85px" type="text" id="qty' + a + '" name="qty' + a + '" onkeyup=\"validasi_number();\"></td>';
				si_inner += '<td style="width:50px;"><input style="width:86px" type="text" id="satuan' + a + '" name="satuan' + a + '" onkeyup="gede(this)"></td>';
				si_inner += '<td style="width:92px;"><input style="width:263px" type="text" id="eremark' + a + '" name="eremark' + a + '" onkeyup="gede(this)"></td>';
				si_inner += '<td style="width:68px;" class="action"><a href="#" onclick=\'hapuss("' + a + '")\'><img height=15px; style="cursor:hand;" src=' + host + 'img/delete.png border="0" alt="delete"></a></td></tr></tbody></table>';
			}

			j = 0;
			var iproduct = Array();
			var eproductname = Array();
			var qty = Array();
			var satuan = Array();
			var eremark = Array();

			for (i = 1; i < a; i++) {
				j++;
				iproduct[j] = document.getElementById("iproduct" + i).value;
				eproductname[j] = document.getElementById("eproductname" + i).value;
				qty[j] = document.getElementById("qty" + i).value;
				satuan[j] = document.getElementById("satuan" + i).value;
				eremark[j] = document.getElementById("eremark" + i).value;
			}
			document.getElementById("detailisi").innerHTML = si_inner;
			j = 0;
			for (i = 1; i < a; i++) {
				j++;
				document.getElementById("iproduct" + i).value = iproduct[j];
				document.getElementById("eproductname" + i).value = eproductname[j];
				document.getElementById("qty" + i).value = qty[j];
				document.getElementById("satuan" + i).value = satuan[j];
				document.getElementById("eremark" + i).value = eremark[j];
			}
		} else {
			alert('Maksimum 15 item');
		}
	}

	function validasi_number() {
		jml = document.getElementById("jml").value;

		for (i = 1; i <= jml; i++) {
			y = formatulang(document.getElementById("qty" + i).value);

			if (!isNaN(y)) {

			} else {
				alert('Input harus numerik');
				document.getElementById("qty" + i).value = 0;
			}
		}
	}

	/* function tambah_item(a) {
	  if (a <= 30) {
	    so_inner = document.getElementById("detailheader").innerHTML;
	    si_inner = document.getElementById("detailisi").innerHTML;
	    if (so_inner == '') {
	      so_inner = '<table id="itemtem" class="tabledgu" style="width:920px;">';
	      so_inner += '<thead><tr><th style="width:20px;"  align="center">No</th>';
	      so_inner += '<th style="width:50px;" align="center">Bukti</th>';
	      so_inner += '<th style="width:50px;" align="center">Lang</th>';
	      so_inner += '<th style="width:250px;" align="center">Nama</th>';
	      so_inner += '<th style="width:50px;"  align="center">Wkt</th>';
	      so_inner += '<th style="width:150px;"  align="center">Area Kota</th>';
	      so_inner += '<th style="width:100px;"  align="center">Rencana</th>';
	      so_inner += '<th style="width:40px;"  align="center">Real</th>';
	      so_inner += '<th style="width:200px;"  align="center">Keterangan</th>';
	      so_inner += '<th style="width:30px;"  align="center" class="action">Act</th></tr></thead></table>';
	      document.getElementById("detailheader").innerHTML = so_inner;
	    } else {
	      so_inner = '';
	    }
	    if (si_inner == '') {
	      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
	      juml = document.getElementById("jml").value;
	      si_inner = '<table><tbody><tr><td style="width:19px;"><input readonly style="width:19px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
	      si_inner += '<td align="center" style="width:50px;"><input type="checkbox" id="fkunjunganvalid' + a + '" name="fkunjunganvalid' + a + '" value="" onclick="cek_valid(' + a + ')"></td>';
	      si_inner += '<td style="width:48px;"><input style="width:48px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
	      si_inner += '<td style="width:227px;"><input readonly style="width:227px;"  type="text" id="ecustomername' + a + '" name="ecustomername' + a + '" value=""></td>';
	      si_inner += '<td style="width:47px;"><input style="width:47px;"  type="text" id="waktu' + a + '" name="waktu' + a + '" value=""></td>';
	      si_inner += '<td style="width:137px;"><input type="hidden" id="icity' + a + '" name="icity' + a + '" value=""><input readonly style="width:137px;" type="text" id="ecityname' + a + '" name="ecityname' + a + '" value="" onclick="view_city(' + a + ');"></td>';
	      si_inner += '<td style="width:97px;"><input type="hidden" id="ikunjungantype' + a + '" name="ikunjungantype' + a + '" value=""><input readonly style="width:97px;" type="text" id="ekunjungantypename' + a + '" name="ekunjungantypename' + a + '" value="" onclick="view_kunjungan(' + a + ');"></td>';
	      si_inner += '<td align="center" style="width:39px;"><input type="checkbox" id="fkunjunganrealisasi' + a + '" name="fkunjunganrealisasi' + a + '" value="" onclick="cek_realisasi(' + a + ')"></td>';
	      si_inner += '<td style="width:187px;"><input style="width:187px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
	      si_inner += '<td style="width:31px;">&nbsp;</td></tr></tbody></table>';
	    } else {
	      document.getElementById("jml").value = parseFloat(document.getElementById("jml").value) + 1;
	      juml = document.getElementById("jml").value;
	      si_inner = si_inner + '<table><tbody><tr><td style="width:19px;"><input readonly style="width:19px;" readonly type="text" id="baris' + a + '" name="baris' + a + '" value="' + a + '"></td>';
	      si_inner += '<td align="center" style="width:50px;"><input type="checkbox" id="fkunjunganvalid' + a + '" name="fkunjunganvalid' + a + '" value="" onclick="cek_valid(' + a + ')"></td>';
	      si_inner += '<td style="width:48px;"><input style="width:48px;" readonly type="text" id="icustomer' + a + '" name="icustomer' + a + '" value=""></td>';
	      si_inner += '<td style="width:227px;"><input readonly style="width:227px;"  type="text" id="ecustomername' + a + '" name="ecustomername' + a + '" value=""></td>';
	      si_inner += '<td style="width:47px;"><input readonly style="width:47px;"  type="text" id="waktu' + a + '" name="waktu' + a + '" value=""></td>';
	      si_inner += '<td style="width:137px;"><input type="hidden" id="icity' + a + '" name="icity' + a + '" value=""><input readonly style="width:137px;" type="text" id="ecityname' + a + '" name="ecityname' + a + '" value="" onclick="view_city(' + a + ');"></td>';
	      si_inner += '<td style="width:97px;"><input type="hidden" id="ikunjungantype' + a + '" name="ikunjungantype' + a + '" value=""><input readonly style="width:97px;" type="text" id="ekunjungantypename' + a + '" name="ekunjungantypename' + a + '" value="" onclick="view_kunjungan(' + a + ');"></td>';
	      si_inner += '<td align="center" style="width:39px;"><input type="checkbox" id="fkunjunganrealisasi' + a + '" name="fkunjunganrealisasi' + a + '" value="" onclick="cek_realisasi(' + a + ')"></td>';
	      si_inner += '<td style="width:187px;"><input style="width:187px;" type="text" id="eremark' + a + '" name="eremark' + a + '" value=""></td>';
	      si_inner += '<td style="width:31px;">&nbsp;</td></tr></tbody></table>';
	    }
	    j = 0;
	    var baris = Array();
	    var fkunjunganvalid = Array();
	    var fkunjunganvalid2 = Array();
	    var icustomer = Array();
	    var ecustomername = Array();
	    var waktu = Array();
	    var icity = Array();
	    var ecityname = Array();
	    var ikunjungantype = Array();
	    var ekunjungantypename = Array();
	    var fkunjunganrealisasi = Array();
	    var fkunjunganrealisasi2 = Array();
	    var eremark = Array();
	    for (i = 1; i < a; i++) {
	      j++;
	      baris[j] = document.getElementById("baris" + i).value;
	      fkunjunganvalid[j] = document.getElementById("fkunjunganvalid" + i).value;
	      fkunjunganvalid2[j] = document.getElementById("fkunjunganvalid" + i).checked;
	      icustomer[j] = document.getElementById("icustomer" + i).value;
	      ecustomername[j] = document.getElementById("ecustomername" + i).value;
	      waktu[j] = document.getElementById("waktu" + i).value;
	      icity[j] = document.getElementById("icity" + i).value;
	      ecityname[j] = document.getElementById("ecityname" + i).value;
	      ikunjungantype[j] = document.getElementById("ikunjungantype" + i).value;
	      ekunjungantypename[j] = document.getElementById("ekunjungantypename" + i).value;
	      fkunjunganrealisasi[j] = document.getElementById("fkunjunganrealisasi" + i).value;
	      fkunjunganrealisasi2[j] = document.getElementById("fkunjunganrealisasi" + i).checked;
	      eremark[j] = document.getElementById("eremark" + i).value;
	    }
	    document.getElementById("detailisi").innerHTML = si_inner;
	    j = 0;
	    for (i = 1; i < a; i++) {
	      j++;
	      document.getElementById("baris" + i).value = baris[j];
	      document.getElementById("fkunjunganvalid" + i).value = fkunjunganvalid[j];
	      document.getElementById("fkunjunganvalid" + i).checked = fkunjunganvalid2[j];
	      document.getElementById("icustomer" + i).value = icustomer[j];
	      document.getElementById("ecustomername" + i).value = ecustomername[j];
	      document.getElementById("waktu" + i).value = waktu[j];
	      document.getElementById("icity" + i).value = icity[j];
	      document.getElementById("ecityname" + i).value = ecityname[j];
	      document.getElementById("ikunjungantype" + i).value = ikunjungantype[j];
	      document.getElementById("ekunjungantypename" + i).value = ekunjungantypename[j];
	      document.getElementById("fkunjunganrealisasi" + i).value = fkunjunganrealisasi[j];
	      document.getElementById("fkunjunganrealisasi" + i).checked = fkunjunganrealisasi2[j];
	      document.getElementById("eremark" + i).value = eremark[j];
	    }
	    area = document.getElementById("iarea").value;
	    showModal("rrkh/cform/customer/" + a + "/" + area + "/", "#light");
	    jsDlgShow("#konten *", "#fade", "#light");
	  } else {
	    alert("Maksimal 30 item");
	  }
	} */

	// function dipales(a) {
	//   cek = 'false';
	//   if ((document.getElementById("dsjp").value != '') &&
	//     (document.getElementById("iarea").value != '')) {
	//     if (a == 0) {
	//       alert('Isi data item minimal 1 !!!');
	//     } else {
	//       for (i = 1; i <= a; i++) {
	//         if ((document.getElementById("icustomer" + i).value == '') ||
	//           (document.getElementById("ikunjungantype" + i).value == '') ||
	//           (document.getElementById("icity" + i).value == '')) {
	//           alert('Data item masih ada yang salah !!!');
	//           exit();
	//           cek = 'false';
	//         } else {
	//           cek = 'true';
	//         }
	//       }
	//     }
	//     if (cek == 'true') {
	//       document.getElementById("login").hidden = true;
	//       document.getElementById("cmdtambahitem").hidden = true;
	//       document.getElementById("cmdcopy").hidden = true;
	//     } else {
	//       document.getElementById("login").hidden = false;
	//     }
	//   } else {
	//     alert('Data header masih ada yang salah !!!');
	//   }
	// }

	function clearitem() {
		document.getElementById("detailisi").innerHTML = '';
		document.getElementById("pesan").innerHTML = '';
		document.getElementById("jml").value = '0';
		document.getElementById("login").hidden = false;
	}

	function view_city(a) {
		$area = document.getElementById("iarea").value;
		showModal("rrkh/cform/city/" + a + "/" + area + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_kunjungan(a) {
		showModal("rrkh/cform/kunjungan/" + a + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function view_salesman() {
		area = document.getElementById("iarea").value;
		showModal("rrkh/cform/salesman/" + area + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}

	function cek_valid(a) {
		valid = document.getElementById("fkunjunganvalid" + a).value;
		if (valid == 'on')
			document.getElementById("fkunjunganvalid" + a).value = '';
		else
			document.getElementById("fkunjunganvalid" + a).value = 'on';
	}

	function cek_realisasi(a) {
		valid = document.getElementById("fkunjunganrealisasi" + a).value;
		if (valid == 'on')
			document.getElementById("fkunjunganrealisasi" + a).value = '';
		else
			document.getElementById("fkunjunganrealisasi" + a).value = 'on';
	}

	function hapuss(r) {
		document.getElementById("itemtem" + r).deleteRow(0);
		document.getElementById("jml").value = document.getElementById("jml").value - 1
	}
</script>