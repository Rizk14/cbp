<style>
	.container {
		position: relative;
		overflow: scroll;
		height: 500px;
	}
</style>

<div id='tmpx'>
	<h2><?php echo $page_title; ?></h2>
	<table class="maintable">
		<tr>
			<td align="left">
				<?php echo $this->pquery->form_remote_tag(array('url' => $folder . '/cform/index/', 'update' => '#main', 'type' => 'post')); ?>
				<div class="effect">
					<div class="accordion2">

						<table border=0>
							<tr>
								<?php
								for ($x = 0; $x <= 267; $x++) {
									echo "<td>&nbsp;</td>";
								}
								?>
								<td>
									<button type="button" value="Tambah Data" onclick="show('<?= $folder ?>/cform/tambah/','#main');">Tambah Data</button>
								</td>
							</tr>
						</table>

						<div class="container">
							<table class="listtablex">
								<thead>
									<tr>
										<td colspan="10" align="center">Cari data :
											<input type="text" id="cari" name="cari" value="<?= $cari ?>">
											<input type="submit" id="bcari" name="bcari" value="Cari">
										</td>
									</tr>
								</thead>
								<th>No SJP</th>
								<th>Area</th>
								<th>Tanggal SJP</th>
								<th>Tanggal Terima</th>
								<th>Pengirim</th>
								<th>Tujuan</th>
								<th>Keterangan</th>
								<th>Status SJP</th>
								<th>Status Cetak</th>
								<th class="action">Action</th>

								<tbody>
									<?php
									if ($isi) {
										foreach ($isi as $row) {

											echo "<tr>";
											if ($row->f_sjp_cancel == 't') {
												echo "<td><h1>$row->i_sjp</h1></td>";
											} else {
												echo "<td>$row->i_sjp</td>";
											}

											echo "	<td>$row->e_area_name</td>
												<td>$row->d_sjp</td>
												<td>$row->d_sjp_receive</td>
												<td>$row->e_sender</td>
												<td>$row->e_recipient</td>
												<td>$row->e_remark</td>
												<td>$row->status_sjp</td>
												<td>$row->n_print</td>
												<td class=\"action\">";

											echo "<a href=\"#\" onclick='show(\"$folder/cform/edit/$row->id/\",\"#main\")'>
												<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/edit.png\" border=\"0\" alt=\"edit\" title=\"Edit\">
											</a>";

											// echo $this->session->userdata('level') - 1;

											if ($row->i_approve == $this->session->userdata('user_id') && $row->d_approve == "" && $row->d_notapprove == "") {
												echo "<a href=\"#\" onclick='show(\"$folder/cform/approvedetail/$row->id/$row->i_sjp/\",\"#main\")'>
													<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/approve_icon4.png\" border=\"0\" alt=\"approve\" title=\"Approve\">
												</a>";
											}

											if ($row->d_sjp_receive == '' && $row->d_approve != "") {
												// echo "	<a href=\"javascript:zzz('" . $row->id . "','" . $row->i_sjp . "');\">
												// 			<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/receive.png\" border=\"0\" alt=\"Receive\">
												// 		</a>";
												echo "<a href=\"#\" onclick='show(\"$folder/cform/receivedetail/$row->id/$row->i_sjp/\",\"#main\")'>
													<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/receive.png\" border=\"0\" alt=\"approve\" title=\"Receive\">
												</a>";
											}

											if ($row->f_printed == 'f' && $row->d_approve != "") {
												echo "<a href=\"javascript:yyy('" . $row->id . "','" . $row->i_sjp . "','" . $row->d_sjp_receive . "');\">
													<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/print.gif\" border=\"0\" alt=\"print\" title=\"Cetak\">
												</a>&nbsp;&nbsp;&nbsp;";
											}

											if (
												$row->f_printed == 't' && (($row->i_approve == $this->session->userdata('user_id')) ||
													$this->session->userdata('level') == '0' || ($this->session->userdata('level') == '3'))
											) {
												echo	"	<a href=\"#\" onclick='reprint(\"$folder/cform/undo/$row->id/$row->i_sjp/\",\"#main\")' title=\"Reprint (Cetak Ulang)\">
															<img height=15px; style=\"cursor:hand;\" src=\"" . base_url() . "img/no-printer.png\" border=\"0\" alt=\"Cancel Print\">
														</a>";
											}

											echo "</td>
												</tr>";
										}
									}
									?>
								</tbody>
							</table>
							<!-- <#?php echo "<center>" . $this->pagination->create_links() . "</center>"; ?> -->
						</div>
					</div>
				</div>
				<?= form_close() ?>
			</td>
		</tr>
	</table>
</div>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {
		/* DISABLED SUBMIT SAAT ENTER KEYBOARD */
		$(window).keydown(function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
		/* *********************************** */
	});

	function yyy(b, c) {
		lebar = 450;
		tinggi = 400;
		eval('window.open("<?php echo site_url(); ?>"+"/<?= $folder ?>/cform/cetak/"+b+"/"+c,"","width="+lebar+"px,height="+tinggi+"px,resizable=1,menubar=1,scrollbars=1,top=' + (screen.height - tinggi) / 2 + ',left=' + (screen.width - lebar) / 2 + '")');
	}

	function refreshview() {
		show('<?= $folder ?>/cform/index/', '#main');
	}

	function zzz(a, b) {
		showModal("<?= $folder ?>/cform/receivedetail/" + a + "/" + b + "/", "#light");
		jsDlgShow("#konten *", "#fade", "#light");
	}
</script>