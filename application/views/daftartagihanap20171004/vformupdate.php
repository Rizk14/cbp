<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'daftartagihanap/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="daftartagihanapform">
	<div class="effect">
	  <div class="accordion2">
	    <table class="mastertable" width="100%" cellspacing="0" cellpadding="1">
		<tr>
		<td>No</td>
		<?php 
			$tmp=explode("-",$isi->d_dtap);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$isi->d_dtap=$hr."-".$bl."-".$th;
		?>
		<td><input id="idtap" name="idtap" 	maxlength="9" value="<?php echo trim($isi->i_dtap);?>"><input id="xdtap" name="xdtap" 	type="hidden" value="<?php echo $isi->i_dtap;?>"><input readonly id="ddtap" name="ddtap" onclick="showCalendar('',this,this,'','ddtap',0,20,1)" value="<?php echo $isi->d_dtap;?>"></td>
		<td>Area</td>
		<td><input readonly id="eareaname" name="eareaname" value="<?php echo $isi->e_area_name;?>"
		onclick='showModal("daftartagihanap/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		<input id="iarea" name="iarea" type="hidden" value="<?php echo $isi->i_area;?>"></td>
		</tr>
		<tr>
		<td>Supplier</td>
		<td><input readonly id="esuppliername" name="esuppliername" value="<?php echo $isi->e_supplier_name;?>" 
		onclick='showModal("daftartagihanap/cform/supplier/","#light");jsDlgShow("#konten *", "#fade", "#light");'>
		<input id="isupplier" name="isupplier" type="hidden" value="<?php echo $isi->i_supplier;?>"></td>
		<td>Jatuh Tempo</td>
		<?php 
			$tmp=explode("-",$isi->d_due_date);
			$th=$tmp[0];
			$bl=$tmp[1];
			$hr=$tmp[2];
			$isi->d_due_date=$hr."-".$bl."-".$th;
		?>
		<td><input readonly id="dduedate" name="dduedate" onclick="showCalendar('',this,this,'','dduedate',0,20,1)" value="<?php echo $isi->d_due_date;?>"></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><input readonly id="esupplieraddress" name="esupplieraddress" value="<?php echo $isi->e_supplier_address;?>"></td>
		<td>Kotor</td>
		<td><input style="text-align:right;" readonly id="vgross" name="vgross"  value="<?php echo number_format($isi->v_gross);?>"></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td><input readonly id="esuppliercity" name="esuppliercity" value="<?php echo $isi->e_supplier_city;?>"></td>
		<td>Potongan</td>
		<td><input style="text-align:right;" id="ndiscount" name="ndiscount" value="<?php echo number_format($isi->n_discount);?>" onkeyup="diskon();">%
			<input style="text-align:right;" id="vdiscount" name="vdiscount" value="<?php echo number_format($isi->v_discount);?>" onkeyup="diskon();"></td>
		</tr>
		<tr>
		<td>Faktur Pajak</td>
		<?php 
			if($isi->d_pajak!=''){
				$tmp=explode("-",$isi->d_pajak);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$isi->d_pajak=$hr."-".$bl."-".$th;
			}
		?>
		<td><input id="ipajak" name="ipajak" value="<?php echo trim($isi->i_pajak);?>"><input readonly id="dpajak" name="dpajak" value="<?php echo $isi->d_pajak;?>" onclick="showCalendar('',this,this,'','dpajak',0,20,1)"></td>
		<td>PPN 10%</td>
		<td><input type="hidden" id="fsupplierpkp" name="fsupplierpkp" value="<?php echo $isi->f_supplier_pkp;?>">
			<input style="text-align:right;" readonly id="vppn" name="vppn" value="<?php echo number_format($isi->v_ppn);?>"></td>
		</tr>
		<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>Jumlah Bayar</td>
		<td><input style="text-align:right;" readonly id="vnetto" name="vnetto" value="<?php echo number_format($isi->v_netto);?>"></td>
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td>
		    <input <?php if(!$bisaedit) echo "disabled"; ?> name="login" id="login" value="Simpan" type="submit" 
			   onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listdaftartagihanap/cform/view/<?php echo $dfrom."/".$dto."/".$isupplier."/"; ?>","#main");'>
		    <input <?php if(!$bisaedit) echo "disabled"; ?> name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"></td>
		  
<!--
		    <input name="login" id="login" value="Simpan" type="submit" onclick="dipales(parseFloat(document.getElementById('jml').value));">
		    <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick='show("listdaftartagihanap/cform/view/<?php echo $dfrom."/".$dto."/".$isupplier."/"; ?>","#main");'>
		    <input name="cmdtambahitem" id="cmdtambahitem" value="Tambah Item" type="button"
			   onclick="tambah_item(parseFloat(document.getElementById('jml').value)+1);"></td>-->
		</tr>
	    </table>
			<div id="detailheader" align="center">
				<table id="itemtem" class="listtable" style="width:800px;">
					<th style="width:25px;" align="center">No</th>
					<th style="width:105px;" align="center">DO</th>
					<th style="width:80px;" align="center">Kd Brg</th>
					<th style="width:250px;" align="center">Nama Barang</th>
					<th style="width:50px;" align="center">Motif</th>
					<th style="width:50px;" align="center">Jml</th>
					<th style="width:80px;" align="center">Hrg</th>
					<th style="width:100px;" align="center">Bayar</th>
					<th style="width:40px;" align="center">Act</th>
				</table>
			</div>
			<div id="detailisi" align="center">
				<?php 				
					$a=0;
					foreach($detail as $row)
					{
						$a++;
            if($row->d_do!=''){
              $tmp=explode('-',$row->d_do);
              $dt=$tmp[2];
              $bl=$tmp[1];
              $th=$tmp[0];
              $row->d_do=$dt.'-'.$bl.'-'.$th;
            }
						$jumlah=number_format($row->n_jumlah);
						$pabrik=number_format($row->v_pabrik);
						$netto =number_format($row->v_netto);
						$bayar =number_format($row->v_gross);
						echo "<table class=\"listtable\" style=\"width:800px;\">";
/*						echo "<tbody><tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"baris$a\" name=\"baris$a\" value=\"$a\"></td><td style=\"width:100px;\"><input style=\"width:100px;\" readonly type=\"text\" id=\"ido$a\" name=\"ido$a\" value=\"$row->i_do\"><input type=\"hidden\" id=\"ddo$a\" name=\"ddo$a\" value=\"$row->d_do\"><input type=\"hidden\" id=\"dop$a\" name=\"dop$a\" value=\"$row->d_op\"></td><td style=\"width:76px;\"><input style=\"width:76px;\" readonly type=\"text\" id=\"iproduct$a\" name=\"iproduct$a\" value=\"$row->i_product\"></td><td style=\"width:240px;\"><input readonly style=\"width:240px;\" type=\"text\" id=\"eproductname$a\" name=\"eproductname$a\" value=\"$row->e_product_name\"></td><td style=\"width:48px;\"><input readonly style=\"width:48px;\" type=\"text\" id=\"eproductmotifname$a\" name=\"eproductmotifname$a\" value=\"$row->e_product_motifname\"><input type=\"hidden\" id=\"iproductmotif$a\" name=\"iproductmotif$a\" value=\"$row->i_product_motif\"><input type=\"hidden\" id=\"iop$a\" name=\"iop$a\" value=\"$row->i_op\"></td><td style=\"width:47px;\"><input readonly style=\"text-align:right; width:47px;\" type=\"text\" id=\"ndeliver$a\" name=\"ndeliver$a\" value=\"$jumlah\"></td><td style=\"width:77px;\"><input style=\"text-align:right; width:77px;\" type=\"text\" id=\"vunitprice$a\" name=\"vunitprice$a\" value=\"$pabrik\" onkeyup=\"hitung()\"></td><td style=\"width:96px;\"><input readonly style=\"text-align:right; width:96px;\" type=\"text\" id=\"vbayar$a\" name=\"vbayar$a\" value=\"$bayar\" onkeyup=\"reformat(this);\"></td><td class=\"action\" style=\"width:200px;\"></td></tr></tbody></table>";*/
    echo "
		      <tbody>
            <tr><td style=\"width:23px;\"><input style=\"width:23px;\" readonly type=\"text\" id=\"baris$a\" name=\"baris$a\" value=\"$a\"></td>
            <td style=\"width:100px;\"><input style=\"width:100px;\" readonly type=\"text\" id=\"ido$a\" name=\"ido$a\" value=\"$row->i_do\"><input type=\"hidden\" id=\"iop$a\" name=\"iop$a\" value=\"$row->i_op\">
<input style=\"width:77px;\" readonly type=\"hidden\" id=\"ddo$a\" name=\"ddo$a\" value=\"$row->d_do\"></td>
            <td style=\"width:76px;\"><input readonly onclick='showModal(\"daftartagihanap/cform/harga/$row->i_product/$a/\",\"#light\");jsDlgShow(\"#konten *\", \"#fade\", \"#light\");' style=\"width:76px;\"  type=\"text\" id=\"iproduct$a\" name=\"iproduct$a\" value=\"$row->i_product\"></td>
            <td style=\"width:240px;\"><input readonly style=\"width:240px;\"  type=\"text\" id=\"eproductname$a\" name=\"eproductname$a\" value=\"$row->e_product_name\"></td>
<td style=\"width:48px;\"><input readonly style=\"width:48px;\" type=\"text\" id=\"eproductmotifname$a\" name=\"eproductmotifname$a\" value=\"$row->e_product_motifname\"><input type=\"hidden\" id=\"iproductmotif$a\" name=\"iproductmotif$a\" value=\"$row->i_product_motif\"><input type=\"hidden\" id=\"iop$a\" name=\"iop$a\" value=\"$row->i_op\"></td>
            <td style=\"width:47px;\"><input readonly style=\"width:47px;text-align:right;\"  type=\"text\" id=\"ndeliver$a\" name=\"ndeliver$a\" value=\"$row->n_jumlah\"></td>
            <td style=\"width:79px;\"><input readonly style=\"width:79px;text-align:right;\"  type=\"text\" id=\"vunitprice$a\" name=\"vunitprice$a\" value=\"".number_format($row->v_pabrik)."\" onkeyup=\"hitung()\"></td><td style=\"width:96px;\"><input readonly style=\"text-align:right; width:96px;\" type=\"text\" id=\"vbayar$a\" name=\"vbayar$a\" value=\"$bayar\" onkeyup=\"reformat(this);\"></td><td class=\"action\" style=\"width:200px;\"></td></tr></tbody></tr></tbody></table>";
					}
				?>
			</div>
			<div id="pesan"></div>
			<input type="hidden" name="jml" id="jml" value="<?php echo $jmlitem; ?>">
	  </div>
	</div>
	</div>
	<?=form_close()?> 
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
  function view_area(){
    lebar =450;
    tinggi=400;
    eval('window.open("<?php echo site_url(); ?>"+"/daftartagihanap/cform/area/","","width="+lebar+"px,height="+tinggi+"px,resizable=1,scrollbars=1,top='+(screen.height-tinggi)/2+',left='+(screen.width-lebar)/2+'")');
  }
/*  function tambah_item(a){
    so_inner=document.getElementById("detailheader").innerHTML;
    si_inner=document.getElementById("detailisi").innerHTML;
    if(so_inner==''){
		so_inner = '<table id="itemtem" class="listtable" style="width:760px;">';
		so_inner+= '<th style="width:25px;" align="center">No</th>';
		so_inner+= '<th style="width:105px;" align="center">DO</th>';
		so_inner+= '<th style="width:80px;" align="center">Kd Brg</th>';
		so_inner+= '<th style="width:250px;" align="center">Nama Barang</th>';
		so_inner+= '<th style="width:50px;" align="center">Motif</th>';
		so_inner+= '<th style="width:50px;" align="center">Jml</th>';
		so_inner+= '<th style="width:80px;" align="center">Hrg</th>';
		so_inner+= '<th style="width:100px;" align="center">Bayar</th>';
		so_inner+= '</table>';
		document.getElementById("detailheader").innerHTML=so_inner;
    }else{
		so_inner=''; 
    }
    if(si_inner==''){
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;	
		si_inner='<table class="listtable" style="width:800px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		si_inner+='<td style="width:100px;"><input style="width:100px;" readonly type="text" id="ido'+a+'" name="ido'+a+'" value=""><input type="hidden" id="ddo'+a+'" name="ddo'+a+'" value=""><input type="hidden" id="dop'+a+'" name="dop'+a+'" value=""></td>';
		si_inner+='<td style="width:76px;"><input style="width:76px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:240px;"><input readonly style="width:240px;"  type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;"><input readonly style="width:48px;"  type="text" id="eproductmotifname'+a+'" name="eproductmotifname'+a+'" value=""><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input type="hidden" id="iop'+a+'" name="iop'+a+'" value=""></td>';
		si_inner+='<td style="width:47px;"><input readonly style="text-align:right; width:47px;"  type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value=""></td>';
		si_inner+='<td style="width:77px;"><input readonly style="text-align:right; width:77px;"  type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:96px;"><input readonly style="text-align:right; width:96px;" type="text" id="vbayar'+a+'" name="vbayar'+a+'" value="" onkeyup="reformat(this);"></td><td style="width:200px;">&nbsp;</td></tr></tbody></table>';
    }else{
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)+1;
		juml=document.getElementById("jml").value;
		si_inner=si_inner+'<table class="listtable" style="width:800px;"><tbody><tr><td style="width:23px;"><input style="width:23px;" readonly type="text" id="baris'+a+'" name="baris'+a+'" value="'+a+'"></td>';
		si_inner+='<td style="width:100px;"><input style="width:100px;" readonly type="text" id="ido'+a+'" name="ido'+a+'" value=""><input type="hidden" id="ddo'+a+'" name="ddo'+a+'" value=""><input type="hidden" id="dop'+a+'" name="dop'+a+'" value=""></td>';
		si_inner+='<td style="width:76px;"><input style="width:76px;" readonly type="text" id="iproduct'+a+'" name="iproduct'+a+'" value=""></td>';
		si_inner+='<td style="width:240px;"><input readonly style="width:240px;"  type="text" id="eproductname'+a+'" name="eproductname'+a+'" value=""></td>';
		si_inner+='<td style="width:48px;"><input readonly style="width:48px;"  type="text" id="eproductmotifname'+a+'" name="eproductmotifname'+a+'" value=""><input type="hidden" id="iproductmotif'+a+'" name="iproductmotif'+a+'" value=""><input type="hidden" id="iop'+a+'" name="iop'+a+'" value=""></td>';
		si_inner+='<td style="width:47px;"><input readonly style="text-align:right; width:47px;"  type="text" id="ndeliver'+a+'" name="ndeliver'+a+'" value=""></td>';
		si_inner+='<td style="width:77px;"><input readonly style="text-align:right; width:77px;"  type="text" id="vproductmill'+a+'" name="vproductmill'+a+'" value=""></td>';
		si_inner+='<td style="width:96px;"><input readonly style="text-align:right; width:96px;" type="text" id="vbayar'+a+'" name="vbayar'+a+'" value="" onkeyup="reformat(this);"></td><td style="width:200px;">&nbsp;</td></tr></tbody></table>';
	}
    j=0;
    var baris		=Array();
    var ido			=Array();
    var iop			=Array();
    var ddo			=Array();
    var dop			=Array();
    var iproduct	=Array();
  	var eproductname=Array();
    var iproductmotif 	 =Array();
  	var eproductmotifname=Array();
    var ndeliver	=Array();
  	var vproductmill=Array();
  	var vbayar		=Array();
    for(i=1;i<a;i++){
		j++;
		baris[j]		= document.getElementById("baris"+i).value;
		ido[j]			= document.getElementById("ido"+i).value;
		iop[j]			= document.getElementById("iop"+i).value;
		ddo[j]			= document.getElementById("ddo"+i).value;
		dop[j]			= document.getElementById("dop"+i).value;
		iproduct[j]		= document.getElementById("iproduct"+i).value;
		eproductname[j]	= document.getElementById("eproductname"+i).value;
		iproductmotif[j]		= document.getElementById("iproductmotif"+i).value;
		eproductmotifname[j]	= document.getElementById("eproductmotifname"+i).value;
		ndeliver[j]		= document.getElementById("ndeliver"+i).value;
		vproductmill[j]	= document.getElementById("vproductmill"+i).value;
		vbayar[j]		= document.getElementById("vbayar"+i).value;
    }
    document.getElementById("detailisi").innerHTML=si_inner;
    j=0;
    for(i=1;i<a;i++){
		j++;
		document.getElementById("baris"+i).value=baris[j];
		document.getElementById("ido"+i).value=ido[j];
		document.getElementById("iop"+i).value=iop[j];
		document.getElementById("ddo"+i).value=ddo[j];
		document.getElementById("dop"+i).value=dop[j];
		document.getElementById("iproduct"+i).value=iproduct[j];
		document.getElementById("eproductname"+i).value=eproductname[j];
		document.getElementById("iproductmotif"+i).value=iproductmotif[j];
		document.getElementById("eproductmotifname"+i).value=eproductmotifname[j];
		document.getElementById("ndeliver"+i).value=ndeliver[j];
		document.getElementById("vproductmill"+i).value=vproductmill[j];
		document.getElementById("vbayar"+i).value=vbayar[j];
    }
	area	=document.getElementById("iarea").value;
	supplier=document.getElementById("isupplier").value;
	showModal("daftartagihanap/cform/idoupdate/"+a+"/"+area+"/"+supplier+"/","#light");
	jsDlgShow("#konten *", "#fade", "#light");
  }*/
  function tambah_item(a){

	  area	   = document.getElementById("iarea").value;
	  supplier = document.getElementById("isupplier").value;
	  idtap    = document.getElementById("idtap").value;
	  ddtap    = document.getElementById("ddtap").value;
    if(idtap=='')idtap='idtap';
    if(ddtap=='')ddtap='ddtap';
    if(supplier=='SP078'){
  	  showModal("daftartagihanap/cform/idotirai/"+a+"/"+area+"/"+supplier+"/"+i+"/"+idtap+"/"+ddtap+"/","#light");
    }else{
  	  showModal("daftartagihanap/cform/ido/"+a+"/"+area+"/"+supplier+"/"+i+"/"+idtap+"/"+ddtap+"/","#light");
    }
	  jsDlgShow("#konten *", "#fade", "#light");
  }
  function dipales(a){
  	 cek='false';
  	 if((document.getElementById("idtap").value!='') &&
  	 	(document.getElementById("ddtap").value!='') &&
  	 	(document.getElementById("iarea").value!='') &&
			(document.getElementById("isupplier").value!='') &&
			(document.getElementById("dduedate").value!='') &&
			(document.getElementById("vnetto").value!='0')) {
  	 	if(a==0){
  	 		alert('Isi data item minimal 1 !!!');
  	 	}else{
    			for(i=1;i<=a;i++){
				if(
//					(document.getElementById("ido"+i).value=='') ||
					(document.getElementById("iproduct"+i).value=='')
				  )
				{
					alert('Data item masih ada yang salah !!!');
					cek='false';
					exit();
				}else{
					cek='true';
				}
			}
		}
		if(cek=='true'){
  	  		document.getElementById("login").disabled=true;
			document.getElementById("cmdtambahitem").disabled=true;
		}
    }else{
   		alert('Data header masih ada yang salah !!!');
    }
  }
  function clearitem(){
    document.getElementById("detailisi").innerHTML='';
    document.getElementById("pesan").innerHTML='';
    document.getElementById("jml").value='0';
    document.getElementById("login").disabled=false;
  }
  function diskon(){
	gross  = formatulang(document.getElementById("vgross").value);
	persen = formatulang(document.getElementById("ndiscount").value);
	nilai  = formatulang(document.getElementById("vdiscount").value);
	if ( (isNaN(parseFloat(persen))) || (isNaN(parseFloat(nilai))) ){
		alert("Input harus numerik");
	}else{
		persen = parseFloat(persen);
		nilai  = parseFloat(nilai);
		gross  = parseFloat(gross);
		bersih = 0;
		if(persen!=0){
			nilai  = Math.round(((gross/100)*persen)*1)/1;
			document.getElementById("vdiscount").value = formatcemua(nilai);
		}else{
			persen = Math.round((nilai/(gross/100))*1)/1;
			document.getElementById("ndiscount").value = formatcemua(persen);
		}
//		ppn    = Math.round(((gross-nilai)*0.1)*1)/1;
    if(document.getElementById("fsupplierpkp").value!='f'){
	  	ppn = Math.round(((gross-nilai)*0.1)*1)/1;
	  }else{
	  	ppn = 0;
	  }
		bersih = gross-nilai+ppn;
		document.getElementById("vppn").value      = formatcemua(ppn);
		document.getElementById("vnetto").value    = formatcemua(bersih);
	}
  }
  function hitung(){
    jml=document.getElementById("jml").value;
    gross=0;
    for(i=1;i<=jml;i++){
      qty=formatulang(document.getElementById("ndeliver"+i).value);
      val=formatulang(document.getElementById("vunitprice"+i).value);
      sub=qty*val;
      gross=gross+sub;
    }
	  document.getElementById("vgross").value      = formatcemua(gross)
	  persen = formatulang(document.getElementById("ndiscount").value);
	  nilai  = formatulang(document.getElementById("vdiscount").value);
	  if ( (isNaN(parseFloat(persen))) || (isNaN(parseFloat(nilai))) ){
		  alert("Input harus numerik");
	  }else{
		  persen = parseFloat(persen);
		  nilai  = parseFloat(nilai);
		  gross  = parseFloat(gross);
		  bersih = 0;
		  if(persen!=0){
			  nilai  = Math.round(((gross/100)*persen)*1)/1;
			  document.getElementById("vdiscount").value = formatcemua(nilai);
		  }else{
			  persen = Math.round((nilai/(gross/100))*1)/1;
		  }
//		  ppn    = Math.round(((gross-nilai)*0.1)*1)/1;
      if(document.getElementById("fsupplierpkp").value!='f'){
		  	ppn = Math.round(((gross-nilai)*0.1)*1)/1;
		  }else{
		  	ppn = 0;
		  }
		  bersih = gross-nilai+ppn;
		  document.getElementById("vppn").value      = formatcemua(ppn);
		  document.getElementById("vnetto").value    = formatcemua(bersih);
	  }
  }
</script>
