<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo NmPerusahaan;?> : <?php echo $page_title;?></title>
</head>
<body id="bodylist">
<div id="main">
<div id="tmp">
<?php echo "<center><h2>$page_title</h2></center>"; ?>
<table class="maintable">
  <tr>
    <td align="left">
	<?php 
		$tujuan = 'daftartagihanap/cform/cariidoupdate/'.$baris.'/'.$area;
	?>

	<?php echo $this->pquery->form_remote_tag(array('url'=>$tujuan,'update'=>'#light','type'=>'post'));?>
	<div id="listform">
	<div class="effect">
	  <div class="accordion2">
    	  <table class="listtable">
	    <thead>
	      <tr>
		<td colspan="4" align="center">Cari data : <input type="text" id="cari" name="cari" value="">&nbsp;<input type="submit" id="bcari" name="bcari" value="Cari"><input type="hidden" id="baris" name="baris" value="<?php echo $baris;?>"></td>
	      </tr>
	    </thead>
      	    <th>DO</th>
	    	<th>Product</th>
			<th>Nama Product</th>
			<th>Jumlah</th>
	    <tbody>
	      <?php 
		if($isi){
			foreach($isi as $row){
			  $tmp=explode("-",$row->d_do);
			  $dd=$tmp[2];
			  $mm=$tmp[1];
			  $yy=$tmp[0];
			  $row->d_do=$dd."-".$mm."-".$yy;
			  echo "<tr> 
				  <td><a href=\"javascript:setValue('$row->i_do','$row->i_area',$baris,'$row->i_product','$row->e_product_name','$row->n_deliver','$row->v_product_mill','$row->i_product_motif','$row->e_product_motifname','$row->i_op','$row->d_do')\">$row->i_do</a></td>
				  <td><a href=\"javascript:setValue('$row->i_do','$row->i_area',$baris,'$row->i_product','$row->e_product_name','$row->n_deliver','$row->v_product_mill','$row->i_product_motif','$row->e_product_motifname','$row->i_op','$row->d_do')\">$row->i_product</a></td>
				  <td><a href=\"javascript:setValue('$row->i_do','$row->i_area',$baris,'$row->i_product','$row->e_product_name','$row->n_deliver','$row->v_product_mill','$row->i_product_motif','$row->e_product_motifname','$row->i_op','$row->d_do')\">$row->e_product_name</a></td>
				  <td><a href=\"javascript:setValue('$row->i_do','$row->i_area',$baris,'$row->i_product','$row->e_product_name','$row->n_deliver','$row->v_product_mill','$row->i_product_motif','$row->e_product_motifname','$row->i_op','$row->d_do')\">".number_format($row->n_deliver)."</a></td>
				</tr>";
			}
		}else{
			echo "<center>Data tidak ada</center>";
		}
	      ?>
	    </tbody>
	  </table>
	  <?php echo "<center>".$this->pagination->create_links()."</center>";?>
	  <br>
	  <center><input type="button" id="batal" name="batal" value="Keluar" onclick="bbatal()"></center>
  	</div>
    </div>
    </div>
      <?=form_close()?>
    </td>
  </tr>
</table>
</div>
</div>
</BODY>
</html>
<script language="javascript" type="text/javascript">
  function setValue(a,b,c,d,e,f,g,h,i,j,k,l)
  {
    ada=false;
    for(z=1;z<=c;z++){
		if(
			(a==document.getElementById("ido"+z).value) &&
			(d==document.getElementById("iproduct"+z).value) &&  
			(z!==c)
		  ){
			alert ("do : "+a+" brg : "+d+" sudah ada !!!!!");
			ada=true;
			break;
		}else{
			ada=false;	   
		}
    }	
    if(!ada){
		document.getElementById("ido"+c).value=a;
		document.getElementById("ddo"+c).value=k;
		document.getElementById("iproduct"+c).value=d;
		document.getElementById("eproductname"+c).value=e;
		document.getElementById("ndeliver"+c).value=f;
		document.getElementById("vproductmill"+c).value=formatcemua(formatulang(g));
		bayar=f*g;
		document.getElementById("vbayar"+c).value=formatcemua(bayar);
		document.getElementById("iproductmotif"+c).value=h;
		document.getElementById("eproductmotifname"+c).value=i;
		document.getElementById("iop"+c).value=j;
		document.getElementById("dop"+c).value=l;
		document.getElementById("vgross").value=formatcemua(parseFloat(formatulang(document.getElementById("vgross").value))+bayar);
		disc=document.getElementById("ndiscount").value;
		if(disc!='0' || disc!=''){
			disc=(parseFloat(formatulang(document.getElementById("ndiscount").value))*parseFloat(formatulang(document.getElementById("vgross").value)))/100;
			document.getElementById("vdiscount").value=formatcemua(disc);
		}else{
			disc=parseFloat(formatulang(document.getElementById("vdiscount").value));
		}
		pkp=document.getElementById("fsupplierpkp").value;
		if(pkp=='t'){
			ppn=(parseFloat(formatulang(document.getElementById("vgross").value))-disc)*0.1;
			document.getElementById("vppn").value = formatcemua(ppn);
		}else{
			document.getElementById("vppn").value = "0";
			ppn=0;
		}
		bersih=(
				(parseFloat(formatulang(document.getElementById("vgross").value)))-disc+ppn
			   );
		document.getElementById("vnetto").value=formatcemua(bersih);
		jsDlgHide("#konten *", "#fade", "#light");
    }
	
  }
  function bbatal(){
	baris	= document.getElementById("baris").value;
	si_inner= document.getElementById("detailisi").innerHTML;
	var temp= new Array();
	temp	= si_inner.split('<table disabled="disabled" class="listtable" style="width: 800px;">');
	if( (document.getElementById("ido"+baris).value=='')){
		si_inner='';
		for(x=1;x<baris;x++){
			si_inner=si_inner+'<table class="listtable" style="width: 800px;">'+temp[x];
		}
		j=0;
		var barbar		= Array();
		var ido			= Array();
		var iop			= Array();
		var ddo			= Array();
		var dop			= Array();
		var iproduct	= Array();
		var eproductname= Array();
		var iproductmotif	 = Array();
		var eproductmotifname= Array();
		var ndeliver	= Array();
		var vproductmill= Array();
		var vbayar		= Array();
		for(i=1;i<baris;i++){
			j++;
			barbar[j]		= document.getElementById("baris"+i).value;
			ido[j]			= document.getElementById("ido"+i).value;
			iop[j]			= document.getElementById("iop"+i).value;
			ddo[j]			= document.getElementById("ddo"+i).value;
			dop[j]			= document.getElementById("dop"+i).value;
			iproduct[j]		= document.getElementById("iproduct"+i).value;
			eproductname[j]	= document.getElementById("eproductname"+i).value;	
			iproductmotif[j]	= document.getElementById("iproductmotif"+i).value;
			eproductmotifname[j]= document.getElementById("eproductmotifname"+i).value;	
			ndeliver[j]		= document.getElementById("ndeliver"+i).value;		
			vproductmill[j]	= document.getElementById("vproductmill"+i).value;
			vbayar[j]		= document.getElementById("vbayar"+i).value;
		}
		document.getElementById("detailisi").innerHTML=si_inner;
		j=0;
		for(i=1;i<baris;i++){
			j++;
			document.getElementById("baris"+i).value		= barbar[j];
			document.getElementById("ido"+i).value			= ido[j];
			document.getElementById("iop"+i).value			= iop[j];
			document.getElementById("ddo"+i).value			= ddo[j];
			document.getElementById("dop"+i).value			= dop[j];
			document.getElementById("iproduct"+i).value		= iproduct[j];
			document.getElementById("eproductname"+i).value	= eproductname[j];
			document.getElementById("iproductmotif"+i).value		= iproductmotif[j];
			document.getElementById("eproductmotifname"+i).value	= eproductmotifname[j];
			document.getElementById("ndeliver"+i).value		= ndeliver[j];		
			document.getElementById("vproductmill"+i).value	= vproductmill[j];
			document.getElementById("vbayar"+i).value		= vbayar[j];
		}
		document.getElementById("jml").value=parseFloat(document.getElementById("jml").value)-1;		
		jsDlgHide("#konten *", "#fade", "#light");
	}
  }
</script>
