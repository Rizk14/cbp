<h2><?php echo $page_title;?></h2>
<table class="maintable">
  <tr>
    <td align="left">
	<?php echo $this->pquery->form_remote_tag(array('url'=>'debetnota/cform/update','update'=>'#pesan','type'=>'post'));?>
	<div id="debetnotaform">
	<div class="effect">
	  <div class="accordion2">
		<?php 
		foreach($isi as $row){ 
			$tmp=explode('-',$row->d_kn);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_kn=$tgl.'-'.$bln.'-'.$thn;
			$tmp=explode('-',$row->d_refference);
			$tgl=$tmp[2];
			$bln=$tmp[1];
			$thn=$tmp[0];
			$row->d_refference=$tgl.'-'.$bln.'-'.$thn;
		?>

	    <table class="mastertable">
	      <tr>
		<td width="12%">Debet Nota</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="idn" id="idn" value="<?php echo $row->i_kn;?>" >
					<input readonly name="ddn" id="ddn" onclick="showCalendar('',this,this,'','ddn',0,20,1)" value="<?php echo $row->d_kn; ?>"></td>
		<td width="12%">Pelanggan</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="icustomer" id="icustomer" value="<?php echo $row->i_customer; ?>" >
						<input type="hidden" name="icustomergroupar" id="icustomergroupar" value="<?php echo $row->i_customer_groupar; ?>">
						<input readonly name="ecustomername" id="ecustomername" value="<?php echo $row->e_customer_name; ?>" onclick="view_customer();"></td>
	      </tr>
	      <tr>
		<td width="12%">Area</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="iarea" id="iarea" value="<?php echo $row->i_area; ?>">
						<input readonly name="eareaname" id="eareaname" value="<?php echo $row->e_area_name; ?>" 
							   onclick='showModal("debetnota/cform/area/","#light");jsDlgShow("#konten *", "#fade", "#light");'></td>
		<td width="12%">Alamat</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="ecustomeraddress" id="ecustomeraddress" value="<?php echo $row->e_customer_address; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">No Refferensi</td>
		<td width="1%">:</td>
		<td width="37%"><input readonly name="irefference" id="irefference" value="<?php echo $row->i_refference; ?>">
						<input readonly name="drefference" id="drefference" value="<?php echo $row->d_refference; ?>"></td>
		<td width="12%">Salesman</td>
		<td width="1%">:</td>
		<td width="37%"><input type="hidden" name="isalesman" id="isalesman" value="<?php echo $row->i_salesman; ?>">
						<input readonly name="esalesmanname" id="esalesmanname" value="<?php echo $row->e_salesman_name; ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Kotor</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vgross" id="vgross" value="<?php echo number_format($row->v_gross); ?>"></td>
		<td width="12%">Insentif</td>
		<td width="1%">:</td>
		<td width="37%"><input type="checkbox" name="finsentif" id="finsentif" 
						<?php if($row->f_insentif=='t') 
							echo "checked value=\"on\" ";
						   else
							echo "value=\"\" ";
						?> 
						onclick="insentif(this.value)">
						&nbsp;Masalah&nbsp;
						<input type="checkbox" name="fmasalah" id="fmasalah" 
						<?php if($row->f_masalah=='t') 
							echo "checked value=\"on\" ";
						   else
							echo "value=\"\" ";
						?> 
						onclick="masalah(this.value)"></td>
	      </tr>
	      <tr>
		<td width="12%">Potongan</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vdiscount" id="vdiscount" value="<?php echo number_format($row->v_discount); ?>"></td>
		<td width="12%">Sisa</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vsisa" id="vsisa" value="<?php echo number_format($row->v_sisa); ?>"></td>
	      </tr>
	      <tr>
		<td width="12%">Bersih</td>
		<td width="1%">:</td>
		<td width="37%"><input style="text-align:right;" readonly name="vnetto" id="vnetto" value="<?php echo number_format($row->v_netto); ?>"></td>
		<td width="12%">Keterangan</td>
		<td width="1%">:</td>
		<td width="37%"><input name="eremark" id="eremark" value="<?php echo $row->e_remark; ?>"></td>
	      </tr>
		<td width="12%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td width="87%" colspan=4>
		  <input name="login" id="login" value="Simpan" type="submit" onclick="dipales()" <?php if($row->v_sisa==0) echo 'disabled'; ?>>
		  <input name="cmdreset" id="cmdreset" value="Keluar" type="button" onclick="show('listdebetnota/cform/view/<?php echo $dfrom."/".$dto."/".$iarea."/"; ?>','#main')"></td>
	      </tr>
		</table>
		<?php } ?>
	  </div>
	  <div id="pesan"></div>
	</div>
	</div>
	<?=form_close()?>
    </td>
  </tr>
</table>
<script language="javascript" type="text/javascript">
	function dipales(){
		if(
			(document.getElementById("dkn").value=='') ||
			(document.getElementById("iarea").value=='') ||
			(document.getElementById("irefference").value=='')
		  )
		{
			alert("Data Header belum lengkap !!!");
		}else{			
			document.getElementById("login").disabled=true;
		}
	}
	function view_bbm(){
		area=document.getElementById("iarea").value;
		if(area!=''){
			showModal("debetnota/cform/bbm/"+area+"/","#light");
			jsDlgShow("#konten *", "#fade", "#light");
		}
	}
	function view_customer(){
		area=document.getElementById("iarea").value;
		showModal("debetnota/cform/customer/"+area+"/","#light"); 
		jsDlgShow("#konten *", "#fade", "#light");
	} 
	function insentif(a){
		if(a==''){
			document.getElementById("finsentif").value='on';
		}else{
			document.getElementById("finsentif").value='';
		}
	}
	function masalah(a){
		if(a==''){
			document.getElementById("fmasalah").value='on';
		}else{
			document.getElementById("fmasalah").value='';
		}
	}
/*	function tesss(){
		document.getElementById("ikn").value="";
		document.getElementById("dkn").value="";
		document.getElementById("icustomer").value="";
		document.getElementById("icustomergroupar").value="";
		document.getElementById("ecustomername").value="";
		document.getElementById("iarea").value="";
		document.getElementById("eareaname").value="";
		document.getElementById("ecustomeraddress").value="";
		document.getElementById("irefference").value="";
		document.getElementById("drefference").value="";
		document.getElementById("isalesman").value="";
		document.getElementById("esalesmanname").value="";
		document.getElementById("vgross").value="";
		document.getElementById("finsentif").value="";
		document.getElementById("finsentif").checked=false;
		document.getElementById("vdiscount").value="";
		document.getElementById("fmasalah").value="";
		document.getElementById("fmasalah").checked=false
		document.getElementById("vnetto").value="";
		document.getElementById("vsisa").value="";
		document.getElementById("eremark").value="";
		document.getElementById("login").disabled=false;
		document.getElementById("pesan").innerHTML='';
	} */
</script>
