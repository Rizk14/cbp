<h2><?php echo $page_title ;?></h2>

<table class="maintable">
	<tr>
		<td align="left">
			<?php echo $this->pquery->form_remote_tag(array('url'=>'debetnota/cform/simpen','update'=>'#pesan','type'=>'post'));?>
			<div id="listform">
				<div class="effect">
					<div class="accordion2">
						<table border = 0>
							<tr>
								<td width="15%" style="font-size:12px"><b>Tanggal Debet Nota&nbsp;</td>
								<td width="1%">:&nbsp;</td>
								<td>							
									<?php 
										$data = array(
											'name'        => 'dkn',
											'id'          => 'dkn',
											'value'       => '',
											'readonly'    => 'true',
											'onclick'	  => "showCalendar('',this,this,'','dkn',0,20,1)");
								  		echo form_input($data)."<td><h2><i><b>* Pastikan Periode Bulan pada Tanggal DN sama dengan Periode Bulan Bank</b></i></h2></td>";
									?>
								</td>
							</tr>
						</table>
						
						
						<table class="listtable" id="sitabel">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Bank</th>
									<th>Bank Masuk</th>
									<th>Tanggal</th>
									<th>Area</th>
									<th>KU/GIRO/TUNAI</th>
									<th>Jumlah</th>
									<th>Sisa</th>
									<th>Ket</th>
									<th class="action">Action <br>
										<?php echo "<input type='checkbox' name='chkall' id='chkall' value='' onclick='pilihsemua(this.value)'>" ;?>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php 
		if($isi){
			$i = 0;
			$x = 1;
			foreach($isi as $row){
				$tmp=explode("-",$row->d_bank);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$thbl=$th.$bl;
				$dbank=$hr."-".$bl."-".$th;
				$nama= str_replace('%20', ' ',$row->e_area_name);
				$namabank= str_replace('%20', ' ',$row->e_bank_name);
			  echo "<tr>
			    <td>$x</td>
			    <td>$row->e_bank_name</td>
				  <td>$row->i_kbank</td>
				  <td>$dbank</td>
				  <td>$nama</td>
				  <td>$row->i_giro</td>
				  <td align=right>".number_format($row->v_bank)."</td>
				  <td align=right>".number_format($row->v_sisa)."</td>
				  <td>$row->e_description</td>
				  <input type = 'hidden' name = 'thbl".$i."' id = 'thbl".$i."' value = '$thbl'>
				  <input type = 'hidden' name = 'periode".$i."' id = 'periode".$i."' value = '$periode'>
				  <td class=\"action\">";
				  if($thbl>=$periode){
					echo "<input type='hidden' name='i_kbank".$i."' id='i_kbank".$i."' value='$row->i_kbank'>";
					echo "<input type='hidden' name='i_area".$i."' id='i_area".$i."' value='$row->i_area'>";
					echo "<input type='hidden' name='i_coa_bank".$i."' id='i_coa_bank".$i."' value='$row->i_coa_bank'>";
					echo "<input type='hidden' name='v_sisa".$i."' id='v_sisa".$i."' value='$row->v_sisa'>";
					echo "<input type='hidden' name='d_bank".$i."' id='d_bank".$i."' value='$row->d_bank'>";
				    echo "<input type='checkbox' name='chk".$i."' id='chk".$i."' value='' onclick='pilihan(this.value,".$i.")'>";
				  }else{
					echo "<input hidden type='checkbox' name='chk".$i."' id='chk".$i."' value=''>";
				  }
				  echo "</td>
				</tr>";
				$x++;
				$i++;
			}
			echo "<tr>
			<td colspan='10' align='center'><input type='submit' id='proses' name='proses' value='Lanjut Proses' onclick = 'return dipales();'></td>
	  </tr>
	  <input type=\"hidden\" id=\"jml\" name=\"jml\" value=\"$i\">
	  <input type=\"hidden\" id=\"cekon\" name=\"cekon\" value='0'>";
		}
	      ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?=form_close()?>
		</td>
	</tr>
</table>
			<div id="pesan"></div>
<script language="javascript" type="text/javascript">
	function pilihan(a,b){
		var jml 	= document.getElementById("jml").value;	
		var cekon 	= document.getElementById("cekon").value;	
		var counter	= 1;

		if(a==''){
			document.getElementById("chk"+b).value='on';
			for(var i=0;i<jml;i++){
				//alert(document.getElementById("chk"+i).value);
				if(document.getElementById("chk"+i).value=='on'){
					document.getElementById("cekon").value = counter++;
				}
			}
		}else{
			document.getElementById("chk"+b).value='';
			for(var i=0;i<jml;i++){
				if(document.getElementById("chk"+i).value==''){
					document.getElementById("cekon").value = cekon-1;
				}
			}
		}
	}

	function pilihsemua(a){
		var jml 	= document.getElementById("jml").value;	
		var cekon 	= document.getElementById("cekon").value;	
		var counter	= 1;
		if(a == ''){
			for(var i=0;i<jml;i++){
				if((document.getElementById("thbl"+i).value)>=(document.getElementById("periode"+i).value)){
					document.getElementById("chk"+i).value='on';
					document.getElementById("chkall").value='on';
					document.getElementById("chk"+i).checked = true;
					
					if(document.getElementById("chk"+i).value=='on'){
						document.getElementById("cekon").value = counter++;
					}
				}else{
					document.getElementById("chk"+i).value='';
					document.getElementById("chk"+i).checked = false;
				}
			}
		}else{
			for(var i=0;i<jml;i++){
					document.getElementById("chk"+i).value='';
					document.getElementById("chk"+i).checked = false;
				}
					document.getElementById("cekon").value = 0;
					document.getElementById("chkall").value='';
			}
	}

function dipales() {
    cek = 'false';
    $s = 0;
    if ((document.getElementById("cekon").value == 0)) {
        alert('Cek data item minimal 1 !!!');
		$s = 1;
		return false;
	} else if ((document.getElementById("dkn").value == "")) {
		alert('Pilih Tanggal Terlebih Dahulu !!!');
		$s = 1;
		return false;
	} else {
		var kon = window.confirm("Simpan Data ?")

		if (kon) {
			cek = 'true';
			document.getElementById("proses").hidden = true;
			return true;
		}else{
			return false;
		}
	}
}
</script>