<?php 
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2010 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    Cell
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.2, 2010-01-11
 */


/** PHPExcel root directory */
if (!defined('ROOT')) {
	/**
	 * @ignore
	 */
	define('ROOT', dirname(__FILE__) . '/../../');
}

/** Cell */
require_once ROOT . 'PHPExcel/Cell.php';

/** Cell_IValueBinder */
require_once ROOT . 'PHPExcel/Cell/IValueBinder.php';

/** Cell_DefaultValueBinder */
require_once ROOT . 'PHPExcel/Cell/DefaultValueBinder.php';

/** Style_NumberFormat */
require_once ROOT . 'PHPExcel/Style/NumberFormat.php';

/** Shared_Date */
require_once ROOT . 'PHPExcel/Shared/Date.php';

/** Shared_String */
require_once ROOT . 'PHPExcel/Shared/String.php';


/**
 * Cell_AdvancedValueBinder
 *
 * @category   PHPExcel
 * @package    Cell
 * @copyright  Copyright (c) 2006 - 2010 PHPExcel (http://www.codeplex.com/PHPExcel)
 */
class Cell_AdvancedValueBinder extends Cell_DefaultValueBinder implements Cell_IValueBinder
{
	/**
	 * Bind value to a cell
	 *
	 * @param Cell $cell	Cell to bind value to
	 * @param mixed $value			Value to bind in cell
	 * @return boolean
	 */
	public function bindValue(Cell $cell, $value = null)
	{
		// sanitize UTF-8 strings
		if (is_string($value)) {
			$value = Shared_String::SanitizeUTF8($value);
		}

		// Find out data type
		$dataType = parent::dataTypeForValue($value);
		
		// Style logic - strings
		if ($dataType === Cell_DataType::TYPE_STRING && !$value instanceof RichText) {
			// Check for percentage
			if (preg_match('/^\-?[0-9]*\.?[0-9]*\s?\%$/', $value)) {
				// Convert value to number
				$cell->setValueExplicit( (float)str_replace('%', '', $value) / 100, Cell_DataType::TYPE_NUMERIC);
				
				// Set style
				$cell->getParent()->getStyle( $cell->getCoordinate() )->getNumberFormat()->setFormatCode( Style_NumberFormat::FORMAT_PERCENTAGE );
				
				return true;
			}
			
			// Check for time e.g. '9:45', '09:45'
			if (preg_match('/^(\d|[0-1]\d|2[0-3]):[0-5]\d$/', $value)) {
				list($h, $m) = explode(':', $value);
				$days = $h / 24 + $m / 1440;
				
				// Convert value to number
				$cell->setValueExplicit($days, Cell_DataType::TYPE_NUMERIC);
				
				// Set style
				$cell->getParent()->getStyle( $cell->getCoordinate() )->getNumberFormat()->setFormatCode( Style_NumberFormat::FORMAT_DATE_TIME3 );
				
				return true;
			}
			
			// Check for date
			if (strtotime($value) !== false) {
				// make sure we have UTC for the sake of strtotime
				$saveTimeZone = date_default_timezone_get();
				date_default_timezone_set('UTC');
				
				// Convert value to Excel date
				$cell->setValueExplicit( Shared_Date::PHPToExcel(strtotime($value)), Cell_DataType::TYPE_NUMERIC);
				
				// Set style
				$cell->getParent()->getStyle( $cell->getCoordinate() )->getNumberFormat()->setFormatCode( Style_NumberFormat::FORMAT_DATE_YYYYMMDD2 );
				
				// restore original value for timezone
				date_default_timezone_set($saveTimeZone);
				
				return true;
			}
		}

		// Not bound yet? Use parent...
		return parent::bindValue($cell, $value);
	}
}
