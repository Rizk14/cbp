<?php if (!defined('APPPATH')) exit('No direct script access allowed');

function view($view, $data = '')
{
    $CI = &get_instance();
    $CI->load->view($view, $data);
}

function session($id)
{
    $CI = &get_instance();
    return $CI->session->$id;
}

if (!function_exists('get_tax')) {
    function get_tax($tanggal)
    {
        $ci  = get_instance();
        $cek = $ci->db->query("
        SELECT
            *, (n_tax / 100 + 1) excl_divider, n_tax / 100 n_tax_val
        FROM
            public.tr_tax_amount 
        where '$tanggal' between d_start and d_finish");
        if ($cek->num_rows() > 0) {
            return $cek->row();
        } else {
            return null;
        }
    }
}

function is_image($filepath){
	if(file_exists($filepath)){
		$image_type = getimagesize($filepath);

		if(true == $image_type){
			if(in_array($image_type[2] , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP))){
				return true;
			}
		}
	}

	return false;
}
