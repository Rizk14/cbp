<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('salestomarketperkota');
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('salestomarketperkota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('salestomarketperkota');
			$this->load->view('salestomarketperkota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('salestomarketperkota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('salestomarketperkota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('salestomarketperkota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('salestomarketperkota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$iarea=strtoupper($this->input->post("iarea"));
			if($iarea=='')$iarea=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/kota/'.$iarea.'/index/';
			$query = $this->db->query(" select distinct(a.i_city), b.e_city_name from tr_kecamatan a, tr_city b where a.i_area = '$iarea' 
                                  and a.i_city=b.i_city and a.i_area=b.i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('salestomarketperkota/mmaster');
			$data['iarea']=$iarea;
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacakota($config['per_page'],$this->uri->segment(6),$iarea);
			$this->load->view('salestomarketperkota/vlistkota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$iarea		= $this->input->post("iarea",false);
			$data['iarea']		= $iarea;
			$cari		= strtoupper($this->input->post('cari',false));
			$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/kota/'.$iarea.'/index/';
			$query = $this->db->query("	select i_city, e_city_name from tr_city where i_area = '$iarea' 
                          and (upper(i_city) like '%$cari%' or upper(e_city_name) like '%$cari%')
												  order by i_city",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('salestomarketperkota/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->carikota($iarea,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('salestomarketperkota/vlistkota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu360')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
				$cari	  = strtoupper($this->input->post('cari'));
				$dfrom	= $this->input->post('dfrom');
				$dto	  = $this->input->post('dto');
				$iarea		= $this->input->post('iarea');
				$icity		= $this->input->post('icity');
				$is_cari	= $this->input->post('is_cari'); 
				
				if($dfrom=='') $dfrom=$this->uri->segment(4);
				if($dto=='') $dto=$this->uri->segment(5);
				if($iarea=='') $iarea=$this->uri->segment(6);
				if($icity=='') $icity=$this->uri->segment(7);
				
				if ($is_cari == '') $is_cari= $this->uri->segment(9);
				if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(8);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$icity.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/salestomarketperkota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$icity.'/index/';
			} 
			if ($is_cari != "1") {
				$sql= "  select distinct(a.i_product), a.e_product_name, sum(a.n_deliver) as total,
                a.v_unit_price, sum(a.n_deliver*a.v_unit_price) as njual, a.i_area, b.i_city, b.e_city_name
                from tm_nota_item a, tr_city b, tm_nota c, tr_customer d
                where a.i_area=b.i_area and b.i_city=d.i_city
                and a.i_nota=c.i_nota and c.i_customer=d.i_customer
                and a.i_area='$iarea' and b.i_city='$icity'
                and a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
                and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                group by a.i_product, a.e_product_name, a.v_unit_price,
                a.i_area, b.i_city, b.e_city_name
                order by a.i_product";
			}
			else {
				$sql= " select distinct(a.i_product), a.e_product_name, sum(a.n_deliver) as total,
                a.v_unit_price, sum(a.n_deliver*a.v_unit_price) as njual, a.i_area, b.i_city, b.e_city_name
                from tm_nota_item a, tr_city b, tm_nota c, tr_customer d
                where a.i_area=b.i_area and b.i_city=d.i_city
                and a.i_nota=c.i_nota and c.i_customer=d.i_customer
                and a.i_area='$iarea' and b.i_city='$icity'
                and a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
                and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                group by a.i_product, a.e_product_name, a.v_unit_price,
                a.i_area, b.i_city, b.e_city_name
                order by a.i_product";
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1"){
				$config['cur_page'] = $this->uri->segment(11);
			  $this->pagination->initialize($config);
			}else{
				$config['cur_page'] = $this->uri->segment(9);
			  $this->pagination->initialize($config);
      }

			
			$this->load->model('salestomarketperkota/mmaster');
			$data['page_title'] = $this->lang->line('salestomarketperkota');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['icity'] = $icity;

			if ($is_cari=="1"){
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$icity,$dfrom,$dto,$config['per_page'],$this->uri->segment(11),$cari);
			}else{
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$icity,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
      }

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Sales To Market Per Kota Area '.$iarea.' Kota:'.$icity.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('salestomarketperkota/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
