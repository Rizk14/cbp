<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjpkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select a.i_sjp from tm_sjp a
                                  inner join tr_area c on (a.i_area=c.i_area)
										              where (upper(a.i_sjp) like '%$cari%') and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsjp');
			$this->load->model('printsjpkhusus/mmaster');
			$data['isj']    = '';
			$data['cari']   = $cari;
			$data['detail'] = '';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$iuser);
			$this->load->view('printsjpkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isj  = $this->uri->segment(4);
			$area = $this->uri->segment(5);
			$this->load->model('printsjpkhusus/mmaster');
			$data['isj']=$isj;
			$data['page_title'] = $this->lang->line('printsjp');
			$data['isi']=$this->mmaster->baca($isj,$area);
			$data['detail']=$this->mmaster->bacadetail($isj,$area);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJP Area:'.$area.' No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjpkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsjp');
			$this->load->view('printsjpkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjpkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" select a.i_sjp from tm_sjp a
                                  inner join tr_area c on (a.i_area=c.i_area)
										              where (upper(a.i_sjp) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printsjpkhusus/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$data['page_title'] = $this->lang->line('printsjp');
			$data['cari']=$cari;
			$data['isj']='';
			$data['detail']='';
	 		$this->load->view('printsjpkhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
