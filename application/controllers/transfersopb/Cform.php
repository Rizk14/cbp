<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu420')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transferso').' Gudang PB';
			$store	= $this->session->userdata('store');
      if($store=='AA') $store='00';
			$data['isi']= directory_map('./sopb/'.$store);
			$data['file']='';
			$this->load->view('transfersopb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu420')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$tgl = $this->input->post('tgl', TRUE);
			$store	= $this->session->userdata('store');
      if($store=='AA') $store='00';
			$data['file']='sopb/'.$store.'/'.$file;
			$data['tgl'] =$tgl;
			$this->load->view('transfersopb/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu420')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $filespb = $this->input->post('fileso', TRUE);
      $this->load->model('transfersopb/mmaster');
      $this->db->trans_begin();
      $db1 =dbase_open($filespb,2) or die ("Could not open dbf file <i>$filespb</i>.");
      $this->db->trans_begin();
      if($db1){
        $record_numbers1=dbase_numrecords($db1);
        $noitem=0;
        for($j=1;$j<=$record_numbers1;$j++){
          $noitem++;
          $col=dbase_get_record_with_names($db1,$j);
          $iproduct		  = substr($col['KODEPROD'],0,7);

          $que=$this->db->query("select e_product_name from tr_product where i_product='$iproduct'", false);
          if($que->num_rows()>0){
        		$tes=$que->row();
            $eproductname=$tes->e_product_name;
          }      
		      $iproductgrade	= 'A';
		      $iproductmotif	= '00';
          if(isset($col['STOCKOPNAM'])){
            if(trim($col['STOCKOPNAM'])=='') $col['STOCKOPNAM']=0;
            $nstockopname   = $col['STOCKOPNAM'];
          }else{
            if(trim($col['STOCKOPNAM'])=='') $col['STOCKOPNAM']=0;
            $nstockopname   = $col['STOCKOPNAM'];
          }
          if(isset($col['STOCKOPNAM'])){
            if($col['STOCKOPNAM']<0) $col['STOCKOPNAME']=0;
          }
		      $nstockopname		= str_replace(',','',$nstockopname);
          $dstockopname 	= $this->input->post('tglso', TRUE);
			    if($dstockopname!=''){
				    $tmp=explode("-",$dstockopname);
				    $th=$tmp[2];
				    $bl=$tmp[1];
				    $hr=$tmp[0];
				    $dstockopname=$th."-".$bl."-".$hr;
            $thbl=substr($th,2,2).$bl;
			    }
          $iarea =substr($filespb,10,2);
			    $istore	= $this->session->userdata('store');
          if($istore=='AA') $istorelocation='01'; else $istorelocation='PB';
			    $istorelocationbin	= '00';
			    $jml			= $this->input->post('jml', TRUE);
			    $benar="false";
          if($noitem==1){
  			    $istockopname	=$this->mmaster->runningnumber($iarea,$thbl);
  			    $this->mmaster->insertheader($istockopname,$dstockopname,$istore,$istorelocation,$iarea);
          }
			      $this->mmaster->insertdetail($iproduct, $iproductgrade, $eproductname, $nstockopname,$istockopname, $istore, 
											                    $istorelocation, $istorelocationbin,$iproductmotif,$dstockopname,$iarea,$j);
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){              
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
#            $this->mmaster->inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$istockopname,$q_in,$q_out,$nstockopname,$q_aw,$q_ak);
            $emutasiperiode='20'.substr($istockopname,3,4);
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
#              $this->mmaster->updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$q_ak);
            }else{
#              $this->mmaster->insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nstockopname);
            }
			      $benar="true";
				    $data['sukses']=true;
				    $data['inomor']=$istockopname;
        }
      }

			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transfersopb/vformgagal',$data);
			}else{
				$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Transfer SO Gudang PB Area '.$iarea.' No:'.$istockopname;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('transfersopb/vformsukses',$data);
			}			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
