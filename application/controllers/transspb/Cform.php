<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu218')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transspb');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('transspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu218')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transspb');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');

      $daerah=$iarea;
      if($iarea=='20') $daerah='07';
      if($iarea=='26') $daerah='07';
      if($iarea=='06') $daerah='23';
      if($iarea=='24') $daerah='05';
      if($iarea=='22') $daerah='09';
      if($iarea=='18') $daerah='11';
      if($iarea=='19') $daerah='11';
      if($iarea=='21') $daerah='11';
      if($iarea=='29') $daerah='11';
      if($iarea=='30') $daerah='11';
      if($iarea=='32') $daerah='11';
      if($iarea=='14') $daerah='12';
      if($iarea=='15') $daerah='12';
      if($iarea=='21') $daerah='13';
      if($iarea=='28') $daerah='13';
      $db_file = 'spb/'.$iarea.'/fdspb'.$iarea.'.dbf';
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $dbase_definition = array (
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('KELOMPOK',  CHARACTER_FIELD,  2),
         array ('KODESALES',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  6),
         array ('TGLDOK',  DATE_FIELD),
         array ('PROGRAM',  BOOLEAN_FIELD),
         array ('NOPROGRAM',  CHARACTER_FIELD,  10),
         array ('TOP',  NUMBER_FIELD,  3,0),
         array ('DISCOUNT', NUMBER_FIELD, 7, 2),     
         array ('DISC2',  NUMBER_FIELD,  4, 1),  
         array ('DISC3',  NUMBER_FIELD,  4, 1),  
         array ('DISC4',  NUMBER_FIELD,  4, 1),  
         array ('POTONGAN', NUMBER_FIELD, 9, 0),
         array ('KOTOR', NUMBER_FIELD, 8, 0),
         array ('BABY', BOOLEAN_FIELD),
         array ('STOKDAERAH', BOOLEAN_FIELD),
         array ('NOSJ', CHARACTER_FIELD, 6),
         array ('TGLSJ', DATE_FIELD),
         array ('KONSINYASI', BOOLEAN_FIELD),
         array ('PO', CHARACTER_FIELD, 15),
         array ('KODEPROD', CHARACTER_FIELD, 9),
         array ('HARGASAT', NUMBER_FIELD, 7, 0),
         array ('JPESAN', NUMBER_FIELD, 7, 0),
         array ('JKIRIM', NUMBER_FIELD, 7, 0),
         array ('STOK', NUMBER_FIELD, 7, 0),
         array ('MOTIF', CHARACTER_FIELD, 40)
      );
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $per=substr($iperiode,2,4);
      $nospb='SPB-'.$per.'%';
      $sql	  = "select a.i_customer, a.i_product_group, a.i_salesman, a.i_spb, a.d_spb, a.f_spb_program, a.i_spb_program,
                 a.n_spb_toplength, a.n_spb_discount1, a.n_spb_discount2, a.n_spb_discount3, a.n_spb_discount4, 
                 a.v_spb_discounttotal, a.v_spb, a.f_spb_stockdaerah, a.i_sj, a.d_sj, a.f_spb_consigment, a.i_spb_po, a.i_price_group,
                 b.i_product, b.v_unit_price, b.n_order, b.n_deliver, b.n_stock, b.e_remark, c.i_price_group as harga 
                 from tm_spb_item b, tm_spb a
                 left join tm_promo c on(a.i_spb_program=c.i_promo)
                 where a.i_spb=b.i_spb and a.i_area=b.i_area and a.i_area='$iarea' and a.i_spb like '$nospb'
                 order by a.i_spb, b.n_item_no";
      $rspb		= $this->db->query($sql);
      $kodekas=false;
      foreach($rspb->result() as $rowpb){
	      $icustomer        = $rowpb->i_customer;
        if(substr($icustomer,2,3)=='000') $kodekas=true;
	      if($rowpb->i_product_group=='00'){
          $kelompok='1';
          $baby='F';
        }elseif($rowpb->i_product_group=='01'){
          $kelompok='2';
          $baby='T';
        }elseif($rowpb->i_product_group=='02'){
          $kelompok='3';
          $baby='T';
        }
        if($rowpb->f_spb_stockdaerah=='f') 
          $stockdaerah='F'; 
        else 
          $stockdaerah='T';
        if($rowpb->f_spb_program=='f') 
          $program='F'; 
        else 
          $program='T';
	      $isalesman        = $rowpb->i_salesman;
	      $ispb             = substr($rowpb->i_spb,9,6);
	      $dspb             = substr($rowpb->d_spb,0,4).substr($rowpb->d_spb,5,2).substr($rowpb->d_spb,8,2);
	      $ispbprogram      = substr($rowpb->i_spb_program,3,10);
	      $nspbtoplength    = $rowpb->n_spb_toplength;
	      $nspbdiscount1    = $rowpb->n_spb_discount1;
	      $nspbdiscount2    = $rowpb->n_spb_discount2;
	      $nspbdiscount3    = $rowpb->n_spb_discount3;
	      $nspbdiscount4    = $rowpb->n_spb_discount4;
	      $vspbdiscounttotal= $rowpb->v_spb_discounttotal;
	      $vspb             = $rowpb->v_spb;
	      $fspbstockdaerah  = $stockdaerah;
        $isj              = substr($rowpb->i_sj,8,6);
	      $dsj              = substr($rowpb->d_sj,0,4).substr($rowpb->d_sj,5,2).substr($rowpb->d_sj,8,2);
	      $fspbconsigment   = false;
	      $ispbpo           = $rowpb->i_spb_po;
        if(trim($rowpb->harga)!='' && $rowpb->harga!=null) $rowpb->i_price_group=$rowpb->harga;
	      $iproduct         = $rowpb->i_product.$rowpb->i_price_group;
	      $vunitprice       = $rowpb->v_unit_price;
	      $norder           = $rowpb->n_order;
	      $nstock           = $rowpb->n_stock;
	      $ndeliver         = 0;
	      $eremark          = $rowpb->e_remark;
        $isi = array ($icustomer,$kelompok,$isalesman,$ispb,$dspb,$program,$ispbprogram,$nspbtoplength,$nspbdiscount1,
                      $nspbdiscount2,$nspbdiscount3,$nspbdiscount4,$vspbdiscounttotal,$vspb,$baby,$fspbstockdaerah,
                      $isj,$dsj,$fspbconsigment,$ispbpo,$iproduct,$vunitprice,$norder,$ndeliver,$nstock,$eremark);
        dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
      }
      dbase_close($id);
      @chmod($db_file, 0777);

########################################################################################################################################################
      if($kodekas){
        $db_file = 'spb/'.$iarea.'/xlang'.$iarea.'.dbf';
        $dbase_definition = array (
           array ('NODOK', CHARACTER_FIELD, 6),
           array ('TGLDOK',  DATE_FIELD),
           array ('WILA',  CHARACTER_FIELD,  2),
           array ('NOLANG',  CHARACTER_FIELD,  3),
           array ('NAMA',  CHARACTER_FIELD,  25),
           array ('ALAMAT',  CHARACTER_FIELD,  50),
           array ('KOTA',  CHARACTER_FIELD,  25),
           array ('KODEPOS',  CHARACTER_FIELD,  5),
           array ('NMMILIK', CHARACTER_FIELD,  35),     
           array ('ALMMILIK',  CHARACTER_FIELD,  60),
           array ('NMPEMILIK', CHARACTER_FIELD,  35),     
           array ('ALMPEMILIK',  CHARACTER_FIELD,  60),
           array ('NPWP',  CHARACTER_FIELD,  15),
           array ('TELEPON',  CHARACTER_FIELD,  12),
           array ('FAX', CHARACTER_FIELD,  12),
           array ('TELEPON2', CHARACTER_FIELD,  12),
           array ('JENIS', NUMBER_FIELD,1,0),
           array ('JENIS2', NUMBER_FIELD,1,0),
           array ('DISCOUNT', NUMBER_FIELD, 7,0),

           array ('BAYAR', NUMBER_FIELD,4,0),
           array ('PKP', NUMBER_FIELD,1,0),
           array ('PKP2', BOOLEAN_FIELD),
           array ('KODESALES', CHARACTER_FIELD, 2),
           array ('TGLDAFTAR', DATE_FIELD),
           array ('CPERSON', CHARACTER_FIELD, 25),
           array ('EMAIL', CHARACTER_FIELD, 25),
           array ('ADAMEMO', BOOLEAN_FIELD),
           array ('MEMO', CHARACTER_FIELD, 255),
           array ('NONPWPBARU', CHARACTER_FIELD, 20),
           array ('PLUSPPN', BOOLEAN_FIELD),
           array ('PLUSDISC', BOOLEAN_FIELD),
           array ('NPWPBARU', BOOLEAN_FIELD),

           array ('TGLDAFTAR',  DATE_FIELD),
           array ('BRGDIJUAL', CHARACTER_FIELD, 15),
           array ('CARAJUAL', CHARACTER_FIELD, 15),
           array ('UKURUSAHA', CHARACTER_FIELD, 15),
           array ('BTKLAYAN', CHARACTER_FIELD, 15),
           array ('KOMPETITOR', CHARACTER_FIELD, 60)

        );
        $create = @ dbase_create($db_file, $dbase_definition)
                  or die ("Could not create dbf file <i>$db_file</i>.");
        $id = @ dbase_open ($db_file, READ_WRITE)
              or die ("Could not open dbf file <i>$db_file</i>."); 

        $sql	  = "select a.i_spb, a.d_spb, a.i_area, a.i_customer, substring(a.i_customer,3,3) as nolang, e_customer_name,
                   e_customer_address, e_customer_kota1, e_postal1, e_customer_npwpname, e_customer_npwpaddress, e_customer_owner,
                   e_customer_owneraddress, c.e_customer_pkpnpwp, e_customer_phone, e_fax1, e_customer_phone2, 
                   i_customer_class, i_customer_class, n_customer_discount, c.n_spb_toplength, c.f_spb_pkp, a.i_salesman, d_customer_entry, 
                   e_customer_contact, e_customer_mail, e_customer_refference, f_customer_plusdiscount, f_customer_plusppn,
                   b.e_customer_producttypename, d.e_customer_salestypename, e.e_customer_gradename, 
                   f.e_customer_servicename, e_kompetitor1, e_kompetitor2, e_kompetitor3
                   from tm_spb a, tr_customer_grade e, tr_customer_service f, tr_customer_tmp c, tr_customer_salestype d,
                   tr_customer_producttype b
                   where a.i_area='$iarea' and a.i_spb like '$nospb' and a.i_spb=c.i_spb and a.i_area=c.i_area
                   and c.i_customer_salestype=d.i_customer_salestype and c.i_customer_grade=e.i_customer_grade
                   and c.i_customer_service=f.i_customer_service and b.i_customer_producttype=c.i_customer_producttype";
        $rspb		= $this->db->query($sql);
        foreach($rspb->result() as $rowpb){
	        $NODOK=substr($rowpb->i_spb,9,6);
          $TGLDOK=substr($rowpb->d_spb,0,4).substr($rowpb->d_spb,5,2).substr($rowpb->d_spb,8,2);
          $WILA=$rowpb->i_area;
          $NOLANG=substr($rowpb->i_customer,2,3);
          $NAMA=$rowpb->e_customer_name;
          $ALAMAT=$rowpb->e_customer_address;
          $KOTA=$rowpb->e_customer_kota1;
          $KODEPOS=$rowpb->e_postal1;
          if($rowpb->f_spb_pkp=='t'){
            $NMMILIK=$rowpb->e_customer_npwpname;
            $ALMMILIK=$rowpb->e_customer_npwpaddress;
          }else{
            $NMMILIK=' ';
            $ALMMILIK=' ';
          }
          $NMPEMILIK=$rowpb->e_customer_owner;
          $ALMPEMILIK=$rowpb->e_customer_owneraddress;
          $NPWP=$rowpb->e_customer_pkpnpwp;
          $TELEPON=$rowpb->e_customer_phone;
          $FAX=$rowpb->e_fax1;
          $TELEPON2=$rowpb->e_customer_phone2;
          $JENIS=$rowpb->i_customer_class;
          $JENIS2=$rowpb->i_customer_class;
          $DISCOUNT=$rowpb->n_customer_discount;
          $BAYAR=$rowpb->n_spb_toplength;
          if($rowpb->f_spb_pkp=='t')
            $nopkp=1;
          else
            $nopkp=2;
          $PKP=$nopkp;
          $PKP2=$rowpb->f_spb_pkp;
          $KODESALES=$rowpb->i_salesman;
          $TGLDAFTAR=$rowpb->d_customer_entry;
          $CPERSON=$rowpb->e_customer_contact;
          $EMAIL=$rowpb->e_customer_mail;
          if($rowpb->e_customer_refference!='') $adamemo=TRUE;
          $ADAMEMO=$rowpb->i_area;
          $MEMO=substr($rowpb->e_customer_refference,0,254);
          $NONPWPBARU=$rowpb->e_customer_pkpnpwp;
          $PLUSPPN=$rowpb->f_customer_plusppn;
          $PLUSDISC=$rowpb->f_customer_plusdiscount;
          if($rowpb->e_customer_pkpnpwp!='')
            $NPWPBARU=TRUE;
          else
            $NPWPBARU=FALSE;
          
          $BRGDIJUAL = $rowpb->e_customer_producttypename;
          $CARAJUAL  = $rowpb->e_customer_salestypename;
          $UKURUSAHA = $rowpb->e_customer_gradename;
          $BTKLAYAN  = $rowpb->e_customer_servicename;
          $KOMPETITOR= $rowpb->e_kompetitor1.' '.$rowpb->e_kompetitor2.' '.$rowpb->e_kompetitor3;
          $isi = array ($NODOK,$TGLDOK,$WILA,$NOLANG,$NAMA,$ALAMAT,$KOTA,$KODEPOS,$NMMILIK,$ALMMILIK,$NMPEMILIK,$ALMPEMILIK,$NPWP,$TELEPON,
                        $FAX,$TELEPON2,$JENIS,$JENIS2,$DISCOUNT,$BAYAR,$PKP,$PKP2,$KODESALES,$TGLDAFTAR,$CPERSON,$EMAIL,$ADAMEMO,$MEMO,
                        $NONPWPBARU,$PLUSPPN,$PLUSDISC,$NPWPBARU,$TGLDAFTAR,$BRGDIJUAL,$CARAJUAL,$UKURUSAHA,$BTKLAYAN,$KOMPETITOR);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
        dbase_close($id);
      }

########################################################################################################################################################
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke SPB lama Area '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu218')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transspb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu218')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/transspb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
