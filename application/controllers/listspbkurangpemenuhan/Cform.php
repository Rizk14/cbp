<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu327')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbkurangpemenuhan');
#			$data['dfrom']='';
#			$data['dto']='';
			$data['iarea']='';
			$this->load->view('listspbkurangpemenuhan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu327')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbkurangpemenuhan');
			$this->load->view('listspbkurangpemenuhan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu327')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listspbkurangpemenuhan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listspbkurangpemenuhan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu327')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspbkurangpemenuhan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listspbkurangpemenuhan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu327')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
#			$dfrom	= $this->input->post('dfrom');
#			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari'); 
				
#			if($dfrom=='') $dfrom=$this->uri->segment(4);
#			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(4);
				
			if ($is_cari == '')
				$is_cari= $this->uri->segment(6);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(5);
			
			if ($is_cari=="1") { 
#				$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
				$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/view2/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
#				$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
				$config['base_url'] = base_url().'index.php/listspbkurangpemenuhan/cform/view2/'.$iarea.'/index/';
			} 
			
      $query 	= $this->db->query("SELECT to_char(current_timestamp,'yyyy/mm/dd') as c");
      $row   	= $query->row();
      $tglsys	= $row->c;
      $tmp    = $this->fungsi->dateAdd("d",-7,$tglsys);
      $tmp 	  = explode("-", $tmp);
			$det1	  = $tmp[2];
			$mon1	  = $tmp[1];
			$yir1 	= $tmp[0];
			$dakhir	= $yir1."-".$mon1."-".$det1;

			if ($is_cari != "1") {
				$sql= " select distinct a.*, c.e_customer_name
                from tm_spb a
                inner join tm_spb_item b on (a.i_spb=b.i_spb and a.i_area=b.i_area)
                inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                where not a.i_sj is null and not a.i_nota is null
                and a.f_spb_cancel='f' and b.n_deliver < b.n_order
                and a.i_area='$iarea'";
/*
and
(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
*/
			}
			else {
				$sql= " select distinct a.*, c.e_customer_name
                from tm_spb a
                inner join tm_spb_item b on (a.i_spb=b.i_spb and a.i_area=b.i_area)
                inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                where not a.i_sj is null and not a.i_nota is null
                and a.f_spb_cancel='f' and b.n_deliver < b.n_order
                and a.i_area='$iarea' AND
                (upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%')";
/*
and
(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
a.d_spb <= to_date('$dto','dd-mm-yyyy'))
*/
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
#				$config['cur_page'] = $this->uri->segment(10);
				$config['cur_page'] = $this->uri->segment(8);
			else
#				$config['cur_page'] = $this->uri->segment(8);
				$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			
			$this->load->model('listspbkurangpemenuhan/mmaster');
			$data['page_title'] = $this->lang->line('listspbkurangpemenuhan');
			$data['cari']	= $cari;
#			$data['dfrom']	= $dfrom;
#			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari=="1")
#				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(8),$cari);
			else
#				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(6),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Kurang Pemenuhan Area '.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listspbkurangpemenuhan/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
