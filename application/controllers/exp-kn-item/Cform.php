<?php
class Cform extends CI_Controller
{
	public $title = "Export KN/DN Per Item";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
		$this->load->model('exp-kn-item/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu305') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['iperiode']	= '';
			$data['iarea']	  = '';

			$this->load->view('exp-kn-item/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu305') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			#      $iarea = $this->input->post('iarea');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			#			if($iarea=='') $iarea=$this->uri->segment(5);

			/* COMMENT 18 JAN 2023 GANTI QUERY KARENA PAJAKNYA BELUM DINAMIS */
			// $this->db->select(" DISTINCT a.d_kn,	a.i_kn,	c.i_customer, c.e_customer_name,b.e_area_name,e.e_salesman_name,d.i_product,
			// 					d.e_product_name, d.n_quantity,	d.v_unit_price,
			// 					(n_quantity*v_unit_price) as jumlah,
			// 					((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity) as v_discount,
			// 					((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity)) as total,
			// 					(((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/(1.1)) as dpp,
			// 					((((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/(1.1))*(0.1)) as ppn,
			// 					(((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/(1.1)) + ( (((n_quantity*v_unit_price)-v_discount)/(1.1))*(0.1)) as piutang,
			// 					d.i_bbm, a.e_remark
			// 					from tm_kn a, tr_area b, tr_customer c, tm_bbm_item d, tr_salesman e
			// 					where a.i_area=b.i_area	
			// 					and a.i_customer=c.i_customer
			// 					and a.i_refference=d.i_bbm
			// 					and a.i_salesman = e.i_salesman
			// 					and to_char(a.d_kn,'yyyymm')='$iperiode'
			// 					and a.f_kn_cancel = 'f'
			// 					group by a.i_kn, a.d_kn, b.e_area_name, c.e_customer_name,d.i_product, d.i_bbm, d.e_product_name, d.v_unit_price, 
			// 					d.n_quantity, a.v_discount, e.i_salesman,c.i_customer, e.e_salesman_name, a.e_remark, a.v_netto, a.v_gross
			// 					order by e.e_salesman_name",false);

			$this->db->select(" DISTINCT a.d_kn,	a.i_kn,	c.i_customer, c.e_customer_name,b.e_area_name,e.e_salesman_name,d.i_product,
								d.e_product_name, d.n_quantity,	d.v_unit_price,
								(n_quantity*v_unit_price) as jumlah,
								((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity) as v_discount,
								((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity)) as total,
								(((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/coalesce(((n_tax/100)+1),1.0)) as dpp,
								((((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/COALESCE(((n_tax/100)+1),1.0))*coalesce((n_tax/100),0.1)) as ppn,
								(((n_quantity*v_unit_price)-((((100-((a.v_netto/a.v_gross)*100)) * d.v_unit_price)/ 100) * d.n_quantity))/coalesce(((n_tax/100)+1),1.0)) + ((((n_quantity*v_unit_price)-v_discount)/coalesce(((n_tax/100)+1),1.0))*coalesce((n_tax/100),0.1)) as piutang,
								d.i_bbm, a.e_remark
								from tm_kn a
								LEFT JOIN tm_nota f ON a.i_pajak = f.i_seri_pajak 
								LEFT JOIN tr_tax_amount tx ON f.d_nota BETWEEN d_start AND d_finish, tr_area b, tr_customer c, tm_bbm_item d, tr_salesman e
								where a.i_area=b.i_area	
								and a.i_customer=c.i_customer
								and a.i_refference=d.i_bbm
								and a.i_salesman = e.i_salesman
								and to_char(a.d_kn,'yyyymm')='$iperiode'
								and a.f_kn_cancel = 'f'
								group by a.i_kn, a.d_kn, b.e_area_name, c.e_customer_name,d.i_product, d.i_bbm, d.e_product_name, d.v_unit_price, 
								d.n_quantity, a.v_discount, e.i_salesman,c.i_customer, e.e_salesman_name, a.e_remark, a.v_netto, a.v_gross, tx.n_tax
								order by e.e_salesman_name", false);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar KN Per-item")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(45);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar KN Per item');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:R5'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No KN');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Kod Lang');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Lang');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kd Product');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama product');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Diskon');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'DPP');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Piutang');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'BBM');
				$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R5', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':R' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->d_kn, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_kn, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->n_quantity, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->v_unit_price, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->jumlah, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->v_discount, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->total, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->dpp, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, $row->ppn, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->piutang, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('Q' . $i, $row->i_bbm, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('R' . $i, $row->e_remark, Cell_DataType::TYPE_STRING);

					$i++;
					$j++;
				}
				$x = $i - 1;
			}

			$nama = 'KNitem' . $iperiode . '.xls';

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');

			// $objWriter->save('excel/00/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export KN/DN item:' . $iperiode;
			// $this->load->model('logger');

			$this->logger->writenew('Export KN/DN item:' . $iperiode);

			// $data['sukses'] = true;
			// $data['inomor']	= "Export KN-item " . $peri;
			// $this->load->view('nomor', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
