<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listmeterai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
            $kemana 	= '';
            $where 		= '';
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url().'index.php/listmeterai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			if($iarea != 'NA'){
				$where = "AND a.i_area = '$iarea'";
			}

			$query = $this->db->query(" 
				select * FROM (
					SELECT a.i_alokasi, c.i_nota, to_char(c.d_nota, 'dd-mm-yyyy') as d_nota, to_char(c.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, to_char(a.d_alokasi, 'dd-mm-yyyy') as d_alokasi,
					d.e_area_name, c.i_customer, e.e_customer_name, e.e_customer_contact || 
					case when e.e_customer_contact is not null and e.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
					case when e.e_customer_phone is not null and e.e_customer_phone <> '' then '(' || e.e_customer_phone || ')' end as contact, f.e_salesman_name, 
					c.v_nota_netto,c.v_sisa,c.v_materai, b.v_jumlah as v_materai_sisa, n_nota_toplength as top  from tm_alokasimt a
					inner join tm_alokasimt_item b on (a.i_alokasi = b.i_alokasi and a.i_kbank = b.i_kbank and a.i_area = b.i_area)
					inner join tm_nota c on (b.i_nota = c.i_nota /* and b.i_area = c.i_area */)
					inner join tr_area d on (c.i_area = d.i_area)
					inner join tr_customer e on (e.i_area = c.i_area and e.i_customer = c.i_customer)
					inner join tr_salesman f on (c.i_salesman = f.i_salesman)
					where c.v_materai > 0 and a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
					and (c.i_nota ilike '%$cari%' or to_char(c.d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f' and a.f_alokasi_cancel = 'f'
				UNION all
					SELECT a.i_alokasi, c.i_nota, to_char(c.d_nota, 'dd-mm-yyyy') as d_nota, to_char(c.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, to_char(a.d_alokasi, 'dd-mm-yyyy') as d_alokasi,
					d.e_area_name, c.i_customer, e.e_customer_name, e.e_customer_contact || 
					case when e.e_customer_contact is not null and e.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
					case when e.e_customer_phone is not null and e.e_customer_phone <> '' then '(' || e.e_customer_phone || ')' end as contact, f.e_salesman_name, 
					c.v_nota_netto,c.v_sisa,c.v_materai, b.v_jumlah as v_materai_sisa, n_nota_toplength as top  from tm_meterai a
					inner join tm_meterai_item b on (a.i_alokasi = b.i_alokasi and a.i_area = b.i_area)
					inner join tm_nota c on (b.i_nota = c.i_nota)
					inner join tr_area d on (c.i_area = d.i_area)
					inner join tr_customer e on (e.i_area = c.i_area and e.i_customer = c.i_customer)
					inner join tr_salesman f on (c.i_salesman = f.i_salesman)
					where c.v_materai > 0 and a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
					and (c.i_nota ilike '%$cari%' or to_char(c.d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f' and a.f_alokasi_cancel = 'f'
			) AS a
			order by a.e_area_name, a.d_nota ",false);
			/* $query = $this->db->query(" 
				select c.i_nota, to_char(c.d_nota, 'dd-mm-yyyy') as d_nota, to_char(c.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, to_char(a.d_alokasi, 'dd-mm-yyyy') as d_alokasi,
				d.e_area_name, c.i_customer, e.e_customer_name, e.e_customer_contact || 
				case when e.e_customer_contact is not null and e.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
				case when e.e_customer_phone is not null and e.e_customer_phone <> '' then '(' || e.e_customer_phone || ')' end as contact, f.e_salesman_name, 
				c.v_materai, b.v_jumlah as v_materai_sisa, n_nota_toplength as top  from tm_alokasimt a
				inner join tm_alokasimt_item b on (a.i_alokasi = b.i_alokasi and a.i_kbank = b.i_kbank and a.i_area = b.i_area)
				inner join tm_nota c on (b.i_nota = c.i_nota and b.i_area = c.i_area)
				inner join tr_area d on (c.i_area = d.i_area)
				inner join tr_customer e on (e.i_area = c.i_area and e.i_customer = c.i_customer)
				inner join tr_salesman f on (c.i_salesman = f.i_salesman)
				where c.v_materai > 0 and a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
				and (c.i_nota ilike '%$cari%' or to_char(c.d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f' and a.f_alokasi_cancel = 'f'
				order by d.e_area_name, c.d_nota ",false); */
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '9999'; 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['cari']		= $cari;
			$data['keyword']	= '';	
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['kemana']		= $kemana;
			$data['isi']		= $this->mmaster->bacaperiode($where,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$nolimit = $this->mmaster->bacaperiode_nolimit($where,$dfrom,$dto,$cari);
			
			$tmp_meterai = 0;
			$tmp_sisameterai = 0;

			if ($nolimit) {
				foreach ($nolimit as $row) {
					$tmp_meterai += $row->v_materai;
					$tmp_sisameterai += $row->v_materai_sisa; 
				}
			}

			$data['tmp_meterai']		= $tmp_meterai;
			$data['tmp_sisameterai']	= $tmp_sisameterai;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Pemakaian Materai Area '.$iarea.' Tanggal:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listmeterai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	/* function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			$nt   = $this->input->post('chkntx');
            $al   = $this->input->post('chkalx');
            $kemana = '';
            $where = '';
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($nt=='') $nt=$this->uri->segment(7);
			if($al=='') $al=$this->uri->segment(8);

			$config['base_url'] = base_url().'index.php/listmeterai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$nt.'/'.$al.'/';
			if($iarea != 'NA'){
				$where = "AND a.i_area = '$iarea'";
			}

			if ($nt == 'qqq') {
				$query = $this->db->query(" 
				select a.i_nota, to_char(a.d_nota, 'dd-mm-yyyy') as d_nota, to_char(a.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, b.e_area_name, a.i_customer, c.e_customer_name, c.e_customer_contact || 
				case when c.e_customer_contact is not null and c.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
				case when c.e_customer_phone is not null and c.e_customer_phone <> '' then '(' || c.e_customer_phone || ')' end as contact, d.e_salesman_name, 
				a.v_materai, a.v_materai_sisa, n_nota_toplength as top
				from tm_nota a 
				inner join tr_area b on (a.i_area = b.i_area)
				inner join tr_customer c on (a.i_area = c.i_area and a.i_customer = c.i_customer)
				inner join tr_salesman d on (a.i_salesman = d.i_salesman)
				where a.v_materai > 0 and d_nota between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
				and (i_nota ilike '%$cari%' or to_char(d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f'
				order by b.e_area_name, d_nota ",false);
				$kemana = 'vformviewnota';
			} else if ($al == 'qqq') {
				$query = $this->db->query(" 
				select c.i_nota, to_char(c.d_nota, 'dd-mm-yyyy') as d_nota, to_char(c.d_jatuh_tempo, 'dd-mm-yyyy') as d_jatuh_tempo, to_char(a.d_alokasi, 'dd-mm-yyyy') as d_alokasi,
				d.e_area_name, c.i_customer, e.e_customer_name, e.e_customer_contact || 
				case when e.e_customer_contact is not null and e.e_customer_contact <> '' then ' - ' end || e_customer_contactgrade || 
				case when e.e_customer_phone is not null and e.e_customer_phone <> '' then '(' || e.e_customer_phone || ')' end as contact, f.e_salesman_name, 
				c.v_materai, b.v_jumlah as v_materai_sisa, n_nota_toplength as top  from tm_alokasimt a
				inner join tm_alokasimt_item b on (a.i_alokasi = b.i_alokasi and a.i_kbank = b.i_kbank and a.i_area = b.i_area)
				inner join tm_nota c on (b.i_nota = c.i_nota and b.i_area = c.i_area)
				inner join tr_area d on (c.i_area = d.i_area)
				inner join tr_customer e on (e.i_area = c.i_area and e.i_customer = c.i_customer)
				inner join tr_salesman f on (c.i_salesman = f.i_salesman)
				where c.v_materai > 0 and a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') $where
				and (c.i_nota ilike '%$cari%' or to_char(c.d_nota, 'dd-mm-yyyy') ilike '%$cari%' or e_customer_name ilike '%$cari%' or e_customer_contact ilike '%$cari%' or e_area_name ilike '%$cari%') and f_nota_cancel = 'f' and a.f_alokasi_cancel = 'f'
				order by d.e_area_name, c.d_nota ",false);
				$kemana = 'vformviewalokasi';
			}
			
#and a.f_ttb_tolak='f'
#and a.f_nota_koreksi='f'
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['cari']		= $cari;
			$data['keyword']	= '';	
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['kemana']		= $kemana;
			$data['chkntx']		= $nt;
			$data['chkalx']		= $al;
			$data['isi']		= $this->mmaster->bacaperiode($where,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari, $nt, $al);

			$nolimit = $this->mmaster->bacaperiode_nolimit($where,$dfrom,$dto,$cari, $nt, $al);
			
			$tmp_meterai = 0;
			$tmp_sisameterai = 0;

			if ($nolimit) {
				foreach ($nolimit as $row) {
					$tmp_meterai += $row->v_materai;
					$tmp_sisameterai += $row->v_materai_sisa; 
				}
			}

			$data['tmp_meterai']		= $tmp_meterai;
			$data['tmp_sisameterai']		= $tmp_sisameterai;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Pemakaian Materai Area '.$iarea.' Tanggal:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listmeterai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 */

	/* function exportexcel(){
		$this->load->model('listmeterai/mmaster');
		$dfrom 		= $this->uri->segment(4);
		$dto 		= $this->uri->segment(5);
		$iarea		= $this->uri->segment(6);
		//$nt		= $this->uri->segment(7);
		//$al		= $this->uri->segment(8);
		$cari = '';
		$where = '';
		$nama_area = 'Nasional';
		if($iarea != 'NA'){
			$where = "AND a.i_area = '$iarea'";
			$nama_area   	= $this->mmaster->get_area($iarea);
		}

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$ObjPHPExcel = new PHPExcel();

		$query=$this->mmaster->bacaperiode_nolimit($where,$dfrom,$dto,$cari);

		if($query){
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept");

			$ObjPHPExcel->getProperties()
			->setTitle("Pemakaian Meterai")
			->setSubject("Pemakaian Meterai")
			->setDescription("Pemakaian Meterai")
			->setKeywords("Meterai By Nota")
			->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			//$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2'
			);

			$header = array(       
				'font' => array(
					'name'=>'Times New Roman',
					'bold'=>true,
					'italic'=>false,
					'size'=>12
				),
				'alignment'=>array(
					'horizontal'=>Style_Alignment::HORIZONTAL_CENTER,
					'vertical'=>Style_Alignment::VERTICAL_CENTER,
					'wrap'=>true
				)
			);

			$footer = array(
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(4);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);

			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Pemakaian Meterai (Tgl Nota)');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');
			$ObjPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($header);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', 'AREA : ' . $nama_area);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');
			$ObjPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($header);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Periode : ".$dfrom." s/d ".$dto);

			$style_col = array(       
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_col1 = array(       
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row1 = array(
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)

				),
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => false,
					'italic'=> false,
					'size'  => 10
				),
			);

			$style_row = array(
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => false,
					'italic'=> false,
					'size'  => 9
				),
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'No Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tanggal Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Tanggal Jatuh Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Area');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Top');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Nama Salesman');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Nilai Meterai');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'Sisa Meterai');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($style_col);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:J6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');
			// foreach(range('A','I') as $columnID) {
			// 	$ObjPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			// }

			$i 	= 1;
			$j 	= 7;
			$nilai = 0;
			$sisa = 0;
			foreach($query as $row){
				$nilai += $row->v_materai;
				$sisa += $row->v_materai_sisa;
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_nota);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->d_nota);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->d_jatuh_tempo);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_area_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$j, '('.$row->i_customer.') '. $row->e_customer_name, Cell_DataType::TYPE_STRING);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('G'.$j, $row->top);
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->e_salesman_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->v_materai);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row1);
				$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row->v_materai_sisa);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($style_row1);
				$i++;
				$j++;
			}

			$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $nilai);
			$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row1);
			$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $sisa);
			$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($style_row1);

			$nama_file = "Pemakaian_Meterai_By_TglNota_".$dfrom."_".$dto.".xls";
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');    
			header('Content-Disposition: attachment; filename='.$nama_file.''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$ObjWriter->save('php://output','w');

		}
	}	 */

	function exportexcelalokasi(){
		$this->load->model('listmeterai/mmaster');
		$dfrom 		= $this->uri->segment(4);
		$dto 		= $this->uri->segment(5);
		$iarea		= $this->uri->segment(6);
		// $nt		= $this->uri->segment(7);
		// $al		= $this->uri->segment(8);
		$cari = '';
		$where = '';
		$nama_area = 'Nasional';
		if($iarea != 'NA'){
			$where = "AND a.i_area = '$iarea'";
			$nama_area   	= $this->mmaster->get_area($iarea);
		}

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$ObjPHPExcel = new PHPExcel();

		$query=$this->mmaster->bacaperiode_nolimit($where,$dfrom,$dto,$cari);

		if($query){
			$ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
			$ObjPHPExcel->getProperties()->setCreator("M.I.S Dept");
			$ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept");

			$ObjPHPExcel->getProperties()
			->setTitle("Pemakaian Meterai")
			->setSubject("Pemakaian Meterai")
			->setDescription("Pemakaian Meterai")
			->setKeywords("Meterai")
			->setCategory("Laporan");

			$ObjPHPExcel->setActiveSheetIndex(0);
			//$ObjPHPExcel->createSheet();

			$ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2'
			);

			$header = array(       
				'font' => array(
					'name'=>'Times New Roman',
					'bold'=>true,
					'italic'=>false,
					'size'=>12
				),
				'alignment'=>array(
					'horizontal'=>Style_Alignment::HORIZONTAL_CENTER,
					'vertical'=>Style_Alignment::VERTICAL_CENTER,
					'wrap'=>true
				)
			);

			$footer = array(
				'font' => array(
					'name'	=> 'Arial',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3); //NO
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15); //NO NOTA
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12); //TGL NOTA
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15); //TGL JT
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); //NAMA AREA
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10); //KODE PELANGGAN
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30); //NAMA PELANGGAN
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);  //TOP
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25); //NAMA SALESMAN
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); //NILAI NOTA
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20); //SISA NOTA
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11); //NILAI METERAI
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11); //SISA METERAI
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13); //NO ALOKASI
			$ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(13); //TGL ALOKASI

			$ObjPHPExcel->getActiveSheet()->mergeCells('A2:O2');
			$ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Pemakaian Meterai');
			$ObjPHPExcel->getActiveSheet()->mergeCells('A3:O3');
			$ObjPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($header);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A3', 'AREA : ' . $nama_area);
			$ObjPHPExcel->getActiveSheet()->mergeCells('A4:O4');
			$ObjPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray($header);
			$ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Periode : ".$dfrom." s/d ".$dto);

			$style_col = array(       
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_col1 = array(       
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => true,
					'italic'=> false,
					'size'  => 10
				),					
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$style_row1 = array(
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)

				),
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => false,
					'italic'=> false,
					'size'  => 10
				),
			);

			$style_row = array(
				'font' => array(
					'name'	=> 'Times New Roman',
					'bold'  => false,
					'italic'=> false,
					'size'  => 9
				),
				'borders' => array(
					'top' 	=> array('style' => Style_Border::BORDER_THIN),
					'bottom'=> array('style' => Style_Border::BORDER_THIN),
					'left'  => array('style' => Style_Border::BORDER_THIN),
					'right' => array('style' => Style_Border::BORDER_THIN)
				),
				'alignment' => array(
					// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
					'vertical'  => Style_Alignment::VERTICAL_CENTER
				)
			);

			$ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
			$ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'No Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'Tanggal Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'Tanggal Jatuh Tempo');
			$ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Area');
			$ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'Kode Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'Nama Pelanggan');
			$ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'Top');
			$ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'Salesman');
			$ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'Nilai Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('K6', 'Sisa Nota');
			$ObjPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'Nilai Meterai');
			$ObjPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('M6', 'Sisa Meterai');
			$ObjPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('N6', 'No Alokasi');
			$ObjPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray($style_col);
			$ObjPHPExcel->getActiveSheet()->setCellValue('O6', 'Tanggal Alokasi');
			$ObjPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray($style_col);

			$ObjPHPExcel->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');
			// foreach(range('A','I') as $columnID) {
			// 	$ObjPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
			// }

			$i 	= 1;
			$j 	= 7;
			$nilai = 0;
			$sisa = 0;
			foreach($query as $row){
				$nilai += $row->v_materai;
				$sisa += $row->v_materai_sisa;
				$ObjPHPExcel->getActiveSheet()->setCellValue('A'.$j, $i);
				$ObjPHPExcel->getActiveSheet()->getStyle('A'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('B'.$j, $row->i_nota);
				$ObjPHPExcel->getActiveSheet()->getStyle('B'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('C'.$j, $row->d_nota);
				$ObjPHPExcel->getActiveSheet()->getStyle('C'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('D'.$j, $row->d_jatuh_tempo);
				$ObjPHPExcel->getActiveSheet()->getStyle('D'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('E'.$j, $row->e_area_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$j, '('.$row->i_customer.') ', Cell_DataType::TYPE_STRING);
				$ObjPHPExcel->getActiveSheet()->getStyle('F'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$j, $row->e_customer_name, Cell_DataType::TYPE_STRING);
				$ObjPHPExcel->getActiveSheet()->getStyle('G'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('H'.$j, $row->top);
				$ObjPHPExcel->getActiveSheet()->getStyle('H'.$j)->applyFromArray($style_row);
				$ObjPHPExcel->getActiveSheet()->setCellValue('I'.$j, $row->e_salesman_name);
				$ObjPHPExcel->getActiveSheet()->getStyle('I'.$j)->applyFromArray($style_row);
				
				$ObjPHPExcel->getActiveSheet()->setCellValue('J'.$j, $row->v_nota_netto);
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
				$ObjPHPExcel->getActiveSheet()->getStyle('J'.$j)->applyFromArray($style_row1);

				$ObjPHPExcel->getActiveSheet()->setCellValue('K'.$j, $row->v_sisa);
				$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$ObjPHPExcel->getActiveSheet()->getStyle('K'.$j)->applyFromArray($style_row1);

				$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, $row->v_materai);
				$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($style_row1);
				
				$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$j, $row->v_materai_sisa);
				$ObjPHPExcel->getActiveSheet()->getStyle('M'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$ObjPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($style_row1);

				$ObjPHPExcel->getActiveSheet()->setCellValue('N'.$j, $row->i_alokasi);
				$ObjPHPExcel->getActiveSheet()->getStyle('N'.$j)->applyFromArray($style_row);

				$ObjPHPExcel->getActiveSheet()->setCellValue('O'.$j, $row->d_alokasi);
				$ObjPHPExcel->getActiveSheet()->getStyle('O'.$j)->applyFromArray($style_row);
				
				$i++;
				$j++;
			}

			$ObjPHPExcel->getActiveSheet()->setCellValue('L'.$j, $nilai);
			$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$ObjPHPExcel->getActiveSheet()->getStyle('L'.$j)->applyFromArray($style_row1);
				
			$ObjPHPExcel->getActiveSheet()->setCellValue('M'.$j, $sisa);
			$ObjPHPExcel->getActiveSheet()->getStyle('M'.$j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$ObjPHPExcel->getActiveSheet()->getStyle('M'.$j)->applyFromArray($style_row1);

			$nama_file = "Pemakaian_Meterai_".$dfrom."_".$dto.".xls";
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');    
			header('Content-Disposition: attachment; filename='.$nama_file.''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
			$ObjWriter->save('php://output','w');

		}
	}	


	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listmeterai');
			$this->load->view('listmeterai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
      $this->db->trans_begin();
			$this->load->model('listmeterai/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);
      if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Delete Nota No:'.$inota.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );
#				$this->db->trans_rollback();
				$this->db->trans_commit();
      }
			$config['base_url'] = base_url().'index.php/listmeterai/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listmeterai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listmeterai/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$data['total']		= $this->mmaster->bacatotal($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8));
			$this->load->view('listmeterai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listmeterai/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('listmeterai');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$data['total']		= $this->mmaster->bacatotal($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8));
			$this->load->view('listmeterai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listmeterai/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listmeterai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listmeterai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listmeterai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listmeterai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
