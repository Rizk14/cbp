<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = "Pembelian DO - Nota";
			$data['iperiode']	= '';
			$this->load->view('exp-donotabeli/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-donotabeli/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');

			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			$this->db->select("	distinct(a.i_do), a.d_do, a.i_supplier, b.i_dtap, c.e_supplier_name
                          from tm_do a
                          left join tm_dtap_item b on (a.i_do=b.i_do and a.i_supplier=b.i_supplier)
                          inner join tr_supplier c on (a.i_supplier=c.i_supplier)
                          where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          order by a.i_supplier, a.i_do", false);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan DO Nota Pembelian")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);


				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan DO Nota Pembelian');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 4, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 4, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:D5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No DO');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tgl DO');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);


				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':D' . $i
					);

					if ($row->d_do) {
						$tmp = explode('-', $row->d_do);
						$tgl = $tmp[2];
						$bln = $tmp[1];
						$thn = $tmp[0];
						$row->d_do = $tgl . '-' . $bln . '-' . $thn;
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_do, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->d_do, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_dtap, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'LapDONotaBeli-' . $iperiode . '.xls';
			$objWriter->save('beli/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export Laporan DO NOta Beli periode:' . $iperiode;
			// $this->load->model('logger');

			$this->logger->writenew('Export Laporan DO Nota Beli periode:' . $iperiode);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= 'beli';

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
