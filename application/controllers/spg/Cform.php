<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$config['base_url'] = base_url().'index.php/spg/cform/index/';
			/*$cari 	= $this->input->post('cari', TRUE);
			$cari	= strtoupper($cari);*/
			$query	= $this->db->query("select a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                                  from tr_spg a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  order by a.i_spg",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('spg');
			$data['ispg']='';
			$this->load->model('spg/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu SPG";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('spg/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$ispg 			= $this->input->post('ispg', TRUE);
			$espgname		= $this->input->post('espgname', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);

			if ($ispg != '' && $iarea != '' && $icustomer != '')
			{
				$this->load->model('spg/mmaster');
				$this->mmaster->insert($ispg,$espgname,$icustomer,$iarea);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master SPG:('.$ispg.') -'.$espgname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$config['base_url'] = base_url().'index.php/spg/cform/index/';
				$cari 			= $this->input->post('cari', TRUE);
				$cari				= strtoupper($cari);
				$query			= $this->db->query("select a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                                  from tr_spg a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                                  or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                                  order by a.i_spg",false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('spg');
				$data['ispg']='';
				$this->load->model('spg/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('spg/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spg');
			$this->load->view('spg/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spg')." update";
			if($this->uri->segment(4)){
				$ispg = $this->uri->segment(4);
				$data['ispg'] = $ispg;
				$this->load->model('spg/mmaster');
				$data['isi']=$this->mmaster->baca($ispg);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit SPG:('.$ispg.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('spg/vmainform',$data);
			}else{
				$this->load->view('spg/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispg				= $this->input->post('ispg', TRUE);
			$espgname		= $this->input->post('espgname', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);

			$this->load->model('spg/mmaster');
			$this->mmaster->update($ispg,$espgname,$icustomer,$iarea);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master SPG:('.$ispg.') -'.$espgname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url().'index.php/spg/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                                  from tr_spg a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                                  or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                                  order by a.i_spg",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('spg');
			$data['icustomer']='';
			$this->load->model('spg/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spg/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispg	= $this->uri->segment(4);
			$this->load->model('spg/mmaster');
			$this->mmaster->delete($ispg);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master SPG:('.$ispg.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
	        
			$config['base_url'] = base_url().'index.php/spg/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
                                  from tr_spg a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                                  or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                                  order by a.i_spg",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('spg');
			$data['ispg']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spg/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/spg/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/spg/cform/cari/'.$cari.'/';
			}

			/*$config['base_url'] = base_url().'index.php/spg/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				  = strtoupper($cari);*/
			$query				= $this->db->query("select a.i_spg, a.e_spg_name, a.i_customer, b.e_customer_name, a.i_area, c.e_area_name
	                                  from tr_spg a, tr_customer b, tr_area c
	                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
	                                  and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
	                                  or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%')
	                                  order by a.i_spg",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('spg');
			$data['ispg']='';
			$this->load->model('spg/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spg/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function pelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spg/cform/pelanggan/'.$baris.'/sikasep/';
			$query = $this->db->query("select * from tr_customer order by e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('spg/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacapelanggan($config['per_page'],$this->uri->segment(6));
			$this->load->view('spg/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/spg/cform/pelanggan/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/spg/cform/caripelanggan/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/spg/cform/caripelanggan/'.$baris.'/sikasep/';

			$query = $this->db->query("	select * from tr_customer 
										where upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spg/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caripelanggan($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('spg/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spg/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_area 
							where upper(i_area) like '%$cari%'
							or upper(e_area_name) like '%$cari%'
							order by i_area ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spg/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spg/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
