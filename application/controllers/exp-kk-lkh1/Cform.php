<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-kk');
			$data['datefrom']='';
			$data['dateto']	='';
			$this->load->view('exp-kk-lkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-kk-lkh/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
      $no   	  = $this->input->post('no');
			if($datefrom!=''){
				$tmp=explode("-",$datefrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$datefrom=$th."-".$bl."-".$hr;
				$periodeawal	= $hr."/".$bl."/".$th;
			}
			if($dateto!=''){
				$tmp=explode("-",$dateto);
				$thto=$tmp[2];
				$blto=$tmp[1];
				$hrto=$tmp[0];
				$toto=$thto."-".$blto."-".$hrto;
			}
			$iarea= $this->input->post('iarea');
			$tmp  = explode("-", $dateto);
			$det	= $tmp[0];
			$mon	= $tmp[1];
			$yir 	= $tmp[2];
			$dtos	= $yir."/".$mon."/".$det;
			$periodeakhir	= $det."/".$mon."/".$yir;
			$dtos	= $this->mmaster->dateAdd("d",1,$dtos);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;
			$data['page_title'] = $this->lang->line('exp-kk');
			$qareaname	= $this->mmaster->eareaname($iarea);
			if($qareaname->num_rows()>0) {
				$row_areaname	= $qareaname->row();
				$aname	= $row_areaname->e_area_name;
			} else {
				$aname	= '';
			}
      $periode=substr($datefrom,0,4).substr($datefrom,5,2);			
      $coaku=KasKecil.$iarea;
      $kasbesar=KasBesar;
      $bank=Bank;
			// $this->db->select("	a.* from(
		    //                   select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, i_area,
		    //                   d_bukti, i_coa, i_bukti_pengeluaran
		    //                   from tm_kb a
		    //                   where a.i_periode='$periode' and a.i_area='$iarea' 
		    //                   and a.d_kb >= to_date('$datefrom','yyyy-mm-dd') and a.d_kb <= to_date('$toto','yyyy-mm-dd')
		    //                   and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
		    //                   and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where 
		    //                   b.d_kk = a.d_kb
		    //                   and b.i_area='$iarea' and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar' 
		    //                   and b.i_periode='$periode')
		    //                   union all
		    //                   select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, 
		    //                   v_bank as v_kk, i_area, d_bank as d_bukti, i_coa, '' as i_bukti_pengeluaran
		    //                   from tm_kbank a
		    //                   where a.i_periode='$periode' and a.i_area='$iarea' 
		    //                   and a.d_bank >= to_date('$datefrom','yyyy-mm-dd') and a.d_bank <= to_date('$toto','yyyy-mm-dd')
		    //                   and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa='$coaku'
		    //                   and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where 
		    //                   b.d_kk = a.d_bank
		    //                   and b.i_area='$iarea' and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' and b.i_periode='$periode')
		    //                   union all
            //               select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa,
            //               a.i_bukti_pengeluaran                          
            //               from tm_kk a, tr_area b
			// 				            where a.d_kk >= to_date('$datefrom','yyyy-mm-dd') and a.d_kk <= to_date('$toto','yyyy-mm-dd')
			// 				            and a.i_area=b.i_area and a.i_area='$iarea' and a.f_kk_cancel='f'
			// 				            ) as a
			// 							order by a.d_kk,a.i_kk",false);.
							$this->db->select("	a.* from(
											select i_kb as i_kk, d_kb as d_kk,c.i_pv, d.i_rv, 
											case when a.f_debet = 't' then c.i_pv else d.i_rv end as voucher, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, a.i_area,
											d_bukti, a.i_coa, i_bukti_pengeluaran
											from tm_kb a
											left join tm_pv_item c on a.i_kb = c.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa
											left join tm_rv_item d on a.i_kb = d.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa
											where a.i_periode='$periode' and a.i_area='$iarea' 
											and a.d_kb >= to_date('$datefrom','yyyy-mm-dd') and a.d_kb <= to_date('$toto','yyyy-mm-dd')
											and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa='$coaku'
											and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where 
											b.d_kk = a.d_kb
											and b.i_area='$iarea' and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$kasbesar%' 
											and b.i_periode='$periode')
											union all
											select i_kbank as i_kk, d_bank as d_kk, c.i_pv, d.i_rv, 
											case when a.f_debet = 't' then c.i_pv else d.i_rv end as voucher, f_debet, e_description, '' as i_kendaraan, 0 as n_km, 
											v_bank as v_kk, a.i_area, d_bank as d_bukti, a.i_coa, '' as i_bukti_pengeluaran
											from tm_kbank a
											left join tm_pv_item c on a.i_kbank = c.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa
											left join tm_rv_item d on a.i_kbank = d.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa
											where a.i_periode='$periode' and a.i_area='$iarea' 
											and a.d_bank >= to_date('$datefrom','yyyy-mm-dd') and a.d_bank <= to_date('$toto','yyyy-mm-dd')
											and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa='$coaku'
											and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where 
											b.d_kk = a.d_bank
											and b.i_area='$iarea' and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' and b.i_periode='$periode')
											union all
											select a.i_kk as i_kk, a.d_kk, c.i_pv, d.i_rv, 
											case when a.f_debet = 't' then c.i_pv else d.i_rv end as voucher,
											a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, a.i_coa,
											a.i_bukti_pengeluaran                          
											from tm_kk a
											inner join tr_area b on a.i_area=b.i_area
											left join tm_pv_item c on a.i_kk = c.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa
											left join tm_rv_item d on a.i_kk = d.i_kk and a.i_area = c.i_area and a.i_coa = c.i_coa and c.i_area = d.i_area
											where a.d_kk >= to_date('$datefrom','yyyy-mm-dd') and a.d_kk <= to_date('$toto','yyyy-mm-dd')
											and a.i_area='$iarea' and a.f_kk_cancel='f' 
											) as a
											order by a.d_kk,a.i_kk",false);.
#			$this->db->select("	* from tm_kk 
#								          inner join tr_area on (tm_kk.i_area=tr_area.i_area)
#								          where tm_kk.d_kk >= '$datefrom' and tm_kk.d_kk <= '$dtos' 
#                          and tm_kk.i_area='$iarea' and tm_kk.f_kk_cancel='f'
#								          order by tm_kk.i_area,tm_kk.d_kk,tm_kk.i_kk",false);
			$query=$this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Kas Kecil Harian")
						->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A4'
				);
        
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
        if($iarea=='00'){
  				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
        }else{
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
		}	

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN KAS HARIAN');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,11,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'AREA '.$aname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No : '.$no);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,4,11,4);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl Trans');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'No Reff');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'NO Perk');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'NO Perk Asal');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'No Kendaraan');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'KM');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Debet');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        if($iarea=='00'){
				  $objPHPExcel->getActiveSheet()->setCellValue('M6', 'No. Voucher');
				  $objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'bottom'=> array('style' => Style_Border::BORDER_THIN),
							  'left'  => array('style' => Style_Border::BORDER_THIN),
							  'right' => array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
        }
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;

				foreach($query->result() as $row){
#          $periode=substr($datefrom,0,4).substr($datefrom,5,2);					
					
					if($row->i_area!=$xarea)
					{
						$saldo	=$this->mmaster->bacasaldo($row->i_area,$periode,$datefrom);

						$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "Saldo Awal");
						$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $saldo);
						$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								),
							)
						);
            if($iarea=='00'){
						  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
							  array(
								  'borders' => array(
									  'top' 	=> array('style' => Style_Border::BORDER_THIN),
									  'bottom'=> array('style' => Style_Border::BORDER_THIN),
									  'left'  => array('style' => Style_Border::BORDER_THIN),
									  'right' => array('style' => Style_Border::BORDER_THIN)
								  ),
							  )
						  );
            }else{
				$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			}
						$i++;
					}
					$xarea=$row->i_area;
					if($row->f_debet=='f' || substr($row->i_kk,0,2)=='KB')
					{
						$debet =$row->v_kk;
						$kredit=0;
          }elseif($row->f_debet=='f' || substr($row->i_kk,0,2)=='BK'){
						$debet =$row->v_kk;
						$kredit=0;
					}else{
						$kredit=$row->v_kk;
						$debet =0;
					}
					$saldo=$saldo+$debet-$kredit;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-6);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_kk!=''){
						$tmp=explode("-",$row->d_kk);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_kk=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->d_kk);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_bukti!=''){
						$tmp=explode("-",$row->d_bukti);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_bukti=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_kk);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_description);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_coa);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
           if(strlen($row->i_coa)==6){
			        $coa  = $row->i_coa.$iarea;
		        }else{
              $coa  = substr($row->i_coa,0,6).$iarea;
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $coa);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          }
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->i_kendaraan);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_km);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $debet);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $kredit);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $saldo);
					$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          if($iarea=='00'){
					  $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->i_bukti_pengeluaran);
					  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
						  array(
							  'borders' => array(
								  'top' 	=> array('style' => Style_Border::BORDER_THIN),
								  'bottom'=> array('style' => Style_Border::BORDER_THIN),
								  'left'  => array('style' => Style_Border::BORDER_THIN),
								  'right' => array('style' => Style_Border::BORDER_THIN)
							  ),
						  )
					  );
          }else{
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->voucher);
					$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
		  }
					$i++;
					$j++;
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('F7:G7'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
				$objPHPExcel->getActiveSheet()->getStyle('J7:L'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
        if($iarea=='00'){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:M'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:M'.$x
			    );
        }else{
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:L'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:L'.$x
			    );
        }
			}
      if($iarea=='00'){
	  		$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      }else{
  			$objPHPExcel->getActiveSheet()->getStyle('A6:L6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='lkh-'.$no.'-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
      if(file_exists('excel/'.$iarea.'/'.$nama)){
        @chmod('excel/'.$iarea.'/'.$nama, 0777);
        @unlink('excel/'.$iarea.'/'.$nama);
      }
			$objWriter->save('excel/'.$iarea.'/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export LKH tanggal:'.$datefrom.' sampai:'.$dateto.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Kas Kecil";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-kk-lkh/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-kk-lkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-kk-lkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-kk-lkh/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-kk-lkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-kk-lkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
