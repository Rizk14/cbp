<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         $data['iperiode']='';
         $data['ialokasi']  ='';
         $data['isi']  ='';

         $this->load->view('alokasihutanglain/vmainform', $data);

      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         $this->load->view('alokasihutanglain/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete', TRUE);
         $this->load->model('alokasihutanglain/mmaster');
         $this->mmaster->delete($ispb);
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         $data['ispb']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('alokasihutanglain/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
                              where a.i_customer=b.i_customer
                              and a.f_lunas = 'f'
                              and (upper(a.i_customer) like '%$cari%'
                                or upper(b.e_customer_name) like '%$cari%'
                                or upper(a.i_spb) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         $data['ispb']='';
         $data['ittb']='';
         $data['idtap']='';
         $this->load->view('alokasihutanglain/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
#            $idt        = str_replace('tandagaring','/',$this->uri->segment(4));
            $ikbank     = $this->uri->segment(4);
            $iarea      = $this->uri->segment(5);
            $eareaname  = $this->uri->segment(6);
            $eareaname  = str_replace('%20', ' ',$eareaname);
            $vsisa      = $this->uri->segment(13);
            $dfrom      = $this->uri->segment(8);
            $dto        = $this->uri->segment(9);
            $dbank      = $this->uri->segment(10);
            $ebankname  = $this->uri->segment(11);
            $icoabank   = $this->uri->segment(12);
            $vsisa      = $this->uri->segment(13);
            $ibank      = $this->uri->segment(14);
            $ebankname  = str_replace('%20', ' ',$ebankname);
//          $query = $this->db->query("select * from tm_nota_item where i_nota = '$idt' and i_area = '$iarea'");
            $data['jmlitem'] = 0;//$query->num_rows();
            $data['ialokasi'] = '';
            $data['ikbank'] = $ikbank;
            $data['iarea']=$iarea;
            $data['eareaname']=$eareaname;
            $data['ebankname']=$ebankname;
            $data['vsisa']=$vsisa;
            $data['dfrom']=$dfrom;
            $data['dto']=$dto;
            $data['dbank']=$dbank;
            $data['icoabank']=$icoabank;
            $data['ibank']=$ibank;  
            
            $this->load->model('alokasihutanglain/mmaster');
            $data['isi']='';//$this->mmaster->baca($idt,$iarea);
            $data['detail']='';//$this->mmaster->bacadetail($idt,$iarea);
            $this->load->view('alokasihutanglain/vmainform',$data);
         }else{
            $this->load->view('alokasihutanglain/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('alokasihutanglain/mmaster');
         $dalokasi  = $this->input->post('dalokasi', TRUE);
         if($dalokasi!=''){
            $tmp=explode("-",$dalokasi);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dalokasi=$th."-".$bl."-".$hr;
            $thbl=$th.$bl;
			      $iperiode=$th.$bl;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
#         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
#         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ibank         = $this->input->post('ibank', TRUE);
         $areabank      = $this->input->post('areabank', TRUE);
         $icoabank      = $this->input->post('icoabank',TRUE);
         $edescription  = $this->input->post('edescription',TRUE);
         #$vjumlahx      = $this->input->post('vjumlah',TRUE); // DICOMMENT KRN AMBIL NOMINAL DARI SALDO HUTANG LAIN"
         $vjumlahx      = $this->input->post('vbank',TRUE);
         $vjumlahx      = str_replace(',','',$vjumlahx);
         $vlebihx       = $this->input->post('vlebih',TRUE);
         $vlebihx       = str_replace(',','',$vlebihx);
         $jml           = $this->input->post('jml', TRUE);
         $idt           = $this->input->post('idt', TRUE);
         $areadt        = $this->input->post('areadt', TRUE);
         $ada=false;

         if(($dalokasi!='') && ($vjumlahx!='') && ($ibank!='') && ($vjumlahx!='0') && ($jml!='0') && ($icustomer!='') ){
          if(!$ada){
            $this->db->trans_begin();
            $inotax = $this->input->post('inota1', TRUE);
            $ialokasi=$this->mmaster->runningnumberpl($iarea,$thbl);
########## Posting ###########
            $egirodescription="Alokasi Hutang lain2 no:".$ialokasi;
			      $fclose			= 'f';
			      $jml			= $this->input->post('jml', TRUE);
			      for($i=1;$i<=$jml;$i++)
			      {
			        $inota=$this->input->post('inota'.$i, TRUE);
			        $ireff=$ialokasi.'|'.$inota; 
              if($i==1){
			          $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dalokasi);
#			          $this->mmaster->updatepelunasan($ialokasi,$iarea,$ikn);
              }
        			$vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				      $vjumlah		= str_replace(',','',$vjumlah);

              /*
                PERUBAHAN 11 OKT 2019
                - accdebet sebelumnya HutangLain dituker dengan PiutangDagang.$iarea 
                - acckredit sebelumnya PiutangDagang.$iarea dituker dengan HutangLain
              */
       				$accdebet		= PiutangDagang.$iarea; 
				      $namadebet	= $this->mmaster->namaacc($accdebet);
				      $tmp			  = $this->mmaster->carisaldo($accdebet,$iperiode);
				      if($tmp) 
					      $vsaldoaw1		= $tmp->v_saldo_awal;
				      else 
					      $vsaldoaw1		= 0;
				      if($tmp) 
					      $vmutasidebet1	= $tmp->v_mutasi_debet;
				      else
					      $vmutasidebet1	= 0;
				      if($tmp) 
					      $vmutasikredit1	= $tmp->v_mutasi_kredit;
				      else
					      $vmutasikredit1	= 0;
				      if($tmp) 
					      $vsaldoak1		= $tmp->v_saldo_akhir;
				      else
					      $vsaldoak1		= 0;
				
				      $acckredit		= HutangLain;
				      $namakredit		= $this->mmaster->namaacc($acckredit);
				      $saldoawkredit	= $this->mmaster->carisaldo($acckredit,$iperiode);
				      if($tmp) 
					      $vsaldoaw2		= $tmp->v_saldo_awal;
				      else
					      $vsaldoaw2		= 0;
				      if($tmp) 
					      $vmutasidebet2	= $tmp->v_mutasi_debet;
				      else
					      $vmutasidebet2	= 0;
				      if($tmp) 
					      $vmutasikredit2	= $tmp->v_mutasi_kredit;
				      else
					      $vmutasikredit2	= 0;
				      if($tmp) 
					      $vsaldoak2		= $tmp->v_saldo_akhir;
				      else
					      $vsaldoak2		= 0;
				      $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vjumlah,$dalokasi);
				      $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlah);
				      $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vjumlah,$dalokasi);
				      $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlah);
				      $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjumlah,$dalokasi,$egirodescription);
				      $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjumlah,$dalokasi,$egirodescription);
			      }
########## End of Posting ###########
            $this->mmaster->insertheader( $ialokasi,$iarea,$icustomer,$dalokasi,$vjumlahx,$vlebihx,$ibank,$icoabank,$edescription,$idt,$areadt);
            $asal=0;
            $pengurang=$vjumlahx-$vlebihx;

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
             $group=$row->i_customer_groupbayar;
          }
          $this->mmaster->updatesaldo($group,$icustomer,$pengurang);
          for($i=1;$i<=$jml;$i++){
            $inota              = $this->input->post('inota'.$i, TRUE);
            $dnota              = $this->input->post('dnota'.$i, TRUE);
            if($dnota!=''){
               $tmp=explode("-",$dnota);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dnota=$th."-".$bl."-".$hr;
            }
            $vjumlah= $this->input->post('vjumlah'.$i, TRUE);
            $vsisa  = $this->input->post('vsisa'.$i, TRUE);
            $vsiso  = $this->input->post('vsisa'.$i, TRUE);
            $vjumlah= str_replace(',','',$vjumlah);
            $vsisa = str_replace(',','',$vsisa);
            $vsiso = str_replace(',','',$vsiso);
            $vsiso  = $vsiso-$vjumlah;
            $eremark= $this->input->post('eremark'.$i,TRUE);
            $this->mmaster->insertdetail($ialokasi,$iarea,$icustomer,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$ibank, $areabank);
            $fupdatenota=$this->mmaster->updatenota($inota,$vjumlah);
            if($fupdatenota==false) break;

            /*if($vsisa>0 && $vsisa<=100 && $fupdatenota==true){
              ########## Posting pembulatan ###########
              $egirodescription="Alokasi Hutang lain2 no:".$ialokasi.'('.$inota.')';
			        $fclose			= 'f';
		          $ireff=$ialokasi.'|'.$inota;
	            $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dalokasi);
       				$accdebet		= ByPembulatan;
			        $namadebet	= $this->mmaster->namaacc($accdebet);
			        $tmp			  = $this->mmaster->carisaldo($accdebet,$iperiode);
			        if($tmp) 
				        $vsaldoaw1		= $tmp->v_saldo_awal;
			        else 
				        $vsaldoaw1		= 0;
			        if($tmp) 
				        $vmutasidebet1	= $tmp->v_mutasi_debet;
			        else
				        $vmutasidebet1	= 0;
			        if($tmp) 
				        $vmutasikredit1	= $tmp->v_mutasi_kredit;
			        else
				        $vmutasikredit1	= 0;
			        if($tmp) 
				        $vsaldoak1		= $tmp->v_saldo_akhir;
			        else
				        $vsaldoak1		= 0;
			
			        $acckredit		= PiutangDagang.$iarea;
			        $namakredit		= $this->mmaster->namaacc($acckredit);
			        $saldoawkredit	= $this->mmaster->carisaldo($acckredit,$iperiode);
			        if($tmp) 
				        $vsaldoaw2		= $tmp->v_saldo_awal;
			        else
				        $vsaldoaw2		= 0;
			        if($tmp) 
				        $vmutasidebet2	= $tmp->v_mutasi_debet;
			        else
				        $vmutasidebet2	= 0;
			        if($tmp) 
				        $vmutasikredit2	= $tmp->v_mutasi_kredit;
			        else
				        $vmutasikredit2	= 0;
			        if($tmp) 
				        $vsaldoak2		= $tmp->v_saldo_akhir;
			        else
				        $vsaldoak2		= 0;
			        $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vsisa,$dalokasi);
			        $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vsisa);
			        $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vsisa,$dalokasi);
			        $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vsisa);
			        $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vsisa,$dalokasi,$egirodescription);
			        $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vsisa,$dalokasi,$egirodescription);
#			        $fupdatebank=$this->mmaster->updatebank($ikbank,$icoabank,$iarea,$vsisa);
  ########## End of Posting pembulatan ###########
            }*/

          }

          if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Input Alokasi Hutang lain2 No:'.$ialokasi.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );
                if($fupdatenota==true){
                  $this->db->trans_commit();
                }else{
                 $this->db->trans_rollback();
                }
#                $this->db->trans_rollback();
                $data['sukses'] = true;
                $data['inomor'] = $ialokasi;
                $this->load->view('nomor',$data);
              }
        }
       }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if ( (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
#         $isupplier   = $this->session->userdata('i_supplier');
         $data['page_title'] = $this->lang->line('alokasihutanglain')." update";
         if(
            ($this->uri->segment(4)) && ($this->uri->segment(5))
           )
         {
            $ialokasi = $this->uri->segment(4);
            $iarea    = $this->uri->segment(5);
            $dfrom    = $this->uri->segment(6);
            $dto      = $this->uri->segment(7);
            $icoabank = $this->uri->segment(8);
            $ikbank   = $this->uri->segment(9);
            #$area     = $this->uri->segment(10);

            $this->load->model('alokasihutanglain/mmaster');
            $query   = $this->db->query("select b.i_nota, to_char(a.d_alokasi,'yyyymm') as thbl 
                                         from tm_alokasihl a, tm_alokasihl_item b
                                         where a.i_alokasi=b.i_alokasi and a.i_area=b.i_area
                                         and a.i_alokasi = '$ialokasi' and a.i_area='$iarea' ");
            if($query->num_rows()>0){
               $data['jmlitem'] = $query->num_rows();
#               $data['vsisa']   = $this->mmaster->sisa($ialokasi,$iarea);
#               $data['vbulat']  = $this->mmaster->bulat($ialokasi,$iarea);
               $data['isi']     = $this->mmaster->bacapl($ialokasi,$iarea);
               $data['detail']  = $this->mmaster->bacadetailpl($ialokasi,$iarea);
               
               $hasilrow = $query->row();
               $thbl  = $hasilrow->thbl;
               $query3  = $this->db->query(" select i_periode from tm_periode ");
               if ($query3->num_rows() > 0){
                 $hasilrow = $query3->row();
                 $i_periode = $hasilrow->i_periode;
               }
               if($i_periode <= $thbl)
                  $data['bisaedit']=true;
                else
                  $data['bisaedit']=false;
            }
          }

          $data['ialokasi'] = $ialokasi;
          $data['iarea']    = $iarea;
          $data['dfrom']    = $dfrom;
          $data['dto']      = $dto;
          $data['iperiode'] = '';
          $this->load->view('alokasihutanglain/vmainform',$data);
         }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updatepelunasan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('alokasihutanglain/mmaster');
         $ipl  = $this->input->post('ipelunasan', TRUE);
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
 
        
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $isupplier         = $this->input->post('isupplier', TRUE);
         $esuppliername     = $this->input->post('esuppliername', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#        $vlebih        = $this->input->post('vlebih',TRUE);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih        = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml        = $this->input->post('jml', TRUE);
      $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='')){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);
             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
#          if( ($vsisa-$vjumlah>0) && ($ipelunasanremark=='') ){
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
          }
          if($ada) break;
        }
        if(!$ada){
              $this->db->trans_begin();
              $asalkn   = $this->mmaster->jmlasalkn($ipl,$idt,$isupplier,$ddt);
              foreach($asalkn as $asl){
                 $jmlpl = $asl->v_jumlah;
                 $lbhpl = $asl->v_lebih;
              }
              $asal  = $jmlpl-$lbhpl;
              $this->mmaster->deleteheader(  $ipl,$idt,$isupplier,$ddt);
              $this->mmaster->insertheader(  $ipl,$idt,$isupplier,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                      $vjumlah,$vlebih);
              $pengurang=$vjumlah-$vlebih;
          $igiro=trim($igiro);

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$isupplier,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$isupplier,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$isupplier,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$isupplier,$egirobank,$pengurang,$asal);
          }
              for($i=1;$i<=$jml;$i++){
                $idtap              = $this->input->post('idtap'.$i, TRUE);
                $idtap              = $this->input->post('idtap'.$i, TRUE);
                if($idtap!=''){
                   $tmp=explode("-",$idtap);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $idtap=$th."-".$bl."-".$hr;
              }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah   = $this->input->post('vjumlah'.$i, TRUE);
                $vasal  = $this->input->post('vasal'.$i, TRUE);
                $vasol  = $this->input->post('vasal'.$i, TRUE);
                if($vasal==''){
                  $vasal   = $this->input->post('vsisa'.$i, TRUE);
                  $vasol   = $this->input->post('vsisa'.$i, TRUE);
                }
                $vjumlah= str_replace(',','',$vjumlah);
                 $vasal = str_replace(',','',$vasal);
                 $vasol = str_replace(',','',$vasol);
#               $vsisa  = $vasal-$vjumlah;
                $vsisa  = $vasal;
                $vsiso  = $vasal-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark    = $this->input->post('eremark'.$i,TRUE);

                $this->mmaster->deletedetail(   $ipl,$idt,$isupplier,$idtap,$ddt);
                $this->mmaster->insertdetail(   $ipl,$idt,$isupplier,$idtap,$idtap,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
#           $this->mmaster->updatedt($idt,$isupplier,$ddt,$idtap,$vsisa);
#               $this->mmaster->updatenota($idtap,$vsisa);
#               $this->mmaster->updatenota($idtap,$vsiso);
                $this->mmaster->updatenota($idtap,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$isupplier,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$isupplier,$ddt);
#             }
              if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update Pelunasan No:'.$ipl.' Area:'.$isupplier;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }

      }else{
         $this->load->view('awal/index.php');
      }
   }
   function dt()
   {
      if (
         (($this->session->userdata('logged_in')) && ($this->session->userdata('menu526') == 't')) || (($this->session->userdata('logged_in')) && ($this->session->userdata('allmenu') == 't'))
      ) {
         $iarea                    = $this->input->post('iarea');
         $dbank                    = $this->input->post('dalokasi');
         $cari                     = strtoupper($this->input->post('cari', FALSE));
         $iuser                    = $this->session->userdata('user_id');
         if ($iarea == '') $iarea     = $this->uri->segment(4);
         if ($dbank == '') $dbank     = $this->uri->segment(5);

         $dfrom   = date('Y-m-d', strtotime($dbank));
         $dto     = date('Y-m-d');

         $config['base_url'] = base_url() . 'index.php/alokasihutanglain/cform/dt/' . $iarea . '/' . $dbank . '/';

         $query   = $this->db->query(" SELECT DISTINCT a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah FROM tm_dt a, tm_dt_item b, tm_nota c
                                       WHERE 
                                       a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt 
                                       AND b.i_nota=c.i_nota AND b.i_customer=c.i_customer AND b.i_area=c.i_area
                                       AND c.v_sisa>0 AND c.f_nota_cancel='f'
                                       AND a.d_dt >= '$dfrom' AND a.d_dt <= '$dto' 
                                       AND a.i_area IN(SELECT i_area FROM tm_user_area WHERE i_user='$iuser')
                                       AND (UPPER(a.i_dt) LIKE '%$cari%') 
                                       ORDER BY a.i_area, a.d_dt DESC", FALSE);

         $config['total_rows'] = $query->num_rows();
         $config['per_page']   = '10';
         $config['first_link'] = 'Awal';
         $config['last_link']  = 'Akhir';
         $config['next_link']  = 'Selanjutnya';
         $config['prev_link']  = 'Sebelumnya';
         $config['cur_page']   = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $data['iarea']        = $iarea;
         $data['dbank']        = $dbank;
         $this->load->model('alokasihutanglain/mmaster');

         $data['page_title']   = $this->lang->line('list_customer');
         $data['isi']          = $this->mmaster->bacadt($iarea, $dfrom, $dto, $cari, $config['per_page'], $this->uri->segment(6), $iuser);

         $this->load->view('alokasihutanglain/vlistdt', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function supplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier  = $this->uri->segment(4);
         $cari   = $this->uri->segment(5);
         if($cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/supplier/'.$isupplier.'/sikasep/';
           $query = $this->db->query("select i_supplier from tr_supplier",false);
         }else{
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/supplier/'.$isupplier.'/'.$cari.'/';
           $query   = $this->db->query(" select i_supplier from tr_supplier 
                                          where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
         }
         $config['per_page'] = '10';
         
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->carisupplier($cari,$isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $data['cari']=$cari;
         $this->load->view('alokasihutanglain/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carisupplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier    = strtoupper($this->input->post('area', FALSE));
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if($isupplier=='' || $isupplier==null) $isupplier = $this->uri->segment(4);
         if($cari=='' || $cari==null) $cari = $this->uri->segment(5);
         if($cari=='' || $cari==null || $cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/supplier/sikasep/';
         }else{
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/supplier/'.$cari.'/';
           $query   = $this->db->query(" select i_supplier from tr_supplier
                                        where  (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->carisupplier($cari,$isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $data['cari']=$cari;
         $this->load->view('alokasihutanglain/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function girocek()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/girocek/index/';
         $config['per_page'] = '10';
         if($this->session->userdata('i_area')=='00' || $this->session->userdata('i_area')=='PB'){
           $query = $this->db->query("select * from tr_jenis_bayar",false);
         }else{
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar<>'05'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('alokasihutanglain/mmaster');
         $data['area']=$this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('list_girocek');
         $data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5),$this->session->userdata('i_area'));
         $this->load->view('alokasihutanglain/vlistgirocek', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function nota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['icustomer']= $this->uri->segment(5);
         $data['idt']= $this->uri->segment(6);
         $baris              = $this->uri->segment(4);
         $icustomer          = $this->uri->segment(5);
         $idt          = $this->uri->segment(6);

         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/nota/'.$baris.'/'.$icustomer.'/'.$idt.'/';
         
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
         // $query = $this->db->query("select a.*, b.* from tm_nota a,  tr_customer_groupbayar b where  
         //                            a.i_customer=b.i_customer and f_nota_cancel='f' and a.v_sisa>0 and b.i_customer_groupbayar='$group'" ,false);

            $query = $this->db->query(" SELECT a.*, b.* FROM tm_nota a,  tr_customer_groupbayar b WHERE  
                                        a.i_customer = b.i_customer AND f_nota_cancel = 'f' AND a.v_sisa > 0 AND NOT a.i_nota IS NULL
                                        AND b.i_customer_groupbayar = '$group'
                                        AND a.i_nota IN(SELECT i_nota FROM tm_dt_item WHERE i_customer = '$icustomer' AND i_dt = '$idt')
                                        ORDER BY i_nota " ,FALSE);
         
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($icustomer,$idt,$config['per_page'],$this->uri->segment(7),$group);
         $data['icustomer']=$icustomer;
         $this->load->view('alokasihutanglain/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   } 

  function carinota()
  {
    if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu526')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
       ){
         $data['baris']    = $this->uri->segment(4);
         $data['icustomer']= $this->uri->segment(5);
         $baris              = $this->uri->segment(4);
         $icustomer          = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/nota/'.$baris.'/'.$icustomer.'/';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
       $cari   = strtoupper($this->input->post('cari', FALSE));

      $query  = $this->db->query("select a.*, b.* from tm_nota a,  tr_customer_groupbayar b where  
                                    a.i_customer=b.i_customer and f_nota_cancel='f' and a.v_sisa>0 and b.i_customer_groupbayar='$group'
                                    and (upper(a.i_nota) like '%$cari%')
                                    ",false);

         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($icustomer,$config['per_page'],$this->uri->segment(6),$group,$cari);
         $data['icustomer']=$icustomer;
         $this->load->view('alokasihutanglain/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   } 
##########
  
   function notaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['idt']      = $this->uri->segment(6);
         $data['icustomer']   = $this->uri->segment(7);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->uri->segment(4);
         $iarea            = $this->uri->segment(5);
         $idt           = $this->uri->segment(6);
         $icustomer        = $this->uri->segment(7);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('alokasihutanglain/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinotaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $data['baris']    = $this->input->post('baris', FALSE);
         $data['iarea']    = $this->input->post('iarea', FALSE);
         $data['idt']      = $this->input->post('idt', FALSE);
         $data['icustomer']   = $this->input->post('icustomer', FALSE);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->input->post('baris', FALSE);
         $iarea            = $this->input->post('iarea', FALSE);
         $idt           = $this->input->post('idt', FALSE);
         $icustomer        = $this->input->post('icustomer', FALSE);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('alokasihutanglain/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
		     $iperiode		= $this->input->post('iperiode');
		     if($iperiode=='') $iperiode	= $this->uri->segment(4);
		     $data['iperiode']		= $iperiode;
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/view/'.$iperiode.'/index/';
         $data['page_title'] = $this->lang->line('alokasihutanglain');
         $this->load->model('alokasihutanglain/mmaster');
         $data['iperiode']=$iperiode;
         $data['vsaldo']   = $this->mmaster->bacaperiode($iperiode);
         $data['ialokasi']   = '';
         $this->load->view('alokasihutanglain/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
          $iperiode                    = $this->uri->segment(4);
          if($iperiode=='') $iperiode  = $this->uri->segment(4);

          $this->load->model('alokasihutanglain/mmaster');
          $data['page_title']          = $this->lang->line('alokasihutanglain');
          $data['iperiode']            = $iperiode;
          $data['isi']                 = $this->mmaster->bacahlreff($iperiode);
         
         $this->load->view('alokasihutanglain/vformexport', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function remark()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/remark/'.$baris.'/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select i_pelunasan_remark from tr_pelunasan_remark",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan_remark');
         $data['isi']=$this->mmaster->bacaremark($config['per_page'],$this->uri->segment(5));
      $data['baris']=$baris;
         $this->load->view('alokasihutanglain/vlistremark', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu526')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/alokasihutanglain/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('alokasihutanglain/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('alokasihutanglain/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu526')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/alokasihutanglain/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('alokasihutanglain/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('alokasihutanglain/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea  = $this->uri->segment(4);
         $idt  = $this->uri->segment(5);
         $cari   = $this->uri->segment(6);
         $cari=strtoupper($cari);

         if($cari=='SIKASEP'){
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/customer/'.$iarea."/".$idt.'/SIKASEP/';
           $query  = $this->db->query(" SELECT DISTINCT a.i_dt, a.d_dt, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_city
                                        FROM tm_dt_item a
                                        LEFT JOIN tm_dt b ON(a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt AND f_dt_cancel='f'), tr_customer c
                                        WHERE 
                                        a.i_customer=c.i_customer AND a.i_area = '$iarea' AND a.i_dt='$idt' ",false);
         }else{
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/customer/'.$iarea."/".$idt.'/'.$cari.'/';
           $query  = $this->db->query(" SELECT DISTINCT a.i_dt, a.d_dt, a.i_customer, c.e_customer_name, c.e_customer_address, c.e_customer_city
                                        FROM tm_dt_item a
                                        LEFT JOIN tm_dt b ON(a.i_dt=b.i_dt AND a.i_area=b.i_area AND a.d_dt=b.d_dt AND f_dt_cancel='f'), tr_customer c
                                        WHERE 
                                        a.i_customer=c.i_customer AND a.i_area = '$iarea' AND a.i_dt='$idt' 
                                        AND (UPPER(a.i_customer) LIKE '%$cari%' OR UPPER(c.e_customer_name) LIKE '%$cari%' OR UPPER(a.i_dt) LIKE '%$cari%') ",false);
         }
         
         $config['per_page'] = '10';         
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$iarea,$idt,$config['per_page'],$this->uri->segment(7));
         $data['iarea']=$iarea;
         $data['cari']=$cari;
         $this->load->view('alokasihutanglain/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

  function bank()
  {
    if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu526')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
       ){
      $iarea      = $this->uri->segment(4);
      $dalokasi   = $this->uri->segment(5);
      #$sm         = PiutangDagangSementara;
      $sm         = PiutangDagang;
         
         if($dalokasi!=''){
            $tmp=explode("-",$dalokasi);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dalokasi=$th."-".$bl."-".$hr;
         }
      $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/bank/'.$iarea.'/'.$dalokasi.'/'.$sm.'/';
      /*$query = $this->db->query("select * from tm_kbank where f_kbank_cancel='false' and i_area='$iarea' and i_coa='$sm' and d_bank<='$dalokasi'",false);*/

      $query = $this->db->query(" SELECT b.i_area,  a.i_kbank, a.i_coa_bank, b.e_description, a.v_sisa, f_lebih_bayar
                                  FROM tm_alokasihl_reff a
                                  INNER JOIN tm_kbank b on(a.i_kbank=b.i_kbank AND a.i_area=b.i_area)
                                  WHERE a.v_sisa>0  AND (a.i_area='$iarea' OR b.i_area='XX')
                                  ORDER BY i_area, i_kbank ",false);

      $config['total_rows'] = $query->num_rows(); 
      $config['per_page']   = '10';
      $config['first_link'] = 'Awal';
      $config['last_link']  = 'Akhir';
      $config['next_link']  = 'Selanjutnya';
      $config['prev_link']  = 'Sebelumnya';
      $config['cur_page']   = $this->uri->segment(7);
      $this->pagination->initialize($config);
      $this->load->model('alokasihutanglain/mmaster');
      $data['page_title']   = $this->lang->line('list_bank');
      $data['isi']          = $this->mmaster->bacabank($config['per_page'],$this->uri->segment(7),$iarea,$dalokasi,$sm);
      $data['iarea']        = $iarea;
      $data['dalokasi']     = $dalokasi;
      $data['sm']           = $sm;
      $this->load->view('alokasihutanglain/vlistbank', $data);
    }else{
      $this->load->view('awal/index.php');
    }
  }

    function caribank()
  {
    if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu526')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
       ){
          $iarea        = strtoupper($this->input->post('iarea', FALSE));
          $dalokasi     = strtoupper($this->input->post('dalokasi', FALSE));
          $cari         = strtoupper($this->input->post('cari', FALSE));
         /*if($iarea=='' || $iarea==null) $iarea = $this->uri->segment(4);
         if($dalokasi=='' || $dalokasi==null) $dalokasi = $this->uri->segment(5);*/
          #$sm=PiutangDagangSementara;
          $sm=PiutangDagang;
          #$config['base_url'] = base_url().'index.php/alokasihutanglain/cform/bank/'.$iarea.'/'.$dalokasi.'/'.$sm.'/';
          $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/bank/index/';

          $query  = $this->db->query("SELECT b.i_area, a.i_kbank, a.i_coa_bank, b.e_description, a.v_sisa
                                      FROM tm_alokasihl_reff a
                                      INNER JOIN tm_kbank b on(a.i_kbank=b.i_kbank)
                                      WHERE a.v_sisa>0 AND (UPPER(a.i_kbank) LIKE '%$cari%' OR UPPER(b.e_description) LIKE '%$cari%')
                                      AND (a.i_area='$iarea' OR b.i_area='XX')
                                      ORDER BY i_area, i_kbank
                                      ",FALSE);

          $config['total_rows'] = $query->num_rows(); 
          $config['per_page']   = '10';
          $config['first_link'] = 'Awal';
          $config['last_link']  = 'Akhir';
          $config['next_link']  = 'Selanjutnya';
          $config['prev_link']  = 'Sebelumnya';
          $config['cur_page']   = $this->uri->segment(5);
          $this->pagination->initialize($config);

          $this->load->model('alokasihutanglain/mmaster');
          $data['page_title'] = $this->lang->line('list_area');
          $data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5),$iarea,$dalokasi,$sm);
          $data['iarea']=$iarea;
          $data['dalokasi']=$dalokasi;
          $data['sm']=$sm;
          $this->load->view('alokasihutanglain/vlistbank', $data);
        }else{
          $this->load->view('awal/index.php');
        }
  }

   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu526')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea    = strtoupper($this->input->post('iarea', FALSE));
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if($iarea=='' || $iarea==null) $iarea = $this->uri->segment(4);
         if($cari=='' || $cari==null) $cari = $this->uri->segment(5);
         if($cari=='' || $cari==null || $cari=='SIKASEP'){
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/customer/'.$iarea.'/SIKASEP/';
         }else{
           $config['base_url'] = base_url().'index.php/alokasihutanglain/cform/customer/'.$iarea.'/'.$cari.'/';
           $query   = $this->db->query(" select i_customer from tr_customer where i_area = '$iarea'
                                         and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('alokasihutanglain/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
         $data['iarea']=$iarea;
         $data['cari']=$cari;
         $this->load->view('alokasihutanglain/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
