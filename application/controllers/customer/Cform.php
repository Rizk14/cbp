<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icity				= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
			//			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$ipricegroup			= $this->input->post('nline', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress		= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);
			$ecustomerretensi		= $this->input->post('ecustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if ($ncustomertoplength == '')
				$ncustomertoplength = 0;
			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $ecustomername != '')) {
				$this->load->model('customer/mmaster');
				$this->mmaster->insert(
					$icustomer,
					$icustomerplugroup,
					$icity,
					$icustomergroup,
					$ipricegroup,
					$iarea,
					$icustomerproducttype,
					$icustomerspecialproduct,
					$icustomergrade,
					$icustomerservice,
					$icustomersalestype,
					$icustomerclass,
					$icustomerstatus,
					$ecustomername,
					$ecustomeraddress,
					$ecityname,
					$ecustomerpostal,
					$ecustomerphone,
					$ecustomerfax,
					$ecustomermail,
					$ecustomersendaddress,
					$ecustomerreceiptaddress,
					$ecustomerremark,
					$ecustomerpayment,
					$ecustomerpriority,
					$ecustomercontact,
					$ecustomercontactgrade,
					$ecustomerrefference,
					$ecustomerothersupplier,
					$fcustomerplusppn,
					$fcustomerplusdiscount,
					$fcustomerpkp,
					$fcustomeraktif,
					$fcustomertax,
					$ecustomerretensi,
					$ncustomertoplength,
					$fcustomercicil
				);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Input Customer KodeLang:' . $icustomer;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$area1	= $this->session->userdata('i_area');
				$area2	= $this->session->userdata('i_area2');
				$area3	= $this->session->userdata('i_area3');
				$area4	= $this->session->userdata('i_area4');
				$area5	= $this->session->userdata('i_area5');
				$cari = strtoupper($this->input->post("cari", false));
				$config['base_url'] = base_url() . 'index.php/customer/cform/index/';
				if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
					$query = $this->db->query("select * from tr_customer 
		                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
				} else {
					$query = $this->db->query("select * from tr_customer 
		                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
		                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
		                                   or i_area = '$area4' or i_area = '$area5')", false);
				}
				$config['total_rows'] = $query->num_rows();
				#				$config['total_rows'] = $this->db->count_all('tr_customer');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('master_customer');
				$data['icustomer'] = '';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('customer/mmaster');
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4), $area1, $area2, $area3, $area4, $area5);
				$this->load->view('customer/vmainform', $data);
			} else {
				$area1	= $this->session->userdata('i_area');
				$area2	= $this->session->userdata('i_area2');
				$area3	= $this->session->userdata('i_area3');
				$area4	= $this->session->userdata('i_area4');
				$area5	= $this->session->userdata('i_area5');
				$cari = strtoupper($this->input->post("cari", false));
				$config['base_url'] = base_url() . 'index.php/customer/cform/index/';
				if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
					$query = $this->db->query("select * from tr_customer 
		                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
				} else {
					$query = $this->db->query("select * from tr_customer 
		                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
		                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
		                                   or i_area = '$area4' or i_area = '$area5')", false);
				}
				$config['total_rows'] = $query->num_rows();
				#				$config['total_rows'] = $this->db->count_all('tr_customer');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('master_customer');
				$data['icustomer'] = '';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('customer/mmaster');
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4), $area1, $area2, $area3, $area4, $area5);

				$sess	= $this->session->userdata('session_id');
				$id 	= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = "Membuka Menu Master Pelanggan";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customer/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_customer');
			$this->load->view('customer/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_customer') . " update";
			if ($this->uri->segment(4)) {
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('customer/mmaster');
				$data['isi'] = $this->mmaster->baca($icustomer);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Membuka Edit Master Pelanggan:(' . $icustomer . ')';
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customer/vmainform', $data);
			} else {
				$this->load->view('customer/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*
			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= trim($this->input->post('icustomerplugroup', TRUE));
			$icity				= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
//			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$ipricegroup			= $this->input->post('nline', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress		= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);
			$ncustomerretensi		= $this->input->post('ncustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if($ncustomertoplength=='')
				$ncustomertoplength=0;
*/
			$icustomer	      		= $this->input->post('icustomer', TRUE);
			$icity          			= $this->input->post('icity', TRUE);
			$iarea								= $this->input->post('iarea', TRUE);
			$isalesman						= $this->input->post('isalesman', TRUE);
			$ecityname      			= $this->input->post('ecityname', TRUE);
			$ecustomercity   			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal  		= $this->input->post('epostal1', TRUE);
			$icustomergroup				= $this->input->post('icustomergroup', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icustomerproducttype	= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct = $this->input->post('icustomerspecialproduct', TRUE);
			$icustomerstatus			= $this->input->post('icustomerstatus', TRUE);
			$icustomergrade				= $this->input->post('icustomergrade', TRUE);
			$icustomerservice			= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$esalesmanname				= $this->input->post('esalesmanname', TRUE);
			$ecustomerfax   			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail	  		= $this->input->post('ecustomermail', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$dsurvey							= $this->input->post('dsurvey', TRUE);
			if ($dsurvey != '') {
				$tmp = explode("-", $dsurvey);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dsurvey = $th . "-" . $bl . "-" . $hr;
			} else {
				$dsurvey = null;
			}
			$nvisitperiod					= $this->input->post('nvisitperiod', TRUE);
			if ($nvisitperiod == '') $nvisitperiod = 0;
			$iretensi   					= $this->input->post('iretensi', TRUE);
			$baru = $this->input->post('chkcriterianew', TRUE);
			#			$fcustomernew					= $this->input->post('fcustomernew', TRUE);
			#			if($fcustomernew!='')
			if ($baru == '')
				$fcustomernew			= 'f';
			else
				$fcustomernew			= 't';
			$ecustomername				= $this->input->post('ecustomername', TRUE);
			#			$ecustomername				= str_replace("'","",$ecustomername);
			$ecustomeraddress			= $this->input->post('ecustomeraddress', TRUE);
			$ecustomersign				= $this->input->post('ecustomersign', TRUE);
			$ecustomerphone				= $this->input->post('ecustomerphone', TRUE);
			$ert1									= $this->input->post('ert1', TRUE);
			$erw1									= $this->input->post('erw1', TRUE);
			$epostal1							= $this->input->post('epostal1', TRUE);
			$ecustomerkelurahan1	= $this->input->post('ecustomerkelurahan1', TRUE);
			$ecustomerkecamatan1	= $this->input->post('ecustomerkecamatan1', TRUE);
			$ecustomerkota1				= $this->input->post('ecustomerkota1', TRUE);
			$ecustomerprovinsi1		= $this->input->post('ecustomerprovinsi1', TRUE);
			$efax1								= $this->input->post('efax1', TRUE);
			$ecustomermonth				= $this->input->post('ecustomermonth', TRUE);
			$ecustomeryear				= $this->input->post('ecustomeryear', TRUE);
			$ecustomerage					= $this->input->post('ecustomerage', TRUE);
			$eshopstatus					= $this->input->post('eshopstatus', TRUE);
			$ishopstatus					= $this->input->post('ishopstatus', TRUE);
			$nshopbroad						= $this->input->post('nshopbroad', TRUE);
			$ecustomerowner				= $this->input->post('ecustomerowner', TRUE);
			$inik					  = $this->input->post('inik', TRUE) == 0 ? "" : $this->input->post('inik', TRUE);
			$ecustomerownerttl		= $this->input->post('ecustomerownerttl', TRUE);
			$ecustomerownerage		= $this->input->post('ecustomerownerage', TRUE);
			$emarriage						= $this->input->post('emarriage', TRUE);
			$imarriage						= $this->input->post('imarriage', TRUE);
			$ejeniskelamin				= $this->input->post('ejeniskelamin', TRUE);
			$ijeniskelamin				= $this->input->post('ijeniskelamin', TRUE);
			$ereligion						= $this->input->post('ereligion', TRUE);
			$ireligion						= $this->input->post('ireligion', TRUE);
			$ecustomerowneraddress = $this->input->post('ecustomerowneraddress', TRUE);
			$ecustomerownerphone	= $this->input->post('ecustomerownerphone', TRUE);
			$ecustomerownerhp			= $this->input->post('ecustomerownerhp', TRUE);
			$ecustomerownerfax		= $this->input->post('ecustomerownerfax', TRUE);
			$ecustomerownerpartner = $this->input->post('ecustomerownerpartner', TRUE);
			$ecustomerownerpartnerttl = $this->input->post('ecustomerownerpartnerttl', TRUE);
			$ecustomerownerpartnerage = $this->input->post('ecustomerownerpartnerage', TRUE);
			$ert2									= $this->input->post('ert2', TRUE);
			$erw2									= $this->input->post('erw2', TRUE);
			$epostal2							= $this->input->post('epostal2', TRUE);
			$ecustomerkelurahan2	= $this->input->post('ecustomerkelurahan2', TRUE);
			$ecustomerkecamatan2	= $this->input->post('ecustomerkecamatan2', TRUE);
			$ecustomerkota2				= $this->input->post('ecustomerkota2', TRUE);
			$ecustomerprovinsi2		= $this->input->post('ecustomerprovinsi2', TRUE);
			$ecustomersendaddress	= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomersendphone		= $this->input->post('ecustomersendphone', TRUE);
			$ecustomercontact	    = $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade = $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$etraversed						= $this->input->post('etraversed', TRUE);
			$itraversed						= $this->input->post('itraversed', TRUE);
			$fparkir							= $this->input->post('fparkir', TRUE);
			if ($fparkir != '')
				$fparkir			= 't';
			else
				$fparkir			= 'f';
			$fkuli								= $this->input->post('fkuli', TRUE);
			if ($fkuli != '')
				$fkuli			= 't';
			else
				$fkuli			= 'f';
			$eekspedisi1					= $this->input->post('eekspedisi1', TRUE);
			$eekspedisi2					= $this->input->post('eekspedisi2', TRUE);
			$ert3									= $this->input->post('ert3', TRUE);
			$erw3									= $this->input->post('erw3', TRUE);
			$epostal3							= $this->input->post('epostal3', TRUE);
			$ecustomerkota3				= $this->input->post('ecustomerkota3', TRUE);
			$ecustomerprovinsi3		= $this->input->post('ecustomerprovinsi3', TRUE);
			$ecustomerpkpnpwp			= $this->input->post('ecustomernpwp', TRUE);
			if ($ecustomerpkpnpwp != '')
				$fspbpkp			= 't';
			else
				$fspbpkp			= 'f';
			$ecustomernpwpname		= $this->input->post('ecustomernpwpname', TRUE);
			$ecustomernpwpaddress	= $this->input->post('ecustomernpwpaddress', TRUE);
			$ecustomerclassname		= $this->input->post('ecustomerclassname', TRUE);
			$icustomerclass				= $this->input->post('icustomerclass', TRUE);
			$epaymentmethod				= $this->input->post('epaymentmethod', TRUE);
			$ipaymentmethod				= $this->input->post('ipaymentmethod', TRUE);
			$ecustomerbank1				= $this->input->post('ecustomerbank1', TRUE);
			$ecustomerbankaccount1 = $this->input->post('ecustomerbankaccount1', TRUE);
			$ecustomerbankname1		= $this->input->post('ecustomerbankname1', TRUE);
			$ecustomerbank2				= $this->input->post('ecustomerbank2', TRUE);
			$ecustomerbankaccount2 = $this->input->post('ecustomerbankaccount2', TRUE);
			$ecustomerbankname2		= $this->input->post('ecustomerbankname2', TRUE);
			$ekompetitor1					= $this->input->post('ekompetitor1', TRUE);
			$ekompetitor2					= $this->input->post('ekompetitor2', TRUE);
			$ekompetitor3					= $this->input->post('ekompetitor3', TRUE);
			$nspbtoplength				= $this->input->post('ncustomertoplength', TRUE);
			$ncustomerdiscount		= $this->input->post('ncustomerdiscount', TRUE);
			$epricegroupname			= $this->input->post('epricegroupname', TRUE);
			$ipricegroup					= $this->input->post('ipricegroup', TRUE);
			$nline								= $this->input->post('nline', TRUE);
			$fkontrabon						= $this->input->post('fkontrabon', TRUE);
			if ($fkontrabon != '')
				$fkontrabon			= 't';
			else
				$fkontrabon			= 'f';
			$fplusppn						= $this->input->post('fplusppn', TRUE);
			if ($fplusppn != '')
				$fplusppn		= 't';
			else
				$fplusppn			= 'f';
			$ecall								= $this->input->post('ecall', TRUE);
			$icall								= $this->input->post('icall', TRUE);
			$ekontrabonhari				= $this->input->post('ekontrabonhari', TRUE);
			$ekontrabonjam1				= $this->input->post('ekontrabonjam1', TRUE);
			$ekontrabonjam2				= $this->input->post('ekontrabonjam2', TRUE);
			$etagihhari						= $this->input->post('etagihhari', TRUE);
			$etagihjam1						= $this->input->post('etagihjam1', TRUE);
			$etagihjam2						= $this->input->post('etagihjam2', TRUE);
			$ecustomercontact		  = $this->input->post('ecustomercontact', TRUE);
			#			$ecustomercontact			= str_replace("'","",$ecustomercontact);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$ncustomerretensi		= $this->input->post('ncustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if ($ncustomertoplength == '')
				$ncustomertoplength = 0;
			$ncustomertoplengthprint		= $this->input->post('ncustomertoplengthprint', TRUE);
			if ($ncustomertoplengthprint == '')
				$ncustomertoplengthprint = 0;
			$dspb 	= $this->input->post('dspb', TRUE);
			if ($dspb != '') {
				$tmp = explode("-", $dspb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspb = $th . "-" . $bl . "-" . $hr;
				$dspbreceive = $dspb;
				$thbl = $th . $bl;
			}
			$ecustomerretensi		= $this->input->post('ecustomerretensi', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);

			$irefcode				= $this->input->post('irefcode', TRUE); /* TAMBAHAN 19 APR 2023 */

			$this->load->model('customer/mmaster');
			$this->db->trans_begin();
			$this->mmaster->update(
				$icustomer,
				$iarea,
				$isalesman,
				$esalesmanname,
				$dsurvey,
				$nvisitperiod,
				$fcustomernew,
				$ecustomername,
				$ecustomeraddress,
				$ecustomersign,
				$ecustomerphone,
				$ert1,
				$erw1,
				$epostal1,
				$ecustomerkelurahan1,
				$ecustomerkecamatan1,
				$ecustomerkota1,
				$ecustomerprovinsi1,
				$efax1,
				$ecustomermonth,
				$ecustomeryear,
				$ecustomerage,
				$eshopstatus,
				$ishopstatus,
				$nshopbroad,
				$ecustomerowner,
				$ecustomerownerttl,
				$emarriage,
				$imarriage,
				$ejeniskelamin,
				$ijeniskelamin,
				$ereligion,
				$ireligion,
				$ecustomerowneraddress,
				$ecustomerownerphone,
				$ecustomerownerhp,
				$ecustomerownerfax,
				$ecustomerownerpartner,
				$ecustomerownerpartnerttl,
				$ecustomerownerpartnerage,
				$ert2,
				$erw2,
				$epostal2,
				$ecustomerkelurahan2,
				$ecustomerkecamatan2,
				$ecustomerkota2,
				$ecustomerprovinsi2,
				$ecustomersendaddress,
				$ecustomersendphone,
				$etraversed,
				$itraversed,
				$fparkir,
				$fkuli,
				$eekspedisi1,
				$eekspedisi2,
				$ert3,
				$erw3,
				$epostal3,
				$ecustomerkota3,
				$ecustomerprovinsi3,
				$ecustomerpkpnpwp,
				$fspbpkp,
				$ecustomernpwpname,
				$ecustomernpwpaddress,
				$ecustomerclassname,
				$icustomerclass,
				$epaymentmethod,
				$ipaymentmethod,
				$ecustomerbank1,
				$ecustomerbankaccount1,
				$ecustomerbankname1,
				$ecustomerbank2,
				$ecustomerbankaccount2,
				$ecustomerbankname2,
				$ekompetitor1,
				$ekompetitor2,
				$ekompetitor3,
				$nspbtoplength,
				$ncustomerdiscount,
				$epricegroupname,
				$ipricegroup,
				$nline,
				$fkontrabon,
				$ecall,
				$icall,
				$ekontrabonhari,
				$ekontrabonjam1,
				$ekontrabonjam2,
				$etagihhari,
				$etagihjam1,
				$etagihjam2,
				$icustomergroup,
				$icustomerplugroup,
				$icustomerproducttype,
				$icustomerspecialproduct,
				$icustomerstatus,
				$icustomergrade,
				$icustomerservice,
				$icustomersalestype,
				$ecustomerownerage,
				$ecustomerrefference,
				$iretensi,
				$fcustomerplusppn,
				$fcustomerplusdiscount,
				$fcustomerpkp,
				$fcustomeraktif,
				$fcustomertax,
				$fcustomercicil,
				$icity,
				$ecustomercity,
				$ecustomerpostal,
				$ecustomerfax,
				$ecustomermail,
				$ecustomerreceiptaddress,
				$ecustomerremark,
				$ecustomerpayment,
				$ecustomerpriority,
				$ecustomercontact,
				$ecustomercontactgrade,
				$ecustomerothersupplier,
				$ncustomertoplength,
				$ecustomerretensi,
				$ncustomertoplengthprint,
				$inik,
				$irefcode
			);
			/*
			$this->mmaster->update(
					$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
					$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
					$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
					$icustomerstatus, $ecustomername, $ecustomeraddress,$ecityname, 
					$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
					$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
					$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
					$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
					$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
					$fcustomertax, $ncustomerretensi, $ncustomertoplength, $fcustomercicil
					      );
*/
			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
				#				    $this->db->trans_rollback();

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Update Customer KodeLang:' . $icustomer;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$data['sukses']			= true;
				$data['inomor']			= $icustomer;
				$this->load->view('nomor', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomer	= $this->uri->segment(4);
			$this->load->model('customer/mmaster');
			$this->mmaster->delete($icustomer);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Hapus Customer KodeLang:' . $icustomer;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/customer/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_customer');
			$data['icustomer'] = '';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
			$this->load->view('customer/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari = strtoupper($this->input->post("cari", false));
			if ($cari == '') $cari = $this->uri->segment(4);
			if ($cari == 'zxvf') {
				$cari = '';
				$config['base_url'] = base_url() . 'index.php/customer/cform/cari/zxvf/';
			} else {
				$config['base_url'] = base_url() . 'index.php/customer/cform/cari/' . $cari . '/';
			}
			#			$config['base_url'] = base_url().'index.php/customer/cform/index/';
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                                 where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                 and a.i_customer=b.i_customer", false);
			} else {
				$query = $this->db->query("select a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                                 where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
                                 and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                 or a.i_area = '$area4' or a.i_area = '$area5')
                                 and a.i_customer=b.i_customer", false);
			}
			#			$query=$this->db->query("select * from tr_customer where i_customer like '%$cari%' or e_customer_name like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('customer/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$data['page_title'] = $this->lang->line('master_customer');
			$data['icustomer'] = '';
			$this->load->view('customer/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function plugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/plugroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plugroup');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi'] = $this->mmaster->bacaplugroup($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistplugroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariplugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/plugroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_plugroup where upper(i_customer_plugroup) like '%$cari%' 
										or upper(e_customer_plugroupname) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi'] = $this->mmaster->cariplugroup($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistplugroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea = $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url() . 'index.php/customer/cform/city/' . $iarea . '/index/';
			//			$config['total_rows'] = $this->db->count_all('tr_city');
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi'] = $this->mmaster->bacacity($iarea, $config['per_page'], $this->uri->segment(6));
			$data['iarea'] = $iarea;
			$this->load->view('customer/vlistcity', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url() . 'index.php/customer/cform/city/' . $iarea . '/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			//			$query = $this->db->query("select * from tr_city where upper(i_city) like '%cari%' or upper(e_city_name) like '%cari%'",false);
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi'] = $this->mmaster->caricity($cari, $iarea, $config['per_page'], $this->uri->segment(6));
			$data['iarea'] = $iarea;
			$this->load->view('customer/vlistcity', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customergroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi'] = $this->mmaster->bacacustomergroup($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomergroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customergroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_group where upper(i_customer_group) like '%cari%' 
										or upper(e_customer_groupname) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi'] = $this->mmaster->caricustomergroup($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomergroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/pricegroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_price_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi'] = $this->mmaster->bacapricegroup($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistpricegroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caripricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/pricegroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_price_group where upper(i_price_group) like '%cari%' 
										or upper(e_price_groupname) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi'] = $this->mmaster->caripricegroup($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistpricegroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/area/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%cari%' or upper(e_area_name) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerstatus/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_status');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi'] = $this->mmaster->bacacustomerstatus($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerstatus', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerstatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_status where upper(i_customer_status) like '%cari%' 
										or upper(e_customer_statusname) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi'] = $this->mmaster->caricustomerstatus($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerstatus', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerproducttype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_producttype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi'] = $this->mmaster->bacacustomerproducttype($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerproducttype', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerproducttype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_producttype where upper(i_customer_producttype) like '%cari%' 
										or upper(e_customer_producttypename) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi'] = $this->mmaster->caricustomerproducttype($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerproducttype', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomerproducttype = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerspecialproduct/index/';
			$this->db->where("i_customer_producttype='$icustomerproducttype'");
			$config['total_rows'] = $this->db->count_all('tr_customer_specialproduct');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi'] = $this->mmaster->bacacustomerspecialproduct($icustomerproducttype, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerspecialproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerspecialproduct/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_specialproduct 
										where upper(i_customer_specialproduct) like '%cari%' 
										or upper(e_customer_specialproductname) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi'] = $this->mmaster->caricustomerspecialproduct($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerspecialproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customergrade/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_grade');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi'] = $this->mmaster->bacacustomergrade($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomergrade', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customergrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_grade 
										where upper(i_customer_grade) like '%cari%' 
										or upper(e_customer_gradename) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi'] = $this->mmaster->caricustomergrade($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomergrade', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerservice/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_service');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi'] = $this->mmaster->bacacustomerservice($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerservice', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerservice/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_customer_service where upper(i_customer_service) like '%cari%' 
										or upper(e_customer_servicename) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi'] = $this->mmaster->caricustomerservice($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerservice', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customersalestype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_salestype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi'] = $this->mmaster->bacacustomersalestype($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomersalestype', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customersalestype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_salestype where upper(i_customer_salestype) like '%cari%' 
										or upper(e_customer_salestypename) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi'] = $this->mmaster->caricustomersalestype($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomersalestype', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerclass/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_class');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi'] = $this->mmaster->bacacustomerclass($config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerclass', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu45') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customer/cform/customerclass/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_class where upper(i_customer_class) like '%cari%' 
										or upper(e_customer_classname) like '%cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi'] = $this->mmaster->caricustomerclass($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('customer/vlistcustomerclass', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
