<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu264')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exprekapspb');
			$data['iperiode']	= '';
			$this->load->view('exp-rekapspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu264')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-rekapspb/mmaster');
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
      $a=substr($iperiode,2,2);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;
      $no='SPB-'.$a.$b.'-%';
      $this->db->select("a.*, b.e_customer_name, c.i_dkb, c.d_dkb, v_nota_netto, d.e_city_name
						from tm_spb a
						left join tr_customer b on a.i_customer=b.i_customer 
						left join tm_nota c on a.i_spb = c.i_spb
						left join tr_city d on b.i_city = d.i_city
						where a.i_customer=b.i_customer and a.i_area=d.i_area
						and a.i_spb like '$no' order by i_spb",false);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Rekap SPB ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			//$status='Status OP';
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
				#$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
				#$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:O1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TGL SPB');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NO SPB');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'KODE TOKO');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NAMA TOKO');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'SPB SEBELUM NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'RUPIAH SPB');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TGL SJ');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'NO SJ');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'RUPIAH SJ');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'NO DKB');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'TGL DKB');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'TGL APPROVE KEUANGAN');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'KOTA');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'STATUS');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				/*
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'NILAI SJ');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'NILAI NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);*/
				$i=2;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':O'.$i
				  );
          /*
          if($row->d_sj!=''){
            $tmp=explode('-',$row->d_sj);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_sj=$hr.'-'.$bl.'-'.$th;
          }
          if($row->d_nota!=''){
            $tmp=explode('-',$row->d_nota);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_nota=$hr.'-'.$bl.'-'.$th;
          }
          if($row->d_jatuh_tempo!=''){
            $tmp=explode('-',$row->d_jatuh_tempo);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_jatuh_tempo=$hr.'-'.$bl.'-'.$th;
          }
          if($row->d_spb!=''){
            $tmp=explode('-',$row->d_spb);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_spb=$hr.'-'.$bl.'-'.$th;
          }*/

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_spb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_spb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $bersih = $row->v_spb-$row->v_spb_discounttotal;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $bersih, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->v_spb_after, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->d_sj, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_dkb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->d_dkb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->d_approve2, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->e_city_name, Cell_DataType::TYPE_STRING);
          if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			  	$status='Batal';
			  }elseif(
				 	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					 ){
			  	$status='Sales';
			  }elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove != null)
					 ){
			  	$status='Reject (sls)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					  ($row->i_notapprove == null)
					 ){
			  	$status='Keuangan';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					  ($row->i_notapprove != null)
					 ){
			  	$status='Reject (ar)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store == null)
					 ){
			  	$status='Gudang';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					 ){
			  	$status='Pemenuhan SPB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					 ){
			  	$status='Proses OP';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					 ){
			  	$status='OP Close';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					 ){
			  	$status='Siap SJ (sales)';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
#			  	$status='Siap SJ (gudang)';
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
        }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					 ){
			  	$status='Siap SJ';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap DKB';
			  }elseif(
					  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			  		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					 ){
			  	$status='Siap Nota';
			  }elseif(
					  ($row->i_approve1 != null) && 
			  		  ($row->i_approve2 != null) &&
			   		  ($row->i_store != null) && 
					  ($row->i_nota != null) 
					 ){
			  	$status='Sudah dinotakan';			  
			  }elseif(($row->i_nota != null)){
			  	$status='Sudah dinotakan';
			  }else{
			  	$status='Unknown';		
			  }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $status, Cell_DataType::TYPE_STRING);
          #$objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->i_dkb, Cell_DataType::TYPE_NUMERIC);
          #$objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->d_dkb, Cell_DataType::TYPE_NUMERIC);
					$i++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='rekapspb'.$iperiode.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Rekap SPB Periode'.$iperiode;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
