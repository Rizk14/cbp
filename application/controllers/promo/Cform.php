<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode = $this->db->query("SELECT i_periode FROM tm_periode", FALSE)->row()->i_periode;
			$ipromo 	= $this->input->post('ipromo', TRUE);
			$dpromo 	= $this->input->post('dpromo', TRUE);
			$ipromotype	= $this->input->post('ipromotype', TRUE);
			if($dpromo!=''){
				$tmp=explode("-",$dpromo);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromo=$th."-".$bl."-".$hr;
			}
			$dpromostart 	= $this->input->post('dpromostart', TRUE);
			if($dpromostart!=''){
				$tmp=explode("-",$dpromostart);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromostart=$th."-".$bl."-".$hr;
			}
			$dpromofinish 	= $this->input->post('dpromofinish', TRUE);
			if($dpromofinish!=''){
				$tmp=explode("-",$dpromofinish);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromofinish=$th."-".$bl."-".$hr;
			}
			$ipricegroup     = $this->input->post('ipricegroup', TRUE);
			$npromodiscount1 = $this->input->post('npromodiscount1', TRUE);
			$npromodiscount2 = $this->input->post('npromodiscount2', TRUE);
			$epromoname	= $this->input->post('epromoname', TRUE);
			$fallproduct		= $this->input->post('fallproduct',TRUE);
			if($fallproduct!='')
				$fallproduct="t";
			else
				$fallproduct="f";
			$fallbaby		= $this->input->post('fallbaby',TRUE);
			if($fallbaby!='')
				$fallbaby="t";
			else
				$fallbaby="f";
			$fallreguler	= $this->input->post('fallreguler',TRUE);
			if($fallreguler!='')
				$fallreguler="t";
			else
				$fallreguler="f";
			$fallnb		= $this->input->post('fallnb',TRUE);
			if($fallnb!='')
				$fallnb="t";
			else
				$fallnb="f";
			$fallcustomer		= $this->input->post('fallcustomer',TRUE);
			if($fallcustomer!='')
				$fallcustomer="t";
			else
				$fallcustomer="f";
			$fcustomergroup		= $this->input->post('fcustomergroup',TRUE);
			if($fcustomergroup!='')
				$fcustomergroup="t";
			else
				$fcustomergroup="f";
			$fallarea		= $this->input->post('fallarea',TRUE);
			if($fallarea!='')
				$fallarea="t";
			else
				$fallarea="f";
			$jmlp		= $this->input->post('jmlp', TRUE);
			$jmlc		= $this->input->post('jmlc', TRUE);
			$jmlg		= $this->input->post('jmlg', TRUE);
			$jmla		= $this->input->post('jmla', TRUE);
			if(
				($dpromo!='' && $ipromotype!='' && $dpromostart!='' && $dpromofinish!='' && $epromoname!='')
			  )
			{
				if(
					($jmlp!='0' || $jmlc!='0' || $jmlg!='0' || $jmla!='0')
				  )
				{
					$this->db->trans_begin();
					$this->load->model('promo/mmaster');
					$ipromo	=$this->mmaster->runningnumber();
					$this->mmaster->insertheader($ipromo,$dpromo,$ipromotype,$dpromostart,$dpromofinish,
												 $epromoname,$fallproduct,$fallcustomer,$fcustomergroup,
												 $npromodiscount1,$npromodiscount2,$fallbaby,$fallreguler,$fallarea,$ipricegroup,$fallnb);
					if($jmlp!='0'){				
						for($i=1;$i<=$jmlp;$i++){
						  $iproduct				= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade	= 'A';
						  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
						  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice			= $this->input->post('vunitprice'.$i, TRUE);
						  $vunitprice2			= str_replace(',','',$vunitprice);
						  $nquantitymin		= $this->input->post('nquantitymin'.$i, TRUE);
						  $this->mmaster->insertdetailp($ipromo,$ipromotype,$iproduct,$iproductgrade,$eproductname,
														$nquantitymin,$vunitprice2,$iproductmotif);
						}
					}
					if($jmlc!='0'){				
						for($i=1;$i<=$jmlc;$i++){
						  $icustomer		=$this->input->post('icustomer'.$i, TRUE);
						  $ecustomername	=$this->input->post('ecustomername'.$i, TRUE);
						  $ecustomeraddress	=$this->input->post('ecustomeraddress'.$i, TRUE);
						  $qu=$this->db->query("select i_area from tr_customer 
												where i_customer = '$icustomer' ", false);
						  if ($qu->num_rows() > 0){
							foreach($qu->result() as $w){							
						  		$this->mmaster->insertdetailc($ipromo,$ipromotype,$icustomer,$ecustomername,$ecustomeraddress,$w->i_area);
							}
						  }
						}
					}
					if($jmlg!='0'){				
						for($i=1;$i<=$jmlg;$i++){
						  $icustomergroup		=$this->input->post('icustomergroup'.$i, TRUE);
						  $ecustomergroupname	=$this->input->post('ecustomergroupname'.$i, TRUE);
						  $qu=$this->db->query(" select distinct(i_area) as i_area from tr_customer 
									   where i_customer_group = '$icustomergroup' ", false);
						  if ($qu->num_rows() > 0){
							foreach($qu->result() as $w){							
								$this->mmaster->insertdetailg($ipromo,$ipromotype,$icustomergroup,$ecustomergroupname,$w->i_area);
							}
						  }
						}
					}
					if($jmla!='0'){				
						for($i=1;$i<=$jmla;$i++){
						  $iarea		=$this->input->post('iarea'.$i, TRUE);
						  $eareaname	=$this->input->post('eareaname'.$i, TRUE);
				  		$this->mmaster->insertdetaila($ipromo,$ipromotype,$iarea,$eareaname);
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

				    $sess=$this->session->userdata('session_id');
				    $id=$this->session->userdata('user_id');
				    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				    $rs		= pg_query($sql);
				    if(pg_num_rows($rs)>0){
					    while($row=pg_fetch_assoc($rs)){
						    $ip_address	  = $row['ip_address'];
						    break;
					    }
				    }else{
					    $ip_address='kosong';
				    }
				    $query 	= pg_query("SELECT current_timestamp as c");
				    while($row=pg_fetch_assoc($query)){
					    $now	  = $row['c'];
				    }
				    $pesan='Input Promo No:'.$ipromo;
				    $this->load->model('logger');
				    $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $ipromo;
						$this->load->view('nomor',$data);
					}
				}else if(
					($jmlp=='0' && $jmlc=='0' && $jmlg=='0' && $jmla=='0' && $fallproduct=='t' && $fallcustomer=='t')	
					||
					($jmlp=='0' && $jmlc=='0' && $jmlg=='0' && $jmla=='0' && $fallbaby=='t' && $fallcustomer=='t')	
					||
					($jmlp=='0' && $jmlc=='0' && $jmlg=='0' && $jmla=='0' && $fallreguler=='t' && $fallcustomer=='t')	
				  )
				{
					$this->db->trans_begin();
					$this->load->model('promo/mmaster');
					$ipromo	=$this->mmaster->runningnumber();
					$this->mmaster->insertheader($ipromo,$dpromo,$ipromotype,$dpromostart,$dpromofinish,
												 $epromoname,$fallproduct,$fallcustomer,$fcustomergroup,
												 $npromodiscount1,$npromodiscount2,$fallbaby,$fallreguler,$fallarea,$ipricegroup,$fallnb);
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
						$data['sukses']			= true;
						$data['inomor']			= $ipromo;
						$this->load->view('nomor',$data);
					}	
				}
			}else{
				$data['page_title'] = $this->lang->line('promo');
				$data['ipromo']='';
				$this->load->model('promo/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$data['jenis']=$this->mmaster->bacajenis();
				$data['detail']="";
				$data['jmlitem']="";
				$data['iperiode'] = $iperiode;
				$this->load->view('promo/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('promo');
			$this->load->view('promo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('promo')." update";
			if($this->uri->segment(4)){
				$ipromo = $this->uri->segment(4);
				$data['ipromo'] = $ipromo;
				$query = $this->db->query("select * from tm_promo_item where i_promo = '$ipromo'");
				$data['jmlitemp'] = $query->num_rows(); 				
				$query = $this->db->query("select * from tm_promo_customer where i_promo = '$ipromo'");
				$data['jmlitemc'] = $query->num_rows(); 				
				$query = $this->db->query("select distinct(i_customer_group) from tm_promo_customergroup where i_promo = '$ipromo'");
				$data['jmlitemg'] = $query->num_rows(); 				
				$query = $this->db->query("select * from tm_promo_area where i_promo = '$ipromo'");
				$data['jmlitema'] = $query->num_rows(); 				
				$this->load->model('promo/mmaster');
        $data['areauser'] = $this->session->userdata('i_area');
				$data['isi']=$this->mmaster->baca($ipromo);
				$data['jenis']=$this->mmaster->bacajenis();
				$data['detailp']=$this->mmaster->bacadetailp($ipromo);
				$data['detailc']=$this->mmaster->bacadetailc($ipromo);
				$data['detailg']=$this->mmaster->bacadetailg($ipromo);
				$data['detaila']=$this->mmaster->bacadetaila($ipromo);
		 		$this->load->view('promo/vmainform',$data);
			}else{
				$this->load->view('promo/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$ipromo 	= $this->input->post('ipromo', TRUE);
			$dpromo 	= $this->input->post('dpromo', TRUE);
			$ipromotype	= $this->input->post('ipromotype', TRUE);
			if($dpromo!=''){
				$tmp=explode("-",$dpromo);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromo=$th."-".$bl."-".$hr;
			}
			$dpromostart 	= $this->input->post('dpromostart', TRUE);
			if($dpromostart!=''){
				$tmp=explode("-",$dpromostart);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromostart=$th."-".$bl."-".$hr;
			}
			$dpromofinish 	= $this->input->post('dpromofinish', TRUE);
			if($dpromofinish!=''){
				$tmp=explode("-",$dpromofinish);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpromofinish=$th."-".$bl."-".$hr;
			}
      $ipricegroup     = $this->input->post('ipricegroup', TRUE);
			$npromodiscount1 = $this->input->post('npromodiscount1', TRUE);
			$npromodiscount2 = $this->input->post('npromodiscount2', TRUE);
			$epromoname	= $this->input->post('epromoname', TRUE);
			$fallproduct		= $this->input->post('fallproduct',TRUE);
			if($fallproduct!='')
				$fallproduct="t";
			else
				$fallproduct="f";
			$fallbaby		= $this->input->post('fallbaby',TRUE);
			if($fallbaby!='')
				$fallbaby="t";
			else
				$fallbaby="f";
			$fallreguler	= $this->input->post('fallreguler',TRUE);
			if($fallreguler!='')
				$fallreguler="t";
			else
				$fallreguler="f";
			$fallcustomer		= $this->input->post('fallcustomer',TRUE);
			if($fallcustomer!='')
				$fallcustomer="t";
			else
				$fallcustomer="f";
			$fcustomergroup		= $this->input->post('fcustomergroup',TRUE);
			if($fcustomergroup!='')
				$fcustomergroup="t";
			else
				$fcustomergroup="f";
			$fallarea		= $this->input->post('fallarea',TRUE);
			if($fallarea!='')
				$fallarea="t";
			else
				$fallarea="f";
			$jmlp		= $this->input->post('jmlp', TRUE);
			$jmlc		= $this->input->post('jmlc', TRUE);
			$jmlg		= $this->input->post('jmlg', TRUE);
			$jmla		= $this->input->post('jmla', TRUE);
			if(
				($dpromo!='' && $ipromotype!='' && $dpromostart!='' && $dpromofinish!='' && $epromoname!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('promo/mmaster');
				$this->mmaster->updateheader($ipromo,$dpromo,$ipromotype,$dpromostart,$dpromofinish,
											 $epromoname,$fallproduct,$fallcustomer,$fcustomergroup,
											 $npromodiscount1,$npromodiscount2,$fallbaby,$fallreguler,$fallarea,$ipricegroup);
				if($jmlp!='0'){				
					for($i=1;$i<=$jmlp;$i++){
					  $iproduct			=$this->input->post('iproduct'.$i, TRUE);
					  $iproductgrade	='A';
					  $iproductmotif	=$this->input->post('motif'.$i, TRUE);
					  $eproductname		=$this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		=$this->input->post('vunitprice'.$i, TRUE);
					  $vunitprice2		=str_replace(',','',$vunitprice);
					  $nquantitymin		=$this->input->post('nquantitymin'.$i, TRUE);
					  $this->mmaster->deletedetailp($ipromo,$iproduct,$iproductgrade,$iproductmotif);
					  $this->mmaster->insertdetailp($ipromo,$ipromotype,$iproduct,$iproductgrade,$eproductname,
													$nquantitymin,$vunitprice2,$iproductmotif);
					}
				}
				if($jmlc!='0'){				
					for($i=1;$i<=$jmlc;$i++){
					  $icustomer		=$this->input->post('icustomer'.$i, TRUE);
					  $ecustomername	=$this->input->post('ecustomername'.$i, TRUE);
					  $ecustomeraddress	=$this->input->post('ecustomeraddress'.$i, TRUE);
					  $this->mmaster->deletedetailc($ipromo,$icustomer);
					  $qu=$this->db->query("select i_area from tr_customer 
											where i_customer = '$icustomer' ", false);
					  if ($qu->num_rows() > 0){
						foreach($qu->result() as $w){							
					  		$this->mmaster->insertdetailc($ipromo,$ipromotype,$icustomer,$ecustomername,$ecustomeraddress,$w->i_area);
						}
					  }
					}
				}
				if($jmlg!='0'){				
					for($i=1;$i<=$jmlg;$i++){
					  $icustomergroup		=$this->input->post('icustomergroup'.$i, TRUE);
					  $ecustomergroupname	=$this->input->post('ecustomergroupname'.$i, TRUE);
					  $this->mmaster->deletedetailg($ipromo,$icustomergroup);
					  //$this->mmaster->insertdetailg($ipromo,$ipromotype,$icustomergroup,$ecustomergroupname);
					  $qu=$this->db->query(" select distinct(i_area) as i_area from tr_customer 
									   where i_customer_group = '$icustomergroup' ", false);
					  if ($qu->num_rows() > 0){
						foreach($qu->result() as $w){							
							$this->mmaster->insertdetailg($ipromo,$ipromotype,$icustomergroup,$ecustomergroupname,$w->i_area);
						}
					  }
					}
				}
				if($jmla!='0'){				
					for($i=1;$i<=$jmla;$i++){
						  $iarea		=$this->input->post('iarea'.$i, TRUE);
						  $eareaname	=$this->input->post('eareaname'.$i, TRUE);
						  $this->mmaster->deletedetaila($ipromo,$iarea);
				  		$this->mmaster->insertdetaila($ipromo,$ipromotype,$iarea,$eareaname);
					}
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Promo:'.$ipromo;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ipromo;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletep()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ipromo			= $this->input->post('ipromodelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			if($ipromo=='')$ipromo=$this->uri->segment(4);
			if($iproduct=='')$iproduct=$this->uri->segment(5);
			if($iproductgrade=='')$iproductgrade=$this->uri->segment(6);
			if($iproductmotif=='')$iproductmotif=$this->uri->segment(7);
			$this->load->model('promo/mmaster');
			$this->mmaster->deletedetailp($ipromo,$iproduct,$iproductgrade,$iproductmotif);
			$data['page_title'] = $this->lang->line('promo');
			$data['ipromo'] = $ipromo;
			$query = $this->db->query("select * from tm_promo_item where i_promo = '$ipromo'");
			$data['jmlitemp'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customer where i_promo = '$ipromo'");
			$data['jmlitemc'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customergroup where i_promo = '$ipromo'");
			$data['jmlitemg'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_area where i_promo = '$ipromo'");
			$data['jmlitema'] = $query->num_rows(); 				
			$this->load->model('promo/mmaster');
			$data['isi']=$this->mmaster->baca($ipromo);
			$data['jenis']=$this->mmaster->bacajenis();
			$data['detailp']=$this->mmaster->bacadetailp($ipromo);
			$data['detailc']=$this->mmaster->bacadetailc($ipromo);
			$data['detailg']=$this->mmaster->bacadetailg($ipromo);
			$data['detaila']=$this->mmaster->bacadetaila($ipromo);
      $data['areauser'] = $this->session->userdata('i_area');
	 		$this->load->view('promo/vmainform',$data);
		}
	}
	function deletec()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ipromo			= $this->input->post('ipromodelete', TRUE);
			$icustomer		= $this->input->post('icustomerdelete', TRUE);
			if($ipromo=='')$ipromo=$this->uri->segment(4);
			if($icustomer=='')$icustomer=$this->uri->segment(5);
			$this->load->model('promo/mmaster');
			$this->mmaster->deletedetailc($ipromo,$icustomer);
			$data['page_title'] = $this->lang->line('promo');
			$data['ipromo'] = $ipromo;
			$query = $this->db->query("select * from tm_promo_item where i_promo = '$ipromo'");
			$data['jmlitemp'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customer where i_promo = '$ipromo'");
			$data['jmlitemc'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customergroup where i_promo = '$ipromo'");
			$data['jmlitemg'] = $query->num_rows();
			$query = $this->db->query("select * from tm_promo_area where i_promo = '$ipromo'");
			$data['jmlitema'] = $query->num_rows(); 				
			$this->load->model('promo/mmaster');
			$data['isi']=$this->mmaster->baca($ipromo);
			$data['jenis']=$this->mmaster->bacajenis();
			$data['detailp']=$this->mmaster->bacadetailp($ipromo);
			$data['detailc']=$this->mmaster->bacadetailc($ipromo);
			$data['detailg']=$this->mmaster->bacadetailg($ipromo);
			$data['detaila']=$this->mmaster->bacadetaila($ipromo);
      $data['areauser'] = $this->session->userdata('i_area');
	 		$this->load->view('promo/vmainform',$data);
		}
	}
	function deleteg()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ipromo			= $this->input->post('ipromodelete', TRUE);
			$icustomergroup	= $this->input->post('icustomergroupdelete', TRUE);
			if($ipromo=='')$ipromo=$this->uri->segment(4);
			if($icustomergroup=='')$icustomergroup=$this->uri->segment(5);
			$this->load->model('promo/mmaster');
			$this->mmaster->deletedetailg($ipromo,$icustomergroup);
			$data['page_title'] = $this->lang->line('promo');
			$data['ipromo'] = $ipromo;
			$query = $this->db->query("select * from tm_promo_item where i_promo = '$ipromo'");
			$data['jmlitemp'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customer where i_promo = '$ipromo'");
			$data['jmlitemc'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customergroup where i_promo = '$ipromo'");
			$data['jmlitemg'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_area where i_promo = '$ipromo'");
			$data['jmlitema'] = $query->num_rows(); 				
			$this->load->model('promo/mmaster');
			$data['isi']=$this->mmaster->baca($ipromo);
			$data['jenis']=$this->mmaster->bacajenis();
			$data['detailp']=$this->mmaster->bacadetailp($ipromo);
			$data['detailc']=$this->mmaster->bacadetailc($ipromo);
			$data['detailg']=$this->mmaster->bacadetailg($ipromo);
			$data['detaila']=$this->mmaster->bacadetaila($ipromo);
      $data['areauser'] = $this->session->userdata('i_area');
	 		$this->load->view('promo/vmainform',$data);
		}
	}
/*****/
	function deletea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ipromo	= $this->input->post('ipromodelete', TRUE);
			$iarea	= $this->input->post('iareadelete', TRUE);
			if($ipromo=='')$ipromo=$this->uri->segment(4);
			if($iarea=='')$iarea=$this->uri->segment(5);
			$this->load->model('promo/mmaster');
			$this->mmaster->deletedetaila($ipromo,$iarea);
			$data['page_title'] = $this->lang->line('promo');
			$data['ipromo'] = $ipromo;
			$query = $this->db->query("select * from tm_promo_item where i_promo = '$ipromo'");
			$data['jmlitemp'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customer where i_promo = '$ipromo'");
			$data['jmlitemc'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_customergroup where i_promo = '$ipromo'");
			$data['jmlitemg'] = $query->num_rows(); 				
			$query = $this->db->query("select * from tm_promo_area where i_promo = '$ipromo'");
			$data['jmlitema'] = $query->num_rows(); 				
			$data['isi']=$this->mmaster->baca($ipromo);
			$data['jenis']=$this->mmaster->bacajenis();
			$data['detailp']=$this->mmaster->bacadetailp($ipromo);
			$data['detailc']=$this->mmaster->bacadetailc($ipromo);
			$data['detailg']=$this->mmaster->bacadetailg($ipromo);
			$data['detaila']=$this->mmaster->bacadetaila($ipromo);
      $data['areauser'] = $this->session->userdata('i_area');
	 		$this->load->view('promo/vmainform',$data);
		}
	}
/*****/
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			//$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/product/'.$baris.'/sikasep/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			/* data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari); */
			$baris   = $this->input->post('baris', FALSE);
	        if($baris=='')$baris=$this->uri->segment(4);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
	        if($cari!='sikasep')
	          $config['base_url'] = base_url().'index.php/promo/cform/cariproduct/'.$baris.'/'.$cari.'/';
	            else
			  $config['base_url'] = base_url().'index.php/promo/cform/cariproduct/'.$baris.'/sikasep/';
			  
			/* $query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false); */
			$query = $this->db->query("	select a.i_product as kode, a.i_product_motif as motif,
									  a.e_product_motifname as namamotif, 
									  c.e_product_name as nama,c.v_product_retail as harga
									  from tr_product_motif a,tr_product c
									  where a.i_product=c.i_product
										 and (upper(a.i_product) ilike '%$cari%' or upper(c.e_product_name) ilike '%$cari%')"
									,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['cari']=$cari;
			$this->load->view('promo/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customer/'.$baris.'/index/';
			$query = $this->db->query("select * from tr_customer ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customer/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_customer
						   where upper(i_customer) like '%$cari%' 
						      or upper(e_customer_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/promo/cform/index/';
			$query = $this->db->query("select * from tm_promo
						   where upper(i_promo) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_promo');
			$data['ipromo']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('promo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customergroup/'.$baris.'/index/';
			$query = $this->db->query("select * from tr_customer_group ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customergroup/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_customer_group
						   where upper(i_customer_group) like '%$cari%' 
						      or upper(e_customer_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->caricustomergroup($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('promo/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customerupdate/'.$baris.'/index/';
			$query = $this->db->query("select * from tr_customer ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomerupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customerupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_customer
									   where upper(i_customer) like '%$cari%' 
									  	  or upper(e_customer_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomerupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergroupupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customergroupupdate/'.$baris.'/index/';
			$query = $this->db->query("select * from tr_customer_group ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomergroupupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroupupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/customergroupupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_customer_group
						   where upper(i_customer_group) like '%$cari%' 
						      or upper(e_customer_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->caricustomergroup($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistcustomergroupupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/area/'.$baris.'/sikasep/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			// $data['baris']=$this->uri->segment(4);
			// $baris=$this->uri->segment(4);
			// $config['base_url'] = base_url().'index.php/promo/cform/area/'.$baris.'/index/';
			// $cari = $this->input->post('cari', FALSE);
			// $cari=strtoupper($cari);
			$baris   = $this->input->post('baris', FALSE);
	        if($baris=='')$baris=$this->uri->segment(4);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
	        if($cari!='sikasep')
	          $config['base_url'] = base_url().'index.php/promo/cform/cariarea/'.$baris.'/'.$cari.'/';
	            else
			  $config['base_url'] = base_url().'index.php/promo/cform/cariarea/'.$baris.'/sikasep/';
			  
			$query = $this->db->query("select * from tr_area where upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['baris']=$baris;
			$data['cari']=$cari;
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $kode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/pricegroup/'.$kode.'/sikasep/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      if($kode=='2'||$kode=='4'||$kode=='5'||$kode=='6'||$kode==''){
			  $query= $this->db->query("select distinct(i_price_group) as i_price_group from tr_product_price 
                                  where i_price_group like '%$cari%' and i_price_group not in (select i_price_group from tr_price_group)",false);
      }else{
			  $query= $this->db->query("select distinct(i_price_group) as i_price_group from tr_product_price 
                                  where i_price_group like '%$cari%'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($kode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('promo/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroupupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu89')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $kode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/promo/cform/pricegroupupdate/'.$kode.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      if($kode=='2'||$kode=='4'||$kode=='5'||$kode=='6'||$kode==''){
			  $query= $this->db->query("select distinct(i_price_group) as i_price_group from tr_product_price 
                                  where i_price_group like '%$cari%' and i_price_group not in (select i_price_group from tr_price_group)",false);
      }else{
			  $query= $this->db->query("select distinct(i_price_group) as i_price_group from tr_product_price 
                                  where i_price_group like '%$cari%'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('promo/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->pricegroupupdate($kode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('promo/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
