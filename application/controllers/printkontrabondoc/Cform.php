<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('printkontrabon');
			$data['dfrom']='';
			$data['dto']='';
			$data['notafrom']='';
			$data['notato']='';
 			$data['isupplier']='';
			$this->load->view('printkontrabondoc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$notafrom		= $this->input->post('notafrom');
			$notato		= $this->input->post('notato');
			$isupplier	= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			if($notafrom=='') $notafrom=$this->uri->segment(7);
			if($notato=='') $notato=$this->uri->segment(8);
			$this->load->model('printkontrabondoc/mmaster');
			$data['page_title'] = $this->lang->line('printkontrabon');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['notafrom']		= str_replace("%20","",$notafrom);
			$data['notato']		= str_replace("%20","",$notato);
			$data['isi']		= $this->mmaster->bacaperiode($notafrom,$notato,$isupplier,$dfrom,$dto,1000,$this->uri->segment(9),$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Kontra Bon supplier:'.$isupplier.' Periode:'.$dfrom.' s/d :'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printkontrabondoc/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('printkontrabon');
			$this->load->view('printkontrabondoc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printkontrabondoc/cform/supplier/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$isupplier=$this->uri->segment(4);
			$query = $this->db->query("	select * from tr_supplier where upper(i_supplier) like '%$cari%' 
              										or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printkontrabondoc/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('printkontrabondoc/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printkontrabondoc/cform/supplier/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$isupplier=$this->uri->segment(4);
			$query = $this->db->query("	select * from tr_supplier where upper(i_supplier) like '%$cari%' 
										or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printkontrabondoc/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printkontrabondoc/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notafrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom=$this->uri->segment(4);
			$dto=$this->uri->segment(5);
			$supp=$this->uri->segment(6);
      if($dfrom=='')$dfrom=$this->input->post("dfrom");
      if($dto=='')$dto=$this->input->post("dto");
      if($supp=='')$supp=$this->input->post("supp");
			$config['base_url'] = base_url().'index.php/printkontrabondoc/cform/notafrom/'.$dfrom.'/'.$dto.'/'.$supp.'/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("	select i_dtap from tm_dtap 
                                  where i_supplier='$supp'
                                  and d_dtap >= to_date('$dfrom','dd-mm-yyyy')
                                  and d_dtap <= to_date('$dto','dd-mm-yyyy')
                                  and (upper(i_dtap) like '%$cari%'
                                  or upper(i_supplier) like '%$cari%') ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printkontrabondoc/mmaster');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['supp']=$supp;
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']		=$this->mmaster->bacanotafrom($dfrom,$dto,$supp,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('printkontrabondoc/vlistnotafrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notato()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu286')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom=$this->uri->segment(4);
			$dto=$this->uri->segment(5);
			$supp=$this->uri->segment(6);
			$notafrom=$this->uri->segment(7);
      if($dfrom=='')$dfrom=$this->input->post("dfrom");
      if($dto=='')$dto=$this->input->post("dto");
      if($supp=='')$supp=$this->input->post("supp");
      if($notafrom=='')$notafrom=$this->input->post("notafrom");
			$config['base_url'] = base_url().'index.php/printkontrabondoc/cform/notato/'.$dfrom.'/'.$dto.'/'.$supp.'/'.$notafrom.'/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("	select i_dtap from tm_dtap 
                                  where i_supplier='$supp'
                                  and i_dtap>'$notafrom'
                                  and d_dtap >= to_date('$dfrom','dd-mm-yyyy')
                                  and d_dtap <= to_date('$dto','dd-mm-yyyy')
                                  and (upper(i_dtap) like '%$cari%'
                                  or upper(i_supplier) like '%$cari%') ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printkontrabondoc/mmaster');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['supp']=$supp;
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']		=$this->mmaster->bacanotato($notafrom,$dfrom,$dto,$supp,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('printkontrabondoc/vlistnotato', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
