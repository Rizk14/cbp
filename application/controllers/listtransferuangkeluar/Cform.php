<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_kuk');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listtransferuangkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listtransferuangkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$teksquery="select distinct a.i_kuk
                  from tm_kuk a
                  left join tr_supplier b on(a.i_supplier=b.i_supplier)
                  left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' 
                  and a.i_supplier=c.i_supplier and ((c.i_jenis_bayar!='02' and 
                  c.i_jenis_bayar!='01' and 
                  c.i_jenis_bayar!='04' and 
                  C.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)))
                  where (upper(a.i_kuk) like '%$cari%' or upper(a.i_supplier) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery."
					and 
          a.i_supplier='$isupplier' and
          (a.d_kuk >= to_date('$dfrom','dd-mm-yyyy') and
          a.d_kuk <= to_date('$dto','dd-mm-yyyy'))";
			$query = $this->db->query($teksquery,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_kuk');
			$this->load->model('listtransferuangkeluar/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Transfer uang keluar Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listtransferuangkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_kuk');
			$this->load->view('listtransferuangkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikuk			= $this->uri->segment(4);
		  $ikuk			= str_replace('%20','',$ikuk);
			$nkukyear	= $this->uri->segment(5);
			$isupplier= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);
			$this->load->model('listtransferuangkeluar/mmaster');
			$this->mmaster->delete($ikuk,$nkukyear);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Transfer uang keluar Supplier '.$isupplier.' No:'.$ikuk;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/listtransferuangkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$teksquery="select distinct a.i_kuk
          from tm_kuk a
          left join tr_supplier b on(a.i_supplier=b.i_supplier)
          left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' 
          and a.i_supplier=c.i_supplier)
          where (upper(a.i_kuk) like '%$cari%' or upper(a.i_supplier) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery."
					and ((c.i_jenis_bayar!='02' and 
          c.i_jenis_bayar!='01' and 
          c.i_jenis_bayar!='04' and 
          C.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)) and 
          a.i_supplier='$isupplier' and
          (a.d_kuk >= to_date('$dfrom','dd-mm-yyyy') and
          a.d_kuk <= to_date('$dto','dd-mm-yyyy')) and a.f_kuk_cancel='f'";
			$query = $this->db->query($teksquery,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_kuk');
			$data['cari']				= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']				= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']				= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listtransferuangkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listtransferuangkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
#			echo $isupplier;
			$teksquery="select distinct a.i_kuk
          from tm_kuk a
          left join tr_supplier b on(a.i_supplier=b.i_supplier)
          left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' 
          and a.i_supplier=c.i_supplier)
          where (upper(a.i_kuk) like '%$cari%' or upper(a.i_supplier) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery."
					and ((c.i_jenis_bayar!='02' and 
          c.i_jenis_bayar!='01' and 
          c.i_jenis_bayar!='04' and 
          C.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)) and 
          a.i_supplier='$isupplier' and
          (a.d_kuk >= to_date('$dfrom','dd-mm-yyyy') and
          a.d_kuk <= to_date('$dto','dd-mm-yyyy')) and a.f_kuk_cancel='f'";
			$query = $this->db->query($teksquery,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_kuk');
			$this->load->model('listtransferuangkeluar/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->cariperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listtransferuangkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listtransferuangkeluar/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listtransferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listtransferuangkeluar/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu166')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listtransferuangkeluar/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listtransferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('listtransferuangkeluar/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
