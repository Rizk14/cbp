<?php
class Cform extends CI_Controller
{
	public $title   = "IKHP Keluar";
	public $folder  = "exp-ikhpkeluar";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
		$this->load->model($this->folder . '/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu276') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data = [
				'page_title' => $this->title,
				'folder'     => $this->folder,
				'iperiode'   => '',
			];

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu276') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iperiode	= $this->input->post('iperiode');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a 		= substr($iperiode, 0, 4);
			$b 		= substr($iperiode, 4, 2);
			$peri 	= mbulan($b) . " - " . $a;

			$this->db->select("	a.*, b.e_ikhp_typename, c.e_area_name
								from tm_ikhp a, tr_ikhp_type b, tr_area c
								where a.i_ikhp_type=b.i_ikhp_type and a.i_area=c.i_area
								and to_char(a.d_bukti,'yyyymm')='$iperiode' and
								not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0)
								order by a.i_area", false);
			$query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("IKHP Pengeluaran")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'IKHP Pengeluaran');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 10, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 10, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:H5'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Uraian');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Terima Tunai');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Terima Giro');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Keluar Tunai');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Keluar Giro');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':H' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row->e_area_name);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_bukti, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->e_ikhp_typename);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->v_terima_tunai);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->v_terima_giro);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->v_keluar_tunai);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->v_keluar_giro);

					$i++;
					$j++;
				}
				$x = $i - 1;
			}
			$objPHPExcel->getActiveSheet()->getStyle('E6:H6' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

			$nama = 'ikhpkeluar' . $iperiode . '.xls';
			$objWriter->save('excel/00/' . $nama);

			$this->logger->writenew('Export IKHP Pengeluaran periode:' . $iperiode);

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
