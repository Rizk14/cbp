<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspb').' Konsinyasi';
			$data['dfrom']='';
			$data['dto']='';
  		$data['iarea']='';
			$this->load->view('listspbkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('listspbkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			  $ispb	= $this->uri->segment(4);
			  $iarea= $this->uri->segment(5);
			  $dfrom= $this->uri->segment(6);
			  $dto	= $this->uri->segment(7);
			  $this->load->model('listspbkonsinyasi/mmaster');
			  $this->mmaster->delete($ispb,$iarea);
        $que=$this->db->query(" select f_spb_consigment from tm_spbkonsinyasi where i_spb='$ispb' and i_area='$iarea'");
        if($que->num_rows()>0){
          foreach ($que->result() as $row){
            $fspbkonsinyasi=$row->f_spb_consigment;
          }
          $this->mmaster->updatebon($ispb,$iarea);
        }

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Delete SPB No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

   		  $is_cari = $this->input->post('is_cari'); 
			  $cari	= strtoupper($this->input->post('cari'));
			  $config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
  			  $sql	= " select a.*, b.e_customer_name from tm_spbkonsinyasi a, tr_customer b
				where a.i_customer=b.i_customer
				and a.i_area='$iarea' and
				(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
				a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			  $query = $this->db->query($sql,false);

			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			
			  $this->load->model('listspbkonsinyasi/mmaster');
			  $data['page_title'] = $this->lang->line('listspb');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['iarea'] = $iarea;
			  $data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			  $this->load->view('listspbkonsinyasi/vmainform',$data);
 		}else{
  		  $this->load->view('awal/index.php');
	  }
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari=$this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); 
			if($dfrom=='') $dfrom = $this->uri->segment(4);
			if($dto=='') $dto = $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/index/';
			
						
			$sql= " select a.*, b.e_customer_name from tm_spbkonsinyasi a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
					and a.i_area='$iarea' and
						(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			
			$this->load->model('listspbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listspbkonsinyasi/vmainform',$data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function paging() 
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($this->uri->segment(5)!=''){
				if($this->uri->segment(4)!='sikasep'){
					$cari=$this->uri->segment(4);
				}else{
					$cari='';
				}
			}elseif($this->uri->segment(4)!='sikasep'){
				$cari=$this->uri->segment(4);
			}else{
				$cari='';
			}
			if($cari=='')
				$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/paging/'.$cari.'/';
			if($this->uri->segment(4)=='sikasep')
				$cari='';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if($this->session->userdata('level')=='0'){
				$sql= " select a.*, b.e_customer_name from tm_spbkonsinyasi a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			}else{
				$sql= " select a.*, b.e_customer_name from tm_spbkonsinyasi a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->model('listspbkonsinyasi/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspbkonsinyasi/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area where f_area_consigment='t' order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where f_area_consigment='t' and i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
      
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listspbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listspbkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where f_area_consigment='t' and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where f_area_consigment='t' and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listspbkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari'); 
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
				
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") {
				$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listspbkonsinyasi/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			
			if ($is_cari != "1") {
				$sql= " select a.i_spb
                from tm_spbkonsinyasi a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
								left join tr_customer b on(a.i_customer=b.i_customer)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and	a.f_spb_consigment='t' and
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
			}
			else {
				$sql= " select a.i_spb
                from tm_spbkonsinyasi a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f') 
								left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and	a.f_spb_consigment='t' and				
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')";
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$this->load->model('listspbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listspb').' Konsinyasi';
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listspbkonsinyasi/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu460')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('xcari'));
			$dfrom	= $this->input->post('xdfrom');
			$dto	= $this->input->post('xdto');
			$iarea	= $this->input->post('xiarea');
			$is_cari= $this->input->post('xis_cari'); 
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			$this->load->model('listspbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

      $this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                          a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                          a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                          a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                          b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                          from tm_spbkonsinyasi a 
									        left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
									        left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
									        left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
									        , tr_area c
                          where 
                            a.i_area=c.i_area and					
                            a.i_area='$iarea' and
                            (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_spb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_spb ",false);
      $query = $this->db->get();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Daftar SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

#####
      $this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SPB ")->setDescription("PT. Dialogue Garmindo Utama");
			$objPHPExcel->setActiveSheetIndex(0);
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SPB');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:J6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Lang');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Discount');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Bersih');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $i=7;
				$j=7;
        $no=0;
				foreach($query->result() as $row){
          $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':J'.$i
				  );
    
          if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			    	$status='Batal';
			    }elseif(
				   	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					   ){
			    	$status='Sales';
			    }elseif(
					    ($row->i_approve1 == null) && ($row->i_notapprove != null)
					   ){
			    	$status='Reject (sls)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					    ($row->i_notapprove == null)
					   ){
			    	$status='Keuangan';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					    ($row->i_notapprove != null)
					   ){
			    	$status='Reject (ar)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store == null)
					   ){
			    	$status='Gudang';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					   ){
			    	$status='Pemenuhan SPB';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					   ){
			    	$status='Proses OP';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					   ){
			    	$status='OP Close';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					   ){
			    	$status='Siap SJ (sales)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					   ){
  #			  	$status='Siap SJ (gudang)';
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					   ){
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap DKB';
          }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap Nota';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					   ){
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap DKB';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap Nota';
			    }elseif(
					    ($row->i_approve1 != null) && 
			    		  ($row->i_approve2 != null) &&
			     		  ($row->i_store != null) && 
					    ($row->i_nota != null) 
					   ){
			    	$status='Sudah dinotakan';			  
			    }elseif(($row->i_nota != null)){
			    	$status='Sudah dinotakan';
			    }else{
			    	$status='Unknown';		
			    }
          $bersih	= $row->v_spb-$row->v_spb_discounttotal;
#          $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
#			    $row->v_spb	= number_format($row->v_spb);
#   			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);  
          if($row->f_spb_stockdaerah=='t')
          {
            $daerah='Ya';
          }else{
            $daerah='Tidak';
          }

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->i_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->d_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->i_salesman);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '('.$row->i_customer.') '.$row->e_customer_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, "'".$row->i_area);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->v_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->v_spb_discounttotal);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $bersih);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $status);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $daerah);
					$i++;
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('F7:H'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SPB'.$iarea.'.xls';
      if(file_exists('spb/'.$iarea.'/'.$nama)){
        @chmod('spb/'.$iarea.'/'.$nama, 0777);
        @unlink('spb/'.$iarea.'/'.$nama);
      }
			$objWriter->save("spb/".$iarea.'/'.$nama); 
      @chmod('spb/'.$iarea.'/'.$nama, 0777);
      echo $nama;
#####
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
