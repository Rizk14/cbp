<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu378')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('pjna');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('pjna/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu378')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-daftarnota/mmaster');
			$iarea  	= $this->input->post('iarea');
			$iperiode	= $this->input->post('iperiode');
      $a=substr($iperiode,2,2);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;
      if($iarea=='NA'){
        $no='FP-'.$a.$b.'-%';
      }else{
        $no='FP-'.$a.$b.'-'.$iarea.'%';
      }
      $this->db->select(" 
						  a.i_nota,
						  a.d_nota,
						  a.i_customer,
						  b.e_customer_name,
						  a.i_salesman,
						  a.i_faktur_komersial,
						  a.i_seri_pajak,
						  a.d_pajak,
						  a.v_nota_gross,
						  a.v_nota_discounttotal,
						  a.v_nota_ppn,
						  a.v_nota_gross,
						  a.v_nota_netto,
						  a.f_pajak_pengganti,
						  a.i_area,
						  a.d_pajak_print,
						  n_pajak_print,
						  b.f_customer_pkp,
						  d.e_customer_pkpname,
						  d.e_customer_pkpaddress,
						  d.e_customer_pkpnpwp,
						  c.f_spb_pkp,
						  (n_tax / 100 + 1) excl_divider, 
						  n_tax / 100 n_tax_val
					  from
					  tm_nota a
					  inner join tm_spb c on 
						  (a.i_spb = c.i_spb and a.i_area = c.i_area)
					  inner join tr_customer b on 
						  (a.i_customer = b.i_customer)
					  left join tr_customer_pkp d on
						  (b.i_customer = d.i_customer)
					  left join tr_tax_amount i on
						  a.d_nota between d_start and d_finish
					  where
						  a.i_nota like '$no'
						  and not a.i_faktur_komersial isnull
						  and not a.i_seri_pajak isnull
						  and a.f_nota_cancel = 'f'
					  order by
						  a.i_area,
						  a.d_nota,
						  a.i_nota",false);
#and a.n_faktur_komersialprint>0 
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("PJNA".$iperiode)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(14);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:V1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODETRAN,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NODOK,C,7');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOFAKTUR,C,6');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'PENGGANTI,l');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NOSERI,C,6');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TGLDOK,D');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TGLPJK,D');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'WILA,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'RAYON,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NOLANG,C,3');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'KODESALES,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'GROSS,N,11,0');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'POTONG,N,11,0');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'DPP,N,11,0');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'PPN,N,11,0');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'NET,N,11,0');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'TGLCETAK,D');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'JUMCETAK,N,6,0');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'TGLBUAT,D');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('T1', 'JAMBUAT,C,8');
				$objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('U1', 'TGLUBAH,D');
				$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('V1', 'JAMUBAH,C,8');
				$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=2;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':V'.$i
				  );
          
          if($row->d_nota!=''){
            $tmp=explode('-',$row->d_nota);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_nota=$hr.'-'.$bl.'-'.$th;
          }
          if($row->d_pajak!=''){
            $tmp=explode('-',$row->d_pajak);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_pajak=$hr.'-'.$bl.'-'.$th;
          }

		  /* $row->v_nota_ppn=floor($row->v_nota_netto/$row->excl_divider*$row->n_tax_val); */
          $row->v_nota_dpp=round($row->v_nota_netto/$row->excl_divider);
          $row->v_nota_ppn=round(($row->v_nota_netto/$row->excl_divider)*$row->n_tax_val);
          if($row->f_customer_pkp){
            $kodetran='04';
          }else{
            $kodetran='07';
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $kodetran, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_faktur_komersial, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->f_pajak_pengganti, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->d_pajak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, null, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, substr($row->i_customer,2,3), Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->v_nota_discounttotal, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_nota_dpp, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->d_pajak_print, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->n_pajak_print, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, null, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, null, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, null, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, null, Cell_DataType::TYPE_STRING);
					$i++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='PJNA '.$iperiode.'.xls';
      if(file_exists('pajak/'.$nama)){
        @chmod('pajak/'.$nama, 0777);
        @unlink('pajak/'.$nama);
      }
			$objWriter->save('pajak/'.$nama); 
      @chmod('pajak/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export PJNA Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu378')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/pjna/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('pjna/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('pjna/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu378')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/pjna/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('pjna/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('pjna/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
