<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu234')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transsj');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('transsj/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu234')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transsj');
			$data['iperiode']	= $iperiode;
      $tahun=substr($iperiode,0,4);      
			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
      $db_file = 'spb/'.$iarea.'/fj'.$iarea.$tahun.'.dbf';
      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  6),
         array ('TGLDOK',  DATE_FIELD),
         array ('NOSPB',  CHARACTER_FIELD,  6),
         array ('TGLSPB',  DATE_FIELD),
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('KODESALES',  CHARACTER_FIELD,  2),
         array ('KOTOR',  NUMBER_FIELD,  10,0),
         array ('POTONGAN', NUMBER_FIELD, 10, 0),     
         array ('DISCOUNT', NUMBER_FIELD, 7, 2),
         array ('DISC2',  NUMBER_FIELD,  4, 1),  
         array ('DISC3',  NUMBER_FIELD,  4, 1),  
         array ('DISC4',  NUMBER_FIELD,  4, 1),  
         array ('BATAL', BOOLEAN_FIELD),
         array ('TGLBUAT', CHARACTER_FIELD,  20),
         array ('TGLUBAH', CHARACTER_FIELD,  20)
      );
      /* $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>.");  */
      $per=substr($iperiode,2,4);
      $dicari='SJ-'.$per.'-'.$iarea.'%';
      $sql	  = " a.* from tm_nota a, tm_spb b 
                  where a.i_spb=b.i_spb and a.i_area=b.i_area and b.f_spb_consigment='f'
                  and a.i_sj like '$dicari'";
      $this->db->select($sql,false);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '03';
	        $nodok            = substr($rowsj->i_sj,8,6);
	        $tgldok           = substr($rowsj->d_sj,0,4).substr($rowsj->d_sj,5,2).substr($rowsj->d_sj,8,2);
	        $nospb            = substr($rowsj->i_spb,9,6);
	        $tglspb           = substr($rowsj->d_spb,0,4).substr($rowsj->d_spb,5,2).substr($rowsj->d_spb,8,2);
	        $kodelang         = $rowsj->i_customer;
	        $kodesales        = $rowsj->i_salesman;
          $kotor            = $rowsj->v_nota_gross;
          $potongan         = $rowsj->v_nota_discounttotal;
	        $discount         = $rowsj->n_nota_discount1;
	        $disc2            = $rowsj->n_nota_discount2;
	        $disc3            = $rowsj->n_nota_discount3;
	        $disc4            = $rowsj->n_nota_discount4;
          if($rowsj->f_nota_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sj_entry;
          $tglubah          = $rowsj->d_sj_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nospb,$tglspb,$kodelang,$kodesales,$kotor,$potongan,
                        $discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }
      $dicari='SJ-'.$per.'-BK%';
      $sql	  = " a.* from tm_nota a, tm_spb b 
                  where a.i_spb=b.i_spb and a.i_area=b.i_area and b.f_spb_consigment='t'
                  and a.i_sj like '$dicari'";
      $this->db->select($sql,false);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '13';
	        $nodok            = substr($rowsj->i_sj,8,6);
	        $tgldok           = substr($rowsj->d_sj,0,4).substr($rowsj->d_sj,5,2).substr($rowsj->d_sj,8,2);
	        $nospb            = substr($rowsj->i_spb,9,6);
	        $tglspb           = substr($rowsj->d_spb,0,4).substr($rowsj->d_spb,5,2).substr($rowsj->d_spb,8,2);
	        $kodelang         = $rowsj->i_customer;
	        $kodesales        = $rowsj->i_salesman;
          $kotor            = $rowsj->v_nota_gross;
          $potongan         = $rowsj->v_nota_discounttotal;
	        $discount         = $rowsj->n_nota_discount1;
	        $disc2            = $rowsj->n_nota_discount2;
	        $disc3            = $rowsj->n_nota_discount3;
	        $disc4            = $rowsj->n_nota_discount4;
          if($rowsj->f_nota_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sj_entry;
          $tglubah          = $rowsj->d_sj_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nospb,$tglspb,$kodelang,$kodesales,$kotor,$potongan,
                        $discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
      }
#      $dicari='SJP-'.$per.'-'.$iarea.'%';
      $dicari='SJP-%-'.$iarea.'%';
#      $dica='SJP-'.$per.'-%';
      $dica='SJP-%-%';
      if($iarea=='00')
      {
         $sql = " select a.* from tm_SJP a
                  left join tm_spmb b on b.i_spmb = a.i_spmb and b.i_area = a.i_area
                  where ( b.f_spmb_consigment='f' or b.f_spmb_consigment isnull )
                  and to_char(a.d_sjp_receive,'yymm')='$per' and not a.d_sjp_receive isnull ";
#and a.i_sjp like '$dica' and not a.d_sjp_receive isnull ";
      }
      else
      {
         $sql = " select a.* from tm_SJP a
                  left join tm_spmb b on b.i_spmb = a.i_spmb and b.i_area = a.i_area
                  where ( b.f_spmb_consigment='f' or b.f_spmb_consigment isnull )
                  and to_char(d_sjp_receive,'yymm')='$per' and a.i_area='$iarea' and not a.d_sjp_receive isnull ";

#                 and a.i_sjp like '$dicari' and a.i_area='$iarea' and not a.d_sjp_receive isnull ";
      }
      $query=$this->db->query($sql,false);
#      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '01';
	        $nodok            = substr($rowsj->i_sjp,9,6);
	        $tgldok           = substr($rowsj->d_sjp_receive,0,4).substr($rowsj->d_sjp_receive,5,2).substr($rowsj->d_sjp_receive,8,2);
	        $nospmb           = substr($rowsj->i_spmb,10,6);
	        $tglspmb          = substr($rowsj->d_spmb,0,4).substr($rowsj->d_spmb,5,2).substr($rowsj->d_spmb,8,2);
	        $kodelang         = '';
	        $kodesales        = '';
          $kotor            = $rowsj->v_sjp;
          $potongan         = 0;
	        $discount         = 0;
	        $disc2            = 0;
	        $disc3            = 0;
	        $disc4            = 0;
          if($rowsj->f_sjp_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sjp_entry;
          $tglubah          = $rowsj->d_sjp_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nospmb,$tglspmb,$kodelang,$kodesales,$kotor,$potongan,
                        $discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }
      if($iarea=='00'){
        $sql	  = " select a.* from tm_SJP a, tm_spmb b
                    where a.i_spmb=b.i_spmb and a.i_area=b.i_area and b.f_spmb_consigment='t'
                    and to_char(a.d_sjp_receive,'yymm')='$per' and not a.d_sjp_receive isnull ";

#                    and a.i_sjp like '$dica' and not a.d_sjp_receive isnull ";
      }else{
        $sql	  = " select a.* from tm_SJP a, tm_spmb b 
                    where a.i_spmb=b.i_spmb and a.i_area=b.i_area and b.f_spmb_consigment='t'
                    and to_char(d_sjp_receive,'yymm')='$per' and a.i_area='$iarea' and not a.d_sjp_receive isnull ";

#                    and a.i_sjp like '$dicari' and a.i_area='$iarea' and not a.d_sjp_receive isnull ";
      }
      $query=$this->db->query($sql,false);
#      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '11';
	        $nodok            = substr($rowsj->i_sjp,9,6);
	        $tgldok           = substr($rowsj->d_sjp_receive,0,4).substr($rowsj->d_sjp_receive,5,2).substr($rowsj->d_sjp_receive,8,2);
	        $nospmb           = substr($rowsj->i_spmb,10,6);
	        $tglspmb          = substr($rowsj->d_spmb,0,4).substr($rowsj->d_spmb,5,2).substr($rowsj->d_spmb,8,2);
	        $kodelang         = '';
	        $kodesales        = '';
          $kotor            = $rowsj->v_sjp;
          $potongan         = 0;
	        $discount         = 0;
	        $disc2            = 0;
	        $disc3            = 0;
	        $disc4            = 0;
          if($rowsj->f_sjp_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sjp_entry;
          $tglubah          = $rowsj->d_sjp_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nospmb,$tglspmb,$kodelang,$kodesales,$kotor,$potongan,
                        $discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
      }
      $dicari='SJR-'.$per.'-'.$iarea.'%';
      if($iarea=='00'){
        $sql	  = " * from tm_SJR where i_sjr like '$dicari' and i_area='$iarea'";
      }else{
        $sql	  = " * from tm_SJR where i_sjr like '$dicari' and i_area='$iarea'";
      }
      $this->db->select($sql);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '04';
	        $nodok            = substr($rowsj->i_sjr,9,6);
	        $tgldok           = substr($rowsj->d_sjr,0,4).substr($rowsj->d_sjr,5,2).substr($rowsj->d_sjr,8,2);
	        $nospmb           = '';
	        $tglspmb          = '';
	        $kodelang         = '';
	        $kodesales        = '';
          $kotor            = $rowsj->v_sjr;
          $potongan         = 0;
	        $discount         = 0;
	        $disc2            = 0;
	        $disc3            = 0;
	        $disc4            = 0;
          if($rowsj->f_sjr_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sjr_entry;
          $tglubah          = $rowsj->d_sjr_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nospb,$tglspb,$kodelang,$kodesales,$kotor,$potongan,
                        $discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }
      /* dbase_close($id); */
      $db_file = 'spb/'.$iarea.'/sj'.$iarea.$tahun.'.dbf';
      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  6),
         array ('TGLDOK',  DATE_FIELD),
         array ('KODEPROD', CHARACTER_FIELD,  9),
         array ('JUMLAH',  NUMBER_FIELD,  6,0),
         array ('HARGA', NUMBER_FIELD, 8, 0),
         array ('TGLBUAT', CHARACTER_FIELD,  20),
         array ('TGLUBAH', CHARACTER_FIELD,  20)
      );
      /* $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>.");  */
      $dicari='SJ-'.$per.'-'.$iarea.'%';
      $sql	  = " a.*, b.d_sj, b.d_sj_entry, b.d_sj_update from tm_nota_item a, tm_nota b, tm_spb c 
                  where c.i_spb=b.i_spb and c.i_area=b.i_area and c.f_spb_consigment='f' 
                  and a.i_sj like '$dicari' and a.i_sj=b.i_sj and a.i_area=b.i_area";
      $this->db->select($sql,false);

      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '03';
	        $nodok            = substr($rowsj->i_sj,8,6);
	        $tgldok           = substr($rowsj->d_sj,0,4).substr($rowsj->d_sj,5,2).substr($rowsj->d_sj,8,2);
	        $kodeprod         = $rowsj->i_product.'00';
          $jumlah           = $rowsj->n_deliver;
          $harga            = $rowsj->v_unit_price;
          $tglbuat          = $rowsj->d_sj_entry;
          $tglubah          = $rowsj->d_sj_update;
          $isi = array ($kodetran,$nodok,$tgldok,$kodeprod,$jumlah,$harga,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }
      $sql	  = " a.*, b.d_sj, b.d_sj_entry, b.d_sj_update from tm_nota_item a, tm_nota b, tm_spb c 
                  where c.i_spb=b.i_spb and c.i_area=b.i_area and c.f_spb_consigment='t' 
                  and a.i_sj like '$dicari' and a.i_sj=b.i_sj and a.i_area=b.i_area";
      $this->db->select($sql,false);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '13';
	        $nodok            = substr($rowsj->i_sj,8,6);
	        $tgldok           = substr($rowsj->d_sj,0,4).substr($rowsj->d_sj,5,2).substr($rowsj->d_sj,8,2);
	        $kodeprod         = $rowsj->i_product.'00';
          $jumlah           = $rowsj->n_deliver;
          $harga            = $rowsj->v_unit_price;
          $tglbuat          = $rowsj->d_sj_entry;
          $tglubah          = $rowsj->d_sj_update;
          $isi = array ($kodetran,$nodok,$tgldok,$kodeprod,$jumlah,$harga,$tglbuat,$tglubah);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
      }
#      $dicari='SJP-'.$per.'-'.$iarea.'%';
      $dicari='SJP-%-'.$iarea.'%';
#      $dica='SJP-'.$per.'-%';
      $dica='SJP-%-%';
      if($iarea=='00')
      {
         $sql = " select a.*, b.d_sjp, b.d_sjp_entry, b.d_sjp_receive, b.d_sjp_update 
                  from tm_sjp_item a
                  inner join tm_sjp b on b.i_sjp=a.i_sjp and a.i_area=b.i_area 
                  left join tm_spmb c on c.i_spmb = b.i_spmb and b.i_area=c.i_area
                  where (c.f_spmb_consigment='f' or c.f_spmb_consigment isnull) 
                  and to_char(b.d_sjp_receive,'yymm')='$per' and not b.d_sjp_receive isnull ";

#                  and a.i_sjp like '$dica' and not b.d_sjp_receive isnull ";
      }
      else
      {
         $sql = " select a.*, b.d_sjp, b.d_sjp_entry, b.d_sjp_receive, b.d_sjp_update 
                  from tm_sjp_item a
                  inner join tm_sjp b on b.i_sjp=a.i_sjp and a.i_area=b.i_area 
                  left join tm_spmb c on c.i_spmb = b.i_spmb and b.i_area=c.i_area
                  where (c.f_spmb_consigment='f' or c.f_spmb_consigment isnull) 
                  and to_char(b.d_sjp_receive,'yymm')='$per' and b.i_area='$iarea' and not b.d_sjp_receive isnull ";

#and a.i_sjp like '$dicari' and not b.d_sjp_receive isnull ";
      }
      $query=$this->db->query($sql,false);
#      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '01';
	        $nodok            = substr($rowsj->i_sjp,9,6);
	        $tgldok           = substr($rowsj->d_sjp_receive,0,4).substr($rowsj->d_sjp_receive,5,2).substr($rowsj->d_sjp_receive,8,2);
	        $kodeprod         = $rowsj->i_product.'00';
          $jumlah           = $rowsj->n_quantity_receive;
          $harga            = $rowsj->v_unit_price;
          $tglbuat          = $rowsj->d_sjp_entry;
          $tglubah          = $rowsj->d_sjp_update;
          $isi = array ($kodetran,$nodok,$tgldok,$kodeprod,$jumlah,$harga,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }
      if($iarea=='00'){
        $sql	  = " select a.*, b.d_sjp, b.d_sjp_entry, b.d_sjp_receive, b.d_sjp_update 
                    from tm_sjp_item a, tm_sjp b, tm_spmb c
                    where b.i_spmb=c.i_spmb and b.i_area=c.i_area and c.f_spmb_consigment='t'
                    and to_char(b.d_sjp_receive,'yymm')='$per' and a.i_sjp=b.i_sjp and a.i_area=b.i_area and not b.d_sjp_receive isnull ";

#                    and a.i_sjp like '$dica' and a.i_sjp=b.i_sjp and a.i_area=b.i_area and not b.d_sjp_receive isnull ";
      }else{
        $sql	  = " select a.*, b.d_sjp, b.d_sjp_entry, b.d_sjp_receive, b.d_sjp_update 
                    from tm_sjp_item a, tm_sjp b, tm_spmb c
                    where b.i_spmb=c.i_spmb and b.i_area=c.i_area and c.f_spmb_consigment='t'
                    and to_char(b.d_sjp_receive,'yymm')='$per' and a.i_sjp=b.i_sjp and b.i_area='$iarea' 
                    and a.i_area=b.i_area and not b.d_sjp_receive isnull ";

#                   and a.i_sjp like '$dicari' and a.i_sjp=b.i_sjp and a.i_area=b.i_area and not b.d_sjp_receive isnull ";
      }
      $query=$this->db->query($sql,false);
#      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '11';
	        $nodok            = substr($rowsj->i_sjp,9,6);
	        $tgldok           = substr($rowsj->d_sjp_receive,0,4).substr($rowsj->d_sjp_receive,5,2).substr($rowsj->d_sjp_receive,8,2);
	        $kodeprod         = $rowsj->i_product.'00';
          $jumlah           = $rowsj->n_quantity_receive;
          $harga            = $rowsj->v_unit_price;
          $tglbuat          = $rowsj->d_sjp_entry;
          $tglubah          = $rowsj->d_sjp_update;
          $isi = array ($kodetran,$nodok,$tgldok,$kodeprod,$jumlah,$harga,$tglbuat,$tglubah);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
      }
      $dicari='SJR-'.$per.'-'.$iarea.'%';
      $sql	  = " a.*, b.d_sjr, b.d_sjr_entry, b.d_sjr_update from tm_sjr_item a, tm_sjr b 
                  where a.i_sjr like '$dicari' and a.i_sjr=b.i_sjr and a.i_area=b.i_area";
      $this->db->select($sql,false);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '04';
	        $nodok            = substr($rowsj->i_sjr,9,6);
	        $tgldok           = substr($rowsj->d_sjr,0,4).substr($rowsj->d_sjr,5,2).substr($rowsj->d_sjr,8,2);
	        $kodeprod         = $rowsj->i_product.'00';
          $jumlah           = $rowsj->n_quantity_retur;
          $harga            = $rowsj->v_unit_price;
          $tglbuat          = $rowsj->d_sjr_entry;
          $tglubah          = $rowsj->d_sjr_update;
          $isi = array ($kodetran,$nodok,$tgldok,$kodeprod,$jumlah,$harga,$tglbuat,$tglubah);
          /* dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>.");  */
        }
      }

      /* dbase_close($id); */
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke SJ lama Area '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu234')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transsj/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select distinct on (a.i_store) a.i_store as i_area, b.e_store_name as e_area_name
                                  from tr_area a, tr_store b 
                                  where a.i_store=b.i_store 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser')
                                  group by a.i_store, b.e_store_name order by a.i_store", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transsj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transsj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu234')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transsj/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query = $this->db->query("select distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                                   from tr_area a, tr_store b
                                   where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                                   and (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transsj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transsj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
