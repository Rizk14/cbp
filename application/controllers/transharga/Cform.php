<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu240')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transharga');
			$data['isi']= directory_map('./beli/');
			$data['file']='';
			$this->load->view('transharga/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu240')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$data['file']='beli/'.$file;
#			$data['periode']=$this->input->post('periode', TRUE);
	#		$data['baru']=$this->input->post('baru', TRUE);
			$this->load->view('transharga/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu240')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
      $this->load->model('transharga/mmaster');
      $this->db->trans_begin();
      for($i=1;$i<=$jml;$i++){
        $kodeprod=$this->input->post('kdprod'.$i,TRUE);
        $sql	= "select e_product_name, v_product_mill from tr_product where i_product='$kodeprod'";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$nama	  = $row['e_product_name'];
            $harga  = $row['v_product_mill'];
            $margin = null;
					}
				}
        $h00=$this->input->post('h00'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '00', $nama, $h00, $harga, $margin);
        $h01=$this->input->post('h01'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '01', $nama, $h01, $harga, $margin);
        $h02=$this->input->post('h02'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '02', $nama, $h02, $harga, $margin);
        $h03=$this->input->post('h03'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '03', $nama, $h03, $harga, $margin);
        $h04=$this->input->post('h04'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '04', $nama, $h04, $harga, $margin);
        $h05=$this->input->post('h05'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', '05', $nama, $h05, $harga, $margin);
        $hg0=$this->input->post('hg0'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', 'G0', $nama, $hg0, $harga, $margin);
        $hg2=$this->input->post('hg2'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', 'G2', $nama, $hg2, $harga, $margin);
        $hg3=$this->input->post('hg3'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', 'G3', $nama, $hg3, $harga, $margin);
        $hg5=$this->input->post('hg5'.$i);
        $this->mmaster->insertdetail($kodeprod, 'A', 'G5', $nama, $hg5, $harga, $margin);
      }
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transharga/vformgagal',$data);
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Transfer Perubahan Harga';
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('transharga/vformsukses');
			}			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
