<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listsjpbedaterima/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			//$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			//if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbedaterima/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select distinct(d.i_sjp), a.d_sjp, f_spmb_consigment, a.i_area, c.e_area_name, a.v_sjp, a.v_sjp_receive, a.d_sjp_receive
                                    from tm_sjp a, tm_spmb b, tr_area c, tm_sjp_item d
                                    where a.i_spmb=b.i_spmb and a.i_area=c.i_area and b.i_area=c.i_area
                                    and a.i_sjp=d.i_sjp and d.n_quantity_deliver<>d.n_quantity_receive
                                    and not a.d_sjp_receive is null and a.f_sjp_cancel='f'
                                    and (upper(a.i_sjp) like '%$cari%' or upper(a.i_spmb) like '%$cari%')
                                    and (a.d_sjp >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjp <= to_date('$dto','dd-mm-yyyy'))
                                    order by a.i_area, d.i_sjp",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			$this->load->model('listsjpbedaterima/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			//$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJP Beda Terima Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listsjpbedaterima/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}	
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('xcari'));
			$dfrom	= $this->input->post('xdfrom');
			$dto	= $this->input->post('xdto');
			//$iarea	= $this->input->post('xiarea');
			$is_cari= $this->input->post('xis_cari'); 
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			//if($iarea=='') $iarea=$this->uri->segment(6);
			if ($is_cari == '')
				$is_cari= $this->uri->segment(7);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(6);
			$this->load->model('listsjpbedaterima/mmaster');
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			//$data['iarea'] = $iarea;

      $this->db->select("	d.i_sjp, a.d_sjp, f_spmb_consigment, a.i_area, c.e_area_name, a.v_sjp, a.v_sjp_receive, a.d_sjp_receive,
                          d.i_product, d.e_product_name, d.n_quantity_deliver, d.n_quantity_receive, d.v_unit_price, a.i_spmb
                          from tm_sjp a, tm_spmb b, tr_area c, tm_sjp_item d
                          where a.i_spmb=b.i_spmb and a.i_area=c.i_area and b.i_area=c.i_area
                          and a.i_sjp=d.i_sjp and d.n_quantity_deliver<>d.n_quantity_receive
                          and not a.d_sjp_receive is null and a.f_sjp_cancel='f'
                          and (a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_area, d.i_sjp ",false);
      $query = $this->db->get();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Daftar SJP Deliver vs Receive Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
      $this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SJP Deliver vs Receive")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SJP Deliver vs Receive');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Dari Tanggal '.$dfrom.' Sampai Tanggal '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,10,3);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:K6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No SJP');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl SJP');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Terima');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'SPMB');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Konsinyasi');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Jml Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Jml Terima');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $i=7;
				$j=7;
        $no=0;
				foreach($query->result() as $row){
          $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':K'.$i
				  );

			    $tmp=explode('-',$row->d_sjp);
			    $tgl=$tmp[2];
			    $bln=$tmp[1];
			    $thn=$tmp[0];
			    $row->d_sjp=$tgl.'-'.$bln.'-'.$thn;

         if($row->d_sjp_receive!=''){
            $tm	= explode('-',$row->d_sjp_receive);
			      $tgl	= $tm[2];
			      $bln	= $tm[1];
			      $thn	= $tm[0];
			      $row->d_sjp_receive=$tgl.'-'.$bln.'-'.$thn;
          }

          if($row->f_spmb_consigment == 't'){
            $kons='ya';
          }else{
            $kons='tidak';
          }

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, '('.$row->i_area.') '.$row->e_area_name);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_sjp);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_sjp);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->d_sjp_receive);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->i_spmb);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $kons);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_quantity_deliver);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_quantity_receive);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->v_unit_price);
					$i++;
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('K7:K'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SJP-Del-vs-Rec.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        @unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			$this->load->view('listsjpbedaterima/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			if($this->uri->segment(4)!=''){
				$isjp	= $this->uri->segment(4);
				$dfrom= $this->uri->segment(5);
				$dto 	= $this->uri->segment(6);
				$data['isjp'] = $isjp;
				$data['dfrom']= $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjp_item where i_sjp = '$isjp'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('listsjpbedaterima/mmaster');
				$data['isi']=$this->mmaster->baca($isjp);
				$data['detail']=$this->mmaster->bacadetail($isjp);
		 		$this->load->view('listsjpbedaterima/vformdetail',$data);
			}else{
				$this->load->view('listsjpbedaterima/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			//$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			//if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbedaterima/cform/view/'.$dfrom.'/'.$dto.'/index/';
	    $query = $this->db->query(" select a.i_sj, a.d_sj, a.i_customer, c.e_customer_name, a.i_area, a.i_spb, a.i_dkb, a.f_nota_cancel, a.i_sj_old,
                          a.d_dkb, a.i_bapb, a.d_bapb, d.n_bal, a.i_spb, a.d_spb, a.i_nota
                          from tm_nota a, tr_area b, tr_customer c, tm_bapb d
		                      where a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_bapb=d.i_bapb and
                          (upper(a.i_sj) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_dkb) like '%$cari%')
			                    and substr(a.i_sj,9,2) = '00' and
			                    a.i_area='$iarea' and 
			                    (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_sj asc, a.d_sj desc ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbedaterima');
			$this->load->model('listsjpbedaterima/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			//$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->cariperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('listsjpbedaterima/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpbedaterima/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpbedaterima/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpbedaterima/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu383')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listsjpbedaterima/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpbedaterima/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpbedaterima/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
