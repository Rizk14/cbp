<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu71')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('mutasipiutang');
			$data['iperiode']='';
      		$this->load->view('mutasipiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu71')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);

			$this->load->model('mutasipiutang/mmaster');
  			$data['page_title'] = $this->lang->line('mutasipiutang');
			$data['iperiode']	= $iperiode;
      		$data['isi']		= $this->mmaster->bacaperiode($iperiode);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Mutasi Piutang Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('mutasipiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu71')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiodefrom	= $this->input->post('iperiodefrom');
			$iperiodeto	= $this->input->post('iperiodeto');
			//if($iperiode=='') $iperiode	= $this->uri->segment(4);

			$this->load->model('mutasipiutang/mmaster');
  			$data['page_title'] = $this->lang->line('mutasipiutang');
			$data['iperiodefrom']	= $iperiodefrom;
			$data['iperiodeto']	= $iperiodeto;
      		$lihatdata['hitungdulu']	= $this->mmaster->hitung($iperiodefrom);
      		$i = 0;
      		$iarea = array();
      		$saldoakhir = array();

      		$cekperiode = $this->mmaster->lihatperiode($iperiodeto);
      		//$cekperiode = "select count(e_mutasi_periode) from tm_mutasi_piutang where e_mutasi_periode = '$iperiodeto'";
      		//$ck 		= $this->db->query($cekperiode);
      		//$totck		= $ck->num_rows();
      		//print_r($cekperiode);die();
      		foreach ($lihatdata['hitungdulu'] as $key) {
      				for ($i = 0; $i < count($key); $i++){
      					$iarea[$i] = $key->i_area;	
      					$saldoakhir[$i] = $key->saldoakhir;
      					if($cekperiode == 'true'){
							//var_dump("update"); die();
      						$this->mmaster->updatemutasi($iarea[$i],$saldoakhir[$i],$iperiodeto);	
      					}else{
							//var_dump("insert"); die();
							$this->mmaster->insertmutasi($iarea[$i],$saldoakhir[$i],$iperiodeto);	
      					}
      				}
      		}
      		$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Transfer Mutasi Piutang Periode: '.$iperiode.' ke periode: '.$iperiodeto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses']         = true;
            $data['inomor']         = 'Transfer mutasi periode'.$iperiodeto.'telah berhasil !';
            $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu71')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('mutasipiutang');
			$this->load->view('mutasipiutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu71')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomer= $this->input->post('icustomer');
      $iperiode	= $this->input->post('iperiode');
			if($icustomer=='') $icustomer	= $this->uri->segment(4);
			if($iperiode=='') $iperiode	= $this->uri->segment(6);
			$saldo	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/mutasipiutang/cform/detail/'.$icustomer.'/'.$saldo.'/'.$iperiode.'/';
      $query = $this->db->query(" select customer from(
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_nota as tglbukti, a.i_nota as bukti, '' as jenis, '1. penjualan' as keterangan, a.v_nota_netto as debet, 
                                  0 as kredit, 0 as dn, 0 as kn
                                  from tm_nota a, tr_customer_groupar b, tr_customer c
                                  where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='$iperiode'
                                  and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'kredit nota' as keterangan, 0 as debet, 0 as kredit, 
                                  0 as dn, 
                                  v_netto as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                                  where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_bukti as tglbukti, a.i_pelunasan as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 
                                  0 as debet, a.v_jumlah as kredit, 0 as dn, 0 as kn
                                  from tm_pelunasan a, tr_customer_groupar b, tr_customer c, tr_jenis_bayar d
                                  where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' 
                                  and to_char(a.d_bukti,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar 
                                  and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                                  a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'debet nota' as keterangan, 0 as debet, 0 as kredit, v_netto as dn, 
                                  0 as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                                  where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  ) as a
                                  ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('mutasipiutang/mmaster');
  		$data['page_title'] = $this->lang->line('mutasipiutang');
			$data['icustomer']	= $icustomer;
			$data['iperiode']	= $iperiode;
			$data['saldo']	= $saldo;
			$data['isi']		= $this->mmaster->bacadetail($icustomer,$iperiode,$config['per_page'],$this->uri->segment(7));
			$this->load->view('mutasipiutang/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
