<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = "Sales Performance By Monthly";
			$data['dfrom']	= '';
	        $data['dto']  ='';
	        $this->load->model('ctrbymonthly_new/mmaster');
	        $data['iproductgroup'] = $this->mmaster->bacaproductgroup();
			$this->load->view('ctrbymonthly_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

        $dfrom  = $this->input->post('dfrom');
        $dto    = $this->input->post('dto');
        $group  = $this->input->post('iproductgroup');
        $this->load->model('ctrbymonthly_new/mmaster');
        $data['page_title'] = "Sales Performance By Monthly";
        $data['dfrom']	= $dfrom;
        $data['dto']    = $dto;
        $data['group']  = $group;

        $tsasih = date('Y-m-d', strtotime('-24 month', strtotime($dto))); //tambah tanggal sebanyak 2 tahun
        $dtos = date('Y-m-d',strtotime($dto));
          
        $ob  = 0;
        $jan = 0;
        $feb = 0;
        $mar = 0;
        $apr = 0;
        $mei = 0;
        $jun = 0;
        $jul = 0;
        $ags = 0;
        $sep = 0;
        $okt = 0;
        $nov = 0;
        $des = 0;
      
      $query = $this->db->query ("  SELECT DISTINCT(a.i_customer) AS i_customer 
                                    FROM tm_nota a, tr_customer b
                                    WHERE 
                                    a.i_customer = b.i_customer
                                    AND b.f_customer_aktif = 't' 
                                    AND b.i_customer_status <> '4'
                                    AND d_nota >='$tsasih' AND d_nota <='$dtos' AND f_nota_cancel='f' ");

      foreach ($query->result() as $riw) {
        $customer = $riw->i_customer;

        $query2 = $this->db->query (" SELECT to_char(d_nota,'mm') AS periode
                                      FROM tm_nota a
                                      WHERE f_nota_cancel = 'f' AND NOT a.i_nota ISNULL AND i_customer = '$customer'
                                      GROUP BY periode, a.i_nota
                                      ORDER BY a.i_nota
                                      LIMIT 1 ");

        foreach ($query2->result() as $rew) {
          $periode = $rew->periode;

          if($periode=='01'){
            $jan++;
          }elseif($periode=='02'){
            $feb++;
          }elseif($periode=='03'){
            $mar++;
          }elseif($periode=='04'){
            $apr++;
          }elseif($periode=='05'){
            $mei++;
          }elseif($periode=='06'){
            $jun++;
          }elseif($periode=='07'){
            $jul++;
          }elseif($periode=='08'){
            $ags++;
          }elseif($periode=='09'){
            $sep++;
          }elseif($periode=='10'){
            $okt++;
          }elseif($periode=='11'){
            $nov++;
          }elseif($periode=='12'){
            $des++;
          }

          $ob++;
        }
      }
      
      // echo $jan .'<br>'; 
      // echo $feb .'<br>'; 
      // echo $mar .'<br>'; 
      // echo $apr .'<br>'; 
      // echo $mei .'<br>'; 
      // echo $jun .'<br>'; 
      // echo $jul .'<br>'; 
      // echo $ags .'<br>'; 
      // echo $sep .'<br>'; 
      // echo $okt .'<br>'; 
      // echo $nov .'<br>'; 
      // echo $des .'<br>'; 
      // die();

      $data['ob']     = $this->mmaster->bacaob($dfrom,$dto,$group);
      $data['obb']    = array(
                            "01"=>$jan.' '.$periode,
                            "02"=>$feb.' '.$periode,
                            "03"=>$mar.' '.$periode,
                            "04"=>$apr.' '.$periode,
                            "05"=>$mei.' '.$periode,
                            "06"=>$jun.' '.$periode,
                            "07"=>$jul.' '.$periode,
                            "08"=>$ags.' '.$periode,
                            "09"=>$sep.' '.$periode,
                            "10"=>$okt.' '.$periode,
                            "11"=>$nov.' '.$periode,
                            "12"=>$des.' '.$periode
                        );
      
      $data['isi']	  = $this->mmaster->baca($dfrom,$dto,$group);

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Ctr by Monthly :'.$dfrom."-".$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('ctrbymonthly_new/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

  function chartoa()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }

      if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            }

      $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSLine.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "Trend OA ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "Trend OA ".$group." YTD".$th ;
      $graph_width        = 1000;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = $th ;
      $dataset[1] = $thprev;
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      $grwoa=0;
      foreach($result as $row){
       
        $oa=0;
        $oa=$oa+$row->oa;
        $arrData[$th][$i] = intval($oa);
        $i++;
      }

      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      $grwqty=0;
      foreach($result as $row){
              $oaprev=0;
              $oaprev=$oaprev+$row->oaprev;
        $arrData[$thprev][$i] = intval($oaprev);
        $i++;
      }

      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }

function chartqty()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }

      if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            }

      $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSLine.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "Trend QTY ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "Trend QTY ".$group." YTD ".$th ;
      $graph_width        = 954;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = $th;
      $dataset[1] = $thprev;
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
       
        $qty=0;
        $qty=$qty+$row->qty;
        $arrData[$th][$i] = intval($qty);
        $i++;
      }

      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
              $qtyprev=0;
              $qtyprev=$qtyprev+$row->qtyprev;
        $arrData[$thprev][$i] = intval($qtyprev);
        $i++;
      }

      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }

  function chartvnota()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }


      if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            }
      $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSLine.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "Trend Net Sales(Rp.) ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "Trend Net Sales(Rp.) ".$group." YTD ".$th ;
      $graph_width        = 954;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = $th;
      $dataset[1] = $thprev;
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
       
        $vnota=0;
        $vnota=$vnota+$row->vnota;
        $arrData[$th][$i] = intval($vnota);
        $i++;
      }

      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
              $vnotaprev=0;
              $vnotaprev=$vnotaprev+$row->vnotaprev;
        $arrData[$thprev][$i] = intval($vnotaprev);
        $i++;
      }

      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }

  function chartgrowthoa()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }

      if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            }

      $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "%Growth OA ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "%Growth OA ".$group." YTD ".$th ;
      $graph_width        = 954;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = 'GROWTH';
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $grwoa = 0;
        if ($row->oaprev == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->oaprev)/$row->oaprev);
          }

        $arrData['GROWTH'][$i] = number_format($grwoa,2);
        $i++;
      }

     
      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }

  function chartgrowthqty()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }

              if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            } 
        $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "%Growth Qty(Unit) ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "%Growth Qty(Unit) ".$group." YTD ".$th ;
      $graph_width        = 954;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = 'GROWTH';
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $grwqty = 0;
           if ($row->qtyprev == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->qty-$row->qtyprev)/$row->qtyprev);
          }

        $arrData['GROWTH'][$i] = number_format($grwqty,2);
        $i++;
      }

     
      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }

  function chartgrowthvnota()
  { 
    if (
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('menu599')=='t')) ||
    (($this->session->userdata('logged_in')) &&
    ($this->session->userdata('allmenu')=='t'))
     ){
        $dfrom= $this->uri->segment(4);
        $dto = $this->uri->segment(5);
        $group = $this->uri->segment(6);

        $query = $this->db->query("select e_product_groupname from tr_product_group where i_product_group='$group'");
          if ($query->num_rows() > 0){
            foreach($query->result() as $tmp){
               $group=$tmp->e_product_groupname;
           }
         }

         if($group=="NA"){
          $group="NASIONAL";
         }

              if($dfrom!=''){
              $tmp=explode("-",$dfrom);
              $hr=$tmp[0];
              $bl=$tmp[1];
              $th=$tmp[2];
              $thprev=$tmp[2]-1;
            } 
        $tipe=$this->uri->segment(7);
      if($tipe==''){
        $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
      }else{
        $tipe=str_replace("tandatitik",".",$tipe);
        $graph_swfFile      = base_url().'flash/'.$tipe;
      }
      $graph_caption      = "%Growth Net Sales(Rp.) ".$group." YTD ".$th ;
      $graph_numberPrefix = '' ;
      $graph_title        = "%Growth Net Sales(Rp.) ".$group." YTD ".$th ;
      $graph_width        = 954;
      $graph_height       = 500;
      
      $this->load->model('ctrbymonthly_new/mmaster');
     //group
      $i=0;
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $period=mbulan($row->i_periode);
        $category[$i] = $period;
        $i++;
      }

      // data set
      $dataset[0] = 'GROWTH';
      //$dataset[2] = 'GROWTHRP';

      //data 1
      $i=0;
      
      $result = $this->mmaster->baca($dfrom,$dto,$group);
      foreach($result as $row){
        $grwrp = 0;
          if ($row->vnotaprev == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->vnota-$row->vnotaprev)/$row->vnotaprev);
          }

        $arrData['GROWTH'][$i] = number_format($grwrp,2);
        $i++;
      }

     
      $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;

      //Convert category to XML and append
      $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
          $strXML .= "<category name='".$c."'/>" ;
      }
      $strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

      //Close <chart> element
      $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['modul']='ctrbymonthly_new';
      $data['eusi']= directory_map('./flash/');
      $data['file']='';
      ########chart end
      
      $config['base_url'] = base_url().'index.php/ctrbymonthly_new/cform/view/'.$dfrom.'/'.$dto.'/'.$group.'/'.$tipe.'/';
      $this->load->view('ctrbymonthly_new/chartoa',$data);
    }else{
      $this->load->view('awal/index.php');
    }

  }
}
?>
