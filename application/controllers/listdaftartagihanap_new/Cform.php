<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listdaftartagihanap_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			//$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihanap_new/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			if($isupplier!='AS'){
/*
			$query = $this->db->query("				
				select distinct a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, f.v_sisa as sisa2,(a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa+f.v_sisa) as jumlah2 , e.d_alokasi as tgl1, g.d_alokasi as tgl2
				from  tr_area b, tr_supplier c, tm_dtap a

				left join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
				left join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier)

				left join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
				left join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier)

				where a.i_area=b.i_area and a.i_supplier=c.i_supplier
				and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
				and a.f_dtap_cancel = 'f'
				and a.i_supplier='$isupplier'

				ORDER BY a.i_supplier, a.i_dtap",false);
*/
			$query =  $this->db->query("				
                select * from (
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, 0 as sisa2, (a.v_sisa) as jumlah1 ,
                (a.v_sisa) as jumlah2 , a.d_dtap as tgl1, a.d_dtap as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') 
                AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                and a.f_dtap_cancel = 'f' and a.i_supplier='$isupplier'
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, 0 as sisa2, (a.v_sisa) as jumlah1 ,
                (a.v_sisa) as jumlah2 , a.d_dtap as tgl1, a.d_dtap as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.d_dtap < to_date('$dfrom','dd-mm-yyyy') 
                and a.f_dtap_cancel = 'f' and a.i_supplier='$isupplier' and a.v_sisa>0
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, 0 as sisa2,
                (a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa) as jumlah2 , e.d_alokasi as tgl1, e.d_alokasi as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a 
                inner join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
                inner join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier
                and e.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
                )
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.f_dtap_cancel = 'f' and a.i_supplier='$isupplier' 
                AND a.d_dtap < to_date('$dfrom','dd-mm-yyyy')
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, f.v_sisa as sisa2,(a.v_sisa) as jumlah1 ,
                (a.v_sisa+f.v_sisa) as jumlah2 , g.d_alokasi as tgl1, g.d_alokasi as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a 
                inner join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
                inner join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier
                and g.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
                )
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.f_dtap_cancel = 'f' and a.i_supplier='$isupplier' 
                AND a.d_dtap < to_date('$dfrom','dd-mm-yyyy') 
                ) as a
                ORDER BY a.i_supplier, a.i_dtap",false);
			}else{
/*
				$query = $this->db->query("				
					select distinct a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, f.v_sisa as sisa2,(a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa+f.v_sisa) as jumlah2 , e.d_alokasi as tgl1, g.d_alokasi as tgl2
				from  tr_area b, tr_supplier c, tm_dtap a
				left join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
				left join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier)
				left join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
				left join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier)
				where a.i_area=b.i_area and a.i_supplier=c.i_supplier
				and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
				and a.f_dtap_cancel = 'f'
				ORDER BY a.i_supplier, a.i_dtap",false);
*/
			$query =  $this->db->query("				
                select * from (
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, 0 as sisa2, (a.v_sisa) as jumlah1 ,
                (a.v_sisa) as jumlah2 , a.d_dtap as tgl1, a.d_dtap as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') 
                AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                and a.f_dtap_cancel = 'f'
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, 0 as sisa2, (a.v_sisa) as jumlah1 ,
                (a.v_sisa) as jumlah2 , a.d_dtap as tgl1, a.d_dtap as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.d_dtap < to_date('$dfrom','dd-mm-yyyy') 
                and a.f_dtap_cancel = 'f' and a.v_sisa>0
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, d.v_sisa as sisa1, 0 as sisa2,
                (a.v_sisa+d.v_sisa) as jumlah1 ,(a.v_sisa) as jumlah2 , e.d_alokasi as tgl1, e.d_alokasi as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a 
                inner join tm_alokasi_bk_item d on(a.i_dtap=d.i_nota and a.i_supplier = d.i_supplier)
                inner join tm_alokasi_bk e on(d.i_alokasi = e.i_alokasi and d.i_kbank = e.i_kbank and d.i_supplier = e.i_supplier
                and e.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
                )
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.f_dtap_cancel = 'f'
                AND a.d_dtap < to_date('$dfrom','dd-mm-yyyy')
                union all
                select a.*, b.e_area_name, c.e_supplier_name, c.i_supplier_group, 0 as sisa1, f.v_sisa as sisa2,(a.v_sisa) as jumlah1 ,
                (a.v_sisa+f.v_sisa) as jumlah2 , g.d_alokasi as tgl1, g.d_alokasi as tgl2
                from  tr_area b, tr_supplier c, tm_dtap a 
                inner join tm_alokasi_kb_item f on(a.i_dtap=f.i_nota and a.i_supplier = f.i_supplier)
                inner join tm_alokasi_kb g on(f.i_alokasi = g.i_alokasi and f.i_kb = g.i_kb and f.i_supplier = g.i_supplier
                and g.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
                )
                where a.i_area=b.i_area and a.i_supplier=c.i_supplier and a.f_dtap_cancel = 'f'
                AND a.d_dtap < to_date('$dfrom','dd-mm-yyyy') 
                ) as a
                ORDER BY a.i_supplier, a.i_dtap",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10000';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$this->load->model('listdaftartagihanap_new/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Hutang Dagang Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdaftartagihanap_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihanap_new');
			$this->load->view('listdaftartagihanap_new/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idt			= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$isupplier= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);
			$this->load->model('listdaftartagihanap_new/mmaster');
			$this->mmaster->delete($idt,$iarea,$isupplier);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Hutang Dagang Supplier Area '.$iarea.' Supplier:'.$isupplier.' No:'.$idt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);		
			$config['base_url'] = base_url().'index.php/listdaftartagihanap_new/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dtap) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap_new');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listdaftartagihanap_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihanap_new/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dtap) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			if($cari!=NULL){$config['per_page'] = '40';}else{
			$config['per_page'] = '10';}
			//$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap_new');
			$this->load->model('listdaftartagihanap_new/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdaftartagihanap_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdaftartagihanap_new/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihanap_new/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listdaftartagihanap_new/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdaftartagihanap_new/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihanap_new/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('listdaftartagihanap_new/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
