<?php 

class Upload extends CI_Controller {
	
	function __construct()
	// function Upload()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	function index()
	{	
		if ($this->session->userdata('logged_in')){
			$this->load->view('uploadtarget/upload_form', array('error' => ' ' ));
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function target_upload()
	{
		if ($this->session->userdata('logged_in')){
			$config['upload_path'] = 'target/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
      $field='userfile';
			if ( ! $this->upload->do_upload($field))
			{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('uploadtarget/upload_form', $error);
			}	
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$this->load->view('uploadtarget/upload_success', $data);
				@chmod('target/*.*', 0777);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
