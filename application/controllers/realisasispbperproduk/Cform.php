<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu465')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('realisasispbperproduk');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$data['isupplier']	= '';
			$this->load->view('realisasispbperproduk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu465')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		
			$userid = $this->session->userdata('user_id');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($cari=='') $cari=$this->uri->segment(6);
			
			if ($cari == '')
				$cari = "all";

			$config['base_url'] = base_url().'index.php/realisasispbperproduk/cform/view/'.$dfrom.'/'.$dto.'/'.$cari.'/';
			
			// -----------------------------------------------------------------------------------------------------										
			if ($cari == "all")
				$keywordcari = "";
			else
				$keywordcari = $cari;
			
			$outputdata = array();
			$stringsql = " SELECT a.i_product, a.e_product_name, b.i_product_motif, b.e_product_motifname 
							FROM tr_product a, tr_product_motif b 
							WHERE a.i_product = b.i_product AND b.f_product_motifaktif = 't' 
							AND (UPPER(a.i_product) like '%".strtoupper($keywordcari)."%' 
							OR UPPER(a.e_product_name) like '%".strtoupper($keywordcari)."%' ) ORDER BY a.i_product ";
			//echo $stringsql; die();
			$sqlnya	= $this->db->query(" SELECT a.i_product, a.e_product_name, b.i_product_motif, b.e_product_motifname 
							FROM tr_product a, tr_product_motif b 
							WHERE a.i_product = b.i_product 
							AND (UPPER(a.i_product) like '%".strtoupper($keywordcari)."%' 
							OR UPPER(a.e_product_name) like '%".strtoupper($keywordcari)."%' ) ORDER BY a.i_product ");
			// AND b.f_product_motifaktif = 't' 
			if ($sqlnya->num_rows() > 0){
				$hasilnya=$sqlnya->result();
				foreach ($hasilnya as $rownya) {
					$iproduct = $rownya->i_product;
					$eproductname = $rownya->e_product_name;
					$eproductmotifname = $rownya->e_product_motifname;
					
					// qty di spb
					$queryxx = $this->db->query(" SELECT sum(b.n_order) as qtyspb from tm_spb a
								  inner join tm_spb_item b on(a.i_spb=b.i_spb AND a.i_area = b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND b.i_product = '".$rownya->i_product."' AND b.i_product_motif = '".$rownya->i_product_motif."'
								  and a.f_spb_cancel='f' ");
								  
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$qtyspb = $hasilxx->qtyspb;
					}
					else
						$qtyspb = 0;
						
					// qty SJ di tm_spb_item berdasarkan i_nota
					$queryxx = $this->db->query(" SELECT sum(b.n_deliver) as qtysj from tm_spb a
								  inner join tm_spb_item b on(a.i_spb=b.i_spb AND a.i_area = b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND b.i_product = '".$rownya->i_product."' AND b.i_product_motif = '".$rownya->i_product_motif."'
								  and a.f_spb_cancel='f' ");
								  
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$qtysj = $hasilxx->qtysj;
					}
					else
						$qtysj = 0;
					
					// qty sj di tm_nota_item berdasarkan i_sj (ga akurat)
				/*	$queryxx = $this->db->query(" SELECT sum(b.n_deliver) as qtysj from tm_nota a
								  inner join tm_nota_item b on(a.i_nota=b.i_nota)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND b.i_product = '".$rownya->i_product."' AND b.i_product_motif = '".$rownya->i_product_motif."'
								  and a.f_nota_cancel='f' AND not b.i_sj isnull ");
								  
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$qtysj = $hasilxx->qtysj;
					}
					else
						$qtysj = 0; */
					
					// qty nota di tm_spb_item berdasarkan i_nota
					$queryxx = $this->db->query(" SELECT sum(b.n_deliver) as qtynota from tm_spb a
								  inner join tm_spb_item b on(a.i_spb=b.i_spb AND a.i_area = b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND b.i_product = '".$rownya->i_product."' AND b.i_product_motif = '".$rownya->i_product_motif."'
								  and a.f_spb_cancel='f' AND not a.i_nota isnull ");
								  // AND (a.i_nota <> '' OR a.i_nota is not null)
					if ($queryxx->num_rows() > 0){
						$hasilxx = $queryxx->row();
						$qtynota = $hasilxx->qtynota;
					}
					else
						$qtynota = 0;
					
					$outputdata[] = array(	'iproduct'=> $iproduct,	
												'eproductname'=> $eproductname,	
												'eproductmotifname'=> $eproductmotifname,
												'qtyspb'=> $qtyspb,
												'qtysj'=> $qtysj,
												'qtynota'=> $qtynota
												);
				}
			}
			else
				$outputdata = '';
			// --------------------------------------------==================================----------------------------------
			
			
			//$config['total_rows'] = $query->num_rows(); 
		/*	$config['total_rows'] = count($outputdata); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config); */

			$this->load->model('realisasispbperproduk/mmaster');
			$data['page_title'] = $this->lang->line('realisasispbperproduk');
			if ($cari == "all")
				$data['cari'] = '';
			else
				$data['cari'] = $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			//$data['isi']		= $this->mmaster->bacaperiode($userid,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$keywordcari);
			//$data['isi']		= $this->mmaster->bacaperiode($userid,$dfrom,$dto,$keywordcari);
			$data['isi'] = $outputdata;
			$this->load->view('realisasispbperproduk/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	// 20-12-2013
	function detailpersales()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu465')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
					
			$iarea = $this->uri->segment(4);
			$dfrom = $this->uri->segment(5);
			$dto = $this->uri->segment(6);
			$cari = $this->uri->segment(7);
			
			// -----------------------------------------------------------------------------------------------------
			// query ambil nama area
			$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$iarea."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namaarea = $hasilxx->e_area_name;
			}
			else
				$namaarea = '';
							
			// query utk ambil data di tm_spb atau di tm_nota berdasarkan sales di area tsb dan dari range tglnya
			
			/*	$sqlnya	= $this->db->query(" SELECT distinct a.i_salesman, b.e_salesman_name FROM tm_spb a, tr_salesman b 
											where a.i_salesman = b.i_salesman AND a.i_area = '$iarea' 
											AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											ORDER BY a.i_salesman ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' 
							  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull 
							  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $iarea,	
												'namaarea'=> $namaarea,	
												'i_salesman'=> $rownya->i_salesman,	
												'e_salesman_name'=> $rownya->e_salesman_name,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					}
				} */
				//else { 
					//$outputdata = '';
					$sqlnya	= $this->db->query(" SELECT distinct a.i_salesman, b.e_salesman_name FROM tm_nota a, tr_salesman b 
												where a.i_salesman = b.i_salesman AND a.i_area = '$iarea' 
												AND ((a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) )
												ORDER BY a.i_salesman ");
					if ($sqlnya->num_rows() > 0){
						$hasilnya=$sqlnya->result();
						foreach ($hasilnya as $rownya) {
							// spb
							$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
									  inner join tr_area b on(a.i_area=b.i_area)
									  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
									  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' and a.f_spb_cancel='f' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaispb = $hasilxx->v_spb_gross;
							}
							else
								$nilaispb = 0;
							
							// sj
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' 
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaisj = $hasilxx->v_sj_gross;
							}
							else
								$nilaisj = 0;
							
							// nota
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' and not a.i_nota isnull 
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilainota = $hasilxx->v_nota_gross;
							}
							else
								$nilainota = 0;
									  
							$outputdata[] = array(	'i_area'=> $iarea,	
													'namaarea'=> $namaarea,	
													'i_salesman'=> $rownya->i_salesman,	
													'e_salesman_name'=> $rownya->e_salesman_name,	
													'nilaispb'=> $nilaispb,
													'nilaisj'=> $nilaisj,
													'nilainota'=> $nilainota
													);
						}
					}
					else
						$outputdata = '';
				//} // end if
			
			// -----------------------------------------------------------------------------------------------------
			
			$data['page_title'] = $this->lang->line('realisasispbvssj');
			$data['iarea']		= $iarea;
			$data['namaarea']		= $namaarea;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['cari']		= $cari;
			$data['isi']		= $outputdata;

			$this->load->view('realisasispbvssj/vdetailpersales',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	// 21-12-2013
	function detailperdata()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu465')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
					
			$isalesman = $this->uri->segment(4);
			$dfrom = $this->uri->segment(5);
			$dto = $this->uri->segment(6);
			$iarea = $this->uri->segment(7);
			
			// -----------------------------------------------------------------------------------------------------
			// query ambil nama area
			$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$iarea."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namaarea = $hasilxx->e_area_name;
			}
			else
				$namaarea = '';
			
			// query ambil nama salesman
			$queryxx = $this->db->query(" SELECT e_salesman_name from tr_salesman where i_salesman = '".$isalesman."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namasalesman = $hasilxx->e_salesman_name;
			}
			else
				$namasalesman = '';
							
			// query utk ambil data no SPB, SJ, dan NOTA berdasarkan sales di area tsb
			
			/*	// 1. spb
				$listspb = array();
				$sqlnya	= $this->db->query(" SELECT a.i_spb, a.d_spb, a.i_customer, b.e_customer_name  FROM tm_spb a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_spb_cancel='f'
								  ORDER BY a.d_spb DESC, a.i_spb DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$ispb = $rownya->i_spb;
						$dspb = $rownya->d_spb;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listspb[] = array(		'ispb'=> $ispb,	
												'dspb'=> $dspb,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					} // end for
				} // end if
				else
					$listspb = '';
			
			// 2. sj
				$listsj = array();
				$sqlnya	= $this->db->query(" SELECT a.i_sj, a.d_sj, a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_nota_cancel='f'
								  ORDER BY a.d_sj DESC, a.i_sj DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$isj = $rownya->i_sj;
						$dsj = $rownya->d_sj;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listsj[] = array(		'isj'=> $isj,	
												'dsj'=> $dsj,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listsj = '';
				
			// 3. nota
				$listnota = array();
				$sqlnya	= $this->db->query(" SELECT a.i_nota, a.d_nota, a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' 
								  and a.f_nota_cancel='f' and not a.i_nota isnull ORDER BY a.d_nota DESC, a.i_nota DESC  ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$inota = $rownya->i_nota;
						$dnota = $rownya->d_nota;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listnota[] = array(	'inota'=> $inota,	
												'dnota'=> $dnota,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listnota = ''; */
			
			$listdata = array();
				$sqlnya	= $this->db->query(" SELECT a.i_spb, a.d_spb, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.v_nota_gross,
									a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND
									(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_spb <= to_date('$dto','dd-mm-yyyy'))
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_nota_cancel='f'
								  ORDER BY a.d_spb DESC, a.i_spb DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$ispb = $rownya->i_spb;
						$dspb = $rownya->d_spb;
						$isj = $rownya->i_sj;
						$dsj = $rownya->d_sj;
						$inota = $rownya->i_nota;
						$dnota = $rownya->d_nota;
						$vnotagross = $rownya->v_nota_gross;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listdata[] = array(		'ispb'=> $ispb,	
												'dspb'=> $dspb,	
												'isj'=> $isj,	
												'dsj'=> $dsj,	
												'inota'=> $inota,	
												'dnota'=> $dnota,	
												'vnotagross'=> $vnotagross,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listdata = '';
			
			// -----------------------------------------------------------------------------------------------------
			//print_r($listnota); die();
			$data['page_title'] = $this->lang->line('realisasispbvssj');
			$data['iarea']		= $iarea;
			$data['namaarea']	= $namaarea;
			$data['isalesman']		= $isalesman;
			$data['namasalesman']	= $namasalesman;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['listdata']	= $listdata;

			$this->load->view('realisasispbvssj/vdetailperdata',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
