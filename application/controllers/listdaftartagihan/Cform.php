<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihan');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listdaftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select distinct(c.i_dt) as i_pelunasan, a.i_dt from tr_area b, tm_dt a
                                  left join tm_pelunasan c on(a.i_dt=c.i_dt and a.i_area=c.i_area and a.d_dt=c.d_dt)
										              where a.i_area=b.i_area
										              and (upper(a.i_dt) like '%$cari%')
										              and a.i_area='$iarea'
										              and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
										              AND a.d_dt <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows']	= $query->num_rows(); 
			$config['per_page']	= '10';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('listdaftartagihan');
			$data['cari']	  = $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	  = $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']	  = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data DT Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdaftartagihan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihan');
			$this->load->view('listdaftartagihan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idt			= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$dfrom		= $this->uri->segment(6);
			$dto			= $this->uri->segment(7);
			$tgl			= $this->uri->segment(8);
			$this->load->model('listdaftartagihan/mmaster');

			if($idt!=''){
			  $tmp=explode("-",$idt);
			  $siji=$tmp[0];
			  $loro=$tmp[1];
			  $idtx=$siji."/".$loro;
		  }
			$query 				= $this->db->query("select i_dt from tm_dt where i_dt = '$idt' and i_area='$iarea' ");
			if($query->num_rows()>0){
				$this->mmaster->delete($idt,$iarea,$tgl);
			}else{
				$this->mmaster->delete($idtx,$iarea,$tgl);
				$idt=$idtx;
			}

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Daftar Tagihan Area '.$iarea.' No:'.$idt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/listdaftartagihan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dt a, tr_area b
																	where a.i_area=b.i_area
																	and (upper(a.i_dt) like '%$cari%')
																	and a.i_area='$iarea'
																	and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
																	AND a.d_dt <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('listdaftartagihan');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listdaftartagihan/vmainform',$data);		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
		$query = $this->db->query(" select a.*, b.e_area_name from tm_dt a, tr_area b
									where a.i_area=b.i_area
									and (upper(a.i_dt) like '%$cari%')
									and a.i_area='$iarea'
									and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
									AND a.d_dt <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('listdaftartagihan');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdaftartagihan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdaftartagihan/cform/area/index/';
			$iuser   	= $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listdaftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu102')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listdaftartagihan/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$iuser   	= $this->session->userdata('user_id');
         $query   	= $this->db->query("select * from tr_area
                                  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listdaftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
