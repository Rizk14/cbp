<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/area/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_area');
			$data['iarea']='';
			$this->load->model('area/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Area";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan );

			$this->load->view('area/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 			= $this->input->post('iarea', TRUE);
			$eareaname 		= $this->input->post('eareaname', TRUE);
			$iareatype 		= $this->input->post('iareatype', TRUE);
			$eareatypename 	= $this->input->post('eareatypename', TRUE);
			$eareaaddress 	= $this->input->post('eareaaddress', TRUE);			
			$eareacity 		= $this->input->post('eareacity', TRUE);
			$eareaphone 	= $this->input->post('eareaphone', TRUE);
			$earearemark 	= $this->input->post('earearemark', TRUE);
			$nareatoleransi	= $this->input->post('nareatoleransi', TRUE);
			$eareashortname	= $this->input->post('eareashortname', TRUE);
			$istore			= $this->input->post('istore', TRUE);
			$estorename		= $this->input->post('estorename', TRUE);

			if ((isset($iarea) && $iarea != '') && (isset($eareaname) && $eareaname != '') && (isset($iareatype) && $iareatype != '') && (isset($istore) && $istore != ''))
			{
				$this->load->model('area/mmaster');
				$this->mmaster->insert($iarea,$eareaname,$iareatype,$eareaaddress,
						       $eareacity,$eareaphone,$earearemark,$nareatoleransi,
						       $eareashortname,$istore);

		        $sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Area:('.$iarea.') -'.$eareaname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );

		        $config['base_url'] = base_url().'index.php/area/cform/index/';
				$config['total_rows'] = $this->db->count_all('tr_area');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_area');
				$data['iarea']='';
				$this->load->model('area/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('area/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_area');
			$this->load->view('area/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_area')." update";
			if($this->uri->segment(4)){
				$iarea = $this->uri->segment(4);
				$data['iarea'] = $iarea;
				$this->load->model('area/mmaster');
				$data['isi']=$this->mmaster->baca($iarea);		 		

		 		$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Area:('.$iarea.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );
		        
		        $this->load->view('area/vmainform',$data);
			}else{
				$this->load->view('area/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 			= $this->input->post('iarea', TRUE);
			$eareaname 		= $this->input->post('eareaname', TRUE);
			$iareatype 		= $this->input->post('iareatype', TRUE);
			$eareatypename	 	= $this->input->post('eareatypename', TRUE);
			$eareaaddress 		= $this->input->post('eareaaddress', TRUE);			
			$eareacity 		= $this->input->post('eareacity', TRUE);
			$eareaphone 		= $this->input->post('eareaphone', TRUE);
			$earearemark 		= $this->input->post('earearemark', TRUE);
			$nareatoleransi		= $this->input->post('nareatoleransi', TRUE);
			$eareashortname		= $this->input->post('eareashortname', TRUE);
			$istore			= $this->input->post('istore', TRUE);
			$estorename		= $this->input->post('estorename', TRUE);
			$this->load->model('area/mmaster');
			$this->mmaster->update($iarea,$eareaname,$iareatype,$eareaaddress,
					       $eareacity,$eareaphone,$earearemark,$nareatoleransi,
					       $eareashortname,$istore);

	        $sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Area:('.$iarea.') -'.$eareaname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now , $pesan );

	        $config['base_url'] = base_url().'index.php/area/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_area');
			$data['iarea']='';
			$this->load->model('area/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('area/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$this->load->model('area/mmaster');
			$this->mmaster->delete($iarea);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
          $ip_address	  = $row['ip_address'];
          break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Master Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/area/cform/delete/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_area');
			$data['iarea']='';
			$this->load->model('area/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('area/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function areatype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/area/cform/areatype/index/';
			$config['total_rows'] = $this->db->count_all('tr_area_type');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('area/mmaster');
			$data['page_title'] = $this->lang->line('list_areatype');
			$data['isi']=$this->mmaster->bacaareatype($config['per_page'],$this->uri->segment(5));
			$this->load->view('area/vlistareatype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/area/cform/store/index/';
			$config['total_rows'] = $this->db->count_all('tr_store');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('area/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5));
			$this->load->view('area/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/area/cform/index/';
			$query=$this->db->query("select * from tr_area 
									 where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'");
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('area/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_area');
			$data['iarea']='';
	 		$this->load->view('area/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariareatype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/area/cform/areatype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select i_area_type from tr_area_type where upper(i_area_type) like '%$cari%' 
										or upper(e_area_typename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('area/mmaster');
			$data['page_title'] = $this->lang->line('list_areatype');
			$data['isi']=$this->mmaster->cariareatype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('area/vlistareatype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu19')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/area/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select i_store from tr_store where upper(i_store) like '%$cari%' 
										or upper(e_store_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('area/mmaster');
			$data['page_title'] = $this->lang->line('list_class');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('area/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
