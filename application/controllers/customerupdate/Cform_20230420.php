<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('paginationxx');
		//		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu217') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomer				= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icity					= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$iarea					= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype	= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct = $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname				= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress	= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress = $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade	= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference	= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier	= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount	= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$ecustomerretensi		= $this->input->post('ecustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if ($ncustomertoplength == '')
				$ncustomertoplength = 0;
			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $ecustomername != '')) {
				$this->load->model('customerupdate/mmaster');
				$this->mmaster->insert(
					$icustomer,
					$icustomerplugroup,
					$icity,
					$icustomergroup,
					$ipricegroup,
					$iarea,
					$icustomerproducttype,
					$icustomerspecialproduct,
					$icustomergrade,
					$icustomerservice,
					$icustomersalestype,
					$icustomerclass,
					$icustomerstatus,
					$ecustomername,
					$ecustomeraddress,
					$ecityname,
					$ecustomerpostal,
					$ecustomerphone,
					$ecustomerfax,
					$ecustomermail,
					$ecustomersendaddress,
					$ecustomerreceiptaddress,
					$ecustomerremark,
					$ecustomerpayment,
					$ecustomerpriority,
					$ecustomercontact,
					$ecustomercontactgrade,
					$ecustomerrefference,
					$ecustomerothersupplier,
					$fcustomerplusppn,
					$fcustomerplusdiscount,
					$fcustomerpkp,
					$fcustomeraktif,
					$fcustomertax,
					$ecustomerretensi,
					$ncustomertoplength
				);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Input Master Update Kode Pelanggan:' . $icustomer;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$cari = strtoupper($this->input->post('cari'));
				$config['base_url'] = base_url() . 'index.php/customerupdate/cform/index/';
				$sql = " select a.i_spb from tr_customer_tmp a, tr_area c,tm_spb d
				        where a.i_area=c.i_area and a.f_approve='t' and a.i_customer like '%000'
								and (a.i_spb like '%$cari%' or a.e_customer_name like '%$cari%') 
								and a.i_spb=d.i_spb
								and a.i_area=d.i_area
								and a.i_customer=d.i_customer
								and c.i_area=d.i_area
								and d.f_spb_cancel='false'";
				#and not a.i_approve_ar isnull and not a.i_approve isnull 
				$query 	= $this->db->query($sql, false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('update_customer');
				$data['ispb'] = '';
				$this->load->model('customerupdate/mmaster');
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
				$this->load->view('customerupdate/vmainform', $data);
			} else {
				$cari = strtoupper($this->input->post('cari'));
				$config['base_url'] = base_url() . 'index.php/customerupdate/cform/index/';
				$sql = " select a.i_spb from tr_customer_tmp a, tr_area c
				        where a.i_area=c.i_area and a.f_approve='t' and a.i_customer like '%000'
								and (a.i_spb like '%$cari%' or a.e_customer_name like '%$cari%')";
				#and not a.i_approve_ar isnull and not a.i_approve isnull 
				$query 	= $this->db->query($sql, false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('update_customer');
				$data['ispb'] = '';
				$this->load->model('customerupdate/mmaster');
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));

				$sess	= $this->session->userdata('session_id');
				$id 	= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = "Membuka Menu Update Kode Pelanggan";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customerupdate/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu217') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('update_customer');
			$this->load->view('customerupdate/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu217') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('update_customer');
			if ($this->uri->segment(4)) {
				$ispb	 = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$ipricegroup = $this->uri->segment(6);
				$data['ispb'] = $ispb;
				$data['iarea'] = $iarea;
				$data['ipricegroup'] = $ipricegroup;
				$this->load->model('customerupdate/mmaster');
				$data['isi'] = $this->mmaster->baca($ispb, $iarea);
				$data['isispb'] = $this->mmaster->bacaspb($ispb, $iarea);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();
				$data['isidetail'] = $this->mmaster->bacadetail($ispb, $iarea, $ipricegroup);
				$qnilaiorderspb	= $this->mmaster->bacadetailnilaiorderspb($ispb, $iarea, $ipricegroup);
				if ($qnilaiorderspb->num_rows() > 0) {
					$row_nilaiorderspb	= $qnilaiorderspb->row();
					$data['nilaiorderspb']	= $row_nilaiorderspb->nilaiorderspb;
				} else {
					$data['nilaiorderspb']	= 0;
				}

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Membuka Edit Master Update Kode Pelanggan SPB area:' . $iarea . ' no:' . $ispb;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customerupdate/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu217') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomer = $this->input->post('icustomer', TRUE);
			$ispb			= $this->input->post('ispb', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$irefcode	= $this->input->post('irefcode', TRUE); /* TAMBAHAN 17 APR 2023 (KODE SPS) */
			##
			if (substr($icustomer, 2, 3) <> '000') {
				$isalesman						= $this->input->post('isalesman', TRUE);
				$icustomergroup				= $this->input->post('icustomergroup', TRUE);
				$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
				$icustomerproducttype	= $this->input->post('icustomerproducttype', TRUE);
				$icustomerspecialproduct = $this->input->post('icustomerspecialproduct', TRUE);
				$icustomerstatus			= $this->input->post('icustomerstatus', TRUE);
				$icustomergrade				= $this->input->post('icustomergrade', TRUE);
				$icustomerservice			= $this->input->post('icustomerservice', TRUE);
				$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
				$esalesmanname				= $this->input->post('esalesmanname', TRUE);
				$dsurvey							= $this->input->post('dsurvey', TRUE);
				if ($dsurvey != '') {
					$tmp = explode("-", $dsurvey);
					$th = $tmp[2];
					$bl = $tmp[1];
					$hr = $tmp[0];
					$dsurvey = $th . "-" . $bl . "-" . $hr;
				} else {
					$dsurvey = null;
				}
				$nvisitperiod					= $this->input->post('nvisitperiod', TRUE);
				$fcustomernew					= $this->input->post('fcustomernew', TRUE);
				if ($fcustomernew != '')
					$fcustomernew			= 'f';
				else
					$fcustomernew			= 't';
				$ecustomername				= $this->input->post('ecustomername', TRUE);
				$ecustomeraddress			= $this->input->post('ecustomeraddress', TRUE);
				$ecustomersign				= $this->input->post('ecustomersign', TRUE);
				$ecustomerphone				= $this->input->post('ecustomerphone', TRUE);
				$ecustomerphone2			= $this->input->post('ecustomerphone2', TRUE);
				$ecustomercontact			= $this->input->post('ecustomercontact', TRUE);
				$ecustomercontactgrade = $this->input->post('ecustomercontactgrade', TRUE);
				$ecustomermail  			= $this->input->post('ecustomermail', TRUE);
				$ecustomerrefference	= $this->input->post('ecustomerrefference', TRUE);
				$ert1									= $this->input->post('ert1', TRUE);
				$erw1									= $this->input->post('erw1', TRUE);
				$epostal1							= $this->input->post('epostal1', TRUE);
				$ecustomerkelurahan1	= $this->input->post('ecustomerkelurahan1', TRUE);
				$ecustomerkecamatan1	= $this->input->post('ecustomerkecamatan1', TRUE);
				$ecustomerkota1				= $this->input->post('ecustomerkota1', TRUE);
				$ecustomerprovinsi1		= $this->input->post('ecustomerprovinsi1', TRUE);
				$efax1								= $this->input->post('efax1', TRUE);
				$ecustomermonth				= $this->input->post('ecustomermonth', TRUE);
				$ecustomeryear				= $this->input->post('ecustomeryear', TRUE);
				$ecustomerage					= $this->input->post('ecustomerage', TRUE);
				$eshopstatus					= $this->input->post('eshopstatus', TRUE);
				$ishopstatus					= $this->input->post('ishopstatus', TRUE);
				$nshopbroad						= $this->input->post('nshopbroad', TRUE);
				$ecustomerowner				= $this->input->post('ecustomerowner', TRUE);
				$ecustomerownerttl		= $this->input->post('ecustomerownerttl', TRUE);
				$ecustomerownerage		= $this->input->post('ecustomerownerage', TRUE);
				$emarriage						= $this->input->post('emarriage', TRUE);
				$imarriage						= $this->input->post('imarriage', TRUE);
				$ejeniskelamin				= $this->input->post('ejeniskelamin', TRUE);
				$ijeniskelamin				= $this->input->post('ijeniskelamin', TRUE);
				$ereligion						= $this->input->post('ereligion', TRUE);
				$ireligion						= $this->input->post('ireligion', TRUE);
				$ecustomerowneraddress = $this->input->post('ecustomerowneraddress', TRUE);
				$ecustomerownerphone	= $this->input->post('ecustomerownerphone', TRUE);
				$ecustomerownerhp			= $this->input->post('ecustomerownerhp', TRUE);
				$ecustomerownerfax		= $this->input->post('ecustomerownerfax', TRUE);
				$ecustomerownerpartner = $this->input->post('ecustomerownerpartner', TRUE);
				$ecustomerownerpartnerttl = $this->input->post('ecustomerownerpartnerttl', TRUE);
				$ecustomerownerpartnerage = $this->input->post('ecustomerownerpartnerage', TRUE);
				$ert2									= $this->input->post('ert2', TRUE);
				$erw2									= $this->input->post('erw2', TRUE);
				$epostal2							= $this->input->post('epostal2', TRUE);
				$ecustomerkelurahan2	= $this->input->post('ecustomerkelurahan2', TRUE);
				$ecustomerkecamatan2	= $this->input->post('ecustomerkecamatan2', TRUE);
				$ecustomerkota2				= $this->input->post('ecustomerkota2', TRUE);
				$ecustomerprovinsi2		= $this->input->post('ecustomerprovinsi2', TRUE);
				$ecustomersendaddress	= $this->input->post('ecustomersendaddress', TRUE);
				$ecustomersendphone		= $this->input->post('ecustomersendphone', TRUE);
				$etraversed						= $this->input->post('etraversed', TRUE);
				$itraversed						= $this->input->post('itraversed', TRUE);
				$fparkir							= $this->input->post('fparkir', TRUE);
				if ($fparkir != '')
					$fparkir			= 't';
				else
					$fparkir			= 'f';
				$fkuli								= $this->input->post('fkuli', TRUE);
				if ($fkuli != '')
					$fkuli			= 't';
				else
					$fkuli			= 'f';
				$eekspedisi1					= $this->input->post('eekspedisi1', TRUE);
				$eekspedisi2					= $this->input->post('eekspedisi2', TRUE);
				$ert3									= $this->input->post('ert3', TRUE);
				$erw3									= $this->input->post('erw3', TRUE);
				$epostal3							= $this->input->post('epostal3', TRUE);
				$ecustomerkota3				= $this->input->post('ecustomerkota3', TRUE);
				$ecustomerprovinsi3		= $this->input->post('ecustomerprovinsi3', TRUE);
				$ecustomerpkpnpwp			= $this->input->post('ecustomernpwp', TRUE);
				if ($ecustomerpkpnpwp != '')
					$fspbpkp			= 't';
				else
					$fspbpkp			= 'f';
				$ecustomernpwpname		= $this->input->post('ecustomernpwpname', TRUE);
				$ecustomernpwpaddress	= $this->input->post('ecustomernpwpaddress', TRUE);
				$ecustomerclassname		= $this->input->post('ecustomerclassname', TRUE);
				$icustomerclass				= $this->input->post('icustomerclass', TRUE);
				$epaymentmethod				= $this->input->post('epaymentmethod', TRUE);
				$ipaymentmethod				= $this->input->post('ipaymentmethod', TRUE);
				$ecustomerbank1				= $this->input->post('ecustomerbank1', TRUE);
				$ecustomerbankaccount1 = $this->input->post('ecustomerbankaccount1', TRUE);
				$ecustomerbankname1		= $this->input->post('ecustomerbankname1', TRUE);
				$ecustomerbank2				= $this->input->post('ecustomerbank2', TRUE);
				$ecustomerbankaccount2 = $this->input->post('ecustomerbankaccount2', TRUE);
				$ecustomerbankname2		= $this->input->post('ecustomerbankname2', TRUE);
				$ekompetitor1					= $this->input->post('ekompetitor1', TRUE);
				$ekompetitor2					= $this->input->post('ekompetitor2', TRUE);
				$ekompetitor3					= $this->input->post('ekompetitor3', TRUE);
				$nspbtoplength				= $this->input->post('ncustomertoplength', TRUE);
				$ncustomerdiscount		= $this->input->post('ncustomerdiscount', TRUE);
				$epricegroupname			= $this->input->post('epricegroupname', TRUE);
				$ipricegroup					= $this->input->post('ipricegroup', TRUE);
				$nline								= $this->input->post('nline', TRUE);
				$fkontrabon						= $this->input->post('fkontrabon', TRUE);
				if ($fkontrabon != '')
					$fkontrabon			= 't';
				else
					$fkontrabon			= 'f';
				$ecall								= $this->input->post('ecall', TRUE);
				$icall								= $this->input->post('icall', TRUE);
				$ekontrabonhari				= $this->input->post('ekontrabonhari', TRUE);
				$ekontrabonjam1				= $this->input->post('ekontrabonjam1', TRUE);
				$ekontrabonjam2				= $this->input->post('ekontrabonjam2', TRUE);
				$etagihhari						= $this->input->post('etagihhari', TRUE);
				$etagihjam1						= $this->input->post('etagihjam1', TRUE);
				$etagihjam2						= $this->input->post('etagihjam2', TRUE);
				$dspb 	= $this->input->post('dspb', TRUE);
				if ($dspb != '') {
					$tmp = explode("-", $dspb);
					$th = $tmp[2];
					$bl = $tmp[1];
					$hr = $tmp[0];
					$dspb = $th . "-" . $bl . "-" . $hr;
					$dspbreceive = $dspb;
					$thbl = $th . $bl;
				}
				$nspbdiscount1				= $this->input->post('ncustomerdiscount1', TRUE);
				$nspbdiscount2				= $this->input->post('ncustomerdiscount2', TRUE);
				$nspbdiscount3				= $this->input->post('ncustomerdiscount3', TRUE);
				$vspbdiscount1				= $this->input->post('vcustomerdiscount1', TRUE);
				$vspbdiscount2				= $this->input->post('vcustomerdiscount2', TRUE);
				$vspbdiscount3				= $this->input->post('vcustomerdiscount3', TRUE);
				$vspbdiscounttotal		= $this->input->post('vspbdiscounttotal', TRUE);
				$vspb									= $this->input->post('vspb', TRUE);
				$nspbdiscount1				= str_replace(',', '', $nspbdiscount1);
				$nspbdiscount2				= str_replace(',', '', $nspbdiscount2);
				$nspbdiscount3				= str_replace(',', '', $nspbdiscount3);
				$vspbdiscount1				= str_replace(',', '', $vspbdiscount1);
				$vspbdiscount2				= str_replace(',', '', $vspbdiscount2);
				$vspbdiscount3				= str_replace(',', '', $vspbdiscount3);
				$vspbdiscounttotal		= str_replace(',', '', $vspbdiscounttotal);
				$vspb									= str_replace(',', '', $vspb);
				$ispbpo								= $this->input->post('ispbpo', TRUE);
				if ($ispbpo == '') $ispbpo = null;
				if ($ispbpo == null) $fspbop = 'f';
				else $fspbop = 't';
				$fspbstockdaerah			= $this->input->post('fspbstockdaerah', TRUE);
				if ($fspbstockdaerah != '') {
					$fspbstockdaerah		= 't';
				} else {
					$fspbstockdaerah		= 'f';
				}
				$fspbconsigment				= 'f';
				$fspbprogram					= 'f';
				$fspbvalid						= 'f';
				$fspbsiapnotagudang		= 'f';
				$fspbcancel						= 'f';
				$fspbfirst						= 't';
				$eremarkx							= $this->input->post('eremarkx', TRUE);
				###### sempat ditutup, karena ada beda master jd dibuka lagi
				$fspbplusppn					= $this->input->post('fspbplusppn', TRUE);
				if ($fspbplusppn != '')
					$fspbplusppn			= 't';
				else
					$fspbplusppn			= 'f';
				$fspbplusdiscount			= $this->input->post('fspbplusdiscount', TRUE);
				if ($fspbplusdiscount != '')
					$fspbplusdiscount			= 't';
				else
					$fspbplusdiscount			= 'f';
				######
				$ispbold							= $this->input->post('ispbold', TRUE);
				$jml									= $this->input->post('jml', TRUE);
				##
				$this->db->trans_begin();
				$this->load->model('customerupdate/mmaster');

				$cek_data = $this->mmaster->cek_data($icustomer);
				if ($cek_data->num_rows() > 0) {
					echo "Kodelang sudah pernah digunakan !";
					die();
				}

				if ($nvisitperiod == '') $nvisitperiod = 1;
				if ($dsurvey == '') $dsurvey = $dspb;
				if ($nshopbroad == '') $nshopbroad = 0;
				$this->mmaster->update(
					$ispb,
					$icustomer,
					$iarea,
					$isalesman,
					$esalesmanname,
					$dsurvey,
					$nvisitperiod,
					$fcustomernew,
					$ecustomername,
					$ecustomeraddress,
					$ecustomersign,
					$ecustomerphone,
					$ert1,
					$erw1,
					$epostal1,
					$ecustomerkelurahan1,
					$ecustomerkecamatan1,
					$ecustomerkota1,
					$ecustomerprovinsi1,
					$efax1,
					$ecustomermonth,
					$ecustomeryear,
					$ecustomerage,
					$eshopstatus,
					$ishopstatus,
					$nshopbroad,
					$ecustomerowner,
					$ecustomerownerttl,
					$emarriage,
					$imarriage,
					$ejeniskelamin,
					$ijeniskelamin,
					$ereligion,
					$ireligion,
					$ecustomerowneraddress,
					$ecustomerownerphone,
					$ecustomerownerhp,
					$ecustomerownerfax,
					$ecustomerownerpartner,
					$ecustomerownerpartnerttl,
					$ecustomerownerpartnerage,
					$ert2,
					$erw2,
					$epostal2,
					$ecustomerkelurahan2,
					$ecustomerkecamatan2,
					$ecustomerkota2,
					$ecustomerprovinsi2,
					$ecustomersendaddress,
					$ecustomersendphone,
					$etraversed,
					$itraversed,
					$fparkir,
					$fkuli,
					$eekspedisi1,
					$eekspedisi2,
					$ert3,
					$erw3,
					$epostal3,
					$ecustomerkota3,
					$ecustomerprovinsi3,
					$ecustomerpkpnpwp,
					$fspbpkp,
					$ecustomernpwpname,
					$ecustomernpwpaddress,
					$ecustomerclassname,
					$icustomerclass,
					$epaymentmethod,
					$ipaymentmethod,
					$ecustomerbank1,
					$ecustomerbankaccount1,
					$ecustomerbankname1,
					$ecustomerbank2,
					$ecustomerbankaccount2,
					$ecustomerbankname2,
					$ekompetitor1,
					$ekompetitor2,
					$ekompetitor3,
					$nspbtoplength,
					$ncustomerdiscount,
					$epricegroupname,
					$ipricegroup,
					$nline,
					$fkontrabon,
					$ecall,
					$icall,
					$ekontrabonhari,
					$ekontrabonjam1,
					$ekontrabonjam2,
					$etagihhari,
					$etagihjam1,
					$etagihjam2,
					$icustomergroup,
					$icustomerplugroup,
					$icustomerproducttype,
					$icustomerspecialproduct,
					$icustomerstatus,
					$icustomergrade,
					$icustomerservice,
					$icustomersalestype,
					$ecustomerownerage,
					$ecustomerphone2,
					$ecustomercontact,
					$ecustomercontactgrade,
					$ecustomermail,
					$ecustomerrefference,
					$fspbplusppn,
					$fspbplusdiscount,
					$irefcode
				);
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					#				$this->db->trans_rollback();
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if (pg_num_rows($rs) > 0) {
						while ($row = pg_fetch_assoc($rs)) {
							$ip_address	  = $row['ip_address'];
							break;
						}
					} else {
						$ip_address = 'kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Update Customer KodeLang:' . $icustomer;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $icustomer;
					$this->load->view('nomor', $data);
				}
			}
			// Tidak sukses Update KodeLang karena kodenya masih 000
			else {
				$data['sukses']			= false;
				$data['inomor']			= $icustomer;
				$this->load->view('nomor', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu217') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/customerupdate/cform/index/';
			$cari = strtoupper($this->input->post('cari'));
			$query = $this->db->query("select * from tr_customer 
                               where upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('customerupdate/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('update_customer');
			$data['icustomer'] = '';
			$this->load->view('customerupdate/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
