<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu297')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printsjrt/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printsjrt');
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('printsjrt/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu297')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/printsjrt/cform/view/'.$dfrom.'/'.$dto.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.*, c.e_area_name from tm_sjrt a, tr_area c
										              where substring(a.i_sjr,10,2)=c.i_area 
										              and (upper(a.i_sjr) like '%$cari%') and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2)='$area2'
										              or substring(a.i_sjr,10,2)='$area3' or substring(a.i_sjr,10,2)='$area4' or substring(a.i_sjr,10,2)='$area5')
																	and a.d_sjr >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjr <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsjrt');
			$this->load->model('printsjrt/mmaster');
			$data['cari']=$cari;
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5,$dfrom,$dto);
			$this->load->view('printsjrt/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu297')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isjr  = $this->uri->segment(4);
			$area = substr($isjr,9,2);
			$this->load->model('printsjrt/mmaster');
			$data['isjr']=$isjr;
			$data['page_title'] = $this->lang->line('printsjrt');
			$data['isi']=$this->mmaster->baca($isjr,$area);
			$data['detail'] = $this->mmaster->bacadetail($isjr,$area);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJ Retur Toko Area:'.$area.' No:'.$isjr;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjrt/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu297')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsjrt');
			$this->load->view('printsjrt/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu297')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjrt/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom= strtoupper($this->input->post('dfrom', FALSE));
			$dto 	= strtoupper($this->input->post('dto', FALSE));
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" select a.*, c.e_area_name from tm_sjrt a, tr_area c
										              where substring(a.i_sjr,10,2)=c.i_area
										              and (upper(a.i_sjr) like '%$cari%') and (substring(a.i_sjr,10,2)='$area1' or substring(a.i_sjr,10,2)='$area2'
										              or substring(a.i_sjr,10,2) = '$area3' or substring(a.i_sjr,10,2) = '$area4' or substring(a.i_sjr,10,2)='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printsjrt/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$dfrom,$dto);
			$data['page_title'] = $this->lang->line('printsjrt');
			$data['cari']=$cari;
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['isj']='';
			$data['detail']='';
	 		$this->load->view('printsjrt/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
