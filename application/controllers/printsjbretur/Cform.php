<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu458')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printsjbretur/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printsj').' Retur Konsinyasi';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('printsjbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		   ($this->session->userdata('menu458')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			 ($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/printsjbretur/cform/view/'.$dfrom.'/'.$dto.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name, c.i_customer, c.i_sjpbr
                                  from tm_sjbr a, tr_customer b, tm_sjpbr c
                                  where c.i_customer=b.i_customer
                                  and a.i_sjbr=c.i_sjbr and a.i_area=c.i_area
                                  and (upper(a.i_sjbr) like '%$cari%' or upper(c.i_customer) like '%$cari%'
                                  or upper(b.e_customer_name) like '%$cari%')
                                  and a.d_sjbr >= to_date('$dfrom','dd-mm-yyyy')
                                  and a.d_sjbr <= to_date('$dto','dd-mm-yyyy')
                                  order by a.i_sjbr",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsj').' Retur Konsinyasi';
			$this->load->model('printsjbretur/mmaster');
      $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(6),$dfrom,$dto);
			$this->load->view('printsjbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu458')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isjbr  = $this->uri->segment(4);
			$this->load->model('printsjbretur/mmaster');
			$data['isjbr']=$isjbr;
			$data['page_title'] = $this->lang->line('printsj').' Retur Konsinyasi';
			$data['isi']=$this->mmaster->baca($isjbr);
			$data['detail']=$this->mmaster->bacadetail($isjbr);
      $this->mmaster->updatesjbr($isjbr);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJB Retur Konsinyasi No:'.$isjbr;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjbretur/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu458')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsj').' Retur Konsinyasi';
			$this->load->view('printsjbretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
