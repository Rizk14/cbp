<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->dbutil();
    $this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-pajak-lotte');
			$this->load->view('exp-pajak-lotte/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function views()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu486')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-pajak-lotte');
			$iperiodeawal	  = $this->input->post('iperiodeawal');
			$iperiodeakhir		= $this->input->post('iperiodeakhir');
			if($iperiodeawal=='') $dfrom=$this->uri->segment(4);
			if($iperiodeakhir=='') $dto=$this->uri->segment(5);
			$this->load->model('exp-pajak-lotte/mmaster');
			$data['iperiodeawal'] = $iperiodeawal;
			$data['iperiodeakhir'] = $iperiodeakhir;
			$data['isi']		= $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghitung plafond Periode:'.$iperiodeawal.' s/d '.$iperiodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('exp-pajak-lotte/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu486')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-pajak-lotte');
			$this->load->view('exp-pajak-lotte/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/exp-pajak-lotte/cform/view/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b, tr_customer_consigment c
                                  where a.i_customer=b.i_customer and a.i_customer=c.i_customer and c.i_price_groupco ='14'
		                              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
		                              or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
		                              and a.i_area='PB' and not a.i_faktur_komersial is null",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-pajak-lotte/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacaperiode($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-pajak-lotte/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-pajak-lotte/cform/bank/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-pajak-lotte/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-pajak-lotte/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu497')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $inota=$this->uri->segment(4);
			  $iarea=$this->uri->segment(5);
			  $this->load->model('exp-pajak-lotte/mmaster');
			  $isi      = $this->mmaster->bacapajak($inota,$iarea);
			  $detail   = $this->mmaster->bacadetail($inota,$iarea);
  			$this->load->library('PHPExcel');
	  		$this->load->library('PHPExcel/IOFactory');
	  		$objPHPExcel = new PHPExcel();
			  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:H2'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'No Seri Pajak');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Alamat');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);			
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'NPWP');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'No Faktur');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=1;
        $vdisc1=0;
        $total=0;
        $vdisc2=0;
        $total=0;
        $vdisc3=0;
        $total=0;
        $vdisc4=0;
        $total=0;
        foreach($isi as $row){
        $i++;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
          if( ($row->f_customer_pkp=='f') ){
            $row->e_customer_name = $row->e_customer_name;
            $row->e_customer_pkpaddress = $row->e_customer_city;
          }else{
        		$row->e_customer_name = $row->e_customer_pkpname;
            $row->e_customer_pkpaddress = $row->e_customer_pkpaddress;
          }
          if( ($row->e_customer_pkpnpwp=='') || ($row->e_customer_pkpnpwp==null) ){
            $row->e_customer_pkpnpwp="00.000.000.0.000.000";
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_customer_pkpaddress, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_customer_pkpnpwp, Cell_DataType::TYPE_STRING);

          if(strlen(trim($row->i_faktur_komersial))>6){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, substr($row->i_faktur_komersial,8,6)."/".substr($row->i_nota,8,8), Cell_DataType::TYPE_STRING);
          }else{
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, trim($row->i_faktur_komersial)."/".substr($row->i_nota,8,8), Cell_DataType::TYPE_STRING);
          }
    
    $row->v_nota_gross=$row->v_nota_gross;
    
    if( ($row->n_nota_discount1+$row->n_nota_discount2+$row->n_nota_discount3+$row->n_nota_discount4==0) && $row->v_nota_discounttotal <> 0 )
    {
      if($row->f_plus_ppn=='t'){
        $vdisc1=$row->v_nota_discounttotal;
      }else{
        $vdisc1=$row->v_nota_discounttotal/1.1;
      }
    }else{
      $vdisc1=round($vdisc1+($total*$row->n_nota_discount1)/100);
    }
	  $vdisc2   = round($vdisc2+((($total-$vdisc1)*$row->n_nota_discount2)/100));
	  $vdisc3   = round($vdisc3+((($total-($vdisc1+$vdisc2))*$row->n_nota_discount3)/100));
  	$vdisc4   = round($vdisc4+((($total-($vdisc1+$vdisc2+$vdisc3))*$row->n_nota_discount4)/100));
    $vdistot	= round($vdisc1+$vdisc2+$vdisc3+$vdisc4);
    $row->v_nota_discount=$vdistot;

    if($row->f_plus_ppn=='t'){
		  $ndpp=round(($row->v_nota_gross-$row->v_nota_discount)/1.1);
      $nppn=round((($row->v_nota_gross-$row->v_nota_discount)/1.1)*0.1);
    }else{
   		$ndpp=round($row->v_nota_gross-$row->v_nota_discount);
      $nppn=round(($row->v_nota_gross-$row->v_nota_discount)*0.1);
    }
        }
	  		$objPHPExcel->getProperties()->setTitle("Pajak Lotte Mart ".$inota)->setDescription(NmPerusahaan);
	  		$objPHPExcel->setActiveSheetIndex(0);
        if(count($isi)>0){
				  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A4:H5'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Harga Jual');
				$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);			
				$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);			
				
        $i=5;
        $j=1;
			  $cell='B';

			  
        foreach($detail as $rowi){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',

				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      'A'.$i.':H'.$i
		      );

      if($rowi->n_deliver>0){
				$name	= $rowi->e_product_name;
				if($row->f_plus_ppn=='t'){
					$tot	= $rowi->n_deliver*$rowi->v_unit_price;
				}else{
					$tot	= $rowi->n_deliver*($rowi->v_unit_price/1.1);
				}
			}
	
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $rowi->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $rowi->n_deliver, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $rowi->v_unit_price, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $tot, Cell_DataType::TYPE_STRING);
			    $i++;
          $j++;
          }
/*          
          $i=$i+2;
          
          $objPHPExcel->getActiveSheet()->duplicateStyleArray( 
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
		      'A'.$i.':H'.$i
				  );

        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'Harga Jual /Penggantian');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
        $i++;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray( 
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
		      'A'.$i.':H'.$i
				  );
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'Potongan');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->v_nota_discount, Cell_DataType::TYPE_NUMERIC);
        $i++;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray( 
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
		      'A'.$i.':H'.$i
				  );
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'DPP');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $notaneto = $row->v_nota_gross-$row->v_nota_discount;
				$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, '100/110 X '.$notaneto, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $ndpp, Cell_DataType::TYPE_NUMERIC);
        $i++;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray( 
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
		      'A'.$i.':H'.$i
				  );
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $nppn, Cell_DataType::TYPE_NUMERIC);
*/

			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Pajak Lotte Mart '.$inota.'.xls';
        if(file_exists('pajak/'.$nama)){
          @chmod('pajak/'.$nama, 0777);
          @unlink('pajak/'.$nama);
        }
			  $objWriter->save('pajak/'.$nama);
			  $sess=$this->session->userdata('session_id');

			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  $pesan='Export Pajak Lotte Mart '.$inota;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }
}
?>
