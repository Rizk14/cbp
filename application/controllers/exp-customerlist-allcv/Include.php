<?php 
if ($isi > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'CUSTOMER LIST ');
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,16,2);
               if($iarea=='NA'){
                 $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area : Nasional");
               }
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,16,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:R5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Alamat');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kota');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'No Tlp');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Disc 1');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Disc 2');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'KS');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Contact Person');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tgl Daftar');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Aktif');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Klp Harga');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Nama PKP');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Alamat PKP');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'NPWP');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Jenis Pelanggan');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $xarea='';
               $no=1;
               $lang='';
               foreach($isi as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array

                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':R'.$i
                  );

                  if($row->f_customer_aktif=='t'){
                    $aktif="Ya";
                  }else{
                    $aktif="Tdk";
                  }

                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_customer);
                     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_customer_address);
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_city_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, "'".$row->e_customer_phone);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_customer_toplength);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_customer_discount1);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_customer_discount2);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, "'".$row->i_salesman);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->e_customer_contact);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->d_signin);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $aktif);
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->e_price_groupname);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->e_customer_pkpname);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->e_customer_pkpaddress);
                     $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->e_customer_pkpnpwp);
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $row->e_customer_classname);
                     $no++;
                     $i++;
                     $j++;
               }
               $x=$i-1;
            }
?>