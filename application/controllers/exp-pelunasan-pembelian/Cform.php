<?php
class Cform extends CI_Controller
{
	public $title 	= "Export Pelunasan Pembelian";
	public $folder 	= "exp-pelunasan-pembelian";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['folder'] 	= $this->folder;
			$data['dfrom'] 		= '';
			$data['dto'] 		= '';

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			//$iarea	= $this->input->post('iarea');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($isupplier == '') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $isupplier . '/index/';
			if ($isupplier != 'AS') {

				$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
			where a.i_area=b.i_area 
			and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%')
			and a.i_supplier='$isupplier' and
			a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')", false);
			} else {

				$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
			where a.i_area=b.i_area 
			and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%') and
			a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy') ORDER BY a.i_supplier, a.i_dtap", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');

			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Membuka Data Hutang Dagang Supplier ' . $isupplier . ' Periode:' . $dfrom . ' s/d ' . $dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$this->load->view($this->folder . '/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$idt			= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$isupplier = $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);

			$this->mmaster->delete($idt, $iarea, $isupplier);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Menghapus Hutang Dagang Supplier Area ' . $iarea . ' Supplier:' . $isupplier . ' No:' . $idt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $isupplier . '/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
		where a.i_area=b.i_area 
		and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
		or upper(a.i_dtap) like '%$cari%')
		and a.i_supplier='$isupplier' and
		a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
		a.d_dtap <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $cari);
			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($isupplier == '') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $isupplier . '/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
		where a.i_area=b.i_area 
		and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
		or upper(a.i_dtap) like '%$cari%')
		and a.i_supplier='$isupplier' and
		a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
		a.d_dtap <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			if ($cari != NULL) {
				$config['per_page'] = '40';
			} else {
				$config['per_page'] = '10';
			}
			//$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');

			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);
			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/supplier/index/';

			$query = $this->db->query("select * from tr_supplier ", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->lang->line('list_supplier');
			$data['isi'] 			= $this->mmaster->bacasupplier($config['per_page'], $this->uri->segment(5));
			$data['folder'] 		= $this->folder;

			$this->load->view($this->folder . '/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/supplier/index/';

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%' ", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->lang->line('list_supplier');
			$data['folder'] 		= $this->folder;
			$data['isi'] 			= $this->mmaster->carisupplier($cari, $config['per_page'], $this->uri->segment(5));

			$this->load->view($this->folder . '/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu135') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$isupplier	= $this->uri->segment('4', TRUE);
			$dfrom		= $this->uri->segment('5', TRUE);
			$dto		= $this->uri->segment('6', TRUE);

			if ($isupplier != 'AS') {
				$query = "	select distinct on(z.i_dtap, z.i_supplier) z.* from(
							select distinct on(x.i_dtap, x.i_supplier) x.* from(
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, c.i_nota, a.v_netto, a.v_sisa, c.i_alokasi, c.v_jumlah, d.d_alokasi from tr_supplier b, tm_dtap a
							inner join tm_alokasi_bk_item c on(a.i_dtap = c.i_nota and a.i_supplier = c.i_supplier)
							inner join tm_alokasi_bk d on(c.i_alokasi = d.i_alokasi and c.i_kbank = d.i_kbank and c.i_supplier = d.i_supplier)
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'
							union all
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, c.i_nota, a.v_netto,  a.v_sisa, c.i_alokasi, c.v_jumlah, d.d_alokasi from tr_supplier b, tm_dtap a
							inner join tm_alokasi_kb_item c on(a.i_dtap = c.i_nota and a.i_supplier = c.i_supplier)
							inner join tm_alokasi_kb d on(c.i_alokasi = d.i_alokasi and c.i_kb = d.i_kb and c.i_supplier = d.i_supplier)
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'

							) as x
							union all
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, '' as i_nota, a.v_netto,  a.v_sisa, '' as i_alokasi, 0 as v_jumlah, null as d_alokasi from tr_supplier b, tm_dtap a
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'
							and a.v_sisa > 0
							) as z
							where z.i_supplier = '$isupplier'
							order by z.i_supplier";
			} else {
				$query = "	select distinct on(z.i_dtap, z.i_supplier) z.* from(
							select distinct on(x.i_dtap, x.i_supplier) x.* from(
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, c.i_nota, a.v_netto, a.v_sisa, c.i_alokasi, c.v_jumlah, d.d_alokasi from tr_supplier b, tm_dtap a
							inner join tm_alokasi_bk_item c on(a.i_dtap = c.i_nota and a.i_supplier = c.i_supplier)
							inner join tm_alokasi_bk d on(c.i_alokasi = d.i_alokasi and c.i_kbank = d.i_kbank and c.i_supplier = d.i_supplier)
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'
							union all
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, c.i_nota, a.v_netto,  a.v_sisa, c.i_alokasi, c.v_jumlah, d.d_alokasi from tr_supplier b, tm_dtap a
							inner join tm_alokasi_kb_item c on(a.i_dtap = c.i_nota and a.i_supplier = c.i_supplier)
							inner join tm_alokasi_kb d on(c.i_alokasi = d.i_alokasi and c.i_kb = d.i_kb and c.i_supplier = d.i_supplier)
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'

							) as x
							union all
							select a.i_supplier, a.i_dtap, a.d_dtap, b.e_supplier_name, '' as i_nota, a.v_netto,  a.v_sisa, '' as i_alokasi, 0 as v_jumlah, null as d_alokasi from tr_supplier b, tm_dtap a
							where a.i_supplier = b.i_supplier
							and a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')
							and a.f_dtap_cancel = 'f'
							and a.v_sisa > 0
							) as z
							order by z.i_supplier";
			}

			// $this->db->select($query);
			$query = $this->db->query($query);

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Pelunasan Pembelian")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:K1'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Tgl Nota');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Sisa');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Alokasi');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Tanggal Alokasi');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 2;
				foreach ($query->result() as $row) {
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':L' . $i
					);

					if ($row->d_dtap != '') {
						$tmp = explode('-', $row->d_dtap);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_dtap = $hr . '-' . $bl . '-' . $th;
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_dtap, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->d_dtap, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->v_netto, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_alokasi, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->d_alokasi, Cell_DataType::TYPE_STRING);
					$i++;
				}
				$x = $i - 1;
			}

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

			$nama = 'pelunasanpembelian_' . $dto . '.xls';

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');

			$this->logger->writenew('Export Pelunasan Pembelian Tanggal ' . $dfrom . 's/d ' . $dto);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
