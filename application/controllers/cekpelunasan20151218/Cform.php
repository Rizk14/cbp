<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_pelunasan');
			$data['dfrom']='';
			$data['dto']  ='';
			$data['ipl']  ='';
			$data['idt']  ='';
			$data['iarea']='';
			$data['isi']  ='';
			$this->load->view('cekpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select i_pelunasan from tm_pelunasan a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										              and a.i_area='$iarea' and a.f_pelunasan_cancel='f' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');
			$this->load->model('cekpelunasan/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['idt']	= '';
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpelunasan');
			$this->load->view('cekpelunasan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('cekpelunasan');
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))			
			  )
			{
				$ipl 	= str_replace('tandagaring','/',$this->uri->segment(4));
				$iarea= $this->uri->segment(5);
				$idt 	= str_replace('tandagaring','/',$this->uri->segment(6));
				$dfrom= $this->uri->segment(7);
				$dto 	= $this->uri->segment(8);
				$query 			= $this->db->query("select * from tm_pelunasan_item 
													where i_pelunasan = '$ipl' and i_area = '$iarea' and i_dt='$idt' ");
				$data['jmlitem'] 		= $query->num_rows(); 				
				$data['ipl'] 			= $ipl;
				$data['iarea']			= $iarea;
				$data['idt']			= $idt;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$this->load->model('cekpelunasan/mmaster');
				$data['vsisa']=$this->mmaster->sisa($iarea,$ipl,$idt);
				$data['isi']=$this->mmaster->bacapl($iarea,$ipl,$idt);
				$data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl,$idt);
		 		$this->load->view('cekpelunasan/vmainform',$data);
			}else{
				$this->load->view('cekpelunasan/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/* Disabled 12-03-2011
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ipl		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$idt		= $this->uri->segment(6);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(7);
			if($dto=='') $dto=$this->uri->segment(8);
			$this->load->model('cekpelunasan/mmaster');
			$this->mmaster->delete($ipl,$iarea,$idt);
			$cari		= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');
			$this->load->model('cekpelunasan/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('cekpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	*/
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										and a.i_area='$iarea' and a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' and
										a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');

			$this->load->model('cekpelunasan/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['idt']	= '';
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekpelunasan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekpelunasan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function updatepelunasan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('cekpelunasan/mmaster');
			$ipl 	= $this->input->post('ipelunasan', TRUE);
			$idt 	= $this->input->post('idt', TRUE);
			$ddt 	= $this->input->post('ddt', TRUE);
			$iarea	= $this->input->post('iarea', TRUE);
			$ecek1	= $this->input->post('ecek1',TRUE);
			if($ecek1=='')
				$ecek1=null;
			$user		=$this->session->userdata('user_id');
			$this->mmaster->updatecek($ecek1,$user,$ipl,$idt,$iarea);
########## Posting ###########
      $dbukti			= $this->input->post('ddt', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
      $egirodescription="Pelunasan DT no:".$idt;
			$fclose			= 'f';
			$jml			= $this->input->post('jml', TRUE);
			for($i=1;$i<=$jml;$i++)
			{
			  $inota=$this->input->post('inota'.$i, TRUE);
			  $ireff=$ipl.'|'.$inota; 
        if($i==1){
			    $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dbukti);
			    $this->mmaster->updatepelunasan($ipl,$iarea,$idt);
        }
  			$vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				$vjumlah		= str_replace(',','',$vjumlah);
				$jenis=$this->mmaster->jenisbayar($ipl,$iarea,$idt);
				if($jenis=='06'){
  				$accdebet		= ByPromosi;				
				}elseif($jenis=='07'){
  				$accdebet		= ByExpedisi;				
				}elseif($jenis=='08'){
  				$accdebet		= ByAdmBank;				
				}elseif($jenis=='09'){
  				$accdebet		= ByAdmPenjualan;				
				}elseif($jenis=='10'){
  				$accdebet		= ByPembulatan;				
				}else{
  				$accdebet		= KHP.$iarea;
				}
				$namadebet		= $this->mmaster->namaacc($accdebet);
				$tmp			= $this->mmaster->carisaldo($accdebet,$iperiode);
				if($tmp) 
					$vsaldoaw1		= $tmp->v_saldo_awal;
				else 
					$vsaldoaw1		= 0;
				if($tmp) 
					$vmutasidebet1	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet1	= 0;
				if($tmp) 
					$vmutasikredit1	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit1	= 0;
				if($tmp) 
					$vsaldoak1		= $tmp->v_saldo_akhir;
				else
					$vsaldoak1		= 0;
				
				$acckredit		= PiutangDagang.$iarea;
				$namakredit		= $this->mmaster->namaacc($acckredit);
				$saldoawkredit	= $this->mmaster->carisaldo($acckredit,$iperiode);
				if($tmp) 
					$vsaldoaw2		= $tmp->v_saldo_awal;
				else
					$vsaldoaw2		= 0;
				if($tmp) 
					$vmutasidebet2	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet2	= 0;
				if($tmp) 
					$vmutasikredit2	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit2	= 0;
				if($tmp) 
					$vsaldoak2		= $tmp->v_saldo_akhir;
				else
					$vsaldoak2		= 0;
				$this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlah);
				$this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlah);
				$this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjumlah,$dbukti,$egirodescription);
				$this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjumlah,$dbukti,$egirodescription);
			}
########## End of Posting ###########
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
	        while($row=pg_fetch_assoc($rs)){
		        $ip_address	  = $row['ip_address'];
		        break;
	        }
        }else{
	        $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
	        $now	  = $row['c'];
        }
        $pesan='Cek Pelunasan No:'.$ipl.' Area:'.$iarea;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ipl;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function girocek()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/girocek/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_jenis_bayar",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_girocek');
			$data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5));
			$this->load->view('cekpelunasan/vlistgirocek', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idt 	= $this->uri->segment(4);
			$iarea 	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/customer/'.$idt.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
										where a.i_customer=b.i_customer
										and a.v_sisa>0
										and a.i_dt = '$idt' and a.i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($idt,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['idt']=$idt;
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekpelunasan/vlistnotaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekpelunasan/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function giro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/giro/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_giro 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and (f_giro_tolak='f' and f_giro_batal='f')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->bacagiro($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/kn/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_kn 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and v_sisa>0",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_kn');
			$data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistkn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ku()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/ku/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';

      $query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
      if($group!='G0000'){
			  $query = $this->db->query("	select a.* from tm_kum a, tr_customer b
										  where b.i_customer_group='$group' and a.i_customer=b.i_customer
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
			}else{
        $query = $this->db->query("	select a.* from tm_kum a
										  where a.i_customer='$icustomer'
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_ku');
      if($group!='G0000'){
  			$data['isi']=$this->mmaster->bacaku($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }else{
  			$data['isi']=$this->mmaster->bacaku2($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistku', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function lebihbayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekpelunasan/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	SELECT 	i_dt, min(v_jumlah), min(v_lebih), i_area,
											d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2))
										from tm_pelunasan_lebih
										where i_customer = '$icustomer'
										and i_area='$iarea'
										and v_lebih>0
										group by i_dt, d_bukti, i_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_pelunasan');
			$data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekpelunasan/vlistpelunasan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
