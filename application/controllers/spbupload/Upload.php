<?php 

class Upload extends CI_Controller {
	
	function Upload()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	function index()
	{	
		if ($this->session->userdata('logged_in')){
			$this->load->view('spbupload/upload_form', array('error' => ' ' ));
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function spb_upload()
	{
		if ($this->session->userdata('logged_in')){
			$config['upload_path'] = './spblama/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= 'TRUE';
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
      $bener=false;
      for($no=1;$no<=5;$no++){
        $field='userfile'.$no;
			  if ( ! $this->upload->do_upload($field) && $no==1)
			  {
				  $error = array('error' => $this->upload->display_errors());
          $bener=false;
				  $this->load->view('spbupload/upload_form', $error);
			  }	
			  else
			  {
          $bener=true;
			  }
      }
      if($bener){
			  $data = array('upload_data' => $this->upload->data());
			  $this->load->view('spbupload/upload_success', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
