<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
		$this->load->library('pagination');
	}
	function index()
	{
	
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu256')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$data['page_title'] = $this->lang->line('tspmb');
			$data['isi']= '';#directory_map('./data/');
			$data['file']='';
			$this->load->view('tspmb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu256')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $dfrom= $this->input->post('dfrom', TRUE);
			  $dto= $this->input->post('dto', TRUE);
			  $iarea= $this->input->post('iarea', TRUE);
#			  echo $dfrom.' - '.$dto.' - '.$iarea;die;
			  $data['dfrom']=$dfrom;
			  $data['dto']=$dto;
			  $data['iarea']=$iarea;
			  $this->load->view('tspmb/vfile', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu256')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/sj/cform/area/index/';
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($iuser,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu256')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/sj/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('sj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer(){
		if (
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('menu256')=='t')) ||
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('tspmb/mmaster');
			$jml= $this->input->post('jml', TRUE);
			$adaheader='';
			$spmbclose='';
			$adadetail='';
			$data['jml']	= $jml;
			$this->db->trans_begin();
			$ispmbx 		='';
			$iareax			='';
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			
			for($i=0;$i<$jml;$i++){
			  $j=1;
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
				$ispmb   		= $this->input->post('ispmb'.$i, FALSE);
				$iarea   		= $this->input->post('iarea'.$i, FALSE);
				$dspmb			= $this->input->post('dspmb'.$i, TRUE);
					if($dspmb!=''){
						$tmp=explode("-",$dspmb);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dspmb=$th."-".$bl."-".$hr;
						$thbl=substr($th,2,2).$bl;
					}
				$iproduct  		    = $this->input->post('iproduct'.$i, FALSE);
				$iproductmotif    = $this->input->post('iproductmotif'.$i, FALSE);
				$istore   		    = $this->input->post('istore'.$i, FALSE);
				$istorelocation	  = $this->input->post('istorelocation'.$i, FALSE);
				$iapprove2 		    = $this->input->post('iapprove2'.$i, FALSE);
				$dapprove2		    = $this->input->post('dapprove2'.$i, FALSE);
				$eremark		      = $this->input->post('eremark'.$i, FALSE);
				$vunitprice 	    = $this->input->post('vunitprice'.$i, FALSE);
				$nsaldo			      = $this->input->post('nsaldo'.$i, FALSE);
				$nacc 			      = $this->input->post('nacc'.$i, FALSE);
				$norder			      = $this->input->post('norder'.$i, FALSE);			
				$item			        = $this->input->post('item'.$i, FALSE);
				
				if($iproductmotif=='STD'){$iproductmotif='00';}
				
				$this->db->select("	* from tm_spmb where i_spmb='$ispmb' 
								 and i_area='$iarea'", false);
				$query = $this->db->get();
					if ($query->num_rows() > 0){
						foreach($query->result() as $ada)
						{
							$spmbclose      = $ada->f_spmb_close;
							$spmbpemenuhan  = $ada->f_spmb_pemenuhan;
							if($spmbclose=='f' && $spmbpemenuhan=='f'){
								$this->mmaster->updateheaderspmb($ispmb,$iarea,$now);
							}else{
								$data['error']='SPMB Sudah di Di Realisasi';
								$this->db->trans_rollback();
								$this->load->view('tspmb/vformgagal',$data);
							}
						}
					}else{
						$this->mmaster->insertheaderspmb($ispmb, $dspmb, $istore, $istorelocation, $iarea, $iapprove2, $dapprove2, $eremark, $now);
					}
					$ispmbx = $ispmb;
					$iareax	= $iarea;

				$this->db->select("	e_product_name from tr_product where i_product='$iproduct'", false);
				$query = $this->db->get();
					if ($query->num_rows() > 0){
						foreach($query->result() as $ada)
						{
							$eproduct =$ada->e_product_name;
						}
					}
				$this->mmaster->inserttempspmb($ispmb,$iproduct,$iproductmotif,$iarea);
				$this->mmaster->insertdetailspmb($eproduct,$ispmb,$iproduct,$iproductmotif,$iarea,$nsaldo,$nacc,$norder,$vunitprice,$j,$eremark);
				$j++;
				}
			}
			$this->db->trans_commit();
#			$this->db->trans_rollback();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke SPMB Berhasil';#Periode:'.$iperiode;
			$this->load->model('logger');
			$data['sukses']='true';
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['inomor']			= 'Transfer SPMB sukses ...';
			$this->load->view('nomor',$data);
		}else{
					$this->load->view('awal/index.php');
		}	
	}
}
?>
