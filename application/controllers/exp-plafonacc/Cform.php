<?php
class Cform extends CI_Controller
{
	public $title 	= "Export Plafon (Acc)";
	public $folder 	= "exp-plafonacc";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->dbutil();
		$this->load->helper('file');
		require_once("php/fungsi.php");
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu486') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data = [
				'page_title'	=> $this->title,
				'folder'		=> $this->folder,
				'iperiodeawal'	=> '',
				'iperiodeakhir'	=> '',
			];

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu497') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iperiodeawal  	= $this->uri->segment('4');
			$a 				= substr($iperiodeawal, 2, 2);
			$b 				= substr($iperiodeawal, 4, 2);
			$periodeawal 	= mbulan($b) . $a;

			$iperiodeakhir	= $this->uri->segment('5');
			$a 				= substr($iperiodeakhir, 2, 2);
			$b 				= substr($iperiodeakhir, 4, 2);
			$periodeakhir 	= mbulan($b) . $a;

			#			$isi		            = $this->mmaster->bacaperiode($iperiode);
			$isi = $this->mmaster->bacano($iperiodeawal, $iperiodeakhir);

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Plafon Acc " . $periodeawal . " - " . $periodeakhir)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($isi) {
				#if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:M1'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'CUSTOMER');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'TOP');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'KATEGORI');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'INDEX');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'RATA TELAT');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TOTAL PENJUALAN');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'MAX PENJUALAN');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'RATA PENJUALAN');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'PLAFOND SEBELUMNYA');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'PLAFON PROGRAM');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'PLAFON ACC');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i = 2;
				$cell = 'B';
				foreach ($isi as $row) {
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',

								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),

						'A' . $i . ':M' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_area . ' - ' . $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_customer_groupbayar . ' - ' . $row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->n_customer_toplength, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_kategori . ' - ' . $row->e_kategori, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_index, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->n_rata_telat, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->v_total_penjualan, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->v_max_penjualan, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->v_rata_penjualan, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->v_plafond_program, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->v_plafond_before, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->v_plafond_before, Cell_DataType::TYPE_NUMERIC);

					$i++;
				}

				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

				$nama = 'Plafon_Acc(' . $periodeawal . '_sd_' . $periodeakhir . ').xls';
				if (file_exists('excel/' . $nama)) {
					@chmod('excel/' . $nama, 0777);
					@unlink('excel/' . $nama);
				}
				$objWriter->save('excel/00/' . $nama);

				$this->logger->writenew('Export Plafon Acc ' . $periodeawal . ' - ' . $periodeakhir);

				// Proses file excel    
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
				header('Cache-Control: max-age=0');

				$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output', 'w');
			} else {
				$data['page_title'] = $this->title;
				$this->load->view($this->folder . '/vformfail', $data);
			}
		}
	}
}
