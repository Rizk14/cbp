<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    public function delete($ispmb) 
    {
		$this->db->query("update tm_spmb set f_spmb_cancel='t' WHERE i_spmb='$ispmb'");
		return TRUE;
    }
    function bacasemua($cari, $num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($this->session->userdata('level')=='0'){
		$this->db->select(" a.*, b.e_area_store_name as e_area_name from tm_spmb a, tr_store b
							          where a.i_area=b.i_store and a.f_spmb_cancel='f'
							          and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
							          or upper(a.i_spmb) like '%$cari%')
							          order by a.i_spmb desc",false)->limit($num,$offset);
		}else{
		$this->db->select(" 	a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%') order by a.i_spmb desc",false)->limit($num,$offset);
//		and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
    function cari($cari,$num,$offset)
    {
		$this->db->select(" a.*, b.e_store_name as e_area_name from tm_spmb a, tr_store b
					where a.i_area=b.i_store and a.f_spmb_cancel='f'
					and (upper(a.i_area) like '%$cari%' or upper(b.e_store_name) like '%$cari%'
					or upper(a.i_spmb) like '%$cari%' or upper(a.i_spmb_old) like '%$cari%')
					order by a.i_spmb desc",FALSE)->limit($num,$offset);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function bacaarea($num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name  as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          group by a.i_store, b.e_store_name
                          order by a.i_store", false)->limit($num,$offset);
		}else{
			$this->db->select(" distinct on (a.i_store) a.i_store as i_area, b.e_store_name as e_area_name
                          from tr_area a, tr_store b 
                          where a.i_store=b.i_store 
                          and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                          or a.i_area = '$area4' or a.i_area = '$area5')
                          group by a.i_store, b.e_store_name order by a.i_store", false)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
	function cariarea($cari,$num,$offset,$area1,$area2,$area3,$area4,$area5)
    {
		if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b 
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
                 and a.i_store=b.i_store
							   order by a.i_store ", FALSE)->limit($num,$offset);
		}else{
			$this->db->select("distinct on (a.i_store) a.i_store, a.i_area, b.e_store_name as e_area_name
                 from tr_area a, tr_store b
                 where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
							   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							   or i_area = '$area4' or i_area = '$area5') order by a.i_store ", FALSE)->limit($num,$offset);
		}
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}
    }
#    function bacaperiode($iarea,$dfrom,$dto,$num,$offset,$cari)
    function bacaperiode($iperiodeawal,$iperiodeakhir)
    {
		$query = $this->db->query("select distinct on(i_customer_groupbayar) data.*, case when v_ratapenjualan isnull then 0 else 
		                            cast(n_index*v_ratapenjualan as integer) end as v_plafon,
                                v_flapond as v_plafonsblmnya, '$iperiodeawal' as periodeawal, '$iperiodeakhir' as periodeakhir        
                                from
                                (
                                    select a.i_area, area.e_area_name, a.i_customer, b.i_customer_groupbayar, 
                                    e_customer_name, e_customer_address, e_customer_city, to_char(d_signin,'dd-mm-yyyy') as d_signin, 
                                    n_customer_toplength, 
                                      case when n_ratatelat isnull then '99-Toko Baru'
                                        else 
                                          case when n_ratatelat <=7 then '01-Baik Sekali'
                                               when n_ratatelat <=14 then '02-Baik'
                                               when n_ratatelat <=21 then '03-Cukup' 
                                               when n_ratatelat <=30 then '04-Calon BL'
                                          else '05-Black List' end                                     
                                      end as e_kategori,
                                      case when n_ratatelat isnull then
                                        0
                                        else
                                        case 
                                          when n_ratatelat <=7 then 2
                                          when n_ratatelat <=14 then 1.5
                                          when n_ratatelat <=21 then 1                                    
                                          when n_ratatelat <=30 then 0.5
                                        else 0 end 
                                      end as n_index,
                                    n_ratatelat, 
                                      case when v_totalpenjualan is null then 0 
                                           when v_totalpenjualan is not null then v_totalpenjualan end
                                    v_totalpenjualan, 
                                      case when v_maxpenjualan is null then 0 
                                           when v_maxpenjualan is not null then v_maxpenjualan end
                                    v_maxpenjualan, 
                                      case when v_ratapenjualan is null then 0 
                                      when v_ratapenjualan is not null then v_ratapenjualan end
                                    v_ratapenjualan
                                    from tr_customer a left join tr_area area on (area.i_area=a.i_area)
                                    left join tr_customer_groupbayar b on (b.i_customer=a.i_customer)
                                    left join f_keterlambatan_pelunasan_piutang_pergroupbayar('$iperiodeawal','$iperiodeakhir') c on
                                    (c.i_customer_groupbayar=b.i_customer_groupbayar)
                                    left join f_penjualan_pergroupbayar('$iperiodeawal','$iperiodeakhir') d on (d.i_customer_groupbayar=b.i_customer_groupbayar)
                                    order by a.i_area, e_customer_name, a.i_customer
                                ) as data
                                left join tr_customer_groupar b on (b.i_customer=data.i_customer)
                                where n_index=2
                                order by i_customer_groupbayar, e_customer_name
                        ");
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
 	function bacano($iperiodeawal, $iperiodeakhir)
  {
    $query = $this->db->query("select a.e_area_name, b.i_customer_groupbayar, c.e_customer_name , b.e_periode_awal, b.e_periode_akhir, b.i_kategori, b.e_kategori,
                        b.n_rata_telat, b.i_index, b.v_total_penjualan, b.v_max_penjualan, b.v_rata_penjualan, b.v_plafond as v_plafond_program,
                        b.v_plafond_before, b.v_plafond_acc, c.n_customer_toplength, c.i_area
                        from tm_plafond b, tr_area a, tr_customer c
                        where a.i_area = b.i_area and b.e_periode_awal ='$iperiodeawal' and b.e_periode_akhir ='$iperiodeakhir'
                        and c.i_customer= b.i_customer_groupbayar",false);
    if ($query->num_rows() > 0){
		  return $query->result();
	  }
  }
 	function bacaplafonacc($iperiodeawal,$iperiodeakhir)
  {
    $this->db->select("a.e_area_name, b.i_customer_groupbayar, b.e_periode_awal, b.e_periode_akhir, b.i_kategori, b.e_kategori, b.n_rata_telat, 
                       b.i_index, b.v_total_penjualan, b.v_max_penjualan, b.v_rata_penjualan, b.v_plafond, b.v_plafond_before, b.v_plafond_acc 
                       from tm_plafond b, tr_area a where b.i_acc is not null and a.i_area = b.i_area and b.e_periode_awal ='$iperiodeawal' 
                       and b.e_periode_akhir ='$iperiodeakhir'",false);
    $tes=$this->db->get();
    return $tes;
  }
}
?>
