<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		)
		{
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/index/';
			$cari 				= strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$data['cari']		= '';
     		$data['dfrom']		= '';
			$data['dto']  		= '';
			#$data['iarea'] 		= '';
			$data['isi']  		= '';
			
			$this->load->view('printsjkkhusus/vmainform', $data);
		}else{
				$this->load->view('awal/index.php');
			}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/area/index/';
      		$iuser   	= $this->session->userdata('user_id');
      		
      		$query 		= $this->db->query("select * from tr_area where i_area in ( select i_area from tm_user_area 
      										where i_user='$iuser') 
      										order by i_area", false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printsjkkhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		= $this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			
			$this->load->view('printsjkkhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser  = $this->session->userdata('user_id');
			
			$query 	= $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjkkhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			
			$this->load->view('printsjkkhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			
			$iuser  				= $this->session->userdata('user_id');
			$dfrom					= $this->input->post('dfrom');
			$dto					= $this->input->post('dto');
      		#$iarea					= $this->input->post('iarea');
      		if($dfrom=='')$dfrom	= $this->uri->segment(4);
      		if($dto=='')$dto		= $this->uri->segment(5);
      		#if($iarea=='')$iarea 	= $this->uri->segment(6);
			$config['base_url'] 	= base_url().'index.php/printsjkkhusus/cform/view/'.$dfrom.'/'.$dto.'/';
			#$config['base_url'] 	= base_url().'index.php/printsjkkhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$cari 					= strtoupper($this->input->post('cari', FALSE));
		
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  							where 
			  							a.i_customer=b.i_customer 
			  							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_sjk) like '%$cari%') 
										and a.i_area=c.i_area 
										and not a.i_sjk isnull
										and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
										and a.d_sj_receive >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_sj_receive <= to_date('$dto','dd-mm-yyyy')
										order by a.i_sjk ",false);
		    /*if($iarea!='PB')
		    	{
			  		$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  							where 
			  							a.i_customer=b.i_customer 
			  							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_sj) like '%$cari%') 
										and substring(a.i_sj,9,2)=c.i_area 
										and substring(a.i_sj,9,2)='$iarea'
										and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
										and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_sj <= to_date('$dto','dd-mm-yyyy')
										order by i_sj desc",false);
      			}else{
			  		$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										where 
										a.i_customer=b.i_customer 
										and substring(a.i_sj,9,2)=c.i_area 
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_sj) like '%$cari%') 
										and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
                                    	and substring(a.i_sj,9,2)='BK'
										and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_sj <= to_date('$dto','dd-mm-yyyy')
										order by i_sj desc",false);
      				}*/

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->lang->line('printsj').' Komersial (Khusus)';
			$this->load->model('printsjkkhusus/mmaster');
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			#$data['iarea']			= $iarea;
			$data['isi']			= $this->mmaster->bacasemua($iuser,$cari,$config['per_page'],$this->uri->segment(6),$dfrom,$dto);

			$this->load->view('printsjkkhusus/vmainform', $data);
		}else{
				$this->load->view('awal/index.php');
			}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$isj  				= $this->uri->segment(4);
			#$iarea 				= substr($isj,8,2);
			$this->load->model('printsjkkhusus/mmaster');
			$data['isj']		= $isj;
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			#$data['isi']		= $this->mmaster->baca($isj,$iarea);
			$data['isi']		= $this->mmaster->baca($isj);
			$data['detail'] 	= $this->mmaster->bacadetail($isj);
			#$data['detail'] 	= $this->mmaster->bacadetail($isj,$iarea);
      		$this->mmaster->updatesj($isj);
      		#$this->mmaster->updatesj($isj,$iarea);
			
			$sess 	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			
			if($rs->num_rows>0)
				{
					foreach($rs->result() as $tes)
						{
							$ip_address	  = $tes->ip_address;
							break;
						}
				}else{
						$ip_address='kosong';
					}
			
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			#$data['iarea']	= $iarea;
			$query 			= pg_query("SELECT current_timestamp as c");
			while($row = pg_fetch_assoc($query))
				{
					$now	= $row['c'];
				}
			$pesan			= 'Cetak SJ Komersial Khusus No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjkkhusus/vformrptcab',$data);
		}else{
				$this->load->view('awal/index.php');
			}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$this->load->view('printsjkkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$cari	= strtoupper($this->input->post('cari', FALSE));
			$dfrom	= strtoupper($this->input->post('dfrom', FALSE));
			$dto 	= strtoupper($this->input->post('dto', FALSE));
     		$area	= $this->session->userdata('i_area');
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iuser 	= $this->session->userdata('user_id');
      
      		if($dfrom=='')$dfrom=$this->uri->segment(4);
      		if($dto=='')$dto=$this->uri->segment(5);
      		if($cari=='')$cari=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$cari.'/';
      		
      		$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
			  							where 
			  							a.i_customer=b.i_customer 
			  							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_sjk) like '%$cari%') 
										and a.i_area=c.i_area 
										and not a.i_sjk isnull
										and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
										and a.d_sj_receive >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_sj_receive <= to_date('$dto','dd-mm-yyyy')
										order by a.i_sjk ",false);
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			
			$this->load->model('printsjkkhusus/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(7),$area,$iuser,$dfrom,$dto);
			$data['page_title'] = $this->lang->line('printsj').' Komersial(Tertentu)';
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isj']		= '';
			$data['detail']		= '';

	 		$this->load->view('printsjkkhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

#	function cari()
	
/*DICOMMENT TANGGAL 15-07-2017*/
/*function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea1	= $this->session->userdata('i_area');
			$iarea2	= $this->session->userdata('i_area2');
			$iarea3	= $this->session->userdata('i_area3');
			$iarea4	= $this->session->userdata('i_area4');
			$iarea5	= $this->session->userdata('i_area5');
			$iuser   = $this->session->userdata('user_id');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/view/'.$dfrom.'/'.$dto.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      if($iarea1!='PB'){
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area 
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2)='$iarea2'
										                or substring(a.i_sj,9,2)='$iarea3' or substring(a.i_sj,9,2)='$iarea4' or substring(a.i_sj,9,2)='$iarea5')
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area 
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2)='$iarea2'
										                or substring(a.i_sj,9,2)='$iarea3' or substring(a.i_sj,9,2)='$iarea4' or substring(a.i_sj,9,2)='$iarea5'
                                    or substring(a.i_sj,9,2)='BK')
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$this->load->model('printsjkkhusus/mmaster');
			$data['cari']=$cari;
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(6),$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$dfrom,$dto);
			$this->load->view('printsjkkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isj  = $this->uri->segment(4);
			$iarea = substr($isj,8,2);
			$this->load->model('printsjkkhusus/mmaster');
			$data['isj']=$isj;
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$data['isi']=$this->mmaster->baca($isj,$iarea);
			$data['detail'] = $this->mmaster->bacadetail($isj,$iarea);
      $this->mmaster->updatesj($isj,$iarea);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $iarea;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJ Area:'.$iarea.' No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			
			$this->load->view('printsjkkhusus/vformrptcab',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$this->load->view('printsjkkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu186')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom= strtoupper($this->input->post('dfrom', FALSE));
			$dto 	= strtoupper($this->input->post('dto', FALSE));
      $iarea1	= $this->session->userdata('i_area');
			$iarea2	= $this->session->userdata('i_area2');
			$iarea3	= $this->session->userdata('i_area3');
			$iarea4	= $this->session->userdata('i_area4');
			$iarea5	= $this->session->userdata('i_area5');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/printsjkkhusus/cform/view/'.$dfrom.'/'.$dto.'/';
      if($iarea1!='PB'){
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2)='$iarea2'
										                or substring(a.i_sj,9,2) = '$iarea3' or substring(a.i_sj,9,2) = '$iarea4' or substring(a.i_sj,9,2)='$iarea5')
                                    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$iarea1' or substring(a.i_sj,9,2)='$iarea2'
										                or substring(a.i_sj,9,2) = '$iarea3' or substring(a.i_sj,9,2) = '$iarea4' or substring(a.i_sj,9,2)='$iarea5'
                                    or substring(a.i_sj,9,2)='BK')
                                    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printsjkkhusus/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$dfrom,$dto);
			$data['page_title'] = $this->lang->line('printsj').' Komersial (Khusus)';
			$data['cari']=$cari;
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['isj']='';
			$data['detail']='';
	 		$this->load->view('printsjkkhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	*/
}
?>
