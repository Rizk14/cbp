<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('paginationxx');
//		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu46')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer				      = $this->input->post('icustomer', TRUE);
			$icustomerplugroup		  = $this->input->post('icustomerplugroup', TRUE);
			$icity					        = $this->input->post('icity', TRUE);
			$icustomergroup   			= $this->input->post('icustomergroup', TRUE);
			$ipricegroup			      = $this->input->post('ipricegroup', TRUE);
			$iarea        					= $this->input->post('iarea', TRUE);
			$icustomerstatus    		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype 	= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			    = $this->input->post('icustomergrade', TRUE);
			$icustomerservice		    = $this->input->post('icustomerservice', TRUE);
			$icustomersalestype  		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass   			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus	    	= $this->input->post('icustomerstatus', TRUE);
			$ecustomername	    		= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		    = $this->input->post('ecustomeraddress', TRUE);
			$ecityname				      = $this->input->post('ecityname', TRUE);
			$ecustomerpostal    		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			    = $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax		      	= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			    = $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress	  = $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		    = $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		    = $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority	  	= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		    = $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade	= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference	  = $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier	= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn	    	= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount	= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp     			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif		    	= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax	      		= $this->input->post('fcustomertax', TRUE);
			$ecustomerretensi	    	= $this->input->post('ecustomerretensi', TRUE);
			$ncustomertoplength	  	= $this->input->post('ncustomertoplength', TRUE);
			if($ncustomertoplength=='')
				$ncustomertoplength=0;
			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $ecustomername != ''))
			{
				$this->load->model('customerapprove/mmaster');
				$this->mmaster->insert
					(
					$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
					$iarea, $icustomerproducttype, $icustomerspecialproduct, 
					$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
					$icustomerstatus, $ecustomername, $ecustomeraddress,$ecityname, 
					$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
					$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
					$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
					$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
					$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
					$fcustomertax, $ecustomerretensi, $ncustomertoplength
					);

				$sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Master Approval Pelanggan:'.$icustomer;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now, $pesan);

			    $config['base_url'] = base_url().'index.php/customerapprove/cform/index/';
				$sql= " SELECT 
							*
						FROM
							tr_customer_tmp a,
							tr_area c,
							tm_spb d
						WHERE
							a.i_area = c.i_area
							AND a.f_approve = 'f'
							AND a.i_spb = d.i_spb
							AND a.i_area = d.i_area
							AND NOT d.i_cek ISNULL
							AND a.i_approve ISNULL
							AND d.f_spb_cancel = 'f'
						ORDER BY
							d.d_spb DESC,
							a.i_area,
							a.i_spb ";
				$query 	= $this->db->query($sql,false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('approve_customer');
				$data['ispb'] = '';
				$cari = $this->input->post('cari', FALSE);
				$this->load->model('customerapprove/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customerapprove/vmainform', $data);
			}
			else
			{
				/*$cari =strtoupper($this->input->post('cari'));*/
				$config['base_url'] = base_url().'index.php/customerapprove/cform/index/';
				$sql= " select a.i_spb from tr_customer_tmp a, tr_area c, tm_spb d
				        where a.i_area=c.i_area and a.f_approve='f' and a.i_spb=d.i_spb and a.i_area=d.i_area and not d.i_cek isnull";
				$query 	= $this->db->query($sql,false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('approve_customer');
				$data['ispb'] = '';
				$cari = $this->input->post('cari', FALSE);
				/*$cari = strtoupper($cari);*/
				$this->load->model('customerapprove/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

				$sess	= $this->session->userdata('session_id');
				$id 	= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan="Membuka Menu Approval Pelanggan";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customerapprove/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu46')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('approve_customer');
			$this->load->view('customerapprove/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu46')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('approve_customer');
			if($this->uri->segment(4)){
				$ispb	 = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$ipricegroup = $this->uri->segment(6);
				$data['ispb'] = $ispb;
				$data['iarea']= $iarea;
				$data['ipricegroup']= $ipricegroup;
				$this->load->model('customerapprove/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['isispb']=$this->mmaster->bacaspb($ispb,$iarea);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']= $query->num_rows();
				$data['isidetail']=$this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);
				$qnilaiorderspb	= $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
				if($qnilaiorderspb->num_rows()>0){
					$row_nilaiorderspb	= $qnilaiorderspb->row();
					$data['nilaiorderspb']	= $row_nilaiorderspb->nilaiorderspb;
				}else{
					$data['nilaiorderspb']	= 0;
				}

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Approval Pelanggan SPB area:'.$iarea.' no:'.$ispb;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('customerapprove/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu46')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb			= $this->input->post('ispb', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$eapprove	= $this->input->post('eapprove', TRUE);
			$iapprove	= $this->session->userdata('user_id');
			$fapprove	= $this->input->post('chkapprove', TRUE);
      if($fapprove!='')
        $fapprove='t';
      else
        $fapprove='f';
			$fparkir							= $this->input->post('chkparkir', TRUE);
			if($fparkir!='')
				$fparkir			= 't';
			else
				$fparkir			= 'f';
			$fkuli								= $this->input->post('chkkuli', TRUE);
			if($fkuli!='')
				$fkuli			= 't';
			else
				$fkuli			= 'f';
			$fkontrabon			    	= $this->input->post('chkkontrabon', TRUE);
			if($fkontrabon!='')
				$fkontrabon			= 't';
			else
				$fkontrabon			= 'f';
			$this->load->model('customerapprove/mmaster');
			$this->mmaster->update($ispb, $iarea, $iapprove, $eapprove, $fapprove, $fparkir, $fkuli, $fkontrabon);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Approve Sales Customer SPB area:'.$iarea.' no:'.$ispb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $ispb;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu46')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerapprove/cform/index/';
      $cari =($this->input->post('cari'));
  		$query=$this->db->query("	select * from tr_customer_tmp a, tr_area c, tm_spb d
									where a.i_area=c.i_area and a.f_approve='f' and a.i_spb=d.i_spb and a.i_area=d.i_area and not d.i_cek isnull
                        			and (a.i_spb ilike '%$cari%' or a.e_customer_name ilike '%$cari%')
									order by a.i_area, a.i_spb", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('customerapprove/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('approve_customer');
			$data['icustomer']='';
			$data['ispb'] = '';
	 		$this->load->view('customerapprove/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
