<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/salesman/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query(" select a.*, b.e_area_name from tr_salesman a, tr_area b order by a.i_salesman asc ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_salesman');
			$data['isalesman'] = '';
			$this->load->model('salesman/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = "Membuka Menu Salesman";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('salesman/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$isalesman 	 	= $this->input->post('isalesman');
			$esalesmanname 	 	= $this->input->post('esalesmanname');
			$iarea 		 	= $this->input->post('iarea');
			$esalesmanaddress	= $this->input->post('esalesmanaddress');
			$esalesmancity 	 	= $this->input->post('esalesmancity');
			$esalesmanpostal 	= $this->input->post('esalesmanpostal');
			$esalesmanphone	 	= $this->input->post('esalesmanphone');
			$fsalesmanaktif	   	= $this->input->post('fsalesmanaktif');
			if ((isset($isalesman) && $isalesman != '') && (isset($esalesmanname) && $esalesmanname != '') && (isset($iarea) && $iarea != '')) {
				$this->load->model('salesman/mmaster');
				$this->mmaster->insert(
					$isalesman,
					$esalesmanname,
					$iarea,
					$esalesmanaddress,
					$esalesmancity,
					$esalesmanpostal,
					$esalesmanphone,
					$fsalesmanaktif
				);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Input Salesman:(' . $isalesman . ') -' . $esalesmanname;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$config['base_url'] = base_url() . 'index.php/salesman/cform/index/';
				$query = $this->db->query(" select a.*, b.e_area_name from tr_salesman a, tr_area b order by a.i_salesman asc ", false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_salesman');
				$data['isalesman'] = '';
				$this->load->model('salesman/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
				$this->load->view('salesman/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_salesman');
			$this->load->view('salesman/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_salesman') . " update";
			if ($this->uri->segment(4)) {
				$isalesman = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$data['isalesman'] = $isalesman;
				$data['iarea'] = $iarea;
				$this->load->model('salesman/mmaster');
				$data['isi'] = $this->mmaster->baca($isalesman, $iarea);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Membuka Menu Edit Salesman:(' . $isalesman . ')';
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('salesman/vmainform', $data);
			} else {
				$this->load->view('salesman/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$isalesman 	 	= $this->input->post('isalesman');
			$esalesmanname 	 	= $this->input->post('esalesmanname');
			$iarea 		 	= $this->input->post('iarea');
			$esalesmanaddress	= $this->input->post('esalesmanaddress');
			$esalesmancity 	 	= $this->input->post('esalesmancity');
			$esalesmanpostal 	= $this->input->post('esalesmanpostal');
			$esalesmanphone	 	= $this->input->post('esalesmanphone');
			$fsalesmanaktif	   	= $this->input->post('fsalesmanaktif');
			if ($fsalesmanaktif != 'on') {
				$fsalesmanaktif = 'off';
			}
			$this->load->model('salesman/mmaster');
			$this->mmaster->update(
				$isalesman,
				$esalesmanname,
				$iarea,
				$esalesmanaddress,
				$esalesmancity,
				$esalesmanpostal,
				$esalesmanphone,
				$fsalesmanaktif
			);

			$this->logger->writenew('Update Salesman:(' . $isalesman . ') -' . $esalesmanname);

			$config['base_url'] = base_url() . 'index.php/salesman/cform/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tr_salesman a, tr_area b order by a.i_salesman asc ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_salesman');
			$data['isalesman'] = '';
			$this->load->model('salesman/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));

			$this->load->view('salesman/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$isalesman = $this->uri->segment(4);
			$this->load->model('salesman/mmaster');
			$this->mmaster->delete($isalesman);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Delete Salesman:' . $isalesman;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/salesman/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_salesman
						   where upper(e_salesman_name) like '%$cari%' 
						   or upper(i_salesman) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('salesman/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_salesman');
			$data['isalesman'] = '';
			$this->load->view('salesman/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php/salesman/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/salesman/cform/area/' . $baris . '/sikasep/';

			$query = $this->db->query(" select * from tr_area order by i_area asc ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = '';
			$this->load->model('salesman/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(6));
			$this->load->view('salesman/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php/salesman/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php/salesman/cform/cariarea/' . $baris . '/' . $cari . '/';
			else
				$config['base_url'] = base_url() . 'index.php/salesman/cform/cariarea/' . $baris . '/sikasep/';

			$query = $this->db->query("select * from tr_area
						   where upper(e_area_name) ilike '%$cari%' 
						   or upper(i_area) ilike '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$this->load->model('salesman/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(6));
			$this->load->view('salesman/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu20') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php/salesman/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$cari = ($this->input->post("cari", false));
			if ($cari == '') $cari = $this->uri->segment(4);
			if ($cari == 'zxvf') {
				$cari = '';
				$config['base_url'] = base_url() . 'index.php/salesman/cform/cari/zxvf/';
			} else {
				$config['base_url'] = base_url() . 'index.php/salesman/cform/cari/' . $cari . '/';
			}

			$query = $this->db->query("select a.*, b.e_area_name 
									from tr_salesman a, tr_area b 
									where (upper(a.e_salesman_name) ilike '%$cari%' 
									or upper(a.i_salesman) ilike '%$cari%' or upper(b.e_area_name) 
									ilike '%$cari%') and a.i_area=b.i_area order by a.i_salesman ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('salesman/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_salesman');
			$data['isalesman'] = '';
			$this->load->view('salesman/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
