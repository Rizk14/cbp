<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plu');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerplu');
			$data['icustomerplugroup']='';
			$data['icustomerplu']='';
			$data['iproduct']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('customerplu/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master PLU";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerplu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerplu 		= $this->input->post('icustomerplu', TRUE);
			$icustomerplugroup 	= $this->input->post('icustomerplugroup', TRUE);
			$iproduct	 	= $this->input->post('iproduct', TRUE);
			if (($icustomerplu != '') && ($icustomerplugroup != '') && ($iproduct != ''))
			{
				$this->load->model('customerplu/mmaster');
				$this->mmaster->insert($icustomerplugroup,$icustomerplu,$iproduct);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master PLU:('.$icustomerplu.') -'.$icustomerplugroup;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $config['base_url'] = base_url().'index.php/customerplu/cform/index/';
				$config['total_rows'] = $this->db->count_all('tr_customer_plu');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_customerplu');
				$data['icustomerplugroup']='';
				$data['icustomerplu']='';
				$data['iproduct']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('customerplu/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customerplu/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerplu');
			$this->load->view('customerplu/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerplu')." update";
			if($this->uri->segment(4) && $this->uri->segment(5)){
				$icustomerplugroup = $this->uri->segment(4);
				$icustomerplu = $this->uri->segment(5);
				$data['icustomerplugroup'] = $icustomerplugroup;
				$data['icustomerplu'] = $icustomerplu;
				$this->load->model('customerplu/mmaster');
				$data['isi']=$this->mmaster->baca($icustomerplugroup,$icustomerplu);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master PLU:('.$icustomerplugroup.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		 		$this->load->view('customerplu/vmainform',$data);
			}else{
				$this->load->view('customerplu/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerplugroup	= $this->input->post('icustomerplugroup', TRUE);
			$icustomerplu	 	= $this->input->post('icustomerplu', TRUE);
			$iproduct 		= $this->input->post('iproduct', TRUE);
			$fcustomerpluaktif	= $this->input->post('fcustomerpluaktif', TRUE);
			$this->load->model('customerplu/mmaster');
			$this->mmaster->update($icustomerplugroup,$icustomerplu,$iproduct,$fcustomerpluaktif);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master PLU:('.$icustomerplugroup.') -'.$icustomerplu;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plu');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerplu');
			$data['icustomerplugroup']='';
			$data['icustomerplu']='';
			$data['iproduct']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('customerplu/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerplu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerplugroup	= $this->uri->segment(4);
			$icustomerplu		= $this->uri->segment(5);
			$this->load->model('customerplu/mmaster');
			$this->mmaster->delete($icustomerplugroup,$icustomerplu);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master PLU:('.$icustomerplugroup.') -'.$icustomerplu;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plu');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerplu');
			$data['icustomerplugroup']='';
			$data['icustomerplu']='';
			$data['iproduct']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerplu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu40')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plu');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('customerplu/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_customerplu');
			$data['icustomerplu']='';
			$data['icustomerplugroup']='';
	 		$this->load->view('customerplu/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function plugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu39')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('customerplu/mmaster');
			$config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$query = $this->db->query("	select i_customer, e_customer_plugroupname, i_customer_plugroup from tr_customer_plugroup 
							where upper(i_customer) like '%$cari%' 
							or upper(e_customer_plugroupname) like '%$cari%' 
							or upper(i_customer_plugroup) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_customerplugroup');
			$data['isi']=$this->mmaster->bacaplugroup($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerplu/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariplugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu39')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('customerplu/mmaster');
			$config['base_url'] = base_url().'index.php/customerplu/cform/index/';
			$query = $this->db->query("	select i_customer, e_customer_plugroupname, i_customer_plugroup from tr_customer_plugroup 
							where upper(i_customer) like '%$cari%' 
							or upper(e_customer_plugroupname) like '%$cari%' 
							or upper(i_customer_plugroup) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_customerplugroup');
			$data['icustomerplugroup']='';
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacaplugroup($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerplu/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
