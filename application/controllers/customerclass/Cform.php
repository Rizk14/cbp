<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerclass');
			$data['icustomerclass']='';
			$this->load->model('customerclass/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Kelas Pelanggan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerclass/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerclass 	= $this->input->post('icustomerclass', TRUE);
			$ecustomerclassname 	= $this->input->post('ecustomerclassname', TRUE);

			if ((isset($icustomerclass) && $icustomerclass != '') && (isset($ecustomerclassname) && $ecustomerclassname != ''))
			{
				$this->load->model('customerclass/mmaster');
				$this->mmaster->insert($icustomerclass,$ecustomerclassname);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Kelas Pelanggan:('.$icustomerclass.') -'.$ecustomerclassname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $data['page_title'] = $this->lang->line('master_customerclass');
				$data['icustomerclass']='';
				$this->load->model('customerclass/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('customerclass/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerclass');
			$this->load->view('customerclass/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerclass')." update";
			if($this->uri->segment(4)){
				$icustomerclass = $this->uri->segment(4);
				$data['icustomerclass'] = $icustomerclass;
				$this->load->model('customerclass/mmaster');
				$data['isi']=$this->mmaster->baca($icustomerclass);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Kelas Pelanggan:('.$icustomerclass.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerclass/vmainform',$data);
			}else{
				$this->load->view('customerclass/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerclass		= $this->input->post('icustomerclass', TRUE);
			$ecustomerclassname 	= $this->input->post('ecustomerclassname', TRUE);
			$this->load->model('customerclass/mmaster');
			$this->mmaster->update($icustomerclass,$ecustomerclassname);
			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Kelas Pelanggan:('.$icustomerclass.') -'.$ecustomerclassname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('master_customerclass');
			$data['icustomerclass']='';
			$this->load->model('customerclass/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerclass/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu34')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerclass		= $this->uri->segment(4);
			$this->load->model('customerclass/mmaster');
			$this->mmaster->delete($icustomerclass);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Kelas Pelanggan:'.$icustomerclass;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$data['page_title'] = $this->lang->line('master_customerclass');
			$data['icustomerclass']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerclass/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
