<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu269')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listfkom')." Nasional";
      $data['dfrom']='';
      $data['dto']='';
			$this->load->view('listfkomnas/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu269')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      $dfrom  = $this->input->post("dfrom");
      $dto    = $this->input->post("dto");
      if($dfrom=='') $dfrom=$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      $data['dfrom']=$dfrom;
      $data['dto']  =$dto;
			$config['base_url'] = base_url().'index.php/listfkomnas/cform/view/'.$dfrom.'/'.$dto.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
										              where a.i_customer=c.i_customer and a.i_area=b.i_area 
                                  and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') 
                                  and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                  and (upper(a.i_nota) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                  and not a.i_faktur_komersial isnull",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listfkom')." Nasional";
			$this->load->model('listfkomnas/mmaster');
			$data['ifaktur']='';
			$data['inota']='';
      $data['tgl']=date('d-m-Y');
			$data['isi']=$this->mmaster->bacasemua($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(6),$dfrom,$dto);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data Faktur Komersial Nasional tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listfkomnas/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu269')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listfkom')." Nasional";
			$this->load->view('listfkomnas/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu269')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispb	= $this->input->post('ifkomdelete', TRUE);
			$this->load->model('listfkomnas/mmaster');
			$this->mmaster->delete($inota,$ifaktur);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Faktur Komersial No:'.$ispb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('listfkom')." Nasional";
			$data['ifaktur']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('listfkomnas/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu269')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
      $dfrom  = $this->input->post("dfrom");
      $dto    = $this->input->post("dto");
      $data['dfrom']=$dfrom;
      $data['dto']  =$dto;
			$config['base_url'] = base_url().'index.php/listfkomnas/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
					                        where a.i_customer=c.i_customer and a.i_area=b.i_area
                                  and not a.i_faktur_komersial isnull and (upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' 
                          				or upper(a.i_nota) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listfkomnas/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listfkom')." Nasional";
			$data['ifaktur']='';
	 		$this->load->view('listfkomnas/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
