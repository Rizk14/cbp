<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbop/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" 	select a.i_spb as no ,a.*, c.e_customer_name 
							from tm_spb a, tr_customer c
							where (
								(not a.i_approve1 isnull 
								and not a.i_approve2 isnull 
								and a.f_spb_stockdaerah='f')
							)
							and a.i_customer=c.i_customer and (upper(c.e_customer_name) like '%$cari%' 
								or upper(c.i_customer) like '%$cari%')
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_spb_op = 'f'
							and a.f_spb_pemenuhan = 'f'
							and a.f_spb_siapnotagudang = 'f'
							and a.f_spb_siapnotasales = 'f'
							and upper(a.i_spb) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('spbop');
			$data['ispb']='';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('spbop/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbop/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('spbop/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('opreq');
			if(
				($this->uri->segment(4)!='')&&($this->uri->segment(5)!='')
			  )
			{
				$ispb = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbop/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbop/vmainform',$data);
			}else{
				$this->load->view('spbop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 				= $this->input->post('ispb', TRUE);
			$iarea 				= $this->input->post('iarea', TRUE);
			$fspbop				= 't';
			$fspbpemenuhan= 't';
			$fspbsiapnotagudang	= 'f';
			$fspbsiapnotasales	= 'f';
			$benar				= 'false';
			$this->db->trans_begin();
			$this->load->model('spbop/mmaster');
			$this->mmaster->updateheader($ispb,$fspbop,$fspbsiapnotagudang,$fspbsiapnotasales,$iarea,$fspbpemenuhan);
			$benar="true";
			if ( ($this->db->trans_status() === FALSE) || ($benar=="false") )
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Pemenuhan SPB (OP) Area '.$iarea.' No:'.$ispb;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function isistore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbop');
			if($this->input->post('ispbedit')){
				$ispb = $this->input->post('ispbedit');
				$query = $this->db->query("select * from tm_spb_item
							   where i_spb = '$ispb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbop/mmaster');
				$data['isi']=$this->mmaster->baca($ispb);
				$data['detail']=$this->mmaster->bacadetail($ispb);
		 		$this->load->view('spbop/vmainform',$data);
			}else{
				$this->load->view('spbop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area=$this->session->userdata('i_area');
			$data['jml']=$this->uri->segment(4);
			$jml =$this->uri->segment(4);
			$data['spb']=$this->uri->segment(5);
			$spb =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbop/cform/store/'.$jml.'/'.$spb.'/index/';
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store
									   order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbop/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(7),$area);
			$this->load->view('spbop/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbop/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store and upper(a.i_store) like '%cari%' 
						      		   or upper(a.e_store_name) like '%cari%' order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spbop/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spbop/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/spbop/cform/index/';
			$query = $this->db->query(" select a.i_spb
							from tm_spb a, tr_customer c
							where (
								(not a.i_approve1 isnull 
								and not a.i_approve2 isnull 
								and a.f_spb_stockdaerah='f')
							)
							and a.i_customer=c.i_customer and (upper(c.e_customer_name) like '%$cari%' 
								or upper(c.i_customer) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_spb_op = 'f'
							and a.f_spb_pemenuhan = 'f'
							and a.f_spb_siapnotagudang = 'f'
							and a.f_spb_siapnotasales = 'f' ",false);
/*
			$query = $this->db->query(" select a.i_spb
							from tm_spb a, tr_customer c
							where (
								(not a.i_approve1 isnull 
								and not a.i_approve2 isnull 
								and a.f_spb_stockdaerah='f')
								or
								(a.f_spb_stockdaerah='t')
							)
							and a.i_customer=c.i_customer and (upper(c.e_customer_name) like '%$cari%' 
								or upper(c.i_customer) like '%$cari%' or upper(a.i_spb) like '%$cari%')
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_spb_op = 'f'
							and a.f_spb_pemenuhan = 'f'
							and a.f_spb_siapnotagudang = 'f'
							and a.f_spb_siapnotasales = 'f' ",false);
*/
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spbop/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('spbop');
			$data['ispb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spbop/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
