<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	/*$cari=($this->input->post("cari",false));*/
			$config['base_url'] = base_url().'index.php/customerdiscount/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_discount');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerdiscount');
			$data['icustomer']='';
			$cari = $this->input->post('cari', FALSE);
			/*$cari = strtoupper($cari);*/
			$this->load->model('customerdiscount/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Discount (Pelanggan)";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerdiscount/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ncustomerdiscount1 	= $this->input->post('ncustomerdiscount1', TRUE);
			$ncustomerdiscount2 	= $this->input->post('ncustomerdiscount2', TRUE);
			$ncustomerdiscount3	= $this->input->post('ncustomerdiscount3', TRUE);
			if ((isset($icustomer) && $icustomer != ''))
			{
				$this->load->model('customerdiscount/mmaster');
				$this->mmaster->insert($icustomer,$ncustomerdiscount1,$ncustomerdiscount2,$ncustomerdiscount3);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Discount (Pelanggan):('.$icustomer.') -'.$ncustomerdiscount1;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $config['base_url'] = base_url().'index.php/customerdiscount/cform/index/';
				$config['total_rows'] = $this->db->count_all('tr_customer_discount');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);

				$data['page_title'] = $this->lang->line('master_customerdiscount');
				$data['icustomer']='';
				/*$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);*/
				//$data['cari'] = $cari;
				$cari = $this->input->post('cari', FALSE);
				$this->load->model('customerdiscount/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customerdiscount/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerdiscount');
			$this->load->view('customerdiscount/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerdiscount')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('customerdiscount/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Discount (Pelanggan):('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		 		$this->load->view('customerdiscount/vmainform',$data);
			}else{
				$this->load->view('customerdiscount/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ncustomerdiscount1 	= $this->input->post('ncustomerdiscount1', TRUE);
			$ncustomerdiscount2	= $this->input->post('ncustomerdiscount2', TRUE);
			$ncustomerdiscount3	= $this->input->post('ncustomerdiscount3', TRUE);
			$this->load->model('customerdiscount/mmaster');
			$this->mmaster->update($icustomer,$ncustomerdiscount1,$ncustomerdiscount2,$ncustomerdiscount3);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Discount (Pelanggan):('.$icustomer.') -'.$ncustomerdiscount1;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $config['base_url'] = base_url().'index.php/customerdiscount/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_discount');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerdiscount');
			$data['icustomer']='';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			//$data['cari'] = $cari;
			$cari = $this->input->post('cari', FALSE);
			$this->load->model('customerdiscount/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerdiscount/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('customerdiscount/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master Discount (Pelanggan):('.$icustomer.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
	        
			$config['base_url'] = base_url().'index.php/customerdiscount/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_discount');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['page_title'] = $this->lang->line('master_customerdiscount');
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerdiscount/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu43')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/customerdiscount/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/customerdiscount/cform/cari/'.$cari.'/';
			}

			//$config['base_url'] = base_url().'index.php/customerdiscount/cform/index/';
			$query = $this->db->query("select a.i_customer, a.n_customer_discount1, a.n_customer_discount2, a.n_customer_discount3, b.e_customer_name
											 	from tr_customer_discount a, tr_customer b 
												where a.i_customer=b.i_customer and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%') 
												order by a.i_customer ",false);
			$config['total_rows'] = $query->num_rows();	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('customerdiscount/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_customerdiscount');
			$data['icustomer']='';
	 		$this->load->view('customerdiscount/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
