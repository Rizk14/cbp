<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/
			$query = $this->db->query(" select a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type order by a.i_city_typeperarea",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_citytypeperarea');
			$data['icitytypeperarea']='';
			$data['iarea']='';
			$data['icitytype']='';
			$cari = $this->input->post('cari', FALSE);
			$this->load->model('citytypeperarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Jenis Kota per Area";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('citytypeperarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytypeperarea 	= $this->input->post('icitytypeperarea', TRUE);
			$ecitytypeperareaname 	= $this->input->post('ecitytypeperareaname', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$icitytype		= $this->input->post('icitytype', TRUE);

			if ((isset($icitytypeperarea) && $icitytypeperarea != '') && (isset($ecitytypeperareaname) && $ecitytypeperareaname != '') && (isset($iarea) && $iarea != '') && (isset($icitytype) && $icitytype!=''))
			{
				$this->load->model('citytypeperarea/mmaster');
				$this->mmaster->insert($icitytypeperarea,$ecitytypeperareaname,$iarea,$icitytype);

		        $sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
			        while($row=pg_fetch_assoc($rs)){
				        $ip_address	  = $row['ip_address'];
				        break;
			        }
		        }else{
			        $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
			        $now	  = $row['c'];
		        }
		        $pesan='Input Jenis Kota per-area:('.$icitytypeperarea.')-'.$ecitytypeperareaname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );

		        $config['base_url'] = base_url().'index.php/citytypeperarea/cform/index/';
				/*$cari = $this->input->post('cari', FALSE);
				$cari =strtoupper($cari);*/
				$query = $this->db->query("select a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type order by a.i_city_typeperarea",false);
				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_citytypeperarea');
				$data['icitytypeperarea']='';
				$data['iarea']='';
				$data['icitytype']='';
				$this->load->model('citytypeperarea/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('citytypeperarea/vmainform', $data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citytypeperarea');
			$this->load->view('citytypeperarea/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citytypeperarea')." update";
			if($this->uri->segment(4)){
				$icitytypeperarea = $this->uri->segment(4);
				$iarea 		  = $this->uri->segment(5);
				$icitytype 	  = $this->uri->segment(6);
				$data['icitytypeperarea'] = $icitytypeperarea;
				$data['iarea'] 		  = $iarea;
				$data['icitytype']	  = $icitytype;
				$this->load->model('citytypeperarea/mmaster');
				$data['isi']=$this->mmaster->baca($icitytypeperarea,$iarea,$icitytype);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Jenis Kota per Area:('.$icitytypeperarea.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('citytypeperarea/vmainform',$data);
			}else{
				$this->load->view('citytypeperarea/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytypeperarea 	= $this->input->post('icitytypeperarea', TRUE);
			$ecitytypeperareaname 	= $this->input->post('ecitytypeperareaname', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$icitytype		= $this->input->post('icitytype', TRUE);
			$this->load->model('citytypeperarea/mmaster');
			$this->mmaster->update($icitytypeperarea,$ecitytypeperareaname,$iarea,$icitytype);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Update Jenis Kota per-area:('.$icitytypeperarea.')-'.$ecitytypeperareaname;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/
			$query = $this->db->query("select a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type order by a.i_city_typeperarea",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_citytypeperarea');
			$data['icitytypeperarea']='';
			$data['iarea']='';
			$data['icitytype']='';
			$this->load->model('citytypeperarea/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('citytypeperarea/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytypeperarea = $this->uri->segment(4);
			$icitytype	  = $this->uri->segment(6);
			$iarea 		  = $this->uri->segment(5);
			$this->load->model('citytypeperarea/mmaster');
			$this->mmaster->delete($icitytypeperarea,$iarea,$icitytype);
			
      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Jenis Kota per-area:('.$icitytypeperarea.')-'.$ecitytypeperareaname;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, tr_city_type c 
						   	where a.i_area=b.i_area and a.i_city_type=c.i_city_type 
							and (upper(a.e_city_typeperareaname) like '%$cari%' or upper(a.i_city_typeperarea) like '%$cari%' 
							or upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%' 
							or upper(c.e_city_typename) like '%$cari%' or upper(c.i_city_type) like '%$cari%')
						   	order by a.i_city_typeperarea",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_citytypeperarea');
			$data['icitytypeperarea']='';
			$data['iarea']='';
			$data['icitytype']='';
			$this->load->model('citytypeperarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('citytypeperarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/area/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_area order by i_area asc ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('citytypeperarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6));
			$this->load->view('citytypeperarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function citytype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/citytype/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_city_type order by i_city_type asc",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('citytypeperarea/mmaster');
			$data['page_title'] = $this->lang->line('list_citytype');
			$data['isi']=$this->mmaster->bacacitytype($config['per_page'],$this->uri->segment(6));
			$this->load->view('citytypeperarea/vlistcitytype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/citytypeperarea/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/citytypeperarea/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/citytypeperarea/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename from tr_city_typeperarea a, tr_area b, 
										tr_city_type c where a.i_area=b.i_area and a.i_city_type=c.i_city_type and (upper 
										(a.i_city_typeperarea) ilike '%$cari%' or upper (a.e_city_typeperareaname) ilike '%$cari%' 
										or upper(b.e_area_name) ilike '%$cari%' or upper (c.e_city_typename) ilike '%$cari%') 
										order by a.i_city_typeperarea ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('citytypeperarea/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_citytypeperarea');
			$data['icitytypeperarea']='';
	 		$this->load->view('citytypeperarea/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/citytypeperarea/cform/kelas/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/cariarea/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/cariarea/'.$baris.'/sikasep/';

			$query = $this->db->query("	select * from tr_area
									   	where upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('citytypeperarea/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('citytypeperarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricitytype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu25')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/citytypeperarea/cform/citytype/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari 	= strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/caricitytype/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/citytypeperarea/cform/caricitytype/'.$baris.'/sikasep/';

			$query 	= $this->db->query(" select * from tr_city_type
						   	where upper(e_city_typename) ilike '%$cari%' or upper(i_city_type) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']=$cari;
			$this->load->model('citytypeperarea/mmaster');
			$data['page_title'] = $this->lang->line('list_citytype');
			$data['isi']=$this->mmaster->caricitytype($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('citytypeperarea/vlistcitytype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
