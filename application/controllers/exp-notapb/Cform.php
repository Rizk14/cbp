<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu368') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-notapb');
			$data['datefrom'] = '';
			$data['dateto']	= '';
			$this->load->view('exp-notapb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu368') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-notapb/mmaster');
			$icustomer    = $this->input->post('icustomer');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
			if ($datefrom != '') {
				$tmp = explode("-", $datefrom);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$datefrom = $th . "-" . $bl . "-" . $hr;
				$periodeawal	= $hr . "/" . $bl . "/" . $th;
			}
			if ($dateto != '') {
				$tmp = explode("-", $dateto);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dateto = $th . "-" . $bl . "-" . $hr;
				$periodeakhir   = $hr . "/" . $bl . "/" . $th;
			}
			$qcustname	= $this->mmaster->ecustname($icustomer);
			if ($qcustname->num_rows() > 0) {
				$row_custname	= $qcustname->row();
				$cname   = $row_custname->e_customer_name;
			} else {
				$cname   = 'Semua Toko';
			}


			$data['page_title'] = $this->lang->line('exp-notapb');
			if ($icustomer == 'AS') {
				$this->db->select("	a.i_notapb, a.d_notapb, b.i_product, b.n_quantity, a.v_notapb_discount,
				a.n_notapb_discount, b.v_unit_price, a.v_notapb_gross, a.i_customer, a.i_periode_nota
				from tm_notapb a, tm_notapb_item b
				where a.i_notapb=b.i_notapb and a.i_customer=b.i_customer
				and a.i_area=b.i_area
				and (a.d_notapb >= to_date('$datefrom','yyyy-mm-dd') and
				a.d_notapb <= to_date('$dateto','yyyy-mm-dd'))
				and a.f_notapb_cancel = 'f'
				order by a.d_notapb", false);
			} else {
				$this->db->select("	a.i_notapb, a.d_notapb, b.i_product, b.n_quantity, a.v_notapb_discount,
							  a.n_notapb_discount, b.v_unit_price, a.v_notapb_gross, a.i_customer, a.i_periode_nota
							  from tm_notapb a, tm_notapb_item b
							  where a.i_notapb=b.i_notapb and a.i_customer=b.i_customer
							  and a.i_area=b.i_area
							  and a.i_customer='$icustomer'
							  and (a.d_notapb >= to_date('$datefrom','yyyy-mm-dd') and
							  a.d_notapb <= to_date('$dateto','yyyy-mm-dd'))
							  and a.f_notapb_cancel = 'f'
							  order by a.d_notapb", false);
			}
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Penjualan Konsinyasi")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Penjualan Konsinyasi');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 9, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Pelanggan : ' . $icustomer . '-' . $cname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 9, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:L5'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 5, 0, 5);
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders'   => array(
							'top'       => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tanggal BON');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1, 5, 1, 5);
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'No BON');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, 5, 2, 5);
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Periode Nota');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, 5, 3, 5);
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kode Brg');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4, 5, 4, 5);
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Qty');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 5, 5);
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', '% Disc');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Harga');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, 5, 7, 5);
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Gross');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 5, 8, 5);
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Disc');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9, 5, 9, 5);
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Netto');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, 5, 10, 5);
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Toko');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(11, 5, 11, 5);
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Nama Toko');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12, 5, 12, 5);
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i = 5;
				$j = 5;
				$no = 0;

				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':M' . $i
					);

					$netto = $row->v_notapb_gross - $row->v_notapb_discount;
					$notapb = substr($row->i_notapb, 8, 8);
					if ($row->d_notapb != '') {
						$tmp = explode("-", $row->d_notapb);
						$th = $tmp[0];
						$bl = $tmp[1];
						$hr = $tmp[2];
						$row->d_notapb = $hr . "-" . $bl . "-" . $th;
					}

					$i++;
					$j++;

					$data_customer = $this->mmaster->ecustname($row->i_customer);
					if ($data_customer->num_rows() > 0) {
						$row_custname	= $data_customer->row();
						$nama_toko   = $row_custname->e_customer_name;
					} else {
						$nama_toko   = '';
					}
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $j - 5, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->d_notapb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $notapb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_periode_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->n_quantity, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->n_notapb_discount, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->v_unit_price, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->v_notapb_gross, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->v_notapb_discount, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $netto, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $nama_toko, Cell_DataType::TYPE_STRING);
				}
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'penjualankons-' . $icustomer . '.xls';
			$objWriter->save('excel/PB/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export Penjualan Konsinyasi tanggal:' . $datefrom . ' sampai:' . $dateto;
			// $this->load->model('logger');
			// $this->logger->write($id, $ip_address, $now, $pesan);

			$this->logger->writenew('Export Penjualan Konsinyasi tanggal:' . $datefrom . ' sampai:' . $dateto);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= 'excel/PB';

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu306') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-notapb/cform/customer/index/';
			//$icustomer	= $this->input->post('i_customer');
			$query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-notapb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->bacacustomer($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-notapb/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu306') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-notapb/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-notapb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->caricustomer($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-notapb/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu368') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-notapb/cform/area/index/';
			$area1   = $this->session->userdata('i_area');
			$area2   = $this->session->userdata('i_area2');
			$area3   = $this->session->userdata('i_area3');
			$area4   = $this->session->userdata('i_area4');
			$area5   = $this->session->userdata('i_area5');
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                 or i_area = '$area4' or i_area = '$area5'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-notapb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-notapb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu368') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/exp-notapb/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false);
			} else {
				$query 	= $this->db->query("select * from tr_area
                                          where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                                          and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                          or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-notapb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-notapb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
