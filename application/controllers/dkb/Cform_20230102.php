<?php 
class Cform extends CI_Controller {
//  $data=array();
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $this->load->model('dkb/mmaster');
			$data['iarealogin'] = $this->session->userdata('i_area');
			$data['page_title'] = $this->lang->line('dkb');
			$data['idkb']='';
			$data['isi']='';
			$data['detail']="";
			$data['jmlitem']="";
			$data['tgl']=date('d-m-Y');
			$data['iarea']='';
			$data['eareaname']='';
			if($this->uri->segment(4)!='')$data['iarea']=$this->uri->segment(4);
			if($this->uri->segment(5)!='')$data['eareaname']=$this->uri->segment(5);
			$this->load->view('dkb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ddkb 		= $this->input->post('ddkb', TRUE);
			if($ddkb!=''){
				$tmp=explode("-",$ddkb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddkb=$th."-".$bl."-".$hr;
        $thbl=$th.$bl;
			}
			$iarea	    = $this->session->userdata('i_area');
			$idkbold    = $this->input->post('idkbold', TRUE);
			$idkb	      = $this->input->post('idkb', TRUE);
			$eareaname  = $this->input->post('eareaname', TRUE);
			$iareasj    = $this->input->post('iarea', TRUE);
			$edkbkirim	= $this->input->post('edkbkirim', TRUE);
			$idkbkirim	= $this->input->post('idkbkirim', TRUE);
			$edkbvia   	= $this->input->post('edkbvia', TRUE);
			$idkbvia 	  = $this->input->post('idkbvia', TRUE);
			$eekspedisi	= $this->input->post('eekspedisi', TRUE);
			$esupirname	= $this->input->post('esupirname', TRUE);
			$ikendaraan	= $this->input->post('ikendaraan', TRUE);		
			$vdkb   	  = $this->input->post('vdkb', TRUE);		
			$vdkb		    = str_replace(',','',$vdkb);
			$jml	      = $this->input->post('jml', TRUE);
			$jmlx	      = $this->input->post('jmlx', TRUE);
			if($iarea=='00') $daer='f';
			if($iarea!='00') $daer='t';
			
			if($ddkb!='' && $iareasj!='' && $idkbkirim!='' && $idkbvia!='' && $jml!='' && $jml!=0)
			{
				$this->db->trans_begin();
				$this->load->model('dkb/mmaster');
				$idkb	=$this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insertheader($idkb, $ddkb, $iareasj, $idkbkirim, $idkbvia, $ikendaraan, $esupirname, $vdkb, $idkbold);
				for($i=1;$i<=$jml;$i++){
				  $isj			= $this->input->post('isj'.$i, TRUE);
				  $dsj			= $this->input->post('dsj'.$i, TRUE);
				  if($dsj!=''){
					$tmp=explode("-",$dsj);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dsj=$th."-".$bl."-".$hr;
				  }
				  $vjumlah		= $this->input->post('vsjnetto'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $eremark		= $this->input->post('eremark'.$i, TRUE);
				  $this->mmaster->insertdetail($idkb,$iareasj,$isj,$ddkb,$dsj,$vjumlah,$eremark,$i);
				  $this->mmaster->updatesj($idkb,$isj,$iareasj,$ddkb);
				}
				if($jmlx>0 && $idkbvia!=2){
					for($i=1;$i<=$jmlx;$i++){
						$iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
						$eremark	= $this->input->post('eremarkx'.$i, TRUE);
						$this->mmaster->insertdetailekspedisi($idkb,$iareasj,$iekspedisi,$ddkb,$eremark,$i);
					}
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input DKB Area '.$iareasj.' No:'.$idkb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				  $data['sukses']			= true;
				  $data['inomor']			= $idkb;
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('dkb');
			$this->load->view('dkb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $data['bisaedit']=false;
			$data['page_title'] = $this->lang->line('dkb')." update";
			if($this->uri->segment(4)!=''){
				$idkb   = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom  = $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
				$query  = $this->db->query("select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 	
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){			
				    $ddkb=substr($row->d_dkb,0,4).substr($row->d_dkb,5,2);
				  }
				}
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();				
				$this->load->model('dkb/mmaster');
				$data['isi']		= $this->mmaster->baca($idkb,$iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb,$iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb,$iarea);
				if($qrunningjml->num_rows()>0){
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				}else{
					$data['jmlx']	= 0;	
				}
				$data['detailx']=$this->mmaster->bacadetailx($idkb,$iarea);
				
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            		$periode=$row->i_periode;
				  }
				  if($periode<=$ddkb)$data['bisaedit']=true;
			  }else{
				  $data['bisaedit']=false;
			  }
		 		$this->load->view('dkb/vmainform',$data);
			}else{
				$this->load->view('dkb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ddkb 		= $this->input->post('ddkb', TRUE);
			if($ddkb!=''){
				$tmp=explode("-",$ddkb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddkb=$th."-".$bl."-".$hr;
			}
      $iarea	    = $this->session->userdata('i_area');
			$idkbold    = $this->input->post('idkbold', TRUE);
			$idkb	      = $this->input->post('idkb', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$iareasj  	= $this->input->post('iarea', TRUE);
			$edkbkirim 	= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 	= $this->input->post('idkbkirim', TRUE);
			$edkbvia   	= $this->input->post('edkbvia', TRUE);
			$idkbvia   	= $this->input->post('idkbvia', TRUE);
			$eekspedisi	= $this->input->post('eekspedisi', TRUE);
			$esupirname	= $this->input->post('esupirname', TRUE);
			$ikendaraan	= $this->input->post('ikendaraan', TRUE);		
			$vdkb   	  = $this->input->post('vdkb', TRUE);		
			$vdkb  		= str_replace(',','',$vdkb);
			$jml      	= $this->input->post('jml', TRUE);
      $jmlx	      = $this->input->post('jmlx', TRUE);
				  				  			
			if($idkb!='' && $ddkb!='' && $iareasj!='' && $idkbkirim!='' && $idkbvia!='')
			{
				$this->db->trans_begin();
  			$this->load->model('dkb/mmaster');
				$this->mmaster->deleteheader($idkb, $iareasj);
				$this->mmaster->insertheader($idkb, $ddkb, $iareasj, $idkbkirim, $idkbvia, $ikendaraan, $esupirname, $vdkb, $idkbold);
				for($i=1;$i<=$jml;$i++){
				  $isj = $this->input->post('isj'.$i, TRUE);
				  $dsj = $this->input->post('dsj'.$i, TRUE);
				  if($dsj!=''){
					  $tmp=explode("-",$dsj);
					  $th=$tmp[2];
					  $bl=$tmp[1];
					  $hr=$tmp[0];
					  $dsj=$th."-".$bl."-".$hr;
				  }
				  $vjumlah		= $this->input->post('vsjnetto'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $eremark		= $this->input->post('eremark'.$i, TRUE);
				  $this->mmaster->deletedetail($idkb,$iareasj,$isj);
				  $this->mmaster->insertdetail($idkb,$iareasj,$isj,$ddkb,$dsj,$vjumlah,$eremark,$i);
				  $this->mmaster->updatesj($idkb,$isj,$iareasj,$ddkb);
				}
				if($jmlx>0 && $idkbvia!=2){	
					for($i=1;$i<=$jmlx;$i++){
					  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
						$eremark	= $this->input->post('eremarkx'.$i, TRUE);
						$this->mmaster->deletedetailekspedisi($idkb,$iareasj,$iekspedisi);
						$this->mmaster->insertdetailekspedisi($idkb,$iareasj,$iekspedisi,$ddkb,$eremark,$i);
					}
				}
						
				if (($this->db->trans_status()===FALSE))
				{
				    $this->db->trans_rollback();
				}else{
			      $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update DKB Area '.$iarea.' No:'.$idkb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

#				    $this->db->trans_rollback();
					  $data['sukses']			= true;
					  $data['inomor']			= $idkb;
					  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetailekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idkb	= str_replace("%20","",$this->uri->segment(4));
			$iarea	= $this->uri->segment(5);
			$iekspedisi	= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			//$icustomer	= $this->uri->segment(9);
			$this->db->trans_begin();
			$this->load->model('dkb/mmaster');
			$this->mmaster->deletedetailekspedisi($idkb,$iarea,$iekspedisi);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			   $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Hapus Ekspedisi DKB Area '.$iarea.' No:'.$idkb.' Ekspedisi'.$iekspedisi;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('dkb')." update";
				$query  = $this->db->query("select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				//$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('dkb/mmaster');
				$data['isi']		= $this->mmaster->baca($idkb,$iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb,$iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb,$iarea);
				if($qrunningjml->num_rows()>0){
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				}else{
					$data['jmlx']	= 0;	
				}
				$data['detailx']	= $this->mmaster->bacadetailx($idkb,$iarea);
		 		$this->load->view('dkb/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('dkb/mmaster');
			$this->mmaster->delete($ispmb);
			$data['page_title'] = $this->lang->line('dkb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('dkb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idkb		= $this->uri->segment(4);
			$iareasj  = substr($this->uri->segment(6),9,2);
			$iarea = $this->uri->segment(5);
			$isj		= $this->uri->segment(6);
			$dfrom	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$this->db->trans_begin();
			$this->load->model('dkb/mmaster');
			$this->mmaster->deletedetail2($idkb, $iarea, $isj, $iareasj);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Hapus Detail DKB Area '.$iarea.' No:'.$idkb.' untuk SJ :'.$isj;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

				$data['bisaedit']=false;
				$data['page_title'] = $this->lang->line('dkb')." Update";
				$query  = $this->db->query("select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();				
				$this->load->model('dkb/mmaster');
				$data['isi']		= $this->mmaster->baca($idkb,$iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb,$iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb,$iarea);
				if($qrunningjml->num_rows()>0){
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				}else{
					$data['jmlx']	= 0;	
				}
				$data['detailx']=$this->mmaster->bacadetailx($idkb,$iarea);

				// $query=$this->db->query("	select i_periode from tm_periode ",false);
			  	// if ($query->num_rows() > 0){
				// 	  foreach($query->result() as $row){
            	// 		$periode=$row->i_periode;
				// 	  }
				// 	  if($periode<=$ddkb)$data['bisaedit']=true;
			  	// }else{
				// 	  $data['bisaedit']=false;
			  	// }


		 		$this->load->view('dkb/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $baris=$this->input->post('baris', FALSE);
      $area=$this->input->post('iarea', FALSE);
      $ddkb=$this->input->post('ddkb', FALSE);
			if($baris=='') $baris=$this->uri->segment(4);
			$data['baris']=$baris;
      if($area=='')	$area=$this->uri->segment(5);
      if($ddkb=='')	$ddkb=$this->uri->segment(6);
      $tmp=explode("-",$ddkb);
      $dd=$tmp[0];
      $mm=$tmp[1];
      $yy=$tmp[2];
      $ddkbx=$yy.'-'.$mm.'-'.$dd;
      $iareasj=$this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$data['area']=$area;
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/dkb/cform/sj/'.$baris.'/'.$area.'/'.$ddkb.'/';
		  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_customer b 
                                  where a.i_nota isnull and a.i_area='$area' and a.d_sj<='$ddkbx'
                                  and (substring(a.i_sj,9,2)='$iareasj' or substring(a.i_sj,9,2)='$area2' or substring(a.i_sj,9,2)='$area3'
                                  or substring(a.i_sj,9,2)='$area4' or substring(a.i_sj,9,2)='$area5')
                                  and a.i_dkb isnull and a.f_nota_cancel='f' and a.i_customer=b.i_customer
                                  and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                  or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  or upper(a.i_sj_old) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->bacasj($ddkbx,$cari,$area,$config['per_page'],$this->uri->segment(7,0),$iareasj,$area2,$area3,$area4,$area5);
			$data['baris']=$baris;
			$this->load->view('dkb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	= $this->uri->segment(4);
			$area	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/dkb/cform/sj/'.$baris.'/'.$area.'/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
			  $query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
					                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_to='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                        and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                and a.i_customer=b.i_customer and a.f_sj_daerah='f'",false);
#					                and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$area')
			  if($query->num_rows()==0 || $query->num_rows()<1){
				  $query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
										where a.i_nota isnull and a.i_sj_type='04' and a.i_area_referensi='$area' 
										and a.i_dkb isnull and a.f_sj_cancel='f'
										and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
										or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
										and a.i_customer=b.i_customer and a.f_sj_daerah='f' and a.f_entry_pusat='t' ",false);				  
		      }			  
      }else{

			  $query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
					                          where a.i_nota isnull and a.i_sj_type='04' and a.i_area_to='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                             	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                          and a.i_customer=b.i_customer and a.f_sj_daerah='t'",false);
#					                          and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$area')
     }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('dkb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/dkb/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('dkb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      		$iuser   = $this->session->userdata('user_id');
      		$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/dkb/cform/area/'.$baris.'/sikasep/';
      		/*$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
      		/*$query = $this->db->query(" select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'", false);*/

	        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      		$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('dkb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      		$iuser   	= $this->session->userdata('user_id');
			/*$config['base_url'] = base_url().'index.php/dkb/cform/area/index/';
      		$cari    	= $this->input->post('cari', FALSE);
      		$cari 		  = strtoupper($cari);
      		$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      		if($area1=='00'){
		  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
		      }else{
					  $query 	= $this->db->query("select * from tr_area
										                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
		              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										                   	or i_area = '$area4' or i_area = '$area5')",false);
		      }*/

		    $baris   = $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
			$config['base_url'] = base_url().'index.php/dkb/cform/cariarea/'.$baris.'/'.$cari.'/';
			  else
			$config['base_url'] = base_url().'index.php/dkb/cform/cariarea/'.$baris.'/sikasep/';

			$query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
     		$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('dkb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kirim()
	{
                $data=array();
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/dkb/cform/kirim/index/';
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
  			$query = $this->db->query("select * from tr_dkb_kirim ",false);
      }else{
  			$query = $this->db->query("select * from tr_dkb_kirim where i_dkb_kirim='1'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->bacakirim($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('dkb/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/dkb/cform/kirim/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_kirim
						   where upper(i_dkb_kirim) like '%$cari%' 
						      or upper(e_dkb_kirim) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->carikirim($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('dkb/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function via()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/dkb/cform/via/index/';
			$query = $this->db->query("select * from tr_dkb_via ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->bacavia($config['per_page'],$this->uri->segment(5));
			$this->load->view('dkb/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carivia()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/dkb/cform/via/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_via
						   where upper(i_dkb_via) like '%$cari%' 
						      or upper(e_dkb_via) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->carivia($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('dkb/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
            $area=$this->uri->segment(5);
            $baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/dkb/cform/ekspedisi/'.$baris.'/'.$area.'/';
			$query = $this->db->query("select * from tr_ekspedisi where i_area='$area' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('vlistekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($area,$config['per_page'],$this->uri->segment(6));
      $data['area']=$area;
      $data['baris']=$baris;
			$this->load->view('dkb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/dkb/cform/ekspedisi/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris = strtoupper($this->input->post('xbaris', FALSE));
			$iarea = strtoupper($this->input->post('xarea', FALSE));
			$query = $this->db->query("select * from tr_ekspedisi
						                     where upper(i_ekspedisi) like '%$cari%' 
						                        or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('vlistekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
      $data['area']=$iarea;
      $data['baris']=$baris;
			$this->load->view('dkb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*
	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['iarea']=$this->uri->segment(5);
			$iarea=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/dkb/cform/sjupdate/'.$baris.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_sj a, tr_customer b 
					                        where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$iarea' 
					                        and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$iarea')
					                        and a.i_customer=b.i_customer ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->bacasj($iarea,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('dkb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$area=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/dkb/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.*, b.e_customer_name from tm_sj a, tr_customer b 
					                        where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
					                        and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$area')
			                           	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%')
					                        and a.i_customer=b.i_customer ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$area;
			$data['baris']=$baris;
			$this->load->view('dkb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
*/
	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/dkb/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			$area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
					where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' and a.i_dkb isnull and a.f_sj_cancel='f'
						and a.i_customer=b.i_customer and a.f_sj_daerah='f'",false);
				  /*		
				  if($query->num_rows()==0 || $query->num_rows()<1){
					  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
									where a.i_nota isnull and a.i_sj_type='04' and a.i_area_referensi='$area' 
														and a.f_entry_pusat='t' and a.i_dkb isnull and a.f_sj_cancel='f'
									and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%') 
									and a.i_customer=b.i_customer and a.f_sj_daerah='f' ",false);
				  }	*/					
      			}else{
			  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
					where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    		and a.i_dkb isnull and a.f_sj_cancel='f'
						and a.i_customer=b.i_customer and a.f_sj_daerah='t'",false);
      			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->bacasj($area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
      		$data['iarea']=$area;
			$this->load->view('dkb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$area=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/dkb/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
					                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                        and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                and a.i_customer=b.i_customer and a.f_sj_daerah='f'",false);
			  /*							
			  if($query->num_rows()==0 || $query->num_rows()<1){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
								where a.i_nota isnull and a.i_sj_type='04' and a.i_area_referensi='$area' 
		                        and a.f_entry_pusat='t' and a.i_dkb isnull and a.f_sj_cancel='f'
								and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%') 
								and a.i_customer=b.i_customer and a.f_sj_daerah='f' ",false);
			  }*/   
      }else{
			  $query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
					                          where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                             	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                          and a.i_customer=b.i_customer and a.f_sj_daerah='t'",false);
     }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$area;
			$data['baris']=$baris;
			$this->load->view('dkb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
	/*
	function ekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/dkb/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('dkb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu160')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	=	$this->input->post('baris', FALSE);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/dkb/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi
																 where upper(i_ekspedisi) like '%$cari%' 
																		or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('dkb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('dkb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	*/

?>
