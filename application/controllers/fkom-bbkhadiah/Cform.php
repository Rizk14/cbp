<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $data['iperiode']='';
			$data['ifaktur']='';
			$data['inota']='';
			$data['isi']='';
			$data['page_title'] = $this->lang->line('fkom-bbkhadiah');
			$this->load->view('fkom-bbkhadiah/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
/*
      $dfrom  = $this->input->post("dfrom");
      $dto    = $this->input->post("dto");
      if($dfrom=='') $dfrom=$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      $data['dfrom']=$dfrom;
      $data['dto']  =$dto;
*/
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
      $data['iperiode']  =$iperiode;
			$config['base_url'] = base_url().'index.php/fkom-bbkhadiah/cform/view/'.$iperiode.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
/*
			$query = $this->db->query(" select a.*, b.e_area_name from tm_bbk a, tr_area b
										              where a.i_area=b.i_area
                                  and a.d_bbk >= to_date('$dfrom','dd-mm-yyyy') 
                                  and a.d_bbk <= to_date('$dto','dd-mm-yyyy')
                                  and (upper(a.i_bbk) like '%$cari%')
                                  and a.i_bbk_type='03'
                                  and a.i_bbk not in(select i_bbk from tm_bbk_pajak)",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
*/
			$data['page_title'] = $this->lang->line('fkom-bbkhadiah');
			$this->load->model('fkom-bbkhadiah/mmaster');
			$data['ifaktur']='';
			$data['inota']='';
      $data['tgl']=date('d-m-Y');
#			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(6),$dfrom,$dto);
			$data['isi']=$this->mmaster->bacasemua($cari,$iperiode);
			$this->load->view('fkom-bbkhadiah/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('fkom-bbkhadiah/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$this->load->model('fkom-bbkhadiah/mmaster');
			$this->mmaster->delete($inota,$ispb);
			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('fkom-bbkhadiah/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
      $dfrom  = $this->input->post("dfrom");
      $dto    = $this->input->post("dto");
      $data['dfrom']=$dfrom;
      $data['dto']  =$dto;
			$config['base_url'] = base_url().'index.php/fkom-bbkhadiah/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
					                        where a.i_customer=c.i_customer and a.i_area=b.i_area
                                  and a.i_faktur_komersial isnull and (upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' 
                          				or upper(a.i_nota) like '%$cari%' or upper(a.i_nota_old) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('fkom-bbkhadiah/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('cnota');
			$data['ispb']='';
	 		$this->load->view('fkom-bbkhadiah/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('fkom-bbkhadiah');
			$jml = $this->input->post('jml', TRUE);
      $ifkom= $this->input->post('ifakturkomersial', TRUE);
			$this->db->trans_begin();
			$this->load->model('fkom-bbkhadiah/mmaster');
      $adatmp=true;
      if($ifkom!=''){
			  for($i=1;$i<=$jml;$i++){
				  $cek=$this->input->post('chk'.$i, TRUE);
				  if($cek=='on'){
					  $ibbk = $this->input->post('ibbk'.$i, TRUE);

       			$dbbk 		= $this->input->post('dbbk'.$i, TRUE);
            if($dbbk!=''){
				      $tmp=explode("-",$dbbk);
				      $th=$tmp[2];
				      $bl=$tmp[1];
				      $hr=$tmp[0];
				      $dbbk=$th."-".$bl."-".$hr;
              $thbl=$th.$bl;
              $tbl=substr($th,2,2).$bl;
			      }
            $tmp="FK-".$tbl."-";
            $ifakturkomersial	= $tmp.$this->input->post('ifakturkomersial', TRUE);

					  $nilai = $this->input->post('nilai'.$i, TRUE);
            $nilai = str_replace(",","",$nilai);
            $ada=$this->mmaster->cekfaktur($ifakturkomersial);
            if(!$ada || $adatmp==false){
              $this->mmaster->insertfkom($ifakturkomersial,$ibbk);
              $adatmp=false;
            }
				  }
			  }
      }
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}elseif(!$adatmp){
				$this->db->trans_commit();
        $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Insert Faktur Komersial No'.$ifakturkomersial.' bbk:'.$ibbk;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  

				$data['sukses']			= true;
				$data['inomor']			= $ifakturkomersial;
				$this->load->view('nomor',$data);
#        $this->db->trans_rollback();
			}else{
        $data['sukses']			= true;
				$data['inomor']			= 'No Faktur sudah ada';
				$this->load->view('nomor',$data);
        $this->db->trans_rollback();
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('fkom-bbkhadiah/mmaster');
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$isj 	= $this->input->post('isj', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}
			$inotaold		= $this->input->post('inotaold', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			= $this->input->post('ispbpo', TRUE);
			if($ispbpo=='') $ispbpo=null;
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname	= $this->input->post('esalesmanname',TRUE);
			$ipricegroup	= $this->input->post('ipricegroup',TRUE);
			$dspbreceive	= $this->input->post('dspbreceive',TRUE);
			$nnotatoplength	= $this->input->post('nspbtoplength',TRUE);
			$nnotadiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nnotadiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nnotadiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$nnotadiscount4	= $this->input->post('ncustomerdiscount4',TRUE);
			$vnotadiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vnotadiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vnotadiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vnotadiscount4	= $this->input->post('vcustomerdiscount4',TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vnotadiscounttotal	= $this->input->post('vspbdiscounttotalafter',TRUE);
			$vnotadiscounttotal	= str_replace(',','',$vnotadiscounttotal);
			$vnotadiscount		= $vnotadiscounttotal;
			$vspb			= $this->input->post('vspbbersih',TRUE);
			$vspb			= str_replace(',','',$vspb);
//===========
			$nprice			= $this->input->post('nprice',TRUE);
			$vnotappn		= $this->input->post('vnotappn',TRUE);
			$vnotappn		= str_replace(',','',$vnotappn);
			$vnotanetto		= $this->input->post('vspbafter',TRUE);
			$vnotanetto		= str_replace(',','',$vnotanetto);
			$fspbplusppn		= $this->input->post('fspbplusppn',TRUE);
#			if($fspbplusppn=='on')
#				$fspbplusppn='t';
#			else
#				$fspbplusppn='f';
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
#			if($fspbplusdiscount=='on')
#				$fspbplusdiscount='t';
#			else
#				$fspbplusdiscount='f';
//===========
			$vsisa			= $this->input->post('vspbafter',TRUE);
			$vsisa			= str_replace(',','',$vsisa);
			$nnotadiscount1		= str_replace(',','',$nnotadiscount1);
			$nnotadiscount2		= str_replace(',','',$nnotadiscount2);
			$nnotadiscount3		= str_replace(',','',$nnotadiscount3);
			$nnotadiscount4		= str_replace(',','',$nnotadiscount4);
			$vnotadiscount1		= str_replace(',','',$vnotadiscount1);
			$vnotadiscount2		= str_replace(',','',$vnotadiscount2);
			$vnotadiscount3		= str_replace(',','',$vnotadiscount3);
			$vnotadiscount4		= str_replace(',','',$vnotadiscount4);
			$jml			= $this->input->post('jml', TRUE);
			$dnota 			= $this->input->post('dnota', TRUE);
#####
			$tmp 	= explode("-", $dnota);
			$det	= $tmp[0];
			$mon	= $tmp[1];
			$yir 	= $tmp[2];
			$ddspb	= $yir."/".$mon."/".$det;
			if($nnotatoplength<0) $nnotatoplength=$nnotatoplength*-1;
			$dudet	= $this->fungsi->dateAdd("d",$nnotatoplength,$ddspb);
			$djatuhtempo=$dudet;
#####
			if($dnota!=''){
				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$inota 		= $this->input->post('inota', TRUE);
			$ispbprogram	= null;
			$eremark 	= $this->input->post('eremark', TRUE);
			$fmasalah 	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif 	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$flunas 	= $this->input->post('flunas', TRUE);
			if($flunas=='')
				$flunas='f';
			else
				$flunas='t';
			$fcicil 	= $this->input->post('fcicil', TRUE);
			if($fcicil=='')
				$fcicil='f';
			else
				$fcicil='t';
			
			if(($dnota!='') && ($inota=='')){
				$inota		   			= $this->mmaster->runningnumber($iarea,$thbl);
				$vspbdiscounttotalafter	= $this->input->post('vspbdiscounttotalafter',TRUE);
				$vspbdiscounttotalafter	= str_replace(',','',$vspbdiscounttotalafter);
				$vspbafter				= $this->input->post('vspbafter',TRUE);
				$vspbafter				= str_replace(',','',$vspbafter);
				if($fspbplusppn=='t'){
					$vnotagross				= $vspbafter+$vspbdiscounttotalafter;
				}else{
					$vnotagross	= $this->input->post('vnotagross',TRUE);
					$vnotagross	= str_replace(',','',$vnotagross);
				}
				$vnotagross				= $vspbafter+$vspbdiscounttotalafter;
				$ispb 					= $this->input->post('ispb', TRUE);
				$this->db->trans_begin();
				$this->mmaster->insertheader($inota,$ispb,$iarea,$icustomer,$isalesman,$ispbprogram,$ispbpo,
								 $dspb,$dnota,$djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,
								 $nnotatoplength,$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,
								 $vnotadiscount1,$vnotadiscount2,$vnotadiscount3,$vnotadiscounttotal,
								 $vnotanetto,$vsisa,$vspbdiscounttotal,$vspb,$fspbplusppn,$fspbplusdiscount,
								 $nprice,$vnotappn,$vnotagross,$vnotadiscount,$nnotadiscount4,$vnotadiscount4,
								 $fcicil,$inotaold,$vnotappn,$isj,$dsj);
				$this->mmaster->updatespb($ispb,$iarea,$inota,$dnota,$vspbdiscounttotalafter,$vspbafter);
				$this->mmaster->updatesj($isj,$iarea,$inota,$dnota);
				for($i=1;$i<=$jml;$i++){
				  $iproduct				= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade		= 'A';
				  $iproductmotif		= $this->input->post('motif'.$i, TRUE);
				  $eproductname			= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice			= $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice			= str_replace(',','',$vunitprice);
				  $ndeliver				= $this->input->post('ndeliver'.$i, TRUE);
				  $ndeliver				= str_replace(',','',$ndeliver);
				  if($ndeliver>0){
					  $this->mmaster->insertdetail(	$inota,$iarea,$iproduct,$iproductgrade,$eproductname,$ndeliver,
													$vunitprice,$iproductmotif,$dnota);
				  }
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Update Faktur Komersial No'.$inota.' spb:'.$ispb;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  

				$data['sukses']			= true;
				$data['inomor']			= $inota;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('nota')." update";
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))
			  )
			{
				$dto 	= $this->uri->segment(8);
				$dfrom= $this->uri->segment(7);
				$area= $this->uri->segment(6);
				$ispb = $this->uri->segment(5);
				$inota= $this->uri->segment(4);
				$query = $this->db->query("select * from tm_nota_item where i_nota = '$inota' and i_area='$area'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb']    = $ispb;
				$data['inota'] 	 = $inota;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$data['iarea']   = $area;
				$this->load->model('fkom-bbkhadiah/mmaster');
				$data['isi']=$this->mmaster->bacanota($inota,$ispb,$area);
				$data['detail']=$this->mmaster->bacadetailnota($inota,$area);
		 		$this->load->view('fkom-bbkhadiah/vmainform',$data);
			}else{
				$this->load->view('fkom-bbkhadiah/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu382')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('fkom-bbkhadiah/mmaster');
			$inota 		= $this->input->post('inota', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);
			$dnota 		= $this->input->post('dnota', TRUE);
      if($dnota!=''){
				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
#        $thbl=substr($th,2,2).$bl;
        $thbl=$th.$bl;
        $tbl=substr($th,2,2).$bl;
			}
      $tmp="FK-".$tbl."-";
      $ifakturkomersial	= "FK-".$tbl."-".$this->input->post('ifakturkomersial', TRUE);
      if($ifakturkomersial!=$tmp){
        $this->db->trans_begin();
			  if(strlen($ifakturkomersial)==14){
  #        $ifakturkomersial=$this->mmaster->runningnumber($iarea,$thbl);
				  $this->mmaster->updatenota($inota,$iarea,$ifakturkomersial);
			  }
			  if ($this->db->trans_status() === FALSE)
			  {
				  $this->db->trans_rollback();
			  }
			  else
			  {
  				$this->db->trans_commit();
#				  $this->db->trans_rollback();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Input Faktur Komersial No'.$ifakturkomersial.' Nota:'.$inota;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );  

				  $data['sukses']			= true;
				  $data['inomor']	= $ifakturkomersial;
				  $this->load->view('nomor',$data);
			  }
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
