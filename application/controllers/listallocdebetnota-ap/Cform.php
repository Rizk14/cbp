<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
			){
			$data['page_title'] = $this->lang->line('listallocdebetnota-ap');
			$data['dfrom']='';
			$data['dto']='';
			$data['isupplier'] = '';
		$this->load->view('listallocdebetnota-ap/vmainform', $data);
	}else{
		$this->load->view('awal/index.php');
	}
}
function view()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$cari		= strtoupper($this->input->post('cari'));
		$cari 		=  str_replace("'","",$cari);
		$dfrom		= $this->input->post('dfrom');
		$dto		= $this->input->post('dto');
		$isupplier  = $this->input->post('isupplier');

		if($dfrom=='') $dfrom=$this->uri->segment(4);
		if($dto=='') $dto=$this->uri->segment(5);
		if($isupplier=='') $isupplier = $this->uri->segment(6);

		$config['base_url'] = base_url().'index.php/listallocdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
		if($isupplier == 'all'){
			$q = " 
				SELECT 
					a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
					a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
				FROM 
					tm_alokasidn a 
					INNER JOIN tr_area b ON (a.i_area = b.i_area)
					INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
				WHERE
					(UPPER(a.i_alokasi) LIKE ('%$cari%')
					OR UPPER(a.i_supplier) LIKE ('%$cari%')
					OR UPPER(a.i_dn) LIKE ('%$cari%')
					OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
					AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
					AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
				ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
			";
		}else{
			$q = "
				SELECT 
					a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
					a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
				FROM 
					tm_alokasidn a 
					INNER JOIN tr_area b ON (a.i_area = b.i_area)
					INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
				WHERE
					(UPPER(a.i_alokasi) LIKE ('%$cari%')
					OR UPPER(a.i_supplier) LIKE ('%$cari%')
					OR UPPER(a.i_dn) LIKE ('%$cari%')
					OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
					AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
					AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
					AND a.i_supplier = '$isupplier'
				ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
			";
		}

		$query = $this->db->query($q,false);
		$config['total_rows'] = $query->num_rows(); 
		if($cari!=NULL){
			$config['per_page'] = '40';
		}else{
			$config['per_page'] = '10';
		}

		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(8);
		$this->paginationxx->initialize($config);
		$data['page_title'] = $this->lang->line('listallocdebetnota-ap');
		$this->load->model('listallocdebetnota-ap/mmaster');
		$data['cari']		= $cari;
		$data['dfrom']		= $dfrom;
		$data['dto']		= $dto;
		$data['isupplier']  = $isupplier;
		$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
		$data['total']		= $this->mmaster->bacatotal($isupplier,$dfrom,$dto);
		$sess=$this->session->userdata('session_id');
		$id=$this->session->userdata('user_id');
		$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs		= pg_query($sql);
		if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
				$ip_address	  = $row['ip_address'];
				break;
			}
		}else{
			$ip_address='kosong';
		}
		$query 	= pg_query("SELECT current_timestamp as c");
		while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
		}
		$pesan='Membuka Data Alokasi Debet Nota A/P '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
		$this->load->model('logger');
		$this->logger->write($id, $ip_address, $now , $pesan );  
		$this->load->view('listallocdebetnota-ap/vmainform', $data);
	}else{
		$this->load->view('awal/index.php');
	}
}
	function insert_fail()
	{
		if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listallocdebetnota-ap');
			$this->load->view('listallocdebetnota-ap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function delete()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ialokasi	= $this->uri->segment(4);
			$idn		= $this->uri->segment(5);
			$isupplier	= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$iarea 		= $this->uri->segment(9);
			$this->load->model('listallocdebetnota-ap/mmaster');
			$this->db->trans_begin();
			$this->mmaster->delete($ialokasi,$idn,$iarea);
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}else{
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Hapus Alokasi Debet Nota A/P No:'.$ialokasi.' Supplier:'.$isupplier;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan ); 
#						$this->db->trans_rollback();
				$this->db->trans_commit();
			}
			$cari		= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/listallocdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';

			$query = $this->db->query("
				SELECT 
					* 
				FROM 
					tm_alokasidn 
				WHERE 
					(UPPER(i_alokasi) LIKE '%$cari%')
					AND f_alokasi_cancel = 'f'
					AND i_supplier = '$isupplier' 
					AND d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy') 
					AND d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
			", FALSE);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(10);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listallocdebetnota-ap');
			$this->load->model('listallocdebetnota-ap/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']  = $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			$data['total']		= $this->mmaster->bacatotal($isupplier,$dfrom,$dto);
			$this->load->view('listallocdebetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function cari()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$cari		= strtoupper($this->input->post('cari'));
		$dfrom		= $this->input->post('dfrom');
		$dto		= $this->input->post('dto');
		$isupplier	= $this->input->post('isupplier');

		if($dfrom=='') $dfrom=$this->uri->segment(4);
		if($dto=='') $dto=$this->uri->segment(5);
		if($isupplier=='') $isupplier = $this->uri->segment(6);

		$config['base_url'] = base_url().'index.php/listallocdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
		if($isupplier == 'all'){
			$q = " 
				SELECT 
					a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
					a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
				FROM 
					tm_alokasidn a 
					INNER JOIN tr_area b ON (a.i_area = b.i_area)
					INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
				WHERE
					(UPPER(a.i_alokasi) LIKE ('%$cari%')
					OR UPPER(a.i_supplier) LIKE ('%$cari%')
					OR UPPER(a.i_dn) LIKE ('%$cari%')
					OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
					AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
					AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
				ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
			";
		}else{
			$q = "
				SELECT 
					a.i_alokasi, a.i_dn, a.i_area, b.e_area_name, a.d_alokasi, 
					a.i_supplier, c.e_supplier_name, a.v_jumlah, a.v_lebih, a.f_alokasi_cancel
				FROM 
					tm_alokasidn a 
					INNER JOIN tr_area b ON (a.i_area = b.i_area)
					INNER JOIN tr_supplier c ON (a.i_supplier = c.i_supplier)
				WHERE
					(UPPER(a.i_alokasi) LIKE ('%$cari%')
					OR UPPER(a.i_supplier) LIKE ('%$cari%')
					OR UPPER(a.i_dn) LIKE ('%$cari%')
					OR UPPER(c.e_supplier_name) LIKE ('%$cari%')) 
					AND a.d_alokasi >= to_date('$dfrom', 'dd-mm-yyyy')
					AND a.d_alokasi <= to_date('$dto', 'dd-mm-yyyy')
					AND a.i_supplier = '$isuppllier'
				ORDER BY a.i_alokasi, a.d_alokasi, a.i_dn
			";
		}

		$query = $this->db->query("
			SELECT 
				* 
			FROM 
				tm_dn_ap 
			WHERE 
				(UPPER(i_dn) LIKE '%$cari%')
				AND f_close = 'f'
				AND d_dn_ap >= to_date('$dfrom', 'dd-mm-yyyy') 
				AND d_dn_ap <= to_date('$dto', 'dd-mm-yyyy')
		", FALSE);

		$config['total_rows'] = $query->num_rows(); 
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(8);
		$this->paginationxx->initialize($config);
		$data['page_title'] = $this->lang->line('listallocdebetnota-ap');
		$this->load->model('listallocdebetnota-ap/mmaster');
		$data['cari']		= $cari;
		$data['dfrom']		= $dfrom;
		$data['dto']		= $dto;
		$data['iarea']		= $iarea;
		$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
		$data['total']		= $this->mmaster->bacatotal($isupplier,$dfrom,$dto);
		$this->load->view('listallocdebetnota-ap/vmainform', $data);
	}else{
		$this->load->view('awal/index.php');
	}
}
function supplier()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/listallocdebetnota-ap/cform/supplier/index/';
		#$iuser   = $this->session->userdata('user_id');
		$query = $this->db->query(" select * from tr_supplier order by i_supplier", false);

		$config['total_rows'] = $query->num_rows(); 
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(5);
		$this->pagination->initialize($config);
			
		$this->load->model('listallocdebetnota-ap/mmaster');
		$data['page_title'] = $this->lang->line('list_supplier');
		$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
		$this->load->view('listallocdebetnota-ap/vlistsupplier', $data);
	}else{	
		$this->load->view('awal/index.php');
	}
}
function carisupplier()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/listallocdebetnota-ap/cform/supplier/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query = $this->db->query("select * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'))",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('listallocdebetnota-ap/mmaster');
	$data['page_title'] = $this->lang->line('list_supplier');
	$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('listallocdebetnota-ap/vlistsupplier', $data);
}else{
	$this->load->view('awal/index.php');
}
}
}
?>
