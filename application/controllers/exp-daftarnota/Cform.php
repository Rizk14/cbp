<?php
class Cform extends CI_Controller
{
	public $title 	= "Export Daftar Nota";
	public $folder 	= "exp-daftarnota";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
		$this->load->model('exp-daftarnota/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu268') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['folder'] 	= $this->folder;
			$data['iperiode']	= '';

			$this->logger->writenew("Membuka Menu " . $this->title);

			$this->load->view('exp-daftarnota/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu268') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$iperiode	= $this->input->post('iperiode') == '' ? $this->uri->segment(4) : $this->input->post('iperiode');

			$query		= $this->mmaster->baca($iperiode);

			/*$this->db->select("	substring(a.i_nota, 9, 2) as i_area, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.i_spb, a.d_spb, 
                          a.v_nota_netto, a.i_customer, a.i_faktur_komersial, a.i_seri_pajak, a.i_salesman, a.v_nota_gross, 
                          a.v_nota_discounttotal, a.v_nota_netto, a.v_nota_ppn, b.f_customer_pkp, a.f_nota_cancel
                          from tm_nota a, tr_customer b 
                          where a.i_nota like '$no' and a.i_customer=b.i_customer
                          order by a.i_faktur_komersial, a.i_area",false);
#                          order by a.d_nota, a.i_area, a.i_nota",false);*/
			// $query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("DAFTAR SJ-NOTA")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {


				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);

				/*$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);*/

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:O1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'TGLNOTA');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'KODESALES');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'KODELANG');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);				
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NAMA PELANGGAN');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'NONOTA');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'NOSJ');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'KOTOR');
				$objPHPExcel->getActiveSheet()->getStyle('GH1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'POTONG');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'DPP');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'JBERSIH');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'NOFAKTUR');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'FK PAJAK');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'STATUS');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Tanggal Input');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				/*$objPHPExcel->getActiveSheet()->setCellValue('M1', 'NOFAKTUR');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'FK PAJAK');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'STATUS');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);*/
				$i = 2;
				foreach ($query->result() as $row) {

					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':P' . $i
					);

					if ($row->d_sj != '') {
						$tmp = explode('-', $row->d_sj);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_sj = $hr . '-' . $bl . '-' . $th;
					}
					if ($row->d_nota != '') {
						$tmp = explode('-', $row->d_nota);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_nota = $hr . '-' . $bl . '-' . $th;
					}
					if ($row->d_jatuh_tempo != '') {
						$tmp = explode('-', $row->d_jatuh_tempo);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_jatuh_tempo = $hr . '-' . $bl . '-' . $th;
					}
					if ($row->d_spb != '') {
						$tmp = explode('-', $row->d_spb);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_spb = $hr . '-' . $bl . '-' . $th;
					}

					if ($iperiode < '202204') {
						$row->v_nota_ppn = round($row->v_nota_netto / 1.1 * 0.1);
					}
					if ($row->f_nota_cancel == 't') {
						$row->v_nota_ppn = 0;
						$row->v_nota_netto = 0;
						$row->v_nota_gross = 0;
						$row->v_nota_discounttotal = 0;
					}
					/*$objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->v_nota_discounttotal, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->d_sj_receive, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->i_sjk, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->i_faktur_komersial, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
          if($row->f_customer_pkp=='t'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, 'PKP', Cell_DataType::TYPE_STRING);
          }else{
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, 'Non PKP', Cell_DataType::TYPE_STRING);*/

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->d_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_salesman, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_sj, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->v_nota_discounttotal, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->v_nota_dpp, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->i_faktur_komersial, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
					if ($row->f_customer_pkp == 't') {
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, 'PKP', Cell_DataType::TYPE_STRING);
					} else {
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, 'Non PKP', Cell_DataType::TYPE_STRING);
					}
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->d_nota_entry, Cell_DataType::TYPE_STRING);
					$i++;
				}
				$x = $i - 1;
			}

			$this->logger->writenew('Export SJ-Nota periode:' . $iperiode);

			$nama = 'daftarsjnota' . $iperiode . '.xls';

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');

			/* SIMPEN FILE DI LOCAL PROGRAM */
			if (file_exists('excel/00/' . $nama)) {
				@chmod('excel/00/' . $nama, 0777);
				@unlink('excel/00/' . $nama);
			}
			$objWriter->save('excel/00/' . $nama);
			@chmod('excel/00/' . $nama, 0777);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
