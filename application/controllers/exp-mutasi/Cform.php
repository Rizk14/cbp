<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-mutasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('exp-mutasi/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
      $this->mmaster->proses($iperiode,$iarea);
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/view/'.$iperiode.'/'.$store.'/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('exp-mutasi/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('exp-mutasi/mmaster');
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      if(strlen($iarea)==1) $iarea='0'.$iarea;
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/view/'.$iperiode.'/'.$store.'/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('expmutasi');
      $cari='';
      $data['cari']=$cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('exp-mutasi/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('exp-mutasi/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('exp-mutasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')))
		   ){
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-mutasi/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-mutasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) )
		   ){
			$config['base_url'] = base_url().'index.php/exp-mutasi/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-mutasi/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-mutasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('exp-mutasi/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
      $iproduct = $this->uri->segment(6);
			$this->load->model('exp-mutasi/mmaster');     
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= $iperiode;
			$data['iarea']	  = $iarea;
			$data['iproduct']	= $iproduct;
			$data['detail']	= $this->mmaster->detail($iperiode,$iarea,$iproduct);
			$this->load->view('exp-mutasi/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-mutasi/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
      $istore = $this->input->post('istore');
      $istorelocation = $this->input->post('istorelocation');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');

      $a=substr($iperiode,0,4);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;

			if($iarea=='') $iarea=$this->uri->segment(5);
      $query = $this->db->query("select a.i_store, a.e_area_name, b.i_store_location, b.e_store_locationname
                                 from tr_area a, tr_store_location b 
                                 where a.i_store=b.i_store and a.i_store='$istore' and b.i_store_location='$istorelocation'");
      $st=$query->row();
      $namaarea=$st->e_area_name;
      $storeloc=$st->i_store_location;
      $store=$st->i_store;
      $namastoreloc=$st->e_store_locationname;
      if($istore=='AA'){
        $daerah='00';
      }else{
        $daerah=$istore;
      }
      $db_file='spb/'.$daerah.'/gd'.$daerah.$iperiode.'.dbf';
      if(file_exists($db_file)){
        @chmod($db_file, 777);
        @unlink($db_file);
      }
      $dbase_definition = array (
         array ('KODEPROD',  CHARACTER_FIELD,  7),
         array ('NAMAPROD',  CHARACTER_FIELD,  45),
         array ('STOCKOPNAM',  NUMBER_FIELD,  7, 0)
      );
      
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 

      /*$this->db->select("	a.*, b.e_product_name, b.v_product_retail, e.e_product_groupname 
                          from tm_mutasi a, tr_product_price b, tr_product c, tr_product_type d, tr_product_group e
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product and b.i_price_group='00'
                          and a.i_product=c.i_product and c.i_product_type=d.i_product_type and d.i_product_group=e.i_product_group
						              and i_store='$store' and i_store_location='$storeloc' 
                          order by e.e_product_groupname, b.e_product_name ",false);*/#->limit($num,$offset);
#, tr_product_price c  and a.i_product=c.i_product and c.i_price_group='00'
      $this->db->select("	a.*, c.e_product_name, b.v_product_retail, e.e_product_groupname 
                          from tm_mutasi a
                          left join tr_product_price b on (a.i_product=b.i_product and b.i_price_group='00' 
                          and b.i_product_grade=a.i_product_grade)
                          inner join tr_product c on (a.i_product=c.i_product)
                          left join tr_product_type d on (c.i_product_type=d.i_product_type)
                          left join tr_product_group e on (d.i_product_group=e.i_product_group)
                          where e_mutasi_periode = '$iperiode'
                          and i_store='$store' and i_store_location='$storeloc'
                          order by e.e_product_groupname, b.e_product_name",false);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,15,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Gudang : '.$namaarea.'    Lokasi : '.$namastoreloc.'     Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,15,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:P6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,5,6,5);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'PENERIMAAN');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7,5,9,5);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Stok');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', '+/-');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'd/Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Ret Outlet');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'in lain-lain');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Penj.');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
        if($iarea=='AA' or $iarea=='00'){
          $judul='KeCabang';
        }else{
          $judul='KePusat';
        }
				$objPHPExcel->getActiveSheet()->setCellValue('I6', $judul);
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Ke Pabrik');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Opnam');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', '( pc )');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'GiT');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O6', 'GiT Pnjln');
				$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('P6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;
        $no=0;
        $group='';
				foreach($query->result() as $row){
          if($group=='' || $group!=$row->e_product_groupname){
    	      $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,15,$i);
				    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->e_product_groupname);
				    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					    array(
						    'borders' => array(
							    'top' 	=> array('style' => Style_Border::BORDER_THIN)
						    ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
					    )
				    );
            $no=0;
            $i++;
          }
          $group=$row->e_product_groupname;
          $no++;
          $opname=$row->n_saldo_stockopname+$row->n_mutasi_git+$row->n_git_penjualan;
          $selisih=$opname-$row->n_saldo_akhir;
          $saldoawal=$row->n_saldo_awal+$row->n_mutasi_gitasal+$row->n_git_penjualanasal;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':P'.$i
				  );

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldoawal);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_mutasi_pembelian);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_mutasi_returoutlet);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_mutasi_returpabrik);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $opname);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $selisih);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->n_mutasi_git);
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->n_git_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->v_product_retail);
					$i++;
					$j++;
          
          $isi = array ($row->i_product,$row->e_product_name,0);
          dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 

				}
        dbase_close($id);
        @chmod($db_file, 0777);
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='gd'.$daerah.$iperiode.'.xls';
      if(file_exists('spb/'.$daerah.'/'.$nama)){
        @chmod('spb/'.$daerah.'/'.$nama, 0777);
        @unlink('spb/'.$daerah.'/'.$nama);
      }
			$objWriter->save("spb/".$daerah.'/'.$nama); 
#      chmod('spb/'.$daerah.'/'.$nama, 777);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Mutasi Periode:'.$iperiode.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Laporan Mutasi Stok";
      @chmod('spb/'.$daerah.'/'.$nama, 0777);
      @chmod($db_file, 0777);
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
