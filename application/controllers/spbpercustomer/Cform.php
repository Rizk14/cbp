<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu95')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbpercustomer');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('spbpercustomer/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu95')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbpercustomer/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
										b.e_area_name, a.i_customer, c.e_customer_name
										from tm_spb a, tr_area b, tr_customer c
										where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
										and a.i_area=b.i_area
										and a.i_customer=c.i_customer
										group by a.i_area, b.e_area_name, a.i_customer, c.e_customer_name 
										order by a.i_area, a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpercustomer/mmaster');
			$data['page_title'] = $this->lang->line('spbpercustomer');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7));

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Per Pelanggan dari tanggal: '.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('spbpercustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu95')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbpercustomer/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
										b.e_area_name, a.i_customer, c.e_customer_name
										from tm_spb a, tr_area b, tr_customer c
										where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
										and(upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%'
										or upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
										and a.i_area=b.i_area
										and a.i_customer=c.i_customer
										group by a.i_area, b.e_area_name, a.i_customer, c.e_customer_name 
										order by a.i_area, a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpercustomer/mmaster');
			$data['page_title'] = $this->lang->line('spbpercustomer');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->cariperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('spbpercustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
