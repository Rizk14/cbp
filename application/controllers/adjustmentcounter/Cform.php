<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iadj 		= $this->input->post('iadj', TRUE);
			$dadj 		= $this->input->post('dadj', TRUE);
			if($dadj!=''){
				$tmp=explode("-",$dadj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dadj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$icustomer    = $this->input->post('icustomer', TRUE);
			$eremark	    = $this->input->post('eremark', TRUE);
			$istockopname	= $this->input->post('istockopname', TRUE);
			$jml		      = $this->input->post('jml', TRUE);
#			if($dadj!='' && $istockopname!='' && $eremark!='' && $iarea!='')
			if($dadj!='' && $icustomer!='')
			{
				$this->db->trans_begin();
				$this->load->model('adjustmentcounter/mmaster');
				$iadj	=$this->mmaster->runningnumber($thbl,$icustomer);
				$this->mmaster->insertheader($iadj, $icustomer, $dadj, $istockopname, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= $this->input->post('grade'.$i, TRUE);
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
#				  if($nquantity>0){
				    $this->mmaster->insertdetail($iadj,$icustomer,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i);
#				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Input Adjustment Counter No:'.$iadj.' Counter'.$icustomer;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iadj;
					$this->load->view('nomor',$data);
			    $this->db->trans_commit();
#			    $this->db->trans_rollback();
				}
			}else{
				$data['page_title'] = $this->lang->line('adjustmentcounter');
				$data['iadj']='';
        $data['cari']='';
				$this->load->model('adjustmentcounter/mmaster');
				$data['isi']="";
				$data['detail']="";
				$data['jmlitem']="";
				$data['tgl']=date('d-m-Y');
				$this->load->view('adjustmentcounter/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('adjustmentcounter');
			$this->load->view('adjustmentcounter/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('adjustmentcounter')." update";
			if($this->uri->segment(4)!=''){
				$iadj     = $this->uri->segment(4);
				$icustomer= $this->uri->segment(5);
				$dfrom    = $this->uri->segment(6);
				$dto 	    = $this->uri->segment(7);
				$data['icustomer'] = $icustomer;
				$data['iadj'] = $iadj;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$query = $this->db->query("select i_product from tm_adjmo_item where i_adj='$iadj' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('adjustmentcounter/mmaster');
				$data['isi']=$this->mmaster->baca($iadj,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($iadj,$icustomer);
		 		$this->load->view('adjustmentcounter/vmainform',$data);
			}else{
				$this->load->view('adjustmentcounter/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iadj 		= $this->input->post('iadj', TRUE);
			$dadj 		= $this->input->post('dadj', TRUE);
			if($dadj!=''){
				$tmp=explode("-",$dadj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dadj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$icustomer    = $this->input->post('icustomer', TRUE);
			$eremark	    = $this->input->post('eremark', TRUE);
			$istockopname	= $this->input->post('istockopname', TRUE);
			$jml		      = $this->input->post('jml', TRUE);
			if($dadj!='' && $istockopname!='' && $icustomer!='')
			{
				$this->db->trans_begin();
				$this->load->model('adjustmentcounter/mmaster');
#				$iadj	=$this->mmaster->runningnumber($thbl,$iarea);
				$this->mmaster->updateheader($iadj, $icustomer, $dadj, $istockopname, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= $this->input->post('grade'.$i, TRUE);
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
				  if($nquantity!=0){
				    $this->mmaster->updatedetail($iadj,$icustomer,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i);
				  }else{
				    $this->mmaster->deletedetail($iadj,$icustomer,$iproduct,$iproductmotif,$iproductgrade);	  
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update Adjustment Counter No:'.$iadj.' Customer'.$icustomer;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iadj;
					$this->load->view('nomor',$data);
			    $this->db->trans_commit();
#			    $this->db->trans_rollback();
				}			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $iadj     = $this->uri->segment(4);
      $icustomer= $this->uri->segment(5);
			$dfrom    = $this->uri->segment(6);
			$dto 	    = $this->uri->segment(7);
			$data['iadj']     = $iadj;
			$data['icustomer']= $icustomer;
			$data['dfrom']    = $dfrom;
			$data['dto']	    = $dto;
      
			$this->load->model('adjustmentcounter/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($iadj,$icustomer);
      $sess=$this->session->userdata('session_id');
      $id   = $this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
	      $now	  = $row['c'];
      }
      $pesan='Delete Adjustment Counter No:'.$iadj.' Toko:'.$icustomer;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );
      $this->db->trans_commit();
#	    $this->db->trans_rollback();
##########
      $cari	  = strtoupper($this->input->post('cari'));
			$is_cari	= $this->input->post('is_cari'); 
			
			if($dfrom=='') $dfrom=$this->uri->segment(6);
			if($dto=='') $dto=$this->uri->segment(7);
			
			if ($is_cari == '')
				$is_cari= $this->uri->segment(9);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(8);
		
	    if ($is_cari=="1") { 
		    $config['base_url'] = base_url().'index.php/listadjustmentcounter/cform/view/'.$iadj.'/'.$icustomer.'/'.$dfrom.'/'.$dto.'/'.$cari.'/'.$is_cari.'/index/';
	    }
	    else { 
		    $config['base_url'] = base_url().'index.php/listadjustmentcounter/cform/view/'.$iadj.'/'.$icustomer.'/'.$dfrom.'/'.$dto.'/index/';
	    } 
		  if ($is_cari != "1") {
			  $sql= " select i_bm
            from tm_bm
            tm_bm
            where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')";
		  }
		  else {
			  $sql= " select i_bm
            from tm_bm
            where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
            and (upper(i_bm) like '%$cari%'";
		  }
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(9);
			else
				$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('listadjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('listadjustment');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Bon Masuk Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listadjustmentcounter/vmainform',$data);			
##########
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk			= $this->input->post('ibbkdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('adjustmentcounter/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ibbk, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Delete Item BBK-Hadiah No:'.$ibbk;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['ibbk'] = $ibbk;
			  $data['iarea'] = $iarea;
			  $data['dfrom'] = $dfrom;
			  $data['dto']	 = $dto;
			  $query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ibbk'");
			  $data['jmlitem'] = $query->num_rows(); 				
			  $this->load->model('adjustmentcounter/mmaster');
			  $data['isi']=$this->mmaster->baca($ibbk);
			  $data['detail']=$this->mmaster->bacadetail($ibbk);
	   		$this->load->view('adjustmentcounter/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      if($cari=='' && $this->uri->segment(6)!='sikasep')$cari=$this->uri->segment(6);
      $baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
      $icustomer=$this->input->post("icustomer");
			if($icustomer=='')$icustomer=$this->uri->segment(5);
			if($cari==''){
	  		$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/product/'.$baris.'/'.$icustomer.'/sikasep/';
			}else{
  			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/product/'.$baris.'/'.$icustomer.'/'.$cari.'/';
  	  }
			$query = $this->db->query(" select a.i_product
						                      from tr_product_motif a,tr_product c, tm_ic_consigment d
						                      where a.i_product=c.i_product and c.i_product=d.i_product and d.i_customer='$icustomer'
						                      and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacaproduct($icustomer,$config['per_page'],$this->uri->segment(7),$cari);
			$data['icustomer']=$icustomer;
			$data['cari']=$cari;
			$this->load->view('adjustmentcounter/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('adjustmentcounter/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentcounter/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ibbk']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('adjustmentcounter/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$data['baris']=$this->uri->segment(4);
#			$baris=$this->uri->segment(4);
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $peraw=$this->input->post("peraw");
      $perak=$this->input->post("perak");
      $area =$this->input->post("area");
			if($baris=='')$baris=$this->uri->segment(4);
      if($peraw=='')$peraw=$this->uri->segment(5);
      if($perak=='')$perak=$this->uri->segment(6);
      if($area=='')$area=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/productupdate/'.$baris.'/'.$peraw.'/'.$perak.'/'.$area.'/';

#			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$peraw,$perak,$cari);
			$data['baris']=$baris;
      $data['peraw']=$peraw;
      $data['perak']=$perak;
      $data['area']=$area;
			$this->load->view('adjustmentcounter/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('adjustmentcounter/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('adjustmentcounter/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('adjustmentcounter/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/customer/'.$baris.'/sikasep/';
			$query = $this->db->query(" select i_customer, e_customer_name from tr_customer
                                  where substring(i_customer,1,2)='PB' and f_customer_aktif='t'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6));
			$this->load->view('adjustmentcounter/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris   = $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/customer/'.$baris.'/'.$cari.'/';
			  else
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/customer/'.$baris.'/sikasep/';

			$query = $this->db->query(" select i_customer, e_customer_name 
                                  from tr_customer
                                  where substring(i_customer,1,2)='PB' and f_customer_aktif='t' 
                                  and (upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%')
                                  group by i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('adjustmentcounter/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function so()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu596')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/adjustmentcounter/cform/so/'.$icustomer.'/';
			$query= $this->db->query("select i_sopb from tm_sopb where f_sopb_cancel='f' and i_customer= '$icustomer'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('adjustmentcounter/mmaster');
			$data['page_title'] = $this->lang->line('list_so');
			$data['isi']=$this->mmaster->bacaso($config['per_page'],$this->uri->segment(5),$icustomer);
			$this->load->view('adjustmentcounter/vlistso', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
