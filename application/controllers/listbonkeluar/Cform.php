<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listbonkeluar/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('listbonkeluar');
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('listbonkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbonkeluar');
			$this->load->view('listbonkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibk		= $this->uri->segment(4);
			$dfrom	= $this->uri->segment(5);
			$dto		= $this->uri->segment(6);
			$this->load->model('listbonkeluar/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ibk);
      if (($this->db->trans_status() === FALSE))
			{
				$this->db->trans_rollback();
			}else{
        $is_cari	= $this->input->post('is_cari'); 
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Delete Bon Masuk No:'.$ibk;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['page_title'] = $this->lang->line('listbonkeluar');
#			  $config['base_url'] = base_url().'index.php/listbonkeluar/cform/index/';
        $config['base_url'] = base_url().'index.php/listbonkeluar/cform/view/'.$dfrom.'/'.$dto.'/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  $sql="select i_bm from tm_bm where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')";
			  $query 	= $this->db->query($sql,false);
			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(7);
			  $this->pagination->initialize($config);
			  $data['page_title'] = $this->lang->line('listbonkeluar');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			  $this->load->view('listbonkeluar/vmainform',$data);	
				$this->db->trans_commit();
#				$this->db->trans_rollback();
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listbonkeluar/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from v_list_do a
										              left join tm_spb b on (b.i_spb=a.i_spb and b.i_area=a.i_spb_area)
										              left join tm_spmb c on (c.i_spmb=a.i_spmb and c.i_area=a.i_spmb_area)
										              where 
										              upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
										              or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
										              or upper(a.i_spmb) like '%$cari%' or upper(a.i_spb) like '%$cari%'
										              or upper(c.i_spmb_old) like '%$cari%' or upper(b.i_spb_old) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listbonkeluar/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listbonkeluar');
			$data['cari'] = '';
			$data['ido']='';
	 		$this->load->view('listbonkeluar/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbonkeluar/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
						
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listbonkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listbonkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listbonkeluar/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listbonkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listbonkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu427')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$is_cari	= $this->input->post('is_cari'); 
			
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			
			if ($is_cari == '')
				$is_cari= $this->uri->segment(7);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(6);
		
	    if ($is_cari=="1") { 
		    $config['base_url'] = base_url().'index.php/listbonkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$cari.'/'.$is_cari.'/index/';
	    }
	    else { 
		    $config['base_url'] = base_url().'index.php/listbonkeluar/cform/view/'.$dfrom.'/'.$dto.'/index/';
	    } 
		  if ($is_cari != "1") {
			  $sql= " select i_bm
            from tm_bm
            tm_bm
            where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')";
		  }
		  else {
			  $sql= " select i_bm
            from tm_bm
            where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
            and (upper(i_bm) like '%$cari%')";
		  }
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(9);
			else
				$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			
			$this->load->model('listbonkeluar/mmaster');
			$data['page_title'] = $this->lang->line('listbonkeluar');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Bon Masuk Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listbonkeluar/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
