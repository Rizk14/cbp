<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $config['base_url'] = base_url().'index.php/areacover/cform/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  
			  $query = $this->db->query("	select * from tr_area_cover where upper(tr_area_cover.i_area) like '%$cari%' and f_ac_cancel='t' ",false);
        
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(4);
			  $this->paginationxx->initialize($config);
			  
			  $data['page_title'] = $this->lang->line('master_areacover');
			  $data['iarea']='';
			  $data['iareacover']='';
			  $data['eareacovername']='';
			  $this->load->model('areacover/mmaster');
			  $data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			  $sess	= $this->session->userdata('session_id');
			  $id 	= $this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
			  	while($row=pg_fetch_assoc($rs)){
			  		$ip_address	  = $row['ip_address'];
			  		break;
			  	}
			  }else{
			  	$ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
			  	$now	  = $row['c'];
			  }
			  $pesan="Membuka Menu Master Area Cover";
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now, $pesan );

			  $this->load->view('areacover/vmainform', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}

/*	function coverarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$config['base_url'] = base_url().'index.php/areacover/cform/index/';
			$iarea = strtoupper($iarea);
			$query2= $this->db->query("select i_area_cover from tr_area_cover where upper(tr_area_cover.i_area) like '%$iarea%'",false);

			$this->load->model('areacover/mmaster');
			$data['isi2']=$this->mmaster->bacaareacover($iarea,$config['per_page'],$this->uri->segment(4));
			$iareacover=$data['isi2'];
		}
	}
*/		
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 		= $this->input->post('iarea', TRUE);
			$iareacover 		= $this->input->post('iareacover', TRUE);
			$eareacovername 		= $this->input->post('eareacovername', TRUE);

			if ((isset($iarea) && $iarea != '') 
			    && (isset($iareacover) && $iareacover != '')
			    && (isset($eareacovername) && $eareacovername != ''))
			{
					
				$this->load->model('areacover/mmaster');	
				$this->mmaster->insert($iarea,$iareacover,$eareacovername);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Area Cover:('.$iarea.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );

		        $config['base_url'] = base_url().'index.php/areacover/cform/index/';
				$cari = strtoupper($this->input->post('cari', FALSE));

				$query = $this->db->query("	select * from tr_area_cover where upper(tr_area_cover.i_area) like '%$cari%' and f_ac_cancel='t' ",false);

				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_areacover');
				$data['iarea']='';
				$data['iareacover']='';
				$data['eareacovername']='';
				$this->load->model('areacover/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('areacover/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_areacover');
			$this->load->view('areacover/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cancel()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->uri->segment(4);
			$iareacover 	= $this->uri->segment(5);
			$this->load->model('areacover/mmaster');
			$this->mmaster->cancel($iarea,$iareacover);
			
			$config['base_url'] = base_url().'index.php/areacover/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_area_cover 
										where upper(tr_area_cover.i_area) like '%$cari%'
										or upper(tr_area_cover.i_area_cover) like '%$cari%'
									    order by tr_area_cover.i_area, tr_area_cover.i_area_cover ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_areacover');
			$data['iarea']='';
			$data['iareacover']='';
			$data['eareacovername']='';
			$this->load->model('areacover/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('areacover/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/areacover/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_area 
							where upper(i_area) like '%$cari%'
							or upper(e_area_name) like '%$cari%'
							order by i_area ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('areacover/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5)); 
			$this->load->view('areacover/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $config['base_url'] = base_url().'index.php/areacover/cform/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  $query = $this->db->query("	select * from tr_area_cover where upper(tr_area_cover.i_area) like '%$cari%' 
											order by tr_area_cover.i_area",false);
        
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(4);
			  $this->paginationxx->initialize($config);
			  $this->load->model('areacover/mmaster');
			  $data['isi']		=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			  $data['page_title'] 	=$this->lang->line('master_areacover');
			  $data['iarea']='';
			  $data['iareacover']='';
			  $data['eareacovername']='';
	   		$this->load->view('areacover/vmainform',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/areacover/cform/cariarea/index/'; 
			$cari = $this->input->get_post('cari');
			$cari = strtoupper($cari);  
			$query = $this->db->query("select i_area from tr_area where i_area like '%$cari%' or e_area_name like '%$cari%'",false); 
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('areacover/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('areacover/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
