<?php 

class Upload extends CI_Controller {
	
	function __construct()
	// function Upload()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	function index()
	{	
		if ($this->session->userdata('logged_in')){
			$this->load->view('soupload/upload_form', array('error' => ' ' ));
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function so_upload()
	{
		if ($this->session->userdata('logged_in')){
      $store=$this->session->userdata('store');
      if($store=='AA')$store='00';
			$config['upload_path'] = 'so/'.$store.'/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
      $field='userfile';
			if ( ! $this->upload->do_upload($field))
			{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('soupload/upload_form', $error);
			}	
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$this->load->view('soupload/upload_success', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
