<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			#			$data['iperiode']='';
			$data['dfrom'] = '';
			$data['dto'] = '';
			$data['iarea'] = '';
			$this->load->view('listspbrealisasi/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if ($iarea == '') $iarea	= $this->uri->segment(4);
			if ($dfrom == '') $dfrom	= $this->uri->segment(5);
			if ($dto == '') $dto	= $this->uri->segment(6);
			$this->load->model('listspbrealisasi/mmaster');
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;

			#			$interval	= $this->mmaster->interval($dfrom,$dto);
			#      $data['interval'] = $interval;
			#			$data['isi']		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
			$data['isi']		  = $this->mmaster->bacaperiode($dfrom, $dto, $iarea);
			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			#			$pesan='Membuka Data Penjualan Per Pelanggan Periode:'.$iperiode;
			$pesan = 'Membuka Data Penjualan Per Pelanggan Periode:' . $dfrom . ' s/d ' . $dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('listspbrealisasi/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			$this->load->view('listspbrealisasi/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('listspbrealisasi/mmaster');
			$this->mmaster->delete($inota, $ispb, $iarea);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Menghapus Penjualan Per Pelanggan dari tanggal:' . $dfrom . ' sampai:' . $dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/listspbrealisasi/cform/view/' . $inota . '/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $cari);
			$this->load->view('listspbrealisasi/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);
			if ($cari == '') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url() . 'index.php/listspbrealisasi/cform/cari/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $cari . '/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listspbrealisasi/mmaster');
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);
			$this->load->view('listspbrealisasi/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));
			$config['base_url'] = base_url() . 'index.php/listspbrealisasi/cform/cariperpages/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $keyword . '/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listspbrealisasi/mmaster');
			$data['page_title'] = $this->lang->line('listspbrealisasi');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $keyword);
			$this->load->view('listspbrealisasi/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/listspbrealisasi/cform/area/index/';
			$iuser	= $this->session->userdata('user_id');
			/* $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5'); */
			// if ($area1 == '00') {
			$query = $this->db->query("select * from tr_area where i_area in (select i_area from tm_user_area where i_user = '$iuser' )", false);
			// } else {
			// 	$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
			// 								or i_area = '$area4' or i_area = '$area5'", false);
			// }
			$config['total_rows'] = $query->num_rows();
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspbrealisasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listspbrealisasi/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iuser	= $this->session->userdata('user_id');
			/* $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5'); */
			$config['base_url'] = base_url() . 'index.php/listspbrealisasi/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			// if ($area1 == '00') {
			$query = $this->db->query("select * from tr_area where i_area in (select i_area from tm_user_area where i_user = '$iuser' ) 
											and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false);
			// } else {
			// 	$query 	= $this->db->query("select * from tr_area
			// 					                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
			//   										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
			// 					                   	or i_area = '$area4' or i_area = '$area5')", false);
			// }
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspbrealisasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listspbrealisasi/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu406') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');

			if ($dfrom == '') $dfrom	= $this->uri->segment(4);
			if ($dto == '') $dto		= $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$this->load->model('listspbrealisasi/mmaster');

			$isi = $this->mmaster->bacaperiode($dfrom, $dto, $iarea);

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Realisasi Pemenuhan SPB Periode" . '  ' . $dfrom . " - " . $dto)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if (count($isi) > 0) {
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30); //Area
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15); //SPB
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10); //Tanggal SPB
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12); //Stok Daerah
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14); //Kode Toko
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35); //Nama Toko
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18); //Sales
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10); //Divisi
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12); //Kode Barang
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35); //Nama Barang
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(5); //Pesan
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15); //Netto
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5); //Kirim
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15); //Netto
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(7); //Pending
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15); //Netto
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(13); //Harga
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(18); //Status
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(18); //No SJ
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(10); //Tanggal SJ
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(18); //No Nota
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(10); //Tanggal Nota
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12); //SPB App 1
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12); //Tanggal App 1
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12); //Approve 1
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(12); //SPB App 2
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12); //Tanggal App 2
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12); //Approve 2
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(8); //SPB-SJ
				$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(8); //SJ-DKB
				$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(10); //DKB-Nota
				$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(10); //SPB-DKB
				$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(25); //Keterangan

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => false
						)
					),
					'A1:AG1'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'SPB');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Tgl SPB');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Stock Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Kode Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Nama Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Sls');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Divisi');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Kode Prod');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Nama Prod');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Pesan');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Netto');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Netto');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Pending');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Netto');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'No SJ');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('T1', 'Tgl SJ');
				$objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('U1', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('V1', 'Tgl Nota');
				$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('W1', 'SPB Approve 1');
				$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('X1', 'Tgl Approve 1');
				$objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'Approve');
				$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'SPB Approve 2');
				$objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'Tgl Approve 2');
				$objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'Approve');
				$objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'SPB-SJ');
				$objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'SJ-DKB');
				$objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AE1', 'DKB-Nota');
				$objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AF1', 'SPB-DKB');
				$objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AG1', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i = 2;
				$j = 1;

				$vpesan 		= 0;
				$vkirim 		= 0;
				$vpending 		= 0;

				$vpesanrp 		= 0;
				$vkirimrp 		= 0;
				$vpendingrp		= 0;
				foreach ($isi as $row) {
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),

						'A' . $i . ':AG' . $i
					);

					if ($row->f_spb_stockdaerah == 't') {
						$stok = "Ya";
					} else {
						$stok = "Tidak";
					}

					$dspb 		= strtotime($row->d_spb);
					$dsales  	= strtotime($row->d_approvesales);
					$dkeuangan  = strtotime($row->d_approve);
					$sj  		= strtotime($row->d_sj);
					$dkb 		= strtotime($row->d_dkb);
					$nota  		= strtotime($row->d_nota);

					$pesan	 	= $row->n_order;
					$kirim 		= $row->n_deliver;
					$pending 	= $kirim - $pesan;

					$vpesan 	+= $pesan;
					$vkirim 	+= $kirim;
					$vpending 	+= $pending;

					//HITUNG JML HARI SPB APPROVE 1		
					if ($dsales != '' && $dsales > $dkeuangan) {
						$spb1 	= $dkeuangan - $dspb;
						$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
					} else if ($dsales != '' && $dsales < $dkeuangan) {
						$spb1 	= $dsales - $dspb;
						$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
					} else if ($dsales != '' && $dsales == $dkeuangan) {
						$spb1 	= $dsales - $dspb;
						$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
					} else if ($dsales == '' && $dkeuangan != '') {
						$spb1 	= $dkeuangan - $dspb;
						$dspb1	= floor($spb1 / (60 * 60 * 24)) . " hari";
					} else {
						$dspb1 	= '-';
					}
					//END JML HARI SPB APPROVE 1

					//SPB APPROVE
					if ($dsales > $dkeuangan /*|| $dsales == $dkeuangan*/) {
						$app1 	= "Keuangan";
						$app2 	= "Sales";
					} else if ($dsales == '' || $dkeuangan == '') {
						$app1 	= '-';
						$app2 	= '-';
					} else if ($dsales == $dkeuangan) {
						$app1 	= "Keuangan";
						$app2 	= "Sales";
					} else {
						$app1 	= "Sales";
						$app2 	= "Keuangan";
					}
					//END HITUNG JML HARI SPB APPROVE 1

					if ($dsales != '') {
						$spbx2 	= $dkeuangan - $dsales;
					}
					//HITUNG JML HARI SPB APPROVE 2
					if ($dkeuangan != '' && $dkeuangan > $dsales) {
						$spb2 	= $dkeuangan - $dspb;
						$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
					} else if ($dkeuangan != '' && $dkeuangan < $dsales) {
						$spb2 	= $dsales - $dspb;
						$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
					} else if ($dkeuangan != '' && $dkeuangan == $dsales) {
						$spb2 	= $dkeuangan - $dspb;
						$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
					} else if ($dkeuangan == '' && $dsales != '') {
						$spb2 	= $dsales - $dspb;
						$dspb2	= floor($spb2 / (60 * 60 * 24)) . " hari";
					} else {
						$dspb2 	= '-';
					}
					//END JML HARI SPB APPROVE 1

					if ($app1 == 'Keuangan') {
						$tglapp1	= $row->d_approve;
						$tglapp2	= $row->d_approvesales;
					} else {
						$tglapp1	= $row->d_approvesales;
						$tglapp2	= $row->d_approve;
					}

					//HITUNG JML HARI SPB - SJ
					if ($sj != '') {
						if ($spbx2 < 0) {
							$salesj	= $sj - $dsales;
							$dspbsj	= floor($salesj / (60 * 60 * 24)) . " hari";
						} else {
							$salesj	= $sj - $dkeuangan;
							$dspbsj	= floor($salesj / (60 * 60 * 24)) . " hari";
						}
					} else {
						$dspbsj	= '-';
					}
					//END HITUNG JML HARI SPB - SJ

					//HITUNG JML HARI SJ - DKB
					if ($dkb != '') {
						$sjdkb 	= $dkb - $sj;
						$dsjdkb	= floor($sjdkb / (60 * 60 * 24)) . " hari";
					} else {
						$dsjdkb	= '-';
					}
					//END HITUNG JML HARI SJ - DKB

					//HITUNG JML HARI DKB - Nota
					if ($nota != '') {
						$dkbnota 	= $nota - $dkb;
						$ddkbnota	= floor($dkbnota / (60 * 60 * 24)) . " hari";
					} else {
						$ddkbnota	= '-';
					}
					//END HITUNG JML HARI DKB - Nota

					//HITUNG JML HARI SPB - DKB
					if ($dkb != '') {
						$spdkb	= $dkb - $dsp;
						$spdkb	= floor($spdkb / (60 * 60 * 24)) . " hari";
						#$spdkb	= $sjdkb + $spb2 . " hari";
						#$dsjdkb	= floor($sjdkb / (60 * 60 * 24))." hari";
					} else {
						$spdkb	= '-';
					}
					//END HITUNG JML HARI SPB - DKB	

					if ($row->n_deliver == '') $row->n_deliver = 0;
					if (
						($row->f_spb_cancel == 't')
					) {
						$status = 'Batal';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove == null)
					) {
						$status = 'Sales';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove != null)
					) {
						$status = 'Reject (sls)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &
						($row->i_notapprove == null)
					) {
						$status = 'Keuangan';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &&
						($row->i_notapprove != null)
					) {
						$status = 'Reject (ar)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store == null)
					) {
						$status = 'Gudang';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					) {
						$status = 'Pemenuhan SPB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					) {
						$status = 'Proses OP';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					) {
						$status = 'OP Close';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					) {
						$status = 'Siap SJ (sales)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						#			  	$status='Siap SJ (gudang)';
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) &&
						($row->i_approve2 != null) &&
						($row->i_store != null) &&
						($row->i_nota != null)
					) {
						$status = 'Sudah dinotakan';
					} elseif (($row->i_nota != null)) {
						$status = 'Sudah dinotakan';
					} else {
						$status = 'Unknown';
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_area . "-" . $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_spb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->d_spb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $stok, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_salesman . " - " . $row->e_salesman_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->e_product_groupname, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->vpesan, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->n_deliver, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->vkirim, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, $pending, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->vpending, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('P' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('Q' . $i, $row->v_unit_price, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('Q' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('R' . $i, $status, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('S' . $i, $row->i_sj, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('T' . $i, $row->d_sj, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('U' . $i, $row->i_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('V' . $i, $row->d_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('W' . $i, $dspb1, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('X' . $i, $tglapp1, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('Y' . $i, $app1, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('Z' . $i, $dspb2, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AA' . $i, $tglapp2, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AB' . $i, $app2, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AC' . $i, $dspbsj, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AD' . $i, $dsjdkb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AE' . $i, $ddkbnota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AF' . $i, $spdkb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('AG' . $i, $row->e_remark, Cell_DataType::TYPE_STRING);
					$j++;
					$i++;
				}

				$nama = 'realisasispb(' . $dfrom . '-' . $dto . '.xls';

				// Proses file excel    
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
				header('Cache-Control: max-age=0');

				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output', 'w');

				// if (file_exists('spb/' . $nama)) {
				// 	@chmod('spb/' . $nama, 0777);
				// 	@unlink('spb/' . $nama);
				// }
				// $objWriter->save('spb/00/' . $nama);
				// @chmod('spb/00/' . $nama, 0777);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {

						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {

					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {

					$now	  = $row['c'];
				}
				$pesan = 'Export Realisasi SPB' . ' ' . $dfrom . ' - ' . $dto;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				// $data['sukses']			= true;
				// $data['inomor']			= $pesan;
				// $this->load->view('nomor', $data);
			} else {
				$this->load->view('awal/index.php');
			}
		}
	}
}
