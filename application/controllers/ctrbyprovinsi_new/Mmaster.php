<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mmaster extends CI_Model{
	public function __construct()
    {
        parent::__construct();
		#$this->CI =& get_instance();
    }
    function baca($tahun,$group)
    {
      $prevth=$tahun-1;
      if($group=='NA'){
	    $sql=" a.e_area_island ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(a.oa) as oa , sum(a.prevvnota)  as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa from (
select a.i_periode, a.e_area_island, sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(a.oa) as oa , sum(a.prevvnota)  as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa from (
        /*============================== Start This Year============================================*/
        /*Hitung Rp.Nota*/
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
                          sum(a.v_nota_netto)  as vnota, 0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all

                          /*Hitung Qty */
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
                          0  as vnota, sum(c.n_deliver) as qnota, 0 as oa , 0 as prevvnota , 0 as prevqnota, 0 as prevoa
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all

                          /*Hitung OA*/
                          select a.i_periode , a.e_area_island ,
                          0 as vnota , 0 as qnota , count(a.oa) as oa , 0 as prevvnota , 0 as prevqnota, 0 as prevoa from (
                          select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , b.e_area_island
                          from tm_nota a , tr_area b where to_char(a.d_nota,'yyyy')='$tahun' and a.f_nota_cancel='false'
                          and not a.i_nota isnull and a.i_area=b.i_area 
                          ) as a
                          group by a.i_periode , a.e_area_island
                          union all
        /*=============================================End This Year=============================================*/
        /*=============================================Start Prev Year===========================================*/
        
                         /*Hitung Rp.Nota*/
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island,
                           0  as vnota, 0 as qnota, 0 as oa , sum(a.v_nota_netto) as prevvnota , 0 as prevqnota, 0 as prevoa
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all

                          /*Hitung Qty */
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
                          0 as vnota, 0 as qnota, 0 as oa , 0 as prevvnota ,sum(c.n_deliver) as prevqnota, 0 as prevoa
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all

                          /*Hitung OA*/
                          select a.i_periode , a.e_area_island , 
                          0 as vnota , 0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota, count(oa) as prevoa from (
                          select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , b.e_area_island
                          from tm_nota a , tr_area b where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
                          and not a.i_nota isnull and a.i_area=b.i_area 
                          ) as a
                          group by a.i_periode , a.e_area_island

        
                          ) as a
                          group by a.i_periode, a.e_area_island
) as a      
group by a.e_area_island
order by a.e_area_island";
    }else{
      $sql =" a.e_area_island ,sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(a.oa) as oa , sum(a.prevvnota)  as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa , a.e_product_groupname ,a.i_product_group from (
    select a.i_periode, a.e_area_island, sum(a.vnota)  as vnota, sum(qnota) as qnota , sum(a.oa) as oa , sum(a.prevvnota)  as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa, a.e_product_groupname ,a.i_product_group from (
        /*============================== Start This Year============================================*/
      /*//Hitung Rp.Nota*/
        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
        sum(a.v_nota_netto)  as vnota, 0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa , d.e_product_groupname , c.i_product_group
        from tm_nota a, tr_area b , tm_spb c , tr_product_group d
        where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
        and a.i_area=c.i_area and a.i_nota=c.i_nota and c.f_spb_cancel='false' and c.i_product_group=d.i_product_group
        group by to_char(a.d_nota,'yyyy'), b.e_area_island,d.e_product_groupname , c.i_product_group
        union all

        /*Hitung Qty */
        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
        0  as vnota, sum(c.n_deliver) as qnota, 0 as oa , 0 as prevvnota , 0 as prevqnota, 0 as prevoa , e.e_product_groupname , d.i_product_group 
        from tm_nota a, tr_area b, tm_nota_item c , tm_spb d , tr_product_group e
        where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
        and a.i_sj=c.i_sj and a.i_area=c.i_area and a.i_area=d.i_area and a.i_nota=d.i_nota and d.f_spb_cancel='false'
        and d.i_product_group=e.i_product_group
        group by to_char(a.d_nota,'yyyy'), b.e_area_island, e.e_product_groupname , d.i_product_group 
        union all

        /*Hitung OA*/
        select a.i_periode , a.e_area_island ,
        0 as vnota , 0 as qnota , count(a.oa) as oa , 0 as prevvnota , 0 as prevqnota, 0 as prevoa , a.e_product_groupname , a.i_product_group from (
        select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , b.e_area_island, d.e_product_groupname , c.i_product_group
        from tm_nota a , tr_area b , tm_spb c , tr_product_group d where to_char(a.d_nota,'yyyy')='$tahun' and a.f_nota_cancel='false'
        and not a.i_nota isnull and a.i_area=b.i_area  and a.i_area=c.i_area and a.i_nota=c.i_nota and c.f_spb_cancel='false'
        and c.i_product_group=d.i_product_group
        ) as a
        group by a.i_periode , a.e_area_island, a.e_product_groupname , a.i_product_group 
        union all
        /*=============================================End This Year=============================================*/
        /*=============================================Start Prev Year===========================================*/
        
        /*Hitung Rp.Nota*/
        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island,
         0  as vnota, 0 as qnota, 0 as oa , sum(a.v_nota_netto) as prevvnota , 0 as prevqnota, 0 as prevoa, d.e_product_groupname , c.i_product_group
        from tm_nota a, tr_area b , tm_spb c , tr_product_group d
        where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
        and a.i_area=c.i_area and a.i_nota=c.i_nota and c.f_spb_cancel='false' and c.i_product_group=d.i_product_group
        group by to_char(a.d_nota,'yyyy'), b.e_area_island,d.e_product_groupname , c.i_product_group
        union all

        /*Hitung Qty */
        select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 
        0 as vnota, 0 as qnota, 0 as oa , 0 as prevvnota ,sum(c.n_deliver) as prevqnota, 0 as prevoa, e.e_product_groupname , d.i_product_group 
        from tm_nota a, tr_area b, tm_nota_item c , tm_spb d , tr_product_group e
        where to_char(a.d_nota,'yyyy') = '$prevth' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
        and a.i_sj=c.i_sj and a.i_area=c.i_area and a.i_area=d.i_area and a.i_nota=d.i_nota and d.f_spb_cancel='false'
        and d.i_product_group=e.i_product_group
        group by to_char(a.d_nota,'yyyy'), b.e_area_island, e.e_product_groupname , d.i_product_group 
        union all

        /*Hitung OA*/
        select a.i_periode , a.e_area_island , 
        0 as vnota , 0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota, count(oa) as prevoa , a.e_product_groupname , a.i_product_group from (
        select distinct on (to_char(a.d_nota,'yyyy'),a.i_customer) a.i_customer as oa , to_char(a.d_nota,'yyyy') as i_periode , b.e_area_island, d.e_product_groupname , c.i_product_group
        from tm_nota a , tr_area b , tm_spb c , tr_product_group d where to_char(a.d_nota,'yyyy')='$prevth' and a.f_nota_cancel='false'
        and not a.i_nota isnull and a.i_area=b.i_area  and a.i_area=c.i_area and a.i_nota=c.i_nota and c.f_spb_cancel='false'
        and c.i_product_group=d.i_product_group
        ) as a
        group by a.i_periode , a.e_area_island, a.e_product_groupname , a.i_product_group

        
                          ) as a
                          group by a.i_periode, a.e_area_island, a.e_product_groupname ,a.i_product_group
) as a      
        where a.i_product_group='$group'
        group by a.e_area_island, a.e_product_groupname ,a.i_product_group
                          order by a.e_area_island";

    }
      $this->db->select($sql,FALSE);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }
    function bacaisland($tahun)
    {
	    $this->db->select("	a.i_periode, a.e_area_island, sum(a.vnota)  as vnota, sum(qnota) as qnota from (
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, sum(a.v_nota_netto)  as vnota, 0 as qnota
                          from tm_nota a, tr_area b
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          union all
                          select to_char(a.d_nota,'yyyy') as i_periode, b.e_area_island, 0  as vnota, sum(c.n_deliver) as qnota
                          from tm_nota a, tr_area b, tm_nota_item c
                          where to_char(a.d_nota,'yyyy') = '$tahun' and a.f_nota_cancel='f' and not a.i_nota isnull and a.i_area=b.i_area
                          and a.i_sj=c.i_sj and a.i_area=c.i_area
                          group by to_char(a.d_nota,'yyyy'), b.e_area_island
                          ) as a
                          group by a.i_periode, a.e_area_island
                          order by a.e_area_island ",false);
		  $query = $this->db->get();
		  if ($query->num_rows() > 0){
			  return $query->result();
		  }
    }

    function bacaproductgroup()
    {
      $this->db->select(" * from tr_product_group",false);
    
      $query = $this->db->get();
    
      if ($query->num_rows() > 0){
        return $query->result();
      }
    }

}
?>
