<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rtunai');
			$this->load->model('exp-rtunai/mmaster');
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$data['bukti']='';
			$this->load->view('exp-rtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			#$lepel			= $this->session->userdata('i_area');
			$cari		  	= strtoupper($this->input->post('cari'));
			$dfrom			= $this->input->post('dfrom');
			$dto		  	= $this->input->post('dto');
			$iarea		  	= $this->input->post('iarea');
			#$irtunai 		= $this->input->post('bukti');
			#$vjml 			= $this->input->post('vjml');
			#$icoabank	= $this->input->post('icoa');
			#$ebankname		= $this->input->post('ebankname');
			#if($dfrom=='') $dfrom 	= $this->uri->segment(4);
			#if($dto=='') $dto 		= $this->uri->segment(5);
			if($iarea=='') $iarea 		= $this->uri->segment(4);
			#if($irtunai=='') $irtunai 	= $this->uri->segment(5);
			#if($icoabank=='') $icoabank	= $this->uri->segment(6);

			$this->load->model('exp-rtunai/mmaster');
      		#$isi 	= $this->mmaster->baca($dfrom,$dto);
      		#$isi = $this->mmaster->bacadetail($iarea,$irtunai);
      		$isi = $this->mmaster->bacadetail($iarea,$dfrom,$dto);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Bukti Setor Area : ".$iarea)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      		
      		
      		if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => false
					)
				),
				'A1:I5'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN SETOR TUNAI');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,2,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'AREA : '.$iarea);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,2,2);
				#$objPHPExcel->getActiveSheet()->setCellValue('A3', 'NO BUKTI SETOR : '.$irtunai);//.' Total : '.$tt
				#$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,5,3);
       			$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No Setor Tunai');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'No Tunai');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Tunai');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);			
			  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Entry');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
#				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'TOTAL');
#				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,11,1);

/*				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);*/

#        $i=2;
#        $j=1;
		$i=6;
		$j=1;
		$total =0;
		$group='';
      	$nosetoran='';
        
        foreach($isi as $row){
        $total = $total+$row->nnota;

        	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'\''.$row->i_area);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$row->i_rtunai);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->i_tunai);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->d_tunai);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_name);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);	

			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_nota);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);			

			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->nnota,Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);	

			/*$objPHPExcel->getActiveSheet()->setCellValue('E'.$j, '=SUM(E6:E'.($j-1).')');
					$objPHPExcel->getActiveSheet()->getStyle('E'.$j)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);	*/

			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->d_entry);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

		/*	$objPHPExcel->getActiveSheet()->setCellValue('F'.$i+1, '=SUM(E6:E'.($i++).')');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i+1)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);*/
		/*					
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',
				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      #'A'.$i.':H'.$i
		      'A'.$i.':F'.$i
		      );
		      
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_tunai, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_tunai, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->nnota, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_remark, Cell_DataType::TYPE_STRING);
          #$objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, number_format($row->tnota), Cell_DataType::TYPE_NUMERIC);
          /*if($row->f_debet=='t'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->v_bank, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, 0, Cell_DataType::TYPE_NUMERIC);
          }elseif($row->f_debet=='f'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, 0, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->v_bank, Cell_DataType::TYPE_STRING);
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->f_debet, Cell_DataType::TYPE_STRING);*/
          	

			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':I'.$i
				  );
			$j++;
			$i++;

		  $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'GRAND TOTAL');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,0,$i);
          #$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,5,1);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i,$total, Cell_DataType::TYPE_NUMERIC);
          	
          }
          		

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        	#$nama='Setor Tunai : '.$irtunai.' Area : '.$iarea.'.xls';
        	$nama='Setor Tunai Area : '.$iarea.'.xls';
        	if($iarea!='NA'){
              $lokasi=$iarea;
            }else{
              $lokasi='00';
            }
    		if(file_exists('excel/'.$lokasi.'/'.$nama)){
          		@chmod('excel/'.$lokasi.'/'.$nama, 0777);
          		@unlink('excel/'.$lokasi.'/'.$nama);
        	}

			  $objWriter->save('excel/'.$lokasi.'/'.$nama);
			  @chmod('excel/'.$lokasi.'/'.$nama, 0777);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){

					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{

				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  #$pesan='Export Setor Tunai '.$irtunai.' Area : '.$iarea;
			  $pesan='Export Setor Tunai Area : '.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rtunai');
			$this->load->view('exp-rtunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('rtunai')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
        $irtunai          = $this->uri->segment(4);
				$iarea		      = $this->uri->segment(5);
				$dfrom		      = $this->uri->segment(6);
				$dto			      = $this->uri->segment(7);
				$data['irtunai']  = $irtunai;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        		$data['pst']	= $this->session->userdata('i_area');
				$this->load->model("exp-rtunai/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$irtunai);
				$data['detail']=$this->mmaster->bacadetail($iarea,$irtunai);
		 		$this->load->view('exp-rtunai/vformupdate',$data);
			}else{
				$this->load->view('exp-rtunai/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $irtunai  = $this->input->post('irtunai', TRUE);
			$drtunai	= $this->input->post('drtunai', TRUE);
			if($drtunai!=''){
				$tmp=explode("-",$drtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$iarea			      = $this->input->post('iarea', TRUE);
			$ibank			      = $this->input->post('ibank', TRUE);
			$xiarea			      = $this->input->post('xiarea', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$jml          		= $this->input->post('jml', TRUE);
			$jml          		= str_replace(',','',$jml);
			if (
				($drtunai != '') && ($iarea!='') && ($vjumlah!='') && ($vjumlah!='0') && ($ibank!='')
			   )
			{
				$this->load->model('exp-rtunai/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($irtunai,$drtunai,$iarea,$xiarea,$eremark,$vjumlah,$ibank);
				for($i=1;$i<=$jml;$i++){
				  $itunai			= $this->input->post('itunai'.$i, TRUE);
				  $iareatunai	= $this->input->post('iarea'.$i, TRUE);
				  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $vjumlahasallagi	= $this->input->post('vjumlahasallagi'.$i, TRUE);
				  $vjumlahasallagi	= str_replace(',','',$vjumlahasallagi);
#				  $this->mmaster->insertdetail($irtunai,$iarea,$itunai,$iareatunai,$vjumlah,$i);
				  $this->mmaster->updatedetail($irtunai,$iarea,$xiarea,$itunai,$iareatunai,$vjumlah,$i);
 				  $this->mmaster->updatetunaix($irtunai,$iarea,$itunai,$iareatunai,$vjumlahasallagi);
				  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai,$vjumlah);
				}		
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Setor Tunai Area '.$iarea.' No:'.$irtunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $irtunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$drtunai	= $this->input->post('drtunai', TRUE);
			if($drtunai!=''){
				$tmp=explode("-",$drtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$iarea			      = $this->input->post('iarea', TRUE);
			$ibank			      = $this->input->post('ibank', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$jml          		= $this->input->post('jml', TRUE);
			$jml          		= str_replace(',','',$jml);
			if (
				($drtunai != '') && ($iarea!='') && ($vjumlah!='') && ($vjumlah!='0') && ($ibank!='')
			   )
			{
				$this->load->model('exp-rtunai/mmaster');
				$this->db->trans_begin();
			  $irtunai = $this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insert($irtunai,$drtunai,$iarea,$eremark,$vjumlah,$ibank);
				for($i=1;$i<=$jml;$i++){
				  $itunai			= $this->input->post('itunai'.$i, TRUE);
				  $iareatunai	= $this->input->post('iarea'.$i, TRUE);
				  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $this->mmaster->insertdetail($irtunai,$iarea,$itunai,$iareatunai,$vjumlah,$i);
				  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai,$vjumlah);
				}		
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Setor Tunai Area '.$iarea.' No:'.$irtunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $irtunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/area/index/';
			$config['per_page'] = '10';
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
  			$query = $this->db->query("	select * from tr_area ",false);
  	  }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-rtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
			  $query = $this->db->query("select * from tr_area
									     	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
      }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
  	                                and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-rtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bukti()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$dfrom		  	= $this->input->post('dfrom');
			$dto		  	= $this->input->post('dto');
			$iarea		  	= $this->input->post('iarea');

			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			if($dto=='') $dto		= $this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
#			if($iarea=='') $iarea	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/bukti/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
#			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/bukti'.$iarea.'/index/';
			
			$query = $this->db->query ("select distinct a.i_rtunai AS irtunai, d.e_area_name AS narea from tm_rtunai a
          								left join tr_area d on(a.i_area=d.i_area)
										where 
										a.i_area='$iarea'
										and(a.d_rtunai >= to_date('$dfrom','dd-mm-yyyy')
                  						and a.d_rtunai <= to_date('$dto','dd-mm-yyyy'))
										and a.f_rtunai_cancel='f'",false);
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('exp-rtunai/mmaster');
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		= $this->mmaster->bacabukti($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8));
#			$data['isi']		= $this->mmaster->bacabukti($iarea,$config['per_page'],$this->uri->segment(6));

			$this->load->view('exp-rtunai/vlistbukti', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/bank/index/';
			$config['per_page'] = '10';
 			$query = $this->db->query("	select * from tr_bank ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-rtunai/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iarea = $this->input->post('iarea');
			if($iarea=='') $iarea 	= $this->uri->segment(4);
		  $tgl = $this->input->post('tgl');
			if($tgl=='') $tgl = $this->uri->segment(5);
			$tmp=explode('-',$tgl);
			$yy=$tmp[2];
			$mm=$tmp[1];
			$per=$yy.$mm;
		  $cari = strtoupper($this->input->post('cari'));
		  if($cari=='' && $this->uri->segment(6)=='sikasep'){
		    $cari = '';
		  }elseif($cari==''){
		    $cari=$this->uri->segment(6);
		  }
		  if($cari==''){
	  		$config['base_url'] = base_url().'index.php/exp-rtunai/cform/customer/'.$iarea.'/'.$tgl.'/sikasep/';
      }else{			
  			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/customer/'.$iarea.'/'.$tgl.'/'.$cari.'/';
      }
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, b.i_customer_groupar, 
			              c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per')
										left join tr_customer_owner d on(a.i_customer=d.i_customer)
										where a.i_area = '$iarea'
										and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$per,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
      $data['tgl']=$tgl;
			$this->load->view('exp-rtunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      $cari=str_replace("%20"," ",$cari);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
								left join tr_customer_groupar b on(a.i_customer=b.i_customer)
								left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
								left join tr_customer_owner d on(a.i_customer=d.i_customer)

								where a.i_area='$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' or 
									upper(d.e_customer_setor) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('exp-rtunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function tunai()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $baris=$this->input->post('baris', FALSE);
      $area=$this->input->post('iarea', FALSE);
      $drtunai=$this->input->post('drtunai', FALSE);
      $eremark		  = $this->uri->segment(11);
	  $eremark	= str_replace(" ","'",$eremark);
	  $iuser 	= $this->session->userdata('user_id');

			if($baris=='') $baris=$this->uri->segment(4);
			$data['baris']=$baris;
      if($area=='')	$area=$this->uri->segment(5);
      if($drtunai=='')	$drtunai=$this->uri->segment(6);
      $tmp=explode("-",$drtunai);
      $dd=$tmp[0];
      $mm=$tmp[1];
      $yy=$tmp[2];
      $drtunaix=$yy.'-'.$mm.'-'.$dd;
      $areax1 = $this->session->userdata('i_area');
			$areax2	= $this->session->userdata('i_area2');
			$areax3	= $this->session->userdata('i_area3');
			$areax4	= $this->session->userdata('i_area4');
			$areax5	= $this->session->userdata('i_area5');
			$data['area']=$area;
			$data['drtunai']=$drtunai;
			$data['baris']=$baris;
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/exp-rtunai/cform/tunai/'.$baris.'/'.$area.'/'.$drtunai.'/';
      if($areax1=='00'){
		    $query = $this->db->query(" select a.i_tunai from tm_tunai a, tr_customer b, tr_area c
                                    where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                                    and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                                    and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_customer) like '%$cari%')",false);
      }else{
		    $query = $this->db->query(" select a.i_tunai from tm_tunai a, tr_customer b, tr_area c
                                    where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                                    and (a.i_area='$areax1' or a.i_area='$areax2' or a.i_area='$areax3' or a.i_area='$areax4' 
                                    or a.i_area='$areax5')
                                    and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                                    and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_customer) like '%$cari%')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('exp-rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_tunai');
			$data['isi']=$this->mmaster->bacatunai($drtunaix,$cari,$area,$config['per_page'],$this->uri->segment(7,0),$areax1,$areax2,$areax3,$areax4,$areax5);
			$this->load->view('exp-rtunai/vlisttunai', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu528')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $data['page_title'] = $this->lang->line('rtunai')." update";
      $irtunai	  = $this->uri->segment(4);
			$iarea		  = $this->uri->segment(5);
      $itunai 	  = $this->uri->segment(6);
			$iareatunai = $this->uri->segment(7);
			$dfrom		  = $this->uri->segment(8);
			$dto			  = $this->uri->segment(9);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('exp-rtunai/mmaster');
			$this->mmaster->deletedetail($irtunai,$iarea,$itunai,$iareatunai);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Item Setor Tunai Area '.$iarea.' No setor:'.$irtunai.'  No tunai:'.$itunai;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['irtunai']  = $irtunai;
			$data['iarea']		= $iarea;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
      $data['pst']	= $this->session->userdata('i_area');
			$data['isi']=$this->mmaster->baca($iarea,$irtunai);
			$data['detail']=$this->mmaster->bacadetail($iarea,$irtunai);
	 		$this->load->view('exp-rtunai/vformupdate',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
#21232f297a57a5a743894a0e4a801fc3
?>
