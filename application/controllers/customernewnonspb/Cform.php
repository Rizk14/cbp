<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer						= $this->input->post('icustomer', TRUE);
			$iarea								= $this->input->post('iarea', TRUE);
			$isalesman						= $this->input->post('isalesman', TRUE);
			$icustomergroup				= $this->input->post('icustomergroup', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icustomerproducttype	= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct=$this->input->post('icustomerspecialproduct', TRUE);
			$icustomerstatus			= $this->input->post('icustomerstatus', TRUE);
			$icustomergrade				= $this->input->post('icustomergrade', TRUE);
			$icustomerservice			= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icity    						= $this->input->post('ikotacity', TRUE);
			$esalesmanname				= $this->input->post('esalesmanname', TRUE);
			$dsurvey							= $this->input->post('dsurvey', TRUE);
			if($dsurvey!=''){
				$tmp=explode("-",$dsurvey);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsurvey=$th."-".$bl."-".$hr;
			}else{
				$dsurvey=null;
			}
			$nvisitperiod					= $this->input->post('nvisitperiod', TRUE);
      if($nvisitperiod=='') $nvisitperiod=1;
      $iretensi   					= $this->input->post('iretensi', TRUE);
			$fcustomernew					= $this->input->post('fcustomernew', TRUE);
			if($fcustomernew!='')
				$fcustomernew			= 'f';
			else
				$fcustomernew			= 't';
			$ecustomername				= $this->input->post('ecustomername', TRUE);
			$ecustomername			  = str_replace("'","''",$ecustomername);
			$ecustomeraddress			= $this->input->post('ecustomeraddress', TRUE);
			$ecustomersign				= $this->input->post('ecustomersign', TRUE);
			$ecustomerphone				= $this->input->post('ecustomerphone', TRUE);
			$ert1									= $this->input->post('ert1', TRUE);
			$erw1									= $this->input->post('erw1', TRUE);
			$epostal1							= $this->input->post('epostal1', TRUE);
			$ecustomerkelurahan1	= $this->input->post('ecustomerkelurahan1', TRUE);
			$ecustomerkecamatan1	= $this->input->post('ecustomerkecamatan1', TRUE);
			$ecustomerkota1				= $this->input->post('ecustomerkota1', TRUE);
			$ecustomerprovinsi1		= $this->input->post('ecustomerprovinsi1', TRUE);
			$efax1								= $this->input->post('efax1', TRUE);
			$ecustomermonth				= $this->input->post('ecustomermonth', TRUE);
			$ecustomeryear				= $this->input->post('ecustomeryear', TRUE);
			$ecustomerage					= $this->input->post('ecustomerage', TRUE);
			$eshopstatus					= $this->input->post('eshopstatus', TRUE);
			$ishopstatus					= $this->input->post('ishopstatus', TRUE);
			$nshopbroad						= $this->input->post('nshopbroad', TRUE);
			$ecustomerowner				= $this->input->post('ecustomerowner', TRUE);
			$inik				= $this->input->post('inik', TRUE);
			$ecustomerownerttl		= $this->input->post('ecustomerownerttl', TRUE);
			$ecustomerownerage		= $this->input->post('ecustomerownerage', TRUE);
			$emarriage						= $this->input->post('emarriage', TRUE);
			$imarriage						= $this->input->post('imarriage', TRUE);
			$ejeniskelamin				= $this->input->post('ejeniskelamin', TRUE);
			$ijeniskelamin				= $this->input->post('ijeniskelamin', TRUE);
			$ereligion						= $this->input->post('ereligion', TRUE);
			$ireligion						= $this->input->post('ireligion', TRUE);
			$ecustomerowneraddress= $this->input->post('ecustomerowneraddress', TRUE);
			$ecustomerownerphone	= $this->input->post('ecustomerownerphone', TRUE);
			$ecustomerownerhp			= $this->input->post('ecustomerownerhp', TRUE);
			$ecustomerownerfax		= $this->input->post('ecustomerownerfax', TRUE);
			$ecustomerownerpartner= $this->input->post('ecustomerownerpartner', TRUE);
			$ecustomerownerpartnerttl= $this->input->post('ecustomerownerpartnerttl', TRUE);
			$ecustomerownerpartnerage= $this->input->post('ecustomerownerpartnerage', TRUE);
			$ert2									= $this->input->post('ert2', TRUE);
			$erw2									= $this->input->post('erw2', TRUE);
			$epostal2							= $this->input->post('epostal2', TRUE);
			$ecustomerkelurahan2	= $this->input->post('ecustomerkelurahan2', TRUE);
			$ecustomerkecamatan2	= $this->input->post('ecustomerkecamatan2', TRUE);
			$ecustomerkota2				= $this->input->post('ecustomerkota2', TRUE);
			$ecustomerprovinsi2		= $this->input->post('ecustomerprovinsi2', TRUE);
			$ecustomersendaddress	= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomersendphone		= $this->input->post('ecustomersendphone', TRUE);
			$etraversed						= $this->input->post('etraversed', TRUE);
			$itraversed						= $this->input->post('itraversed', TRUE);
			$fparkir							= $this->input->post('fparkir', TRUE);
			if($fparkir!='')
				$fparkir			= 't';
			else
				$fparkir			= 'f';
			$fkuli								= $this->input->post('fkuli', TRUE);
			if($fkuli!='')
				$fkuli			= 't';
			else
				$fkuli			= 'f';
			$eekspedisi1					= $this->input->post('eekspedisi1', TRUE);
			$eekspedisi2					= $this->input->post('eekspedisi2', TRUE);
			$ert3									= $this->input->post('ert3', TRUE);
			$erw3									= $this->input->post('erw3', TRUE);
			$epostal3							= $this->input->post('epostal3', TRUE);
			$ecustomerkota3				= $this->input->post('ecustomerkota3', TRUE);
			$ecustomerprovinsi3		= $this->input->post('ecustomerprovinsi3', TRUE);
			$ecustomerpkpnpwp			= $this->input->post('ecustomernpwp', TRUE);
			if($ecustomerpkpnpwp!='')
				$fspbpkp			= 't';
			else
				$fspbpkp			= 'f';
			$ecustomernpwpname		= $this->input->post('ecustomernpwpname', TRUE);
			$ecustomernpwpaddress	= $this->input->post('ecustomernpwpaddress', TRUE);
			$ecustomerclassname		= $this->input->post('ecustomerclassname', TRUE);
			$icustomerclass				= $this->input->post('icustomerclass', TRUE);
			$epaymentmethod				= $this->input->post('epaymentmethod', TRUE);
			$ipaymentmethod				= $this->input->post('ipaymentmethod', TRUE);
			$ecustomerbank1				= $this->input->post('ecustomerbank1', TRUE);
			$ecustomerbankaccount1= $this->input->post('ecustomerbankaccount1', TRUE);
			$ecustomerbankname1		= $this->input->post('ecustomerbankname1', TRUE);
			$ecustomerbank2				= $this->input->post('ecustomerbank2', TRUE);
			$ecustomerbankaccount2= $this->input->post('ecustomerbankaccount2', TRUE);
			$ecustomerbankname2		= $this->input->post('ecustomerbankname2', TRUE);
			$ekompetitor1					= $this->input->post('ekompetitor1', TRUE);
			$ekompetitor1				  = str_replace("'","''",$ekompetitor1);
			$ekompetitor2					= $this->input->post('ekompetitor2', TRUE);
			$ekompetitor2				  = str_replace("'","''",$ekompetitor2);
			$ekompetitor3					= $this->input->post('ekompetitor3', TRUE);
			$ekompetitor3				  = str_replace("'","''",$ekompetitor3);
			$nspbtoplength				= $this->input->post('ncustomertoplength', TRUE);
			$ncustomerdiscount		= $this->input->post('ncustomerdiscount', TRUE);
			$epricegroupname			= $this->input->post('epricegroupname', TRUE);
			$ipricegroup					= $this->input->post('ipricegroup', TRUE);
			$nline								= $this->input->post('nline', TRUE);
			$fkontrabon						= $this->input->post('fkontrabon', TRUE);
			if($fkontrabon!='')
				$fkontrabon			= 't';
			else
				$fkontrabon			= 'f';
			$ecall								= $this->input->post('ecall', TRUE);
			$icall								= $this->input->post('icall', TRUE);
			$ekontrabonhari				= $this->input->post('ekontrabonhari', TRUE);
			$ekontrabonjam1				= $this->input->post('ekontrabonjam1', TRUE);
			$ekontrabonjam2				= $this->input->post('ekontrabonjam2', TRUE);
			$etagihhari						= $this->input->post('etagihhari', TRUE);
			$etagihjam1						= $this->input->post('etagihjam1', TRUE);
			$etagihjam2						= $this->input->post('etagihjam2', TRUE);
			$ecustomerrefference  = $this->input->post('ecustomerrefference', TRUE);

			$eremarkx							= $this->input->post('eremarkx', TRUE);
			$fspbplusppn			= 'f';
			$fspbplusdiscount			= 'f';

			if ( ($icustomer!= '') && ($ecustomername!= '') && ($iarea!='') && ($ipricegroup!='') && 
           ($nspbtoplength!='') && ($dsurvey!='') && ($isalesman!='') && ($ncustomerdiscount!='') && ($nvisitperiod!='') && 
           ($icustomergroup!='') && ($icustomerproducttype!='') && ($icustomerstatus!='') && ($icustomergrade!='') &&
           ($icustomerservice!='') && ($icustomersalestype!='') && ($ipaymentmethod!='') &&
           ((!$fspbpkp && $inik!='') || ($fspbpkp))
         )
			{
				$this->db->trans_begin();
				$this->load->model('customernewnonspb/mmaster');
				$cek_data = $this->mmaster->cek_data($icustomer);
				if($cek_data->num_rows() > 0){
					echo "Kodelang sudah pernah digunakan !";
					die();
				}
				$this->mmaster->insert
					($icustomer,$iarea,$isalesman,$esalesmanname,$dsurvey,$nvisitperiod,
					 $fcustomernew,$ecustomername,$ecustomeraddress,$ecustomersign,
					 $ecustomerphone,$ert1,$erw1,$epostal1,$ecustomerkelurahan1,$ecustomerkecamatan1,
					 $ecustomerkota1,$ecustomerprovinsi1,$efax1,$ecustomermonth,$ecustomeryear,
					 $ecustomerage,$eshopstatus,$ishopstatus,$nshopbroad,$ecustomerowner,$ecustomerownerttl,
					 $emarriage,$imarriage,$ejeniskelamin,$ijeniskelamin,$ereligion,$ireligion,
					 $ecustomerowneraddress,$ecustomerownerphone,$ecustomerownerhp,$ecustomerownerfax,
					 $ecustomerownerpartner,$ecustomerownerpartnerttl,$ecustomerownerpartnerage,$ert2,$erw2,
					 $epostal2,$ecustomerkelurahan2,$ecustomerkecamatan2,$ecustomerkota2,$ecustomerprovinsi2,
					 $ecustomersendaddress,$ecustomersendphone,$etraversed,$itraversed,$fparkir,$fkuli,
					 $eekspedisi1,$eekspedisi2,$ert3,$erw3,$epostal3,$ecustomerkota3,$ecustomerprovinsi3,
					 $ecustomerpkpnpwp,$fspbpkp,$ecustomernpwpname,$ecustomernpwpaddress,$ecustomerclassname,
					 $icustomerclass,$epaymentmethod,$ipaymentmethod,$ecustomerbank1,$ecustomerbankaccount1,
					 $ecustomerbankname1,$ecustomerbank2,$ecustomerbankaccount2,$ecustomerbankname2,
					 $ekompetitor1,$ekompetitor2,$ekompetitor3,$nspbtoplength,$ncustomerdiscount,$epricegroupname,
					 $ipricegroup,$nline,$fkontrabon,$ecall,$icall,$ekontrabonhari,$ekontrabonjam1,
					 $ekontrabonjam2,$etagihhari,$etagihjam1,$etagihjam2,$icustomergroup,$icustomerplugroup,
					 $icustomerproducttype,$icustomerspecialproduct,$icustomerstatus,$icustomergrade,
					 $icustomerservice,$icustomersalestype,$ecustomerownerage,$ecustomerrefference,$iretensi,$icity,
					 $inik
					);
				if ( ($this->db->trans_status() === FALSE) )
				{
				  	$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
				  	$this->db->trans_commit();

				  	$sess=$this->session->userdata('session_id');
				    $id=$this->session->userdata('user_id');
				    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				    $rs		= pg_query($sql);
				    if(pg_num_rows($rs)>0){
					    while($row=pg_fetch_assoc($rs)){
						    $ip_address	  = $row['ip_address'];
						    break;
					    }
				    }else{
					    $ip_address='kosong';
				    }
				    $query 	= pg_query("SELECT current_timestamp as c");
				    while($row=pg_fetch_assoc($query)){
					    $now	  = $row['c'];
				    }
				    $pesan='Input Pelanggan non SPB:'.$icustomer;
				    $this->load->model('logger');
				    $this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $icustomer;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
				$data['page_title'] = $this->lang->line('customernewnonspb');
				$data['ispb']='';
				$data['isi']='';
        $data['tgl']=date('d-m-Y');
				$this->load->model('customernewnonspb/mmaster');

				$sess	= $this->session->userdata('session_id');
				$id 	= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan="Membuka Menu Input Pelanggan non SPB";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);
				
				$this->load->view('customernewnonspb/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('customernew');
			$this->load->view('customernewnonspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function plugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/plugroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plugroup');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->bacaplugroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariplugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/plugroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_plugroup where upper(i_customer_plugroup) like '%$cari%' 
										or upper(e_customer_plugroupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->cariplugroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customergroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customergroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_group where upper(i_customer_group) like '%cari%' 
										or upper(e_customer_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->caricustomergroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/pricegroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_price_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/pricegroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_price_group where upper(i_price_group) like '%cari%' 
										or upper(e_price_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->caripricegroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/area/'.$baris.'/sikasep/';
			$allarea= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);
			}

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('customernewnonspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$allarea= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			/*$config['base_url'] = base_url().'index.php/customernewnonspb/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			/* $query = $this->db->query("select * from tr_area where upper(i_area) like '%cari%' or upper(e_area_name) like '%cari%'",false); */
			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/cariarea/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/cariarea/'.$baris.'/sikasep/';

			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%') order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') or (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' or (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%') order by i_area", false);
			}

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('customernewnonspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerstatus/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_status');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->bacacustomerstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerstatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_status where upper(i_customer_status) like '%cari%' 
										or upper(e_customer_statusname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->caricustomerstatus($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerproducttype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_producttype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->bacacustomerproducttype($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerproducttype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_producttype where upper(i_customer_producttype) like '%cari%' 
										or upper(e_customer_producttypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->caricustomerproducttype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerspecialproduct/index/';

			$this->db->where("i_customer_producttype='$icustomerproducttype'");
			$config['total_rows'] = $this->db->count_all('tr_customer_specialproduct');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->bacacustomerspecialproduct($icustomerproducttype,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerspecialproduct/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_specialproduct 
										where upper(i_customer_specialproduct) like '%cari%' 
										or upper(e_customer_specialproductname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->caricustomerspecialproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customergrade/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_grade');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->bacacustomergrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customergrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_grade 
										where upper(i_customer_grade) like '%cari%' 
										or upper(e_customer_gradename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->caricustomergrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerservice/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_service');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->bacacustomerservice($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerservice/index/';
			$cari = $this->input->post('cari', FALSE);

			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_customer_service where upper(i_customer_service) like '%cari%' 
										or upper(e_customer_servicename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->caricustomerservice($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customersalestype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_salestype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->bacacustomersalestype($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customersalestype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_salestype where upper(i_customer_salestype) like '%cari%' 
										or upper(e_customer_salestypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->caricustomersalestype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerclass/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_class');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->bacacustomerclass($config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/customerclass/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_class where upper(i_customer_class) like '%cari%' 
										or upper(e_customer_classname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->caricustomerclass($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customernewnonspb/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			if($area=='')$area=$this->uri->segment(4);
			if($cari==''){
				if( ($this->uri->segment(5)!='zxqf') && ($this->uri->segment(5)!='') ){
					$cari=$this->uri->segment(5);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/salesman/'.$area.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/salesman/'.$area.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/salesman/'.$area.'/'.$cari.'/';
			}
			$query = $this->db->query(" select distinct(i_salesman) from tr_customer_salesman 
																	where i_area = '$area' 
																	and (upper(i_salesman) like '%$cari%' 
																		or upper(e_salesman_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(6),$area,$cari);
			$this->load->view('customernewnonspb/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kota1()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			if($area=='')$area=$this->uri->segment(4);
			if($cari==''){
				if( ($this->uri->segment(5)!='zxqf') && ($this->uri->segment(5)!='') ){
					$cari=$this->uri->segment(5);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota1/'.$area.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota1/'.$area.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota1/'.$area.'/'.$cari.'/';
			}
			$query = $this->db->query(" select distinct(a.i_city), b.e_city_name from tr_kecamatan a, tr_city b where a.i_area = '$area' 
                                  and a.i_city=b.i_city and a.i_area=b.i_area
                                  and (upper(a.i_city) like '%$cari%' or upper(b.e_city_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacakota1($config['per_page'],$this->uri->segment(6),$area,$cari);
			$this->load->view('customernewnonspb/vlistkota1', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kota2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			if($area=='')$area=$this->uri->segment(4);
			if($cari==''){
				if( ($this->uri->segment(5)!='zxqf') && ($this->uri->segment(5)!='') ){
					$cari=$this->uri->segment(5);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota2/'.$area.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota2/'.$area.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota2/'.$area.'/'.$cari.'/';
			}
			$query = $this->db->query(" select distinct(a.i_city), b.e_city_name from tr_kecamatan a, tr_city b where a.i_area = '$area' 
                                  and a.i_city=b.i_city and a.i_area=b.i_area
                                  and (upper(a.i_city) like '%$cari%' or upper(b.e_city_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacakota1($config['per_page'],$this->uri->segment(6),$area,$cari);
			$this->load->view('customernewnonspb/vlistkota2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kota3()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			if($area=='')$area=$this->uri->segment(4);
			if($cari==''){
				if( ($this->uri->segment(5)!='zxqf') && ($this->uri->segment(5)!='') ){
					$cari=$this->uri->segment(5);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota3/'.$area.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota3/'.$area.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kota3/'.$area.'/'.$cari.'/';
			}
			$query = $this->db->query(" select distinct(a.i_city), b.e_city_name from tr_kecamatan a, tr_city b where a.i_area = '$area' 
                                  and a.i_city=b.i_city and a.i_area=b.i_area
                                  and (upper(a.i_city) like '%$cari%' or upper(b.e_city_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacakota1($config['per_page'],$this->uri->segment(6),$area,$cari);
			$this->load->view('customernewnonspb/vlistkota3', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(5)) ? $this->uri->segment(5) : $cari;

			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/city/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacacity($iarea,$cari,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$data['cari'] = $cari;
			$this->load->view('customernewnonspb/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea= $this->input->post('iarea', FALSE);
			$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(5)) ? $this->uri->segment(5) : $cari;
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/city/'.$iarea.'/'.$cari.'/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_city where (upper(i_city) ilike '%$cari%' 
							or upper(e_city_name) ilike '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->caricity($iarea,$cari,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$data['cari'] = $cari;
			$this->load->view('customernewnonspb/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kecamatan1()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			$kota=strtoupper($this->input->post("kota"));
			if($area=='')$area=$this->uri->segment(4);
			if($kota=='')$kota=$this->uri->segment(5);
			if($cari==''){
				if( ($this->uri->segment(6)!='zxqf') && ($this->uri->segment(6)!='') ){
					$cari=$this->uri->segment(6);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan1/'.$area.'/'.$kota.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan1/'.$area.'/'.$kota.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan1/'.$area.'/'.$kota.'/'.$cari.'/';
			}
			$query = $this->db->query(" select i_kecamatan from tr_kecamatan where i_area = '$area' 
                                  and i_city='$kota' and (upper(e_kecamatan_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['kota']=$kota;
			$data['page_title'] = $this->lang->line('list_kecamatan');
			$data['isi']=$this->mmaster->bacakecamatan1($config['per_page'],$this->uri->segment(7),$area,$kota,$cari);
			$this->load->view('customernewnonspb/vlistkecamatan1', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kecamatan2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$area=strtoupper($this->input->post("area"));
			$kota=strtoupper($this->input->post("kota"));
			if($area=='')$area=$this->uri->segment(4);
			if($kota=='')$kota=$this->uri->segment(5);
			if($cari==''){
				if( ($this->uri->segment(6)!='zxqf') && ($this->uri->segment(6)!='') ){
					$cari=$this->uri->segment(6);
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan2/'.$area.'/'.$kota.'/'.$cari.'/';
				}else{
					$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan2/'.$area.'/'.$kota.'/zxqf/';
				}
			}else{
				$config['base_url'] = base_url().'index.php/customernewnonspb/cform/kecamatan2/'.$area.'/'.$kota.'/'.$cari.'/';
			}
			$query = $this->db->query(" select i_kecamatan from tr_kecamatan where i_area = '$area' 
                                  and i_city='$kota' and (upper(e_kecamatan_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['area']=$area;
			$data['kota']=$kota;
			$data['page_title'] = $this->lang->line('list_kecamatan');
			$data['isi']=$this->mmaster->bacakecamatan1($config['per_page'],$this->uri->segment(7),$area,$kota,$cari);
			$this->load->view('customernewnonspb/vlistkecamatan2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function shopstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/shopstatus/';
			$query = $this->db->query(" select i_shop_status from tr_shop_status", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_shopstatus');
			$data['isi']=$this->mmaster->bacashopstatus($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistshopstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function jeniskelamin()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/jeniskelamin/';
			$query = $this->db->query(" select i_jeniskelamin from tr_jeniskelamin", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_jeniskelamin');
			$data['isi']=$this->mmaster->bacajeniskelamin($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistjeniskelamin', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function religion()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/religion/';
			$query = $this->db->query(" select i_religion from tr_religion", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_religion');
			$data['isi']=$this->mmaster->bacareligion($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistreligion', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function marriage()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/marriage/';
			$query = $this->db->query(" select i_marriage from tr_marriage", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_marriage');
			$data['isi']=$this->mmaster->bacamarriage($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistmarriage', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function traversed()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/traversed/';
			$query = $this->db->query(" select i_traversed from tr_traversed", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_traversed');
			$data['isi']=$this->mmaster->bacatraversed($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlisttraversed', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function paymentmethod()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/paymentmethod/';
			$query = $this->db->query(" select i_paymentmethod from tr_paymentmethod", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_paymentmethod');
			$data['isi']=$this->mmaster->bacapaymentmethod($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistpaymentmethod', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function call()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/call/';
			$query = $this->db->query(" select i_call from tr_call", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_call');
			$data['isi']=$this->mmaster->bacacall($config['per_page'],$this->uri->segment(4));
			$this->load->view('customernewnonspb/vlistcall', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function retensi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu357')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post("cari"));
			$config['base_url'] = base_url().'index.php/customernewnonspb/cform/retensi/'.$cari.'/';
			$query = $this->db->query(" select i_retensi from tr_retensi
																	where (upper(i_retensi) like '%$cari%' 
																	or upper(e_retensi) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customernewnonspb/mmaster');
			$data['page_title'] = $this->lang->line('list_retensi');
			$data['isi']=$this->mmaster->bacaretensi($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('customernewnonspb/vlistretensi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
