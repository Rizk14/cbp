<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title1'] = $this->lang->line('exp-bonkeluar');
			$data['page_title2'] = $this->lang->line('exp-bonmasuk');
			$data['dfrom']	= '';
			$data['dto']	= '';
			//$data['iperiode']	= '';
			//$data['iarea']	  = '';
			$this->load->view('exp-bonkm-all/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$this->load->model('tesx/mmaster');
			$dfrom = $this->input->post('dfrom');
			$dto   = $this->input->post('dto');

			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['isi'] = $this->mmaster->readperiode($dfrom,$dto);

			$this->load->view('tesx/vformview',$data);


		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportkeluar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			//$this->load->model('tesx/mmaster');
			$dfrom = $this->input->post('dfrom');
			$dto   = $this->input->post('dto');
			

			$dfrom=date('d-m-Y',strtotime($dfrom));
			$dto=date('d-m-Y',strtotime($dto));

      /*$this->db->select("a.*, b.e_product_motifname,d.d_bk,d.e_remark as keterangan,d.v_bk,d.e_subdist
						from tr_product_motif b, tm_bk_item a
						inner join tm_bk d on(a.i_bk=d.i_bk)
						where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						and d.d_bk >= to_date('$dfrom','dd-mm-yyyy') and d.d_bk <= to_date('$dto','dd-mm-yyyy')
						and d.f_bk_cancel!='t'
						order by a.i_bk,a.n_item_no",false);*/


      $this->db->select("* from tm_bk
                		where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
                		and f_bk_cancel!='t'
	                    order by d_bk, i_bk",false);
      
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Keluar ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(65);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BON KELUAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,8,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,8,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,8,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Kode Barang');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Sub Total');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Sub Dist');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:K6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 11
					  ),
				  ),
				  'A6:K6'
				  );

				$i=7;
				$a=$i+1;
				$na=1;
				foreach($query->result() as $row){

			$this->db->select(" count(i_bk) as cnt from tm_bk_item where i_bk='$row->i_bk'", false);
			$jml = $this->db->get();
			foreach($jml->result() as $raw){
				//$mcell=$raw->cnt+1;
				$jm=($raw->cnt+$i)-1;
				$ja=$i+1;
			$objPHPExcel->getActiveSheet()->mergeCells("A$i:A$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
        	$objPHPExcel->getActiveSheet()->mergeCells("B$i:B$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bk);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("C$i:C$jm");
        	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bk);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("D$i:D$jm");
        	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
        	/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'QTY * Hargaaa');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);*/
			$objPHPExcel->getActiveSheet()->mergeCells("J$i:J$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->v_bk);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("K$i:K$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->e_subdist);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i=($i+$raw->cnt);
					$na++;
				}
			}

			$a=7;
				$sql=" select * from tm_bk
		                		where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
		                		and f_bk_cancel!='t'
			                    order by d_bk, i_bk";
			    $pquery = $this->db->query($sql,false);

				foreach($pquery->result() as $tmp){
				$this->db->select(" a.*, b.e_product_motifname,d.d_bk,(select count(i_bk) from tm_bk_item where i_bk='$tmp->i_bk') as cnt
									from tr_product_motif b, tm_bk_item a
									inner join tm_bk d on(a.i_bk=d.i_bk)
									where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
									and d.d_bk >= to_date('$dfrom','dd-mm-yyyy') and d.d_bk <= to_date('$dto','dd-mm-yyyy')
									and d.f_bk_cancel!='t' and a.i_bk='$tmp->i_bk'
									order by a.i_bk,a.n_item_no
									--limit (select count(i_bk) from tm_bk_item where i_bk='$tmp->i_bk')
									", false);
				$detail = $this->db->get();
					foreach($detail->result() as $det){
				
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						'wrap'      => true
						)
					),
					"G$a:H$a"
					);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						'wrap'      => true
						)
					),
					"F$a:F$a"
					);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_TOP,
						'wrap'      => true
					)
				  ),
				  'A'.$a.':K'.$a
				  );
					//$aw=$a+1;
					$objPHPExcel->getActiveSheet()->setCellValue("E$a", $det->i_product);
					$objPHPExcel->getActiveSheet()->getStyle("E$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("F$a", $det->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle("F$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("G$a", $det->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle("G$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("H$a", $det->n_quantity);
					$objPHPExcel->getActiveSheet()->getStyle("H$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$subtotal=$det->n_quantity*$det->v_unit_price;
					$objPHPExcel->getActiveSheet()->setCellValue("I$a", $subtotal);
					$objPHPExcel->getActiveSheet()->getStyle("I$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					$a++;
				}
			}

        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Keluar + Detail '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Bon Keluar'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

function exportmasuk()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			//$this->load->model('tesx/mmaster');
			$dfrom = $this->input->post('dfromm');
			$dto   = $this->input->post('dtom');
			

			$dfrom=date('d-m-Y',strtotime($dfrom));
			$dto=date('d-m-Y',strtotime($dto));

      /*$this->db->select("a.*, b.e_product_motifname,d.d_bm,d.e_remark as keterangan,d.v_bm,d.e_subdist
						from tr_product_motif b, tm_bm_item a
						inner join tm_bm d on(a.i_bm=d.i_bm)
						where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
						and d.d_bm >= to_date('$dfrom','dd-mm-yyyy') and d.d_bm <= to_date('$dto','dd-mm-yyyy')
						and d.f_bm_cancel!='t'
						order by a.i_bm,a.n_item_no",false);*/


      $this->db->select("* from tm_bm
                		where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
                		and f_bm_cancel!='t'
	                    order by d_bm, i_bm",false);
      
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Masuk ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(65);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BON MASUK');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,8,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,8,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,8,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Kode Barang');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Sub Total');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Sub Dist');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:K6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 11
					  ),
				  ),
				  'A6:K6'
				  );

				$i=7;
				$a=$i+1;
				$na=1;
				foreach($query->result() as $row){

			$this->db->select(" count(i_bm) as cnt from tm_bm_item where i_bm='$row->i_bm'", false);
			$jml = $this->db->get();
			foreach($jml->result() as $raw){
				//$mcell=$raw->cnt+1;
				$jm=($raw->cnt+$i)-1;
				$ja=$i+1;
			$objPHPExcel->getActiveSheet()->mergeCells("A$i:A$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
        	$objPHPExcel->getActiveSheet()->mergeCells("B$i:B$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bm);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("C$i:C$jm");
        	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bm);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("D$i:D$jm");
        	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
        	/*$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'QTY * Hargaaa');
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);*/
			$objPHPExcel->getActiveSheet()->mergeCells("J$i:J$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->v_bm);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->mergeCells("K$i:K$jm");
			$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->e_subdist);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i=($i+$raw->cnt);
					$na++;
				}
			}

			$a=7;
				$sql=" select * from tm_bm
		                		where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
		                		and f_bm_cancel!='t'
			                    order by d_bm, i_bm";
			    $pquery = $this->db->query($sql,false);

				foreach($pquery->result() as $tmp){
				$this->db->select(" a.*, b.e_product_motifname,d.d_bm,(select count(i_bm) from tm_bm_item where i_bm='$tmp->i_bm') as cnt
									from tr_product_motif b, tm_bm_item a
									inner join tm_bm d on(a.i_bm=d.i_bm)
									where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
									and d.d_bm >= to_date('$dfrom','dd-mm-yyyy') and d.d_bm <= to_date('$dto','dd-mm-yyyy')
									and d.f_bm_cancel!='t' and a.i_bm='$tmp->i_bm'
									order by a.i_bm,a.n_item_no
									--limit (select count(i_bm) from tm_bm_item where i_bm='$tmp->i_bm')
									", false);
				$detail = $this->db->get();
					foreach($detail->result() as $det){
				
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						'wrap'      => true
						)
					),
					"G$a:H$a"
					);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						'wrap'      => true
						)
					),
					"F$a:F$a"
					);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_TOP,
						'wrap'      => true
					)
				  ),
				  'A'.$a.':K'.$a
				  );
					//$aw=$a+1;
					$objPHPExcel->getActiveSheet()->setCellValue("E$a", $det->i_product);
					$objPHPExcel->getActiveSheet()->getStyle("E$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("F$a", $det->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle("F$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("G$a", $det->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle("G$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue("H$a", $det->n_quantity);
					$objPHPExcel->getActiveSheet()->getStyle("H$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$subtotal=$det->n_quantity*$det->v_unit_price;
					$objPHPExcel->getActiveSheet()->setCellValue("I$a", $subtotal);
					$objPHPExcel->getActiveSheet()->getStyle("I$a")->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					$a++;
				}
			}

        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Masuk + Detail '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Bon Masuk'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>