<?php
class Cform extends CI_Controller
{
	public $title 	= "Daftar Kiriman Barang SJP (DKB-SJP)";
	public $folder 	= "dkbsjp";
	public $balik 	= "listdkbsjp";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		$data = [
			'page_title' 	=> $this->title,
			'folder' 		=> $this->folder,
			'iarealogin' 	=> $this->session->userdata('i_area'),
			'idkb' 			=> "",
			'isi'       	=> "",
			'detail'       	=> "",
			'jmlitem'       => "",
			'tgl'       	=> date('d-m-Y'),
			'iarea'       	=> "",
			'eareaname'     => "",
		];

		$this->logger->writenew("Membuka Menu " . $this->title);

		$this->load->view($this->folder . '/vmainform', $data);
	}

	function sjp()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari   	= strtoupper($this->input->post('cari', false));
			$cari 		= str_replace("'", "", $cari);
			$is_cari	= $this->input->post('is_cari');
			$baris 		= $this->input->post('baris', FALSE);
			$area 		= $this->input->post('iarea', FALSE);
			$ddkb 		= $this->input->post('ddkb', FALSE);

			if ($baris == '') $baris = $this->uri->segment(4);
			if ($area == '') $area = $this->uri->segment(5);
			if ($ddkb == '') $ddkb = $this->uri->segment(6);

			if ($is_cari == '') $is_cari = $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") {
				$cari = $this->uri->segment(7);
				if ($cari == '') $is_cari = "";
			}

			$tmp 	= explode("-", $ddkb);
			$dd 	= $tmp[0];
			$mm 	= $tmp[1];
			$yy 	= $tmp[2];
			$ddkbx 	= $yy . '-' . $mm . '-' . $dd;

			if ($is_cari == "1") {
				$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/sjp/' . $baris . '/' . $area . '/' . $ddkb . '/' . $cari . '/' . $is_cari . '/index/';
			} else {
				$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/sjp/' . $baris . '/' . $area . '/' . $ddkb . '/index/';
			}

			if ($is_cari != "1") {
				$query = $this->db->query("	SELECT i_sjp, d_sjp, a.i_area, b.e_area_name, v_sjp
											FROM tm_sjp a
											INNER JOIN tr_area b ON a.i_area = b.i_area 
											WHERE
											a.i_area = '$area' AND d_sjp <= '$ddkbx'
											AND i_dkb IS NULL AND f_sjp_cancel='f'
											ORDER BY d_sjp DESC ", false);
			} else {
				$query = $this->db->query("	SELECT i_sjp, d_sjp, a.i_area, b.e_area_name, v_sjp
											FROM tm_sjp a
											INNER JOIN tr_area b ON a.i_area = b.i_area 
											WHERE
											a.i_area = '$area' AND d_sjp <= '$ddkbx'
											AND i_dkb IS NULL AND f_sjp_cancel='f'
											AND (upper(a.i_sjp) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' 
											OR upper(a.i_spmb) LIKE '%$cari%' OR upper(a.i_area) LIKE '%$cari%')
											ORDER BY d_sjp DESC ", false);
			}

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			if ($is_cari == "1") {
				$config['cur_page'] = $this->uri->segment(10);
			} else {
				$config['cur_page'] = $this->uri->segment(8);
			}
			$this->pagination->initialize($config);

			if ($is_cari == "1") {
				$data['isi'] = $this->mmaster->bacasjp($ddkbx, $cari, $area, $config['per_page'], $this->uri->segment(10, 0));
			} else {
				$data['isi'] = $this->mmaster->bacasjp($ddkbx, $cari, $area, $config['per_page'], $this->uri->segment(8, 0));
			}

			$data['ddkbx']		= $ddkb;
			$data['area']		= $area;
			$data['cari']		= $cari;
			$data['baris']		= $baris;
			$data['folder']		= $this->folder;
			$data['page_title']	= $this->lang->line('list_sjp');

			$this->load->view($this->folder . '/vlistsjp', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function simpan()
	{
		$ddkb 		= $this->input->post('ddkb', TRUE);

		if ($ddkb != '') {
			$tmp = explode("-", $ddkb);
			$th = $tmp[2];
			$bl = $tmp[1];
			$hr = $tmp[0];
			$ddkb = $th . "-" . $bl . "-" . $hr;
			$thbl = $th . $bl;
		}

		$iarea	    = $this->session->userdata('i_area');
		$idkbold    = $this->input->post('idkbold', TRUE);
		$idkb	    = $this->input->post('idkb', TRUE);
		$eareaname  = $this->input->post('eareaname', TRUE);
		$iareasj    = $this->input->post('iarea', TRUE);
		$edkbkirim	= $this->input->post('edkbkirim', TRUE);
		$idkbkirim	= $this->input->post('idkbkirim', TRUE);
		$edkbvia   	= $this->input->post('edkbvia', TRUE);
		$idkbvia 	= $this->input->post('idkbvia', TRUE);
		$eekspedisi	= $this->input->post('eekspedisi', TRUE);
		$esupirname	= $this->input->post('esupirname', TRUE);
		$ikendaraan	= $this->input->post('ikendaraan', TRUE);
		$vdkb   	= $this->input->post('vdkb', TRUE);
		$vdkb		= str_replace(',', '', $vdkb);
		$jml	    = $this->input->post('jml', TRUE);
		$jmlx	    = $this->input->post('jmlx', TRUE);

		if ($ddkb != '' && $iareasj != '' && $idkbkirim != '' && $idkbvia != '' && $jml != '' && $jml != 0) {
			$this->db->trans_begin();

			$idkb	= $this->mmaster->runningnumber($iarea, $thbl);

			$this->mmaster->insertheader($idkb, $ddkb, $iareasj, $idkbkirim, $idkbvia, $ikendaraan, $esupirname, $vdkb, $idkbold);

			for ($i = 1; $i <= $jml; $i++) {
				$isjp			= $this->input->post('isjp' . $i, TRUE);
				$dsjp			= $this->input->post('dsjp' . $i, TRUE);
				$telat1	    	= $this->input->post('etelat' . $i, TRUE);
				$telat2	    	= $this->input->post('rtelat' . $i, TRUE);

				$nkoli	    	= $this->input->post('nkoli' . $i, TRUE);
				$nikat	    	= $this->input->post('nikat' . $i, TRUE);
				$nkardus    	= $this->input->post('nkardus' . $i, TRUE);

				if ($telat2 == '') {
					$etelat = $telat1;
				} else {
					$etelat = $telat2;
				}

				if ($dsjp != '') {
					$tmp = explode("-", $dsjp);
					$th = $tmp[2];
					$bl = $tmp[1];
					$hr = $tmp[0];
					$dsjp = $th . "-" . $bl . "-" . $hr;
				}

				$vjumlah		= $this->input->post('vsjnetto' . $i, TRUE);
				$vjumlah		= str_replace(',', '', $vjumlah);
				$eremark		= $this->input->post('eremark' . $i, TRUE);

				$this->mmaster->insertdetail($idkb, $iareasj, $isjp, $ddkb, $dsjp, $vjumlah, $eremark, $i, $nkoli, $nikat, $nkardus);
				$this->mmaster->updatesjp($idkb, $isjp, $iareasj, $ddkb, $etelat);
			}

			if ($jmlx > 0 && $idkbvia != 2) {
				for ($i = 1; $i <= $jmlx; $i++) {
					$iekspedisi	= $this->input->post('iekspedisi' . $i, TRUE);
					$eremark	= $this->input->post('eremarkx' . $i, TRUE);
					$this->mmaster->insertdetailekspedisi($idkb, $iareasj, $iekspedisi, $ddkb, $eremark, $i);
				}
			}

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew('Input DKB-SJP Area ' . $iareasj . ' No:' . $idkb);

				$data['sukses']			= true;
				$data['inomor']			= $idkb;
				$this->load->view('nomor', $data);
			}
		}
	}

	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['bisaedit'] 	= false;
			$data['page_title'] = $this->title . " Update";

			if ($this->uri->segment(4) != '') {
				$idkb   = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom  = $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$iareax	= $this->uri->segment(8);

				$query  = $this->db->query("select * from tm_dkb_sjp_item where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$ddkb = substr($row->d_dkb, 0, 4) . substr($row->d_dkb, 5, 2);
					}
				}

				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['iareax']		= $iareax;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;

				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();

				$data['isi']		= $this->mmaster->baca($idkb, $iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb, $iarea);

				$qrunningjml		= $this->mmaster->bacadetailrunningjml($idkb, $iarea);
				if ($qrunningjml->num_rows() > 0) {
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				} else {
					$data['jmlx']	= 0;
				}

				$data['detailx'] 	= $this->mmaster->bacadetailx($idkb, $iarea);
				$data['balik']		= $this->balik;
				$data['folder']		= $this->folder;

				$query = $this->db->query("	select i_periode from tm_periode ", false);
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$periode = $row->i_periode;
					}
					if ($periode <= $ddkb) $data['bisaedit'] = true;
				}

				$this->logger->writenew('View Detail DKB-SJP Area ' . $iarea . ' No:' . $idkb);

				$this->load->view($this->folder . '/vmainform', $data);
			} else {
				$this->load->view($this->folder . '/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ddkb 		= $this->input->post('ddkb', TRUE);

			if ($ddkb != '') {
				$tmp = explode("-", $ddkb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$ddkb = $th . "-" . $bl . "-" . $hr;
			}
			$iarea	    = $this->session->userdata('i_area');
			$idkbold    = $this->input->post('idkbold', TRUE);
			$idkb	    = $this->input->post('idkb', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$iareasj  	= $this->input->post('iarea', TRUE);
			$edkbkirim 	= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 	= $this->input->post('idkbkirim', TRUE);
			$edkbvia   	= $this->input->post('edkbvia', TRUE);
			$idkbvia   	= $this->input->post('idkbvia', TRUE);
			$eekspedisi	= $this->input->post('eekspedisi', TRUE);
			$esupirname	= $this->input->post('esupirname', TRUE);
			$ikendaraan	= $this->input->post('ikendaraan', TRUE);
			$vdkb   	= $this->input->post('vdkb', TRUE);
			$vdkb  		= str_replace(',', '', $vdkb);
			$jml      	= $this->input->post('jml', TRUE);
			$jmlx	    = $this->input->post('jmlx', TRUE);

			if ($idkb != '' && $ddkb != '' && $iareasj != '' && $idkbkirim != '' && $idkbvia != '') {
				$this->db->trans_begin();

				$this->mmaster->deleteheader($idkb, $iareasj);
				$this->mmaster->insertheader($idkb, $ddkb, $iareasj, $idkbkirim, $idkbvia, $ikendaraan, $esupirname, $vdkb, $idkbold);
				for ($i = 1; $i <= $jml; $i++) {
					$isjp = $this->input->post('isjp' . $i, TRUE);
					$dsjp = $this->input->post('dsjp' . $i, TRUE);
					if ($dsjp != '') {
						$tmp = explode("-", $dsjp);
						$th = $tmp[2];
						$bl = $tmp[1];
						$hr = $tmp[0];
						$dsjp = $th . "-" . $bl . "-" . $hr;
					}
					$vjumlah		= $this->input->post('vsjnetto' . $i, TRUE);
					$vjumlah		= str_replace(',', '', $vjumlah);
					$eremark		= $this->input->post('eremark' . $i, TRUE);
					$telat1	    	= $this->input->post('etelat' . $i, TRUE);
					$telat2	    	= $this->input->post('rtelat' . $i, TRUE);

					$nkoli	    	= $this->input->post('nkoli' . $i, TRUE);
					$nikat	    	= $this->input->post('nikat' . $i, TRUE);
					$nkardus    	= $this->input->post('nkardus' . $i, TRUE);

					if ($telat2 == '') {
						$etelat = $telat1;
					} else {
						$etelat = $telat2;
					}

					$this->mmaster->deletedetail($idkb, $iareasj, $isjp);
					$this->mmaster->insertdetail($idkb, $iareasj, $isjp, $ddkb, $dsjp, $vjumlah, $eremark, $i, $nkoli, $nikat, $nkardus);
					$this->mmaster->updatesjp($idkb, $isjp, $iareasj, $ddkb, $etelat);
				}
				if ($jmlx > 0 && $idkbvia != 2) {
					for ($i = 1; $i <= $jmlx; $i++) {
						$iekspedisi	= $this->input->post('iekspedisi' . $i, TRUE);
						$eremark	= $this->input->post('eremarkx' . $i, TRUE);
						$this->mmaster->deletedetailekspedisi($idkb, $iareasj, $iekspedisi);
						$this->mmaster->insertdetailekspedisi($idkb, $iareasj, $iekspedisi, $ddkb, $eremark, $i);
					}
				}

				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();

					$this->logger->writenew('Update DKB Area ' . $iarea . ' No:' . $idkb);

					$data['sukses']			= true;
					$data['inomor']			= $idkb;
					$this->load->view('nomor', $data);
				}
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function deletedetailekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['bisaedit'] 	= false;
			$idkb	= str_replace("%20", "", $this->uri->segment(4));
			$iarea	= $this->uri->segment(5);
			$iekspedisi	= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$ddkb		= $this->uri->segment(9);
			//$icustomer	= $this->uri->segment(9);
			$this->db->trans_begin();

			$this->mmaster->deletedetailekspedisi($idkb, $iarea, $iekspedisi);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->write('Hapus Ekspedisi DKB Area ' . $iarea . ' No:' . $idkb . ' Ekspedisi' . $iekspedisi);

				$data['page_title'] = $this->title . " Update";

				$query  = $this->db->query("select * from tm_dkb_item where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows();
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['folder']	   	= $folder;
				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();

				$data['isi']		= $this->mmaster->baca($idkb, $iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb, $iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb, $iarea);
				if ($qrunningjml->num_rows() > 0) {
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				} else {
					$data['jmlx']	= 0;
				}
				$data['detailx']	= $this->mmaster->bacadetailx($idkb, $iarea);

				$query = $this->db->query("	select i_periode from tm_periode ", false);
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$periode = $row->i_periode;
					}
					if ($periode <= $ddkb) $data['bisaedit'] = true;
				}

				$this->load->view($this->folder . '/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispmb	= $this->input->post('ispmbdelete', TRUE);

			$this->mmaster->delete($ispmb);
			$data['page_title'] = $this->title;
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$data['isi'] = $this->mmaster->bacasemua();
			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['bisaedit'] 	= false;
			$idkb		= $this->uri->segment(4);
			$iareasjp  	= substr($this->uri->segment(6), 9, 2);
			$iarea  	= $this->uri->segment(5);
			$isjp		= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$ddkb		= date('Ym', strtotime($this->uri->segment(9)));
			$iareax		= $this->uri->segment(10);

			$this->db->trans_begin();

			$this->mmaster->deletedetail2($idkb, $iarea, $isjp, $iareasjp);

			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew('Hapus Detail DKB Area ' . $iarea . ' No:' . $idkb . ' untuk SJP :' . $isjp);

				$data['page_title'] = $this->title . " Update";

				$query  = $this->db->query("select * from tm_dkb_sjp_item where i_dkb='$idkb' and i_area='$iarea'");

				$data['jmlitem'] 	= $query->num_rows();
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['folder']	   	= $this->folder;
				$data['balik']	   	= $this->balik;
				$data['iareax']	   	= $iareax;

				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();

				$data['isi']		= $this->mmaster->baca($idkb, $iarea);
				$data['detail']		= $this->mmaster->bacadetail($idkb, $iarea);
				$qrunningjml		= $this->mmaster->bacadetailrunningjml($idkb, $iarea);

				if ($qrunningjml->num_rows() > 0) {
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				} else {
					$data['jmlx']	= 0;
				}
				$data['detailx'] = $this->mmaster->bacadetailx($idkb, $iarea);

				$query = $this->db->query("	select i_periode from tm_periode ", false);
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						$periode = $row->i_periode;
					}
					if ($periode <= $ddkb) $data['bisaedit'] = true;
				}

				$this->load->view($this->folder . '/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function carisj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris	= $this->uri->segment(4);
			$area	= $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/sj/' . $baris . '/' . $area . '/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00') {
				$query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
					                where a.i_sj_type='04' and a.i_area_to='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                        and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                and a.i_customer=b.i_customer and a.f_sj_daerah='f'", false);
				#					                and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$area')
				if ($query->num_rows() == 0 || $query->num_rows() < 1) {
					$query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
										where a.i_sj_type='04' and a.i_area_referensi='$area' 
										and a.i_dkb isnull and a.f_sj_cancel='f'
										and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
										or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
										and a.i_customer=b.i_customer and a.f_sj_daerah='f' and a.f_entry_pusat='t' ", false);
				}
			} else {

				$query = $this->db->query("	select a.i_sj from tm_nota a, tr_customer b 
					                          where a.i_sj_type='04' and a.i_area_to='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                             	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                          and a.i_customer=b.i_customer and a.f_sj_daerah='t'", false);
				#					                          and a.i_sj not in(select i_sj from tm_dkb_item where i_area='$area')
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi'] = $this->mmaster->carisj($area, $cari, $config['per_page'], $this->uri->segment(7));
			$data['area'] = $area;
			$data['baris'] = $baris;
			$this->load->view($this->folder . '/vlistsj', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iuser   = $this->session->userdata('user_id');
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/' . $baris . '/sikasep/';
			$query = $this->db->query(" SELECT 	
											*
										FROM
											tr_area
										WHERE
											i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser')
											AND i_area IN (SELECT i_store FROM tr_store WHERE f_aktif = 't')
										ORDER BY
											i_area", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = '';

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(6), $iuser);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iuser   	= $this->session->userdata('user_id');
			/*$config['base_url'] = base_url().'index.php/'.$folder.'/cform/area/index/';
      		$cari    	= $this->input->post('cari', FALSE);
      		$cari 		  = strtoupper($cari);*/
			$baris   = $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/cariarea/' . $baris . '/' . $cari . '/';
			else
				$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/cariarea/' . $baris . '/sikasep/';

			$query   	= $this->db->query("SELECT 
												*
											FROM
												tr_area
											WHERE
												i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser')
												AND i_area IN (SELECT i_store FROM tr_store WHERE f_aktif = 't')
												AND (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
											ORDER BY
												i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(6), $iuser);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function kirim()
	{
		$data = array();
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/kirim/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			// if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
			// 	$query = $this->db->query("select * from tr_dkb_kirim ", false);
			// } else {
			$query = $this->db->query("select * from tr_dkb_kirim where i_dkb_kirim='2'", false);
			// }
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkirim');

			$data['isi'] = $this->mmaster->bacakirim($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);

			$this->load->view($this->folder . '/vlistkirim', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function carikirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/kirim/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_kirim
						   where upper(i_dkb_kirim) like '%$cari%' 
						      or upper(e_dkb_kirim) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi'] = $this->mmaster->carikirim($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view($this->folder . '/vlistkirim', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function via()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea = $this->uri->segment('4');

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/via/index/';

			$query = $this->db->query("select * from tr_dkb_via ", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listvia');

			$data['isi'] 	= $this->mmaster->bacavia($config['per_page'], $this->uri->segment(5), $iarea);
			$data['iarea'] 	= $iarea;

			$this->load->view($this->folder . '/vlistvia', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function carivia()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/via/index/';

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			$iarea = $this->input->post('iarea', TRUE);

			$query = $this->db->query("	select * from tr_dkb_via
										where upper(i_dkb_via) like '%$cari%' 
										or upper(e_dkb_via) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listvia');

			$data['isi'] 	= $this->mmaster->carivia($cari, $config['per_page'], $this->uri->segment(5));
			$data['iarea'] 	= $iarea;

			$this->load->view($this->folder . '/vlistvia', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}



	function ekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area = $this->uri->segment(5);
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/ekspedisi/' . $baris . '/' . $area . '/';
			$query = $this->db->query("select * from tr_ekspedisi where i_area='$area' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);


			$data['page_title'] = $this->lang->line('vlistekspedisi');
			$data['isi'] = $this->mmaster->bacaekspedisi($area, $config['per_page'], $this->uri->segment(6));
			$data['area'] = $area;
			$data['baris'] = $baris;
			$this->load->view($this->folder . '/vlistekspedisi', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/ekspedisi/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris = strtoupper($this->input->post('xbaris', FALSE));
			$iarea = strtoupper($this->input->post('xarea', FALSE));
			$query = $this->db->query("select * from tr_ekspedisi
						                     where upper(i_ekspedisi) like '%$cari%' 
						                        or upper(e_ekspedisi) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('vlistekspedisi');
			$data['isi'] = $this->mmaster->cariekspedisi($cari, $config['per_page'], $this->uri->segment(5));
			$data['area'] = $iarea;
			$data['baris'] = $baris;
			$this->load->view($this->folder . '/vlistekspedisi', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['baris'] = $this->uri->segment(4);
			$baris = $this->uri->segment(4);
			$data['area'] = $this->uri->segment(5);
			$area = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/sjupdate/' . $baris . '/' . $area . '/index/';
			$area1	= $this->session->userdata('i_area');
			if ($area1 == '00') {
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
					where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' and a.i_dkb isnull and a.f_sj_cancel='f'
						and a.i_customer=b.i_customer and a.f_sj_daerah='f'", false);
			} else {
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
					where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    		and a.i_dkb isnull and a.f_sj_cancel='f'
						and a.i_customer=b.i_customer and a.f_sj_daerah='t'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);


			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi'] = $this->mmaster->bacasj($area, $config['per_page'], $this->uri->segment(7));
			$data['baris'] = $baris;
			$data['iarea'] = $area;
			$this->load->view($this->folder . '/vlistsjupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu160') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris = $this->uri->segment(4);
			$area = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/sjupdate/' . $baris . '/' . $area . '/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			if ($area1 == '00') {
				$query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
					                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                        and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                and a.i_customer=b.i_customer and a.f_sj_daerah='f'", false);
			} else {
				$query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
					                          where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                                    and a.i_dkb isnull and a.f_sj_cancel='f'
			                             	and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
					                          and a.i_customer=b.i_customer and a.f_sj_daerah='t'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi'] = $this->mmaster->carisj($area, $cari, $config['per_page'], $this->uri->segment(7));
			$data['iarea'] = $area;
			$data['baris'] = $baris;
			$this->load->view($this->folder . '/vlistsjupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function toleransi()
	{
		$iarea		= $this->input->post('iarea') ? $this->input->get_post('iarea') : $this->input->get_post('iarea');
		$idkbvia	= $this->input->post('idkbvia') ? $this->input->get_post('idkbvia') : $this->input->get_post('idkbvia');

		$query	= $this->db->query(" SELECT COALESCE (n_tolerance, 0) AS n_tolerance FROM tm_send_tolerance WHERE i_area = '$iarea' AND i_send_type = '$idkbvia' ");
		if ($query->num_rows() > 0) {
			$query  = array(
				'data' => $this->mmaster->get_toleransi($iarea, $idkbvia)->result_array()
			);
			echo json_encode($query);
		} else {
			echo "<script></script>";
		}
	}
}
