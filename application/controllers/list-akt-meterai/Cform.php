<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list-akt-meterai');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('list-akt-meterai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list-akt-meterai');
			$lepel		= $this->session->userdata('i_area');
			$cari 		= '';
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/list-akt-meterai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			
			if($iarea=="NA"){
				$query = $this->db->query("	SELECT a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
											a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
												FROM tm_alokasimt a
												INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
												a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
											UNION ALL
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' AS i_coa_bank
												FROM tm_meterai a
												INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												a.i_area, a.f_alokasi_cancel
											) AS a 
												WHERE
											a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%' ",false);
			}else{
				$query = $this->db->query(" SELECT a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
											a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank FROM (
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel, a.i_coa_bank
												FROM tm_alokasimt a
												INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												AND a.i_area = '$iarea'
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
												a.i_kbank, a.i_area, a.f_alokasi_cancel, a.i_coa_bank
											UNION ALL
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel, '' AS i_coa_bank
												FROM tm_meterai a
												INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												AND a.i_area = '$iarea'
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah,
												a.i_area, a.f_alokasi_cancel
											) AS a 
												WHERE
											a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%' ",false);
			}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->paginationxx->initialize($config);

			$this->load->model('list-akt-meterai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['lepel']		= $lepel;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$data['total']		= $this->mmaster->bacatotal($iarea,$dfrom,$dto);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    		$now	  = $row['c'];
			}
			$pesan='Membuka Informasi Alokasi Meterai Periode : '.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('list-akt-meterai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->view('list-akt-meterai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ialokasi	= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$dfrom		= $this->uri->segment(6);
			$dto		= $this->uri->segment(7);
      		$iareax		= $this->uri->segment(8);

			$this->load->model('list-akt-meterai/mmaster');

      		$this->db->trans_begin();
			
			$this->mmaster->delete($ialokasi,$iarea);
      		
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$sess	= $this->session->userdata('session_id');
				$id		= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Hapus Alokasi Meterai No : '.$ialokasi.' Area : '.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan ); 
				
				$this->db->trans_commit();
      		}
			
			$cari		= strtoupper($this->input->post('cari'));
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$lepel = $this->session->userdata('i_area');

			$config['base_url'] = base_url().'index.php/list-akt-meterai/cform/view/'.$dfrom.'/'.$dto.'/'.$iareax.'/index/';
			
			if($iareax=="NA"){
				$query = $this->db->query("	SELECT a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
											a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel FROM (
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel 
												FROM tm_alokasimt a
												INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
												a.i_kbank, a.i_area, a.f_alokasi_cancel
											UNION ALL
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel
												FROM tm_meterai a
												INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												a.i_area, a.f_alokasi_cancel
											) AS a 
												WHERE
											a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%' ",false);
			}else{
				$query = $this->db->query(" SELECT a.i_alokasi, a.d_alokasi, a.i_area, a.e_area_name, a.i_customer, 
											a.e_customer_name, a.v_jumlah, v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel FROM (
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, a.i_alokasi_reff, a.i_kbank, a.f_alokasi_cancel
												FROM tm_alokasimt a
												INNER JOIN tm_alokasimt_item b ON (a.i_alokasi = b.i_alokasi AND a.i_kbank = b.i_kbank AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												AND a.i_area = '$iarea'
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, a.i_alokasi_reff, 
												a.i_kbank, a.i_area, a.f_alokasi_cancel
											UNION ALL
												SELECT a.i_alokasi, a.d_alokasi, a.i_area, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah, 
												(a.v_jumlah-sum(b.v_sisa)) AS v_sisa, '' AS i_alokasi_reff, '' AS i_kbank, a.f_alokasi_cancel
												FROM tm_meterai a
												INNER JOIN tm_meterai_item b ON (a.i_alokasi = b.i_alokasi AND a.i_area = b.i_area)
												INNER JOIN tr_area d on (a.i_area = d.i_area)
												INNER JOIN tr_customer e on (a.i_area = e.i_area and e.i_customer = a.i_customer)
												WHERE
												a.d_alokasi between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
												AND a.i_area = '$iareax'
												GROUP BY a.i_alokasi, a.d_alokasi, d.e_area_name, a.i_customer, e.e_customer_name, a.v_jumlah,
												a.i_area, a.f_alokasi_cancel
											) AS a 
												WHERE
											a.i_alokasi LIKE '%$cari%' OR a.i_customer LIKE '%$cari%' ",false);
			}
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('list-akt-meterai');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iareax;
			$data['lepel']		= $lepel;
			$data['isi']		= $this->mmaster->bacaperiode($iareax,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$data['total']		= $this->mmaster->bacatotal($iareax,$dfrom,$dto);

			$this->load->view('list-akt-meterai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/list-akt-meterai/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('list-akt-meterai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('list-akt-meterai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu537')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/list-akt-meterai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      		$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('list-akt-meterai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('list-akt-meterai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
