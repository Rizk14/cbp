<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printbbmap');
			$data['apfrom']='';
			$data['apto']	='';
			$this->load->view('printbbmapkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function apfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printbbmapkelompok/cform/apfrom/index/';
			$query = $this->db->query("	select i_ap from tm_ap
										where upper(i_ap) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printbbmapkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_ap');
			$data['isi']=$this->mmaster->bacaap($config['per_page'],$this->uri->segment(5));
			$this->load->view('printbbmapkelompok/vlistapfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariapfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printbbmapkelompok/cform/apfrom/index/';
			$query = $this->db->query("	select i_ap, i_area from tm_ap
										where upper(i_ap) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printbbmapkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_ap');
			$data['isi']=$this->mmaster->cariap($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printbbmapkelompok/vlistapfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function apto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$area	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printbbmapkelompok/cform/apto/'.$area.'/';
			$query = $this->db->query("	select i_ap from tm_ap
										where upper(i_ap) like '%$cari%' and i_area ='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printbbmapkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_ap');
			$data['isi']=$this->mmaster->bacaap2($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('printbbmapkelompok/vlistapto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariapto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printbbmapkelompok/cform/apfrom/index/';
			$query = $this->db->query("	select i_ap, i_area from tm_ap
										where upper(i_ap) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printbbmapkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_ap');
			$data['isi']=$this->mmaster->cariap2($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printbbmapkelompok/vlistapto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu104')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('areafrom');
			$apfrom= $this->input->post('apfrom');
			$apto	= $this->input->post('apto');
			$this->load->model('printbbmapkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']	= $this->mmaster->bacamaster($apfrom,$apto);
			$data['area']		= $area;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']		= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak BBM-AP Area '.$area.' No:'.$apfrom.' s/d '.$apto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printbbmapkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
