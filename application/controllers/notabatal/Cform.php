<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu452')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('notabatal');
			$data['inota']='';
			$this->load->view('notabatal/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu452')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('notabatal');
			$this->load->view('notabatal/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu452')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']='';
  		$data['cari']='';
			$this->load->view('notabatal/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function carinota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu452')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='xswzaq')$cari='';
      if($cari==''){
  			$config['base_url'] = base_url().'index.php/notabatal/cform/carinota/xswzaq/';
  	  }else{
  			$config['base_url'] = base_url().'index.php/notabatal/cform/carinota/'.$cari.'/';
  	  }
			$query= $this->db->query("select * from tm_nota 
			                          where upper(i_nota) like '%$cari%' 
			                          and f_nota_cancel='f' and v_nota_gross=v_sisa
			                          order by i_nota",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('notabatal/mmaster');
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->carinota($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('notabatal/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
  function simpan()
  {
    if (
       (($this->session->userdata('logged_in')) &&
       (($this->session->userdata('menu452')=='t'))) ||
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('allmenu')=='t'))
       ){
#       if(
#          ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
#         )
#       {
          $inota  = $this->input->post('inota');
          $eremark= $this->input->post('eremark');
     			$user		= $this->session->userdata('user_id');
          $this->load->model('notabatal/mmaster');
          $this->db->trans_begin();
          $this->mmaster->batal($inota,$eremark,$user);
          if ( ($this->db->trans_status() === FALSE) )
            {
              $this->db->trans_rollback();
            }else{
              $this->db->trans_commit();
#               $this->db->trans_rollback();
               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Batal Nota No:'.$inota;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

               $data['sukses']         = true;
               $data['inomor']         = $inota;
               $this->load->view('nomor',$data);
            }
#       }else{
#          $this->load->view('notabatal/vinsert_fail',$data);
#       }
    }else{
       $this->load->view('awal/index.php');

    }
  }
}
?>
