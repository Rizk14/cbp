<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu428')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ppsales');
			$data['iperiode']='';

			$sess  = $this->session->userdata('session_id');
			$id   = $this->session->userdata('user_id');
			$sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs   = pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			 $ip_address   = $row['ip_address'];
			 break;
			}
			}else{
			$ip_address='kosong';
			}
			$query  = pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now    = $row['c'];
			}
			$pesan="Membuka Menu Pindah Periode Salesman";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$this->load->view('ppsales/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu428')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		) {
			  $data['page_title'] = $this->lang->line('ppsales');
			  $this->load->view('ppsales/vinsert_fail',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function pindah()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu428')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode1	= $this->input->post('iperiode1', TRUE);
			$iperiode2	= $this->input->post("iperiode2", TRUE);
			$this->db->trans_begin();
			$this->load->model('ppsales/mmaster');
			$this->mmaster->pindah($iperiode1,$iperiode2);

      if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $data['user']	= $this->session->userdata('user_id');
  #			$data['host']	= $this->session->userdata('printerhost');
		    $data['host']	= $ip_address;
		    $data['uri']	= $this->session->userdata('printeruri');
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Pindah Periode Salesman dari periode '.$iperiode1.' ke:'.$iperiode2;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );
        $this->db->trans_commit();
    		$data['sukses']			= true;
			  $data['inomor']			= $iperiode2;
			  $this->load->view('nomor',$data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
