<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu245')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transrrkh');
			$data['iperiode']	= '';
			$this->load->view('transrrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu245')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$data['page_title'] = $this->lang->line('transrrkh');
			$data['iperiode']	= $iperiode;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
      $db_file = 'spb/00/rkh'.substr($iperiode,2,4).'.dbf';
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $dbase_definition = array (
         array ('KODEAREA',  CHARACTER_FIELD,  2),
         array ('KODESALES',  CHARACTER_FIELD,  2),
         array ('TGLDOK',  DATE_FIELD),
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('WAKTU',  CHARACTER_FIELD, 10),
         array ('RENCANA',  CHARACTER_FIELD,  2),
         array ('REALISASI',  BOOLEAN_FIELD),
         array ('KET',  CHARACTER_FIELD,  20),
         array ('KUNJVALID',  BOOLEAN_FIELD),
         array ('USERID',  CHARACTER_FIELD,  1),
         array ('TGLBUAT', DATE_FIELD),
         array ('JAMBUAT',  CHARACTER_FIELD, 10),
         array ('TGLUBAH', DATE_FIELD),
         array ('JAMUBAH',  CHARACTER_FIELD, 10),
         array ('BATAL',  BOOLEAN_FIELD)
      );
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $sql	  = " select a.i_area, a.i_salesman, a.d_rrkh, a.f_rrkh_cancel, a.i_entry, a.d_entry, a.d_update, a.n_print, a.d_receive1,
                  b.i_customer, b.i_kunjungan_type, b.i_city, b.f_kunjungan_realisasi, b.f_kunjungan_valid, b.e_remark, b.n_item_no
                  from tm_rrkh a, tm_rrkh_item b
                  where a.i_area=b.i_area and a.i_salesman=b.i_salesman and a.d_rrkh=b.d_rrkh and to_char(a.d_rrkh,'yyyymm')= '$iperiode'
                  order by a.i_area, a.i_salesman, a.d_rrkh, b.n_item_no";
      $rspb		= $this->db->query($sql);
      foreach($rspb->result() as $rowpb){
        $KODEAREA   = $rowpb->i_area;
        $KODESALES  = $rowpb->i_salesman;
        $TGLDOK     = substr($rowpb->d_rrkh,0,4).substr($rowpb->d_rrkh,5,2).substr($rowpb->d_rrkh,8,2);
        $KODELANG   = $rowpb->i_customer;
        $WAKTU      = 
        $RENCANA    = $rowpb->i_kunjungan_type;
        if($rowpb->f_kunjungan_realisasi=='f') 
          $REALISASI= 'F'; 
        else 
          $REALISASI= 'T';
        $KET        = $rowpb->e_remark;
        if($rowpb->f_kunjungan_valid=='f') 
          $KUNJVALID= 'F'; 
        else 
          $KUNJVALID= 'T';
        $USERID     = '';
        $TGLBUAT    = substr($rowpb->d_entry,0,4).substr($rowpb->d_entry,5,2).substr($rowpb->d_entry,8,2);
        if($rowpb->d_entry!=''){
          $JAMBUAT  = substr($rowpb->d_entry,11,2).':'.substr($rowpb->d_entry,14,2).':'.substr($rowpb->d_entry,17,2);
        }else{
          $JAMBUAT  = '';
        }
        $TGLUBAH    = substr($rowpb->d_update,0,4).substr($rowpb->d_update,5,2).substr($rowpb->d_update,8,2);
        if($rowpb->d_update!=''){
          $JAMUBAH    = substr($rowpb->d_update,11,2).':'.substr($rowpb->d_update,14,2).':'.substr($rowpb->d_update,17,2);
        }else{
          $JAMUBAH  = '';
        }
        if($rowpb->f_rrkh_cancel=='f') 
          $BATAL= 'F'; 
        else 
          $BATAL= 'T';
        $isi = array ($KODEAREA,$KODESALES,$TGLDOK,$KODELANG,$WAKTU,$RENCANA,$REALISASI,$KET,$KUNJVALID,$USERID,$TGLBUAT,$JAMBUAT,
                      $TGLUBAH,$JAMUBAH,$BATAL);
        dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
      }
      dbase_close($id);
      @chmod($db_file, 0777);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke RRKH lama Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
