<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			if($cari=='' && $this->uri->segment(4)!='sikasep') $cari=$this->uri->segment(4);
			if($cari==''){
	  		$config['base_url'] = base_url().'index.php/adjustmentapprovecounter/cform/index/sikasep/';			
			}else{
  			$config['base_url'] = base_url().'index.php/adjustmentapprovecounter/cform/index/'.$cari.'/';			
			}

		  $query = $this->db->query(" select a.* from tm_adjmo a where (upper(a.i_adj) like '%$cari%')
		                              and a.i_approve isnull and a.f_adj_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('adjustmentapprovecounter');
			$this->load->model('adjustmentapprovecounter/mmaster');
			$data['iadj']='';
			$data['cari']		= $cari;
			$data['isi']		= $this->mmaster->bacaperiode($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('adjustmentapprovecounter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('adjustmentapprovecounter');
			$this->load->view('adjustmentapprovecounter/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/adjustmentapprovecounter/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.*, b.e_customer_name from tm_adjmo a, tr_customer b
										  where a.i_customer=b.i_customer and (a.i_customer like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
										  and a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_ttb <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_customer_name from tm_adjmo a, tr_customer b
										  where a.i_customer=b.i_customer and (a.i_customer like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
										  and a.i_area='$iarea' and
										  a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_ttb <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('adjustmentapprovecounter');
			$this->load->model('adjustmentapprovecounter/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('adjustmentapprovecounter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/adjustmentapprovecounter/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('adjustmentapprovecounter/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('adjustmentapprovecounter/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/adjustmentapprovecounter/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('adjustmentapprovecounter/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('adjustmentapprovecounter/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('adjustmentapprovecounter');
			if($this->uri->segment(4)!=''){
				$iadj    = $this->uri->segment(4);
				$iarea   = $this->uri->segment(5);
				$data['iarea'] = $iarea;
				$data['iadj'] = $iadj;
				$query = $this->db->query("select i_product from tm_adjmo_item where i_adj='$iadj' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('adjustmentapprovecounter/mmaster');
				$data['isi']=$this->mmaster->baca($iadj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($iadj,$iarea);
		 		$this->load->view('adjustmentapprovecounter/vmainform',$data);
			}else{
				$this->load->view('adjustmentapprovecounter/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu595')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iadj 		= $this->input->post('iadj', TRUE);
			$dadj 		= $this->input->post('dadj', TRUE);
			if($dadj!=''){
				$tmp=explode("-",$dadj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dadj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$iarea		        = $this->input->post('iarea', TRUE);
			$istore		        = $this->input->post('istore', TRUE);
			$istorelocation   = $this->input->post('istorelocation', TRUE);
			$istorelocationbin= '00';
			$eremark	        = $this->input->post('eremark', TRUE);
			$istockopname	    = $this->input->post('istockopname', TRUE);
			$jml		          = $this->input->post('jml', TRUE);
			if($dadj!='' && $istockopname!='' && $eremark!='' && $iarea!='')
			{
				$this->db->trans_begin();
				$this->load->model('adjustmentapprovecounter/mmaster');
#				$iadj	=$this->mmaster->runningnumber($thbl,$iarea);
        $user		=$this->session->userdata('user_id');
				$this->mmaster->approve($iadj, $iarea, $user);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= $this->input->post('grade'.$i, TRUE);
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
#############################################################################################################################################
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $query 	= $this->db->query("SELECT current_timestamp as c, to_char(current_timestamp,'yyyymm') as d");
            $row   	= $query->row();
            $emutasiperiode	  = $row->d;
            if($nquantity>0){
              $ibbm=$this->mmaster->runningnumberbbm($thbl,$iarea);
              $this->mmaster->insertheaderbbm($ibbm,$iadj,$dadj,$iarea,$eremark);
              $this->mmaster->insertdetailbbm($ibbm,$iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$nquantity,0,$eremark,$eproductname,$dadj,$i);
              $this->mmaster->inserttransbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbm,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
              {
                $this->mmaster->updateicbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
              }else{
                $this->mmaster->inserticbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
              }
            }else{
              $nquantity=$nquantity*-1;
              $ibbk=$this->mmaster->runningnumberbbk($thbl,$iarea);
              $this->mmaster->insertheaderbbk($ibbk,$iadj,$dadj,$iarea,$eremark);
              $this->mmaster->insertdetailbbk($ibbk,$iadj,$iarea,$iproduct,$iproductmotif,$iproductgrade,$nquantity,0,$eremark,$eproductname,$dadj,$i);
              $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasibbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
              {
                $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
              }else{
                $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
              }
            }
#############################################################################################################################################
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update Adjustment No:'.$iadj.' Area'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iadj;
					$this->load->view('nomor',$data);
			    $this->db->trans_commit();
#			    $this->db->trans_rollback();
				}			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
