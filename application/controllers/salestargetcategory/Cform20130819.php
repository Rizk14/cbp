<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 			= $this->input->post('iarea', TRUE);
			$eareaname 		= $this->input->post('eareaname', TRUE);
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesmanname 	= $this->input->post('esalesmanname', TRUE);
			$icity			= $this->input->post('icity', TRUE);
			$ecityname		= $this->input->post('ecityname', TRUE);
			$vareatarget	= $this->input->post('vareatarget', TRUE);
			$vareatarget	= str_replace(',','',$vareatarget);
			$vsalesmantarget= $this->input->post('vsalesmantarget', TRUE);
			$vsalesmantarget= str_replace(",","",$vsalesmantarget);
			$vcitytarget	= $this->input->post('vcitytarget', TRUE);
			$vcitytarget	= str_replace(",","",$vcitytarget);
			$bulan			= $this->input->post('bulan', TRUE);
			$tahun			= $this->input->post('tahun', TRUE);
			$iperiode		= $this->input->post('iperiode', TRUE);
			if (
				(isset($iarea) && $iarea != '') 
				&& (isset($eareaname) && $eareaname != '') 
				&& (isset($isalesman) && $isalesman != '') 
			    && (isset($esalesmanname) && $esalesmanname != '') 
				&& (isset($icity) && $icity != '') 
			    && (isset($ecityname) && $ecityname != '')
			    && (isset($bulan) && $bulan != '')
			    && (isset($tahun) && $tahun != '')
			    && (isset($iperiode) && $iperiode != '')
			   )
			{
				$this->db->trans_begin();
				$this->load->model('salestarget/mmaster');
				$this->mmaster->insert($iperiode,$iarea,$isalesman,$icity, $vareatarget, $vsalesmantarget, $vcitytarget);
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

				    $sess=$this->session->userdata('session_id');
				    $id=$this->session->userdata('user_id');
				    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				    $rs		= pg_query($sql);
				    if(pg_num_rows($rs)>0){
					    while($row=pg_fetch_assoc($rs)){
						    $ip_address	  = $row['ip_address'];
						    break;
					    }
				    }else{
					    $ip_address='kosong';
				    }
				    $query 	= pg_query("SELECT current_timestamp as c");
				    while($row=pg_fetch_assoc($query)){
					    $now	  = $row['c'];
				    }
				    $pesan='Input Target Sales Periode:'.$iperiode.' Area:'.$iarea;
				    $this->load->model('logger');
				    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $isalesman;
					$this->load->view('nomor',$data);
				}
			}
			else
			{
				$config['base_url'] = base_url().'index.php/salestarget/cform/index/';
				$query	= $this->db->query("select * from tm_target",false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);

				$data['page_title'] = $this->lang->line('salestarget');
				$data['iarea']='';
				$data['isalesman']='';
				$data['icity']='';
				$this->load->model('salestarget/mmaster');
				$cari = strtoupper($this->input->post('cari', FALSE));
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('salestarget/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('salestarget');
			$this->load->view('salestarget/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode		= $this->uri->segment(4);
			$iarea 			= $this->uri->segment(5);
			$targetarea		= $this->uri->segment(6);
			$isalesman 		= $this->uri->segment(7);
			$targetsales 	= $this->uri->segment(8);
			$icity			= $this->uri->segment(9);
			$targetkota		= $this->uri->segment(10);
			$query	= $this->db->query("select * from tr_area where i_area='$iarea'");
			foreach($query->result() as $row){
				$eareaname=$row->e_area_name;
			}
			$query	= $this->db->query("select * from tr_salesman where i_area='$iarea' and i_salesman='$isalesman'");
			foreach($query->result() as $row){
				$esalesmanname=$row->e_salesman_name;
			}
			$query	= $this->db->query("select * from tr_city where i_area='$iarea' and i_city='$icity'");
			foreach($query->result() as $row){
				$ecityname=$row->e_city_name;
			}
			$data['page_title'] 	= $this->lang->line('salestarget')." Update";
			$data['iarea']			= $iarea;
			$data['isalesman']		= $isalesman;
			$data['icity']			= $icity;
			$data['iperiode']		= $iperiode;
			$data['targetarea']		= $targetarea;
			$data['targetsales']	= $targetsales;
			$data['targetkota']		= $targetkota;
			$data['eareaname']		= $eareaname;
			$data['esalesmanname']	= $esalesmanname;
			$data['ecityname']		= $ecityname;
			$this->load->view('salestarget/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 			= $this->input->post('iarea', TRUE);
			$eareaname 		= $this->input->post('eareaname', TRUE);
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesmanname 	= $this->input->post('esalesmanname', TRUE);
			$icity			= $this->input->post('icity', TRUE);
			$ecityname		= $this->input->post('ecityname', TRUE);
			$vareatarget	= $this->input->post('vareatarget', TRUE);
			$vareatarget	= str_replace(',','',$vareatarget);
			$vsalesmantarget= $this->input->post('vsalesmantarget', TRUE);
			$vsalesmantarget= str_replace(",","",$vsalesmantarget);
			$vcitytarget	= $this->input->post('vcitytarget', TRUE);
			$vcitytarget	= str_replace(",","",$vcitytarget);
			$iperiode		= $this->input->post('iperiode', TRUE);
			if (
				(isset($iarea) && $iarea != '') 
				&& (isset($eareaname) && $eareaname != '') 
				&& (isset($isalesman) && $isalesman != '') 
			    && (isset($esalesmanname) && $esalesmanname != '') 
				&& (isset($icity) && $icity != '') 
			    && (isset($ecityname) && $ecityname != '')
			    && (isset($iperiode) && $iperiode != '')
			   )
			{
				$this->load->model('salestarget/mmaster');
				$this->mmaster->update($iperiode,$iarea,$isalesman,$icity, $vareatarget, $vsalesmantarget, $vcitytarget);
				
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalestarget 	= $this->input->post('isalestargetdelete', TRUE);
			$istore 		= $this->input->post('istoredelete', TRUE);
			$isalestargetbin 	= $this->input->post('isalestargetbindelete', TRUE);
			$this->load->model('salestarget/mmaster');
			$this->mmaster->delete($isalestarget, $istore, $isalestargetbin);
			
			$config['base_url'] = base_url().'index.php/salestarget/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_store_location');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('salestarget');
			$data['isalestarget']	='';
			$data['istore']		='';
			$this->load->model('salestarget/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('salestarget/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/salestarget/cform/area/'.$iperiode.'/index/';
			$query = $this->db->query("	select * from tm_target
										right join tr_area on (tm_target.i_area=tr_area.i_area and tm_target.i_periode='$iperiode')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['iperiode']	= $iperiode;
			$data['isi']=$this->mmaster->bacaarea($iperiode,$config['per_page'],$this->uri->segment(6));
			$this->load->view('salestarget/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari=strtoupper($this->input->post('cari',false));
			$iperiode=$this->input->post('iperiode',false);
			$config['base_url'] = base_url().'index.php/salestarget/cform/area/'.$iperiode.'/index/';
			$query = $this->db->query("	select * from tm_target
										right join tr_area on (tm_target.i_area=tr_area.i_area and tm_target.i_periode='$iperiode')
										where tr_area.i_area like '%$cari%' or tr_area.e_area_name like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['iperiode']	= $iperiode;
			$data['isi']=$this->mmaster->cariarea($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('salestarget/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/salestarget/cform/salesman/'.$iarea.'/'.$iperiode.'/index/';
			$query = $this->db->query("	select distinct on (tr_customer_salesman.i_salesman) tr_customer_salesman.i_salesman
                                  from tr_customer_salesman
                                  left join tm_target_itemsls on (tm_target_itemsls.i_salesman=tr_customer_salesman.i_salesman 
                                  and tm_target_itemsls.i_periode='$iperiode' and tm_target_itemsls.i_area='$iarea'
                                  and tm_target_itemsls.i_periode=tr_customer_salesman.e_periode 
                                  and tm_target_itemsls.i_area=tr_customer_salesman.i_area)
                                  where tr_customer_salesman.i_area='$iarea' and tr_customer_salesman.e_periode='$iperiode'",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['iarea']		=$iarea;
			$data['iperiode']	=$iperiode;
			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($iperiode,$iarea,$config['per_page'],$this->uri->segment(7));
			$this->load->view('salestarget/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisalesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari',false));
			$iarea		= $this->input->post('iarea',false);
			$iperiode	= $this->input->post('iperiode',false);
			$data['iarea']		=$iarea;
			$data['iperiode']	=$iperiode;
			$config['base_url'] = base_url().'index.php/salestarget/cform/salesman/'.$iarea.'/'.$iperiode.'/index/';
			$query = $this->db->query("	select distinct on (tr_customer_salesman.i_salesman) tr_customer_salesman.i_salesman 
                                  from tr_customer_salesman
                                  left join tm_target_itemsls on (tm_target_itemsls.i_salesman=tr_customer_salesman.i_salesman 
                                  and tm_target_itemsls.i_periode='$iperiode' and tm_target_itemsls.i_area='$iarea'
                                  and tm_target_itemsls.i_periode=tr_customer_salesman.e_periode 
                                  and tm_target_itemsls.i_area=tr_customer_salesman.i_area)
                                  where tr_customer_salesman.i_area='$iarea' and tr_customer_salesman.e_periode='$iperiode'
										and (tr_customer_salesman.i_salesman like '%$cari%' or tr_customer_salesman.e_salesman_name like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->carisalesman($iperiode,$iarea,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('salestarget/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$isalesman	= $this->uri->segment(6);
			$data['iarea']		= $iarea;
			$data['iperiode']	= $iperiode;
			$data['isalesman']	= $isalesman;
			$config['base_url'] = base_url().'index.php/salestarget/cform/city/'.$iarea.'/'.$iperiode.'/'.$isalesman.'/index/';
			$query = $this->db->query("	select * from tm_target_itemkota 
										right join tr_city on (tm_target_itemkota.i_city=tr_city.i_city 
										and tm_target_itemkota.i_periode='$iperiode' and tm_target_itemkota.i_salesman='$isalesman' 
                              and tm_target_itemkota.i_area='$iarea') 
										where tr_city.i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacacity($isalesman,$iperiode,$iarea,$config['per_page'],$this->uri->segment(8));
			$this->load->view('salestarget/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$iarea		= $this->input->post("iarea",false);
			$iperiode	= $this->input->post("iperiode",false);
			$isalesman	= $this->input->post("isalesman",false);
			$data['iarea']		= $iarea;
			$data['iperiode']	= $iperiode;
			$data['isalesman']	= $isalesman;
			$cari		= strtoupper($this->input->post('cari',false));
			$config['base_url'] = base_url().'index.php/salestarget/cform/city/'.$iarea.'/'.$iperiode.'/'.$isalesman.'/index/';
			$query = $this->db->query("	select * from tm_target_itemkota 
										right join tr_city on (tm_target_itemkota.i_city=tr_city.i_city 
										and tm_target_itemkota.i_periode='$iperiode' and tm_target_itemkota.i_salesman='$isalesman') 
										where tr_city.i_area='$iarea' 
										and (tr_city.e_city_name like '%$cari%' or tr_city.i_city like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('salestarget/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->caricity($isalesman,$iperiode,$iarea,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('salestarget/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu97')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/salestarget/cform/index/';
			$query = $this->db->query("	select i_store_location from tr_store_location 
										where upper(i_store) like '%$cari%' or upper(e_store_locationname) like '%$cari%'
										or upper(i_store_location) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari = $this->input->post('cari', FALSE);
			$this->load->model('salestarget/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('salestarget');
			$data['isalestarget']	='';
			$data['istore']		='';
	 		$this->load->view('salestarget/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
