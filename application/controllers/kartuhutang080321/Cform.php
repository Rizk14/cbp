<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartuhutang');
#			$data['iarea']='';
      $data['iperiode']='';
			$this->load->view('kartuhutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
      $iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$config['per_page'] = '10';
			$this->load->model('kartuhutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartuhutang');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
      $data['hal'] = $this->uri->segment(5);
			$data['isi']		= $this->mmaster->bacaperiode($iperiode,$config['per_page'],$this->uri->segment(5),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Kartu Piutang Periode:'.$iperiode;#.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('kartuhutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartuhutang');
			$this->load->view('kartuhutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartuhutang/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartuhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('kartuhutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/kartuhutang/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartuhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('kartuhutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('kartuhutang/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Cetak Kartu Piutang sampai tanggal:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$this->load->view('kartuhutang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu532')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isupplier= $this->input->post('isupplier');
      $iperiode	= $this->input->post('iperiode');
			if($isupplier=='') $isupplier	= $this->uri->segment(4);
			if($iperiode=='') $iperiode	= $this->uri->segment(6);
			$saldo	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/kartuhutang/cform/detail/'.$isupplier.'/'.$saldo.'/'.$iperiode.'/';
      $query = $this->db->query(" select supplier from(
                                  select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_dtap as tglbukti, a.i_dtap as bukti, 
                                  '' as jenis, '1. pembelian' as keterangan, a.v_netto as debet, 0 as kredit
                                  from tm_dtap a, tr_supplier c
                                  where f_dtap_cancel='f' and a.i_supplier=c.i_supplier and to_char(a.d_dtap, 'yyyymm')='$iperiode'
                                  and a.i_supplier='$isupplier'
                                  union all
                                  select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_bukti as tglbukti, 
                                  a.i_pelunasanap as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 0 as debet, 
                                  a.v_jumlah as kredit from tm_pelunasanap a, tr_supplier c, tr_jenis_bayar d
                                  where a.f_pelunasanap_cancel='f' and to_char(d_bukti, 'yyyymm')='$iperiode'
                                  and a.i_supplier=c.i_supplier and a.i_jenis_bayar=d.i_jenis_bayar 
                                  and a.i_supplier='$isupplier'
                                  
                                  union all
                                  select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti, 
                                  '' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
                                  from tm_alokasi_bk a, tr_supplier c
                                  where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
                                  and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
                                  union all
                                  select a.i_supplier as supplier, c.e_supplier_name as nama, a.d_alokasi as tglbukti, a.i_alokasi as bukti,
                                  '' as jenis, '2.' as keterangan, 0 as debet, a.v_jumlah as kredit
                                  from tm_alokasi_kb a, tr_supplier c
                                  where a.f_alokasi_cancel='f' and to_char(d_alokasi, 'yyyymm')='$iperiode'
                                  and a.i_supplier=c.i_supplier and a.i_supplier='$isupplier'
                                  ) as a
                                  ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('kartuhutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartuhutang');
			$data['isupplier']	= $isupplier;
			$data['iperiode']	  = $iperiode;
			$data['saldo']	    = $saldo;
			$data['num']	      = $config['per_page'];
			$data['offset']	    = $this->uri->segment(7);
			$data['isi']		    = $this->mmaster->bacadetail($isupplier,$iperiode,$config['per_page'],$this->uri->segment(7));
			$this->load->view('kartuhutang/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
