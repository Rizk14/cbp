<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('growthbyitemproduct');
			$data['todate']	= '';
			$this->load->view('growthbyitemproduct/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$todate	= $this->input->post('todate');
			if($todate=='') $todate=$this->uri->segment(4);
			if($todate!=''){
				$tmp=explode("-",$todate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$todate  =$th."-".$bl."-".$hr;
				$prevth  =$th-1;
				$prevdate=$prevth."-".$bl."-".$hr;
			}
			$config['base_url'] = base_url().'index.php/growthbyitemproduct/cform/view/'.$todate.'/index/';
			$this->load->model('growthbyitemproduct/mmaster');
			$data['page_title'] = $this->lang->line('growthbyitemproduct');
			$data['cari']	= $cari;
			$data['todate']	= $todate;
			$data['th']=$th;
			$data['prevth']=$prevth;
			$data['bl']=$bl;
			$data['total']	= $this->mmaster->bacatotal($todate,$prevdate,$th,$prevth,$bl);			
			$data['isi']	= $this->mmaster->baca($todate,$prevdate,$th,$prevth,$bl);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Sales Performance todate:'.$todate;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('growthbyitemproduct/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

  function chartx()
	{		
    $iperiode=$this->uri->segment(4);
    $data['charts'] = $this->getChart($iperiode);
    $this->load->view('growthbyitemproduct/charts',$data);
	}
	
  function getChart($iperiode)
	{
    $this->load->library('highcharts');
    $th=substr($iperiode,0,4);
    $bl=substr($iperiode,4,2);
    $bl=mbulan($bl);
    $this->highcharts->set_title('Target Penjualan Per area', 'Periode : '.$bl.' '.$th);
    $this->highcharts->set_dimensions(1340, 500); 
    $this->highcharts->set_type('column');
    $this->highcharts->set_axis_titles('Area', 'Nilai');
    $credits->href = base_url();
    $credits->text = NmPerusahaan;
    $this->highcharts->set_credits($credits);
#    $this->highcharts->render_to("my_div");
    $this->load->model('growthbyitemproduct/mmaster');
    $result = $this->mmaster->bacatarget($iperiode);
    foreach($result as $row){
      $target[] = intval($row->v_target);
    }
    $result = $this->mmaster->bacaspb($iperiode);
    foreach($result as $row){
      $spb[] = intval($row->v_spb_gross);
    }
    $result = $this->mmaster->bacasj($iperiode);
    foreach($result as $row){
      $sj[] = intval($row->v_sj_gross);
    }
    $result = $this->mmaster->bacanota($iperiode);
    foreach($result as $row){
      $nota[] = intval($row->v_nota_gross);
    }
    $result = $this->mmaster->bacaarea($iperiode);
    foreach($result as $row){
      $area[] = $row->i_area;
    }
    $data['axis']['categories'] = $area;
    $data['targets']['data'] = $target;
		$data['targets']['name'] = 'Target';
    $data['spbs']['data'] = $spb;
		$data['spbs']['name'] = 'SPB';
    $data['sjs']['data'] = $sj;
		$data['sjs']['name'] = 'SJ';
    $data['notas']['data'] = $nota;
		$data['notas']['name'] = 'Nota';
  
    $this->highcharts->set_xAxis($data['axis']);
		$this->highcharts->set_serie($data['targets']);
		$this->highcharts->set_serie($data['spbs']);
		$this->highcharts->set_serie($data['sjs']);
		$this->highcharts->set_serie($data['notas']);
    return $this->highcharts->render();
	}
  function fcf(){
    $iperiode=$this->uri->segment(4);
    $tipe=$this->uri->segment(5);
    if($tipe==''){
      $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
    }else{
      $tipe=str_replace("tandatitik",".",$tipe);
      $graph_swfFile      = base_url().'flash/'.$tipe;
    }
    $th=substr($iperiode,0,4);
    $bl=substr($iperiode,4,2);
    $bl=mbulan($bl);
    $graph_caption      = 'Target Penjualan Per Area Periode : '.$bl.' '.$th ;
    $graph_numberPrefix = 'Rp.' ;
    $graph_title        = 'Penjualan Produk' ;
    $graph_width        = 954;
    $graph_height       = 500;
    $this->load->model('growthbyitemproduct/mmaster');

    // Area
    $i=0;
    $result = $this->mmaster->bacaarea($iperiode);
    foreach($result as $row){
      $category[$i] = $row->i_area;
      $i++;
    }

    // data set
    $dataset[0] = 'Target' ;
    $dataset[1] = 'SPB' ;
    $dataset[2] = 'SJ' ;
    $dataset[3] = 'Nota' ;

    //data 1
    $i=0;
    $result = $this->mmaster->bacatarget($iperiode);
    foreach($result as $row){
      $arrData['Target'][$i] = intval($row->v_target);
      $i++;
    }

    //data 2
    $i=0;
    $result = $this->mmaster->bacaspb($iperiode);
    foreach($result as $row){
      $arrData['SPB'][$i] = intval($row->v_spb_gross);
      $i++;
    }

    //data 3
    $i=0;
    $result = $this->mmaster->bacasj($iperiode);
    foreach($result as $row){
      $arrData['SJ'][$i] = intval($row->v_sj_gross);
      $i++;
    }


    //data 4
    $i=0;
    $result = $this->mmaster->bacanota($iperiode);
    foreach($result as $row){
      $arrData['Nota'][$i] = intval($row->v_nota_gross);
      $i++;
    }

    $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0'>" ;

    //Convert category to XML and append
    $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
    foreach ($category as $c) {
        $strXML .= "<category name='".$c."'/>" ;
    }
    $strXML .= "</categories>" ;

    //Convert dataset and data to XML and append
    foreach ($dataset as $set) {
        $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
        foreach ($arrData[$set] as $d) {
            $strXML .= "<set value='".$d."'/>" ;
        }
        $strXML .= "</dataset>" ;
    }

//Close <chart> element
$strXML .= "</graph>";

    $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
    $data['iperiode']=$iperiode;
    $data['modul']='growthbyitemproduct';
		$data['isi']= directory_map('./flash/');
		$data['file']='';

    $this->load->view('growthbyitemproduct/chart_view',$data) ;
  }
}
?>
