<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bapbsjpb');
			$this->load->model('bapbsjpb/mmaster');
			$data['tgl']=date('d-m-Y');
			$this->load->view('bapbsjpb/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);

			$config['base_url'] = base_url().'index.php/bapbsjpb/cform/sj/'.$baris.'/index/';

			$query = $this->db->query(" select a.i_sjpb, a.d_sjpb, sum(b.n_deliver * b.v_unit_price) as v_sjpb from tm_sjpb a, tm_sjpb_item b
				where 
				a.i_sjpb = b.i_sjpb
				and a.i_area = b.i_area
				and a.i_bapb isnull 
				and a.f_sjpb_cancel = 'f' 
				and a.i_area_entry isnull 
				and a.d_sjpb >= '2019-05-01'
				group by a.i_sjpb, a.d_sjpb
				order by i_sjpb desc ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->bacasj2($config['per_page'],$this->uri->segment(6));

			$data['baris']=$baris;
			$this->load->view('bapbsjpb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$baris		= $this->input->post('baris', TRUE);

			$config['base_url'] = base_url().'index.php/bapbsjpb/cform/sj/'.$baris.'/index/';

			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.i_sjpb, a.d_sjpb, sum(b.n_deliver * b.v_unit_price) as v_sjpb from tm_sjpb a, tm_sjpb_item b
				where 
				a.i_sjpb = b.i_sjpb
				and a.i_area = b.i_area
				and a.i_bapb isnull 
				and a.f_sjpb_cancel = 'f' 
				and a.i_area_entry isnull
				and a.d_sjpb >= '2019-05-01'
				and upper(a.i_sjpb) like '%$cari%' 
				group by a.i_sjpb, a.d_sjpb
				order by i_sjpb desc ",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bapbsjpb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$dbapb 	= $this->input->post('dbapb', TRUE);
			$vbapb	= $this->input->post('vbapb', TRUE);
			$vbapb	= str_replace(',','',$vbapb);
			$jml 	=  $this->input->post('jml', TRUE);

			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}

			if($dbapb!='' &&  $vbapb!='0' )
			{
				$iarea = 'PB';
				$this->db->trans_begin();
				$this->load->model('bapbsjpb/mmaster');
				$ibapb	=$this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insertheader($ibapb, $dbapb, $iarea, $vbapb);
				$nilaitotal = 0;
				for($i=1;$i<=$jml;$i++){
					$isj			= $this->input->post('isj'.$i, TRUE);
					$dsj			= $this->input->post('dsj'.$i, TRUE);
					
					if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
					}
					$vsj	= $this->input->post('vsj'.$i, TRUE);
					$vsj	= str_replace(',','',$vsj);
					$eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
					$this->mmaster->insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj);
					$this->mmaster->updatesj($ibapb,$isj,$iarea,$dbapb);
					$nilaitotal = $nilaitotal + $vsj;
				}
				$this->mmaster->updatesjb($ibapb,$iarea,$nilaitotal);


				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input BAPB-SJPB No:'.$ibapb.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('bapbsjpb')." update";
			if($this->uri->segment(4)!=''){
				$ibapb 		= $this->uri->segment(4);
				$iarea		= $this->uri->segment(5);
				$dfrom		= $this->uri->segment(6);
				$dto 			= $this->uri->segment(7);
				$query = $this->db->query("select * from tm_bapbsjpb_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;				
				$this->load->model('bapbsjpb/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$this->load->view('bapbsjpb/vformupdate',$data);
			}else{
				$this->load->view('bapbsjpb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$ibapb		= $this->uri->segment(4);
			$ibapb  	= str_replace('%20','',$ibapb);
			$iarea		= $this->uri->segment(5);
			$isj			= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);
			$this->db->trans_begin();
			$area1	= $this->session->userdata('i_area');
			if($area1=='00'){
				$daer='f';
			}else{
				$daer='t';
			}
			$this->load->model('bapbsjpb/mmaster');
			$this->mmaster->deletedetail($ibapb, $iarea, $isj, $daer);
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Delete item bapb-sjp no:'.$ibapb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('bapbsjpb')." update";
				$query = $this->db->query("select * from tm_bapbsjpb_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;				
				$this->load->model('bapbsjpb/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$this->load->view('bapbsjpb/vformupdate',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function update(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$dbapb 	= $this->input->post('dbapb', TRUE);
			$vbapb	= $this->input->post('vbapb', TRUE);
			$vbapb	= str_replace(',','',$vbapb);
			$jml 	=  $this->input->post('jml', TRUE);
			$ibapb	=  $this->input->post('ibapb', TRUE);

			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}

			if($dbapb!='' &&  $vbapb!='0' )
			{
				$iarea = 'PB';
				$this->db->trans_begin();
				$this->load->model('bapbsjpb/mmaster');
				
				$this->mmaster->deleteitem($ibapb, $iarea);
				$nilaitotal = 0;
				for($i=1;$i<=$jml;$i++){
					$isj			= $this->input->post('isj'.$i, TRUE);
					$dsj			= $this->input->post('dsj'.$i, TRUE);
					
					if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
					}
					$vsj	= $this->input->post('vsj'.$i, TRUE);
					$vsj	= str_replace(',','',$vsj);
					$eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
					$this->mmaster->insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj);
					$this->mmaster->updatesj($ibapb,$isj,$iarea,$dbapb);
					
					$nilaitotal = $nilaitotal + $vsj;
				}
				$this->mmaster->updatesjb($ibapb,$iarea,$nilaitotal);
				$this->mmaster->updatesjpb($ibapb,$iarea);

				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update BAPB-SJPB No:'.$ibapb.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}


}
?>
