<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('updatekunjungan');
			$data['dfrom']='';
			$data['dto']='';				
			$this->load->view('updatekunjungan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/updatekunjungan/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
						
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('updatekunjungan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('updatekunjungan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/updatekunjungan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('updatekunjungan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('updatekunjungan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$dfrom= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$nlama= $this->input->post('nlamaorder');
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($nlama=='') $nlama=$this->uri->segment(6);

      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$this->load->model('updatekunjungan/mmaster');
			$data['page_title'] = $this->lang->line('updatekunjungan');
			$data['dfrom']= $dfrom;
			$data['dto']	= $dto;
			$data['nlama']= $nlama;
      $tmp=explode("-",$dfrom);
      $th1=$tmp[0];
      $bl1=$tmp[1];
      $hr1=$tmp[2];
      $tmp=explode("-",$dto);
      $th2=$tmp[0];
      $bl2=$tmp[1];
      $hr2=$tmp[2];
      if( ($th1==$th2)&&($bl1==$bl2)&&($hr1=='01')&&($hr2=='28'||$hr2=='29'||$hr2=='30'||$hr2=='31') ){
  			$this->mmaster->simpan($nlama,$dfrom,$dto);
      }
			$data['isi']	= $this->mmaster->bacaperiode($nlama,$dfrom,$dto);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Proses Update kunjungan Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('updatekunjungan/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function order()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$dfrom= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$isalesman= '';
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isalesman=='') $isalesman=$this->uri->segment(6);

      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$this->load->model('updatekunjungan/mmaster');
			$data['page_title'] = $this->lang->line('updatekunjungan');
			$data['dfrom']= $dfrom;
			$data['dto']	= $dto;
			$data['isalesman']= $isalesman;
      $tmp=explode("-",$dfrom);
      $th1=$tmp[0];
      $bl1=$tmp[1];
      $hr1=$tmp[2];
      $tmp=explode("-",$dto);
      $th2=$tmp[0];
      $bl2=$tmp[1];
      $hr2=$tmp[2];
			$data['isi']	= $this->mmaster->bacaorder($isalesman,$dfrom,$dto);
			$this->load->view('updatekunjungan/vformvieworder',$data);			
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kunjungan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu343')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$dfrom= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$isalesman= '';
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isalesman=='') $isalesman=$this->uri->segment(6);
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$this->load->model('updatekunjungan/mmaster');
			$data['page_title'] = $this->lang->line('updatekunjungan');
			$data['dfrom']= $dfrom;
			$data['dto']	= $dto;
			$data['isalesman']= $isalesman;
      $tmp=explode("-",$dfrom);
      $th1=$tmp[0];
      $bl1=$tmp[1];
      $hr1=$tmp[2];
      $tmp=explode("-",$dto);
      $th2=$tmp[0];
      $bl2=$tmp[1];
      $hr2=$tmp[2];
			$data['isi']	= $this->mmaster->bacakunjungan($isalesman,$dfrom,$dto);
			$this->load->view('updatekunjungan/vformviewkunjungan',$data);			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
