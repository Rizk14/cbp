<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerservice');
			$data['icustomerservice']='';
			$this->load->model('customerservice/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Layanan Pelanggan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerservice/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerservice 	= $this->input->post('icustomerservice', TRUE);
			$ecustomerservicename 	= $this->input->post('ecustomerservicename', TRUE);

			if ((isset($icustomerservice) && $icustomerservice != '') && (isset($ecustomerservicename) && $ecustomerservicename != ''))
			{
				$this->load->model('customerservice/mmaster');
				$this->mmaster->insert($icustomerservice,$ecustomerservicename);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Layanan Pelanggan:('.$icustomerservice.') -'.$ecustomerservicename;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$data['page_title'] = $this->lang->line('master_customerservice');
				$data['icustomerservice']='';
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('customerservice/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerservice');
			$this->load->view('customerservice/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerservice')." update";
			if($this->uri->segment(4)){
				$icustomerservice = $this->uri->segment(4);
				$data['icustomerservice'] = $icustomerservice;
				$this->load->model('customerservice/mmaster');
				$data['isi']=$this->mmaster->baca($icustomerservice);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Layanan Pelanggan:('.$icustomerservice.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerservice/vmainform',$data);
			}else{
				$this->load->view('customerservice/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerservice	= $this->input->post('icustomerservice', TRUE);
			$ecustomerservicename 	= $this->input->post('ecustomerservicename', TRUE);
			$this->load->model('customerservice/mmaster');
			$this->mmaster->update($icustomerservice,$ecustomerservicename);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Layanan Pelanggan:('.$icustomerservice.') -'.$ecustomerservicename;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('master_customerservice');
			$data['icustomerservice']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerservice/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu31')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerservice	= $this->uri->segment(4);
			$this->load->model('customerservice/mmaster');
			$this->mmaster->delete($icustomerservice);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Layanan Pelanggan:'.$icustomerservice;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$data['page_title'] = $this->lang->line('master_customerservice');
			$data['icustomerservice']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerservice/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
