<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerproducttype');
			$data['icustomerproducttype']='';
			$this->load->model('customerproducttype/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Tipe Produk Pelanggan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerproducttype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype 	= $this->input->post('icustomerproducttype', TRUE);
			$ecustomerproducttypename = $this->input->post('ecustomerproducttypename', TRUE);

			if ((isset($icustomerproducttype) && $icustomerproducttype != '') && (isset($ecustomerproducttypename) && $ecustomerproducttypename != ''))
			{
				$this->load->model('customerproducttype/mmaster');
				$this->mmaster->insert($icustomerproducttype,$ecustomerproducttypename);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Tipe Produk Pelanggan:('.$icustomerproducttype.') -'.$ecustomerproducttypename;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$data['page_title'] = $this->lang->line('master_customerproducttype');
				$data['icustomerproducttype']='';
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('customerproducttype/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerproducttype');
			$this->load->view('customerproducttype/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerproducttype')." update";
			if($this->uri->segment(4)){
				$icustomerproducttype = $this->uri->segment(4);
				$data['icustomerproducttype'] = $icustomerproducttype;
				$this->load->model('customerproducttype/mmaster');
				$data['isi']=$this->mmaster->baca($icustomerproducttype);
				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Tipe Produk Pelanggan:('.$icustomerproducttype.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerproducttype/vmainform',$data);
			}else{
				$this->load->view('customerproducttype/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$ecustomerproducttypename 	= $this->input->post('ecustomerproducttypename', TRUE);
			$this->load->model('customerproducttype/mmaster');
			$this->mmaster->update($icustomerproducttype,$ecustomerproducttypename);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Tipe Produk Pelanggan:('.$icustomerproducttype.') -'.$ecustomerproducttypename;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('master_customerproducttype');
			$data['icustomerproducttype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerproducttype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu35')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype	= $this->uri->segment(4);
			$this->load->model('customerproducttype/mmaster');
			$this->mmaster->delete($icustomerproducttype);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Tipe Produk Pelanggan:'.$icustomerproducttype;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$data['page_title'] = $this->lang->line('master_customerproducttype');
			$data['icustomerproducttype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerproducttype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
