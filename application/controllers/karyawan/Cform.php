<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		        ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query(" select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%'",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_karyawan');
			$data['i_nik']='';
			$this->load->model('karyawan/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Karyawan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('karyawan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$i_nik			    		= $this->input->post('i_nik', TRUE);
			$e_nama_karyawan_lengkap	= $this->input->post('e_nama_karyawan_lengkap', TRUE);
			$i_department			  	= $this->input->post('i_department', TRUE);
			$e_nama_karyawan_panggilan	= $this->input->post('e_nama_karyawan_panggilan', TRUE);
			$i_area						= $this->input->post('i_area', TRUE);
			$e_jenis_kelamin			= $this->input->post('e_jenis_kelamin', TRUE);
			$i_karyawan_status			= $this->input->post('i_karyawan_status', TRUE);
			$e_tempat_lahir				= $this->input->post('e_tempat_lahir', TRUE);
			$d_kontrak_ke1_awal			= $this->input->post('d_kontrak_ke1_awal', TRUE);
      		$d_lahir					= $this->input->post('d_lahir', TRUE);
			$d_kontrak_ke1_akhir		= $this->input->post('d_kontrak_ke1_akhir', TRUE);
			$e_agama					= $this->input->post('e_agama', TRUE);
			$d_kontrak_ke2_awal			= $this->input->post('d_kontrak_ke2_awal', TRUE);
			$e_no_ktp					= $this->input->post('e_no_ktp', TRUE);
			$d_kontrak_ke2_akhir		= $this->input->post('d_kontrak_ke2_akhir', TRUE);
			$e_no_kk					= $this->input->post('e_no_kk', TRUE);
			$d_kontrak_ke3_awal			= $this->input->post('d_kontrak_ke3_awal', TRUE);
			$e_alamat_ktp				= $this->input->post('e_alamat_ktp', TRUE);
			$d_kontrak_ke3_akhir		= $this->input->post('d_kontrak_ke3_akhir', TRUE);
			$e_alamat_sekarang			= $this->input->post('e_alamat_sekarang', TRUE);
			$d_join_date				= $this->input->post('d_join_date', TRUE);
			$e_pendidikan_terakhir		= $this->input->post('e_pendidikan_terakhir', TRUE);
			$e_nomor_bpjs_ketenagakerjaan	= $this->input->post('e_nomor_bpjs_ketenagakerjaan', TRUE);
			$e_jurusan					= $this->input->post('e_jurusan', TRUE);
			$e_nomor_bpjs_kesehatan		= $this->input->post('e_nomor_bpjs_kesehatan', TRUE);
			$e_alamat_pasangan			= $this->input->post('e_alamat_pasangan', TRUE);
			$e_nomor_npwp				= $this->input->post('e_nomor_npwp', TRUE);
			$e_status_pernikahan		= $this->input->post('e_status_pernikahan', TRUE);
			$e_sim_a					= $this->input->post('e_sim_a', TRUE);
			$e_nama_pasangan			= $this->input->post('e_nama_pasangan', TRUE);
			$e_sim_b					= $this->input->post('e_sim_b', TRUE);
			$e_asal_universitas_sekolah	= $this->input->post('e_asal_universitas_sekolah', TRUE);
			$e_sim_c					= $this->input->post('e_sim_c', TRUE);
			$e_nomor_telepon			= $this->input->post('e_nomor_telepon', TRUE);
			$e_karyawan_daerah					= $this->input->post('e_karyawan_daerah', TRUE);
			$e_karyawan_jabatan			= $this->input->post('e_karyawan_jabatan', TRUE);
			$e_karyawan_penempatan			= $this->input->post('e_karyawan_penempatan', TRUE);
			$v_sisa_cuti 				= '7';
			
			if($d_lahir!=''){
				$tmp=explode("-",$d_lahir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_lahir=$th."-".$bl."-".$hr;
			}else{
        $d_lahir=null;
      }

      		if($d_kontrak_ke1_awal!=''){
					$tmp=explode("-",$d_kontrak_ke1_awal);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$d_kontrak_ke1_awal=$th."-".$bl."-".$hr;
				}else{
	        $d_kontrak_ke1_awal=null;
	      }
	      if($d_kontrak_ke1_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke1_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke1_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke1_akhir=null;
      }
      	  if($d_kontrak_ke2_awal!=''){
				$tmp=explode("-",$d_kontrak_ke2_awal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke2_awal=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke2_awal=null;
      }
      	  if($d_kontrak_ke2_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke2_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke2_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke2_akhir=null;
      }
      	  if($d_kontrak_ke3_awal!=''){
				$tmp=explode("-",$d_kontrak_ke3_awal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke3_awal=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke3_awal=null;
      }
      	  if($d_kontrak_ke3_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke3_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke3_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke3_akhir=null;
      }
      	  if($d_join_date!=''){
				$tmp=explode("-",$d_join_date);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_join_date=$th."-".$bl."-".$hr;
				$kode_nik=$th.$bl.$hr;
				$sub_kalimat = substr($kode_nik,2);
				$th_skrng=date('Y');
				$th_jml=$th_skrng-$th;
				$th_masaberlaku=$th+$th_jml;
				$th_cuti_akhir=$th_masaberlaku+1;
			 	$d_masaberlaku_cutiakhir= $th_cuti_akhir."-".$bl."-".$hr;
				$d_masaberlaku_cutiawal	= $th_masaberlaku."-".$bl."-".$hr;
			 	$th_dptcuti=$th+1;
				}else{
        $d_join_date=null;
      }
      		$v_hak_cuti='12';
      		$v_cuti_lebaran='6';
      		$v_saldo_cuti=$v_hak_cuti-$v_cuti_lebaran;


				$query = $this->db->query(" select i_nik from tr_karyawan where i_nik like '%$sub_kalimat%'" ,false);
          		$tot   = $query->num_rows();
           if($tot!=''){
      		$no_urut	= $tot+1;
			$nik_jadi	=$sub_kalimat.'00'.$nourut;
			$text		=$sub_kalimat;
			$no_urut	='00'.$nourut;   	
		   	$this->load->model('karyawan/mmaster');
					$this->mmaster->insertheader($text,$no_urut,$nik_jadi, $i_department, $i_area,$i_karyawan_status,$d_kontrak_ke1_awal, $d_kontrak_ke1_akhir, $d_kontrak_ke2_awal, $d_kontrak_ke2_akhir,$d_kontrak_ke3_awal, $d_kontrak_ke3_akhir, $d_join_date, $e_nomor_bpjs_ketenagakerjaan,
												 $e_nomor_bpjs_kesehatan, $e_nomor_npwp, $e_sim_a,$e_sim_b,$e_sim_c,$e_karyawan_jabatan,$e_karyawan_penempatan,$e_karyawan_daerah,$v_saldo_cuti);
		       		$this->mmaster->insertdetail($nik_jadi, $e_nama_karyawan_lengkap, $e_nama_karyawan_panggilan, $e_jenis_kelamin, 
												 $e_tempat_lahir, $d_lahir, $e_agama, $e_no_kk, 
											 	 $e_no_ktp, $e_alamat_ktp, $e_alamat_sekarang, $e_jurusan,
												 $e_asal_universitas_sekolah, $e_pendidikan_terakhir, $e_status_pernikahan,$e_nama_pasangan,$e_alamat_pasangan,$e_nomor_telepon
												);
		       		

		   }else{

					$nik_jadi	=$sub_kalimat.'001';
					$text		=$sub_kalimat;
					$no_urut	='001';
					$this->load->model('karyawan/mmaster');
					$this->mmaster->insertheader($text,$no_urut,$nik_jadi, $i_department, $i_area,$i_karyawan_status,$d_kontrak_ke1_awal, $d_kontrak_ke1_akhir, $d_kontrak_ke2_awal, $d_kontrak_ke2_akhir,$d_kontrak_ke3_awal, $d_kontrak_ke3_akhir, $d_join_date, $e_nomor_bpjs_ketenagakerjaan,
												 $e_nomor_bpjs_kesehatan, $e_nomor_npwp, $e_sim_a,$e_sim_b,$e_sim_c,$e_karyawan_jabatan,$e_karyawan_penempatan,$e_karyawan_daerah,$v_saldo_cuti);
		       		$this->mmaster->insertdetail($nik_jadi, $e_nama_karyawan_lengkap, $e_nama_karyawan_panggilan, $e_jenis_kelamin, 
												 $e_tempat_lahir, $d_lahir, $e_agama, $e_no_kk, 
											 	 $e_no_ktp, $e_alamat_ktp, $e_alamat_sekarang, $e_jurusan,
												 $e_asal_universitas_sekolah, $e_pendidikan_terakhir, $e_status_pernikahan,$e_nama_pasangan,$e_alamat_pasangan,$e_nomor_telepon
												);				
				}
			if($th!=$th_skrng){
					
					$this->load->model('karyawan/mmaster');
					$this->mmaster->insertheadercuti($nik_jadi,$d_masaberlaku_cutiawal,$d_masaberlaku_cutiakhir,$v_hak_cuti,$v_cuti_lebaran,$v_saldo_cuti,$th_skrng);
			}else{
					$this->load->model('karyawan/mmaster');
					$this->mmaster->insertheadernotcuti($nik_jadi,$d_masaberlaku_cutiawal,$d_masaberlaku_cutiakhir,$v_hak_cuti,$v_cuti_lebaran,$v_saldo_cuti,$th_dptcuti,$th_skrng);
			}

			$nomor=$nik_jadi." - ".$e_nama_karyawan_lengkap;
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Input Karyawan:'.$nik_jadi.' Nama Karyawan:'.$e_nama_karyawan_lengkap;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

						$data['sukses']			= true;
						$data['inomor']			= $nomor;
						$this->load->view('nomor',$data);
					}
        	
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_karyawan');
			$this->load->view('karyawan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_karyawan')." update";
			if($this->uri->segment(4)){
				$i_nik = $this->uri->segment(4);
				$data['i_nik'] = $i_nik;
				$this->load->model('karyawan/mmaster');
				$data['isi']=$this->mmaster->baca($i_nik);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Karyawan ('.$i_nik.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('karyawan/vmainform',$data);
			}else{
				$this->load->view('productbase/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$i_nik			    		= $this->input->post('i_nik', TRUE);
			$e_nama_karyawan_lengkap	= $this->input->post('e_nama_karyawan_lengkap', TRUE);
			$i_department			  	= $this->input->post('i_department', TRUE);
			$e_nama_karyawan_panggilan	= $this->input->post('e_nama_karyawan_panggilan', TRUE);
			$i_area						= $this->input->post('i_area', TRUE);
			$e_jenis_kelamin			= $this->input->post('e_jenis_kelamin', TRUE);
			$i_karyawan_status			= $this->input->post('i_karyawan_status', TRUE);
			$e_tempat_lahir				= $this->input->post('e_tempat_lahir', TRUE);
			$d_kontrak_ke1_awal			= $this->input->post('d_kontrak_ke1_awal', TRUE);
      		$d_lahir					= $this->input->post('d_lahir', TRUE);
			$d_kontrak_ke1_akhir		= $this->input->post('d_kontrak_ke1_akhir', TRUE);
			$e_agama					= $this->input->post('e_agama', TRUE);
			$d_kontrak_ke2_awal			= $this->input->post('d_kontrak_ke2_awal', TRUE);
			$e_no_ktp					= $this->input->post('e_no_ktp', TRUE);
			$d_kontrak_ke2_akhir		= $this->input->post('d_kontrak_ke2_akhir', TRUE);
			$e_no_kk					= $this->input->post('e_no_kk', TRUE);
			$d_kontrak_ke3_awal			= $this->input->post('d_kontrak_ke3_awal', TRUE);
			$e_alamat_ktp				= $this->input->post('e_alamat_ktp', TRUE);
			$d_kontrak_ke3_akhir		= $this->input->post('d_kontrak_ke3_akhir', TRUE);
			$e_alamat_sekarang			= $this->input->post('e_alamat_sekarang', TRUE);
			$d_join_date				= $this->input->post('d_join_date', TRUE);
			$e_pendidikan_terakhir		= $this->input->post('e_pendidikan_terakhir', TRUE);
			$e_nomor_bpjs_ketenagakerjaan	= $this->input->post('e_nomor_bpjs_ketenagakerjaan', TRUE);
			$e_jurusan					= $this->input->post('e_jurusan', TRUE);
			$e_nomor_bpjs_kesehatan		= $this->input->post('e_nomor_bpjs_kesehatan', TRUE);
			$e_alamat_pasangan			= $this->input->post('e_alamat_pasangan', TRUE);
			$e_nomor_npwp				= $this->input->post('e_nomor_npwp', TRUE);
			$e_status_pernikahan		= $this->input->post('e_status_pernikahan', TRUE);
			$e_sim_a					= $this->input->post('e_sim_a', TRUE);
			$e_nama_pasangan			= $this->input->post('e_nama_pasangan', TRUE);
			$e_sim_b					= $this->input->post('e_sim_b', TRUE);
			$e_asal_universitas_sekolah	= $this->input->post('e_asal_universitas_sekolah', TRUE);
			$e_sim_c					= $this->input->post('e_sim_c', TRUE);
			$e_nomor_telepon			= $this->input->post('e_nomor_telepon', TRUE);
			$e_karyawan_daerah					= $this->input->post('e_karyawan_daerah', TRUE);
			$e_karyawan_jabatan			= $this->input->post('e_karyawan_jabatan', TRUE);
			$e_karyawan_penempatan			= $this->input->post('e_karyawan_penempatan', TRUE);
			$v_sisa_cuti 				= '7';
			
			if($d_lahir!=''){
				$tmp=explode("-",$d_lahir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_lahir=$th."-".$bl."-".$hr;
			}else{
        $d_lahir=null;
      }

      		if($d_kontrak_ke1_awal!=''){
					$tmp=explode("-",$d_kontrak_ke1_awal);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$d_kontrak_ke1_awal=$th."-".$bl."-".$hr;
				}else{
	        $d_kontrak_ke1_awal=null;
	      }
	      if($d_kontrak_ke1_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke1_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke1_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke1_akhir=null;
      }
      	  if($d_kontrak_ke2_awal!=''){
				$tmp=explode("-",$d_kontrak_ke2_awal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke2_awal=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke2_awal=null;
      }
      	  if($d_kontrak_ke2_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke2_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke2_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke2_akhir=null;
      }
      	  if($d_kontrak_ke3_awal!=''){
				$tmp=explode("-",$d_kontrak_ke3_awal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke3_awal=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke3_awal=null;
      }
      	  if($d_kontrak_ke3_akhir!=''){
				$tmp=explode("-",$d_kontrak_ke3_akhir);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_kontrak_ke3_akhir=$th."-".$bl."-".$hr;
			}else{
        $d_kontrak_ke3_akhir=null;
      }
      	  if($d_join_date!=''){
				$tmp=explode("-",$d_join_date);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_join_date=$th."-".$bl."-".$hr;
				$kode_nik=$th.$bl.$hr;
				$sub_kalimat = substr($kode_nik,2);
				}else{
        $d_join_date=null;
      }
			$this->load->model('karyawan/mmaster');
			$this->mmaster->updateheader(
										$i_nik, $i_department, $i_area,$i_karyawan_status,$d_kontrak_ke1_awal, $d_kontrak_ke1_akhir, $d_kontrak_ke2_awal, $d_kontrak_ke2_akhir,$d_kontrak_ke3_awal, $d_kontrak_ke3_akhir, $d_join_date, $e_nomor_bpjs_ketenagakerjaan,
										$e_nomor_bpjs_kesehatan, $e_nomor_npwp, $e_sim_a,$e_sim_b,$e_sim_c,$e_karyawan_jabatan,$e_karyawan_penempatan,$e_karyawan_daerah,$v_sisa_cuti
					      				);
			$this->mmaster->updatedetail(
												 $i_nik, $e_nama_karyawan_lengkap, $e_nama_karyawan_panggilan, $e_jenis_kelamin, 
												 $e_tempat_lahir, $d_lahir, $e_agama, $e_no_kk, 
											 	 $e_no_ktp, $e_alamat_ktp, $e_alamat_sekarang, $e_jurusan,
												 $e_asal_universitas_sekolah, $e_pendidikan_terakhir, $e_status_pernikahan,$e_nama_pasangan,$e_alamat_pasangan,$e_nomor_telepon
					      );

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Karyawan ('.$i_nik.') -'.$e_nama_karyawan_lengkap;
	        $this->load->model('logger');

      		$config['base_url'] = base_url().'index.php/karyawan/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query("select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%'",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_karyawan');
			$data['i_nik']='';
			$this->load->model('karyawan/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('karyawan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct	= $this->uri->segment(4);
			$this->load->model('productbase/mmaster');
			$this->mmaster->delete($iproduct);

			$config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productbase/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query=$this->db->query("select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%'",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_karyawan');
			$data['i_nik']='';
	 		$this->load->view('karyawan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caridepartment()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/department/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tr_department where upper(i_department) like '%$cari%' or upper(e_department_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_department');
			$data['isi']=$this->mmaster->caridepartment($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistdepartment', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function department()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/department/index/';
			$query = $this->db->query("select * from tr_department",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacadepartment($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistdepartment', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/area/index/';
			$query = $this->db->query("select * from tr_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikaryawan_status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/karyawan_status/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tr_karyawan_status where upper(i_karyawan) like '%$cari%' or upper(e_karyawan_status) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_karyawan_status');
			$data['isi']=$this->mmaster->carikaryawanstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistkaryawanstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function karyawan_status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/karyawan_status/index/';
			$query = $this->db->query("select * from tr_karyawan_status",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_karyawan_status');
			$data['isi']=$this->mmaster->bacakaryawanstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistkaryawanstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
