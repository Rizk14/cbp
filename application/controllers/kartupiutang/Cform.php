<?php 
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
		require_once "php/fungsi.php";
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('kartupiutang');
			$data['iarea'] 		= '';

			$this->load->view('kartupiutang/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$iarea 		= $this->input->post('iarea');
			$iperiode	= $this->input->post('iperiode');

			$cust   	= $this->input->post('chkntx');
			$area   	= $this->input->post('chkjtx');

			$thn 		= substr($iperiode, 0, 4);
			$bln 		= substr($iperiode, 4, 2);
			$eperiode 	= mbulan($bln) . " " . $thn;

			$this->load->model('kartupiutang/mmaster');

			if ($iarea == "NA") {
				$eareaname = "NASIONAL";
			} else {
				$eareaname = $this->db->query(" SELECT e_area_name FROM tr_area WHERE i_area = '$iarea' ")->row()->e_area_name;
			}

			$data['page_title'] = $this->lang->line('kartupiutang');
			$data['cari']		= $cari;
			$data['iarea']		= $iarea;
			$data['eareaname']	= $eareaname;
			$data['iperiode']	= $iperiode;
			$data['eperiode']	= $eperiode;
			$data['isi']		= $this->mmaster->bacaperiode($iarea, $iperiode, $cari, $cust, $area);
			$data['chkntx']		= $cust;
			$data['chkjtx']		= $area;
			$data['thn']		= $thn;
			$data['bln']		= $bln;

			$this->logger->writenew('Membuka Kartu Piutang Periode:' . $iperiode . ' Area:' . $iarea);

			$this->load->view('kartupiutang/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('kartupiutang');
			$this->load->view('kartupiutang/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/kartupiutang/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/kartupiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dto	    = $this->input->post('dto');
			$iarea = $this->input->post('iarea');
			if ($iarea == '') $icustomer = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			$this->load->model('kartupiutang/mmaster');
			if ($dto != '') {
				$tmp = explode("-", $dto);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dto = $th . "-" . $bl . "-" . $hr;
			}
			$data['dto']  = $dto;
			$data['iarea'] = $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi'] = $this->mmaster->baca($iarea, $dto);
			$data['user']	= $this->session->userdata('user_id');
			$sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}

			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Cetak Kartu Piutang sampai tanggal:' . $dto . ' Area:' . $iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$this->load->view('kartupiutang/vformrpt', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu266') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomer = $this->input->post('icustomer');
			$iperiode	= $this->input->post('iperiode');
			if ($icustomer == '') $icustomer	= $this->uri->segment(4);
			if ($iperiode == '') $iperiode	= $this->uri->segment(5);
			$saldo	= $this->uri->segment(6);

			$thn = substr($iperiode, 0, 4);
			$bln = substr($iperiode, 4, 2);
			$eperiode = mbulan($bln) . " " . $thn;

			$config['base_url'] = base_url() . 'index.php/kartupiutang/cform/detail/' . $icustomer . '/' . $iperiode . '/' . $saldo . '/';
			$query = $this->db->query(" select x.* from(
		/* Penjualan */
		select 'Penjualan' as status, a.i_customer, b.e_customer_name, a.i_area, a.i_nota, a.d_nota, a.v_nota_netto from
		tm_nota a, tr_customer b
		where
		a.i_customer = b.i_customer
		and to_char(a.d_nota,'yyyymm')='$iperiode'
		and a.f_nota_cancel = 'f'
		and a.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, a.v_jumlah from tm_alokasi_item a, tm_alokasi b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_kbank = b.i_kbank
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, a.v_jumlah from tm_alokasihl_item a, tm_alokasihl b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, a.v_jumlah  from tm_alokasikn_item a, tm_alokasikn b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Alokasi' as status, d.i_customer, c.e_customer_name, d.i_area, a.i_alokasi||' - '||a.i_nota as i_nota, b.d_alokasi, a.v_jumlah from tm_alokasiknr_item a, tm_alokasiknr b, tr_customer c, tm_nota d
		where a.i_alokasi = b.i_alokasi
		and a.i_area = b.i_area
		and a.i_kn = b.i_kn
		and d.i_customer = c.i_customer
		and b.f_alokasi_cancel = 'f'
		and a.i_nota = d.i_nota
		and to_char(b.d_alokasi,'yyyymm')='$iperiode'
		and d.i_customer = '$icustomer'
		union all
		select 'Pembulatan' as status, a.i_customer, c.e_customer_name, a.i_area, p.i_refference, p.d_mutasi, p.v_mutasi_debet as v_jumlah from tm_general_ledger p, tr_customer c, tr_city g, tr_salesman b, tm_spb h, tr_product_group i, tr_area area, tm_nota a
		where to_char(p.d_mutasi,'yyyymm')='$iperiode' and not a.i_nota isnull
		and a.i_salesman=b.i_salesman and a.f_nota_cancel='f'
		and a.i_nota=substring(p.i_refference, 15, 15) and p.f_debet='t' and p.i_coa='610-2902'
		and c.i_city = g.i_city and c.i_area = g.i_area and a.i_salesman=b.i_salesman and h.i_product_group=i.i_product_group
		and a.i_spb=h.i_spb and a.i_area=h.i_area  and area.i_area=a.i_area  and a.i_customer=c.i_customer
		and a.i_customer = '$icustomer'
		) as x order by x.d_nota asc
                                  ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('kartupiutang');
			$data['icustomer']	= $icustomer;
			$data['iperiode']	= $iperiode;
			$data['eperiode']	= $eperiode;
			$data['saldo']	= $saldo;
			$data['isi']		= $this->mmaster->bacadetail($icustomer, $iperiode, $config['per_page'], $this->uri->segment(7));

			$this->load->view('kartupiutang/vformdetail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
