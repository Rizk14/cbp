<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutang');
			$data['iarea']='';
      $data['djt']='';
			$this->load->view('kartupiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$iarea= $this->input->post('iarea');
      $iperiode	= $this->input->post('iperiode');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($iperiode=='') $iperiode	= $this->uri->segment(5);
/*
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/view/'.$iarea.'/'.$iperiode.'/';
      $query = $this->db->query(" select i_customer from(
                                  select i_customer, (sum(nota)+sum(dn))-(sum(pl)+sum(kn))as saldo, 0 as nota, 0 as dn, 0 as pl, 0 as kn from(
                                  select i_customer, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
                                  select b.i_customer_groupar as i_customer ,sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn 
                                  from tm_nota a, tr_customer_groupar b
                                  where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')< '201211'
                                  and a.i_area='06' group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn 
                                  from tm_kn a, tr_customer_groupar b
                                  where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '201211'
                                  and a.i_customer=b.i_customer and a.i_area='06' group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn 
                                  from tm_pelunasan a, tr_customer_groupar b
                                  where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and to_char(a.d_bukti,'yyyymm')< '201211'
                                  and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_customer=b.i_customer and a.i_area='06'
                                  group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn 
                                  from tm_kn a, tr_customer_groupar b
                                  where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(a.d_kn,'yyyymm')< '201211'
                                  and a.i_customer=b.i_customer and a.i_area='06' group by b.i_customer_groupar
                                  ) as a group by i_customer
                                  ) as b group by i_customer

                                  union all

                                  select i_customer, 0 as saldo, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
                                  select i_customer, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
                                  select b.i_customer_groupar as i_customer, sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn 
                                  from tm_nota a, tr_customer_groupar b
                                  where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='201211'
                                  and a.i_area='06' group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn 
                                  from tm_kn a, tr_customer_groupar b
                                  where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and a.i_customer=b.i_customer 
                                  and to_char(a.d_kn,'yyyymm')='201211' and a.i_area='06' group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn 
                                  from tm_pelunasan a, tr_customer_groupar b
                                  where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.i_customer=b.i_customer 
                                  and to_char(a.d_bukti,'yyyymm')='201211'
                                  and a.i_jenis_bayar<>'04' and a.i_jenis_bayar<>'05' and a.i_area='06' group by b.i_customer_groupar
                                  union all
                                  select b.i_customer_groupar as i_customer, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn 
                                  from tm_kn a, tr_customer_groupar b
                                  where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and a.i_customer=b.i_customer  
                                  and to_char(a.d_kn,'yyyymm')='201211' and a.i_area='06' group by b.i_customer_groupar
                                  ) as a group by i_customer
                                  ) as b group by i_customer
                                  ) as c
                                  where saldo<>0 or nota>0 or dn>0 or pl>0 or kn>0
                                  group by i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
*/
			$config['per_page'] = '10';
/*
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
*/
			$this->load->model('kartupiutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartupiutang');
			$data['cari']		= $cari;
			$data['iarea']	= $iarea;
			$data['iperiode']	= $iperiode;
      $data['hal'] = $this->uri->segment(6);
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$iperiode,$config['per_page'],$this->uri->segment(6),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Kartu Piutang Periode:'.$iperiode.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('kartupiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutang');
			$this->load->view('kartupiutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('kartupiutang/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Cetak Kartu Piutang sampai tanggal:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$this->load->view('kartupiutang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomer= $this->input->post('icustomer');
      $iperiode	= $this->input->post('iperiode');
			if($icustomer=='') $icustomer	= $this->uri->segment(4);
			if($iperiode=='') $iperiode	= $this->uri->segment(6);
			$saldo	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/detail/'.$icustomer.'/'.$saldo.'/'.$iperiode.'/';
      $query = $this->db->query(" select customer from(
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_nota as tglbukti, a.i_nota as bukti, '' as jenis, '1. penjualan' as keterangan, a.v_nota_netto as debet, 
                                  0 as kredit, 0 as dn, 0 as kn
                                  from tm_nota a, tr_customer_groupar b, tr_customer c
                                  where f_nota_cancel='f' and a.i_customer=b.i_customer and to_char(a.d_nota,'yyyymm')='$iperiode'
                                  and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'kredit nota' as keterangan, 0 as debet, 0 as kredit, 
                                  0 as dn, 
                                  v_netto as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                                  where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, 
                                  c.e_customer_name as nama, 
                                  a.d_bukti as tglbukti, a.i_pelunasan as bukti, a.i_jenis_bayar as jenis, '2. '|| d.e_jenis_bayarname as keterangan, 
                                  0 as debet, a.v_jumlah as kredit, 0 as dn, 0 as kn
                                  from tm_pelunasan a, tr_customer_groupar b, tr_customer c, tr_jenis_bayar d
                                  where a.f_pelunasan_cancel='f' and a.f_giro_tolak='f' and a.f_giro_batal='f' 
                                  and to_char(a.d_bukti,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar 
                                  and b.i_customer_groupar='$icustomer'
                                  union all
                                  select a.i_area as area, b.i_customer_groupar as groupar, a.i_customer as customer, c.e_customer_name as nama, 
                                  a.d_kn as tglbukti, a.i_kn as bukti, '' as jenis, 'debet nota' as keterangan, 0 as debet, 0 as kredit, v_netto as dn, 
                                  0 as kn from tm_kn a, tr_customer_groupar b, tr_customer c
                                  where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and to_char(d_kn,'yyyymm')='$iperiode'
                                  and a.i_customer=b.i_customer and a.i_customer=c.i_customer and b.i_customer_groupar='$icustomer'
                                  ) as a
                                  ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('kartupiutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartupiutang');
			$data['icustomer']	= $icustomer;
			$data['iperiode']	= $iperiode;
			$data['saldo']	= $saldo;
			$data['isi']		= $this->mmaster->bacadetail($icustomer,$iperiode,$config['per_page'],$this->uri->segment(7));
			$this->load->view('kartupiutang/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
