<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $config['base_url'] = base_url().'index.php/arearayon/cform/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  
			  $query = $this->db->query("	select * from tr_customer_rayon where upper(tr_customer_rayon.i_customer) like '%$cari%' and f_ar_cancel='t' ",false);
        
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(4);
			  $this->paginationxx->initialize($config);
			  
			  $data['page_title'] = $this->lang->line('master_arearayon');
			  $data['icustomer']='';
			  $data['iareacover']='';
			  $data['eareacovername']='';
			  $this->load->model('arearayon/mmaster');
			  $data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			  $sess	= $this->session->userdata('session_id');
			  $id 	= $this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
			  	while($row=pg_fetch_assoc($rs)){
			  		$ip_address	  = $row['ip_address'];
			  		break;
			  	}
			  }else{
			  	$ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
			  	$now	  = $row['c'];
			  }
			  $pesan="Membuka Menu Master Area Rayon";
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now, $pesan);
			  $this->load->view('arearayon/vmainform', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
		
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$iareacover 		= $this->input->post('iareacover', TRUE);
			$eareacovername 		= $this->input->post('eareacovername', TRUE);

			if ((isset($icustomer) && $icustomer != '') 
			    
			    )
			{
					
				$this->load->model('arearayon/mmaster');	
				$this->mmaster->insert($icustomer,$iareacover,$eareacovername);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Area Rayon:('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);	

		        $config['base_url'] = base_url().'index.php/arearayon/cform/index/';
				$cari = strtoupper($this->input->post('cari', FALSE));

				$query = $this->db->query("	select * from tr_customer_rayon where upper(tr_customer_rayon.i_customer) like '%$cari%' and f_ar_cancel='t' ",false);

				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_arearayon');
				$data['icustomer']='';
				$data['iareacover']='';
				$data['eareacovername']='';
				$this->load->model('arearayon/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('arearayon/vmainform', $data);	        
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_arearayon');
			$this->load->view('arearayon/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cancel()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$iareacover 	= $this->uri->segment(5);
			$this->load->model('arearayon/mmaster');
			$this->mmaster->cancel($icustomer,$iareacover);
			
			$config['base_url'] = base_url().'index.php/arearayon/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_rayon 
										where upper(tr_customer_rayon.i_customer) like '%$cari%'
										or upper(tr_customer_rayon.i_area_cover) like '%$cari%'
									    order by tr_customer_rayon.i_customer, tr_customer_rayon.i_area_cover ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_arearayon');
			$data['icustomer']='';
			$data['iareacover']='';
			$data['eareacovername']='';
			$this->load->model('arearayon/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('arearayon/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);

			$config['base_url'] = base_url().'index.php/arearayon/cform/customer/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_customer order by i_customer ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('arearayon/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6)); 
			$this->load->view('arearayon/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function rayon()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/arearayon/cform/rayon/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_rayon order by i_area_rayon ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('arearayon/mmaster');
			$data['page_title'] = $this->lang->line('list_rayon');
			$data['isi']=$this->mmaster->bacarayon($config['per_page'],$this->uri->segment(6)); 
			$this->load->view('arearayon/vlistrayon', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $config['base_url'] = base_url().'index.php/arearayon/cform/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  $query = $this->db->query("	select a.*, b.e_customer_name from tr_customer_rayon a, tr_customer b
			  								where a.i_customer=b.i_customer and upper(a.i_customer) like '%$cari%'  and upper(b.i_customer) like '%$cari%' 
											order by a.i_customer",false);
        
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(4);
			  $this->paginationxx->initialize($config);
			  $this->load->model('arearayon/mmaster');
			  $data['isi']		=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			  $data['page_title'] 	=$this->lang->line('master_arearayon');
			  $data['icustomer']='';
			  $data['iareacover']='';
			  $data['eareacovername']='';
	   		$this->load->view('arearayon/vmainform',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/arearayon/cform/caricustomer/index/'; 
			$cari = $this->input->get_post('cari');
			$cari = strtoupper($cari);  */

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/arearayon/cform/caricustomer/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/arearayon/cform/caricustomer/'.$baris.'/sikasep/';

			$query = $this->db->query("select i_customer from tr_customer where i_customer ilike '%$cari%' or e_customer_name ilike '%$cari%'",false); 
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('arearayon/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('arearayon/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carirayon()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/arearayon/cform/carirayon/index/'; 
			$cari = $this->input->get_post('cari');
			$cari = strtoupper($cari);  */

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/arearayon/cform/carirayon/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/arearayon/cform/carirayon/'.$baris.'/sikasep/';

			$query = $this->db->query("select i_area_rayon from tr_rayon where (upper (i_area_rayon) ilike '%$cari%' or (e_area_rayon_name) ilike '%$cari%')",false); 
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('arearayon/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_rayon');
			$data['isi']=$this->mmaster->carirayon($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('arearayon/vlistrayon', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>