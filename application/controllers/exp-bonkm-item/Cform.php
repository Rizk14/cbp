<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title1'] = $this->lang->line('exp-dbonkeluar');
			$data['page_title2'] = $this->lang->line('exp-dbonmasuk');
			$data['dfrom']	= '';
			$data['dto']	= '';
			//$data['iperiode']	= '';
			//$data['iarea']	  = '';
			$this->load->view('exp-bonkm-item/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportkeluar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm-item/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');

			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));

			$this->db->select(" a.*, b.e_product_motifname,d.d_bk
								from tr_product_motif b, tm_bk_item a
								inner join tm_bk d on(a.i_bk=d.i_bk)
								where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
								and d.d_bk >= to_date('$dfrom','dd-mm-yyyy') and d.d_bk <= to_date('$dto','dd-mm-yyyy')
								and d.f_bk_cancel!='t'
								order by a.i_bk,a.n_item_no ", false);

      /*$this->db->select("* from tm_bk
                		where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
                		and f_bk_cancel!='t'
	                    order by d_bk, i_bk",false);*/

		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Keluar ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DETAIL BON KELUAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,8,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,8,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,8,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Produk');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Motif Produk');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Produk Grade');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'QTY');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Harga Produk');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');

				$i=7;
				$na=1;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':I'.$i
				  );
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bk);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bk);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_product_motif);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_product_grade);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_quantity);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$na++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Keluar Detail '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Detail Bon Keluar'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportmasuk()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm-item/mmaster');
			$datefrom = $this->input->post('datefrom2');
			$dateto	  = $this->input->post('dateto2');

			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));

			$this->db->select(" a.*, b.e_product_motifname,d.d_bm
								from tr_product_motif b, tm_bm_item a
								inner join tm_bm d on(a.i_bm=d.i_bm)
								where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
								and d.d_bm >= to_date('$dfrom','dd-mm-yyyy') and d.d_bm <= to_date('$dto','dd-mm-yyyy')
								and d.f_bm_cancel!='t'
								order by a.i_bm,a.n_item_no ", false);

      /*$this->db->select("* from tm_bm
                		where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
                		and f_bm_cancel!='t'
	                    order by d_bm, i_bm",false);*/

		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Masuk ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'DETAIL BON MASUK');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,8,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,8,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,8,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Produk');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Motif Produk');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Produk Grade');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'QTY');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Harga Produk');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');

				$i=7;
				$na=1;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':I'.$i
				  );
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bm);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bm);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_product_motif);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_product_grade);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_quantity);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$na++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Masuk Detail '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Detail Bon Masuk'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>