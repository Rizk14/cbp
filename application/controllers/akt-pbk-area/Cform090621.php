<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bk').' (Masuk)';
			$this->load->view('akt-pbk-area/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){

			date_default_timezone_set("Asia/Jakarta");
        	#$hari_ini = date("Y-m-d");
			$now 				= date("Y-m")."-04";
			$data['page_title'] = $this->lang->line('bk').' (Masuk)'." Update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikbank		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$lepel		= $this->uri->segment(9);
				$icoabank = $this->uri->segment(10);
				$ibank = $this->uri->segment(11);
				$this->load->model("akt-pbk-area/mmaster");
				$data['isi']=$this->mmaster->baca($ikbank,$iperiode,$iarea,$ibank,$icoabank);
				$data['iarea']  = $iarea;
				$data['dfrom']  = $dfrom;
				$data['dto']    = $dto;
				$data['lepel']	= $lepel;
				$data['now']	= $now;
 		 		$this->load->view('akt-pbk-area/vformupdate',$data);
			}else{
				$this->load->view('akt-pbk-area/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikbank  	= $this->input->post('ikbank', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$iperiode 	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$irvtype  	= $this->input->post('irvtype', TRUE);
			$tah		= substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		= $this->input->post('iperiodebl', TRUE);
			$vbank	    = $this->input->post('vbank', TRUE);
			$vbank 	  	= str_replace(',','',$vbank);
			$dbank  	= $this->input->post('dbank', TRUE);
			$ibank    	= $this->input->post('ibank', TRUE);
			$icoabank 	= $this->input->post('icoabank', TRUE);
			$icoa      	= $this->input->post('icoa', TRUE);
			$ecoaname 	= $this->input->post('ecoaname', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$vbankold   = $this->input->post('vbankold', TRUE);
			$vbankold 	= str_replace(',','',$vbankold);
			$icoaold   	= $this->input->post('icoaold', TRUE);
			$iareaold	= $this->input->post('iareaold', TRUE);
			$igiro	  	= $this->input->post('igiro', TRUE);
      $jml        = $this->input->post('jml', TRUE);
			if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
			$fdebet='f';
			if (
				(isset($ikbank) && $ikbank != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($dbank) && $dbank != '') &&
				(isset($icoa) && $icoa != '') &&
				(isset($icoabank) && $icoabank != '')
			   )
			{
				$this->load->model('akt-pbk-area/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iarea,$ikbank,$iperiode,$icoa,$vbank,$dbank,$ecoaname,$edescription,$fdebet,$irvtype,$icoabank,
									   $vbankold,$iareaold,$icoaold,$igiro);
				// $this->mmaster->updategiro($igiro, $ikbank);
				$nomor=$ikbank;
###########posting##########
          $eremark		= $edescription;
          $fclose			= 'f';
			    $this->mmaster->inserttransheader($ikbank,$iarea,$eremark,$fclose,$dbank);
			    if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= $icoabank;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
			    }else{
				    $accdebet		  = $icoabank;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
			    }
			    $this->mmaster->inserttransitemdebet($accdebet,$ikbank,$namadebet,'t','t',$iarea,$eremark,$vbank,$dbank,$iarea);
			    $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vbank);
			    $this->mmaster->inserttransitemkredit($acckredit,$ikbank,$namakredit,'f','t',$iarea,$eremark,$vbank,$dbank,$iarea);
			    $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vbank);
			    $this->mmaster->insertgldebet($accdebet,$ikbank,$namadebet,'t',$iarea,$vbank,$dbank,$eremark);
			    $this->mmaster->insertglkredit($acckredit,$ikbank,$namakredit,'f',$iarea,$vbank,$dbank,$eremark);
###########end of posting##########
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update Bank Masuk No:'.$ikbank.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );
#					$this->db->trans_rollback();
					$this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function giro()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      	{
        	$area     	= $this->uri->segment(4);
         	$xtgl      	= $this->uri->segment(5);
         	$barisx     = $this->uri->segment(6);

         	/*$ikbank		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$area		= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$xtgl		= $this->uri->segment(8);
			$lepel		= $this->uri->segment(9);
			$icoabank 	= $this->uri->segment(10);
			$ibank 		= $this->uri->segment(11);*/

         	if(($area!=NULL) && ($xtgl!=NULL))
         		{
         			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/giro/'.$area.'/'.$xtgl.'/'.$barisx.'/';
         		}else{
         			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/giro/'.$barisx.'/';
         		}

         	$group='xxx';

         	if(($area!=NULL) && ($xtgl!=NULL))
         	{
         		$query = $this->db->query("	select a.* from (
					                        select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
					                        where a.i_customer=b.i_customer and a.i_area='$area'
					                        and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
					                        and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl' and not a.i_giro in(select i_giro from tm_kbank)
					                        union all
					                        select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
					                        tm_rtunai_item d
					                        where a.i_customer=b.i_customer and a.i_area='$area'
					                        and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
					                        and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
					                        and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and not a.i_tunai in(select i_giro from tm_kbank)
					                        union all
					                        select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
					                        where a.i_customer=b.i_customer and a.i_area='$area'
					                        and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
					                        and d_kum<='$xtgl' and not a.i_kum in(select i_giro from tm_kbank)
					                        )as a
					                        order by a.tgl, a.bayar ",false);
											
		        $config['total_rows'] = $query->num_rows();
		        $config['per_page']  = '10';
		        $config['first_link'] = 'Awal';
		        $config['last_link'] = 'Akhir';
		        $config['next_link'] = 'Selanjutnya';
		        $config['prev_link'] = 'Sebelumnya';
		        $config['cur_page'] = $this->uri->segment(7);
         		$this->pagination->initialize($config);
         
         		$data['baris']=$this->uri->segment(6);
     		}

         $this->load->model('akt-pbk-area/mmaster');
         
         $data['page_title'] = $this->lang->line('list_kugirotunai');
         
         if(($area!=NULL) && ($xtgl!=NULL))
         {
         	$data['isi']=$this->mmaster->bacagiro($area,$xtgl,$config['per_page'],$this->uri->segment(7),$group);
	        $data['area']=$area;
         	$data['tgl']=$xtgl;
         }else{
         	$data['isi'] ="";
         	$data['area']="";
         	$data['tgl'] ="";
         }

         	$this->load->view('akt-pbk-area/vlistgiro', $data);
    	}else{
         	$this->load->view('awal/index.php');
      	}
   }

   function carigiro()
   {
	   //-------------------------------------------------------------\\
	   if (
		   (($this->session->userdata('logged_in')) &&
		   ($this->session->userdata('menu502')=='t')) ||
		   (($this->session->userdata('logged_in')) &&
		   ($this->session->userdata('allmenu')=='t'))
		  ){
		   $area     = $this->uri->segment(4);
			$xtgl      = $this->uri->segment(5);
			#$barisx     = $this->uri->segment(6);


			$area= $this->input->post('area', FALSE);
			$barisx= $this->input->post('barisx', FALSE);
			$tgl     = strtoupper($this->input->post('tgl', FALSE));

	 
	 if($tgl!=''){
	   $tmp=explode('-',$tgl);
	   $th=$tmp[0];
	   $bl=$tmp[1];
	   $hr=$tmp[2];
	   $xtgl=$th.'-'.$bl.'-'.$hr;
	 }
	 $cari = strtoupper($this->input->post('cari', FALSE));
	   // if(($area!=NULL) && ($xtgl!=NULL)){
		$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/giro/'.$area.'/'.$xtgl.'/';
	   // }else{
		//$config['base_url'] = base_url().'index.php/akt-pbk/cform/giro/'.$barisx.'/';
	   // }
	   $group='xxx';
	   //	 if(($area!=NULL) && ($xtgl!=NULL)){
		$query = $this->db->query("select a.* from (
					   select a.i_giro as bayar, a.d_giro_cair as tgl, a.v_jumlah from tm_giro  a, tr_customer_groupar b
					   where a.i_customer=b.i_customer and a.i_area='$area'
					   and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
					   and (upper(a.i_giro) like '%$cari%')
					   and not a.d_giro_cair isnull and a.d_giro_cair<='$xtgl'
					   union all
					   select a.i_tunai as bayar, a.d_tunai as tgl, a.v_jumlah from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
					   tm_rtunai_item d
					   where a.i_customer=b.i_customer and a.i_area='$area'
					   and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
					   and a.i_tunai=d.i_tunai and a.d_tunai<='$xtgl'
					   and (upper(a.i_tunai) like '%$cari%')
					   and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
					   union all
					   select a.i_kum as bayar, d_kum as tgl, a.v_jumlah from tm_kum a, tr_customer_groupar b
					   where a.i_customer=b.i_customer and a.i_area='$area'
					   and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
					   and (upper(a.i_kum) like '%$cari%')
					   and d_kum<='$xtgl'
					   )as a
					   order by a.tgl, a.bayar  ",false);
										   
/*
		$query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupar b
									  where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
									  and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
									  and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'",false);
*/
/*
		$query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupbayar b
										 where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
										 and (a.f_giro_tolak='f' or a.f_giro_batal='f')",false);
#                                           and a.i_area='$iarea'
*/
		$config['total_rows'] = $query->num_rows();
		$config['per_page']  = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(7);
		$this->pagination->initialize($config);
		$data['baris']=$barisx;
		//}
	   $this->load->model('akt-pbk-area/mmaster');
	   $data['page_title'] = $this->lang->line('list_kugirotunai');
		//if(($area!=NULL) && ($xtgl!=NULL)){
	   $data['isi']=$this->mmaster->carigiro($cari,$area,$xtgl,$config['per_page'],$this->uri->segment(7),$group);
	   //var_dump($cari,$area,$xtgl,$config['per_page'],$this->uri->segment(8),$group);
	   //die();
			//$data['icustomer']=$icustomer;
		$data['area']=$area;
		$data['tgl']=$xtgl;
		//die();
		//}else{
		//$data['isi'] ="";
		//$data['area']="";
		//$data['tgl'] ="";
		//}//$data['barisx']=$barisx;
		//$data['xdbukti']=$xdbukti;
		//$data['group']=$group;
		$this->load->view('akt-pbk-area/vlistgiro', $data);
	 }else{
		$this->load->view('awal/index.php');
	 }
   }
	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-pbk-area/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-pbk-area/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where i_coa like '6%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-pbk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pbk-area/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk-area/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
						    									where (upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
																	and upper(i_coa) like '6%'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pbk-area/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
