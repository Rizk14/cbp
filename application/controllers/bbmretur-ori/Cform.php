<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbmretur');
			$data['ibbm']='';
			$this->load->model('bbmretur/mmaster');
			$data['isi']="";
      $data['iarea']='';
      $data['eareaname']='';
      $data['ittb']='';
      $data['icustomer']='';
      $data['ecustomername']='';
      $data['isalesman']='';
      $data['esalesmanname']='';
      $data['dttb']='';
			$data['detail']="";
			$data['jmlitem']="";
			$this->load->view('bbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm 	= $this->input->post('ibbm', TRUE);
			$dbbm 	= $this->input->post('dbbm', TRUE);
			if($dbbm!=''){
				$tmp=explode("-",$dbbm);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbm=$th."-".$bl."-".$hr;
#        $thbl=substr($th,2,2).$bl;
        $thbl=$th.$bl;
			}
			$dttb 	= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$thttb=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$thttb."-".$bl."-".$hr;
			}
			$icustomer	= $this->input->post('icustomer', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$ittb				= $this->input->post('ittb', TRUE);
			$isalesman	= $this->input->post('isalesman',TRUE);
			$jml				= $this->input->post('jml', TRUE);
  			if(
				($ibbm=='')
				&& ($icustomer!='')
				&& ($dbbm!='') 
				&& ($ittb!='')
				&& ($dttb!='')
				&& ($isalesman!='')
				&& (($jml!='')&&($jml!='0'))
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbmretur/mmaster');
				$ibbm				= $this->mmaster->runningnumberbbm($thbl,$iarea);
				$istore				= 'AA';
				$istorelocation		= '01';
				$istorelocationbin	= '00';
				$eremark			= 'TTB Retur';
				$ibbmtype			= '05';
				$this->mmaster->insertbbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isalesman);
				$this->mmaster->updatettbheader($ittb,$thttb,$iarea,$ibbm,$dbbm);
					for($i=1;$i<=$jml;$i++){
				  	$iproduct1				= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade1			= $this->input->post('iproductgrade'.$i, TRUE);
						$iproductmotif1			= $this->input->post('iproductmotif'.$i, TRUE);
						$eproductname1			= $this->input->post('eproductname'.$i, TRUE);
						$vunitprice1			= $this->input->post('vunitprice'.$i, TRUE);
						$vunitprice1			= str_replace(',','',$vunitprice1);
						$nttb					= $this->input->post('nttb'.$i, TRUE);
				  	$iproduct2				= $this->input->post('iproductx'.$i, TRUE);
						$iproductgrade2			= $this->input->post('iproductgradex'.$i, TRUE);
						$iproductmotif2			= $this->input->post('iproductmotifx'.$i, TRUE);
						$eproductname2			= $this->input->post('eproductnamex'.$i, TRUE);
						$vunitprice2			= $this->input->post('vunitpricex'.$i, TRUE);
						$vunitprice2			= str_replace(',','',$vunitprice2);
						$nbbm					= $this->input->post('nbbm'.$i, TRUE);
						$inota					= $this->input->post('inota'.$i, TRUE);
						$this->mmaster->updatettbdetail($iproduct1,$iproductgrade1,$iproductmotif1,$nttb,
														$iproduct2,$iproductgrade2,$iproductmotif2,$nbbm,
														$ittb,$thttb,$iarea,$inota);
##############
            $trans=$this->mmaster->lasttrans($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$eproductname2,$ibbm,$q_in,$q_out,$nbbm,$q_aw,$q_ak);
            $th=substr($dbbm,0,4);
            $bl=substr($dbbm,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$nbbm,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$nbbm,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$nbbm,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct2,$iproductgrade2,$iproductmotif2,$istore,$istorelocation,$istorelocationbin,$eproductname2,$nbbm);
            }
##############
						$this->mmaster->insertbbmdetail($iproduct2,$iproductgrade2,$eproductname2,$iproductmotif2,$nbbm,
														$vunitprice2,$ittb,$ibbm,$eremark,$dttb,$ibbmtype,$i);
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();

					  $sess=$this->session->userdata('session_id');
					  $id=$this->session->userdata('user_id');
					  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					  $rs		= pg_query($sql);
					  if(pg_num_rows($rs)>0){
						  while($row=pg_fetch_assoc($rs)){
							  $ip_address	  = $row['ip_address'];
							  break;
						  }
					  }else{
						  $ip_address='kosong';
					  }
					  $query 	= pg_query("SELECT current_timestamp as c");
					  while($row=pg_fetch_assoc($query)){
						  $now	  = $row['c'];
					  }
					  $pesan='Input BBM Retur No:'.$ibbm.' Area:'.$iarea;
					  $this->load->model('logger');
					  $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $ibbm;
						$this->load->view('nomor',$data);
					}
				
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbmretur');
			$this->load->view('bbmretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbmretur')." update";
			if(

				($this->uri->segment(4)!=='')
			  ){
				$ibbm		= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto		= $this->uri->segment(7);
				$query 	= $this->db->query("select a.*, b.e_product_motifname 
											              from tm_bbm_item a, tr_product_motif b 
											              where a.i_bbm = '$ibbm' 
											              and a.i_product=b.i_product 
											              and a.i_product_motif=b.i_product_motif");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ibbm'] = $ibbm;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto'] 	= $dto;
				$this->load->model('bbmretur/mmaster');
				$data['isi']=$this->mmaster->baca($ibbm);
				$data['detail']=$this->mmaster->bacadetail($ibbm);
				$this->load->view('bbmretur/vmainform',$data);
			}else{
				$this->load->view('bbmretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm 	= $this->input->post('ibbm', TRUE);
			$dbbm 	= $this->input->post('dbbm', TRUE);
			if($dbbm!=''){
				$tmp=explode("-",$dbbm);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbm=$th."-".$bl."-".$hr;
			}
			$dttb 	= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$thttb=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$thttb."-".$bl."-".$hr;
			}
			$icustomer	= $this->input->post('icustomer', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$ittb				= $this->input->post('ittb', TRUE);
			$isalesman	= $this->input->post('isalesman',TRUE);
			$jml				= $this->input->post('jml', TRUE);
  			if(
				($ibbm!='')
				&& ($icustomer!='')
				&& ($dbbm!='') 
				&& ($ittb!='')
				&& ($dttb!='')
				&& ($isalesman!='')
				&& (($jml!='')&&($jml!='0'))
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbmretur/mmaster');
				$istore						= 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$eremark					= 'TTB Retur';
				$ibbmtype					= '05';
				$this->mmaster->deletebbmheader($ibbm);
				$this->mmaster->insertbbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isalesman);
				$this->mmaster->updatettbheader($ittb,$thttb,$iarea,$ibbm,$dbbm);
				for($i=1;$i<=$jml;$i++){
				  	$iproduct1				= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade1		= $this->input->post('iproductgrade'.$i, TRUE);
						$iproductmotif1		= $this->input->post('iproductmotif'.$i, TRUE);
						$eproductname1		= $this->input->post('eproductname'.$i, TRUE);
						$vunitprice1			= $this->input->post('vunitprice'.$i, TRUE);
						$vunitprice1			= str_replace(',','',$vunitprice1);
						$nttb							= $this->input->post('nttb'.$i, TRUE);
				  	$iproduct2				= $this->input->post('iproductx'.$i, TRUE);
						$iproductgrade2		= $this->input->post('iproductgradex'.$i, TRUE);
						$iproductmotif2		= $this->input->post('iproductmotifx'.$i, TRUE);
						$eproductname2		= $this->input->post('eproductnamex'.$i, TRUE);
						$vunitprice2			= $this->input->post('vunitpricex'.$i, TRUE);
						$vunitprice2			= str_replace(',','',$vunitprice2);
						$nbbm							= $this->input->post('nbbm'.$i, TRUE);
						$inota						= $this->input->post('inota'.$i, TRUE);
			  		$iproductxxx			= $this->input->post('iproductxxx'.$i, TRUE);
						$iproductgradexxx	= $this->input->post('iproductgradexxx'.$i, TRUE);
						$iproductmotifxxx	= $this->input->post('iproductmotifxxx'.$i, TRUE);
						$this->mmaster->deletebbmdetail($iproductxxx,$iproductgradexxx,$iproductmotifxxx,$ibbm,$ibbmtype);
						$this->mmaster->insertbbmdetail($iproduct2,$iproductgrade2,$eproductname2,$iproductmotif2,$nbbm,
														$vunitprice2,$ittb,$ibbm,$eremark,$dttb,$ibbmtype,$i);
						$this->mmaster->updatettbdetail($iproduct1,$iproductgrade1,$iproductmotif1,$nttb,
														$iproduct2,$iproductgrade2,$iproductmotif2,$nbbm,
														$ittb,$thttb,$iarea,$inota);
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
  		    $this->db->trans_rollback();
				}else{
// 		    $this->db->trans_rollback();
			    $this->db->trans_commit();

				  $sess=$this->session->userdata('session_id');
				  $id=$this->session->userdata('user_id');
				  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				  $rs		= pg_query($sql);
				  if(pg_num_rows($rs)>0){
					  while($row=pg_fetch_assoc($rs)){
						  $ip_address	  = $row['ip_address'];
						  break;
					  }
				  }else{
					  $ip_address='kosong';
				  }
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Update BBM Retur No:'.$ibbm.' Area:'.$iarea;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibbm;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm		= $this->input->post('ibbmdelete', TRUE);
			$isupplier	= $this->input->post('isupplierdelete', TRUE);
			$iop		= $this->input->post('iopdelete', TRUE);
			$this->load->model('bbmretur/mmaster');
			$this->mmaster->delete($ibbm,$isupplier,$iop);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
		  while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
		  }
		  $pesan='Delete BBM Retur No:'.$ibbm;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('bbmretur');
			$data['ibbm']='';
			$data['isupplier']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm						= $this->uri->segment(4);
			$iproduct				= $this->uri->segment(5);
			$iproductmotif	= $this->uri->segment(6);
			$iproductgrade	= $this->uri->segment(7);
			$iproductx			= $this->uri->segment(8);
			$iproductmotifx	= $this->uri->segment(9);
			$iproductgradex	= $this->uri->segment(10);
			$ittb						= str_replace('%20',' ',$this->uri->segment(11));
			$thttb					= $this->uri->segment(12);
			$iarea					= $this->uri->segment(13);
      $ibbmtype				= '05';
			$this->db->trans_begin();
			$this->load->model('bbmretur/mmaster');
  		$this->mmaster->deletebbmdetail($iproductx,$iproductgradex,$iproductmotifx,$ibbm,$ibbmtype);
			$this->mmaster->hapusttbdetail($iproductx,$iproductgradex,$iproductmotifx,$ittb,$thttb,$iarea);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Delete Item BBM Retur No:'.$ibbm;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$dfrom	= $this->uri->segment(14);
				$dto		= $this->uri->segment(15);
				$data['page_title'] = $this->lang->line('bbmretur')." update";
				$query 	= $this->db->query("select a.*, b.e_product_motifname 
											from tm_bbm_item a, tr_product_motif b 
											where a.i_bbm = '$ibbm' 
											and a.i_product=b.i_product 
											and a.i_product_motif=b.i_product_motif");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ibbm'] = $ibbm;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto'] 	= $dto;
				$this->load->model('bbmretur/mmaster');
				$data['isi']=$this->mmaster->baca($ibbm);
				$data['detail']=$this->mmaster->bacadetail($ibbm);
				$this->load->view('bbmretur/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$data['ttb']=str_replace('%20','',$this->uri->segment(5));
			$baris=$this->uri->segment(4);
			$ttb=str_replace('%20','',$this->uri->segment(5));
			$config['base_url'] = base_url().'index.php/bbmretur/cform/product/'.$baris.'/'.$ttb.'/';
			$query = $this->db->query(" select a.i_product as kode, 
										              a.i_product_motif as motif,
										              a.e_product_motifname as namamotif, 
										              c.e_product_name as nama,
										              b.v_unit_price as harga, 
										              b.n_quantity,
										              b.i_nota,
										              b.i_product1_grade as i_product_grade
										              from tr_product_motif a,tr_product c, tm_ttbretur_item b
										              where b.i_ttb='$ttb' and a.i_product=c.i_product 
										              and b.i_product1=a.i_product and b.i_product1_motif=a.i_product_motif" ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$ttb);
			$data['baris']=$baris;
			$this->load->view('bbmretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$data['ttb']=$this->uri->segment(5);
			$baris=$this->uri->segment(4);
			$ttb=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbmretur/cform/product/'.$baris.'/'.$ttb.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("	select a.i_product as kode, 
										a.i_product_motif as motif,
										a.e_product_motifname as namamotif, 
										c.e_product_name as nama,
										b.v_unit_price as harga, 
										b.n_quantity,
										b.i_nota,
										b.i_product1_grade
										from tr_product_motif a,tr_product c, tm_ttbretur_item b
										where b.i_ttb='$ttb' and a.i_product=c.i_product 
										and b.i_product1=a.i_product and b.i_product1_motif=a.i_product_motif
									   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(7),$ttb);
			$data['baris']=$baris;
			$this->load->view('bbmretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$data['op']=$this->uri->segment(5);
			$baris=$this->uri->segment(4);
			$op=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbmretur/cform/productupdate/'.$baris.'/'.$op.'/index/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga, b.n_order
										from tr_product_motif a,tr_product c,tm_op_item b 
										where b.i_op='$op' and a.i_product=c.i_product 
										and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif" ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$op);
			$this->load->view('bbmretur/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$data['op']=$this->uri->segment(5);
			$baris=$this->uri->segment(4);
			$op=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbmretur/cform/productupdate/'.$baris.'/'.$op.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("	select a.i_product as kode, 
										a.i_product_motif as motif,
										a.e_product_motifname as namamotif, 
										c.e_product_name as nama,
										c.v_product_mill as harga
										from tr_product_motif a,tr_product c, tm_op_item b
										where b.i_op='$op' and a.i_product=c.i_product 
										and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
									   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5),$op);
			$data['baris']=$baris;
			$this->load->view('bbmretur/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bbmretur/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('bbmretur/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ttb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bbmretur/cform/ttb/index/';
			$config['per_page'] = '10';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
										              from tm_ttbretur a, tr_customer b, tr_area c, tr_salesman d
										              where a.i_customer=b.i_customer
										              and (a.i_ttb like '%$cari%')
										              and a.i_area=c.i_area
                                  and b.i_area=c.i_area and d.i_area=c.i_area 
                                  and not d_receive1 is NULL
										              and a.i_bbm isnull
                                  and a.f_ttb_cancel='f'
										              and a.i_salesman=d.i_salesman",false);
#and a.i_area=d.i_area
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_ttbr');
			$data['isi']=$this->mmaster->bacattb($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('bbmretur/vlistttb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carittb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bbmretur/cform/ttb/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
										from tm_ttbretur a, tr_customer b, tr_area c, tr_salesman d
										where a.i_customer=b.i_customer
										and (a.i_ttb like '%$cari%')
										and a.i_area=c.i_area
                    and b.i_area=c.i_area and d.i_area=c.i_area
                    and not d_receive1 is NULL
										and a.i_bbm isnull
                    and a.f_ttb_cancel='f'
										and a.i_salesman=d.i_salesman",false);
# and a.i_area=d.i_area
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_ttb');
			$data['isi']=$this->mmaster->carittb($cari, $config['per_page'],$this->uri->segment(5));
			//$data['iarea']=$iarea;
			$this->load->view('bbmretur/vlistttb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productlain()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbmretur/cform/productlain/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c, tr_product_price d
										              where a.i_product=c.i_product and c.i_product=d.i_product and d.i_price_group='00' " ,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproductlain($config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbmretur/vlistproductlain', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductlain()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$data['ttb']=$this->uri->segment(5);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbmretur/cform/productlain/'.$baris.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("	select a.i_product
										              from tr_product_motif a,tr_product c, tr_product_price d
										              where a.i_product=c.i_product and c.i_product=d.i_product and d.i_price_group='00'
									               	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
  									              ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproductlain($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbmretur/vlistproductlain', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function adattb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbmretur');
			$this->load->model('bbmretur/mmaster');
			$data['ibbm']='';
      $iarea=$this->uri->segment(4);
      $eareaname=str_replace('%20',' ',$this->uri->segment(5));
      $ittb=str_replace('%20','',$this->uri->segment(6));
      $icustomer=$this->uri->segment(7);
      $ecustomername=str_replace('%20',' ',$this->uri->segment(8));
      $ecustomername=str_replace('tandakurungbuka','(',$ecustomername);
      $ecustomername=str_replace('tandakurungtutup',')',$ecustomername);
      $ecustomername=str_replace('tandadan','&',$ecustomername);
      $ecustomername=str_replace('tandatitik','.',$ecustomername);
      $ecustomername=str_replace('tandakoma',',',$ecustomername);
      $ecustomername=str_replace('tandagaring','/',$ecustomername);
      $isalesman    =str_replace('%20',' ',$this->uri->segment(9));
      $esalesmanname=str_replace('%20',' ',$this->uri->segment(10));
      $esalesmanname=str_replace('tandatitik','.',$esalesmanname);
      $dttb=$this->uri->segment(11);

      $data['iarea']=$iarea;
      $data['eareaname']=$eareaname;
      $data['ittb']=$ittb;
      $data['icustomer']=$icustomer;
      $data['ecustomername']=$ecustomername;
      $data['isalesman']=$isalesman;
      $data['esalesmanname']=$esalesmanname;
      $data['dttb']=$dttb;
      $thn=substr($dttb,6,4);
		  $query=$this->db->query(" 	select a.i_product, a.i_product_motif, a.e_product_motifname, c.e_product_name,
						                      b.v_unit_price, b.n_quantity, b.i_nota, b.i_product1_grade as i_product_grade
						                      from tr_product_motif a,tr_product c, tm_ttbretur_item b
						                      where trim(b.i_ttb)='$ittb' and b.i_area='$iarea' and n_ttb_year=$thn
                                  and a.i_product=c.i_product and b.i_product1=a.i_product and b.i_product1_motif=a.i_product_motif",false);
      $data['jmlitem']=$query->num_rows();
			$data['detail']=$this->mmaster->bacattbdetail($ittb,$iarea,$thn);
			$this->load->view('bbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
