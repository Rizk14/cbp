<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('tunai');
			$this->load->model('tunai/mmaster');
			$data['ikum']='';
			$this->load->view('tunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('tunai');
			$this->load->view('tunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('tunai')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
        $itunai          = $this->uri->segment(4);
				$iarea		      = $this->uri->segment(5);
				$dfrom		      = $this->uri->segment(6);
				$dto			      = $this->uri->segment(7);
				$data['itunai']     = $itunai;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        $data['pst']	= $this->session->userdata('i_area');
				$this->load->model("tunai/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$itunai);
		 		$this->load->view('tunai/vformupdate',$data);
			}else{
				$this->load->view('tunai/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$itunai	= $this->input->post('itunai', TRUE);
			$dtunai	= $this->input->post('dtunai', TRUE);
			if($dtunai!=''){
				$tmp=explode("-",$dtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$isalesman		    = $this->input->post('isalesman', TRUE);
			$iarea			      = $this->input->post('iarea', TRUE);
			$xiarea			      = $this->input->post('xiarea', TRUE);
			$eareaname		    = $this->input->post('eareaname', TRUE);
			$icustomer    		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$vsisa      			= $this->input->post('vsisa', TRUE);
			$vsisa      			= str_replace(',','',$vsisa);
			if (
				($dtunai != '') && ($iarea!='') && ($icustomer!='') && ($vjumlah!='')
			   )
			{
				$this->load->model('tunai/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$eremark,$vjumlah,$vsisa,$xiarea);
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update Tunai Area '.$iarea.' No:'.$itunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $itunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dtunai	= $this->input->post('dtunai', TRUE);
			if($dtunai!=''){
				$tmp=explode("-",$dtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$isalesman		    = $this->input->post('isalesman', TRUE);
			$iarea			      = $this->input->post('iarea', TRUE);
			$eareaname		    = $this->input->post('eareaname', TRUE);
			$icustomer    		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$vsisa      			= $this->input->post('vsisa', TRUE);
			$vsisa      			= str_replace(',','',$vsisa);
			if (
				($dtunai != '') && ($iarea!='') && ($icustomer!='') && ($vjumlah!='')
			   )
			{
				$this->load->model('tunai/mmaster');
				$this->db->trans_begin();
			  $itunai = $this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insert($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$eremark,$vjumlah,$vsisa);
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Tunai Area '.$iarea.' No:'.$itunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $itunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/tunai/cform/area/index/';
			$config['per_page'] = '10';
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
  			$query = $this->db->query("	select * from tr_area ",false);
  	  }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('tunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('tunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/tunai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
			  $query = $this->db->query("select * from tr_area
									     	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
      }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
  	                                and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('tunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('tunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iarea = $this->input->post('iarea');
			if($iarea=='') $iarea 	= $this->uri->segment(4);
		  $tgl = $this->input->post('tgl');
			if($tgl=='') $tgl = $this->uri->segment(5);
			$tmp=explode('-',$tgl);
			$yy=$tmp[2];
			$mm=$tmp[1];
			$per=$yy.$mm;
		  $cari = strtoupper($this->input->post('cari'));
		  if($cari=='' && $this->uri->segment(6)=='sikasep'){
		    $cari = '';
		  }elseif($cari==''){
		    $cari=$this->uri->segment(6);
		  }
		  if($cari==''){
	  		$config['base_url'] = base_url().'index.php/tunai/cform/customer/'.$iarea.'/'.$tgl.'/sikasep/';
      }else{			
  			$config['base_url'] = base_url().'index.php/tunai/cform/customer/'.$iarea.'/'.$tgl.'/'.$cari.'/';
      }
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, b.i_customer_groupar, 
			              c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per')
										left join tr_customer_owner d on(a.i_customer=d.i_customer)
										where a.i_area = '$iarea'
										and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('tunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$per,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
      $data['tgl']=$tgl;
			$this->load->view('tunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      $cari=str_replace("%20"," ",$cari);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/tunai/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/tunai/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
								left join tr_customer_groupar b on(a.i_customer=b.i_customer)
								left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
								left join tr_customer_owner d on(a.i_customer=d.i_customer)

								where a.i_area='$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' or 
									upper(d.e_customer_setor) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('tunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('tunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu490')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea = strtoupper($this->input->post('iarea', FALSE));
         if($iarea=='') $iarea=$this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/tunai/cform/salesman/'.$iarea.'/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query = $this->db->query("select distinct a.i_salesman, a.e_salesman_name from tr_salesman a
                                    where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                    and a.i_area='$iarea' and a.f_salesman_aktif='true' ",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $this->load->model('tunai/mmaster');
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->bacasalesman($iarea,$cari,$config['per_page'],$this->uri->segment(5));
         $this->load->view('tunai/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
