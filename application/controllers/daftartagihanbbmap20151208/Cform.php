<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title']	= $this->lang->line('dtap');
      $data['xdtap']            = '';
			$data['idtap']		        = '';
      $data['do1']              = '';
      $data['do2']              = '';
      $data['do3']              = '';
      $data['do4']              = '';
      $data['do5']              = '';
      $data['do6']              = '';
      $data['do7']              = '';
      $data['do8']              = '';
      $data['do9']              = '';
      $data['do10']             = '';
      $data['do11']              = '';
      $data['do12']              = '';
      $data['do13']              = '';
      $data['do14']              = '';
      $data['do15']              = '';
      $data['do16']              = '';
      $data['do17']              = '';
      $data['do18']              = '';
      $data['do19']              = '';
      $data['do20']             = '';
      $data['ddtap']            = '';
      $data['eareaname']        = '';
      $data['iarea']            = '';
      $data['esuppliername']    = '';
      $data['isupplier']        = '';
      $data['dduedate']         = '';
      $data['esupplieraddress'] = '';
      $data['esuppliercity']    = '';
      $data['ipajak']           = '';
      $data['dpajak']           = '';
      $data['gross']            = '0';
      $data['ndisc']            = '0';
      $data['vdisc']            = '0';
      $data['fsupplierpkp']     = '';
			$data['jmlitem']	        = "0";
			$this->load->view('daftartagihanbbmap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idtap 	= $this->input->post('idtap', TRUE);
			$ddtap 	= $this->input->post('ddtap', TRUE);
			if($ddtap!=''){
				$tmp	=explode("-",$ddtap);
				$th  	= $tmp[2];
				$bl  	= $tmp[1];
				$hr  	= $tmp[0];
				$ddtap= $th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$iarea						= $this->input->post('iarea', TRUE);
			$eareaname				= $this->input->post('eareaname', TRUE);
			$isupplier				= $this->input->post('isupplier', TRUE);
			$esuppliername		= $this->input->post('esuppliername', TRUE);
			$esuppliercity		= $this->input->post('esuppliercity', TRUE);
			$esupplieraddress	= $this->input->post('esupplieraddress', TRUE);
			$fsupplierpkp 		= $this->input->post('fsupplierpkp', TRUE);
			$dduedate	 				= $this->input->post('dduedate', TRUE);
			if($dduedate!=''){
				$tmp=explode("-",$dduedate);
				$th  = $tmp[2];
				$bl  = $tmp[1];
				$hr  = $tmp[0];
				$dduedate = $th."-".$bl."-".$hr;
			}
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$ndiscount	= $this->input->post('ndiscount', TRUE);
			$ndiscount	= str_replace(',','',$ndiscount);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$ipajak		= $this->input->post('ipajak', TRUE);
			$dpajak	 	= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th  = $tmp[2];
				$bl  = $tmp[1];
				$hr  = $tmp[0];
				$dpajak = $th."-".$bl."-".$hr;
			}
			$vppn	= $this->input->post('vppn', TRUE);
			$vppn	= str_replace(',','',$vppn);
			$vnetto	= $this->input->post('vnetto', TRUE);
			$vnetto	= str_replace(',','',$vnetto);
			$jml	= $this->input->post('jml', TRUE);
			if(
				($iarea!='')
				&& ($idtap!='')
				&& ($isupplier!='')
				&& ($vnetto!='0')
				&& ($jml!='0')
				&& ($dduedate!='')
			  )
			{				
				$this->db->trans_begin();
				$this->load->model('daftartagihanbbmap/mmaster');
				for($i=1;$i<=$jml;$i++){
          $ido			= $this->input->post('ido'.$i, TRUE);
          $iop			= $this->input->post('iop'.$i, TRUE);
          $ddo	 	= $this->input->post('ddo'.$i, TRUE);
			    if($ddo!=''){
				    $tmp=explode("-",$ddo);
				    $th  = $tmp[2];
				    $bl  = $tmp[1];
				    $hr  = $tmp[0];
				    $ddo = $th."-".$bl."-".$hr;
			    }
			      	$iproduct		    = $this->input->post('iproduct'.$i, TRUE);
			      	$iproductmotif	= '00';
					    $eproductname	  = $this->input->post('eproductname'.$i, TRUE);
					    $njumlah		= $this->input->post('ndeliver'.$i, TRUE);
					    $njumlah		= str_replace(',','',$njumlah);
					    $vpabrik		= $this->input->post('vunitprice'.$i, TRUE);
					    $vpabrik		= str_replace(',','',$vpabrik);
					    $diskon			= ($njumlah*$vpabrik*$ndiscount)/100;
      				$this->mmaster->insertdetail($idtap,$ido,$iop,$isupplier,$iarea,$iproduct,$iproductmotif,
						     						 $eproductname,$ddtap,$njumlah,$vpabrik,$diskon,$ddo,$i,$thbl);
				}
				$this->mmaster->insertheader( $idtap,$iarea,$isupplier,$ipajak,$ddtap,$dduedate,$dpajak,
											                $fsupplierpkp,$ndiscount,$vgross,$vdiscount,$vppn,$vnetto,$thbl);
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input DT BBM AP No:'.$idtap.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $idtap;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('dtap');
			$this->load->view('daftartagihanbbmap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('dtap')." update";
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='') && ($this->uri->segment(6)!='')
			  ){
        $xdtap    = '';
				$idtap		= $this->uri->segment(4);
				$idtap		= str_replace('%20','',$this->uri->segment(4));
				$iarea		= $this->uri->segment(5);
				$isupplier= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto 			= $this->uri->segment(8);
				$query 				= $this->db->query("select * from tm_dtap_item where i_dtap = '$idtap' and i_area='$iarea' and i_supplier='$isupplier' ");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['idtap'] 		= $idtap;
				$data['xdtap'] 		= $xdtap;
				$data['iarea']		= $iarea;
				$data['isupplier']= $isupplier;
				$data['dfrom']		= $dfrom;
				$data['dto']			= $dto;
				$this->load->model('daftartagihanbbmap/mmaster');
				$data['isi']		= $this->mmaster->baca($idtap,$iarea,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($idtap,$iarea,$isupplier);
		 		$this->load->view('daftartagihanbbmap/vmainform',$data);
			}else{
				$this->load->view('daftartagihanbbmap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $xdtap 	= $this->input->post('xdtap', TRUE);
			$idtap 	= $this->input->post('idtap', TRUE);
			$ddtap 	= $this->input->post('ddtap', TRUE);
			if($ddtap!=''){
				$tmp=explode("-",$ddtap);
				$th  = $tmp[2];
				$bl  = $tmp[1];
				$hr  = $tmp[0];
				$ddtap = $th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$iarea			= $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$isupplier		= $this->input->post('isupplier', TRUE);
			$esuppliername	= $this->input->post('esuppliername', TRUE);
			$esuppliercity	= $this->input->post('esuppliercity', TRUE);
			$esupplieraddress	= $this->input->post('esupplieraddress', TRUE);
			$fsupplierpkp 	= $this->input->post('fsupplierpkp', TRUE);
			$dduedate	 	= $this->input->post('dduedate', TRUE);
			if($dduedate!=''){
				$tmp=explode("-",$dduedate);
				$th  = $tmp[2];
				$bl  = $tmp[1];
				$hr  = $tmp[0];
				$dduedate = $th."-".$bl."-".$hr;
			}
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$ndiscount= $this->input->post('ndiscount', TRUE);
			$ndiscount= str_replace(',','',$ndiscount);
			$vdiscount= $this->input->post('vdiscount', TRUE);
			$vdiscount= str_replace(',','',$vdiscount);
			$ipajak		= $this->input->post('ipajak', TRUE); //echo $ipajak; die();
			$dpajak	 	= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th  = $tmp[2];
				$bl  = $tmp[1];
				$hr  = $tmp[0];
				$dpajak = $th."-".$bl."-".$hr;
			}
			$vppn	= $this->input->post('vppn', TRUE);
			$vppn	= str_replace(',','',$vppn);
			$vnetto	= $this->input->post('vnetto', TRUE);
			$vnetto	= str_replace(',','',$vnetto);
			$jml	= $this->input->post('jml', TRUE);
			if(
				($iarea!='')
				&& ($idtap!='')
				&& ($isupplier!='')
				&& ($vnetto!='0')
				&& ($jml!='0')
				&& ($dduedate!='')
			  )
			{				
				$this->db->trans_begin();
				$this->load->model('daftartagihanbbmap/mmaster');
				for($i=1;$i<=$jml;$i++){
				  $ido			= $this->input->post('ido'.$i, TRUE);
				  $iproduct		= $this->input->post('iproduct'.$i, TRUE);
				  $iproductmotif	= $this->input->post('iproductmotif'.$i, TRUE);
					$eproductname	= $this->input->post('eproductname'.$i, TRUE);
				  $iop				= $this->input->post('iop'.$i, TRUE);
					$njumlah		= $this->input->post('ndeliver'.$i, TRUE);
					$njumlah		= str_replace(',','',$njumlah);
					$vpabrik		= $this->input->post('vunitprice'.$i, TRUE);
					$vpabrik		= str_replace(',','',$vpabrik);
					$diskon			= ($njumlah*$vpabrik*$ndiscount)/100;
					$ddo			 	= $this->input->post('ddo'.$i, TRUE);
					if($ddo!=''){
						$tmp=explode("-",$ddo);
						$th  = $tmp[2];
						$bl  = $tmp[1];
						$hr  = $tmp[0];
						$ddo = $th."-".$bl."-".$hr;
					}
					$this->mmaster->deletedetail($xdtap,$ido,$iop,$isupplier,$iarea,$iproduct,$iproductmotif,
												 $njumlah,$vpabrik,$diskon,$vppn,$i);
          if($ddo!=''){
					  $this->mmaster->insertdetail($idtap,$ido,$iop,$isupplier,$iarea,$iproduct,$iproductmotif,
						   						 $eproductname,$ddtap,$njumlah,$vpabrik,$diskon,$ddo,$i,$thbl);
          }else{
					  $this->mmaster->insertdetailkhusus($idtap,$ido,$iop,$isupplier,$iarea,$iproduct,$iproductmotif,
						   						 $eproductname,$ddtap,$njumlah,$vpabrik,$diskon,$i,$thbl);
          }
				}
				$this->mmaster->deleteheader($xdtap,$iarea,$isupplier);
				$this->mmaster->insertheader($idtap,$iarea,$isupplier,$ipajak,$ddtap,$dduedate,$dpajak,
											 $fsupplierpkp,$ndiscount,$vgross,$vdiscount,$vppn,$vnetto,$thbl);
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update DT BBM AP No:'.$idtap.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $idtap;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idtap		= $this->input->post('idtapdelete', TRUE);
			$iarea		= $this->input->post('iareadelete', TRUE);
			$this->load->model('daftartagihanbbmap/mmaster');
			$this->mmaster->delete($idtap,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus DT BBM AP No:'.$idtap.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title']	= $this->lang->line('dtap');
			$data['idtap']		= '';
			$data['iarea']		= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
			$data['isi']		= '';
			$this->load->view('daftartagihanbbmap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idtap				= $this->uri->segment(4);
			$ido					= $this->uri->segment(5);
			$iop					= $this->uri->segment(6);
			$isupplier		= $this->uri->segment(7);
			$iarea				= $this->uri->segment(8);
			$iproduct			= $this->uri->segment(9);
			$iproductmotif= $this->uri->segment(10);
			$jumlah				= $this->uri->segment(11);
			$pabrik				= $this->uri->segment(12);
			$ndiskon			= $this->uri->segment(13);
			$vppn					= $this->uri->segment(14);
			$dfrom				= $this->uri->segment(15);
			$dto					= $this->uri->segment(16);
			
			$this->db->trans_begin();
			$this->load->model('daftartagihanbbmap/mmaster');
			$this->mmaster->deletedetail($idtap,$ido,$iop,$isupplier,$iarea,$iproduct,$iproductmotif,
										 $jumlah,$pabrik,$vppn,$ndiskon);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('dtap')." Update";
				$query 				= $this->db->query("select * from tm_dtap_item  where i_dtap = '$idtap' and i_area='$iarea' 
														and i_supplier='$isupplier'");
				$data['jmlitem'] 	= $query->num_rows();
				$data['idtap'] 		= $idtap;
				$data['iarea']		= $iarea;
				$data['isupplier']= $isupplier;
				$data['dfrom']		= $dfrom;
				$data['dto']			= $dto;
				$data['isi']		= $this->mmaster->baca($idtap,$iarea,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($idtap,$iarea,$isupplier);
				$this->load->view('daftartagihanbbmap/vmainform', $data);
			}

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ido()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				  = $this->uri->segment(5);
			$data['area']	= $this->uri->segment(5);
			$supplier			= $this->uri->segment(6);
			$data['supplier'] 	= $this->uri->segment(6);
			$ndo			= $this->uri->segment(7);
			$data['ndo'] 	= $this->uri->segment(7);
			$idtap 	= $this->uri->segment(8);
			$data['idtap'] 	= $this->uri->segment(8);
			$ddtap 	= $this->uri->segment(9);
			$data['ddtap'] 	= $this->uri->segment(9);
      $periode=substr($ddtap,6,4).substr($ddtap,3,2);
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/ido/'.$baris.'/'.$area.'/'.$supplier.'/'.$ndo.'/'.$idtap.'/'.$ddtap.'/';
      $query = $this->db->query(" select sum(c.n_receive*c.v_product_mill) as v_do_gross
                                  from tm_ap a, tr_supplier b, tm_ap_item c
                                  where a.i_supplier=b.i_supplier and a.i_supplier=c.i_supplier
                                  and a.i_ap not in
                                  (select d.i_do from tm_dtap_item d, tm_dtap e
                                  where to_char(d.d_dtap,'yyyymm')='$periode' and d.i_dtap=e.i_dtap and d.i_area=e.i_area 
                                  and d.i_supplier=e.i_supplier
                                  and a.i_area=d.i_area and a.i_supplier=d.i_supplier and a.i_op=d.i_op and e.f_dtap_cancel='f')
                                  and a.i_ap=c.i_ap and a.f_ap_cancel='f' and a.i_area='$area' and a.i_supplier='$supplier'
                                  and to_char(a.d_ap,'yyyymm')='$periode'
                                  group by a.i_ap, a.d_ap, a.i_op, a.i_supplier, b.e_supplier_name" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(10);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['isi']=$this->mmaster->bacado($periode,$supplier,$area,$config['per_page'],$this->uri->segment(10));
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariido()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris				= $this->input->post("baris");
      if($baris=='') $baris=$this->uri->segment(4);
      $data['baris']= $baris;
			$area				  = $this->input->post("area");
      if($area=='') $area=$this->uri->segment(5);
			$data['area']	= $area;
			$supplier			= $this->input->post("supplier");
      if($supplier=='') $supplier=$this->uri->segment(6);
			$data['supplier'] 	= $supplier;
			$ndo			= $this->input->post("barisdo");
      if($ndo=='') $ndo=$this->uri->segment(7);
			$data['ndo'] 	= $ndo;
			$idtap			= $this->input->post("nodtap");
      if($idtap=='') $idtap=$this->uri->segment(8);
			$data['idtap'] 	= $idtap;
			$ddtap			= $this->input->post("tgldtap");
      if($ddtap=='') $ddtap 	= $this->uri->segment(9);
			$data['ddtap'] 	= $ddtap;
      $periode=substr($ddtap,6,4).substr($ddtap,3,2);
      $cari=trim(strtoupper($this->input->post("cari")));
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/cariido/'.$baris.'/'.$area.'/'.$supplier.'/'.$ndo.'/'.$idtap.'/'.$ddtap.'/';
      $query = $this->db->query(" select sum(c.n_deliver*c.v_product_mill) as v_do_gross
                                  from tm_ap a, tr_supplier b, tm_ap_item c
                                  where a.i_supplier=b.i_supplier
                                  and a.i_do not in
                                  (select d.i_do from tm_dtap_item d, tm_dtap e
                                  where to_char(d.d_dtap,'yyyymm')='$periode' and d.i_dtap=e.i_dtap and d.i_area=e.i_area and d.i_supplier=e.i_supplier
                                  and a.i_area=d.i_area and a.i_supplier=d.i_supplier and a.i_op=d.i_op and a.d_do=d.d_do and e.f_dtap_cancel='f')
                                  and a.i_do=c.i_do
                                  and a.f_do_cancel='f'
                                  and a.i_area='$area'
                                  and a.i_supplier='$supplier'
                                  and to_char(a.d_do,'yyyymm')='$periode'
                                  and (upper(a.i_do) like '%$cari%')
                                  group by a.i_do, a.d_do, a.i_op, a.i_supplier, b.e_supplier_name" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['isi']=$this->mmaster->carido($periode,$cari,$supplier,$area,$config['per_page'],$this->uri->segment(9));
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function idotirai()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $baris				= $this->input->post("baris");
      if($baris=='') $baris=$this->uri->segment(4);
      $data['baris']= $baris;
			$area				  = $this->input->post("area");
      if($area=='') $area=$this->uri->segment(5);
			$data['area']	= $area;
			$supplier			= $this->input->post("supplier");
      if($supplier=='') $supplier=$this->uri->segment(6);
			$data['supplier'] 	= $supplier;
			$ndo			= $this->input->post("barisdo");
      if($ndo=='') $ndo=$this->uri->segment(7);
			$data['ndo'] 	= $ndo;
			$idtap			= $this->input->post("nodtap");
			if($idtap=='')$idtap 	= $this->uri->segment(8);
			$data['idtap'] 	= $idtap;
      $ddtap			= $this->input->post("ddtap");
			if($ddtap=='')$ddtap 	= $this->uri->segment(9);
			$data['ddtap'] 	= $ddtap;
      $periode=substr($ddtap,6,4).substr($ddtap,3,2);
      $cari=trim(strtoupper($this->input->post("cari")));
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/idotirai/'.$baris.'/'.$area.'/'.$supplier.'/'.$ndo.'/'.$idtap.'/'.$ddtap.'/';
/*
      $query = $this->db->query(" select sum(c.n_deliver*c.v_product_mill) as v_do_gross
                                  from tm_ap a, tr_supplier b, tm_ap_item c
                                  where a.i_supplier=b.i_supplier
                                  and a.i_do=c.i_do
                                  and a.f_do_cancel='f'
                                  and a.d_do <= to_date('$ddtap','dd-mm-yyyy')
                                  and a.i_supplier='$supplier'
                                  group by a.i_do, a.d_do, a.i_op, b.e_supplier_name" ,false);
#                                  and a.d_area='$area'

*/
      $query = $this->db->query(" select sum(c.n_deliver*c.v_product_mill) as v_do_gross
                                  from tm_ap a, tr_supplier b, tm_ap_item c
                                  where a.i_supplier=b.i_supplier
                                  and a.i_do=c.i_do and a.i_supplier=c.i_supplier
                                  and a.f_do_cancel='f'
                                  and a.i_supplier='$supplier'
                                  and to_char(a.d_do,'yyyymm')='$periode'
                                  and a.i_do not in
                                  (select d.i_do from tm_dtap_item d, tm_dtap e 
                                  where to_char(d.d_dtap,'yyyymm')='$periode' and d.i_dtap=e.i_dtap and d.i_area=e.i_area and d.i_supplier=e.i_supplier
                                  and a.i_supplier=d.i_supplier and a.i_op=d.i_op and a.d_do=d.d_do and e.f_dtap_cancel='f')
                                  and (upper(a.i_do) like '%$cari%' or upper(a.i_op) like '%$cari%')
                                  group by a.i_do, a.d_do, a.i_op, a.i_supplier, b.e_supplier_name" ,false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(10);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['isi']=$this->mmaster->bacadotirai($cari,$periode,$supplier,$area,$config['per_page'],$this->uri->segment(10),$ddtap);
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdotirai', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariidotirai()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris				= $this->input->post("baris");
      if($baris=='') $baris=$this->uri->segment(4);
      $data['baris']= $baris;
			$area				  = $this->input->post("area");
      if($area=='') $area=$this->uri->segment(5);
			$data['area']	= $area;
			$supplier			= $this->input->post("supplier");
      if($supplier=='') $supplier=$this->uri->segment(6);
			$data['supplier'] 	= $supplier;
			$ndo			= $this->input->post("barisdo");
      if($ndo=='') $ndo=$this->uri->segment(7);
			$data['ndo'] 	= $ndo;
			$idtap			= $this->input->post("nodtap");
      if($idtap=='') $idtap=$this->uri->segment(8);
			$data['idtap'] 	= $idtap;
      $ddtap			= $this->input->post("ddtap");
      if($ddtap=='')$ddtap=$this->uri->segment(9);
			$data['ddtap'] 	= $ddtap;
      $periode=substr($ddtap,6,4).substr($ddtap,3,2);
      $cari=trim(strtoupper($this->input->post("cari")));
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/idotirai/'.$baris.'/'.$area.'/'.$supplier.'/'.$ndo.'/'.$idtap.'/'.$ddtap.'/';
      $query = $this->db->query(" select sum(c.n_deliver*c.v_product_mill) as v_do_gross
                                  from tm_ap a, tr_supplier b, tm_ap_item c
                                  where a.i_supplier=b.i_supplier
                                  and a.i_do=c.i_do and a.i_supplier=c.i_supplier
                                  and a.f_do_cancel='f'
                                  and a.i_supplier='$supplier'
                                  and to_char(a.d_do,'yyyymm')='$periode'
                                  and a.i_do not in
                                  (select d.i_do from tm_dtap_item d, tm_dtap e 
                                  where to_char(d.d_dtap,'yyyymm')='$periode' and d.i_dtap=e.i_dtap and d.i_area=e.i_area and d.i_supplier=e.i_supplier
                                  and a.i_supplier=d.i_supplier and a.i_op=d.i_op and a.d_do=d.d_do and e.f_dtap_cancel='t')
                                  and (upper(a.i_do) like '%$cari%' or upper(a.i_op) like '%$cari%')
                                  group by a.i_do, a.d_do, a.i_op, a.i_supplier, b.e_supplier_name" ,false);
#and a.i_area='$area'
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['isi']=$this->mmaster->caridotirai($cari,$supplier,$area,$config['per_page'],$this->uri->segment(9));
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function idoupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				= $this->uri->segment(5);
			$data['area']		= $this->uri->segment(5);
			$supplier			= $this->uri->segment(6);
			$data['supplier']	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/idoupdate/'.$baris.'/'.$area.'/'.$supplier.'/index/';
			$query = $this->db->query(" select x.*, a.i_op, b.e_supplier_name, c.e_area_name, d.e_product_motifname
										from tm_ap_item x, tm_ap a, tr_supplier b, tr_area c, tr_product_motif d
										where a.i_supplier=b.i_supplier
										and a.i_area=c.i_area
										and x.i_do=a.i_do
										and x.i_supplier=a.i_supplier
										and x.i_product=d.i_product
										and x.i_product_motif=d.i_product_motif
										and a.i_area='$area'
										and a.i_supplier='$supplier'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_do');
			$data['isi']=$this->mmaster->bacado($supplier,$area,$config['per_page'],$this->uri->segment(8));
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdoupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariidoupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				= $this->uri->segment(5);
			$data['area']		= $this->uri->segment(5);
			$supplier			= $this->uri->segment(6);
			$data['supplier']	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/ido/'.$baris.'/'.$area.'/'.$supplier.'/index/';
			$query = $this->db->query(" select x.*, a.i_op, b.e_supplier_name, c.e_area_name, d.e_product_motifname
										from tm_ap_item x, tm_ap a, tr_supplier b, tr_area c, tr_product_motif d
										where a.i_supplier=b.i_supplier
										and a.i_area=c.i_area
										and x.i_do=a.i_do
										and x.i_supplier=a.i_supplier
										and x.i_product=d.i_product
										and x.i_product_motif=d.i_product_motif
										and a.i_area='$area'
										and a.i_supplier='$supplier'
										and(a.i_do like '%$cari%' or a.i_supplier like '%$cari%')" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_do');
			$data['isi']=$this->mmaster->carido($cari,$supplier,$area,$config['per_page'],$this->uri->segment(8));
			$data['baris']=$baris;
			$this->load->view('daftartagihanbbmap/vlistdo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/area/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('daftartagihanbbmap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/area/index/';
			$config['per_page'] = '10';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser); 
			$this->load->view('daftartagihanbbmap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function areaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/areaupdate/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('daftartagihanbbmap/vlistareaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariareaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/areaupdate/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('daftartagihanbbmap/vlistareaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/index/';
			$query=$this->db->query("select * from tm_dt",false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title']	= $this->lang->line('dtap');
			$data['idtap']		= '';
			$data['iarea']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
	 		$this->load->view('daftartagihanbbmap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/supplier/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$iarea=$this->uri->segment(4);
			$query = $this->db->query("	select * from tr_supplier where (upper(i_supplier) like '%$cari%' 
              										or upper(e_supplier_name) like '%$cari%') and i_supplier_group='G0000' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('daftartagihanbbmap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/supplier/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$iarea=$this->uri->segment(4);
			$query = $this->db->query("	select * from tr_supplier where upper(i_supplier) like '%$cari%' 
										or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('daftartagihanbbmap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function adado()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('dtap');
			$this->load->model('daftartagihanbbmap/mmaster');
			$data['idtap']='';
      $do1=str_replace('%20',' ',$this->uri->segment(4));
      if($do1=='dudung') $do1='';
      $do2=str_replace('%20',' ',$this->uri->segment(5));
      if($do2=='dudung') $do2='';
      $do3=str_replace('%20',' ',$this->uri->segment(6));
      if($do3=='dudung') $do3='';
      $do4=str_replace('%20',' ',$this->uri->segment(7));
      if($do4=='dudung') $do4='';
      $do5=str_replace('%20',' ',$this->uri->segment(8));
      if($do5=='dudung') $do5='';
      $do6=str_replace('%20',' ',$this->uri->segment(9));
      if($do6=='dudung') $do6='';
      $do7=str_replace('%20',' ',$this->uri->segment(10));
      if($do7=='dudung') $do7='';
      $do8=str_replace('%20',' ',$this->uri->segment(11));
      if($do8=='dudung') $do8='';
      $do9=str_replace('%20',' ',$this->uri->segment(12));
      if($do9=='dudung') $do9='';
      $do10=str_replace('%20',' ',$this->uri->segment(13));
      if($do10=='dudung') $do10='';
      $do11=str_replace('%20',' ',$this->uri->segment(14));
      if($do11=='dudung') $do11='';
      $do12=str_replace('%20',' ',$this->uri->segment(15));
      if($do12=='dudung') $do12='';
      $do13=str_replace('%20',' ',$this->uri->segment(16));
      if($do13=='dudung') $do13='';
      $do14=str_replace('%20',' ',$this->uri->segment(17));
      if($do14=='dudung') $do14='';
      $do15=str_replace('%20',' ',$this->uri->segment(18));
      if($do15=='dudung') $do15='';
      $do16=str_replace('%20',' ',$this->uri->segment(19));
      if($do16=='dudung') $do16='';
      $do17=str_replace('%20',' ',$this->uri->segment(20));
      if($do17=='dudung') $do17='';
      $do18=str_replace('%20',' ',$this->uri->segment(21));
      if($do18=='dudung') $do18='';
      $do19=str_replace('%20',' ',$this->uri->segment(22));
      if($do19=='dudung') $do19='';
      $do20=str_replace('%20',' ',$this->uri->segment(23));
      if($do20=='dudung') $do20='';
      $ddtap=$this->uri->segment(24);
      $eareaname=str_replace('%20',' ',$this->uri->segment(25));
      $iarea=$this->uri->segment(26);
      $esuppliername=str_replace('%20',' ',$this->uri->segment(27));
      $esuppliername=str_replace('tandakurungbuka','(',$esuppliername);
      $esuppliername=str_replace('tandadan','&',$esuppliername);
      $esuppliername=str_replace('tandakurungtutup',')',$esuppliername);
      $esuppliername=str_replace('tandatitik','.',$esuppliername);
      $esuppliername=str_replace('tandakoma',',',$esuppliername);
      $esuppliername=str_replace('tandagaring','/',$esuppliername);
      $isupplier=$this->uri->segment(28);
      $dduedate=$this->uri->segment(29);
      $esupplieraddress=str_replace('%20',' ',$this->uri->segment(30));
      $esupplieraddress=str_replace('tandatitik','.',$esupplieraddress);
      $esupplieraddress=str_replace('tandakurungbuka','(',$esupplieraddress);
      $esupplieraddress=str_replace('tandakurungtutup',')',$esupplieraddress);
      $esupplieraddress=str_replace('tandatitikkoma',';',$esupplieraddress);
      $esupplieraddress=str_replace('tandakoma',',',$esupplieraddress);
      $esupplieraddress=str_replace('tandagaring','/',$esupplieraddress);
      $esupplieraddress=str_replace('tandadan','&',$esupplieraddress);
	  if($esupplieraddress=='kosong') $esupplieraddress='';
      $ipajak=$this->uri->segment(31);
      if($ipajak=='kosong') $ipajak='';
      $dpajak=$this->uri->segment(32);
      if($dpajak=='kosong') $dpajak='';
      $fsupplierpkp=$this->uri->segment(33);
      $esuppliercity=$this->uri->segment(34);
 	  if($esuppliercity=='kosong') $esuppliercity='';
      $gross=$this->uri->segment(35);
      $gross=str_replace('tandakoma',',',$gross);
      $ndisc=$this->uri->segment(36);
      $ndisc=str_replace('tandakoma',',',$ndisc);
      $vdisc=$this->uri->segment(37);
      $vdisc=str_replace('tandakoma',',',$vdisc);
      $xdtap=$this->uri->segment(38);
      if($xdtap=='')$xdtap='idtap';
      $data['do1']=$do1;
      $data['do2']=$do2;
      $data['do3']=$do3;
      $data['do4']=$do4;
      $data['do5']=$do5;
      $data['do6']=$do6;
      $data['do7']=$do7;
      $data['do8']=$do8;
      $data['do9']=$do9;
      $data['do10']=$do10;
      $data['do11']=$do11;
      $data['do12']=$do12;
      $data['do13']=$do13;
      $data['do14']=$do14;
      $data['do15']=$do15;
      $data['do16']=$do16;
      $data['do17']=$do17;
      $data['do18']=$do18;
      $data['do19']=$do19;
      $data['do20']=$do20;
      $data['ddtap']=$ddtap;
      $data['eareaname']=$eareaname;
      $data['iarea']=$iarea;
      $data['esuppliername']=$esuppliername;
      $data['isupplier']=$isupplier;
      $data['dduedate']=$dduedate;
      $data['esupplieraddress']=$esupplieraddress;
      $data['esuppliercity']=$esuppliercity;
      $data['ipajak']=$ipajak;
      $data['dpajak']=$dpajak;
      $data['fsupplierpkp']=$fsupplierpkp;
      $data['gross']=$gross;
      $data['ndisc']=$ndisc;
      $data['vdisc']=$vdisc;
      $data['xdtap']=$xdtap;
      $data['jmlitem']	= "0";
			$this->load->view('daftartagihanbbmap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function harga()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu332')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari   = strtoupper($this->input->post('cari', FALSE));
      $baris  = strtoupper($this->input->post('baris', FALSE));
      $iproduct= strtoupper($this->input->post('iproduct'.$baris, FALSE));
 			if($iproduct=='') $iproduct=$this->uri->segment(4);
 			if($baris=='') $baris=$this->uri->segment(5);
      $config['base_url'] = base_url().'index.php/daftartagihanbbmap/cform/harga/'.$iproduct.'/'.$baris.'/';
			$str	= " select i_product from tr_harga_beli where i_product='$iproduct'
								and (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%') ";
			$query = $this->db->query($str,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('daftartagihanbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_price');
			$data['isi']=$this->mmaster->bacaharga($cari,$config['per_page'],$this->uri->segment(6),$iproduct);
			$data['baris']=$baris;
			$data['iproduct']=$iproduct;
			$data['cari']=$cari;
			$this->load->view('daftartagihanbbmap/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
