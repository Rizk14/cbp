<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $dby        = $this->input->post('dby', FALSE);
         $iarea      = $this->input->post('iarea', FALSE);
         $icustomer  = $this->input->post('icustomer', FALSE);
         $ijenisby   = $this->input->post('ijenisby', FALSE);
         $vby        = $this->input->post('vby', TRUE);
         $isalesman        = $this->input->post('isalesman', TRUE);
         $vby        = str_replace(',','',$vby);
         $vsisa      = $this->input->post('vsisa', FALSE);
         $remark     = $this->input->post('remark',FALSE);
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_penjualan_manual');

         if( ($dby!='') && ($iarea!='') && ($icustomer!='') && ($vby!=''))
          {
             $this->db->trans_begin();
             $iby=$this->mmaster->runningnumber($iarea);
             $this->mmaster->simpanby($iby,$dby,$iarea,$icustomer,$ijenisby,$vby,$remark,$isalesman);
               if (($this->db->trans_status() === FALSE))
               {
                    $this->db->trans_rollback();
                }else{
                  $this->db->trans_commit();
                   $sess=$this->session->userdata('session_id');
                   $id=$this->session->userdata('user_id');
                   $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                   $rs      = pg_query($sql);
                   if(pg_num_rows($rs)>0){
                      while($row=pg_fetch_assoc($rs)){
                         $ip_address   = $row['ip_address'];
                         break;
                      }
                   }else{
                      $ip_address='kosong';
                   }
                   $query   = pg_query("SELECT current_timestamp as c");
                   while($row=pg_fetch_assoc($query)){
                      $now    = $row['c'];
                   }
                   $pesan='Input Penjualan Manual '.$iarea.' No:'.$iby;
                   $this->load->model('logger');
                   $this->logger->write($id, $ip_address, $now , $pesan );
                   $data['sukses']         = true;
                   $data['inomor']         = $iby;
                   $this->load->view('nomor',$data);
                 }
          }elseif(($dby=='') && ($iarea=='') && ($icustomer=='') && ($vby==''))
          {
            $data['page_title'] = $this->lang->line('inputpm');
            $this->load->model('penjualan_manual/mmaster');  
            $data['isi'] = '';
            $this->load->view('penjualan_manual/vmainform', $data);
          }
        }else{
           $this->load->view('awal/index.php');
        }
     }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('master_spb');
         $this->load->view('penjualan_manual/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
  {
    if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu168')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
    ){
      $data['page_title'] = $this->lang->line('penjualan_manual')." update";
      if($this->uri->segment(4)!=''){
        $dpm    = $this->uri->segment(4);
        $isalesman= $this->uri->segment(5);
        $iarea    = $this->uri->segment(6);
        $dfrom    = $this->uri->segment(7);
        $dto    = $this->uri->segment(8);
        $data['dpm']  = $dpm;
        $data['isalesman']= $isalesman;
        $data['iarea']  = $iarea;
        $data['dfrom']  = $dfrom;
        $data['dto']  = $dto;
        $tmp=explode("-",$dpm);
        $th=$tmp[0];
        $bl=$tmp[1];
        $hr=$tmp[2];
        //$data['hari'] = dinten($hr,$bl,$th);
        $query = $this->db->query("select * from tm_penjualan where d_pm='$dpm' and i_salesman='$isalesman' and i_area='$iarea'");
        $data['jmlitem'] = $query->num_rows();        
        $this->load->model('penjualan_manual/mmaster');
        $data['isi']   = $this->mmaster->baca($dpm,$isalesman,$iarea);
        $this->load->view('penjualan_manual/vmainform',$data);
      }else{
        $this->load->view('penjualan_manual/vinsert_fail',$data);
      }
    }else{
      $this->load->view('awal/index.php');
    }
  }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/area/index/';
         $allarea= $this->session->userdata('allarea');
         $iuser   = $this->session->userdata('user_id');
         if($allarea=='t')
         {
            $query = $this->db->query(" select * from tr_area order by i_area", false);
         }
         else
         {
            $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
         $this->load->view('penjualan_manual/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $allarea = $this->session->userdata('allarea');
         $iuser   = $this->session->userdata('user_id');
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);

         if($allarea=='t'){
            $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
         }
         else{
            $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
         $this->load->view('penjualan_manual/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea   = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/customer/'.$iarea.'/';
         $query = $this->db->query(" select i_customer from tr_customer where i_area = '$iarea' order by i_customer", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$iarea);
         $this->load->view('penjualan_manual/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea   = $this->uri->segment(4);
         $data['iarea']=$iarea;
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/customer/'.$iarea.'/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);

         $query = $this->db->query(" select i_customer from tr_customer where i_area = '$iarea' and (upper(e_customer_name) like '%$cari%'
                                     or upper(i_customer) like '%$cari%') order by i_customer", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_cust');
         $data['isi']=$this->mmaster->caricustomer($config['per_page'],$this->uri->segment(5),$iarea,$cari);
         $this->load->view('penjualan_manual/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carisalesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea   = $this->uri->segment(4);
         $data['iarea']=$iarea;
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/salesman/'.$iarea.'/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);

         $query = $this->db->query(" select i_salesman from tr_customer_salesman where i_area = '$iarea' and (upper(e_salesman_name) like '%$cari%'
                                     or upper(i_salesman) like '%$cari%') order by i_salesman", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_cust');
         $data['isi']=$this->mmaster->carisalesman($config['per_page'],$this->uri->segment(5),$iarea,$cari);
         $this->load->view('penjualan_manual/vjenisby', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu49')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea   = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/penjualan_manual/cform/salesman/'.$iarea.'/';
         $query = $this->db->query(" select distinct(i_salesman) from tr_customer_salesman where i_area = '$iarea' order by i_salesman", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $this->load->model('penjualan_manual/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(5),$iarea);
         $this->load->view('penjualan_manual/vjenisby', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
