<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rekapspb');
			$data['dfrom']='';
			$data['dto']='';				
			$this->load->view('rekapspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rekapspb');
			$this->load->view('rekapspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/rekapspb/cform/area/'.$baris.'/sikasep/';
      		$iuser   = $this->session->userdata('user_id');
      		$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      		$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('rekapspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			/*$config['base_url'] = base_url().'index.php/rekapspb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);*/

			$baris   = $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
			$config['base_url'] = base_url().'index.php/rekapspb/cform/cariarea/'.$baris.'/'.$cari.'/';
			  else
			$config['base_url'] = base_url().'index.php/rekapspb/cform/cariarea/'.$baris.'/sikasep/';

			/*if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}*/

			$iuser   	= $this->session->userdata('user_id');
			$query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('rekapspb/mmaster');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('rekapspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$status  = $this->session->userdata('status');
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/rekapspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$this->load->model('rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('rekapspb');
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$status);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('rekapspb/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rekap()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml  = $this->input->post('jml', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto  = $this->input->post('dto', TRUE);
			$this->db->trans_begin();
#      $dspmb='2015-01-31';#date('Y-m-d');
      $dspmb=date('Y-m-d');
      $tmp=explode("-",$dspmb);
      $th=$tmp[0];
      $bl=$tmp[1];
      $dt=$tmp[2];
      $thbl=substr($th,2,2).$bl;
      $ispmb  = '';
      $ispmbx = '';
			$this->load->model('rekapspb/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb     = $this->input->post('ispb'.$i, TRUE);
					$eareaname= $this->input->post('eareaname'.$i, TRUE);
					$iarea    = $this->input->post('iarea'.$i, TRUE);
          if($ispmb=='')$ispmb=$this->mmaster->runningnumberspmb($thbl);
          if($ispmbx==''){
#            $eremark='Rekap SPB';
            $this->mmaster->insertheader($ispmb, $dspmb, $iarea, null);
          }
					$this->mmaster->updatespb($ispb,$iarea,$ispmb);
          $this->mmaster->insertdetail($ispb,$iarea,$ispmb);
          $ispmbx=$ispmb;
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
  
        $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }

		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $data['uri']	= $this->session->userdata('printeruri');
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Input SPMB (rekap-spb) Area:'.$iarea.' No:'.$ispmb;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $ispmb;
				$this->load->view('nomor',$data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
