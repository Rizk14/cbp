<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu281')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transoputksupp');
			$data['iperiode']	= '';
#			$data['iarea']	  = '';
			$this->load->model('transoputksupp/mmaster');
			$data['list_supplier'] = $this->mmaster->get_supplier();
			$this->load->view('transoputksupp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu281')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			// 01-10-2014
			$i_supplier	= $this->input->post('i_supplier');
			$areanya	= $this->input->post('area');
#      $iarea = $this->input->post('iarea');
			if($iperiode=='' && $i_supplier == '' && $areanya == ''){
				$iperiode=$this->uri->segment(4);
				$i_supplier=$this->uri->segment(5);
				$areanya=$this->uri->segment(6);
		    }
#			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transoputksupp');
			$data['iperiode']	= $iperiode;
			$data['i_supplier']	= $i_supplier;
			$data['areanya']	= $areanya;
      $tahun=substr($iperiode,0,4);      
#			$data['iarea']	= $iarea;
      $per=substr($iperiode,2,4);
      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
      $db_file = 'beli/po_imma.dbf';
      $dbase_definition = array (
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('KODEAREA',  CHARACTER_FIELD, 3),
         array ('NOSPMB',  CHARACTER_FIELD,  6),
         array ('TGLSPMB',  DATE_FIELD),
         array ('NODOK',  CHARACTER_FIELD, 10),
         array ('TGLDOK',  DATE_FIELD),
         array ('KODEPROD',  CHARACTER_FIELD,  9),
         array ('JUMLAH', NUMBER_FIELD, 6, 0),
         array ('HARGASAT', NUMBER_FIELD, 8, 0),
         array ('MOTIF', CHARACTER_FIELD, 40),
         array ('TGLJT',  DATE_FIELD),
         array ('TEMPO',  NUMBER_FIELD,3, 0),
         array ('KET',  CHARACTER_FIELD,  20,0),
         array ('CETAK',  CHARACTER_FIELD,  2),
         array ('BTSKIRIM',  NUMBER_FIELD,  3, 0),
         array ('STAT',  CHARACTER_FIELD, 2),
         array ('TGLPROSES',  DATE_FIELD),
         array ('JAM', CHARACTER_FIELD, 8),
         array ('BATAL', BOOLEAN_FIELD)
      );
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $dicari="OP-".$per."-%";
      $sql	  = " select a.i_supplier, a.i_op, a.d_op, a.d_op + cast(a.n_top_length as integer) as d_jt,
                    a.i_reff, d_reff, a.n_delivery_limit, a.n_top_length, a.i_area, 
                    a.n_op_print, a.i_op_status, a.e_op_remark, a.d_entry, a.f_op_cancel,
                    b.i_product, b.n_order, b.n_delivery, b.v_product_mill, b.e_product_name,
                    c.e_product_motifname
                    from tm_op a, tm_op_item b, tr_product_motif c
                    where a.i_op=b.i_op and a.i_op like '$dicari' and to_char(a.d_op,'yyyymm')='$iperiode' 
                    and b.i_product=c.i_product and b.i_product_motif=c.i_product_motif
                    AND a.i_supplier = '$i_supplier' ";
        if ($areanya == '1')
			$sql.= " AND a.i_area <> 'PB' ";
		else if ($areanya == '2')
			$sql.= " AND a.i_area = 'PB' ";
#and a.f_op_cancel='f' 
    $query=$this->db->query($sql);
    if ($query->num_rows() > 0){
        foreach($query->result() as $rowsj){
                $kodesupp         = $rowsj->i_supplier;
                $nodok            = substr($rowsj->i_op,8,6).'/ '.$rowsj->i_area;
                $tgldok           = substr($rowsj->d_op,0,4).substr($rowsj->d_op,5,2).substr($rowsj->d_op,8,2);
                $nospmb           = substr($rowsj->i_reff,9,6);
                $tglspmb          = substr($rowsj->d_reff,0,4).substr($rowsj->d_reff,5,2).substr($rowsj->d_reff,8,2);
                $tgljt            = substr($rowsj->d_jt,0,4).substr($rowsj->d_jt,5,2).substr($rowsj->d_jt,8,2);
                $btskirim         = $rowsj->n_delivery_limit;
                $top              = $rowsj->n_top_length;
                $kodearea         = $rowsj->i_area;
                $cetak            = $rowsj->n_op_print;
                $stat             = $rowsj->i_op_status;
                $keterangan       = $rowsj->e_op_remark;
                $tglproses        = substr($rowsj->d_entry,0,4).substr($rowsj->d_entry,5,2).substr($rowsj->d_entry,8,2);
                $jam              = substr($rowsj->d_entry,11,2).":".substr($rowsj->d_entry,14,2).":".substr($rowsj->d_entry,17,2);
            if($rowsj->f_op_cancel=='f'){ 
                $batal='F';
            }
            else{ 
                $batal='T';
            }
            $kodeprod         = $rowsj->i_product.'00';
            $jumlah           = $rowsj->n_order;
            $jkirim           = $rowsj->n_delivery;
            $hargasat         = $rowsj->v_product_mill;
            $motif            = $rowsj->e_product_motifname;
            
            $isi = array ($kodesupp,$kodearea,$nospmb,$tglspmb,$nodok,$tgldok,$kodeprod,$jumlah,
                        $hargasat,$motif,$tgljt,$top,$keterangan,$cetak,$btskirim,$stat,$tglproses,
                        $jam,$batal);
            dbase_add_record ($id, $isi) or die ("Gagal transfer ke file <i>$db_file</i>."); 
        }
      }
      dbase_close($id);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer OP ke file dbf:'.$db_file;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu281')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
